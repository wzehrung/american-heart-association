﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using _3CiDataManagerSend;

namespace _3CiGenerateReminders
{
    public partial class AHAGenerateReminders : ServiceBase
    {
        public AHAGenerateReminders()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _3CiDataManagerSend.SendAPI cl = new SendAPI();
            cl.ServiceName = "AHAGenerateReminders";
            cl.generateReminders();
        }

        protected override void OnStop()
        {
        }
    }
}
