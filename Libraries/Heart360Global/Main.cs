﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GroupManager;

namespace Heart360Global
{
    public class Main
    {
        static Dictionary<string, LocaleHolder> listLocaleHolder = new Dictionary<string, LocaleHolder>();
        static bool bIsLoaded = false;
        static object objLock = new object();

        private static void _loadData()
        {
            if (!listLocaleHolder.ContainsKey(System.Threading.Thread.CurrentThread.CurrentUICulture.Name))
            {
                bIsLoaded = false;
            }

            if (!bIsLoaded)
            {
                lock (objLock)
                {
                    if (!bIsLoaded)
                    {
                        _parseAndLoadData();
                        bIsLoaded = true;
                    }
                }
            }
        }

        private static void _parseAndLoadData()
        {
            if (listLocaleHolder.ContainsKey(System.Threading.Thread.CurrentThread.CurrentUICulture.Name))
            {
                listLocaleHolder.Remove(System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
            }

            LocaleHolder objLocaleHolder = new LocaleHolder();

            objLocaleHolder.PageList = AHAPage.GetList();
            objLocaleHolder.GroupInfoList = GroupHelper.GetList();


            listLocaleHolder.Add(System.Threading.Thread.CurrentThread.CurrentUICulture.Name, objLocaleHolder);
        }

        public static AHAPage GetPageDetailsFromUrl(string strUrl)
        {
            _loadData();

            AHAPage objPage = null;
            if (listLocaleHolder[System.Threading.Thread.CurrentThread.CurrentUICulture.Name].PageList.ContainsKey(strUrl.ToLower()))
            {
                objPage = listLocaleHolder[System.Threading.Thread.CurrentThread.CurrentUICulture.Name].PageList[strUrl.ToLower()] as AHAPage;
            }

            return objPage;
        }

        public static List<GroupInfo> GetSystemCreatedGroups()
        {
            _loadData();

            return listLocaleHolder[System.Threading.Thread.CurrentThread.CurrentUICulture.Name].GroupInfoList;
        }

        public static GroupInfo GetGroupByTypeID(GroupType eGroupType)
        {
            _loadData();

            return listLocaleHolder[System.Threading.Thread.CurrentThread.CurrentUICulture.Name].GroupInfoList.Where(g => g.TypeID == eGroupType.ToString()).SingleOrDefault();
        }
    }
}
