﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GroupManager;

namespace Heart360Global
{
    public class LocaleHolder
    {
        public Dictionary<string, AHAPage> PageList
        {
            get;
            internal set;
        }

        public List<GroupInfo> GroupInfoList
        {
            get;
            internal set;
        }
    }
}
