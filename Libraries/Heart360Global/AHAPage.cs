﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using System.Xml;
using GRCBase;

namespace Heart360Global
{
    public class AHAPage
    {
        public string Url
        {
            get;
            set;
        }

        public string PageTitle
        {
            get;
            set;
        }

        public string ShortTitle
        {
            get;
            set;
        }

        public string MainTabID
        {
            get;
            set;
        }

        public bool IsProviderPage
        {
            get;
            set;
        }

        public bool IsFullWidthPage
        {
            get;
            set;
        }

        public bool ShowCircle
        {
            get;
            set;
        }

        public bool HideRightColumn
        {
            get;
            set;
        }

        public static Dictionary<string, AHAPage> GetList()
        {
            Dictionary<string, AHAPage> dicPageTitles = new Dictionary<string, AHAPage>();

            System.Xml.XmlDocument document = new System.Xml.XmlDocument();

            string xmlFilePath = string.Format("{0}PageTitle.{1}.xml", AHAAppSettings.XmlFilesDirectory, System.Threading.Thread.CurrentThread.CurrentUICulture.Name);

            System.IO.FileStream fileStream = System.IO.File.OpenRead(xmlFilePath);
            document.Load(fileStream);

            XmlNodeList nodeList = document.SelectNodes("Pages/Page");
            foreach (XmlNode pageNode in nodeList)
            {
                AHAPage objPage = new AHAPage
                {
                    Url = XmlUtility.GetAttributeStringValue(pageNode, "Url"),
                    PageTitle = XmlUtility.GetAttributeStringValue(pageNode, "Title"),
                    ShortTitle = XmlUtility.GetAttributeStringValue(pageNode, "ShortTitle"),
                    MainTabID = XmlUtility.GetAttributeStringValue(pageNode, "MainTabID"),
                    IsProviderPage = !string.IsNullOrEmpty(XmlUtility.GetAttributeStringValue(pageNode, "IsProviderPage")) ? bool.Parse(XmlUtility.GetAttributeStringValue(pageNode, "IsProviderPage")) : false,
                    IsFullWidthPage = !string.IsNullOrEmpty(XmlUtility.GetAttributeStringValue(pageNode, "IsFullWidthPage")) ? bool.Parse(XmlUtility.GetAttributeStringValue(pageNode, "IsFullWidthPage")) : false,
                    ShowCircle = !string.IsNullOrEmpty(XmlUtility.GetAttributeStringValue(pageNode, "ShowCircle")) ? bool.Parse(XmlUtility.GetAttributeStringValue(pageNode, "ShowCircle")) : false,
                    HideRightColumn = !string.IsNullOrEmpty(XmlUtility.GetAttributeStringValue(pageNode, "HideRightColumn")) ? bool.Parse(XmlUtility.GetAttributeStringValue(pageNode, "HideRightColumn")) : false
                };

                dicPageTitles.Add(objPage.Url.ToLower(), objPage);
            }

            fileStream.Close();
            fileStream.Dispose();

            return dicPageTitles;
        }
    }
}
