﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GroupManager;
using System.Xml.XPath;
using AHACommon;

namespace Heart360Global
{
    public class GroupHelper
    {
        public static List<GroupInfo> GetList()
        {
            List<GroupInfo> m_List = new List<GroupInfo>();
            string xmlFilePath = string.Format("{0}SystemCreatedGroups.{1}.xml", AHAAppSettings.XmlFilesDirectory, System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
            XDocument document = XDocument.Load(xmlFilePath);

            var queryResult = document.Descendants("Group").Select(Group => new GroupInfo
            {
                TypeID = Group.Attribute("TypeID") != null ? Group.Attribute("TypeID").Value : string.Empty,
                Name = Group.XPathSelectElement("Name").Value,
                Description = Group.XPathSelectElement("Description").Value,
                AddRemovePatientsAutomatically = bool.Parse(Group.XPathSelectElement("AddRemovePatientsAutomatically").Value),
                GroupRule = Group.XPathSelectElement("GroupRule").ToString()
            });

           return queryResult.ToList();
        }
    }
}
