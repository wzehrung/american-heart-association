﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web ;
using System.Configuration ;

namespace AHAProvider
{
    [Serializable]
    public class PersonRecord
    {
        public Guid PersonGUID
        {
            get;
            set;
        }

        public Guid RecordGUID
        {
            get;
            set;
        }

        public bool HasGivenOffineAccess
        {
            get;
            set;
        }
    }

}
