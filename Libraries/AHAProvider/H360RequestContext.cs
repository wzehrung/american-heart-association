﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Globalization;

using GRCBase;          // for UrlUtility
using AHAHelpContent;   // for ProviderDetails



namespace AHAProvider
{
    public class H360RequestContext
    {
        public HttpContext CurrentContext
        {
            get;
            set;
        }

        public string ContextUrl
        {
            get;
            set;
        }

        public ProviderDetails ProviderDetails
        {
            get;
            set;
        }

        public List<AHAHelpContent.Patient> PatientList
        {
            get;
            set;
        }

        public int? TotalLoginCount
        {
            get;
            set;
        }

        public AHAHelpContent.Language SelectedLanguage
        {
            get;
            set;
        }

        private const string contextKey = "H360RequestContext";

        public H360RequestContext(System.Web.HttpContext currentContext)
        {
            CurrentContext = currentContext;
        }

        public static H360RequestContext GetContext()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new System.Exception("This call is only valid during an HTTP request");
            }

            System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

            if (currentContext.Items[contextKey] == null)
            {
                return null;
            }

            return currentContext.Items[contextKey] as H360RequestContext;
        }

        public static void PopulateContextData()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new System.Exception("This call is only valid during an HTTP request");
            }

            System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

            if (currentContext.Items[contextKey] != null)
            {
                throw new System.Exception("H360RequestContext already populated");
            }

            currentContext.Items[contextKey] = new H360RequestContext(System.Web.HttpContext.Current);


            H360RequestContext objH360RequestContext = currentContext.Items[contextKey] as H360RequestContext;
            objH360RequestContext.DoPopulate();
        }

        private void DoPopulate()
        {
            ContextUrl = UrlUtility.FullCurrentPageUrl;

            Uri uriContext = new Uri(ContextUrl);

            if (AHAProvider.SessionInfo.IsProviderLoggedIn())
            {
                ProviderContext objPContext = AHAProvider.SessionInfo.GetProviderContext();
                if (objPContext != null)
                {
                    ProviderDetails = ProviderDetails.FindByProviderID(objPContext.ProviderID);

                    PatientList = AHAHelpContent.Patient.FindAllByProviderID(ProviderDetails.ProviderID);

                    TotalLoginCount = AHAHelpContent.ProviderLoginDetails.GetProviderLoginCount(ProviderDetails.ProviderID);

                    LanguageHelper.SetLocaleAndCulture(ProviderDetails.LanguageLocale);

                    SelectedLanguage = new Language { LanguageID = ProviderDetails.DefaultLanguageID, LanguageLocale = ProviderDetails.LanguageLocale };
                }
            }

            if (SelectedLanguage == null || !AHAProvider.SessionInfo.IsProviderLoggedIn() )
            {
                LanguageHelper.SetLocaleAndCulture(LanguageHelper.GetSelectedLanguageLocaleForVisitor());
                SelectedLanguage = AHAHelpContent.Language.GetLanguageByLocale(LanguageHelper.GetSelectedLanguageLocaleForVisitor());
            }
        }
    }
}
