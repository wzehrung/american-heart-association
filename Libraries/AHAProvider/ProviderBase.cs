﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

// PJB commented this out
//using Microsoft.Health.Web;

// TODO
// a) configurable redirects instead of hard-coded constants.
// b) _EnsureProviderAccess

namespace AHAProvider
{
    public class ProviderBase : Page
    {
        public Guid? RecordID
        {
            get;
            set;
        }

        public Guid? PersonID
        {
            get;
            set;
        }

        public void ForceInit()
        {
            _EnsureProviderAccess();

            _ProcessRequest();
        }

        protected override void OnPreInit(EventArgs e)
        {
            _EnsureProviderAccess();

            _ProcessRequest();
            
            base.OnPreInit(e);
        }

        private void _ProcessRequest()
        {
            PersonRecord objPR = H360Utility.GetPersonRecordDetailsFromRequest(false);

            if( objPR != null )
            {
                PersonID = objPR.PersonGUID ;
                RecordID = objPR.RecordGUID ;
            

                AHAProvider.ProviderContext objProContext = AHAProvider.SessionInfo.GetProviderContext();
                bool bCheckForOfflineAccess = false;

                if (objProContext.PersonRecordItem == null)
                {
                    objProContext.PersonRecordItem = objPR;
                    bCheckForOfflineAccess = true;
                }
                else
                {
                    if (objPR.PersonGUID != objProContext.PersonRecordItem.PersonGUID ||
                        objPR.RecordGUID != objProContext.PersonRecordItem.RecordGUID)
                    {
                        objProContext.PersonRecordItem = objPR;
                        bCheckForOfflineAccess = true;
                    }
                }

                if (bCheckForOfflineAccess)
                {
                    objProContext.PersonRecordItem.HasGivenOffineAccess = H360Utility.HasPatientGivenOfflineAccessToApp(PersonID.Value, RecordID.Value);
                }

                if (!objProContext.PersonRecordItem.HasGivenOffineAccess)
                {
                    // PJB - this should not be a hard-coded url. Needs to be configurable!!!
                    //System.Web.HttpContext.Current.Response.Redirect(string.Format("{0}", AHAAPI.Provider.ProviderPageUrl.PATIENT_DATA_ACCESS_DENIED), true);
                }

                if (PersonID.HasValue && RecordID.HasValue)
                {
                    AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserOfflineHealthRecordGUID(RecordID.Value);

                    if (objPatient == null)
                    {
                        throw new InvalidProgramException(string.Format("Invalid Patient record guid - {0}!", RecordID.Value));
                    }

                    bool bIsValidPatientForProvider = AHAHelpContent.ProviderDetails.IsValidPatientForProvider(objPatient.UserHealthRecordID, objProContext.ProviderID);

                    if (!bIsValidPatientForProvider)
                    {
                        //the provider does not have access to this record
                        // PJB - this should not be a hard-coded url. Needs to be configurable!!!
                        // System.Web.HttpContext.Current.Response.Redirect(string.Format("{0}", AHAAPI.Provider.ProviderPageUrl.PATIENT_DATA_ACCESS_DENIED), true);
                    }
                }
            }
        }

        private void _EnsureProviderAccess()
        {
            if (!SessionInfo.IsProviderLoggedIn())
            {
                // PJB - this seems to be related to Patient Portal?!
                /**
                Microsoft.Health.PersonInfo objPersonInfo = Microsoft.Health.Web.WebApplicationUtilities.LoadPersonInfoFromCookie(System.Web.HttpContext.Current);

                if (objPersonInfo != null)
                {
                    Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);
                }
                else
                {
                    //System.Web.HttpContext.Current.Response.Redirect(string.Format("{0}?ReturnUrl={1}", AHAAPI.Provider.ProviderPageUrl.LOGIN, System.Web.HttpContext.Current.Server.UrlEncode(GRCBase.UrlUtility.FullCurrentPageUrlWithQV)));
                    string strRedirect = "<script>if (window.opener != null) {window.opener.location.href = '" + AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP + "';window.close();}else {window.location.href = '"+ AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP + "';}</script>";
                    System.Web.HttpContext.Current.Response.Write(strRedirect);
                    //System.Web.HttpContext.Current.Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT);
                    System.Web.HttpContext.Current.Response.End();
                    return;
                }
                **/
            }
        }
    }
}
