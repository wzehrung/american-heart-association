﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Web;

using GRCBase;  // for CookieHelper

namespace AHAProvider
{

    public class LanguageHelper
    {
        static string __CookieKey = "Heart360LocaleString";
        static int __CookieValidaityInDays = 3;
        public static void SetLocaleAndCulture(string strLanguageLocale)
        {
            SetLocaleInfo(strLanguageLocale);
            CultureInfo objCultureInfo = new CultureInfo(strLanguageLocale);
            System.Threading.Thread.CurrentThread.CurrentUICulture = objCultureInfo;
            System.Threading.Thread.CurrentThread.CurrentCulture = objCultureInfo;
        }

        public static void SetLocaleInfo(string strLanguageLocale)
        {
            HttpCookie objCookie = CookieHelper.GetCookie(__CookieKey);
            if (objCookie == null || (objCookie != null && objCookie.Value != strLanguageLocale))
            {
                CookieHelper.SetCookie(__CookieKey, strLanguageLocale, DateTime.Now.AddDays(__CookieValidaityInDays));
            }            
        }

        public static string GetSelectedLanguageLocaleForVisitor()
        {
            HttpCookie objCookie = CookieHelper.GetCookie(__CookieKey);
            if (objCookie != null)
            {
                return objCookie.Value;
            }
            else
            {
                return AHAHelpContent.Language.GetDefaultLanguage().LanguageLocale;
            }
        }   
    }
}
