﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AHAProvider
{
    [Serializable]
    public class ProviderContext
    {
        public bool IsLoggedIn
        {
            get;
            set;
        }

        public int ProviderID
        {
            get;
            set;
        }

        public PersonRecord PersonRecordItem
        {
            get;
            set;
        }

        public string LanguageLocale
        {
            get;
            set;
        }

        public int? LanguageID
        {
            get;
            set;
        }

        public int? CampaignID
        {
            get;
            set;
        }

        public string CampaignUrl
        {
            get;
            set;
        }

        public string ProviderCode
        {
            get;
            set;
        }
    }
}
