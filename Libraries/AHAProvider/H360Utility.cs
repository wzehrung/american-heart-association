﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.XPath;
using System.Configuration;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.Health;
using Microsoft.Health.Web;
using Microsoft.Health.ItemTypes;

using AHAHelpContent;

// TODO
// a) redirection in HasPatientGivenOfflineAccessToApp

namespace AHAProvider
{
    

    public class H360Utility
    {
        
        public static int GetCurrentLanguageID()
        {
            H360RequestContext objContext = H360RequestContext.GetContext();
            if (objContext != null)
            {
                return objContext.SelectedLanguage.LanguageID;
            }

            return AHAHelpContent.Language.GetDefaultLanguage().LanguageID;
        }

        public static void AddImmediateExpiryHeadersToResponse()
        {
            if (System.Web.HttpContext.Current.Request.HttpMethod.ToLower() == "get")
            {
                System.Web.HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
                System.Web.HttpContext.Current.Response.Expires = 0;
                System.Web.HttpContext.Current.Response.Cache.SetNoStore();
                System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
            }
            return;
        }

        public static bool HasPatientGivenAccessForFileType(AuthenticatedConnection conn, Guid hvpersonid, Guid hvrecordid, out bool bIsOnlineConnection)
        {
            HealthRecordAccessor accessor = new HealthRecordAccessor(conn, hvrecordid);

            IList<Guid> idlist = new List<Guid>();
            idlist.Add(File.TypeId);

            Collection<HealthRecordItemTypePermission> list = accessor.QueryPermissions(idlist);

            bIsOnlineConnection = true;
            bool bHasOfflineAccess = true;

            if (list.Count > 0)
            {
                if (list[0].OfflineAccessPermissions != HealthRecordItemPermissions.All)
                {
                    bHasOfflineAccess = false;
                }
                if (list[0].OnlineAccessPermissions != HealthRecordItemPermissions.All)
                {
                    bIsOnlineConnection = false;
                }
            }
            else
            {
                bHasOfflineAccess = false;
                bIsOnlineConnection = false;
            }

            return bHasOfflineAccess || bIsOnlineConnection;
        }

        
        public static bool HasPatientGivenOfflineAccessToApp(Guid hvpersonid, Guid hvrecordid)
        {
            OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
            offlineConn.Authenticate();
            HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);

            IList<Guid> idlist = new List<Guid>();
            idlist.Add(Microsoft.Health.ItemTypes.BloodPressure.TypeId);
            idlist.Add(Personal.TypeId);

            //System.Web.HttpContext.Current.Session["RecordId"] = hvrecordid;

            Collection<HealthRecordItemTypePermission> list  = null;
            try
            {
                list = accessor.QueryPermissions(idlist);
            }
            catch (Microsoft.Health.HealthServiceException hse)
            {
                if (hse.ErrorCode == HealthServiceStatusCode.InvalidRecordState ||
                    hse.ErrorCode == HealthServiceStatusCode.AccessDenied)
                {
                    if (AHAProvider.SessionInfo.IsProviderLoggedIn())
                    {
                        //Remove from db  (with some comment)
                        //
                        // PJB removed the following two lines and added the return false ;
                        //AHAHelpContent.ProviderDetails.DisconnectPatientOrProvider(AHAProvider.SessionInfo.GetProviderContext().ProviderID, hvrecordid);
                        //System.Web.HttpContext.Current.Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);

                        return false;
                    }
                }
                    
            }

            bool bHasOfflineAccess = true;

            foreach (HealthRecordItemTypePermission item in list)
            {
                if (item.OfflineAccessPermissions != HealthRecordItemPermissions.All)
                {
                    bHasOfflineAccess = false;
                    break;
                }
            }

            //if (context != null)
            //    context.HasUserGivenOfflineAccess = bHasOfflineAccess;

            return bHasOfflineAccess;
        }

        public static bool IsGUID(string strGUIDCandidate)
        {
            try
            {
                Guid myGuid = new Guid(strGUIDCandidate);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static PersonRecord GetPersonRecordDetailsFromRequest( bool _throwError )
        {
            string strPersonGUID = System.Web.HttpContext.Current.Request["pg"];
            string strRecordGUID = System.Web.HttpContext.Current.Request["rg"];

            PersonRecord outPR = null;

            if (IsGUID(strPersonGUID) && IsGUID(strRecordGUID))
            {
                outPR = new PersonRecord { PersonGUID = new Guid(strPersonGUID), RecordGUID = new Guid(strRecordGUID) };
            }
            else if( _throwError )
            {
                throw new ArgumentException("No Record ID & Person ID found!");
            }

            return outPR;
        }

        

        

        #region campaign
        private const string sessionKeyCampaign = "Heart360Campaign";

        public static void SetCampaignSession(int iCampaignID)
        {
            System.Web.HttpContext.Current.Session[sessionKeyCampaign] = iCampaignID;
        }

        public static int? GetCurrentCampaignID()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[sessionKeyCampaign] != null)
            {
                return (int)System.Web.HttpContext.Current.Session[sessionKeyCampaign];
            }
            return null;
        }

        private const String sessionKeyCampaignUrl = "Heart360CampaignUrl";

        public static void SetCampaignSessionUrl(String iCampaignUrl)
        {
            System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl] = iCampaignUrl;
        }

        public static String GetCurrentCampaignUrl()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl] != null)
            {
                return (string)System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl];
            }
            return null;
        }

        public static void ResetCampaignSession()
        {
            System.Web.HttpContext.Current.Session[sessionKeyCampaign] = null;
            System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl] = null;
        }
        #endregion

        

    }

    
}
