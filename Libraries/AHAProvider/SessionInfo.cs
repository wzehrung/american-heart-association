﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AHAHelpContent;   // for ProviderDetails

namespace AHAProvider
{
    [Serializable]
    public class ImageItem
    {
        public byte[] ImageByteArray
        {
            get;
            set;
        }

        public bool IsResized
        {
            get;
            set;
        }

        public bool HasHVImage
        {
            get;
            set;
        }

        public Guid RecordID
        {
            get;
            set;
        }
    }

    public class SessionInfo
    {
        // See AHACommon.SessionKeys for key constants

        public static void SetProviderSession(ProviderDetails objProviderDetails)
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderIdentityContext] = new ProviderContext { IsLoggedIn = true, ProviderID = objProviderDetails.ProviderID, LanguageID = objProviderDetails.DefaultLanguageID, LanguageLocale = objProviderDetails.LanguageLocale, ProviderCode = objProviderDetails.ProviderCode };
        }

        public static bool IsProviderLoggedIn()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderIdentityContext] != null)
            {
                return true;
            }
            return false;
        }

        public static ProviderContext GetProviderContext()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderIdentityContext] != null)
            {
                return System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderIdentityContext] as ProviderContext;
            }
            return null;
        }

        public static void ResetProviderSession()
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderIdentityContext] = null;
        }

        public static void AddPatientImageToList(ImageItem obj)
        {
            if (System.Web.HttpContext.Current.Session != null)
            {
                List<ImageItem> list = new List<ImageItem>();
                if (System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientImageList] != null)
                {
                    list = System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientImageList] as List<ImageItem>;
                    if (list.Where(i => i.RecordID == obj.RecordID).SingleOrDefault() == null)
                    {
                        list.Add(obj);
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientImageList] = list;
                    list.Add(obj);
                }
            }
        }

        public static ImageItem GetPatientImage(Guid gRecordID)
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientImageList] != null)
            {
                List<ImageItem> list = System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientImageList] as List<ImageItem>;
                return list.Where(i => i.RecordID == gRecordID).SingleOrDefault();
            }
            return null;
        }

        public static void ResetPatientImageList()
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientImageList] = null;
        }

            // Methods dealing with the Provider's Current Patient (Single Participant page).
        public static void SetProviderCurrentPatient(string patGuid)
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderCurrentPatient] = patGuid;
        }

        public static string GetProviderCurrentPatient()
        {
            return (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderCurrentPatient] != null) ? System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderCurrentPatient].ToString() : string.Empty;
        }

        public static void ResetProviderCurrentPatient()
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyProviderCurrentPatient] = null;
        }

 #region campaign
        
        public static void SetCampaignSession(int iCampaignID)
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaign] = iCampaignID;
        }

        public static int? GetCurrentCampaignID()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaign] != null)
            {
                return (int)System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaign];
            }
            return null;
        }

        

        public static void SetCampaignSessionUrl(String iCampaignUrl)
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaignUrl] = iCampaignUrl;
        }

        public static String GetCurrentCampaignUrl()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaignUrl] != null)
            {
                return (string)System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaignUrl];
            }
            return null;
        }

        public static void ResetCampaignSession()
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaign] = null;
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyCampaignUrl] = null;
        }
#endregion

        public static void SetPatientInvitationId(string value)
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientInvitationId] = value;
        }

        public static string GetPatientInvitationId()
        {
            return (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientInvitationId] != null) ? System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyPatientInvitationId].ToString() : string.Empty;
        }


    }
}
