﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;


namespace AHABusinessLogic
{
    public enum LastReadingChange
    {
        [Description("Increased")]
        Increased,
        [Description("Decreased")]
        Decreased,
        [Description("Not Changed")]
        NotChanged
    }

    public abstract class UserHealthDataDetails
    {
        public virtual bool HideBadge
        {
            get;
            set;
        }
        public virtual bool HasGoal
        {
            get;
            set;
        }
        public virtual bool HasThreeBoxes
        {
            get;
            set;
        }
        public virtual object[] LatestValue
        {
            get;
            set;
        }
        public virtual object[] LatestGoal
        {
            get;
            set;
        }
        public virtual AHACommon.PatientTrackers.TrackerType TrackerType
        {
            get;
            set;
        }
        public virtual string TrackerTypeString
        {
            get;
            set;
        }
        public virtual LastReadingChange LastReadingChange
        {
            get;
            set;
        }

        public string LastReadingChangeString
        {
            get
            {
                return GRCBase.MiscUtility.GetComponentModelPropertyDescription(LastReadingChange);
            }
        }
        public abstract RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading();
        public abstract RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal();
    }
}
