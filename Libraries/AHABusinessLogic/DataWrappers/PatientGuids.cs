﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AHABusinessLogic
{
    public class PatientGuids
    {
        public Guid PersonID
        {
            get;
            set;
        }


        public Guid RecordID
        {
            get;
            set;
        }

        //DEPRECATED
        public static PatientGuids CurrentPatientGuids
        {
            get
            {
                return new PatientGuids
                {
                    PersonID = (System.Web.HttpContext.Current.Handler as PatientInfo).PersonGUID,
                    RecordID = (System.Web.HttpContext.Current.Handler as PatientInfo).RecordGUID
                };
            }
        }
    }
}
