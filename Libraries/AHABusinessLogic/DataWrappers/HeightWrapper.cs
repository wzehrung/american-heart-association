﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using System.Collections.Generic;

namespace AHABusinessLogic
{
    public class HeightWrapper
    {
        /// <summary>
        /// Gets the latest item
        /// </summary>        /// 
        /// <returns>HVManager.HeightDataManager.HeightItem</returns>
        public static HVManager.HeightDataManager.HeightItem GetLatestHeightItem(HVManager.AllItemManager hvMgr)
        {
            LinkedList<HVManager.HeightDataManager.HeightItem> objList = hvMgr.HeightDataManager.GetLatestNItems(1);
            if (objList != null && objList.Count > 0)
            {
                return objList.First.Value;
            }

            return null;
        }
    }
}
