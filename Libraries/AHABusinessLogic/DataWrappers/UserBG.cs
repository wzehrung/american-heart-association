﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using HVManager;
using GroupManager;

namespace AHABusinessLogic
{
    public class UserBG : UserHealthDataDetails
    {
        private List<GroupInfo> mCultureSpecificGroupList;

        public UserBG( HVManager.AllItemManager hvMgr, List<GroupInfo> cultureSpecificGroupList )
        {
            mCultureSpecificGroupList = cultureSpecificGroupList;

            //HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetDataForLast2Year();
            TrackerType = AHACommon.PatientTrackers.TrackerType.BloodGlucose;
            TrackerTypeString = GRCBase.MiscUtility.GetComponentModelPropertyDescription(AHACommon.PatientTrackers.TrackerType.BloodGlucose);
            IEnumerable<BloodGlucoseDataManager.BGItem> objItemList = hvMgr.BloodGlucoseDataManager.GetLatestNItems(2).Reverse();
            BloodGlucoseDataManager.BGItem objItemLatest = null;
            BloodGlucoseDataManager.BGItem objItemSecond = null;

            if (objItemList.Count() > 0)
            {
                objItemLatest = objItemList.ElementAt(0);
                LatestValue = new object[] { objItemLatest.Value.Value };
            }

            if (objItemList.Count() > 1)
            {
                objItemSecond = objItemList.ElementAt(1);
            }

            if (objItemLatest != null && objItemSecond != null)
            {
                if (objItemLatest.Value.Value > objItemSecond.Value.Value)
                {
                    LastReadingChange = LastReadingChange.Increased;
                }
                else if (objItemLatest.Value.Value == objItemSecond.Value.Value)
                {
                    LastReadingChange = LastReadingChange.NotChanged;
                }
                else
                {
                    LastReadingChange = LastReadingChange.Decreased;
                }
            }
            else
            {
                LastReadingChange = LastReadingChange.NotChanged;
            }

            HasThreeBoxes = false;
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading()
        {
            if (LatestValue != null)
            {
                RiskLevelHelper.RiskLevelBloodGlucose objRiskLevelItem = new RiskLevelHelper.RiskLevelBloodGlucose(mCultureSpecificGroupList, Convert.ToDouble(LatestValue[0]));
                return objRiskLevelItem.GetRiskLevelResult();
            }
            return null;            
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal()
        {
            if (LatestGoal != null)
            {
                RiskLevelHelper.RiskLevelBloodGlucose objRiskLevelGoal = new RiskLevelHelper.RiskLevelBloodGlucose(mCultureSpecificGroupList, Convert.ToDouble(LatestValue[0]));
                return objRiskLevelGoal.GetRiskLevelResult();
            }
            return null;  
        }
    }
}
