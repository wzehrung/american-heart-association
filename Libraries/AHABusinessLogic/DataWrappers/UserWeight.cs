﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using HVManager;
using GroupManager;

namespace AHABusinessLogic
{
    public class UserWeight : UserHealthDataDetails
    {
        private List<GroupInfo> mCultureSpecificGroupList;
        double? dBMI;
        double? dBMIGoal;

        public UserWeight(HVManager.AllItemManager hvMgr, List<GroupInfo> cultureSpecificGroupList )
        {
            mCultureSpecificGroupList = cultureSpecificGroupList;

            //HVHelper.HVManagerPatientBase.WeightDataManager.GetDataForLast2Year();
            TrackerType = AHACommon.PatientTrackers.TrackerType.Weight;
            TrackerTypeString = GRCBase.MiscUtility.GetComponentModelPropertyDescription(AHACommon.PatientTrackers.TrackerType.Weight);
            IEnumerable<WeightDataManager.WeightItem> objItemList = hvMgr.WeightDataManager.GetLatestNItems(2).Reverse();
            WeightGoalDataManager.WeightGoalItem objGoalItem = hvMgr.WeightGoalDataManager.Item;
            WeightDataManager.WeightItem objItemLatest = null;
            WeightDataManager.WeightItem objItemSecond = null;

            if (objItemList.Count() > 0)
            {
                objItemLatest = objItemList.ElementAt(0);
                LatestValue = new object[] { objItemLatest.CommonWeight.Value.ToPounds() };
            }

            if (objItemList.Count() > 1)
            {
                objItemSecond = objItemList.ElementAt(1);
            }

            if (objItemLatest != null && objItemSecond != null)
            {
                if (objItemLatest.CommonWeight.Value.ToPounds() > objItemSecond.CommonWeight.Value.ToPounds())
                {
                    LastReadingChange = LastReadingChange.Increased;
                }
                else if (objItemLatest.CommonWeight.Value.ToPounds() == objItemSecond.CommonWeight.Value.ToPounds())
                {
                    LastReadingChange = LastReadingChange.NotChanged;
                }
                else
                {
                    LastReadingChange = LastReadingChange.Decreased;
                }
            }
            else
            {
                LastReadingChange = LastReadingChange.NotChanged;
            }

            if (objGoalItem != null && objGoalItem.TargetValue != null)
            {
                HasGoal = true;
                LatestGoal = new object[] { objGoalItem.TargetValue.Value };
            }

            if (objItemLatest != null)
            {
                dBMI = AHABusinessLogic.VirtualCoach.UserSummary.GetBMIForCurrentRecordForWeight( hvMgr, objItemLatest.CommonWeight.Value.ToPounds());
            }

            if (objGoalItem != null && objGoalItem.TargetValue != null)
            {
                dBMIGoal = AHABusinessLogic.VirtualCoach.UserSummary.GetBMIForCurrentRecordForWeight(hvMgr, objGoalItem.TargetValue.Value);
            }

            HasThreeBoxes = true;
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading()
        {
            if (dBMI.HasValue)
            {
                RiskLevelHelper.RiskLevelBMI objRiskLevelItem = new RiskLevelHelper.RiskLevelBMI(mCultureSpecificGroupList, dBMI.Value);
                return objRiskLevelItem.GetRiskLevelResult();
            }
            return null;            
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal()
        {
            if (dBMIGoal.HasValue)
            {
                RiskLevelHelper.RiskLevelBMI objRiskLevelGoal = new RiskLevelHelper.RiskLevelBMI( mCultureSpecificGroupList, dBMIGoal.Value);
                return objRiskLevelGoal.GetRiskLevelResult();
            }
            return null;  
        }
    }
}
