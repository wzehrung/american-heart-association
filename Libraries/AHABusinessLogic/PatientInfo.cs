﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using HVManager;

namespace AHABusinessLogic
{
    public class AHAUserPatientGuids
    {
        public Guid PersonID
        {
            get;
            set;
        }


        public Guid RecordID
        {
            get;
            set;
        }

    };

    /// <summary>
    /// This is the patient info
    /// </summary>
    public class PatientInfo
    {
        public int UserHealthRecordID
        {
            get;
            set;
        }

        public Guid PersonGUID
        {
            get;
            set;
        }

        public Guid RecordGUID
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public int TotalAlerts
        {
            get;
            set;
        }

        public DateTime? DateJoined
        {
            get;
            set;
        }

        public DateTime? LastActive
        {
            get;
            set;
        }

        /**
        public DateTime? DateOfBirth
        {
            get;
            set;
        }
        **/

        public int? YearOfBirth
        {
            get;
            set;
        }

        public bool IsGenderMale
        {
            get;
            set;
        }

        public bool HasAlertRule
        {
            get;
            set;
        }

        public double? BloodGlucose
        {
            get;
            set;
        }

        public string BloodGlucoseReadingType
        {
            get;
            set;
        }

        public DateTime BloodGlucoseEffectiveDate
        {
            get;
            set;
        }

        public int? Systolic
        {
            get;
            set;
        }

        public int? Diastolic
        {
            get;
            set;
        }

        public DateTime BloodPressureEffectiveDate
        {
            get;
            set;
        }

        public double? Weight
        {
            get;
            set;
        }

        public DateTime WeightEffectiveDate
        {
            get;
            set;
        }

        public int? Cholesterol
        {
            get;
            set;
        }

        public DateTime CholesterolEffectiveDate
        {
            get;
            set;
        }

        public DateTime? ProviderConnectionDate
        {
            get;
            set;
        }

        public DateTime? LastReadingDate
        {
            get
            {
                DateTime? result = new DateTime?();

                bool gotOne = false;

                if (Systolic.HasValue && Diastolic.HasValue)
                {
                    result = BloodPressureEffectiveDate;
                    gotOne = true;
                }
                if (BloodGlucose.HasValue)
                {
                    if (!gotOne || BloodGlucoseEffectiveDate.CompareTo(result.Value) > 0)
                    {
                        result = BloodGlucoseEffectiveDate;
                        gotOne = true;
                    }
                }
                if (Cholesterol.HasValue)
                {
                    if (!gotOne || CholesterolEffectiveDate.CompareTo(result.Value) > 0)
                    {
                        result = CholesterolEffectiveDate;
                        gotOne = true;
                    }
                }
                return result;
            }
        }

        public static List<PatientInfo> GetPopulatedPatientInfo(List<AHAHelpContent.Patient> lstPatients)
        {
            List<PatientInfo> lstPatientInfo = new List<PatientInfo>();

            lstPatients.ForEach(delegate(AHAHelpContent.Patient objPatient)
            {
                // PJB - defensive programming
                if( objPatient.OfflineHealthRecordGUID.HasValue && objPatient.OfflinePersonGUID.HasValue )
                {
                    PatientInfo objPatientInfo = new PatientInfo();
                    objPatientInfo.UserHealthRecordID = objPatient.UserHealthRecordID;
                    objPatientInfo.PersonGUID = objPatient.OfflinePersonGUID.Value;
                    objPatientInfo.RecordGUID = objPatient.OfflineHealthRecordGUID.Value;
                    objPatientInfo.HasAlertRule = objPatient.HasAlertRule;
                    objPatientInfo.DateJoined = objPatient.CreatedDate; // objPatient.DateJoined;
                    objPatientInfo.ProviderConnectionDate = objPatient.ProviderConnectionDate;
                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                    //commented by arun - no need to cache data - MS had issue on March 7, 2011 where a user had lots of data in HV for the exercise data type
                    //mgr.DownloadDataBetweenDates(new List<string> 
                    //{ 
                    //    typeof(HVManager.BasicDataManager).ToString(), 
                    //    typeof(HVManager.BloodGlucoseDataManager).ToString(), 
                    //    typeof(HVManager.BloodPressureDataManager).ToString(),
                    //    typeof(HVManager.CholesterolDataManager).ToString(), 
                    //    typeof(HVManager.ExerciseDataManager).ToString(), 
                    //    typeof(HVManager.HeightDataManager).ToString(),
                    //    typeof(HVManager.MedicationDataManager).ToString(), 
                    //    typeof(HVManager.PersonalDataManager).ToString(), 
                    //    typeof(HVManager.PersonalContactDataManager).ToString(), 
                    //    typeof(HVManager.WeightDataManager).ToString() 
                    //}, DateTime.Today.AddYears(-2), DateTime.MaxValue);

                    PersonalContactDataManager.ContactItem ci = mgr.PersonalContactDataManager.Item;

                    if (mgr.PersonalDataManager.Item != null && !String.IsNullOrEmpty(mgr.PersonalDataManager.Item.FirstName.Value))
                        objPatientInfo.FirstName = mgr.PersonalDataManager.Item.FirstName.Value;
                    else
                        objPatientInfo.FirstName = "-";

                    if (mgr.PersonalDataManager.Item != null && !String.IsNullOrEmpty(mgr.PersonalDataManager.Item.LastName.Value))
                        objPatientInfo.LastName = mgr.PersonalDataManager.Item.LastName.Value;
                    else
                        objPatientInfo.LastName = "-";

                    objPatientInfo.FullName = string.Format("{0} {1}", objPatientInfo.FirstName, objPatientInfo.LastName);
                    if (mgr.PersonalDataManager.Item == null)
                    {
                        objPatientInfo.FullName = mgr.Name;
                    }

                    if (mgr.BasicDataManager.Item != null)
                        objPatientInfo.YearOfBirth = mgr.BasicDataManager.Item.YearOfBirth.Value;
                        
                    /** PJB: this was wrong
                    if (mgr.PersonalDataManager.Item != null && mgr.PersonalDataManager.Item.DateOfBirth.Value.HasValue)
                        objPatientInfo.DateOfBirth = mgr.PersonalDataManager.Item.DateOfBirth.Value.Value;
                     **/

                    objPatientInfo.LastActive = mgr.LastActiveDate;
                    if (mgr.BloodGlucoseDataManager.GetLatestNItems(1).Count > 0)
                    {
                        objPatientInfo.BloodGlucose = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.Value.Value;
                        objPatientInfo.BloodGlucoseReadingType = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.ReadingType.Value;
                        objPatientInfo.BloodGlucoseEffectiveDate = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                    }

                    if (mgr.BloodPressureDataManager.GetLatestNItems(1).Count > 0)
                    {
                        objPatientInfo.Systolic = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.Systolic.Value;
                        objPatientInfo.Diastolic = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.Diastolic.Value;
                        objPatientInfo.BloodPressureEffectiveDate = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                    }

                    if (mgr.CholesterolDataManager.GetLatestNItems(1).Count > 0)
                    {
                        objPatientInfo.Cholesterol = mgr.CholesterolDataManager.GetLatestNItems(1).First.Value.TotalCholesterol.Value;
                        objPatientInfo.CholesterolEffectiveDate = mgr.CholesterolDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                    }

                    if (mgr.WeightDataManager.GetLatestNItems(1).Count > 0)
                    {
                        objPatientInfo.Weight = mgr.WeightDataManager.GetLatestNItems(1).First.Value.CommonWeight.Value.ToPounds();
                        objPatientInfo.WeightEffectiveDate = mgr.WeightDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                    }

                    objPatientInfo.TotalAlerts = objPatient.TotalAlerts;
                    lstPatientInfo.Add(objPatientInfo);
                }
            });

            return lstPatientInfo;
        }

        public static PatientInfo GetPopulatedPatientInfo( AHAHelpContent.Patient objPatient )
        {
            try
            {
                PatientInfo objPatientInfo = new PatientInfo();
                objPatientInfo.UserHealthRecordID = objPatient.UserHealthRecordID;
                objPatientInfo.PersonGUID = objPatient.OfflinePersonGUID.Value;
                objPatientInfo.RecordGUID = objPatient.OfflineHealthRecordGUID.Value;
                objPatientInfo.HasAlertRule = objPatient.HasAlertRule;
                objPatientInfo.DateJoined = objPatient.DateJoined;
                objPatientInfo.ProviderConnectionDate = objPatient.ProviderConnectionDate;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                //commented by arun - no need to cache data - MS had issue on March 7, 2011 where a user had lots of data in HV for the exercise data type
                //mgr.DownloadDataBetweenDates(new List<string> 
                //{ 
                //    typeof(HVManager.BasicDataManager).ToString(), 
                //    typeof(HVManager.BloodGlucoseDataManager).ToString(), 
                //    typeof(HVManager.BloodPressureDataManager).ToString(),
                //    typeof(HVManager.CholesterolDataManager).ToString(), 
                //    typeof(HVManager.ExerciseDataManager).ToString(), 
                //    typeof(HVManager.HeightDataManager).ToString(),
                //    typeof(HVManager.MedicationDataManager).ToString(), 
                //    typeof(HVManager.PersonalDataManager).ToString(), 
                //    typeof(HVManager.PersonalContactDataManager).ToString(), 
                //    typeof(HVManager.WeightDataManager).ToString() 
                //}, DateTime.Today.AddYears(-2), DateTime.MaxValue);


                if (mgr.PersonalDataManager.Item != null && !String.IsNullOrEmpty(mgr.PersonalDataManager.Item.FirstName.Value))
                    objPatientInfo.FirstName = mgr.PersonalDataManager.Item.FirstName.Value;
                else
                    objPatientInfo.FirstName = "-";

                if (mgr.PersonalDataManager.Item != null && !String.IsNullOrEmpty(mgr.PersonalDataManager.Item.LastName.Value))
                    objPatientInfo.LastName = mgr.PersonalDataManager.Item.LastName.Value;
                else
                    objPatientInfo.LastName = "-";

                objPatientInfo.FullName = string.Format("{0} {1}", objPatientInfo.FirstName, objPatientInfo.LastName);
                if (mgr.PersonalDataManager.Item == null)
                {
                    objPatientInfo.FullName = mgr.Name;
                }

                if (mgr.BasicDataManager.Item != null)
                {
                    objPatientInfo.YearOfBirth = mgr.BasicDataManager.Item.YearOfBirth.Value;
                /** PJB - use YearOfBirth above
                if (mgr.PersonalDataManager.Item != null && mgr.PersonalDataManager.Item.DateOfBirth.Value.HasValue)
                    objPatientInfo.DateOfBirth = mgr.PersonalDataManager.Item.DateOfBirth.Value.Value;
                **/

                    // Heart360 requires gender
                    objPatientInfo.IsGenderMale = (mgr.BasicDataManager.Item.GenderOfPerson.Value.HasValue && (mgr.BasicDataManager.Item.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Male)) ? true : false;
                }

                objPatientInfo.LastActive = mgr.LastActiveDate;
                if (mgr.BloodGlucoseDataManager.GetLatestNItems(1).Count > 0)
                {
                    objPatientInfo.BloodGlucose = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.Value.Value;
                    objPatientInfo.BloodGlucoseReadingType = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.ReadingType.Value;
                    objPatientInfo.BloodGlucoseEffectiveDate = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                }

                if (mgr.BloodPressureDataManager.GetLatestNItems(1).Count > 0)
                {
                    objPatientInfo.Systolic = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.Systolic.Value;
                    objPatientInfo.Diastolic = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.Diastolic.Value;
                    objPatientInfo.BloodPressureEffectiveDate = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                }

                if (mgr.CholesterolDataManager.GetLatestNItems(1).Count > 0)
                {
                    objPatientInfo.Cholesterol = mgr.CholesterolDataManager.GetLatestNItems(1).First.Value.TotalCholesterol.Value;
                    objPatientInfo.CholesterolEffectiveDate = mgr.CholesterolDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                }

                if (mgr.WeightDataManager.GetLatestNItems(1).Count > 0)
                {
                    objPatientInfo.Weight = mgr.WeightDataManager.GetLatestNItems(1).First.Value.CommonWeight.Value.ToPounds();
                    objPatientInfo.WeightEffectiveDate = mgr.WeightDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                }

                objPatientInfo.TotalAlerts = objPatient.TotalAlerts;

                return objPatientInfo;
            }
            catch (Exception)
            { }
            return null;
        }


        public static List<PatientInfo> GetPopulatedPatientInfoRefactored(List<AHAHelpContent.Patient> lstPatients, ref List<AllItemManagerRefactored> masterList )
        {
            List<PatientInfo> lstPatientInfo = new List<PatientInfo>();

            // PJB - Not going to use LINQ here because of masterList reference
            //lstPatients.ForEach(delegate(AHAHelpContent.Patient objPatient)
            foreach( AHAHelpContent.Patient objPatient in lstPatients )
            {
                PatientInfo objPatientInfo = new PatientInfo();
                objPatientInfo.UserHealthRecordID = objPatient.UserHealthRecordID;
                objPatientInfo.PersonGUID = objPatient.OfflinePersonGUID.Value;
                objPatientInfo.RecordGUID = objPatient.OfflineHealthRecordGUID.Value;
                objPatientInfo.HasAlertRule = objPatient.HasAlertRule;
                objPatientInfo.DateJoined = objPatient.DateJoined;
                objPatientInfo.ProviderConnectionDate = objPatient.ProviderConnectionDate;

                HVManager.AllItemManagerRefactored mgr = HVManager.AllItemManagerRefactored.GetItemManagersForRecordId( objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value, ref masterList );

                mgr.DownloadMostRecentData(new List<HVManager.AllItemManagerRefactored.HVDataManager> 
                                            {  
                                                HVManager.AllItemManagerRefactored.HVDataManager.PERSONAL,
                                                HVManager.AllItemManagerRefactored.HVDataManager.BLOOD_GLUCOSE,
                                                HVManager.AllItemManagerRefactored.HVDataManager.BLOOD_PRESSURE,
                                                HVManager.AllItemManagerRefactored.HVDataManager.CHOLESTEROL,
                                            },
                                            DateTime.MinValue, DateTime.MaxValue, 1);

                // Set some defaults
                objPatientInfo.FirstName = objPatientInfo.LastName =  "-";

                if (mgr.PersonalDataManager.Item != null)
                {
                    if (!String.IsNullOrEmpty(mgr.PersonalDataManager.Item.FirstName.Value))
                        objPatientInfo.FirstName = mgr.PersonalDataManager.Item.FirstName.Value;

                    if (!String.IsNullOrEmpty(mgr.PersonalDataManager.Item.LastName.Value))
                        objPatientInfo.LastName = mgr.PersonalDataManager.Item.LastName.Value;

                    objPatientInfo.FullName = string.Format("{0} {1}", objPatientInfo.FirstName, objPatientInfo.LastName);

                    if (mgr.BasicDataManager.Item != null)
                        objPatientInfo.YearOfBirth = mgr.BasicDataManager.Item.YearOfBirth.Value;
                    /** PJB - use YearOfBirth
                    if( mgr.PersonalDataManager.Item.DateOfBirth.Value.HasValue )
                        objPatientInfo.DateOfBirth = mgr.PersonalDataManager.Item.DateOfBirth.Value.Value;
                     **/
                }
                else
                {
                    objPatientInfo.FullName = mgr.Name;
                }

                objPatientInfo.LastActive = mgr.LastActiveDate;

                if( mgr.BloodGlucoseDataManager.Count > 0 )
                {
                    BloodGlucoseDataManager.BGItem  bgi = (BloodGlucoseDataManager.BGItem)mgr.BloodGlucoseDataManager.LatestItemInCache ;

                    objPatientInfo.BloodGlucose = bgi.Value.Value ;
                    objPatientInfo.BloodGlucoseReadingType = bgi.ReadingType.Value;
                    objPatientInfo.BloodGlucoseEffectiveDate = bgi.EffectiveDate;
                }

                if (mgr.BloodPressureDataManager.Count > 0)
                {
                    BloodPressureDataManager.BPItem bpi = (BloodPressureDataManager.BPItem)mgr.BloodPressureDataManager.LatestItemInCache;

                    objPatientInfo.Systolic = bpi.Systolic.Value;
                    objPatientInfo.Diastolic = bpi.Diastolic.Value;
                    objPatientInfo.BloodPressureEffectiveDate = bpi.EffectiveDate;
                }

                if (mgr.CholesterolDataManager.Count > 0)
                {
                    CholesterolDataManager.CholesterolItem ci = (CholesterolDataManager.CholesterolItem)mgr.CholesterolDataManager.LatestItemInCache;

                    objPatientInfo.Cholesterol = ci.TotalCholesterol.Value;
                    objPatientInfo.CholesterolEffectiveDate = ci.EffectiveDate;
                }

                if (mgr.WeightDataManager.Count > 0)
                {
                    WeightDataManager.WeightItem wi = (WeightDataManager.WeightItem)mgr.WeightDataManager.LatestItemInCache;

                    objPatientInfo.Weight = wi.CommonWeight.Value.ToPounds();
                    objPatientInfo.WeightEffectiveDate = wi.EffectiveDate;
                }

                objPatientInfo.TotalAlerts = objPatient.TotalAlerts;
                lstPatientInfo.Add(objPatientInfo);
            }

            // });  // end of LINQ

            return lstPatientInfo;
        }
    }


    

}
