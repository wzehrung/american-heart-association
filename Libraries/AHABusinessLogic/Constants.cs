﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.XPath;
using System.Configuration;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace AHABusinessLogic
{

    public enum TrackerType
    {
        [Description("Blood Pressure")]
        BloodPressure,
        [Description("Cholesterol")]
        Cholesterol,
        [Description("Blood Glucose")]
        BloodGlucose,
        [Description("Medication")]
        Medication,
        [Description("Weight")]
        Weight,
        [Description("Physical Activity")]
        PhysicalActivity
    }
}
