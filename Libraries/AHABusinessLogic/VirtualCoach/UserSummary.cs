﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using HVManager;
using System.Collections.Generic;

namespace AHABusinessLogic.VirtualCoach
{

    public enum UserWeightRangeType
    {
        Unknown,
        OverWeight,
        Obese
    }


    public class UserSummary
    {
        /*
        English BMR Formula 
        Women: BMR = 655 + ( 4.35 x weight in pounds ) + ( 4.7 x height in inches ) - ( 4.7 x age in years )
        Men: BMR = 66 + ( 6.23 x weight in pounds ) + ( 12.7 x height in inches ) - ( 6.8 x age in year ) 
          
        Metric BMR Formula 
        Women: BMR = 655 + ( 9.6 x weight in kilos ) + ( 1.8 x height in cm ) - ( 4.7 x age in years )
        Men: BMR = 66 + ( 13.7 x weight in kilos ) + ( 5 x height in cm ) - ( 6.8 x age in years ) 
        as per http://www.bmi-calculator.net/bmr-calculator/bmr-formula.php
        
        Imperial BMI Formula
        =====================
        
        BMI ( kg/m² ) = (weight in pounds * 703 ) / height in inches² 

        Metric Imperial BMI Formula
        ===================================
         
        BMI ( kg/m² ) =  weight in kilograms / height in meters² 
         
         */

            // PJB: this is the new way, pass in the manager, not the guids.
        public static double? GetBMIForCurrentRecordForWeight( HVManager.AllItemManager hvMgr, double weight)
        {
            if (hvMgr != null)
            {
                LinkedList<HVManager.HeightDataManager.HeightItem> objH = hvMgr.HeightDataManager.GetLatestNItems(1);
                if (objH != null && objH.Count > 0)
                {
                    HVManager.HeightDataManager.HeightItem objHeight = objH.First.Value as HVManager.HeightDataManager.HeightItem;
                    double dHeightInInches = UnitConversion.MetersToInches(objHeight.HeightInMeters.Value);
                    double bmi = (weight * 703) / ((dHeightInInches) * (dHeightInInches));
                    return bmi;

                }
            }
            return null;
        }


        public static double? GetWeightThresholdInPoundsForCurrentRecord(PatientGuids objGuids, UserWeightRangeType rangeType)
        {
            double? dThresholdInPounds = null;
            double? dHeightInInches = null;

            if (objGuids!=null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                LinkedList<HVManager.HeightDataManager.HeightItem> objH = mgr.HeightDataManager.GetLatestNItems(1);
                if (objH != null && objH.Count > 0)
                {
                    dHeightInInches = UnitConversion.MetersToInches(objH.First.Value.HeightInMeters.Value);
                }
            }

            double dBMI = 0;
            if (rangeType == UserWeightRangeType.OverWeight)
            {
                dBMI = 25.0;
            }
            else if (rangeType == UserWeightRangeType.Obese)
            {
                dBMI = 30.0;
            }
            
            if (dHeightInInches.HasValue)
            {
                dThresholdInPounds = (dBMI * dHeightInInches.Value * dHeightInInches.Value) / 703;
                dThresholdInPounds = Math.Round(dThresholdInPounds.Value, 2);
            }
            return dThresholdInPounds;
        }


        public static double? GetBMIForCurrentRecordForWeight(PatientGuids objGuids, double weight)
        {
            if (objGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.HeightDataManager.HeightItem> objH = mgr.HeightDataManager.GetLatestNItems(1);
                if (objH != null && objH.Count > 0)
                {
                    HVManager.HeightDataManager.HeightItem objHeight = objH.First.Value as HVManager.HeightDataManager.HeightItem;                   
                    double dHeightInInches = UnitConversion.MetersToInches(objHeight.HeightInMeters.Value);
                    double bmi = (weight * 703) / ((dHeightInInches) * (dHeightInInches));
                    return bmi;

                }
            }
            return null;
        }


/** PJB: commented this out because of use of HVHelper
        public static double? GetBMRForCurrentRecord()
        {
            double? dBMR = null;

            WeightDataManager.WeightItem objWeightDataItem = MyWeightWrapper.GetLatestWeight();
            double? dWeightInPounds = null;
            if (objWeightDataItem != null)
            {
                dWeightInPounds = objWeightDataItem.CommonWeight.Value.ToPounds();
            }

            HeightDataManager.HeightItem objHeight = HeightWrapper.GetLatestHeightItem();
            double? dHeightInInches = null;
            if (objHeight != null)
            {
                dHeightInInches = HVManager.UnitConversion.MetersToInches(objHeight.HeightInMeters.Value);
            }

            int? iAgeInYears = null;
            DateTime? dtDOB = null;
            if (HVHelper.HVManagerPatientBase.PersonalDataManager.Item != null)
            {
                dtDOB = HVHelper.HVManagerPatientBase.PersonalDataManager.Item.DateOfBirth.Value;
            }

            if (dtDOB.HasValue)
            {

                TimeSpan objTimeSpan = DateTime.Today.Subtract(dtDOB.Value.Date);
                iAgeInYears = (int)(objTimeSpan.Days / 365.25);
            }

            bool? bIsMale = null;
            HVManager.BasicDataManager.BasicItem basicItem = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
            if (basicItem != null && basicItem.GenderOfPerson.Value.HasValue && basicItem.GenderOfPerson.Value.Value != Microsoft.Health.ItemTypes.Gender.Unknown)
            {
                if (basicItem.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Male)
                {
                    bIsMale = true;
                }
                else
                {
                    bIsMale = false;
                }
            }

            if (dWeightInPounds.HasValue && dHeightInInches.HasValue && iAgeInYears.HasValue && bIsMale.HasValue)
            {
                dBMR = _GetEnglishBMR(dWeightInPounds.Value,
                    dHeightInInches.Value,
                    iAgeInYears.Value,
                    bIsMale.Value);
            }
            return dBMR;
        }
**/
        private static double _GetEnglishBMR(double weightInPounds, double heigthInInches, int ageInYears, bool IsMale)
        {
            double BMR = 0.0;

            if (IsMale)
                BMR = 66 + (6.23 * weightInPounds) + (12.7 * heigthInInches) - (6.8 * ageInYears);
            else
                BMR = 655 + (4.35 * weightInPounds) + (4.7 * heigthInInches) - (4.7 * ageInYears);

            return Math.Round(BMR, 2);
        }

        /*NOT USED*/
        private static double _GetMetricBMR(double weightInKilos, double heigthInCms, int ageInYears, bool IsMale)
        {

            double BMR = 0.0;
            if (IsMale)
                BMR = 66 + (13.7 * weightInKilos) + (5 * heigthInCms) - (6.8 * ageInYears);
            else
                BMR = 655 + (9.6 * weightInKilos) + (1.8 * heigthInCms) - (4.7 * ageInYears);

            return Math.Round(BMR, 2);
        }

    }
}
