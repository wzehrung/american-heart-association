﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HVManager;

namespace AHABusinessLogic
{
    public class PatientContact
    {
        public string PrimaryPhone
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public AddressItem Address
        {
            get;
            set;
        }

        public string ContactPhone
        {
            get;
            set;
        }

        public string MobilePhone
        {
            get;
            set;
        }

        public static PatientContact GetPatientContact(AHAHelpContent.Patient objPatient)
        {
            PatientContact pcResult = null;

            if (objPatient != null && objPatient.OfflinePersonGUID.HasValue && objPatient.OfflineHealthRecordGUID.HasValue)
            {

                // Now try and get the Patients name from HV
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                try
                {
                    PersonalContactDataManager.ContactItem ci = mgr.PersonalContactDataManager.Item;
                    AHAHelpContent.PhoneNumbers objPhoneNumbers = AHAHelpContent.PhoneNumbers.GetPhoneNumbers(objPatient.UserHealthRecordID);
                    if (ci != null)
                    {
                        pcResult = new PatientContact();

                        pcResult.PrimaryPhone = ci.Phone.Value;
                        if (objPhoneNumbers != null)
                        {
                            pcResult.ContactPhone = objPhoneNumbers.PhoneContact;
                            pcResult.MobilePhone = objPhoneNumbers.PhoneMobile;
                        }
                        if (ci.EmailList.Count > 0)
                            pcResult.Email = ci.EmailList[0].Email.Value;
                        if (ci.AddressList.Count > 0)
                            pcResult.Address = ci.AddressList[0];

                    }
                }
                catch (Microsoft.Health.HealthServiceException hse)
                {
                    if (hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.InvalidRecordState ||
                        hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.AccessDenied)
                    {
                        //
                    }
                    pcResult = null;
                }
            }


            return pcResult;
        }
    }
}
