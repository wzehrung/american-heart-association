﻿using System;
using System.Xml.XPath;

namespace PolkaDataManager
{
    public class BloodPressureLogItem : IXmlPersist
    {
        public BloodPressureLogItem()
        {
        }


        private int _bpRecordID;

        public int BPRecordID
        {
            get { return _bpRecordID; }
            set { _bpRecordID = value; }
        }

        private int _heartRate;

        public int HeartRate
        {
            get { return _heartRate; }
            set { _heartRate = value; }
        }

        private int _systolic;

        public int Systolic
        {
            get { return _systolic; }
            set { _systolic = value; }
        }

        private int _diastolic;

        public int Diastolic
        {
            get { return _diastolic; }
            set { _diastolic = value; }
        }

        private string _readingSource;

        public string ReadingSource
        {
            get { return _readingSource; }
            set { _readingSource = value; }
        }

        private string _comments;

        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        private DateTime _dateMeasuredUTC;

        public DateTime DateMeasuredUTC
        {
            get { return _dateMeasuredUTC; }
            set { _dateMeasuredUTC = value; }
        }


        #region IXmlPersist Members

        public void ParseXml(System.Xml.XPath.IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.Clone();

            if (!string.IsNullOrEmpty(navigator.GetAttribute("BPRecordID", string.Empty)))
            {
                BPRecordID = Convert.ToInt32(navigator.GetAttribute("BPRecordID", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("HeartRate", string.Empty)))
            {
                HeartRate = Convert.ToInt32(navigator.GetAttribute("HeartRate", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Systolic", string.Empty)))
            {
                Systolic = Convert.ToInt32(navigator.GetAttribute("Systolic", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Diastolic", string.Empty)))
            {
                Diastolic = Convert.ToInt32(navigator.GetAttribute("Diastolic", string.Empty));
            }

            ReadingSource = navigator.GetAttribute("ReadingSource", string.Empty);
            Comments = navigator.GetAttribute("Comments", string.Empty);

            DateMeasuredUTC = DateTime.Parse(navigator.GetAttribute("DateMeasuredUTC", string.Empty));
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("BloodPressureLogItem");
            writer.WriteAttributeString("BPRecordID", this.BPRecordID.ToString());
            writer.WriteAttributeString("HeartRate", this.HeartRate.ToString());
            writer.WriteAttributeString("Systolic", this.Systolic.ToString());
            writer.WriteAttributeString("Diastolic", this.Diastolic.ToString());
            writer.WriteAttributeString("ReadingSource", this.ReadingSource);
            writer.WriteAttributeString("Comments", this.Comments);
            writer.WriteAttributeString("DateMeasuredUTC", AHACommon.CommonUtility.GetDateTimeForDB(this.DateMeasuredUTC));
            writer.WriteEndElement();
        }

        #endregion
    }
}