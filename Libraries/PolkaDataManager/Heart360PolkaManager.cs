﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using GRCBase;
using System.IO;
using System.Xml.XPath;
using HVManager;
using Microsoft.Health.ItemTypes;
using Microsoft.Health;
using Microsoft.Health.Web;
using System.Xml.Linq;
using System.Linq.Expressions;

namespace PolkaDataManager
{
    public class Heart360PolkaManager
    {
        const string POLKA_DATA_SOURCE = "Mobile";

        public static void ProcessForUser(Guid gPersonGUID, bool bIsOnline, AHAHelpContent.Patient objPatient)
        {
            Guid gRecordGUID = bIsOnline ? objPatient.UserHealthRecordGUID : objPatient.OfflineHealthRecordGUID.Value;
            //DBContext dbContext = null;
            //bool bException = false;
            try
            {
                //dbContext = DBContext.GetDBContext();

                //fetch polka data for user

                DataServiceResponse serviceResponseData;

                ServiceCallManager serviceManager = new ServiceCallManager();

                DateTime? dtLastUploadedDate = AHAHelpContent.PolkaData.GetLastHVUploadedDate(objPatient.PersonalItemGUID);
                if (dtLastUploadedDate.HasValue)
                {
                    //to make sure that we don't miss any data here
                    dtLastUploadedDate = dtLastUploadedDate.Value.AddDays(-1);
                }
                else
                {
                    //since we don't have any last sync date, perform fetch fo the last six months
                    dtLastUploadedDate = DateTime.Today.AddDays(-180);
                }

                string param_givenTime = dtLastUploadedDate.Value.ToString("G");
                string param_uids = objPatient.PolkaID;

                serviceResponseData = serviceManager.CallServiceGetRecordsByUserIDsAfterGivenTime("GetUsersRecordsByIDsAfterGivenTime", param_uids, param_givenTime);

                bool bHasDataFromPolka = false;

                StringBuilder sbDataForDB = new StringBuilder();
                if (serviceResponseData != null
                    && serviceResponseData.People != null
                    && serviceResponseData.People.Length > 0)
                {

                    using (XmlWriter writer = XmlWriter.Create(sbDataForDB))
                    {
                        writer.WriteStartElement("PolkaData");
                        if (serviceResponseData.People[0].WeightLogItems != null)
                        {
                            bHasDataFromPolka = true;
                            foreach (WeightLogItem objWeightLogItem in serviceResponseData.People[0].WeightLogItems)
                            {
                                objWeightLogItem.WriteXml(writer);
                            }
                        }
                        //if (serviceResponseData.People[0].PhysicalActivityLogItems != null)
                        //{
                        //    bHasDataFromPolka = true;
                        //    foreach (PhysicalActivityLogItem objPhysicalActivityLogItem in serviceResponseData.People[0].PhysicalActivityLogItems)
                        //    {
                        //        objPhysicalActivityLogItem.WriteXml(writer);
                        //    }
                        //}
                        if (serviceResponseData.People[0].BloodGlucoseLogItems != null)
                        {
                            bHasDataFromPolka = true;
                            foreach (BloodGlucoseLogItem objBloodGlucoseLogItem in serviceResponseData.People[0].BloodGlucoseLogItems)
                            {
                                objBloodGlucoseLogItem.WriteXml(writer);
                            }
                        }
                        if (serviceResponseData.People[0].BloodPressureLogItems != null)
                        {
                            bHasDataFromPolka = true;
                            foreach (BloodPressureLogItem objBloodPressureLogItem in serviceResponseData.People[0].BloodPressureLogItems)
                            {
                                objBloodPressureLogItem.WriteXml(writer);
                            }
                        }
                        if (serviceResponseData.People[0].CholesterolLogItems != null)
                        {
                            bHasDataFromPolka = true;
                            foreach (CholesterolLogItem objCholesterolLogItem in serviceResponseData.People[0].CholesterolLogItems)
                            {
                                objCholesterolLogItem.WriteXml(writer);
                            }
                        }
                        //if (serviceResponseData.People[0].MedicationLogItems != null)
                        //{
                        //    bHasDataFromPolka = true;
                        //    foreach (MedicationLogItem objMedicationLogItem in serviceResponseData.People[0].MedicationLogItems)
                        //    {
                        //        objMedicationLogItem.WriteXml(writer);
                        //    }
                        //}
                        writer.WriteEndElement();
                    }
                }

                if (sbDataForDB.Length == 0)
                {
                    return;
                }

                //insert into DB if there is data
                if (bHasDataFromPolka)
                {
                    AHAHelpContent.PolkaData.Create(objPatient.PersonalItemGUID, sbDataForDB.ToString());
                }

                //fetch data for upload to HV
                string strXmlDataForUpload = AHAHelpContent.PolkaData.GetPolkaDataForUploadForPatient(objPatient.PersonalItemGUID);

                List<int> listPolkaIDToBeUploaded;

                Dictionary<string, object> listForUpload = _CreateItmesForUploadToHV(strXmlDataForUpload, out listPolkaIDToBeUploaded);
                if (listForUpload == null || (listForUpload != null && listForUpload.Count == 0))
                    return;

                //List<HVThing> listHVThing = GetHVItems(bIsOnline, gPersonGUID, gRecordGUID, listPolkaIDToBeUploaded);
                
                List<HVThing> listHVThing = GetHVItemsAlreadyExisting(bIsOnline, gPersonGUID, gRecordGUID, listPolkaIDToBeUploaded);

                //create subset of data to be uploaded
                listForUpload = _CreateActualItmesForUploadToHV(listForUpload, listHVThing);
                if (listForUpload == null || (listForUpload != null && listForUpload.Count == 0))
                    return;

                //upload to HV
                HVManager.AllItemManager mgr = null;
                if (!bIsOnline)
                {
                    mgr = new AllItemManager(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                }
                else
                {
                    mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);
                }
                mgr.CreatePolkaItems(bIsOnline, listForUpload);

                //mark items as uploaded to HV, this is done first because anyways this is in a transaction
                AHAHelpContent.PolkaData.MarkPolkaDataAsUploadedToHV(objPatient.PersonalItemGUID, strXmlDataForUpload);
            }
            catch (Exception ex)
            {
                //bException = true;
                GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
            }
            //finally
            //{
            //    try
            //    {
            //        if (bException)
            //        {
            //            if (dbContext != null)
            //            {
            //                dbContext.ReleaseDBContext(false);
            //            }
            //        }
            //        else
            //        {
            //            dbContext.ReleaseDBContext(true);
            //        }
            //    }
            //    catch
            //    {
            //        if (dbContext != null)
            //        {
            //            dbContext.ReleaseDBContext(false);
            //        }
            //        throw;
            //    }
            //}
        }

        public static void ProcessFor3CiUser(Guid gPersonGUID, bool bIsOnline, AHAHelpContent.Patient objPatient)
        {
            Guid gRecordGUID = bIsOnline ? objPatient.UserHealthRecordGUID : objPatient.OfflineHealthRecordGUID.Value;
            //DBContext dbContext = null;
            //bool bException = false;
            try
            {

 
                //fetch data for upload to HV
                string strXmlDataForUpload = AHAHelpContent.PolkaData.GetPolkaDataForUploadForPatient(objPatient.PersonalItemGUID);

                List<int> listPolkaIDToBeUploaded;

                Dictionary<string, object> listForUpload = _CreateItmesForUploadToHV(strXmlDataForUpload, out listPolkaIDToBeUploaded);
                if (listForUpload == null || (listForUpload != null && listForUpload.Count == 0))
                    return;

                //List<HVThing> listHVThing = GetHVItems(bIsOnline, gPersonGUID, gRecordGUID, listPolkaIDToBeUploaded);

                List<HVThing> listHVThing = GetHVItemsAlreadyExisting(bIsOnline, gPersonGUID, gRecordGUID, listPolkaIDToBeUploaded);

                //create subset of data to be uploaded
                listForUpload = _CreateActualItmesForUploadToHV(listForUpload, listHVThing);
                if (listForUpload == null || (listForUpload != null && listForUpload.Count == 0))
                    return;

                //upload to HV
                HVManager.AllItemManager mgr = null;
                if (!bIsOnline)
                {
                    mgr = new AllItemManager(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                }
                else
                {
                    mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);
                }
                mgr.CreatePolkaItems(bIsOnline, listForUpload);

                //mark items as uploaded to HV, this is done first because anyways this is in a transaction
                AHAHelpContent.PolkaData.MarkPolkaDataAsUploadedToHV(objPatient.PersonalItemGUID, strXmlDataForUpload);
            }
            catch (Exception ex)
            {
                //bException = true;
                GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
            }
            //finally
            //{
            //    try
            //    {
            //        if (bException)
            //        {
            //            if (dbContext != null)
            //            {
            //                dbContext.ReleaseDBContext(false);
            //            }
            //        }
            //        else
            //        {
            //            dbContext.ReleaseDBContext(true);
            //        }
            //    }
            //    catch
            //    {
            //        if (dbContext != null)
            //        {
            //            dbContext.ReleaseDBContext(false);
            //        }
            //        throw;
            //    }
            //}

        
        }
        private static Expression<Func<BloodGlucoseDataManager.BGItem, bool>> _BGItemNotIn(
                                        List<HVThing> listHVThing)
        {
            var predicate = PredicateBuilder.True<BloodGlucoseDataManager.BGItem>();

            if (listHVThing != null && listHVThing.Count > 0)
            {
                listHVThing = listHVThing.Where(i => i.TypeID == BloodGlucose.TypeId).ToList();
                foreach (HVThing item in listHVThing)
                {
                    string temp = item.PolkaItemID.ToLower();
                    predicate = predicate.And(p => !p.PolkaItemID.Value.ToLower().Equals(temp));
                }
            }
            return predicate;
        }

        private static Expression<Func<BloodPressureDataManager.BPItem, bool>> _BPItemNotIn(
                                        List<HVThing> listHVThing)
        {
            var predicate = PredicateBuilder.True<BloodPressureDataManager.BPItem>();

            if (listHVThing != null && listHVThing.Count > 0)
            {
                listHVThing = listHVThing.Where(i => i.TypeID == BloodPressure.TypeId).ToList();
                foreach (HVThing item in listHVThing)
                {
                    string temp = item.PolkaItemID.ToLower();
                    predicate = predicate.And(p => !p.PolkaItemID.Value.ToLower().Equals(temp));
                }
            }
            return predicate;
        }

        private static Expression<Func<WeightDataManager.WeightItem, bool>> _WeightItemNotIn(
                                        List<HVThing> listHVThing)
        {
            var predicate = PredicateBuilder.True<WeightDataManager.WeightItem>();

            if (listHVThing != null && listHVThing.Count > 0)
            {
                listHVThing = listHVThing.Where(i => i.TypeID == Weight.TypeId).ToList();
                foreach (HVThing item in listHVThing)
                {
                    string temp = item.PolkaItemID.ToLower();
                    predicate = predicate.And(p => !p.PolkaItemID.Value.ToLower().Equals(temp));
                }
            }
            return predicate;
        }

        private static Expression<Func<ExerciseDataManager.ExerciseItem, bool>> _ExerciseItemNotIn(
                                        List<HVThing> listHVThing)
        {
            var predicate = PredicateBuilder.True<ExerciseDataManager.ExerciseItem>();

            if (listHVThing != null && listHVThing.Count > 0)
            {
                listHVThing = listHVThing.Where(i => i.TypeID == Exercise.TypeId).ToList();
                foreach (HVThing item in listHVThing)
                {
                    string temp = item.PolkaItemID.ToLower();
                    predicate = predicate.And(p => !p.PolkaItemID.Value.ToLower().Equals(temp));
                }
            }
            return predicate;
        }

        private static Expression<Func<MedicationDataManager.MedicationItem, bool>> _MedicationItemNotIn(
                                        List<HVThing> listHVThing)
        {
            var predicate = PredicateBuilder.True<MedicationDataManager.MedicationItem>();

            if (listHVThing != null && listHVThing.Count > 0)
            {
                listHVThing = listHVThing.Where(i => i.TypeID == Medication.TypeId).ToList();
                foreach (HVThing item in listHVThing)
                {
                    string temp = item.PolkaItemID.ToLower();
                    predicate = predicate.And(p => !p.PolkaItemID.Value.ToLower().Equals(temp));
                }
            }
            return predicate;
        }

        private static Expression<Func<CholesterolDataManager.CholesterolItem, bool>> _CholesterolItemNotIn(
                                        List<HVThing> listHVThing)
        {
            var predicate = PredicateBuilder.True<CholesterolDataManager.CholesterolItem>();

            if (listHVThing != null && listHVThing.Count > 0)
            {
                listHVThing = listHVThing.Where(i => i.TypeID == CholesterolProfileV2.TypeId).ToList();
                foreach (HVThing item in listHVThing)
                {
                    string temp = item.PolkaItemID.ToLower();
                    predicate = predicate.And(p => !p.PolkaItemID.Value.ToLower().Equals(temp));
                }
            }
            return predicate;
        }

        private static Dictionary<string, object> _CreateActualItmesForUploadToHV(Dictionary<string, object> list, List<HVThing> listHVThing)
        {
            Dictionary<string, object> listNew = new Dictionary<string, object>();
            foreach (string strKey in list.Keys)
            {
                if (strKey == typeof(BloodGlucoseDataManager).ToString())
                {
                    List<BloodGlucoseDataManager.BGItem> objItemList = list[strKey] as List<BloodGlucoseDataManager.BGItem>;
                    //get items after removing the items having ids in listHVThing
                    if (objItemList != null && objItemList.Count > 0)
                    {
                        var qItemNotIn = _BGItemNotIn(listHVThing).Compile();

                        List<BloodGlucoseDataManager.BGItem> objFilteredItemList = objItemList.Where(qItemNotIn).ToList();

                        if (objFilteredItemList != null && objFilteredItemList.Count > 0)
                        {
                            listNew.Add(typeof(HVManager.BloodGlucoseDataManager).ToString(), objFilteredItemList);
                        }
                    }
                }
                else if (strKey == typeof(BloodPressureDataManager).ToString())
                {
                    List<BloodPressureDataManager.BPItem> objItemList = list[strKey] as List<BloodPressureDataManager.BPItem>;
                    //get items after removing the items having ids in listHVThing
                    if (objItemList != null && objItemList.Count > 0)
                    {
                        var qItemNotIn = _BPItemNotIn(listHVThing).Compile();

                        List<BloodPressureDataManager.BPItem> objFilteredItemList = objItemList.Where(qItemNotIn).ToList();

                        if (objFilteredItemList != null && objFilteredItemList.Count > 0)
                        {
                            listNew.Add(typeof(HVManager.BloodPressureDataManager).ToString(), objFilteredItemList);
                        }
                    }
                }
                else if (strKey == typeof(WeightDataManager).ToString())
                {
                    List<WeightDataManager.WeightItem> objItemList = list[strKey] as List<WeightDataManager.WeightItem>;
                    //get items after removing the items having ids in listHVThing
                    if (objItemList != null && objItemList.Count > 0)
                    {
                        var qItemNotIn = _WeightItemNotIn(listHVThing).Compile();

                        List<WeightDataManager.WeightItem> objFilteredItemList = objItemList.Where(qItemNotIn).ToList();

                        if (objFilteredItemList != null && objFilteredItemList.Count > 0)
                        {
                            listNew.Add(typeof(HVManager.WeightDataManager).ToString(), objFilteredItemList);
                        }
                    }
                }
                else if (strKey == typeof(MedicationDataManager).ToString())
                {
                    List<MedicationDataManager.MedicationItem> objItemList = list[strKey] as List<MedicationDataManager.MedicationItem>;
                    //get items after removing the items having ids in listHVThing
                    if (objItemList != null && objItemList.Count > 0)
                    {
                        var qItemNotIn = _MedicationItemNotIn(listHVThing).Compile();

                        List<MedicationDataManager.MedicationItem> objFilteredItemList = objItemList.Where(qItemNotIn).ToList();

                        if (objFilteredItemList != null && objFilteredItemList.Count > 0)
                        {
                            listNew.Add(typeof(HVManager.MedicationDataManager).ToString(), objFilteredItemList);
                        }
                    }
                }
                else if (strKey == typeof(ExerciseDataManager).ToString())
                {
                    List<ExerciseDataManager.ExerciseItem> objItemList = list[strKey] as List<ExerciseDataManager.ExerciseItem>;
                    //get items after removing the items having ids in listHVThing
                    if (objItemList != null && objItemList.Count > 0)
                    {
                        var qItemNotIn = _ExerciseItemNotIn(listHVThing).Compile();

                        List<ExerciseDataManager.ExerciseItem> objFilteredItemList = objItemList.Where(qItemNotIn).ToList();

                        if (objFilteredItemList != null && objFilteredItemList.Count > 0)
                        {
                            listNew.Add(typeof(HVManager.ExerciseDataManager).ToString(), objFilteredItemList);
                        }
                    }
                }
                else if (strKey == typeof(CholesterolDataManager).ToString())
                {
                    List<CholesterolDataManager.CholesterolItem> objItemList = list[strKey] as List<CholesterolDataManager.CholesterolItem>;
                    //get items after removing the items having ids in listHVThing
                    if (objItemList != null && objItemList.Count > 0)
                    {
                        var qItemNotIn = _CholesterolItemNotIn(listHVThing).Compile();

                        List<CholesterolDataManager.CholesterolItem> objFilteredItemList = objItemList.Where(qItemNotIn).ToList();

                        if (objFilteredItemList != null && objFilteredItemList.Count > 0)
                        {
                            listNew.Add(typeof(HVManager.CholesterolDataManager).ToString(), objFilteredItemList);
                        }
                    }
                }
            }

            return listNew;
        }

        private static Dictionary<string, object> _CreateItmesForUploadToHV(string strXml, out List<int> listPolkaIDToBeUploaded)
        {
            listPolkaIDToBeUploaded = new List<int>();

            if (string.IsNullOrEmpty(strXml))
                return null;

            Dictionary<string, object> list = new Dictionary<string, object>();
            XPathDocument doc = null;
            int? iNull = null;
            double? dNull = null;
            using (StringReader rdr = new StringReader(strXml))
            {
                doc = new XPathDocument(rdr);
                XPathNavigator navigator = ((IXPathNavigable)doc).CreateNavigator();

                #region Blood Glucose
                XPathNodeIterator iteratorParam = navigator.Select("PolkaData/BloodGlucoseLogItem");
                List<BloodGlucoseDataManager.BGItem> objBGItemList = new List<BloodGlucoseDataManager.BGItem>();
                while (iteratorParam.MoveNext())
                {
                    XPathNavigator nav = iteratorParam.Current;
                    BloodGlucoseLogItem objBloodGlucoseLogItem = new BloodGlucoseLogItem();
                    objBloodGlucoseLogItem.ParseXml(nav);

                    BloodGlucoseDataManager.BGItem objBGItem = new BloodGlucoseDataManager.BGItem();
                    objBGItem.Value.Value = objBloodGlucoseLogItem.BloodGlucoseValue;
                    objBGItem.ReadingType.Value = "";  //objBloodGlucoseLogItem.ReadingType;
                    objBGItem.ActionTaken.Value = objBloodGlucoseLogItem.ActionITook;
                    objBGItem.Note.Value = objBloodGlucoseLogItem.Notes;
                    objBGItem.When.Value = objBloodGlucoseLogItem.DateMeasuredUTC;
                    objBGItem.PolkaItemID.Value = objBloodGlucoseLogItem.BGRecordID.ToString();
                    objBGItem.Source.Value = !string.IsNullOrEmpty(objBloodGlucoseLogItem.ReadingType) ? objBloodGlucoseLogItem.ReadingType : POLKA_DATA_SOURCE;
                    objBGItemList.Add(objBGItem);
                    listPolkaIDToBeUploaded.Add(objBloodGlucoseLogItem.BGRecordID);
                }
                if (objBGItemList.Count > 0)
                {
                    list.Add(typeof(HVManager.BloodGlucoseDataManager).ToString(), objBGItemList);
                }
                #endregion

                #region Blood Pressure
                iteratorParam = navigator.Select("PolkaData/BloodPressureLogItem");
                List<BloodPressureDataManager.BPItem> objBPItemList = new List<BloodPressureDataManager.BPItem>();
                while (iteratorParam.MoveNext())
                {
                    XPathNavigator nav = iteratorParam.Current;
                    BloodPressureLogItem objBloodPressureLogItem = new BloodPressureLogItem();
                    objBloodPressureLogItem.ParseXml(nav);

                    BloodPressureDataManager.BPItem objBPItem = new BloodPressureDataManager.BPItem();
                    objBPItem.HeartRate.Value = objBloodPressureLogItem.HeartRate > 0 ? objBloodPressureLogItem.HeartRate : iNull;
                    objBPItem.Systolic.Value = objBloodPressureLogItem.Systolic;
                    objBPItem.Diastolic.Value = objBloodPressureLogItem.Diastolic;
                    objBPItem.Source.Value = !string.IsNullOrEmpty(objBloodPressureLogItem.ReadingSource) ? objBloodPressureLogItem.ReadingSource : POLKA_DATA_SOURCE;
                    objBPItem.Note.Value = objBloodPressureLogItem.Comments;
                    objBPItem.When.Value = objBloodPressureLogItem.DateMeasuredUTC;
                    objBPItem.PolkaItemID.Value = objBloodPressureLogItem.BPRecordID.ToString();
                    objBPItemList.Add(objBPItem);
                    listPolkaIDToBeUploaded.Add(objBloodPressureLogItem.BPRecordID);
                }
                if (objBPItemList.Count > 0)
                {
                    list.Add(typeof(HVManager.BloodPressureDataManager).ToString(), objBPItemList);
                }
                #endregion

                #region Cholesterol
                iteratorParam = navigator.Select("PolkaData/CholesterolLogItem");
                List<CholesterolDataManager.CholesterolItem> objCholesterolItemList = new List<CholesterolDataManager.CholesterolItem>();
                while (iteratorParam.MoveNext())
                {
                    XPathNavigator nav = iteratorParam.Current;
                    CholesterolLogItem objCholesterolLogItem = new CholesterolLogItem();
                    objCholesterolLogItem.ParseXml(nav);

                    CholesterolDataManager.CholesterolItem objCholesterolItem = new CholesterolDataManager.CholesterolItem();
                    objCholesterolItem.TotalCholesterol.Value = objCholesterolLogItem.TotalCholesterol > 0 ? objCholesterolLogItem.TotalCholesterol : iNull;
                    objCholesterolItem.HDL.Value = objCholesterolLogItem.Hdl > 0 ? objCholesterolLogItem.Hdl : iNull;
                    objCholesterolItem.LDL.Value = objCholesterolLogItem.Ldl > 0 ? objCholesterolLogItem.Ldl : iNull;
                    objCholesterolItem.Triglycerides.Value = objCholesterolLogItem.Triglyceride > 0 ? objCholesterolLogItem.Triglyceride : iNull;
                    objCholesterolItem.TestLocation.Value = "";  //objCholesterolLogItem.Location;
                    objCholesterolItem.When.Value = objCholesterolLogItem.DateMeasuredUTC;
                    objCholesterolItem.PolkaItemID.Value = objCholesterolLogItem.CHRecordID.ToString();
                    objCholesterolItem.Source.Value = !string.IsNullOrEmpty(objCholesterolLogItem.Location) ? objCholesterolLogItem.Location : POLKA_DATA_SOURCE; 
                    objCholesterolItemList.Add(objCholesterolItem);
                    listPolkaIDToBeUploaded.Add(objCholesterolLogItem.CHRecordID);
                }
                if (objCholesterolItemList.Count > 0)
                {
                    list.Add(typeof(HVManager.CholesterolDataManager).ToString(), objCholesterolItemList);
                }
                #endregion

                #region Weight
                iteratorParam = navigator.Select("PolkaData/WeightLogItem");
                List<WeightDataManager.WeightItem> objWeightItemList = new List<WeightDataManager.WeightItem>();
                while (iteratorParam.MoveNext())
                {
                    XPathNavigator nav = iteratorParam.Current;
                    WeightLogItem objWeightLogItem = new WeightLogItem();
                    objWeightLogItem.ParseXml(nav);

                    WeightDataManager.WeightItem objWeightItem = new WeightDataManager.WeightItem();
                    CommonWeight objCommonWeight = CommonWeight.FromPounds(objWeightLogItem.Weight);
                    objWeightItem.CommonWeight.Value = objCommonWeight;
                    objWeightItem.Source.Value = !string.IsNullOrEmpty(objWeightLogItem.ReadingSource) ? objWeightLogItem.ReadingSource : POLKA_DATA_SOURCE;
                    objWeightItem.Note.Value = objWeightLogItem.Comments;
                    objWeightItem.When.Value = objWeightLogItem.DateMeasuredUTC;
                    objWeightItem.PolkaItemID.Value = objWeightLogItem.WTRecordID.ToString();
                    objWeightItemList.Add(objWeightItem);
                    listPolkaIDToBeUploaded.Add(objWeightLogItem.WTRecordID);
                }
                if (objWeightItemList.Count > 0)
                {
                    list.Add(typeof(HVManager.WeightDataManager).ToString(), objWeightItemList);
                }
                #endregion

                #region Exercise
                iteratorParam = navigator.Select("PolkaData/PhysicalActivityLogItem");
                List<ExerciseDataManager.ExerciseItem> objExerciseItemList = new List<ExerciseDataManager.ExerciseItem>();
                while (iteratorParam.MoveNext())
                {
                    XPathNavigator nav = iteratorParam.Current;
                    PhysicalActivityLogItem objPhysicalActivityLogItem = new PhysicalActivityLogItem();
                    objPhysicalActivityLogItem.ParseXml(nav);

                    ExerciseDataManager.ExerciseItem objExerciseItem = new ExerciseDataManager.ExerciseItem();
                    objExerciseItem.Activity.Value = "Physical Activity"; // objPhysicalActivityLogItem.ExerciseType;
                    objExerciseItem.Duration.Value = objPhysicalActivityLogItem.Duration > 0 ? objPhysicalActivityLogItem.Duration : dNull;

                //    switch (objPhysicalActivityLogItem.Intensity.ToLower())
                //    {
                //        case "high": objExerciseItem.Intensity.Value = RelativeRating.High;
                //            break;
                //        case "low": objExerciseItem.Intensity.Value = RelativeRating.Low;
                //            break;
                //        case "moderate": objExerciseItem.Intensity.Value = RelativeRating.Moderate;
                //            break;
                //        case "none": objExerciseItem.Intensity.Value = RelativeRating.None;
                //            break;
                //        case "veryhigh": objExerciseItem.Intensity.Value = RelativeRating.VeryHigh;
                //            break;
                //        case "verylow": objExerciseItem.Intensity.Value = RelativeRating.VeryLow;
                //            break;
                //        default:
                //            objExerciseItem.Intensity.Value = RelativeRating.None;
                //            break;

                //    }
                //    objExerciseItem.NumberOfSteps.Value = objPhysicalActivityLogItem.NumberOfSteps;
                    objExerciseItem.Note.Value = objPhysicalActivityLogItem.Comments;
                    objExerciseItem.When.Value = objPhysicalActivityLogItem.DateOfSessionUTC;

                    objExerciseItem.PolkaItemID.Value = objPhysicalActivityLogItem.PARecordID.ToString();
                    objExerciseItem.Source.Value = !string.IsNullOrEmpty(objPhysicalActivityLogItem.ExerciseType) ? objPhysicalActivityLogItem.ExerciseType : POLKA_DATA_SOURCE;
                    objExerciseItemList.Add(objExerciseItem);
                    listPolkaIDToBeUploaded.Add(objPhysicalActivityLogItem.PARecordID);
                }
                if (objExerciseItemList.Count > 0)
                {
                    list.Add(typeof(HVManager.ExerciseDataManager).ToString(), objExerciseItemList);
                }
                #endregion

                #region Medication
                //iteratorParam = navigator.Select("PolkaData/MedicationLogItem");
                //List<MedicationDataManager.MedicationItem> objMedicationItemList = new List<MedicationDataManager.MedicationItem>();
                //while (iteratorParam.MoveNext())
                //{
                //    XPathNavigator nav = iteratorParam.Current;
                //    MedicationLogItem objMedicationLogItem = new MedicationLogItem();
                //    objMedicationLogItem.ParseXml(nav);

                //    MedicationDataManager.MedicationItem objMedicationItem = new MedicationDataManager.MedicationItem();
                //    objMedicationItem.DateEntered.Value = objMedicationLogItem.DateTakenUTC;
                //    objMedicationItem.StartDate.Value = new HVApproximateDateTime(objMedicationLogItem.DateTakenUTC);
                //    objMedicationItem.Note.Value = objMedicationLogItem.Notes;
                //    objMedicationItem.PolkaItemID.Value = objMedicationLogItem.MDRecordID.ToString();
                //    objMedicationItem.Source.Value = POLKA_DATA_SOURCE;
                //    objMedicationItemList.Add(objMedicationItem);
                //    listPolkaIDToBeUploaded.Add(objMedicationLogItem.MDRecordID);
                //}
                //if (objMedicationItemList.Count > 0)
                //{
                //    list.Add(typeof(HVManager.MedicationDataManager).ToString(), objMedicationItemList);
                //}
                #endregion
                rdr.Close();
            }

            return list;
        }


        private static List<HVThing> GetHVItemsAlreadyExisting(bool bIsOnline, Guid hvpersonid, Guid hvrecordid, List<int> clientIdList)
        {

            foreach (var i in clientIdList)
            {
                if (i <= 0)
                {
                    throw new ArgumentException("Invalid client id passed - " + i);
                }

            }

            HealthRecordAccessor accessor = GetAccessor(bIsOnline, hvpersonid, hvrecordid);
            HealthRecordSearcher searcher = accessor.CreateSearcher();
            HealthRecordFilter filter = new HealthRecordFilter();
            filter.EffectiveDateMin = DateTime.MinValue;
            filter.EffectiveDateMax = DateTime.MaxValue;
            

            filter.TypeIds.Add(Microsoft.Health.ItemTypes.BloodGlucose.TypeId);
            filter.TypeIds.Add(Microsoft.Health.ItemTypes.BloodPressure.TypeId);
            filter.TypeIds.Add(Microsoft.Health.ItemTypes.Weight.TypeId);
            filter.TypeIds.Add(Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId);
            filter.TypeIds.Add(Microsoft.Health.ItemTypes.Exercise.TypeId);
            filter.TypeIds.Add(Microsoft.Health.ItemTypes.Medication.TypeId);

            foreach (int clientId in clientIdList)
            {
                filter.ClientItemIds.Add("POLKA-" + clientId.ToString());
            }

            searcher.Filters.Add(filter);

            //var matchingItems = searcher.GetMatchingItems()[0];

            //List<HVThing> result = new List<HVThing>();
            //foreach (HealthRecordItem item in matchingItems)
            //{
            //    HVThing hvThing = new HVThing();
            //    hvThing.ThingID = item.Key.Id;
            //    hvThing.TypeID = item.TypeId;
            //    hvThing.PolkaItemID = item.CommonData.ClientId;
            //    result.Add(hvThing);
            //}


            string strXsl = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?>
                <xsl:stylesheet version=""1.0""
	                xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:wc=""urn:com.microsoft.wc.methods.response.GetThings3"">
                <xsl:template match=""/"">
                  <root>
                    <xsl:for-each select=""//response/wc:info/group/thing"">
                    <thing>
                      <thing-id><xsl:value-of select=""thing-id"" /></thing-id>
                      <type-id><xsl:value-of select=""type-id"" /></type-id>
                      <polka-id><xsl:value-of select=""data-xml/common/extension/polka_item_id"" /></polka-id>
                    </thing>
                    </xsl:for-each>
                  </root>
                </xsl:template>
                </xsl:stylesheet>";

            string strXml = searcher.GetTransformedItems(strXsl);
            List<HVThing> listHVThing = new List<HVThing>();
            XDocument xDoc = XDocument.Parse(strXml);
            var queryResult = xDoc.Descendants("thing").Select(objAppDataType => new HVThing
            {
                ThingID = new Guid(objAppDataType.XPathSelectElement("thing-id").Value),
                TypeID = new Guid(objAppDataType.XPathSelectElement("type-id").Value),
                PolkaItemID = objAppDataType.XPathSelectElement("polka-id").Value,
            });
            return queryResult.ToList();



            //return result;
        }

        private static List<HVThing> GetHVItems(bool bIsOnline, Guid hvpersonid, Guid hvrecordid, List<int> iList)
        {
            HealthRecordAccessor accessor = GetAccessor(bIsOnline, hvpersonid, hvrecordid);
            HealthRecordSearcher searcher = accessor.CreateSearcher();

            HealthRecordFilter filterBloodGlucose = new HealthRecordFilter(Microsoft.Health.ItemTypes.BloodGlucose.TypeId);
            filterBloodGlucose.EffectiveDateMin = DateTime.MinValue;
            filterBloodGlucose.EffectiveDateMax = DateTime.MaxValue;
            searcher.Filters.Add(filterBloodGlucose);

            HealthRecordFilter filterBloodPressure = new HealthRecordFilter(Microsoft.Health.ItemTypes.BloodPressure.TypeId);
            filterBloodPressure.EffectiveDateMin = DateTime.MinValue;
            filterBloodPressure.EffectiveDateMax = DateTime.MaxValue;
            searcher.Filters.Add(filterBloodPressure);

            HealthRecordFilter filterWeight = new HealthRecordFilter(Microsoft.Health.ItemTypes.Weight.TypeId);
            filterWeight.EffectiveDateMin = DateTime.MinValue;
            filterWeight.EffectiveDateMax = DateTime.MaxValue;
            searcher.Filters.Add(filterWeight);

            HealthRecordFilter filterExercise = new HealthRecordFilter(Microsoft.Health.ItemTypes.Exercise.TypeId);
            filterExercise.EffectiveDateMin = DateTime.MinValue;
            filterExercise.EffectiveDateMax = DateTime.MaxValue;
            searcher.Filters.Add(filterExercise);

            HealthRecordFilter filterMedication = new HealthRecordFilter(Microsoft.Health.ItemTypes.Medication.TypeId);
            filterMedication.EffectiveDateMin = DateTime.MinValue;
            filterMedication.EffectiveDateMax = DateTime.MaxValue;
            searcher.Filters.Add(filterMedication);

            HealthRecordFilter filterCholesterolProfile = new HealthRecordFilter(Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId);
            filterCholesterolProfile.EffectiveDateMin = DateTime.MinValue;
            filterCholesterolProfile.EffectiveDateMax = DateTime.MaxValue;
            searcher.Filters.Add(filterCholesterolProfile);

            StringBuilder sbCondition = new StringBuilder();
            bool bFirst = true;
            foreach (int item in iList)
            {
                if (bFirst)
                {
                    sbCondition.AppendFormat("text()= '{0}'", item);
                    bFirst = false;
                }
                else
                {
                    sbCondition.AppendFormat(" or text()= '{0}'", item);
                }
            }

            string strXsl = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?>
                <xsl:stylesheet version=""1.0""
	                xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:wc=""urn:com.microsoft.wc.methods.response.GetThings3"">
                <xsl:template match=""/"">
                  <root>
                    <xsl:for-each select=""//response/wc:info/group/thing"">
                        <xsl:if test=""data-xml/common/extension/polka_item_id[{0}]"">
                    <thing>
                      <thing-id><xsl:value-of select=""thing-id"" /></thing-id>
                      <type-id><xsl:value-of select=""type-id"" /></type-id>
                      <polka-id><xsl:value-of select=""data-xml/common/extension/polka_item_id"" /></polka-id>
                    </thing>
                    </xsl:if>
                    </xsl:for-each>
                  </root>
                </xsl:template>
                </xsl:stylesheet>";

            string strXml = searcher.GetTransformedItems(string.Format(strXsl, sbCondition));
            List<HVThing> listHVThing = new List<HVThing>();
            XDocument xDoc = XDocument.Parse(strXml);
            var queryResult = xDoc.Descendants("thing").Select(objAppDataType => new HVThing
            {
                ThingID = new Guid(objAppDataType.XPathSelectElement("thing-id").Value),
                TypeID = new Guid(objAppDataType.XPathSelectElement("type-id").Value),
                PolkaItemID = objAppDataType.XPathSelectElement("polka-id").Value,
            });
            return queryResult.ToList();
        }

        public static Microsoft.Health.HealthRecordAccessor GetAccessor(bool bIsOnline, Guid hvpersonid, Guid hvrecordid)
        {
            if (!bIsOnline)
            {
                OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
                offlineConn.Authenticate();
                HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);
                return accessor;
            }
            else
            {
                Microsoft.Health.Web.HealthServicePage objCurrentPage = Microsoft.Health.Web.HealthServicePage.CurrentPage;
                if (objCurrentPage != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords.Count > 0)
                {
                    return Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords[hvrecordid];
                }
            }

            return null;
        }
    }
}
