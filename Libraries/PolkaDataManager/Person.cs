﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PolkaDataManager
{
    public class Person
    {
        public Person()
        {
        }

        private string _h360ID;

        public string H360ID
        {
            get { return _h360ID; }
            set { _h360ID = value; }
        }
        
        private BloodGlucoseLogItem[] _bloodGlucoseLogItems;

        public BloodGlucoseLogItem[] BloodGlucoseLogItems
        {
            get { return _bloodGlucoseLogItems; }
            set { _bloodGlucoseLogItems = value; }
        }

        private BloodPressureLogItem[] _bloodPressureLogItems;

        public BloodPressureLogItem[] BloodPressureLogItems
        {
            get { return _bloodPressureLogItems; }
            set { _bloodPressureLogItems = value; }
        }

        private CholesterolLogItem[] _cholesterolLogItems;

        public CholesterolLogItem[] CholesterolLogItems
        {
            get { return _cholesterolLogItems; }
            set { _cholesterolLogItems = value; }
        }

        private MedicationLogItem[] _medicationLogItems;

        public MedicationLogItem[] MedicationLogItems
        {
            get { return _medicationLogItems; }
            set { _medicationLogItems = value; }
        }

        private PhysicalActivityLogItem[] _physicalActivityLogItems;

        public PhysicalActivityLogItem[] PhysicalActivityLogItems
        {
            get { return _physicalActivityLogItems; }
            set { _physicalActivityLogItems = value; }
        }

        private WeightLogItem[] _weightLogItems;

        public WeightLogItem[] WeightLogItems
        {
            get { return _weightLogItems; }
            set { _weightLogItems = value; }
        }
    }
}