﻿using System;
using System.Xml.XPath;

namespace PolkaDataManager
{
    public class PhysicalActivityLogItem : IXmlPersist
    {
        public PhysicalActivityLogItem()
        {
        }


        private int _paRecordID;

        public int PARecordID
        {
            get { return _paRecordID; }
            set { _paRecordID = value; }
        }

        private string _exerciseType;

        public string ExerciseType
        {
            get { return _exerciseType; }
            set { _exerciseType = value; }
        }

        private float _duration;

        public float Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        private string _intensity;

        public string Intensity
        {
            get { return _intensity; }
            set { _intensity = value; }
        }

        private int _numberOfSteps;

        public int NumberOfSteps
        {
            get { return _numberOfSteps; }
            set { _numberOfSteps = value; }
        }

        private string _comments;

        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        private DateTime _dateOfSessionUTC;

        public DateTime DateOfSessionUTC
        {
            get { return _dateOfSessionUTC; }
            set { _dateOfSessionUTC = value; }
        }


        #region IXmlPersist Members

        public void ParseXml(System.Xml.XPath.IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.Clone();

            if (!string.IsNullOrEmpty(navigator.GetAttribute("PARecordID", string.Empty)))
            {
                PARecordID = Convert.ToInt32(navigator.GetAttribute("PARecordID", string.Empty));
            }

            ExerciseType = navigator.GetAttribute("ExerciseType", string.Empty);

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Duration", string.Empty)))
            {
                Duration = float.Parse(navigator.GetAttribute("Duration", string.Empty));
            }

            Intensity = navigator.GetAttribute("Intensity", string.Empty);

            if (!string.IsNullOrEmpty(navigator.GetAttribute("NumberOfSteps", string.Empty)))
            {
                NumberOfSteps = Convert.ToInt32(navigator.GetAttribute("NumberOfSteps", string.Empty));
            }

            Comments = navigator.GetAttribute("Comments", string.Empty);

            DateOfSessionUTC = DateTime.Parse(navigator.GetAttribute("DateMeasuredUTC", string.Empty));
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("PhysicalActivityLogItem");
            writer.WriteAttributeString("PARecordID", this.PARecordID.ToString());
            writer.WriteAttributeString("ExerciseType", this.Comments);
            writer.WriteAttributeString("Duration", this.Duration.ToString());
            writer.WriteAttributeString("Intensity", this.Intensity);
            writer.WriteAttributeString("NumberOfSteps", this.NumberOfSteps.ToString());
            writer.WriteAttributeString("Comments", this.Comments);
            writer.WriteAttributeString("DateOfSessionUTC", AHACommon.CommonUtility.GetDateTimeForDB(this.DateOfSessionUTC));
            writer.WriteEndElement();
        }

        #endregion
    }
}