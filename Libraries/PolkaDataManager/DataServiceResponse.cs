﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PolkaDataManager
{
    public class DataServiceResponse
    {
        public DataServiceResponse()
        {
        }

        private bool _requestSuccessful;

        public bool RequestSuccessful
        {
            get { return _requestSuccessful; }
            set { _requestSuccessful = value; }
        }

        private string _errorMessage;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        private Person[] _people;

        public Person[] People
        {
            get { return _people; }
            set { _people = value; }
        }

        private DateTime _timestampUTC;

        public DateTime TimestampUTC
        {
            get { return _timestampUTC; }
            set { _timestampUTC = value; }
        }
    }
}