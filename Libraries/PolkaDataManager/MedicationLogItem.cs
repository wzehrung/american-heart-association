﻿using System;
using System.Xml.XPath;

namespace PolkaDataManager
{
    public class MedicationLogItem : IXmlPersist
    {
        public MedicationLogItem()
        {
        }


        private int _mdRecordID;

        public int MDRecordID
        {
            get { return _mdRecordID; }
            set { _mdRecordID = value; }
        }

        private int _timeframe;

        public int Timeframe
        {
            get { return _timeframe; }
            set { _timeframe = value; }
        }

        private string _notes;

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        private DateTime _dateTakenUTC;

        public DateTime DateTakenUTC
        {
            get { return _dateTakenUTC; }
            set { _dateTakenUTC = value; }
        }


        #region IXmlPersist Members

        public void ParseXml(System.Xml.XPath.IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.Clone();

            if (!string.IsNullOrEmpty(navigator.GetAttribute("MDRecordID", string.Empty)))
            {
                MDRecordID = Convert.ToInt32(navigator.GetAttribute("MDRecordID", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Timeframe", string.Empty)))
            {
                Timeframe = Convert.ToInt32(navigator.GetAttribute("Timeframe", string.Empty));
            }

            Notes = navigator.GetAttribute("Notes", string.Empty);

            DateTakenUTC = DateTime.Parse(navigator.GetAttribute("DateTakenUTC", string.Empty));
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("MedicationLogItem");
            writer.WriteAttributeString("MDRecordID", this.MDRecordID.ToString());
            writer.WriteAttributeString("Timeframe", this.Timeframe.ToString());
            writer.WriteAttributeString("Notes", this.Notes);
            writer.WriteAttributeString("DateTakenUTC", AHACommon.CommonUtility.GetDateTimeForDB(this.DateTakenUTC));
            writer.WriteEndElement();
        }

        #endregion
    }
}