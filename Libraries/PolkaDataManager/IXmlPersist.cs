﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using System.Xml;

namespace PolkaDataManager
{
    public interface IXmlPersist
    {
        void ParseXml(IXPathNavigable typeSpecificXml);
        void WriteXml(XmlWriter writer);
    }
}
