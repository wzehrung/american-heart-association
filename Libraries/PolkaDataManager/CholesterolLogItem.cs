﻿using System;
using System.Xml.XPath;

namespace PolkaDataManager
{
    public class CholesterolLogItem : IXmlPersist
    {
        public CholesterolLogItem()
        {
        }


        private int _chRecordID;

        public int CHRecordID
        {
            get { return _chRecordID; }
            set { _chRecordID = value; }
        }

        private int _hdl;

        public int Hdl
        {
            get { return _hdl; }
            set { _hdl = value; }
        }

        private int _ldl;

        public int Ldl
        {
            get { return _ldl; }
            set { _ldl = value; }
        }

        private int _triglyceride;

        public int Triglyceride
        {
            get { return _triglyceride; }
            set { _triglyceride = value; }
        }

        private int _totalCholesterol;

        public int TotalCholesterol
        {
            get { return _totalCholesterol; }
            set { _totalCholesterol = value; }
        }

        private string _location;

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        private DateTime _dateMeasuredUTC;

        public DateTime DateMeasuredUTC
        {
            get { return _dateMeasuredUTC; }
            set { _dateMeasuredUTC = value; }
        }


        #region IXmlPersist Members

        public void ParseXml(System.Xml.XPath.IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.Clone();

            if (!string.IsNullOrEmpty(navigator.GetAttribute("CHRecordID", string.Empty)))
            {
                CHRecordID = Convert.ToInt32(navigator.GetAttribute("CHRecordID", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Hdl", string.Empty)))
            {
                Hdl = Convert.ToInt32(navigator.GetAttribute("Hdl", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Ldl", string.Empty)))
            {
                Ldl = Convert.ToInt32(navigator.GetAttribute("Ldl", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Triglyceride", string.Empty)))
            {
                Triglyceride = Convert.ToInt32(navigator.GetAttribute("Triglyceride", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("TotalCholesterol", string.Empty)))
            {
                TotalCholesterol = Convert.ToInt32(navigator.GetAttribute("TotalCholesterol", string.Empty));
            }

            Location = navigator.GetAttribute("Location", string.Empty);

            DateMeasuredUTC = DateTime.Parse(navigator.GetAttribute("DateMeasuredUTC", string.Empty));
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("CholesterolLogItem");
            writer.WriteAttributeString("CHRecordID", this.CHRecordID.ToString());
            writer.WriteAttributeString("Hdl", this.Hdl.ToString());
            writer.WriteAttributeString("Ldl", this.Ldl.ToString());
            writer.WriteAttributeString("Triglyceride", this.Triglyceride.ToString());
            writer.WriteAttributeString("TotalCholesterol", this.TotalCholesterol.ToString());
            writer.WriteAttributeString("Location", this.Location);
            writer.WriteAttributeString("DateMeasuredUTC", AHACommon.CommonUtility.GetDateTimeForDB(this.DateMeasuredUTC));
            writer.WriteEndElement();
        }

        #endregion
    }
}