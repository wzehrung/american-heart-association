﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolkaDataManager
{
    public class HVThing
    {
        public Guid ThingID
        {
            get;
            set;
        }

        public Guid TypeID
        {
            get;
            set;
        }

        public string PolkaItemID
        {
            get;
            set;
        }
    }
}
