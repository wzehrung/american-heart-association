﻿using System;
using System.Xml.XPath;

namespace PolkaDataManager
{
    [Serializable]
    public class WeightLogItem : IXmlPersist
    {
        public WeightLogItem()
        {
        }


        private int _wtRecordID;

        public int WTRecordID
        {
            get { return _wtRecordID; }
            set { _wtRecordID = value; }
        }

        private float _weight;

        public float Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private string _readingSource;

        public string ReadingSource
        {
            get { return _readingSource; }
            set { _readingSource = value; }
        }

        private string _comments;

        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        private DateTime _dateMeasuredUTC;

        public DateTime DateMeasuredUTC
        {
            get { return _dateMeasuredUTC; }
            set { _dateMeasuredUTC = value; }
        }


        #region IXmlPersist Members

        public void ParseXml(System.Xml.XPath.IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.Clone();

            if (!string.IsNullOrEmpty(navigator.GetAttribute("WTRecordID", string.Empty)))
            {
                WTRecordID = Convert.ToInt32(navigator.GetAttribute("WTRecordID", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("Weight", string.Empty)))
            {
                Weight = float.Parse(navigator.GetAttribute("Weight", string.Empty));
            }

            ReadingSource = navigator.GetAttribute("ReadingSource", string.Empty);

            Comments = navigator.GetAttribute("Comments", string.Empty);

            DateMeasuredUTC = DateTime.Parse(navigator.GetAttribute("DateMeasuredUTC", string.Empty));
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("WeightLogItem");
            writer.WriteAttributeString("WTRecordID", this.WTRecordID.ToString());
            writer.WriteAttributeString("Weight", this.Weight.ToString());
            writer.WriteAttributeString("ReadingSource", this.ReadingSource);
            writer.WriteAttributeString("Comments", this.Comments);
            writer.WriteAttributeString("DateMeasuredUTC", AHACommon.CommonUtility.GetDateTimeForDB(this.DateMeasuredUTC));
            writer.WriteEndElement();
        }

        #endregion
    }
}