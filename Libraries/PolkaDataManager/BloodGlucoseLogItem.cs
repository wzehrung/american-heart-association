﻿using System;
using System.Xml.XPath;

namespace PolkaDataManager
{
    public class BloodGlucoseLogItem : IXmlPersist
    {
        public BloodGlucoseLogItem()
        {
        }


        private int _bgRecordID;

        public int BGRecordID
        {
            get { return _bgRecordID; }
            set { _bgRecordID = value; }
        }


        private float _bloodGlucoseValue;

        public float BloodGlucoseValue
        {
            get { return _bloodGlucoseValue; }
            set { _bloodGlucoseValue = value; }
        }

        private string _readingType;

        public string ReadingType
        {
            get { return _readingType; }
            set { _readingType = value; }
        }

        private string _actionITook;

        public string ActionITook
        {
            get { return _actionITook; }
            set { _actionITook = value; }
        }

        private string _notes;

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        private DateTime _dateMeasuredUTC;

        public DateTime DateMeasuredUTC
        {
            get { return _dateMeasuredUTC; }
            set { _dateMeasuredUTC = value; }
        }


        #region IXmlPersist Members

        public void ParseXml(System.Xml.XPath.IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.Clone();

            if (!string.IsNullOrEmpty(navigator.GetAttribute("BGRecordID", string.Empty)))
            {
                BGRecordID = Convert.ToInt32(navigator.GetAttribute("BGRecordID", string.Empty));
            }

            if (!string.IsNullOrEmpty(navigator.GetAttribute("BloodGlucoseValue", string.Empty)))
            {
                BloodGlucoseValue = float.Parse(navigator.GetAttribute("BloodGlucoseValue", string.Empty));
            }

            ReadingType = navigator.GetAttribute("ReadingType", string.Empty);
            ActionITook = navigator.GetAttribute("ActionITook", string.Empty);
            Notes = navigator.GetAttribute("Notes", string.Empty);

            DateMeasuredUTC = DateTime.Parse(navigator.GetAttribute("DateMeasuredUTC", string.Empty));
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("BloodGlucoseLogItem");
            writer.WriteAttributeString("BGRecordID", this.BGRecordID.ToString());
            writer.WriteAttributeString("BloodGlucoseValue", this.BloodGlucoseValue.ToString());
            writer.WriteAttributeString("ReadingType", this.ReadingType);
            writer.WriteAttributeString("ActionITook", this.ActionITook);
            writer.WriteAttributeString("Notes", this.Notes);
            writer.WriteAttributeString("DateMeasuredUTC", AHACommon.CommonUtility.GetDateTimeForDB(this.DateMeasuredUTC));
            writer.WriteEndElement();
        }

        #endregion
    }
}