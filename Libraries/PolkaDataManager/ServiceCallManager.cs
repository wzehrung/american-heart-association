﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.IO;
using JsonExSerializer;

namespace PolkaDataManager
{
    public class ServiceCallManager
    {
        // Left public in case dynamic adjustments need to be made w/o recompile.
        public string encryptionKey = "357b785e61394756543e544f4b";
        public string callerID = "31512a244b3f74715d5d4c3349";
        public string url = "https://360.polka.com/polkaservices/Services.aspx?";
        public string json = "";

        public ServiceCallManager()
        {
        }

        public DataServiceResponse CallServiceGetRecordsByUserIDs(string sServiceName, string UserIDsCommaDelimited)
        {
            string sUrl = url;
            
            sUrl += "service=" + sServiceName + "&uids=" + UserIDsCommaDelimited + "&udr_key=" + callerID;

            return CallService(sUrl);
        }

        public DataServiceResponse CallServiceGetRecordsAfterGivenTime(string sServiceName, string sGivenTimeUTC)
        {
            string sUrl = url;
            
            sUrl += "service=" + sServiceName + "&giventime=" + sGivenTimeUTC + "&udr_key=" + callerID;

            return CallService(sUrl);
        }

        public DataServiceResponse CallServiceGetRecordsByUserIDsAfterGivenTime
            (string sServiceName, string UserIDsCommaDelimited, string sGivenTimeUTC)
        {
            string sUrl = url;

            sUrl += "service=" + sServiceName + "&uids=" + UserIDsCommaDelimited + "&giventime=" + sGivenTimeUTC + 
                "&udr_key=" + callerID;

            return CallService(sUrl);
        }

        

        private DataServiceResponse CallService(string sUrl)
        {
            Serializer ser = new Serializer(typeof(DataServiceResponse));
            Security sec = new Security();
            DataServiceResponse serviceData = null;
            WebResponse oResponse;
            string serviceResponseEncrypted;
            string decryptedJson = "";


            WebRequest oRequest = HttpWebRequest.Create(sUrl);

            // *** Make call to web service ***
            oResponse = oRequest.GetResponse();

            using (StreamReader sr = new StreamReader(oResponse.GetResponseStream()))
            {
                serviceResponseEncrypted = sr.ReadToEnd();
                sr.Close();
            }

            try
            {
                // Security Decryption.
                decryptedJson = sec.Decrypt(serviceResponseEncrypted, encryptionKey);

                // Deserialize JSON.
                serviceData = (DataServiceResponse)ser.Deserialize(decryptedJson);

                json = decryptedJson;
            }
            catch (Exception ex)
            {
                // Catch problem with decrypting or deserializing however needed.
            }

            return serviceData;
        }
    }
}