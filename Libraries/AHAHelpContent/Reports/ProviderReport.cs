﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace AHAHelpContent
{
    public class ProviderReport
    {
            // used in V6, issue #155, 156
        public static DataSet GetAllProvidersReport_RollUp(DateTime dtStart, DateTime dtEnd)
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_H360Providers_RollUp]", dtStart, dtEnd);
        }

            // used in V6 - Replaced by GetAllProvidersReport_RollUp - WZ
        //public static DataSet GetAllProvidersReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        //{
        //    return Misc.GetReport("[V6_Proc_Report_Provider_H360Providers]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        //}

            // used in V6
        public static DataSet GetAllProviderVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_AllProviderPercentage]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID );
        }

            // used in V6, issue #155, 156
        public static DataSet GetAllProviderVisitsReport_RollUp(DateTime dtStart, DateTime dtEnd)
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_AllProviderVisits_RollUp]", dtStart, dtEnd);
        }

            // Replaced by GetAllProviderVisitsReport_RollUp - WZ
        //public static DataSet GetAllProviderVisitsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        //{
        //    return Misc.GetReport("[V6_Proc_Report_Provider_AllProviderVisits]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        //}

            // used in V6
        public static DataSet GetNewProviderVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_NewProvidersPercentage]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID );
        }

            // used in V6, issue #155, 156
        public static DataSet GetNewProviderVisitsReport_RollUp(DateTime dtStart, DateTime dtEnd)
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_NewProviderVisits_RollUp]", dtStart, dtEnd);
        }

            // Replaced by GetNewProviderVisitsReport_RollUp - WZ
        public static DataSet GetNewProviderVisitsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_NewProviderVisits]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

            // used in V6
        public static DataSet GetPatientCountPerProviderReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets )
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_PatientsPerProvider]", dtStart, dtEnd, iCampaignID, sMarkets );
        }

            // used in V6
        public static DataSet GetProviderConnectedToPatientsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets )
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_ConnectedToPatients]", dtStart, dtEnd, iCampaignID, sMarkets );
        }

        public static DataSet GetReportedUploadsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            return Misc.GetReport("[Proc_Report_Provider_ReportedUploads]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID );
        }
    }

    public class ProviderReportCampaign
    {
        public static DataSet GetCampaignsForAffiliatesReport()
        {
            return Misc.GetReport("[Proc_Report_GetAllCampaignsForAffiliates]", "@Affiliate", null );
        }

        public static DataSet GetPartnersForCampaignsReport(string Campaigns)
        {
            return Misc.GetReport("[Proc_Report_GetPartnersForCampaigns]", "@CampaignIDs", Campaigns);
        }

        // used in V6, needed a campaign specific one - WZ
        public static DataSet GetAllProvidersReport_RollUp(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            return Misc.GetReport("[V6_Proc_Report_Provider_H360Providers_RollUp_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

            // used in V6
        public static DataSet GetAllProviderVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_AllProviderPercentage_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

            // used in V6
        public static DataSet GetAllProviderVisitsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_AllProviderVisits_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID );
        }
            // used in V6, Issues #155, 156
        public static DataSet GetAllProviderVisitsReport_RollUp(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_AllProviderVisits_RollUp_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }


        public static DataSet GetCampaignProvidersReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReportCampaign("[Proc_Report_Provider_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

            // used in V6
        public static DataSet GetProviderConnectedToPatientsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_ConnectedToPatients_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

            // used in V6
        public static DataSet GetAllProvidersReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_H360Providers_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

            // used in V6
        public static DataSet GetNewProviderVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_NewProvidersPercentage_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

        // used in V6 - Replaced by GetNewProviderVisitsReport_RollUp_Campaign
        public static DataSet GetNewProviderVisitsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_NewProviderVisits_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }
            // used in V6, issue #155, 156
        public static DataSet GetNewProviderVisitsReport_RollUp(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_NewProviderVisits_RollUp_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

            // used in V6
        public static DataSet GetPatientCountPerProviderReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Provider_PatientsPerProvider_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }

        public static DataSet GetReportedUploadsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            return Misc.GetReportCampaign("[Proc_Report_Provider_ReportedUploads_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets, iUserTypeID);
        }


    }
}
