﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GRCBase;
using System.Data;
using AHACommon;


namespace AHAHelpContent.Reports
{
    public enum PatientHealth_ReportType
    {
        StartingPeriodReport,
        EndingPeriodReport,
        PeriodDifferenceReport
    }

    public class HealthStatus
    {
        public string Gender
        {
            get;
            set;
        }

        public double Healthy
        {
            get;
            set;
        }

        public double HealthyPerc
        {
            get;
            set;
        }

        public double Acceptable
        {
            get;
            set;
        }

        public double AcceptablePerc
        {
            get;
            set;
        }

        public double AtRisk
        {
            get;
            set;
        }

        public double AtRiskPerc
        {
            get;
            set;
        }
    }

    public class PatientHealthReport
    {
        public PatientHealth_ReportType PatientHealthReportType
        {
            get;
            set;
        }
        public List<HealthStatus> HealthStatusList
        {
            get;
            set;
        }

        public static List<PatientHealthReport> GetPatientHealthReport(string ahaGender, int? campaignID, int? providerID, bool takeActiveUsers, DateTime startDate, DateTime? endDate, TrackerDataType trackerDataType)
        {
            SqlDataReader reader = null;
            DBContext dbContext = null;
            SqlCommand cmd = null;

            bool bException = false;

            List<PatientHealthReport> patientHealthReportList = new List<PatientHealthReport>();

            try
            {
                dbContext = DBContext.GetDBContext();
                string sql = string.Empty;
                switch(trackerDataType)
                {
                    case TrackerDataType.BloodGlucose :
                        sql = "Proc_Report_HyperTension_BloodGlucose";
                        break;
                    case TrackerDataType.BloodPressure:
                        sql = "Proc_Report_HyperTension_Bloodpressure";
                        break;
                    case TrackerDataType.Cholesterol:
                        sql = "Proc_Report_HyperTension_Cholesterol";
                        break;
                    case TrackerDataType.Weight:
                        sql = "Proc_Report_HyperTension_BMI";
                        break;

                }
                    
                SqlParameter param = null;


                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@gender", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(ahaGender);


                param = cmd.Parameters.Add("@campaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(campaignID);

                param = cmd.Parameters.Add("@providerID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(providerID);

                param = cmd.Parameters.Add("@takeActiveUsers", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(takeActiveUsers);

                param = cmd.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(startDate);

                param = cmd.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.NullableDateToDbValue(endDate);

                reader = cmd.ExecuteReader();
                int intResultCount = 0;
                do
                {
                    List<HealthStatus> healthStatusList = new List<HealthStatus>();
                    while (reader.Read())
                    {
                        HealthStatus healthStatus = new HealthStatus();
                        if (reader["Gender"] != null)
                        {
                            healthStatus.Gender = reader["Gender"].ToString();
                        }
                        if (reader["Healthy"] != null)
                        {
                            healthStatus.Healthy = Convert.ToDouble(reader["Healthy"]);
                        }
                        if (reader["HealthyPerc"] != null)
                        {
                            healthStatus.HealthyPerc = Convert.ToDouble(reader["HealthyPerc"]);
                        }
                        if (reader["Acceptable"] != null)
                        {
                            healthStatus.Acceptable = Convert.ToDouble(reader["Acceptable"]);
                        }
                        if (reader["AcceptablePerc"] != null)
                        {
                            healthStatus.AcceptablePerc = Convert.ToDouble(reader["AcceptablePerc"]);
                        }
                        if (reader["AtRisk"] != null)
                        {
                            healthStatus.AtRisk = Convert.ToDouble(reader["AtRisk"]);
                        }
                        if (reader["AtRiskPerc"] != null)
                        {
                            healthStatus.AtRiskPerc = Convert.ToDouble(reader["AtRiskPerc"]);
                        }
                        healthStatusList.Add(healthStatus);
                    }
                    PatientHealthReport patientHealthReport = new PatientHealthReport();
                    switch (intResultCount)
                    {
                        case 0:
                            patientHealthReport.PatientHealthReportType = PatientHealth_ReportType.StartingPeriodReport;
                            break;
                        case 1:
                            patientHealthReport.PatientHealthReportType = PatientHealth_ReportType.EndingPeriodReport;
                            break;
                        case 2:
                            patientHealthReport.PatientHealthReportType = PatientHealth_ReportType.PeriodDifferenceReport;
                            break;
                    }
                    patientHealthReport.HealthStatusList = healthStatusList;
                    patientHealthReportList.Add(patientHealthReport);
                    intResultCount++;
                } while (reader.NextResult());
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return patientHealthReportList;
        }
    }
}
