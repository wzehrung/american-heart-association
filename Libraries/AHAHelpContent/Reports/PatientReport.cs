﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
        
    public class PatientReport
    {
        // PJB: note in V6 I just left iPartnerID parameter, even though it is always null
        //      the stored procs actually take 4 parameters, the last two being null
        //      further refactoring would be best after customer requirements.

            // used in V6
        public static DataSet GetHeart360UserReport(DateTime dtStart, DateTime dtEnd, int? iPartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_H360Users]", dtStart, dtEnd, iPartnerID);
        }

            // used in V6
        public static DataSet GetTrackerReport(DateTime dtStart, DateTime dtEnd, int? iPartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_Tracking]", dtStart, dtEnd, iPartnerID);
        }

            // used in V6
        public static DataSet GetGenderBreakdownReport(DateTime dtStart, DateTime dtEnd, int? PartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_Gender]", dtStart, dtEnd, PartnerID);
        }

            // used in V6
        public static DataSet GetAgeBreakdownReport(DateTime dtStart, DateTime dtEnd, int? PartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_Age]", dtStart, dtEnd, PartnerID);
        }

        public static DataSet GetNewUserVisitsReport(DateTime dtStart, DateTime dtEnd, int? PartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_NewUsers]", dtStart, dtEnd, PartnerID);
        }

            // used in V6
        public static DataSet GetAllUserVisitsReport(DateTime dtStart, DateTime dtEnd, int? PartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_AllUsers]", dtStart, dtEnd, PartnerID);
        }

            
        public static DataSet GetNewUserVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? PartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_NewUsersPercentage]", dtStart, dtEnd, PartnerID);
        }

            // used in V6
        public static DataSet GetAllUserVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? PartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_AllUsersPercentage]", dtStart, dtEnd, PartnerID);
        }

            // used in V6
        public static DataSet GetUploadMethodTrackerReport(DateTime dtStart, DateTime dtEnd, int? iPartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_UploadMethodTracking]", dtStart, dtEnd, iPartnerID);
        }

            // used in V6
        public static DataSet GetEthnicityBreakdownReport(DateTime dtStart, DateTime dtEnd, int? iPartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_Ethnicity]", dtStart, dtEnd, iPartnerID);
        }

            // used in V6
        public static DataSet GetReportedUploadsReport(DateTime dtStart, DateTime dtEnd, int? iPartnerID)
        {
            return Misc.GetReportPatients("[Proc_Report_ReportedUploads]", dtStart, dtEnd, iPartnerID);
        }

        public static DataSet GetCampaignsForAffiliatesReport(string inAffiliate)
        {
            // PJB 1.28.2014 Keep this backward compatible.
            return Misc.GetReport("[Proc_Report_GetAllCampaignsForAffiliates]", "@Affiliate", inAffiliate);
        }

        public static DataSet GetBusinessAdminCampaignsForAffiliatesReport(int BusinessID)
        {
            return Misc.GetReportById("[Proc_Report_GetBusinessAdminCampaigns]", "@BusinessID", BusinessID );
        }

        public static DataSet GetAffiliatesForCampaignsReport(int? inCampaignId)
        {
            return Misc.GetReportById("[Proc_Report_GetAllAffiliatesForCampaign]", "@CampaignId", inCampaignId);
        }

        public static DataSet GetMarketsForAffiliateReport( string inAffiliate)
        {
            return Misc.GetReport("[Proc_Report_GetAllMarketsForAffiliate]", "@AffiliateName", inAffiliate);
        }

        public static DataSet GetPartnersForCampaignsReport(string Campaigns)
        {
            return Misc.GetReport("[Proc_Report_GetPartnersForCampaigns]", "@CampaignIDs", Campaigns);
        }
    }

    public class PatientReportCampaigns
    {
        // WZ - Added 5/12/2015

        // Heart360 Users
        public static DataSet GetHeart360UserReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_H360Users_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // % Tracking (Monthly Cumulative)
        public static DataSet GetTrackerReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Tracking_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // Reported Uploads (# of users who uploaded data 2X's per month.)
        public static DataSet GetReportedUploadsReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_ReportedUploads_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // % Method of Upload/Reading Source (Monthly Cumulative)
        public static DataSet GetUploadMethodTrackerReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_UploadMethodTracking_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // Reported Visits (All/New Users) – (New Patients/Participants only who visited Heart360® during the time frame selected.)
        public static DataSet GetAllUserVisitsReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_AllUsers_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        public static DataSet GetNewUserVisitsReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[Proc_Report_NewUsers_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // Reported Visits in % (All/New Users) – (New Patients/Participants only who visited Heart360® during the time frame selected.)
        public static DataSet GetAllUserVisitsPercentageReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_AllUsersPercentage_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        public static DataSet GetNewUserVisitsPercentageReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[Proc_Report_NewUsersPercentage_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // Gender (Monthly Cumulative)
        public static DataSet GetGenderBreakdownReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Gender_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // Ethnicity (Monthly Cumulative)
        public static DataSet GetEthnicityBreakdownReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Ethnicity_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }

        // Age Breakdown (Monthly Cumulative)
        public static DataSet GetAgeBreakdownReportCampaign(DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Age_Campaign]", dtStart, dtEnd, sCampaignIDs, sMarkets);
        }
        
        
        // Current 
        public static DataSet GetHeart360UserReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_H360Users_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetTrackerReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Tracking_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetGenderBreakdownReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Gender_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetAgeBreakdownReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Age_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetNewUserVisitsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[Proc_Report_NewUsers_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetAllUserVisitsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_AllUsers_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetNewUserVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[Proc_Report_NewUsersPercentage_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetAllUserVisitsPercentageReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_AllUsersPercentage_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetCampaign_PatientsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[Proc_Report_Campaign_Patients_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetUploadMethodTrackerReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_UploadMethodTracking_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetEthnicityBreakdownReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_Ethnicity_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }

        public static DataSet GetReportedUploadsReport(DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets)
        {
            return Misc.GetReportCampaign("[V6_Proc_Report_ReportedUploads_Campaign]", dtStart, dtEnd, iCampaignID, sMarkets);
        }
    }
}
