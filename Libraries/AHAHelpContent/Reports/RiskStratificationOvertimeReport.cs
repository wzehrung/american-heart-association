﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GRCBase;
using System.Data;

namespace AHAHelpContent.Reports
{
    public class RiskStratificationOvertimeReport
    {
        public DateTime Date
        {
            get;
            set;
        }

        public double Healthy
        {
            get;
            set;
        }

        public double HealthyPerc
        {
            get;
            set;
        }

        public double Acceptable
        {
            get;
            set;
        }

        public double AcceptablePerc
        {
            get;
            set;
        }



        public double AtRisk
        {
            get;
            set;
        }

        public double AtRiskPerc
        {
            get;
            set;
        }

        public double VeryHighRisk
        {
            get;
            set;
        }

        public double VeryHighRiskPerc
        {
            get;
            set;
        }

        public double InActiveUsers
        {
            get;
            set;
        }

        public double InActiveUsersPerc
        {
            get;
            set;
        }

        public double Total
        {
            get;
            set;
        }


        public static List<RiskStratificationOvertimeReport> GetRiskStratificationOverTimeReport(DateTime startDate, DateTime endDate, int? campaignID, int? providerID, TrackerDataType trackerDataType)
        {
            SqlDataReader reader = null;
            DBContext dbContext = null;
            SqlCommand cmd = null;

            bool bException = false;

            List<RiskStratificationOvertimeReport> RiskStratificationOvertimeReportList = new List<RiskStratificationOvertimeReport>();

            try
            {
                dbContext = DBContext.GetDBContext();
                string sql = string.Empty;
                switch (trackerDataType)
                {
                    case TrackerDataType.Cholesterol:
                        sql = "Proc_Report_Additional_Cholesterol";
                        break;
                    case TrackerDataType.BloodPressure:
                        sql = "Proc_Report_Additional_BloodPressure";
                        break;
                    case TrackerDataType.BloodGlucose:
                        sql = "Proc_Report_Additional_BloodGlucose";
                        break;
                    case TrackerDataType.Weight:
                        sql = "Proc_Report_Additional_BMI";
                        break;
                }

                SqlParameter param = null;


                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@campaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(campaignID);

                param = cmd.Parameters.Add("@providerID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(providerID);

                param = cmd.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(startDate);

                param = cmd.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(endDate);

               

                reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    RiskStratificationOvertimeReport riskStratificationOvertimeReport = new RiskStratificationOvertimeReport();
                    if (reader["Date1"] != null)
                    {
                        riskStratificationOvertimeReport.Date = Convert.ToDateTime(reader["Date1"]);
                    }
                    if (reader["Healthy"] != null)
                    {
                        riskStratificationOvertimeReport.Healthy = Convert.ToDouble(reader["Healthy"]);
                    }
                    if (reader["HealthyPerc"] != null)
                    {
                        riskStratificationOvertimeReport.HealthyPerc = Convert.ToDouble(reader["HealthyPerc"]);
                    }
                    if (reader["Acceptable"] != null)
                    {
                        riskStratificationOvertimeReport.Acceptable = Convert.ToDouble(reader["Acceptable"]);
                    }
                    if (reader["AcceptablePerc"] != null)
                    {
                        riskStratificationOvertimeReport.AcceptablePerc = Convert.ToDouble(reader["AcceptablePerc"]);
                    }
                    if (reader["AtRisk"] != null)
                    {
                        riskStratificationOvertimeReport.AtRisk = Convert.ToDouble(reader["AtRisk"]);
                    }
                    if (reader["AtRiskPerc"] != null)
                    {
                        riskStratificationOvertimeReport.AtRiskPerc = Convert.ToDouble(reader["AtRiskPerc"]);
                    }

                    if (reader["inactiveUsers"] != null)
                    {
                        riskStratificationOvertimeReport.InActiveUsers = Convert.ToDouble(reader["inactiveUsers"]);
                    }
                    if (reader["inactiveUsersPerc"] != null)
                    {
                        riskStratificationOvertimeReport.InActiveUsersPerc = Convert.ToDouble(reader["inactiveUsersPerc"]);
                    }

                    //if (trackerDataType == TrackerDataType.BloodPressure)
                    //{
                    //    if (reader["VeryHighRisk"] != null)
                    //    {
                    //        riskStratificationOvertimeReport.VeryHighRisk = Convert.ToDouble(reader["VeryHighRisk"]);
                    //    }
                    //    if (reader["VeryHighRiskPerc"] != null)
                    //    {
                    //        riskStratificationOvertimeReport.VeryHighRiskPerc = Convert.ToDouble(reader["VeryHighRiskPerc"]);
                    //    }
                    //}
                    riskStratificationOvertimeReport.Total = riskStratificationOvertimeReport.Healthy + riskStratificationOvertimeReport.Acceptable + riskStratificationOvertimeReport.AtRisk + riskStratificationOvertimeReport.VeryHighRisk + riskStratificationOvertimeReport.InActiveUsers;
                    RiskStratificationOvertimeReportList.Add(riskStratificationOvertimeReport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return RiskStratificationOvertimeReportList;
        }
    }
}
