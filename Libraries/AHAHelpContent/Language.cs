﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GRCBase;
using System.Data;

namespace AHAHelpContent
{
    public class Language
    {
        public int LanguageID
        {
            get;
            set;
        }

        public string LanguageName
        {
            get;
            set;
        }
        public string LanguageLocale
        {
            get;
            set;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
            this.LanguageName = BasicConverter.DbToStringValue(reader["LanguageName"]);
            this.LanguageLocale = BasicConverter.DbToStringValue(reader["LanguageLocale"]);

        }

        public static List<Language> GetAllLanguages(int iLanguageID)
        {
            DBContext dbContext = null;
            List<Language> objLanguagesList = new List<Language>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@SelectedLanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                string sql = "[Proc_GetAllLanguagesNew]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Language objLanguages = new Language();
                    objLanguages.OnDataBind(reader);
                    objLanguagesList.Add(objLanguages);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objLanguagesList;
        }

        public static Language GetDefaultLanguage()
        {
            DBContext dbContext = null;
            Language objLanguages = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetDefaultLanguage]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objLanguages = new Language();
                    objLanguages.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objLanguages;
        }

        public static Language GetLanguageByID(int languageid)
        {
            DBContext dbContext = null;
            Language objLanguages = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(languageid);
                
                string sql = "[Proc_GetLanguageByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;



                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objLanguages = new Language();
                    objLanguages.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objLanguages;
        }

        public static Language GetLanguageByLocale(string strLocale)
        {
            DBContext dbContext = null;
            Language objLanguages = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageLocale", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strLocale);


                string sql = "[Proc_GetLanguageByLocale]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;



                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objLanguages = new Language();
                    objLanguages.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objLanguages;
        }
    }
}
