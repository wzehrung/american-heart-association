﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class Cholesterol
    {
        /// <summary>
        /// Creates or updates Cholesterol data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="Totalcholesterol"></param>
        /// <param name="LDL"></param>
        /// <param name="HDL"></param>
        /// <param name="Triglyceride"></param>
        /// <param name="TestLocation"></param>
        /// <param name="DateMeasured"></param>
        public static void CreateOrChangeCholesterol(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, int? Totalcholesterol, int? LDL, int? HDL, int? Triglyceride, string TestLocation, DateTime DateMeasured)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);


                if (Totalcholesterol.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Totalcholesterol", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(Totalcholesterol.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Totalcholesterol", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }


                if (LDL.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@LDL", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(LDL.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@LDL", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }

                if (HDL.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HDL", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(HDL.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HDL", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }

                //newly added field
                if (Triglyceride.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Triglyceride", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(Triglyceride.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Triglyceride", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }


                if (!string.IsNullOrEmpty(TestLocation))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@TestLocation", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(TestLocation);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@TestLocation", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateMeasured", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(DateMeasured);


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeCholesterol";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }

            return;
        }

        /// <summary>
        /// Deletes Cholesterol data
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropCholesterol(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropCholesterol";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }
    }
}
