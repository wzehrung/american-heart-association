﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    [Serializable]
    public class AdminMenu
    {
        public int MenuID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int CategoryID
        {
            get;
            set;
        }

        public int RoleID
        {
            get;
            set;
        }
        public bool Enabled
        {
            get;
            set;
        }

        public static List<AdminMenu> GetAdminMenuByID(int intUserID)
        {
            DBContext dbContext = null;
            List<AdminMenu> objAdminMenuList = new List<AdminMenu>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAdminMenuByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intUserID);


                reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    AdminMenu objAdminMenu = new AdminMenu();
                    objAdminMenu.OnDataBind(reader);
                    objAdminMenuList.Add(objAdminMenu);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminMenuList;
        }

        public static List<AdminMenu> GetAllAdminMenuByCategoryID(int CategoryID)
        {
            DBContext dbContext = null;
            List<AdminMenu> objAdminMenuList = new List<AdminMenu>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAllAdminMenuByCategoryID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CategoryID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(CategoryID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminMenu objAdminMenu = new AdminMenu();
                    objAdminMenu.OnDataBind(reader);
                    objAdminMenuList.Add(objAdminMenu);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminMenuList;
        }

        public static List<AdminMenu> GetAllAdminMenuByCategoryIDByRoleID(int CategoryID, int RoleID)
        {
            DBContext dbContext = null;
            List<AdminMenu> objAdminMenuList = new List<AdminMenu>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAllAdminMenuByCategoryIDByRoleID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CategoryID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(CategoryID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RoleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(RoleID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminMenu objAdminMenu = new AdminMenu();
                    objAdminMenu.OnDataBind(reader);
                    objAdminMenuList.Add(objAdminMenu);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminMenuList;
        }

        public static List<AdminMenu> GetAllAdminMenu()
        {
            DBContext dbContext = null;
            List<AdminMenu> objAdminMenuList = new List<AdminMenu>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllAdminMenu]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminMenu objAdminMenu = new AdminMenu();
                    objAdminMenu.OnDataBind(reader);
                    objAdminMenuList.Add(objAdminMenu);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminMenuList;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.MenuID = BasicConverter.DbToIntValue(reader["MenuID"]);
            this.Name = BasicConverter.DbToStringValue(reader["Name"]);
            this.Description = BasicConverter.DbToStringValue(reader["Description"]);
            this.CategoryID = BasicConverter.DbToIntValue(reader["CategoryID"]);
            this.Enabled = BasicConverter.DbToBoolValue(reader["Enabled"]);
        }

        public static void SetAdminMenuByRoleID(List<AdminMenu> lstAdmMenus)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                foreach (AdminMenu admMenu in lstAdmMenus)
                {

                    dbContext = DBContext.GetDBContext();
                    dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = null;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@RoleID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(admMenu.RoleID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@MenuID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(admMenu.MenuID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@Enabled", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(admMenu.Enabled);

                    dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateAdminMenu]";
                    dbContext.ContextSqlCommand.ExecuteNonQuery();
                    dbContext.ReleaseDBContext(true);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
    }
}
