﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GRCBase;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;

namespace AHAHelpContent
{
    public enum TrackerDataType
    {
        [Description("Weight")]
        Weight,
        [Description("Blood Glucose")]
        BloodGlucose,
        [Description("Blood Pressure")]
        BloodPressure,
        [Description("Cholesterol")]
        Cholesterol,
        [Description("Exercise")]
        Exercise,
        [Description("Medication")]
        Medication
    }

    public class Alert
    {
        #region Public Properties
        public int AlertID
        {
            get;
            set;
        }

        public string AlertRuleXml
        {
            get;
            set;
        }

        public string AlertXml
        {
            get;
            set;
        }

        public int PatientID
        {
            get;
            set;
        }

        public bool IsDismissed
        {
            get;
            set;
        }

        public DateTime ItemEffectiveDate
        {
            get;
            set;
        }

        public TrackerDataType Type
        {
            get;
            set;
        }

        public string Notes
        {
            get;
            set;
        }
        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.AlertID = BasicConverter.DbToIntValue(reader["AlertID"]);
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
            this.IsDismissed = BasicConverter.DbToBoolValue(reader["IsDismissed"]);
            this.AlertRuleXml = BasicConverter.DbToStringValue(reader["AlertData"]);
            this.AlertXml = BasicConverter.DbToStringValue(reader["AlertXml"]);
            this.ItemEffectiveDate = BasicConverter.DbToDateValue(reader["ItemEffectiveDate"]);
            this.Notes = BasicConverter.DbToStringValue(reader["Notes"]);

            string strType = BasicConverter.DbToStringValue(reader["Type"]);
            Type = ((TrackerDataType)Enum.Parse(typeof(TrackerDataType), strType, false));
        }
        #endregion


        #region Public Methods
        public static Alert CreateAlert(int iAlertRuleID,
            int iPatientID,
            Guid gHealthRecordItemGUID,
            string strAlertData,
            DateTime dtItemEffectiveDate)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iAlertID;
            Alert objAlert = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertRuleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertRuleID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertXml", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strAlertData);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ItemEffectiveDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtItemEffectiveDate);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@AlertID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateAlert]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iAlertID = Convert.ToInt32(param_output.Value);
                objAlert = new Alert
                {
                    AlertID = iAlertID,
                    PatientID = iPatientID,
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objAlert;
        }

        public static void DismissAlert(int iAlertID, string strNotes)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Notes", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strNotes);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DismissAlert]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void UpdateAlertNotes(int iAlertID, string strNotes)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Notes", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strNotes);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateAlertNotes]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteAlertByHealthRecordItemGUID(Guid gHealthRecordItemGUID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordItemGUID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteAlertByHealthRecordItemGUID]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteAlert(int iAlertID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteAlert]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DismissAllPatientAlerts(Guid gHealthRecordGUID, int iProviderID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientHealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DismissAllPatientAlerts]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DismissAllAlertsForProvider(int iProviderID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DismissAllAlertsForProvider]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DismissAlertsOlderThanNWeeks()
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                dbContext.ContextSqlCommand.CommandText = "[Proc_DismissAlertsOlderThanNWeeks]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static Alert FindByAlertID(int iAlertID)
        {
            DBContext dbContext = null;
            Alert objAlert = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAlert = new Alert();
                    objAlert.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlert;
        }

        public static List<Alert> FindByProviderID(int iProviderID)
        {
            DBContext dbContext = null;
            List<Alert> objAlertList = new List<Alert>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertByProviderID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Alert objAlert = new Alert();
                    objAlert.OnDataBind(reader);
                    objAlertList.Add(objAlert);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertList;
        }

        public static int GetAlertCountByProviderIDAndDate(int iProviderID, DateTime dt)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;  
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetAlertCountByProviderIDAndDate]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RequiredDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dt);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }

        public static List<Alert> FindByProviderIDAndDate(int iProviderID, DateTime dt)
        {
            DBContext dbContext = null;
            List<Alert> objAlertList = new List<Alert>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertByProviderID_RequiredDate]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RequiredDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dt);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Alert objAlert = new Alert();
                    objAlert.OnDataBind(reader);
                    objAlertList.Add(objAlert);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertList;
        }

        public static List<Alert> FindByGroupID(int iGroupID)
        {
            DBContext dbContext = null;
            List<Alert> objAlertList = new List<Alert>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertByGroupID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Alert objAlert = new Alert();
                    objAlert.OnDataBind(reader);
                    objAlertList.Add(objAlert);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertList;
        }

        public static List<Alert> FindByPatientID(int iPatientID, int iProviderID)
        {
            DBContext dbContext = null;
            List<Alert> objAlertList = new List<Alert>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertByPatientID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Alert objAlert = new Alert();
                    objAlert.OnDataBind(reader);
                    objAlertList.Add(objAlert);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertList;
        }

        public static Alert FindAlertByHealthRecordItemGUIDAndAlertRuleID(Guid gHealthRecordItemGUID, int iAlertRuleID)
        {
            DBContext dbContext = null;
            Alert objAlert = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertByHealthRecordItemGUIDAndAlertRuleID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertRuleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertRuleID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAlert = new Alert();
                    objAlert.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlert;
        }

        public static List<Alert> FindByPatientHealthRecordGUID(Guid gHealthRecordGUID, int iProviderID)
        {
            DBContext dbContext = null;
            List<Alert> objAlertList = new List<Alert>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertByPatientHealthRecordGUID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientHealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Alert objAlert = new Alert();
                    objAlert.OnDataBind(reader);
                    objAlertList.Add(objAlert);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertList;
        }

        public static int GetNewAlertCountForProvider(int iProviderID)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetNewAlertCountForProvider]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }
        #endregion
    }
}
