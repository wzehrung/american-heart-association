﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;

namespace AHAHelpContent
{
    public class PatientReminder
    {
        #region public properties
        public int ReminderID
        {
            get;
            set;
        }

        public int? PatientID
        {
            get;
            set;
        }

        public string MobileNumber
        {
            get;
            set;
        }

        public string PIN
        {
            get;
            set;
        }

        public string TimeZone
        {
            get;
            set;
        }

        public Boolean Terms
        {
            get;
            set;
        }

        public Boolean PinValidated
        {
            get;
            set;
        }

        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.ReminderID = BasicConverter.DbToIntValue(reader["ReminderID"]);
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
            this.MobileNumber = BasicConverter.DbToStringValue(reader["MobileNumber"]);
            this.PIN = BasicConverter.DbToStringValue(reader["PIN"]);
            this.PinValidated = BasicConverter.DbToBoolValue(reader["PinValidated"]);
            this.Terms = BasicConverter.DbToBoolValue(reader["Terms"]);
            this.TimeZone = BasicConverter.DbToStringValue(reader["TimeZone"]);
        }

        struct ReminderCategories
        {
            int ReminderCategoryID
            {
                get;
                set;
            }

            String Name
            {
                get;
                set;
            }

            String TriggerID
            {
                get;
                set;
            }

        }
        #endregion

        #region public methods
        public static PatientReminder GetPatientReminders(int? PatientID)
        {
            DBContext dbContext = null;
            PatientReminder objPatient = new PatientReminder();
            //objPatient = null;
            //List<PatientReminder> objPatientList = new List<PatientReminder>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "Proc_GetPatientSMSReminder";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(PatientID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    objPatient.OnDataBind(reader);
                    //objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatient;
        }

        public static PatientReminder CreateReminder(
            int? iPatientID,
            string strMobileNumber,
            string strPIN,
            string strTimeZone,
            bool bTerms,
            bool bPinValidated)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iReminderID;
            PatientReminder objPatientReminder = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.VarChar);
                param.Value = BasicConverter.NullableIntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strMobileNumber);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PIN", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPIN);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TimeZone", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strTimeZone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Terms", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bTerms);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PinValidated", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bPinValidated);

                dbContext.ContextSqlCommand.CommandText = "[Proc_InsertPatientSMSReminder]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ReminderID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iReminderID = Convert.ToInt32(param_output.Value);
                objPatientReminder = new PatientReminder
                {
                    ReminderID = iReminderID,
                    PatientID = iPatientID,
                    MobileNumber = strMobileNumber,
                    TimeZone = strTimeZone,
                    PIN = strPIN,
                    Terms = bTerms,
                    PinValidated = bPinValidated
                };

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objPatientReminder;
        }

        public static void DeletePatientReminders(int? PatientID)
        {
            DBContext dbContext = null;
            //PatientReminder objPatient = new PatientReminder();
            //objPatient = null;
            //List<PatientReminder> objPatientList = new List<PatientReminder>();
            //SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "Proc_DeletePatientSMSReminder";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(PatientID);


                //reader = cmd.ExecuteReader();
                cmd.ExecuteNonQuery();

                //while (reader.Read())
                //{

                //    objPatient.OnDataBind(reader);
                //    //objPatientList.Add(objPatient);
                //}
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                //DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            //return objPatient;
        }

        public static PatientReminder UpdateReminder(PatientReminder objReminder)
        {
            DBContext dbContext = null;
            bool bException = false;
            PatientReminder objPatientReminder = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;
                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(objReminder.ReminderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(objReminder.PatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(objReminder.MobileNumber);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PIN", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(objReminder.PIN);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TimeZone", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(objReminder.TimeZone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Terms", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(objReminder.Terms);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PinValidated", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(objReminder.PinValidated);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdatePatientSMSReminder]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                objPatientReminder = GetPatientReminders(objReminder.PatientID);

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objPatientReminder;
        }

        public static void UpdateReminderPIN(
                    int iReminderID,
                    int? iPatientID,
                    string strPIN)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iReminderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PIN", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPIN);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdatePatientSMSReminderPIN]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();


            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

        }

        public static void UpdateReminderPINValidated(
                            int iReminderID,
                            int? iPatientID,
                            bool bPINValidated)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iReminderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PINValidated", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bPINValidated);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdatePatientSMSReminderPINValidated]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();


            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

        }

        public static void CreatePINValidationLog(
             int iReminderID
            , string sPIN
            , string sTimeZone
            , bool bValid)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;
                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iReminderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PIN", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sPIN);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TimeZone", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sTimeZone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Valid", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bValid);

                dbContext.ContextSqlCommand.CommandText = "[Proc_InsertPatientPINValidationLog]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

        }

        public static string CreatePIN()
        {
            int maxSize = 4;
            char[] chars = new char[62];
            chars = "1234567890".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[maxSize];


            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();

        }

        public static string FormatPhoneNumberfor3Ci(string sPhoneNumber)
        {
            String sMobileNumber = sPhoneNumber.Replace("-", "");
            sMobileNumber = sMobileNumber.Length == 10 ? "1" + sMobileNumber : sMobileNumber;

            return sMobileNumber;

        }

        public static int GetReminderCategoryIDByName(string sCategory)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iReminderCateogryID;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Name", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sCategory);

                dbContext.ContextSqlCommand.CommandText = "[Proc_GetPatientReminderCategoryByName]";

                iReminderCateogryID = int.Parse(dbContext.ContextSqlCommand.ExecuteScalar().ToString());

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return iReminderCateogryID;
        }

        #endregion
    }

    //Class - ReminderSchedule
    public class PatientReminderSchedule
    {
        #region public properties
        public int ReminderScheduleID
        {
            get;
            set;
        }

        public int ReminderID
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public Boolean Monday
        {
            get;
            set;
        }

        public Boolean Tuesday
        {
            get;
            set;
        }

        public Boolean Wednesday
        {
            get;
            set;
        }

        public Boolean Thursday
        {
            get;
            set;
        }

        public Boolean Friday
        {
            get;
            set;
        }

        public Boolean Saturday
        {
            get;
            set;
        }

        public Boolean Sunday
        {
            get;
            set;
        }

        public String Time
        {
            get;
            set;
        }


        public String DisplayTime
        {
            get;
            set;
        }

        public int ReminderCategoryID
        {
            get;
            set;
        }

        public String ReminderCategory
        {
            get;
            set;
        }
        public string TimeZone
        {
            get;
            set;
        }
        public string ReminderGroupID
        {
            get;
            set;
        }
        #endregion

        #region public methods
        public static List<PatientReminderSchedule> GetPatientReminderSchedules(int ReminderID)
        {
            DBContext dbContext = null;

            List<PatientReminderSchedule> objReminderSchedules = new List<PatientReminderSchedule>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "Proc_GetPatientSMSReminderSchedules";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(ReminderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientReminderSchedule objSchedule = new PatientReminderSchedule();
                    objSchedule.OnDataBind(reader);
                    objReminderSchedules.Add(objSchedule);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objReminderSchedules;

        }

        public static List<PatientReminderSchedule> GetPatientReminderSchedulesByCategory(int iReminderID, int iCategoryID)
        {
            DBContext dbContext = null;

            List<PatientReminderSchedule> objReminderSchedules = new List<PatientReminderSchedule>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "Proc_GetPatientSMSReminderSchedulesbyCategory";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iReminderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CategoryID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCategoryID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientReminderSchedule objSchedule = new PatientReminderSchedule();
                    objSchedule.OnDataBind(reader);
                    objReminderSchedules.Add(objSchedule);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objReminderSchedules;

        }

        public static List<PatientReminderSchedule> CreateReminderSchedule(
            int iReminderID,
            string sMessage,
            Boolean bMonday,
            Boolean bTuesday,
            Boolean bWednesday,
            Boolean bThursday,
            Boolean bFriday,
            Boolean bSaturday,
            Boolean bSunday,
            String sTime,
            string sDisplayTime,
            int iReminderCategoryID)
        {
            DBContext dbContext = null;
            bool bException = false;
            List<PatientReminderSchedule> objPatientReminderSchedules = null;
            int iReminderScheduleID;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.VarChar);
                param.Value = BasicConverter.NullableIntToDbValue(iReminderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Message", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sMessage);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Monday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bMonday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Tuesday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bTuesday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Wednesday", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bWednesday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Thursday", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bThursday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Friday", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bFriday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Saturday", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bSaturday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Sunday", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bSunday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Time", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sTime);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DisplayTime", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sDisplayTime);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderCategoryID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iReminderCategoryID);


                dbContext.ContextSqlCommand.CommandText = "[Proc_InsertPatientSMSReminderSchedule]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ReminderScheduleID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iReminderScheduleID = Convert.ToInt32(param_output.Value);

                objPatientReminderSchedules = GetPatientReminderSchedulesByCategory(iReminderID, iReminderCategoryID);

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }

            }

            return objPatientReminderSchedules;
        }

        public static List<PatientReminderSchedule> UpdateReminderSchedule(
            int iReminderScheduleID,
            int iReminderID,
            string sMessage,
            Boolean bMonday,
            Boolean bTuesday,
            Boolean bWednesday,
            Boolean bThursday,
            Boolean bFriday,
            Boolean bSaturday,
            Boolean bSunday,
            string sTime,
            string sDisplayTime,
            int iReminderCategoryID)
        {
            DBContext dbContext = null;
            bool bException = false;
            List<PatientReminderSchedule> objPatientReminderSchedules = null;
            //int iReminderScheduleID;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderScheduleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iReminderScheduleID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iReminderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Message", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sMessage);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Monday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bMonday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Tuesday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bTuesday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Wednesday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bWednesday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Thursday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bThursday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Friday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bFriday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Saturday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bSaturday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Sunday", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bSunday);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Time", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sTime);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DisplayTime", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sDisplayTime);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderCategoryID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iReminderCategoryID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Update_SMS_ReminderSchedule]";

                //SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ReminderScheduleID", SqlDbType.Int));
                //param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                //iReminderScheduleID = Convert.ToInt32(param_output.Value);

                objPatientReminderSchedules = GetPatientReminderSchedulesByCategory(iReminderID, iReminderCategoryID);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }

            }

            return objPatientReminderSchedules;
        }

        public static void DeleteReminderSchedule(int iReminderScheduleID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderScheduleID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iReminderScheduleID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Delete_SMS_Schedule]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteReminderScheduleDay(
            int iReminderScheduleID,
            string sDay)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderScheduleID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iReminderScheduleID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Day", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sDay);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeletePatientSMSReminderScheduleDay]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static string ConvertTimetoUTC(string sTime, string sTimeZone)
        {
            string sUTCTime;
            string sZoneID = string.Empty;
            try
            {

                //List<System.TimeZoneInfo> sZone = TimeZoneInfo.GetSystemTimeZones();

                switch (sTimeZone.ToLower())
                {
                    case "eastern":
                        sZoneID = "Eastern Standard Time";
                        break;
                    case "central":
                        sZoneID = "Central Standard Time";
                        break;
                    case "mountain":
                        sZoneID = "Mountain Standard Time";
                        break;
                    case "pacific":
                        sZoneID = "Pacific Standard Time";
                        break;
                }

                char[] chArr1 = { ':' };
                int iHour = int.Parse(sTime.Split(chArr1[0])[0].ToString());
                DateTime dtTime = new DateTime(2012, 01, 02, iHour == 24 ? 00 : iHour, 00, 00);
                //DateTime.Parse(sTime);
                TimeZoneInfo tziZone = TimeZoneInfo.FindSystemTimeZoneById(sZoneID);
                DateTime dtUTCTime = TimeZoneInfo.ConvertTimeToUtc(dtTime, tziZone);

                char[] chArr2 = { '0' };
                sUTCTime = dtUTCTime.TimeOfDay.ToString().Remove(5, 3).TrimStart(chArr2);
                //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);
                //Console.WriteLine("The date and time are {0} UTC.",
                //                  TimeZoneInfo.ConvertTimeToUtc(easternTime, easternZone));
            }
            catch (TimeZoneNotFoundException)
            {
                throw;
                //("Unable to find the {0} zone in the registry.", sTimeZone);
            }
            catch (InvalidTimeZoneException)
            {
                throw;
                //Console.WriteLine("Registry data on the {0} zone has been corrupted.", sTimeZone);
            }

            return sUTCTime;
        }

        public static string ConvertUTCtoLocalTime(string sUTCTime, string sTimeZone)
        {
            string sLocalTime;
            string sZoneID = string.Empty;
            try
            {
                //if (sUTCTime == "24:00")
                //{
                //    sLocalTime = "12:00 AM";
                //}
                //else
                //{
                //List<System.TimeZoneInfo> sZone = TimeZoneInfo.GetSystemTimeZones();

                switch (sTimeZone.ToLower())
                {
                    case "eastern":
                        sZoneID = "Eastern Standard Time";
                        break;
                    case "central":
                        sZoneID = "Central Standard Time";
                        break;
                    case "mountain":
                        sZoneID = "Mountain Standard Time";
                        break;
                    case "pacific":
                        sZoneID = "Pacific Standard Time";
                        break;
                }

                char[] chArr1 = { ':' };
                int iHour = int.Parse(sUTCTime.Split(chArr1[0])[0].ToString());

                //inline check for midnight, if it is, then replace it with 00 hours
                DateTime dtTime = new DateTime(2012, 01, 02, iHour, 00, 00);

                //DateTime.Parse(sTime);
                TimeZoneInfo tziZone = TimeZoneInfo.FindSystemTimeZoneById(sZoneID);
                DateTime dtLocalTime = TimeZoneInfo.ConvertTimeFromUtc(dtTime, tziZone);

                //if  dtLocalTime
                //char[] chArr2 = { '0' };
                sLocalTime = dtLocalTime.ToShortTimeString();
                //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);
                //Console.WriteLine("The date and time are {0} UTC.",
                //                  TimeZoneInfo.ConvertTimeToUtc(easternTime, easternZone));
                //}
            }
            catch (TimeZoneNotFoundException)
            {
                throw;
                //("Unable to find the {0} zone in the registry.", sTimeZone);
            }
            catch (InvalidTimeZoneException)
            {
                throw;
                //Console.WriteLine("Registry data on the {0} zone has been corrupted.", sTimeZone);
            }

            return sLocalTime;
        }

        public static List<ScheduleGridDisplay> GetScheduleGridDisplay(int iReminderID, int iCategory)
        {

            List<ScheduleGridDisplay> objScheduleGridDisplay = new List<ScheduleGridDisplay>();

            List<PatientReminderSchedule> objSchedules = new List<PatientReminderSchedule>();
            try
            {

                //Get all schedules or category schedules
                if (iCategory == 0)
                {
                    objSchedules = GetPatientReminderSchedules(iReminderID);
                }
                else
                {
                    objSchedules = GetPatientReminderSchedulesByCategory(iReminderID, iCategory);
                }

                //if there are any schedules
                if (objSchedules.Count > 0)
                {
                    foreach (PatientReminderSchedule objSchedule in objSchedules)
                    {
                        //check each day and add a row to display object
                        if (objSchedule.Monday)
                        {
                            ScheduleGridDisplay objDisplay = BindDisplayGridData(DayOfWeek.Monday, objSchedule);
                            objScheduleGridDisplay.Add(objDisplay);
                        }
                        if (objSchedule.Tuesday)
                        {
                            ScheduleGridDisplay objDisplay = BindDisplayGridData(DayOfWeek.Tuesday, objSchedule);
                            objScheduleGridDisplay.Add(objDisplay);
                        }
                        if (objSchedule.Wednesday)
                        {
                            ScheduleGridDisplay objDisplay = BindDisplayGridData(DayOfWeek.Wednesday, objSchedule);
                            objScheduleGridDisplay.Add(objDisplay);
                        }
                        if (objSchedule.Thursday)
                        {
                            ScheduleGridDisplay objDisplay = BindDisplayGridData(DayOfWeek.Thursday, objSchedule);
                            objScheduleGridDisplay.Add(objDisplay);
                        }
                        if (objSchedule.Friday)
                        {
                            ScheduleGridDisplay objDisplay = BindDisplayGridData(DayOfWeek.Friday, objSchedule);
                            objScheduleGridDisplay.Add(objDisplay);
                        }
                        if (objSchedule.Saturday)
                        {
                            ScheduleGridDisplay objDisplay = BindDisplayGridData(DayOfWeek.Saturday, objSchedule);
                            objScheduleGridDisplay.Add(objDisplay);
                        }
                        if (objSchedule.Sunday)
                        {
                            ScheduleGridDisplay objDisplay = BindDisplayGridData(DayOfWeek.Sunday, objSchedule);
                            objScheduleGridDisplay.Add(objDisplay);
                        }


                    }
                    //objScheduleGridDisplay.Sort(delegate(ScheduleGridDisplay Sch1, ScheduleGridDisplay Sch2) { return Sch1.Day.CompareTo(Sch2.Day); });
                    objScheduleGridDisplay.Sort(delegate(ScheduleGridDisplay Sch1, ScheduleGridDisplay Sch2) { var ret = Sch1.Day.CompareTo(Sch2.Day); if (ret == 0) ret = Sch1.Time.CompareTo(Sch2.Time); return ret; });

                    //((x, y) => {     var ret = x.PlayOrder.CompareTo(y.PlayOrder);     if (ret == 0) ret = x.Name.CompareTo(y.Name);     return ret; }); 
                    //((x, y) => x.CheckedIn == y.CheckedIn ? string.Compare(x.LastName, y.LastName) : (x.CheckedIn ? -1 : 1)); 

                }

            }
            catch
            {
                throw;
            }
            return objScheduleGridDisplay;
        }

        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.ReminderScheduleID = BasicConverter.DbToIntValue(reader["ReminderScheduleID"]);
            this.ReminderID = BasicConverter.DbToIntValue(reader["ReminderID"]);
            this.Message = BasicConverter.DbToStringValue(reader["Message"]);
            this.Monday = BasicConverter.DbToBoolValue(reader["Monday"]);
            this.Tuesday = BasicConverter.DbToBoolValue(reader["Tuesday"]);
            this.Wednesday = BasicConverter.DbToBoolValue(reader["Wednesday"]);
            this.Thursday = BasicConverter.DbToBoolValue(reader["Thursday"]);
            this.Friday = BasicConverter.DbToBoolValue(reader["Friday"]);
            this.Saturday = BasicConverter.DbToBoolValue(reader["Saturday"]);
            this.Sunday = BasicConverter.DbToBoolValue(reader["Sunday"]);
            this.Time = BasicConverter.DbToStringValue(reader["Time"]);
            this.DisplayTime = BasicConverter.DbToStringValue(reader["DisplayTime"]);
            this.ReminderCategoryID = BasicConverter.DbToIntValue(reader["ReminderCategoryID"]);
            this.ReminderCategory = BasicConverter.DbToStringValue(reader["ReminderCategory"]);
            this.TimeZone = BasicConverter.DbToStringValue(reader["TimeZone"]);
            this.ReminderGroupID = BasicConverter.DbToStringValue(reader["GroupID"]); 
        }

        private static ScheduleGridDisplay BindDisplayGridData(DayOfWeek sDay, PatientReminderSchedule objSchedule)
        {
            ScheduleGridDisplay objDisplay = new ScheduleGridDisplay();
            char[] delimitChars = { ':' };


            objDisplay.ReminderID = objSchedule.ReminderID;
            objDisplay.ReminderScheduleID = objSchedule.ReminderScheduleID;
            objDisplay.DisplayTime = objSchedule.DisplayTime;
            objDisplay.Time = objSchedule.Time == "24:00" ? 0 : int.Parse(objSchedule.Time.Split(delimitChars)[0]);
            objDisplay.Message = objSchedule.Message;
            objDisplay.Category = objSchedule.ReminderCategory;
            objDisplay.CategoryID = objSchedule.ReminderCategoryID;
            objDisplay.ReminderGroupID = objSchedule.ReminderGroupID;

            objDisplay.LastSent = "Scheduled";
            objDisplay.Day = sDay;

            return objDisplay;

        }
        #endregion
    }

    //Class for list objects for grid display
    public class ScheduleGridDisplay
    {
        #region public properties
        public int ReminderID { get; set; }
        public int ReminderScheduleID { get; set; }
        public DayOfWeek Day { get; set; }
        public string DisplayTime { get; set; }
        public int Time { get; set; }
        public string Message { get; set; }
        public string Category { get; set; }
        public int CategoryID { get; set; }
        public string LastSent { get; set; }
        public string ReminderGroupID { get; set; }

        #endregion

    }

    //Class for list of Reminder Categories
    public class ReminderCategories
    {
        #region public properties
        public int ReminderCategoriesID { get; set; }
        public string ReminderCategory { get; set; }
        public string TriggerID { get; set; }
        public string GroupID { get; set; }

        public int ReminderCategoryID { get; set; }
        public int ReminderCategoryIDGroupID { get; set; }
        public string ReminderCategoryName { get; set; }

        #endregion

        #region public Methods

        private void OnDataBind(SqlDataReader reader)
        {
            this.ReminderCategoriesID = BasicConverter.DbToIntValue(reader["ReminderCategoryID"]);
            this.ReminderCategory = BasicConverter.DbToStringValue(reader["ReminderCategory"]);
            this.TriggerID = BasicConverter.DbToStringValue(reader["ReminderTriggerID"]);
            this.GroupID = BasicConverter.DbToStringValue(reader["ReminderGroupID"]);
        }

        private void OnDataBind_Categories(SqlDataReader reader)
        {
            this.ReminderCategoryID = BasicConverter.DbToIntValue(reader["ReminderCategoryID"]);
            this.ReminderCategoryIDGroupID = BasicConverter.DbToIntValue(reader["ReminderCategoryIDGroupID"]);
            this.ReminderCategoryName = BasicConverter.DbToStringValue(reader["ReminderCategoryName"]);
        }

        #endregion

        #region public Methods

        public static List<ReminderCategories> GetReminderCategories()
        {
            DBContext dbContext = null;

            List<ReminderCategories> objPatientCategories = new List<ReminderCategories>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "SELECT * FROM v_Get_Reminders_Categories";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ReminderCategories objPatientCategory = new ReminderCategories();
                    objPatientCategory.OnDataBind_Categories(reader);
                    objPatientCategories.Add(objPatientCategory);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientCategories;
        }

        public static List<ReminderCategories> GetPatientSelectedReminderCategories(int ReminderID)
        {
            DBContext dbContext = null;

            List<ReminderCategories> objPatientCategories = new List<ReminderCategories>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "Proc_GetPatientSelectedReminderCategories";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ReminderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(ReminderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ReminderCategories objPatientCategory = new ReminderCategories();
                    objPatientCategory.OnDataBind(reader);
                    objPatientCategories.Add(objPatientCategory);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientCategories;
        }
        #endregion
    }

    public class IVRReminders
    {
        #region IVR Stuff

        public int? PatientID
        {
            get;
            set;
        }

        public bool IVREnabled
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public string PIN
        {
            get;
            set;
        }

        public string Time
        {
            get;
            set;
        }

        public string Day
        {
            get;
            set;
        }

        public string TimeZone
        {
            get;
            set;
        }

        public DateTime CreatedDateTime
        {
            get;
            set;
        }

        public DateTime UpdatedDateTime
        {
            get;
            set;
        }
        #endregion

        private void OnDataBindIVR(SqlDataReader reader)
        {
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
            this.IVREnabled = BasicConverter.DbToBoolValue(reader["Enabled"]);
            this.PhoneNumber = BasicConverter.DbToStringValue(reader["PhoneNumber"]);
            this.PIN = BasicConverter.DbToStringValue(reader["PIN"]);
            this.Time = BasicConverter.DbToStringValue(reader["Time"]);
            this.Day = BasicConverter.DbToStringValue(reader["Day"]);
            this.TimeZone = BasicConverter.DbToStringValue(reader["TimeZone"]);
            this.CreatedDateTime = BasicConverter.DbToDateValue(reader["CreatedDateTime"]);
            this.UpdatedDateTime = BasicConverter.DbToDateValue(reader["UpdatedDateTime"]);
        }

        public static void CreateOrUpdateIVRSchedule(int iPatientID, string sPIN, string sTime, string sDay, string sTimeZone, bool bEnabled, string phoneNumber)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PIN", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sPIN);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Time", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sTime);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Day", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sDay);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TimeZone", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sTimeZone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Enabled", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bEnabled);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(phoneNumber);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrUpdateIVRSchedule]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static IVRReminders GetIVRScheduleByPatient(int iPatientID)
        {
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            IVRReminders objPatient = new IVRReminders();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetIVRScheduleByPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objPatient.OnDataBindIVR(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatient;
        }

        public static void DeleteIVRSchedule(int iPatientID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_IVR_Delete_Schedule]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
    
    }
}