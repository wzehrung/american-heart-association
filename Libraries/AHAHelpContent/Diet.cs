﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{

    public class Diet
    {
        /// <summary>
        /// Creates or updates Diet data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="CalorieAmount"></param>
        /// <param name="MealType"></param>
        /// <param name="FoodItems"></param>
        /// <param name="DateOfEntry"></param>
        public static void CreateOrChangeDiet(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, double CalorieAmount, string MealType, string FoodItems, DateTime DateOfEntry)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CalorieAmount", SqlDbType.Float);
                param.Value = BasicConverter.DoubleToDbValue(CalorieAmount);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Meal", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(MealType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@FoodItems", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(FoodItems);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateOfEntry", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(DateOfEntry);


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeDiet";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// Deletes Diet data
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropDiet(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropDiet";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

    }
}
