﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent.AdminReports
{
    public class ReportHelper
    {
        public static int GetTotalPatientsInPeriod(DateTime dtStart, DateTime dtEnd)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetTotalPatientsInPeriod]";

                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }

        public static DataSet GetDetails(DateTime dtStart, DateTime dtEnd)
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                //SqlParameter param = null;
                string sql = "[Proc_GetAllThingTips]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                //param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                //param.Value = BasicConverter.DateToDbValue(dtStart);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                //param.Value = BasicConverter.DateToDbValue(dtEnd);


                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        #region Backend Report Specific

        public static DataSet GetTrackingMonthlyPercent(DateTime dtStart, DateTime dtEnd)
        {
            return _GetReport("[Proc_Report_Tracking]",dtStart,dtEnd);
        }

        public static DataSet GetNewUsersVisit(DateTime dtStart, DateTime dtEnd)
        {
            return _GetReport("[PROC_Report_newUsers]", dtStart, dtEnd);
        }

        public static DataSet GetAllUsersVisit(DateTime dtStart, DateTime dtEnd)
        {
            return _GetReport("[PROC_Report_AllUsers]", dtStart, dtEnd);
        }

        public static DataSet GetGenderCumulativeReport(DateTime dtStart, DateTime dtEnd)
        {
            return _GetReport("[Proc_Report_Gender]", dtStart, dtEnd);
        }

        public static DataSet _GetReport(string sql, DateTime dtStart, DateTime dtEnd)
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);


                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        #endregion
    }
}
