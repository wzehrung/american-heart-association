﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{


    public class WeightGoal
    {
        /// <summary>
        /// Creates / updates WeightGoal data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="StartWeight"></param>
        /// <param name="StartDate"></param>
        /// <param name="TargetWeight"></param>
        /// <param name="TargetDate"></param>
        public static void CreateOrChangeWeightGoal(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, double StartWeight, DateTime StartDate, double TargetWeight, DateTime TargetDate)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartWeight", SqlDbType.Decimal);
                param.Value = BasicConverter.DoubleToDbValue(StartWeight);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(StartDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TargetWeight", SqlDbType.Decimal);
                param.Value = BasicConverter.DoubleToDbValue(TargetWeight);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TargetDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(TargetDate);


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeWeightGoal";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }

            return;
        }

        /// <summary>
        /// Deletes WeightGoal Data
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropWeightGoal(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropWeightGoal";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }
    }
}
