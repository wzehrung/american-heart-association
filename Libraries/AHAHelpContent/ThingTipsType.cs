﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class ThingTipsType
    {
        public int ThingTipsTypeID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets RuleName
        /// </summary>
        public string ThingTipType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DisplayName
        /// </summary>
        public string DisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Binds the data from the database to the corresponding variables
        /// </summary>
        /// <param name="objSource"></param>
        /// <returns></returns>

        private void OnDataBind(SqlDataReader reader)
        {
            this.ThingTipsTypeID = BasicConverter.DbToIntValue(reader["ThingTipsTypeId"]);
            this.ThingTipType = BasicConverter.DbToStringValue(reader["ThingTipsType"]);
            this.DisplayName = BasicConverter.DbToStringValue(reader["DisplayName"]);
            return;
        }

        /// <summary>
        /// Gets all ThingTipsType data
        /// </summary>
        /// <returns>List<ThingTipsType></returns>
        public static List<ThingTipsType> GetAllThingTipsTypes(int intLanguagID)
        {
            DBContext dbContext = null;
            List<ThingTipsType> objThingTipsTypesList = new List<ThingTipsType>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAllThingTipsTypes]";
                SqlParameter param = null;
                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;
                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguagID);

                reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    ThingTipsType objThingTipsType = new ThingTipsType();
                    objThingTipsType.OnDataBind(reader);
                    objThingTipsTypesList.Add(objThingTipsType);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objThingTipsTypesList;
        }
    }
}
