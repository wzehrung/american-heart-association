﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class ProviderNote
    {
        public int ProviderNoteID
        {
            get;
            set;
        }

        public int ProviderID
        {
            get;
            set;
        }

        public int PatientID    // actually the Patient's UserHealthRecordID
        {
            get;
            set;
        }

        public DateTime DateCreated
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Note
        {
            get;
            set;
        }

#region Public Methods
        public static ProviderNote CreateProviderNote( int iProviderID, int iPatientID, DateTime dtCreationDate, string sTitle, string sNote )
        {
            ProviderNote result = null;
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CreationDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtCreationDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Note", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sNote);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ProviderNoteID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateProviderNote]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                int iProviderNoteID = Convert.ToInt32(param_output.Value);
                result = new ProviderNote { ProviderNoteID = iProviderNoteID, ProviderID = iProviderID, PatientID = iPatientID, DateCreated = dtCreationDate, Title = sTitle, Note=sNote };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return result;
        }

        public static void DeleteProviderNote(int iProviderNoteID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderNoteID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderNoteID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteProviderNote]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

        }

        public static void UpdateProviderNote(int iProviderNoteID, string sTitle, string sNote)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderNoteID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderNoteID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Note", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sNote);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateProviderNote]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

        }

        public static ProviderNote FindByProviderNoteID(int iProviderNoteID)
        {
            DBContext dbContext = null;
            ProviderNote objProviderNote = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_FindProviderNoteByID]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderNoteID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderNoteID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderNote = new ProviderNote();
                    objProviderNote.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderNote;
        }

        public static List<ProviderNote> GetAllNotesForProviderAndPatient(int iProviderID, int iPatientID)
        {
            List<ProviderNote> result = null;
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                result = new List<ProviderNote>();
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_FindAllProviderNotesByProviderIDAndPatientID]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderNote objProviderNote = new ProviderNote();
                    objProviderNote.OnDataBind(reader );
                   result.Add(objProviderNote);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return result;
        }

#endregion

        private void OnDataBind(SqlDataReader reader )
        {
            this.ProviderNoteID = BasicConverter.DbToIntValue(reader["ProviderNoteID"]);
            this.ProviderID = BasicConverter.DbToIntValue(reader["ProviderID"]);
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
            this.DateCreated = BasicConverter.DbToDateValue(reader["CreationDate"]);
            this.Title = BasicConverter.DbToStringValue(reader["Title"]);
            this.Note = BasicConverter.DbToStringValue(reader["Note"]);

        }
    }
}