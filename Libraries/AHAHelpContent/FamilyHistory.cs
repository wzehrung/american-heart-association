﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{


    public class FamilyHistory
    {

        /// <summary>
        /// Creates or Updates FamilyHistory table
        /// </summary>
        /// <param name="UserHealthRecordID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="FamilyMember"></param>
        /// <param name="Disease"></param>
        /// <param name="HvCreatedDate"></param>
        /// <returns></returns>
        public static void CreateOrChangeFamilyHistory(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, string FamilyMember, string Disease, DateTime? HvCreatedDate)
        {

            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyMember", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(FamilyMember);

                if (!string.IsNullOrEmpty(Disease))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Disease", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(Disease);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Disease", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }

                if (HvCreatedDate.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HvCreatedDate", SqlDbType.DateTime);
                    param.Value = HvCreatedDate.Value;
                }

                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HvCreatedDate", SqlDbType.DateTime);
                    param.Value = DBNull.Value;
                }


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeFamilyHistory";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }

            return;
        }

        /// <summary>
        /// Delete FamilyHistory
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropFamilyHistory(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropFamilyHistory";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

    }
}
