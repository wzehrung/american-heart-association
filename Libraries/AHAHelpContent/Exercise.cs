﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class Exercise
    {
        /// <summary>
        /// Creates or updates Exercise data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="DateofSession"></param>
        /// <param name="ExerciseType"></param>
        /// <param name="Intensity"></param>
        /// <param name="Duration"></param>
        /// <param name="Comments"></param>
        public static void CreateOrChangeExercise(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, DateTime DateofSession, string ExerciseType, string Intensity, double? Duration, string Comments, int? NumberOfSteps, string ReadingSource)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateofSession", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(DateofSession);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ExerciseType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(ExerciseType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Intensity", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(Intensity);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Duration", SqlDbType.Float);
                param.Value = BasicConverter.NullableDoubleToDbValue(Duration);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(Comments);

                param = dbContext.ContextSqlCommand.Parameters.Add("@NumberOfSteps", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(NumberOfSteps);

                if (string.IsNullOrEmpty(ReadingSource))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(ReadingSource);
                }
                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeExercise";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// Deletes Exercise data
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropExercise(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropExercise";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

    }
}
