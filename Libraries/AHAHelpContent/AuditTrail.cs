﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public enum AuditTrailType
    {
        ProviderResources,
        PatientResources,
        ProviderContent,
        PatientContent,        
        ProviderGrantSupport,
        PatientGrantSupport,
        ProviderResourceGraphics,
        PatientResourceGraphics,
        ProviderTips,
        ThingTips,
        Content        
    }

    public class AuditTrail
    {
        public string Name
        {
            get;
            set;
        }

        public DateTime AuditDate
        {
            get;
            set;
        }

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.Name = BasicConverter.DbToStringValue(reader["Name"]);
            this.AuditDate = BasicConverter.DbToDateValue(reader["AuditDate"]);
        }
        #endregion

        public static List<AuditTrail> GetAuditDetails(AuditTrailType eAuditTrailType, int id,int LanguageID)
        {
            DBContext dbContext = null;
            List<AuditTrail> objList = new List<AuditTrail>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAuditDetails]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(id);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Type", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(eAuditTrailType.ToString());


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AuditTrail objAuditTrail = new AuditTrail();
                    objAuditTrail.OnDataBind(reader);
                    objList.Add(objAuditTrail);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }
    }
}
