﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class EmailScheduler
    {
        public int SchedulerID
        {
            get;
            set;
        }

        public DateTime ScheduledDate
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public bool IsDownloaded
        {
            get;
            set;
        }

        public bool IsProviderList
        {
            get;
            set;
        }

        public int? CampaignID
        {
            get;
            set;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.CampaignID = BasicConverter.DbToNullableIntValue(reader["CampaignID"]);
            this.SchedulerID = BasicConverter.DbToIntValue(reader["SchedulerID"]);
            this.ScheduledDate = BasicConverter.DbToDateValue(reader["ScheduledDate"]);
            this.Title = BasicConverter.DbToStringValue(reader["Title"]);
            this.IsDownloaded = BasicConverter.DbToBoolValue(reader["IsDownloaded"]);
            this.IsProviderList = BasicConverter.DbToBoolValue(reader["IsProviderList"]);
        }


        public static bool DoesEmailSchedulerExistForCampaignIDAndDate(
          int? iCampaignID,
          DateTime dt,
            bool bIsProviderList)
        {
            DBContext dbContext = null;
            bool bException = false;
            SqlDataReader reader = null;
            bool bRetVal = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ScheduledDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dt);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsProviderList", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsProviderList);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DoesEmailSchedulerExistForCampaignIDAndDate]";
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                reader = dbContext.ContextSqlCommand.ExecuteReader();

                if (reader.Read())
                {
                    bRetVal = true;
                }

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return bRetVal;
        }

        public static int? CreateEmailListScheduler(
            int? iCampaignID,
            DateTime dtDate,
            bool bIsProviderList
           )
        {
            DBContext dbContext = null;
            bool bException = false;
            int? iSchedulerID = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ScheduledDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsProviderList", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsProviderList);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsDownloaded", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(false);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@SchedulerID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateEmailListScheduler]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iSchedulerID = Convert.ToInt32(param_output.Value);

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return iSchedulerID;
        }

        public static void DeleteEmailListScheduler(
            int iSchedulerID
           )
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@SchedulerID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iSchedulerID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteEmailListScheduler]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static List<EmailScheduler> GetAllEmailListSchedulersForDate(DateTime? dtScheduledDate, int iLanguageID, bool bProviderOnly)
        {
            DBContext dbContext = null;
            List<EmailScheduler> objList = new List<EmailScheduler>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "Proc_GetAllEmailListSchedulersForDate";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = dbContext.ContextSqlCommand.Parameters.Add("@ScheduledDate", SqlDbType.DateTime);
                param.Value = BasicConverter.NullableDateToDbValue(dtScheduledDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderOnly", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bProviderOnly);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EmailScheduler objEmailScheduler = new EmailScheduler();
                    objEmailScheduler.OnDataBind(reader);
                    objList.Add(objEmailScheduler);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static EmailScheduler FindEmailSchedulerByID(int iSchedulerID)
        {
            DBContext dbContext = null;
            EmailScheduler objEmailScheduler = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindEmailSchedulerByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@SchedulerID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iSchedulerID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objEmailScheduler = new EmailScheduler();
                    objEmailScheduler.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objEmailScheduler;
        }

        public static void MarkEmailListSchedulerDownloaded(
            int iSchedulerID,
            bool bIsDownloaded
           )
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@SchedulerID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iSchedulerID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsDownloaded", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsDownloaded);

                dbContext.ContextSqlCommand.CommandText = "[Proc_MarkEmailListSchedulerDownloaded]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
    }
}
