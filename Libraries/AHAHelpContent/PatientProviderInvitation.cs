﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class PatientProviderInvitation
    {
        private List<PatientProviderInvitationDetails> m_InvitationDetails;
        private bool bLoaded;

        #region Public Members

        public int InvitationID
        {
            get;
            set;
        }
        public string InvitationCode
        {
            get;
            set;
        }
        public int? ProviderID
        {
            get;
            set;
        }
        public int? PatientID
        {
            get;
            set;
        }
        public DateTime DateSent
        {
            get;
            set;
        }
        public bool IsSendByProvider
        {
            get
            {
                if (this.ProviderID.HasValue)
                {
                    return true;
                }
                return false;
            }
        }
        public bool SentConfirmation
        {
            get;
            set;
        }

        public List<PatientProviderInvitationDetails> InvitationDetails
        {
            get
            {
                if (bLoaded == false)
                {
                    m_InvitationDetails = PatientProviderInvitationDetails.FindByInvitationCode(this.InvitationCode);
                    bLoaded = true;
                }

                return m_InvitationDetails;
            }
        }

        public List<PatientProviderInvitationDetails> PendingInvitationDetails
        {
            get
            {
                if (bLoaded == false)
                {
                    m_InvitationDetails = PatientProviderInvitationDetails.FindByInvitationCode(this.InvitationCode);
                    bLoaded = true;
                }

                return m_InvitationDetails.Where(ID => ID.Status!=PatientProviderInvitationDetails.InvitationStatus.Blocked && ID.Status != PatientProviderInvitationDetails.InvitationStatus.Accepted).ToList();

            }
        }

        #endregion

        #region Private Methods

        private void OnDataBind(SqlDataReader reader)
        {
            this.InvitationID = BasicConverter.DbToIntValue(reader["InvitationID"]);
            this.InvitationCode = BasicConverter.DbToStringValue(reader["InvitationCode"]);
            this.ProviderID = BasicConverter.DbToNullableIntValue(reader["SentProviderID"]);
            this.PatientID = BasicConverter.DbToNullableIntValue(reader["SentPatientID"]);
            this.DateSent = BasicConverter.DbToDateValue(reader["DateSent"]);
            this.SentConfirmation = BasicConverter.DbToBoolValue(reader["SentConfirmation"]);
            this.m_InvitationDetails = null;
        }

        private static PatientProviderInvitation _CreateInvitation(int? iProviderID, int? iPatientID, bool bSentConfirmation)
        {
            DBContext dbContext = null;
            bool bException = false;
            PatientProviderInvitation objInvitation = null;

            try
            {
                int iInvitationID;
                string strInvitationCode;

                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                SqlParameter paramInvitationCode = dbContext.ContextSqlCommand.Parameters.Add("@InvitationCode", SqlDbType.NChar, 6);
                paramInvitationCode.Direction = ParameterDirection.Output;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@sentConfirmation", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bSentConfirmation);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@InvitationID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreatePatientProviderInvitation]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iInvitationID = Convert.ToInt32(param_output.Value);
                strInvitationCode = BasicConverter.DbToStringValue(paramInvitationCode.Value);

                objInvitation = new PatientProviderInvitation
                {
                    InvitationID = iInvitationID,
                    InvitationCode = strInvitationCode,
                    PatientID = iPatientID,
                    ProviderID = iProviderID,
                    SentConfirmation = bSentConfirmation
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objInvitation;
        }

        private static PatientProviderInvitation _FindByInvitationCode(string gInvitationCode, bool bPendingOnly)
        {
            DBContext dbContext = null;
            PatientProviderInvitation objInvitation = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = string.Empty;

                if (bPendingOnly)
                    sql = "[Proc_FindPendingInvitationByInvitationCode]";
                else
                    sql = "[Proc_FindInvitationByInvitationCode]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@InvitationCode", SqlDbType.NChar);
                param.Value = BasicConverter.StringToDbValue(gInvitationCode);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objInvitation = new PatientProviderInvitation();
                    objInvitation.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objInvitation;
        }

        #endregion

        #region Public Methods

        public static PatientProviderInvitation CreatePatientInvitation(string strFName, string strLName, string strEmail, int iProviderID, bool bSentConfirmation)
        {
            PatientProviderInvitation objCreatedInvitation = _CreateInvitation(iProviderID, null, bSentConfirmation);

            PatientProviderInvitationDetails.CreateInvitation(strFName, strLName, strEmail, PatientProviderInvitationDetails.InvitationStatus.None, objCreatedInvitation.InvitationID);

            return objCreatedInvitation;

        }

        public static PatientProviderInvitation CreateProviderInvitation(string strFName, string strLName, string strEmail, int iUserHealthRecordID, bool bSentConfirmation)
        {
            PatientProviderInvitation objCreatedInvitation = _CreateInvitation(null, iUserHealthRecordID, bSentConfirmation);

            //A patient can invite only one provider.
            PatientProviderInvitationDetails.CreateInvitation(strFName, strLName, strEmail, PatientProviderInvitationDetails.InvitationStatus.None, objCreatedInvitation.InvitationID);

            return objCreatedInvitation;
        }

        public static PatientProviderInvitation FindByInvitationCode(string gInvitationCode)
        {
            return _FindByInvitationCode(gInvitationCode, false);
        }

        public static PatientProviderInvitation FindPendingInvitationByInvitationCode(string gInvitationCode)
        {
            return _FindByInvitationCode(gInvitationCode, true);
        }

        public static List<PatientProviderInvitation> FindPendingInvitationsByAcceptedPatient(int iPatientID, PatientProviderInvitationDetails.InvitationStatus status)
        {
            DBContext dbContext = null;

            List<PatientProviderInvitation> objInvitations = new List<PatientProviderInvitation>();

            PatientProviderInvitation objInvitation = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;

                cmd.CommandText = "[Proc_GetInvitationByAcceptedPatientID]";

                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AcceptedPatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Status", SqlDbType.TinyInt);
                param.Value = (int)status;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objInvitation = new PatientProviderInvitation();
                    objInvitation.OnDataBind(reader);

                    objInvitations.Add(objInvitation);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objInvitations;
        }

        public static List<PatientProviderInvitation> FindPendingInvitationsByAcceptedProvider(int iProviderID, PatientProviderInvitationDetails.InvitationStatus status)
        {
            DBContext dbContext = null;

            List<PatientProviderInvitation> objInvitations = new List<PatientProviderInvitation>();

            PatientProviderInvitation objInvitation = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;

                cmd.CommandText = "[Proc_GetInvitationByAcceptedProviderID]";

                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AcceptedProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Status", SqlDbType.TinyInt);
                param.Value = (int)status;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objInvitation = new PatientProviderInvitation();
                    objInvitation.OnDataBind(reader);

                    objInvitations.Add(objInvitation);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objInvitations;
        }

        public static PatientProviderInvitation FindByInvitationID(int iInvitationID)
        {
            DBContext dbContext = null;
            PatientProviderInvitation objInvitation = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_FindInvitationByInvitationID]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@InvitationID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iInvitationID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objInvitation = new PatientProviderInvitation();
                    objInvitation.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objInvitation;
        }

        public void Update()
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                //InvitationCode
                param = dbContext.ContextSqlCommand.Parameters.Add("@InvitationCode", SqlDbType.NChar);
                param.Value = BasicConverter.StringToDbValue(this.InvitationCode);

                //SentPatientID
                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(this.PatientID);

                //SendProviderID
                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(this.ProviderID);

                dbContext.ContextSqlCommand.CommandText = "Proc_UpdatePatientProviderInvitation";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        #endregion

    }

    public class PatientProviderInvitationDetails
    {
        public enum InvitationStatus
        {
            None,
            Pending,
            RemindLater,
            Blocked,
            Accepted,
            ProviderCodeOnly
        }

        #region Public Members

        public int InvitationDetailsID
        {
            get;
            set;
        }
        public int InvitationID
        {
            get;
            set;
        }
        public string Name
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName).Trim();
            }
        }
        public string FirstName
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public DateTime DateInserted
        {
            get;
            set;
        }
        public DateTime? DateAccepted
        {
            get;
            set;
        }
        public InvitationStatus Status
        {
            get;
            set;
        }
        public int? AcceptedPatientID
        {
            get;
            set;
        }
        public int? AcceptedProviderID
        {
            get;
            set;
        }
        public bool IsPrintInvitation
        {
            get
            {
                if (this.Email == null)
                    return true;

                return false;
            }
        }

        #endregion

        #region Private Methods

        private void OnDataBind(SqlDataReader reader)
        {
            this.InvitationDetailsID = BasicConverter.DbToIntValue(reader["InvitationDetailsID"]);
            this.InvitationID = BasicConverter.DbToIntValue(reader["InvitationID"]);
            this.FirstName = BasicConverter.DbToStringValue(reader["FirstName"]);
            this.LastName = BasicConverter.DbToStringValue(reader["LastName"]);
            this.Email = BasicConverter.DbToStringValue(reader["Email"]);
            this.DateInserted = BasicConverter.DbToDateValue(reader["DateInserted"]);
            this.DateAccepted = BasicConverter.DbToNullableDateValue(reader["DateAccepted"]);
            this.AcceptedPatientID = BasicConverter.DbToNullableIntValue(reader["AcceptedPatientID"]);
            this.AcceptedProviderID = BasicConverter.DbToNullableIntValue(reader["AcceptedProviderID"]);

            this.Status = (InvitationStatus)Enum.ToObject(typeof(InvitationStatus), BasicConverter.DbToIntValue(reader["status"]));
        }

        private static List<PatientProviderInvitationDetails> _GetList(string CommandText, List<SqlParameter> ParamColletion)
        {
            DBContext dbContext = null;

            List<PatientProviderInvitationDetails> objInvitations = new List<PatientProviderInvitationDetails>();

            PatientProviderInvitationDetails objInvitation = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = CommandText;

                cmd.CommandType = CommandType.StoredProcedure;

                dbContext.ContextSqlCommand.Parameters.AddRange(ParamColletion.ToArray());

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objInvitation = new PatientProviderInvitationDetails();
                    objInvitation.OnDataBind(reader);

                    objInvitations.Add(objInvitation);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objInvitations;
        }

        private static List<PatientProviderInvitationDetails> _FindByInvitationID(string gInvitationCode, bool bPendingOnly)
        {
            SqlParameter param = null;
            List<SqlParameter> ParamCollection = new List<SqlParameter>();

            param = new SqlParameter("@InvitationCode", SqlDbType.NChar);
            param.Value = BasicConverter.StringToDbValue(gInvitationCode);

            ParamCollection.Add(param);


            if (bPendingOnly)
            {
                return _GetList("[Proc_FindPendingInvitationDetailsByInvitationCode]", ParamCollection);
            }
            else
            {
                return _GetList("[Proc_FindInvitationDetailsByInvitationCode]", ParamCollection);
            }
        }

        #endregion

        #region Public Methods

        public static List<PatientProviderInvitationDetails> FindByInvitationCode(string gInvitationCode)
        {
            return _FindByInvitationID(gInvitationCode, false);
        }

        public static List<PatientProviderInvitationDetails> FindPendingDetailsByInvitationCode(string gInvitationCode)
        {
            return _FindByInvitationID(gInvitationCode, true);
        }

        public static PatientProviderInvitationDetails CreateInvitation(string strFName, string strLName, string strEmail, InvitationStatus Status, int iInvitationID)
        {
            DBContext dbContext = null;
            bool bException = false;
            PatientProviderInvitationDetails objInvitation = null;

            try
            {
                int iInvitationDetailsID;

                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@InvitationID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iInvitationID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strFName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LastName", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strLName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Email", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Status", SqlDbType.TinyInt);
                param.Value = BasicConverter.DbToIntValue((int)Status);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@InvitationDetailsID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreatePatientProviderInvitationDetails]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iInvitationDetailsID = Convert.ToInt32(param_output.Value);

                objInvitation = new PatientProviderInvitationDetails
                {
                    InvitationDetailsID = iInvitationDetailsID,
                    InvitationID = iInvitationID,
                    Email = strEmail,
                    FirstName = strFName,
                    LastName = strLName,
                    Status = Status
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objInvitation;
        }

        public static List<PatientProviderInvitationDetails> FindPendingInvitationByProviderID(int iProviderID)
        {
            SqlParameter param = null;
            List<SqlParameter> ParamCollection = new List<SqlParameter>();

            param = new SqlParameter("@SentProviderID", SqlDbType.Int);
            param.Value = BasicConverter.IntToDbValue(iProviderID);

            ParamCollection.Add(param);

            return _GetList("[Proc_GetInvitationDetailsBySentProviderID]", ParamCollection).Where(InvDet => InvDet.Status != InvitationStatus.Blocked).ToList();

        }

        public static PatientProviderInvitationDetails FindByInvitationDetailsID(int iInvitationDetailsID)
        {
            DBContext dbContext = null;
            PatientProviderInvitationDetails objInvitation = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_FindPatientProviderInvitationDetails]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@InvitationDetailsID", SqlDbType.NChar);
                param.Value = BasicConverter.IntToDbValue(iInvitationDetailsID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objInvitation = new PatientProviderInvitationDetails();
                    objInvitation.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objInvitation;
        }

        public void Update()
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@InvitationDetailsID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(this.InvitationDetailsID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Status", SqlDbType.TinyInt);
                param.Value = (int)this.Status;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AcceptedPatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(this.AcceptedPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AcceptedProviderID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(this.AcceptedProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateAccepted", SqlDbType.DateTime);
                param.Value = BasicConverter.NullableDateToDbValue(this.DateAccepted);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdatePatientProviderInvitationDetails]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void CreateResentInvitation(int iInvitationDetailsID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@InvitationDetailsID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iInvitationDetailsID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateResentInvitation]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

        }

        public static void MarkExistingInvitationsAsBlocked(int iPatientID, int iProviderID, bool? bIsSentByProvider)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderId", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@isSentByProvider", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bIsSentByProvider);

                dbContext.ContextSqlCommand.CommandText = "[Proc_MarkExistingInvitationsAsBlocked]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        #endregion

    }


    //[Proc_GetInvitationDetailsBySentProviderID]
    //[Proc_GetInvitationByAcceptedPatientID]

}
