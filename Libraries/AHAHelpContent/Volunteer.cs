﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

// PJB - this started out supporting functionality for assigning campaigns to volunteers, hence the volunteer class.
//     - when requirements got extended to support providers and providers+volunteers, I just extended this class
//     - without renaming it. So this deals with both providers and volunteers as stored in the Providers table.
//     - This class exports UserID as ProviderID in Providers table.
namespace AHAHelpContent
{
    [Serializable]
    public class Volunteer
    {
        public int UserID
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public int CampaignID
        {
            get;
            set;
        }

        public string CampaignName
        {
            get;
            set;
        }

        
        private void OnDataBind(SqlDataReader reader)
        {
            this.UserID = BasicConverter.DbToIntValue(reader["UserID"]);
            this.Email = BasicConverter.DbToStringValue(reader["Email"]);
            this.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
            this.CampaignName = BasicConverter.DbToStringValue(reader["CampaignName"]);
            this.FirstName = BasicConverter.DbToStringValue(reader["FirstName"]);
            this.LastName = BasicConverter.DbToStringValue(reader["LastName"]);
        }

        

        public static void UpdateUserCampaign(int intVolunteerID, int intCampaignID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intVolunteerID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intCampaignID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateUserCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static Volunteer GetUserByID(int intUserID)
        {
            DBContext           dbContext = null;
            Volunteer           objVolunteer = null;
            SqlDataReader       reader = null;
            SqlCommand          cmd = null;
            bool                bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetUserByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intUserID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objVolunteer = new Volunteer();
                    objVolunteer.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objVolunteer;
        }
        

        public static List<Volunteer> GetAllVolunteers( bool bOnlyNullCampaigns )
        {
            DBContext           dbContext = null;
            List<Volunteer>     objVolList = new List<Volunteer>();
            SqlDataReader       reader = null;
            SqlCommand          cmd = null;
            bool                bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllProviders]";

                cmd = dbContext.ContextSqlCommand;

                SqlParameter param = dbContext.ContextSqlCommand.Parameters.Add("@UserType", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(2);

                param = dbContext.ContextSqlCommand.Parameters.Add("@OnlyNullCampaigns", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bOnlyNullCampaigns);

                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Volunteer   objVol = new Volunteer();
                    objVol.OnDataBind(reader);
                    objVolList.Add(objVol);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objVolList;
        }

        public static List<Volunteer> GetAllProviders(bool bOnlyNullCampaigns)
        {
            DBContext dbContext = null;
            List<Volunteer> objVolList = new List<Volunteer>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllProviders]";

                cmd = dbContext.ContextSqlCommand;

                SqlParameter param = dbContext.ContextSqlCommand.Parameters.Add("@UserType", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(1);

                param = dbContext.ContextSqlCommand.Parameters.Add("@OnlyNullCampaigns", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bOnlyNullCampaigns);

                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Volunteer objVol = new Volunteer();
                    objVol.OnDataBind(reader);
                    objVolList.Add(objVol);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objVolList;
        }

        public static List<Volunteer> GetAllProvidersAndVolunteers(bool bOnlyNullCampaigns)
        {
            DBContext dbContext = null;
            List<Volunteer> objVolList = new List<Volunteer>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllProviders]";

                cmd = dbContext.ContextSqlCommand;

                SqlParameter param = dbContext.ContextSqlCommand.Parameters.Add("@UserType", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue( null );   // signifies providers and volunteers

                param = dbContext.ContextSqlCommand.Parameters.Add("@OnlyNullCampaigns", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bOnlyNullCampaigns);

                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Volunteer objVol = new Volunteer();
                    objVol.OnDataBind(reader);
                    objVolList.Add(objVol);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objVolList;
        }
    }
}
