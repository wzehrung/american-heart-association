﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    [Serializable]
    public class Resource
    {
        #region Public Properties
        public int ResourceID
        {
            get;
            set;
        }

        public string ContentTitle
        {
            get;
            set;
        }

        public string ContentTitleUrl
        {
            get;
            set;
        }

        public string ContentText
        {
            get;
            set;
        }

        public int SortOrder
        {
            get;
            set;
        }

        public int LanguageID
        {
            get;
            set;
        }

        public int PatientID
        {
            get;
            set;
        }

        public bool IsProvider
        {
            get;
            set;
        }

        public string ProviderName
        {
            get;
            set;
        }

        public string AnnouncementTitle
        {
            get;
            set;
        }

        public string AnnouncementText
        {
            get;
            set;
        }

        public DateTime? ExpirationDate
        {
            get;
            set;
        }

        public int? CreatedByUserID
        {
            get;
            set;
        }

        public int? UpdatedByUserID
        {
            get;
            set;
        }

        public string CreatedDate
        {
            get;
            set;
        }

        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.ResourceID = BasicConverter.DbToIntValue(reader["ResourceID"]);
            this.ContentTitle = BasicConverter.DbToStringValue(reader["ContentTitle"]);
            this.ContentTitleUrl = BasicConverter.DbToStringValue(reader["ContentTitleUrl"]);
            this.ContentText = BasicConverter.DbToStringValue(reader["ContentText"]);
            this.SortOrder = BasicConverter.DbToIntValue(reader["SortOrder"]);
            this.IsProvider = BasicConverter.DbToBoolValue(reader["IsProvider"]);
            this.ExpirationDate = BasicConverter.DbToNullableDateValue(reader["ExpirationDate"]);
            this.CreatedByUserID = BasicConverter.DbToNullableIntValue(reader["CreatedByUserID"]);
            this.UpdatedByUserID = BasicConverter.DbToNullableIntValue(reader["UpdatedByUserID"]);
            
        }

        private void OnDataBindAnnouncement(SqlDataReader readerAnnouncement)
        {
            //this.IsProvider = BasicConverter.DbToBoolValue(readerAnnouncement["IsProvider"]);
            this.LanguageID = BasicConverter.DbToIntValue(readerAnnouncement["LanguageID"]);
            this.PatientID = BasicConverter.DbToIntValue(readerAnnouncement["PatientID"]);
            this.AnnouncementTitle = BasicConverter.DbToStringValue(readerAnnouncement["AnnouncementTitle"]);
            this.AnnouncementText = BasicConverter.DbToStringValue(readerAnnouncement["AnnouncementText"]);
            this.CreatedDate = BasicConverter.DbToStringValue(readerAnnouncement["CreatedDate"]);   
            this.ProviderName = BasicConverter.DbToStringValue(readerAnnouncement["ProviderName"]);        
        }

        private void OnDataBindGetAnnouncements(SqlDataReader reader)
        {
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
        }

        #region Public Methods
        public static Resource CreateResource(
            string strContentTitle,
            string strContentTitleUrl,
            string strContentText,
            bool bIsProvider,
            DateTime? ExpirationDate,
            int? intCreatedByUserID,
            int? intUpdatedByUserID,
            int intLanguageID,
            int intProviderID = 0)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iResourceID=0;
            Resource objResource = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitle", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitleUrl", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitleUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentText", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentText);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                param.Value = BasicConverter.NullableDateToDbValue(ExpirationDate);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                //param.Value = BasicConverter.NullableIntToDbValue(intCreatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(intUpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intProviderID);

               // param = dbContext.ContextSqlCommand.Parameters.Add("@ResourceID", SqlDbType.Int);
               // param.Value = BasicConverter.IntToDbValue(iResourceID);


                //dbContext.ContextSqlCommand.CommandText = "[Proc_CreateResource]";

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeResource]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ResourceID", SqlDbType.Int));
                param_output.Value = BasicConverter.IntToDbValue(iResourceID);
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iResourceID = Convert.ToInt32(param_output.Value);
                objResource = new Resource
                {
                    ResourceID=iResourceID,
                    ContentTitle = strContentTitle,
                    ContentTitleUrl = strContentTitleUrl,
                    ContentText = strContentText
                };

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objResource;
        }

        public static void UpdateResource(
            int iResourceID,
            string strContentTitle,
            string strContentTitleUrl,
            string strContentText,
             bool bIsProvider,
            DateTime? ExpirationDate,
             int? intCreatedByUserID,
            int? intUpdatedByUserID,
            int intLanguageID,
            int intProviderID=0)
        {
            DBContext dbContext = null;
            bool bException = false;
            
            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                //param = dbContext.ContextSqlCommand.Parameters.Add("@ResourceID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(iResourceID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitle", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitleUrl", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitleUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentText", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentText);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                param.Value = BasicConverter.NullableDateToDbValue(ExpirationDate);

               // param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                //param.Value = BasicConverter.NullableIntToDbValue(intCreatedByUserID);

                              


                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(intUpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intProviderID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateResource]";

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeResource]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ResourceID", SqlDbType.Int));
                param_output.Value = BasicConverter.IntToDbValue(iResourceID);
                //param_output.Direction = ParameterDirection.Output;


                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iResourceID = Convert.ToInt32(param_output.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void SwapResources(
            int iFirstResourceID,
            int iSecondResourceID,
            int intUpdatedByUserID,
            int intLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FirstResourceID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iFirstResourceID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@SecondResourceID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iSecondResourceID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(intUpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);


                dbContext.ContextSqlCommand.CommandText = "[Proc_SwapResources]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteResource(
            int iResourceID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ResourceID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iResourceID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteResource]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteAnnouncement(
    int iAnnouncementID, int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AnnouncementID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAnnouncementID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteAnnoucement]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }


        #endregion

        public static List<Resource> FindAllResources(bool bIsProvider, int intLanguageID, int intProviderID = 0)
        {
            DBContext dbContext = null;
            List<Resource> objResourceList = new List<Resource>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAllResources]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = intProviderID;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Resource objResource = new Resource();
                    objResource.OnDataBind(reader);
                    objResourceList.Add(objResource);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResourceList;
        }

        public static List<Announcement> FindAllAnnouncements(bool bIsProvider, int intLanguageID, int intProviderID = 0)
        {
            DBContext dbContext = null;
            List<Announcement> objAnnouncementList = new List<Announcement>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAllAnnouncements]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = intProviderID;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Announcement objAnnouncement = new Announcement(BasicConverter.DbToIntValue(reader["PatientContentID"]), BasicConverter.DbToStringValue(reader["ContentTitle"]), BasicConverter.DbToStringValue(reader["ContentText"]));
                    objAnnouncementList.Add(objAnnouncement);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAnnouncementList;
        }

        public static List<Resource> FindLocalResourcesForPatient(bool bIsProvider, int intLanguageID, int intPatientID)
        {
            DBContext dbContext = null;
            List<Resource> objResourceList = new List<Resource>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindLocalResourcesForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = intPatientID;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Resource objResource = new Resource();
                    objResource.OnDataBind(reader);
                    objResourceList.Add(objResource);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResourceList;
        }

        public static string FindAnnouncementsForPatient(int intLanguageID, int intPatientID)
        {
            DBContext dbContext = null;
            StringBuilder strRetVal = new StringBuilder();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlDataAdapter adapter = new SqlDataAdapter();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAnnouncementsForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                //param = dbContext.ContextSqlCommand.Parameters.Add("@IsProvider", SqlDbType.Bit);
                //param.Value = BasicConverter.BoolToDbValue(bIsProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = intPatientID;

                DataSet dataSet = new DataSet("Announcements");
                adapter.Fill(dataSet);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    strRetVal = strRetVal.Append(reader["AnnouncementTitle"].ToString());
                    strRetVal.Append(reader["AnnouncementText"].ToString());
                    strRetVal.Append(reader["ProviderName"].ToString());
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return strRetVal.ToString();
        }

            // PJB: this is used in V6.
        public static int GetAnnouncementCountForPatient(int intPatientID)
        {
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            int     iRetval = 0 ;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_Get_NewAnnouncementsForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                //param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = intPatientID;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    iRetval++;
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iRetval;
        }

        public static List<Resource> FindAllResourcesExpiringOn(DateTime expiringDate)
        {
            DBContext dbContext = null;
            List<Resource> objResourceList = new List<Resource>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetResourcesExpiringOn]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(expiringDate);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Resource objResource = new Resource();
                    objResource.OnDataBind(reader);
                    objResourceList.Add(objResource);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResourceList;
        }

        public static List<Resource> FindAllNonExpiredResources(bool bIsProvider,int LanguageID)
        {
            DBContext dbContext = null;
            List<Resource> objResourceList = new List<Resource>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlParameter param = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();               

                string sql = "[Proc_FindAllNonExpiredResources]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                //SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Resource objResource = new Resource();
                    objResource.OnDataBind(reader);
                    objResourceList.Add(objResource);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResourceList;
        }

        public static Resource FindByResourceID(int iResourceID, int intLanguageID)
        {
            DBContext dbContext = null;
            Resource objResource = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindResourceByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ResourceID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iResourceID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objResource = new Resource();
                    objResource.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResource;
        }

        public static List<Resource> GetAnnouncements(int iPatientID, int iLanguageID)
        {
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            List<Resource> objResourceList = new List<Resource>();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Get_AnnouncementsForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Resource objPatient = new Resource();
                    objPatient.OnDataBindAnnouncement(reader);
                    objResourceList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResourceList;
        }

         #endregion
    }
    [Serializable]
    public class Announcement
    {
        public string Title
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public int AnnouncementID
        {
            get;
            set;
        }

        public Announcement(int AnnouncementID, string title, string text)
        {
            this.AnnouncementID = AnnouncementID;
            this.Title = title;
            this.Text = text;
        }

        private void OnDataBindAnnouncement(SqlDataReader readerAnnouncement)
        {
            //this.IsProvider = BasicConverter.DbToBoolValue(readerAnnouncement["IsProvider"]);
            this.AnnouncementID = BasicConverter.DbToIntValue(readerAnnouncement["PatientContentID"].ToString());
            this.Title = BasicConverter.DbToStringValue(readerAnnouncement["ContentTitle"]);
            this.Text = BasicConverter.DbToStringValue(readerAnnouncement["ContentText"]);
        }
    }

    //[Serializable]
    //public class PatientResourceGraphicsPreview
    //{
    //    public byte Image
    //    {
    //        get;
    //        set;
    //    }

    //    public PatientResourceGraphicsPreview(byte image)
    //    {
    //        this.Image = image;
    //    }
    //}
}
