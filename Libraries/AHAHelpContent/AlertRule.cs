﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    // PJB - I just love this non-documented simplicity. Let me guess, A = All, P = Patient/Provider, G = Group ?
    public enum AlertRuleSubscriberType
    {
        A,
        P,
        G
    }

    public class AlertRule
    {
        #region Public Properties
        public int AlertRuleID
        {
            get;
            set;
        }

        public int ProviderID
        {
            get;
            set;
        }

        public int? PatientID
        {
            get;
            set;
        }

        public string GroupName
        {
            get;
            set;
        }

        public int? GroupID
        {
            get;
            set;
        }

        public AlertRuleSubscriberType AlertRuleSubscriberType
        {
            get;
            set;
        }

        public TrackerDataType Type
        {
            get;
            set;
        }

        public string AlertRuleData
        {
            get;
            set;
        }

        public bool? SendMessageToPatient
        {
            get;
            set;
        }

        public string MessageToPatient
        {
            get;
            set;
        }
        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.AlertRuleID = BasicConverter.DbToIntValue(reader["AlertRuleID"]);
            this.ProviderID = BasicConverter.DbToIntValue(reader["ProviderID"]);
            this.PatientID = BasicConverter.DbToNullableIntValue(reader["PatientID"]);
            this.GroupID = BasicConverter.DbToNullableIntValue(reader["GroupID"]);
            this.GroupName = BasicConverter.DbToStringValue(reader["GroupName"]);
            string strType = BasicConverter.DbToStringValue(reader["Type"]);
            Type = ((TrackerDataType)Enum.Parse(typeof(TrackerDataType), strType, false));

            string strAlertRuleSubscriberType = BasicConverter.DbToStringValue(reader["AlertRuleSubscriberType"]);
            AlertRuleSubscriberType = ((AlertRuleSubscriberType)Enum.Parse(typeof(AlertRuleSubscriberType), strAlertRuleSubscriberType, false));

            string strAlertRuleData = BasicConverter.DbToStringValue(reader["AlertData"]);

            AlertRuleData = strAlertRuleData;

            SendMessageToPatient = BasicConverter.DbToNullableBoolValue(reader["SendMessageToPatient"]);
            this.MessageToPatient = BasicConverter.DbToStringValue(reader["MessageToPatient"]);
        }
        #endregion


        #region Public Methods
        public static AlertRule CreateAlertRule(int iProviderID,
            int? iPatientID,
            int? iGroupID,
            TrackerDataType eType,
            string strAlertRuleData,
            bool bSendMessageToPatient,
            string strMessageToPatient)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iAlertRuleID;
            AlertRule objAlertRule = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iGroupID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Type", SqlDbType.VarChar);
                param.Value = eType.ToString();

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertData", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strAlertRuleData);

                param = dbContext.ContextSqlCommand.Parameters.Add("@SendMessageToPatient", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bSendMessageToPatient);

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageToPatient", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strMessageToPatient);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@AlertRuleID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateAlertRule]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iAlertRuleID = Convert.ToInt32(param_output.Value);
                objAlertRule = new AlertRule
                {
                    AlertRuleID = iAlertRuleID,
                    ProviderID = iProviderID,
                    PatientID = iPatientID,
                    GroupID = iGroupID,
                    Type = eType,
                    AlertRuleData = strAlertRuleData
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objAlertRule;
        }

        public static void UpdateAlertRule(
            int iAlertRuleID,
            int? iPatientID,
            int? iGroupID,
            TrackerDataType eType,  
            string strAlertRuleData,
            bool bSendMessageToPatient,
            string strMessageToPatient)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iGroupID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertData", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strAlertRuleData);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Type", SqlDbType.VarChar);
                param.Value = eType.ToString();

                param = dbContext.ContextSqlCommand.Parameters.Add("@SendMessageToPatient", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bSendMessageToPatient);

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageToPatient", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strMessageToPatient);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertRuleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertRuleID);


                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateAlertRule]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteAlertRule(int iAlertRuleID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertRuleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertRuleID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteAlertRuleByID]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static AlertRule FindByAlertRuleID(int iAlertRuleID)
        {
            DBContext dbContext = null;
            AlertRule objAlertRule = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertRuleByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertRuleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertRuleID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAlertRule = new AlertRule();
                    objAlertRule.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertRule;
        }

        public static List<AlertRule> FindByProviderID(int iProviderID)
        {
            DBContext dbContext = null;
            List<AlertRule> objAlertRuleList = new List<AlertRule>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertRuleByProviderID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AlertRule objAlertRule = new AlertRule();
                    objAlertRule.OnDataBind(reader);
                    objAlertRuleList.Add(objAlertRule);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertRuleList;
        }

        public static List<AlertRule> FindByGroupID(int iGroupID)
        {
            DBContext dbContext = null;
            List<AlertRule> objAlertRuleList = new List<AlertRule>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertRuleByGroupID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AlertRule objAlertRule = new AlertRule();
                    objAlertRule.OnDataBind(reader);
                    objAlertRuleList.Add(objAlertRule);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertRuleList;
        }

        public static List<AlertRule> FindByPatientID(int iPatientID, TrackerDataType? eType)
        {
            DBContext dbContext = null;
            List<AlertRule> objAlertRuleList = new List<AlertRule>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertRuleByPatientIDAndType]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Type", SqlDbType.VarChar);
                if (eType.HasValue)
                {
                    param.Value = eType.ToString();
                }
                else
                {
                    param.Value = DBNull.Value;
                }



                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AlertRule objAlertRule = new AlertRule();
                    objAlertRule.OnDataBind(reader);
                    objAlertRuleList.Add(objAlertRule);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertRuleList;
        }

        public static List<AlertRule> FindByUserHealthRecordGUID(Guid gUserHealthRecordGUID, TrackerDataType? eType)
        {
            DBContext dbContext = null;
            List<AlertRule> objAlertRuleList = new List<AlertRule>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAlertRuleByPatientGUIDAndType]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gUserHealthRecordGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Type", SqlDbType.VarChar);
                if (eType.HasValue)
                {
                    param.Value = eType.ToString();
                }
                else
                {
                    param.Value = DBNull.Value;
                }



                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AlertRule objAlertRule = new AlertRule();
                    objAlertRule.OnDataBind(reader);
                    objAlertRuleList.Add(objAlertRule);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAlertRuleList;
        }

        public static void RemovePatientFromAlertRule(int iAlertRuleID, List<int> listPatient)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertRuleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertRuleID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.VarChar);
                string strComma = string.Join(",", listPatient.ConvertAll<string>(i => i.ToString()).ToArray());
                param.Value = BasicConverter.StringToDbValue(strComma);

                dbContext.ContextSqlCommand.CommandText = "[proc_RemovePatientsFromAlertRuleMapping]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
        #endregion
    }
}
