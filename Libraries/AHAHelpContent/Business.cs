﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using GRCBase;

namespace AHAHelpContent
{
    [Serializable]
    public class Business
    {
            #region Public Properties
            public int BusinessID
            {
                get;
                set;
            }

            public string Name
            {
                get;
                set;
            }

            public string Comments
            {
                get;
                set;
            }

            #endregion

            #region Private Methods
            private void OnDataBind(SqlDataReader reader)
            {
                this.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                this.Name = BasicConverter.DbToStringValue(reader["BusinessName"]);
                this.Comments = BasicConverter.DbToStringValue(reader["Comments"]);
            }
            #endregion

            #region Public Methods

            public static Business CreateBusiness(string strName, string strComments)
            {
                DBContext dbContext = null;
                bool bException = false;
                int iBusinessID = 0;
                Business objBusiness = null;

                try
                {
                    dbContext = DBContext.GetDBContext();
                    dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessName", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue(strName);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue(strComments);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iBusinessID);

                    dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeBusiness]";

                    dbContext.ContextSqlCommand.ExecuteNonQuery();

                    // PJB TODO - check this out

                    //iCampaignID = Convert.ToInt32(param_output.Value);
                    objBusiness = new Business
                    {
                        //CampaignID = iCampaignID,
                        Name = strName,
                        Comments = strComments,
                    };
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {

                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }

                return objBusiness;
            }

            public static void UpdateBusiness(int iBusinessID, string strName, string strComments )
            {
                DBContext dbContext = null;
                bool bException = false;


                try
                {
                    dbContext = DBContext.GetDBContext();
                    dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessName", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue(strName);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue(strComments);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iBusinessID);

                    dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeBusiness]";

                    dbContext.ContextSqlCommand.ExecuteNonQuery();

                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
            }


            public static Business FindBusinessByBusinessID(int iBusinessID )
            {
                DBContext dbContext = null;
                Business objBusiness = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "[Proc_FindBusinessByID]";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iBusinessID);

                    reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        objBusiness = new Business();
                        //objCampaign.OnDataBind(reader);
                        objBusiness.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                        objBusiness.Name = BasicConverter.DbToStringValue(reader["BusinessName"]);
                        objBusiness.Comments = BasicConverter.DbToStringValue(reader["Comments"]);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objBusiness;
            }

            public static List<Business> FindAllBusinesses()
            {
                DBContext dbContext = null;
                List<Business> objBusinessList = new List<Business>();
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                try
                {
                    dbContext = DBContext.GetDBContext();

                    string sql = "[Proc_FindAllBusinesses]";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Business objBusiness = new Business();
                        objBusiness.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                        objBusiness.Name = BasicConverter.DbToStringValue(reader["BusinessName"]);
                        objBusiness.Comments = BasicConverter.DbToStringValue(reader["Comments"]);

                        objBusinessList.Add(objBusiness);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objBusinessList;
            }

            #endregion
        }

}
