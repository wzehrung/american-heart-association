﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class AHAUserLoginDetails
    {
        public struct LoginDate
        {
            public DateTime? LastLoginDate;
            public DateTime PresentLoginDate;
            public int NumberOfLogins;

            /// <summary>
            /// Creates the Login Details of the AHAUser
            /// </summary>
            /// <param name="lastLoginDate"></param>
            /// <param name="presentLoginDate"></param>
            /// <param name="numberOfLogins"></param>
            /// <returns></returns>
            public void Create(DateTime? lastLoginDate, DateTime presentLoginDate, int numberOfLogins)
            {
                LastLoginDate = lastLoginDate;
                PresentLoginDate = presentLoginDate;
                NumberOfLogins = numberOfLogins;
            }
        }

        /// <summary>
        /// Creates AHAUserLoginDetails
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <param name="sDeviceName">Can be string.Empty or name of Client Device</param>
        /// <param name="sDeviceId">Can be string.Empty or Client Device ID</param>
        /// <returns></returns>
        public static void CreateAHAUserLoginDetails(Guid PersonGuid, string sDeviceName, string sDeviceId )
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGuid);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@LoginDetailsID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateAHAUserLoginDetails";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return;
        }

        /// <summary>
        /// Gets the last visited time of a user to the application
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <returns>DateTime</returns>
        public static LoginDate GetUserLastLoggedInDate(Guid PersonGuid)
        {
            DBContext dbContext = null;
            LoginDate loginStruct = new LoginDate();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGuid);

                SqlParameter param_output_lastvisteddate = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("LastVistedDate", SqlDbType.DateTime));
                param_output_lastvisteddate.Direction = ParameterDirection.Output;

                SqlParameter param_output_currentlogintime = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("CurrentLoginTime", SqlDbType.DateTime));
                param_output_currentlogintime.Direction = ParameterDirection.Output;

                SqlParameter param_output_nooflogins = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("NoOfLogIns", SqlDbType.Int));
                param_output_nooflogins.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "Proc_GetAHAUserLogInDetails";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                loginStruct.Create(
                    BasicConverter.DbToNullableDateValue(param_output_lastvisteddate.Value),
                    BasicConverter.DbToDateValue(param_output_currentlogintime.Value),
                    BasicConverter.DbToIntValue(param_output_nooflogins.Value)
                    );
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return loginStruct;
        }
    }
}
