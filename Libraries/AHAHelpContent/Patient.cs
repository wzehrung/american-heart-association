﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class Patient
    {
        #region Public Properties

        public string PolkaID
        {
            get;
            set;
        }

        public int UserHealthRecordID
        {
            get;
            set;
        }

        public Guid? OfflinePersonGUID
        {
            get;
            set;
        }

        public Guid UserHealthRecordGUID
        {
            get;
            set;
        }

        public Guid? OfflineHealthRecordGUID
        {
            get;
            set;
        }

        public bool HasAgreedProviderTOU
        {
            get;
            set;
        }

        public bool HasProvidedOfflineAccess
        {
            get;
            set;
        }

        public Guid PersonalItemGUID
        {
            get;
            set;
        }

        public DateTime CreatedDate
        {
            get;
            set;
        }

        public DateTime UpdatedDate
        {
            get;
            set;
        }

        // Note: this is actually the date the Patient joined the group, the date field from the PatientGroupMapping table.
        public DateTime? DateJoined
        {
            get;
            set;
        }

        // Note: this is the date the Patient connected with the Provider
        public DateTime? ProviderConnectionDate
        {
            get;
            set;
        }

        public bool IsMigrated
        {
            get;
            set;
        }

        public int TotalAlerts
        {
            get;
            set;
        }

        public DateTime? LastActivityDate
        {
            get;
            set;
        }

        public string GroupIDList
        {
            get;
            set;
        }

        public bool HasAlertRule
        {
            get;
            set;
        }

        public int? PatientID
        {
            get;
            set;
        }
        #endregion

        #region Private Methods

        private void OnDataBind(SqlDataReader reader, bool bIncludeAlert, bool bIncludeGroupIDList, bool bHasAlertRule)
        {
            this.UserHealthRecordID = BasicConverter.DbToIntValue(reader["UserHealthRecordID"]);
            this.OfflinePersonGUID = BasicConverter.DbToNullableGuidValue(reader["OfflinePersonGUID"]);
            this.OfflineHealthRecordGUID = BasicConverter.DbToNullableGuidValue(reader["OfflineHealthRecordGUID"]);
            this.UserHealthRecordGUID = BasicConverter.DbToGuidValue(reader["UserHealthRecordGUID"]);
            this.HasAgreedProviderTOU = BasicConverter.DbToBoolValue(reader["HasAgreedProviderTOU"]);
            this.HasProvidedOfflineAccess = BasicConverter.DbToBoolValue(reader["HasProvidedOfflineAccess"]);
            this.PersonalItemGUID = BasicConverter.DbToGuidValue(reader["PersonalItemGUID"]);
            this.CreatedDate = BasicConverter.DbToDateValue(reader["CreatedDate"]);
            this.UpdatedDate = BasicConverter.DbToDateValue(reader["UpdatedDate"]);
            this.IsMigrated = BasicConverter.DbToBoolValue(reader["IsMigrated"]);

            this.LastActivityDate = BasicConverter.DbToNullableDateValue(reader["LastActivityDate"]);

            if (bIncludeAlert)
            {
                this.TotalAlerts = BasicConverter.DbToIntValue(reader["TotalAlerts"]);
            }

            if (bIncludeGroupIDList)
            {
                this.GroupIDList = BasicConverter.DbToStringValue(reader["GroupIDs"]);
            }

            if (bHasAlertRule)
            {
                this.HasAlertRule = BasicConverter.DbToBoolValue(reader["HasAlertRule"]);
            }

            using (DataTable dt = reader.GetSchemaTable())
            {
                if (Misc.DoesColumnExist(dt, "Date"))
                {
                    this.DateJoined = BasicConverter.DbToDateValue(reader["Date"]);
                }

                if (Misc.DoesColumnExist(dt, "ProviderConnectionDate"))
                {
                    this.ProviderConnectionDate = BasicConverter.DbToDateValue(reader["ProviderConnectionDate"]);
                }
            }

            this.PolkaID = BasicConverter.DbToStringValue(reader["PolkaID"]);
        }

        private void OnDataBindInCampaign(SqlDataReader reader)
        {
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
        }

        #endregion

        #region Public Methods

        public static List<Patient> FindAllByProviderID(int iProviderID)
        {
            DBContext dbContext = null;
            List<Patient> objPatientList = new List<Patient>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAllPatientsByProviderID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patient objPatient = new Patient();
                    objPatient.OnDataBind(reader, true, true, false);
                    objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static List<Patient> FindAllByGroupID(int iGroupID)
        {
            DBContext dbContext = null;
            List<Patient> objPatientList = new List<Patient>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientsByGroupID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patient objPatient = new Patient();
                    objPatient.OnDataBind(reader, true, false, false);
                    objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static List<Patient> FindPatientsWithProviderGroupRules()
        {
            DBContext dbContext = null;
            List<Patient> objPatientList = new List<Patient>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetPatientsWithProviderGroupRules]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patient objPatient = new Patient();
                    objPatient.OnDataBind(reader, false, false, false);
                    objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static List<Patient> FindEligiblePatientsForGroup(int iGroupID)
        {
            DBContext dbContext = null;
            List<Patient> objPatientList = new List<Patient>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindEligiblePatientsForGroup]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patient objPatient = new Patient();
                    objPatient.OnDataBind(reader, false, false, false);
                    objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static void SetTOUAcceptedByPatient(int iPatientID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_SetTOUAcceptedByPatient]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void SetHasProvidedOfflineAccess(int iPatientID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_SetHasProvidedOfflineAccess]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static Patient FindByUserHealthRecordGUID(Guid gUserHealthRecordGUID)
        {
            DBContext dbContext = null;
            Patient objPatient = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientByHealthRecordGUID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gUserHealthRecordGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatient = new Patient();
                    objPatient.OnDataBind(reader, true, false, false);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatient;
        }

        public static Patient FindByPersonalItemGUID(Guid gFindByPersonalItemGUID)
        {
            DBContext dbContext = null;
            Patient objPatient = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientByPersonalItemGUID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gFindByPersonalItemGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatient = new Patient();
                    objPatient.OnDataBind(reader, true, false, false);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatient;
        }

        public static Patient FindByUserOfflineHealthRecordGUID(Guid gUserHealthRecordGUID)
        {
            DBContext dbContext = null;
            Patient objPatient = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientByOfflineHealthRecordGUID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gUserHealthRecordGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatient = new Patient();
                    objPatient.OnDataBind(reader, true, false, false);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatient;
        }

        public static Patient FindByUserHealthRecordID(int iPatientID)
        {
            DBContext dbContext = null;
            Patient objPatient = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientByPatientID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatient = new Patient();
                    objPatient.OnDataBind(reader, false, false, false);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatient;
        }

        public static int GetProviderCount(int iPatientID)
        {
            DBContext dbContext = null;
            int iCount = 0;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetNumberOfProvidersForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    iCount = BasicConverter.DbToIntValue(reader["NoOfProviders"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iCount;
        }

        public static void DisconnectProvider(int iPatientID, int iProviderID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);


                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DisconnectPatient]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void CreateHealthRecord(Guid? PersonalItemGUID, Guid SelectedRecordGUID, Guid PersonGUID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = SelectedRecordGUID;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                if (PersonalItemGUID.HasValue)
                {
                    param.Value = BasicConverter.GuidToDbValue(PersonalItemGUID.Value);
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGUID);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@HealthRecordID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateHealthRecord";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return;
        }

        public static void UpdateOfflinePersonGuid(Guid gPersonalItemGUID, Guid gHealthRecordGUID, Guid gOfflinePersonGUID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = gPersonalItemGUID;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = gHealthRecordGUID;

                param = dbContext.ContextSqlCommand.Parameters.Add("@OfflinePersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = gOfflinePersonGUID;

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateOfflinePersonGUIDandOfflineHealthRecordGUIDForPersonalItemGUID]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return;
        }

        public static bool IsMessagingEnabledForPatient(int iPatientID)
        {
            DBContext dbContext = null;
            bool bIsMessageEnabled = false;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_IsMessagingEnabledForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Result", SqlDbType.Bit);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                if (BasicConverter.DbToBoolValue(param.Value))
                {
                    bIsMessageEnabled = true;
                }

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return bIsMessageEnabled;
        }

        #endregion

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(Patient))
            {
                Patient objPatient = obj as Patient;

                if (objPatient.UserHealthRecordID == this.UserHealthRecordID)
                {
                    return true;
                }
            }
            return false;
        }

        public static List<Patient> FindAllByAlertRuleID(int iAlertRuleID)
        {
            DBContext dbContext = null;
            List<Patient> objPatientList = new List<Patient>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientsByAlertRuleID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AlertRuleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAlertRuleID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patient objPatient = new Patient();
                    objPatient.OnDataBind(reader, false, false, true);
                    objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static List<Patient> GetPatientsForPolkaDataProcessing()
        {
            DBContext dbContext = null;
            List<Patient> objPatientList = new List<Patient>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Polka_GetPatientsForPolkaDataProcessing]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patient objPatient = new Patient();
                    objPatient.OnDataBind(reader, false, false, false);
                    objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static List<Patient> GetPatientsForPolkaDataProcessingV2()
        {
            DBContext dbContext = null;
            List<Patient> objPatientList = new List<Patient>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Polka_GetPatientsForPolkaDataProcessingV2]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patient objPatient = new Patient();
                    objPatient.OnDataBind(reader, false, false, false);
                    objPatientList.Add(objPatient);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static Patient IsPatientInACampaign(int iPatientID)
        {
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            Patient objPatient = new Patient();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_IsPatientInACampaign]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objPatient.OnDataBindInCampaign(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatient;
        }
    }
}
