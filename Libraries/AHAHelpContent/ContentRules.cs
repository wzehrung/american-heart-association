﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    /// <summary>
    /// Primary Key
    /// </summary>
    public class ContentRulesPK : PrimaryKey
    {
        private int m_ContentRulesID;

        public int ContentRulesID
        {
            get
            {
                return m_ContentRulesID;
            }
        }
        public ContentRulesPK(int ContentRulesID)
        {
            m_ContentRulesID = ContentRulesID;
        }
        public override string WhereClause
        {
            get
            {
                return "ContentRulesID = " + m_ContentRulesID;
            }
        }
    }

    public class ContentRules : DataObject
    {
        private ContentRulesPK m_ContentRulesPK;
        /// <summary>
        /// Gets or sets Primary Key
        /// </summary>
        public override PrimaryKey Key
        {
            get
            {
                return m_ContentRulesPK;
            }
            set
            {
                m_ContentRulesPK = (ContentRulesPK)value;
            }
        }

        public int ContentRuleID
        {
            get
            {
                return m_ContentRulesPK.ContentRulesID;
            }
        }
        
        private string m_RuleName;
        /// <summary>
        /// Gets or sets RuleName
        /// </summary>
        public string RuleName
        {
            get
            {
                return m_RuleName;
            }
            set
            {
                m_RuleName = value;
            }
        }

        private string m_ClassName;
        /// <summary>
        /// Gets or sets ClassName
        /// </summary>
        public string ClassName
        {
            get
            {
                return m_ClassName;
            }
            set
            {
                m_ClassName = value;
            }
        }

        private char m_Abbreviation;
        /// <summary>
        /// Gets or sets Abbreviation
        /// </summary>
        public char Abbreviation
        {
            get
            {
                return m_Abbreviation;
            }
            set
            {
                m_Abbreviation = value;
            }
        }

        private string m_DisplayName;
        /// <summary>
        /// Gets or sets DisplayName
        /// </summary>
        public string DisplayName
        {
            get
            {
                return m_DisplayName;
            }
            set
            {
                m_DisplayName = value;
            }
        }
        

        /// <summary>
        /// Creates a new row ContentRules
        /// </summary>
        /// <param name="RuleName"></param>
        /// <param name="ClassName"></param>
        /// <param name="Abbreviation"></param>
        /// <returns>ContentRulesPK</returns>
        public static ContentRulesPK Create(string RuleName, string ClassName, char Abbreviation,string Displayname)
        {
            ContentRules objContentRules = new ContentRules();
            objContentRules.m_RuleName = RuleName;
            objContentRules.m_ClassName = ClassName;
            objContentRules.m_Abbreviation = Abbreviation;
            objContentRules.m_DisplayName = Displayname;
            return (ContentRulesPK)objContentRules.Create();
        }

        /// <summary>
        /// Binds the data from the database to the corresponding variables
        /// </summary>
        /// <param name="objSource"></param>
        /// <returns></returns>

        protected override void internalOnDataBind(object objSource)
        {
            SqlDataReader reader = (SqlDataReader)objSource;
            this.m_ContentRulesPK = new ContentRulesPK(Convert.ToInt32(reader["ContentRulesID"]));
            this.m_RuleName = BasicConverter.DbToStringValue(reader["RuleName"]);
            this.m_ClassName = BasicConverter.DbToStringValue(reader["ClassName"]);
            this.m_Abbreviation = BasicConverter.DbToCharValue(reader["Abbreviation"]);
            this.m_DisplayName = BasicConverter.DbToStringValue(reader["DisplayName"]);
            return;
        }

        /// <summary>
        /// deletes given ContentRules data
        /// </summary>
        /// <param name="objCmd"></param>
        protected override void internalDelete(SqlCommand objCmd)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                string sql = "Delete from ContentRules where " + m_ContentRulesPK.WhereClause;

                SQLHelper.DoSqlStatement(dbContext, sql);
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// inserts  the data into the ContentRules Table.
        /// </summary>
        /// <param name="objCmd"></param>
        /// <returns>PrimaryKey</returns>

        protected override PrimaryKey internalInsert(SqlCommand objCmd)
        {
            string sql = "Insert into ContentRules(RuleName,ClassName,Abbreviation,DisplayName) values(@RuleName,@ClassName,@Abbreviation,@DisplayName)";
            objCmd.CommandType = CommandType.Text;
            objCmd.CommandText = sql;
            SqlParameter param = null;

            param = objCmd.Parameters.Add("@RuleName", SqlDbType.NVarChar);
            param.Value = BasicConverter.StringToDbValue(this.m_RuleName);

            param = objCmd.Parameters.Add("@ClassName", SqlDbType.NVarChar);
            param.Value = BasicConverter.StringToDbValue(this.m_ClassName);

            param = objCmd.Parameters.Add("@Abbreviation", SqlDbType.NChar);
            param.Value = BasicConverter.CharToDbValue(this.m_Abbreviation);

            param = objCmd.Parameters.Add("@DisplayName", SqlDbType.NVarChar);
            param.Value = BasicConverter.StringToDbValue(this.m_DisplayName);

            objCmd.ExecuteNonQuery();

            SqlDataReader reader = null;
            objCmd.CommandText = "Select @@IDENTITY";
            try
            {
                reader = objCmd.ExecuteReader();
                reader.Read();
                m_ContentRulesPK = new ContentRulesPK(Convert.ToInt32(reader[0].ToString()));
            }
            finally
            {
                DBContext.CloseReader(reader);
            }
            return m_ContentRulesPK;
        }


        /// <summary>
        /// fetches  the data from the ContentRules Table.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="objCmd"></param>
        /// <returns>bool</returns>
        protected override bool internalLoad(PrimaryKey key, SqlCommand objCmd)
        {
            SqlDataReader reader = null;
            bool retValue = false;
            ContentRulesPK pk = (ContentRulesPK)key;
            objCmd.CommandText = "select * from ContentRules where " + pk.WhereClause;
            try
            {
                objCmd.CommandType = CommandType.Text;
                reader = objCmd.ExecuteReader();
                if (reader.Read())
                {
                    this.internalOnDataBind(reader);
                    retValue = true;
                }
                return retValue;
            }
            finally
            {
                DBContext.CloseReader(reader);
            }
        }

        /// <summary>
        /// updates  the data in ContentRules Table.
        /// </summary>
        /// <param name="objCmd"></param>

        protected override void internalUpdate(SqlCommand objCmd)
        {
            string sql = "Update ContentRules set RuleName=@RuleName,ClassName=@ClassName,Abbreviation=@Abbreviation,DisplayName=@DisplayName";
            objCmd.CommandType = CommandType.Text;
            objCmd.CommandText = sql;
            SqlParameter param = null;

            param = objCmd.Parameters.Add("@RuleName", SqlDbType.NVarChar);
            param.Value = BasicConverter.StringToDbValue(this.m_RuleName);

            param = objCmd.Parameters.Add("@ClassName", SqlDbType.NVarChar);
            param.Value = BasicConverter.StringToDbValue(this.m_ClassName);

            param = objCmd.Parameters.Add("@Abbreviation", SqlDbType.NChar);
            param.Value = BasicConverter.CharToDbValue(this.m_Abbreviation);

            param = objCmd.Parameters.Add("@DisplayName", SqlDbType.NVarChar);
            param.Value = BasicConverter.StringToDbValue(this.m_DisplayName);

            objCmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Gets ContentRules data for given ContentRuleID
        /// </summary>
        /// <param name="ContentRulesID"></param>
        /// <returns>ContentRules</returns>
        public static ContentRules FindByContentRulesID(int ContentRulesID)
        {

            SqlDataReader reader = null;
            DBContext dbContext = null;
            ContentRules objContentRules = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                string sql = "select * from ContentRules where ContentRulesID= " + ContentRulesID;


                reader = SQLHelper.DoSql(dbContext, sql);

                if (reader.Read())
                {
                    objContentRules = new ContentRules();
                    objContentRules.DataBind(reader);
                }

            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    dbContext.ReleaseDBContext(true);
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objContentRules;
        }

        /// <summary>
        /// Gets all ContentRules data
        /// </summary>
        /// <returns>List<ContentRules></returns>
        public static List<ContentRules> GetAllContentRules(int LanguageID)
        {
            SqlDataReader reader = null;
            DBContext dbContext = null;
            List<ContentRules> objContentRulesList = new List<ContentRules>();

            try
            {
                dbContext = DBContext.GetDBContext();
                string sql = "select * from ContentRules where LanguageID=" + LanguageID;



                reader = SQLHelper.DoSql(dbContext, sql);


                while (reader.Read())
                {
                    ContentRules objContentRules = new ContentRules();
                    objContentRules.DataBind(reader);
                    objContentRulesList.Add(objContentRules);
                }
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    dbContext.ReleaseDBContext(true);
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objContentRulesList;
        }

    }
}
