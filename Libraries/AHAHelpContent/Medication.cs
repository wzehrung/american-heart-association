﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class Medication
    {

        /// <summary>
        /// Creates / updates Medication data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="MedicationName"></param>
        /// <param name="MedicationType"></param>
        /// <param name="Dosage"></param>
        /// <param name="Frequency"></param>
        /// <param name="DateDiscontinued"></param>
        /// <param name="Notes"></param>

        public static void CreateOrChangeMedication(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, string MedicationName, string MedicationType, string Dosage, string strStrength, string Frequency, DateTime? DateStarted, DateTime? DateDiscontinued, string Notes)
        {

            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@MedicationName", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(MedicationName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@MedicationType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(MedicationType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Dosage", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(Dosage);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Strength", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strStrength);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Frequency", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(Frequency);


                if (DateStarted.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                    param.Value = BasicConverter.DateToDbValue(DateStarted.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                    param.Value = DBNull.Value;
                }
                if (DateDiscontinued.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DateDiscontinued", SqlDbType.DateTime);
                    param.Value = BasicConverter.DateToDbValue(DateDiscontinued.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DateDiscontinued", SqlDbType.DateTime);
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@Notes", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(Notes);



                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeMedication";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }

            return;
        }

        /// <summary>
        /// Deletes Medication data for the given ids
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropMedication(Guid HVItemGUID)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGUID);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropMedication";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;

        }
    }
}
