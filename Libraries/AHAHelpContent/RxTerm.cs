﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class RxTerm
    {
        public string Term
        {
            get;
            set;
        }

        public static void UpdateRxTerms(string strRxTermXml)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@RxTermList", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strRxTermXml);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateRxTerms]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        static bool bIsLoaded = false;
        static object objLock = new object();
        static DataTable m_DataTable = new DataTable();

        private static void _LoadData()
        {
            if (!bIsLoaded)
            {
                lock (objLock)
                {
                    if (!bIsLoaded)
                    {
                        _DoLoad();
                        bIsLoaded = true;
                    }
                }
            }
        }

        private static void _DoLoad()
        {
            DBContext dbContext = null;
            SqlDataReader reader = null;
            DataTable dataTable = new DataTable();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                dataTable.Columns.Add("Term", typeof(string));

                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                dbContext.ContextSqlCommand.CommandText = "[Proc_GetAllRxTerms]";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    DataRow r = dataTable.NewRow();
                    r["Term"] = reader["RxTerm"].ToString();
                    dataTable.Rows.Add(r);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            m_DataTable = dataTable;
        }

        public static DataTable GetAllRxTerms()
        {
            _LoadData();

            return m_DataTable;
        }
    }
}
