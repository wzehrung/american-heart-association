﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    [Serializable]
    public class AdminRoles
    {

        public int RoleID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
        public static AdminRoles GetAdminRoleByID(int intUserID)
        {
            DBContext dbContext = null;
            AdminRoles objAdminRoles = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAdminRoleByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intUserID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAdminRoles = new AdminRoles();
                    objAdminRoles.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminRoles;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.RoleID = BasicConverter.DbToIntValue(reader["RoleID"]);
            this.Name = BasicConverter.DbToStringValue(reader["Name"]);
            this.Description = BasicConverter.DbToStringValue(reader["Description"]);
        }

        public static List<AdminRoles> GetAllAdminRoles()
        {
            DBContext dbContext = null;
            List<AdminRoles> objAdminRolesList = new List<AdminRoles>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllAdminRoles]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminRoles objAdminRoles = new AdminRoles();
                    objAdminRoles.OnDataBind(reader);
                    objAdminRolesList.Add(objAdminRoles);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminRolesList;
        }

    }
}
