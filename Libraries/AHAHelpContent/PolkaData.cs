﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class PolkaData
    {
        public static void Create(Guid gPersonalItemGUID, string strXmlPolkaData)
        {
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Polka_CreateData]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@xmlPolkaData", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strXmlPolkaData);


                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void MarkPolkaDataAsUploadedToHV(Guid gPersonalItemGUID, string strXmlPolkaData)
        {
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Polka_MarkPolkaDataAsUploadedToHV]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@xmlPolkaData", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strXmlPolkaData);


                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static DateTime? GetLastHVUploadedDate(Guid gPersonalItemGUID)
        {

            DBContext dbContext = null;
            SqlDataReader reader = null;
            DateTime? retval = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);


                dbContext.ContextSqlCommand.CommandText = "[Proc_Polka_GetLastHVUploadedDateForUser]";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["LastHVUploadedDate"] != DBNull.Value)
                        retval = Convert.ToDateTime(reader["LastHVUploadedDate"]);
                }
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    dbContext.ReleaseDBContext(true);
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return retval;
        }

        public static string GetPolkaDataForUploadForPatient(Guid gPersonalItemGUID)
        {

            DBContext dbContext = null;
            SqlDataReader reader = null;
            string retval = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);


                dbContext.ContextSqlCommand.CommandText = "[Proc_Polka_GetPolkaDataForUploadForPatient]";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    retval = BasicConverter.DbToStringValue(reader["xmlPolkaData"]);                        
                }
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    dbContext.ReleaseDBContext(true);
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return retval;
        }
    }
}
