﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    // WE need to be able to do some client-side logic based upon Admin User Role.
    // These constants should be kept in synch with the 
    public enum AdminUserRole
    {
        SuperAdmin = 1,
        Admin = 2,
        Director = 3,
        BusinessAdmin = 4
    } ;


    [Serializable]
    public class AdminUser
    {
        public int AdminUserID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public int RoleID
        {
            get;
            set;
        }

        public string RoleName
        {
            get;
            set;
        }

        public string RoleTypeIDs
        {
            get;
            set;
        }

        public int BusinessID
        {
            get;
            set;
        }

        public string BusinessName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DefaultLanguageID
        /// </summary>
        public int DefaultLanguageID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LanguageLocale
        /// </summary>
        public string LanguageLocale
        {
            get;
            set;
        }
        
        private void OnDataBind(SqlDataReader reader)
        {
            this.AdminUserID = BasicConverter.DbToIntValue(reader["AdminUserID"]);
            this.Email = BasicConverter.DbToStringValue(reader["Email"]);
            this.IsActive = BasicConverter.DbToBoolValue(reader["IsActive"]);
            this.RoleID = BasicConverter.DbToIntValue(reader["RoleID"]);
            this.RoleName = BasicConverter.DbToStringValue(reader["RoleName"]);
            this.Name = BasicConverter.DbToStringValue(reader["Name"]);
            this.Password = BasicConverter.DbToStringValue(reader["Password"]);
            this.UserName = BasicConverter.DbToStringValue(reader["UserName"]);

            this.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
            this.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

            using (DataTable dt = reader.GetSchemaTable())
            {
                if (Misc.DoesColumnExist(dt, "LanguageLocale"))
                {
                    this.LanguageLocale = BasicConverter.DbToStringValue(reader["LanguageLocale"]);
                }
                if (Misc.DoesColumnExist(dt, "DefaultLanguageID"))
                {
                    this.DefaultLanguageID = BasicConverter.DbToIntValue(reader["DefaultLanguageID"]);
                }
            }
        }

        public static AdminUser CreateAdminUser(string strName, string strUserName, string strPassword, string strEmail, int intRoleID, int intBusinessID, int iLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            int intAdminUserID;
            AdminUser objAdminUser = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Name", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserName", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strUserName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Password", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strPassword);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Email", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RoleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intRoleID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intBusinessID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateAdminUser]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@AdminUserID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                intAdminUserID = Convert.ToInt32(param_output.Value);
                objAdminUser = new AdminUser
                {
                    Email = strEmail,
                    UserName = strUserName,
                    Password = strPassword,
                    RoleID = intRoleID,
                    BusinessID = intBusinessID,
                    Name = strName
                };

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objAdminUser;
        }

        public static void UpdateAdminUser(int intAdminUserID, string strName, string strEmail, int intRoleID, int intBusinessID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intAdminUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Name", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Email", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RoleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intRoleID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intBusinessID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateAdminUser]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static AdminUser GetAdminUserByID(int intUserID)
        {
            DBContext dbContext = null;
            AdminUser objAdminUser = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAdminUserByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intUserID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAdminUser = new AdminUser();
                    objAdminUser.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminUser;
        }

        public static AdminUser GetAdminUserByUserName(string strUserName)
        {
            DBContext dbContext = null;
            AdminUser objAdminUser = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAdminUserByUserName]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserName", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strUserName);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAdminUser = new AdminUser();
                    objAdminUser.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminUser;
        }

        public static List<AdminUser> GetAllAdminUsers()
        {
            DBContext dbContext = null;
            List<AdminUser> objAdminUserList = new List<AdminUser>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllAdminUsers]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminUser objAdminUser = new AdminUser();
                    objAdminUser.OnDataBind(reader);
                    objAdminUserList.Add(objAdminUser);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminUserList;
        }

        public static List<AdminUser> GetAllActiveAdminUsers()
        {
            DBContext dbContext = null;
            List<AdminUser> objAdminUserList = new List<AdminUser>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllActiveAdminUsers]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminUser objAdminUser = new AdminUser();
                    objAdminUser.OnDataBind(reader);
                    objAdminUserList.Add(objAdminUser);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminUserList;
        }

        // WZ: This is used by the content expiration process to get only the given role types
        // Before it was getting *all* active admin users to send notifications to
        public static List<AdminUser> GetAllActiveAdminUsersByRoleTypeID(string sRoleTypeIDs)
        {
            DBContext dbContext = null;
            List<AdminUser> objAdminUserList = new List<AdminUser>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                string sql = "[Proc_Admin_GetAllActiveAdminUsersByRoleTypeID]";

                param = dbContext.ContextSqlCommand.Parameters.Add("@RoleTypeIDs", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sRoleTypeIDs);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminUser objAdminUser = new AdminUser();
                    objAdminUser.OnDataBind(reader);
                    objAdminUserList.Add(objAdminUser);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminUserList;
        }


        public static bool IsValidAdminUser(string strUserName, string strPassword)
        {
            DBContext dbContext = null;
            bool bException = false;
            bool isValidAdminUser;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserName", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strUserName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Password", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strPassword);

                dbContext.ContextSqlCommand.CommandText = "[Proc_IsValidAdminUser]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@IsValidAdminUser", SqlDbType.Bit));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                isValidAdminUser = Convert.ToBoolean(param_output.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return isValidAdminUser;
        }

        public static void ChangePassword(int intAdminUserID, string strPassword)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intAdminUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Password", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strPassword);

                dbContext.ContextSqlCommand.CommandText = "[Proc_ChangePassword]";
                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DisableAdminUser(int intAdminUserID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intAdminUserID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DisableAdminUser]";
                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void EnableAdminUser(int intAdminUserID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intAdminUserID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_EnableAdminUser]";
                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static AdminUser GetDefaultLanguageIDForAdminUser(int intUserID)
        {
            DBContext dbContext = null;
            AdminUser objAdminUser = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                
                string sql = "select * from AdminUser where AdminUserID="+intUserID;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;

               


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAdminUser = new AdminUser();
                    objAdminUser.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminUser;
        }

        public static void UpdateDefaultLanguage(int iAdminUserID, int iLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iAdminUserID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateLanguage_AdminUser]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
    }
}
