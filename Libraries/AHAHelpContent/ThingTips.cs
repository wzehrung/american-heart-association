﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class ThingTips
    {
        /// <summary>
        /// Gets ContentID
        /// </summary>
        public int ThingTipsID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets URL
        /// </summary>
        /// <summary>
        public string Content
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets URL
        /// </summary>
        /// <summary>
        public string ThingTipTypes
        {
            get;
            set;
        }


        /// Gets tooltip for the day for given ThingTipsType
        /// </summary>
        /// <param name="strThingTipsType"></param>
        /// <returns>string</returns>
        public static string GetTipForTheDay(string strThingTipsType, int intLanguageID)
        {

            SqlDataReader reader = null;
            string strTipForTheDay = string.Empty;
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;



                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                dbContext.ContextSqlCommand.CommandText = "Proc_FetchThingTiptypeFortheDay";

                param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strThingTipsType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);


                reader = dbContext.ContextSqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    strTipForTheDay = BasicConverter.DbToStringValue(reader["ThingTipsData"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return strTipForTheDay;
        }

        /// <summary>
        /// Gets All contents
        /// </summary>
        /// <param name="strAbbrev"></param>
        /// <returns>List<Content></returns>
        public static List<ThingTips> GetAllThingTips(int LanguageID)
        {
            DBContext dbContext = null;
            SqlParameter param = null;
            List<ThingTips> listThingTips = new List<ThingTips>();
            SqlDataReader reader = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_GetAllThingTips]";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                

                while (reader.Read())
                {
                    ThingTips objThingTips = new ThingTips();
                    objThingTips.ThingTipsID = Convert.ToInt32(reader["ThingTipsId"]);
                    objThingTips.Content = reader["ThingTipsData"].ToString();
                    objThingTips.ThingTipTypes = reader["ThingTipsTypeIDs"].ToString();
                    listThingTips.Add(objThingTips);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return listThingTips;
        }

        public static ThingTips CreateThingTip(string strContent, List<string> thingtiptypeids, int CreatedByUserID, int UpdatedByUserID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            ThingTips objThingTips = null;
            int iThingTipID=0;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                //param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(iThingTipID);


                param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsData", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strContent);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsTypeIds", SqlDbType.NVarChar);
                if (thingtiptypeids != null)
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", thingtiptypeids.ConvertAll<string>(i => i.ToString()).ToArray()));
                }
                else
                {
                    param.Value = DBNull.Value;
                }

               // param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(CreatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_CreateThingTips]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeThingTips]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ThingTipsID", SqlDbType.Int));
                param_output.Value = BasicConverter.IntToDbValue(iThingTipID);
                param_output.Direction = ParameterDirection.Output;


                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iThingTipID = Convert.ToInt32(param_output.Value);

                objThingTips = new ThingTips
                {
                    Content = strContent,
                    ThingTipsID = iThingTipID
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objThingTips;
        }

        public static void UpdateThingTip(string content, List<string> thingtiptypeids, int iThingTipID, int UpdatedByUserID,int CreatedByUserID, int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsData", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(content);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsTypeIds", SqlDbType.NVarChar);
                if (thingtiptypeids != null)
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", thingtiptypeids.ConvertAll<string>(i => i.ToString()).ToArray()));
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                //param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(iThingTipID);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ThingTipsID", SqlDbType.Int));
                param_output.Value = BasicConverter.IntToDbValue(iThingTipID);

               // param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
               // param.Value = BasicConverter.IntToDbValue(CreatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_ChangeThingTips]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeThingTips]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteThingTip(int iThingTipID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsid", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iThingTipID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteThingTips]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static ThingTips FindByThingTipID(int iThingTipID,int Languageid)
        {
            DBContext dbContext = null;
            ThingTips objThingTip = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetThingTipsByThingTipsID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ThingTipsid", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iThingTipID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(Languageid);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objThingTip = new ThingTips();
                    objThingTip.ThingTipsID = Convert.ToInt32(reader["ThingTipsId"]);
                    objThingTip.Content = reader["ThingTipsData"].ToString();
                    objThingTip.ThingTipTypes = reader["ThingTipsTypeIDs"].ToString();
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objThingTip;
        }
    }
}
