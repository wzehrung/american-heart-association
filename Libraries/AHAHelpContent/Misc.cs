﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using GRCBase;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class Misc
    {
        public static bool DoesColumnExist(DataTable dt, string columnName)
        {
            dt.DefaultView.RowFilter = "ColumnName= '" + columnName + "'";
            return (dt.DefaultView.Count > 0);
        }

        #region Non-Campaigns

        internal static DataSet GetReportPatients(string spName, DateTime dtStart, DateTime dtEnd, int? iPartnerID)
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iPartnerID);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        internal static DataSet GetReport(string spName, DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Markets", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sMarkets);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserTypeID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iUserTypeID);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        internal static DataSet GetReport(string spName, DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets )
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Markets", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sMarkets);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        internal static DataSet GetReport(string spName, DateTime dtStart, DateTime dtEnd)
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        internal static DataSet GetReport(string spName, string paramName, string listValues)
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add(paramName, SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(listValues);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        internal static DataSet GetReportById(string spName, string paramName, int? paramValue )
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add(paramName, SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(paramValue);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        #endregion

        #region Campaigns

        internal static DataSet GetReportCampaign(string spName, DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets )
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int );
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Markets", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sMarkets);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        internal static DataSet GetReportCampaign(string spName, DateTime dtStart, DateTime dtEnd, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Markets", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sMarkets);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserTypeID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iUserTypeID);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        internal static DataSet GetReportCampaign(string spName, DateTime dtStart, DateTime dtEnd, string sCampaignIDs, string sMarkets)
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            bool bException = false;
            DataSet ds = new DataSet();

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStart);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEnd);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignIDs", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sCampaignIDs);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Markets", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(sMarkets);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return ds;
        }

        #endregion

        public static bool SyncData(XDocument xDoc, bool bUpdateLastSyncDate, string phoneNumber, string zipCode)
        {
            DBContext dbContext = null;
            bool bException = false;
            bool bRetVal = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@SyncDataXml", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(xDoc.ToString());

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdateLastSyncDate", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bUpdateLastSyncDate);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@isSynched", SqlDbType.Bit));
                param_output.Direction = ParameterDirection.Output;

                param = dbContext.ContextSqlCommand.Parameters.Add("@phonenumber", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(phoneNumber);

                param = dbContext.ContextSqlCommand.Parameters.Add("@zipcode", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(zipCode);

                dbContext.ContextSqlCommand.CommandText = "[Proc_SyncData]";
                dbContext.ContextSqlCommand.ExecuteNonQuery();

                bRetVal =  BasicConverter.DbToBoolValue(param_output.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return bRetVal;
        }
    }
}
