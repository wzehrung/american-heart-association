﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class ListItem
    {
        /// <summary>
        /// Gets ListItemID
        /// </summary>
        public int ListItemID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListItemName
        /// </summary>
        public string ListItemName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ListItemName
        /// </summary>
        public string ListItemCode
        {
            get;
            set;
        }


        /// <summary>
        /// Binds the data from the database to the corresponding variables
        /// </summary>
        /// <param name="objSource"></param>
        /// <returns></returns>
        private void OnDataBind(SqlDataReader reader)
        {
            this.ListItemID = BasicConverter.DbToIntValue(reader["ListItemID"]);
            this.ListItemName = BasicConverter.DbToStringValue(reader["ListItemName"]);
            this.ListItemCode = BasicConverter.DbToStringValue(reader["ListItemCode"]);
            if (this.ListItemCode == null)
            {
                this.ListItemCode = this.ListItemName;
            }
            return;
        }


        


        /// <summary>
        /// Get all ListItems for the given ListCode
        /// </summary>
        /// <param name="strListCode"></param>
        /// <returns>List<ListItems></returns>
        public static List<ListItem> FindListItemsByListCode(string strListCode, int intLanguageID)
        {
            SqlDataReader reader = null;
            DBContext dbContext = null;
            SqlCommand cmd = null;
            List<ListItem> objList = new List<ListItem>();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                string sql = "Proc_FetchListItems";
                SqlParameter param = null;


                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ListCode", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strListCode);


                param = cmd.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ListItem objListItem = new ListItem();
                    objListItem.OnDataBind(reader);
                    objList.Add(objListItem);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        /// <summary>
        /// Get  ListItem for the given ListItemCode
        /// </summary>
        /// <param name="strListItemCode"></param>
        /// <returns>string</returns>
        public static string FindListItemNameByListItemCode(string strListItemCode, string strListCode, int intLanguageID)
        {
            DBContext dbContext = null;
            SqlCommand cmd = null;
            string strListItemName = string.Empty;

            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                string sql = "Proc_FetchListItemByListItemCode";
                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ListItemCode", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strListItemCode);

                param = cmd.Parameters.Add("@ListCode", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strListCode);

                param = cmd.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                object obj = cmd.ExecuteScalar();

                if (obj != null)
                {
                    strListItemName = obj.ToString();
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return strListItemName;
        }

        public static ListItem FindByListItemID(int iListItemID, int intLanguageID)
        {
            DBContext dbContext = null;
            ListItem objListItem = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FetchListItemByListItemID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ListItemID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iListItemID);
                
                param = cmd.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objListItem = new ListItem();
                    objListItem.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objListItem;
        }


        public static List<ListItem> GetPartners()
        {
            DBContext dbContext = null;
            ListItem objListItem = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            List<ListItem> objList = new List<ListItem>();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetPartners]";
                
                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objListItem = new ListItem();
                    objListItem.OnDataBind(reader);
                    objList.Add(objListItem);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }


        public static List<ListItem> GetRegionAffiliate()
        {
            DBContext dbContext = null;
            ListItem objListItem = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            List<ListItem> objList = new List<ListItem>();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetRegionAffiliate]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objListItem = new ListItem();
                    objListItem.OnDataBind(reader);
                    objList.Add(objListItem);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

    }    
}
