﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class ExerciseGoal
    {
        /// <summary>
        /// Creates or updates ExerciseGoal data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="Totalcholesterol"></param>
        /// <param name="LDL"></param>
        /// <param name="HDL"></param>
        /// <param name="Triglyceride"></param>
        public static void CreateOrChangeExerciseGoal(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, string Intensity, double Duration)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Intensity", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(Intensity);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Duration", SqlDbType.Float);
                param.Value = BasicConverter.DoubleToDbValue(Duration);



                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeExerciseGoal]";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }
    }
}
