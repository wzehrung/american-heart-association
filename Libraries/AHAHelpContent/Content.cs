﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    /// <summary>
    /// Primary Key
    /// </summary>
    public class Content
    {

        private int m_ContentID;
        /// <summary>
        /// Gets ContentID
        /// </summary>
        public int ContentID
        {
            get
            {
                return m_ContentID;
            }
            set
            {
                m_ContentID = value;
            }
        }

        private string m_Url;
        /// <summary>
        /// Gets or sets URL
        /// </summary>
        public string URL
        {
            get
            {
                return m_Url;
            }
            set
            {
                m_Url = value;
            }
        }

        private string m_Title;
        /// <summary>
        /// Gets or sets Title
        /// </summary>
        public string Title
        {
            get
            {
                return m_Title;
            }
            set
            {
                m_Title = value;
            }
        }

        private string m_DidYouKnowLinkTitle;
        /// <summary>
        /// Gets or sets DidYouKnowLinkTitle
        /// </summary>
        public string DidYouKnowLinkTitle
        {
            get
            {
                return m_DidYouKnowLinkTitle;
            }
            set
            {
                m_DidYouKnowLinkTitle = value;
            }
        }

        private string m_DidYouKnowText;
        /// <summary>
        /// Gets or sets DidYouKnowText
        /// </summary>
        public string DidYouKnowText
        {
            get
            {
                return m_DidYouKnowText;
            }
            set
            {
                m_DidYouKnowText = value;
            }
        }

        private string m_ContentRuleNames;
        /// <summary>
        /// Gets or sets DidYouKnowText
        /// </summary>
        public string ContentRuleNames
        {
            get
            {
                return m_ContentRuleNames;
            }
            set
            {
                m_ContentRuleNames = value;
            }
        }

        public DateTime? ExpirationDate
        {
            get;
            set;
        }

        public int? CreatedByUserID
        {
            get;
            set;
        }

        public int? UpdatedByUserID
        {
            get;
            set;
        }



        /// <summary>
        /// Gets Content data for given comma seperated Abbreviation string
        /// </summary>
        /// <param name="strAbbrev"></param>
        /// <returns>List<Content></returns>
        public static List<Content> GetContentInformation(string strAbbrev,int LanguageID)
        {
            DBContext dbContext = null;
            List<Content> listContent = new List<Content>();
            SqlDataReader reader = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AbbreviationString", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strAbbrev);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "Proc_FetchContentinformation";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    Content objContent = new Content();
                    objContent.URL = reader["DidYouKnowLinkUrl"].ToString();
                    objContent.Title = reader["Title"].ToString();
                    objContent.DidYouKnowText = reader["DidYouKnowText"].ToString();
                    objContent.DidYouKnowLinkTitle = reader["DidYouKnowLinkTitle"].ToString();
                    listContent.Add(objContent);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return listContent;
        }




        /// <summary>
        /// Gets Content data for given comma seperated Abbreviation string
        /// </summary>
        /// <param name="strAbbrev"></param>
        /// <returns>List<Content></returns>
        public static List<Content> GetContentInformationForSpecificType(string strAbbrev, int iLanguageID)
        {
            DBContext dbContext = null;
            List<Content> listContent = new List<Content>();
            SqlDataReader reader = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Abbreviation", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strAbbrev);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                dbContext.ContextSqlCommand.CommandText = "Proc_FetchContentinformationForSpecificAbbreviation";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    Content objContent = new Content();
                    objContent.URL = reader["DidYouKnowLinkUrl"].ToString();
                    objContent.Title = reader["Title"].ToString();
                    objContent.DidYouKnowText = reader["DidYouKnowText"].ToString();
                    objContent.DidYouKnowLinkTitle = reader["DidYouKnowLinkTitle"].ToString();
                    listContent.Add(objContent);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return listContent;
        }

        /// <summary>
        /// Gets All contents
        /// </summary>
        /// <param name="strAbbrev"></param>
        /// <returns>List<Content></returns>
        public static List<Content> GetAllContents(int LanguageID)
        {
            DBContext dbContext = null;
            List<Content> listContent = new List<Content>();
            SqlDataReader reader = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_GetAllContents]";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    Content objContent = new Content();
                    objContent.ContentID = Convert.ToInt32(reader["ContentID"]);
                    objContent.URL = reader["DidYouKnowLinkUrl"].ToString();
                    objContent.Title = reader["Title"].ToString();
                    objContent.DidYouKnowText = reader["DidYouKnowText"].ToString();
                    objContent.DidYouKnowLinkTitle = reader["DidYouKnowLinkTitle"].ToString();
                    objContent.ContentRuleNames = reader["ContentRulesIDs"].ToString();
                    if (reader["ExpirationDate"]!=DBNull.Value)
                    {
                        objContent.ExpirationDate = Convert.ToDateTime(reader["ExpirationDate"]);
                    }
                    else
                    {
                        objContent.ExpirationDate = null;
                    }

                    if (reader["CreatedByUserID"] != DBNull.Value)
                    {
                        objContent.CreatedByUserID = Convert.ToInt32(reader["CreatedByUserID"]);
                    }
                    else
                    {
                        objContent.CreatedByUserID = null;
                    }

                    if (reader["UpdatedByUserID"] != DBNull.Value)
                    {
                        objContent.UpdatedByUserID = Convert.ToInt32(reader["UpdatedByUserID"]);
                    }
                    else
                    {
                        objContent.UpdatedByUserID = null;
                    }

                    listContent.Add(objContent);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return listContent;
        }

        /// <summary>
        /// Gets All contents
        /// </summary>
        /// <param name="strAbbrev"></param>
        /// <returns>List<Content></returns>
        public static List<Content> GetContentsExpiringOn(DateTime expiringDate)
        {
            DBContext dbContext = null;
            List<Content> listContent = new List<Content>();
            SqlDataReader reader = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                
                dbContext.ContextSqlCommand.CommandText = "[Proc_GetContentsExpiringOn]";
                SqlParameter param = null;

                

                param = dbContext.ContextSqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(expiringDate);
               

                reader = dbContext.ContextSqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    Content objContent = new Content();
                    objContent.ContentID = Convert.ToInt32(reader["ContentID"]);
                    objContent.URL = reader["DidYouKnowLinkUrl"].ToString();
                    objContent.Title = reader["Title"].ToString();
                    objContent.DidYouKnowText = reader["DidYouKnowText"].ToString();
                    objContent.DidYouKnowLinkTitle = reader["DidYouKnowLinkTitle"].ToString();

                    if (reader["ExpirationDate"] != DBNull.Value)
                    {
                        objContent.ExpirationDate = Convert.ToDateTime(reader["ExpirationDate"]);
                    }
                    else
                    {
                        objContent.ExpirationDate = null;
                    }

                    if (reader["CreatedByUserID"] != DBNull.Value)
                    {
                        objContent.CreatedByUserID = Convert.ToInt32(reader["CreatedByUserID"]);
                    }
                    else
                    {
                        objContent.CreatedByUserID = null;
                    }

                    if (reader["UpdatedByUserID"] != DBNull.Value)
                    {
                        objContent.UpdatedByUserID = Convert.ToInt32(reader["UpdatedByUserID"]);
                    }
                    else
                    {
                        objContent.UpdatedByUserID = null;
                    }

                    listContent.Add(objContent);
                }
            }
            catch
            {
                bException = true;
                throw; 
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return listContent;
        }


        public static Content CreateContent(string strTitle, string strDidYouKnowLinkUrl, string strDidYouKnowLinkTitle, string strDidYouKnowText, List<string> lstContentRuleIds, DateTime? dtExpirationDate, int? intCreatedByUserID, int? intUpdatedByUserID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            Content objContent = null;
            int iContentID = 0;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DidYouKnowLinkUrl", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDidYouKnowLinkUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DidYouKnowLinkTitle", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDidYouKnowLinkTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DidYouKnowText", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDidYouKnowText);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                param.Value = BasicConverter.NullableDateToDbValue(dtExpirationDate);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                //param.Value = BasicConverter.NullableIntToDbValue(intCreatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(intUpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(LanguageID);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@ContentID", SqlDbType.Int);
                //param.Value = BasicConverter.NullableIntToDbValue(ContentID);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ContentID", SqlDbType.Int));
                param_output.Value = BasicConverter.IntToDbValue(iContentID);
                param_output.Direction = ParameterDirection.Output;


                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentRuleIDs", SqlDbType.NVarChar);
                if (lstContentRuleIds != null)
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", lstContentRuleIds.ConvertAll<string>(i => i.ToString()).ToArray()));
                }
                else
                {
                    param.Value = DBNull.Value;
                }


                //dbContext.ContextSqlCommand.CommandText = "[Proc_CreateContent]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeContent]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iContentID = Convert.ToInt32(param_output.Value);
                objContent = new Content
                {
                    Title = strTitle,
                    URL = strDidYouKnowLinkUrl,
                    DidYouKnowText = strDidYouKnowText,
                    DidYouKnowLinkTitle = strDidYouKnowLinkTitle,
                    ContentID = iContentID
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objContent;
        }

        public static void UpdateContent(string strTitle, string strDidYouKnowLinkUrl, string strDidYouKnowLinkTitle, string strDidYouKnowText, int iContentID, List<string> lstContentRuleIds, DateTime? ExpirationDate, int? UpdatedByUserID, int? CreatedByUserID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DidYouKnowLinkUrl", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDidYouKnowLinkUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DidYouKnowLinkTitle", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDidYouKnowLinkTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DidYouKnowText", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDidYouKnowText);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iContentID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                param.Value = BasicConverter.NullableDateToDbValue(ExpirationDate);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                //param.Value = BasicConverter.NullableIntToDbValue(CreatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(LanguageID);


                

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentRuleIDs", SqlDbType.NVarChar);
                if (lstContentRuleIds != null)
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", lstContentRuleIds.ConvertAll<string>(i => i.ToString()).ToArray()));
                }
                else
                {
                    param.Value = DBNull.Value;
                }


                //dbContext.ContextSqlCommand.CommandText = "[Proc_ChangeContent]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeContent]";
                

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteContent(int iContentID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iContentID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteContent]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static Content FindByContentID(int iContentID,int LanguageID)
        {
            DBContext dbContext = null;
            Content objContent = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetContentByContentID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iContentID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objContent = new Content();
                    objContent.ContentID = Convert.ToInt32(reader["ContentID"]);
                    objContent.URL = reader["DidYouKnowLinkUrl"].ToString();
                    objContent.Title = reader["Title"].ToString();
                    objContent.DidYouKnowText = reader["DidYouKnowText"].ToString();
                    objContent.DidYouKnowLinkTitle = reader["DidYouKnowLinkTitle"].ToString();
                    objContent.ContentRuleNames = reader["ContentRulesIDs"].ToString();
                    if (reader["ExpirationDate"] != DBNull.Value)
                    {
                        objContent.ExpirationDate = Convert.ToDateTime(reader["ExpirationDate"]);
                    }
                    else
                    {
                        objContent.ExpirationDate = null;
                    }
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objContent;
        }
    }

}
