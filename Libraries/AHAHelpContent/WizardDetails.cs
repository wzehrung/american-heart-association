﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GRCBase;
using System.Data;

namespace AHAHelpContent
{
    public class WizardDetails
    {
        public int? WizardStepIndex
        {
            get;
            set;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            
            this.WizardStepIndex = BasicConverter.DbToNullableIntValue(reader["WizardStepIndex"]);
        }

        public static WizardDetails FindWizardDetailsForRecord(Guid gPersonalItemGUID)
        {
            DBContext dbContext = null;
            WizardDetails obj = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "Proc_GetWizardDetails";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    obj = new WizardDetails();
                    obj.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return obj;
        }

        public static void CreateOrChangeWizardDetails(Guid gPersonalItemGUID, int? iWizardStepIndex)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@WizardStepIndex", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iWizardStepIndex);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeWizardDetails]";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }
    }
}
