﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;
namespace AHAHelpContent
{
    public class ProviderTipsType
    {
        public int ProviderTipsTypeID
        {
            get;
            set;
        }

        public string strProviderTipsType
        {
            get;
            set;
        }

        public string DisplayName
        {
            get;
            set;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.ProviderTipsTypeID = BasicConverter.DbToIntValue(reader["ProviderTipsTypeID"]);
            this.strProviderTipsType = BasicConverter.DbToStringValue(reader["ProviderTipsType"]);
            this.DisplayName = BasicConverter.DbToStringValue(reader["DisplayName"]);
            return;
        }

        public static List<ProviderTipsType> GetAllProviderTipTypes(int LanguageId)
        {
            DBContext dbContext = null;
            List<ProviderTipsType> objProviderTipsTypeList = new List<ProviderTipsType>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageId);

                string sql = "[Proc_FindAllProviderTipsTypes]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    ProviderTipsType objProviderTipsType = new ProviderTipsType();
                    objProviderTipsType.OnDataBind(reader);
                    objProviderTipsTypeList.Add(objProviderTipsType);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderTipsTypeList;
        }
    }
}
