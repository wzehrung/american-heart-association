﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    [Serializable]
    public class AdminReport
    {
        public int ReportID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int CategoryID
        {
            get;
            set;
        }
        public int RoleID
        {
            get;
            set;
        }
        public bool Enabled
        {
            get;
            set;
        }

        public static List<AdminReport> GetAdminReportByID(int intUserID)
        {
            DBContext dbContext = null;
            List<AdminReport> objAdminReportList = new List<AdminReport>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAdminReportByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intUserID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminReport objAdminReport = new AdminReport();
                    objAdminReport.OnDataBind(reader);
                    objAdminReportList.Add(objAdminReport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminReportList;
        }

        public static List<AdminReport> GetAllAdminReport()
        {
            DBContext dbContext = null;
            List<AdminReport> objAdminReportList = new List<AdminReport>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetAllAdminReport]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminReport objAdminReport = new AdminReport();
                    objAdminReport.OnDataBind(reader);
                    objAdminReportList.Add(objAdminReport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminReportList;
        }

        public static List<AdminReport> GetAllAdminReportByCategoryID(int CategoryID)
        {
            DBContext dbContext = null;
            List<AdminReport> objAdminReportList = new List<AdminReport>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAllAdminReportByCategoryID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CategoryID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(CategoryID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminReport objAdminReport = new AdminReport();
                    objAdminReport.OnDataBind(reader);
                    objAdminReportList.Add(objAdminReport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminReportList;
        }

        public static List<AdminReport> GetAllAdminReportByCategoryIDByRoleID(int CategoryID, int RoleID)
        {
            DBContext dbContext = null;
            List<AdminReport> objAdminReportList = new List<AdminReport>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetAllAdminReportByCategoryIDByRoleID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CategoryID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(CategoryID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RoleID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(RoleID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AdminReport objAdminReport = new AdminReport();
                    objAdminReport.OnDataBind(reader);
                    objAdminReportList.Add(objAdminReport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAdminReportList;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.ReportID = BasicConverter.DbToIntValue(reader["ReportID"]);
            this.Name = BasicConverter.DbToStringValue(reader["Name"]);
            this.Description = BasicConverter.DbToStringValue(reader["Description"]);
            this.CategoryID = BasicConverter.DbToIntValue(reader["CategoryID"]);
            this.Enabled = BasicConverter.DbToBoolValue(reader["Enabled"]);
        }

        public static void SetAdminReportByRoleID(List<AdminReport> lstAdmReports)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {

                foreach (AdminReport admReport in lstAdmReports)
                {
                    dbContext = DBContext.GetDBContext();
                    dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@RoleID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(admReport.RoleID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReportID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(admReport.ReportID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@Enabled", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(admReport.Enabled);

                    dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateAdminReport]";
                    dbContext.ContextSqlCommand.ExecuteNonQuery();
                    dbContext.ReleaseDBContext(true);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

    }
}
