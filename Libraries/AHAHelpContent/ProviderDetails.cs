﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

using System.Text.RegularExpressions;

namespace AHAHelpContent
{
    public class ProviderDetails
    {
        #region Public Properties
        public int ProviderID
        {
            get;
            set;
        }

        public string FormattedName
        {
            get
            {
                if (!string.IsNullOrEmpty(Salutation))
                {
                    return string.Format("{0} {1} {2}", Salutation, FirstName, LastName);
                }
                else
                {
                    return string.Format("{0} {1}", FirstName, LastName);
                }
            }
        }

        public string Email
        {
            get;
            set;
        }

        public string Campaign
        {
            get;
            set;
        }
        public string Affiliate
        {
            get;
            set;
        }
        public string Market
        {
            get;
            set;
        }
        public string NumberOfConnections
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string Salutation
        {
            get;
            set;
        }

        public string PracticeName
        {
            get;
            set;
        }

        public string PracticeEmail
        {
            get;
            set;
        }

        public string PracticeAddress
        {
            get;
            set;
        }

        public string PracticePhone
        {
            get;
            set;
        }

        public string PracticeFax
        {
            get;
            set;
        }

        public string Speciality
        {
            get;
            set;
        }

        public string PharmacyPartner
        {
            get;
            set;
        }

        public DateTime DateOfRegistration
        {
            get;
            set;
        }

        public string ProfileMessage
        {
            get;
            set;
        }

        public string PracticeUrl
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public int StateID
        {
            get;
            set;
        }

        public string Zip
        {
            get;
            set;
        }

        public bool? AllowPatientsToSendMessages
        {
            get;
            set;
        }

        public int DismissAlertAfterWeeks
        {
            get;
            set;
        }

        public bool? IncludeAlertNotificationInDailySummary
        {
            get;
            set;
        }

        public bool? IncludeMessageNotificationInDailySummary
        {
            get;
            set;
        }

        public bool? NotifyNewAlertImmediately
        {
            get;
            set;
        }

        public bool? NotifyNewMessageImmediately
        {
            get;
            set;
        }

        public string EmailForDailySummary
        {
            get;
            set;
        }

        public string EmailForIndividualAlert
        {
            get;
            set;
        }

        public string EmailForIndividualMessage
        {
            get;
            set;
        }

        public string ProviderCode
        {
            get;
            set;
        }

        public bool? AllowAHAEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DefaultLanguageID
        /// </summary>
        public int DefaultLanguageID
        {
            get;
            set;
        }

        public string StateName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LanguageLocale
        /// </summary>
        public string LanguageLocale
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets UserTypeID
        /// </summary>
        public int UserTypeID
        {
            get;
            set;
        }
        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.ProviderID = BasicConverter.DbToIntValue(reader["ProviderID"]);
            this.UserName = BasicConverter.DbToStringValue(reader["UserName"]);
            this.Email = BasicConverter.DbToStringValue(reader["Email"]);
            this.Password = BasicConverter.DbToStringValue(reader["Password"]);
            this.FirstName = BasicConverter.DbToStringValue(reader["FirstName"]);
            this.LastName = BasicConverter.DbToStringValue(reader["LastName"]);
            this.Salutation = BasicConverter.DbToStringValue(reader["Salutation"]);
            this.PracticeName = BasicConverter.DbToStringValue(reader["PracticeName"]);
            this.PracticeEmail = BasicConverter.DbToStringValue(reader["PracticeEmail"]);
            this.PracticeAddress = BasicConverter.DbToStringValue(reader["PracticeAddress"]);
            this.PracticePhone = BasicConverter.DbToStringValue(reader["PracticePhone"]);
            this.PracticeFax = BasicConverter.DbToStringValue(reader["PracticeFax"]);
            this.Speciality = BasicConverter.DbToStringValue(reader["Speciality"]);
            this.PharmacyPartner = BasicConverter.DbToStringValue(reader["PharmacyPartner"]);
            this.ProfileMessage = BasicConverter.DbToStringValue(reader["ProfileMessage"]);
            this.PracticeUrl = BasicConverter.DbToStringValue(reader["PracticeUrl"]);
            this.City = BasicConverter.DbToStringValue(reader["City"]);
            this.StateID = BasicConverter.DbToIntValue(reader["StateID"]);
            this.Zip = BasicConverter.DbToStringValue(reader["Zip"]);

            if (reader.GetSchemaTable().Columns.Contains("campaignname"))
                this.Campaign = BasicConverter.DbToStringValue(reader["campaignname"]);
            else
                this.Campaign = string.Empty;
            if (reader.GetSchemaTable().Columns.Contains("market"))
                this.Market = BasicConverter.DbToStringValue(reader["market"]);
            else
                this.Market = string.Empty;
            if (reader.GetSchemaTable().Columns.Contains("affiliate"))
                this.Affiliate = BasicConverter.DbToStringValue(reader["affiliate"]);
            else
                this.Affiliate = string.Empty;

            this.AllowPatientsToSendMessages = BasicConverter.DbToNullableBoolValue(reader["AllowPatientsToSendMessages"]);
            this.IncludeAlertNotificationInDailySummary = BasicConverter.DbToNullableBoolValue(reader["IncludeAlertNotificationInDailySummary"]);
            this.IncludeMessageNotificationInDailySummary = BasicConverter.DbToNullableBoolValue(reader["IncludeMessageNotificationInDailySummary"]);
            this.NotifyNewAlertImmediately = BasicConverter.DbToNullableBoolValue(reader["NotifyNewAlertImmediately"]);
            this.NotifyNewMessageImmediately = BasicConverter.DbToNullableBoolValue(reader["NotifyNewMessageImmediately"]);
            this.DismissAlertAfterWeeks = BasicConverter.DbToIntValue(reader["DismissAlertAfterWeeks"]);
            this.DateOfRegistration = BasicConverter.DbToDateValue(reader["DateOfRegistration"]);
            this.EmailForDailySummary = BasicConverter.DbToStringValue(reader["EmailForDailySummary"]);
            this.EmailForIndividualAlert = BasicConverter.DbToStringValue(reader["EmailForIndividualAlert"]);
            this.EmailForIndividualMessage = BasicConverter.DbToStringValue(reader["EmailForIndividualMessage"]);
            this.ProviderCode = BasicConverter.DbToStringValue(reader["ProviderCode"]);
            this.AllowAHAEmail = BasicConverter.DbToNullableBoolValue(reader["AllowAHAEmail"]);
            this.UserTypeID = BasicConverter.DbToIntValue(reader["UserTypeID"]);

            using (DataTable dt = reader.GetSchemaTable())
            {
                if (Misc.DoesColumnExist(dt, "LanguageLocale"))
                {
                    this.LanguageLocale = BasicConverter.DbToStringValue(reader["LanguageLocale"]);
                }
                if (Misc.DoesColumnExist(dt, "DefaultLanguageID"))
                {
                    this.DefaultLanguageID = BasicConverter.DbToIntValue(reader["DefaultLanguageID"]);
                }
                if (Misc.DoesColumnExist(dt, "StateName"))
                {
                    this.StateName = BasicConverter.DbToStringValue(reader["StateName"]);
                }
            }
        }

        // From the SOW for the new Provider Portal:
        // The system will enforce unique user names for Providers/Volunteers.  
        // The username will be limited to 5-50 characters and only allow the following special characters: "@", ".", "-", "_"
        public static bool ValidateUsernameFormat(string username, ref string errmsg )
        {
            bool retval = true ;
            string regexchars = @"[^a-zA-Z0-9@\-_.]" ;
            if (String.IsNullOrEmpty(username))
            {
                retval = false;
                errmsg = "Empty Username.";
            }
            else if (username.Contains(' '))
            {
                retval = false;
                errmsg = "Username cannot contain blanks.";
            }
            else if (username.Length < 5 || username.Length > 50 )
            {
                retval = false;
                errmsg = "Username is limited to 5-50 characters.";
            }
            else if (Regex.IsMatch(username, regexchars))
            {
                retval = false;
                errmsg = "Username can only contain the four following special characters: \'@\', '.', '-', and '_'.";
            }
            else
            {
                // TODO: Check for uniqueness in database.
                try
                {
                    ProviderDetails pd = FindByUsername(username);
                    if (pd != null)
                    {
                        retval = false;
                        errmsg = "Username is already taken, please choose a different one.";
                    }
                }
                catch
                {
                    retval = false;
                    errmsg = "Internal error validating Username.";
                }
            }

            return retval;
        }

        public static bool ValidatePasswordFormat(string password, ref string errmsg)
        {
            bool retval = true;

            if (string.IsNullOrEmpty(password))
            {
                retval = false;
                errmsg = "Password cannot be empty.";
            }
            else if (password.Contains(' '))
            {
                retval = false;
                errmsg = "Password cannot contain blanks.";
            }
            else if (password.Length < 6)
            {
                retval = false;
                errmsg = "Password must be at least six characters long.";
            }
            return retval;
        }

        public static ProviderDetails CreateProvider(
            string strUsername,
            string strEmail,
            string strPassword,
            string strFirstName,
            string strLastName,
            string strSalutation,
            string strPracticeName,
            string strPracticeEmail,
            string strPracticeAddress,
            string strPracticePhone,
            string strPracticeFax,
            string strSpeciality,
            string strPharmacyPartner,
            string strProfileMessage,
            string strPracticeUrl,
            string strCity,
            int iStateID,
            string strZip,
            bool? bAllowPatientsToSendMessages,
            bool? bIncludeAlertNotificationInDailySummary,
            bool? bIncludeMessageNotificationInDailySummary,
            bool? bNotifyNewAlertImmediately,
            bool? bNotifyNewMessageImmediately,
            int iDismissAlertAfterWeeks,
            string strEmailForDailySummary,
            string strEmailForIndividualAlert,
            string strEmailForIndividualMessage,
            bool? bAllowAHAEmail,
            int iLanguageID, 
            int iUserTypeID)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iProviderID;
            ProviderDetails objProviderDetails = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strUsername);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Email", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Password", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPassword);

                param = dbContext.ContextSqlCommand.Parameters.Add("@FirstName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strFirstName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strLastName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Salutation", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strSalutation);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeEmail", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeAddress", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeAddress);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticePhone", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticePhone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeFax", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeFax);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Speciality", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strSpeciality);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PharmacyPartner", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPharmacyPartner);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProfileMessage", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strProfileMessage);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeUrl", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@City", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strCity);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StateID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iStateID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Zip", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strZip);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AllowPatientsToSendMessages", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bAllowPatientsToSendMessages);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DismissAlertAfterWeeks", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iDismissAlertAfterWeeks);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IncludeAlertNotificationInDailySummary", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bIncludeAlertNotificationInDailySummary);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IncludeMessageNotificationInDailySummary", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bIncludeMessageNotificationInDailySummary);

                param = dbContext.ContextSqlCommand.Parameters.Add("@NotifyNewAlertImmediately", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bNotifyNewAlertImmediately);

                param = dbContext.ContextSqlCommand.Parameters.Add("@NotifyNewMessageImmediately", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bNotifyNewMessageImmediately);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EmailForDailySummary", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmailForDailySummary);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EmailForIndividualAlert", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmailForIndividualAlert);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EmailForIndividualMessage", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmailForIndividualMessage);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AllowAHAEmail", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bAllowAHAEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserTypeID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iUserTypeID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateProvider]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ProviderID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iProviderID = Convert.ToInt32(param_output.Value);
                objProviderDetails = new ProviderDetails
                {
                    Email = strEmail,
                    FirstName = strFirstName,
                    LastName = strLastName,
                    Password = strPassword,
                    PharmacyPartner = strPharmacyPartner,
                    PracticeAddress = strPracticeAddress,
                    PracticeEmail = strPracticeEmail,
                    PracticeFax = strPracticeFax,
                    PracticeName = strPracticeName,
                    PracticePhone = strPracticePhone,
                    Salutation = strSalutation,
                    Speciality = strSpeciality,
                    ProfileMessage = strProfileMessage,
                    PracticeUrl = strPracticeUrl,
                    City = strCity,
                    StateID = iStateID,
                    Zip = strZip,
                    ProviderID = iProviderID,
                    DateOfRegistration = DateTime.Now,
                    UserTypeID = iUserTypeID
                };

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderDetails;
        }

        public static void UpdatePassword(
            int iProviderID,
            string strPassword)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Password", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPassword);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdatePassword]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void UpdateProvider(
            int iProviderID,
            string strEmail,
            string strFirstName,
            string strLastName,
            string strSalutation,
            string strPracticeName,
            string strPracticeEmail,
            string strPracticeAddress,
            string strPracticePhone,
            string strPracticeFax,
            string strSpeciality,
            string strPharmacyPartner,
            string strProfileMessage,
            string strPracticeUrl,
            string strCity,
            int iStateID,
            string strZip,
            bool? bAllowPatientsToSendMessages,
            bool? bIncludeAlertNotificationInDailySummary,
            bool? bIncludeMessageNotificationInDailySummary,
            bool? bNotifyNewAlertImmediately,
            bool? bNotifyNewMessageImmediately,
            int iDismissAlertAfterWeeks,
            string strEmailForDailySummary,
            string strEmailForIndividualAlert,
            string strEmailForIndividualMessage,
            bool? bAllowAHAEmail)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Email", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@FirstName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strFirstName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strLastName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Salutation", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strSalutation);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeEmail", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeAddress", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeAddress);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticePhone", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticePhone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeFax", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeFax);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Speciality", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strSpeciality);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PharmacyPartner", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPharmacyPartner);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProfileMessage", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strProfileMessage);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeUrl", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@City", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strCity);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StateID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iStateID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Zip", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strZip);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AllowPatientsToSendMessages", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bAllowPatientsToSendMessages);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DismissAlertAfterWeeks", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iDismissAlertAfterWeeks);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IncludeAlertNotificationInDailySummary", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bIncludeAlertNotificationInDailySummary);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IncludeMessageNotificationInDailySummary", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bIncludeMessageNotificationInDailySummary);

                param = dbContext.ContextSqlCommand.Parameters.Add("@NotifyNewAlertImmediately", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bNotifyNewAlertImmediately);

                param = dbContext.ContextSqlCommand.Parameters.Add("@NotifyNewMessageImmediately", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bNotifyNewMessageImmediately);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EmailForDailySummary", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmailForDailySummary);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EmailForIndividualAlert", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmailForIndividualAlert);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EmailForIndividualMessage", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmailForIndividualMessage);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AllowAHAEmail", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(bAllowAHAEmail);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateProvider]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static ProviderDetails FindByProviderID(int iProviderID)
        {
            DBContext dbContext = null;
            ProviderDetails objProviderDetails = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindProviderByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderDetails;
        }

        public static ProviderDetails FindByProviderCode(string strCode)
        {
            DBContext dbContext = null;
            ProviderDetails objProviderDetails = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindProviderByCode]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Code", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strCode);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderDetails;
        }

        public static ProviderDetails FindByUserNameAndPassword(string strUserName, string strPassword)
        {
            DBContext dbContext = null;
            ProviderDetails objProviderDetails = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindProviderByUserNameandPassword]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strUserName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Password", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPassword);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderDetails;
        }

        public static ProviderDetails FindByUsername(string strUsername)
        {
            DBContext dbContext = null;
            ProviderDetails objProviderDetails = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindProviderByUsername]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strUsername);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderDetails;
        }

        public static ProviderDetails FindByEmail(string strEmail)
        {
            DBContext dbContext = null;
            ProviderDetails objProviderDetails = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindProviderByEmail]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Email", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmail);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderDetails;
        }

        public static List<ProviderDetails> FindProviderByPatientID(int iPatientID)
        {
            DBContext dbContext = null;
            List<ProviderDetails> objProviderDetailsList = new List<ProviderDetails>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAllProvidersByPatientID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderDetails objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                    objProviderDetailsList.Add(objProviderDetails);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderDetailsList;
        }

        public static List<ProviderDetails> ProviderNotLoggedInForLastNDays(int intNumberOfDays, int? iCampaignID)
        {
            DBContext dbContext = null;
            List<ProviderDetails> objProviderDetailsList = new List<ProviderDetails>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetProvidersNotLoggedInForLastNdays]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@NumberofDays", SqlDbType.Int);
                param.Value = intNumberOfDays;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderDetails objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                    objProviderDetailsList.Add(objProviderDetails);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderDetailsList;
        }


        public static List<ProviderDetails> FindProvidersForDailyDigest()
        {
            DBContext dbContext = null;
            List<ProviderDetails> objProviderDetailsList = new List<ProviderDetails>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindProvidersForDailyDigest]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderDetails objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                    objProviderDetailsList.Add(objProviderDetails);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderDetailsList;
        }

        // PJB: added two optional parameters for V6. They are optional for backwards compatibility
        public static List<ProviderDetails> SearchProvider(string strFirstName, string strLastName, string strPracticeName, string strPhone, string strCity, int? iStateID, int iLanguageID, string strZip = null, string strEmail = null )
        {
            DBContext dbContext = null;
            List<ProviderDetails> objProviderDetailsList = new List<ProviderDetails>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_SearchProvider]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FirstName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strFirstName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strLastName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticePhone", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPhone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@City", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strCity);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StateID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iStateID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ZipCode", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strZip);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeEmail", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strEmail);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderDetails objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                    objProviderDetailsList.Add(objProviderDetails);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderDetailsList;
        }


        public static List<ProviderDetails> SearchProviderV2(string strFirstName, string strLastName, string strPracticeName, string strProviderCode, string strPhone, string strCity, int? iStateID, int iLanguageID)
        {
            DBContext dbContext = null;
            List<ProviderDetails> objProviderDetailsList = new List<ProviderDetails>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_SearchProviderV2]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FirstName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strFirstName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strLastName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticeName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPracticeName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderCode", SqlDbType.NChar, 6);
                param.Value = BasicConverter.StringToDbValue(strProviderCode);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PracticePhone", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPhone);

                param = dbContext.ContextSqlCommand.Parameters.Add("@City", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strCity);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StateID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iStateID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderDetails objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                    objProviderDetailsList.Add(objProviderDetails);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderDetailsList;
        }

        public static List<ProviderDetails> FindAllProvidersForAHAEmails(int? iCampaignID)
        {
            DBContext dbContext = null;
            List<ProviderDetails> objProviderDetailsList = new List<ProviderDetails>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAllProvidersForAHAEmails]";

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderDetails objProviderDetails = new ProviderDetails();
                    objProviderDetails.OnDataBind(reader);
                    objProviderDetailsList.Add(objProviderDetails);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderDetailsList;
        }

        public static void DisconnectPatientOrProvider(int iProviderID, int iUserHealthRecordID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iUserHealthRecordID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DisconnectPatient]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DisconnectPatientOrProvider(int iProviderID, Guid gUserHealthRecordID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gUserHealthRecordID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DisconnectPatientByRecordGUID]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }


        public static bool IsValidPatientForProvider(int iPatientID, int iProviderID)
        {
            DBContext dbContext = null;
            bool bRetValue = false;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_IsValidPatientForProvider]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@IsValid", SqlDbType.Bit));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                bRetValue = BasicConverter.DbToBoolValue(Convert.ToInt32(param_output.Value));
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return bRetValue;
        }



        public static int GetCountOfRecentlyAddedPatients(int iProviderID, int iLastHowManyDays)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetCountOfRecentlyAddedPatients]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = cmd.Parameters.Add("@NoOfDays", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLastHowManyDays);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }

        public static int GetCountOfRecentlyDisconnectedPatients(int iProviderID, int iLastHowManyDays)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetCountOfRecentlyDisconnectedPatients]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = cmd.Parameters.Add("@NoOfDays", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLastHowManyDays);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }

        public static void UpdateDefaultLanguage(
            int iProviderID,
            int iLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateLanguage_Provider]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
        #endregion
    }
}
