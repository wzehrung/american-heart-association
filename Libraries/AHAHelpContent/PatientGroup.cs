﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class PatientGroup
    {
        /**
         * M: manually created groups
         * A: automatically created groups, tier 1
         * D: automatically created groups, tier 2/Diabetes
         * null: default groups, no display to users
         **/
        public enum GroupPatientRemoveOption
        {
            M,
            A,
            D
        }
        #region Public Properties
        public int GroupID
        {
            get;
            set;
        }

        public int ProviderID
        {
            get;
            set;
        }

        public DateTime? ProviderConnectionDate
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public DateTime DateCreated
        {
            get;
            set;
        }

        public int? TotalMembers
        {
            get;
            set;
        }

        public int TotalAlerts
        {
            get;
            set;
        }

        public bool IsDefault
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string GroupRuleXml
        {
            get;
            set;
        }

        public bool IsSystemGenerated
        {
            get;
            set;
        }

        public GroupPatientRemoveOption RemoveOption
        {
            get;
            set;
        }
        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader, bool bIncludeTotalMembers, bool bIncludeAlert, bool bIncludeIsSystemGenerated)
        {
            this.GroupID = BasicConverter.DbToIntValue(reader["GroupID"]);
            this.ProviderID = BasicConverter.DbToIntValue(reader["ProviderID"]);
            this.Name = BasicConverter.DbToStringValue(reader["Name"]);
            this.DateCreated = BasicConverter.DbToDateValue(reader["DateCreated"]);
            this.IsDefault = BasicConverter.DbToBoolValue(reader["isDefault"]);

            if (bIncludeTotalMembers)
            {
                this.TotalMembers = BasicConverter.DbToNullableIntValue(reader["TotalMembers"]);
            }

            if (bIncludeAlert)
            {
                this.TotalAlerts = BasicConverter.DbToIntValue(reader["TotalAlerts"]);
            }

            this.Description = BasicConverter.DbToStringValue(reader["Description"]);
            this.GroupRuleXml = BasicConverter.DbToStringValue(reader["GroupRuleXml"]);

            string strRemoveOption = BasicConverter.DbToStringValue(reader["RemoveOption"]);
            if (!String.IsNullOrEmpty(strRemoveOption))
                RemoveOption = ((GroupPatientRemoveOption)Enum.Parse(typeof(GroupPatientRemoveOption), strRemoveOption, false));

            if (bIncludeIsSystemGenerated)
                this.IsSystemGenerated = BasicConverter.DbToBoolValue(reader["IsSystemGenerated"]);

            using (DataTable dt = reader.GetSchemaTable())
            {
                if (Misc.DoesColumnExist(dt, "ProviderConnectionDate"))
                {
                    this.ProviderConnectionDate = BasicConverter.DbToDateValue(reader["ProviderConnectionDate"]);
                }
            }

        }
        #endregion

        #region Public Methods
        public static PatientGroup CreateGroup(int iProviderID, string strGroupName, string strDescription, string strGroupRuleXml, GroupPatientRemoveOption eRemoveOption, bool bIsSystemGenerated)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iGroupID;
            PatientGroup objGroup = null;
            string strRemoveOption = GRCBase.MiscUtility.GetComponentModelPropertyDescription(eRemoveOption);
            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Name", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strGroupName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Description", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strDescription);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupRuleXml", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strGroupRuleXml);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RemoveOption", SqlDbType.Char);
                param.Value = BasicConverter.StringToDbValue(strRemoveOption);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsSystemGenerated", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsSystemGenerated);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@GroupID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateGroup]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iGroupID = Convert.ToInt32(param_output.Value);
                objGroup = new PatientGroup { GroupID = iGroupID, Name = strGroupName, ProviderID = iProviderID };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objGroup;
        }

        public static void UpdateGroup(int iGroupID, string strGroupName, string strDescription, string strGroupRuleXml, GroupPatientRemoveOption eRemoveOption)
        {
            DBContext dbContext = null;
            bool bException = false;
            string strRemoveOption = GRCBase.MiscUtility.GetComponentModelPropertyDescription(eRemoveOption);

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strGroupName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Description", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strDescription);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupRuleXml", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strGroupRuleXml);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RemoveOption", SqlDbType.Char);
                param.Value = BasicConverter.StringToDbValue(strRemoveOption);

                dbContext.ContextSqlCommand.CommandText = "[proc_UpdateGroup]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static PatientGroup FindByGroupID(int iGroupID)
        {
            DBContext dbContext = null;
            PatientGroup objPatientGroup = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindGroupByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatientGroup = new PatientGroup();
                    objPatientGroup.OnDataBind(reader, true, true, false);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientGroup;
        }

        public static PatientGroup FindByGroupNameAndProviderID(int iProviderID, string strGroupName)
        {
            DBContext dbContext = null;
            PatientGroup objPatientGroup = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindGroupByNameAndProviderID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strGroupName);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatientGroup = new PatientGroup();
                    objPatientGroup.OnDataBind(reader, true, true, false);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientGroup;
        }

        public static List<PatientGroup> FindEligibleGroupsForPatient(int iPatientID, int iProviderID)
        {
            DBContext dbContext = null;
            List<PatientGroup> objPatientList = new List<PatientGroup>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindEligibleGroupsForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientGroup objPatientGroup = new PatientGroup();
                    objPatientGroup.OnDataBind(reader, false, false, false);
                    objPatientList.Add(objPatientGroup);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientList;
        }

        public static List<PatientGroup> FindAllByProviderID(int iProviderID)
        {
            DBContext dbContext = null;
            List<PatientGroup> objPatientGroupList = new List<PatientGroup>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAllGroupsByProviderID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientGroup objPatientGroup = new PatientGroup();
                    objPatientGroup.OnDataBind(reader, true, true, true);
                    objPatientGroupList.Add(objPatientGroup);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientGroupList;
        }

        public static List<PatientGroup> FindAllGroupsWithRuleXml()
        {
            DBContext dbContext = null;
            List<PatientGroup> objPatientGroupList = new List<PatientGroup>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAllGroupsWithRuleXml]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientGroup objPatientGroup = new PatientGroup();
                    objPatientGroup.OnDataBind(reader, false, false, false);
                    objPatientGroupList.Add(objPatientGroup);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientGroupList;
        }

        public static List<PatientGroup> FindGroupsWithProviderGroupRulesForPatient(int iPatientID)
        {
            DBContext dbContext = null;
            List<PatientGroup> objPatientGroupList = new List<PatientGroup>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindGroupsWithProviderGroupRulesForPatient]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientGroup objPatientGroup = new PatientGroup();
                    objPatientGroup.OnDataBind(reader, false, false, false);
                    objPatientGroupList.Add(objPatientGroup);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientGroupList;
        }

        public static List<PatientGroup> FindAllByPatientIDAndProviderID(int iPatientID, int iProviderID)
        {
            DBContext dbContext = null;
            List<PatientGroup> objPatientGroupList = new List<PatientGroup>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAllGroupsByPatientID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientGroup objPatientGroup = new PatientGroup();
                    objPatientGroup.OnDataBind(reader, false, false, false);
                    objPatientGroupList.Add(objPatientGroup);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientGroupList;
        }

        public static void DeleteGroup(int iGroupID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DropGroup]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void RemovePatientFromGroup(int iPatientID, int iGroupID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_RemovePatientFromGroup]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        //public static void AddPatientToCampaign(int iPatientID, int iProviderID)
        //{
        //    DBContext dbContext = null;
        //    bool bException = false;

        //    try
        //    {
        //        dbContext = DBContext.GetDBContext();
        //        dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter param = null;

        //        param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
        //        param.Value = gPersonalItemGUID;

        //        param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.VarChar);
        //        param.Value = BasicConverter.IntToDbValue(iProviderID);

        //        dbContext.ContextSqlCommand.CommandText = "[Proc_]";

        //        dbContext.ContextSqlCommand.ExecuteNonQuery();
        //    }
        //    catch
        //    {
        //        bException = true;
        //        throw;
        //    }
        //    finally
        //    {

        //        try
        //        {
        //            if (bException)
        //            {
        //                if (dbContext != null)
        //                {
        //                    dbContext.ReleaseDBContext(false);
        //                }
        //            }
        //            else
        //            {
        //                dbContext.ReleaseDBContext(true);
        //            }
        //        }
        //        catch
        //        {
        //            if (dbContext != null)
        //            {
        //                dbContext.ReleaseDBContext(false);
        //            }
        //            throw;
        //        }
        //    }
        //}

        public static void AddPatientToGroup(List<int> listPatient, List<int> listGroup, int iProviderID)
        {
            DBContext dbContext = null;
            bool bException = false;

            bool bIsSinglePatient = listGroup != null && listGroup.Count > 1;

            if (listGroup == null)
            {
                bIsSinglePatient = true;
            }

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.VarChar);
                if (listGroup != null)
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", listGroup.ConvertAll<string>(i => i.ToString()).ToArray()));
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.VarChar);
                string strComma = string.Join(",", listPatient.ConvertAll<string>(i => i.ToString()).ToArray());
                param.Value = BasicConverter.StringToDbValue(strComma);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@isSinglePatient", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsSinglePatient);

                dbContext.ContextSqlCommand.CommandText = "[Proc_AddPatientToGroup]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void AddPatientToGroup(List<int> listPatient, int iGroupID, int iProviderID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iGroupID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.VarChar);
                if (listPatient.Count == 0)
                {
                    param.Value = DBNull.Value;
                }
                else
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", listPatient.ConvertAll<string>(i => i.ToString()).ToArray()));
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_AddPatientToGroupWIthRule]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(PatientGroup))
            {
                PatientGroup objGroup = obj as PatientGroup;
                if (objGroup.GroupID == this.GroupID &&
                    objGroup.ProviderID == this.ProviderID)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
