﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;


namespace AHAHelpContent
{
    public class AHAUser
    {

        #region Properties

        /// <summary>
        /// Gets or sets HealthHistory Completed Status
        /// </summary>
        public bool IsHealthHistoryCompleted
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HVCreateDate
        /// </summary>
        public DateTime CreatedDate
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets HVCreateDate
        /// </summary>
        public DateTime UpdatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets PersonGUID
        /// </summary>
        public Guid PersonGUID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets SelfHealthRecordID
        /// </summary>
        public int SelfHealthRecordID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ProfileDataCompleted status
        /// </summary>
        public bool IsPersonalInfoCompleted
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TermsAndConditions status
        /// </summary>
        public bool TermsAndConditions
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Consent
        /// </summary>
        public bool Consent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets NewTermsAndConditions
        /// </summary>
        public bool NewTermsAndConditions
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TermsAcceptanceDate
        /// </summary>
        public DateTime TermsAcceptanceDate
        {
            get; set;
        }

        /// <summary>
        /// Gets UserID
        /// </summary>
        public int UserID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Current Selected Record GUID
        /// </summary>
        public Guid? CurrentSelectedRecordGUID
        {
            get;
            set;
        }

        /// <summary>
        /// AllowAHAEmail
        /// </summary>
        public bool AllowAHAEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets HVCreateDate
        /// </summary>
        public DateTime AllowAHAEmailUpdatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DefaultLanguageID
        /// </summary>
        public int DefaultLanguageID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LanguageLocale
        /// </summary>
        public string LanguageLocale
        {
            get;
            set;
        }

        /// <summary>
        /// Gets Group1
        /// </summary>
        public int Group1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets Group2
        /// </summary>
        public int Group2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets Group3
        /// </summary>
        public int Group3
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// Sets IsPersonalInfoCompleted for AHAUser
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <param name="IsPersonalInfoCompleted"></param>
        public static void UpdateUserIsPersonalInfoCompleted(Guid PersonGUID, bool IsPersonalInfoCompleted)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsPersonalInfoCompleted", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(IsPersonalInfoCompleted);


                dbContext.ContextSqlCommand.CommandText = "Proc_ModifyAHAUser_IsPersonalInfoCompleted";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Sets IsHealthHistoryCompleted for AHAUser
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <param name="IsHealthHistoryCompleted"></param>
        public static void UpdateUserIsHealthHistoryCompleted(Guid PersonGUID, bool IsHealthHistoryCompleted)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsHealthHistoryCompleted", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(IsHealthHistoryCompleted);


                dbContext.ContextSqlCommand.CommandText = "Proc_ModifyAHAUser_IsHealthHistoryCompleted";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Updates 'Terms and Conditions' and 'Consent'
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <param name="bTerms"></param>
        /// <param name="bConsent"></param>
        public static void UpdateUserTermsAndConsent(Guid PersonGUID, bool bTerms, bool bConsent)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TermsAndConditions", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bTerms);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Consent", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bConsent);


                dbContext.ContextSqlCommand.CommandText = "Proc_ModifyAHAUser_TermsAndConditions_Consent";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        /// <summary>
        /// Updates 'Terms and Conditions' and 'Consent'
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <param name="bTerms"></param>
        /// <param name="bConsent"></param>
        public static void UpdateUserNewTermsAndConsent(Guid PersonGUID, bool bNewTermsAndConditions)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@NewTermsAndConditions", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bNewTermsAndConditions);


                dbContext.ContextSqlCommand.CommandText = "Proc_ModifyAHAUser_NewTermsAndConditions";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.UserID = BasicConverter.DbToIntValue(reader["UserID"]);
            this.SelfHealthRecordID = BasicConverter.DbToIntValue(reader["SelfHealthRecordID"]);
            this.IsPersonalInfoCompleted = BasicConverter.DbToBoolValue(reader["IsPersonalInfoCompleted"]);
            this.IsHealthHistoryCompleted = BasicConverter.DbToBoolValue(reader["IsHealthHistoryCompleted"]);
            this.TermsAndConditions = BasicConverter.DbToBoolValue(reader["TermsAndConditions"]);
            this.TermsAcceptanceDate = BasicConverter.DbToDateValue(reader["TermsAcceptanceDate"]);
            this.Consent = BasicConverter.DbToBoolValue(reader["Consent"]);
            this.CreatedDate = BasicConverter.DbToDateValue(reader["CreatedDate"]);
            this.UpdatedDate = BasicConverter.DbToDateValue(reader["UpdatedDate"]);
            this.CurrentSelectedRecordGUID = BasicConverter.DbToNullableGuidValue(reader["CurrentSelectedRecordGUID"]);
            this.PersonGUID = BasicConverter.DbToGuidValue(reader["PersonGUID"]);
            this.NewTermsAndConditions = BasicConverter.DbToBoolValue(reader["NewTermsAndConditions"]);
            this.AllowAHAEmail = BasicConverter.DbToBoolValue(reader["AllowAHAEmail"]);
            this.AllowAHAEmailUpdatedDate = BasicConverter.DbToDateValue(reader["AllowAHAEmailUpdatedDate"]);
            using (DataTable dt = reader.GetSchemaTable())
            {
                if (Misc.DoesColumnExist(dt, "LanguageLocale"))
                {
                    this.LanguageLocale = BasicConverter.DbToStringValue(reader["LanguageLocale"]);
                }
                if (Misc.DoesColumnExist(dt, "DefaultLanguageID"))
                {
                    this.DefaultLanguageID = BasicConverter.DbToIntValue(reader["DefaultLanguageID"]);
                }
                if (Misc.DoesColumnExist(dt, "Group1"))
                {
                    this.Group1 = BasicConverter.DbToIntValue(reader["Group1"]);
                }
                if (Misc.DoesColumnExist(dt, "Group2"))
                {
                    this.Group2 = BasicConverter.DbToIntValue(reader["Group2"]);
                }
                if (Misc.DoesColumnExist(dt, "Group3"))
                {
                    this.Group3 = BasicConverter.DbToIntValue(reader["Group3"]);
                }
            }
        }

        /// <summary>
        /// function temporarily added to retrieve AHAUser data for given PersonGUID
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <returns></returns>
        public static AHAUser GetAHAUserByPersonGUID(Guid gPersonGUID)
        {
            DBContext dbContext = null;
            AHAUser objAHAUser = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAHAUserByPersonGUID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objAHAUser = new AHAUser();
                    objAHAUser.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objAHAUser;
        }

        /// <summary>
        /// Creates AHAUser
        /// </summary>
        /// <param name="SelfPersonalItemGUID"></param>
        /// <param name="PersonGUID"></param>
        /// <param name="SelfHealthRecordID"></param>
        /// <returns></returns>
        public static AHAUser CreateAHAUser(Guid? gSelfPersonalItemGUID, Guid gPersonGUID, Guid? gSelfHealthRecordID, int iLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iAHAUserID;
            AHAUser objAHAUser = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);

                if (gSelfHealthRecordID.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@SelfUserHealthRecordGUID", SqlDbType.UniqueIdentifier);
                    param.Value = gSelfHealthRecordID.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@SelfUserHealthRecordGUID", SqlDbType.UniqueIdentifier);
                    param.Value = DBNull.Value;
                }

                if (gSelfPersonalItemGUID.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@SelfPersonalItemGUID", SqlDbType.UniqueIdentifier);
                    param.Value = gSelfPersonalItemGUID.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@SelfPersonalItemGUID", SqlDbType.UniqueIdentifier);
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);


                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateAHAUser";
                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iAHAUserID = Convert.ToInt32(param_output.Value);

                objAHAUser = new AHAUser
                {
                    UserID = iAHAUserID,
                    PersonGUID = gPersonGUID
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objAHAUser;
        }



        public static void SetHostDevice(Guid gPersonGUID, string sDevName, string sDevId)
        {

            // TODO: pjb
        }


        /// <summary>
        /// Delete AHAUser
        /// </summary>
        /// <param name="PersonGUID"></param>
        public static void DropAHAUser(Guid PersonGUID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;
                param = dbContext.ContextSqlCommand.Parameters.Add("PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGUID);

                dbContext.ContextSqlCommand.CommandText = "Proc_DropAHAUser";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return;
        }

        /// <summary>
        /// updates AHAUser with changed user
        /// </summary>
        /// <param name="PersonGUID"></param>
        /// <param name="CurrentSelectedRecordGUID"></param>
        public static void ChangeSelectedRecord(Guid PersonGUID, Guid? CurrentSelectedRecordGUID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(PersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CurrentSelectedRecordGUID", SqlDbType.UniqueIdentifier);
                if (CurrentSelectedRecordGUID.HasValue)
                {
                    param.Value = BasicConverter.GuidToDbValue(CurrentSelectedRecordGUID.Value);
                }
                else
                {
                    param.Value = DBNull.Value;
                }


                dbContext.ContextSqlCommand.CommandText = "Proc_ModifyAHAUser_CurrentSelectedRecordGUID";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return;
        }

        public static void SetAHAMailAllowedForUser(Guid gPersonGUID, bool bAllowed)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AllowAHAEmail", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bAllowed);

                dbContext.ContextSqlCommand.CommandText = "[proc_UpdateAHAUser_AllowAHAEmail]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static List<AHAUser> FindAllUsersForAHAEmails(int? iCampaignID)
        {
            DBContext dbContext = null;
            List<AHAUser> objList = new List<AHAUser>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAllAHAUserForAHAEmails]";

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AHAUser objAHAEmailUser = new AHAUser();
                    objAHAEmailUser.OnDataBind(reader);
                    objList.Add(objAHAEmailUser);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static List<AHAUser> FindAllUsersSignedUpForLastNMonths(int iNumberOfMonths, int? iCampaignID)
        {
            DBContext dbContext = null;
            List<AHAUser> objList = new List<AHAUser>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetPatientsSignedUpInForLastNMonths]";

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@NumberofMonths", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iNumberOfMonths);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AHAUser objAHAEmailUser = new AHAUser();
                    objAHAEmailUser.OnDataBind(reader);
                    objList.Add(objAHAEmailUser);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static List<AHAUser> FindAllUsersNotLoggedInForLastNDays(int iNumberOfDays, int? iCampaignID)
        {
            DBContext dbContext = null;
            List<AHAUser> objList = new List<AHAUser>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetPatientsNotLoggedInForLastNdays]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = dbContext.ContextSqlCommand.Parameters.Add("@NumberofDays", SqlDbType.Int);
                param.Value = iNumberOfDays;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AHAUser objUser = new AHAUser();
                    objUser.OnDataBind(reader);
                    objList.Add(objUser);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static void UpdateDefaultLanguage(
            Guid gPersonGUID,
            int iLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateLanguage_Patient]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static string FetchDashboardSortOrderForAHAuserHealthRecord(Guid gPersonGUID, Guid gHealthRecordGUID)
        {
            DBContext dbContext = null;
            string strXml = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FetchDashboardSortOrderForAHAuserHealthRecord]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    strXml = BasicConverter.DbToStringValue(reader["DashboardSortOrder"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return strXml;
        }

        public static void ModifyAHAuserHealthRecordDashBoard(
           Guid gPersonGUID,
           Guid gHealthRecordGUID,
           string strDashboardSortOrderXml)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DashboardSortOrder", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strDashboardSortOrderXml);

                dbContext.ContextSqlCommand.CommandText = "[Proc_ModifyAHAuserHealthRecordDashBoard]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static string FetchTrackerDetailsForAHAuserHealthRecord(Guid gPersonGUID, Guid gHealthRecordGUID)
        {
            DBContext dbContext = null;
            string strXml = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FetchTrackerDetailsForAHAuserHealthRecord]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    strXml = BasicConverter.DbToStringValue(reader["TrackerDetails"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return strXml;
        }

        public static void ModifyAHAuserHealthRecordTrackerDetails(
           Guid gPersonGUID,
           Guid gHealthRecordGUID,
           string strTrackerDetailsXml)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthRecordGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gHealthRecordGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TrackerDetails", SqlDbType.Xml);
                param.Value = BasicConverter.StringToDbValue(strTrackerDetailsXml);

                dbContext.ContextSqlCommand.CommandText = "[Proc_ModifyAHAuserHealthRecordTrackerDetails]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static List<AHAUser> FindUsersForAHABPTriggerEmails(int iWeeks, int? iCampaignID)
        {
            DBContext dbContext = null;
            List<AHAUser> objList = new List<AHAUser>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_GetPatientsActiveForLastNweeks_ForBPgroups]";

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Numberofweeks", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iWeeks);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AHAUser objAHAEmailUser = new AHAUser();
                    objAHAEmailUser.OnDataBind(reader);
                    objList.Add(objAHAEmailUser);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static string FindAllProvidersUserTypeByPatientID(int iPatientID)
        {
            DBContext dbContext = null;
            string strConnect = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindAllProvidersUserTypeByPatientID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    strConnect = BasicConverter.DbToStringValue(reader["IsConnectedToProvider"]) + "," + BasicConverter.DbToStringValue(reader["IsConnectedToVolunteer"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return strConnect;
        }

    }
}
