﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GRCBase;
using System.Data;

namespace AHAHelpContent
{
    public class ProviderTips
    {
        /// <summary>
        /// Gets ContentID
        /// </summary>
        public int ProviderTipsID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets URL
        /// </summary>
        /// <summary>
        public string Content
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets URL
        /// </summary>
        /// <summary>
        public string ProviderTipTypes
        {
            get;
            set;
        }


        /// Gets tips for given ProviderTipsType
        /// </summary>
        /// <param name="strProviderTipsType"></param>
        /// <returns>string</returns>
        public static string GetTipForTheDay(string strProviderTipsType,int LanguageID)
        {

            SqlDataReader reader = null;
            string strTipForTheDay = string.Empty;
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                dbContext.ContextSqlCommand.CommandText = "Proc_FetchProviderTiptypeFortheDay";

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strProviderTipsType);


                reader = dbContext.ContextSqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    strTipForTheDay = BasicConverter.DbToStringValue(reader["ProviderTipsData"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return strTipForTheDay;
        }

        /// <summary>
        /// Gets All contents
        /// </summary>
        /// <param name="strAbbrev"></param>
        /// <returns>List<Content></returns>
        public static List<ProviderTips> GetAllProviderTips(int LanguageID)
        {
            DBContext dbContext = null;
            List<ProviderTips> listProviderTips = new List<ProviderTips>();
            SqlDataReader reader = null;
            bool bException = false;
            SqlParameter param = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_GetAllProviderTips]";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    ProviderTips objProviderTips = new ProviderTips();
                    objProviderTips.ProviderTipsID = Convert.ToInt32(reader["ProviderTipsId"]);
                    objProviderTips.Content = reader["ProviderTipsData"].ToString();
                    objProviderTips.ProviderTipTypes = reader["ProviderTipsTypeIDs"].ToString();
                    listProviderTips.Add(objProviderTips);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return listProviderTips;
        }

        public static ProviderTips CreateProviderTip(string strContent, List<string> Providertiptypeids, int CreatedByUserID, int UpdatedByUserID, int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            ProviderTips objProviderTips = null;
            int iprovidertipid = 0;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsData", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strContent);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsTypeIds", SqlDbType.NVarChar);
                if (Providertiptypeids != null)
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", Providertiptypeids.ConvertAll<string>(i => i.ToString()).ToArray()));
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(CreatedByUserID);



                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(providertipid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@ProviderTipsID", SqlDbType.Int));
                param_output.Value = BasicConverter.IntToDbValue(iprovidertipid);
                param_output.Direction = ParameterDirection.Output;

                //dbContext.ContextSqlCommand.CommandText = "[Proc_CreateProviderTips]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateORchangeProviderTips]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iprovidertipid = Convert.ToInt32(param_output.Value);

                objProviderTips = new ProviderTips
                {
                    Content = strContent,
                    ProviderTipsID = iprovidertipid
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objProviderTips;
        }

        public static void UpdateProviderTip(string content, List<string> Providertiptypeids, int iProviderTipID,int CreatedByUserID, int UpdatedByUserID, int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsData", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(content);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsTypeIds", SqlDbType.NVarChar);
                if (Providertiptypeids != null)
                {
                    param.Value = BasicConverter.StringToDbValue(string.Join(",", Providertiptypeids.ConvertAll<string>(i => i.ToString()).ToArray()));
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderTipID);


                param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(CreatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_ChangeProviderTips]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateORchangeProviderTips]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteProviderTip(int iProviderTipID, int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsid", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderTipID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteProviderTips]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static ProviderTips FindByProviderTipID(int iProviderTipID, int LanguageID)
        {
            DBContext dbContext = null;
            ProviderTips objProviderTip = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetProviderTipsByProviderTipsID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderTipsid", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderTipID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderTip = new ProviderTips();
                    objProviderTip.ProviderTipsID = Convert.ToInt32(reader["ProviderTipsId"]);
                    objProviderTip.Content = reader["ProviderTipsData"].ToString();
                    objProviderTip.ProviderTipTypes = reader["ProviderTipsTypeIDs"].ToString();
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderTip;
        }
    }

}
