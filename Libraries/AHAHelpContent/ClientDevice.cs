﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AHAHelpContent
{
    public class ClientDevice
    {
        //
        // Note: these enumerations have to match the (enumeration) values in the ClientDevice table of the Heart360 database
        public enum DeviceType
        {
            DEVICE_TYPE_DEFAULT = 0,
            DEVICE_TYPE_STAY_HEALTHY_KIOSK
        };

            // This returns the name for the given client device type. This name can be passed to Data Access Layer routines
            // that need to use Client Devices in the ClientDevice table.
        public static string GetDeviceName(DeviceType _dt)
        {
            string result = string.Empty;

            if (_dt == DeviceType.DEVICE_TYPE_STAY_HEALTHY_KIOSK)
                result = "StayHealthyKiosk";
            else if (_dt == DeviceType.DEVICE_TYPE_DEFAULT)
                result = "Default";

            return result;

        }
    }
}
