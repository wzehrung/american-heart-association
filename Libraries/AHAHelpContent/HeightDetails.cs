﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{

    public class HeightDetails
    {

        /// <summary>
        /// Creates or updates Height data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="Height"></param>
        /// <param name="Comments"></param>
        /// <param name="DateMeasured"></param>
        public static void CreateOrChangeHeight(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, double Height, string Comments, DateTime DateMeasured)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Height", SqlDbType.Float);
                param.Value = BasicConverter.NullableDoubleToDbValue(Height);

                if (!string.IsNullOrEmpty(Comments))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.NText);
                    param.Value = BasicConverter.StringToDbValue(Comments);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.NText);
                    param.Value = DBNull.Value;
                }


                param = dbContext.ContextSqlCommand.Parameters.Add("@DateMeasured", SqlDbType.DateTime);
                param.Value = DateMeasured;




                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeHeight";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }

            return;
        }

        /// <summary>
        /// Delete Height
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropHeight(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropHeight";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }

            return;
        }

    }
}
