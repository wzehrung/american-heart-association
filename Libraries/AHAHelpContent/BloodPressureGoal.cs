﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class BloodPressureGoal
    {

        /// <summary>
        /// Creates or updates BloodPressureGoal data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>       
        /// <param name="TargetSystolic"></param>
        /// <param name="TargetDiastolic"></param>

        public static void CreateOrChangeBloodPressureGoal(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, int TargetSystolic, int TargetDiastolic)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TargetSystolic", SqlDbType.Int);
                param.Value = BasicConverter.DoubleToDbValue(TargetSystolic);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TargetDiastolic", SqlDbType.Int);
                param.Value = BasicConverter.DoubleToDbValue(TargetDiastolic);


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeBloodPressureGoal";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }

            return;
        }

        /// <summary>
        /// Delete BloodPressureGoal data
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropBloodPressureGoal(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropBloodPressureGoal";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }
    }
}
