﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Linq;
using GRCBase;

namespace AHAHelpContent
{
        public class EMRConnections
        {
            public int ConnectionID
            {
                get;
                set;
            }
            public string ConnectionKey
            {
                get;
                set;
            }
            public bool IsConnected
            {
                get;
                set;
            }
            public int UserHealthRecordID
            {
                get;
                set;
            }
            public string MessageIdentifier
            {
                get;
                set;
            }
            public string ResponseIdentifier
            {
                get;
                set;
            }
            public string OrganizationID
            {
                get;
                set;
            }
            public string OrganizationName
            {
                get;
                set;
            }
            public string OrganizationDesc
            {
                get;
                set;
            }
            public string SecretQuestion
            {
                get;
                set;
            }
            public string SecretAnswer
            {
                get;
                set;
            }
            public DateTime RequestDate
            {
                get;
                set;
            }
            public DateTime ConnectDate
            {
                get;
                set;
            }
            public DateTime DisconnectDate
            {
                get;
                set;
            }
            public string ConnectionMessage
            {
                get;
                set;
            }

            private void OnDataBind(SqlDataReader reader)
            {
                this.ConnectionID       = BasicConverter.DbToIntValue(reader["ConnectionID"]);
                this.ConnectionKey      = BasicConverter.DbToStringValue(reader["ConnectionKey"]);
                this.IsConnected        = BasicConverter.DbToBoolValue(reader["IsConnected"]);
                this.UserHealthRecordID = BasicConverter.DbToIntValue(reader["UserHealthRecordID"]);
                this.OrganizationID     = BasicConverter.DbToStringValue(reader["OrganizationID"]);
                this.OrganizationName   = BasicConverter.DbToStringValue(reader["OrganizationName"]);
                this.OrganizationDesc   = BasicConverter.DbToStringValue(reader["OrganizationDesc"]);
                this.SecretQuestion     = BasicConverter.DbToStringValue(reader["SecretQuestion"]);
                this.SecretAnswer       = BasicConverter.DbToStringValue(reader["SecretAnswer"]);
                this.RequestDate        = BasicConverter.DbToDateValue(reader["RequestDate"]);
                this.ConnectDate        = BasicConverter.DbToDateValue(reader["ConnectDate"]);
                this.DisconnectDate     = BasicConverter.DbToDateValue(reader["DisconnectDate"]);
            }

            public static EMRConnections GetConnectionRequest(string ConnectionKey, string OrganizationID)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                EMRConnections objRequest = new EMRConnections();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_GetConnectionRequest";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ConnectionKey", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(ConnectionKey);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(OrganizationID);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {                
                        objRequest.OnDataBind(reader);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }

            public static EMRConnections UpdateConnection(bool IsConnected, int ConnectionID, int UserHealthRecordID)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                EMRConnections objRequest = new EMRConnections();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_UpdateConnection";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ConnectionID", SqlDbType.Int);
                    param.Value = BasicConverter.DbToIntValue(ConnectionID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@IsConnected", SqlDbType.Bit);
                    param.Value = BasicConverter.DbToBoolValue(IsConnected);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                    param.Value = BasicConverter.DbToIntValue(UserHealthRecordID);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {                
                        objRequest.OnDataBind(reader);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }

            public static List<EMRConnections> GetConnections(int UserHealthRecordID)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                List<EMRConnections> objRequest = new List<EMRConnections>();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_GetConnections";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                    param.Value = BasicConverter.DbToIntValue(UserHealthRecordID);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        EMRConnections objConnections = new EMRConnections();
                        objConnections.OnDataBind(reader);
                        objRequest.Add(objConnections);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }
        }

        public class EMRNewConnections
        {
            public string ConnectionKey
            {
                get;
                set;
            }
            public bool IsConnected
            {
                get;
                set;
            }
            public string MessageIdentifier
            {
                get;
                set;
            }
            public string ResponseIdentifier
            {
                get;
                set;
            }
            public string OrganizationID
            {
                get;
                set;
            }
            public string SecretQuestion
            {
                get;
                set;
            }
            public string SecretAnswer
            {
                get;
                set;
            }
            public DateTime RequestDate
            {
                get;
                set;
            }
            public string ConnectionMessage
            {
                get;
                set;
            }

            private void OnDataBind(SqlDataReader reader)
            {
                this.ConnectionKey = BasicConverter.DbToStringValue(reader["ConnectionKey"]);
                this.IsConnected = BasicConverter.DbToBoolValue(reader["IsConnected"]);
                this.ResponseIdentifier = BasicConverter.DbToStringValue(reader["ResponseIdentifier"]);
                this.MessageIdentifier = BasicConverter.DbToStringValue(reader["MessageIdentifier"]);
                this.OrganizationID = BasicConverter.DbToStringValue(reader["OrganizationID"]);
                this.SecretQuestion = BasicConverter.DbToStringValue(reader["SecretQuestion"]);
                this.SecretAnswer = BasicConverter.DbToStringValue(reader["SecretAnswer"]);
                this.RequestDate = BasicConverter.DbToDateValue(reader["RequestDate"]);
                this.ConnectionMessage = BasicConverter.DbToStringValue(reader["ConnectionMessage"]);
            }

            public static bool DoesRequestExist(string strConnectionKey, string strOrganizationID)
            {
                DBContext dbContext = null;
                bool bException = false;
                bool isValidConnection;

                try
                {
                    dbContext = DBContext.GetDBContext();
                    dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ConnectionKey", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(strConnectionKey);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(strOrganizationID);

                    dbContext.ContextSqlCommand.CommandText = "[Proc_EMR_GetRequest]";

                    SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@DoesExist", SqlDbType.Bit));
                    param_output.Direction = ParameterDirection.Output;

                    dbContext.ContextSqlCommand.ExecuteNonQuery();

                    isValidConnection = Convert.ToBoolean(param_output.Value);
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {

                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }

                return isValidConnection;
            }

            public static EMRNewConnections CreateConnectionRequest(string ConnectionKey, string OrganizationID, string MessageIdentifier, string SecretQuestion, string SecretAnswer, string ConnectionMessage)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                EMRNewConnections objRequest = new EMRNewConnections();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_CreateConnectionRequest";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ConnectionKey", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(ConnectionKey);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(OrganizationID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@MessageIdentifier", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(MessageIdentifier);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@SecretQuestion", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(SecretQuestion);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@SecretAnswer", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(SecretAnswer);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ConnectionMessage", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(ConnectionMessage);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        objRequest.OnDataBind(reader);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }
        }

        public class EMRData
        {
            public string ConnectionKey
            {
                get;
                set;
            }
            public bool IsConnected
            {
                get;
                set;
            }
            public string MessageIdentifier
            {
                get;
                set;
            }
            public string ResponseIdentifier
            {
                get;
                set;
            }
            public string MessageData
            {
                get;
                set;
            }
            public string ItemType
            {
                get;
                set;
            }
            public string PolkaData
            {
                get;
                set;
            }

            private void OnDataBind(SqlDataReader reader)
            {
                this.MessageIdentifier = BasicConverter.DbToStringValue(reader["MessageIdentifier"]);
                this.ResponseIdentifier = BasicConverter.DbToStringValue(reader["ResponseIdentifier"]);
                this.MessageData = BasicConverter.DbToStringValue(reader["MessageData"]);
            }

            public static bool DoesConnectionExist(string strConnectionKey, string strOrganizationID)
            {
                DBContext dbContext = null;
                bool bException = false;
                bool isValidConnection;

                try
                {
                    dbContext = DBContext.GetDBContext();
                    dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ConnectionKey", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(strConnectionKey);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@OrganizationID", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(strOrganizationID);

                    dbContext.ContextSqlCommand.CommandText = "[Proc_EMR_CheckExistingConnection]";

                    SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@DoesConnectionExist", SqlDbType.Bit));
                    param_output.Direction = ParameterDirection.Output;

                    dbContext.ContextSqlCommand.ExecuteNonQuery();

                    isValidConnection = Convert.ToBoolean(param_output.Value);
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {

                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }

                return isValidConnection;
            }

            public static EMRData InsertMessageData(string ConnectionKey, int UserHealthRecordID, string MessageData, string MessageIdentifier)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                EMRData objRequest = new EMRData();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_InsertMessageData";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ConnectionKey", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(ConnectionKey);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                    param.Value = BasicConverter.DbToIntValue(UserHealthRecordID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@MessageData", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(MessageData);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@MessageIdentifier", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(MessageIdentifier);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        objRequest.OnDataBind(reader);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }

            public static EMRData InsertPolkaData(int UserHealthRecordID, string ItemType, string PolkaData, string DateCreated)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                EMRData objRequest = new EMRData();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_InsertPolkaData";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                    param.Value = BasicConverter.DbToIntValue(UserHealthRecordID);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@ItemType", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(ItemType);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@PolkaData", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(PolkaData);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@DateCreated", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(DateCreated);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        objRequest.OnDataBind(reader);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }

            public static EMRData GetDataResponseIdentifier(string MessageIdentifier)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                EMRData objRequest = new EMRData();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_GetDataResponseIdentifier";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@MessageIdentifier", SqlDbType.NVarChar);
                    param.Value = BasicConverter.DbToStringValue(MessageIdentifier);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        objRequest.OnDataBind(reader);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }
        }

        public class EMRRecords
        {
            public int EMRDataID
            {
                get;
                set;
            }
            public int ConnectionID
            {
                get;
                set;
            }
            public string ConnectionKey
            {
                get;
                set;
            }
            public int UserHealthRecordID
            {
                get;
                set;
            }            
            public string OrganizationID
            {
                get;
                set;
            }
            public string OrganizationName
            {
                get;
                set;
            }
            public string MessageData
            {
                get;
                set;
            }
            public DateTime CreatedDate
            {
                get;
                set;
            }
            private void OnDataBind(SqlDataReader reader)
            {
                this.EMRDataID = BasicConverter.DbToIntValue(reader["EMRDataID"]);
                this.ConnectionID = BasicConverter.DbToIntValue(reader["ConnectionID"]);
                this.ConnectionKey = BasicConverter.DbToStringValue(reader["ConnectionKey"]);
                this.UserHealthRecordID = BasicConverter.DbToIntValue(reader["UserHealthRecordID"]);
                this.OrganizationID = BasicConverter.DbToStringValue(reader["OrganizationID"]);
                this.OrganizationName = BasicConverter.DbToStringValue(reader["OrganizationName"]);
                this.MessageData = BasicConverter.DbToStringValue(reader["MessageData"]);
                this.CreatedDate = BasicConverter.DbToDateValue(reader["CreatedDate"]);
            }

            public static List<EMRRecords> GetRecords(int UserHealthRecordID)
            {
                DBContext dbContext = null;
                SqlDataReader reader = null;
                SqlCommand cmd = null;
                bool bException = false;

                List<EMRRecords> objRequest = new List<EMRRecords>();

                try
                {
                    dbContext = DBContext.GetDBContext();

                    SqlParameter param = null;
                    string sql = "Proc_EMR_GetRecords";

                    cmd = dbContext.ContextSqlCommand;
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.StoredProcedure;

                    param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                    param.Value = BasicConverter.DbToIntValue(UserHealthRecordID);

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        EMRRecords objConnections = new EMRRecords();
                        objConnections.OnDataBind(reader);
                        objRequest.Add(objConnections);
                    }
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    DBContext.CloseReader(reader);
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
                return objRequest;
            }

        }
}
