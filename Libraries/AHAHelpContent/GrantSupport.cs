﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    [Serializable]
    public class GrantSupport
    {
        public int GrantSupportID
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public string Description
        {
            get;
            set;
        }
        public byte[] Logo
        {
            get;
            set;
        }
        public string ContentType
        {
            get;
            set;
        }
        public bool IsForProvider
        {
            get;
            set;
        }

        public string Version
        {
            get;
            set;
        }


        private void OnDataBind(SqlDataReader reader)
        {
            this.Description = BasicConverter.DbToStringValue(reader["Description"]);
            this.GrantSupportID = BasicConverter.DbToIntValue(reader["GrantSupportID"]);
            this.IsForProvider = BasicConverter.DbToBoolValue(reader["IsForProvider"]);
            this.Logo = reader["Logo"] != DBNull.Value ? (byte[])reader["Logo"] : null;
            this.ContentType = BasicConverter.DbToStringValue(reader["ContentType"]);
            this.Title = BasicConverter.DbToStringValue(reader["Title"]);
            this.Version = BasicConverter.DbToStringValue(reader["Version"]);
        }

        public static GrantSupport CreateGrantSupport(string strTitle,
                                               string strDescription,
                                               byte[] logo,
                                               string strContentType,
                                               bool isForProvider, int CreatedByUserID, int UpdatedByUserID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            int intGrantSupportID=0;
            GrantSupport objGrantSupport = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Description", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDescription);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Logo", SqlDbType.Binary);
                if (logo != null)
                {
                    param.Value = logo;
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strContentType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsForProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(isForProvider);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(CreatedByUserID);


                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@GrantSupportID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(intGrantSupportID);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@GrantSupportID", SqlDbType.Int));
                param_output.Value = BasicConverter.IntToDbValue(intGrantSupportID);
                param_output.Direction = ParameterDirection.Output;

                //dbContext.ContextSqlCommand.CommandText = "[Proc_CreateGrantSupport]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeGrantSupport]";

               // SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@GrantSupportID", SqlDbType.Int));
               // param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                intGrantSupportID = Convert.ToInt32(param_output.Value);
                objGrantSupport = new GrantSupport
                {
                    Description = strDescription,
                    IsForProvider = isForProvider,
                    Logo = logo,
                    Title = strTitle,
                    GrantSupportID = intGrantSupportID
                };

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objGrantSupport;
        }

        public static void UpdateGrantSupport(int intGrantSupportID,
                                          string strTitle,
                                          string strDescription,
                                          byte[] logo,
                                          string strContentType,
                                          bool isForProvider, int UpdatedByUserID,int CreateByUserID, int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GrantSupportID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intGrantSupportID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Description", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strDescription);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Logo", SqlDbType.Binary);
                if (logo != null)
                {
                    param.Value = logo;
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strContentType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsForProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(isForProvider);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
               // param.Value = BasicConverter.IntToDbValue(CreateByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateGrantSupport]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeGrantSupport]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static GrantSupport GetGrantSupportByID(int intGrantSupportID,int LanguageID)
        {
            DBContext dbContext = null;
            GrantSupport objGrantSupport = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetGrantSupportByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GrantSupportID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intGrantSupportID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objGrantSupport = new GrantSupport();
                    objGrantSupport.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objGrantSupport;
        }
        public static List<GrantSupport> GetAllGrantSupport(int LanguageID)
        {
            DBContext dbContext = null;
            List<GrantSupport> objGrantSupportList = new List<GrantSupport>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlParameter param = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                string sql = "[Proc_GetAllGrantSupport]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GrantSupport objGrantSupport = new GrantSupport();
                    objGrantSupport.OnDataBind(reader);
                    objGrantSupportList.Add(objGrantSupport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objGrantSupportList;
        }


        public static List<GrantSupport> GetAllPatientGrantSupport(int LanguageID)
        {
            DBContext dbContext = null;
            List<GrantSupport> objGrantSupportList = new List<GrantSupport>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlParameter param = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();



                string sql = "[Proc_GetALLPatientGrantSupport]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GrantSupport objGrantSupport = new GrantSupport();
                    objGrantSupport.OnDataBind(reader);
                    objGrantSupportList.Add(objGrantSupport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objGrantSupportList;
        }


        public static List<GrantSupport> GetAllProviderGrantSupport(int LanguageID)
        {
            DBContext dbContext = null;
            List<GrantSupport> objGrantSupportList = new List<GrantSupport>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;
            SqlParameter param = null;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                string sql = "[Proc_GetAllProviderGrantSupport]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GrantSupport objGrantSupport = new GrantSupport();
                    objGrantSupport.OnDataBind(reader);
                    objGrantSupportList.Add(objGrantSupport);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objGrantSupportList;
        }


        public static void DeleteGrantSupport(int intGrantSupportID,int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@GrantSupportID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intGrantSupportID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                dbContext.ContextSqlCommand.CommandText = "[Proc_DeleteGrantSupport]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

    }
}
