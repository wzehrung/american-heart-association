﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class AdminUserLoginDetails
    {
        public static void CreateAdminUserLoginDetails(int intAdminUserID)
                                             
        {
            DBContext dbContext = null;
            bool bException = false;
          
            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intAdminUserID);


                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateAdminUserLoginDetails]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@AdminUserLoginDetailsID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                intAdminUserID = Convert.ToInt32(param_output.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
    }
}
