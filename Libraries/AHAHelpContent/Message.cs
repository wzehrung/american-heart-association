﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

//
// Note: folder types for some of the methods in here are the following (taken from Heart360 database Folder table
//      "I" - Inbox
//      "S" - Sent
//      "T" - Trash
//      "D" - Drafts (don't think we use this in V6
//

namespace AHAHelpContent
{
    public enum UserType
    {
        Patient,
        Provider
    }

    public class UserTypeDetails
    {
        public UserType UserType
        {
            get;
            set;
        }

        public int UserID
        {
            get;
            set;
        }
    }

    public class Message
    {
        public int MessageID
        {
            get;
            set;
        }

        public int ProviderID
        {
            get;
            set;
        }

        public int PatientID
        {
            get;
            set;
        }

        public DateTime SentDate
        {
            get;
            set;
        }

        public char MessageType
        {
            get;
            set;
        }

        public bool IsRead
        {
            get;
            set;
        }

        public bool IsFlaged
        {
            get;
            set;
        }

        public int? GroupID
        {
            get;
            set;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.MessageID = BasicConverter.DbToIntValue(reader["MessageID"]);
            this.ProviderID = BasicConverter.DbToIntValue(reader["ProviderID"]);
            this.GroupID = BasicConverter.DbToNullableIntValue(reader["GroupID"]);
            
            if (!this.GroupID.HasValue)
                this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);

            this.SentDate = BasicConverter.DbToDateValue(reader["SentDate"]);
            this.MessageType = BasicConverter.DbToCharValue(reader["MessageType"]);
            this.IsRead = BasicConverter.DbToBoolValue(reader["isRead"]);
            this.IsFlaged = BasicConverter.DbToBoolValue(reader["IsFlaged"]);            
        }

        public static int GetNumberOfUnreadMessagesForUser(UserTypeDetails obj, string strFolderType)
        {
            DBContext dbContext = null;
            int iNumberOfMessages = 0;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[PROC_GetNumberOfUnreadMessages]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(obj.UserID);

                param = cmd.Parameters.Add("@UserType", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(obj.UserType.ToString());

                param = cmd.Parameters.Add("@FolderType", SqlDbType.Char);
                param.Value = BasicConverter.CharToDbValue(Convert.ToChar(strFolderType));

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    iNumberOfMessages = Convert.ToInt32(reader[0]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iNumberOfMessages;
        }

        public static List<Message> GetAllMessagesByFolderForUser(UserTypeDetails obj, string strFolderType)
        {
            DBContext dbContext = null;
            List<Message> listMessage = new List<Message>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[PROC_GetAllMessagesByFolder]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(obj.UserID);

                param = cmd.Parameters.Add("@UserType", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(obj.UserType.ToString());

                param = cmd.Parameters.Add("@FolderType", SqlDbType.Char);
                param.Value = BasicConverter.CharToDbValue(Convert.ToChar(strFolderType));

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Message objMessage = new Message();
                    objMessage.OnDataBind(reader);
                    listMessage.Add(objMessage);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return listMessage;
        }

        public static Message GetMessageDetails(UserTypeDetails obj, string strFolderType, int iMessageID)
        {
            List<Message> list = GetAllMessagesByFolderForUser(obj, strFolderType);
            return list.SingleOrDefault(m => m.MessageID == iMessageID);
        }

        public static bool DeleteMessage(UserTypeDetails obj, int iMessageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            bool bRemoveFromHV = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iMessageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(obj.UserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserType", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(obj.UserType.ToString());

                param = dbContext.ContextSqlCommand.Parameters.Add("@deletefromHV", SqlDbType.Bit);
                param.Direction = ParameterDirection.Output;                

                dbContext.ContextSqlCommand.CommandText = "[PROC_DeleteMessage]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                bRemoveFromHV = BasicConverter.DbToBoolValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return bRemoveFromHV;
        }

        public static void MarkMessageAsReadOrUnread(UserTypeDetails obj, int iMessageID, bool bRead)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iMessageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(obj.UserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserType", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(obj.UserType.ToString());

                param = dbContext.ContextSqlCommand.Parameters.Add("@isRead", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bRead);

                dbContext.ContextSqlCommand.CommandText = "[PROC_MarkMessageAsReadOrUnRead]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void MarkMessageAsFlagedOrUnflaged(UserTypeDetails obj, int iMessageID, bool bFlaged)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iMessageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(obj.UserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserType", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(obj.UserType.ToString());

                param = dbContext.ContextSqlCommand.Parameters.Add("@isFlaged", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bFlaged);

                dbContext.ContextSqlCommand.CommandText = "[PROC_MarkMessageAsFlagedOrUnflaged]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void MoveMessageToFolder(UserTypeDetails obj, int iMessageID, string strFolderType)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iMessageID);

                 param = dbContext.ContextSqlCommand.Parameters.Add("@UserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(obj.UserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserType", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(obj.UserType.ToString());

                param = dbContext.ContextSqlCommand.Parameters.Add("@FolderType", SqlDbType.Char);
                param.Value = BasicConverter.CharToDbValue(Convert.ToChar(strFolderType));

                dbContext.ContextSqlCommand.CommandText = "[PROC_MoveMessageToFolder]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static int GetMessageCountByProviderIDAndDate(int iProviderID, DateTime dt)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetMessageCountByProviderIDAndDate]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@RequiredDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dt);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }

        public static int CreateMessage(int iFromUserID
            , int iToUserID
            , string strMessageType
            , bool bIsSentByProvider)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FromUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iFromUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ToUserID", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(iToUserID.ToString());

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageType", SqlDbType.Char);
                param.Value = BasicConverter.CharToDbValue(Convert.ToChar(strMessageType));

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsSentByProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsSentByProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = DBNull.Value;

                SqlParameter paramMessageID = dbContext.ContextSqlCommand.Parameters.Add("@MessageID", SqlDbType.Int);
                paramMessageID.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[PROC_CreateMessage]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                return BasicConverter.DbToIntValue(paramMessageID.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }


        public static int CreateMessage(int iFromUserID
            , string strToUserID
            , string strMessageType
            , bool bIsSentByProvider
            , int? iGroupID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FromUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iFromUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ToUserID", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strToUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@MessageType", SqlDbType.Char);
                param.Value = BasicConverter.CharToDbValue(Convert.ToChar(strMessageType));

                param = dbContext.ContextSqlCommand.Parameters.Add("@IsSentByProvider", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bIsSentByProvider);

                param = dbContext.ContextSqlCommand.Parameters.Add("@GroupID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(iGroupID);

                SqlParameter paramMessageID = dbContext.ContextSqlCommand.Parameters.Add("@MessageID", SqlDbType.Int);
                paramMessageID.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[PROC_CreateMessage]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                return BasicConverter.DbToIntValue(paramMessageID.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }


        public static int GetNewMessageCountForProvider(int iProviderID)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetNewMessageCountForProvider]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }
    }
}
