﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class MLCActionICanTake
    {
        public int? Rating
        {
            get;
            set;
        }
        public string RatingDisplayText
        {
            get;
            set;
        }
        public string ActionText
        {
            get;
            set;
        }

        /// <summary>
        /// Binds the data from the database to the corresponding variables
        /// </summary>
        /// <param name="objSource"></param>
        /// <returns></returns>

        private void OnDataBind(SqlDataReader reader)
        {
            this.Rating = BasicConverter.DbToNullableIntValue(reader["Rating"]);
            this.RatingDisplayText = BasicConverter.DbToStringValue(reader["RatingDisplayText"]);
            this.ActionText = BasicConverter.DbToStringValue(reader["ActionText"]);
            return;
        }

        public static List<MLCActionICanTake> GetActionICanTakeBasedOnTypeAndReading(int? intRating, TrackerDataType Tracker_DataType, int intLanguageID)
        {
            SqlDataReader reader = null;
            List<MLCActionICanTake> listMLCActionICanTake = new List<MLCActionICanTake>();
            DBContext dbContext = null;
            SqlParameter param = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Rating", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(intRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Type", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(Tracker_DataType.ToString());

                dbContext.ContextSqlCommand.CommandText = "[Proc_GetActionICanTakeBasedOnTypeandRating]";
                reader = dbContext.ContextSqlCommand.ExecuteReader();


                while (reader.Read())
                {
                    MLCActionICanTake objMLCActionICanTake = new MLCActionICanTake();
                    objMLCActionICanTake.OnDataBind(reader);
                    listMLCActionICanTake.Add(objMLCActionICanTake);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return listMLCActionICanTake;
        }
    }
}
