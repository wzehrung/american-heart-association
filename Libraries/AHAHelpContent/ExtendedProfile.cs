﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    [Serializable]
    public class PhoneNumbers
    {
        public string PhoneContact
        {
            get;
            set;
        }

        public string PhoneMobile
        {
            get;
            set;
        }

        public string PhoneSMS
        {
            get;
            set;
        }

        public int PhoneContactCountryCode
        {
            get;
            set;
        }

        public int PhoneMobileCountryCode
        {
            get;
            set;
        }

        public string TimeZone
        {
            get;
            set;
        }

        public bool IsPinValidated
        {
            get;
            set;
        }

        public bool IsTermsChecked
        {
            get;
            set;
        }

        public static PhoneNumbers GetPhoneNumbers(int iUserHealthRecordID)
        {
            DBContext dbContext = null;
            PhoneNumbers objPhoneNumbers = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                SqlParameter param = null;
                string sql = "[Proc_Get_PhoneNumbers]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iUserHealthRecordID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPhoneNumbers = new PhoneNumbers();
                    objPhoneNumbers.PhoneContactCountryCode = BasicConverter.DbToIntValue(reader["PhoneContactCountryCodeID"]);
                    objPhoneNumbers.PhoneContact = BasicConverter.DbToStringValue(reader["PhoneContact"]);
                    objPhoneNumbers.PhoneMobileCountryCode = BasicConverter.DbToIntValue(reader["PhoneMobileCountryCodeID"]);
                    objPhoneNumbers.PhoneMobile = BasicConverter.DbToStringValue(reader["PhoneMobile"]);
                    objPhoneNumbers.PhoneSMS = BasicConverter.DbToStringValue(reader["PhoneSMS"]);
                    objPhoneNumbers.TimeZone = BasicConverter.DbToStringValue(reader["TimeZone"]);
                    objPhoneNumbers.IsPinValidated = BasicConverter.DbToBoolValue(reader["PinValidated"]);
                    objPhoneNumbers.IsTermsChecked = BasicConverter.DbToBoolValue(reader["Terms"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objPhoneNumbers;
        }

        // Set the contact and mobile phone numbers from the new user or profile pages.
        public static void CreateOrChangePhoneNumbers(
            int iUserHealthRecordID,
            int iPhoneContactCountryCodeID,
            string sPhoneContact,
            int iPhoneMobileCountryCodeID,
            string sPhoneMobile)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iUserHealthRecordID);

                // Contact Phone Number Country Code
                if (iPhoneContactCountryCodeID.ToString().Length < 1)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContactCountryCodeID", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContactCountryCodeID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iPhoneContactCountryCodeID);
                }

                // Contact Phone Number
                if (string.IsNullOrEmpty(sPhoneContact))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContact", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContact", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sPhoneContact);
                }

                // Mobile Phone Number Country Code
                if (iPhoneMobileCountryCodeID.ToString().Length < 1)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobileCountryCodeID", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobileCountryCodeID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iPhoneMobileCountryCodeID);
                }

                // Mobile Phone Number
                if (string.IsNullOrEmpty(sPhoneMobile))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobile", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobile", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sPhoneMobile);
                }

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrUpdate_PhoneNumbers";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }    
    
        // Set or update the mobile number for SMS reminders
        public static void CreateOrChangeSMSNumber(
            int iUserHealthRecordID,
            string sPhoneMobileCountryCodeID,
            string sPhoneMobile,
            string sPin,
            string sTimeZone,
            string sDelete)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iUserHealthRecordID);

                // Mobile Phone Number Country Code
                // We require the country code, so default to 1 if none was passed
                if (sPhoneMobileCountryCodeID.Length < 1)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobileCountryCodeID", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue("1");
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobileCountryCodeID", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue(sPhoneMobileCountryCodeID);
                }

                // Mobile Phone Number
                if (string.IsNullOrEmpty(sPhoneMobile))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobile", SqlDbType.VarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobile", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue(sPhoneMobile);
                }
                
                // Delete is true if the user is either removing their mobile number from the profile page
                // or choosing to delete the number from the reminders page
                if (sDelete == "true")
                {   
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Delete", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue("true");

                    param = dbContext.ContextSqlCommand.Parameters.Add("@Pin", SqlDbType.VarChar);
                    param.Value = BasicConverter.StringToDbValue(sPin);

                    param = dbContext.ContextSqlCommand.Parameters.Add("@TimeZone", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sTimeZone);
                }             
                else
                {
                    // PIN
                    // 0 should never be passed, but just in case, default to 1234
                    if (String.IsNullOrEmpty(sPin))
                    {
                        param = dbContext.ContextSqlCommand.Parameters.Add("@Pin", SqlDbType.VarChar);
                        param.Value = BasicConverter.StringToDbValue("1234");
                    }
                    else
                    {
                        param = dbContext.ContextSqlCommand.Parameters.Add("@Pin", SqlDbType.VarChar);
                        param.Value = BasicConverter.StringToDbValue(sPin);
                    }

                    // TimeZone
                    // We require the time zone, default to US/Pacific if none was passed
                    if (string.IsNullOrEmpty(sTimeZone))
                    {
                        param = dbContext.ContextSqlCommand.Parameters.Add("@TimeZone", SqlDbType.NVarChar);
                        param.Value = BasicConverter.StringToDbValue("US/Pacific");
                    }
                    else
                    {
                        param = dbContext.ContextSqlCommand.Parameters.Add("@TimeZone", SqlDbType.NVarChar);
                        param.Value = BasicConverter.StringToDbValue(sTimeZone);
                    }

                    param = dbContext.ContextSqlCommand.Parameters.Add("@Delete", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue("false");
                }

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrUpdate_SMS_PhoneNumber";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }    
    }

    public class ExtendedProfile
    {
        // Saves health history data on the My Profile > My Health History page
        public static void CreateOrUpdateHealthHistory(
            int iUserHealthRecordID,
            string BloodType,
            string MedicationAllergy,
            bool? HighBPMedication,
            bool? HighBPDiet,
            bool? KidneyDisease,
            bool? DiabetesDiagnosis,
            bool? FamilyHistoryHD,
            bool? HeartDiagnosis,
            bool? FamilyHistoryStroke,
            bool? DoYouSmoke,
            bool? HaveYouEverTriedToQuitSmoking)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iUserHealthRecordID);

                if (FamilyHistoryHD.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryHD", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(FamilyHistoryHD.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryHD", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HighBPMedication.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPMedication", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HighBPMedication.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPMedication", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HighBPDiet.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPDiet", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HighBPDiet.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPDiet", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (KidneyDisease.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@KidneyDisease", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(KidneyDisease.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@KidneyDisease", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (DiabetesDiagnosis.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DiabetesDiagnosis", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(DiabetesDiagnosis.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DiabetesDiagnosis", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HeartDiagnosis.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HeartDiagnosis", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HeartDiagnosis.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HeartDiagnosis", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (string.IsNullOrEmpty(BloodType))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@BloodType", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@BloodType", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(BloodType);
                }

                if (string.IsNullOrEmpty(MedicationAllergy))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MedicationAllergy", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MedicationAllergy", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(MedicationAllergy);
                }

                if (FamilyHistoryStroke.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryStroke", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(FamilyHistoryStroke.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryStroke", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (DoYouSmoke.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DoYouSmoke", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(DoYouSmoke.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DoYouSmoke", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HaveYouEverTriedToQuitSmoking.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HaveYouEverTriedToQuitSmoking", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HaveYouEverTriedToQuitSmoking.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HaveYouEverTriedToQuitSmoking", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrUpdate_HealthHistory";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        // Saves health history data on the My Profile > Edit My Profile page
        public static void CreateOrUpdateMyProfile(
            int iUserHealthRecordID,
            string sCountry,
            string sGender,
            string sEthnicity,
            int? iYearOfBirth,
            string sZipCode,
            int iPhoneContactCountryCodeID,
            string sPhoneContact,
            int iPhoneMobileCountryCodeID,
            string sPhoneMobile
            )
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserHealthRecordID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iUserHealthRecordID);

                if (string.IsNullOrEmpty(sCountry))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Country", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Country", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sCountry);
                }

                if (string.IsNullOrEmpty(sGender))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Gender", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Gender", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sGender);
                }

                if (string.IsNullOrEmpty(sEthnicity))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sEthnicity);
                }

                if (!iYearOfBirth.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@YearOfBirth", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@YearOfBirth", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iYearOfBirth.Value);
                }

                if (string.IsNullOrEmpty(sZipCode))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sZipCode);
                }

                // Contact Phone Number Country Code
                if (iPhoneContactCountryCodeID.ToString().Length < 1)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContactCountryCodeID", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContactCountryCodeID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iPhoneContactCountryCodeID);
                }

                // Contact Phone Number
                if (string.IsNullOrEmpty(sPhoneContact))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContact", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneContact", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sPhoneContact);
                }

                // Mobile Phone Number Country Code
                if (iPhoneMobileCountryCodeID.ToString().Length < 1)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobileCountryCodeID", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobileCountryCodeID", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iPhoneMobileCountryCodeID);
                }

                // Mobile Phone Number
                if (string.IsNullOrEmpty(sPhoneMobile))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobile", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneMobile", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(sPhoneMobile);
                }

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrUpdate_MyProfile";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        // Original SP that we have split into sections
        public static void CreateOrChangeExtendedProfile(Guid gPersonalItemGUID, bool? FamilyHistoryHD, bool? FamilyHistoryStroke, bool? HighBPDiet, bool? HighBPMedication, bool? KidneyDisease, bool? DiabetesDiagnosis, bool? HeartDiagnosis, bool? Stroke, string PhysicalActivityLevel, string BloodType, string MedicationAllergy, double? TravelTimeToWorkInMinutes, string MeansOfTransportation, string AttitudesTowardsHealthcare, bool? DoYouSmoke, bool? HaveYouEverTriedToQuitSmoking, string Country, string Gender, string Ethnicity, string MaritalStatus, int? iYearOfBirth, string zipCode)//, DateTime? LastSynchDate)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                if (FamilyHistoryHD.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryHD", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(FamilyHistoryHD.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryHD", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (FamilyHistoryStroke.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryStroke", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(FamilyHistoryStroke.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@FamilyHistoryStroke", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HighBPDiet.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPDiet", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HighBPDiet.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPDiet", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HighBPMedication.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPMedication", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HighBPMedication.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HighBPMedication", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (KidneyDisease.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@KidneyDisease", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(KidneyDisease.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@KidneyDisease", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (DiabetesDiagnosis.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DiabetesDiagnosis", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(DiabetesDiagnosis.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DiabetesDiagnosis", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HeartDiagnosis.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HeartDiagnosis", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HeartDiagnosis.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HeartDiagnosis", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (Stroke.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Stroke", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(Stroke.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Stroke", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (string.IsNullOrEmpty(PhysicalActivityLevel))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhysicalActivityLevel", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@PhysicalActivityLevel", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(PhysicalActivityLevel);
                }

                if (string.IsNullOrEmpty(BloodType))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@BloodType", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@BloodType", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(BloodType);
                }

                if (string.IsNullOrEmpty(MedicationAllergy))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MedicationAllergy", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MedicationAllergy", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(MedicationAllergy);
                }

                if (TravelTimeToWorkInMinutes.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@TravelTimeToWorkInMinutes", SqlDbType.Float);
                    param.Value = BasicConverter.DoubleToDbValue(TravelTimeToWorkInMinutes.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@TravelTimeToWorkInMinutes", SqlDbType.Float);
                    param.Value = DBNull.Value;
                }

                if (string.IsNullOrEmpty(MeansOfTransportation))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MeansOfTransportation", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MeansOfTransportation", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(MeansOfTransportation);
                }

                if (string.IsNullOrEmpty(AttitudesTowardsHealthcare))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@AttitudesTowardsHealthcare", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@AttitudesTowardsHealthcare", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(AttitudesTowardsHealthcare);
                }

                if (DoYouSmoke.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DoYouSmoke", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(DoYouSmoke.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@DoYouSmoke", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (HaveYouEverTriedToQuitSmoking.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HaveYouEverTriedToQuitSmoking", SqlDbType.Bit);
                    param.Value = BasicConverter.BoolToDbValue(HaveYouEverTriedToQuitSmoking.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@HaveYouEverTriedToQuitSmoking", SqlDbType.Bit);
                    param.Value = DBNull.Value;
                }

                if (string.IsNullOrEmpty(Country))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Country", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Country", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(Country);
                }

                if (string.IsNullOrEmpty(Gender))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Gender", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Gender", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(Gender);
                }

                if (string.IsNullOrEmpty(Ethnicity))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Ethnicity", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(Ethnicity);
                }

                if (string.IsNullOrEmpty(MaritalStatus))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MaritalStatus", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@MaritalStatus", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(MaritalStatus);
                }

                if (!iYearOfBirth.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@YearOfBirth", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@YearOfBirth", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(iYearOfBirth.Value);
                }

                // Not retrieving or saving to HealthVault anymore
                //if (string.IsNullOrEmpty(phoneNumber))
                //{
                //    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar);
                //    param.Value = DBNull.Value;
                //}
                //else
                //{
                //    param = dbContext.ContextSqlCommand.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar);
                //    param.Value = BasicConverter.StringToDbValue(phoneNumber);
                //}
                if (string.IsNullOrEmpty(zipCode))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ZipCode", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(zipCode);
                }
                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeExtendedProfile";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// Sets last synchronized date 
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="LastSynchDate"></param>
        public static void SetLastSyncDate(Guid gPersonalItemGUID, DateTime LastSynchDate)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LastSynchDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(LastSynchDate);


                dbContext.ContextSqlCommand.CommandText = "Proc_ModifyExtendedProfile_LastSynchDate";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// Gets LastSyncDate for given PersonalItemGUID
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <returns>DateTime?</returns>
        public static DateTime? GetLastSyncDate(Guid gPersonalItemGUID)
        {

            DBContext dbContext = null;
            SqlDataReader reader = null;
            DateTime? retval = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);


                dbContext.ContextSqlCommand.CommandText = "Proc_GetLastSynchDateOfExtendedProfile";
                reader = dbContext.ContextSqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["LastSynchDate"] != DBNull.Value)
                        retval = Convert.ToDateTime(reader["LastSynchDate"]);
                }
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    dbContext.ReleaseDBContext(true);
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return retval;
        }

        /// <summary>
        /// Deletes ExtendedProfile data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        public static void DropExtendedProfile(Guid gPersonalItemGUID)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropExtendedProfile";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }
    }
}