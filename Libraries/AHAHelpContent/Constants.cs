﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AHAHelpContent
{
    //
    // These constants map to the UserType table in the Heart360 database.
    // In the Provider table, there is a UserTypeID field to specify provider vs. volunteer
    //
    public enum DbUserType
    {
        DbUserTypeUnknown = 0,
        DbUserTypeProvider = 1,
        DbUserTypeVolunteer = 2,
        DbUserTypePatient = 3
    }
    
}
