﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class PatientContent
    {
        public int PatientContentID
        {
            get;
            set;
        }
        public string ZoneTitle
        {
            get;
            set;
        }
        public string ContentTitle
        {
            get;
            set;
        }
        public string ContentTitleUrl
        {
            get;
            set;
        }
        public string ContentText
        {
            get;
            set;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.PatientContentID = BasicConverter.DbToIntValue(reader["PatientContentID"]);
            this.ZoneTitle = BasicConverter.DbToStringValue(reader["ZoneTitle"]);
            this.ContentTitle = BasicConverter.DbToStringValue(reader["ContentTitle"]);
            this.ContentTitleUrl = BasicConverter.DbToStringValue(reader["ContentTitleUrl"]);
            this.ContentText = BasicConverter.DbToStringValue(reader["ContentText"]);
        }

        public static void UpdateContent(
           int intPatientContentID,
           string strZoneTitle,
           string strContentTitle,
           string strContentTitleUrl,
           string strContentText,
           int UpdatedByUserID,
            int intLanguageID,
            int intProviderID=0)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientContentID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intPatientContentID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ZoneTitle", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strZoneTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitle", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitleUrl", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitleUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentText", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strContentText);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intProviderID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdatePatientContent]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangePatientContent]";


                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }


        public static PatientContent FindPatientContentByZoneTitle(string strZoneTitle, int intLanguageID, int intProviderID = 0)
        {
            DBContext dbContext = null;
            PatientContent objPatientContent = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientContentByZoneTitle]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ZoneTitle", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strZoneTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intProviderID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatientContent = new PatientContent();
                    objPatientContent.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientContent;
        }

        public static List<PatientContent> FindAllPatientContent()
        {
            DBContext dbContext = null;
            List<PatientContent> objContentList = new List<PatientContent>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_FindAllPatientContent]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    PatientContent objPatientContent = new PatientContent();
                    objPatientContent.OnDataBind(reader);
                    objContentList.Add(objPatientContent);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objContentList;
        }


        public static PatientContent FindPatientContentByID(int iPatientContentID, int intLanguageID)
        {
            DBContext dbContext = null;
            PatientContent objPatientContent = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindPatientContentByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientContentID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientContentID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objPatientContent = new PatientContent();
                    objPatientContent.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objPatientContent;
        }
 
    }
}
