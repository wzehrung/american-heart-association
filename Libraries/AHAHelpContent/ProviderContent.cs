﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class ProviderContent
    {
        #region Public Properties
        public int ContentID
        {
            get;
            set;
        }

        public string ZoneTitle
        {
            get;
            set;
        }

        public string ContentTitle
        {
            get;
            set;
        }

        public string ContentTitleUrl
        {
            get;
            set;
        }

        public string ContentText
        {
            get;
            set;
        }

        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.ContentID = BasicConverter.DbToIntValue(reader["ContentID"]);
            this.ZoneTitle = BasicConverter.DbToStringValue(reader["ZoneTitle"]);
            this.ContentTitle = BasicConverter.DbToStringValue(reader["ContentTitle"]);
            this.ContentTitleUrl = BasicConverter.DbToStringValue(reader["ContentTitleUrl"]);
            this.ContentText = BasicConverter.DbToStringValue(reader["ContentText"]);
        }

        public static void UpdateContent(
            int ContentID,
            string strZoneTitle,
            string strContentTitle,
            string strContentTitleUrl,
            string strContentText,
            int CreateUserID,
            int UpdatedByUserID,
            int LanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(ContentID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ZoneTitle", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strZoneTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitle", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentTitleUrl", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentTitleUrl);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ContentText", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strContentText);

                //param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                //param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CreatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(CreateUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateContent]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeProviderContent]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static ProviderContent FindContentByZoneTitle(string strZoneTitle,int LanguageID)
        {
            DBContext dbContext = null;
            ProviderContent objProviderContent = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindContentByZoneTitle]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ZoneTitle", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strZoneTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objProviderContent = new ProviderContent();
                    objProviderContent.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderContent;
        }

        public static List<ProviderContent> FindAllProviderContent(int LanguageID)
        {
            DBContext dbContext = null;
            List<ProviderContent> objContentList = new List<ProviderContent>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlParameter param = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                string sql = "[Proc_FindAllProviderContent]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderContent objProviderContent = new ProviderContent();
                    objProviderContent.OnDataBind(reader);
                    objContentList.Add(objProviderContent);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objContentList;
        }

        #endregion
    }
}
