﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{

    public class BloodGlucose
    {
        /// <summary>
        /// Create or update BloodGlucose data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="BloodGlucoseValue"></param>
        /// <param name="ActionITook"></param>
        /// <param name="ReadingType"></param>
        /// <param name="DateMeasured"></param>
        public static void CreateOrChangeBloodGlucose(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, double BloodGlucoseValue, string ActionITook, string ReadingType, DateTime DateMeasured, string strNotes, string ReadingSource)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();

                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BloodGlucoseValue", SqlDbType.Float);
                param.Value = BasicConverter.DoubleToDbValue(BloodGlucoseValue);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ActionITook", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(ActionITook);


                if (string.IsNullOrEmpty(ReadingSource))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(ReadingSource);
                }
                //newly added Reading Type
                if (string.IsNullOrEmpty(ReadingType))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingType", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingType", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(ReadingType);
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateMeasured", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(DateMeasured);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Notes", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strNotes);


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeBloodGlucose";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// Delete BloodGlucose data
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropBloodGlucose(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropBloodGlucose";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }


        public static void CreateOrChangeHbA1c(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, double HbA1cValue,  DateTime DateMeasured, string strNotes, string ReadingSource)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();

                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HbA1cValue", SqlDbType.Float);
                param.Value = BasicConverter.DoubleToDbValue(HbA1cValue);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateMeasured", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(DateMeasured);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Notes", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strNotes);

                if (string.IsNullOrEmpty(ReadingSource))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(ReadingSource);
                }

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeHbA1c";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }
    }
}
