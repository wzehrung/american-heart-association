﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using GRCBase;

namespace AHAHelpContent
{
    [Serializable]
    public class CampaignPartnerImage
    {
        public byte[] PartnerImage
        {
            get;
            set;
        }
        public string PartnerImageContentType
        {
            get;
            set;
        }

        public Guid? PartnerImageVersion
        {
            get;
            set;
        }
        public bool IsActive
        {
            get;
            set;
        }
    }

    [Serializable]
    public class CampaignPartnerLogo
    {
        public byte[] PartnerLogo
        {
            get;
            set;
        }

        public string PartnerLogoContentType
        {
            get;
            set;
        }

        public Guid? PartnerLogoVersion
        {
            get;
            set;
        }
        public bool IsActive
        {
            get;
            set;
        }
    }

    [Serializable]
    public class Campaign
    {
        #region Public Properties
        public int CampaignID
        {
            get;
            set;
        }

        public bool IsPublished
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string CampaignCode
        {
            get;
            set;
        }

        public string CampaignURL
        {
            get;
            set;
        }

        public string PromotionalTagLine
        {
            get;
            set;
        }

        public byte[] PartnerLogo
        {
            get;
            set;
        }

        public string PartnerLogoContentType
        {
            get;
            set;
        }

        public string PartnerLogoFileExtension
        {
            get;
            set;
        }

        public byte[] PartnerImage
        {
            get;
            set;
        }

        public string PartnerImageContentType
        {
            get;
            set;
        }

        public string PartnerImageFileExtension
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public int LanguageID
        {
            get;
            set;
        }

        public int BusinessID
        {
            get;
            set;
        }

        public string BusinessName
        {
            get;
            set;
        }

        public int AffiliateID
        {
            get;
            set;
        }

        public string AffiliateName
        {
            get;
            set;
        }

        public int Participants
        {
            get;
            set;
        }

        public Guid? PartnerLogoVersion
        {
            get;
            set;
        }

        public Guid? PartnerImageVersion
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public bool PublishAll
        {
            get;
            set;
        }
        public bool UnPublishAll
        {
            get;
            set;
        }


        //public bool IsDisabled
        //{
        //    get;
        //    set;
        //}

        public string ResourceTitle
        {
            get;
            set;
        }

        public string ResourceURL
        {
            get;
            set;
        }

        public string CampaignImages
        {
            get;
            set;
        }

        public string SearchByCampaignURL
        {
            get;
            set;
        }

        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
            this.Title = BasicConverter.DbToStringValue(reader["Title"]);
            this.Description = BasicConverter.DbToStringValue(reader["Description"]);
            this.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
            this.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
            this.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
            this.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
            this.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
            this.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
            this.PartnerImage = (reader["PartnerImage"] != DBNull.Value) ? (byte[])reader["PartnerImage"] : null;
            this.PartnerImageContentType = BasicConverter.DbToStringValue(reader["PartnerImageContentType"]);
            this.PartnerLogo = (reader["PartnerLogo"] != DBNull.Value) ? (byte[])reader["PartnerLogo"] : null;
            this.PartnerLogoContentType = BasicConverter.DbToStringValue(reader["PartnerLogoContentType"]);
            this.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
            this.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
            this.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
            this.ResourceTitle = BasicConverter.DbToStringValue(reader["ResourceTitle"]);
            this.ResourceURL = BasicConverter.DbToStringValue(reader["ResourceURL"]);

            this.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
            this.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);
        }
        #endregion

        #region Public Methods

        public static void UnPublishCampaign(int iCampaignID, int? intLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(intLanguageID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateCampaign]";
                dbContext.ContextSqlCommand.CommandText = "[proc_UnPublishCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static bool PublishCampaign(int iCampaignID, int? intLanguageID)
        {
            DBContext dbContext = null;
            bool bException = false;
            bool isPublished = false;
            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(intLanguageID);

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add("@IsPublished", SqlDbType.Bit);
                param_output.Direction = ParameterDirection.Output;


                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateCampaign]";
                dbContext.ContextSqlCommand.CommandText = "[proc_PublishCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
                isPublished = Convert.ToBoolean(param_output.Value);

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return isPublished;
        }

        public static void DisableCampaign(int iCampaignID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateCampaign]";
                dbContext.ContextSqlCommand.CommandText = "[proc_DisableCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void EnableCampaign(int iCampaignID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                //dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateCampaign]";
                dbContext.ContextSqlCommand.CommandText = "[proc_EnableCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void DeleteCampaign(int iCampaignID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_DeleteCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static CampaignPartnerLogo GetCampaignPartnerLogobyID(int iCampaignID, int LanguageID)
        {
            DBContext dbContext = null;
            CampaignPartnerLogo campaignPartnerLogo = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetCampaignPartnerLogoByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    campaignPartnerLogo = new CampaignPartnerLogo();
                    campaignPartnerLogo.PartnerLogo = (reader["PartnerLogo"] != DBNull.Value) ? (byte[])reader["PartnerLogo"] : null;
                    campaignPartnerLogo.PartnerLogoContentType = BasicConverter.DbToStringValue(reader["PartnerLogoContentType"]);
                    campaignPartnerLogo.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    campaignPartnerLogo.IsActive = BasicConverter.DbToBoolValue(reader["isActive"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return campaignPartnerLogo;
        }

        public static CampaignPartnerImage GetCampaignPartnerImageByID(int iCampaignID, int LanguageID)
        {
            DBContext dbContext = null;
            CampaignPartnerImage campaignPartnerImage = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetCampaignPartnerImageByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    campaignPartnerImage = new CampaignPartnerImage();
                    campaignPartnerImage.PartnerImage = (reader["PartnerImage"] != DBNull.Value) ? (byte[])reader["PartnerImage"] : null;
                    campaignPartnerImage.PartnerImageContentType = BasicConverter.DbToStringValue(reader["PartnerImageContentType"]);
                    campaignPartnerImage.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    campaignPartnerImage.IsActive = BasicConverter.DbToBoolValue(reader["isActive"]); 
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return campaignPartnerImage;
        }

        public static Campaign FindCampaignByCampaignID(int iCampaignID, int LanguageID)
        {
            DBContext dbContext = null;
            Campaign objCampaign = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindCampaignByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);
                    objCampaign.Description = BasicConverter.DbToStringValue(reader["Description"]);
                    objCampaign.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
                    objCampaign.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
                    objCampaign.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
                    objCampaign.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    objCampaign.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    objCampaign.IsActive = BasicConverter.DbToBoolValue(reader["isActive"]);
                    objCampaign.ResourceTitle = BasicConverter.DbToStringValue(reader["ResourceTitle"]);
                    objCampaign.ResourceURL = BasicConverter.DbToStringValue(reader["ResourceURL"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

                    objCampaign.AffiliateID = BasicConverter.DbToIntValue(reader["AffiliateID"]);
                    objCampaign.AffiliateName = BasicConverter.DbToStringValue(reader["AffiliateName"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaign;
        }

        public static Campaign FindCampaignByPatientID(int iPatientID, int LanguageID)
        {
            DBContext dbContext = null;
            Campaign objCampaign = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindCampaignByPatientID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPatientID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);
                    objCampaign.Description = BasicConverter.DbToStringValue(reader["Description"]);
                    objCampaign.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
                    objCampaign.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
                    objCampaign.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
                    objCampaign.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    objCampaign.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    objCampaign.IsActive = BasicConverter.DbToBoolValue(reader["isActive"]);
                    objCampaign.ResourceTitle = BasicConverter.DbToStringValue(reader["ResourceTitle"]);
                    objCampaign.ResourceURL = BasicConverter.DbToStringValue(reader["ResourceURL"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaign;
        }

        public static Campaign FindCampaignByProviderID(int iProvID, int LanguageID)
        {
            DBContext dbContext = null;
            Campaign objCampaign = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_FindCampaignByProviderID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProvID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);



                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);
                    objCampaign.Description = BasicConverter.DbToStringValue(reader["Description"]);
                    objCampaign.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
                    objCampaign.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
                    objCampaign.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
                    objCampaign.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    objCampaign.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    objCampaign.IsActive = BasicConverter.DbToBoolValue(reader["isActive"]);
                    objCampaign.ResourceTitle = BasicConverter.DbToStringValue(reader["ResourceTitle"]);
                    objCampaign.ResourceURL = BasicConverter.DbToStringValue(reader["ResourceURL"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaign;
        }

        public static bool DoesCampaignExistByURL(string strURL)
        {
            bool bIsValid = false;
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Url", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strURL);

                ////param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                ////param.Value = BasicConverter.IntToDbValue(LanguageID);


                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@isValid", SqlDbType.Bit));
                param_output.Direction = ParameterDirection.Output;



                dbContext.ContextSqlCommand.CommandText = "[Proc_DoesCampaignExists_Url]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                bIsValid = BasicConverter.DbToBoolValue(param_output.Value);

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return bIsValid;
        }

        public static bool DoesCampaignExistByTitle(string strTitle, int LanguageID)
        {
            bool bIsValid = false;
            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@isValid", SqlDbType.Bit));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.CommandText = "[Proc_DoesCampaignExists_Title]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                bIsValid = BasicConverter.DbToBoolValue(param_output.Value);

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return bIsValid;
        }

        public static List<Campaign> GetAffiliatesByCampaignID(int iCampaignID)
        {
            DBContext dbContext = null;
            List<Campaign> objCampaignList = new List<Campaign>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;
            SqlParameter param = null;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                string sql = "[Proc_Admin_GetAffiliatesByCampaignID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Campaign objCampaign = new Campaign();

                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.AffiliateID = BasicConverter.DbToIntValue(reader["AffiliateID"]);

                    objCampaignList.Add(objCampaign);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaignList;
        }

        public static List<Campaign> FindAllActiveCampaigns(int LanguageID, string sAffiliate, string sCampaignURL)
        {
            DBContext dbContext = null;
            List<Campaign> objCampaignList = new List<Campaign>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;
            SqlParameter param = null;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Affiliate", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sAffiliate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignURL", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sCampaignURL);

                string sql = "[Proc_FindAllActiveCampaign]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Campaign objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    //objCampaignList.Add(objCampaign);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);
                    objCampaign.Description = BasicConverter.DbToStringValue(reader["Description"]);
                    objCampaign.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
                    objCampaign.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
                    objCampaign.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
                    objCampaign.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    objCampaign.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    //objCampaign.PublishAll = BasicConverter.DbToBoolValue(reader["PublishToAll"]);
                    //objCampaign.UnPublishAll = BasicConverter.DbToBoolValue(reader["NotPublishToAll"]);
                    //objCampaign.IsDisabled = BasicConverter.DbToBoolValue(reader["IsDisabled"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

                    objCampaign.AffiliateID = BasicConverter.DbToIntValue(reader["AffiliateID"]);
                    objCampaign.AffiliateName = BasicConverter.DbToStringValue(reader["AffiliateName"]);

                    objCampaign.Participants = BasicConverter.DbToIntValue(reader["Participants"]);

                    objCampaignList.Add(objCampaign);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaignList;
        }

        public static List<Campaign> FindAllExpiredCampaigns(int LanguageID)
        {
            DBContext dbContext = null;
            List<Campaign> objCampaignList = new List<Campaign>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;
            SqlParameter param = null;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                string sql = "[Proc_FindAllExpiredCampaign]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Campaign objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    //objCampaignList.Add(objCampaign);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);
                    objCampaign.Description = BasicConverter.DbToStringValue(reader["Description"]);
                    objCampaign.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
                    objCampaign.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
                    objCampaign.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
                    objCampaign.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    objCampaign.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    //objCampaign.IsDisabled = BasicConverter.DbToBoolValue(reader["IsDisabled"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

                    objCampaignList.Add(objCampaign);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaignList;
        }

        public static List<Campaign> FindAllCampaignsInLast2Years(int LanguageID)
        {
            DBContext dbContext = null;
            List<Campaign> objCampaignList = new List<Campaign>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlParameter param = null;

            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.NullableIntToDbValue(LanguageID);

                string sql = "[Proc_getAllCampaigns]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Campaign objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

                    objCampaignList.Add(objCampaign);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaignList;
        }

        public static List<Campaign> FindBusinessAdminCampaignsInLast2Years( int BusinessID, int LanguageID)
        {
            DBContext dbContext = null;
            List<Campaign> objCampaignList = new List<Campaign>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlParameter param = null;

            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                //param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                //param.Value = BasicConverter.NullableIntToDbValue(LanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(BusinessID);

                string sql = "[Proc_GetCampaignsByBusinessID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Campaign objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.CampaignCode = BasicConverter.DbToStringValue(reader["CampaignCode"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

                    objCampaignList.Add(objCampaign);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaignList;
        }

        public static List<Campaign> SearchCampaigns(string strTitle, DateTime dtStartDate, DateTime dtEndDate, int LanguageID)
        {
            DBContext dbContext = null;
            List<Campaign> objCampaignList = new List<Campaign>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_SearchCampaign]";

                SqlParameter param = null;
                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStartDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEndDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Campaign objCampaign = new Campaign();
                    objCampaign.OnDataBind(reader);
                    objCampaignList.Add(objCampaign);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaignList;
        }

        public static void UpdateCampaignForPatient(Guid gPersonalItemGUID, int iCampaignID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = gPersonalItemGUID;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateCampaignForPatient]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void UpdateCampaignForProvider(int iProviderID, int iCampaignID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateCampaignForProvider]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        #endregion

        #region New or changed since 3/10/2014
        // WZ: New or changed since 3/10/2014

        // Used by create/update campaign page in admin portal
        public static Campaign GetCampaignByCampaignID(int iCampaignID, int LanguageID)
        {
            DBContext dbContext = null;
            Campaign objCampaign = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Admin_GetCampaignByID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objCampaign = new Campaign();
                    //objCampaign.OnDataBind(reader);
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);
                    objCampaign.Description = BasicConverter.DbToStringValue(reader["Description"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
                    objCampaign.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
                    objCampaign.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
                    objCampaign.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    objCampaign.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    objCampaign.IsActive = BasicConverter.DbToBoolValue(reader["isActive"]);
                    objCampaign.ResourceTitle = BasicConverter.DbToStringValue(reader["ResourceTitle"]);
                    objCampaign.ResourceURL = BasicConverter.DbToStringValue(reader["ResourceURL"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

                    objCampaign.CampaignImages = BasicConverter.DbToStringValue(reader["CampaignImages"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaign;
        }

        public static Campaign FindCampaignByURL(string strURL, int intLanguageID)
        {
            DBContext dbContext = null;
            Campaign objCampaign = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Admin_FindCampaignByUrl]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Url", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strURL);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(intLanguageID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objCampaign = new Campaign();
                    objCampaign.CampaignID = BasicConverter.DbToIntValue(reader["CampaignID"]);
                    objCampaign.Title = BasicConverter.DbToStringValue(reader["Title"]);
                    objCampaign.Description = BasicConverter.DbToStringValue(reader["Description"]);
                    objCampaign.CampaignURL = BasicConverter.DbToStringValue(reader["Url"]);
                    objCampaign.StartDate = BasicConverter.DbToDateValue(reader["StartDate"]);
                    objCampaign.EndDate = BasicConverter.DbToDateValue(reader["EndDate"]);
                    objCampaign.LanguageID = BasicConverter.DbToIntValue(reader["LanguageID"]);
                    objCampaign.PromotionalTagLine = BasicConverter.DbToStringValue(reader["PromotionalTagLine"]);
                    objCampaign.IsPublished = BasicConverter.DbToBoolValue(reader["IsPublished"]);
                    objCampaign.PartnerLogoVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerLogoVersion"]);
                    objCampaign.PartnerLogoContentType = BasicConverter.DbToStringValue(reader["PartnerLogoContentType"]);
                    objCampaign.PartnerImageVersion = BasicConverter.DbToNullableGuidValue(reader["PartnerImageVersion"]);
                    objCampaign.PartnerImageContentType = BasicConverter.DbToStringValue(reader["PartnerImageContentType"]);
                    objCampaign.IsActive = BasicConverter.DbToBoolValue(reader["isActive"]);

                    objCampaign.BusinessID = BasicConverter.DbToIntValue(reader["BusinessID"]);
                    objCampaign.BusinessName = BasicConverter.DbToStringValue(reader["BusinessName"]);

                    objCampaign.CampaignImages = BasicConverter.DbToStringValue(reader["CampaignImages"]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objCampaign;
        }

        public static Campaign CreateCampaign(
            string strTitle,
            string strDescription,
            string strCampaignCode,
            string strURL,
            DateTime dtStartDate,
            DateTime dtEndDate,
            int LanguageID,
            string strPromotionalTagLine,
            int iBusinessID,
            byte[] parterLogo,
            string strPartnerLogoContentType,
            byte[] partnerImage,
            string strPartnerImageContentType,
            string strRscTitle,
            string strRscURL,
            string strAffiliateID,
            string strImages)
        {
            DBContext dbContext = null;
            bool bException = false;
            int iCampaignID = 0;
            Campaign objCampaign = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Description", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strDescription);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignCode", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strCampaignCode);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Url", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strURL);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStartDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEndDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PromotionalTagLine", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPromotionalTagLine);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                if (iBusinessID != 0)
                    param.Value = BasicConverter.IntToDbValue(iBusinessID);
                else
                    param.Value = DBNull.Value;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerLogo", SqlDbType.Binary);
                if (parterLogo != null)
                {
                    param.Value = parterLogo;
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerLogoContentType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strPartnerLogoContentType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerImage", SqlDbType.Binary);
                if (partnerImage != null)
                {
                    param.Value = partnerImage;
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerImageContentType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strPartnerImageContentType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@strRscTitle", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strRscTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@strRscURL", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strRscURL);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AffiliateID", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strAffiliateID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@strImages", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strImages);

                // SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@CampaignID", SqlDbType.Int));
                // param_output.Direction = ParameterDirection.Output;

                // dbContext.ContextSqlCommand.CommandText = "[Proc_CreateCampaign]";

                // WZ - Deprecated
                // dbContext.ContextSqlCommand.CommandText = "[Proc_CreateOrChangeCampaign]";
                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_CreateCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                //iCampaignID = Convert.ToInt32(param_output.Value);
                objCampaign = new Campaign
                {
                    //CampaignID = iCampaignID,
                    Title = strTitle,
                    Description = strDescription,
                    CampaignCode = strCampaignCode,
                    CampaignURL = strURL,
                    StartDate = dtStartDate,
                    EndDate = dtEndDate,
                    BusinessID = iBusinessID,
                    BusinessName = string.Empty     // we don't know the business name in this context
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objCampaign;
        }

        public static void UpdateCampaign(
            int iCampaignID,
            string strTitle,
            string strDescription,
            DateTime dtStartDate,
            DateTime dtEndDate,
            int LanguageID,
            string strPromotionalTagLine,
            int iBusinessID,
            byte[] parterLogo,
            string strPartnerLogoContentType,
            byte[] partnerImage,
            string strPartnerImageContentType,
            string strRscTitle,
            string strRscURL,
            string strAffiliateID,
            string strImages)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Title", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Description", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strDescription);

                param = dbContext.ContextSqlCommand.Parameters.Add("@StartDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtStartDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EndDate", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(dtEndDate);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PromotionalTagLine", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strPromotionalTagLine);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BusinessID", SqlDbType.Int);
                if (iBusinessID > 0)
                    param.Value = BasicConverter.IntToDbValue(iBusinessID);
                else
                    param.Value = DBNull.Value;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerLogo", SqlDbType.Binary);
                if (parterLogo != null)
                {
                    param.Value = parterLogo;
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerLogoContentType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strPartnerLogoContentType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerImage", SqlDbType.Binary);
                if (partnerImage != null)
                {
                    param.Value = partnerImage;
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@PartnerImageContentType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strPartnerImageContentType);

                param = dbContext.ContextSqlCommand.Parameters.Add("@strRscTitle", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strRscTitle);

                param = dbContext.ContextSqlCommand.Parameters.Add("@strRscURL", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strRscURL);

                param = dbContext.ContextSqlCommand.Parameters.Add("@AffiliateID", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strAffiliateID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@strImages", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strImages);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_UpdateCampaign]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        #endregion
    }
}
