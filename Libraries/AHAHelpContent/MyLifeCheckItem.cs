﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class MyLifeCheckItem
    {
        public double HealthScore
        {
            get;
            set;
        }

        public int ActivityRating
        {
            get;
            set;
        }

        public int BloodSugarRating
        {
            get;
            set;
        }

        public int BPRating
        {
            get;
            set;
        }

        public int CholRating
        {
            get;
            set;
        }

        public int WeightRating
        {
            get;
            set;
        }

        public int DietRating
        {
            get;
            set;
        }

        public int SmokingRating
        {
            get;
            set;
        }

        public DateTime UpdatedDate
        {
            get;
            set;
        }

        public double BMI
        {
            get;
            set;
        }

        public double BloodGlucose
        {
            get;
            set;
        }

        public int TotalCholesterol
        {
            get;
            set;
        }

        public int SystolicBP
        {
            get;
            set;
        }

        public int DiastolicBP
        {
            get;
            set;
        }

        private void OnDataBind(SqlDataReader reader)
        {
            this.HealthScore = BasicConverter.DbToDoubleValue(reader["HealthScore"]);
            this.ActivityRating = BasicConverter.DbToIntValue(reader["ActivityRating"]);
            this.BloodSugarRating = BasicConverter.DbToIntValue(reader["BloodSugarRating"]);
            this.BPRating = BasicConverter.DbToIntValue(reader["BPRating"]);
            this.CholRating = BasicConverter.DbToIntValue(reader["CholRating"]);
            this.WeightRating = BasicConverter.DbToIntValue(reader["WeightRating"]);
            this.DietRating = BasicConverter.DbToIntValue(reader["DietRating"]);
            this.SmokingRating = BasicConverter.DbToIntValue(reader["SmokingRating"]);
            this.BMI = BasicConverter.DbToDoubleValue(reader["BMI"]);
            this.BloodGlucose = BasicConverter.DbToDoubleValue(reader["BloodGlucose"]);
            this.TotalCholesterol = BasicConverter.DbToIntValue(reader["TotalCholesterol"]);
            this.SystolicBP = BasicConverter.DbToIntValue(reader["SystolicBP"]);
            this.DiastolicBP = BasicConverter.DbToIntValue(reader["DiastolicBP"]);
            this.UpdatedDate = BasicConverter.DbToDateValue(reader["UpdatedDate"]);
        }

        public static void CreateMLCItem(Guid gPersonalItemGUID
            , double iHealthScore
            , int iActivityRating
            , int iBloodSugarRating
            , int iBPRating
            , int iCholRating
            , int iWeightRating
            , int iDietRating
            , int iSmokingRating
            , int iSystolic
            , int iDiastolic
            , int iTotalCholesterol
            , double dBloodGlucose
            , double dBMI
            )
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HealthScore", SqlDbType.Float);
                param.Value = BasicConverter.DoubleToDbValue(iHealthScore);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ActivityRating", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iActivityRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BloodSugarRating", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iBloodSugarRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BPRating", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iBPRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CholRating", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iCholRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@WeightRating", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iWeightRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DietRating", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iDietRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@SmokingRating", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iSmokingRating);

                param = dbContext.ContextSqlCommand.Parameters.Add("@SystolicBP", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iSystolic);

                param = dbContext.ContextSqlCommand.Parameters.Add("@DiastolicBP", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iDiastolic);

                param = dbContext.ContextSqlCommand.Parameters.Add("@TotalCholesterol", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iTotalCholesterol);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BloodGlucose", SqlDbType.Float);
                param.Value = BasicConverter.DoubleToDbValue(dBloodGlucose);

                param = dbContext.ContextSqlCommand.Parameters.Add("@BMI", SqlDbType.Float);
                param.Value = BasicConverter.DoubleToDbValue(dBMI);


                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateMLCItem]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static MyLifeCheckItem GetLatestMLCItem(Guid gPersonalItemGUID)
        {
            DBContext dbContext = null;
            bool bException = false;
            SqlDataReader reader = null;
            MyLifeCheckItem objMyLifeCheckItem = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_GetLatestMLCItem]";

                reader = dbContext.ContextSqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    objMyLifeCheckItem = new MyLifeCheckItem();
                    objMyLifeCheckItem.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objMyLifeCheckItem;
        }
    }
}
