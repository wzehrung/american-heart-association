﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using GRCBase;

namespace AHAHelpContent
{
    public class Feedback
    {
        #region Public Properties

        public int FeedbackTypeID
        {
            get;
            set;
        }

        public string FeedbackTypeName
        {
            get;
            set;
        }

        public int FeedbackID
        {
            get;
            set;
        }

        public string FeedbackText
        {
            get;
            set;
        }

        public int LanguageID
        {
            get;
            set;
        }

        public bool? Enabled
        {
            get;
            set;
        }

        public int AdminUserID
        {
            get;
            set;
        }

        public string AdminName
        {
            get;
            set;
        }

        public string AdminEmailAddress
        {
            get;
            set;
        }

        public bool Recipient
        {
            get;
            set;
        }

        #endregion

        #region Private Methods

        private void OnDataBind(SqlDataReader reader)
        {
            this.FeedbackTypeID = BasicConverter.DbToIntValue(reader["FeedbackTypeID"]);
            this.FeedbackTypeName = BasicConverter.DbToStringValue(reader["FeedbackTypeName"]);
            this.Enabled = BasicConverter.DbToBoolValue(reader["Enabled"]);
        }

        private void OnDataBind_AdminUsers(SqlDataReader reader)
        {
            this.AdminUserID = BasicConverter.DbToIntValue(reader["AdminUserID"]);
            this.AdminName = BasicConverter.DbToStringValue(reader["Name"]);
            this.AdminEmailAddress = BasicConverter.DbToStringValue(reader["Email"]);
            this.Recipient = BasicConverter.DbToBoolValue(reader["FeedbackRecipient"]);
        }

        private void OnDataBind_Recipients(SqlDataReader reader)
        {
            this.AdminEmailAddress = BasicConverter.DbToStringValue(reader["Email"]);
        }

        #endregion

        #region Public Methods

        public static List<Feedback> GetFeedbackTypes(bool bEnabled)
        {
            DBContext dbContext = null;
            Feedback objListItem = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            List<Feedback> objList = new List<Feedback>();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_Admin_Feedback_GetTypes]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Enabled", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bEnabled);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objListItem = new Feedback();
                    objListItem.OnDataBind(reader);
                    objList.Add(objListItem);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static Feedback CreateFeedbackType(string sFeedbackTypeName, bool bEnabled)
        {
            DBContext dbContext = null;
            bool bException = false;
            Feedback objFeedback = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FeedbackTypeName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sFeedbackTypeName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Enabled", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bEnabled);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_Feedback_CreateType]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                objFeedback = new Feedback
                {
                    FeedbackTypeName = sFeedbackTypeName,
                    Enabled = bEnabled
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objFeedback;
        }

        public static Feedback UpdateFeedbackType(string sFeedbackTypeName, int iFeedbackTypeID)
        {
            DBContext dbContext = null;
            bool bException = false;
            Feedback objFeedback = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FeedbackTypeName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sFeedbackTypeName);

                param = dbContext.ContextSqlCommand.Parameters.Add("@FeedbackTypeID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iFeedbackTypeID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_Feedback_UpdateType]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                objFeedback = new Feedback
                {
                    FeedbackTypeName = sFeedbackTypeName,
                    FeedbackTypeID = iFeedbackTypeID
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objFeedback;
        }

        public static Feedback EnableFeedbackType(int iFeedbackTypeID, bool bEnabled)
        {
            DBContext dbContext = null;
            bool bException = false;
            Feedback objFeedback = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FeedbackTypeID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iFeedbackTypeID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Enabled", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bEnabled);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_Feedback_EnableType]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                objFeedback = new Feedback
                {
                    FeedbackTypeID = iFeedbackTypeID,
                    Enabled = bEnabled
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objFeedback;
        }
     
        public static Feedback DeleteFeedbackType(int iFeedbackTypeID)
        {
            DBContext dbContext = null;
            bool bException = false;
            Feedback objFeedback = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FeedbackTypeID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iFeedbackTypeID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_Feedback_DeleteType]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objFeedback;
        }

        public static Feedback FeedbackInsert(string sFeedbackText, int iFeedbackTypeID)
        {
            DBContext dbContext = null;
            bool bException = false;
            Feedback objFeedback = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@FeedbackText", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(sFeedbackText);

                param = dbContext.ContextSqlCommand.Parameters.Add("@FeedbackTypeID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iFeedbackTypeID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_Feedback_Insert]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                objFeedback = new Feedback
                {
                    FeedbackTypeName = sFeedbackText
                };
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objFeedback;
        }

        public static List<Feedback> GetAdminUsers()
        {
            DBContext dbContext = null;
            Feedback objListItem = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            List<Feedback> objList = new List<Feedback>();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_Admin_Feedback_GetAdminUsers]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objListItem = new Feedback();
                    objListItem.OnDataBind_AdminUsers(reader);
                    objList.Add(objListItem);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static List<Feedback> GetFeedbackRecipients()
        {
            DBContext dbContext = null;
            Feedback objListItem = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            List<Feedback> objList = new List<Feedback>();
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_Admin_Feedback_GetRecipients]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;


                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    objListItem = new Feedback();
                    objListItem.OnDataBind_Recipients(reader);
                    objList.Add(objListItem);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objList;
        }

        public static Feedback AddFeedbackRecipients(int iAdminUserID, bool bRecipient)
        {
            DBContext dbContext = null;
            bool bException = false;
            Feedback objFeedback = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@AdminUserID", SqlDbType.Int);
                param.Value = BasicConverter.DbToIntValue(iAdminUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Recipient", SqlDbType.Bit);
                param.Value = BasicConverter.BoolToDbValue(bRecipient);

                dbContext.ContextSqlCommand.CommandText = "[Proc_Admin_Feedback_AddRecipients]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                //objFeedback = new Feedback
                //{
                //    FeedbackTypeName = sFeedbackText
                //};
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objFeedback;
        }

        #endregion
    }
}
