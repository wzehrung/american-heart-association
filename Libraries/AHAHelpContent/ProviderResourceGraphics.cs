﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data;
using System.Data.SqlClient;

namespace AHAHelpContent
{
    public class ProviderResourceGraphics
    {
        public int ProviderResourceGraphicsID
        {
            get;
            set;
        }
        public string Title
        {
            get;
            set;
        }
        public byte[] Graphics
        {
            get;
            set;
        }

        public string Version
        {
            get;
            set;
        }

        public string ImageURL
        {
            get;
            set;
        }
      
        private void OnDataBind(SqlDataReader reader)
        {
            this.ProviderResourceGraphicsID = BasicConverter.DbToIntValue(reader["ProviderResourceGraphicsID"]);
            this.Graphics = reader["Graphics"] != DBNull.Value ? (byte[])reader["Graphics"] : null;
            this.Title = BasicConverter.DbToStringValue(reader["Title"]);
            this.Version = BasicConverter.DbToStringValue(reader["Version"]);
            this.ImageURL = BasicConverter.DbToStringValue(reader["ImageURL"]);
        }
      
        
        public static void UpdateProviderResourceGraphics(int intProviderResourceGraphicsID,
                                             byte[] graphics, int UpdatedByUserID,int LanguageID,string ImageURL)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderResourceGraphicsID", SqlDbType.Int);
                param.Value = intProviderResourceGraphicsID;


                param = dbContext.ContextSqlCommand.Parameters.Add("@Graphics", SqlDbType.Binary);
                if (graphics != null)
                {
                    param.Value = graphics;
                }
                else
                {
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@UpdatedByUserID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(UpdatedByUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ImageURL", SqlDbType.NVarChar);
                param.Value = ImageURL;

                dbContext.ContextSqlCommand.CommandText = "[Proc_UpdateProviderResourceGraphics]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static List<ProviderResourceGraphics> GetProviderResourceGraphics(int LanguageID)
        {
            DBContext dbContext = null;
            List<ProviderResourceGraphics> objProviderResourceGraphicsList = new List<ProviderResourceGraphics>();

            SqlDataReader reader = null;
            SqlCommand cmd = null;
            SqlParameter param = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                param = dbContext.ContextSqlCommand.Parameters.Add("@LanguageID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LanguageID);

               
                string sql = "[Proc_GetProviderResourceGraphics]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ProviderResourceGraphics objProvResourceGraph = new ProviderResourceGraphics();
                    objProvResourceGraph.OnDataBind(reader);
                    objProviderResourceGraphicsList.Add(objProvResourceGraph);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objProviderResourceGraphicsList;
        }
    }
}
