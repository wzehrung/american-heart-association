﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GRCBase;
using System.Data;

namespace AHAHelpContent
{
    public class SiteErrorLog
    {
        #region Public Properties

        public int ErrorID
        {
            get;
            set;
        }
        public string ErrorText
        {
            get;
            set;
        }
        public string ErrorMessage
        {
            get;
            set;
        }
        public DateTime DateInserted
        {
            get;
            set;
        }

        #endregion

        #region Private Members

        private void OnDataBind(SqlDataReader reader)
        {
            this.ErrorID = BasicConverter.DbToIntValue(reader["ErrorID"]);
            this.ErrorMessage = BasicConverter.DbToStringValue(reader["ErrorMessage"]);
            this.ErrorText = BasicConverter.DbToStringValue(reader["ErrorText"]);
            this.DateInserted = BasicConverter.DbToDateValue(reader["DateInserted"]);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets all errors for the page
        /// </summary>
        /// <param name="iPageIndex">Index of the page</param>
        /// <param name="iPageSize">Number of records to be included in a page</param>
        /// <returns>List of errors</returns>
        public static List<SiteErrorLog> FindErrorLogs(int iPageIndex, int iPageSize)
        {
            DBContext dbContext = null;
            List<SiteErrorLog> listSiteErrorLog = new List<SiteErrorLog>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[proc_GetSiteErrorLog]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PageIndex", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPageIndex);

                param = dbContext.ContextSqlCommand.Parameters.Add("@PageSize", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPageSize);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    SiteErrorLog objLog = new SiteErrorLog();
                    objLog.OnDataBind(reader);
                    listSiteErrorLog.Add(objLog);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return listSiteErrorLog;
        }

        /// <summary>
        /// Gets the total count of page for the given size
        /// </summary>
        /// <param name="iPageSize">Number of recordes to be included in a page</param>
        /// <returns>Count of page(s)</returns>
        public static int GetPageCountBySize(int iPageSize)
        {
            DBContext dbContext = null;
            int iCount = 0;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[proc_GetSiteErrorLogTotalPageCount]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PageSize", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iPageSize);

                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    iCount = BasicConverter.DbToIntValue(reader[0]);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iCount;
        }

        #endregion
    }
}
