﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;


namespace AHAHelpContent
{


    public class WeightDetails
    {

        /// <summary>
        /// Creates or updates Weight data 
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="Weight"></param>
        /// <param name="ReadingSource"></param>
        /// <param name="Comments"></param>
        /// <param name="DateMeasured"></param>
        public static void CreateOrChangeWeight(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, double Weight, string ReadingSource, string Comments, DateTime DateMeasured)//, DateTime? LastUpdated)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Weight", SqlDbType.Float);
                param.Value = BasicConverter.NullableDoubleToDbValue(Weight);

                

                if (!string.IsNullOrEmpty(ReadingSource))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(ReadingSource);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }

                if (!string.IsNullOrEmpty(Comments))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(Comments);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateMeasured", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(DateMeasured);


                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeWeight";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// Delete Weight
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropWeight(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropWeight";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

    }
}
