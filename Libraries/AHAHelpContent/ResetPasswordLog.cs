﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class ResetPasswordLog
    {
        #region Public Properties
        public int PasswordResetID
        {
            get;
            set;
        }

        public Guid ResetGUID
        {
            get;
            set;
        }

        public int ProviderID
        {
            get;
            set;
        }

        public DateTime CreatedDate
        {
            get;
            set;
        }

        public DateTime? ResetedDate
        {
            get;
            set;
        }
        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.PasswordResetID = BasicConverter.DbToIntValue(reader["PasswordResetID"]);
            this.ResetGUID = BasicConverter.DbToGuidValue(reader["ResetGUID"]);
            this.ProviderID = BasicConverter.DbToIntValue(reader["ProviderID"]);
            this.CreatedDate = BasicConverter.DbToDateValue(reader["CreatedDate"]);
            this.ResetedDate = BasicConverter.DbToNullableDateValue(reader["ResetedDate"]);
        }
        #endregion

        #region Public Methods

        public static ResetPasswordLog CreateLog(Guid gResetGUID, int iProviderID)
        {
            DBContext dbContext = null;
            bool bException = false;
            ResetPasswordLog objResetPasswordLog = null;
            int iLogID;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ResetGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gResetGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateResetPasswordLog]";

                SqlParameter param_output = dbContext.ContextSqlCommand.Parameters.Add(new SqlParameter("@PasswordResetID", SqlDbType.Int));
                param_output.Direction = ParameterDirection.Output;

                dbContext.ContextSqlCommand.ExecuteNonQuery();

                iLogID = Convert.ToInt32(param_output.Value);
                objResetPasswordLog = new ResetPasswordLog
                    {
                        ProviderID = iProviderID,
                        CreatedDate = DateTime.Now,
                        PasswordResetID = iLogID,
                        ResetGUID = gResetGUID
                    };

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            return objResetPasswordLog;
        }

        public static void SetPasswordResetForResetGUID(Guid gResetGUID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ResetGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gResetGUID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_SetResetDate]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();

            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static ResetPasswordLog GetLogByResetGUIDAndProviderUserName(Guid gResetGUID, string strUserName)
        {
            DBContext dbContext = null;
            ResetPasswordLog objResetPasswordLog = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetLogByResetGUIDAndProviderUserName]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ResetGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gResetGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserName", SqlDbType.VarChar);
                param.Value = BasicConverter.StringToDbValue(strUserName);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objResetPasswordLog = new ResetPasswordLog();
                    objResetPasswordLog.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResetPasswordLog;
        }

        public static ResetPasswordLog GetLogByResetGUID(Guid gResetGUID)
        {
            DBContext dbContext = null;
            ResetPasswordLog objResetPasswordLog = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_GetLogByResetGUID]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ResetGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gResetGUID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objResetPasswordLog = new ResetPasswordLog();
                    objResetPasswordLog.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResetPasswordLog;
        }

        public static ResetPasswordLog FindByID(int iResetPasswordLogID)
        {
            DBContext dbContext = null;
            ResetPasswordLog objResetPasswordLog = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ResetPasswordLogID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iResetPasswordLogID);


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    objResetPasswordLog = new ResetPasswordLog();
                    objResetPasswordLog.OnDataBind(reader);
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return objResetPasswordLog;
        }

        #endregion
    }
}
