﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{

    public class BloodPressure
    {
        /// <summary>
        /// Creates or updates BloodPressure data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="Systolic"></param>
        /// <param name="Diastolic"></param>
        /// <param name="Pulse"></param>
        /// <param name="ReadingSource"></param>
        /// <param name="Comments"></param>
        /// <param name="DateMeasured"></param>
        public static void CreateOrChangeBloodPressure(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, int Systolic, int Diastolic, int? Pulse, string ReadingSource, string Comments, DateTime DateMeasured, bool? IrregularHeartbeat)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Systolic", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(Systolic);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Diastolic", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(Diastolic);

                if (Pulse.HasValue)
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Pulse", SqlDbType.Int);
                    param.Value = BasicConverter.IntToDbValue(Pulse.Value);
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Pulse", SqlDbType.Int);
                    param.Value = DBNull.Value;
                }

                if (string.IsNullOrEmpty(ReadingSource))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@ReadingSource", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(ReadingSource);
                }

                if (string.IsNullOrEmpty(Comments))
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.NVarChar);
                    param.Value = DBNull.Value;
                }
                else
                {
                    param = dbContext.ContextSqlCommand.Parameters.Add("@Comments", SqlDbType.NVarChar);
                    param.Value = BasicConverter.StringToDbValue(Comments);
                }

                param = dbContext.ContextSqlCommand.Parameters.Add("@DateMeasured", SqlDbType.DateTime);
                param.Value = BasicConverter.DateToDbValue(DateMeasured);

                param = param = dbContext.ContextSqlCommand.Parameters.Add("@IrregularHeartbeat", SqlDbType.Bit);
                param.Value = BasicConverter.NullableBoolToDbValue(IrregularHeartbeat);

                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeBloodPressure";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

        /// <summary>
        /// Deletes BloodPressure data
        /// </summary>
        /// <param name="HVItemGuid"></param>
        public static void DropBloodPressure(Guid HVItemGuid)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);


                dbContext.ContextSqlCommand.CommandText = "Proc_DropBloodPressure";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

    }
}
