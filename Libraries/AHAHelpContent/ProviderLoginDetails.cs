﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class ProviderLoginDetails
    {
        #region Public Properties
        public int LoginDetailsID
        {
            get;
            set;
        }

        public int ProviderID
        {
            get;
            set;
        }

        public DateTime LoggedInDate
        {
            get;
            set;
        }
        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader)
        {
            this.LoginDetailsID = BasicConverter.DbToIntValue(reader["LoginDetailsID"]);
            this.ProviderID = BasicConverter.DbToIntValue(reader["ProviderID"]);
            this.LoggedInDate = BasicConverter.DbToDateValue(reader["LoggedInDate"]);
        }
        #endregion
        #region Public Methods
        public static void CreateLoginDetails(int iProviderID)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateProviderLoginDetails]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {

                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static int GetProviderLoginCount(int iProviderID)
        {
            DBContext dbContext = null;
            int iResult;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = "[Proc_GetCountofProviderLogin]";
                cmd.CommandType = CommandType.StoredProcedure;

                param = cmd.Parameters.Add("@ProviderID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(iProviderID);

                param = cmd.Parameters.Add("@Count", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                iResult = BasicConverter.DbToIntValue(param.Value);
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            return iResult;
        }
        #endregion
    }
}
