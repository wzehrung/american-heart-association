﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace AHAHelpContent
{
    public class EmailDespatchDetails
    {
        public static void CreateDespatchDetails(int? iUserID, int? iProviderID, int? iCampaignID, string strEmailType)
        {
            DBContext dbContext = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@UserID", SqlDbType.NVarChar);
                param.Value = BasicConverter.NullableIntToDbValue(iUserID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@ProviderID", SqlDbType.NVarChar);
                param.Value = BasicConverter.NullableIntToDbValue(iProviderID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@CampaignID", SqlDbType.NVarChar);
                param.Value = BasicConverter.NullableIntToDbValue(iCampaignID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@EmailType", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(strEmailType);

                dbContext.ContextSqlCommand.CommandText = "[Proc_CreateEmailDespatchDetails]";

                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }
    }
}
