﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GRCBase;

namespace AHAHelpContent
{
    public class CholesterolGoal
    {
        /// <summary>
        /// Creates or updates CholesterolGoal data
        /// </summary>
        /// <param name="PersonalItemGUID"></param>
        /// <param name="HVItemGuid"></param>
        /// <param name="HVVersionStamp"></param>
        /// <param name="Totalcholesterol"></param>
        /// <param name="LDL"></param>
        /// <param name="HDL"></param>
        /// <param name="Triglyceride"></param>
        public static void CreateOrChangeCholesterolGoal(Guid gPersonalItemGUID, Guid HVItemGuid, Guid HVVersionStamp, int Totalcholesterol, int LDL, int HDL, int Triglyceride)
        {
            DBContext dbContext = null;

            try
            {
                dbContext = DBContext.GetDBContext();
                dbContext.ContextSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PersonalItemGUID", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(gPersonalItemGUID);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVItemGuid", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVItemGuid);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HVVersionStamp", SqlDbType.UniqueIdentifier);
                param.Value = BasicConverter.GuidToDbValue(HVVersionStamp);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Totalcholesterol", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(Totalcholesterol);

                param = dbContext.ContextSqlCommand.Parameters.Add("@LDL", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(LDL);

                param = dbContext.ContextSqlCommand.Parameters.Add("@HDL", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(HDL);

                param = dbContext.ContextSqlCommand.Parameters.Add("@Triglyceride", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(Triglyceride);



                dbContext.ContextSqlCommand.CommandText = "Proc_CreateOrChangeCholesterolGoal";
                dbContext.ContextSqlCommand.ExecuteNonQuery();
                dbContext.ReleaseDBContext(true);
            }
            catch
            {
                if (dbContext != null)
                {
                    dbContext.ReleaseDBContext(false);
                }
                throw;
            }
            return;
        }

    }
}
