﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    //
    // PJB (11.18.2013): Note that the state of Arizona was not handled properly in the system previous to the creation of this file.
    // The Database value used was "US/Mountain (AZ no daylight savings)", but there is no TimeZone for that in the timezone_offsets table in Heart360.
    // Not sure why it was done this way.
    // I configured it so that Arizona is part of "US/Mountain"
    //
    // There is also logic in SMS Reminders (see 3CiDataManagerSend/SendAPI.cs) that builds dynamic queries using time zones.
    //
    //
namespace AHAHelpContent
{
    public enum TimeZone
    {
        US_EASTERN = 0,
        US_CENTRAL,
        US_MOUNTAIN,
        US_MOUNTAIN_AZ, // special case, they don't observe daylight savings time
        US_PACIFIC,
        US_ALASKA,
        US_HAWAII
    } ;

    public class TimeZoneNames
    {
            //
            // this returns the string that should be used for storing in the Heart360 database,
            // like things for Patient Portal SMS Reminders.
        public static string GetTimeZoneDbName( TimeZone tz)
        {
            string  result = "Unknown" ;

            switch( tz )
            {
                case TimeZone.US_ALASKA:
                    result = "US/Alaska" ;
                    break ;

                case TimeZone.US_CENTRAL:
                    result = "US/Central" ;
                    break ;

                case TimeZone.US_EASTERN:
                    result = "US/Eastern" ;
                    break ;

                case TimeZone.US_HAWAII:
                    result = "US/Hawaii" ;
                    break ;

                case TimeZone.US_MOUNTAIN:
                    result = "US/Mountain" ;
                    break ;

                case TimeZone.US_MOUNTAIN_AZ:
                    result = "US/Mountain" ;    // "US/Mountain (AZ no daylight savings)" ;
                    break ;

                case TimeZone.US_PACIFIC:
                    result = "US/Pacific" ;
                    break ;

                default:
                    break ;
            }

            return result ;
        }

            // _langId: 1 = english, 2 - spanish
        public static string GetTimeZoneForUX(TimeZone tz, int _langId)
        {
            //
            // TODO: this should go into a resource file!
            //

            string result = "Unknown";

            switch (tz)
            {
                case TimeZone.US_ALASKA:
                    result = "Alaska";
                    break;

                case TimeZone.US_CENTRAL:
                    result = "Central";
                    break;

                case TimeZone.US_EASTERN:
                    result = "Eastern";
                    break;

                case TimeZone.US_HAWAII:
                    result = "Hawaii";
                    break;

                case TimeZone.US_MOUNTAIN:
                    result = "Mountain";
                    break;

                case TimeZone.US_MOUNTAIN_AZ:
                    result = "Mountain (AZ no daylight savings)";
                    break;

                case TimeZone.US_PACIFIC:
                    result = "Pacific";
                    break;

                default:
                    break;
            }

            return result;


        }
    }
}
