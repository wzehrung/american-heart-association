﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace GroupManager
{
    public class Rule
    {
        public RuleType RuleType
        {
            get;
            set;
        }

        public bool IsAverage
        {
            get;
            set;
        }

        public int PeriodCount
        {
            get;
            set;
        }

        public PeriodType PeriodType
        {
            get;
            set;
        }

        public int? LastNReadings
        {
            get;
            set;
        }

        public double? MinValue
        {
            get;
            set;
        }

        public double? MaxValue
        {
            get;
            set;
        }

        public Rule(RuleType eRuleType
            , bool bIsAverage
            , int iPeriodCount
            , PeriodType ePeriodType
            , int? iLastNReadings
            , double? dMinValue
            , double? dMaxValue)
        {
            RuleType = eRuleType;
            IsAverage = bIsAverage;
            PeriodCount = iPeriodCount;
            PeriodType = ePeriodType;
            LastNReadings = iLastNReadings;
            MinValue = dMinValue;
            MaxValue = dMaxValue;
        }

        public XElement GetRuleXElement()
        {
            return new XElement("Rule", new XAttribute("Type", RuleType.ToString())
                                        , new XAttribute("IsAverage", IsAverage ? true.ToString() : false.ToString())
                                        , new XAttribute("PeriodCount", PeriodCount.ToString())
                                        , new XAttribute("PeriodType", PeriodType.ToString())
                                        , new XAttribute("LastNReadings", LastNReadings.HasValue ? LastNReadings.ToString() : string.Empty)
                                        , new XAttribute("MinValue", MinValue.HasValue ? MinValue.ToString() : string.Empty)
                                        , new XAttribute("MaxValue", MaxValue.HasValue ? MaxValue.ToString() : string.Empty));
        }

        public string GetRuleXml()
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Rule", new XAttribute("Type", RuleType.ToString())
                                       , new XAttribute("IsAverage", IsAverage ? true.ToString() : false.ToString())
                                       , new XAttribute("PeriodCount", PeriodCount.ToString())
                                       , new XAttribute("PeriodType", PeriodType.ToString())
                                       , new XAttribute("LastNReadings", LastNReadings.HasValue ? LastNReadings.ToString() : string.Empty)
                                       , new XAttribute("MinValue", MinValue.HasValue ? MinValue.ToString() : string.Empty)
                                       , new XAttribute("MaxValue", MaxValue.HasValue ? MaxValue.ToString() : string.Empty)));

            return xDoc.ToString();
        }

        public static Rule ParseRuleXml(string strXml)
        {
            if (string.IsNullOrEmpty(strXml))
                return null;

            XDocument xDoc = XDocument.Parse(strXml);

            string strType = xDoc.Root.Attribute("Type").Value;
            RuleType eType = (RuleType)Enum.Parse(typeof(RuleType), strType, false);

            bool bIsAverage = Boolean.Parse(xDoc.Root.Attribute("IsAverage").Value);
            int iPeriodCount = Convert.ToInt32(xDoc.Root.Attribute("PeriodCount").Value);

            PeriodType ePeriodType = (PeriodType)Enum.Parse(typeof(PeriodType), xDoc.Root.Attribute("PeriodType").Value, false);

            int? iLastNReadings = null;
            if (!string.IsNullOrEmpty(xDoc.Root.Attribute("LastNReadings").Value))
            {
                iLastNReadings = Convert.ToInt32(xDoc.Root.Attribute("LastNReadings").Value);
            }

            int? dMinValue = null;
            if (!string.IsNullOrEmpty(xDoc.Root.Attribute("MinValue").Value))
            {
                dMinValue = Convert.ToInt32(xDoc.Root.Attribute("MinValue").Value);
            }

            int? dMaxValue = null;
            if (!string.IsNullOrEmpty(xDoc.Root.Attribute("MaxValue").Value))
            {
                dMaxValue = Convert.ToInt32(xDoc.Root.Attribute("MaxValue").Value);
            }

            return new Rule(eType, bIsAverage, iPeriodCount, ePeriodType, iLastNReadings, dMinValue, dMaxValue);
        }
    }
}
