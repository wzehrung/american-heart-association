﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.Health;
using AHAHelpContent;

namespace GroupManager
{
    public class GroupRuleManager
    {
        public static bool DoesPatientQualifyForRule(Patient objPatient, Dictionary<Guid, List<HealthRecordItem>> allResults, GroupRule groupRule)
        {
            bool bDoesQualify = false;

            if (groupRule.Age != null && groupRule.Age.IsAbove.HasValue)
            {
                bDoesQualify = false;
                if (allResults.ContainsKey(Microsoft.Health.ItemTypes.Personal.TypeId))
                {
                    AgeRuleManager arm = new AgeRuleManager();
                    IEnumerable<Microsoft.Health.ItemTypes.Personal> listPersonal = allResults[Microsoft.Health.ItemTypes.Personal.TypeId].Cast<Microsoft.Health.ItemTypes.Personal>();
                    Microsoft.Health.ItemTypes.Personal personalItem = null;
                    if (listPersonal.Count() > 0)
                    {
                        personalItem = listPersonal.FirstOrDefault();
                    }
                    bDoesQualify = arm.DoesPatientQualifyForRule(personalItem, groupRule.Age.IsAbove.Value, groupRule.Age.Value);
                }
                if (!bDoesQualify)
                    return false;
            }

            if (groupRule.Gender != null && groupRule.Gender.IsMale.HasValue)
            {
                bDoesQualify = false;
                if (allResults.ContainsKey(Microsoft.Health.ItemTypes.BasicV2.TypeId))
                {
                    GenderRuleManager grm = new GenderRuleManager();

                    IEnumerable<Microsoft.Health.ItemTypes.BasicV2> listBasic = allResults[Microsoft.Health.ItemTypes.BasicV2.TypeId].Cast<Microsoft.Health.ItemTypes.BasicV2>();

                    Microsoft.Health.ItemTypes.BasicV2 basicItem = null;
                    if (listBasic.Count() > 0)
                    {
                        basicItem = listBasic.FirstOrDefault();
                    }

                    bDoesQualify = grm.DoesPatientQualifyForRule(basicItem, groupRule.Gender.IsMale.Value);
                }
                if (!bDoesQualify)
                    return false;
            }

            foreach (Rule objRule in groupRule.RuleList)
            {
                switch (objRule.RuleType)
                {
                    case RuleType.Systolic:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.BloodPressure.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            SystolicRuleManager srm = new SystolicRuleManager(objRule);
                            bDoesQualify = srm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.BloodPressure.TypeId]);
                        }
                        break;
                    case RuleType.Diastolic:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.BloodPressure.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
                            bDoesQualify = drm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.BloodPressure.TypeId]);
                        }
                        break;
                    case RuleType.TotalCholesterol:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            TotalCholesterolRuleManager tcrm = new TotalCholesterolRuleManager(objRule);
                            bDoesQualify = tcrm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId]);
                        }
                        break;
                    case RuleType.HDL:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            HDLRuleManager hdlrm = new HDLRuleManager(objRule);
                            bDoesQualify = hdlrm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId]);
                        }
                        break;
                    case RuleType.LDL:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            LDLRuleManager ldlrm = new LDLRuleManager(objRule);
                            bDoesQualify = ldlrm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId]);
                        }
                        break;
                    case RuleType.Triglyceride:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            TriglycerideRuleManager trm = new TriglycerideRuleManager(objRule);
                            bDoesQualify = trm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId]);
                        }
                        break;
                    case RuleType.BloodGlucose:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.BloodGlucose.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            BloodGlucoseRuleManager bgrm = new BloodGlucoseRuleManager(objRule);
                            bDoesQualify = bgrm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.BloodGlucose.TypeId]);
                        }
                        break;
                    case RuleType.Weight:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.Weight.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            WeightRuleManager wrm = new WeightRuleManager(objRule);
                            bDoesQualify = wrm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.Weight.TypeId]);
                        }
                        break;
                    case RuleType.BMI:
                        if (!allResults.ContainsKey(Microsoft.Health.ItemTypes.Weight.TypeId)
                            || !allResults.ContainsKey(Microsoft.Health.ItemTypes.Height.TypeId))
                        {
                            bDoesQualify = false;
                        }
                        else
                        {
                            BMIRuleManager bmirm = new BMIRuleManager(objRule);
                            bDoesQualify = bmirm.DoesPatientQualifyForRule(allResults[Microsoft.Health.ItemTypes.Weight.TypeId], allResults[Microsoft.Health.ItemTypes.Height.TypeId]);
                        }
                        break;
                    case RuleType.ActivePatient:
                        ActivePatientRuleManager aprm = new ActivePatientRuleManager(objRule);
                        bDoesQualify = aprm.DoesPatientQualifyForRule(objPatient);
                        break;
                    case RuleType.InactivePatient:
                        InactivePatientRuleManager iprm = new InactivePatientRuleManager(objRule);
                        bDoesQualify = iprm.DoesPatientQualifyForRule(objPatient);
                        break;
                    case RuleType.PatientConnection:
                        PatientProviderConnectRuleManager ppcrm = new PatientProviderConnectRuleManager(objRule);
                        bDoesQualify = ppcrm.DoesPatientQualifyForRule(objPatient);
                        break;
                }

                if (!bDoesQualify)
                    return false;
            }

            return true;
        }

        public static List<int> GetPatientsQualifyingForGroupRule(int iProviderID, GroupRule groupRule)
        {
            List<AHAHelpContent.Patient> list = new List<AHAHelpContent.Patient>();

            List<AHAHelpContent.Patient> listProviderPatient = AHAHelpContent.Patient.FindAllByProviderID(iProviderID);
            bool bDoesQualify = false;

            foreach (AHAHelpContent.Patient objPatient in listProviderPatient)
            {
                if (groupRule.Age != null && groupRule.Age.IsAbove.HasValue)
                {
                    AgeRuleManager arm = new AgeRuleManager();
                    bDoesQualify = arm.DoesPatientQualifyForRule(objPatient, groupRule.Age.IsAbove.Value, groupRule.Age.Value);
                    if (!bDoesQualify)
                        continue;
                }

                if (groupRule.Gender != null && groupRule.Gender.IsMale.HasValue)
                {
                    GenderRuleManager grm = new GenderRuleManager();
                    bDoesQualify = grm.DoesPatientQualifyForRule(objPatient, groupRule.Gender.IsMale.Value);
                    if (!bDoesQualify)
                        continue;
                }

                bDoesQualify = false;
                foreach (Rule objRule in groupRule.RuleList)
                {
                    switch (objRule.RuleType)
                    {
                        case RuleType.Systolic:
                            SystolicRuleManager srm = new SystolicRuleManager(objRule);
                            bDoesQualify = srm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.Diastolic:
                            DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
                            bDoesQualify = drm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.TotalCholesterol:
                            TotalCholesterolRuleManager tcrm = new TotalCholesterolRuleManager(objRule);
                            bDoesQualify = tcrm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.HDL:
                            HDLRuleManager hdlrm = new HDLRuleManager(objRule);
                            bDoesQualify = hdlrm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.LDL:
                            LDLRuleManager ldlrm = new LDLRuleManager(objRule);
                            bDoesQualify = ldlrm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.Triglyceride:
                            TriglycerideRuleManager trm = new TriglycerideRuleManager(objRule);
                            bDoesQualify = trm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.BloodGlucose:
                            BloodGlucoseRuleManager bgrm = new BloodGlucoseRuleManager(objRule);
                            bDoesQualify = bgrm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.Weight:
                            WeightRuleManager wrm = new WeightRuleManager(objRule);
                            bDoesQualify = wrm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.BMI:
                            BMIRuleManager bmirm = new BMIRuleManager(objRule);
                            bDoesQualify = bmirm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.ActivePatient:
                            ActivePatientRuleManager aprm = new ActivePatientRuleManager(objRule);
                            bDoesQualify = aprm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.InactivePatient:
                            InactivePatientRuleManager iprm = new InactivePatientRuleManager(objRule);
                            bDoesQualify = iprm.DoesPatientQualifyForRule(objPatient);
                            break;
                        case RuleType.PatientConnection:
                            PatientProviderConnectRuleManager ppcrm = new PatientProviderConnectRuleManager(objRule);
                            bDoesQualify = ppcrm.DoesPatientQualifyForRule(objPatient);
                            break;
                    }

                    if (!bDoesQualify)
                        break;
                }

                if (bDoesQualify)
                {
                    list.Add(objPatient);
                }
                else
                {
                    continue;
                }
            }

            return list.Select(p => p.UserHealthRecordID).ToList();
        }

        //public static bool DoesPatientQualifyForRule(Patient objPatient,ReadOnlyCollection<HealthRecordItemCollection> allResults, GroupRule groupRule)
        //{
        //    bool bDoesQualify = false;

        //    if (groupRule.Age != null && groupRule.Age.IsAbove.HasValue)
        //    {
        //        AgeRuleManager arm = new AgeRuleManager();
        //        IEnumerable<Microsoft.Health.ItemTypes.Personal> listPersonal = allResults[5].Cast<Microsoft.Health.ItemTypes.Personal>();
        //        Microsoft.Health.ItemTypes.Personal personalItem = null;
        //        if (listPersonal.Count() > 0)
        //        {
        //            personalItem = listPersonal.FirstOrDefault();
        //        }
        //        bDoesQualify = arm.DoesPatientQualifyForRule(personalItem, groupRule.Age.IsAbove.Value, groupRule.Age.Value);
        //        if (!bDoesQualify)
        //            return false;
        //    }

        //    if (groupRule.Gender != null && groupRule.Gender.IsMale.HasValue)
        //    {
        //        GenderRuleManager grm = new GenderRuleManager();

        //        IEnumerable<Microsoft.Health.ItemTypes.BasicV2> listBasic = allResults[4].Cast<Microsoft.Health.ItemTypes.Basic>();

        //        Microsoft.Health.ItemTypes.BasicV2 basicItem = null;
        //        if (listBasic.Count() > 0)
        //        {
        //            basicItem = listBasic.FirstOrDefault();
        //        }

        //        bDoesQualify = grm.DoesPatientQualifyForRule(basicItem, groupRule.Gender.IsMale.Value);
        //        if (!bDoesQualify)
        //            return false;
        //    }

        //    foreach (Rule objRule in groupRule.RuleList)
        //    {
        //        switch (objRule.RuleType)
        //        {
        //            case RuleType.Systolic:
        //                SystolicRuleManager srm = new SystolicRuleManager(objRule);
        //                bDoesQualify = srm.DoesPatientQualifyForRule(allResults[1]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.Diastolic:
        //                DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
        //                bDoesQualify = drm.DoesPatientQualifyForRule(allResults[1]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.TotalCholesterol:
        //                TotalCholesterolRuleManager tcrm = new TotalCholesterolRuleManager(objRule);
        //                bDoesQualify = tcrm.DoesPatientQualifyForRule(allResults[3]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.HDL:
        //                HDLRuleManager hdlrm = new HDLRuleManager(objRule);
        //                bDoesQualify = hdlrm.DoesPatientQualifyForRule(allResults[3]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.LDL:
        //                LDLRuleManager ldlrm = new LDLRuleManager(objRule);
        //                bDoesQualify = ldlrm.DoesPatientQualifyForRule(allResults[3]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.Triglyceride:
        //                TriglycerideRuleManager trm = new TriglycerideRuleManager(objRule);
        //                bDoesQualify = trm.DoesPatientQualifyForRule(allResults[3]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.BloodGlucose:
        //                BloodGlucoseRuleManager bgrm = new BloodGlucoseRuleManager(objRule);
        //                bDoesQualify = bgrm.DoesPatientQualifyForRule(allResults[0]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.BMI:
        //                BMIRuleManager bmirm = new BMIRuleManager(objRule);
        //                bDoesQualify = bmirm.DoesPatientQualifyForRule(allResults[2], allResults[6]);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.ActivePatient:
        //                ActivePatientRuleManager aprm = new ActivePatientRuleManager(objRule);
        //                bDoesQualify = aprm.DoesPatientQualifyForRule(objPatient);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //            case RuleType.InactivePatient:
        //                InactivePatientRuleManager iprm = new InactivePatientRuleManager(objRule);
        //                bDoesQualify = iprm.DoesPatientQualifyForRule(objPatient);
        //                if (!bDoesQualify)
        //                    return false;
        //                break;
        //        }
        //    }

        //    return true;
        //}

        //public static List<int> GetPatientsQualifyingForGroupRule(int iProviderID, GroupRule groupRule)
        //{
        //    List<AHAHelpContent.Patient> list = new List<AHAHelpContent.Patient>();

        //    List<AHAHelpContent.Patient> listProviderPatient = AHAHelpContent.Patient.FindAllByProviderID(iProviderID);
        //    bool bDoesQualify = false;

        //    foreach (AHAHelpContent.Patient objPatient in listProviderPatient)
        //    {
        //        if (groupRule.Age != null && groupRule.Age.IsAbove.HasValue)
        //        {
        //            AgeRuleManager arm = new AgeRuleManager();
        //            bDoesQualify = arm.DoesPatientQualifyForRule(objPatient, groupRule.Age.IsAbove.Value, groupRule.Age.Value);
        //            if (!bDoesQualify)
        //                continue;
        //        }

        //        if (groupRule.Gender != null && groupRule.Gender.IsMale.HasValue)
        //        {
        //            GenderRuleManager grm = new GenderRuleManager();
        //            bDoesQualify = grm.DoesPatientQualifyForRule(objPatient, groupRule.Gender.IsMale.Value);
        //            if (!bDoesQualify)
        //                continue;
        //        }

        //        bDoesQualify = false;
        //        foreach (Rule objRule in groupRule.RuleList)
        //        {
        //            switch (objRule.RuleType)
        //            {
        //                case RuleType.Systolic:
        //                    SystolicRuleManager srm = new SystolicRuleManager(objRule);
        //                    bDoesQualify = srm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.Diastolic:
        //                    DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
        //                    bDoesQualify = drm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.TotalCholesterol:
        //                    TotalCholesterolRuleManager tcrm = new TotalCholesterolRuleManager(objRule);
        //                    bDoesQualify = tcrm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.HDL:
        //                    HDLRuleManager hdlrm = new HDLRuleManager(objRule);
        //                    bDoesQualify = hdlrm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.LDL:
        //                    LDLRuleManager ldlrm = new LDLRuleManager(objRule);
        //                    bDoesQualify = ldlrm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.Triglyceride:
        //                    TriglycerideRuleManager trm = new TriglycerideRuleManager(objRule);
        //                    bDoesQualify = trm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.BloodGlucose:
        //                    BloodGlucoseRuleManager bgrm = new BloodGlucoseRuleManager(objRule);
        //                    bDoesQualify = bgrm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.Weight:
        //                    WeightRuleManager wrm = new WeightRuleManager(objRule);
        //                    bDoesQualify = wrm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.BMI:
        //                    BMIRuleManager bmirm = new BMIRuleManager(objRule);
        //                    bDoesQualify = bmirm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.ActivePatient:
        //                    ActivePatientRuleManager aprm = new ActivePatientRuleManager(objRule);
        //                    bDoesQualify = aprm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //                case RuleType.InactivePatient:
        //                    InactivePatientRuleManager iprm = new InactivePatientRuleManager(objRule);
        //                    bDoesQualify = iprm.DoesPatientQualifyForRule(objPatient);
        //                    break;
        //            }

        //            if (!bDoesQualify)
        //                break;
        //        }

        //        if (bDoesQualify)
        //        {
        //            list.Add(objPatient);
        //        }
        //    }

        //    return list.Select(p => p.UserHealthRecordID).ToList();
        //}
    }
}
