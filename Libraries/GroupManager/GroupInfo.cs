﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace GroupManager
{
    public enum GroupType
    {
        PreHypertension,
        Hypertension,
        CholesterolMale,
        CholesterolFemale,
        BMIOverweight,
        BMIObese,
        Diabetes
    }

    public class GroupInfo
    {
        public int ProviderID
        {
            get;
            set;
        }

        public List<int> PatientList
        {
            get;
            set;
        }

        public string TypeID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public AHAHelpContent.PatientGroup.GroupPatientRemoveOption RemoveOption
        {
            get;
            set;
        }

        public string GroupRule
        {
            get;
            set;
        }

        public bool AddRemovePatientsAutomatically
        {
            get;
            set;
        }

        static bool bIsLoaded = false;
        static object objLock = new object();
        static List<GroupInfo> m_List = new List<GroupInfo>();

        private static void _loadData()
        {
            if (!bIsLoaded)
            {
                lock (objLock)
                {
                    if (!bIsLoaded)
                    {
                        _parsePartnerXml();
                        bIsLoaded = true;
                    }
                }
            }
        }

        private static void _parsePartnerXml()
        {
            XDocument document = XDocument.Load(System.Web.HttpContext.Current.Server.MapPath("~/XmlFiles/SystemCreatedGroups.xml"));

            var queryResult = document.Descendants("Group").Select(Group => new GroupInfo
            {
                TypeID = Group.Attribute("TypeID") != null ? Group.Attribute("TypeID").Value : string.Empty,
                Name = Group.XPathSelectElement("Name").Value,
                Description = Group.XPathSelectElement("Description").Value,
                AddRemovePatientsAutomatically = bool.Parse(Group.XPathSelectElement("AddRemovePatientsAutomatically").Value),
                GroupRule = Group.XPathSelectElement("GroupRule").ToString()
            });

            m_List = queryResult.ToList();
        }

        public static List<GroupInfo> GetSystemCreatedGroups()
        {
            _loadData();

            return m_List;
        }

        public static GroupInfo GetGroupByTypeID(GroupType eGroupType)
        {
            _loadData();

            return m_List.Where(g => g.TypeID == eGroupType.ToString()).SingleOrDefault();
        }
    }
}
