﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GroupManager
{
    public class AgeRuleManager
    {
        public bool DoesPatientQualifyForRule(Microsoft.Health.ItemTypes.Personal item, bool bIsAbove, int iAge)
        {
            bool bDoesQualify = false;

            if (item != null && item.BirthDate != null)
            {
                int? rAge = _GetAge(item.BirthDate.ToDateTime());
                bDoesQualify = rAge.HasValue && ((bIsAbove && rAge.Value > iAge) ||
                       (!bIsAbove && rAge.Value < iAge));
            }

            return bDoesQualify;
        }

        public bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient, bool bIsAbove, int iAge)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
            HVManager.PersonalDataManager.PersonalItem item = mgr.PersonalDataManager.Item;

            if (item != null)
            {
                int? rAge = _GetAge(item.DateOfBirth.Value);
                bDoesQualify = rAge.HasValue && ((bIsAbove && rAge.Value > iAge) ||
                       (!bIsAbove && rAge.Value < iAge));
            }

            return bDoesQualify;
        }

        private int? _GetAge(DateTime? dtBirthYear)
        {
            if (dtBirthYear.HasValue)
            {
                TimeSpan timespan = DateTime.Now.ToUniversalTime().Subtract(dtBirthYear.Value.ToUniversalTime());
                float fOneYear = 365.25f;
                if (timespan.Days < fOneYear)
                {
                    return 0;
                }
                else
                {
                    int iYears = (int)(timespan.Days / fOneYear);
                    return iYears;
                }
            }
            return null;
        }
    }
}
