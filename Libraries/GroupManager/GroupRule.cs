﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace GroupManager
{
    public class GroupRule
    {
        public Age Age
        {
            get;
            set;
        }

        public Gender Gender
        {
            get;
            set;
        }

        public List<Rule> RuleList
        {
            get;
            set;
        }

        public GroupRule(Age age, Gender gender, List<Rule> ruleList)
        {
            Age = age;
            Gender = gender;
            RuleList = ruleList;
        }

        public static GroupRule ParseGroupRuleXml(string strXml)
        {
            if (string.IsNullOrEmpty(strXml))
                return null;

            XDocument xDoc = XDocument.Parse(strXml);



            Age objAge = new Age();

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("GroupRule/Age").Attribute("IsAbove").Value))
            {
                objAge.IsAbove = Boolean.Parse(xDoc.XPathSelectElement("GroupRule/Age").Attribute("IsAbove").Value);
                objAge.Value = Convert.ToInt32(xDoc.XPathSelectElement("GroupRule/Age").Value);
            }
            else
            {
                objAge = null;
            }

            Gender objGender = new Gender();

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("GroupRule/Gender").Attribute("IsMale").Value))
            {
                objGender.IsMale = Boolean.Parse(xDoc.XPathSelectElement("GroupRule/Gender").Attribute("IsMale").Value);
            }
            else
            {
                objGender = null;
            }

            List<Rule> ruleList = new List<Rule>();

            foreach (XElement element in xDoc.XPathSelectElements("GroupRule/Rules/Rule"))
            {
                ruleList.Add(Rule.ParseRuleXml(element.ToString()));
            }

            return new GroupRule(objAge, objGender, ruleList);
        }

        public string GetGroupRuleXml()
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("GroupRule"
                                , new XElement("Age", new XAttribute("IsAbove", Age.IsAbove.HasValue ? Age.IsAbove.Value.ToString() : string.Empty), Age.Value.ToString())
                                , new XElement("Gender", new XAttribute("IsMale", Gender.IsMale.HasValue ? Gender.IsMale.Value.ToString() : string.Empty), string.Empty)
                                , new XElement("Rules",string.Empty)));

            XElement rulesElement = xDoc.XPathSelectElements("GroupRule/Rules").Last();
            foreach (Rule rule in RuleList)
            {
                rulesElement.Add(rule.GetRuleXElement());
            }

            return xDoc.ToString();
        }
    }
}
