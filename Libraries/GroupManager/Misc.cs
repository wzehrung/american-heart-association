﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace GroupManager
{
    public enum PeriodType
    {
        [Description("Days")]
        D,
        [Description("Weeks")]
        W,
        [Description("Months")]
        M,
        [Description("NotSet")]
        N
    }

    public enum RuleType
    {
        [Description("BP Systolic")]
        Systolic,
        [Description("BP Diastolic")]
        Diastolic,
        [Description("Cholesterol Total")]
        TotalCholesterol,
        [Description("Cholesterol HDL")]
        HDL,
        [Description("Cholesterol LDL")]
        LDL,
        [Description("Triglyceride")]
        Triglyceride,
        [Description("Blood Glucose")]
        BloodGlucose,
        [Description("Weight")]
        Weight,
        [Description("BMI")]
        BMI,
        [Description("Active Patient")]
        ActivePatient,
        [Description("Inactive Patient")]
        InactivePatient,
        [Description("Patient Connection")]
        PatientConnection
    }

    public class Age
    {
        public int Value
        {
            get;
            set;
        }

        public bool? IsAbove
        {
            get;
            set;
        }
    }

    public class Gender
    {
        public bool? IsMale
        {
            get;
            set;
        }
    }
}
