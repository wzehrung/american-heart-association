﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GroupManager
{
    public class ActivePatientRuleManager : RuleManagerBase
    {
        public ActivePatientRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            bool bDoesQualify = false;

            bDoesQualify = objPatient.LastActivityDate.HasValue && objPatient.LastActivityDate.Value >= this.DateFrom;

            return bDoesQualify;
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<Microsoft.Health.HealthRecordItem> list)
        {
            throw new NotImplementedException();
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<Microsoft.Health.HealthRecordItem> list1, IEnumerable<Microsoft.Health.HealthRecordItem> list2)
        {
            return base.DoesPatientQualifyForRule(list1, list2);
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }

        public override bool DoesReadingQualifyForRule(double val)
        {
            throw new NotImplementedException();
        }
    }
}
