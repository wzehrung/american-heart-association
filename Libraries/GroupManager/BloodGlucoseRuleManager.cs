﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;

namespace GroupManager
{
    public class BloodGlucoseRuleManager : RuleManagerBase
    {
        public BloodGlucoseRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> list)
        {
            bool bDoesQualify = false;

            list = list.Where(b => b.EffectiveDate > DateFrom);

            if (list.Count() == 0)
                return false;

            IEnumerable<Microsoft.Health.ItemTypes.BloodGlucose> listBG = list.Cast<Microsoft.Health.ItemTypes.BloodGlucose>();

            if (Rule.IsAverage)
            {
                if (listBG.Count() > 0)
                {
                    double iAverage = 0;
                    if (listBG.Count() >= Rule.LastNReadings.Value)
                    {
                        iAverage = listBG.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => HVManager.Helper.GlocoseMmolToMgdl(l.Value.Value));
                    }
                    else
                    {
                        iAverage = listBG.OrderByDescending(i => i.EffectiveDate).Average(l => HVManager.Helper.GlocoseMmolToMgdl(l.Value.Value));
                    }

                    bDoesQualify = DoesValueFallInRange(iAverage);
                }
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    Microsoft.Health.ItemTypes.BloodGlucose item = listBG.First() as Microsoft.Health.ItemTypes.BloodGlucose;
                    double iVal = HVManager.Helper.GlocoseMmolToMgdl(item.Value.Value);

                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(listBG.Select(i => HVManager.Helper.GlocoseMmolToMgdl(i.Value.Value)));
                }
            }

            return bDoesQualify;
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

            LinkedList<HVManager.BloodGlucoseDataManager.BGItem> list = mgr.BloodGlucoseDataManager.GetDataBetweenDates(DateFrom, DateTime.Now);

            if (list.Count == 0)
                return false;

            if (Rule.IsAverage)
            {
                double iAverage = 0;
                if (list.Count() >= Rule.LastNReadings.Value)
                {
                    iAverage = list.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.Value.Value);
                }
                else
                {
                    iAverage = list.OrderByDescending(i => i.EffectiveDate).Average(l => l.Value.Value);
                }

                bDoesQualify = DoesValueFallInRange(iAverage);
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    HVManager.BloodGlucoseDataManager.BGItem item = list.First.Value;
                    double iVal = item.Value.Value;

                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(list.Select(i => i.Value.Value));
                }                
            }

            return bDoesQualify;
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }

        public override bool DoesReadingQualifyForRule(double dVal)
        {
            return DoesValueFallInRange(dVal);
        }
    }
}
