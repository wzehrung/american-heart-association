﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GroupManager
{
    public class GenderRuleManager
    {
        public bool DoesPatientQualifyForRule(Microsoft.Health.ItemTypes.BasicV2 item, bool bIsMale)
        {
            bool bDoesQualify = false;

            if (item != null && item.Gender.HasValue)
            {
                bDoesQualify = (bIsMale && item.Gender.Value == Microsoft.Health.ItemTypes.Gender.Male)
                    || (!bIsMale && item.Gender.Value == Microsoft.Health.ItemTypes.Gender.Female);
            }
            return bDoesQualify;
        }

        public bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient, bool bIsMale)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
            HVManager.BasicDataManager.BasicItem item = mgr.BasicDataManager.Item;

            if (item != null && item.GenderOfPerson.Value.HasValue)
            {
                bDoesQualify = (bIsMale && item.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Male)
                    || (!bIsMale && item.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Female);
            }

            return bDoesQualify;
        }
    }
}
