﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;

namespace GroupManager
{
    public class WeightRuleManager : RuleManagerBase
    {
        public WeightRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> list)
        {
            bool bDoesQualify = false;

            list = list.Where(b => b.EffectiveDate > DateFrom);

            if (list.Count() == 0)
                return false;

            IEnumerable<Microsoft.Health.ItemTypes.Weight> listWeight = list.Cast<Microsoft.Health.ItemTypes.Weight>();

            double kilograms_to_pounds = 2.205;

            if (Rule.IsAverage)
            {
                if (listWeight.Count() > 0)
                {
                    double iAverage = 0;
                    if (listWeight.Count() >= Rule.LastNReadings.Value)
                    {
                        iAverage = listWeight.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => (l.Value.Value * kilograms_to_pounds));
                    }
                    else
                    {
                        iAverage = listWeight.OrderByDescending(i => i.EffectiveDate).Average(l => (l.Value.Value * kilograms_to_pounds));
                    }

                    bDoesQualify = DoesValueFallInRange(iAverage);
                }
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    Microsoft.Health.ItemTypes.Weight item = listWeight.First() as Microsoft.Health.ItemTypes.Weight;
                    double iVal = (item.Value.Value * kilograms_to_pounds);

                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(listWeight.Select(i => (i.Value.Value * kilograms_to_pounds)));
                }
            }

            return bDoesQualify;
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

            LinkedList<HVManager.WeightDataManager.WeightItem> list = mgr.WeightDataManager.GetDataBetweenDates(DateFrom, DateTime.Now);

            if (list.Count == 0)
                return false;

            if (Rule.IsAverage)
            {
                double iAverage = 0;
                if (list.Count() >= Rule.LastNReadings.Value)
                {
                    iAverage = list.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.CommonWeight.Value.ToPounds());
                }
                else
                {
                    iAverage = list.OrderByDescending(i => i.EffectiveDate).Average(l => l.CommonWeight.Value.ToPounds());
                }

                bDoesQualify = DoesValueFallInRange(iAverage);
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    HVManager.WeightDataManager.WeightItem item = list.First.Value;
                    double iVal = item.CommonWeight.Value.ToPounds();

                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(list.Select(i => i.CommonWeight.Value.ToPounds()));
                }
            }

            return bDoesQualify;
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }

        public override bool DoesReadingQualifyForRule(double dVal)
        {
            return DoesValueFallInRange(dVal);
        }
    }
}
