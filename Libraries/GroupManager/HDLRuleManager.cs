﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;

namespace GroupManager
{
    public class HDLRuleManager : RuleManagerBase
    {
        public HDLRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> list)
        {
            bool bDoesQualify = false;

            list = list.Where(b => b.EffectiveDate > DateFrom);

            if (list.Count() == 0)
                return false;

            // PJB: better class
            //IEnumerable<Microsoft.Health.ItemTypes.CholesterolProfileV2> listCho = list.Cast<Microsoft.Health.ItemTypes.CholesterolProfileV2>();
            IEnumerable<HVWrapper.ExtendedThingTypes.CholesterolEx> listCho = list.Cast<HVWrapper.ExtendedThingTypes.CholesterolEx>();
            listCho = listCho.Where(h => h.HdlHasValue );

            if (listCho.Count() == 0)
                return false;

            if (Rule.IsAverage)
            {
                double iAverage = 0;
                if (listCho.Count() >= Rule.LastNReadings.Value)
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.HdlInMgdlUnits);
                }
                else
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Average(l => l.HdlInMgdlUnits);
                }

                bDoesQualify = DoesValueFallInRange(iAverage);
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    // PJB: better class
                    //Microsoft.Health.ItemTypes.CholesterolProfileV2 item = listCho.First() as Microsoft.Health.ItemTypes.CholesterolProfileV2;
                    HVWrapper.ExtendedThingTypes.CholesterolEx item = listCho.First() as HVWrapper.ExtendedThingTypes.CholesterolEx;
                    bDoesQualify = DoesValueFallInRange( item.HdlInMgdlUnits );
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(listCho.Select(i => i.HdlInMgdlUnits));
                }
            }

            return bDoesQualify;
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

            IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> listCho = mgr.CholesterolDataManager.GetDataBetweenDates(DateFrom, DateTime.Now).ToList();

            if (listCho.Count() == 0)
                return false;

            listCho = listCho.Where(h => h.HDL.Value.HasValue);

            if (listCho.Count() == 0)
                return false;

            if (Rule.IsAverage)
            {
                double iAverage = 0;
                if (listCho.Count() >= Rule.LastNReadings.Value)
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.HDL.Value.Value);
                }
                else
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Average(l => l.HDL.Value.Value);
                }

                bDoesQualify = DoesValueFallInRange(iAverage);
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    HVManager.CholesterolDataManager.CholesterolItem item = listCho.First() as HVManager.CholesterolDataManager.CholesterolItem;
                    int iVal = item.HDL.Value.Value;
                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(listCho.Select(i => i.HDL.Value.Value));
                }
            }

            return bDoesQualify;
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }

        public override bool DoesReadingQualifyForRule(double dVal)
        {
            return DoesValueFallInRange(dVal);
        }
    }
}
