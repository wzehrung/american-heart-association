﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;

namespace GroupManager
{
    public abstract class RuleManagerBase : IRuleManager
    {
        Rule m_Rule;
        public RuleManagerBase(Rule objRule)
        {
            m_Rule = objRule;
        }

        internal Rule Rule
        {
            get
            {
                return m_Rule;
            }
        }

        public DateTime DateFrom
        {
            get
            {
                DateTime dtFrom;

                if (this.Rule.PeriodType == PeriodType.M)
                {
                    dtFrom = DateTime.Today.AddMonths(-1 * this.Rule.PeriodCount);
                }
                else if (this.Rule.PeriodType == PeriodType.W)
                {
                    dtFrom = DateTime.Today.AddDays(-1 * (this.Rule.PeriodCount * 7));
                }
                else if (this.Rule.PeriodType == PeriodType.D)
                {
                    dtFrom = DateTime.Today.AddDays(-1 * this.Rule.PeriodCount);
                }
                else if (this.Rule.PeriodType == PeriodType.N)
                {
                    dtFrom = DateTime.MinValue;
                }
                else
                {
                    throw new InvalidProgramException(string.Format("Invalid Period type:- {0}", this.Rule.PeriodType.ToString()));
                }

                return dtFrom;
            }
        }

        public bool DoesValueFallInRange(double dVal)
        {
            bool bDoesQualify = false; ;

            if (Rule.MinValue.HasValue && Rule.MaxValue.HasValue)
            {
                bDoesQualify = dVal >= Rule.MinValue.Value && dVal <= Rule.MaxValue.Value;
            }
            else if (Rule.MinValue.HasValue && !Rule.MaxValue.HasValue)
            {
                bDoesQualify = dVal > Rule.MinValue.Value;
            }
            else if (!Rule.MinValue.HasValue && Rule.MaxValue.HasValue)
            {
                bDoesQualify = dVal < Rule.MaxValue.Value;
            }

            return bDoesQualify;
        }

        public bool DoesValueFallInRange(IEnumerable<double> dValList)
        {
            bool bDoesQualify = false; ;

            if (Rule.MinValue.HasValue && Rule.MaxValue.HasValue)
            {
                bDoesQualify = dValList.Where(dVal => dVal >= Rule.MinValue.Value && dVal <= Rule.MaxValue.Value).Count() > 0;
            }
            else if (Rule.MinValue.HasValue && !Rule.MaxValue.HasValue)
            {
                bDoesQualify = dValList.Where(dVal => dVal > Rule.MinValue.Value).Count() > 0;
            }
            else if (!Rule.MinValue.HasValue && Rule.MaxValue.HasValue)
            {
                bDoesQualify = dValList.Where(dVal => dVal < Rule.MaxValue.Value).Count() > 0;
            }

            return bDoesQualify;
        }

        public bool DoesValueFallInRange(IEnumerable<int> dValList)
        {
            bool bDoesQualify = false; ;

            if (Rule.MinValue.HasValue && Rule.MaxValue.HasValue)
            {
                bDoesQualify = dValList.Where(dVal => dVal > Rule.MinValue.Value && dVal <= Rule.MaxValue.Value).Count() > 0;
            }
            else if (Rule.MinValue.HasValue && !Rule.MaxValue.HasValue)
            {
                bDoesQualify = dValList.Where(dVal => dVal > Rule.MinValue.Value).Count() > 0;
            }
            else if (!Rule.MinValue.HasValue && Rule.MaxValue.HasValue)
            {
                bDoesQualify = dValList.Where(dVal => dVal < Rule.MaxValue.Value).Count() > 0;
            }

            return bDoesQualify;
        }

        #region IRuleManager Members
        public abstract bool DoesReadingQualifyForRule(double val);
        public abstract bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient);
        public abstract bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> list);
        public abstract List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID);
        public virtual bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> list1, IEnumerable<HealthRecordItem> list2)
        {
            return false;
        }

        public virtual bool DoesPatientQualifyForRule()
        {
            return false;
        }
        #endregion
    }
}
