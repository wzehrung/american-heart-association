﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GroupManager
{
    interface IRuleManager
    {
        bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient);
        List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID);
    }
}
