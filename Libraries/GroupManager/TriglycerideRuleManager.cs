﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;

namespace GroupManager
{
    public class TriglycerideRuleManager : RuleManagerBase
    {
        public TriglycerideRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> list)
        {
            bool bDoesQualify = false;

            list = list.Where(b => b.EffectiveDate > DateFrom);

            if (list.Count() == 0)
                return false;

            // PJB: Use CholesterolEx instead of CholesterolProfileV2
            //IEnumerable<Microsoft.Health.ItemTypes.CholesterolProfileV2> listCho = list.Cast<Microsoft.Health.ItemTypes.CholesterolProfileV2>();
            IEnumerable<HVWrapper.ExtendedThingTypes.CholesterolEx> listCho = list.Cast<HVWrapper.ExtendedThingTypes.CholesterolEx>();
            listCho = listCho.Where(h => h.TriglycerideHasValue );

            if (listCho.Count() == 0)
                return false;

            if (Rule.IsAverage)
            {
                double iAverage = 0;
                if (listCho.Count() >= Rule.LastNReadings.Value)
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.TriglycerideInMgdlUnits);
                }
                else
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Average(l => l.TriglycerideInMgdlUnits);
                }

                bDoesQualify = DoesValueFallInRange(iAverage);
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    // PJB: change to better type
                    //Microsoft.Health.ItemTypes.CholesterolProfileV2 item = listCho.First() as Microsoft.Health.ItemTypes.CholesterolProfileV2;
                    HVWrapper.ExtendedThingTypes.CholesterolEx item = listCho.First() as HVWrapper.ExtendedThingTypes.CholesterolEx;
                    bDoesQualify = DoesValueFallInRange( item.TriglycerideInMgdlUnits ) ;
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(listCho.Select(i => i.TriglycerideInMgdlUnits));
                }
            }

            return bDoesQualify;
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

            IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> listCho = mgr.CholesterolDataManager.GetDataBetweenDates(DateFrom, DateTime.Now).ToList();

            if (listCho.Count() == 0)
                return false;

            listCho = listCho.Where(h => h.Triglycerides.Value.HasValue);

            if (listCho.Count() == 0)
                return false;

            if (Rule.IsAverage)
            {
                double iAverage = 0;
                if (listCho.Count() >= Rule.LastNReadings.Value)
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.Triglycerides.Value.Value);
                }
                else
                {
                    iAverage = listCho.OrderByDescending(i => i.EffectiveDate).Average(l => l.Triglycerides.Value.Value);
                }

                bDoesQualify = DoesValueFallInRange(iAverage);
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    HVManager.CholesterolDataManager.CholesterolItem item = listCho.First() as HVManager.CholesterolDataManager.CholesterolItem;
                    int iVal = item.Triglycerides.Value.Value;
                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(listCho.Select(i => i.Triglycerides.Value.Value));
                }
            }

            return bDoesQualify;
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }

        public override bool DoesReadingQualifyForRule(double dVal)
        {
            return DoesValueFallInRange(dVal);
        }
    }
}
