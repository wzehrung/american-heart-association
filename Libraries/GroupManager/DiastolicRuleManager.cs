﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;

namespace GroupManager
{
    public class DiastolicRuleManager : RuleManagerBase
    {
        public DiastolicRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> list)
        {
            bool bDoesQualify = false;

            list = list.Where(b => b.EffectiveDate > DateFrom);

            if (list.Count() == 0)
                return false;

            IEnumerable<Microsoft.Health.ItemTypes.BloodPressure> listBP = list.Cast<Microsoft.Health.ItemTypes.BloodPressure>();

            if (Rule.IsAverage)
            {
                if (listBP.Count() > 0)
                {
                    double iAverage = 0;
                    if (listBP.Count() >= Rule.LastNReadings.Value)
                    {
                        iAverage = listBP.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.Diastolic);
                    }
                    else
                    {
                        iAverage = listBP.OrderByDescending(i => i.EffectiveDate).Average(l => l.Diastolic);
                    }

                    bDoesQualify = DoesValueFallInRange(iAverage);
                }
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    Microsoft.Health.ItemTypes.BloodPressure item = listBP.First() as Microsoft.Health.ItemTypes.BloodPressure;
                    int iVal = item.Diastolic;

                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(listBP.Select(i => i.Diastolic));
                }
            }

            return bDoesQualify;
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

            LinkedList<HVManager.BloodPressureDataManager.BPItem> list = mgr.BloodPressureDataManager.GetDataBetweenDates(DateFrom, DateTime.Now);

            if (list.Count == 0)
                return false;

            if (Rule.IsAverage)
            {
                double iAverage = 0;
                if (list.Count() >= Rule.LastNReadings.Value)
                {
                    iAverage = list.OrderByDescending(i => i.EffectiveDate).Take(Rule.LastNReadings.Value).Average(l => l.Diastolic.Value);
                }
                else
                {
                    iAverage = list.OrderByDescending(i => i.EffectiveDate).Average(l => l.Diastolic.Value);
                }

                bDoesQualify = DoesValueFallInRange(iAverage);
            }
            else
            {
                if (Rule.LastNReadings.HasValue && Rule.LastNReadings.Value == 1)
                {
                    HVManager.BloodPressureDataManager.BPItem item = list.First.Value;
                    int iVal = item.Diastolic.Value;

                    bDoesQualify = DoesValueFallInRange(iVal);
                }
                else
                {
                    bDoesQualify = DoesValueFallInRange(list.Select(i => i.Systolic.Value));
                }
            }

            return bDoesQualify;
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }

        public override bool DoesReadingQualifyForRule(double dVal)
        {
            return DoesValueFallInRange(dVal);
        }
    }
}
