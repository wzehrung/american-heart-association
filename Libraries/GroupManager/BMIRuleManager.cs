﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;

namespace GroupManager
{
    public class BMIRuleManager : RuleManagerBase
    {
        public BMIRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        private double _MetersToInches(double dMeter)
        {
            return dMeter / 0.0254;
        }

        private double _CalculateBMI(double dWeight, double dHeight)
        {
            double dHeightInInches = _MetersToInches(dHeight);
            double bmi = (dWeight * 703) / ((dHeightInInches) * (dHeightInInches));

            return bmi;
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> listWt, IEnumerable<HealthRecordItem> listHt)
        {
            bool bDoesQualify = false;

            listWt = listWt.Where(b => b.EffectiveDate > DateFrom);

            if (listWt.Count() == 0 || listHt.Count() == 0)
                return false;

            IEnumerable<Microsoft.Health.ItemTypes.Weight> listWeight = listWt.Cast<Microsoft.Health.ItemTypes.Weight>();
            IEnumerable<Microsoft.Health.ItemTypes.Height> listHeight = listHt.Cast<Microsoft.Health.ItemTypes.Height>();

            double dHeight = (listHeight.First() as Microsoft.Health.ItemTypes.Height).Value.Meters;

            double kilograms_to_pounds = 2.205;

            double dWeight = ((listWeight.First() as Microsoft.Health.ItemTypes.Weight).Value.Value * kilograms_to_pounds);

            double dBMI = _CalculateBMI(dWeight, dHeight);

            bDoesQualify = DoesValueFallInRange(dBMI);

            return bDoesQualify;
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            bool bDoesQualify = false;

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

            LinkedList<HVManager.WeightDataManager.WeightItem> listWeight = mgr.WeightDataManager.GetDataBetweenDates(DateFrom, DateTime.Now);
            LinkedList<HVManager.HeightDataManager.HeightItem> listHeight = mgr.HeightDataManager.GetDataBetweenDates(DateFrom, DateTime.Now);

            if (listWeight.Count == 0 || listHeight.Count == 0)
                return false;

            double dHeight = listHeight.First.Value.HeightInMeters.Value;

            double dWeight = listWeight.First.Value.CommonWeight.Value.ToPounds();

            double dBMI = _CalculateBMI(dWeight, dHeight);

            bDoesQualify = DoesValueFallInRange(dBMI);

            return bDoesQualify;
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<HealthRecordItem> listWeight)
        {
            throw new NotImplementedException();
        }

        public override bool DoesReadingQualifyForRule(double dVal)
        {
            return DoesValueFallInRange(dVal);
        }
    }
}
