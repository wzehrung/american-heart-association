﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GroupManager
{
    public class CommonRuleManager : RuleManagerBase
    {
        public CommonRuleManager(Rule objRule)
            : base(objRule)
        {
        }

        public override bool DoesReadingQualifyForRule(double val)
        {
            throw new NotImplementedException();
        }

        public override bool DoesPatientQualifyForRule(AHAHelpContent.Patient objPatient)
        {
            throw new NotImplementedException();
        }

        public override bool DoesPatientQualifyForRule(IEnumerable<Microsoft.Health.HealthRecordItem> list)
        {
            throw new NotImplementedException();
        }

        public override List<AHAHelpContent.Patient> GetPatientsQualifyingForRule(int iProviderID)
        {
            throw new NotImplementedException();
        }
    }
}
