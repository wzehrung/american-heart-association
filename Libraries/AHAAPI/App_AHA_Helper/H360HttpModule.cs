﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.Health.Web;

namespace AHAAPI
{
    public class H360HttpModule : IHttpModule, System.Web.SessionState.IRequiresSessionState
    {
        public void Dispose()
        {

        }

        string __ProviderUrlRelativePath = "/provider/";
        string strPatientConnectPage = "/v5/ConnectPatient.aspx";
        string strCheckBPClearActiveSessionPage = "checkbpclearactivesession.aspx";
        string strProviderConnectPage = "ConnectProvider.aspx";
        string strProviderConnectPage_Short = "/connectprovider";
        string strPatientConnectPage_Short = "/connectpatient";
        string strHttpsPrefix = "https://";
        string strHttpPrefix = "http://";

        public virtual void Init(HttpApplication application)
        {
            application.BeginRequest += new EventHandler(BeginRequest);
            application.PreRequestHandlerExecute += new EventHandler(PreRequestHandlerExecute);
            application.Error += new EventHandler(OnError);
        }

        protected virtual void BeginRequest(object sender, EventArgs args)
        {
            HttpApplication application = (HttpApplication)sender;
            string strFullUrl = GRCBase.UrlUtility.FullCurrentPageUrl.ToLower();


            if (strFullUrl.EndsWith(strProviderConnectPage_Short))
            {
                application.Response.Redirect(string.Format("{0}{1}/{2}", AHACommon.AHAAppSettings.IsSSLEnabled ? strHttpsPrefix : strHttpPrefix, GRCBase.UrlUtility.ServerName, strProviderConnectPage));
                application.Response.End();
                return;
            }

            if (strFullUrl.EndsWith(strPatientConnectPage_Short))
            {
                application.Response.Redirect(string.Format("{0}{1}/{2}", AHACommon.AHAAppSettings.IsSSLEnabled ? strHttpsPrefix : strHttpPrefix, GRCBase.UrlUtility.ServerName, strPatientConnectPage));
                application.Response.End();
                return;
            }

            if (!strFullUrl.EndsWith(".aspx") && !strFullUrl.EndsWith("checkbp"))
                return;

            if ((strFullUrl.Contains(__ProviderUrlRelativePath)
                || strFullUrl.EndsWith(strPatientConnectPage.ToLower())
                 || strFullUrl.EndsWith(strProviderConnectPage.ToLower())
                 || strFullUrl.EndsWith(strCheckBPClearActiveSessionPage))
                && AHACommon.AHAAppSettings.IsSSLEnabled
                && !application.Request.IsSecureConnection
                && application.Request.RequestType == "GET")
            {
                if (!strFullUrl.EndsWith("checkbp.aspx") && !strFullUrl.EndsWith("checkbp"))
                {
                    application.Response.Redirect(string.Format("https://{0}{1}", GRCBase.UrlUtility.ServerName, GRCBase.UrlUtility.RelativeCurrentPageUrlWithQV));
                    application.Response.End();
                    return;
                }
            }

            if (AHACommon.AHAAppSettings.IsSSLEnabled
                && application.Request.IsSecureConnection
                && application.Request.RequestType == "GET"
                && (
                strFullUrl.EndsWith("checkbp.aspx")
                || strFullUrl.EndsWith("checkbp")))
            {
                application.Response.Redirect(string.Format("http://{0}{1}", GRCBase.UrlUtility.ServerName, GRCBase.UrlUtility.RelativeCurrentPageUrlWithQV));
                application.Response.End();
                return;
            }
        }

        protected virtual void PreRequestHandlerExecute(object sender, EventArgs args)
        {
            HttpApplication application = (HttpApplication)sender;

            string strFullUrl = GRCBase.UrlUtility.FullCurrentPageUrl.ToLower();

            if (application.Request.Path.Substring(1).IndexOf(".") == -1)
            {
                _SetCulture();

                AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByURL(application.Request.Path.Substring(1), H360Utility.GetCurrentLanguageID());
                if (objCampaign != null && objCampaign.IsActive)
                {
                    //do campaign handling here
                    application.Context.Response.Redirect("~/Default.aspx?cid=" + GRCBase.BlowFish.EncryptString(application.Request.Path.Substring(1)));
                    application.Response.End();
                    return;
                }
            }

            if (!strFullUrl.EndsWith(".aspx"))
                return;

            if (AHAAPI.SessionInfo.IsUserLoggedIn
                && !strFullUrl.EndsWith("error.aspx")
                && !strFullUrl.EndsWith("filenotfound.aspx")
                && (strFullUrl.Contains(__ProviderUrlRelativePath) || strFullUrl.EndsWith(strPatientConnectPage.ToLower())))
            {
                application.Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_HOME);
            }

            if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn()
                && !strFullUrl.Contains(__ProviderUrlRelativePath)
                && !strFullUrl.EndsWith(strPatientConnectPage.ToLower())
                && !strFullUrl.EndsWith("error.aspx")
                && !strFullUrl.EndsWith("filenotfound.aspx"))
            {
                application.Response.Redirect(AHAAPI.Provider.ProviderPageUrl.HOME);
            }

            if (application.Request["PrinterFriendly"] == "1" 
                && !AHAAPI.Provider.SessionInfo.IsProviderLoggedIn()
                && !strFullUrl.Contains(__ProviderUrlRelativePath))
            {
                Microsoft.Health.PersonInfo objPersonInfo = Microsoft.Health.Web.WebApplicationUtilities.LoadPersonInfoFromCookie(System.Web.HttpContext.Current);
                if (objPersonInfo == null)
                {
                    AHAAPI.H360Utility.MSHealthSignOut(AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP);
                    return;
                }
            }

            H360RequestContext.PopulateContextData();
        }

        protected virtual void OnError(object sender, EventArgs args)
        {
            if (System.Web.HttpContext.Current.Error != null)
            {
                Exception current = System.Web.HttpContext.Current.Error;
                while (current != null)
                {
                    if (current.GetType() == typeof(Microsoft.Health.HealthServiceCredentialTokenExpiredException))
                    {
                        System.Web.HttpContext.Current.Session.Abandon();
                        HttpContext.Current.ClearError();
                        H360Utility.MSHealthSignOut(WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP);
                        //HealthServicePage page = System.Web.HttpContext.Current.CurrentHandler as HealthServicePage;

                        //if (page != null)
                        //{
                        //    page.SignOut();
                        //}
                        //else
                        //{
                        //    H360Utility.MSHealthSignOut(WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP);
                        //}
                       
                        System.Web.HttpContext.Current.Response.End();
                       
                        break;
                    }
                    else if (current.GetType() == typeof(Microsoft.Health.HealthServiceAccessDeniedException))
                    {
                        //We are having this to address the following scenario.
                        //If the permissions granted to application id changes, the users need
                        //to get reauthorized. But they will still show on authorized records collection
                        //already. If any such user is made as selected record, we need to force
                        //reauthorization to application.

                        if (Microsoft.Health.Web.HealthServicePage.CurrentPage != null)
                        {
                            //once we get an access denied exception clear the selected record id in db
                            //so that then the user comes back after authorization to heart360, we don't try to set this as the selected record
                            if (Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo != null
                                && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.PersonId != null)
                            {
                                AHAHelpContent.AHAUser.ChangeSelectedRecord(Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.PersonId, null);
                            }

                            StringBuilder sb = new StringBuilder(128);
                            sb.Append("appid=");
                            sb.Append(WebApplicationConfiguration.AppId.ToString());
                            //sb.Append("&selrecordid=");
                            //sb.Append(HttpUtility.UrlEncode(Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.SelectedRecord.Id.ToString()));
                            sb.Append("&actionqs=");
                            sb.Append(HttpUtility.UrlEncode(System.Web.HttpContext.Current.Request.Url.PathAndQuery));

                            Microsoft.Health.Web.WebApplicationUtilities.RedirectToShellUrl(HttpContext.Current,
                                "APPAUTH",
                                sb.ToString());

                            System.Web.HttpContext.Current.Response.End();
                            HttpContext.Current.ClearError();
                            break;
                        }
                    }
                    else if (current.GetType() == typeof(Microsoft.Health.HealthServiceException))
                    {
                        Microsoft.Health.HealthServiceException hsex = current as Microsoft.Health.HealthServiceException;
                        if (hsex != null && hsex.ErrorCode == Microsoft.Health.HealthServiceStatusCode.InvalidRecordState)
                        {
                            if (Microsoft.Health.Web.HealthServicePage.CurrentPage != null)
                            {
                                //once we get an access denied exception clear the selected record id in db
                                //so that then the user comes back after authorization to heart360, we don't try to set this as the selected record
                                if (Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo != null
                                    && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.PersonId != null)
                                {
                                    AHAHelpContent.AHAUser.ChangeSelectedRecord(Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.PersonId, null);
                                }
                                    
                                StringBuilder sb = new StringBuilder(128);
                                sb.Append("appid=");
                                sb.Append(WebApplicationConfiguration.AppId.ToString());
                                //sb.Append("&selrecordid=");
                                //sb.Append(HttpUtility.UrlEncode(Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.SelectedRecord.Id.ToString()));
                                sb.Append("&actionqs=");
                                sb.Append(HttpUtility.UrlEncode(System.Web.HttpContext.Current.Request.Url.PathAndQuery));

                                Microsoft.Health.Web.WebApplicationUtilities.RedirectToShellUrl(HttpContext.Current,
                                    "APPAUTH",
                                    sb.ToString());

                                System.Web.HttpContext.Current.Response.End();
                                HttpContext.Current.ClearError();
                                break;
                            }
                            else
                            {
                                if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
                                {
                                    //Guid gRecordGUID = 
                                    //AHAHelpContent.ProviderDetails.DisconnectPatientOrProvider(AHAAPI.Provider.SessionInfo.GetProviderContext().ProviderID,
                                }
                            }
                        }
                    }

                    current = current.InnerException;
                }
            }
        }

        private void _SetCulture()
        {
            if (SessionInfo.IsUserLoggedIn)
            {
                UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
                if (userIdentityContext != null && !string.IsNullOrEmpty(userIdentityContext.LanguageLocale))
                {
                    LanguageHelper.SetLocaleAndCulture(userIdentityContext.LanguageLocale);
                }
            }

            if ((!AHAAPI.Provider.SessionInfo.IsProviderLoggedIn()
                && !SessionInfo.IsUserLoggedIn))
            {
                LanguageHelper.SetLocaleAndCulture(LanguageHelper.GetSelectedLanguageLocaleForVisitor());
            }
        }
    }
}
