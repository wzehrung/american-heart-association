namespace AHAAPI
{
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //
    using System;
    using System.Web;
    using System.Web.UI;
    using System.Text;
    using Microsoft.Health;

    /// <summary>
    /// Cross page naviation helpers specific to our heart-health applicaiton
    /// </summary>
    public partial class WebAppNavigation
    {
        public static string FullAppPath
        {
            get
            {
                return string.Format("{0}{1}", GRCBase.UrlUtility.ServerRootUrl, AppDomainAppVirtualPath);
            }
        }

        public static string AppDomainAppVirtualPath
        {
            get
            {
                return HttpRuntime.AppDomainAppVirtualPath;
            }
        }
        /// <summary>
        /// Used for changing the health record the logged in user is currently working with
        /// </summary>
        public const string QueryParam_SelectRecordId = "SelectRecordId";

        /// <summary>
        /// Used for changing the health record the logged in user is currently working with
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static Guid? ParseSelectedRecordId(Page page)
        {
            return ParseGuidFromUrlParam(page, QueryParam_SelectRecordId);
        }

        /// <summary>
        /// Helper to parse a Guid from a URL string
        /// </summary>
        /// <param name="page"></param>
        /// <param name="paramName"></param>
        /// <returns></returns>
        private static Guid? ParseGuidFromUrlParam(Page page, string paramName)
        {
            string paramValue = page.Request.Params[paramName];
            if (string.IsNullOrEmpty(paramValue))
            {
                return null;
            }
            //remove bookmark if the browser sent it, happens in case of IE 6
            if (paramValue.Contains("#"))
            {
                int ignoreIndex = paramValue.IndexOf('#');
                paramValue = paramValue.Remove(ignoreIndex);
            }
            return new Guid(paramValue);
        }

        /// <summary>
        /// 
        /// </summary>
        public static class H360
        {
            public const string PAGE_VISITOR_DEFAULT = "~/Default.aspx";
            public const string PAGE_VISITOR_DEFAULT_NO_APP = "/Default.aspx";
            public const string PAGE_VISITOR_PREREG = "~/visitor/prereg.aspx";      // deprecated, does not exist in V6
            public const string PAGE_PATIENT_SIGNUP = "~/SignUp.aspx";                          // used by ConnectProvider.aspx when new patient
            public const string PAGE_PATIENT_HOME = "/Pages/Patient/Dashboard.aspx";
            public const string PAGE_PATIENT_HOME_NO_APP = "/Pages/Patient/Dashboard.aspx";
            public const string PAGE_PUBLIC_INVITATION_ACCEPT = "~/ConnectProvider.aspx";
            public const string PAGE_PATIENT_INVITATION_ACCEPT = "~/patient/invitation/acceptinvitation.aspx";
            public const string PAGE_PATIENT_INVITATION_INVITE = "~/patient/invitation/ProviderInvitation.aspx";
            public const string PAGE_PATIENT_GET_STARTED = "~/Patient/GetStarted.aspx"; // deprecated, does not exist in V6
            public const string PAGE_CONSENT = "/Pages/Patient/NewUser.aspx";
            public const string PAGE_CONSENT_NO_APP = "/Pages/Patient/NewUser.aspx";
            public const string PAGE_NEW_TERMS = "/Pages/Patient/NewTerms.aspx";

            public const string PAGE_PATIENT_MLC_EXTERNAL_FINAL = "/Pages/Patient/MyLifeCheck/MLCExternalFinal.aspx";
            public const string PAGE_PATIENT_MLC_FINAL = "/Pages/Patient/MyLifeCheck/MLCFinal.aspx";
            public const string PAGE_PATIENT_MLC_IMPORT = "/Pages/Patient/MyLifeCheck/DataExport.aspx";
            public const string PAGE_PATIENT_MLC_UPLOAD = "/Pages/Patient/MyLifeCheck/UploadMLCToHV.aspx";

            /**
             ** All of this should have been out of scope for V6
             **
            public const string PAGE_AUTHORIZE_MORE_RECORDS = "~/Patient/AuthorizeMoreRecords.aspx";    // deprecated, does not exist in V6
            public const string PAGE_PERSONAL_INFO_NO_APP = "/Patient/MyPersonalInfo.aspx";             // deprecated, does not exist in V6
            public const string PAGE_HEALTH_HISTORY_NO_APP = "/Patient/MyHealthHistory.aspx";           // deprecated, does not exist in V6
            public const string PAGE_PERSONAL_INFO = "~/Patient/MyPersonalInfo.aspx";                   // deprecated, does not exist in V6
            public const string PAGE_MY_HEALTH_HISTORY = "~/Patient/MyHealthHistory.aspx";              // deprecated, does not exist in V6

            public const string PAGE_SCIENTIFIC_SUCCESS_NO_APP = "/ScientificSession/Congratulations.aspx"; // deprecated, does not exist in V6
            public const string PAGE_SCIENTIFIC_HOME = "~/ScientificSession/Default.aspx";                  // deprecated, does not exist in V6
            public const string PAGE_SCIENTIFIC_HOME_NO_APP = "/ScientificSession/Default.aspx";            // deprecated, does not exist in V6
            public const string PAGE_SCIENTIFIC_PREREG = "~/ScientificSession/PreReg.aspx";                 // deprecated, does not exist in V6

            public const string PAGE_PATIENT_PROVIDER = "/Patient/ProviderInfo/Home.aspx";
            public const string PAGE_PATIENT_MAIL = "/Patient/Mail/MailHome.aspx";
            public const string PAGE_PATIENT_EDIT_PROFILE = "~/Patient/EditProfile.aspx";
            public const string PAGE_PATIENT_EDIT_PROFILE_NO_APP = "/Patient/EditProfile.aspx";
            public const string PAGE_PATIENT_GRANT_SUPPORT_NO_APP = "/Patient/Grantor.aspx";
            

            public const string PAGE_PATIENT_WIZARD_START = "/Patient/H360GuideWizard/Start.aspx";
            public const string PAGE_PATIENT_WIZARD_STEP1 = "/Patient/H360GuideWizard/Step1.aspx?DT={0}";
            public const string PAGE_PATIENT_WIZARD_STEP2 = "/Patient/H360GuideWizard/Step2.aspx?DT={0}";
            public const string PAGE_PATIENT_WIZARD_STEP3 = "/Patient/H360GuideWizard/Step3.aspx?DT={0}";
            public const string PAGE_PATIENT_WIZARD_STEP4 = "/Patient/H360GuideWizard/Step4.aspx?DT={0}";
            public const string PAGE_PATIENT_WIZARD_STEP5 = "/Patient/H360GuideWizard/Step5.aspx?DT={0}";
            public const string PAGE_PATIENT_WIZARD_STEP6 = "/Patient/H360GuideWizard/Step6.aspx?DT={0}";

            public const string PAGE_TRACKER_BP = "/Patient/BloodPressure/MyBloodPressure.aspx";
            public const string PAGE_TRACKER_Cholesterol = "/Patient/Cholesterol/MyCholesterol.aspx";
            public const string PAGE_TRACKER_BG = "/Patient/BloodGlucose.aspx";
            public const string PAGE_TRACKER_Medication = "/Patient/Medication/MyMedication.aspx";
            public const string PAGE_TRACKER_Weight = "/Patient/Weight/MyWeight.aspx";
            public const string PAGE_TRACKER_Exercise = "/Patient/Exercise/MyExercise.aspx";

            public const string PAGE_PATIENT_RESOURCES_NoApp = "Patient/NewsAndArticles.aspx";

            //Patient dashboard pages Begin
            public const string PAGE_PATIENT_REPORT = "/Patient/GenerateReports.aspx";
            //Patient dashboard pages End

            public const string PAGE_PATIENT_WIZARD_Welcome = "/patient/h360guidewizard/welcome.aspx";
            public const string PAGE_PATIENT_WIZARD_PersonalInformationForm = "/patient/h360guidewizard/personalinformationform.aspx";
            public const string PAGE_PATIENT_WIZARD_HealthHistoryForm = "/patient/h360guidewizard/healthhistoryform.aspx";
            public const string PAGE_PATIENT_WIZARD_EmergencyContactForm = "/patient/h360guidewizard/emergencycontactform.aspx";
            public const string PAGE_PATIENT_WIZARD_ViewingYourData = "/patient/h360guidewizard/viewingyourdata.aspx";
            public const string PAGE_PATIENT_WIZARD_OrganizeDashboard = "/patient/h360guidewizard/organizedashboard.aspx";
            public const string PAGE_PATIENT_WIZARD_UsingTrackers = "/patient/h360guidewizard/usingtrackers.aspx";
            public const string PAGE_PATIENT_WIZARD_UnderstandingData = "/patient/h360guidewizard/understandingdata.aspx";
            public const string PAGE_PATIENT_WIZARD_EnteringData = "/patient/h360guidewizard/enteringdata.aspx";
            public const string PAGE_PATIENT_WIZARD_BloodPressureForm = "/patient/h360guidewizard/bloodpressureform.aspx";
            public const string PAGE_PATIENT_WIZARD_BloodGlucoseForm = "/patient/h360guidewizard/bloodglucoseform.aspx";
            public const string PAGE_PATIENT_WIZARD_CholesterolForm = "/patient/h360guidewizard/cholesterolform.aspx";
            public const string PAGE_PATIENT_WIZARD_MedicationForm = "/patient/h360guidewizard/medicationform.aspx";
            public const string PAGE_PATIENT_WIZARD_WeightForm = "/patient/h360guidewizard/weightform.aspx";
            public const string PAGE_PATIENT_WIZARD_PhysicalActivityForm = "/patient/h360guidewizard/physicalactivityform.aspx";
            public const string PAGE_PATIENT_WIZARD_UpdatingInformation = "/patient/h360guidewizard/updatinginformation.aspx";
            public const string PAGE_PATIENT_WIZARD_MobileDevice = "/patient/h360guidewizard/mobiledevice.aspx";
            public const string PAGE_PATIENT_WIZARD_SettingReminders = "/patient/h360guidewizard/settingreminders.aspx";
            public const string PAGE_PATIENT_WIZARD_WorkingWithProvider = "/patient/h360guidewizard/workingwithprovider.aspx";
            public const string PAGE_PATIENT_WIZARD_CreatingReports = "/patient/h360guidewizard/creatingreports.aspx";

            **/
        }
    }
}