﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Diagnostics;

namespace AHAAPI.App_AHA_Helper
{
    public class MedicationVocabHelper
    {
        public static void DownloadMedicationVocabulary()
        {
            VocabularyKey key = new VocabularyKey("RxNorm Active Medicines", "RxNorm", "07AC_080303F");

            XDocument xDoc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));

            Debug.WriteLine("Download start and counting.....\r\n***********************************************\r\n");

            int iDataSavedAt = 2000;
            string strFileName = string.Format("{0}Medication_{1}.xml", System.Web.HttpContext.Current.Server.MapPath("~/XmlFiles/"), DateTime.Now.Year.ToString());

            ApplicationConnection conn = new ApplicationConnection(Microsoft.Health.Web.WebApplicationConfiguration.AppId, Microsoft.Health.Web.WebApplicationConfiguration.HealthServiceUrl);
            conn.RequestTimeoutSeconds = 3600;
            conn.RequestTimeToLive = 3600;
            Vocabulary vocabulary = null;

            DateTime startMain = DateTime.Now;
            DateTime endMain = DateTime.Now;
            try
            {
                Debug.WriteLine("Downloading items...");
                DateTime start = DateTime.Now;
                vocabulary = conn.GetVocabulary(key, false);
                DateTime end = DateTime.Now;
                Debug.WriteLine("Downloading " + vocabulary.Count + " items Took " + (end - start).TotalMilliseconds.ToString() + "ms");
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return;
            }


            int totalCount = 0;

            bool bNewFile = true;
            while (true)
            {
                string lastVocabValue = null;

                foreach (KeyValuePair<string, VocabularyItem> item in vocabulary)
                {
                    if (bNewFile)
                    {
                        xDoc.Add(new XElement("VocabList",
                            new XElement("Vocab", new XAttribute("Key", item.Key),
                            new XElement("VocabularyName", item.Value.VocabularyName),
                            new XElement("DisplayText", item.Value.DisplayText),
                            new XElement("Version", item.Value.Version),
                            new XElement("AbbreviationText", item.Value.AbbreviationText),
                            new XElement("Family", item.Value.Family),
                            new XElement("Value", item.Value.Value))));
                        bNewFile = false;
                    }
                    else
                    {
                        XElement last = xDoc.XPathSelectElements("VocabList/Vocab").Last();
                        last.AddAfterSelf(new XElement("Vocab", new XAttribute("Key", item.Key),
                            new XElement("VocabularyName", item.Value.VocabularyName),
                            new XElement("DisplayText", item.Value.DisplayText),
                            new XElement("Version", item.Value.Version),
                            new XElement("AbbreviationText", item.Value.AbbreviationText),
                            new XElement("Family", item.Value.Family),
                            new XElement("Value", item.Value.Value)));
                    }

                    lastVocabValue = item.Key;
                }

                totalCount += vocabulary.Count;

                if (totalCount % iDataSavedAt == 0)
                {
                    Debug.WriteLine("Saving " + iDataSavedAt + " items to " + strFileName);
                    xDoc.Save(strFileName);
                    xDoc = null;
                    xDoc = XDocument.Load(strFileName);
                }

                //if (totalCount % iNewFileAt == 0)
                //{
                //    iBatch++;
                //    bNewFile = true;
                //    strFileName = string.Format("c:\\Snowmed\\{0}_Snomed_2009.xml", iBatch.ToString());
                //    Debug.WriteLine("Starting new batch & file {0}", strFileName);
                //    xDoc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"));
                //}

                Debug.WriteLine("Total Count....." + totalCount.ToString());
                endMain = DateTime.Now;
                Debug.WriteLine("Total time after download started: " + (endMain - startMain).TotalMinutes.ToString() + "minutes\r\n***********************************************\r\n");

                //if (totalCount == 2500)
                //    break;

                if (!vocabulary.IsTruncated)
                    break;

                key = new VocabularyKey(key.Name, key.Family, key.Version, lastVocabValue);
                Debug.WriteLine("Downloading items...");
                DateTime start = DateTime.Now;
                vocabulary = conn.GetVocabulary(key, false);
                DateTime end = DateTime.Now;
                Debug.WriteLine("Downloading " + vocabulary.Count + " Took " + (end - start).TotalMilliseconds.ToString() + "ms");
            }

            if (bNewFile)
            {
                xDoc.Add(new XElement("VocabList",
                    new XElement("Vocab", new XAttribute("Key", "Key"),
                    new XElement("VocabularyName", "VocabularyName"),
                    new XElement("DisplayText", "DisplayText"),
                    new XElement("Version", "Version"),
                    new XElement("AbbreviationText", "AbbreviationText"),
                    new XElement("Family", "Family"),
                    new XElement("Value", "Value"))));
            }
            else
            {
                XElement lastOne = xDoc.XPathSelectElements("VocabList/Vocab").Last();
                lastOne.AddAfterSelf(new XElement("Vocab", new XAttribute("Key", "Key"),
                    new XElement("VocabularyName", "VocabularyName"),
                    new XElement("DisplayText", "DisplayText"),
                    new XElement("Version", "Version"),
                    new XElement("AbbreviationText", "AbbreviationText"),
                    new XElement("Family", "Family"),
                    new XElement("Value", "Value")));
            }

            xDoc.Save(strFileName);
            endMain = DateTime.Now;
            Debug.WriteLine("Total time taken to download medication "+ (endMain - startMain).TotalMinutes.ToString() +"minutes");
        }
    }
}
