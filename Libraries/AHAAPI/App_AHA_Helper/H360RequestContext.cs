﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using GRCBase;
using AHAHelpContent;
using AHAAPI.Provider;
using System.Globalization;

namespace AHAAPI
{
    public class H360RequestContext
    {
        public HttpContext CurrentContext
        {
            get;
            set;
        }

        public string ContextUrl
        {
            get;
            set;
        }

        public ProviderDetails ProviderDetails
        {
            get;
            set;
        }

        public List<AHAHelpContent.Patient> PatientList
        {
            get;
            set;
        }

        public int? TotalLoginCount
        {
            get;
            set;
        }

        public AHAHelpContent.Language SelectedLanguage
        {
            get;
            set;
        }


        public H360RequestContext(System.Web.HttpContext currentContext)
        {
            CurrentContext = currentContext;
        }

        public static H360RequestContext GetContext()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new System.Exception("This call is only valid during an HTTP request");
            }

            System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

            if (currentContext.Items["H360RequestContext"] == null)
            {
                return null;
            }

            return currentContext.Items["H360RequestContext"] as H360RequestContext;
        }

        public static void PopulateContextData()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new System.Exception("This call is only valid during an HTTP request");
            }

            System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

            if (currentContext.Items["H360RequestContext"] != null)
            {
                throw new System.Exception("H360RequestContext already populated");
            }

            currentContext.Items["H360RequestContext"] = new H360RequestContext(System.Web.HttpContext.Current);


            H360RequestContext objH360RequestContext = currentContext.Items["H360RequestContext"] as H360RequestContext;
            objH360RequestContext.DoPopulate();
        }

        private void DoPopulate()
        {
            ContextUrl = UrlUtility.FullCurrentPageUrl;

            Uri uriContext = new Uri(ContextUrl);

            if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
            {
                ProviderContext objPContext = AHAAPI.Provider.SessionInfo.GetProviderContext();
                if (objPContext != null)
                {
                    ProviderDetails = ProviderDetails.FindByProviderID(objPContext.ProviderID);

                    PatientList = AHAHelpContent.Patient.FindAllByProviderID(ProviderDetails.ProviderID);

                    TotalLoginCount = AHAHelpContent.ProviderLoginDetails.GetProviderLoginCount(ProviderDetails.ProviderID);

                    LanguageHelper.SetLocaleAndCulture(ProviderDetails.LanguageLocale);

                    SelectedLanguage = new Language { LanguageID = ProviderDetails.DefaultLanguageID, LanguageLocale = ProviderDetails.LanguageLocale };
                }
            }

            if (SessionInfo.IsUserLoggedIn)
            {
                UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
                if (userIdentityContext != null && !string.IsNullOrEmpty(userIdentityContext.LanguageLocale))
                {
                    LanguageHelper.SetLocaleAndCulture(userIdentityContext.LanguageLocale);
                    SelectedLanguage = new Language { LanguageID = userIdentityContext.LanguageID.Value, LanguageLocale = userIdentityContext.LanguageLocale };
                }
            }

            if (SelectedLanguage == null || (!AHAAPI.Provider.SessionInfo.IsProviderLoggedIn() 
                && !SessionInfo.IsUserLoggedIn))
            {
                LanguageHelper.SetLocaleAndCulture(LanguageHelper.GetSelectedLanguageLocaleForVisitor());
                SelectedLanguage = AHAHelpContent.Language.GetLanguageByLocale(LanguageHelper.GetSelectedLanguageLocaleForVisitor());
            }
        }
    }
}
