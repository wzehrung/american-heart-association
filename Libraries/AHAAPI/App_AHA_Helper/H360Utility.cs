﻿using System;
using System.Collections.Generic;
using Microsoft.Health;
using Microsoft.Health.Web;
using System.Collections.ObjectModel;
using Microsoft.Health.ItemTypes;
using System.Xml.XPath;
using System.Configuration;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using AHAHelpContent;

namespace AHAAPI
{
    [Serializable]
    public class PersonRecord
    {
        public Guid PersonGUID
        {
            get;
            set;
        }

        public Guid RecordGUID
        {
            get;
            set;
        }

        public bool HasGivenOffineAccess
        {
            get;
            set;
        }
    }

    public enum TrackerType
    {
        [Description("Blood Pressure")]
        BloodPressure,
        [Description("Cholesterol")]
        Cholesterol,
        [Description("Blood Glucose")]
        BloodGlucose,
        [Description("Medication")]
        Medication,
        [Description("Weight")]
        Weight,
        [Description("Physical Activity")]
        PhysicalActivity,
        [Description("HbA1c")]
        HbA1c
    }

    public class H360Utility
    {
        private const string sessionPatientAnnouncementPreview = "patientannouncementpreview";
        
        public static int GetCurrentLanguageID()
        {
            H360RequestContext objContext = H360RequestContext.GetContext();
            if (objContext != null)
            {
                return objContext.SelectedLanguage.LanguageID;
            }

            return AHAHelpContent.Language.GetDefaultLanguage().LanguageID;
        }

        public static string GetTrademarkedHeart360()
        {
            return "Heart360<sup>&reg;</sup>";  // "Heart360<sup>&#174;</sup>";
        }

        public static string GetTrademarkedHealthVault()
        {
            return "HealthVault&trade;";    // "HealthVault&#153;";
        }

        public static string GetRegisteredMicrosoft()
        {
            return "Microsoft<sup>&reg;</sup>"; // "Microsoft<sup>&#174;</sup>";
        }

        /// <summary>
        /// To convert mmol/l of glucose to mg/dL, multiply by 18.
        /// http://www.faqs.org/faqs/diabetes/faq/part1/section-9.html
        /// </summary>
        /// <param name="dMmol"></param>
        /// <returns>double</returns>
        public static double GlocoseMmolToMgdl(double dMmol)
        {
            return dMmol * 18;
        }

        /// <summary>
        /// To convert mg/dL of glucose to mmol/l, divide by 18 or multiply by 0.055.
        /// http://www.faqs.org/faqs/diabetes/faq/part1/section-9.html
        /// </summary>
        /// <param name="dMgdl"></param>
        /// <returns>double</returns>
        public static double GlucoseMgdlToMmol(double dMgdl)
        {
            return dMgdl / 18;
        }

        public static double HbA1cToPercentage(double dHbA1c)
        {
            return dHbA1c * 100;
        }

        public static bool IsUseNewValue(string strOldValue, string strNewValue)
        {
            //We consider null and empty string the same
            if (String.IsNullOrEmpty(strOldValue) && String.IsNullOrEmpty(strNewValue))
                return false;

            return (strOldValue != strNewValue);
        }

        public static bool IsUseNewValue(DateTime? dtOldValue, DateTime dtNewValue)
        {
            if (!dtOldValue.HasValue)
                return true;

            return (dtOldValue != dtNewValue);
        }

        public static bool IsUseNewValue(DateTime? dtOldValue, DateTime? dtNewValue)
        {
            if (!dtOldValue.HasValue && !dtNewValue.HasValue)
                return false;

            return (dtOldValue != dtNewValue);
        }

        public static bool IsUseNewValue(double? dOldValue, double dNewValue)
        {
            if (!dOldValue.HasValue)
                return true;

            return (dOldValue != dNewValue);
        }



        public static bool IsUseNewValue(int? iOldValue, int iNewValue)
        {
            if (!iOldValue.HasValue)
                return true;


            return (iOldValue != iNewValue);
        }

        public static bool IsUseNewValue(int? iOldValue, int? iNewValue)
        {
            if (!iOldValue.HasValue && !iNewValue.HasValue)
                return false;

            return (iOldValue != iNewValue);
        }

        public static bool IsUseNewValue(bool? bOldValue, bool bNewValue)
        {
            if (!bOldValue.HasValue)
                return true;
            return (bOldValue.Value != bNewValue);
        }

        /// <summary>
        /// checks for zip code format
        /// </summary>
        /// <param name="strZipCode">
        /// Zipcode value
        /// </param>
        /// <param name="isrequired">
        /// specifies it zip code is a required field.    
        /// </param>
        /// <returns>
        /// true, if valid zipcode else returns false.
        /// </returns>
        public static bool IsValidZipCode(string strZipCode, bool isrequired)
        {
            if (isrequired && String.IsNullOrEmpty(strZipCode))
                return false;


            if (strZipCode.Length == 5)
            {
                try
                {
                    int iZip = Convert.ToInt32(strZipCode);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            if (strZipCode.Length == 10)
            {
                try
                {
                    string[] strArr = strZipCode.Split("-".ToCharArray());

                    if (strArr != null && strArr.Length == 2)
                    {
                        try
                        {
                            int iZip1 = Convert.ToInt32(strArr[0]);
                            int iZip2 = Convert.ToInt32(strArr[1]);
                            return true;
                        }
                        catch
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        public static void AddImmediateExpiryHeadersToResponse()
        {
            if (System.Web.HttpContext.Current.Request.HttpMethod.ToLower() == "get")
            {
                System.Web.HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
                System.Web.HttpContext.Current.Response.Expires = 0;
                System.Web.HttpContext.Current.Response.Cache.SetNoStore();
                System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
            }
            return;
        }

        public static void RedirectToHVCreateAccountPage(string strTargetPage)
        {
            StringBuilder sb = new StringBuilder(128);
            sb.Append("appid=");
            sb.Append(WebApplicationConfiguration.AppId.ToString());
            //sb.Append("&offopt1=Offline");
            sb.Append("&actionqs=");
            sb.Append(System.Web.HttpUtility.UrlEncode(strTargetPage));
            Microsoft.Health.Web.WebApplicationUtilities.RedirectToShellUrl(
                System.Web.HttpContext.Current,
                "CREATEACCOUNT",
                sb.ToString());
        }

        public static void RedirectToHVSelectRecordPageWithForceAppAuth(string strTargetPage)
        {
            StringBuilder sb = new StringBuilder(128);
            sb.Append("appid=");
            sb.Append(WebApplicationConfiguration.AppId.ToString());
            sb.Append("&forceappauth=true");
            sb.Append("&actionqs=");
            sb.Append(HttpUtility.UrlEncode(strTargetPage));
            Microsoft.Health.Web.WebApplicationUtilities.RedirectToShellUrl(
                System.Web.HttpContext.Current,
                "AUTH",
                sb.ToString());
        }

        public static void RedirectToHVSelectRecordPage(string strTargetPage)
        {
            StringBuilder sb = new StringBuilder(128);
            sb.Append("appid=");
            sb.Append(WebApplicationConfiguration.AppId.ToString());
            sb.Append("&actionqs=");
            sb.Append(HttpUtility.UrlEncode(strTargetPage));
            Microsoft.Health.Web.WebApplicationUtilities.RedirectToShellUrl(
                System.Web.HttpContext.Current,
                "APPAUTH",
                sb.ToString());
        }

        public static void RedirectToHVSelectRecordOfflineAccessForFilePage(string strTargetPage)
        {
            StringBuilder sb = new StringBuilder(128);
            sb.Append("appid=");
            sb.Append(WebApplicationConfiguration.AppId.ToString());
            sb.Append("&offopt1=File&onopt1=File");
            sb.Append("&actionqs=");
            sb.Append(HttpUtility.UrlEncode(strTargetPage));
            Microsoft.Health.Web.WebApplicationUtilities.RedirectToShellUrl(
                System.Web.HttpContext.Current,
                "APPAUTH",
                sb.ToString());
        }

        public static bool HasPatientGivenAccessForFileType(AuthenticatedConnection conn, Guid hvpersonid, Guid hvrecordid, out bool bIsOnlineConnection)
        {
            HealthRecordAccessor accessor = new HealthRecordAccessor(conn, hvrecordid);

            IList<Guid> idlist = new List<Guid>();
            idlist.Add(File.TypeId);

            Collection<HealthRecordItemTypePermission> list = accessor.QueryPermissions(idlist);

            bIsOnlineConnection = true;
            bool bHasOfflineAccess = true;

            if (list.Count > 0)
            {
                if (list[0].OfflineAccessPermissions != HealthRecordItemPermissions.All)
                {
                    bHasOfflineAccess = false;
                }
                if (list[0].OnlineAccessPermissions != HealthRecordItemPermissions.All)
                {
                    bIsOnlineConnection = false;
                }
            }
            else
            {
                bHasOfflineAccess = false;
                bIsOnlineConnection = false;
            }

            return bHasOfflineAccess || bIsOnlineConnection;
        }

        public static void RedirectToHVSelectRecordOfflineAccessPage()
        {
            StringBuilder sb = new StringBuilder(128);
            sb.Append("appid=");
            sb.Append(WebApplicationConfiguration.AppId.ToString());
            sb.Append("&offopt1=Offline");
            //sb.Append("&actionqs=");
            //sb.Append(System.Web.HttpUtility.UrlEncode("Context=" + strContext + "&HVGUID=" + strHVGUID + "&InviteGUID=" + strInvitationGUID + "&TargetURL=" + strTargetPage));
            Microsoft.Health.Web.WebApplicationUtilities.RedirectToShellUrl(
                System.Web.HttpContext.Current,
                "APPAUTH",
                sb.ToString());
        }

        public static bool HasPatientGivenOfflineAccessToApp(Guid hvpersonid, Guid hvrecordid)
        {
            OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
            offlineConn.Authenticate();
            HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);

            IList<Guid> idlist = new List<Guid>();
            idlist.Add(Microsoft.Health.ItemTypes.BloodPressure.TypeId);
            idlist.Add(Personal.TypeId);

            //System.Web.HttpContext.Current.Session["RecordId"] = hvrecordid;

            Collection<HealthRecordItemTypePermission> list  = null;
            try
            {
                list = accessor.QueryPermissions(idlist);
            }
            catch (Microsoft.Health.HealthServiceException hse)
            {
                if (hse.ErrorCode == HealthServiceStatusCode.InvalidRecordState ||
                    hse.ErrorCode == HealthServiceStatusCode.AccessDenied)
                {
                    if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
                    {
                        //Remove from db  (with some comment)
                        AHAHelpContent.ProviderDetails.DisconnectPatientOrProvider(AHAAPI.Provider.SessionInfo.GetProviderContext().ProviderID, hvrecordid);
                        System.Web.HttpContext.Current.Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
                    }
                }
                    
            }

            bool bHasOfflineAccess = true;

            foreach (HealthRecordItemTypePermission item in list)
            {
                if (item.OfflineAccessPermissions != HealthRecordItemPermissions.All)
                {
                    bHasOfflineAccess = false;
                    break;
                }
            }

            //if (context != null)
            //    context.HasUserGivenOfflineAccess = bHasOfflineAccess;

            return bHasOfflineAccess;
        }

        public static bool IsGUID(string strGUIDCandidate)
        {
            try
            {
                Guid myGuid = new Guid(strGUIDCandidate);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static PersonRecord GetPersonRecordDetailsFromRequest()
        {
            string strPersonGUID = System.Web.HttpContext.Current.Request["pg"];
            string strRecordGUID = System.Web.HttpContext.Current.Request["rg"];

            if (!IsGUID(strPersonGUID) || !IsGUID(strRecordGUID))
            {
                throw new ArgumentException("No RecordId & Person ID found!");
            }

            return new PersonRecord { PersonGUID = new Guid(strPersonGUID), RecordGUID = new Guid(strRecordGUID) };
        }

        public static double? ParseDouble(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            double valueDouble = xNav_Item.ValueAsDouble;

            return valueDouble;
        }

        public static int? ParseInt(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            int valueInt = xNav_Item.ValueAsInt;

            return valueInt;
        }

        public static DateTime? ParseDateTime(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            DateTime valueDateTime = xNav_Item.ValueAsDateTime;

            return valueDateTime;
        }

        /// <summary>
        /// Parse a bool out of the XML
        /// </summary>
        /// <param name="xPathNav"></param>
        /// <returns></returns>
        public static bool? ParseTrueFalseNullMissing(XPathNavigator xPathNav)
        {
            //If there is no node... it's null
            if (xPathNav == null) return null;

            string valueText = xPathNav.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xPathNav.ValueAsBoolean;
        }

        public static bool? SelectAndParse_TrueFalseNullMissing(
            XPathNavigator xPathNav,
            string query)
        {
            XPathNavigator xNavItem = xPathNav.SelectSingleNode(query);
            return ParseTrueFalseNullMissing(xNavItem);
        }

        /// <summary>
        /// Parses a TRUE/FALSE/NULL value
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="xPath"></param>
        /// <returns></returns>
        public static bool? ParseTrueFalseNull(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xNav_Item.ValueAsBoolean;
        }

        public static string ParseString(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return valueText;
        }

        public static void MSHealthSignOut(string strTargetPage)
        {
            Microsoft.Health.Web.WebApplicationUtilities.SavePersonInfoToCookie(System.Web.HttpContext.Current, null, true);

            SessionInfo.ClearLoggedInUserState();

            string strHttp = System.Web.HttpContext.Current.Request.IsSecureConnection ? "https://" : "http://";

            //string url = string.Concat(new object[] { ConfigurationManager.AppSettings["ShellUrl"] + "redirect.aspx?target=", "APPSIGNOUT&targetqs=?appid=", Microsoft.Health.Web.WebApplicationConfiguration.AppId, System.Web.HttpContext.Current.Server.UrlEncode("&redirect=" + strHttp + System.Web.HttpContext.Current.Request["SERVER_NAME"] + strTargetPage) });

            string url = string.Concat(new object[] { 
                ConfigurationManager.AppSettings["ShellUrl"] + "redirect.aspx?target=", 
                "APPSIGNOUT&targetqs=?appid=", 
                Microsoft.Health.Web.WebApplicationConfiguration.AppId, 
                System.Web.HttpContext.Current.Server.UrlEncode("&redirect=" + strHttp + System.Web.HttpContext.Current.Request["SERVER_NAME"] + "/redirect.aspx&actionqs=" + strHttp + System.Web.HttpContext.Current.Request["SERVER_NAME"] + strTargetPage) 
            });


            System.Web.HttpContext.Current.Response.Write(GRCBase.MiscUtility.GetScriptForRedirection(url));
            System.Web.HttpContext.Current.Response.End();
        }

        public static void MSHealthSignOut(string strTargetPage, bool bClearSession)
        {
            if (bClearSession)
            {
                Microsoft.Health.Web.WebApplicationUtilities.SavePersonInfoToCookie(System.Web.HttpContext.Current, null, true);

                SessionInfo.ClearLoggedInUserState();
            }

            string strHttp = System.Web.HttpContext.Current.Request.IsSecureConnection ? "https://" : "http://";

            string url = string.Concat(new object[] { 
                ConfigurationManager.AppSettings["ShellUrl"] + "redirect.aspx?target=", 
                "APPSIGNOUT&targetqs=?appid=", 
                Microsoft.Health.Web.WebApplicationConfiguration.AppId, 
                System.Web.HttpContext.Current.Server.UrlEncode("&redirect=" + strHttp + System.Web.HttpContext.Current.Request["SERVER_NAME"] + "/redirect.aspx&actionqs=" + strHttp + System.Web.HttpContext.Current.Request["SERVER_NAME"] + strTargetPage) 
            });

            System.Web.HttpContext.Current.Response.Write(GRCBase.MiscUtility.GetScriptForRedirection(url));
            System.Web.HttpContext.Current.Response.End();
        }

        //
        // PJB: THIS CAMPAIGN STUFF SHOULD PROBABLY GO IN SessionInfo.cs
        // WZ: Copied this into AHAProvider.SessionInfo.cs
        //
        #region campaign
        private const string sessionKeyCampaign = "Heart360Campaign";

        public static void SetCampaignSession(int iCampaignID)
        {
            System.Web.HttpContext.Current.Session[sessionKeyCampaign] = iCampaignID;
        }

        public static int? GetCurrentCampaignID()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[sessionKeyCampaign] != null)
            {
                return (int)System.Web.HttpContext.Current.Session[sessionKeyCampaign];
            }
            return null;
        }

        private const String sessionKeyCampaignUrl = "Heart360CampaignUrl";

        public static void SetCampaignSessionUrl(String iCampaignUrl)
        {
            System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl] = iCampaignUrl;
        }

        public static String GetCurrentCampaignUrl()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl] != null)
            {
                return (string)System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl];
            }
            return null;
        }

        public static void ResetCampaignSession()
        {
            System.Web.HttpContext.Current.Session[sessionKeyCampaign] = null;
            System.Web.HttpContext.Current.Session[sessionKeyCampaignUrl] = null;
        }
        #endregion

        public static string RenderLink(string strContent)
        {
            //Construct Links
            if (!string.IsNullOrEmpty(strContent))
            {
                strContent = Regex.Replace(strContent,
                    "\\[(L|l)(I|i)(N|n)(K|k)(\\s)*(T|t)(E|e)(X|x)(T|t)(\\s)*=(\\s)*'.*'(\\s)*(U|u)(R|r)(L|l)(\\s)*=(\\s)*'.*'(\\s)*\\]",
                    delegate(Match match)
                    {
                        string strMatchText = match.Value;
                        strMatchText = Regex.Replace(strMatchText, "\\[(L|l)(I|i)(N|n)(K|k)(\\s)*", string.Empty);
                        strMatchText = Regex.Replace(strMatchText, "(\\s)*\\]", string.Empty);

                        //Remove unwanted space
                        strMatchText = Regex.Replace(strMatchText, "(\\s)*=(\\s)*", "=");
                        strMatchText = Regex.Replace(strMatchText, "'(\\s)+", "',");

                        string strText = string.Empty;
                        string strHref = string.Empty;

                        strMatchText.Split((",".ToCharArray())).ToList().ForEach(delegate(string strAttributes)
                        {
                            if (strAttributes.ToLower().Contains("text"))
                                strText = Regex.Replace(strAttributes, "(T|t)(E|e)(X|x)(T|t)(\\s)*=(\\s)*", string.Empty).Replace("'", string.Empty);

                            if (strAttributes.ToLower().Contains("url"))
                                strHref = Regex.Replace(strAttributes, "(U|u)(R|r)(L|l)(\\s)*=(\\s)*", string.Empty).Replace("'", string.Empty);
                        });

                        if (strHref.ToLower().StartsWith("#"))
                        {
                            return string.Format("<a href=\"{0}\">{1}</a>", strHref, strText);
                        }
                        else
                        {
                            if (!strHref.ToLower().StartsWith("http://"))
                            {
                                strHref = strHref.Insert(0, "http://");
                            }
                        }

                        return string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", strHref, strText);
                    });
            }

            return strContent;
        }

        // WZ - These are the only items that reference AHAHelpContent
        #region AHAHelpContent
        public static void SetPatientAnnouncementPreviewToSession(Announcement obj)
        {
            System.Web.HttpContext.Current.Session[sessionPatientAnnouncementPreview] = obj;
        }

        public static void ResetPatientAnnouncementPreviewToSession()
        {
            System.Web.HttpContext.Current.Session[sessionPatientAnnouncementPreview] = null;
        }

        public static Announcement GetPatientAnnouncementPreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionPatientAnnouncementPreview] != null)
            {
                return System.Web.HttpContext.Current.Session[sessionPatientAnnouncementPreview] as Announcement;
            }
            return null;
        }
        #endregion
    }

    public enum WizardItemType
    {
        [Description("Blood Pressure")]
        BloodPressure,
        [Description("Cholesterol")]
        Cholesterol,
        [Description("Blood Glucose")]
        BloodGlucose,
        [Description("Medication")]
        Medication,
        [Description("Weight")]
        Weight,
        [Description("Physical Activity")]
        PhysicalActivity,
        [Description("Height")]
        Height
    }
}