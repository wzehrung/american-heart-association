﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Microsoft.Health.Web;

namespace AHAAPI.Provider
{
    public class ProviderBase : Page
    {
        public Guid? RecordID
        {
            get;
            set;
        }

        public Guid? PersonID
        {
            get;
            set;
        }

        public void ForceInit()
        {
            _EnsureProviderAccess();

            _ProcessRequest();
        }

        protected override void OnPreInit(EventArgs e)
        {
            _EnsureProviderAccess();

            _ProcessRequest();
            
            base.OnPreInit(e);
        }

        private void _ProcessRequest()
        {
            string strPersonGUID = System.Web.HttpContext.Current.Request["pg"];
            string strRecordGUID = System.Web.HttpContext.Current.Request["rg"];

            if (H360Utility.IsGUID(strPersonGUID) && H360Utility.IsGUID(strRecordGUID))
            {
                PersonID = new Guid(strPersonGUID);
                RecordID = new Guid(strRecordGUID);

                PersonRecord obj = H360Utility.GetPersonRecordDetailsFromRequest();

                AHAAPI.Provider.ProviderContext objProContext = AHAAPI.Provider.SessionInfo.GetProviderContext();
                bool bCheckForOfflineAccess = false;

                if (objProContext.PersonRecordItem == null)
                {
                    objProContext.PersonRecordItem = obj;
                    bCheckForOfflineAccess = true;
                }
                else
                {
                    if (obj.PersonGUID != objProContext.PersonRecordItem.PersonGUID ||
                        obj.RecordGUID != objProContext.PersonRecordItem.RecordGUID)
                    {
                        objProContext.PersonRecordItem = obj;
                        bCheckForOfflineAccess = true;
                    }
                }

                if (bCheckForOfflineAccess)
                {
                    objProContext.PersonRecordItem.HasGivenOffineAccess = H360Utility.HasPatientGivenOfflineAccessToApp(PersonID.Value, RecordID.Value);
                }

                if (!objProContext.PersonRecordItem.HasGivenOffineAccess)
                {
                    System.Web.HttpContext.Current.Response.Redirect(string.Format("{0}", AHAAPI.Provider.ProviderPageUrl.PATIENT_DATA_ACCESS_DENIED), true);
                }

                if (PersonID.HasValue && RecordID.HasValue)
                {
                    AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserOfflineHealthRecordGUID(RecordID.Value);

                    if (objPatient == null)
                    {
                        throw new InvalidProgramException(string.Format("Invalid Patient record guid - {0}!", RecordID.Value));
                    }

                    bool bIsValidPatientForProvider = AHAHelpContent.ProviderDetails.IsValidPatientForProvider(objPatient.UserHealthRecordID, objProContext.ProviderID);

                    if (!bIsValidPatientForProvider)
                    {
                        //the provider does not have access to this record
                        System.Web.HttpContext.Current.Response.Redirect(string.Format("{0}", AHAAPI.Provider.ProviderPageUrl.PATIENT_DATA_ACCESS_DENIED), true);
                    }
                }
            }
        }

        private void _EnsureProviderAccess()
        {
            if (!SessionInfo.IsProviderLoggedIn())
            {
                Microsoft.Health.PersonInfo objPersonInfo = Microsoft.Health.Web.WebApplicationUtilities.LoadPersonInfoFromCookie(System.Web.HttpContext.Current);

                if (objPersonInfo != null)
                {
                    Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);
                }
                else
                {
                    //System.Web.HttpContext.Current.Response.Redirect(string.Format("{0}?ReturnUrl={1}", AHAAPI.Provider.ProviderPageUrl.LOGIN, System.Web.HttpContext.Current.Server.UrlEncode(GRCBase.UrlUtility.FullCurrentPageUrlWithQV)));
                    string strRedirect = "<script>if (window.opener != null) {window.opener.location.href = '" + AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP + "';window.close();}else {window.location.href = '"+ AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP + "';}</script>";
                    System.Web.HttpContext.Current.Response.Write(strRedirect);
                    //System.Web.HttpContext.Current.Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT);
                    System.Web.HttpContext.Current.Response.End();
                    return;
                }
            }
        }
    }
}
