﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AHAAPI.Provider
{
    public class ProviderPageUrl
    {
        public const string HOME = "~/Provider/Home.aspx";
        public const string HOME_NoApp = "/Provider/Home.aspx";
        public const string PATIENT_DATA_ACCESS_DENIED = "~/Provider/PatientDataAccessDenied.aspx";
        public const string PATIENT_DASHBOARD = "~/Provider/PatientDetails/DashBoard.aspx";
        public const string PATIENT_DASHBOARD_NO_APP = "/Provider/PatientDetails/DashBoard.aspx";
        public const string REGISTRATION = "/Provider/Register.aspx";
        public const string REGISTRATION_CONFIRMATION = "~/Provider/Confirmation.aspx";
        public const string LOGIN = "/Provider/Login.aspx";
        public const string FORGOT_PASSWORD = "~/Provider/ForgotPassword.aspx";
        public const string RESET_PASSWORD = "~/Provider/ResetPassword.aspx";
        public const string RESET_PASSWORD_NO_APP_PATH = "/Provider/ResetPassword.aspx";
        public const string INFO = "~/Provider/Info.aspx";
        public const string GROUP_CREATE = "~/Provider/Groups/CreateGroup.aspx";
        public const string GROUP_CREATE_NoApp = "/Provider/Groups/CreateGroup.aspx";
        public const string GROUP_UPDATE = "~/Provider/Groups/UpdateGroup.aspx";
        public const string GROUP_VIEW = "~/Provider/Groups/ViewIndividualGroup.aspx";
        public const string GROUP_LIST_VIEW = "~/Provider/Groups/ViewGroups.aspx";
        public const string GROUP_ADD_PATIENT = "~/Provider/Groups/AddPatientToGroup.aspx";
        public const string GROUP_ADD_GROUPS_FOR_PATIENT = "~/Provider/Groups/AddGroupsToPatient.aspx";
        public const string INVITATION_ACCEPT = "~/Provider/Invitation/AcceptInvitation.aspx";
        public const string INVITATION_INVITE = "~/Provider/Invitation/InvitePatient.aspx";
        public const string INVITATION_INVITE_NoApp = "/Provider/Invitation/InvitePatient.aspx";
        public const string PUBLIC_INVITATION_ACCEPT = "/ConnectPatient.aspx";
        public const string RESOURCES = "~/Provider/Resources.aspx";
        public const string ALERTS = "~/Provider/Alert/Alerts.aspx";
        public const string CREATE_ALERT = "~/Provider/Alert/CreateAlert.aspx";
        public const string CREATE_ALERT_RULE = "~/Provider/Alert/CreateAlertRule.aspx";
        public const string REMOVE_PATIENT_ALERT_RULE = "~/Provider/Alert/RemovePatientFromAlertRule.aspx";
        public const string EDIT_ALERT_RULE = "~/Provider/Alert/EditAlertRule.aspx";
        public const string UPDATE_PROFILE = "~/Provider/UpdateProfile.aspx";
        public const string MAIL = "~/provider/mail/mailhome.aspx";
        public const string REPORT_HOME = "~/Provider/Reports/IndividualPatientReport.aspx";
        public const string REPORT_SPR = "~/Provider/Reports/StandardPracticeReport.aspx";
        public const string REPORT_CPR = "~/Provider/Reports/CustomPracticeReport.aspx";
        public const string REPORT_CSV = "~/Provider/Reports/DownloadCSV.aspx";
        public const string REPORT_HOME_NoApp = "/Provider/Reports/IndividualPatientReport.aspx";
        public const string REPORT_SPR_NoApp = "/Provider/Reports/StandardPracticeReport.aspx";
        public const string REPORT_CPR_NoApp = "/Provider/Reports/CustomPracticeReport.aspx";
        public const string REPORT_CSV_NoApp = "/Provider/Reports/DownloadCSV.aspx";

                

        public const string PATIENT_SUMMARY = "~/Provider/PatientDetails/DashBoard.aspx";
        public const string PATIENT_ALERTS = "~/Provider/PatientDetails/Alerts.aspx";
        public const string PATIENT_GROUP_MEMBERSHIP = "~/Provider/PatientDetails/GroupMembership.aspx";
        public const string PATIENT_BLOOD_PRESSURE = "~/Provider/PatientDetails/BloodPressure/View.aspx";
        public const string PATIENT_BLOOD_GLUCOSE = "~/Provider/PatientDetails/BloodGlucose/View.aspx";
        public const string PATIENT_WEIGHT = "~/Provider/PatientDetails/Weight/View.aspx";
        public const string PATIENT_PHYSICAL_ACTIVITY = "~/Provider/PatientDetails/PhysicalActivity/View.aspx";
        public const string PATIENT_CHOLESTEROL = "~/Provider/PatientDetails/Cholesterol/View.aspx";
        public const string PATIENT_MEDICATIONS = "~/Provider/PatientDetails/Medication/View.aspx";
        public const string PATIENT_HEALTH_HISTORY = "~/Provider/PatientDetails/HealthHistory.aspx";
        public const string PATIENT_REPORTS = "~/Provider/PatientDetails/Report.aspx";
    }
}
