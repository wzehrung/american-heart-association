﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AHAAPI
{
    public class HVErrorConditions
    {
        public static void ThrowCouldNotParseOneToFiveValue()
        {
            throw new Exception("Value was not in between 1 and 5");
        }
    }
}
