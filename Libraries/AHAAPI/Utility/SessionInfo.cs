namespace AHAAPI
{
    using System;
    using System.Configuration;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.SessionState;
    using Microsoft.Health;
    using Microsoft.Health.Web;
    using System.Collections.Specialized;

    [Serializable]
    public class MLCInfo
    {
        public NameValueCollection NameValueCollection
        {
            get;
            set;
        }

        public Guid RecordID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }

    public enum PatientInvitationMode
    {
        ProviderCode,
        PrintOrEmail,
        SearchMode
    }

    [Serializable]
    public class PatientInvitation
    {
        public string InvitationCode
        {
            get;
            set;
        }

        public string ProviderCode
        {
            get;
            set;
        }

        public bool IsPatientAccept
        {
            get;
            set;
        }

        public PatientInvitationMode Mode
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Identifies a specific user & their selected record (since users can have more than 1 record they work with)
    /// </summary>
    [Serializable]
    public class UserIdentityContext
    {
        Guid m_UserId;
        Guid m_RecordId;
        Guid? m_SelfRecordId;
        Guid m_PersonalItemGUID;
        bool m_IsUserFirstTimeLoggedIn;
        bool m_HasUserAgreedToTermsAndConditions;
        bool m_HasUserAgreedToNewTermsAndConditions;
        bool m_IsShowGSE;
        bool m_HasUserConsented;
        bool? m_HasUserGivenOfflineAccess;
        int? m_PatientID;
        int? m_CampaignID;
        bool m_HasCompletedProfile;

        public string CampaignUrl
        {
            get;
            set;
        }

        public string LanguageLocale
        {
            get;
            set;
        }

        public int? LanguageID
        {
            get;
            set;
        }

        public Guid UserId
        {
            get { return m_UserId; }
            set { m_UserId = value; }
        }

        public Guid RecordId
        {
            get { return m_RecordId; }
            set { m_RecordId = value; }
        }

        public Guid? SelfRecordId
        {
            get { return m_SelfRecordId; }
            set { m_SelfRecordId = value; }
        }

        public Guid PersonalItemGUID
        {
            get { return m_PersonalItemGUID; }
            set { m_PersonalItemGUID = value; }
        }

        public bool IsUserFirstTimeLoggedIn
        {
            get { return m_IsUserFirstTimeLoggedIn; }
            set { m_IsUserFirstTimeLoggedIn = value; }
        }

        public bool HasUserAgreedToTermsAndConditions
        {
            get { return m_HasUserAgreedToTermsAndConditions; }
            set { m_HasUserAgreedToTermsAndConditions = value; }
        }

        public bool HasUserAgreedToNewTermsAndConditions
        {
            get { return m_HasUserAgreedToNewTermsAndConditions; }
            set { m_HasUserAgreedToNewTermsAndConditions = value; }
        }

        public bool IsShowGSE
        {
            get { return m_IsShowGSE; }
            set { m_IsShowGSE = value; }
        }

        public bool HasUserConsented
        {
            get { return m_HasUserConsented; }
            set { m_HasUserConsented = value; }
        }

        public bool? HasUserGivenOfflineAccess
        {
            get { return m_HasUserGivenOfflineAccess; }
            set { m_HasUserGivenOfflineAccess = value; }
        }

        public int? PatientID
        {
            get { return m_PatientID; }
            set { m_PatientID = value; }
        }

        public int? CampaignID
        {
            get { return m_CampaignID; }
            set { m_CampaignID = value; }
        }

        public bool HasCompletedProfile
        {
            get { return m_HasCompletedProfile; }
            set { m_HasCompletedProfile = value; }
        }


        public UserIdentityContext(Guid userId, Guid recordId, Guid? selfRecordId, Guid personalItemGUID, bool bIsUserFirstTimeLoggedIn, bool bHasUserAgreedToTermsAndConditions, bool bIsShowGSE, bool bHasUserConsented, bool bHasUserAgreedToNewTermsAndConditions, int? iCampaignId, string sCampaignURL)
        {
            m_UserId = userId;
            m_RecordId = recordId;
            m_SelfRecordId = selfRecordId;
            m_PersonalItemGUID = personalItemGUID;
            m_HasUserAgreedToTermsAndConditions = bHasUserAgreedToTermsAndConditions;
            m_IsShowGSE = bIsShowGSE;
            m_HasUserConsented = bHasUserConsented;
            m_HasUserAgreedToNewTermsAndConditions = bHasUserAgreedToNewTermsAndConditions;
            m_CampaignID = iCampaignId;
            CampaignUrl = sCampaignURL;
        }

        /// <summary>
        /// Returns true if the user matches the expected user ID
        /// </summary>
        /// <param name="userIdToCompare"></param>
        /// <returns></returns>
        public bool ValidateUserIdentity(Guid userIdToCompare)
        {
            if (this.UserId == userIdToCompare) return true;

            return false;
        }

        public bool ValidateRecord(Guid recordIdToCompare)
        {
            if (this.RecordId == recordIdToCompare) return true;

            return false;
        }

        /// <summary>
        /// True if this identity represents a valid logged in user with
        /// UserId and RecordId
        /// </summary>
        public bool IsHealthUserFullyLoggedIn
        {
            get
            {
                if (this.UserId == Guid.Empty) return false;

                if (this.RecordId == Guid.Empty) return false;

                //Everything seems good.
                return true;
            }
        }

    }

    /// <summary>
    /// Identifies a user's third party context
    /// </summary>
    [Serializable]
    public class ThirdPartyContext
    {
        

        private AHAHelpContent.ClientDevice.DeviceType  mDeviceType;
        private string                                  mDeviceId;

        public AHAHelpContent.ClientDevice.DeviceType Device
        {
            get { return mDeviceType; }
            set { mDeviceType = value; }
        }

        public string DeviceName
        {
            get { return AHAHelpContent.ClientDevice.GetDeviceName(mDeviceType); }
        }

        public string DeviceId
        {
            get { return mDeviceId; }
            set { mDeviceId = value; }
        }

        public ThirdPartyContext()
        {
            mDeviceType = AHAHelpContent.ClientDevice.DeviceType.DEVICE_TYPE_DEFAULT;
            mDeviceId = string.Empty;
        }
    }

    /// <summary>
    /// Summary description for SessionData
    /// </summary>
    public static class SessionInfo
    {
        /// <summary>
        /// used to gain access to the user identity object in session
        /// </summary>
        
        // Use AHACommon.SessionKeys constants

        public static void SetBPCenterItemToSession(object item)
        {
            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyBPCenterItem] = item;
        }

        public static void ResetBPCenterItem()
        {
            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyBPCenterItem] = null;
        }

        public static object GetBPCenterItemFromSession()
        {
            if (HttpContext.Current.Session != null && HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyBPCenterItem] != null)
            {
                return HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyBPCenterItem];
            }

            return null;
        }

        public static void SetMLCInfoToSession(MLCInfo item)
        {
            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyMLCInfo] = item;
        }

        public static void ResetMLCInfo()
        {
            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyMLCInfo] = null;
        }

        public static MLCInfo GetMLCInfoFromSession()
        {
            if (HttpContext.Current.Session != null && HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyMLCInfo] != null)
            {
                return HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyMLCInfo] as MLCInfo;
            }

            return null;
        }

        public static void SetThirdPartyContext(AHAHelpContent.ClientDevice.DeviceType _deviceType, string _deviceId)
        {
            ThirdPartyContext   tpc = new ThirdPartyContext() ;
            tpc.Device = _deviceType ;
            tpc.DeviceId = _deviceId ;

            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyThirdParty] = tpc;
        }

        public static void ClearThirdPartyContext()
        {
            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyThirdParty] = null;
        }

        public static ThirdPartyContext GetThirdPartyContext()
        {
            ThirdPartyContext tpc = null;

            if (HttpContext.Current.Session != null && HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyThirdParty] != null)
            {
                tpc = (ThirdPartyContext)HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyThirdParty];
            }

            return tpc;
        }



        /// <summary>
        /// Used to create a session object to store details regarding a user
        /// details incluse the selected record id, self record id & the user person info guid
        /// </summary>
        /// <param name="userID"></param>
        public static void SetUserIdentityContext(Guid gUserID, Guid gSelectedRecordID, Guid? gSelfRecordID, Guid gPersonalItemGUID, bool bHasUserAgreedToTermsOfUse, bool bShowGSE, bool bHasUserConsented, bool bHasUserAgreedToNewTermsAndConditions, int? iCampaignID, string sCampaignUrl)
        {
            UserIdentityContext objUserContext = new UserIdentityContext(gUserID,
                                                                         gSelectedRecordID,
                                                                         gSelfRecordID,
                                                                         gPersonalItemGUID,
                                                                         false,
                                                                         bHasUserAgreedToTermsOfUse,
                                                                         bShowGSE,
                                                                         bHasUserConsented,
                                                                         bHasUserAgreedToNewTermsAndConditions,
                                                                         iCampaignID,
                                                                         sCampaignUrl);

            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserIdentityContext] = objUserContext;
        }


        public static void ClearUserIdentityContext()
        {
            HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserIdentityContext] = null;
        }


        /// <summary>
        /// Get the user identity context object to get the details about the user
        /// </summary>
        /// <returns></returns>
        public static UserIdentityContext GetUserIdentityContext()
        {
            return (AHACommon.SessionKeys.DoesSessionKeyHaveValue(AHACommon.SessionKeys.sessionKeyUserIdentityContext)) ? 
                (UserIdentityContext)HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserIdentityContext] : null;
/**
            if (HttpContext.Current.Session != null && HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserIdentityContext] != null)
            {
                UserIdentityContext objUserContext = (UserIdentityContext)HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserIdentityContext];
                return objUserContext;
            }
            else
            {
                return null;
            }
 **/
        }

        /// <summary>
        /// Checks to see if a user is actually logged in
        /// </summary>
        public static bool IsUserLoggedIn
        {
            get
            {
                return (HttpContext.Current.Session != null && HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserIdentityContext] != null);
            }
        }


        /// <summary>
        /// Is called as part of log out.  This fluses all user state we have been holdign
        /// </summary>
        public static void ClearLoggedInUserState()
        {
            //Blow away everything about the user
            //CKR: Changed to Abandon by CKR. For some reason db
            //based sessions doesnt clear correctly by Clear() call
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session.Abandon();
            }
        }

        public static void SetPatientInvitationSession(PatientInvitation obj)
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserPatientInvitation] = obj;
        }

        public static PatientInvitation GetPatientInvitationSession()
        {
            if (System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserPatientInvitation] != null)
            {
                return System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserPatientInvitation] as PatientInvitation;
            }
            return null;
        }

        public static void ResetPatientInvitationSession()
        {
            System.Web.HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserPatientInvitation] = null;
        }
    }
}