namespace AHAAPI
{
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //
    using System;
    using System.Web.UI;
    using System.Configuration;
    using System.Text;
    /// <summary>
    /// A list of file paths that are used throughout the application
    /// </summary>
    public class GlobalPaths
    {
        public const string WebConfigSetting_DataCacheDiagnostics = "DataCacheDiagnostics";

        public static string GetInvitationLink(string strInvitationGUID, bool isPatient)
        {
            StringBuilder sbInvitation = new StringBuilder();
            sbInvitation.Append(GRCBase.UrlUtility.ServerRootUrl);
            if(!isPatient)
                sbInvitation.Append(GetURLWithoutApp(Provider.ProviderPageUrl.PUBLIC_INVITATION_ACCEPT));
            else
                sbInvitation.Append(GetURLWithoutApp(AHAAPI.WebAppNavigation.H360.PAGE_PUBLIC_INVITATION_ACCEPT));

            sbInvitation.Append("?invitationid=");
            //sbInvitation.Append(GRCBase.BlowFish.EncryptString(strInvitationGUID));
            sbInvitation.Append(strInvitationGUID);
            return sbInvitation.ToString();
        }

        private static string GetURLWithoutApp(string strUrl)
        {
            if (strUrl.StartsWith("~"))
                return strUrl.Substring(1).ToLower();

            return strUrl.ToLower();
        }
    }
}