﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;

namespace _3CiDataManagerSend
{
    public class SendAPI
    {
            // A consumer of this module should set ServiceName
        public string ServiceName
        {
            get;
            set;
        }

        public String URL   // Url of 3Ci web service
        {
            get;
            set;
        }

        public String IVRURL
        {
            get;
            set;
        }

        //
        // For testing purposes, we may want to have someone register a "progress listener"
        //

        public delegate void ProgressListener( string _msg );

        private ProgressListener    mProgressListener ;

        public ProgressListener Listener
        {
            set { mProgressListener = value; }
        }

        //
        //
        // For testing purposes, can specify specific day of week and time of day
        //

        public enum DayOfWeek
        {
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY,
            SUNDAY,
            ANY             // This is a wildcard for testing.
        } ;

        public const int AnyTime = -1;      // This is a wildcard for testing

        // Note: Time of day is 0 = Midnight, 12 is noon, 23 is 11pm

        //
        // Constructor
        //
        public SendAPI()
        {
            ServiceName = "Unknown" ;
            URL = "" ;
            IVRURL = "" ;
            
            mProgressListener = null ;
        }

        private void ListenerInfo(string _info)
        {
            if (mProgressListener != null)
                mProgressListener(_info);
        }

        private readonly String LogInsertSQL = "Insert into [dbo].[3CiPatientReminderLog] (ReminderScheduleID, PatientID, Message, Messagedate, Direction) Values ('{0}', '{1}','{2}','{3}','{4}')";

        private String HeaderValuesSQL = "select URL, username, password, shortcode, SubscriberURL from dbo.[3CiConfigs] ";
        private String IVRHeaderValuesSQL = "select URL, apikey, method from dbo.[IVRConfigs] ";  
        private String groupSQL = "select GroupID from [3CiPatientReminderCategory] where name != 'PIN'";
        private String pinSQL = "select TriggerID from [3CiPatientReminderCategory]  where name = 'PIN'  ";

        // PJB - this was public, but I don't think it needs to be or should be
        private String PatientRemindersConnectionName = "DBConnStr";

        private readonly String ivrInsertLogSQL = "INSERT INTO [Heart360].[dbo].[IVRPatientReminderLog] ([PatientID],[ActionDate],[ActionSource]) VALUES ('{0}', getdate(), 'IVRReminderService')";

        private readonly String sp_checkIVRReminders = "exec proc_checkIVRReminders '{0}'";

        private readonly String setCampaignSQL = "update tmpSetCampaign set patientid ='{0}' where FirstName='{1}' and LastName='{2}' and EmailAddress='{3}'  ";

        private readonly String attachCampaignSQL = "select a.*, '0000000000' as phonenumber, '0' as PatientID, '0' as PIN from HealthRecord a where OfflineHealthRecordGUID is not null and offlinepersonguid is not null ";

        private readonly String updateCampaignSQL = "update healthrecord set campaignid = a.CampaignID from campaigndetails a, tmpSetCampaign b where a.title = b.CampaignName and healthrecord.UserHealthRecordID=b.PatientID ";

        private readonly String hltSQL = "INSERT INTO [Heart360].[dbo].[HLTLog] ([PatientID],[Zipcode],[BPSource],[BPDate]) VALUES ('{0}', '{1}','{2}','{3}')";

        private readonly String createFileSQL = "select b.patientid, b.zipcode, c.url, a.readingsource, a.datemeasured from bloodpressure a, HLTLog b left outer join campaign c ON b.campaign = c.campaignid where a.userhealthrecordid = b.patientid   order by b.patientid, datemeasured";

        //private readonly String setCampaignSQL = "update tmpSetCampaign set patientid ='{0}' where FirstName='{1}' and LastName='{2}' and EmailAddress='{3}'  ";

        private readonly String pinValidatedSQL = "update [3CiPatientReminder] set PinValidated =1 where MobileNumber='{1}' ";

        public XmlDocument submitRequest(String xml)
        {
            // Create a request using a URL that can receive a post. 
            //WebRequest request = WebRequest.Create("https://platform.3cinteractive.com/api/workflow-initiator.php");
            WebRequest  request = null;
            Stream      dataStream = null;
            WebResponse response = null;
            string      responseFromServer = null;
            bool        wroteRequest = false ;
            if (!String.IsNullOrEmpty(URL))
            {
                request = WebRequest.Create(URL);
            }
            else
            {
                return null;
            }

            try
            {
                // Set the Method property of the request to POST.
                request.Method = "POST";
                // Create POST data and convert it to a byte array.
                string postData = xml;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.
                request.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                // Get the request stream.
                dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
                wroteRequest = true ;

                // Get the response.
                response = request.GetResponse();
                // Display the status.
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();

                if (dataStream != null)
                {
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();
                    // Display the content.
                    //Console.WriteLine(responseFromServer);
                    // Clean up the streams.
                    reader.Close();
                }
            }

            catch (Exception ex)
            {
                //// create database
                //SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                ////set the sql command
                //DbCommand dbCommand = dbCommand = dbSQL.GetSqlStringCommand(String.Format(ErrorLogInsertSQL, "EGalaxy", "Error while sending Request to 3Ci. The Error is: " + ex.Message));


                ////Execute the command
                //dbSQL.ExecuteNonQuery(dbCommand);

                if (request == null)
                    throw new Exception("Variable 'request' is null: " + ex.Message);
                else if (dataStream == null)
                    throw new Exception("Variable 'dataStream' is null (" + wroteRequest.ToString() + "): " + ex.Message);
                else if (response == null)
                    throw new Exception("Variable 'response' is null: " + ex.Message);
                else
                    throw new Exception("Must be variable 'reader' is null: " + ex.Message);


                //throw ex;
            }
            finally
            {
                if( dataStream != null )
                    dataStream.Close();
                if (request != null) request.GetRequestStream().Close();
                if (response != null) response.GetResponseStream().Close();
            }

            XmlDocument doc = new XmlDocument();
            doc.InnerXml = responseFromServer;

            if (hasError(doc))
            {
                throw new SystemException("Could not process the Request.");
            }

            return doc;
        }
        
        private bool hasError(XmlNode body)
        {
            if (getNodeValue(body, "Error") != null || getNodeValue(body, "ErrorCode") != null)
            {
                System.Diagnostics.EventLog.WriteEntry("AHA-3Ci", getNodeValue(body, "ErrorText"));

                //// create database
                //SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                ////set the sql command
                //DbCommand dbCommand = dbCommand = dbSQL.GetSqlStringCommand(String.Format(ErrorLogInsertSQL, "EGalaxy", "Error while sending Request to 3Ci. The Error is: "));


                ////Execute the command
                //dbSQL.ExecuteNonQuery(dbCommand);

                System.Diagnostics.EventLog.WriteEntry("AHA-3Ci", getNodeValue(body, "ErrorText"));
                return true;
            }

            return false;
        }

        private String getNodeValue(XmlNode parent, String name)
        {
            String value = null;

            if (parent.Name == name)
            {
                return parent.FirstChild.Value;
            }

            foreach (XmlNode child in parent.ChildNodes)
            {
                value = getNodeValue(child, name);

                if (value != null)
                {
                    return value;
                }
            }

            return null;
        }

        private string safeXml(string input)
        {
            return string.IsNullOrEmpty(input) ? input :
            input.Replace("&", "&amp;").
                    Replace(">", "&gt;").
                    Replace("<", "&lt;").
                    Replace("'", "&apos;");
        }

        private string generateheader(bool needShortCode = false, bool isSubURL = false)
        {
            StringBuilder xml = new StringBuilder();

            // create database
            SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

            //set the sql command
            DbCommand dbCommand = dbSQL.GetSqlStringCommand(HeaderValuesSQL);

            using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    xml.Append("username=" + dataReader["UserName"].ToString().Trim());
                    xml.Append("&password=" + dataReader["Password"].ToString().Trim());

                    if (needShortCode)
                    {
                        xml.Append("&short_code_id=" + dataReader["ShortCode"].ToString().Trim());
                    }
                    //Set the URL
                    if (isSubURL)
                    {
                        URL = dataReader["SubscriberURL"].ToString().Trim();
                    }
                    else
                    {
                        URL = dataReader["URL"].ToString().Trim();
                    }
                }
            }
            return xml.ToString();
        }

        private string generateIVRheader()
        {
            StringBuilder xml = new StringBuilder();

            // create database
            SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

            //set the sql command
            DbCommand dbCommand = dbSQL.GetSqlStringCommand(IVRHeaderValuesSQL);

            using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    xml.Append("apikey=" + dataReader["apikey"].ToString().Trim());
                    xml.Append("&method=" + dataReader["method"].ToString().Trim());

                    //Set the URL
                     URL = dataReader["URL"].ToString().Trim();
                }
            }
            return xml.ToString();
        }

        public String getAppSetting(String settingName)
        {
            String setting = ConfigurationManager.AppSettings[settingName];
            return setting;
        }

        // format strings so that they don't break sql calls
        public static String parseSQLStringParam(String value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                value = value.Replace("'", "''");
            }

            return value;
        }

        public void generateReminders()
        {
            generate3CiReminders( string.Empty );
            generateIVRReminders( string.Empty );
        }

        public void generateReminders(string mobileNumber)
        {
            generate3CiReminders(mobileNumber);
            generateIVRReminders(mobileNumber);
        }

            // This should be in a stored procedure!!!
            // if mobileNumber is null/empty, then we generate the query for everybody. Otherwise just the single mobile number
        private string BuildSmsRemindersQuery( string mobileNumber )
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                    
                sb.Append( "select Message, MobileNumber, Name, TriggerID, b.PatientID, a.ReminderScheduleID from dbo.[3CiPatientReminderSchedule] a, [3CiPatientReminder] b, [3CiPatientReminderCategory] c where a.ReminderID = b.ReminderID and a.ReminderCategoryID = c.ReminderCategoryID  and a.Status=1 " ) ;

                if ( !string.IsNullOrEmpty(mobileNumber))
                {
                    sb.Append(" and mobilenumber = '" + mobileNumber + "'");
                }

                //
                // We need to take into consideration the timezone of each person.
                // The server is running in Texas (Central Time Zone) so we may need to forward or back up one day for
                // people who are in other time zones.
                //
                sb.Append( " and case when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_EASTERN) + "'  and datepart(hh, getdate()) = 23 then " );
                sb.Append( " case  datename(weekday, dateadd(dd, 1, getdate())) when 'Monday' then Monday ");
                sb.Append( " when 'Tuesday' then Tuesday ");
                sb.Append( " when 'Wednesday' then Wednesday ");
                sb.Append( " when 'Thursday' then Thursday ");
                sb.Append( " when 'Friday' then Friday ");
                sb.Append( " when 'Saturday' then Saturday ");
                sb.Append( " when 'Sunday' then Sunday ");
                sb.Append( " end ");
                sb.Append( " when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_MOUNTAIN) + "'  and datepart(hh, getdate()) = 0 then   ");
                sb.Append( " case  datename(weekday, dateadd(dd, -1, getdate())) when 'Monday' then Monday ");
                sb.Append( " when 'Tuesday' then Tuesday ");
                sb.Append( " when 'Wednesday' then Wednesday ");
                sb.Append( " when 'Thursday' then Thursday ");
                sb.Append( " when 'Friday' then Friday ");
                sb.Append( " when 'Saturday' then Saturday ");
                sb.Append( " when 'Sunday' then Sunday ");
                sb.Append( " end ");
                sb.Append( " when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_PACIFIC) + "'  and datepart(hh, getdate()) < 2 then   ");
                sb.Append( " case  datename(weekday, dateadd(dd, -1, getdate())) when 'Monday' then Monday ");
                sb.Append( " when 'Tuesday' then Tuesday ");
                sb.Append( " when 'Wednesday' then Wednesday ");
                sb.Append( " when 'Thursday' then Thursday ");
                sb.Append( " when 'Friday' then Friday ");
                sb.Append( " when 'Saturday' then Saturday ");
                sb.Append( " when 'Sunday' then Sunday ");
                sb.Append( " end ");
                sb.Append(" when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_ALASKA) + "'  and datepart(hh, getdate()) < 3 then   ");
                sb.Append(" case  datename(weekday, dateadd(dd, -1, getdate())) when 'Monday' then Monday ");
                sb.Append(" when 'Tuesday' then Tuesday ");
                sb.Append(" when 'Wednesday' then Wednesday ");
                sb.Append(" when 'Thursday' then Thursday ");
                sb.Append(" when 'Friday' then Friday ");
                sb.Append(" when 'Saturday' then Saturday ");
                sb.Append(" when 'Sunday' then Sunday ");
                sb.Append(" end ");
                sb.Append(" when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_HAWAII) + "'  and datepart(hh, getdate()) < 4 then   ");
                sb.Append(" case  datename(weekday, dateadd(dd, -1, getdate())) when 'Monday' then Monday ");
                sb.Append(" when 'Tuesday' then Tuesday ");
                sb.Append(" when 'Wednesday' then Wednesday ");
                sb.Append(" when 'Thursday' then Thursday ");
                sb.Append(" when 'Friday' then Friday ");
                sb.Append(" when 'Saturday' then Saturday ");
                sb.Append(" when 'Sunday' then Sunday ");
                sb.Append(" end ");
                sb.Append( " else  ");
                sb.Append( " case  datename(weekday, getdate()) when 'Monday' then Monday ");
                sb.Append( " when 'Tuesday' then Tuesday ");
                sb.Append( " when 'Wednesday' then Wednesday ");
                sb.Append( " when 'Thursday' then Thursday ");
                sb.Append( " when 'Friday' then Friday ");
                sb.Append( " when 'Saturday' then Saturday ");
                sb.Append( " when 'Sunday' then Sunday ");
                sb.Append( " end ");
                sb.Append( " end  ");
                sb.Append( " =1 ");

                sb.Append(" and datepart(hh, dbo.get_UTC_datetime(dateadd(hh, cast(substring(time, 0, charindex(':', time)) as int), CAST( CONVERT( CHAR(8), GetDate(), 112) AS DATETIME)), TimeZone)) = '");
                sb.Append(DateTime.Now.ToUniversalTime().Hour.ToString());
                sb.Append("'");

                return sb.ToString();
            }
            catch { }
            return string.Empty;
        }

        private void DoMockDays(DayOfWeek _day, ref string refToday, ref string refTomorrow, ref string refYesterday)
        {
            switch (_day)
            {
                case DayOfWeek.MONDAY:
                    refToday = "Monday";
                    refTomorrow = "Tuesday";
                    refYesterday = "Sunday";
                    break;

                case DayOfWeek.TUESDAY:
                    refToday = "Tuesday";
                    refTomorrow = "Wednesday";
                    refYesterday = "Monday";
                    break;

                case DayOfWeek.WEDNESDAY:
                    refToday = "Wednesday";
                    refTomorrow = "Thursday";
                    refYesterday = "Tuesday";
                    break;

                case DayOfWeek.THURSDAY:
                    refToday = "Thursday";
                    refTomorrow = "Friday";
                    refYesterday = "Wednesday";
                    break;

                case DayOfWeek.FRIDAY:
                    refToday = "Friday";
                    refTomorrow = "Saturday";
                    refYesterday = "Thursday";
                    break;

                case DayOfWeek.SATURDAY:
                    refToday = "Saturday";
                    refTomorrow = "Sunday";
                    refYesterday = "Friday";
                    break;

                case DayOfWeek.SUNDAY:
                    refToday = "Sunday";
                    refTomorrow = "Monday";
                    refYesterday = "Saturday";
                    break;

                case DayOfWeek.ANY:
                default:
                    refToday = refTomorrow = refYesterday = "Any";
                    break;
            }

        }

        private string BuildSmsRemindersQueryTest(string mobileNumber, DayOfWeek _day, int _hourOfDay )
        {
            try
            {
                string mockHour = _hourOfDay.ToString();
                string mockToday = "";
                string mockTomorrow = "";
                string mockYesterday = "";

                DoMockDays(_day, ref mockToday, ref mockTomorrow, ref mockYesterday);

                StringBuilder sb = new StringBuilder();

                sb.Append("select Message, MobileNumber, Name, TriggerID, b.PatientID, a.ReminderScheduleID from dbo.[3CiPatientReminderSchedule] a, [3CiPatientReminder] b, [3CiPatientReminderCategory] c where a.ReminderID = b.ReminderID and a.ReminderCategoryID = c.ReminderCategoryID  and a.Status=1 ");

                if (!string.IsNullOrEmpty(mobileNumber))
                {
                    sb.Append(" and mobilenumber = '" + mobileNumber + "'");
                }


                if (_day != DayOfWeek.ANY)
                {
                    if (_hourOfDay != SendAPI.AnyTime)
                    {
                        sb.Append(" and case when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_EASTERN) + "' and " + mockHour + " = 23 then ");
                        sb.Append(" case  '" + mockTomorrow + "' when 'Monday' then Monday ");
                        sb.Append(" when 'Tuesday' then Tuesday ");
                        sb.Append(" when 'Wednesday' then Wednesday ");
                        sb.Append(" when 'Thursday' then Thursday ");
                        sb.Append(" when 'Friday' then Friday ");
                        sb.Append(" when 'Saturday' then Saturday ");
                        sb.Append(" when 'Sunday' then Sunday ");
                        sb.Append(" end ");
                        sb.Append(" when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_MOUNTAIN) + "'  and " + mockHour + " = 0 then ");
                        sb.Append(" case  '" + mockYesterday + "' when 'Monday' then Monday ");
                        sb.Append(" when 'Tuesday' then Tuesday ");
                        sb.Append(" when 'Wednesday' then Wednesday ");
                        sb.Append(" when 'Thursday' then Thursday ");
                        sb.Append(" when 'Friday' then Friday ");
                        sb.Append(" when 'Saturday' then Saturday ");
                        sb.Append(" when 'Sunday' then Sunday ");
                        sb.Append(" end ");
                        sb.Append(" when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_PACIFIC) + "'  and " + mockHour + " < 2 then ");
                        sb.Append(" case '" + mockYesterday + "' when 'Monday' then Monday ");
                        sb.Append(" when 'Tuesday' then Tuesday ");
                        sb.Append(" when 'Wednesday' then Wednesday ");
                        sb.Append(" when 'Thursday' then Thursday ");
                        sb.Append(" when 'Friday' then Friday ");
                        sb.Append(" when 'Saturday' then Saturday ");
                        sb.Append(" when 'Sunday' then Sunday ");
                        sb.Append(" end ");
                        sb.Append(" when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_ALASKA) + "' and " + mockHour + " < 3 then ");
                        sb.Append(" case '" + mockYesterday + "' when 'Monday' then Monday ");
                        sb.Append(" when 'Tuesday' then Tuesday ");
                        sb.Append(" when 'Wednesday' then Wednesday ");
                        sb.Append(" when 'Thursday' then Thursday ");
                        sb.Append(" when 'Friday' then Friday ");
                        sb.Append(" when 'Saturday' then Saturday ");
                        sb.Append(" when 'Sunday' then Sunday ");
                        sb.Append(" end ");
                        sb.Append(" when timezone = '" + AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_HAWAII) + "' and " + mockHour + " < 4 then ");
                        sb.Append(" case '" + mockYesterday + "' when 'Monday' then Monday ");
                        sb.Append(" when 'Tuesday' then Tuesday ");
                        sb.Append(" when 'Wednesday' then Wednesday ");
                        sb.Append(" when 'Thursday' then Thursday ");
                        sb.Append(" when 'Friday' then Friday ");
                        sb.Append(" when 'Saturday' then Saturday ");
                        sb.Append(" when 'Sunday' then Sunday ");
                        sb.Append(" end ");
                        sb.Append(" else  ");
                        sb.Append(" case  '" + mockToday + "' when 'Monday' then Monday ");
                        sb.Append(" when 'Tuesday' then Tuesday ");
                        sb.Append(" when 'Wednesday' then Wednesday ");
                        sb.Append(" when 'Thursday' then Thursday ");
                        sb.Append(" when 'Friday' then Friday ");
                        sb.Append(" when 'Saturday' then Saturday ");
                        sb.Append(" when 'Sunday' then Sunday ");
                        sb.Append(" end ");
                        sb.Append(" end  ");
                        sb.Append(" =1 ");
                    }
                    else
                    {  
                        sb.Append(" and case  '" + mockToday + "' when 'Monday' then Monday ");
                        sb.Append(" when 'Tuesday' then Tuesday ");
                        sb.Append(" when 'Wednesday' then Wednesday ");
                        sb.Append(" when 'Thursday' then Thursday ");
                        sb.Append(" when 'Friday' then Friday ");
                        sb.Append(" when 'Saturday' then Saturday ");
                        sb.Append(" when 'Sunday' then Sunday ");
                        sb.Append(" end ");
                        sb.Append(" =1 ");
                    }
                }

                if (_hourOfDay != SendAPI.AnyTime)
                {
                        // This does logic for time of day. It determines the offset from UTC Greenwhich taking into consideration daylight savings time.
                    sb.Append(" and datepart(hh, dbo.get_UTC_datetime(dateadd(hh, cast(substring(time, 0, charindex(':', time)) as int), CAST( CONVERT( CHAR(8), GetDate(), 112) AS DATETIME)), TimeZone)) = '");
                    sb.Append( mockHour );  // DateTime.Now.ToUniversalTime().Hour.ToString());
                    sb.Append("'");
                }

                return sb.ToString();
            }
            catch { }
            return string.Empty;
        }

        private void LogError( string errMessage )
        {
            try
            {
                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                string escapedMsg = FormatStringForSqlParameter(errMessage);

                string qry = "Insert into [dbo].[ServicesErrorLog] (ServiceName, ErrorContent) Values ('{0}', '{1}')";
                DbCommand dbCmdError = dbSQL.GetSqlStringCommand(String.Format(qry, ServiceName, escapedMsg));

                dbSQL.ExecuteNonQuery(dbCmdError);
            }
            catch (Exception ex)
            {
                // Wow, most likely an issue with errMessage parameter. We will try with generic message
                try
                {
                    SqlDatabase dbSQL2 = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                    string escapedMsg2 = FormatStringForSqlParameter( "LogError Exception: " + ex.Message);

                    string qry = "Insert into [dbo].[ServicesErrorLog] (ServiceName, ErrorContent) Values ('{0}', '{1}')";
                    DbCommand dbCmdError = dbSQL2.GetSqlStringCommand(String.Format(qry, ServiceName, escapedMsg2));

                    dbSQL2.ExecuteNonQuery(dbCmdError);
                }
                catch { }
            }
        }

            // Need to escape single quotes, not sure of any others. Ampersand?
        private string FormatStringForSqlParameter(string _s)
        {
            return _s.Replace("'", "''");
        }

            // NOPE: Need to escape the following characters: > < " ' &
            // 3Ci is not xml, it is query parameters, so msg cannot have ampersands
        private string FormatStringFor3Ci(string _s)
        {
            return _s.Replace('&', ' ');
//            string escAmp = _s.Replace("&", "&amp;");
//            string escApostrophe = escAmp.Replace("'", "&apos;");
//            string escDq = escApostrophe.Replace("\"", "&quot;");
//            string escLt = escDq.Replace("<", "&lt;");
//            string escGt = escLt.Replace(">", "&gt;");
//            return escGt;
        }

            //
            // if _mobileNumber is not empty/null, then reminders will be generated only for the given mobile number
            //
        public void generate3CiReminders( string _mobileNumber )
        {
            SqlDatabase dbSQL = null;

            ListenerInfo("Generating reminders for " + ((string.IsNullOrEmpty(_mobileNumber)) ? "everyone" : _mobileNumber) + ".\n" );

            try
            {
                    // generateheader will do a sql query, set the local URL value, and some other things.
                string headerXml = generateheader();

                ListenerInfo("Generated xml header for 3Ci.\n" );

                dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                ListenerInfo("Got SqlDatabase.\n");

                //set the sql command
                string sqlquery = BuildSmsRemindersQuery( _mobileNumber );
                if (string.IsNullOrEmpty( sqlquery) )
                    throw new Exception("SMS Reminder Query could not be built");

                DbCommand dbCommand = dbSQL.GetSqlStringCommand(sqlquery);

                ListenerInfo("Got dbCommand for SqlDatabase query.\n");

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    ListenerInfo("Executed dbCommand.\n" );

                    int counter = 0;
                    while (dataReader.Read())
                    {
                        string PatientID = "";
                        string ReminderScheduleID = "";

                        ListenerInfo("Processing a record.....\n");

                        try
                        {
                            StringBuilder xml = new StringBuilder();

                            // build the xml request

                            xml.Append(headerXml);

                            string Message = dataReader["Message"].ToString().Trim();                       
                            string Category = dataReader["Name"].ToString().Trim();

                            ReminderScheduleID = dataReader["ReminderScheduleID"].ToString().Trim();
                            PatientID = dataReader["PatientID"].ToString().Trim();

                            xml.Append("&phone_number=" + dataReader["MobileNumber"].ToString().Trim());
                            xml.Append("&trigger_id=" + dataReader["TriggerID"].ToString().Trim());
                            xml.Append("&attributes[msg]=" + FormatStringFor3Ci(Message) );
                            xml.Append("&attributes[category]=" + Category );
                            xml.Append("&attributes[patientid]=" + PatientID );
                            xml.Append("&attributes[reminderscheduleid]=" + ReminderScheduleID);
                            xml.Append("&attributes[phonenumber]=" + dataReader["MobileNumber"].ToString().Trim());

                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Weight"))
                            //{
                            //    xml.Append("&trigger_id=122041");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Blood Pressure"))
                            //{
                            //    xml.Append("&trigger_id=122062");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Cholesterol"))
                            //{
                            //    xml.Append("&trigger_id=122066");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Blood Glucose"))
                            //{
                            //    xml.Append("&trigger_id=122065");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Medication"))
                            //{
                            //    xml.Append("&trigger_id=122064");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Physical Activity"))
                            //{
                            //    xml.Append("&trigger_id=122063");
                            //}


                            // Send this reminder to 3Ci. this throws an exception on error
                            ListenerInfo("...submitting to 3Ci...");
                            XmlDocument doc = submitRequest(xml.ToString());

                            ListenerInfo("...logging submission to log file...");
                            //set the sql command
                            string escapedMsg = FormatStringForSqlParameter(Message);
                            DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, ReminderScheduleID, PatientID, escapedMsg, DateTime.Now, 0));

                            //Execute the command
                            dbSQL.ExecuteNonQuery(dbCommand1);

                            ListenerInfo("...done with record!\n");
                            counter++;
                        }
                        catch (Exception ex)
                        {
                            // Log this error
                            string errMessage = "PatientID = " + PatientID + ", ReminderScheduleID = " + ReminderScheduleID + ", Error = " + ex.Message;
                            ListenerInfo( "ERROR: " + errMessage );
                            LogError( errMessage );
                        }
                    }

                    ListenerInfo("***Done processing " + counter.ToString() + " records ***\n");
                }
            }
            catch (Exception ex)
            {
                ListenerInfo("ERROR: " + ex.Message);
                // Log this error
                LogError( ex.Message );
            }
        }

        public void generate3CiRemindersTest(string _mobileNumber, DayOfWeek _day, int _hourOfDay, bool _sendReminders )
        {
            SqlDatabase dbSQL = null;

            if ( (_hourOfDay < 0 || _hourOfDay > 23) && _hourOfDay != SendAPI.AnyTime )
            {
                ListenerInfo("Error: invalid hour of day parameter");
                return;
            }

            ListenerInfo("Generating reminders for " + ((string.IsNullOrEmpty(_mobileNumber)) ? "everyone" : _mobileNumber) + " on " + _day.ToString() + " at " + _hourOfDay.ToString() + "hr \n");

            try
            {
                // generateheader will do a sql query, set the local URL value, and some other things.
                // Note: this is NOT xml format, this is ampersand delimited values like url parameters.
                string headerXml = generateheader();

                ListenerInfo("Generated xml header for 3Ci.\n");

                dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                ListenerInfo("Got SqlDatabase.\n");

                //set the sql command
                string sqlquery = BuildSmsRemindersQueryTest(_mobileNumber, _day, _hourOfDay );
                if (string.IsNullOrEmpty(sqlquery))
                    throw new Exception("SMS Reminder Query could not be built");

                bool sendTo3Ci = _sendReminders;    // (getAppSetting("SendAPITodayTestMode").Equals("0")) ? false : true;

                ListenerInfo("Will send to Service: " + ((sendTo3Ci) ? "true" : "false") + "\n" );

                DbCommand dbCommand = dbSQL.GetSqlStringCommand(sqlquery);

                ListenerInfo("Got dbCommand for SqlDatabase query.\n");

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    ListenerInfo("Executed dbCommand.\n");

                    int counter = 0;
                    while (dataReader.Read())
                    {
                        string PatientID = "";
                        string ReminderScheduleID = "";

                        ListenerInfo("Processing a record.....\n");

                        try
                        {
                            StringBuilder xml = new StringBuilder();

                            // build the xml request

                            xml.Append(headerXml);

                            string Message = dataReader["Message"].ToString().Trim();
                            string Category = dataReader["Name"].ToString().Trim();

                            ReminderScheduleID = dataReader["ReminderScheduleID"].ToString().Trim();
                            PatientID = dataReader["PatientID"].ToString().Trim();

                            xml.Append("&phone_number=" + dataReader["MobileNumber"].ToString().Trim());
                            xml.Append("&trigger_id=" + dataReader["TriggerID"].ToString().Trim());
                            xml.Append("&attributes[msg]=" + FormatStringFor3Ci( Message ) ) ;
                            xml.Append("&attributes[category]=" + Category );
                            xml.Append("&attributes[patientid]=" + PatientID );
                            xml.Append("&attributes[reminderscheduleid]=" + ReminderScheduleID );
                            xml.Append("&attributes[phonenumber]=" + dataReader["MobileNumber"].ToString().Trim());

                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Weight"))
                            //{
                            //    xml.Append("&trigger_id=122041");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Blood Pressure"))
                            //{
                            //    xml.Append("&trigger_id=122062");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Cholesterol"))
                            //{
                            //    xml.Append("&trigger_id=122066");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Blood Glucose"))
                            //{
                            //    xml.Append("&trigger_id=122065");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Medication"))
                            //{
                            //    xml.Append("&trigger_id=122064");
                            //}
                            //if (StringComparer.CurrentCultureIgnoreCase.Equals(Category, "Physical Activity"))
                            //{
                            //    xml.Append("&trigger_id=122063");
                            //}


                            if (sendTo3Ci)
                            {
                                // Send this reminder to 3Ci. this throws an exception on error
                                ListenerInfo("...submitting to Service...");
                                XmlDocument doc = submitRequest(xml.ToString());
                            }
                            else
                            {
                                Message = "TEST: Let's do it!" ;
                            }

                            ListenerInfo("...logging submission to log file...");
                            //set the sql command
                            string strEscapedMessage = FormatStringForSqlParameter(Message);
                            DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, ReminderScheduleID, PatientID, strEscapedMessage, DateTime.Now, 0));

                            //Execute the command
                            dbSQL.ExecuteNonQuery(dbCommand1);
                            
                            ListenerInfo("...done with record!\n");
                            counter++;
                        }
                        catch (Exception ex)
                        {
                            // Log this error
                            string errMessage = "PatientID = " + PatientID + ", ReminderScheduleID = " + ReminderScheduleID + ", Error = " + ex.Message;
                            ListenerInfo("ERROR: " + errMessage);
                            LogError( errMessage );
                        }
                    }

                    ListenerInfo("***Done processing " + counter.ToString() + " records ***\n");
                }
            }
            catch (Exception ex)
            {
                ListenerInfo("ERROR: " + ex.Message);
                // Log this error
                LogError(ex.Message);

            }
        }

        private string BuildIVRRemindersQuery( string _phone)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                sb.Append( "Select a.*, PhoneNumber, PatientID, PIN from HealthRecord a, dbo.[IVRPatientReminder] b where Enabled=1 and OfflineHealthRecordGUID is not null and offlinepersonguid is not null and a.UserHealthRecordID = b.PatientID and not exists (select 1 from dbo.IVRPatientReminderLog c where b.patientID = c.patientID and actiondate >= dateadd(dd, -14, getdate()))" );

                if( !string.IsNullOrEmpty( _phone ) )
                    sb.Append(" and PhoneNumber = '" + _phone + "'" );

                sb.Append( " and case when timezone = 'US/Eastern'  and datepart(hh, getdate()) = 23 then ");
                sb.Append( " case  datename(weekday, dateadd(dd, 1, getdate())) when 'Monday' then 'Monday' ");
                sb.Append( " when 'Tuesday' then 'Tuesday' ");
                sb.Append( " when 'Wednesday' then 'Wednesday' ");
                sb.Append( " when 'Thursday' then 'Thursday' ");
                sb.Append( " when 'Friday' then 'Friday' ");
                sb.Append( " when 'Saturday' then 'Saturday' ");
                sb.Append( " when 'Sunday' then 'Sunday' ");
                sb.Append( " end ");
                sb.Append( " when timezone = 'US/Pacific'  and datepart(hh, getdate()) = 0 then   ");
                sb.Append( " case  datename(weekday, dateadd(dd, -1, getdate())) when 'Monday' then 'Monday' ");
                sb.Append( " when 'Tuesday' then 'Tuesday' ");
                sb.Append( " when 'Wednesday' then 'Wednesday' ");
                sb.Append( " when 'Thursday' then 'Thursday' ");
                sb.Append( " when 'Friday' then 'Friday' ");
                sb.Append( " when 'Saturday' then 'Saturday' ");
                sb.Append( " when 'Sunday' then 'Sunday' ");
                sb.Append( " end ");
                sb.Append( " when timezone = 'US/Pacific'  and datepart(hh, getdate()) = 1 then   ");
                sb.Append( " case  datename(weekday, dateadd(dd, -1, getdate())) when 'Monday' then 'Monday' ");
                sb.Append( " when 'Tuesday' then 'Tuesday' ");
                sb.Append( " when 'Wednesday' then 'Wednesday' ");
                sb.Append( " when 'Thursday' then 'Thursday' ");
                sb.Append( " when 'Friday' then 'Friday' ");
                sb.Append( " when 'Saturday' then 'Saturday' ");
                sb.Append( " when 'Sunday' then 'Sunday' ");
                sb.Append( " end ");
                sb.Append( " else  ");
                sb.Append( " case  datename(weekday, getdate()) when 'Monday' then 'Monday' ");
                sb.Append( " when 'Tuesday' then 'Tuesday' ");
                sb.Append( " when 'Wednesday' then 'Wednesday' ");
                sb.Append( " when 'Thursday' then 'Thursday' ");
                sb.Append( " when 'Friday' then 'Friday' ");
                sb.Append( " when 'Saturday' then 'Saturday' ");
                sb.Append( " when 'Sunday' then 'Sunday' ");
                sb.Append( " end ");
                sb.Append( " end  ");
                sb.Append( " = [day] ");

                sb.Append( " and datepart(hh, dbo.get_UTC_datetime(dateadd(hh, cast(substring(time, 0, charindex(':', time)) as int), CAST( CONVERT( CHAR(8), GetDate(), 112) AS DATETIME)), TimeZone)) = '");
                sb.Append(DateTime.Now.ToUniversalTime().Hour.ToString());
                sb.Append("'");

                return sb.ToString();
            }
            catch { }

            return string.Empty;
        }

        public void generateIVRReminders( string _phone)
        {    
            List<IVRPatient> objPatientList = new List<IVRPatient>();
            SqlDatabase dbSQL = null ;
            
            try
            {
                string headerXml = generateIVRheader();
                dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));


                //set the sql command
                string qry = BuildIVRRemindersQuery( _phone );
                if (string.IsNullOrEmpty(qry))
                    throw new Exception("IVR Reminder Query could not be built." );

                DbCommand dbCommand = dbSQL.GetSqlStringCommand( qry );

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        string      patientID = "";
                        IVRPatient objPatient = null;

                        try
                        {
                            objPatient = new IVRPatient();

                            objPatient.OnDataBindIVR(dataReader);
                            objPatientList.Add(objPatient);


                            //to make sure that we don't miss any data here
                            DateTime dtSyncEndDate = DateTime.Now.AddDays(1);

                            //download all data relevant to the patient
                            bool isDataExist = HVHelper.HVManagerPatientBase(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value).DownloadDataBetweenDatesforIVR(new List<string> 
                                                                                { 
                                                                                    typeof(HVManager.BloodGlucoseDataManager).ToString(),                     
                                                                                    typeof(HVManager.BloodPressureDataManager).ToString(),                    
                                                                                    typeof(HVManager.CholesterolDataManager).ToString(), 
                                                                                    typeof(HVManager.ExerciseDataManager).ToString(), 
                                                                                    typeof(HVManager.MedicationDataManager).ToString(),    
                                                                                    typeof(HVManager.WeightDataManager).ToString()
                    
                                                                                }, DateTime.Today.AddDays(-14), DateTime.Today.AddDays(1), false);

                            StringBuilder xml = new StringBuilder();

                            // build the xml request

                            xml.Append(headerXml);

                            patientID = dataReader["PatientID"].ToString().Trim();
                            xml.Append("&phone=" + dataReader["PhoneNumber"].ToString().Trim().Substring(1, 10));
                            xml.Append("&patientid=" + dataReader["PatientID"].ToString().Trim());
                            xml.Append("&password=" + dataReader["PIN"].ToString().Trim());

                            // If the patient has not added any readings for the last 14 days, then they need to be reminded!
                            if (!isDataExist)
                            {
                                XmlDocument doc = submitRequest(xml.ToString());

                                //set the sql command
                                DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(ivrInsertLogSQL, patientID));

                                //Execute the command
                                dbSQL.ExecuteNonQuery(dbCommand1);
                            }
                        }
                        catch (Exception ex)
                        {
                            string errMessage = "PatientID = " + patientID + ", Error = " + ex.Message;
                            LogError(errMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
            }
        }

        public void attachCampaigns()
        {
            List<IVRPatient> objPatientList = new List<IVRPatient>();

             try
            {
                 SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //set the sql command
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(attachCampaignSQL);

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    int i = 0;
                    while (dataReader.Read())
                    {
                        string firstName = "";
                        string lastName = "";
                        string emailAddress = "";
                        string zipCode = "";

                        if (i % 100 == 0)
                        {
                            System.Diagnostics.EventLog.WriteEntry("Heart360", "Processing " + i.ToString() + " out of " + dataReader.RecordsAffected.ToString());
                        }
                        i = i + 1;
                        IVRPatient objPatient = new IVRPatient();
                        objPatient.OnDataBindIVR(dataReader);
                        objPatientList.Add(objPatient);

                        //to make sure that we don't miss any data here
                        DateTime dtSyncEndDate = DateTime.Now.AddDays(1);

                        //doanload all data relevant to the patient
                        try
                        {
                            //List<HVManager.BloodPressureDataManager.BPItem> bpList = new List<HVManager.BloodPressureDataManager.BPItem>();
                            string retVal = HVHelper.HVManagerPatientBase(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value).DownloadDataBetweenDatesforCampaign(objPatient.UserHealthRecordID, new List<string> 
                        { 
                            typeof(HVManager.PersonalDataManager).ToString(), 
                            typeof(HVManager.PersonalContactDataManager).ToString()
                        }, DateTime.Today.AddDays(-4), DateTime.Today.AddDays(1), false);


                            string[] data = retVal.Split(new Char[] { '!' });
                            if (data != null)
                            {
                                firstName = data[0];
                                lastName = data[1];
                                emailAddress = data[2];
                                zipCode = data[3];
                            }

                            //set the sql command
                            DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(setCampaignSQL, objPatient.UserHealthRecordID, firstName, lastName, emailAddress, zipCode));

                            //Execute the command
                            dbSQL.ExecuteNonQuery(dbCommand1);

                            //foreach (HVManager.BloodPressureDataManager.BPItem bp in bpList)
                            //{
                            //    //set the sql command
                            //    DbCommand dbCommand3 = dbSQL.GetSqlStringCommand(String.Format(hltSQL, objPatient.UserHealthRecordID, zipCode, "", bp.When.Value));

                            //    //Execute the command
                            //    dbSQL.ExecuteNonQuery(dbCommand3);
                            //}
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.EventLog.WriteEntry("Attach Campaigns", "Error while processing " + i.ToString() + ". Message is: " + ex.Message);
                        }

                    }
                    //set the sql command
                    DbCommand dbCommand2 = dbSQL.GetSqlStringCommand(updateCampaignSQL);

                    //Execute the command
                    dbSQL.ExecuteNonQuery(dbCommand2);

                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("Attach Campaigns", "Error while processing. Message is: " + ex.Message);

            }

        }

        private int checkIVRReminders(string patientID)
        {
            SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

            int retVal = 0; 
            //set the sql command
            try
            {
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(String.Format(sp_checkIVRReminders, patientID));

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        retVal = Int32.Parse(dataReader["checkIVR"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return retVal;
        }

        public void sendPIN(string mobileNumber, string PIN, string patientID)
        {
            try
            {
                string headerXml = generateheader();

                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //set the sql command
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(pinSQL);

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        
                        StringBuilder xml = new StringBuilder();

                        // build the xml request

                        xml.Append(headerXml);

                        xml.Append("&phone_number=" + mobileNumber);
                        xml.Append("&trigger_id=" + dataReader["TriggerID"].ToString().Trim());
                        xml.Append("&attributes[Message]=Heart360 Text Reminders: To begin receiving reminders at the day/times you choose, please enter PIN: " + PIN + " online. Msg/data rates may apply. Reply HELP for help.");

                        XmlDocument doc = submitRequest(xml.ToString());

                        //Insert into Log Table
                        //set the sql command
                        DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, 0, patientID, "Heart360 Text Reminders: To begin receiving reminders at the day/times you choose, please enter PIN: " + PIN + " online. Msg/data rates may apply. Reply HELP for help.", DateTime.Now, 0));
                        //Execute the command
                        dbSQL.ExecuteNonQuery(dbCommand1);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void pinConfirm(string mobileNumber, string patientID)
        {
            try
            {
                string headerXml = generateheader();

                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //set the sql command
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(pinSQL);

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        StringBuilder xml = new StringBuilder();

                        // build the xml request

                        xml.Append(headerXml);

                        xml.Append("&phone_number=" + mobileNumber);
                        xml.Append("&trigger_id=" + dataReader["TriggerID"].ToString().Trim());
                        xml.Append("&attributes[Message]=Heart360: Thanks! Now let's get started by setting reminders on your Dashboard! Msg/data rates may apply. Reply HELP for help. Reply STOP to cancel");

                        XmlDocument doc = submitRequest(xml.ToString());

                        //Insert into Log Table
                        //set the sql command
                        DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, 0, patientID, "Heart360: Thanks! Now let's get started by setting reminders on your Dashboard! Msg/data rates may apply. Reply HELP for help. Reply STOP to cancel", DateTime.Now, 0));
                        //Execute the command
                        dbSQL.ExecuteNonQuery(dbCommand1);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void setPinValidated(string mobileNumber, string patientID)
        {
            try
            {
                string headerXml = generateheader();

                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //set the sql command
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(String.Format(pinValidatedSQL,patientID,mobileNumber));
                dbSQL.ExecuteNonQuery(dbCommand);

                //Insert into Log Table
                //set the sql command
                DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, 0, patientID, "Heart360: PIN has been validated", DateTime.Now, 0));
                //Execute the command
                dbSQL.ExecuteNonQuery(dbCommand1);

            }
            catch (Exception ex)
            {

            }
        }

        public void addPersonTo3CiGroup(string mobileNumber, string catGroupID, string patientID)
        {
            string headerXml = generateheader(true, true);
            StringBuilder xml = new StringBuilder();

            // build the xml request
            xml.Append(headerXml);
            xml.Append("&phone_number=" + mobileNumber + "&action=add&group_id=" + catGroupID);
            XmlDocument doc = submitRequest(xml.ToString());

            SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

            //set the sql command
            DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, 0, patientID, patientID + " has been added to GroupID: " + catGroupID, DateTime.Now, 0));

            //Execute the command
            dbSQL.ExecuteNonQuery(dbCommand1);
        }

        public void removePersonFrom3CiGroup(string mobileNumber, string catGroupID, string patientID)
        {
            string headerXml = generateheader(true, true);
            StringBuilder xml = new StringBuilder();

            // build the xml request
            xml.Append(headerXml);
            xml.Append("&phone_number=" + mobileNumber + "&action=remove&group_id=" + catGroupID);
            XmlDocument doc = submitRequest(xml.ToString());

            SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

            //set the sql command
            DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, 0, patientID, patientID + " has been removed from GroupID: " + catGroupID, DateTime.Now, 0));

            //Execute the command
            dbSQL.ExecuteNonQuery(dbCommand1);
        }

        public void removePersonFromAll3CiGroups(string mobileNumber, string patientID)
        {
            string headerXml = generateheader(true, true);

            SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

            //set the sql command
            DbCommand dbCommand = dbSQL.GetSqlStringCommand(groupSQL);

            using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    StringBuilder xml = new StringBuilder();

                    // build the xml request

                    xml.Append(headerXml);

                    string catGroupID = dataReader["GroupID"].ToString().Trim();
                    xml.Append("&phone_number=" + mobileNumber + "&action=remove&group_id=" + catGroupID);
                    XmlDocument doc = submitRequest(xml.ToString());

                    //set the sql command
                    DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, 0, patientID, patientID + " has been removed from GroupID: " + catGroupID, DateTime.Now, 0));

                    //Execute the command
                    dbSQL.ExecuteNonQuery(dbCommand1);
                }
            }
        }
    }
}

/**
 * 
 * This is the query that gets built from BuildSmsRemindersQuery()
 * 
DECLARE @UniverTimeHour INT

SET @UniverTimeHour = datepart(hh,getutcdate())

select Message, MobileNumber, Name, TriggerID, b.PatientID, a.ReminderScheduleID 
	from Heart360.dbo.[3CiPatientReminderSchedule] a, Heart360.dbo.[3CiPatientReminder] b, Heart360.dbo.[3CiPatientReminderCategory] c 
	where a.ReminderID = b.ReminderID and a.ReminderCategoryID = c.ReminderCategoryID  and 
	      a.Status=1 and case 
				when timezone = 'US/Eastern'  and datepart(hh, getdate()) = 23 then
					case  datename(weekday, dateadd(dd, 1, getdate())) 
						when 'Monday' then Monday 
						when 'Tuesday' then Tuesday
						when 'Wednesday' then Wednesday
						when 'Thursday' then Thursday
						when 'Friday' then Friday
						when 'Saturday' then Saturday 
						when 'Sunday' then Sunday
					end
				when timezone = 'US/Pacific'  and datepart(hh, getdate()) = 0 then
					case  datename(weekday, dateadd(dd, -1, getdate())) 
						when 'Monday' then Monday
						when 'Tuesday' then Tuesday
						when 'Wednesday' then Wednesday
						when 'Thursday' then Thursday
						when 'Friday' then Friday
						when 'Saturday' then Saturday
						when 'Sunday' then Sunday
					end
				when timezone = 'US/Pacific'  and datepart(hh, getdate()) = 1 then
					case  datename(weekday, dateadd(dd, -1, getdate())) 
						when 'Monday' then Monday
						when 'Tuesday' then Tuesday
						when 'Wednesday' then Wednesday
						when 'Thursday' then Thursday
						when 'Friday' then Friday
						when 'Saturday' then Saturday
						when 'Sunday' then Sunday
					end
				else
					case  datename(weekday, getdate())
						when 'Monday' then Monday
						when 'Tuesday' then Tuesday
						when 'Wednesday' then Wednesday
						when 'Thursday' then Thursday
						when 'Friday' then Friday
						when 'Saturday' then Saturday
						when 'Sunday' then Sunday
					end
				end
				= 1 and
				datepart(hh, Heart360.dbo.get_UTC_datetime(dateadd(hh, cast(substring(time, 0, charindex(':', time)) as int), CAST( CONVERT( CHAR(8), GetDate(), 112) AS DATETIME)), TimeZone)) = @UniverTimeHour


 **/
