﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AHACommon
{
    public class CommonUtility
    {
        public static string GetDateTimeForDB(DateTime dt)
        {
            return String.Format("{0:yyyy/M/d HH:mm:ss}", dt);
        }
    }
}
