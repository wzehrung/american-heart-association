﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace AHACommon
{
    public class Locale
    {
        public const string English = "en-US";
        public const string Spanish = "es-MX";
    }

    public static class AHAGender
    {
        public const string Male = "Male";
        public const string Female = "Female";
    }

    public enum DataDateReturnType
    {
        [Description("Last Week")]
        LastWeek,
        [Description("Last Month")]
        LastMonth,
        [Description("Last 60 days")]
        Last60Days,
        [Description("Last 3 Months")]
        Last3Months,
        [Description("Last 6 Months")]
        Last6Months,
        [Description("Last 9 Months")]
        Last9Months,
        [Description("Last Year")]
        LastYear,
        [Description("Last 2 Years")]
        Last2Years,
        [Description("All Data")]
        AllData
    }

    public class DataDateFormatter
    {
        public static string GetDateLabelForDateRange(AHACommon.DataDateReturnType dtType)
        {
            if (dtType == AHACommon.DataDateReturnType.LastYear || dtType == AHACommon.DataDateReturnType.Last2Years)
                return "MM/yy";
            else
                return AHACommon.DateTimeHelper.GetShortDateFormatForWeb();
        }
    }

    public enum NotificationType
    {
        ProviderSummary,
        ProviderImmediate,
        MessageNew,
        MessageSummary
    }

    public enum PatientUserControl
    {
        BPForm,
        BPGoalForm,
        WeightForm,
        WeightGoalForm,
        BGForm,
        CholesterolForm,
        CholesterolGoalForm,
        ExerciseForm,
        ExerciseGoalForm,
        MedicationForm
    }

    public class PatientTrackers
    {
        public enum TrackerType
        {
            [Description("Blood Pressure")]
            BloodPressure,
            [Description("Cholesterol")]
            Cholesterol,
            [Description("Blood Glucose")]
            BloodGlucose,
            [Description("Medication")]
            Medication,
            [Description("Weight")]
            Weight,
            [Description("Physical Activity")]
            PhysicalActivity,
            [Description("HbA1c")]
            HbA1c
        }

        public const string BloodPressure = "bp";
        public const string Weight = "we";
        public const string BloodGlucose = "bg";
        public const string Cholesterol = "ch";
        public const string Exercise = "ex";
        public const string Medication = "me";
    }

    public static class PatientUserControlPath
    {
        public const string BPForm = "/UserCtrls/Patient/BloodPressure/BPItemForm.ascx";
        public const string BPGoalForm = "/UserCtrls/Patient/BloodPressure/BPItemGoal.ascx";
        public const string WeightForm = "/UserCtrls/Patient/Weight/WeightItemForm.ascx";
        public const string WeightGoalForm = "/UserCtrls/Patient/Weight/WeightItemGoal.ascx";
        public const string BloodGlucoseForm = "/UserCtrls/Patient/BloodGlucose/BloodGlucoseItemForm.ascx";
        public const string CholesterolForm = "/UserCtrls/Patient/Cholestrol/CholestrolItemForm.ascx";
        public const string CholesterolGoalForm = "/UserCtrls/Patient/Cholestrol/CholestrolItemGoal.ascx";
        public const string ExerciseForm = "/UserCtrls/Patient/Exercise/ExerciseItemForm.ascx";
        public const string ExerciseGoalForm = "/UserCtrls/Patient/Exercise/ExerciseItemGoal.ascx";
        public const string MedicationForm = "/UserCtrls/Patient/Medication/MedicationItemForm.ascx";
    }

    public class AHADefs
    {
        public enum ContentType
        {
            ProviderAnnouncement,
            Patients, Groups, CreateGroup, Alerts, CreateAlert, Messages, Reports, PatientSummary, PatientAlerts,
            PatientGroupMembership, BloodPressure, BloodGlucose, Weight, Cholesterol, PhysicalActivity, Medication,
            HealthHistory, EditProfile, ViewAlertRules, ViewPatientAlertRules, Registration, RegistrationConfirmation,
            ForgotPassword, ForgotPasswordConfirmation, Invitation, AddPatientsToGroup, DisconnectPatient, Login, ResetPassword,
            UpdateGroup, GroupsRightRail, AlertRightRail, GrantProviderAccess, ConnectWithPatient, NewUserConfirmation,
            AddGroupsToPatient, GrantProviderAccessEmail, PatientAnnouncement
        }

        public enum ProviderTipTypes
        {
            Patients,
            Groups,
            Alerts,
            Messages,
            Reports
        }

        public enum PatientContentType
        {
            None, PatientHome, BloodPressureTop, BloodPressureBottom, CholestrolTop, CholestrolBottom,
            BloodGlucoseTop, BloodGlucoseBottom, MedicationTop, MedicationBottom,
            WeightTop, WeightBottom, PhysicalActivityTop, PhysicalActivityBottom,
            CreateReportTop, CreatReportBottom, Resources,
            ChangeUser, ProviderInvitation, MailHome, MailHome_No_Provider_With_Messaging,
            MailView_No_Provider_With_Messaging

        }

        public class FolderType
        {
            public const string MESSAGE_FOLDER_INBOX = "I";
            public const string MESSAGE_FOLDER_DRAFTS = "D";
            public const string MESSAGE_FOLDER_SENT_MESSAGES = "S";
            public const string MESSAGE_FOLDER_TRASH_CAN = "T";
        }

        public class MessageType
        {
            public const string MESSAGE_TYPE_NEW = "N";
            public const string MESSAGE_TYPE_REPLY = "R";
            public const string MESSAGE_TYPE_FORWARD = "F";
        }

        public class MailActionType
        {
            public const string MAIL_ACTION_HOME = "H";
            public const string MAIL_ACTION_NEW = "N";
            public const string MAIL_ACTION_REPLY = "R";
            public const string MAIL_ACTION_FLAG = "F";
            public const string MAIL_ACTION_DELETE = "D";
            public const string MAIL_ACTION_SEND = "S";
            public const string MAIL_ACTION_CANCEL = "C";
        }

        /// <summary>
        /// Defines all Vocabularies
        /// </summary>
        public class VocabDefs
        {

            public const string RelationshipStatus = "RS";
            public const string InsuranceType = "IT";
            public const string IncomeLevel = "IL";
            public const string Ethnicity = "EC";
            public const string EducationLevel = "EL";
            public const string Employment = "EP";
            public const string AccessToTechnology = "AT";
            public const string MeansOfTransportation = "MT";
            public const string PhysicianOrProviderSpecialty = "PS";
            public const string BloodType = "BT";
            public const string States = "ST";
            public const string NameSuffixes = "NS";
            public const string NamePrefixes = "NP";
            public const string Countries = "CT";
            public const string GenderTypes = "GT";
            public const string PersonTypes = "PT";
            public const string RelationshipTypes = "RT";
            public const string PersonalRelationship = "PR";
            public const string Activity = "AV";
            public const string Attitudes = "AD";
            public const string MealTypes = "MP";
            public const string TestLocation = "TL";
            public const string MedicationTypes = "MS";
            public const string ReadingSource = "RE";
            public const string ActionITook = "AI";
            public const string ActivityType = "AP";
            public const string ExerciseIntensity = "EI";
            public const string ReadingType = "RP";
            public const string Salutation = "SL";
            public const string DissmissAlert = "DA";
            public const string IrregularHeartbeat = "IH";

        }

        /// <summary>
        /// Defines all ThingTipTypes which are used to retrieve the 'Tip of the day' 
        /// from the database for the particular Item Type.
        /// </summary>
        public class ThingTipTypes
        {
            public const string Weight = "weight";
            public const string BloodPressure = "blood pressure";
            public const string Diet = "diet";
            public const string Cholesterol = "cholesterol";
            public const string BloodGlucose = "blood glucose";
            public const string Exercise = "Exercise";
        }

    }
}
