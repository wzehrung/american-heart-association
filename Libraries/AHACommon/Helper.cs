﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.AccessControl;

namespace AHACommon
{
    public class Helper
    {
        public static FileSystemRights GetDirectoryPermissions(bool bIsFile, string user, string domainName, string folderPath)
        {
            if (!bIsFile && !Directory.Exists(folderPath))
            {
                return (0);
            }

            if (bIsFile && !File.Exists(folderPath))
            {
                return (0);
            }

            string identityReference = ((domainName + @"\" + user) as string).ToLower();
            DirectorySecurity dirSecurity = Directory.GetAccessControl(folderPath, AccessControlSections.Access);
            foreach (FileSystemAccessRule fsRule in dirSecurity.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount)))
            {
                if (fsRule.IdentityReference.Value.ToLower() == identityReference)
                {
                    return (fsRule.FileSystemRights);
                }
            }
            return (0);
        }

        public static bool DoesCurrentUserHaveReadAccess(string strDir)
        {
            FileSystemRights objFileSystemRights = AHACommon.Helper.GetDirectoryPermissions(true, Environment.UserName, Environment.UserDomainName, strDir);

            if ((objFileSystemRights & FileSystemRights.Modify) == FileSystemRights.Modify)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool DoesCurrentUserHaveFullAccess(bool bIsFile, string strDir)
        {
            FileSystemRights objFileSystemRights = AHACommon.Helper.GetDirectoryPermissions(bIsFile, Environment.UserName, Environment.UserDomainName, strDir);

            if ((objFileSystemRights & FileSystemRights.FullControl) == FileSystemRights.FullControl)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetFileNameForEmailList(bool bIsProvider, int iSchedulerID, string strCampaignTitle)
        {
            string strFileName = string.Empty;
            if (bIsProvider)
            {
                strFileName = string.Format("File_Provider_{0}_{1}", iSchedulerID, !string.IsNullOrEmpty(strCampaignTitle) ? strCampaignTitle : "None");
            }
            else
            {
                strFileName = string.Format("File_Patient_{0}_{1}", iSchedulerID, !string.IsNullOrEmpty(strCampaignTitle) ? strCampaignTitle : "None");
            }
            return strFileName;
        }
        public static bool IsValidDate(DateTime? dt)
        {
            bool bReturn = true;
            if (!dt.HasValue || !(dt.Value.Year >= 1000 && dt.Value.Year <= 9999))
            {
                bReturn = false;
            }
            return bReturn;
        }
    }
}
