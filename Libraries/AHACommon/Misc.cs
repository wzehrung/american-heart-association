﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace AHACommon
{
    public enum PeriodType
    {
        [Description("Days")]
        D,
        [Description("Weeks")]
        W,
        [Description("Months")]
        M
    }
}
