﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO;
using System.Web.Hosting;   // For HostingEnvironment
using System.Xml.Serialization;
using System.Text.RegularExpressions;   // for validation


namespace AHACommon
{
    //
    // This is used for receiving json input to Page Methods.
    //
    public class NameValue
    {
        public NameValue() { }
        public NameValue(string n, string v) { name = n; value = v; }

        public string name { get; set; }
        public string value { get; set; }

    }

    //
    // This is used to represent the metadata of a control
    // Note that because of the way form fields are posted,
    // the RadioGroup type is the value posted from a group of 2 or more Radio.
    // Each Radio in a RadioGroup can be assigned a default value.
    //

    [Serializable]
    public enum ControlWidgetType
    {
        [System.Xml.Serialization.XmlEnum("string")]
        String,
        [System.Xml.Serialization.XmlEnum("dropdown")]
        DropDown,
        [System.Xml.Serialization.XmlEnum("email")]
        Email,
        [System.Xml.Serialization.XmlEnum("url")]
        Url,
        [System.Xml.Serialization.XmlEnum("phonenumber")]
        PhoneNumber,
        [System.Xml.Serialization.XmlEnum("zipcode")]
        Zipcode,
        [System.Xml.Serialization.XmlEnum("checkbox")]
        CheckBox,
        [System.Xml.Serialization.XmlEnum("radio")]
        Radio,
        [System.Xml.Serialization.XmlEnum("radiogroup")]
        RadioGroup,
        [System.Xml.Serialization.XmlEnum("hidden")]
        Hidden,
        [System.Xml.Serialization.XmlEnum("integer")]
        Integer,
        [System.Xml.Serialization.XmlEnum("double")]
        Double,
        [System.Xml.Serialization.XmlEnum("date")]
        Date
    }

    [Serializable()]
    public class ControlWidget
    {
        [System.Xml.Serialization.XmlAttribute("name")]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlAttribute("id")]
        public string Id { get; set; }

        [System.Xml.Serialization.XmlAttribute("type")]
        public ControlWidgetType WidgetType { get; set; }

        [System.Xml.Serialization.XmlAttribute("required")]
        public bool Required { get; set; }

            // This is used during run-time for assignment/use by encapsulating control
        public string Value { get; set; }

        private string ValidationError()
        {
                // this is the generic message
            string  retval = " is required and needs a value" ;

            switch (this.WidgetType)
            {
                case ControlWidgetType.CheckBox:
                    retval = " needs to be checked";
                    break;

                case ControlWidgetType.RadioGroup:
                    retval = " radio button group needs a selection";
                    break;

                case ControlWidgetType.Integer:
                    retval = " input field needs to be a whole number";
                    break;

                case ControlWidgetType.Double:
                    retval = " input field needs to be a whole or decimal number";
                    break;

                default:
                    // already set, use generic message
                    break;
            }

            return retval;
        }

            // This function is used to validate a ControlWidget of any type except dropdown.
            // inValue is the value to validate.
            // refErrMsg is where an error message can be assigned if validation fails
            // returns true if validated, false if not validated
        public bool Validate(string inValue, ref string refErrMsg)
        {
            bool    retval = true ;

            if (this.Required && string.IsNullOrEmpty(inValue))
            {
                refErrMsg = this.Name + ValidationError();
                retval = false;
            }
            else if (!GenericValidation(inValue))
            {
                refErrMsg = this.Name + " contains illegal content";
                retval = false;
            }
            else if( !string.IsNullOrEmpty( inValue ) )     // only validate if there is a value
            {
                switch (this.WidgetType)
                {
                    case ControlWidgetType.String:
                    case ControlWidgetType.DropDown:
                    case ControlWidgetType.CheckBox:
                    case ControlWidgetType.Radio:
                    case ControlWidgetType.RadioGroup:
                    case ControlWidgetType.Hidden:
                        // nothing to do here other than what has already been done
                        // html5 checkbox in chrome uses "on" as value, but it can vary by browser.
                        break;

                    case ControlWidgetType.Email:
                        {
                            try
                            {
                                // .Net way of validating an email address without having to write some Regex
                                System.Net.Mail.MailAddress ma = new System.Net.Mail.MailAddress(inValue);
                            }
                            catch
                            {
                                refErrMsg = this.Name + " is not a valid email address";
                                retval = false;
                            }
                        }
                        break;

                    case ControlWidgetType.PhoneNumber:
                        {
                            // validate the following patterns after removing whitespace:
                            // ##########
                            // ###-###-####
                            // (###)#######
                            // (###)###-####
                            string phregex = @"[(]?[2-9]{1}[0-9]{2}[)-. ,]?[2-9]{1}[0-9]{2}[-. ,]?[0-9]{4}";
                            string cleaned = inValue.Replace(" ", "");
                            if (!Regex.IsMatch(cleaned, phregex))
                            {
                                refErrMsg = this.Name + " is not a valid US phone number";
                                retval = false;
                            }
                        }
                        break;

                    case ControlWidgetType.Zipcode:
                        {
                            // validate US zipcode only
                            string zipregex10 = @"\d{5}-\d{4}";
                            string zipregex5 = @"\d{5}";
                            int inlen = inValue.Length;

                            if (! ((Regex.IsMatch(inValue, zipregex10) && inlen == 10) || (Regex.IsMatch(inValue, zipregex5 ) && inlen == 5)) )
                            {
                                refErrMsg = this.Name + " is not a valid US zipcode";
                                retval = false;
                            }
                        }
                        break;

                    case ControlWidgetType.Url:
                        {
                            Uri uriResult;
                            if (!Uri.TryCreate(inValue, UriKind.Absolute, out uriResult) || !(uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
                            {
                                refErrMsg = this.Name + " is not a valid web address (e.g., http://www.site.com)";
                                retval = false;
                            }
                        }
                        break;

                    case ControlWidgetType.Integer:
                        {
                            int ival;

                            if (!Int32.TryParse(inValue, out ival))
                            {
                                refErrMsg = this.Name + " is not a whole number";
                                retval = false;
                            }
                        }
                        break;

                    case ControlWidgetType.Double:
                        {
                            double dval;
                            if (!double.TryParse(inValue, out dval))
                            {
                                refErrMsg = this.Name + " is not a whole or decimal number";
                                retval = false;
                            }
                        }
                        break;

                    case ControlWidgetType.Date:
                        {
                            DateTime    dt ;
                            if (!DateTime.TryParse(inValue, out dt))
                            {
                                refErrMsg = this.Name + " is not a date";
                                retval = false;
                            }
                        }
                        break;

                    default:
                        retval = false;
                        refErrMsg = "Internal Error: (" + this.Name + ") has unknown type";
                        break;
                }
            }
            return retval ;
        }

            // This routine is used to prevent SQL Injection and cross-site scripting (XSS) attacks.
            // More checks can/should be added
        private bool GenericValidation(string inStr)
        {
            
            if (inStr.Length > 1000)
                return false;

                // don't allow for some standard sql content
            string sqlcheck = inStr.ToLower();
            if (sqlcheck.Contains("@@") ||
                sqlcheck.Contains("select * ") ||
                sqlcheck.Contains("drop table ") ||
                sqlcheck.Contains("--"))
                //sqlcheck.Contains(';'))
                return false;


            return true;
        }
    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("UXControl")]
    public class ControlWidgetCollection
    {
        [System.Xml.Serialization.XmlArray("ControlWidgets")]
        [System.Xml.Serialization.XmlArrayItem("ControlWidget", typeof(ControlWidget))]
        public ControlWidget[] Widgets { get; set; }
    }



    //
    // This is a base class for Controls
    //
    public abstract class UXControl
    {
        private string mMarkup;
        private ControlWidgetCollection mWidgets;

        public const int    EnglishLanguage = 1;    // A lot of the DAL routines require a language
        public const string InputsKeyword_Print = "print";
        public const string InputsValue_True = "true";
        public const string HtmlCheckBoxChecked = "on";

        // The name also determines the xml and html files for the control
        public string Name
        {
            get;
            set;
        }

        public ControlWidgetCollection Metadata
        {
            get { return mWidgets; }
        }

        public Dictionary<string, ControlWidget> MetadataHash
        {
            get;
            set;
        }

        //control initialization
        virtual public bool Initialize()
        {
            if (string.IsNullOrEmpty(Name))
                return false;

            //
            // Read the .html (layout) file for the control
            //
            bool result = true;
            try
            {
                string fnLayout = "~/Controls/Assets/" + Name + ".html";    // TODO: GET PREFIX FROM CONFIGURATION FILE!!!!!!!!!!
                // Can't use Server.MapPath() in a static function
                using (StreamReader sr = new StreamReader(HostingEnvironment.MapPath(fnLayout)))
                {
                    mMarkup = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                // result = "<div>Error: " + ex.Message + "</div>";
                mMarkup = string.Empty;
                result = false;
            }

            //
            // Read the .xml (metadata) file for the control
            //
            try
            {
                string fnMeta = "~/Controls/Assets/" + Name + ".xml";       // TODO: GET PREFIX FROM CONFIGURATION FILE!!!!!!!!!!
                XmlSerializer serializer = new XmlSerializer(typeof(ControlWidgetCollection));
                StreamReader sr = new StreamReader(HostingEnvironment.MapPath(fnMeta));
                mWidgets = (ControlWidgetCollection)serializer.Deserialize(sr);
                sr.Close();

                // Create a hashtable of widgets for quicker lookup
                MetadataHash = mWidgets.Widgets.ToDictionary(k => k.Name, v => v);
            }
            catch (Exception ex2)
            {
                mWidgets = null;
                result = false;
            }

            return result;
        }

        //standard handler for dynamic content areas and popups, assumes login is required
        public static string ControlHandler(UXControl control, bool INIT, AHACommon.NameValue[] inputs)
        {
            return ControlHandler(control, INIT, inputs, true);
        }

        //standard handler for dynamic content areas and popups, must specify if login is required
        public static string ControlHandler(UXControl control, bool INIT, AHACommon.NameValue[] inputs, bool LoginRequired)
        {
            string result = AHACommon.UXControl.GenericHtmlErrorMessage;

            if (LoginRequired && !CheckProviderSessionActive())
            {
                return (INIT) ? RedirectToLoginJavascript : JsonRedirectToLogin;
            }

            if (control != null)
            {
                result = (INIT) ? control.GetMarkup(inputs) : control.ProcessFormValues(inputs);
            }

            return result;
        }

        //override to process request for content to render
        abstract public string GetMarkup(NameValue[] inputs);

        //handles assignment of values per the control's XML configuration file
        protected string PrepareMarkup( )
        {
            //FIXME - fails on no session cases
            //check for session expiration
            //if (!UXControl.CheckProviderSessionActive())
            //{
             //   return RedirectToLoginJavascript;
            //}

            string result = mMarkup;

            if (!string.IsNullOrEmpty(mMarkup))
            {
                //
                // Perform any boilerplate assignment of widget values
                // Supported widget types are:
                //  1. String, Email, Url, PhoneNumber, Zipcode - replaces $xxx$ with ControlWidget.Value
                //  2. CheckBox - if ControlWidget.Value equals "true", then will check the checkbox
                //  3. Radio - if ControlWidget.Value equals "true", then will select that radio button.
                foreach (ControlWidget cw in Metadata.Widgets)
                {
                    switch (cw.WidgetType)
                    {
                        case ControlWidgetType.Email:
                        case ControlWidgetType.PhoneNumber:
                        case ControlWidgetType.String:
                        case ControlWidgetType.Url:
                        case ControlWidgetType.Zipcode:
                        case ControlWidgetType.DropDown:
                        case ControlWidgetType.Hidden:
                        case ControlWidgetType.Integer:
                        case ControlWidgetType.Double:
                        case ControlWidgetType.Date:
                            result = result.Replace("$" + cw.Id + "$", cw.Value);
                            break;

                        case ControlWidgetType.CheckBox:
                            result = result.Replace("$" + cw.Id + "$", (cw.Value.Equals("true") ? "checked=\"checked\"" : string.Empty));
                            break;

                        case ControlWidgetType.Radio:
                            result = result.Replace("$" + cw.Id + "$", (cw.Value.Equals("true") ? "checked=\"checked\"" : string.Empty));
                            break;

                        default:
                            break;
                    }
                }
            }

            return result;
        }

        //default handler for values returned to the control
        virtual public string ProcessFormValues(NameValue[] inputs )
        {
            return JsonFailure("Unimplemented functionality");
        }

        //check if provider login has occured
        public static bool CheckProviderSessionActive()
        {
            return (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session["ProviderIdentityContext"] != null);
        }

        //convenience content return methods

        public static string JsonSuccess(string sTitle, string sMsg)
        {
            return "[{\"success\" : \"true\",\"message\" : \"" + PopupResultMarkup(sTitle, sMsg) + "\"}]";
        }

        [Obsolete("JsonSuccess(string) is deprecated, please use JsonSuccess(string, string) instead.")]
        public static string JsonSuccess(string sMsg)
        {
            return "[{\"success\" : \"true\",\"message\" : \"" + sMsg + "\"}]";
        }

        public static bool IsJsonSuccess(string sJson)
        {
            return sJson.Contains( "\"success\" : \"true\"" ) ;
        }

        public static string JsonFailure(string sMsg)
        {
            return "[{\"success\" : \"false\",\"message\" : \"" + sMsg + "\"}]";
        }

        public static string JsonRedirect(string sUrl)
        {
            return "[{\"success\" : \"redirect\",\"message\" : \"" + sUrl + "\"}]";
        }

        public static string JsonRefresh(string sTitle, string sMsg)
        {
            return "[{\"success\" : \"true\",\"message\" : \"" + PopupResultMarkup(sTitle, sMsg) + "\", \"refresh\" : \"true\"}]";
        }

        [Obsolete("JsonRefresh(string) is deprecated, please use JsonRefresh(string, string) instead.")]
        public static string JsonRefresh(string sMsg)
        {
            return "[{\"success\" : \"true\",\"message\" : \"" + sMsg + "\", \"refresh\" : \"true\"}]";
        }

        private static string PopupResultMarkup(string sTitle, string sMsg) 
        {
            return "<div id='data-form'><div class='twelvecol first'><div class='twelvecol first'><h3>" + sTitle
                + "</h3></div><div class='twelvecol first tablet-twelvecol mobile-twelvecol'><p>" + sMsg
                + "</p><div class='twelvecol first'><div class='cancel-popup gradient-button center sixcol first'>OK</div></div></div></div></div>";
        }

        public static string JsonPrint(string sPrintContents)
        {
            // PJB: we need to either escape all the double quotes, or replace double quotes with single quotes.
            // Andrew suggested we replace with single quotes.
            // John and Phil found that newline and tab character also gum up the works
            // Lastly, there are some JSON encoding classes in C#, but they only seem to be available in Visual Studio 2012
            string contents = sPrintContents.Replace("\"", "'");
            contents = contents.Replace("\r\n", "\n");
            contents = contents.Replace("\r", "\n");
            contents = contents.Replace("\n", "\\n");
            contents = contents.Replace("\t", "\\t");
            return "[{\"success\" : \"print\",\"message\" : \"" + contents + "\"}]";
        }

        public static string FormatErrorPopupContent(string popupTitle)
        {
            return string.Format("<div id=\"data-form\"><div class=\"popup-title\">{0}</div><div class=\"popup-body\"><p>Error initializing popup. Please close and try again.</p></div></div>", popupTitle);
        }

        public static string GenericHtmlErrorMessage = "<div>Error: Initialization of control failed</div>";

        public static string RedirectToLoginJavascript = "<script>window.location.replace('/');</script>";

        public static string JsonRedirectToLogin = "[{\"success\" : \"redirect\",\"message\" : \"/\"}]";

    }
}
