﻿using System;
using System.Collections.Generic;
using System.Text;
using GRCBase;

namespace AHACommon
{
    public struct PatientProviderFaxSettings
    {
        public string File_Invitation_To_Patient;
        public string File_Invitation_To_Provider;
    }

    public struct FaxSettings
    {
        public string AccountID;
        public string UserName;
        public string Password;
    }

    public struct ContentExpNotificationEmailSettings
    {
        public string Subject_Content_Expiration;
        public string File_Content_Expiration;
    }

    public struct InActiveNotificationEmailSettings
    {
        public string Subject_InActive_Notification;
        public string File_InActive_Notification;
    }

    public struct Heart360CampaignEmailProcessSettings
    {
        public string File_14Day_Inactive_Patient;
        public string File_30Day_Inactive_Patient;
        public string File_14Day_Inactive_Provider;
        public string File_30Day_Inactive_Provider;
        public string File_AppointmentReminder;
        public string File_BP_Trigger_Excellent;
        public string File_BP_Trigger_NeedsImprovement;
        public string File_BP_Trigger_Warning;
    }

    public struct PatientProviderEmailSettings
    {
        public string Subject_Register_Provider;
        public string File_Register_Provider;
        public string Subject_Forgot_Password;
        public string File_Forgot_Password;
        public string Subject_Forgot_Username;
        public string File_Forgot_Username;
        public string Subject_Disconnect_Provider;
        public string File_Disconnect_Provider;
        public string Subject_Disconnect_Patient;
        public string File_Disconnect_Patient;
        public string Subject_Invite_Patient;
        public string File_Invite_Patient;
        public string Subject_Invite_Provider;
        public string File_Invite_Provider;
        public string File_Invitation_Confirmation_Patient;
        public string Subject_Invitation_Confirmation_Patient;
        public string File_Invitation_Confirmation_Provider;
        public string Subject_Invitation_Confirmation_Provider;

        public string File_Alert_Provider;
        public string Subject_Alert_Provider;

        public string File_Summary_Email_Provider;
        public string Subject_Summary_Email_Provider;

        public string File_AlertModified;
        public string Subject_AlertModified;

        public string File_New_Message_Provider;
        public string Subject_New_Message_Provider;

        public string File_New_Message_Patient;
        public string Subject_New_Message_Patient;

        public string File_Alert_Message_Patient;
        public string Subject_Alert_Message_Patient;
    }
    /// <summary>
    /// This struct declares the variables required for EmailSettings.
    /// </summary>
    public struct EmailSettings
    {
        public string FromName;
        public string FromEmail;
        public string Subject;
        public string MailTemplate;
        public string ToEmail;
        public string ToName;
    }

    /// <summary>
    /// This struct declares the variables required for Heart360HeaderLinks.
    /// </summary>
    public struct Heart360HeaderLinks
    {
        public string DonateLink;
        public string HelpLink;
        public string ContactLink;
        public string SiteIndexLink;
        public string CareersLink;
    }

    /// <summary>
    /// This struct declares the variables required for Heart360FooterLinks.
    /// </summary>
    public struct Heart360FooterLinks
    {
        public string UseOfPersonalInformationLink;
        public string CopyrightLink;
        public string EthicsPolicyLink;
        public string ConflictOfInterestLink;
        public string LinkingPolicyLink;
        public string DiversityLink;
    }

    /// <summary>
    /// This struct declares the variables required for PDFReportSetting.
    /// </summary>
    public struct PDFReportSetting
    {
        public string Author;
        public string Title;
        public string PDFReportFileName;
    }

    public class AHAAppSettings
    {
        /// <summary>
        /// TempFilesURL
        /// </summary>
        public static string TempFilesURL
        {
            get
            {
                return ConfigReader.GetValue("TempFilesURL");
            }
        }

        public static string TempFilesDirectory
        {
            get
            {
                return ConfigReader.GetValue("TempFilesDirectory");
            }
        }


        /// <summary>
        /// MyLifeCheckURL
        /// </summary>
        public static string MyLifeCheckURL
        {
            get
            {
                return ConfigReader.GetValue("MyLifeCheckURL");
            }
        }

        /// <summary>
        /// MLCCampaignURL
        /// </summary>
        public static string MLCCampaignURL
        {
            get
            {
                return ConfigReader.GetValue("MLCCampaignURL");
            }
        }

        /// <summary>
        /// BPCampaignURL
        /// </summary>
        public static string BPCampaignURL
        {
            get
            {
                return ConfigReader.GetValue("BPCampaignURL");
            }
        }

        /// <summary>
        /// PolkaReminderScriptServiceUrl
        /// </summary>
        public static string PolkaReminderScriptServiceUrl
        {
            get
            {
                return ConfigReader.GetValue("PolkaReminderScriptServiceUrl");
            }
        }

        /// <summary>
        /// PolkaReminderIframeUrl
        /// </summary>
        public static string PolkaReminderIframeUrl
        {
            get
            {
                return ConfigReader.GetValue("PolkaReminderIframeUrl");
            }
        }

        /// <summary>
        /// EmailListDownloadPath
        /// </summary>
        public static string EmailListDownloadPath
        {
            get
            {
                return ConfigReader.GetValue("EmailListDownloadPath");
            }
        }

        /// <summary>
        /// EnableJSErrorLog
        /// </summary>
        public static bool EnableJSErrorLog
        {
            get
            {
                return ConfigReader.GetValue("EnableJSErrorLog") == "1" ? true : false;
            }
        }

        /// <summary>
        /// Disclaimer_Provider
        /// </summary>
        public static string Disclaimer_Provider_PDF
        {
            get
            {
                //return System.Web.HttpContext.GetGlobalResourceObject("Common1", "Provider_Footer_Disclaimer").ToString();
                return System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Footer_Disclaimer_Provider").ToString().Replace("<br/>", "\r\n");
            }
        }

        /// <summary>
        /// Disclaimer_Patient
        /// </summary>
        public static string Disclaimer_Patient_PDF
        {
            get
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Footer_Disclaimer_Patient").ToString();
                //return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Footer_Disclaimer").ToString();
            }
        }

        /// <summary>
        /// Disclaimer_Provider
        /// </summary>
        public static string Disclaimer_Provider_HTML
        {
            get
            {
                string strVal = System.Web.HttpContext.GetGlobalResourceObject("Email", "Provider_Footer_Disclaimer").ToString().Replace("\\r\\n", "<br />");
                strVal = System.Text.RegularExpressions.Regex.Replace(strVal, "##REGISTERED##", "<sup>&#174;</sup>");
                return strVal;
            }
        }

        /// <summary>
        /// Disclaimer_Patient
        /// </summary>
        public static string Disclaimer_Patient_HTML
        {
            get
            {
                string strVal = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Footer_Disclaimer_Patient").ToString().Replace("\\r\\n", "<br />");
                strVal = System.Text.RegularExpressions.Regex.Replace(strVal, "##REGISTERED##", "<sup>&#174;</sup>");
                return strVal;
            }
        }

        /// <summary>
        /// SiteUrl
        /// </summary>
        public static string SiteUrl
        {
            get
            {
                return ConfigReader.GetValue("SiteUrl");
            }
        }

        /// <summary>
        /// SiteUrlNoHttps
        /// </summary>
        public static string SiteUrlNoHttps
        {
            get
            {
                return ConfigReader.GetValue("SiteUrl").Replace("https://", "http://");
            }
        }

        /// <summary>
        /// SystemEmail
        /// </summary>
        public static string SystemEmail
        {
            get
            {
                return ConfigReader.GetValue("SystemEmail");
            }
        }

        /// <summary>
        /// FeedbackToEmail
        /// </summary>
        public static string FeedbackToEmail
        {
            get
            {
                return ConfigReader.GetValue("FeedbackToEmail");
            }
        }

        /// <summary>
        /// AlertGenerationProcessErrorEmail
        /// </summary>
        public static string AlertGenerationProcessErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("AlertGenerationProcessErrorEmail");
            }
        }

        /// <summary>
        /// DailyMailerErrorEmail
        /// </summary>
        public static string DailyMailerErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("DailyMailerErrorEmail");
            }
        }

        /// <summary>
        /// DailyGroupMembershipProcessErrorEmail
        /// </summary>
        public static string DailyGroupMembershipProcessErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("DailyGroupMembershipProcessErrorEmail");
            }
        }

        /// <summary>
        /// ContentExpirationNotificationProcessErrorEmail
        /// </summary>
        public static string ContentExpirationNotificationProcessErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("ContentExpirationNotificationProcessErrorEmail");
            }
        }

        /// <summary>
        /// Heart360CampaignEmailProcessCampaignUrlSuffix
        /// </summary>
        public static string Heart360CampaignEmailProcessCampaignUrlSuffix
        {
            get
            {
                return ConfigReader.GetValue("Heart360CampaignEmailProcessCampaignUrlSuffix");
            }
        }

        /// <summary>
        /// Heart360CampaignEmailProcessErrorEmail
        /// </summary>
        public static string Heart360CampaignEmailProcessErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("Heart360CampaignEmailProcessErrorEmail");
            }
        }

        /// <summary>
        /// InActiveUserNotificationProcessErrorEmail
        /// </summary>
        public static string InActiveUserNotificationProcessErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("InActiveUserNotificationProcessErrorEmail");
            }
        }

        /// <summary>
        /// RxTermUpdateProcessErrorEmail
        /// </summary>
        public static string RxTermUpdateProcessErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("RxTermUpdateProcessErrorEmail");
            }
        }

        /// <summary>
        /// PolkaDataHVUploaderProcessErrorEmail
        /// </summary>
        public static string PolkaDataHVUploaderProcessErrorEmail
        {
            get
            {
                return ConfigReader.GetValue("PolkaDataHVUploaderProcessErrorEmail");
            }
        }

        /// <summary>
        /// SupportEmail
        /// </summary>
        public static string SupportEmail
        {
            get
            {
                return ConfigReader.GetValue("SupportEmail");
            }
        }

        /// <summary>
        /// Error page url
        /// </summary>
        public static string ErrorPageRelativePath
        {
            get
            {
                return ConfigReader.GetValue("ErrorPageRelativePath");
            }
        }

        /// <summary>
        /// file not found page url
        /// </summary>
        public static string FileNotFoundPageRelativeURL
        {
            get
            {
                return ConfigReader.GetValue("FileNotFoundPageRelativeURL");
            }
        }

        /// <summary>
        /// This function is used to get the information about which is the SMTPServer to be used.
        /// </summary>
        public static string SMTPServer
        {
            get
            {
                return ConfigReader.GetValue("SMTPServer");
            }
        }

        /// <summary>
        /// This function is used to get the information about XmlFilesDirectory. It is here
        /// where all the Xml files used by the application are stored.
        /// </summary>
        public static string XmlFilesDirectory
        {
            get
            {
                return ConfigReader.GetValue("XmlFilesDirectory");
            }
        }

        /// <summary>
        /// This function is used to get the information about EmailTemplateDirectory.
        /// </summary>
        public static string EmailTemplateDirectory
        {
            get
            {
                return ConfigReader.GetValue("EmailTemplateDirectory");
            }
        }

        /// <summary>
        /// This function is used to get the information about VersionNumber.
        /// </summary>
        public static string VersionNumber
        {
            get
            {
                return ConfigReader.GetValue("VersionNumber");
            }
        }

        /// <summary>
        /// This function is used to know if Messaging is enabled or not.
        /// </summary>
        public static bool IsMessagingEnabled
        {
            get
            {
                return ConfigReader.GetValue("Is_Messaging_Enabled") == "1" ? true : false;
            }
        }

        /// <summary>
        /// This function is used to know if EnableDataCacheDiagnostics is enabled or not.
        /// </summary>
        public static bool EnableDataCacheDiagnostics
        {
            get
            {
                return ConfigReader.GetValue("EnableDataCacheDiagnostics") == "1" ? true : false;
            }
        }

        ///// <summary>
        ///// DisclaimerTextPDF
        ///// </summary>
        //public static string DisclaimerTextPDF
        //{
        //    get
        //    {
        //        return ConfigReader.GetValue("DisclaimerTextPDF");
        //    }
        //}

        ///// <summary>
        ///// DisclaimerTextHtml
        ///// </summary>
        //public static string DisclaimerTextHtml
        //{
        //    get
        //    {
        //        return ConfigReader.GetValue("DisclaimerTextHtml");
        //    }
        //}

        /// <summary>
        /// SenderEmail
        /// </summary>
        public static string SenderEmail
        {
            get
            {
                return ConfigReader.GetValue("SenderEmail");
            }
        }

        /// <summary>
        /// Terms of use
        /// </summary>
        public static string TermsOfUse
        {
            get
            {
                return ConfigReader.GetValue("TermsOfUse");
            }
        }

        /// <summary>
        /// Provider Portal
        /// </summary>
        public static string ProviderPortal
        {
            get
            {
                return ConfigReader.GetValue("ProviderPortal");
            }
        }

        public static string ProviderLogin
        {
            get
            {
                return ConfigReader.GetValue("ProviderLogin");
            }
        }

        public static string ProviderRegister
        {
            get
            {
                return ConfigReader.GetValue("ProviderRegister");
            }
        }

        public static string ProviderHome
        {
            get
            {
                return ConfigReader.GetValue("ProviderHome");
            }
        }

        public static string VolunteerLogin
        {
            get
            {
                return ConfigReader.GetValue("VolunteerLogin");
            }
        }
        public static string VolunteerRegister
        {
            get
            {
                return ConfigReader.GetValue("VolunteerRegister");
            }
        }

        public static string ProviderForgotPassword
        {
            get
            {
                return ConfigReader.GetValue("ProviderForgotPassword");
            }
        }

        public static bool DisableMedicationSuggestions
        {
            get
            {
                return ConfigReader.GetValue("DisableMedicationSuggestions") == "0" ? false : true;
            }
        }

        /// <summary>
        /// This function is used to know if IsSSLEnabled is enabled or not.
        /// </summary>
        public static bool IsSSLEnabled
        {
            get
            {
                return ConfigReader.GetValue("IsSSLEnabled") == "1" ? true : false;
            }
        }

        /// <summary>
        /// This function is used to know if IsGoogleAnalyticsEnabled is enabled or not.
        /// </summary>
        public static bool IsGoogleAnalyticsEnabled
        {
            get
            {
                return ConfigReader.GetValue("IsGoogleAnalyticsEnabled") == "0" ? false : true;
            }
        }

        /// <summary>
        /// This function is used to check the number of days  for which provider was Inactive.
        /// </summary>
        public static int Provider_NumberOfInActiveDays
        {
            get
            {
                return Convert.ToInt32(ConfigReader.GetValue("Provider_NumberOfInActiveDays"));
            }
        }

        /// <summary>
        /// This function is used to check the number of days  for which provider was Inactive.
        /// </summary>
        public static int User_NumberOfInActiveDays
        {
            get
            {
                return Convert.ToInt32(ConfigReader.GetValue("User_NumberOfInActiveDays"));
            }
        }

        /// <summary>
        /// This function is used to get number of days before the email has to be sent for the content expiration .
        /// </summary>
        /** PJB: fluff, only used in a scheduled task
        public static int DaysBefore_ContentExpirationNotification
        {
            get
            {
                return Convert.ToInt32(ConfigReader.GetValue("DaysBefore_ContentExpirationNotification"));
            }
        }
        **/

        /// <summary>
        /// This function is used to get information about the Author, PersonalInfoTitle, PersonalInfoReportFileName
        /// for the PDF of My Personal Information.
        /// </summary>
        public static PDFReportSetting PersonalInfoPDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("PersonalInfoTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("PersonalInfoReportFileName");
                return setting;
            }
        }

        /// <summary>
        /// This function is used to get information about the Author, HealthHistoryTitle, HealthHistoryReportFileName
        /// for the PDF of My Health History.
        /// </summary>
        public static PDFReportSetting HealthHistoryPDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("HealthHistoryTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("HealthHistoryReportFileName");
                return setting;
            }
        }

        /// <summary>
        /// This function is used to get information about the Author, EmergencyContactTitle, EmergencyContactReportFileName
        /// for the PDF of My Emergency Contact.
        /// </summary>

        public static PDFReportSetting EmergencyContactPDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("EmergencyContactTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("EmergencyContactReportFileName");
                return setting;
            }
        }


        /// <summary>
        /// This function is used to get information about the Author, WeightTitle, WeightReportFileName
        /// for the PDF of My Weight.
        /// </summary>
        public static PDFReportSetting WeightPDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("WeightTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("WeightReportFileName");
                return setting;
            }
        }

        /// <summary>
        /// This function is used to get information about the Author, BloodPressureTitle, BloodPressureReportFileName
        /// for the PDF of My BloodPressure.
        /// </summary>
        public static PDFReportSetting BloodPressurePDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("BloodPressureTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("BloodPressureReportFileName");
                return setting;
            }
        }


        /// <summary>
        /// This function is used to get information about the Author, BloodGlucoseTitle, BloodGlucoseReportFileName
        /// for the PDF of My BloodGlucose.
        /// </summary>
        public static PDFReportSetting BloodGlucosePDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("BloodGlucoseTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("BloodGlucoseReportFileName");
                return setting;
            }
        }

        /// <summary>
        /// This function is used to get information about the Author, CholesterolTitle, CholesterolReportFileName
        /// for the PDF of My Cholesterol.
        /// </summary>
        public static PDFReportSetting CholesterolPDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("CholesterolTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("CholesterolReportFileName");
                return setting;
            }
        }

        /// <summary>
        /// This function is used to get information about the Author, CurrentMedicationTitle, CurrentMedicationReportFileName
        /// for the PDF of Current Medication.
        /// </summary>
        public static PDFReportSetting CurrentMedicationPDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("CurrentMedicationTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("CurrentMedicationReportFileName");
                return setting;
            }
        }

        /// <summary>
        /// This function is used to get information about the Author, DiscontinuedMedicationTitle, DiscontinuedMedicationReportFileName
        /// for the PDF of Discontinued Medication.
        /// </summary>
        public static PDFReportSetting DiscontinuedMedicationPDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("DiscontinuedMedicationTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("DiscontinuedMedicationReportFileName");
                return setting;
            }
        }


        /// <summary>
        /// This function is used to get information about the Author, ExerciseTitle, ExerciseReportFileName
        /// for the PDF of My Exercise.
        /// </summary>
        public static PDFReportSetting ExercisePDFCreationSettings
        {
            get
            {
                PDFReportSetting setting = new PDFReportSetting();
                setting.Author = ConfigReader.GetValue("Author");
                setting.Title = ConfigReader.GetValue("ExerciseTitle");
                setting.PDFReportFileName = ConfigReader.GetValue("ExerciseReportFileName");
                return setting;
            }
        }


        /// <summary>
        /// This function is get the effective days for applying color coding
        /// </summary>
        public static int? ColorCoding_EffectiveDays
        {
            get
            {
                string strEffectiveDays = ConfigReader.GetValue("ColorCoding_EffectiveDays");
                int? intEffectiveDays = null;
                if (!string.IsNullOrEmpty(strEffectiveDays))
                {
                    intEffectiveDays = -1 * Convert.ToInt32(strEffectiveDays);
                }
                return intEffectiveDays;
            }
        }

        public static string PatientForgotPasswordURL
        {
            get
            {
                return ConfigReader.GetValue("PatientForgotPasswordURL");
            }
        }

        #region Email Settings

        /// <summary>
        /// This function is used to get the settings that are required for sending mail to 
        /// the technical support regarding billing queries or issues.
        /// </summary>
        public static EmailSettings TechSupportBillingEmailSetting
        {
            get
            {
                EmailSettings emailsetting = new EmailSettings();
                emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportBillingEmailTemplate");
                emailsetting.Subject = ConfigReader.GetValue("TechSupportBillingEmailSubject");
                emailsetting.FromEmail = ConfigReader.GetValue("TechSupportBillingEmailBillingFrom");
                emailsetting.FromName = ConfigReader.GetValue("TechSupportBillingEmailFromName");
                emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Billing");
                emailsetting.ToName = ConfigReader.GetValue("TechSupportBillingEmailToName");
                return emailsetting;
            }
        }

        /// <summary>
        /// This function is used to get the settings that are required for sending mail to 
        /// the technical support regarding ajax related issues.
        /// </summary>
        public static EmailSettings TechSupportAjaxEmailSetting
        {
            get
            {
                EmailSettings emailsetting = new EmailSettings();
                emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportAjaxEmailTemplate");
                emailsetting.Subject = ConfigReader.GetValue("TechSupportAjaxEmailSubject");
                emailsetting.FromEmail = ConfigReader.GetValue("TechSupportAjaxEmailFrom");
                emailsetting.FromName = ConfigReader.GetValue("TechSupportAjaxEmailFromName");
                emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Ajax");
                emailsetting.ToName = ConfigReader.GetValue("TechSupportAjaxEmailToName");
                return emailsetting;
            }
        }

        /// <summary>
        /// This function is used to get the settings that are required for sending mail to 
        /// the technical support regarding anthem related issues.
        /// </summary>
        public static EmailSettings TechSupportAnthemEmailSetting
        {
            get
            {
                EmailSettings emailsetting = new EmailSettings();
                emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportAnthemEmailTemplate");
                emailsetting.Subject = ConfigReader.GetValue("TechSupportAnthemEmailSubject");
                emailsetting.FromEmail = ConfigReader.GetValue("TechSupportAnthemEmailFrom");
                emailsetting.FromName = ConfigReader.GetValue("TechSupporAnthemEmailFromName");
                emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Anthem");
                emailsetting.ToName = ConfigReader.GetValue("TechSupportAnthemEmailToName");
                return emailsetting;
            }
        }

        /// <summary>
        /// This function is used to get the settings that are required for sending mail to 
        /// the technical support regarding component art related issues.
        /// </summary>
        public static EmailSettings TechSupportComponentArtEmailSetting
        {
            get
            {
                EmailSettings emailsetting = new EmailSettings();
                emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportComponentArtEmailTemplate");
                emailsetting.Subject = ConfigReader.GetValue("TechSupportComponentArtEmailSubject");
                emailsetting.FromEmail = ConfigReader.GetValue("TechSupportComponentArtEmailFrom");
                emailsetting.FromName = ConfigReader.GetValue("TechSupporComponentArtEmailFromName");
                emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_ComponentArt");
                emailsetting.ToName = ConfigReader.GetValue("TechSupportComponentArtEmailToName");
                return emailsetting;
            }
        }

        /// <summary>
        /// This function is used to get the settings that are required for sending mail to 
        /// the technical support regarding application specific issues.
        /// </summary>
        public static EmailSettings TechSupportApplicationEmailSetting
        {
            get
            {
                EmailSettings emailsetting = new EmailSettings();
                emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportApplicationEmailTemplate");
                emailsetting.Subject = ConfigReader.GetValue("TechSupportApplicationEmailSubject");
                emailsetting.FromEmail = ConfigReader.GetValue("TechSupportApplicationEmailFrom");
                emailsetting.FromName = ConfigReader.GetValue("TechSupporApplicationEmailFromName");
                emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Application");
                emailsetting.ToName = ConfigReader.GetValue("TechSupportApplicationEmailToName");
                return emailsetting;
            }
        }

        /// <summary>
        /// This function is used to get the settings that are required for sending mail to 
        /// the technical support regarding javascript issues.
        /// </summary>
        public static EmailSettings TechSupportJavascriptEmailSetting
        {
            get
            {
                EmailSettings emailsetting = new EmailSettings();
                emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportJavascriptEmailTemplate");
                emailsetting.Subject = ConfigReader.GetValue("TechSupportJavascriptEmailSubject");
                emailsetting.FromEmail = ConfigReader.GetValue("TechSupportJavascriptEmailFrom");
                emailsetting.FromName = ConfigReader.GetValue("TechSupporJavascriptEmailFromName");
                emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Javascript");
                emailsetting.ToName = ConfigReader.GetValue("TechSupportJavascriptEmailToName");
                return emailsetting;
            }
        }

        /// <summary>
        /// This function is used to get the settings that are required for sending mail to 
        /// the technical support regarding other issues.
        /// </summary>
        public static EmailSettings TechSupportDefaultEmailSetting
        {
            get
            {
                EmailSettings emailsetting = new EmailSettings();
                emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportDefaultEmailTemplate");
                emailsetting.Subject = ConfigReader.GetValue("TechSupportDefaultEmailSubject");
                emailsetting.FromEmail = ConfigReader.GetValue("TechSupportDefaultEmailFrom");
                emailsetting.FromName = ConfigReader.GetValue("TechSupporDefaultEmailFromName");
                emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Default");
                emailsetting.ToName = ConfigReader.GetValue("TechSupportDefaultEmailToName");
                return emailsetting;
            }
        }

        /// <summary>
        /// This function is used to get the links corresponding to the header
        /// </summary>
        public static Heart360HeaderLinks HeaderLinks
        {
            get
            {
                Heart360HeaderLinks headerlinks = new Heart360HeaderLinks();
                headerlinks.DonateLink = ConfigReader.GetValue("DonateLink");
                headerlinks.CareersLink = ConfigReader.GetValue("CareersLink");
                headerlinks.ContactLink = ConfigReader.GetValue("ContactLink");
                headerlinks.HelpLink = ConfigReader.GetValue("HelpLink");
                headerlinks.SiteIndexLink = ConfigReader.GetValue("SiteIndexLink");
                return headerlinks;
            }
        }

        /// <summary>
        /// This function is used to get the links corresponding to the footer
        /// </summary>
        public static Heart360FooterLinks FooterLinks
        {
            get
            {
                Heart360FooterLinks footerlinks = new Heart360FooterLinks();
                footerlinks.ConflictOfInterestLink = ConfigReader.GetValue("ConflictOfInterestLink");
                footerlinks.CopyrightLink = ConfigReader.GetValue("CopyrightLink");
                footerlinks.DiversityLink = ConfigReader.GetValue("DiversityLink");
                footerlinks.EthicsPolicyLink = ConfigReader.GetValue("EthicsPolicyLink");
                footerlinks.LinkingPolicyLink = ConfigReader.GetValue("LinkingPolicyLink");
                footerlinks.UseOfPersonalInformationLink = ConfigReader.GetValue("UseOfPersonalInformationLink");
                return footerlinks;
            }
        }

        public static PatientProviderEmailSettings PatientProviderEmailSettingsConfig
        {
            get
            {
                PatientProviderEmailSettings emailSettings = new PatientProviderEmailSettings();

                if (IsMessagingEnabled)
                {
                    emailSettings.File_Disconnect_Patient = ConfigReader.GetValue("File_Disconnect_Patient_Messaging");
                    emailSettings.File_Disconnect_Provider = ConfigReader.GetValue("File_Disconnect_Provider_Messaging");
                    emailSettings.File_Invitation_Confirmation_Patient = ConfigReader.GetValue("File_Invite_Confirmation_Patient_Messaging");
                    emailSettings.File_Invite_Provider = ConfigReader.GetValue("File_Invite_Provider_Messaging");
                }
                else
                {
                    emailSettings.File_Disconnect_Patient = ConfigReader.GetValue("File_Disconnect_Patient");
                    emailSettings.File_Disconnect_Provider = ConfigReader.GetValue("File_Disconnect_Provider");
                    emailSettings.File_Invitation_Confirmation_Patient = ConfigReader.GetValue("File_Invite_Confirmation_Patient");
                    emailSettings.File_Invite_Provider = ConfigReader.GetValue("File_Invite_Provider");
                }


                emailSettings.File_Forgot_Password = ConfigReader.GetValue("File_Forgot_Password");
                emailSettings.File_Forgot_Username = ConfigReader.GetValue("File_Forgot_Username");
                emailSettings.File_Invite_Patient = ConfigReader.GetValue("File_Invite_Patient");
                emailSettings.File_Register_Provider = ConfigReader.GetValue("File_Register_Provider");
                emailSettings.Subject_Disconnect_Patient = ConfigReader.GetValue("Subject_Disconnect_Patient");
                emailSettings.Subject_Disconnect_Provider = ConfigReader.GetValue("Subject_Disconnect_Provider");
                emailSettings.Subject_Forgot_Password = ConfigReader.GetValue("Subject_Forgot_Password");
                emailSettings.Subject_Forgot_Username = ConfigReader.GetValue("Subject_Forgot_Username");
                emailSettings.Subject_Invite_Patient = ConfigReader.GetValue("Subject_Invite_Patient");
                emailSettings.Subject_Invite_Provider = ConfigReader.GetValue("Subject_Invite_Provider");
                emailSettings.Subject_Register_Provider = ConfigReader.GetValue("Subject_Register_Provider");

                emailSettings.File_Invitation_Confirmation_Provider = ConfigReader.GetValue("File_Invite_Confirmation_Provider");

                emailSettings.Subject_Invitation_Confirmation_Patient = ConfigReader.GetValue("Subject_Invite_Confirmation_Patient");
                emailSettings.Subject_Invitation_Confirmation_Provider = ConfigReader.GetValue("Subject_Invite_Confirmation_Provider");

                emailSettings.File_Alert_Provider = ConfigReader.GetValue("File_Alert_Provider");
                emailSettings.Subject_Alert_Provider = ConfigReader.GetValue("Subject_Alert_Provider");

                emailSettings.File_Summary_Email_Provider = ConfigReader.GetValue("File_Summary_Email_Provider");
                emailSettings.Subject_Summary_Email_Provider = ConfigReader.GetValue("Subject_Summary_Email_Provider");

                emailSettings.File_AlertModified = ConfigReader.GetValue("File_AlertModified");
                emailSettings.Subject_AlertModified = ConfigReader.GetValue("Subject_AlertModified");

                emailSettings.File_New_Message_Provider = ConfigReader.GetValue("File_New_Message_Provider");
                emailSettings.Subject_New_Message_Provider = ConfigReader.GetValue("Subject_New_Message_Provider");

                emailSettings.File_New_Message_Patient = ConfigReader.GetValue("File_New_Message_Patient");
                emailSettings.Subject_New_Message_Patient = ConfigReader.GetValue("Subject_New_Message_Patient");

                emailSettings.File_Alert_Message_Patient = ConfigReader.GetValue("File_Alert_Message_Patient");
                emailSettings.Subject_Alert_Message_Patient = ConfigReader.GetValue("Subject_Alert_Message_Patient");

                return emailSettings;
            }
        }

        public static PatientProviderFaxSettings PatientProviderFaxSettingsConfig
        {
            get
            {
                PatientProviderFaxSettings faxSettings = new PatientProviderFaxSettings();

                faxSettings.File_Invitation_To_Patient = ConfigReader.GetValue("File_Invitation_To_Patient");
                faxSettings.File_Invitation_To_Provider = ConfigReader.GetValue("File_Invitation_To_Provider");

                return faxSettings;
            }
        }

        public static FaxSettings FaxSettingsConfig
        {
            get
            {
                FaxSettings faxSettings = new FaxSettings();
                faxSettings.AccountID = ConfigReader.GetValue("Fax_AccountID");
                faxSettings.UserName = ConfigReader.GetValue("Fax_UserName");
                faxSettings.Password = ConfigReader.GetValue("Fax_Password");

                return faxSettings;
            }
        }

        public static ContentExpNotificationEmailSettings ContentExpNotificationEmailSettingsConfig
        {
            get
            {
                ContentExpNotificationEmailSettings contentExpNotificationEmailSettings = new ContentExpNotificationEmailSettings();
                contentExpNotificationEmailSettings.File_Content_Expiration = ConfigReader.GetValue("File_Content_Expiration");
                contentExpNotificationEmailSettings.Subject_Content_Expiration = ConfigReader.GetValue("Subject_Content_Expiration");
                return contentExpNotificationEmailSettings;
            }
        }

        public static InActiveNotificationEmailSettings InActiveProviderNotificationEmailSettingsCofig
        {
            get
            {
                InActiveNotificationEmailSettings inActiveProviderNotificationEmailSettings = new InActiveNotificationEmailSettings();
                inActiveProviderNotificationEmailSettings.File_InActive_Notification = ConfigReader.GetValue("File_InActive_Provider_Notification");
                inActiveProviderNotificationEmailSettings.Subject_InActive_Notification = ConfigReader.GetValue("Subject_InActive_Provider_Notification");

                return inActiveProviderNotificationEmailSettings;
            }
        }

        public static Heart360CampaignEmailProcessSettings Heart360CampaignEmailProcessConfig
        {
            get
            {
                Heart360CampaignEmailProcessSettings settings = new Heart360CampaignEmailProcessSettings();
                settings.File_AppointmentReminder = ConfigReader.GetValue("File_AppointmentReminder");
                settings.File_14Day_Inactive_Patient = ConfigReader.GetValue("File_14Day_Inactive_Patient");
                settings.File_30Day_Inactive_Patient = ConfigReader.GetValue("File_30Day_Inactive_Patient");
                settings.File_BP_Trigger_Excellent = ConfigReader.GetValue("File_BP_Trigger_Excellent");
                settings.File_BP_Trigger_NeedsImprovement = ConfigReader.GetValue("File_BP_Trigger_NeedsImprovement");
                settings.File_BP_Trigger_Warning = ConfigReader.GetValue("File_BP_Trigger_Warning");
                return settings;
            }
        }

        public static InActiveNotificationEmailSettings InActiveUserNotificationEmailSettingsCofig
        {
            get
            {
                InActiveNotificationEmailSettings inActiveUserNotificationEmailSettings = new InActiveNotificationEmailSettings();
                inActiveUserNotificationEmailSettings.File_InActive_Notification = ConfigReader.GetValue("File_InActive_User_Notification");
                inActiveUserNotificationEmailSettings.Subject_InActive_Notification = ConfigReader.GetValue("Subject_InActive_User_Notification");

                return inActiveUserNotificationEmailSettings;
            }
        }
        #endregion
    }
}
