﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;

namespace AHACommon
{
    public class DateTimeHelper
    {
        public static string GetShortDateStringForWebSlash(DateTime dt)
        {
            return dt.ToString(System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
        }

        public static GRCBase.GRCDateFormat GetDateFormatForWeb()
        {
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != Locale.English)
            {
                //return GRCBase.GRCDateFormat.DDMMYYYY;
                return GRCBase.GRCDateFormat.DDMMYY;
            }

            //return GRCBase.GRCDateFormat.MMDDYYYY;
            return GRCBase.GRCDateFormat.MMDDYY;
        }

        public static GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat GetDateFormatForCalendar()
        {
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != Locale.English)
            {
                return GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.DDMMYYYY;
            }

            return GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.MMDDYYYY;
        }

        public static string GetShortDateFormatForWeb()
        {
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != Locale.English)
            {
                return "dd/MM";
            }

            return "MM/dd";
        }

        public static string GetMonthDayDateStringForWeb(int iMonth, int iDay)
        {
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name != Locale.English)
            {
                return string.Format("{0}/{1}", iDay, iMonth);
            }
            else
            {
                return string.Format("{0}/{1}", iMonth, iDay);
            }
        }
    }
}
