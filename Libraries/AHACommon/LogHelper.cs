﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AHACommon
{
    /// <summary>
    /// Summary description for Tracer.
    /// </summary>
    public class LogHelper
    {
        private static bool m_bEnableLogging = true;
        public enum MailLogTraceLevel : int { FATAL = 0, ERROR = 1, INFO = 2, WARNING = 3, TRACE = 4 };

        int m_trace_level = 4; //TRACE


        FileStream m_fileStream;
        System.IO.StreamWriter m_sw;

        public LogHelper(string strProcessName)
        {
            string log_path = string.Format("{0}\\{1}_{2}_{3}.log",
                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                strProcessName, DateTime.Now.ToLongDateString(),
                DateTime.Now.Ticks.ToString());

            m_fileStream = System.IO.File.Open(log_path, System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.Read);

            m_sw = new StreamWriter(m_fileStream);
        }

        public void WriteTrace(MailLogTraceLevel level, string str)
        {
            if (m_bEnableLogging)
            {
                if (m_trace_level >= Convert.ToInt16(level))
                {
                    m_sw.WriteLine(DateTime.Now.ToShortTimeString() + ":" + str);
                    m_sw.Flush();
                }
            }
        }
    }
}
