﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GRCBase;

namespace AHACommon
{
    //
    // This class works in conjunction with MinimumAppSettings
    //
    public class ConfigurableEmailTemplateHelper
    {
        public static string SiteUrlForImage(bool bProcess)
        {
            if (bProcess)
            {
                return MinimumAppSettings.SiteUrlNoHttps;
            }
            else
            {
                if (string.IsNullOrEmpty(System.Web.HttpContext.Current.Request["PrinterFriendly"]))
                {
                    return GRCBase.UrlUtility.ServerRootUrl.Replace("https://", "http://");
                }
                else
                {
                    return GRCBase.UrlUtility.ServerRootUrl;
                }
            }
        }

        public static string GetEmailTemplateDirectory(bool bProcess, string strLocale)
        {
            if (bProcess)
            {
                if (strLocale != AHACommon.Locale.English)
                {
                    return string.Format(MinimumAppSettings.EmailTemplateDirectory + ".{0}", strLocale);
                }
                return MinimumAppSettings.EmailTemplateDirectory ;
            }
            else
            {
                if (strLocale != AHACommon.Locale.English)
                {
                    return System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates.{0}/", strLocale));
                }

                return System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/");
            }
        }

        public static string GetEmailHeader(bool bProcess, string strLocale)
        {
            string strFilePath = string.Format("{0}\\Header.htm", GetEmailTemplateDirectory(bProcess, strLocale));

            string strHeader = IOUtility.ReadHtmlFile(strFilePath);

            strHeader = System.Text.RegularExpressions.Regex.Replace(strHeader, "##EmailAssetUrl##", MinimumAppSettings.EmailAssetUrl);
            

            return strHeader;
        }

        public static string GetEmailFooter(bool bProcess, string strLocale)
        {
            string strFilePath = string.Format("{0}\\Footer.htm", GetEmailTemplateDirectory(bProcess, strLocale));
            string strFooter = IOUtility.ReadHtmlFile(strFilePath);

            Heart360EmailLinks emaillinks = MinimumAppSettings.EmailLinks;

            //strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##SiteUrl##", SiteUrlForImage(bProcess));
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##EmailAssetUrl##", MinimumAppSettings.EmailAssetUrl);

            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##PrivacyPolicy##", emaillinks.PrivacyPolicy);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##Copyright##", emaillinks.Copyright);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##EthicsPolicy##", emaillinks.EthicsPolicy);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##ConflictOfInterest##", emaillinks.ConflictOfInterest);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##LinkingPolicy##", emaillinks.LinkingPolicy);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##Diversity##", emaillinks.Diversity);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##Careers##", emaillinks.Careers);

            // PJB: Anything application dependent that is not configurable via MinimumAppSettings has to be done at a higher level!
            /**
            if (!bProcess)
            {
                if (bIsForProvider)
                {
                    strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##DISCLAIMER##", LanguageResourceHelper.Disclaimer_Provider_HTML);
                }
                else
                {
                    strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##DISCLAIMER##", LanguageResourceHelper.Disclaimer_Patient_HTML);
                }
            }
            **/

            return strFooter;
        }

        public static string GetCompleteHtmlForEmail(bool bProcess, string strContent, string strLocale)
        {
            // strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##SiteUrl##", SiteUrlForImage(bProcess));
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##EmailAssetUrl##", MinimumAppSettings.EmailAssetUrl);

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{0}{1}{2}", GetEmailHeader(bProcess, strLocale), strContent, GetEmailFooter(bProcess, strLocale));

            return sb.ToString();
        }

            // PJB: this should be called after GetCompleteHtmlForEmail()
            // This is architected this way because different applications can have different disclaimers
        public static string HtmlEmailSetDisclaimer(string strContent, string strDisclaimer)
        {
            return System.Text.RegularExpressions.Regex.Replace(strContent, "##DISCLAIMER##", strDisclaimer);
        }
    }
}
