﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Web.SessionState;

namespace AHACommon
{
    //
    // This is a class to define all the custom Session State keys for Provider and Patient Portals.
    //
    public class SessionKeys
    {
            // Patient Portal
        public const string sessionKeyUserIdentityContext = "UserIdentityContext";
        public const string sessionKeyUserPatientInvitation = "PatientInvitation";
        public const string sessionKeyBPCenterItem = "BPCenterItem";
        public const string sessionKeyMLCInfo = "MLCInfo";
        public const string sessionKeyThirdParty = "ThirdPartyContext"; 
            // Provider Portal
        public const string sessionKeyProviderIdentityContext = "ProviderIdentityContext";
        public const string sessionKeyPatientImageList = "PatientImageList";

        public const string sessionKeyCampaign = "Heart360Campaign";
        public const String sessionKeyCampaignUrl = "Heart360CampaignUrl";
        public const string sessionKeyProviderCurrentPatient = "ProviderCurrentPatient";
        public const string sessionKeyPatientInvitationId = "Heart360PatientInvitationId";

        public static bool DoesSessionKeyHaveValue( string sessionKey )
        {
            return (HttpContext.Current.Session != null && HttpContext.Current.Session[sessionKey] != null) ? true : false;
        }
    }
}
