﻿using System;
using System.Collections.Generic;
using System.Text;

using GRCBase;

//
// PJB: This class represents the mininum configuration settings for a Heart360 application,
// be it a web app/site, web service, or scheduled task. This is a work in progress,
// first put together so that the scheduled tasks can have a simple AHA configuration file and
// not reference the one in the patient portal.
//

namespace AHACommon
{
    /// <summary>
    /// This struct declares the variables required for Heart360 Email Links for header, footer and/or generic content
    /// </summary>
    public struct Heart360EmailLinks
    {
        public string PrivacyPolicy;
        public string Copyright;
        public string EthicsPolicy;
        public string ConflictOfInterest;
        public string LinkingPolicy;
        public string Diversity;
        public string Careers;
    }

    public class MinimumAppSettings
    {
        /// <summary>
        /// SiteUrl - this is historical and too ingrained.
        /// </summary>
        public static string SiteUrl
        {
            get
            {
                return ConfigReader.GetValue("SiteUrl");
            }
        }

        /// <summary>
        /// SiteUrlNoHttps
        /// </summary>
        public static string SiteUrlNoHttps
        {
            get
            {
                return ConfigReader.GetValue("SiteUrl").Replace("https://", "http://");
            }
        }

        /// <summary>
        /// SystemEmail - the email address used as the sender for all system generated email
        /// </summary>
        public static string SystemEmail
        {
            get
            {
                return ConfigReader.GetValue("SystemEmail");
            }
        }

        public static string EmailTemplateDirectory
        {
            get
            {
                return ConfigReader.GetValue("EmailTemplateDirectory");
            }
        }

        /// <summary>
        /// EmailAssetUrl - publically available url of (graphic) assets for all emails
        /// </summary>
        public static string EmailAssetUrl
        {
            get
            {
                return ConfigReader.GetValue("EmailAssetUrl");
            }
        }

        /// <summary>
        /// This function is used to get the information about which is the SMTPServer to be used.
        /// </summary>
        public static string SMTPServer
        {
            get
            {
                return ConfigReader.GetValue("SMTPServer");
            }
        }


        /// <summary>
        /// This function is used to get the links corresponding to the Heart360 Email
        /// </summary>
        public static Heart360EmailLinks EmailLinks
        {
            get
            {
                Heart360EmailLinks emaillinks = new Heart360EmailLinks();
                emaillinks.PrivacyPolicy = ConfigReader.GetValue("url.H360PrivacyPolicy");
                emaillinks.Copyright = ConfigReader.GetValue("url.H360Copyright");
                emaillinks.EthicsPolicy = ConfigReader.GetValue("url.H360EthicsPolicy");
                emaillinks.ConflictOfInterest = ConfigReader.GetValue("url.H360ConflictOfInterest");
                emaillinks.LinkingPolicy = ConfigReader.GetValue("url.H360LinkingPolicy");
                emaillinks.Diversity = ConfigReader.GetValue("url.H360Diversity");
                emaillinks.Careers = ConfigReader.GetValue("url.H360Careers");
                return emaillinks;
            }
        }
    }
}
