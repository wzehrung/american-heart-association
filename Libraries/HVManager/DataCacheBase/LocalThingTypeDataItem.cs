﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;

    /// <summary>
    /// This class serves as the base-type from which locally cached MSHealth
    /// data Things are derived from.  The class devices basic data that 
    /// all cached Things needs to have such as:
    ///   * The ID (Guid) of the Thing.  (needed if we want to edit the Thing)
    ///   * The Effective and LastUpdated dates (useful for sorting)
    ///   * Any MSHealth Transforms that were requested when the data was queried 
    ///     from MSHealth.  (MSHealth can perfom useful transforms such as pre-generating
    ///     HTML text that can be used to show the Thing's data in a web browser)
    /// 
    /// This class is closely assoicated with the 'LocalThingTypeDataManager' class, which
    /// acts a a container class for holding date-range sets of this class.
    /// </summary>
    /// 
    [Serializable]
    public abstract class LocalThingTypeDataItem
    {
        public LocalThingTypeDataItem()
        {
        }
        /// <summary>
        /// Extended by GRC.
        /// If there is anything mandatory that this application requires on an item
        /// and cant handle this item type generically (since there will be instances of item
        /// created by other applications), override this method in derived class
        /// </summary>
        /// <returns>True if this item acceptable to be processed by current application</returns>
        public virtual bool IsItemAcceptableByCurrentApplication()
        {
            return true;
        }

        public Guid PersonGUID
        {
            get;
            set;
        }

        public Guid RecordGUID
        {
            get;
            set;
        }

        /// <summary>
        /// Takes a look at items returns the one with the most current date.
        /// This is commonly used to compare items in 2 parallel linked lists
        /// to see which item should be displayed first by chronological ordering
        /// </summary>
        /// <param name="itemXYZ"></param>
        /// <param name="itemABC"></param>
        /// <returns></returns>
        public static LocalThingTypeDataItem ChooseMostRecentItem(
                    LocalThingTypeDataItem itemXYZ,
                    LocalThingTypeDataItem itemABC)
        {
            //If we have no more itemXYZ, its a 1 horse race
            if (itemXYZ == null)
            {
                //We have no data at all....
                if (itemABC == null) return null;

                //Return the current health incident item
                return itemABC;
            }

            //If we have only itemXYZ, or
            //We have both ABC and XYZ; return the most current item
            if ((itemABC == null) || ((itemXYZ.EffectiveDate >= itemABC.EffectiveDate)))
            {
                return itemXYZ;
            }

            return itemABC;
        }

        /// <summary>
        /// The key that identifies both the item and its version stamp
        /// </summary>
        public HealthRecordItemKey ThingKey
        {
            get
            {
                if (ThingID == Guid.Empty && VersionStamp == Guid.Empty)
                    return null;

                return new HealthRecordItemKey(ThingID, VersionStamp);
            }
        }

        /// <summary>
        /// Unique ID with which the data is stored in the Health Server
        /// </summary>
        /// 

        Guid _thingID;
        public Guid ThingID
        {
            get
            {
                return _thingID;
            }
        }


        Guid _versionStamp;
        public Guid VersionStamp
        {
            get
            {
                return _versionStamp;
            }
        }



        /// <summary>
        /// What was the timestamp of the last update.  This is useful
        /// to know we end up with two items with the same matching GUID
        /// </summary>
        public readonly Nullable<DateTime> LastUpdated;

        public readonly Nullable<DateTime> DateCreated;

        /// <summary>
        /// The date/time that this data item is referring to
        /// </summary>
        public DateTime EffectiveDate;

        /// <summary>
        /// The source of the data (typically the brand name where the data came from)
        /// </summary>
        public PossibleUpdate<string> Source;

        /// <summary>
        /// Details about the data (typically the model name if it is a device)
        /// </summary>
        public string SourceDetails;

        /// <summary>
        /// Text notes associated with this item
        /// </summary>
        public PossibleUpdate<string> Note;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="thingID"></param>
        /// <param name="effectiveDate"></param>
        /// <param name="lastUpdated"></param>
        public LocalThingTypeDataItem(HealthRecordItemKey itemKey, DateTime effectiveDate, Nullable<DateTime> lastUpdated, string source, string sourceDetails, string notes)
        {

            if (itemKey.Id == Guid.Empty)
            {
                throw new ArgumentException("Key.Id of an item cannot be empty");
            }

            if (itemKey.VersionStamp == Guid.Empty)
            {
                throw new ArgumentException("Key.VersionStamp of an item cannot be empty");
            }



            this._thingID = itemKey.Id;
            this._versionStamp = itemKey.VersionStamp;
            this.EffectiveDate = effectiveDate;
            this.LastUpdated = lastUpdated;
            this.Source = new PossibleUpdate<string>(source);
            this.SourceDetails = sourceDetails;
            this.Note = new PossibleUpdate<string>(notes);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="thingID"></param>
        /// <param name="effectiveDate"></param>
        /// <param name="lastUpdated"></param>
        public LocalThingTypeDataItem(Guid gPersonGUID, Guid gRecordGUID, HealthRecordItem item)
        {
            PersonGUID = gPersonGUID;
            RecordGUID = gRecordGUID;

            if (item.Key.Id == Guid.Empty)
            {
                throw new ArgumentException("Key.Id of an item cannot be empty");
            }

            if (item.Key.VersionStamp == Guid.Empty)
            {
                throw new ArgumentException("Key.VersionStamp of an item cannot be empty");
            }


            this._thingID = item.Key.Id;
            this._versionStamp = item.Key.VersionStamp;


            this.EffectiveDate = item.EffectiveDate;

            if (item.LastUpdated != null)
            {
                this.LastUpdated = item.LastUpdated.Timestamp;
            }

            if (item.Created != null)
            {
                this.DateCreated = item.Created.Timestamp;
            }

            this.Source = new PossibleUpdate<string>(item.CommonData.Source);
            this.SourceDetails = "";
            this.Note = new PossibleUpdate<string>(item.CommonData.Note);
        }
    }
}