﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using AHACommon;
    using Microsoft.Health;

    /// <summary>
    /// Class that serves as the generic/abstract base for local data caching classes.  
    /// It provides the common functionality necessary for workign with date-range
    /// windows of Thing data of a specific type (T).
    /// 
    /// It acts as a container of date-ordered lists of 'LocalThingTypeDataItem' objects.
    /// </summary>
    /// 

    [Serializable]
    public abstract class LocalThingTypeDataManager<T> : IDataCacheManagerBase where T : LocalThingTypeDataItem
    {
        public LocalThingTypeDataManager(Guid hvpersonid, Guid hvrecordid)
        {
            PersonGUID = hvpersonid;
            RecordGUID = hvrecordid;
        }

        public abstract HVDataTypeFilter GetHVFilter();

        public Guid PersonGUID
        {
            get;
            set;
        }

        public Guid RecordGUID
        {
            get;
            set;
        }

        [Serializable]
        private class CacheOverflowLatch
        {
            private bool _value;

            public bool Value
            {
                get { return _value; }
            }

            public void Reset()
            {
                _value = false;
            }

            public void Set()
            {
                _value = true;
            }
        }

        /// <summary>
        /// Derrived classes must implement the function that performs a server query for data
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxItemsToReturn">The maximum # of results we will allow to be returned</param>
        /// <param name="didResultsOverflowCache">returns TRUE if the results exceeded the maximimum number allowed</param>
        /// <returns></returns>
        protected abstract List<T> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflowCache);

        /// <summary>
        /// 5 items/day x 90 days = 450 items... 500 seems like a generous cache size
        /// for most items.  Larger sized items may want to trim down the allowable maximum
        /// </summary>
        public const int SuggestedMaxCacheSize = 5000;
        public abstract int MaxSizeOfCache
        {
            get;
        }

        private CacheOverflowLatch m_cacheOverflowLatch = new CacheOverflowLatch();
        /// <summary>
        /// True if the results of the last query could not fit in the cache
        /// </summary>
        public bool CacheOverflowing
        {
            get
            {
                return m_cacheOverflowLatch.Value;
            }
        }

        /// <summary>
        /// This function is useful when working with multiple parallel lists 
        /// of DataItems.  A list node is passed in, if the node is NULL (e.g. the last item of the list)
        /// NULL is returned.  If the node is non-NULL, it returns the value of the item
        /// </summary>
        /// <param name="listItem"></param>
        /// <returns></returns>
        public static LocalThingTypeDataItem GetListItemOrNull(LinkedListNode<T> listItem)
        {
            if (listItem == null) return null;
            return listItem.Value;
        }


        /// <summary>
        /// Returns the final item in the cache (the one with the latest date)
        /// Returns NULL if the cache is empty
        /// </summary>
        public LocalThingTypeDataItem LatestItemInCache
        {
            get
            {
                if ((m_ascendingListOfItems == null) || (m_ascendingListOfItems.Count == 0))
                {
                    return null;
                }

                return m_ascendingListOfItems.Last.Value;
            }
        }

        /// <summary>
        /// Function to get the end-date for GetDataForLastNNNNNDays functions.
        /// </summary>
        /// <param name="allowInfiniteFuture">
        /// if TRUE, we will include any data in the future.  (useful for time-zone flexibility)
        /// if FALSE, we will allow only data from tommorrow.
        /// </param>
        /// <returns></returns>
        private DateTime EndDateForLastNItemsQuery(bool allowInfiniteFuture)
        {
            if (allowInfiniteFuture)
            {
                return DateTime.MaxValue;
            }

            return DateTime.Now.Date.AddDays(1);
        }

        public LinkedList<T> GetDataBetweenDates(DataDateReturnType dataDateReturnType)
        {
            if (dataDateReturnType.CompareTo(DataDateReturnType.LastWeek) == 0)
            {
                return GetDataForLast7Days();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.LastMonth) == 0)
            {
                return GetDataForLast30Days();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.Last60Days) == 0)
            {
                return GetDataForLast60Days();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.Last3Months) == 0)
            {
                return GetDataForLast90Days();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.Last6Months) == 0)
            {
                return GetDataForLast180Days();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.Last9Months) == 0)
            {
                return GetDataForLast270Days();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.LastYear) == 0)
            {
                return GetDataForLast1Year();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.Last2Years) == 0)
            {
                return GetDataForLast2Year();
            }
            else if (dataDateReturnType.CompareTo(DataDateReturnType.AllData) == 0)
            {
                return GetDataBetweenDates(DateTime.MinValue, DateTime.Now.AddDays(1));
            }
            else
            {
                return GetDataForLast30Days();
            }
        }

        public LinkedList<T> GetDataForLast30Days(bool allowInfiniteFuture)
        {
            //Go back 30 days from today
            DateTime startDate = DateTime.Today.AddDays(-30);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast30Days()
        {
            return GetDataForLast30Days(true);
        }

        public LinkedList<T> GetDataForLast60Days(bool allowInfiniteFuture)
        {
            //Go back 60 days from today
            DateTime startDate = DateTime.Today.AddDays(-60);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast60Days()
        {
            return GetDataForLast60Days(true);
        }

        public LinkedList<T> GetDataForLast90Days(bool allowInfiniteFuture)
        {
            //Go back 90 days from today
            DateTime startDate = DateTime.Today.AddDays(-90);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast90Days()
        {
            return GetDataForLast90Days(true);
        }

        public LinkedList<T> GetDataForLast180Days(bool allowInfiniteFuture)
        {
            //Go back 90 days from today
            DateTime startDate = DateTime.Today.AddDays(-180);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast180Days()
        {
            return GetDataForLast180Days(true);
        }


        public LinkedList<T> GetDataForLast270Days(bool allowInfiniteFuture)
        {
            //Go back 90 days from today
            DateTime startDate = DateTime.Today.AddDays(-270);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast270Days()
        {
            return GetDataForLast270Days(true);
        }

        public LinkedList<T> GetDataForLast1Year(bool allowInfiniteFuture)
        {
            //Go back 90 days from today
            DateTime startDate = DateTime.Today.AddYears(-1);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast1Year()
        {
            return GetDataForLast1Year(true);
        }

        public LinkedList<T> GetDataForLast2Year(bool allowInfiniteFuture)
        {
            //Go back 90 days from today
            DateTime startDate = DateTime.Today.AddYears(-2);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast2Year()
        {
            return GetDataForLast2Year(true);
        }

        public LinkedList<T> GetDataForLast7Days(bool allowInfiniteFuture)
        {
            //Go back 90 days from today
            DateTime startDate = DateTime.Today.AddDays(-7);

            DateTime endDate = EndDateForLastNItemsQuery(allowInfiniteFuture);

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }

        public LinkedList<T> GetDataForLast7Days()
        {
            return GetDataForLast7Days(true);
        }

        /// <summary>
        /// Gets the data surrounding a date; excludes dates past the end of today
        /// </summary>
        /// <param name="centerDate"></param>
        /// <param name="numberDays"></param>
        /// <returns></returns>
        public LinkedList<T> GetDataSurroundingDate(DateTime centerDate, int numberDays)
        {
            return GetDataSurroundingDate(centerDate, numberDays, false);
        }

        /// <summary>
        /// Gets the data for 'numberDays' surrounding this date
        /// </summary>
        /// <param name="centerDate"></param>
        /// <param name="numberDays"></param>
        /// <param name="includeFuture"></param>
        /// <returns></returns>
        public LinkedList<T> GetDataSurroundingDate(DateTime centerDate, int numberDays, bool includeFuture)
        {
            //Can not have a negative # of days
            if (numberDays < 1)
            {
                throw new ArgumentException();
            }

            //Go to the beginning of day, and add the # of full days past this dat
            DateTime endDate = centerDate.Date.AddDays(numberDays + 1);
            //Go back N days from the beginning of this day
            DateTime startDate = centerDate.Date.AddDays(0 - numberDays);

            //If we are not intending to include the future, bound the date range the beginning
            //of tommorrow
            if (!includeFuture)
            {
                DateTime beginningOfTommorrow = DateTime.Now.Date.AddDays(1);
                if (endDate > beginningOfTommorrow)
                {
                    endDate = beginningOfTommorrow;
                }

                if (startDate > beginningOfTommorrow)
                {
                    startDate = beginningOfTommorrow;
                }
            }

            //Fetch the data
            return GetDataBetweenDates(startDate, endDate);
        }


        /// <summary>
        /// Returns the items that fall between two dates, queries for items if
        /// necessary.
        /// </summary>
        /// <param name="mshealth"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public LinkedList<T> GetDataBetweenDates(DateTime startDate, DateTime endDate)
        {
            return GetDataBetweenDates(startDate, endDate, true);
        }


        /// <summary>
        /// Returns the Thing associated with a specific Thing ID; if it exists in the 
        /// users Record
        /// </summary>
        /// <param name="thingId"></param>
        /// <returns></returns>
        public T GetDataWithThingId(Guid thingId)
        {
            //1st, see if its in the cache
            T aItem = this.SeekThingInsideCacheWindow(thingId);

            //If we found it in memory; return it
            if (aItem != null)
            {
                return aItem;
            }

            //Package up a request to seek this Thing
            Collection<Guid> thingIdsToSeek = new Collection<Guid>();
            thingIdsToSeek.Add(thingId);

            //===================================================
            //Call the server to look for the item
            //===================================================
            bool placeholderBool;
            List<T> matchingItems =
            QueryForThings(
                    null, null, //No date boundaries
                    thingIdsToSeek,
                    1,
                    false,
                    out placeholderBool);

            //No items == the item is not in the user's record
            if (matchingItems.Count == 0)
            {
                return null;
            }

            //If would be very odd to get back more then one result
            //with the same guid
            System.Diagnostics.Debug.Assert(matchingItems.Count == 1);

            T item = matchingItems[0];

            //If the data falls within the boundaries of our current cache,
            //we should add the item to the cache (it is new data that 
            //was added to MSHealth since we last checked)
            if (this.IsDateInsideCacheWindow(item.EffectiveDate))
            {
                int numItemsReplaced;
                this.AddItem(item, out numItemsReplaced);
            }

            return item;
        }


        /// <summary>
        /// Removes all the items from "1 end of the list" until the specific item.  
        /// Which end of the list we remove from is chosen based on which end the
        /// item is closest to.  (We remove the smaller set, either before or after 
        /// the cleaving point set by this item, including the item).
        /// 
        /// This is useful to do when we know that a specific item has become invalid
        /// and we do not want to necessarily re-querry for the item and replace it right away.
        /// Since we cannot simply remove the item (it would be missing data), we 
        /// can instead shrink the size of our cache to not contain the item.  
        /// If we happen to re-query for the item later, it will just get added to 
        /// cache again, in the correct spot
        /// 
        /// </summary>
        /// <param name="idOfThingToRemove"></param>
        /// <returns></returns>
        public bool RemoveSmallerPartitionContainingItem(Guid idOfThingToRemove)
        {
            LinkedListNode<T> listNode = m_ascendingListOfItems.First;
            int itemCount = 0;
            while (listNode != null)
            {
                itemCount++;

                //See if this is the item we want to remove
                if (listNode.Value.ThingID == idOfThingToRemove)
                {

                    //See if we are in the 1st half or the second half of the list        
                    if (itemCount < (m_ascendingListOfItems.Count / 2))
                    {
                        //We are before the half way point, we want to remove all teh items
                        //from the beginning of the list until this one
                        while (m_ascendingListOfItems.First != listNode)
                        {
                            m_ascendingListOfItems.RemoveFirst();
                        }
                        //Now remove this one
                        m_ascendingListOfItems.RemoveFirst();
                    }
                    else
                    {
                        //We are past the half way point, we want to remove all the items from
                        //the end of the list until this one
                        while (m_ascendingListOfItems.Last != listNode)
                        {
                            m_ascendingListOfItems.RemoveLast();
                        }
                        //No remove this one
                        m_ascendingListOfItems.RemoveLast();
                    }

                    //Figure out the new date bounds of the dataset
                    helper_setTimeWindowFromListData();
                    return true;
                }

                //Move onto the next node in our search for node with 
                //a matching ThingId
                listNode = listNode.Next;
            }


            return false;  //We did not find the item
        }

        /// <summary>
        /// Looks in our set of data and sets the date ranges appropriatly
        /// </summary>
        private void helper_setTimeWindowFromListData()
        {
            if ((m_ascendingListOfItems == null) || (m_ascendingListOfItems.Count == 0))
            {
                m_earliestDate = null;
                m_latestDate = null;
            }

            m_earliestDate = m_ascendingListOfItems.First.Value.EffectiveDate;
            m_latestDate = m_ascendingListOfItems.Last.Value.EffectiveDate;
        }

        /// <summary>
        /// Removes an item with a specific ThingID, if it exists in
        /// the list
        /// </summary>
        /// <param name="idOfThingToRemove"></param>
        /// <returns>TRUE if the item was found and removed, FALSE if the item
        /// was not in the list</returns>
        public bool RemoveItem(Guid idOfThingToRemove)
        {
            LinkedListNode<T> listNode = m_ascendingListOfItems.First;
            while (listNode != null)
            {
                //If we found the item, remove it
                if (listNode.Value.ThingID == idOfThingToRemove)
                {
                    //Remove the item from the list
                    m_ascendingListOfItems.Remove(listNode);
                    return true;  // Indicate that we removed the item
                }

                //Move onward in the lust
                listNode = listNode.Next;
            }

            return false; //We never found the item to remove
        }

        public void DeleteItem(HealthRecordItemKey itemKey)
        {
            WCQueries.DeleteThing(Helper.GetAccessor(PersonGUID, RecordGUID), itemKey);
            RemoveItem(itemKey.Id);
        }

        /// <summary>
        /// Returns up to N items with the most current dates.
        /// The returned list moves in ascending date order
        /// </summary>
        /// <param name="numberItemsRequested"></param>
        /// <returns></returns>
        public LinkedList<T> GetLatestNItems(int numberItemsRequested)
        {
            LinkedList<T> resultsList = new LinkedList<T>();
            this.FetchMostRecentNItemsIfNeeded(numberItemsRequested);


            //Start with the last item
            LinkedListNode<T> nodeInCache = m_ascendingListOfItems.Last;

            //As long as we've got items to add to the cache...fill-em up!
            while ((nodeInCache != null) && (resultsList.Count < numberItemsRequested))
            {
                //Keep adding the items to the beginning of the list; since
                //each successive item we look at is earlier in time
                T thisDataItem = nodeInCache.Value;
                resultsList.AddFirst(thisDataItem);

                //move back in time 
                nodeInCache = nodeInCache.Previous;
            }

            //Return the items we've queried for
            return resultsList;
        }

        /// <summary>
        /// Returns the items that fall between two dates, queries for items if
        /// necessary.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public LinkedList<T> GetDataBetweenDates(DateTime startDate, DateTime endDate, bool includeAllEndDay)
        {
            LinkedList<T> resultsList = new LinkedList<T>();

            //If we want to include all of the end-date, we want to make sure we fetch data up until
            //the end of the day
            DateTime fetchData_endDate = endDate;
            if (includeAllEndDay)
            {
                //As long as the end-date is not already within 1 day of 'max-day'
                if (endDate < DateTime.MaxValue.AddDays(-1))
                {

                    DateTime dayAfterEndDay = endDate.Date.AddDays(1); //Go to the beginning of the next day
                    DateTime endOfEndDay = dayAfterEndDay.AddMilliseconds(-1); //Go back a millisecond

                    if (fetchData_endDate < endOfEndDay)
                    {
                        fetchData_endDate = endOfEndDay;
                    }
                    else
                    {
                        //Possible edge clase...the 'endDate' might be "closer than 1ms" to the end of today
                        //in which case we'll keep it as is.
                        //(this could only happen if the resolution of DateTime is smaller than 1 milliseconds)
                    }
                }
                else //End-Date is already within 1 day of MaxValue, so we cannot add a full day
                {
                    fetchData_endDate = DateTime.MaxValue;
                }
            }

            //Fetch data if we need it
            this.FetchDataIfNeeded(startDate, fetchData_endDate);

            //Start with the 1st item
            LinkedListNode<T> nodeInCache = m_ascendingListOfItems.First;

            //Move until we get up to the start data
            while ((nodeInCache != null) && (nodeInCache.Value.EffectiveDate < startDate))
            {
                nodeInCache = nodeInCache.Next;
            }

            while (nodeInCache != null)
            {
                T thisDataItem = nodeInCache.Value;

                //If the date (ignoring time of day) is greater than the 
                //end date, then stop the loop
                if (thisDataItem.EffectiveDate.Date > endDate.Date)
                {
                    nodeInCache = null;
                }
                else if (!thisDataItem.IsItemAcceptableByCurrentApplication())
                {
                    //nodeInCache = null;
                    //since we don't want to include this in our application, go to the next node
                    nodeInCache = nodeInCache.Next;
                }
                else if ((!includeAllEndDay) && (thisDataItem.EffectiveDate > endDate.Date))
                {
                    //We don't want to include all of the end day, and the date is greater 
                    //than the posted end date
                    nodeInCache = null;
                }
                else
                {
                    //The item is inside the range we want, so use it.

                    //Add the item to our results list
                    resultsList.AddLast(nodeInCache.Value);

                    //move to next
                    nodeInCache = nodeInCache.Next;
                }
            }

            //Return the results list
            return resultsList;
        }

        protected LinkedList<T> m_ascendingListOfItems = new LinkedList<T>();

        /// <summary>
        /// The total count of the # of times we have had to hit the server for data
        /// </summary>
        protected int m_numberServerQueriesRun;

        public int NumberServerQueriesRun
        {
            get
            {
                return m_numberServerQueriesRun;
            }
            set
            {
                m_numberServerQueriesRun = value;
            }
        }

        /// <summary>
        /// How many times has the data cache for this item been
        /// flushed?
        /// </summary>
        private int m_numberCacheFlushes;


        /// <summary>
        /// How many times has the data cache for this item been
        /// flushed?
        /// </summary>
        public int NumberCacheFlushes
        {
            get
            {
                return m_numberCacheFlushes;
            }
        }



        /// <summary>
        /// Returns the number of items currently in the cache
        /// </summary>
        public int Count
        {
            get
            {
                if (m_ascendingListOfItems == null) return 0;
                return m_ascendingListOfItems.Count;
            }
        }




        private T RemoveOldestListItem()
        {
            if (m_ascendingListOfItems.Count == 0) return null;
            T item = m_ascendingListOfItems.First.Value;
            //Remove the oldest date in the list (the list moves forward in time)
            m_ascendingListOfItems.RemoveFirst();

            return item;
        }

        private T RemoveNewestListItem()
        {
            if (m_ascendingListOfItems.Count == 0) return null;
            T item = m_ascendingListOfItems.Last.Value;
            //Remove the oldest date in the list (the list moves forward in time)
            m_ascendingListOfItems.RemoveLast();

            return item;
        }

        /// <summary>
        /// Removes the "least important" element in the linked list.
        /// 
        /// If no parameters are specified; we remove the oldest item (since newer data is more useful)
        /// 
        /// If 'favor items before/after' parameters are passed we try to accomodate these criteria by
        /// seeing if an acceptable item  to remove exists
        /// 
        /// </summary>
        /// <param name="favorItemsAfterThisDate"></param>
        /// <param name="favorItemsBeforeThisDate"></param>
        private T helper_RemoveLeastSignicantElement(DateTime? favorItemsAfterThisDate, DateTime? favorItemsBeforeThisDate)
        {
            //Empty list
            if (m_ascendingListOfItems.Count == 0) return null;

            //Since there are no favorites; remove the oldest item
            if ((favorItemsAfterThisDate == null) && (favorItemsBeforeThisDate == null))
            {
                return RemoveOldestListItem();
            }

            //If we have a suggested oldest-time; see if our oldest item falls before it
            if (favorItemsAfterThisDate != null)
            {
                //Sanity check to make sure our dates are in the proper order
                System.Diagnostics.Debug.Assert(
                    (favorItemsBeforeThisDate == null) ||
                    (favorItemsAfterThisDate.Value <= favorItemsBeforeThisDate.Value));

                //If the 1st item is older than our lower bound; we can discard it from the cache
                if (m_ascendingListOfItems.First.Value.EffectiveDate < favorItemsAfterThisDate.Value)
                {
                    return RemoveOldestListItem();
                }
            }

            //=====================================================================================
            //Now we know that the 1st (oldest) item in the list is within our suggested 'things we want to keep'  window...
            //=====================================================================================

            //If we have a suggested newest-time upper boundary; see if the newest (most future) item in the list falls after this date
            if (favorItemsBeforeThisDate != null)
            {
                if (m_ascendingListOfItems.Last.Value.EffectiveDate > favorItemsBeforeThisDate.Value)
                {
                    return RemoveNewestListItem();
                }
            }
            else
            {
                //We have no favored upper bound... remove the newest item
                return RemoveNewestListItem();
            }

            //=====================================================================================
            //Now we know that the last (newest) item in the list is within our suggested 'things we want to keep' window...
            //=====================================================================================
            //We have to remove something... so it's going to be the oldest item
            return RemoveOldestListItem();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="favorItemsAfterThisDate"></param>
        /// <param name="favorItemsBeforeThisDate"></param>
        /// <returns></returns>
        private bool isItemWithinRange(T item, DateTime? lowerBound, DateTime? upperBound)
        {
            //There is no range... so dont' worry about it.
            if ((upperBound == null) && (upperBound == null))
            {
                return false;
            }

            //There is no lower bound date...
            if (lowerBound == null)
            {
                //If the item is on or below the upper bound date...return true
                if (item.EffectiveDate <= upperBound.Value) return true;

                return false;
            }

            //No upper bound date...
            if (upperBound == null)
            {
                //If the item is on or after the lower bound date...return true
                if (item.EffectiveDate >= lowerBound.Value) return true;

                return false;
            }

            //=================================================
            //We have both upper and lower bounds
            //=================================================

            //If the item is 
            if (item.EffectiveDate < lowerBound.Value) return false;
            if (item.EffectiveDate > upperBound.Value) return false;

            //The item is inside the date range
            return true;
        }


        /// <summary>
        /// Enforces the cache size.  Removes items from either the front or end of the list if needed to 
        /// reduce the cache size
        /// </summary>
        /// <param name="favorItemsAfterThisDate"></param>
        /// <param name="favorItemsBeforeThisDate"></param>
        private void EnforceMaxCacheSize(DateTime? favorItemsAfterThisDate, DateTime? favorItemsBeforeThisDate)
        {
            while (m_ascendingListOfItems.Count > this.MaxSizeOfCache)
            {
                T itemRemoved = helper_RemoveLeastSignicantElement(favorItemsAfterThisDate, favorItemsBeforeThisDate);

                //If the item we removed fell within the ranges specified, we should indicate
                //that we had a cache overflow
                if (isItemWithinRange(itemRemoved, favorItemsAfterThisDate, favorItemsBeforeThisDate))
                {
                    m_cacheOverflowLatch.Set();
                }

            } //end while
        }

        public void AddItem(T item, TimeSpan maxDeltaBeforeFlushCache)
        {
            AddItem(item, maxDeltaBeforeFlushCache, null, null);
        }

        /// <summary>
        /// Adds an item to the existing cache.  If the item falls within
        /// the time boundaries that we've already queried for, the item
        /// is always added to the cache.
        /// 
        /// If the time-stamp of the item falls outside the boundaries we have 
        /// queried for then we need to make a decision whether we need to throw
        /// out the existing cache (since we don't know what data we're missing
        /// inbetween our new item and the old one) or to simply extend the window
        /// of our data-cache and assume that there is no data we care about in 
        /// the gap.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="maxDeltaBeforeFlushCache"></param>
        public void AddItem(T item, TimeSpan maxDeltaBeforeFlushCache, DateTime? queryStart, DateTime? queryEnd)
        {
            int numberItemsReplaced;

            //If we have no data, or the new item falls within the date window we are dealing with
            //then add the item.
            if ((m_earliestDate == null) ||
               ((item.EffectiveDate >= m_earliestDate.Value) && (item.EffectiveDate <= m_latestDate.Value)))
            {
                AddItem(item, out numberItemsReplaced, queryStart, queryEnd);
                return;
            }

            //------------------------------------------------------------------------
            //The new item falls outside the date window we currently have
            //------------------------------------------------------------------------

            //How far outside the date window?
            TimeSpan positiveTimeDelta;
            if (item.EffectiveDate <= m_earliestDate.Value)
            {
                positiveTimeDelta = m_earliestDate.Value - item.EffectiveDate;
            }
            else
            {
                positiveTimeDelta = item.EffectiveDate - m_latestDate.Value;
            }

            //Make sure we did not screw up on the math... out time delta should always be > 0
            System.Diagnostics.Debug.Assert(positiveTimeDelta.TotalDays > 0);

            //We fall outside the allowable time range, and believe
            //there may be data in the time-gap that we care about
            //flush the cache
            if (positiveTimeDelta > maxDeltaBeforeFlushCache)
            {
                FlushCache();
            }

            //Add the new item to the cache
            AddItem(item, out numberItemsReplaced, queryStart, queryEnd);
        }

        protected void AddItem(T item, out int numberItemsReplaced)
        {
            AddItem(item, out numberItemsReplaced, null, null);
        }


        /// <summary>
        /// If our existing date windows do not include the supplied item's date,
        /// expand hte windows
        /// </summary>
        /// <param name="expandToIncludeDate"></param>
        private void ExpandCacheWindowTimesAsNeeded(DateTime expandToIncludeDate)
        {
            //If we have no date-window, or the window does not fit the date, expand the window
            if ((m_latestDate == null) || (expandToIncludeDate > m_latestDate.Value))
            {
                m_latestDate = expandToIncludeDate;
            }

            //If we have no date-window, or the window does not fit the date, expand the window
            if ((m_earliestDate == null) || (expandToIncludeDate < m_earliestDate.Value))
            {
                m_earliestDate = expandToIncludeDate;
            }
        }

        // <param name="item"></param>
        /// <summary>
        /// Adds an item to the set.
        /// NOTE: This is not intended to fully cover the case of data that has changed
        //  dates.  (If the Thing was indeed changed, and the new date is later 
        //  then its previous date, we will remove the older item.  However if 
        //  an item changes and the new item occurs BEFORE the existing instance
        //  we will end up with 2 copies.  This is "by design" because we don't 
        //  want to check the whole list each item an item is added.  If the caller
        //  wants to be sure of fresh data they can Flush() the cache before the call.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="numberItemsReplaced">If we found any items with matching GUIDs, this indicates how many</param>
        protected void AddItem(T item, out int numberItemsReplaced, DateTime? currentQueryLowerBound, DateTime? currentQueryUpperBound)
        {
            numberItemsReplaced = 0;

            //===========================================================
            //Move the date window as necessary
            //===========================================================
            ExpandCacheWindowTimesAsNeeded(item.EffectiveDate);

            //===========================================================
            //Place the item in the currect spot in the list
            //===========================================================

            if (m_ascendingListOfItems.Count == 0)
            {
                m_ascendingListOfItems.AddFirst(item);
                goto done_adding_item;
            }

            LinkedListNode<T> existingNode = m_ascendingListOfItems.First;
            while (existingNode != null)
            {
                //=====================================================================
                //If the thing GUID's match, we are adding an item that has already been added
                //This situation can occur when when are splicing two sets of items together.
                //In most cases the Thing will be indentical (e.g. same contained data, same date)
                //
                //NOTE: This is not intended to fully cover the case of data that has changed
                //      dates.  (If the Thing was indeed changed, and the new date is later 
                //      then its previous date, we will remove the older item.  However if 
                //      an item changes and the new item occurs BEFORE the existing instance
                //      we will end up with 2 copies.  This is "by design" because we don't 
                //      want to check the whole list each item an item is added.  If the caller
                //      wants to be sure of fresh data they can Flush() the cache before the call.
                //=====================================================================
                if (existingNode.Value.ThingID == item.ThingID)
                {
                    //Move our pointer onto the next item
                    LinkedListNode<T> dupNode = existingNode;
                    existingNode = existingNode.Next;

                    m_ascendingListOfItems.Remove(dupNode);
                    numberItemsReplaced++;
                }
                else if (existingNode.Value.EffectiveDate > item.EffectiveDate)
                {
                    //Add the item in front it its date is earlier (and then exit, as we are done)
                    m_ascendingListOfItems.AddBefore(existingNode, item);
                    goto done_adding_item;
                }
                else
                {
                    existingNode = existingNode.Next;
                }
            }
            //This item has the latest time stamp.  Add it to the end
            m_ascendingListOfItems.AddLast(item);

        done_adding_item:
            //If our cache has grown too large; we need to trim it down...
            EnforceMaxCacheSize(currentQueryLowerBound, currentQueryUpperBound);
        }


        /// <summary>
        /// Returns true if the date falls within the current date boundaries
        /// that we are maintaining this date window for
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool IsDateInsideCacheWindow(DateTime date)
        {
            //If we have no defined date range; the item cannot be within these bounds.
            if (m_earliestDate == null) return false;
            //It would an unexpected condition to have an earlyest date, but not have a latest date.
            System.Diagnostics.Debug.Assert(m_latestDate != null);
            if (m_latestDate == null) return false;

            if (date < m_earliestDate.Value) return false;
            if (date > m_latestDate.Value) return false;

            return true;
        }

        /// <summary>
        /// Looks for a specific Thing inside the cache.  Returns TRUE if it is found
        /// </summary>
        /// <param name="seekingThingId"></param>
        /// <returns></returns>
        public T SeekThingInsideCacheWindow(Guid seekingThingId)
        {
            foreach (T item in m_ascendingListOfItems)
            {
                if (item.ThingID == seekingThingId)
                {
                    return item;
                }
            }

            //The item is not inside the current cache
            return null;
        }


        /// <summary>
        /// Date window
        /// </summary>
        protected Nullable<DateTime> m_earliestDate;
        /// <summary>
        /// Date window
        /// </summary>
        protected Nullable<DateTime> m_latestDate;


        /// <summary>
        /// Expands the window of data we claim to hold
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        protected void ExpandDataWindowBounds(DateTime startDate, DateTime endDate)
        {
            //--------------------------------------------------------------
            //Move the data window to meet our required bounds
            //NOTE: This boundary may exceed our limits
            //--------------------------------------------------------------
            if ((m_earliestDate == null) || (startDate <= m_earliestDate))
            {
                m_earliestDate = startDate;
            }

            if ((m_latestDate == null) || (endDate >= m_latestDate))
            {
                m_latestDate = endDate;
            }


        }


        /// <summary>
        /// Returns the date-stamp of the earliest data item
        /// </summary>
        public Nullable<DateTime> EarliestDateData
        {
            get
            {
                //No data
                if (m_ascendingListOfItems.Count == 0) return null;

                //Date of earliest data
                return m_ascendingListOfItems.First.Value.EffectiveDate;
            }
        }



        /// <summary>
        /// The ending date of our data window
        /// </summary>
        public Nullable<DateTime> LatestDateQueried
        {
            get
            {
                return m_latestDate;
            }
        }

        /// <summary>
        /// Returns the date-stamp of the last data item
        /// </summary>
        public Nullable<DateTime> LatestDateData
        {
            get
            {
                //No data
                if (m_ascendingListOfItems.Count == 0) return null;

                //Date of earliest data
                return m_ascendingListOfItems.Last.Value.EffectiveDate;
            }
        }

        /// <summary>
        /// The beginning date of our data-window
        /// </summary>
        public Nullable<DateTime> EarliestDateQueried
        {
            get
            {
                return m_earliestDate;
            }
        }

        /// <summary>
        /// Flushes the existing data cache if necessary
        /// and offers adjusted ranges to query data for to
        /// ensure no overlap
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="queryStart"></param>
        /// <param name="queryEnd"></param>
        /// <returns>FALSE if no query needed</returns>
        protected bool AdjustQueryBoundsAndCacheBasedOnExistingDataWindow(DateTime startDate, DateTime endDate, out DateTime queryStart, out DateTime queryEnd)
        {
            if (m_earliestDate == null)
            {
                //If we have no early date than that's that...
                queryStart = startDate;
                queryEnd = endDate;
            }
            else
            {
                //If we are inside the current data window then do nothing
                if ((startDate >= m_earliestDate.Value) && (endDate <= m_latestDate.Value))
                {
                    //Return out of range values
                    queryStart = DateTime.MinValue;
                    queryEnd = DateTime.MinValue;
                    return false;
                }

                //If the windows do not overlap then we should throw out our old cacle
                if ((m_latestDate.Value < startDate) || (m_earliestDate.Value > endDate))
                {
                    this.FlushCache();
                    queryStart = startDate;
                    queryEnd = endDate;
                }
                else if ((startDate < m_earliestDate.Value) && (endDate > m_latestDate.Value))
                {
                    //The requesting window overlaps our whole window.  
                    //Since we don't deal with duplicate records yet, flush the cache
                    this.FlushCache();
                    queryStart = startDate;
                    queryEnd = endDate;
                }
                else if (startDate < m_earliestDate.Value)
                {
                    //Start right before our dataset
                    queryStart = startDate;
                    queryEnd = m_earliestDate.Value;
                }
                else
                {
                    //Start right after our dataset
                    queryStart = m_latestDate.Value;
                    queryEnd = endDate;
                }
            }

            return true; //Yes, we should perform a query
        }


        /// <summary>
        /// This function is typically called when a new Thing is created and stored by the applicaiton.
        /// This function will call MSHealth and get the Thing (along with any XML Transforms we want).
        /// If the Effective date of the item we get falls within the Cache's window, then it is added.
        /// Otherwise the item is ignored.
        /// </summary>
        /// <param name="mshealthInfo"></param>
        /// <param name="thingID"></param>
        /// <returns></returns>
        public int FetchAndStoreItemIfDateWindowFits(Guid thingID)
        {
            Collection<Guid> thingGuids = new Collection<Guid>();
            thingGuids.Add(thingID);
            return FetchAndStoreItemIfDateWindowFits(thingGuids);
        }

        /// <summary>
        /// This function is typically called when a new Thing is created and stored by the applicaiton.
        /// This function will call MSHealth and get the Thing (along with any XML Transforms we want).
        /// If the Effective date of the item we get falls within the Cache's window, then it is added.
        /// Otherwise the item is ignored.
        /// 
        /// </summary>
        /// <param name="mshealthInfo"></param>
        /// <param name="thingItem"></param>
        /// <returns></returns>
        public int FetchAndStoreItemIfDateWindowFits(Collection<Guid> thingIDs)
        {
            int itemsAddedToCache = 0;

            //Increase our tally of queries performed
            m_numberServerQueriesRun++;

            //Get the list back and add it to our set
            bool placeholderBool;
            List<T> dataItems =
                QueryForThings(
                        null,
                        null,
                        thingIDs,
                        thingIDs.Count,
                        false,
                        out placeholderBool);

            //-----------------------------------------------------------------
            //If there are no items to store then do nothing
            //-----------------------------------------------------------------
            if ((dataItems == null) || (dataItems.Count == 0))
            {
                return 0; //No items addes
            }

            //Look through the result to see if it belongs
            foreach (T dataItem in dataItems)
            {
                //See if the date of the item we queried fits it into our cache window
                if (this.IsDateInsideCacheWindow(dataItem.EffectiveDate))
                {
                    this.AddItem(dataItem, TimeSpan.MaxValue);
                    itemsAddedToCache++;
                }
                else
                {
                    //It is possible that this item we are looking at
                    //is a Thing that was modified since we last queried it
                    //and that the Thing's effective date used to belong
                    //to the date-window we are looking at.  If so, 
                    //we should remove the Thing from the things list
                    this.RemoveItem(dataItem.ThingID);
                }
            }

            return itemsAddedToCache;
        }



        /// <summary>
        /// Returns number of items added or updated in the cache
        /// </summary>
        /// <param name="mshealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public int FetchMostRecentNItemsIfNeeded(int maxItemsToRetrieve)
        {
            //1. Do not allow a query of more items than our max cache size
            //2. We must ask for at least 1 item to be returned
            if ((maxItemsToRetrieve > this.MaxSizeOfCache) || (maxItemsToRetrieve < 1))
            {
                throw new ArgumentException();
            }


            //If our latest date is 'infinite future'; we may already have all the items we 
            //want to query for.
            //
            //Note: If we have an 'earliest date', we must also (by definition) have a 'latest date')
            if ((m_latestDate != null) && (m_latestDate.Value == DateTime.MaxValue))
            {
                //If we already have all the data from 'infinite past' to 'infinite future'; there is 
                //nothing to query.
                if (m_earliestDate.Value == DateTime.MinValue) { return 0; }

                //If our end-date is already 'infinite future' and we already have 'maxItemsToRetrieve' items
                //this means that the cache already contains the items that a query would return.
                if ((m_ascendingListOfItems != null) && (m_ascendingListOfItems.Count >= maxItemsToRetrieve))
                {
                    return 0;
                }

                //added by arun to fix issue where th app goes to hv to fetch data when the number of items have already been 0
                //if (m_ascendingListOfItems != null && m_ascendingListOfItems.Count == 0)
                //    return 0;
            } //end if

            /**ADDED by ARUN to fix issue where th app goes to hv to fetch data when the number of items have already been 0*/
            //DateTime queryStart;
            //DateTime queryEnd;
            //bool performQuery = true;

            //if (m_earliestDate.HasValue && m_latestDate.HasValue && (m_ascendingListOfItems != null && m_ascendingListOfItems.Count >= maxItemsToRetrieve))
            //{
            //    //performQuery = AdjustQueryBoundsAndCacheBasedOnExistingDataWindow(m_earliestDate.Value, m_latestDate.Value, out queryStart, out queryEnd);
            //    //if (!performQuery)
            //    return 0;
            //}
            /**ADDED by ARUN*/


            //========================================================================
            //Perform the server query
            //========================================================================
            //Increase our tally of queries performed
            m_numberServerQueriesRun++;
            bool dummyBool;
            List<T> dataItems = QueryForThings(
                                      DateTime.MinValue, DateTime.MaxValue,
                                      null,
                                      maxItemsToRetrieve,
                                      false,
                                      out dummyBool);

            //If there is no data in the set...There are 0 data items
            if ((dataItems == null) || (dataItems.Count == 0))
            {
                /**ADDED by ARUN to fix issue where th app goes to hv to fetch data when the number of items have already been 0*/
                m_earliestDate = DateTime.MinValue;
                m_latestDate = DateTime.MaxValue;
                //No results
                return 0;
            }

            //===========================================================
            //Figure out how these new items affect what's already in 
            //the cache
            //===========================================================
            if (dataItems.Count < maxItemsToRetrieve)
            {
                //======================================================================
                //The number of results returned was less than the 'max #' we requested
                //This indicates that we got ALL of the user's data.
                //======================================================================

                //Might as well throw out any old data we had
                FlushCache();

                //Add the new data
                ExpandDataWindowBounds(DateTime.MinValue, DateTime.MaxValue);
            }
            else
            {
                //======================================================================
                //We got the max-items we requested; now we need to figure out if we
                //want to keep any of our existing data
                //======================================================================

                //We should never get more items than what we asked for
                System.Diagnostics.Debug.Assert(dataItems.Count == maxItemsToRetrieve);

                T earliestItemInNewSet = dataItems[dataItems.Count - 1];
                DateTime earliestDateInNewSet = earliestItemInNewSet.EffectiveDate;

                //If the latest date we currently have in the set does not overlap
                //with the earliest data we just queried, we should flush the existing data
                //since there may be a gap between the 'latest' existing item, and the 'earliest'
                //new item
                if ((m_latestDate != null) && (m_latestDate.Value < earliestDateInNewSet))
                { FlushCache(); }

                //Expand the cache date to indicate that it included the 'infinitie future'.
                //The lower date bound will be determined by the data items we add.
                ExpandCacheWindowTimesAsNeeded(DateTime.MaxValue);
            }


            int numberItemsUpdatedInCache = 0;
            //===========================================================================
            //Go throuh the set of items and add them all into the cache
            //===========================================================================
            foreach (T queryResultItem in dataItems)
            {
                //If a copy of it exists in the cache already, remove it.
                RemoveItem(queryResultItem.ThingID);

                if (queryResultItem.IsItemAcceptableByCurrentApplication())
                {
                    //Add the item into the cache
                    AddItem(queryResultItem, TimeSpan.MaxValue);
                    numberItemsUpdatedInCache++;
                }
            }

            return numberItemsUpdatedInCache;
        }

        /// <summary>
        /// Returns true if we Retrieved any new data...
        /// </summary>
        /// <param name="mshealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public bool FetchDataIfNeeded(DateTime startDate, DateTime endDate)
        {
            //Parameter checking
            System.Diagnostics.Debug.Assert(startDate <= endDate);

            DateTime queryStart;
            DateTime queryEnd;

            bool performQuery = AdjustQueryBoundsAndCacheBasedOnExistingDataWindow(startDate, endDate, out queryStart, out queryEnd);
            if (!performQuery) return false;

            //Increase our tally of queries performed
            m_numberServerQueriesRun++;

            //Get the list back and add it to our set
            bool cacheOverflow;
            List<T> dataItems = QueryForThings(
                                      queryStart, queryEnd,
                                      null,
                                      this.MaxSizeOfCache,
                                      true,
                                      out cacheOverflow);

            if (cacheOverflow) m_cacheOverflowLatch.Set();

            foreach (T item in dataItems)
            {
                this.AddItem(item, TimeSpan.MaxValue, startDate, endDate);
            }

            ExpandDataWindowBounds(startDate, endDate);

            //Return true if we have at least one item
            return (dataItems.Count > 0);
        }

        public bool GetAdjustedDatesForQuery(DateTime startDate, DateTime endDate, out DateTime queryStart, out DateTime queryEnd)
        {
            //Parameter checking
            System.Diagnostics.Debug.Assert(startDate <= endDate);

            bool performQuery = AdjustQueryBoundsAndCacheBasedOnExistingDataWindow(startDate, endDate, out queryStart, out queryEnd);

            return performQuery;
        }

        public void ArrangeData(List<T> dataItems, bool cacheOverflow, DateTime dtStart, DateTime dtEnd)
        {
            if (cacheOverflow) m_cacheOverflowLatch.Set();

            foreach (T item in dataItems)
            {
                this.AddItem(item, TimeSpan.MaxValue, dtStart, dtEnd);
            }

            ExpandDataWindowBounds(dtStart, dtEnd);
        }

        #region IDataCacheManagerBase Members

        /// <summary>
        /// Clears out the data...
        /// </summary>
        public virtual void FlushCache()
        {
            m_ascendingListOfItems = new LinkedList<T>();
            m_earliestDate = null;
            m_latestDate = null;
            m_numberCacheFlushes++;
            m_cacheOverflowLatch.Reset();

        }

        public int NumberItemsInCache
        {
            get
            {
                //No list = empty cache
                if (m_ascendingListOfItems == null) return 0;
                return m_ascendingListOfItems.Count;
            }
        }

        public DateTime? CacheWindowStart
        {
            get
            {
                return m_earliestDate;
            }
        }

        public DateTime? CacheWindowEnd
        {
            get
            {
                return m_latestDate;
            }
        }

        #endregion
    }
}