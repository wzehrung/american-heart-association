﻿namespace HVManager
{
    using System;

    /// <summary>
    /// While the "LocalThingTypeDataManager" class manages a sizable cache of 0 to N Thing instances.
    ///
    /// This class manages access to a "single instace" of the most current "Thing" of a given type.
    ///
    /// This is useful for cases when the only data that is relevent is the single most up-to-date instace
    /// of a given type of data.  For example, getting the most recently defined Red, Yellow, Green zones
    /// for some data.
    /// 
    /// This class checks the user's whole data store for the single most recent item of a given type.
    /// 
    /// Thus derrived classes of this class manage either 0 or 1 instances of given ThingType.
    /// </summary>
    /// 
    [Serializable]
    public abstract class LocalThingTypeSingleInstanceManager<T> : IDataCacheManagerBase where T : LocalThingTypeDataItem
    {
        public LocalThingTypeSingleInstanceManager(Guid hvpersonid, Guid hvrecordid)
        {
            PersonGUID = hvpersonid;
            RecordGUID = hvrecordid;
        }

        public abstract HVDataTypeFilter GetHVFilter();

        public Guid PersonGUID
        {
            get;
            set;
        }

        public Guid RecordGUID
        {
            get;
            set;
        }

        /// <summary>
        /// The parent HealthRecordCache that owns us
        /// </summary>
        T _item;
        bool _wasItemQueriedFor;

        public bool WasItemQueriedFor
        {
            get
            {
                //Presently, we don't keep count.  (This could be added if desired)
                return _wasItemQueriedFor;
            }
            internal set
            {
                _wasItemQueriedFor = value;
            }
        }

        /// <summary>
        /// Returns the count of the # of times we have had to hit the 
        /// MSHealth server for data
        /// </summary>

        private int m_numberServerQueriesRun;

        public int NumberServerQueriesRun
        {
            get
            {
                //Presently, we don't keep count.  (This could be added if desired)
                return m_numberServerQueriesRun;
            }
            set
            {
                m_numberServerQueriesRun = value;
            }
        }


        /// <summary>
        /// Returns the single instance of the item we are managing.
        /// Returns NULL if the item does not exist in the server store
        /// </summary>
        public T Item
        {
            get
            {
                //If we have not queried for the item, do so now
                if ((_item == null) && (!_wasItemQueriedFor))
                {
                    //Query for the item
                    _item = GetLatestInstance();
                    m_numberServerQueriesRun++;
                    _wasItemQueriedFor = true;
                }

                return _item;
            }
            internal set
            {
                _item = value;
                m_numberServerQueriesRun++;
                _wasItemQueriedFor = true;
            }
        }

        public void DeleteItem()
        {
            if (Item != null)
            {
                WCQueries.DeleteThing(Helper.GetAccessor(PersonGUID, RecordGUID), Item.ThingKey);
                FlushCache();
            }
        }

        protected abstract T GetLatestInstance();

        #region IDataCacheManagerBase Members

        /// <summary>
        /// Clears out the data...
        /// </summary>
        public virtual void FlushCache()
        {
            _wasItemQueriedFor = false;
            _item = null;
        }


        public int NumberItemsInCache
        {
            get
            {
                //We have either 0 or 1 items.
                if (_item == null) return 0;
                return 1;
            }
        }

        public DateTime? CacheWindowStart
        {
            get
            {
                //We have either 0 or 1 items.
                if (_item == null) return null; // no item = no time
                return _item.EffectiveDate; //return the time of the single item
            }
        }

        public DateTime? CacheWindowEnd
        {
            get
            {
                //We have either 0 or 1 items.
                if (_item == null) return null; // no item = no time
                return _item.EffectiveDate; //return the time of the single item
            }
        }

        #endregion
    }
}