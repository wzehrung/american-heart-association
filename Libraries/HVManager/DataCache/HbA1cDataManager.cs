﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Linq;
    using HVWrapper.ExtendedThingTypes;

    [Serializable]
    public partial class HbA1cDataManager : LocalThingTypeDataManager<HbA1cDataManager.HbA1cItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class HbA1cItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<double> Value { get; private set; }
            public PossibleUpdate<DateTime> When { get; private set; }
            public PossibleUpdate<string> Source { get; private set; }

            public PossibleUpdate<string> PolkaItemID { get; private set; }
            public PossibleUpdate<string> ReadingType { get; private set; }
            public string ReadingTypeCodedValue { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (Value.IsDirty
                        || When.IsDirty
                        || Note.IsDirty
                        || PolkaItemID.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                Value.ResetDirtyFlag();
                When.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                PolkaItemID.ResetDirtyFlag();
            }

            public HbA1cItem()
            {
                Value = new PossibleUpdate<double>();
                When = new PossibleUpdate<DateTime>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                PolkaItemID = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public HbA1cItem(Guid gPersonGUID, Guid gRecordGUID, HbA1cEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                this.Value = new PossibleUpdate<double>(item.Value);
                this.When = new PossibleUpdate<DateTime>(item.When.ToDateTime());

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(gPersonGUID, gRecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;
                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);

                ResetDirtyFlag();
            }

            public override bool IsItemAcceptableByCurrentApplication()
            {
                if (float.IsInfinity((float)this.Value.Value))
                    return false;

                return true;
            }
        }

        DateTime dtStartDefault = DateTime.Today.AddDays(-180);

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(HbA1C.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(HbA1C.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public HbA1cDataManager(Guid hvpersonid, Guid hvrecordid)
            : base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }

        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<HbA1cItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<HbA1cItem> dataItemsA1c = new List<HbA1cItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               HbA1cEx.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

            IEnumerable<HbA1cEx> listA1c = dataItems.Cast<HbA1cEx>();

            foreach (HbA1cEx thing in listA1c)
            {
                HbA1cItem item = new HbA1cItem(PersonGUID, RecordGUID, thing);
                dataItemsA1c.Add(item);
            }

            return dataItemsA1c;
        }

        public HbA1cEx PopulateItem(HbA1cItem itemEdits)
        {
            HbA1cEx thing = new HbA1cEx();
            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public HbA1cItem CreateItem(HbA1cItem itemEdits)
        {
            HbA1cEx thing = new HbA1cEx();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(itemEdits.When.Value))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new HbA1cItem(PersonGUID, RecordGUID, thing);
        }

        public HbA1cItem UpdateItem(HbA1cItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            HbA1cEx myThing = (HbA1cEx)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            RemoveItem(myThing.Key.Id);

            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            return new HbA1cItem(PersonGUID, RecordGUID, myThing);
        }

        private void _UpdateFields(HbA1cEx thing, HbA1cItem itemEdits)
        {
            if (itemEdits.When.IsDirty) thing.When = new HealthServiceDateTime(itemEdits.When.Value);

            HbA1C objHbA1c = new HbA1C(thing.When, itemEdits.Value.Value);
            DisplayValue displayValue = new DisplayValue();

            displayValue.Units = "mmol-per-mol";
            displayValue.Value = itemEdits.Value.Value;

            if (itemEdits.Value.IsDirty) thing.Value = itemEdits.Value.Value;

            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}
