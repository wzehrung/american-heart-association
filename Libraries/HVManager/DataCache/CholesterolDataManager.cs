﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Linq;
    using HVWrapper.ExtendedThingTypes;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "CholesterolDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class CholesterolDataManager : LocalThingTypeDataManager<CholesterolDataManager.CholesterolItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class CholesterolItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            

                //
                // Note: values for TC, HDL, LDL and Triglycerides are stored in mg/dL
                //
            public PossibleUpdate<int?> TotalCholesterol { get; private set; }
            public PossibleUpdate<int?> HDL { get; private set; }
            public PossibleUpdate<int?> LDL { get; private set; }
            public PossibleUpdate<int?> Triglycerides { get; private set; }
            public PossibleUpdate<DateTime> When { get; private set; }
            public PossibleUpdate<string> TestLocation { get; private set; }
            public PossibleUpdate<string> PolkaItemID { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (TotalCholesterol.IsDirty
                        || HDL.IsDirty
                        || LDL.IsDirty
                        || Triglycerides.IsDirty
                        || When.IsDirty
                        || TestLocation.IsDirty
                        || Note.IsDirty
                        || PolkaItemID.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                TotalCholesterol.ResetDirtyFlag();
                HDL.ResetDirtyFlag();
                LDL.ResetDirtyFlag();
                Triglycerides.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                When.ResetDirtyFlag();
                TestLocation.ResetDirtyFlag();
                PolkaItemID.ResetDirtyFlag();
            }

            public CholesterolItem()
            {
                TotalCholesterol = new PossibleUpdate<int?>();
                HDL = new PossibleUpdate<int?>();
                LDL = new PossibleUpdate<int?>();
                Triglycerides = new PossibleUpdate<int?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                When = new PossibleUpdate<DateTime>();
                TestLocation = new PossibleUpdate<string>();
                PolkaItemID = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public CholesterolItem(Guid gPersonGUID, Guid gRecordGUID, CholesterolEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                int? ival = null;

                if (item.TotalCholesterolHasValue)
                    ival = item.TotalCholesterolInMgdlUnits;
                TotalCholesterol = new PossibleUpdate<int?>(ival);

                ival = null;
                if (item.HdlHasValue)
                    ival = item.HdlInMgdlUnits;
                HDL = new PossibleUpdate<int?>(ival);

                ival = null;
                if( item.LdlHasValue )
                    ival = item.LdlInMgdlUnits ;
                LDL = new PossibleUpdate<int?>(ival);

                ival = null;
                if (item.TriglycerideHasValue)
                    ival = item.TriglycerideInMgdlUnits;
                Triglycerides = new PossibleUpdate<int?>(ival);

                if (item.When != null)
                {
                    When = new PossibleUpdate<DateTime>(new DateTime(item.When.Date.Year, item.When.Date.Month, item.When.Date.Day));
                }
                TestLocation = new PossibleUpdate<string>(item.TestLocation);

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(gPersonGUID, gRecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;

                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);

                ResetDirtyFlag();
            }
        }

        DateTime dtStartDefault = DateTime.Today.AddDays(-180);

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(CholesterolEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(CholesterolEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public CholesterolDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<CholesterolItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<CholesterolItem> dataItemsC = new List<CholesterolItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               CholesterolProfileV2.TypeId,                 // <------- was CholesterolProfile.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

            IEnumerable<CholesterolEx> listBG = dataItems.Cast<CholesterolEx>();

            foreach (CholesterolEx thing in listBG)
            {
                CholesterolItem item = new CholesterolItem(PersonGUID, RecordGUID, thing);
                dataItemsC.Add(item);
            }

            return dataItemsC;
        }

        public CholesterolEx PopulateItem(CholesterolItem itemEdits)
        {
            CholesterolEx thing = new CholesterolEx();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public CholesterolItem CreateItem(CholesterolItem itemEdits)
        {
            CholesterolEx thing = new CholesterolEx();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            //=============================================================
            //Save the new Thing
            //=============================================================
            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(itemEdits.When.Value))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new CholesterolItem(PersonGUID, RecordGUID, thing);
        }

        public CholesterolItem UpdateItem(
            CholesterolItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            CholesterolEx myThing = (CholesterolEx)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            //Remove the item if it was in the cache
            RemoveItem(myThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            //Return the updated thing
            return new CholesterolItem(PersonGUID, RecordGUID, myThing);
        }

            //
            // PJB: itemEdits is the source, thing is the destination
            //
        private void _UpdateFields(CholesterolEx thing, CholesterolItem itemEdits)
        {
            if (itemEdits.When.IsDirty)
            {
                DateTime when = itemEdits.When.Value;
                thing.When = new HealthServiceDateTime(when);
            }

            if (itemEdits.LDL.IsDirty)
            {
                thing.LdlInMgdlUnits = itemEdits.LDL.Value.Value;
            }
            if (itemEdits.HDL.IsDirty)
            {
                thing.HdlInMgdlUnits = itemEdits.HDL.Value.Value;
            }
            if (itemEdits.TotalCholesterol.IsDirty)
            {
                thing.TotalCholesterolInMgdlUnits = itemEdits.TotalCholesterol.Value.Value;
            }
            if (itemEdits.TestLocation.IsDirty) thing.TestLocation = itemEdits.TestLocation.Value;
            if (itemEdits.Triglycerides.IsDirty)
            {
                thing.TriglycerideInMgdlUnits = itemEdits.Triglycerides.Value.Value;
            }
            if (itemEdits.PolkaItemID.IsDirty) thing.PolkaItemID = itemEdits.PolkaItemID.Value;
            if (itemEdits.PolkaItemID.IsDirty)
            {
                if (!String.IsNullOrEmpty(itemEdits.PolkaItemID.Value))
                {
                    thing.CommonData.ClientId = "POLKA-" + itemEdits.PolkaItemID.Value;
                }
                else
                {
                    thing.CommonData.ClientId = null;
                }
            }
            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}