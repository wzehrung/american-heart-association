﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Microsoft.Health;
using Microsoft.Health.Web;
using Microsoft.Health.ItemTypes;
using System.IO;
using System.Collections;
using System.Drawing;
using ImageLib;


namespace HVManager.DataCache
{
    public partial class PersonalImageManager : LocalThingTypeDataManager<PersonalImageManager.PersonalImageItem>, ILocalThingTypeDataManager 
    {

        public class PersonalImageItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            private static object personalImagesLock = new object();
            
            public PossibleUpdate<PersonalImage> PersonalPhoto { get; private set; }

            public bool IsDirty
            {
                get { return PersonalPhoto.IsDirty; }
            }

            public void ResetDirtyFlag()
            {
                PersonalPhoto.ResetDirtyFlag();
            }

            public PhotoItem()
            {
                PersonalPhoto = new PossibleUpdate<PersonalImage>();
                ResetDirtyFlag();
            }

            public PhotoItem(Guid gPersonGUID, Guid gRecordGUID, PersonalImage item)
                : base(gPersonGUID, gRecordGUID, item)

        }
    }
}
