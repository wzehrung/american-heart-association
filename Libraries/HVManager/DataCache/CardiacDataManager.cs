﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class CardiacDataManager : LocalThingTypeSingleInstanceManager<CardiacDataManager.CardiacItem>
    {
        [Serializable]
        public class CardiacItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<bool?> IsOnHypertensionMedication { get; private set; }
            public PossibleUpdate<bool?> IsOnHypertensionDiet { get; private set; }
            public PossibleUpdate<bool?> HasRenalFailureBeenDiagnosed { get; private set; }
            public PossibleUpdate<bool?> HasDiabetesBeenDiagnosed { get; private set; }
            public PossibleUpdate<bool?> HasFamilyHeartDiseaseHistory { get; private set; }
            public PossibleUpdate<bool?> HasFamilyStrokeHistory { get; private set; }
            public PossibleUpdate<bool?> HasPersonalHeartDiseaseHistory { get; private set; }
            public PossibleUpdate<bool?> HasPersonalStrokeHistory { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (IsOnHypertensionMedication.IsDirty
                    || IsOnHypertensionDiet.IsDirty
                    || HasRenalFailureBeenDiagnosed.IsDirty
                    || HasDiabetesBeenDiagnosed.IsDirty
                    || HasFamilyHeartDiseaseHistory.IsDirty
                    || HasFamilyStrokeHistory.IsDirty
                    || HasPersonalHeartDiseaseHistory.IsDirty
                    || HasPersonalStrokeHistory.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                IsOnHypertensionMedication.ResetDirtyFlag();
                IsOnHypertensionDiet.ResetDirtyFlag();
                HasRenalFailureBeenDiagnosed.ResetDirtyFlag();
                HasDiabetesBeenDiagnosed.ResetDirtyFlag();
                HasFamilyHeartDiseaseHistory.ResetDirtyFlag();
                HasFamilyStrokeHistory.ResetDirtyFlag();
                HasPersonalHeartDiseaseHistory.ResetDirtyFlag();
                HasPersonalStrokeHistory.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public CardiacItem()
            {
                IsOnHypertensionMedication = new PossibleUpdate<bool?>();
                IsOnHypertensionDiet = new PossibleUpdate<bool?>();
                HasRenalFailureBeenDiagnosed = new PossibleUpdate<bool?>();
                HasDiabetesBeenDiagnosed = new PossibleUpdate<bool?>();
                HasFamilyHeartDiseaseHistory = new PossibleUpdate<bool?>();
                HasFamilyStrokeHistory = new PossibleUpdate<bool?>();
                HasPersonalHeartDiseaseHistory = new PossibleUpdate<bool?>();
                HasPersonalStrokeHistory = new PossibleUpdate<bool?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public CardiacItem(Guid gPersonGUID, Guid gRecordGUID, CardiacProfile item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                IsOnHypertensionMedication = new PossibleUpdate<bool?>();
                IsOnHypertensionDiet = new PossibleUpdate<bool?>();
                HasRenalFailureBeenDiagnosed = new PossibleUpdate<bool?>();
                HasDiabetesBeenDiagnosed = new PossibleUpdate<bool?>();
                HasFamilyHeartDiseaseHistory = new PossibleUpdate<bool?>();
                HasFamilyStrokeHistory = new PossibleUpdate<bool?>();
                HasPersonalHeartDiseaseHistory = new PossibleUpdate<bool?>();
                HasPersonalStrokeHistory = new PossibleUpdate<bool?>();

                if (item.IsOnHypertensionMedication.HasValue)
                    IsOnHypertensionMedication = new PossibleUpdate<bool?>(item.IsOnHypertensionMedication);
                if (item.IsOnHypertensionDiet.HasValue)
                    IsOnHypertensionDiet = new PossibleUpdate<bool?>(item.IsOnHypertensionDiet);
                if (item.HasRenalFailureBeenDiagnosed.HasValue)
                    HasRenalFailureBeenDiagnosed = new PossibleUpdate<bool?>(item.HasRenalFailureBeenDiagnosed);
                if (item.HasDiabetesBeenDiagnosed.HasValue)
                    HasDiabetesBeenDiagnosed = new PossibleUpdate<bool?>(item.HasDiabetesBeenDiagnosed);
                if (item.HasFamilyHeartDiseaseHistory.HasValue)
                    HasFamilyHeartDiseaseHistory = new PossibleUpdate<bool?>(item.HasFamilyHeartDiseaseHistory);
                if (item.HasFamilyStrokeHistory.HasValue)
                    HasFamilyStrokeHistory = new PossibleUpdate<bool?>(item.HasFamilyStrokeHistory);
                if (item.HasPersonalHeartDiseaseHistory.HasValue)
                    HasPersonalHeartDiseaseHistory = new PossibleUpdate<bool?>(item.HasPersonalHeartDiseaseHistory);
                if (item.HasPersonalStrokeHistory.HasValue)
                    HasPersonalStrokeHistory = new PossibleUpdate<bool?>(item.HasPersonalStrokeHistory);
                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(CardiacProfile.TypeId, null, null, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public CardiacDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override CardiacItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   CardiacProfile.TypeId, null);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            CardiacProfile cThing = (CardiacProfile)thing;

            return new CardiacItem(PersonGUID, RecordGUID, cThing);
        }

        internal bool IsCreateOrUpdate(CardiacItem itemEdits, out CardiacProfile item)
        {
            bool bCreate = false;
            if (!itemEdits.IsDirty)
            {
                item = null;
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            CardiacProfile thing =
                (CardiacProfile)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    CardiacProfile.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                thing = new CardiacProfile();
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            item = thing;

            return bCreate;
        }

        private void _CreateItem(CardiacItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            CardiacProfile thing = new CardiacProfile();

            _UpdateFields(thing, itemEdits);

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            accessor.NewItem(thing);

            this.FlushCache();
        }

        public CardiacItem CreateOrUpdateItem(
            CardiacItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            CardiacProfile thing =
                (CardiacProfile)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    CardiacProfile.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(CardiacProfile thing,
                                                CardiacItem itemEdits)
        {
            //Set the field if appropriate
            if (itemEdits.HasRenalFailureBeenDiagnosed.IsDirty)
            {
                thing.HasRenalFailureBeenDiagnosed = itemEdits.HasRenalFailureBeenDiagnosed.Value;
            }

            //Set the field if appropriate
            if (itemEdits.HasDiabetesBeenDiagnosed.IsDirty)
            {
                thing.HasDiabetesBeenDiagnosed = itemEdits.HasDiabetesBeenDiagnosed.Value;
            }

            //Set the field if appropriate
            if (itemEdits.IsOnHypertensionDiet.IsDirty)
            {
                thing.IsOnHypertensionDiet = itemEdits.IsOnHypertensionDiet.Value;
            }

            //Set the field if appropriate
            if (itemEdits.IsOnHypertensionMedication.IsDirty)
            {
                thing.IsOnHypertensionMedication = itemEdits.IsOnHypertensionMedication.Value;
            }

            //Set the field if appropriate
            if (itemEdits.HasFamilyHeartDiseaseHistory.IsDirty)
            {
                thing.HasFamilyHeartDiseaseHistory = itemEdits.HasFamilyHeartDiseaseHistory.Value;
            }

            //Set the field if appropriate
            if (itemEdits.HasFamilyStrokeHistory.IsDirty)
            {
                thing.HasFamilyStrokeHistory = itemEdits.HasFamilyStrokeHistory.Value;
            }

            //Set the field if appropriate
            if (itemEdits.HasPersonalHeartDiseaseHistory.IsDirty)
            {
                thing.HasPersonalHeartDiseaseHistory = itemEdits.HasPersonalHeartDiseaseHistory.Value;
            }

            //Set the field if appropriate
            if (itemEdits.HasPersonalStrokeHistory.IsDirty)
            {
                thing.HasPersonalStrokeHistory = itemEdits.HasPersonalStrokeHistory.Value;
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}