﻿namespace HVManager
{
    using System;
    using System.Text;

    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;

    using HVWrapper.ExtendedThingTypes;
    

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class CholesterolGoalDataManager : LocalThingTypeSingleInstanceManager<CholesterolGoalDataManager.CholesterolGoalItem>
    {
        [Serializable]
        public class CholesterolGoalItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<int> TargetTotalCholesterol { get; private set; }
            public PossibleUpdate<int?> TargetHDL { get; private set; }
            public PossibleUpdate<int?> TargetLDL { get; private set; }
            public PossibleUpdate<int?> TargetTriglyceride { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (TargetTotalCholesterol.IsDirty
                    || TargetHDL.IsDirty
                    || TargetLDL.IsDirty
                    || TargetTriglyceride.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                TargetTotalCholesterol.ResetDirtyFlag();
                TargetHDL.ResetDirtyFlag();
                TargetLDL.ResetDirtyFlag();
                TargetTriglyceride.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public CholesterolGoalItem()
            {
                TargetTotalCholesterol = new PossibleUpdate<int>();
                TargetHDL = new PossibleUpdate<int?>();
                TargetLDL = new PossibleUpdate<int?>();
                TargetTriglyceride = new PossibleUpdate<int?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public CholesterolGoalItem(Guid gPersonGUID, Guid gRecordGUID, LifeGoalEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                TargetTotalCholesterol = new PossibleUpdate<int>();
                TargetHDL = new PossibleUpdate<int?>();
                TargetLDL = new PossibleUpdate<int?>();
                TargetTriglyceride = new PossibleUpdate<int?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();

                string[] strTargetValueArray = item.TargetValue.Split(',');

                foreach (string strTargetVal in strTargetValueArray)
                {
                    string[] strTargetValSpecificArray = strTargetVal.Split(':');
                    if (strTargetValSpecificArray[0].ToLower() == "hdl")
                    {
                        if (!string.IsNullOrEmpty(strTargetValSpecificArray[1]))
                        {
                            this.TargetHDL = new PossibleUpdate<int?>(Convert.ToInt32(strTargetValSpecificArray[1]));
                        }
                        else
                        {
                            this.TargetHDL = new PossibleUpdate<int?>(null);
                        }
                    }
                    else if (strTargetValSpecificArray[0].ToLower() == "ldl")
                    {
                        if (!string.IsNullOrEmpty(strTargetValSpecificArray[1]))
                        {
                            this.TargetLDL = new PossibleUpdate<int?>(Convert.ToInt32(strTargetValSpecificArray[1]));
                        }
                        else
                        {
                            this.TargetLDL = new PossibleUpdate<int?>(null);
                        }
                    }
                    else if (strTargetValSpecificArray[0].ToLower() == "totalcholesterol")
                    {
                        this.TargetTotalCholesterol = new PossibleUpdate<int>(Convert.ToInt32(strTargetValSpecificArray[1]));
                    }
                    else if (strTargetValSpecificArray[0].ToLower() == "triglyceride")
                    {
                        if (!string.IsNullOrEmpty(strTargetValSpecificArray[1]))
                        {
                            this.TargetTriglyceride = new PossibleUpdate<int?>(Convert.ToInt32(strTargetValSpecificArray[1]));
                        }
                        else
                        {
                            this.TargetTriglyceride = new PossibleUpdate<int?>(null);
                        }
                    }
                }

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        static string xpathFilter = "/thing/data-xml/common/extension[goal_type='ReduceCholestrol']";
        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(LifeGoalEx.TypeId, xpathFilter, 1, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public CholesterolGoalDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override CholesterolGoalItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   LifeGoalEx.TypeId, xpathFilter);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            LifeGoalEx lThing = (LifeGoalEx)thing;

            return new CholesterolGoalItem(PersonGUID, RecordGUID, lThing);
        }

        private LifeGoalEx _CreateItem(CholesterolGoalItem itemEdits, bool bHVCreate)
        {
            if (!itemEdits.IsDirty)
                return null;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing = new LifeGoalEx();

            thing.Description = "ReduceCholestrol";
            thing.Goal = new Microsoft.Health.ItemTypes.Goal();
            thing.TypeOfGoal = GoalType.ReduceCholestrol;

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            if (bHVCreate)
            {
                accessor.NewItem(thing);

                this.FlushCache();
            }

            return thing;
        }

        internal bool IsCreateOrUpdate(CholesterolGoalItem itemEdits, out LifeGoalEx item)
        {
            bool bCreate = false;
            item = null;
            if (!itemEdits.IsDirty)
            {
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                item = _CreateItem(itemEdits, false);
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            if (thing != null)
            {
                _UpdateFields(thing, itemEdits);

                item = thing;
            }

            return bCreate;
        }

        public CholesterolGoalItem CreateOrUpdateItem(
            CholesterolGoalItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits, true);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(LifeGoalEx thing,
                                                CholesterolGoalItem itemEdits)
        {
            if (itemEdits.TargetHDL.IsDirty ||
                itemEdits.TargetLDL.IsDirty ||
                itemEdits.TargetTotalCholesterol.IsDirty ||
                itemEdits.TargetTriglyceride.IsDirty)
            {
                StringBuilder sbGoalTarget = new StringBuilder();
                sbGoalTarget.Append("HDL:");
                sbGoalTarget.Append(itemEdits.TargetHDL.Value.ToString());
                sbGoalTarget.Append(",");
                sbGoalTarget.Append("LDL:");
                sbGoalTarget.Append(itemEdits.TargetLDL.Value.ToString());
                sbGoalTarget.Append(",");
                sbGoalTarget.Append("TotalCholesterol:");
                sbGoalTarget.Append(itemEdits.TargetTotalCholesterol.Value.ToString());
                sbGoalTarget.Append(",");
                sbGoalTarget.Append("Triglyceride:");
                sbGoalTarget.Append(itemEdits.TargetTriglyceride.Value.ToString());

                thing.TargetValue = sbGoalTarget.ToString();

                //this is not needed but, just in case we want to track when this goal was updated, we can use this!
                thing.StartDate = new HealthServiceDateTime(DateTime.Now);
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}