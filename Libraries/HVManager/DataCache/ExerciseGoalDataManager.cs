﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using HVWrapper.ExtendedThingTypes;
    using System.Text;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class ExerciseGoalDataManager : LocalThingTypeSingleInstanceManager<ExerciseGoalDataManager.ExerciseGoalItem>
    {
        [Serializable]
        public class ExerciseGoalItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<double?> Duration { get; private set; }
            public PossibleUpdate<string> Intensity { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (Duration.IsDirty
                    || Intensity.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                Duration.ResetDirtyFlag();
                Intensity.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public ExerciseGoalItem()
            {
                Duration = new PossibleUpdate<double?>();
                Intensity = new PossibleUpdate<string>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public ExerciseGoalItem(Guid gPersonGUID, Guid gRecordGUID, LifeGoalEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                Duration = new PossibleUpdate<double?>();
                Intensity = new PossibleUpdate<string>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();

                string[] strTargetValueArray = item.TargetValue.Split(',');

                foreach (string strTargetVal in strTargetValueArray)
                {
                    string[] strTargetValSpecificArray = strTargetVal.Split(':');
                    if (strTargetValSpecificArray[0].ToLower() == "intensity")
                    {
                        this.Intensity = new PossibleUpdate<string>(strTargetValSpecificArray[1]);
                    }
                    else if (strTargetValSpecificArray[0].ToLower() == "duration")
                    {
                        if (!string.IsNullOrEmpty(strTargetValSpecificArray[1]))
                        {
                            this.Duration = new PossibleUpdate<double?>(Convert.ToDouble(strTargetValSpecificArray[1]));
                        }
                    }
                }

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        static string xpathFilter = "/thing/data-xml/common/extension[goal_type='Exercise']";
        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(LifeGoalEx.TypeId, xpathFilter, 1, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public ExerciseGoalDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override ExerciseGoalItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   LifeGoalEx.TypeId, xpathFilter);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            LifeGoalEx lThing = (LifeGoalEx)thing;

            return new ExerciseGoalItem(PersonGUID, RecordGUID, lThing);
        }

        private LifeGoalEx _CreateItem(ExerciseGoalItem itemEdits, bool bHVCreate)
        {
            if (!itemEdits.IsDirty)
                return null;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing = new LifeGoalEx();
            thing.Description = "Exercise";
            thing.Goal = new Microsoft.Health.ItemTypes.Goal();
            thing.TypeOfGoal = GoalType.Exercise;

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            if (bHVCreate)
            {
                accessor.NewItem(thing);

                this.FlushCache();
            }

            return thing;
        }

        internal bool IsCreateOrUpdate(ExerciseGoalItem itemEdits, out LifeGoalEx item)
        {
            bool bCreate = false;
            item = null;
            if (!itemEdits.IsDirty)
            {
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                item = _CreateItem(itemEdits, false);
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            if (thing != null)
            {
                _UpdateFields(thing, itemEdits);

                item = thing;
            }

            return bCreate;
        }

        public ExerciseGoalItem CreateOrUpdateItem(
            ExerciseGoalItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits, true);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(LifeGoalEx thing,
                                                ExerciseGoalItem itemEdits)
        {
            if (itemEdits.Intensity.IsDirty ||
                itemEdits.Duration.IsDirty)
            {
                StringBuilder sbGoalTarget = new StringBuilder();
                sbGoalTarget.Append("Intensity:");
                if (!string.IsNullOrEmpty(itemEdits.Intensity.Value))
                {
                    sbGoalTarget.Append(itemEdits.Intensity.Value.ToString());
                }
                sbGoalTarget.Append(",");
                sbGoalTarget.Append("Duration:");
                if (itemEdits.Duration.Value.HasValue)
                {
                    sbGoalTarget.Append(itemEdits.Duration.Value.Value.ToString());
                }

                thing.TargetValue = sbGoalTarget.ToString();

                //this is not needed but, just in case we want to track when this goal was updated, we can use this!
                thing.StartDate = new HealthServiceDateTime(DateTime.Now);
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}