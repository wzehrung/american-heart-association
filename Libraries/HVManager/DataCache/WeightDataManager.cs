﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Linq;
    using HVWrapper.ExtendedThingTypes;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "WeightDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class WeightDataManager : LocalThingTypeDataManager<WeightDataManager.WeightItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class WeightItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<CommonWeight> CommonWeight { get; private set; }
            public PossibleUpdate<string> WeightText { get; private set; }
            public PossibleUpdate<DateTime> When { get; private set; }
            public PossibleUpdate<string> PolkaItemID { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (CommonWeight.IsDirty
                        || WeightText.IsDirty
                        || When.IsDirty
                        || Note.IsDirty
                        || PolkaItemID.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                CommonWeight.ResetDirtyFlag();
                WeightText.ResetDirtyFlag();
                When.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                PolkaItemID.ResetDirtyFlag();
            }

            public WeightItem()
            {
                CommonWeight = new PossibleUpdate<CommonWeight>();
                WeightText = new PossibleUpdate<string>();
                When = new PossibleUpdate<DateTime>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                PolkaItemID = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public WeightItem(Guid gPersonGUID, Guid gRecordGUID, WeightEx item)
                : base(gPersonGUID, gRecordGUID,item)
            {
                //Store the values
                HVManager.CommonWeight cWeight = HVManager.CommonWeight.FromKilograms(item.Value.Kilograms);

                //If we don't have a preferred unit specified to show the weight in, then show it
                //as formatted kilograms
                string weightText;
                if (item.Value.DisplayValue == null)
                {
                    weightText = cWeight.ToString();
                }
                else
                {
                    //Try to normize the weight unit, if it is a known value
                    string unitText = HVManager.CommonWeight.AttemptWeightUnitTextNormalization(item.Value.DisplayValue.Units);

                    //Store the users preferred unit
                    CommonWeight.WeightUnit unit = HVManager.CommonWeight.ConvertStringToUnit(unitText);
                    if (unit == HVManager.CommonWeight.WeightUnit.Unknown)
                    {
                        cWeight.PreferredUnit = HVManager.CommonWeight.WeightUnit.Kilograms;
                    }
                    else
                    {
                        cWeight.PreferredUnit = unit;
                    }

                    //Store the normalized text representation
                    weightText = item.Value.DisplayValue.Value.ToString() + " " + unitText;
                }

                WeightText = new PossibleUpdate<string>(weightText);

                CommonWeight = new PossibleUpdate<CommonWeight>(cWeight);

                When = new PossibleUpdate<DateTime>(item.When.ToDateTime());

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(PersonGUID,RecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;

                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);

                ResetDirtyFlag();
            }

            public WeightItem(Guid gPersonGUID, Guid gRecordGUID, Microsoft.Health.ItemTypes.Weight w )
                : base(w.Key, w.When.ToDateTime(), null, w.CommonData.Source, string.Empty, w.CommonData.Note)
            {
                //Store the values
                HVManager.CommonWeight cWeight = HVManager.CommonWeight.FromKilograms(w.Value.Kilograms);

                //If we don't have a preferred unit specified to show the weight in, then show it
                //as formatted kilograms
                string weightText;
                if (w.Value.DisplayValue == null)
                {
                    weightText = cWeight.ToString();
                }
                else
                {
                    //Try to normize the weight unit, if it is a known value
                    string unitText = HVManager.CommonWeight.AttemptWeightUnitTextNormalization( w.Value.DisplayValue.Units);

                    //Store the users preferred unit
                    CommonWeight.WeightUnit unit = HVManager.CommonWeight.ConvertStringToUnit(unitText);
                    if (unit == HVManager.CommonWeight.WeightUnit.Unknown)
                    {
                        cWeight.PreferredUnit = HVManager.CommonWeight.WeightUnit.Kilograms;
                    }
                    else
                    {
                        cWeight.PreferredUnit = unit;
                    }

                    //Store the normalized text representation
                    weightText = w.Value.DisplayValue.Value.ToString() + " " + unitText;
                }

                WeightText = new PossibleUpdate<string>(weightText);

                CommonWeight = new PossibleUpdate<CommonWeight>(cWeight);

                When = new PossibleUpdate<DateTime>(w.When.ToDateTime());

                /** PJB
                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing( PersonGUID, RecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;

                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);
                 **/

                ResetDirtyFlag();
            }
        }

        DateTime dtStartDefault = DateTime.Today.AddDays(-180);

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(WeightEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(WeightEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public WeightDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<WeightItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<WeightItem> dataItemsWeight = new List<WeightItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               WeightEx.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

/** PJB Debugging stuff **/
/**
            if (dataItems.Count > 0)
            {
                // see if we can process WeightEx.
                try
                {
                    HealthRecordItem hri = dataItems[0];
                    WeightEx we = (WeightEx)hri;

                    // if we are here, then process WeightEx
                    IEnumerable<WeightEx> listWeight = dataItems.Cast<WeightEx>();

                    foreach (WeightEx thing in listWeight)
                    {
                        WeightItem item = new WeightItem(PersonGUID, RecordGUID, thing);
                        dataItemsWeight.Add(item);
                    }
                }
                catch
                {
                    // can't process WeightEx

                    foreach (Microsoft.Health.ItemTypes.Weight wc in dataItems)
                    {
                        //WeightEx wex = new WeightEx();
                        //Microsoft.Health.ItemTypes.Weight dst = (Microsoft.Health.ItemTypes.Weight)wex;
                        //dst = wc;
                        //dst.Key = wc.Key;
                        //dst.Value = wc.Value;

                        WeightItem item = new WeightItem( PersonGUID, RecordGUID, wc );
                        dataItemsWeight.Add(item);
                    }
                }
            }
**/
            // PJB - this cast does not work!!!
            IEnumerable<WeightEx> listWeight = dataItems.Cast<WeightEx>();

            foreach (WeightEx thing in listWeight)
            {
                WeightItem item = new WeightItem( PersonGUID, RecordGUID, thing);
                dataItemsWeight.Add(item);
            }
      
            return dataItemsWeight;
        }

        public WeightEx PopulateItem(WeightItem itemEdits)
        {
            WeightEx thing = new WeightEx();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public WeightItem CreateItem(WeightItem itemEdits)
        {
            WeightEx thing = new WeightEx();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            //=============================================================
            //Save the new Thing
            //=============================================================
            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(itemEdits.When.Value))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new WeightItem(PersonGUID, RecordGUID, thing);
        }

        public WeightItem UpdateItem(
            WeightItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            WeightEx myThing = (WeightEx)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            //Remove the item if it was in the cache
            RemoveItem(myThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            //Return the updated thing
            return new WeightItem(PersonGUID, RecordGUID, myThing);
        }

        private void _UpdateFields(WeightEx thing, WeightItem itemEdits)
        {
            //=============================================================
            //Set the time stamp, note and weight
            //=============================================================
            if (itemEdits.When.IsDirty) thing.When = new HealthServiceDateTime(itemEdits.When.Value);
            if (itemEdits.CommonWeight.IsDirty) thing.Value = helper_CreateMSHealthWeightValue(itemEdits.CommonWeight.Value);
            if (itemEdits.PolkaItemID.IsDirty) thing.PolkaItemID = itemEdits.PolkaItemID.Value;

            if (itemEdits.PolkaItemID.IsDirty)
            {
                if (!String.IsNullOrEmpty(itemEdits.PolkaItemID.Value))
                {
                    thing.CommonData.ClientId = "POLKA-" + itemEdits.PolkaItemID.Value;
                }
                else
                {
                    thing.CommonData.ClientId = null;
                }
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;

        }

        /// <summary>
        /// Creates a MSHealth Weight-Value object from internal weight data representations
        /// </summary>
        /// <param name="weightData"></param>
        /// <returns></returns>
        private static WeightValue helper_CreateMSHealthWeightValue(CommonWeight weightData)
        {
            WeightValue wcWeightValue =
                new WeightValue();
            wcWeightValue.Kilograms = weightData.ToKilograms();

            DisplayValue
                displayValue = new DisplayValue();

            //Store the preferred display format
            displayValue.Units = weightData.PerferredUnitText;
            displayValue.Value = weightData.convertTo(weightData.PreferredUnit);
            wcWeightValue.DisplayValue = displayValue;

            return wcWeightValue;
        }
    }
}