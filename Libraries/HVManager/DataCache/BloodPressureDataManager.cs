﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Linq;
    using HVWrapper.ExtendedThingTypes;
    
    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "BloodPressureDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class BloodPressureDataManager : LocalThingTypeDataManager<BloodPressureDataManager.BPItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class BPItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<int?> HeartRate { get; private set; }
            public PossibleUpdate<int> Systolic { get; private set; }
            public PossibleUpdate<int> Diastolic { get; private set; }
            public PossibleUpdate<DateTime> When { get; private set; }
            public PossibleUpdate<string> PolkaItemID { get; private set; }

            public PossibleUpdate<bool?> IrregularHeartbeat { get; private set; } // WHZ 3-26-15

            public bool IsDirty
            {
                get
                {
                    return (HeartRate.IsDirty
                        || Systolic.IsDirty
                        || Diastolic.IsDirty
                        || When.IsDirty
                        || Note.IsDirty
                        || PolkaItemID.IsDirty
                        || Source.IsDirty
                        || IrregularHeartbeat.IsDirty); // WHZ 3-26-15
                }
            }

            public void ResetDirtyFlag()
            {
                HeartRate.ResetDirtyFlag();
                Systolic.ResetDirtyFlag();
                Diastolic.ResetDirtyFlag();
                When.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                PolkaItemID.ResetDirtyFlag();
                IrregularHeartbeat.ResetDirtyFlag();  // WHZ 3-26-15
            }

            public BPItem()
            {
                HeartRate = new PossibleUpdate<int?>();
                Systolic = new PossibleUpdate<int>();
                Diastolic = new PossibleUpdate<int>();
                When = new PossibleUpdate<DateTime>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                PolkaItemID = new PossibleUpdate<string>();
                IrregularHeartbeat = new PossibleUpdate<bool?>();  // WHZ 3-26-15
                ResetDirtyFlag();
            }

            public BPItem(Guid gPersonGUID, Guid gRecordGUID, BloodPressureEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                HeartRate = new PossibleUpdate<int?>(item.Pulse);
                Systolic = new PossibleUpdate<int>(item.Systolic);
                Diastolic = new PossibleUpdate<int>(item.Diastolic);
                When = new PossibleUpdate<DateTime>(item.When.ToDateTime());

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(gPersonGUID, gRecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;

                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);

                IrregularHeartbeat = new PossibleUpdate<bool?>(item.IrregularHeartbeatDetected);  // WHZ 3-26-15

                ResetDirtyFlag();
            }
        }

        DateTime dtStartDefault = DateTime.Today.AddDays(-180);

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(BloodPressureEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(BloodPressureEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public BloodPressureDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<BPItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<BPItem> dataItemsBP = new List<BPItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               BloodPressureEx.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

            IEnumerable<BloodPressureEx> listBG = dataItems.Cast<BloodPressureEx>();

            foreach (BloodPressureEx thing in listBG)
            {
                BPItem item = new BPItem(PersonGUID, RecordGUID, thing);
                dataItemsBP.Add(item);
            }

            return dataItemsBP;
        }

        public BloodPressureEx PopulateItem(BPItem itemEdits)
        {
            BloodPressureEx thing = new BloodPressureEx();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public BPItem CreateItem(BPItem itemEdits)
        {
            BloodPressureEx thing = new BloodPressureEx();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            //=============================================================
            //Save the new Thing
            //=============================================================
            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(itemEdits.When.Value))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new BPItem(PersonGUID, RecordGUID, thing);
        }

        public BPItem UpdateItem(
            BPItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            BloodPressureEx myThing = (BloodPressureEx)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            //Remove the item if it was in the cache
            RemoveItem(myThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            //Return the updated thing
            return new BPItem(PersonGUID, RecordGUID, myThing);
        }

        private void _UpdateFields(BloodPressureEx thing, BPItem itemEdits)
        {
            //Update only the fields indicated
            if (itemEdits.When.IsDirty) thing.When = new HealthServiceDateTime(itemEdits.When.Value);
            if (itemEdits.Diastolic.IsDirty) thing.Diastolic = itemEdits.Diastolic.Value;
            if (itemEdits.Systolic.IsDirty) thing.Systolic = itemEdits.Systolic.Value;
            if (itemEdits.HeartRate.IsDirty) thing.Pulse = itemEdits.HeartRate.Value;
            if (itemEdits.PolkaItemID.IsDirty) thing.PolkaItemID = itemEdits.PolkaItemID.Value;
            if (itemEdits.PolkaItemID.IsDirty)
            {
                if (!String.IsNullOrEmpty(itemEdits.PolkaItemID.Value))
                {
                    thing.CommonData.ClientId = "POLKA-" + itemEdits.PolkaItemID.Value;
                }
                else
                {
                    thing.CommonData.ClientId = null;
                }
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
            if (itemEdits.IrregularHeartbeat.IsDirty) thing.IrregularHeartbeatDetected = itemEdits.IrregularHeartbeat.Value;  // WHZ 3-26-15
        }
    }
}