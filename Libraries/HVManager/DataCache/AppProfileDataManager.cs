﻿
using System;
using Microsoft.Health.ItemTypes;
using Microsoft.Health;

using HVWrapper;

namespace HVManager
{
    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class AppProfileDataManager : LocalThingTypeSingleInstanceManager<AppProfileDataManager.AppProfileItem>
    {
        [Serializable]
        public class AppProfileItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<bool?> HasUserAgreedToStoreDataInDB { get; private set; }
            public PossibleUpdate<bool?> HasUserAgreedToTermsAndConditions { get; private set; }
            public PossibleUpdate<bool?> HasUserAgreedToNewTermsAndConditions { get; private set; }
            
            public bool IsDirty
            {
                get
                {
                    return (HasUserAgreedToStoreDataInDB.IsDirty
                    || HasUserAgreedToTermsAndConditions.IsDirty
                    || HasUserAgreedToNewTermsAndConditions.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                HasUserAgreedToStoreDataInDB.ResetDirtyFlag();
                HasUserAgreedToTermsAndConditions.ResetDirtyFlag();
                HasUserAgreedToNewTermsAndConditions.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public AppProfileItem()
            {
                HasUserAgreedToStoreDataInDB = new HVManager.PossibleUpdate<bool?>();
                HasUserAgreedToTermsAndConditions = new HVManager.PossibleUpdate<bool?>();
                HasUserAgreedToNewTermsAndConditions = new HVManager.PossibleUpdate<bool?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public AppProfileItem(Guid gPersonGUID, Guid gRecordGUID, HealthRecordItem item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                CustomHealthTypeWrapper customWrapperForThing = (CustomHealthTypeWrapper)item;

                
                H360AppProfile customThing = (H360AppProfile)customWrapperForThing.WrappedObject;

                HasUserAgreedToStoreDataInDB = new HVManager.PossibleUpdate<bool?>(customThing.HasUserAgreedToStoreDataInDB);
                HasUserAgreedToTermsAndConditions = new HVManager.PossibleUpdate<bool?>(customThing.HasUserAgreedToTermsAndConditions);
                HasUserAgreedToNewTermsAndConditions = new HVManager.PossibleUpdate<bool?>(customThing.HasUserAgreedToNewTermsAndConditions);

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(customThingTypes, H360AppProfile.XPathFilterForQuery, 1, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public AppProfileDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override AppProfileItem GetLatestInstance()
        {
            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                     Helper.GetAccessor(PersonGUID, RecordGUID),
                                    customThingTypes,
                                    H360AppProfile.XPathFilterForQuery
                                    );

            //Null data result
            if (thing == null) return null;

            return new AppProfileItem(PersonGUID, RecordGUID, thing);
        }

        private void _CreateItem(AppProfileItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            H360AppProfile thing = new H360AppProfile();
            CustomHealthTypeWrapper wrapper = new CustomHealthTypeWrapper(thing);
            thing.When = new HealthServiceDateTime(DateTime.Now);

            _UpdateFields(thing, itemEdits);

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            accessor.NewItem(wrapper);

            this.FlushCache();
        }

        public AppProfileItem CreateOrUpdateItem(
            AppProfileItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            CustomHealthTypeWrapper thing =
                (CustomHealthTypeWrapper)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    customThingTypes,
                                    H360AppProfile.XPathFilterForQuery);

            H360AppProfile wrappedThing = null;

            if (thing != null)
                wrappedThing = (H360AppProfile)thing.WrappedObject;


            //No instance was returned from the server
            if (wrappedThing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits);

                return Item;
            }

            //If the keys do not match; its an error
            if (!wrappedThing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(wrappedThing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(H360AppProfile wrappedThing,
                                                AppProfileItem itemEdits)
        {
            if (itemEdits.HasUserAgreedToStoreDataInDB.IsDirty)
                wrappedThing.HasUserAgreedToStoreDataInDB = itemEdits.HasUserAgreedToStoreDataInDB.Value;

            if (itemEdits.HasUserAgreedToTermsAndConditions.IsDirty)
                wrappedThing.HasUserAgreedToTermsAndConditions = itemEdits.HasUserAgreedToTermsAndConditions.Value;

            if (itemEdits.HasUserAgreedToNewTermsAndConditions.IsDirty)
                wrappedThing.HasUserAgreedToNewTermsAndConditions = itemEdits.HasUserAgreedToNewTermsAndConditions.Value;

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) wrappedThing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) wrappedThing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}