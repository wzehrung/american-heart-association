﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;
    using System.Linq;
    using HVWrapper.ExtendedThingTypes;
    using Microsoft.Health.ItemTypes;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "ExerciseDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class ExerciseDataManager : LocalThingTypeDataManager<ExerciseDataManager.ExerciseItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class ExerciseItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<string> Activity { get; private set; }
            public PossibleUpdate<DateTime> When { get; private set; }
            public PossibleUpdate<RelativeRating> Intensity { get; private set; }
            public PossibleUpdate<double?> Duration { get; private set; }
            public PossibleUpdate<int?> NumberOfSteps { get; private set; }
            public PossibleUpdate<string> PolkaItemID { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (Activity.IsDirty
                        || When.IsDirty
                        || Intensity.IsDirty
                        || Duration.IsDirty
                        || NumberOfSteps.IsDirty
                        || Note.IsDirty
                        || PolkaItemID.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                Activity.ResetDirtyFlag();
                When.ResetDirtyFlag();
                Intensity.ResetDirtyFlag();
                Duration.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                NumberOfSteps.ResetDirtyFlag();
                PolkaItemID.ResetDirtyFlag();
            }

            public ExerciseItem()
            {
                Activity = new HVManager.PossibleUpdate<string>();
                When = new HVManager.PossibleUpdate<DateTime>();
                Intensity = new HVManager.PossibleUpdate<RelativeRating>();
                Duration = new HVManager.PossibleUpdate<double?>();
                NumberOfSteps = new HVManager.PossibleUpdate<int?>();
                Source = new PossibleUpdate<string>();
                Note = new HVManager.PossibleUpdate<string>();
                PolkaItemID = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public ExerciseItem(Guid gPersonGUID, Guid gRecordGUID, ExerciseEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                Activity = new PossibleUpdate<string>();
                if (item.Title != null)
                {
                    Activity.Value = item.Title;
                }
                else
                {
                    if (item.Activity != null)
                    {
                        Activity.Value = item.Activity.Text;
                    }
                }

                ExerciseDetail objIntensityDetail = ExerciseDataManager.GetExerciseDetailFromThing(item, VocabHelper.STR_EXERCISE_INTERSITY);

                Intensity = new PossibleUpdate<RelativeRating>(RelativeRating.None);
                if (objIntensityDetail != null)
                {
                    if (objIntensityDetail.Value != null)
                    {
                        Intensity = new PossibleUpdate<RelativeRating>(ExerciseDataManager.GetRelativeRatingFromIntensityLevel((int)objIntensityDetail.Value.Value));
                    }
                }

                Duration = new PossibleUpdate<double?>();
                if (item.Duration.HasValue)
                {
                    Duration.Value = item.Duration;
                }

                ExerciseDetail objStepsDetail = ExerciseDataManager.GetExerciseDetailFromThing(item, VocabHelper.STR_EXERCISE_STEPS_COUNT);

                NumberOfSteps = new PossibleUpdate<int?>();
                if (objStepsDetail != null
                    && objStepsDetail.Value != null)
                {
                    NumberOfSteps.Value = (int)objStepsDetail.Value.Value;
                }

                When = new PossibleUpdate<DateTime>();
                if (item.When != null
                    && item.When.ApproximateDate != null
                    && item.When.ApproximateDate.Month.HasValue
                    && item.When.ApproximateDate.Day.HasValue
                    )
                {

                    When.Value = new DateTime(item.When.ApproximateDate.Year, item.When.ApproximateDate.Month.Value, item.When.ApproximateDate.Day.Value);
                }

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(gPersonGUID, gRecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;

                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);

                ResetDirtyFlag();
            }

            /// <summary>
            /// The way pedometers work is that if you stop walking for more than 2 minutes, 
            /// it ends one Aerobic Exercise Session (AES) and starts another. 
            /// Therefore if someone wears a pedometer all day (which people do to track their total daily walking) 
            /// then they end up with dozens of AESs. The Pedometer also does not record a duration for an AES 
            /// unless it is >10 uninterrupted minutes of walking at a reasonable pace. 
            /// This is how the pedometer distinguishes between a real exercise session and a minor walk. 
            /// If we display every AES that a pedometer stores then we end up with Heart360 screens full of AESs 
            /// that have no duration. They have no useful information as far as Heart360 is concerned. 
            /// Therefore what we need to do is to filter the display of AES so that we only display those 
            /// that have positive values for duration. 
            /// </summary>
            /// <returns>bool</returns>
            public override bool IsItemAcceptableByCurrentApplication()
            {
                if (this.Duration.Value.HasValue && this.Duration.Value.Value > 0)
                    return true;
                return false;
            }
        }

        DateTime dtStartDefault = DateTime.Today.AddDays(-180);

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(ExerciseEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(ExerciseEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public ExerciseDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<ExerciseItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<ExerciseItem> dataItemsEx = new List<ExerciseItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               ExerciseEx.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

            IEnumerable<ExerciseEx> listBG = dataItems.Cast<ExerciseEx>();

            foreach (ExerciseEx thing in listBG)
            {
                ExerciseItem item = new ExerciseItem(PersonGUID, RecordGUID, thing);
                dataItemsEx.Add(item);
            }

            return dataItemsEx;
        }

        public ExerciseEx PopulateItem(ExerciseItem itemEdits)
        {
            ExerciseEx thing = new ExerciseEx();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public ExerciseItem CreateItem(ExerciseItem itemEdits)
        {
            ExerciseEx thing = new ExerciseEx();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            //=============================================================
            //Save the new Thing
            //=============================================================
            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(itemEdits.When.Value))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new ExerciseItem(PersonGUID, RecordGUID, thing);
        }

        public ExerciseItem UpdateItem(
            ExerciseItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            ExerciseEx myThing = (ExerciseEx)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            //Remove the item if it was in the cache
            RemoveItem(myThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            //Return the updated thing
            return new ExerciseItem(PersonGUID, RecordGUID, myThing);
        }

        public static ExerciseDetail GetExerciseDetailFromThing(ExerciseEx thing, string strKey)
        {
            ExerciseDetail objDetail = null;
            if (thing.Details != null && thing.Details.Count > 0)
            {
                KeyValuePair<string, ExerciseDetail> objDetailPair = thing.Details.Where(d => d.Key.ToLower() == strKey.ToLower()).SingleOrDefault();
                if (!string.IsNullOrEmpty(objDetailPair.Key))
                {
                    objDetail = objDetailPair.Value;
                }
            }

            return objDetail;
        }

        private static int GetIntensityLevelFromRelativeRating(RelativeRating eRating)
        {
            switch (eRating)
            {
                case Microsoft.Health.ItemTypes.RelativeRating.High:
                case Microsoft.Health.ItemTypes.RelativeRating.Low:
                case Microsoft.Health.ItemTypes.RelativeRating.Moderate:
                case Microsoft.Health.ItemTypes.RelativeRating.VeryHigh:
                case Microsoft.Health.ItemTypes.RelativeRating.VeryLow:
                    return (int)eRating;

                case Microsoft.Health.ItemTypes.RelativeRating.None:
                default:
                    break;
            }
            return 0;
        }

        private static RelativeRating GetRelativeRatingFromIntensityLevel(int iVal)
        {
            switch (iVal)
            {
                case 1:
                    return Microsoft.Health.ItemTypes.RelativeRating.VeryLow;

                case 2:
                    return Microsoft.Health.ItemTypes.RelativeRating.Low;

                case 3:
                    return Microsoft.Health.ItemTypes.RelativeRating.Moderate;

                case 4:
                    return Microsoft.Health.ItemTypes.RelativeRating.High;

                case 5:
                    return Microsoft.Health.ItemTypes.RelativeRating.VeryHigh;

                default:
                    break;       
            }
            return RelativeRating.None;
        }

        private void _UpdateFields(ExerciseEx thing, ExerciseItem itemEdits)
        {
            //=============================================================
            //Date
            //=============================================================                
            ApproximateDateTime wcDate =
                new ApproximateDateTime(itemEdits.When.Value);
            thing.When = wcDate;

            //=============================================================
            //Activity name/type
            //=============================================================
            if (itemEdits.Activity.IsDirty)
            {
                thing.Title = itemEdits.Activity.Value;
                thing.Activity = HVManager.CodedAndCodable.GetCodableValue(itemEdits.Activity.Value,
                                                VocabHelper.STR_AEROBIC_ACTIVITIES_KEY,
                                                VocabHelper.STR_FAMILY_WC);
            }

            //=============================================================
            //Duration in minutes
            //=============================================================
            if (itemEdits.Duration.Value.HasValue)
            {
                thing.Duration = itemEdits.Duration.Value;
            }
            else
            {
                thing.Duration = null;
            }

            if (itemEdits.NumberOfSteps.IsDirty)
            {
                ExerciseDetail objStepsDetail = GetExerciseDetailFromThing(thing, VocabHelper.STR_EXERCISE_STEPS_COUNT);
                if (objStepsDetail == null)
                {
                    objStepsDetail = new ExerciseDetail();
                    objStepsDetail.Name = new CodedValue(VocabHelper.STR_EXERCISE_STEPS_COUNT, VocabHelper.STR_EXERCISE_DETAIL_NAMES_KEY, VocabHelper.STR_FAMILY_WC, VocabHelper.STR_VERSION_1);
                }

                if (itemEdits.NumberOfSteps.Value.HasValue)
                {
                    StructuredMeasurement objStructuredMeasurement = new StructuredMeasurement();
                    objStructuredMeasurement.Units = HVManager.CodedAndCodable.GetCodableValue(VocabHelper.STR_EXERCISE_STEPS_COUNT_VALUE,
                                                VocabHelper.STR_EXERCISE_UNITS_KEY,
                                                VocabHelper.STR_FAMILY_WC);
                    objStructuredMeasurement.Value = itemEdits.NumberOfSteps.Value.Value;

                    objStepsDetail.Value = objStructuredMeasurement;

                    thing.Details.Remove(VocabHelper.STR_EXERCISE_STEPS_COUNT);
                    thing.Details.Add(VocabHelper.STR_EXERCISE_STEPS_COUNT, objStepsDetail);
                }
                else
                {
                    if (objStepsDetail != null)
                    {
                        thing.Details.Remove(VocabHelper.STR_EXERCISE_STEPS_COUNT);
                    }
                }
            }


            //=============================================================
            //Intensity in minutes
            //=============================================================
            if (itemEdits.Intensity.IsDirty)
            {
                ExerciseDetail objIntensityDetail = GetExerciseDetailFromThing(thing, VocabHelper.STR_EXERCISE_INTERSITY);
                if (objIntensityDetail == null)
                {
                    objIntensityDetail = new ExerciseDetail();
                    objIntensityDetail.Name = new CodedValue(VocabHelper.STR_EXERCISE_INTERSITY, VocabHelper.STR_EXERCISE_DETAIL_NAMES_KEY, VocabHelper.STR_FAMILY_WC, VocabHelper.STR_VERSION_1);
                }

                StructuredMeasurement objStructuredMeasurement = new StructuredMeasurement();
                int iIntensity = GetIntensityLevelFromRelativeRating(itemEdits.Intensity.Value);
                if (itemEdits.Intensity.Value == RelativeRating.None)
                {
                    if (objIntensityDetail != null)
                    {
                        thing.Details.Remove(VocabHelper.STR_EXERCISE_INTERSITY);
                    }
                }
                else
                {
                    objStructuredMeasurement.Units = HVManager.CodedAndCodable.GetCodableValue(itemEdits.Intensity.Value.ToString(),
                                                ((int)itemEdits.Intensity.Value).ToString(),
                                                VocabHelper.STR_EXERCISE_INTENSITIES_KEY,
                                                VocabHelper.STR_FAMILY_WC);
                    if (objStructuredMeasurement.Units != null
                        && objStructuredMeasurement.Units.Count > 0)
                    {
                        objStructuredMeasurement.Value = Convert.ToDouble(objStructuredMeasurement.Units[0].Value);
                    }

                    objIntensityDetail.Value = objStructuredMeasurement;

                    thing.Details.Remove(VocabHelper.STR_EXERCISE_INTERSITY);
                    thing.Details.Add(VocabHelper.STR_EXERCISE_INTERSITY, objIntensityDetail);
                }
            }

            if (itemEdits.PolkaItemID.IsDirty) thing.PolkaItemID = itemEdits.PolkaItemID.Value;
            if (itemEdits.PolkaItemID.IsDirty)
            {
                if (!String.IsNullOrEmpty(itemEdits.PolkaItemID.Value))
                {
                    thing.CommonData.ClientId = "POLKA-" + itemEdits.PolkaItemID.Value;
                }
                else
                {
                    thing.CommonData.ClientId = null;
                }
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}