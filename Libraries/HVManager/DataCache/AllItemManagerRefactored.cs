﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using Microsoft.Health.Web;

using HVWrapper.ExtendedThingTypes;

namespace HVManager
{
    [Serializable]
    public class AllItemManagerRefactored
    {
        Guid m_hvPersonId;
        Guid m_hvRecordId;

        public static object _lock = new object();
        public List<BloodPressureDataManager.BPItem> dataItemsBPForCampaign = new List<BloodPressureDataManager.BPItem>();

        
        public enum HVDataManager
        {
            APP_PROFILE = 0,
            BASIC,
            BLOOD_GLUCOSE,
            BLOOD_PRESSURE,
            BLOOD_PRESSURE_GOAL,
            CARDIAC,
            CHOLESTEROL,
            CHOLESTEROL_GOAL,
            EMERGENCY_CONTACT,
            EXERCISE,
            EXERCISE_GOAL,
            EXTENDED_PROFILE,
            FILE,
            HEIGHT,
            MEDICATION,
            MESSAGE,
            PERSONAL,
            PERSONAL_CONTACT,
            WEIGHT,
            WEIGHT_GOAL,
            HBA1C
        } ;

        private enum HVRecordItemType
        {
            BLOOD_GLUCOSE_EX = 0,
            BLOOD_PRESSURE_EX,
            CHOLESTEROL_EX,
            EXERCISE_EX,
            HEIGHT,
            MEDICATION_EX,
            WEIGHT_EX,
            PERSONAL,
            BASICV2,        // aka Basic
            CONTACT,        // aka Personal Contact
            CUSTOM,         // aka CustomHealthTypeWrapper
            CARDIAC_PROFILE,
            PERSON,
            LIFE_GOAL_EX,
            FILE_EX,
            HBA1C_EX
        } ;


        #region DataManagers
        FileDataManager m_FileDataManager;
        public FileDataManager FileDataManager
        {
            get
            {
                return m_FileDataManager;
            }
        }

        BasicDataManager m_BasicDataManager;
        public BasicDataManager BasicDataManager
        {
            get
            {
                return m_BasicDataManager;
            }
        }

        //BasicV2 m_BasicV2DataManager;
        //public BasicV2 BasicV2DataManager
        //{
        //    get
        //    {
        //        return m_BasicV2DataManager;
        //    }
        //}

        EmergencyContactDataManager m_EmergencyContactDataManager;
        public EmergencyContactDataManager EmergencyContactDataManager
        {
            get
            {
                return m_EmergencyContactDataManager;
            }
        }

        BloodGlucoseDataManager m_BloodGlucoseDataManager;
        public BloodGlucoseDataManager BloodGlucoseDataManager
        {
            get
            {
                return m_BloodGlucoseDataManager;
            }
        }

        BloodPressureDataManager m_BloodPressureDataManager;
        public BloodPressureDataManager BloodPressureDataManager
        {
            get
            {
                return m_BloodPressureDataManager;
            }
        }

        BPGoalDataManager m_BPGoalDataManager;
        public BPGoalDataManager BPGoalDataManager
        {
            get
            {
                return m_BPGoalDataManager;
            }
        }

        CardiacDataManager m_CardiacDataManager;
        public CardiacDataManager CardiacDataManager
        {
            get
            {
                return m_CardiacDataManager;
            }
        }

        CholesterolDataManager m_CholesterolDataManager;
        public CholesterolDataManager CholesterolDataManager
        {
            get
            {
                return m_CholesterolDataManager;
            }
        }

        CholesterolGoalDataManager m_CholesterolGoalDataManager;
        public CholesterolGoalDataManager CholesterolGoalDataManager
        {
            get
            {
                return m_CholesterolGoalDataManager;
            }
        }

        ExerciseDataManager m_ExerciseDataManager;
        public ExerciseDataManager ExerciseDataManager
        {
            get
            {
                return m_ExerciseDataManager;
            }
        }

        ExtendedProfileDataManager m_ExtendedProfileDataManager;
        public ExtendedProfileDataManager ExtendedProfileDataManager
        {
            get
            {
                return m_ExtendedProfileDataManager;
            }
        }

        HeightDataManager m_HeightDataManager;
        public HeightDataManager HeightDataManager
        {
            get
            {
                return m_HeightDataManager;
            }
        }

        MedicationDataManager m_MedicationDataManager;
        public MedicationDataManager MedicationDataManager
        {
            get
            {
                return m_MedicationDataManager;
            }
        }

        MessageDataManager m_MessageDataManager;
        public MessageDataManager MessageDataManager
        {
            get
            {
                return m_MessageDataManager;
            }
        }

        PersonalContactDataManager m_PersonalContactDataManager;
        public PersonalContactDataManager PersonalContactDataManager
        {
            get
            {
                return m_PersonalContactDataManager;
            }
        }

        PersonalDataManager m_PersonalDataManager;
        public PersonalDataManager PersonalDataManager
        {
            get
            {
                return m_PersonalDataManager;
            }
        }

        WeightDataManager m_WeightDataManager;
        public WeightDataManager WeightDataManager
        {
            get
            {
                return m_WeightDataManager;
            }
        }

        WeightGoalDataManager m_WeightGoalDataManager;
        public WeightGoalDataManager WeightGoalDataManager
        {
            get
            {
                return m_WeightGoalDataManager;
            }
        }

        AppProfileDataManager m_AppProfileDataManager;
        public AppProfileDataManager AppProfileDataManager
        {
            get
            {
                return m_AppProfileDataManager;
            }
        }

        ExerciseGoalDataManager m_ExerciseGoalDataManager;
        public ExerciseGoalDataManager ExerciseGoalDataManager
        {
            get
            {
                return m_ExerciseGoalDataManager;
            }
        }

        HbA1cDataManager m_HbA1cDataManager;
        public HbA1cDataManager HbA1cDataManager
        {
            get
            {
                return m_HbA1cDataManager;
            }
        }

        #endregion

        public bool IsDBSynchronized
        {
            get;
            set;
        }

        public bool IsPolkaDataProcessed
        {
            get;
            set;
        }

        bool m_IsNameSet = false;
        string m_Name = string.Empty;
        public string Name
        {
            get
            {
                if (m_IsNameSet)
                    return m_Name;

                Microsoft.Health.Web.HealthServicePage objCurrentPage = Microsoft.Health.Web.HealthServicePage.CurrentPage;
                if (objCurrentPage != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords.Count > 0)
                {
                    PersonInfo objPersonInfo = Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo;
                    if (objPersonInfo != null
                        && objPersonInfo.AuthorizedRecords != null
                        && objPersonInfo.AuthorizedRecords.Count > 0
                        && !string.IsNullOrEmpty(objPersonInfo.AuthorizedRecords[m_hvRecordId].Name))
                    {
                        m_Name = objPersonInfo.AuthorizedRecords[m_hvRecordId].Name;
                    }

                    m_IsNameSet = true;
                }
                else
                {

                    OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(m_hvPersonId);
                    offlineConn.Authenticate();

                    PersonInfo objPersonInfo = offlineConn.GetPersonInfo();
                    if (objPersonInfo != null
                        && objPersonInfo.AuthorizedRecords != null
                        && objPersonInfo.AuthorizedRecords.Count > 0
                        && !string.IsNullOrEmpty(objPersonInfo.AuthorizedRecords[m_hvRecordId].Name))
                    {
                        m_Name = objPersonInfo.AuthorizedRecords[m_hvRecordId].Name;
                    }

                    m_IsNameSet = true;
                }

                return m_Name;
            }
        }

        //made public by arun on March 7, 2011 for use by PolkaDataHVUploaderProcess
        public AllItemManagerRefactored(Guid hvpersonid, Guid hvrecordid)
        {
            m_hvPersonId = hvpersonid;
            m_hvRecordId = hvrecordid;

            //m_BasicV2DataManager = new BasicV2(hvpersonid, hvrecordid);
            m_BasicDataManager = new BasicDataManager(hvpersonid, hvrecordid);
            m_BloodGlucoseDataManager = new BloodGlucoseDataManager(hvpersonid, hvrecordid);
            m_BloodPressureDataManager = new BloodPressureDataManager(hvpersonid, hvrecordid);
            m_BPGoalDataManager = new BPGoalDataManager(hvpersonid, hvrecordid);
            m_CardiacDataManager = new CardiacDataManager(hvpersonid, hvrecordid);
            m_CholesterolDataManager = new CholesterolDataManager(hvpersonid, hvrecordid);
            m_CholesterolGoalDataManager = new CholesterolGoalDataManager(hvpersonid, hvrecordid);
            m_ExerciseDataManager = new ExerciseDataManager(hvpersonid, hvrecordid);
            m_ExtendedProfileDataManager = new ExtendedProfileDataManager(hvpersonid, hvrecordid);
            m_HeightDataManager = new HeightDataManager(hvpersonid, hvrecordid);
            m_MedicationDataManager = new MedicationDataManager(hvpersonid, hvrecordid);
            m_MessageDataManager = new MessageDataManager(hvpersonid, hvrecordid);
            m_PersonalContactDataManager = new PersonalContactDataManager(hvpersonid, hvrecordid);
            m_PersonalDataManager = new PersonalDataManager(hvpersonid, hvrecordid);
            m_WeightDataManager = new WeightDataManager(hvpersonid, hvrecordid);
            m_WeightGoalDataManager = new WeightGoalDataManager(hvpersonid, hvrecordid);
            m_EmergencyContactDataManager = new EmergencyContactDataManager(hvpersonid, hvrecordid);
            m_AppProfileDataManager = new AppProfileDataManager(hvpersonid, hvrecordid);
            m_FileDataManager = new FileDataManager(hvpersonid, hvrecordid);
            m_ExerciseGoalDataManager = new ExerciseGoalDataManager(hvpersonid, hvrecordid);
            m_HbA1cDataManager = new HbA1cDataManager(hvpersonid, hvrecordid);
        }

        public void FlushCache()
        {
            m_BasicDataManager.FlushCache();
            m_BloodGlucoseDataManager.FlushCache();
            m_BloodPressureDataManager.FlushCache();
            m_BPGoalDataManager.FlushCache();
            m_CardiacDataManager.FlushCache();
            m_CholesterolDataManager.FlushCache();
            m_CholesterolGoalDataManager.FlushCache();
            m_ExerciseDataManager.FlushCache();
            m_ExtendedProfileDataManager.FlushCache();
            m_HeightDataManager.FlushCache();
            m_MedicationDataManager.FlushCache();
            m_MessageDataManager.FlushCache();
            m_PersonalContactDataManager.FlushCache();
            m_PersonalDataManager.FlushCache();
            m_WeightDataManager.FlushCache();
            m_WeightGoalDataManager.FlushCache();
            m_EmergencyContactDataManager.FlushCache();
            m_AppProfileDataManager.FlushCache();
            m_FileDataManager.FlushCache();
            m_ExerciseGoalDataManager.FlushCache();
            m_HbA1cDataManager.FlushCache();
        }

        bool m_LastActiveDateSet = false;
        DateTime? m_LastActiveDate;
        public DateTime? LastActiveDate
        {
            get
            {
                if (!m_LastActiveDateSet)
                {
                    AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordGUID(m_hvRecordId);
                    if (objPatient == null)
                    {
                        objPatient = AHAHelpContent.Patient.FindByUserOfflineHealthRecordGUID(m_hvRecordId);
                    }

                    m_LastActiveDate = objPatient.LastActivityDate;
                    m_LastActiveDateSet = true;
                }

                return m_LastActiveDate;
            }
        }

        public void CreateUpdateWizardItems(Dictionary<string, object> list)
        {
            List<HealthRecordItem> listCreate = new List<HealthRecordItem>();
            List<HealthRecordItem> listUpdate = new List<HealthRecordItem>();
            bool bCreate = false;

            foreach (string key in list.Keys)
            {
                if (key == typeof(BloodGlucoseDataManager).ToString())
                {
                    BloodGlucoseDataManager.BGItem item = list[key] as BloodGlucoseDataManager.BGItem;
                    BloodGlucoseEx objItem = null;
                    objItem = BloodGlucoseDataManager.PopulateItem(item as BloodGlucoseDataManager.BGItem);
                    if (objItem != null)
                    {
                        listCreate.Add(objItem);
                    }
                }
                else if (key == typeof(HbA1cDataManager).ToString())
                {
                    HbA1cDataManager.HbA1cItem item = list[key] as HbA1cDataManager.HbA1cItem;
                    
                    
                }
                else if (key == typeof(WeightDataManager).ToString())
                {
                    WeightDataManager.WeightItem item = list[key] as WeightDataManager.WeightItem;
                    WeightEx objItem = null;
                    objItem = WeightDataManager.PopulateItem(item as WeightDataManager.WeightItem);
                    if (objItem != null)
                    {
                        listCreate.Add(objItem);
                    }
                }
                else if (key == typeof(HeightDataManager).ToString())
                {
                    HeightDataManager.HeightItem item = list[key] as HeightDataManager.HeightItem;
                    Height objItem = null;
                    objItem = HeightDataManager.PopulateItem(item as HeightDataManager.HeightItem);
                    if (objItem != null)
                    {
                        listCreate.Add(objItem);
                    }
                }
                else if (key == typeof(BloodPressureDataManager).ToString())
                {
                    BloodPressureDataManager.BPItem item = list[key] as BloodPressureDataManager.BPItem;
                    BloodPressureEx objItem = null;
                    objItem = BloodPressureDataManager.PopulateItem(item as BloodPressureDataManager.BPItem);
                    if (objItem != null)
                    {
                        listCreate.Add(objItem);
                    }
                }
                else if (key == typeof(CholesterolDataManager).ToString())
                {
                    CholesterolDataManager.CholesterolItem item = list[key] as CholesterolDataManager.CholesterolItem;
                    CholesterolEx objItem = null;
                    objItem = CholesterolDataManager.PopulateItem(item as CholesterolDataManager.CholesterolItem);
                    if (objItem != null)
                    {
                        listCreate.Add(objItem);
                    }
                }
                else if (key == typeof(ExerciseDataManager).ToString())
                {
                    ExerciseDataManager.ExerciseItem item = list[key] as ExerciseDataManager.ExerciseItem;
                    ExerciseEx objItem = null;
                    objItem = ExerciseDataManager.PopulateItem(item as ExerciseDataManager.ExerciseItem);
                    if (objItem != null)
                    {
                        listCreate.Add(objItem);
                    }
                }
                else if (key == typeof(BPGoalDataManager).ToString())
                {
                    BPGoalDataManager.BPGoalItem objGoalItem = list[key] as BPGoalDataManager.BPGoalItem;
                    LifeGoalEx objItem = null;
                    bCreate = BPGoalDataManager.IsCreateOrUpdate(objGoalItem as BPGoalDataManager.BPGoalItem, out objItem);
                    if (objItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(objItem);
                        }
                        else
                        {
                            listUpdate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(WeightGoalDataManager).ToString())
                {
                    WeightGoalDataManager.WeightGoalItem objGoalItem = list[key] as WeightGoalDataManager.WeightGoalItem;
                    LifeGoalEx objItem = null;
                    bCreate = WeightGoalDataManager.IsCreateOrUpdate(objGoalItem as WeightGoalDataManager.WeightGoalItem, out objItem);
                    if (objItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(objItem);
                        }
                        else
                        {
                            listUpdate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(CholesterolGoalDataManager).ToString())
                {
                    CholesterolGoalDataManager.CholesterolGoalItem objGoalItem = list[key] as CholesterolGoalDataManager.CholesterolGoalItem;
                    LifeGoalEx objItem = null;
                    bCreate = CholesterolGoalDataManager.IsCreateOrUpdate(objGoalItem as CholesterolGoalDataManager.CholesterolGoalItem, out objItem);
                    if (objItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(objItem);
                        }
                        else
                        {
                            listUpdate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(ExerciseGoalDataManager).ToString())
                {
                    ExerciseGoalDataManager.ExerciseGoalItem objGoalItem = list[key] as ExerciseGoalDataManager.ExerciseGoalItem;
                    LifeGoalEx objItem = null;
                    bCreate = ExerciseGoalDataManager.IsCreateOrUpdate(objGoalItem as ExerciseGoalDataManager.ExerciseGoalItem, out objItem);
                    if (objItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(objItem);
                        }
                        else
                        {
                            listUpdate.Add(objItem);
                        }
                    }
                }
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(m_hvPersonId, m_hvRecordId);
            if (listCreate.Count > 0)
            {
                accessor.NewItems(listCreate);
            }

            if (listUpdate.Count > 0)
            {
                accessor.UpdateItems(listUpdate);
            }

            List<HealthRecordItem> masterlist = new List<HealthRecordItem>();
            masterlist.AddRange(listCreate);
            masterlist.AddRange(listUpdate);

            foreach (HealthRecordItem item in masterlist)
            {
                if (item.GetType() == typeof(LifeGoalEx))
                {
                    BPGoalDataManager.FlushCache();
                    CholesterolGoalDataManager.FlushCache();
                    WeightGoalDataManager.FlushCache();
                    ExerciseGoalDataManager.FlushCache();
                }

                if (item.GetType() == typeof(CholesterolEx))
                {
                    CholesterolDataManager.FlushCache();
                }

                if (item.GetType() == typeof(BloodPressureEx))
                {
                    BloodPressureDataManager.FlushCache();
                }

                if (item.GetType() == typeof(BloodGlucoseEx))
                {
                    BloodGlucoseDataManager.FlushCache();
                }

                if (item.GetType() == typeof(MedicationEx))
                {
                    MedicationDataManager.FlushCache();
                }

                if (item.GetType() == typeof(WeightEx))
                {
                    WeightDataManager.FlushCache();
                }
                if (item.GetType() == typeof(Height))
                {
                    HeightDataManager.FlushCache();
                }
                if (item.GetType() == typeof(ExerciseEx))
                {
                    ExerciseDataManager.FlushCache();
                }
            }
        }

        public void CreatePolkaItems(bool bIsOnline, Dictionary<string, object> list)
        {
            List<HealthRecordItem> listCreate = new List<HealthRecordItem>();

            foreach (string key in list.Keys)
            {
                if (key == typeof(BloodGlucoseDataManager).ToString())
                {
                    List<BloodGlucoseDataManager.BGItem> itemList = list[key] as List<BloodGlucoseDataManager.BGItem>;
                    foreach (BloodGlucoseDataManager.BGItem item in itemList)
                    {
                        BloodGlucoseEx objItem = null;
                        objItem = BloodGlucoseDataManager.PopulateItem(item as BloodGlucoseDataManager.BGItem);
                        if (objItem != null)
                        {
                            listCreate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(WeightDataManager).ToString())
                {
                    List<WeightDataManager.WeightItem> itemList = list[key] as List<WeightDataManager.WeightItem>;
                    foreach (WeightDataManager.WeightItem item in itemList)
                    {
                        WeightEx objItem = null;
                        objItem = WeightDataManager.PopulateItem(item as WeightDataManager.WeightItem);
                        if (objItem != null)
                        {
                            listCreate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(BloodPressureDataManager).ToString())
                {
                    List<BloodPressureDataManager.BPItem> itemList = list[key] as List<BloodPressureDataManager.BPItem>;
                    foreach (BloodPressureDataManager.BPItem item in itemList)
                    {
                        BloodPressureEx objItem = null;
                        objItem = BloodPressureDataManager.PopulateItem(item as BloodPressureDataManager.BPItem);
                        if (objItem != null)
                        {
                            listCreate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(CholesterolDataManager).ToString())
                {
                    List<CholesterolDataManager.CholesterolItem> itemList = list[key] as List<CholesterolDataManager.CholesterolItem>;
                    foreach (CholesterolDataManager.CholesterolItem item in itemList)
                    {
                        CholesterolEx objItem = null;
                        objItem = CholesterolDataManager.PopulateItem(item as CholesterolDataManager.CholesterolItem);
                        if (objItem != null)
                        {
                            listCreate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(ExerciseDataManager).ToString())
                {
                    List<ExerciseDataManager.ExerciseItem> itemList = list[key] as List<ExerciseDataManager.ExerciseItem>;
                    foreach (ExerciseDataManager.ExerciseItem item in itemList)
                    {
                        ExerciseEx objItem = null;
                        objItem = ExerciseDataManager.PopulateItem(item as ExerciseDataManager.ExerciseItem);
                        if (objItem != null)
                        {
                            listCreate.Add(objItem);
                        }
                    }
                }
                else if (key == typeof(MedicationDataManager).ToString())
                {
                    List<MedicationDataManager.MedicationItem> itemList = list[key] as List<MedicationDataManager.MedicationItem>;
                    foreach (MedicationDataManager.MedicationItem item in itemList)
                    {
                        MedicationEx objItem = null;
                        objItem = MedicationDataManager.PopulateItem(item as MedicationDataManager.MedicationItem);
                        if (objItem != null)
                        {
                            listCreate.Add(objItem);
                        }
                    }
                }
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(bIsOnline, m_hvPersonId, m_hvRecordId);
            if (listCreate.Count > 0)
            {
                accessor.NewItems(listCreate);
            }

            foreach (string key in list.Keys)
            {
                if (key == typeof(CholesterolDataManager).ToString())
                {
                    CholesterolDataManager.FlushCache();
                }
                else if (key == typeof(BloodPressureDataManager).ToString())
                {
                    BloodPressureDataManager.FlushCache();
                }
                else if (key == typeof(BloodGlucoseDataManager).ToString())
                {
                    BloodGlucoseDataManager.FlushCache();
                }
                else if (key == typeof(MedicationDataManager).ToString())
                {
                    MedicationDataManager.FlushCache();
                }
                else if (key == typeof(WeightDataManager).ToString())
                {
                    WeightDataManager.FlushCache();
                }
                else if (key == typeof(ExerciseDataManager).ToString())
                {
                    ExerciseDataManager.FlushCache();
                }
            }
        }

        public void CreateOrUpdateSingletons(Dictionary<string, object> list)
        {
            List<HealthRecordItem> listCreate = new List<HealthRecordItem>();
            List<HealthRecordItem> listUpdate = new List<HealthRecordItem>();
            bool bCreate = false;

            foreach (string key in list.Keys)
            {
                if (key == typeof(PersonalContactDataManager).ToString())
                {
                    PersonalContactDataManager.ContactItem item = list[key] as PersonalContactDataManager.ContactItem;
                    Contact cItem = null;
                    bCreate = PersonalContactDataManager.IsCreateOrUpdate(item as PersonalContactDataManager.ContactItem, out cItem);
                    if (cItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(cItem);
                        }
                        else
                        {
                            listUpdate.Add(cItem);
                        }
                    }
                }

                if (key == typeof(BasicDataManager).ToString())
                {
                    BasicDataManager.BasicItem item = list[key] as BasicDataManager.BasicItem;
                    BasicV2 hvItem = null;
                    bCreate = BasicDataManager.IsCreateOrUpdate(item as BasicDataManager.BasicItem, out hvItem);
                    if (hvItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(hvItem);
                        }
                        else
                        {
                            listUpdate.Add(hvItem);
                        }
                    }
                }

                if (key == typeof(CardiacDataManager).ToString())
                {
                    CardiacDataManager.CardiacItem item = list[key] as CardiacDataManager.CardiacItem;
                    CardiacProfile hvItem = null;
                    bCreate = CardiacDataManager.IsCreateOrUpdate(item as CardiacDataManager.CardiacItem, out hvItem);
                    if (hvItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(hvItem);
                        }
                        else
                        {
                            listUpdate.Add(hvItem);
                        }
                    }
                }

                if (key == typeof(EmergencyContactDataManager).ToString())
                {
                    EmergencyContactDataManager.EmergencyContactItem item = list[key] as EmergencyContactDataManager.EmergencyContactItem;
                    Person hvItem = null;
                    bCreate = EmergencyContactDataManager.IsCreateOrUpdate(item as EmergencyContactDataManager.EmergencyContactItem, out hvItem);
                    if (hvItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(hvItem);
                        }
                        else
                        {
                            listUpdate.Add(hvItem);
                        }
                    }
                }

                if (key == typeof(PersonalDataManager).ToString())
                {
                    PersonalDataManager.PersonalItem item = list[key] as PersonalDataManager.PersonalItem;
                    Personal hvItem = null;
                    bCreate = PersonalDataManager.IsCreateOrUpdate(item as PersonalDataManager.PersonalItem, out hvItem);
                    if (hvItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(hvItem);
                        }
                        else
                        {
                            listUpdate.Add(hvItem);
                        }
                    }
                }

                if (key == typeof(ExtendedProfileDataManager).ToString())
                {
                    ExtendedProfileDataManager.ExtendedProfileItem item = list[key] as ExtendedProfileDataManager.ExtendedProfileItem;
                    HVWrapper.CustomHealthTypeWrapper hvItem = null;
                    bCreate = ExtendedProfileDataManager.IsCreateOrUpdate(item as ExtendedProfileDataManager.ExtendedProfileItem, out hvItem);
                    if (hvItem != null)
                    {
                        if (bCreate)
                        {
                            listCreate.Add(hvItem);
                        }
                        else
                        {
                            listUpdate.Add(hvItem);
                        }
                    }
                }
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(m_hvPersonId, m_hvRecordId);
            if (listCreate.Count > 0)
            {
                accessor.NewItems(listCreate);
            }

            if (listUpdate.Count > 0)
            {
                accessor.UpdateItems(listUpdate);
            }

            List<HealthRecordItem> masterlist = new List<HealthRecordItem>();
            masterlist.AddRange(listCreate);
            masterlist.AddRange(listUpdate);

            foreach (HealthRecordItem item in masterlist)
            {
                if (item.GetType() == typeof(BasicV2))
                {
                    BasicDataManager.FlushCache();
                }

                if (item.GetType() == typeof(Contact))
                {
                    PersonalContactDataManager.FlushCache();
                }

                if (item.GetType() == typeof(CardiacProfile))
                {
                    CardiacDataManager.FlushCache();
                }

                if (item.GetType() == typeof(Personal))
                {
                    PersonalDataManager.FlushCache();
                }

                if (item.GetType() == typeof(Person))
                {
                    EmergencyContactDataManager.FlushCache();
                }

                if (item.GetType() == typeof(HVWrapper.CustomHealthTypeWrapper))
                {
                    HVWrapper.CustomHealthTypeWrapper customWrapperForThing = (HVWrapper.CustomHealthTypeWrapper)item;
                    if (customWrapperForThing.WrappedObject.GetType() == typeof(HVWrapper.ExtendedProfile))
                    {
                        ExtendedProfileDataManager.FlushCache();
                    }
                }
            }
        }

        private Dictionary<Type, int> GetHVDataManagerTypeList()
        {
            Dictionary<Type, int> result = new Dictionary<Type, int>();

            result.Add( typeof(BloodGlucoseEx), (int)HVRecordItemType.BLOOD_GLUCOSE_EX ) ;
            result.Add( typeof(BloodPressureEx), (int)HVRecordItemType.BLOOD_PRESSURE_EX ) ;
            result.Add( typeof(CholesterolEx), (int)HVRecordItemType.CHOLESTEROL_EX ) ;
            result.Add( typeof(ExerciseEx), (int)HVRecordItemType.EXERCISE_EX ) ;
            result.Add( typeof(Height), (int)HVRecordItemType.HEIGHT ) ;
            result.Add( typeof(MedicationEx), (int)HVRecordItemType.MEDICATION_EX ) ;
            result.Add( typeof(WeightEx), (int)HVRecordItemType.WEIGHT_EX ) ;
            result.Add( typeof(Personal), (int)HVRecordItemType.PERSONAL ) ;
            result.Add( typeof(BasicV2), (int)HVRecordItemType.BASICV2 ) ;
            result.Add( typeof(Contact), (int)HVRecordItemType.CONTACT ) ;
            result.Add( typeof(HVWrapper.CustomHealthTypeWrapper), (int)HVRecordItemType.CUSTOM ) ;
            result.Add( typeof(CardiacProfile), (int)HVRecordItemType.CARDIAC_PROFILE ) ;
            result.Add( typeof(Person), (int)HVRecordItemType.PERSON ) ;
            result.Add( typeof(LifeGoalEx), (int)HVRecordItemType.LIFE_GOAL_EX ) ;
            result.Add( typeof(FileEx), (int)HVRecordItemType.FILE_EX ) ;

            return result;
        }

            //
            // This function will throw an HVWrapper/HVExceptions.cs if errors occur. You must handle them.
            //
        public void DownloadDataBetweenDates(List<HVDataManager> lDataManagerTypes, DateTime dtStart, DateTime dtEnd)
        {
            List<HVDataTypeFilter>  applicableFilters = new List<HVDataTypeFilter>();

            //
            // Step1 - get all the filters together as specified by lDataManagerTypes
            //

            HVDataTypeFilter filterPersonalContact = null;
            if (lDataManagerTypes.Contains(HVDataManager.PERSONAL_CONTACT))
            {
                if (m_PersonalContactDataManager.ShouldQuery())
                {
                    filterPersonalContact = m_PersonalContactDataManager.GetHVFilter();
                    applicableFilters.Add(filterPersonalContact);
                }
            }

            HVDataTypeFilter filterFileInfo = null;
            if (lDataManagerTypes.Contains(HVDataManager.FILE))
            {
                if (m_FileDataManager.ShouldQuery())
                {
                    filterFileInfo = m_FileDataManager.GetHVFilter();
                    applicableFilters.Add(filterFileInfo);
                }
            }

            HVDataTypeFilter filterEC = null;
            if (lDataManagerTypes.Contains(HVDataManager.EMERGENCY_CONTACT))
            {
                if (m_EmergencyContactDataManager.ShouldQuery())
                {
                    filterEC = m_EmergencyContactDataManager.GetHVFilter();
                    applicableFilters.Add(filterEC);
                }
            }

            HVDataTypeFilter filterBPGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.BLOOD_PRESSURE_GOAL))
            {
                if (m_BPGoalDataManager.ShouldQuery())
                {
                    filterBPGoal = m_BPGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterBPGoal);
                }
            }

            HVDataTypeFilter filterCholesterolGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.CHOLESTEROL_GOAL))
            {
                if (m_CholesterolGoalDataManager.ShouldQuery())
                {
                    filterCholesterolGoal = m_CholesterolGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterCholesterolGoal);
                }
            }

            HVDataTypeFilter filterWeightGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.WEIGHT_GOAL))
            {
                if (m_WeightGoalDataManager.ShouldQuery())
                {
                    filterWeightGoal = m_WeightGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterWeightGoal);
                }
            }

            HVDataTypeFilter filterPersonalInfo = null;
            if (lDataManagerTypes.Contains(HVDataManager.PERSONAL))
            {
                if (m_PersonalDataManager.ShouldQuery())
                {
                    filterPersonalInfo = m_PersonalDataManager.GetHVFilter();
                    applicableFilters.Add(filterPersonalInfo);
                }
            }

            HVDataTypeFilter filterExtendedProfile = null;
            if (lDataManagerTypes.Contains(HVDataManager.EXTENDED_PROFILE))
            {
                if (m_ExtendedProfileDataManager.ShouldQuery())
                {
                    filterExtendedProfile = m_ExtendedProfileDataManager.GetHVFilter();
                    applicableFilters.Add(filterExtendedProfile);
                }
            }


            HVDataTypeFilter filterAppProfile = null;
            if (lDataManagerTypes.Contains(HVDataManager.APP_PROFILE))
            {
                if (m_AppProfileDataManager.ShouldQuery())
                {
                    filterAppProfile = m_AppProfileDataManager.GetHVFilter();
                    applicableFilters.Add(filterAppProfile);
                }
            }


            HVDataTypeFilter filterCardiacProfile = null;
            if (lDataManagerTypes.Contains(HVDataManager.CARDIAC))
            {
                if (m_CardiacDataManager.ShouldQuery())
                {
                    filterCardiacProfile = m_CardiacDataManager.GetHVFilter();
                    applicableFilters.Add(filterCardiacProfile);
                }
            }

            HVDataTypeFilter filterBasicInfo = null;
            if (lDataManagerTypes.Contains(HVDataManager.BASIC))
            {
                if (m_BasicDataManager.ShouldQuery())
                {
                    filterBasicInfo = m_BasicDataManager.GetHVFilter();
                    applicableFilters.Add(filterBasicInfo);
                }
            }

            HVDataTypeFilter    filterBG = null;
            if (lDataManagerTypes.Contains(HVDataManager.BLOOD_GLUCOSE))
            {
                if (m_BloodGlucoseDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterBG = m_BloodGlucoseDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterBG);
                }
            }

            HVDataTypeFilter filterBP = null;
            if (lDataManagerTypes.Contains(HVDataManager.BLOOD_PRESSURE))
            {
                if (m_BloodPressureDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterBP = m_BloodPressureDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterBP);
                }
            }

            HVDataTypeFilter filterC = null;
            if (lDataManagerTypes.Contains(HVDataManager.CHOLESTEROL))
            {
                if (m_CholesterolDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterC = m_CholesterolDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterC);
                }
            }


            HVDataTypeFilter filterE = null;
            if (lDataManagerTypes.Contains(HVDataManager.EXERCISE))
            {
                if (m_ExerciseDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterE = m_ExerciseDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterE);
                }
            }

            HVDataTypeFilter filterEGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.EXERCISE_GOAL))
            {
                if (m_ExerciseGoalDataManager.ShouldQuery())
                {
                    filterEGoal = m_ExerciseGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterEGoal);
                }
            }

            HVDataTypeFilter filterH = null;
            if (lDataManagerTypes.Contains(HVDataManager.HEIGHT))
            {
                if (m_HeightDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterH = m_HeightDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterH);
                }
            }


            HVDataTypeFilter filterM = null;
            if (lDataManagerTypes.Contains(HVDataManager.MEDICATION))
            {
                if (m_MedicationDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterM = m_MedicationDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterM);
                }
            }

            HVDataTypeFilter filterWeight = null;
            if (lDataManagerTypes.Contains(HVDataManager.WEIGHT))
            {
                if (m_WeightDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterWeight = m_WeightDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterWeight);
                }
            }
            

            //
            // Do a check to make sure there is data to retrieve   ////////////////////////////////
            //
            if ( applicableFilters.Count == 0 )
                return ;
            

            List<WeightDataManager.WeightItem>                  dataItemsWeight = new List<WeightDataManager.WeightItem>();
            List<BloodGlucoseDataManager.BGItem>                dataItemsBG = new List<BloodGlucoseDataManager.BGItem>();
            List<BloodPressureDataManager.BPItem>               dataItemsBP = new List<BloodPressureDataManager.BPItem>();
            List<CholesterolDataManager.CholesterolItem>        dataItemsC = new List<CholesterolDataManager.CholesterolItem>();
            List<ExerciseDataManager.ExerciseItem>              dataItemsE = new List<ExerciseDataManager.ExerciseItem>();
            List<HeightDataManager.HeightItem>                  dataItemsH = new List<HeightDataManager.HeightItem>();
            List<MedicationDataManager.MedicationItem>          dataItemsM = new List<MedicationDataManager.MedicationItem>();
            PersonalDataManager.PersonalItem                    dataPersonalItem = null;
            BasicDataManager.BasicItem                          dataBasicItem = null;
            PersonalContactDataManager.ContactItem              dataContactItem = null;
            ExtendedProfileDataManager.ExtendedProfileItem      dataExtendedProfileItem = null;
            CardiacDataManager.CardiacItem                      dataCardiacProfileItem = null;
            EmergencyContactDataManager.EmergencyContactItem    dataEmergencyContactItem = null;
            BPGoalDataManager.BPGoalItem                        dataBPGoalItem = null;
            CholesterolGoalDataManager.CholesterolGoalItem      dataCholesterolGoalItem = null;
            WeightGoalDataManager.WeightGoalItem                dataWeightGoalItem = null;
            ExerciseGoalDataManager.ExerciseGoalItem            dataExerciseGoalItem = null;
            AppProfileDataManager.AppProfileItem                dataAppProfileItem = null;
            FileDataManager.FileItem                            dataFileItem = null;
            //BasicV2 basicV2Item = null;


            ReadOnlyCollection<HealthRecordItemCollection> listAll = null;

            try
            {
                listAll = WCQueries.RefactoredDownloadData(applicableFilters, Helper.GetAccessor(m_hvPersonId, m_hvRecordId));
            }
            catch (HVWrapper.HVFatalException hvfe)
            {
                throw;
            }

            Dictionary<Type, int>                           switcher = this.GetHVDataManagerTypeList() ;

            foreach (HealthRecordItemCollection healthRecordCollection in listAll)
            {
                // PJB - I am not sure if all the HealthRecordItems in a HealthRecordItemCollection are of the same type??
                // PJB - Yes they are! From: http://msdn.microsoft.com/en-us/library/jj730569.aspx
                // "If you are using the HealthVault .Net SDK, the paging logic is handled for you. The GetMatchingItems method of the HealthRecordSearcher class 
                // returns a collection of HealthRecordItemCollection objects, one for each filter in the searcher object's Filters collection."
                if (healthRecordCollection.Count == 0)
                    continue;
                if ( !switcher.ContainsKey(healthRecordCollection[0].GetType()) )
                    continue;

                switch( switcher[healthRecordCollection[0].GetType()] )
                {

                    case (int)HVRecordItemType.BLOOD_GLUCOSE_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsBG.Add( new BloodGlucoseDataManager.BGItem(m_hvPersonId, m_hvRecordId, (BloodGlucoseEx)healthRecordItem) ) ;
                        }
                        break ;

                    case (int)HVRecordItemType.BLOOD_PRESSURE_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsBP.Add( new BloodPressureDataManager.BPItem(m_hvPersonId, m_hvRecordId, (BloodPressureEx)healthRecordItem) ) ;
                        }
                        break ;
                    
                    case (int)HVRecordItemType.CHOLESTEROL_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsC.Add( new CholesterolDataManager.CholesterolItem(m_hvPersonId, m_hvRecordId, (CholesterolEx)healthRecordItem) );
                        }
                        break ;

                    case (int)HVRecordItemType.EXERCISE_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsE.Add( new ExerciseDataManager.ExerciseItem(m_hvPersonId, m_hvRecordId, (ExerciseEx)healthRecordItem) ) ;
                        }
                        break ;

                    case (int)HVRecordItemType.HEIGHT:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsH.Add( new HeightDataManager.HeightItem(m_hvPersonId, m_hvRecordId, (Height)healthRecordItem) ) ;
                        }
                        break ;

                    case (int)HVRecordItemType.MEDICATION_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsM.Add( new MedicationDataManager.MedicationItem(m_hvPersonId, m_hvRecordId, (MedicationEx)healthRecordItem) ) ;
                        }
                        break ;

                    case (int)HVRecordItemType.WEIGHT_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsWeight.Add( new WeightDataManager.WeightItem(m_hvPersonId, m_hvRecordId, (WeightEx)healthRecordItem) ) ;
                        }
                        break ;

                    case (int)HVRecordItemType.PERSONAL:
                        dataPersonalItem = new PersonalDataManager.PersonalItem(m_hvPersonId, m_hvRecordId, (Personal)(healthRecordCollection[0]));
                        break ;

                    case (int)HVRecordItemType.BASICV2:
                        dataBasicItem = new BasicDataManager.BasicItem(m_hvPersonId, m_hvRecordId, (BasicV2)(healthRecordCollection[0]));
                        break ;

                    case (int)HVRecordItemType.CONTACT:
                        dataContactItem = new PersonalContactDataManager.ContactItem(m_hvPersonId, m_hvRecordId, (Contact)(healthRecordCollection[0]));
                        break;

                    case (int)HVRecordItemType.CUSTOM:
                        {
                            HVWrapper.CustomHealthTypeWrapper customWrapperForThing = (HVWrapper.CustomHealthTypeWrapper)(healthRecordCollection[0]);
                            if( customWrapperForThing.WrappedObject != null )
                            {
                                if( customWrapperForThing.WrappedObject.GetType() == typeof(HVWrapper.ExtendedProfile) )
                                    dataExtendedProfileItem = new ExtendedProfileDataManager.ExtendedProfileItem(m_hvPersonId, m_hvRecordId, healthRecordCollection[0]);
                                else if( customWrapperForThing.WrappedObject.GetType() == typeof(HVWrapper.H360AppProfile) )
                                    dataAppProfileItem = new AppProfileDataManager.AppProfileItem( m_hvPersonId, m_hvRecordId, healthRecordCollection[0] ) ;
                            }
                        }
                        break ;

                    case (int)HVRecordItemType.CARDIAC_PROFILE:
                        dataCardiacProfileItem = new CardiacDataManager.CardiacItem(m_hvPersonId, m_hvRecordId, (CardiacProfile)(healthRecordCollection[0]));
                        break;

                    case (int)HVRecordItemType.PERSON:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            Person  pers = (Person)healthRecordItem ;
                            if( pers.PersonType == null )
                            {
                                continue ;
                            }
                            else
                            {
                                string strPersonType = (pers.PersonType.Count > 0 ) ? pers.PersonType[0].Value : pers.PersonType.Text;

                                if ( strPersonType != EmergencyContactDataManager.PersonType && strPersonType != EmergencyContactDataManager.STR_PersonType)
                                    continue ;
                            }

                            dataEmergencyContactItem = new EmergencyContactDataManager.EmergencyContactItem(m_hvPersonId, m_hvRecordId, pers );
                            break;
                        }
                        break ;

                    case (int)HVRecordItemType.LIFE_GOAL_EX:
                        {
                            LifeGoalEx  lge = (LifeGoalEx)(healthRecordCollection[0]) ;
                            switch( lge.TypeOfGoal )
                            {
                                case HVWrapper.ExtendedThingTypes.GoalType.ReduceBloodPressure:
                                    dataBPGoalItem = new BPGoalDataManager.BPGoalItem(m_hvPersonId, m_hvRecordId, lge );
                                    break ;

                                case HVWrapper.ExtendedThingTypes.GoalType.ReduceCholestrol:
                                    dataCholesterolGoalItem = new CholesterolGoalDataManager.CholesterolGoalItem(m_hvPersonId, m_hvRecordId, lge );
                                    break ;

                                case HVWrapper.ExtendedThingTypes.GoalType.ReduceWeight:
                                    dataWeightGoalItem = new WeightGoalDataManager.WeightGoalItem(m_hvPersonId, m_hvRecordId, lge );
                                    break ;

                                case HVWrapper.ExtendedThingTypes.GoalType.Exercise:
                                    dataExerciseGoalItem = new ExerciseGoalDataManager.ExerciseGoalItem(m_hvPersonId, m_hvRecordId, lge );
                                    break ;

                                default:
                                    // TODO: DO WE NEED TO LOG/THROW AN ERROR HERE?
                                    break ;
                            }
                        }
                        break ;

                    case (int)HVRecordItemType.FILE_EX:
                        dataFileItem = new FileDataManager.FileItem( true, false, m_hvPersonId, m_hvRecordId, (HVWrapper.ExtendedThingTypes.FileEx)(healthRecordCollection[0]));
                        break ;

                    default:
                        // TODO: DO WE NEED TO LOG/THROW AN ERROR HERE?
                        break ;
                }
            }

            //
            // Last bit of processing
            //
            if( filterBG != null )
            {
                m_BloodGlucoseDataManager.ArrangeData(dataItemsBG, false, filterBG.EffectiveDateMin, filterBG.EffectiveDateMax);
            }

            if (filterBP != null)
            {
                m_BloodPressureDataManager.ArrangeData(dataItemsBP, false, filterBP.EffectiveDateMin, filterBP.EffectiveDateMax);
            }

            if (filterC != null)
            {
                m_CholesterolDataManager.ArrangeData(dataItemsC, false, filterC.EffectiveDateMin, filterC.EffectiveDateMax);
            }

            if (filterE != null)
            {
                m_ExerciseDataManager.ArrangeData(dataItemsE, false, filterE.EffectiveDateMin, filterE.EffectiveDateMax);
            }

            if (filterH != null)
            {
                m_HeightDataManager.ArrangeData(dataItemsH, false, filterH.EffectiveDateMin, filterH.EffectiveDateMax);
            }

            if (filterM != null)
            {
                m_MedicationDataManager.ArrangeData(dataItemsM, false, filterM.EffectiveDateMin, filterM.EffectiveDateMax);
            }

            if (filterWeight != null)
            {
                m_WeightDataManager.ArrangeData(dataItemsWeight, false, filterWeight.EffectiveDateMin, filterWeight.EffectiveDateMax);
            }

            if (filterPersonalInfo != null)
            {
                m_PersonalDataManager.Item = dataPersonalItem;
            }

            if ( filterBasicInfo != null)
            {
                m_BasicDataManager.Item = dataBasicItem;
                //m_BasicV2DataManager = basicV2Item;
            }

            if ( filterPersonalContact != null )
            {
                m_PersonalContactDataManager.Item = dataContactItem;
            }

            if (filterExtendedProfile != null)
            {
                m_ExtendedProfileDataManager.Item = dataExtendedProfileItem;
            }

            if ( filterAppProfile != null )
            {
                m_AppProfileDataManager.Item = dataAppProfileItem;
            }

            if ( filterCardiacProfile != null )
            {
                m_CardiacDataManager.Item = dataCardiacProfileItem;
            }

            if (filterEC != null )
            {
                m_EmergencyContactDataManager.Item = dataEmergencyContactItem;
            }

            if (filterBPGoal != null )
            {
                m_BPGoalDataManager.Item = dataBPGoalItem;
            }

            if ( filterCholesterolGoal != null )
            {
                m_CholesterolGoalDataManager.Item = dataCholesterolGoalItem;
            }

            if (filterWeightGoal != null )
            {
                m_WeightGoalDataManager.Item = dataWeightGoalItem;
            }

            if (filterFileInfo != null)
            {
                m_FileDataManager.Item = dataFileItem;
            }

            if (filterEGoal != null)
            {
                m_ExerciseGoalDataManager.Item = dataExerciseGoalItem;
            }
        }


        public bool DownloadDataBetweenDatesforIVR(List<string> strManagerTypes, DateTime dtStart, DateTime dtEnd, bool isOnline = true)
        {

            //Returns true if there are atleast one record within the specified dates
            List<HVDataTypeFilter> applicableFilters = new List<HVDataTypeFilter>();

            bool bShouldQueryPersonalContact = false;
            if (strManagerTypes.Contains(typeof(PersonalContactDataManager).ToString()))
            {
                bShouldQueryPersonalContact = m_PersonalContactDataManager.ShouldQuery();
            }

            bool bShouldQueryPersonalInfo = false;
            if (strManagerTypes.Contains(typeof(PersonalDataManager).ToString()))
            {
                bShouldQueryPersonalInfo = m_PersonalDataManager.ShouldQuery();
            }

            bool bShouldQueryBloodGlucose = false;
            if (strManagerTypes.Contains(typeof(BloodGlucoseDataManager).ToString()))
            {
                bShouldQueryBloodGlucose = m_BloodGlucoseDataManager.ShouldQuery(dtStart, dtEnd);
            }

            bool bShouldQueryBloodPressure = false;
            if (strManagerTypes.Contains(typeof(BloodPressureDataManager).ToString()))
            {
                bShouldQueryBloodPressure = m_BloodPressureDataManager.ShouldQuery(dtStart, dtEnd);
            }

            bool bShouldQueryCholesterol = false;
            if (strManagerTypes.Contains(typeof(CholesterolDataManager).ToString()))
            {
                bShouldQueryCholesterol = m_CholesterolDataManager.ShouldQuery(dtStart, dtEnd);
            }

            bool bShouldQueryExercise = false;
            if (strManagerTypes.Contains(typeof(ExerciseDataManager).ToString()))
            {
                bShouldQueryExercise = m_ExerciseDataManager.ShouldQuery(dtStart, dtEnd);
            }

            bool bShouldQueryMedication = false;
            if (strManagerTypes.Contains(typeof(MedicationDataManager).ToString()))
            {
                bShouldQueryMedication = m_MedicationDataManager.ShouldQuery(dtStart, dtEnd);
            }

            bool bShouldQueryWeight = false;
            if (strManagerTypes.Contains(typeof(WeightDataManager).ToString()))
            {
                bShouldQueryWeight = m_WeightDataManager.ShouldQuery(dtStart, dtEnd);
            }

            if (!bShouldQueryBloodGlucose
                && !bShouldQueryBloodPressure
                && !bShouldQueryCholesterol
                && !bShouldQueryExercise
                && !bShouldQueryPersonalContact
                && !bShouldQueryPersonalInfo
                && !bShouldQueryMedication
                && !bShouldQueryWeight
                )
                return true;

            HVDataTypeFilter filterBG = null;
            if (bShouldQueryBloodGlucose)
            {
                filterBG = m_BloodGlucoseDataManager.GetHVFilter(dtStart, dtEnd);
                applicableFilters.Add(filterBG);
            }

            HVDataTypeFilter filterBP = null;
            if (bShouldQueryBloodPressure)
            {
                filterBP = m_BloodPressureDataManager.GetHVFilter(dtStart, dtEnd);
                applicableFilters.Add(filterBP);
            }

            HVDataTypeFilter filterC = null;
            if (bShouldQueryCholesterol)
            {
                filterC = m_CholesterolDataManager.GetHVFilter(dtStart, dtEnd);
                applicableFilters.Add(filterC);
            }

            HVDataTypeFilter filterE = null;
            if (bShouldQueryExercise)
            {
                filterE = m_ExerciseDataManager.GetHVFilter(dtStart, dtEnd);
                applicableFilters.Add(filterE);
            }

            HVDataTypeFilter filterM = null;
            if (bShouldQueryMedication)
            {
                filterM = m_MedicationDataManager.GetHVFilter(dtStart, dtEnd);
                applicableFilters.Add(filterM);
            }

            HVDataTypeFilter filterWeight = null;
            if (bShouldQueryWeight)
            {
                filterWeight = m_WeightDataManager.GetHVFilter(dtStart, dtEnd);
                applicableFilters.Add(filterWeight);
            }

            HVDataTypeFilter filterPersonalInfo = null;
            if (bShouldQueryPersonalInfo)
            {
                filterPersonalInfo = m_PersonalDataManager.GetHVFilter();
                applicableFilters.Add(filterPersonalInfo);
            }

            HVDataTypeFilter filterPersonalContact = null;
            if (bShouldQueryPersonalContact)
            {
                filterPersonalContact = m_PersonalContactDataManager.GetHVFilter();
                applicableFilters.Add(filterPersonalContact);
            }

            List<WeightDataManager.WeightItem> dataItemsWeight = new List<WeightDataManager.WeightItem>();
            List<BloodGlucoseDataManager.BGItem> dataItemsBG = new List<BloodGlucoseDataManager.BGItem>();
            List<BloodPressureDataManager.BPItem> dataItemsBP = new List<BloodPressureDataManager.BPItem>();
            List<CholesterolDataManager.CholesterolItem> dataItemsC = new List<CholesterolDataManager.CholesterolItem>();
            List<ExerciseDataManager.ExerciseItem> dataItemsE = new List<ExerciseDataManager.ExerciseItem>();
            List<MedicationDataManager.MedicationItem> dataItemsM = new List<MedicationDataManager.MedicationItem>();
            //PersonalDataManager.PersonalItem dataPersonalItem = null;
            //PersonalContactDataManager.ContactItem dataContactItem = null;


            ReadOnlyCollection<HealthRecordItemCollection> listAll = null;

            if (isOnline)
            {
                listAll = WCQueries.DownloadData(applicableFilters, Helper.GetAccessor(m_hvPersonId, m_hvRecordId));
            }
            else
            {
                listAll = WCQueries.DownloadData(applicableFilters, Helper.GetAccessor(isOnline, m_hvPersonId, m_hvRecordId));
            }
            foreach (HealthRecordItemCollection healthRecordCollection in listAll)
            {
                if (healthRecordCollection.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public string DownloadDataBetweenDatesforCampaign(int userHealthRecordID, List<string> strManagerTypes, DateTime dtStart, DateTime dtEnd, bool isOnline)
        {
            string firstName = "";
            string lastName = "";
            string eMailAddress = "";
            bool isDataExist = false;
            string zipcode = "";
            string retVal = "";
            string DOB = "";
            int yearOfBirth = 0;


            //Returns true if there are atleast one record within the specified dates
            List<HVDataTypeFilter> applicableFilters = new List<HVDataTypeFilter>();


            bool bShouldQueryPersonalContact = false;
            if (strManagerTypes.Contains(typeof(PersonalContactDataManager).ToString()))
            {
                bShouldQueryPersonalContact = m_PersonalContactDataManager.ShouldQuery();
            }

            bool bShouldQueryPersonalInfo = false;
            if (strManagerTypes.Contains(typeof(PersonalDataManager).ToString()))
            {
                bShouldQueryPersonalInfo = m_PersonalDataManager.ShouldQuery();
            }

            bool bShouldQueryBasicInfo = false;
            if (strManagerTypes.Contains(typeof(BasicDataManager).ToString()))
            {
                bShouldQueryBasicInfo = m_BasicDataManager.ShouldQuery();
            }

            if (!bShouldQueryPersonalContact
                && !bShouldQueryPersonalInfo
                && !bShouldQueryBasicInfo
                )
                return "";

            HVDataTypeFilter filterPersonalInfo = null;
            if (bShouldQueryPersonalInfo)
            {
                filterPersonalInfo = m_PersonalDataManager.GetHVFilter();
                applicableFilters.Add(filterPersonalInfo);
            }

            HVDataTypeFilter filterBasicInfo = null;
            if (bShouldQueryBasicInfo)
            {
                filterBasicInfo = m_BasicDataManager.GetHVFilter();
                applicableFilters.Add(filterBasicInfo);
            }

            HVDataTypeFilter filterPersonalContact = null;
            if (bShouldQueryPersonalContact)
            {
                filterPersonalContact = m_PersonalContactDataManager.GetHVFilter();
                applicableFilters.Add(filterPersonalContact);
            }


            PersonalDataManager.PersonalItem dataPersonalItem = null;
            PersonalContactDataManager.ContactItem dataContactItem = null;
            BasicV2 dataBasicItem = null;

            ReadOnlyCollection<HealthRecordItemCollection> listAll = null;

            if (isOnline)
            {
                listAll = WCQueries.DownloadData(applicableFilters, Helper.GetAccessor(m_hvPersonId, m_hvRecordId));
            }
            else
            {
                listAll = WCQueries.DownloadData(applicableFilters, Helper.GetAccessor(isOnline, m_hvPersonId, m_hvRecordId));
            }

            foreach (HealthRecordItemCollection healthRecordCollection in listAll)
            {
                foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                {
                    if (bShouldQueryPersonalInfo && healthRecordItem.GetType() == typeof(Personal))
                    {
                        dataPersonalItem = new PersonalDataManager.PersonalItem(m_hvPersonId, m_hvRecordId, (Personal)healthRecordItem);
                        if (dataPersonalItem != null)
                        {
                            if ((dataPersonalItem.FirstName != null) && (dataPersonalItem.LastName != null))
                            {
                                firstName = dataPersonalItem.FirstName.Value;
                                lastName = dataPersonalItem.LastName.Value;
                                DOB = dataPersonalItem.DateOfBirth.Value.ToString();
                                isDataExist = true;
                            }
                        }
                        break;
                    }
                    else if (bShouldQueryBasicInfo && healthRecordItem.GetType() == typeof(BasicV2))
                    {
                        dataBasicItem = (BasicV2)healthRecordItem;
                        if (dataBasicItem != null)
                        {
                            if (dataBasicItem.PostalCode != null)
                            {
                                zipcode = dataBasicItem.PostalCode;
                                isDataExist = true;
                            }
                            if (dataBasicItem.BirthYear != null)
                            {
                                yearOfBirth = dataBasicItem.BirthYear.Value;
                            }
                        }
                        break;
                    }
                    else if (bShouldQueryPersonalContact && healthRecordItem.GetType() == typeof(Contact))
                    {
                        dataContactItem = new PersonalContactDataManager.ContactItem(m_hvPersonId, m_hvRecordId, (Contact)healthRecordItem);
                        if (dataContactItem != null)
                        {
                            if ((dataContactItem.EmailList != null) && (dataContactItem.EmailList.Count > 0))
                            {
                                if (dataContactItem.EmailList[0].Email != null)
                                {
                                    eMailAddress = dataContactItem.EmailList[0].Email.Value;
                                    isDataExist = true;
                                }
                            }
                            //if (dataContactItem.AddressList!= null)
                            //{
                            //    if (dataContactItem.AddressList.Count > 0)
                            //    {
                            //        if (dataContactItem.AddressList[0].Zip != null)
                            //        {
                            //            zipcode = dataContactItem.AddressList[0].Zip.Value;
                            //            isDataExist = true;
                            //            break;
                            //        }
                            //    }
                            //}
                        }
                    }
                }
            }
            if (isDataExist)
            {
                retVal = firstName + "!" + lastName + "!" + eMailAddress + "!" + zipcode + "!" + DOB + "!" + yearOfBirth.ToString() + "!";

            }
            return retVal;
        }


        public static AllItemManagerRefactored GetItemManagersForRecordId(Guid hvpersonid, Guid hvrecordid, ref List<AllItemManagerRefactored> masterList)
        {

            //
            // If a master list of AllItemManagers is being used, then check to see if it is already in there.
            // 

            if (masterList != null)
            {
                var existing = masterList.Where(x => x.m_hvPersonId == hvpersonid && x.m_hvRecordId == hvrecordid).SingleOrDefault();
                if (existing != null)
                {
                    return existing;
                }
            }

            //
            // If we are here, we need to create a new AllItemManager
            //
            
            AllItemManagerRefactored mgrNew = new AllItemManagerRefactored(hvpersonid, hvrecordid);
            if (masterList != null)
            {
                masterList.Add(mgrNew);
                // PJB - Not sure why this was done this way. I don't think mgrNew is copied when added,
                // so it must be to enforce the singleton via .Single()
                mgrNew = masterList.Where(x => x.m_hvPersonId == hvpersonid && x.m_hvRecordId == hvrecordid).Single();
            }

            return mgrNew;
        }

        //
        // This function will throw an HVWrapper/HVExceptions.cs if errors occur. You must handle them.
        // nbItems represents how many most recent items are desired per manager
        //
        public void DownloadMostRecentData(List<HVDataManager> lDataManagerTypes, DateTime dtStart, DateTime dtEnd, int nbItems)
        {
            List<HVDataTypeFilter> applicableFilters = new List<HVDataTypeFilter>();

            //
            // Step1 - get all the filters together as specified by lDataManagerTypes
            //

            HVDataTypeFilter filterPersonalContact = null;
            if (lDataManagerTypes.Contains(HVDataManager.PERSONAL_CONTACT))
            {
                if (m_PersonalContactDataManager.ShouldQuery())
                {
                    filterPersonalContact = m_PersonalContactDataManager.GetHVFilter();
                    applicableFilters.Add(filterPersonalContact);
                }
            }

            HVDataTypeFilter filterFileInfo = null;
            if (lDataManagerTypes.Contains(HVDataManager.FILE))
            {
                if (m_FileDataManager.ShouldQuery())
                {
                    filterFileInfo = m_FileDataManager.GetHVFilter();
                    applicableFilters.Add(filterFileInfo);
                }
            }

            HVDataTypeFilter filterEC = null;
            if (lDataManagerTypes.Contains(HVDataManager.EMERGENCY_CONTACT))
            {
                if (m_EmergencyContactDataManager.ShouldQuery())
                {
                    filterEC = m_EmergencyContactDataManager.GetHVFilter();
                    applicableFilters.Add(filterEC);
                }
            }

            HVDataTypeFilter filterBPGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.BLOOD_PRESSURE_GOAL))
            {
                if (m_BPGoalDataManager.ShouldQuery())
                {
                    filterBPGoal = m_BPGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterBPGoal);
                }
            }

            HVDataTypeFilter filterCholesterolGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.CHOLESTEROL_GOAL))
            {
                if (m_CholesterolGoalDataManager.ShouldQuery())
                {
                    filterCholesterolGoal = m_CholesterolGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterCholesterolGoal);
                }
            }

            HVDataTypeFilter filterWeightGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.WEIGHT_GOAL))
            {
                if (m_WeightGoalDataManager.ShouldQuery())
                {
                    filterWeightGoal = m_WeightGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterWeightGoal);
                }
            }

            HVDataTypeFilter filterPersonalInfo = null;
            if (lDataManagerTypes.Contains(HVDataManager.PERSONAL))
            {
                if (m_PersonalDataManager.ShouldQuery())
                {
                    filterPersonalInfo = m_PersonalDataManager.GetHVFilter();
                    applicableFilters.Add(filterPersonalInfo);
                }
            }

            HVDataTypeFilter filterExtendedProfile = null;
            if (lDataManagerTypes.Contains(HVDataManager.EXTENDED_PROFILE))
            {
                if (m_ExtendedProfileDataManager.ShouldQuery())
                {
                    filterExtendedProfile = m_ExtendedProfileDataManager.GetHVFilter();
                    applicableFilters.Add(filterExtendedProfile);
                }
            }


            HVDataTypeFilter filterAppProfile = null;
            if (lDataManagerTypes.Contains(HVDataManager.APP_PROFILE))
            {
                if (m_AppProfileDataManager.ShouldQuery())
                {
                    filterAppProfile = m_AppProfileDataManager.GetHVFilter();
                    applicableFilters.Add(filterAppProfile);
                }
            }


            HVDataTypeFilter filterCardiacProfile = null;
            if (lDataManagerTypes.Contains(HVDataManager.CARDIAC))
            {
                if (m_CardiacDataManager.ShouldQuery())
                {
                    filterCardiacProfile = m_CardiacDataManager.GetHVFilter();
                    applicableFilters.Add(filterCardiacProfile);
                }
            }

            HVDataTypeFilter filterBasicInfo = null;
            if (lDataManagerTypes.Contains(HVDataManager.BASIC))
            {
                if (m_BasicDataManager.ShouldQuery())
                {
                    filterBasicInfo = m_BasicDataManager.GetHVFilter();
                    applicableFilters.Add(filterBasicInfo);
                }
            }

            HVDataTypeFilter filterBG = null;
            if (lDataManagerTypes.Contains(HVDataManager.BLOOD_GLUCOSE))
            {
                if (m_BloodGlucoseDataManager.ShouldQuery())
                {
                    filterBG = m_BloodGlucoseDataManager.GetHVFilter(dtStart, dtEnd);
                    filterBG.MaxItemsToReturn = nbItems;
                    applicableFilters.Add(filterBG);
                }
            }

            HVDataTypeFilter filterBP = null;
            if (lDataManagerTypes.Contains(HVDataManager.BLOOD_PRESSURE))
            {
                if (m_BloodPressureDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterBP = m_BloodPressureDataManager.GetHVFilter(dtStart, dtEnd);
                    filterBG.MaxItemsToReturn = nbItems;
                    applicableFilters.Add(filterBP);
                }
            }

            HVDataTypeFilter filterC = null;
            if (lDataManagerTypes.Contains(HVDataManager.CHOLESTEROL))
            {
                if (m_CholesterolDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterC = m_CholesterolDataManager.GetHVFilter(dtStart, dtEnd);
                    filterC.MaxItemsToReturn = nbItems;
                    applicableFilters.Add(filterC);
                }
            }


            HVDataTypeFilter filterE = null;
            if (lDataManagerTypes.Contains(HVDataManager.EXERCISE))
            {
                if (m_ExerciseDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterE = m_ExerciseDataManager.GetHVFilter(dtStart, dtEnd);
                    filterE.MaxItemsToReturn = nbItems;
                    applicableFilters.Add(filterE);
                }
            }

            HVDataTypeFilter filterEGoal = null;
            if (lDataManagerTypes.Contains(HVDataManager.EXERCISE_GOAL))
            {
                if (m_ExerciseGoalDataManager.ShouldQuery())
                {
                    filterEGoal = m_ExerciseGoalDataManager.GetHVFilter();
                    applicableFilters.Add(filterEGoal);
                }
            }

            HVDataTypeFilter filterH = null;
            if (lDataManagerTypes.Contains(HVDataManager.HEIGHT))
            {
                if (m_HeightDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterH = m_HeightDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterH);
                }
            }


            HVDataTypeFilter filterM = null;
            if (lDataManagerTypes.Contains(HVDataManager.MEDICATION))
            {
                if (m_MedicationDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterM = m_MedicationDataManager.GetHVFilter(dtStart, dtEnd);
                    applicableFilters.Add(filterM);
                }
            }

            HVDataTypeFilter filterWeight = null;
            if (lDataManagerTypes.Contains(HVDataManager.WEIGHT))
            {
                if (m_WeightDataManager.ShouldQuery(dtStart, dtEnd))
                {
                    filterWeight = m_WeightDataManager.GetHVFilter(dtStart, dtEnd);
                    filterWeight.MaxItemsToReturn = nbItems;
                    applicableFilters.Add(filterWeight);
                }
            }


            //
            // Do a check to make sure there is data to retrieve   ////////////////////////////////
            //
            if (applicableFilters.Count == 0)
                return;


            List<WeightDataManager.WeightItem> dataItemsWeight = new List<WeightDataManager.WeightItem>();
            List<BloodGlucoseDataManager.BGItem> dataItemsBG = new List<BloodGlucoseDataManager.BGItem>();
            List<BloodPressureDataManager.BPItem> dataItemsBP = new List<BloodPressureDataManager.BPItem>();
            List<CholesterolDataManager.CholesterolItem> dataItemsC = new List<CholesterolDataManager.CholesterolItem>();
            List<ExerciseDataManager.ExerciseItem> dataItemsE = new List<ExerciseDataManager.ExerciseItem>();
            List<HeightDataManager.HeightItem> dataItemsH = new List<HeightDataManager.HeightItem>();
            List<MedicationDataManager.MedicationItem> dataItemsM = new List<MedicationDataManager.MedicationItem>();
            PersonalDataManager.PersonalItem dataPersonalItem = null;
            BasicDataManager.BasicItem dataBasicItem = null;
            PersonalContactDataManager.ContactItem dataContactItem = null;
            ExtendedProfileDataManager.ExtendedProfileItem dataExtendedProfileItem = null;
            CardiacDataManager.CardiacItem dataCardiacProfileItem = null;
            EmergencyContactDataManager.EmergencyContactItem dataEmergencyContactItem = null;
            BPGoalDataManager.BPGoalItem dataBPGoalItem = null;
            CholesterolGoalDataManager.CholesterolGoalItem dataCholesterolGoalItem = null;
            WeightGoalDataManager.WeightGoalItem dataWeightGoalItem = null;
            ExerciseGoalDataManager.ExerciseGoalItem dataExerciseGoalItem = null;
            AppProfileDataManager.AppProfileItem dataAppProfileItem = null;
            FileDataManager.FileItem dataFileItem = null;
            //BasicV2 basicV2Item = null;


            ReadOnlyCollection<HealthRecordItemCollection> listAll = null;

            try
            {
                listAll = WCQueries.RefactoredDownloadData(applicableFilters, Helper.GetAccessor(m_hvPersonId, m_hvRecordId));
            }
            catch (HVWrapper.HVFatalException hvfe)
            {
                throw;
            }

            Dictionary<Type, int> switcher = this.GetHVDataManagerTypeList();

            foreach (HealthRecordItemCollection healthRecordCollection in listAll)
            {
                // PJB - I am not sure if all the HealthRecordItems in a HealthRecordItemCollection are of the same type??
                // PJB - Yes they are! From: http://msdn.microsoft.com/en-us/library/jj730569.aspx
                // "If you are using the HealthVault .Net SDK, the paging logic is handled for you. The GetMatchingItems method of the HealthRecordSearcher class 
                // returns a collection of HealthRecordItemCollection objects, one for each filter in the searcher object's Filters collection."
                if (healthRecordCollection.Count == 0)
                    continue;
                if (!switcher.ContainsKey(healthRecordCollection[0].GetType()))
                    continue;

                switch (switcher[healthRecordCollection[0].GetType()])
                {

                    case (int)HVRecordItemType.BLOOD_GLUCOSE_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsBG.Add(new BloodGlucoseDataManager.BGItem(m_hvPersonId, m_hvRecordId, (BloodGlucoseEx)healthRecordItem));
                        }
                        break;

                    case (int)HVRecordItemType.BLOOD_PRESSURE_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsBP.Add(new BloodPressureDataManager.BPItem(m_hvPersonId, m_hvRecordId, (BloodPressureEx)healthRecordItem));
                        }
                        break;

                    case (int)HVRecordItemType.CHOLESTEROL_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsC.Add(new CholesterolDataManager.CholesterolItem(m_hvPersonId, m_hvRecordId, (CholesterolEx)healthRecordItem));
                        }
                        break;

                    case (int)HVRecordItemType.EXERCISE_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsE.Add(new ExerciseDataManager.ExerciseItem(m_hvPersonId, m_hvRecordId, (ExerciseEx)healthRecordItem));
                        }
                        break;

                    case (int)HVRecordItemType.HEIGHT:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsH.Add(new HeightDataManager.HeightItem(m_hvPersonId, m_hvRecordId, (Height)healthRecordItem));
                        }
                        break;

                    case (int)HVRecordItemType.MEDICATION_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsM.Add(new MedicationDataManager.MedicationItem(m_hvPersonId, m_hvRecordId, (MedicationEx)healthRecordItem));
                        }
                        break;

                    case (int)HVRecordItemType.WEIGHT_EX:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            dataItemsWeight.Add(new WeightDataManager.WeightItem(m_hvPersonId, m_hvRecordId, (WeightEx)healthRecordItem));
                        }
                        break;

                    case (int)HVRecordItemType.PERSONAL:
                        dataPersonalItem = new PersonalDataManager.PersonalItem(m_hvPersonId, m_hvRecordId, (Personal)(healthRecordCollection[0]));
                        break;

                    case (int)HVRecordItemType.BASICV2:
                        dataBasicItem = new BasicDataManager.BasicItem(m_hvPersonId, m_hvRecordId, (BasicV2)(healthRecordCollection[0]));
                        break;

                    case (int)HVRecordItemType.CONTACT:
                        dataContactItem = new PersonalContactDataManager.ContactItem(m_hvPersonId, m_hvRecordId, (Contact)(healthRecordCollection[0]));
                        break;

                    case (int)HVRecordItemType.CUSTOM:
                        {
                            HVWrapper.CustomHealthTypeWrapper customWrapperForThing = (HVWrapper.CustomHealthTypeWrapper)(healthRecordCollection[0]);
                            if (customWrapperForThing.WrappedObject != null)
                            {
                                if (customWrapperForThing.WrappedObject.GetType() == typeof(HVWrapper.ExtendedProfile))
                                    dataExtendedProfileItem = new ExtendedProfileDataManager.ExtendedProfileItem(m_hvPersonId, m_hvRecordId, healthRecordCollection[0]);
                                else if (customWrapperForThing.WrappedObject.GetType() == typeof(HVWrapper.H360AppProfile))
                                    dataAppProfileItem = new AppProfileDataManager.AppProfileItem(m_hvPersonId, m_hvRecordId, healthRecordCollection[0]);
                            }
                        }
                        break;

                    case (int)HVRecordItemType.CARDIAC_PROFILE:
                        dataCardiacProfileItem = new CardiacDataManager.CardiacItem(m_hvPersonId, m_hvRecordId, (CardiacProfile)(healthRecordCollection[0]));
                        break;

                    case (int)HVRecordItemType.PERSON:
                        foreach (HealthRecordItem healthRecordItem in healthRecordCollection)
                        {
                            Person pers = (Person)healthRecordItem;
                            if (pers.PersonType == null)
                            {
                                continue;
                            }
                            else
                            {
                                string strPersonType = (pers.PersonType.Count > 0) ? pers.PersonType[0].Value : pers.PersonType.Text;

                                if (strPersonType != EmergencyContactDataManager.PersonType && strPersonType != EmergencyContactDataManager.STR_PersonType)
                                    continue;
                            }

                            dataEmergencyContactItem = new EmergencyContactDataManager.EmergencyContactItem(m_hvPersonId, m_hvRecordId, pers);
                            break;
                        }
                        break;

                    case (int)HVRecordItemType.LIFE_GOAL_EX:
                        {
                            LifeGoalEx lge = (LifeGoalEx)(healthRecordCollection[0]);
                            switch (lge.TypeOfGoal)
                            {
                                case HVWrapper.ExtendedThingTypes.GoalType.ReduceBloodPressure:
                                    dataBPGoalItem = new BPGoalDataManager.BPGoalItem(m_hvPersonId, m_hvRecordId, lge);
                                    break;

                                case HVWrapper.ExtendedThingTypes.GoalType.ReduceCholestrol:
                                    dataCholesterolGoalItem = new CholesterolGoalDataManager.CholesterolGoalItem(m_hvPersonId, m_hvRecordId, lge);
                                    break;

                                case HVWrapper.ExtendedThingTypes.GoalType.ReduceWeight:
                                    dataWeightGoalItem = new WeightGoalDataManager.WeightGoalItem(m_hvPersonId, m_hvRecordId, lge);
                                    break;

                                case HVWrapper.ExtendedThingTypes.GoalType.Exercise:
                                    dataExerciseGoalItem = new ExerciseGoalDataManager.ExerciseGoalItem(m_hvPersonId, m_hvRecordId, lge);
                                    break;

                                default:
                                    // TODO: DO WE NEED TO LOG/THROW AN ERROR HERE?
                                    break;
                            }
                        }
                        break;

                    case (int)HVRecordItemType.FILE_EX:
                        dataFileItem = new FileDataManager.FileItem(true, false, m_hvPersonId, m_hvRecordId, (HVWrapper.ExtendedThingTypes.FileEx)(healthRecordCollection[0]));
                        break;

                    default:
                        // TODO: DO WE NEED TO LOG/THROW AN ERROR HERE?
                        break;
                }
            }

            //
            // Last bit of processing
            //
            if (filterBG != null)
            {
                m_BloodGlucoseDataManager.ArrangeData(dataItemsBG, false, filterBG.EffectiveDateMin, filterBG.EffectiveDateMax);
            }

            if (filterBP != null)
            {
                m_BloodPressureDataManager.ArrangeData(dataItemsBP, false, filterBP.EffectiveDateMin, filterBP.EffectiveDateMax);
            }

            if (filterC != null)
            {
                m_CholesterolDataManager.ArrangeData(dataItemsC, false, filterC.EffectiveDateMin, filterC.EffectiveDateMax);
            }

            if (filterE != null)
            {
                m_ExerciseDataManager.ArrangeData(dataItemsE, false, filterE.EffectiveDateMin, filterE.EffectiveDateMax);
            }

            if (filterH != null)
            {
                m_HeightDataManager.ArrangeData(dataItemsH, false, filterH.EffectiveDateMin, filterH.EffectiveDateMax);
            }

            if (filterM != null)
            {
                m_MedicationDataManager.ArrangeData(dataItemsM, false, filterM.EffectiveDateMin, filterM.EffectiveDateMax);
            }

            if (filterWeight != null)
            {
                m_WeightDataManager.ArrangeData(dataItemsWeight, false, filterWeight.EffectiveDateMin, filterWeight.EffectiveDateMax);
            }

            if (filterPersonalInfo != null)
            {
                m_PersonalDataManager.Item = dataPersonalItem;
            }

            if (filterBasicInfo != null)
            {
                m_BasicDataManager.Item = dataBasicItem;
                //m_BasicV2DataManager = basicV2Item;
            }

            if (filterPersonalContact != null)
            {
                m_PersonalContactDataManager.Item = dataContactItem;
            }

            if (filterExtendedProfile != null)
            {
                m_ExtendedProfileDataManager.Item = dataExtendedProfileItem;
            }

            if (filterAppProfile != null)
            {
                m_AppProfileDataManager.Item = dataAppProfileItem;
            }

            if (filterCardiacProfile != null)
            {
                m_CardiacDataManager.Item = dataCardiacProfileItem;
            }

            if (filterEC != null)
            {
                m_EmergencyContactDataManager.Item = dataEmergencyContactItem;
            }

            if (filterBPGoal != null)
            {
                m_BPGoalDataManager.Item = dataBPGoalItem;
            }

            if (filterCholesterolGoal != null)
            {
                m_CholesterolGoalDataManager.Item = dataCholesterolGoalItem;
            }

            if (filterWeightGoal != null)
            {
                m_WeightGoalDataManager.Item = dataWeightGoalItem;
            }

            if (filterFileInfo != null)
            {
                m_FileDataManager.Item = dataFileItem;
            }

            if (filterEGoal != null)
            {
                m_ExerciseGoalDataManager.Item = dataExerciseGoalItem;
            }
        }


    }
}

