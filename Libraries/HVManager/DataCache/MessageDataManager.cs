﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;

    using HVWrapper.ExtendedThingTypes;
    using HVWrapper;
    
    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "MessageDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class MessageDataManager : LocalThingTypeDataManager<MessageDataManager.MessageItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class MessageItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<int?> MessageID { get; private set; }
            public PossibleUpdate<bool?> IsSentByProvider { get; private set; }
            public PossibleUpdate<DateTime?> DateSent { get; private set; }
            public PossibleUpdate<string> Subject { get; private set; }
            public PossibleUpdate<string> MessageBody { get; private set; }
            public PossibleUpdate<int?> ProviderID { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (MessageID.IsDirty
                        || IsSentByProvider.IsDirty
                        || DateSent.IsDirty
                        || Subject.IsDirty
                        || MessageBody.IsDirty
                        || ProviderID.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                MessageID.ResetDirtyFlag();
                IsSentByProvider.ResetDirtyFlag();
                DateSent.ResetDirtyFlag();
                Subject.ResetDirtyFlag();
                MessageBody.ResetDirtyFlag();
                ProviderID.ResetDirtyFlag();
            }

            public MessageItem()
            {
                MessageID = new PossibleUpdate<int?>();
                IsSentByProvider = new PossibleUpdate<bool?>();
                DateSent = new PossibleUpdate<DateTime?>();
                Subject = new PossibleUpdate<string>();
                MessageBody = new PossibleUpdate<string>();
                ProviderID = new PossibleUpdate<int?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public MessageItem(Guid gPersonGUID, Guid gRecordGUID, HealthRecordItem item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                CustomHealthTypeWrapper customWrapperForThing = (CustomHealthTypeWrapper)item;

                HVWrapper.Message customThing = (HVWrapper.Message)customWrapperForThing.WrappedObject;

                MessageID = new PossibleUpdate<int?>(customThing.MessageID);
                IsSentByProvider = new PossibleUpdate<bool?>(customThing.IsSentByProvider);
                DateSent = new PossibleUpdate<DateTime?>(customThing.DateSent);
                Subject = new PossibleUpdate<string>(customThing.Subject);
                MessageBody = new PossibleUpdate<string>(customThing.MessageBody);
                ProviderID = new PossibleUpdate<int?>(customThing.ProviderID);

                ResetDirtyFlag();
            }
        }

        DateTime dtStartDefault = DateTime.MinValue;

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(customThingTypes, HVWrapper.Message.XPathFilterForQuery, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public MessageDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<MessageItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {
            List<MessageItem> dataItemsMsg = new List<MessageItem>();

            HealthRecordItemCollection dataItems;

            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               customThingTypes,
               startDate,
               endDate,
               thingIDs,
               HVWrapper.Message.XPathFilterForQuery,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

            foreach (HealthRecordItem thing in dataItems)
            {
                MessageItem item = new MessageItem(PersonGUID, RecordGUID, thing);
                dataItemsMsg.Add(item);
            }

            return dataItemsMsg;
        }

        public List<MessageItem> GetAllMessagesForProvider(int iProviderID)
        {
            DateTime dtStart = DateTime.MinValue;
            DateTime dtEnd = DateTime.MaxValue;

            if (m_earliestDate == dtStart && m_latestDate == dtEnd)
                return GetDataBetweenDates(dtStart, dtEnd).ToList();

            List<MessageItem> dataItemsMsg = new List<MessageItem>();

            HealthRecordItemCollection dataItems;

            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            bool didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               customThingTypes,
               dtStart,
               dtEnd,
               null,
               string.Format("{0}[provider-id='{1}']",HVWrapper.Message.XPathFilterForQuery,iProviderID),
               1000,
               true,
               out dataItems,
               out didResultsOverflow);

            foreach (HealthRecordItem thing in dataItems)
            {
                MessageItem item = new MessageItem(PersonGUID, RecordGUID, thing);
                dataItemsMsg.Add(item);
            }

            ArrangeData(dataItemsMsg, didResultsOverflow, DateTime.MinValue, DateTime.MaxValue);

            return dataItemsMsg;
        }

        public MessageItem CreateItem(MessageItem itemEdits)
        {
            HVWrapper.Message messageThing = new HVWrapper.Message();
            CustomHealthTypeWrapper wrapper = new CustomHealthTypeWrapper(messageThing);

            messageThing.When = new HealthServiceDateTime(DateTime.Now);

            _UpdateFields(messageThing, itemEdits);

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);
            
            accessor.NewItem(wrapper);

            if (IsDateInsideCacheWindow(messageThing.When.ToDateTime()))
            {
                FetchAndStoreItemIfDateWindowFits(messageThing.Key.Id);
            }

            return new MessageItem(PersonGUID, RecordGUID, wrapper);
        }

        public MessageItem UpdateItem(
            MessageItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            CustomHealthTypeWrapper customWrapperForThing = (CustomHealthTypeWrapper)thing;
            HVWrapper.Message messageThing = (HVWrapper.Message)customWrapperForThing.WrappedObject;

            _UpdateFields(messageThing, itemEdits);

            accessor.UpdateItem(customWrapperForThing);

            //Remove the item if it was in the cache
            RemoveItem(messageThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(messageThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(messageThing.Key.Id);
            }

            //Return the updated thing
            return new MessageItem(PersonGUID, RecordGUID, messageThing.Wrapper);
        }

        private void _UpdateFields(HVWrapper.Message messageThing, MessageItem itemEdits)
        {
            CustomHealthTypeWrapper customWrapperForThing = (CustomHealthTypeWrapper)messageThing.Wrapper;

            if (itemEdits.IsDirty)
            {
                if (itemEdits.MessageID.IsDirty)
                {
                    messageThing.MessageID = itemEdits.MessageID.Value;
                }

                if (itemEdits.IsSentByProvider.IsDirty)
                {
                    messageThing.IsSentByProvider = itemEdits.IsSentByProvider.Value;
                }

                if (itemEdits.DateSent.IsDirty)
                {
                    messageThing.DateSent = itemEdits.DateSent.Value;
                }

                if (itemEdits.Subject.IsDirty)
                {
                    messageThing.Subject = itemEdits.Subject.Value;
                }

                if (itemEdits.MessageBody.IsDirty)
                {
                    messageThing.MessageBody = itemEdits.MessageBody.Value;
                }

                if (itemEdits.ProviderID.IsDirty)
                {
                    messageThing.ProviderID = itemEdits.ProviderID.Value;
                }

                //=============================================================
                //Store the source of the data
                //=============================================================
                if (itemEdits.Source.IsDirty)
                {
                    customWrapperForThing.CommonData.Source = itemEdits.Source.Value;
                }

                //=============================================================
                //Store the source of the data
                //=============================================================
                if (itemEdits.Source.IsDirty) customWrapperForThing.CommonData.Source = itemEdits.Source.Value;
                if (itemEdits.Note.IsDirty) customWrapperForThing.CommonData.Note = itemEdits.Note.Value;
            }
        }
    }
}