﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using System.Text;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class EmergencyContactDataManager : LocalThingTypeSingleInstanceManager<EmergencyContactDataManager.EmergencyContactItem>
    {
        public static string STR_PersonType = "1";//"Emergency Contact";
        public static string PersonType = "Emergency Contact";

        [Serializable]
        public class EmergencyContactItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<string> FirstName { get; private set; }
            public PossibleUpdate<string> LastName { get; private set; }
            public PossibleUpdate<string> StreetAddress { get; private set; }
            public PossibleUpdate<string> City { get; private set; }
            public PossibleUpdate<string> State { get; private set; }
            public PossibleUpdate<string> Zip { get; private set; }
            public PossibleUpdate<string> Country { get; private set; }
            public PossibleUpdate<string> Phone { get; private set; }
            public PossibleUpdate<string> EmailAddress { get; private set; }
            public PossibleUpdate<DateTime> EffectiveDate { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (
                    FirstName.IsDirty
                    || LastName.IsDirty
                    || StreetAddress.IsDirty
                    || City.IsDirty
                    || State.IsDirty
                    || Zip.IsDirty
                    || Country.IsDirty
                    || Phone.IsDirty
                    || EmailAddress.IsDirty
                    || Note.IsDirty
                    || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                FirstName.ResetDirtyFlag();
                LastName.ResetDirtyFlag();
                StreetAddress.ResetDirtyFlag();
                City.ResetDirtyFlag();
                State.ResetDirtyFlag();
                Zip.ResetDirtyFlag();
                Country.ResetDirtyFlag();
                Phone.ResetDirtyFlag();
                EmailAddress.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public EmergencyContactItem()
            {
                FirstName = new HVManager.PossibleUpdate<string>();
                LastName = new HVManager.PossibleUpdate<string>();
                StreetAddress = new HVManager.PossibleUpdate<string>();
                City = new HVManager.PossibleUpdate<string>();
                State = new HVManager.PossibleUpdate<string>();
                Zip = new HVManager.PossibleUpdate<string>();
                Country = new HVManager.PossibleUpdate<string>();
                Phone = new HVManager.PossibleUpdate<string>();
                EmailAddress = new HVManager.PossibleUpdate<string>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public EmergencyContactItem(Guid gPersonGUID, Guid gRecordGUID, Person item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                //Store the values
                this.FirstName = new PossibleUpdate<string>(item.Name.First);
                this.LastName = new PossibleUpdate<string>(item.Name.Last);

                this.StreetAddress = new PossibleUpdate<string>();
                this.City = new PossibleUpdate<string>();
                this.State = new PossibleUpdate<string>();
                this.Zip = new PossibleUpdate<string>();
                this.Country = new PossibleUpdate<string>();
                
                Address objAddress = null;
                if (item.ContactInformation != null)
                {
                    objAddress = item.ContactInformation.PrimaryAddress;
                    if (objAddress != null)
                    {
                        if (objAddress != null && objAddress.Street != null && objAddress.Street.Count > 0)
                        {
                            this.StreetAddress = new PossibleUpdate<string>(objAddress.Street[0]);
                        }

                        this.City = new PossibleUpdate<string>(objAddress.City);
                        this.State = new PossibleUpdate<string>(objAddress.State);
                        this.Zip = new PossibleUpdate<string>(objAddress.PostalCode);
                        this.Country = new PossibleUpdate<string>(objAddress.Country);
                    }
                }

                Phone objPhone = null;
                this.Phone = new PossibleUpdate<string>();
                if (item.ContactInformation != null)
                {
                    objPhone = item.ContactInformation.PrimaryPhone;
                    if (objPhone != null)
                    {
                        this.Phone = new PossibleUpdate<string>(objPhone.Number);
                    }
                }

                Email objEmailAddress = null;
                this.EmailAddress = new PossibleUpdate<string>();
                if (item.ContactInformation != null)
                {
                    objEmailAddress = item.ContactInformation.PrimaryEmail;
                    if (objEmailAddress != null)
                    {
                        this.EmailAddress = new PossibleUpdate<string>(objEmailAddress.Address);
                    }
                }

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        static string xpathFilter = "/thing/data-xml/person/type/code[value='1'][family='wc'][type='person-types']";
        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(Person.TypeId, xpathFilter, null, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public EmergencyContactDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {

        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override EmergencyContactItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   Person.TypeId, xpathFilter);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            Person personalThing = (Person)thing;

            return new EmergencyContactItem(PersonGUID, RecordGUID, personalThing);
        }

        internal bool IsCreateOrUpdate(EmergencyContactItem itemEdits, out Person item)
        {
            bool bCreate = false;
            if (!itemEdits.IsDirty)
            {
                item = null;
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Person thing =
                 (Person)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                     accessor,
                                     Person.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                thing = new Person();
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            item = thing;

            return bCreate;
        }

        private void _CreateItem(EmergencyContactItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            Person thing = new Person();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            accessor.NewItem(thing);

            this.FlushCache();
        }

        public EmergencyContactItem CreateOrUpdateItem(
            EmergencyContactItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Person thing =
                (Person)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    Person.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(Person thing,
                                                EmergencyContactItem itemEdits)
        {

            thing.PersonType = HVManager.CodedAndCodable.GetCodableValue(
                                                PersonType,
                                                STR_PersonType,
                                                VocabHelper.STR_PERSON_TYPES_KEY,
                                                VocabHelper.STR_FAMILY_WC);

            if (thing.Name == null)
            {
                thing.Name = new Name();
            }

            if (itemEdits.FirstName.IsDirty)
            {
                thing.Name.First = itemEdits.FirstName.Value;
            }
            if (itemEdits.LastName.IsDirty)
            {
                thing.Name.Last = itemEdits.LastName.Value;
            }

            if (itemEdits.FirstName.IsDirty || itemEdits.LastName.IsDirty)
            {
                thing.Name.Full = thing.Name.First + " " + thing.Name.Last;
            }

            bool bCreateContactInfo = false;
            bool bCreateAddress = false;
            bool bCreatePhone = false;
            bool bCreateEmailAddress = false;

            ContactInfo objContactInfo = thing.ContactInformation;
            if (thing.ContactInformation == null)
            {
                bCreateContactInfo = true;
                objContactInfo = new ContactInfo();
            }

            Address primaryAddress = null;
            if (thing.ContactInformation != null && thing.ContactInformation.PrimaryAddress != null)
            {
                primaryAddress = thing.ContactInformation.PrimaryAddress;
            }
            else
            {
                bCreateAddress = true;
                primaryAddress = new Address();
                primaryAddress.IsPrimary = true;
            }

            if (itemEdits.StreetAddress.IsDirty)
            {
                primaryAddress.Street.Clear();
                if (!(string.IsNullOrEmpty(itemEdits.StreetAddress.Value)))
                {
                    primaryAddress.Street.Add(itemEdits.StreetAddress.Value);
                }
                else
                {
                    primaryAddress.Street.Clear();
                    primaryAddress.Street.Add("-");
                }
            }

            if (itemEdits.City.IsDirty)
            {
                if (!(string.IsNullOrEmpty(itemEdits.City.Value)))
                {
                    primaryAddress.City = itemEdits.City.Value;
                }
                else
                {
                    primaryAddress.City = "-";
                }
            }

            if (itemEdits.State.IsDirty)
            {
                if (!(string.IsNullOrEmpty(itemEdits.State.Value)))
                {
                    primaryAddress.State = itemEdits.State.Value;
                }
                else
                {
                    primaryAddress.State = null;
                }
            }

            if (itemEdits.Zip.IsDirty)
            {
                if (!(string.IsNullOrEmpty(itemEdits.Zip.Value)))
                {
                    primaryAddress.PostalCode = itemEdits.Zip.Value;
                }
                else
                {
                    primaryAddress.PostalCode = "-";
                }
            }

            if (itemEdits.Country.IsDirty)
            {
                if (!(string.IsNullOrEmpty(itemEdits.Country.Value)))
                {
                    primaryAddress.Country = itemEdits.Country.Value;
                }
                else
                {
                    primaryAddress.Country = "-";
                }
            }

            Phone primaryPhone = null;
            if (thing.ContactInformation != null && thing.ContactInformation.PrimaryPhone != null)
            {
                primaryPhone = thing.ContactInformation.PrimaryPhone;
            }
            else
            {
                bCreatePhone = true;
                primaryPhone = new Phone();
                primaryPhone.IsPrimary = true;
            }

            if (itemEdits.Phone.IsDirty)
            {
                if (!(string.IsNullOrEmpty(itemEdits.Phone.Value)))
                {
                    primaryPhone.Number = itemEdits.Phone.Value;
                }
            }

            Email emailAddress = null;
            if (thing.ContactInformation != null && thing.ContactInformation.PrimaryEmail != null)
            {
                emailAddress = thing.ContactInformation.PrimaryEmail;
            }
            else
            {
                bCreateEmailAddress = true;
                emailAddress = new Email();
                emailAddress.IsPrimary = true;
            }

            if (itemEdits.EmailAddress.IsDirty)
            {
                if (!(string.IsNullOrEmpty(itemEdits.EmailAddress.Value)))
                {
                    emailAddress.Address = itemEdits.EmailAddress.Value;
                }
            }

            if (bCreateAddress)
            {
                if (primaryAddress.Street.Count == 0)
                {
                    primaryAddress.Street.Add("-");
                }

                if (string.IsNullOrEmpty(itemEdits.City.Value))
                {
                    primaryAddress.City = "-";
                }

                if (string.IsNullOrEmpty(itemEdits.Zip.Value))
                {
                    primaryAddress.PostalCode = "-";
                }

                if (string.IsNullOrEmpty(itemEdits.Country.Value))
                {
                    primaryAddress.Country = "-";
                }
                objContactInfo.Address.Add(primaryAddress);
            }

            if (bCreatePhone)
            {
                objContactInfo.Phone.Add(primaryPhone);
            }

            if (bCreateEmailAddress)
            {
                objContactInfo.Email.Add(emailAddress);
            }

            if (bCreateContactInfo)
            {
                thing.ContactInformation = objContactInfo;
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}