﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class BasicDataManager : LocalThingTypeSingleInstanceManager<BasicDataManager.BasicItem>
    {
        [Serializable]
        public class BasicItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<Microsoft.Health.ItemTypes.Gender?> GenderOfPerson { get; private set; }
            public PossibleUpdate<string> PostalCode { private set; get; }
            public PossibleUpdate<int> YearOfBirth { private set; get; }

            public bool IsDirty
            {
                get
                {
                    return GenderOfPerson.IsDirty
                        || PostalCode.IsDirty
                        || YearOfBirth.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty;
                }
            }

            public void ResetDirtyFlag()
            {
                GenderOfPerson.ResetDirtyFlag();
                PostalCode.ResetDirtyFlag();
                YearOfBirth.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public BasicItem()
            {
                GenderOfPerson = new PossibleUpdate<Gender?>();
                PostalCode = new PossibleUpdate<string>();
                YearOfBirth = new PossibleUpdate<int>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public BasicItem(Guid gPersonGUID, Guid gRecordGUID, BasicV2 item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                GenderOfPerson = new PossibleUpdate<Gender?>();
                if (item.Gender.HasValue)
                {
                    GenderOfPerson = new PossibleUpdate<Gender?>(item.Gender.Value);
                }

                PostalCode = new PossibleUpdate<string>();
                if (!String.IsNullOrEmpty(item.PostalCode))
                {
                    PostalCode = new PossibleUpdate<string>(item.PostalCode);
                }

                YearOfBirth = new PossibleUpdate<int>();
                if (item.BirthYear.HasValue)
                {
                    YearOfBirth = new PossibleUpdate<int>(item.BirthYear.Value);
                }

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(BasicV2.TypeId, null, null, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public BasicDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override BasicItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   BasicV2.TypeId, null);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            BasicV2 basicThing = (BasicV2)thing;

            return new BasicItem(PersonGUID, RecordGUID, basicThing);
        }

        internal bool IsCreateOrUpdate(BasicItem itemEdits, out BasicV2 item)
        {
            bool bCreate = false;
            if (!itemEdits.IsDirty)
            {
                item = null;
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            BasicV2 thing =
                (BasicV2)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    BasicV2.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                thing = new BasicV2();
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            item = thing;

            return bCreate;
        }

        private void _CreateItem(BasicItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            BasicV2 thing = new BasicV2();

            _UpdateFields(thing, itemEdits);

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            accessor.NewItem(thing);

            this.FlushCache();
        }

        public BasicItem CreateOrUpdateItem(
            BasicItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            BasicV2 thing =
                (BasicV2)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    BasicV2.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(BasicV2 thing,
                                                BasicItem itemEdits)
        {
            if (itemEdits.GenderOfPerson.IsDirty)
            {
                thing.Gender = itemEdits.GenderOfPerson.Value;
            }

            if (itemEdits.PostalCode.IsDirty)
            {
                thing.PostalCode = itemEdits.PostalCode.Value;
            }

            if (itemEdits.YearOfBirth.IsDirty)
            {
                thing.BirthYear = itemEdits.YearOfBirth.Value;
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }

    //[Serializable]
    //public class BasicV2DataManager : Microsoft.Health.ItemTypes.BasicV2
    //{
    //}
}