﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;

    using HVWrapper.ExtendedThingTypes;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "MedicationDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class MedicationDataManager : LocalThingTypeDataManager<MedicationDataManager.MedicationItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class MedicationItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<string> Name { get; private set; }
            public PossibleUpdate<string> Dosage { get; private set; }
            public PossibleUpdate<string> Frequency { get; private set; }
            public PossibleUpdate<string> MedicationType { get; private set; }
            public PossibleUpdate<string> Strength { get; private set; }
            public PossibleUpdate<HVApproximateDateTime> DateDiscontinued { get; private set; }
            public PossibleUpdate<HVApproximateDateTime> StartDate { get; private set; }
            public PossibleUpdate<DateTime> DateEntered { get; private set; }
            public bool HasDiscontinued { get; private set; }
            public PossibleUpdate<string> PolkaItemID { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (Name.IsDirty
                        || Dosage.IsDirty
                        || Frequency.IsDirty
                        || Strength.IsDirty
                        || MedicationType.IsDirty
                        || DateDiscontinued.IsDirty
                        || StartDate.IsDirty
                        || Note.IsDirty
                        || PolkaItemID.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                Name.ResetDirtyFlag();
                Dosage.ResetDirtyFlag();
                Frequency.ResetDirtyFlag();
                MedicationType.ResetDirtyFlag();
                Strength.ResetDirtyFlag();
                DateDiscontinued.ResetDirtyFlag();
                StartDate.ResetDirtyFlag();
                DateEntered.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                PolkaItemID.ResetDirtyFlag();
            }

            public MedicationItem()
            {
                Name = new PossibleUpdate<string>();
                Dosage = new PossibleUpdate<string>();
                Frequency = new PossibleUpdate<string>();
                MedicationType = new PossibleUpdate<string>();
                Strength = new PossibleUpdate<string>();
                DateEntered = new PossibleUpdate<DateTime>();
                DateDiscontinued = new PossibleUpdate<HVApproximateDateTime>();
                StartDate = new PossibleUpdate<HVApproximateDateTime>();
                DateEntered = new PossibleUpdate<DateTime>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                PolkaItemID = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public MedicationItem(Guid gPersonGUID, Guid gRecordGUID, MedicationEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                Name = new PossibleUpdate<string>(item.Name.Text);


                string strDosage = item.Dosage;
                if (item.Dose != null)
                {
                    strDosage = item.Dose.Display;
                }
                Dosage = new PossibleUpdate<string>(strDosage);

                Frequency = new PossibleUpdate<string>();
                if (item.Frequency != null)
                {
                    Frequency.Value = item.Frequency.Display;
                }

                string strStrength = string.Empty;
                if (item.Strength != null)
                {
                    strStrength = item.Strength.Display;
                }
                Strength = new PossibleUpdate<string>(strStrength);

                HVApproximateDateTime dt = new HVApproximateDateTime(item.DateDiscontinued);

                DateDiscontinued = new PossibleUpdate<HVApproximateDateTime>(dt);

                HasDiscontinued = item.DateDiscontinued != null;

                HVApproximateDateTime dtStart = new HVApproximateDateTime(item.DateStarted);
                StartDate = new PossibleUpdate<HVApproximateDateTime>(dtStart);

                MedicationType = new PossibleUpdate<string>(item.MedicationType);

                if (item.DateEntered != null)
                {
                    DateEntered = new PossibleUpdate<DateTime>(item.DateEntered.ToDateTime());
                }
                else
                {
                    DateEntered = new PossibleUpdate<DateTime>(item.EffectiveDate);
                }

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(gPersonGUID, gRecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;

                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);

                ResetDirtyFlag();
            }
        }

        DateTime dtStartDefault = DateTime.MinValue;

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(MedicationEx.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(MedicationEx.TypeId, null, null, queryStart, queryEnd, true);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public MedicationDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<MedicationItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<MedicationItem> dataItemsM = new List<MedicationItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               MedicationEx.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow
               , true);

            IEnumerable<MedicationEx> listBG = dataItems.Cast<MedicationEx>();

            foreach (MedicationEx thing in listBG)
            {
                MedicationItem item = new MedicationItem(PersonGUID, RecordGUID, thing);
                dataItemsM.Add(item);
            }

            return dataItemsM;
        }

        public MedicationEx PopulateItem(MedicationItem itemEdits)
        {
            MedicationEx thing = new MedicationEx();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public MedicationItem CreateItem(MedicationItem itemEdits)
        {
            MedicationEx thing = new MedicationEx();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            //=============================================================
            //Save the new Thing
            //=============================================================
            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(thing.DateEntered.ToDateTime()))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new MedicationItem(PersonGUID, RecordGUID, thing);
        }

        public MedicationItem UpdateItem(
            MedicationItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            MedicationEx myThing = (MedicationEx)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            //Remove the item if it was in the cache
            RemoveItem(myThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            //Return the updated thing
            return new MedicationItem(PersonGUID, RecordGUID, myThing);
        }

        private void _UpdateFields(MedicationEx thing, MedicationItem itemEdits)
        {
            if (itemEdits.Name.IsDirty)
            {

                CodableValue codableValueMedicationName = new CodableValue();

                //Store the text as the user specified it
                codableValueMedicationName.Text = itemEdits.Name.Value;

                thing.Name = codableValueMedicationName;
            }

            if (itemEdits.MedicationType.IsDirty)
            {
                thing.MedicationType = itemEdits.MedicationType.Value;
            }

            if (itemEdits.Dosage.IsDirty)
            {
                thing.Dosage = itemEdits.Dosage.Value;

                if (string.IsNullOrEmpty(itemEdits.Dosage.Value))
                {
                    thing.Dose = null;
                }
                else
                {
                    GeneralMeasurement objGM = new GeneralMeasurement();
                    objGM.Display = itemEdits.Dosage.Value;
                    thing.Dose = objGM;
                }
            }

            if (itemEdits.Strength.IsDirty)
            {
                if (string.IsNullOrEmpty(itemEdits.Strength.Value))
                {
                    thing.Strength = null;
                }
                else
                {
                    GeneralMeasurement objGM = new GeneralMeasurement();
                    objGM.Display = itemEdits.Strength.Value;
                    thing.Strength = objGM;
                }
            }

            if (itemEdits.Frequency.IsDirty)
            {
                if (string.IsNullOrEmpty(itemEdits.Frequency.Value))
                {
                    thing.Frequency = null;
                }
                else
                {
                    thing.Frequency = new GeneralMeasurement(itemEdits.Frequency.Value);
                }
            }

            if (itemEdits.DateDiscontinued.IsDirty)
            {
                if (itemEdits.DateDiscontinued.Value == null)
                {
                    thing.DateDiscontinued = null;
                }
                else
                {
                    thing.DateDiscontinued = itemEdits.DateDiscontinued.Value.GetApproximateDateTime();
                }
            }

            if (itemEdits.StartDate.IsDirty)
            {
                if (itemEdits.StartDate.Value == null)
                {
                    thing.DateStarted = null;
                }
                else
                {
                    thing.DateStarted = itemEdits.StartDate.Value.GetApproximateDateTime();
                }
            }

            if (itemEdits.DateEntered.IsDirty)
            {
                thing.DateEntered = new HealthServiceDateTime(itemEdits.DateEntered.Value);
            }

            if (itemEdits.PolkaItemID.IsDirty) thing.PolkaItemID = itemEdits.PolkaItemID.Value;
            if (itemEdits.PolkaItemID.IsDirty)
            {
                if (!String.IsNullOrEmpty(itemEdits.PolkaItemID.Value))
                {
                    thing.CommonData.ClientId = "POLKA-" + itemEdits.PolkaItemID.Value;
                }
                else
                {
                    thing.CommonData.ClientId = null;
                }
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}