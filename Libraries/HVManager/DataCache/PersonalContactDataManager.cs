﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using System.Linq;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class PersonalContactDataManager : LocalThingTypeSingleInstanceManager<PersonalContactDataManager.ContactItem>
    {
        [Serializable]
        public class ContactItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public TrackableList<AddressItem> AddressList { get; private set; }
            public TrackableList<EmailItem> EmailList { get; private set; }

            public PossibleUpdate<string> Street { get; private set; }
            public PossibleUpdate<string> City { get; private set; }
            public PossibleUpdate<string> Zip { get; private set; }
            public PossibleUpdate<string> State { get; private set; }
            public PossibleUpdate<string> Country { get; private set; }
            public PossibleUpdate<string> Email { get; private set; }
            public PossibleUpdate<string> Phone { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (AddressList.IsDirty 
                        || EmailList.IsDirty
                        || Street.IsDirty
                        || City.IsDirty
                        || Zip.IsDirty
                        || State.IsDirty
                        || Country.IsDirty
                        || Email.IsDirty
                        || Phone.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                AddressList.ResetDirtyFlag();
                EmailList.ResetDirtyFlag();
                Street.ResetDirtyFlag();
                City.ResetDirtyFlag();
                Zip.ResetDirtyFlag();
                State.ResetDirtyFlag();
                Country.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                Email.ResetDirtyFlag();
                Phone.ResetDirtyFlag();
            }

            public ContactItem()
            {
                AddressList = new TrackableList<AddressItem>();
                EmailList = new TrackableList<EmailItem>();
                Street = new PossibleUpdate<string>();
                City = new PossibleUpdate<string>();
                Zip = new PossibleUpdate<string>();
                State = new PossibleUpdate<string>();
                Country = new PossibleUpdate<string>();
                Email = new PossibleUpdate<string>();
                Phone = new PossibleUpdate<string>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public ContactItem(Guid gPersonGUID, Guid gRecordGUID, Contact item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                AddressList = new TrackableList<AddressItem>();

                foreach (Address addr in item.ContactInformation.Address.OrderByDescending(x => x.IsPrimary).ToList())
                {
                    AddressItem d_addr = new AddressItem(addr);
                    AddressList.Add(d_addr);
                }
                
                Email = new PossibleUpdate<string>();

                EmailList = new TrackableList<EmailItem>();
                foreach (Email addr in item.ContactInformation.Email.OrderByDescending(x => x.IsPrimary).ToList())
                {
                    EmailItem d_addr = new EmailItem(addr.Address, addr.Description);
                    EmailList.Add(d_addr);
                }

                Street = new PossibleUpdate<string>();
                City = new PossibleUpdate<string>();
                Zip = new PossibleUpdate<string>();
                State = new PossibleUpdate<string>();
                Country = new PossibleUpdate<string>();
                Email = new PossibleUpdate<string>();
                Phone = new PossibleUpdate<string>();

                if (item.ContactInformation.PrimaryPhone != null)
                {
                    Phone.Value = item.ContactInformation.PrimaryPhone.Number;
                }
                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(Contact.TypeId, null, null, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public PersonalContactDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override ContactItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   Contact.TypeId, null);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            Contact contactThing = (Contact)thing;

            return new ContactItem(PersonGUID, RecordGUID, contactThing);
        }

        internal bool IsCreateOrUpdate(ContactItem itemEdits, out Contact item)
        {
            bool bCreate = false;
            if (!itemEdits.IsDirty)
            {
                item = null;
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Contact thing =
                (Contact)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    Contact.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                thing = new Contact();
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            item = thing;

            return bCreate;
        }

        private void _CreateItem(ContactItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            Contact thing = new Contact();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            accessor.NewItem(thing);

            this.FlushCache();
        }

        public ContactItem CreateOrUpdateItem(
            ContactItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Contact thing =
                (Contact)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    Contact.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(Contact thing,
                                                ContactItem itemEdits)
        {
            bool bCreatePhone = false;

            //If we have not contact info, create it...
            if (thing.ContactInformation == null)
            {
                thing.ContactInformation = new ContactInfo();
            }

            //See if we have a primary address
            Address primaryAddress = _FindPrimaryOrFirstAddress(thing.ContactInformation);
            //If no, create one
            if (primaryAddress == null)
            {
                primaryAddress = new Address();
                primaryAddress.IsPrimary = true;

                thing.ContactInformation.Address.Add(primaryAddress);
            }


            //Street
            if (itemEdits.Street.IsDirty)
            {
                primaryAddress.Street.Clear();

                //Only add the street address if its non-null
                if (!(string.IsNullOrEmpty(itemEdits.Street.Value)))
                {
                    primaryAddress.Street.Add(itemEdits.Street.Value);
                }
            }

            //City
            if (itemEdits.City.IsDirty || primaryAddress.City == null)
                primaryAddress.City
                    = itemEdits.City.Value;


            //State
            if (itemEdits.State.IsDirty || primaryAddress.State == null)
                primaryAddress.State = itemEdits.State.Value;

            //Postal code
            if (itemEdits.Zip.IsDirty || primaryAddress.PostalCode == null)
                primaryAddress.PostalCode = itemEdits.Zip.Value;

            if (itemEdits.Country.IsDirty || primaryAddress.Country == null)
                primaryAddress.Country = itemEdits.Country.Value;

            if (itemEdits.Email.IsDirty)
            {
                //See if we have a primary address
                Email primaryEmail = _FindPrimaryOrFirstEmail(thing.ContactInformation);
                //If no, create one
                if (primaryEmail == null)
                {
                    primaryEmail = new Email();
                    primaryEmail.IsPrimary = true;

                    thing.ContactInformation.Email.Add(primaryEmail);
                }
                
                primaryEmail.Address = itemEdits.Email.Value;
            }

            //pHone
            Phone primaryPhone = null;
            if (thing.ContactInformation != null && thing.ContactInformation.PrimaryPhone != null)
            {
                primaryPhone = thing.ContactInformation.PrimaryPhone;
            }
            else
            {
                bCreatePhone = true;
                primaryPhone = new Phone();
                primaryPhone.IsPrimary = true;
            }

            if (itemEdits.Phone.IsDirty)
            {
                if (!(string.IsNullOrEmpty(itemEdits.Phone.Value)))
                {
                    primaryPhone.Number = itemEdits.Phone.Value;
                }
            }

            if (bCreatePhone)
            {
                thing.ContactInformation.Phone.Add(primaryPhone);
            }
            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }

        /// <summary>
        /// Looks for address marked Primary; returns NULL if none found
        /// </summary>
        /// <param name="thing"></param>
        /// <returns></returns>
        private static Address _FindPrimaryOrFirstAddress(ContactInfo contactInfo)
        {
            foreach (Address thisAddress in contactInfo.Address)
            {
                if (thisAddress.IsPrimary != null)
                {
                    if (thisAddress.IsPrimary.Value) return thisAddress;
                }
            }

            if (contactInfo.Address.Count > 0)
            {
                return contactInfo.Address[0];
            }

            return null; //No primary address
        }

        /// <summary>
        /// Looks for email marked Primary; returns NULL if none found
        /// </summary>
        /// <param name="thing"></param>
        /// <returns></returns>
        internal static Email _FindPrimaryOrFirstEmail(ContactInfo contactInfo)
        {
            foreach (Email thisEmail in contactInfo.Email)
            {
                if (thisEmail.IsPrimary.HasValue)
                {
                    if (thisEmail.IsPrimary.Value) return thisEmail;
                }
            }

            if (contactInfo.Email.Count > 0)
            {
                return contactInfo.Email[0];
            }

            return null; //No primary email
        }

    }
}