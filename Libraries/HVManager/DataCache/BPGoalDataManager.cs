﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using HVWrapper.ExtendedThingTypes;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class BPGoalDataManager : LocalThingTypeSingleInstanceManager<BPGoalDataManager.BPGoalItem>
    {
        [Serializable]
        public class BPGoalItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<int> TargetSystolic { get; private set; }
            public PossibleUpdate<int> TargetDiastolic { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (TargetSystolic.IsDirty
                    || TargetDiastolic.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                TargetSystolic.ResetDirtyFlag();
                TargetDiastolic.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public BPGoalItem()
            {
                TargetSystolic = new PossibleUpdate<int>();
                TargetDiastolic = new PossibleUpdate<int>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public BPGoalItem(Guid gPersonGUID, Guid gRecordGUID, LifeGoalEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                string[] strTargetValueArray = item.TargetValue.Split('/');

                this.TargetSystolic = new PossibleUpdate<int>(Convert.ToInt32(strTargetValueArray[0]));
                this.TargetDiastolic = new PossibleUpdate<int>(Convert.ToInt32(strTargetValueArray[1]));
                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        static string xpathFilter = "/thing/data-xml/common/extension[goal_type='ReduceBloodPressure']";
        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(LifeGoalEx.TypeId, xpathFilter, 1, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public BPGoalDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override BPGoalItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   LifeGoalEx.TypeId, xpathFilter);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            LifeGoalEx lThing = (LifeGoalEx)thing;

            return new BPGoalItem(PersonGUID, RecordGUID, lThing);
        }

        private LifeGoalEx _CreateItem(BPGoalItem itemEdits, bool bHVCreate)
        {
            if (!itemEdits.IsDirty)
                return null;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing = new LifeGoalEx();
            thing.Description = "ReduceBloodPressure";
            thing.Goal = new Microsoft.Health.ItemTypes.Goal();
            thing.TypeOfGoal = GoalType.ReduceBloodPressure;

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            if (bHVCreate)
            {
                accessor.NewItem(thing);

                this.FlushCache();
            }

            return thing;
        }

        internal bool IsCreateOrUpdate(BPGoalItem itemEdits, out LifeGoalEx item)
        {
            bool bCreate = false;
            item = null;
            if (!itemEdits.IsDirty)
            {
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                item = _CreateItem(itemEdits, false);
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            if (thing != null)
            {
                _UpdateFields(thing, itemEdits);

                item = thing;
            }

            return bCreate;
        }

        public BPGoalItem CreateOrUpdateItem(
            BPGoalItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits, true);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(LifeGoalEx thing,
                                                BPGoalItem itemEdits)
        {
            if (itemEdits.TargetSystolic.IsDirty || itemEdits.TargetDiastolic.IsDirty)
            {
                thing.TargetValue = itemEdits.TargetSystolic.Value.ToString() + "/" + itemEdits.TargetDiastolic.Value.ToString();
                //this is not needed but, just in case we want to track when this goal was updated, we can use this!
                thing.StartDate = new HealthServiceDateTime(DateTime.Now);
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}