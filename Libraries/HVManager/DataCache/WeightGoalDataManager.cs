﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using HVWrapper.ExtendedThingTypes;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class WeightGoalDataManager : LocalThingTypeSingleInstanceManager<WeightGoalDataManager.WeightGoalItem>
    {
        [Serializable]
        public class WeightGoalItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<double> StartValue { get; private set; }
            public PossibleUpdate<DateTime?> StartDate { get; private set; }
            public PossibleUpdate<double> TargetValue { get; private set; }
            public PossibleUpdate<DateTime?> TargetDate { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (StartValue.IsDirty
                    || StartDate.IsDirty
                    || TargetValue.IsDirty
                    || TargetDate.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                StartValue.ResetDirtyFlag();
                StartDate.ResetDirtyFlag();
                TargetValue.ResetDirtyFlag();
                TargetDate.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public WeightGoalItem()
            {
                StartValue = new PossibleUpdate<double>();
                StartDate = new PossibleUpdate<DateTime?>();
                TargetValue = new PossibleUpdate<double>();
                TargetDate = new PossibleUpdate<DateTime?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public WeightGoalItem(Guid gPersonGUID, Guid gRecordGUID, LifeGoalEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                StartValue = new PossibleUpdate<double>();
                StartDate = new PossibleUpdate<DateTime?>();
                TargetValue = new PossibleUpdate<double>();
                TargetDate = new PossibleUpdate<DateTime?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();

                if (!string.IsNullOrEmpty(item.StartValue))
                {
                    this.StartValue = new PossibleUpdate<double>(Convert.ToDouble(item.StartValue));
                }

                this.StartDate = new PossibleUpdate<DateTime?>();
                if (item.StartDate != null)
                {
                    this.StartDate = new PossibleUpdate<DateTime?>(new DateTime(item.StartDate.Date.Year,
                        item.StartDate.Date.Month,
                        item.StartDate.Date.Day));
                }

                if (!string.IsNullOrEmpty(item.TargetValue))
                {
                    this.TargetValue = new PossibleUpdate<double>(Convert.ToDouble(item.TargetValue));
                }

                this.TargetDate = new PossibleUpdate<DateTime?>();
                if (item.Goal.TargetDate.ApproximateDate != null)
                {
                    this.TargetDate = new PossibleUpdate<DateTime?>(new DateTime(item.Goal.TargetDate.ApproximateDate.Year,
                        item.Goal.TargetDate.ApproximateDate.Month.Value,
                        item.Goal.TargetDate.ApproximateDate.Day.Value));
                }

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        static string xpathFilter = "/thing/data-xml/common/extension[goal_type='ReduceWeight']";
        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(LifeGoalEx.TypeId, xpathFilter, 1, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public WeightGoalDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override WeightGoalItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   LifeGoalEx.TypeId, xpathFilter);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            LifeGoalEx lThing = (LifeGoalEx)thing;

            return new WeightGoalItem(PersonGUID, RecordGUID, lThing);
        }

        private LifeGoalEx _CreateItem(WeightGoalItem itemEdits, bool bHVCreate)
        {
            if (!itemEdits.IsDirty)
                return null;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing = new LifeGoalEx();
            thing.Description = "ReduceWeight";
            ApproximateDate apDate = new ApproximateDate(itemEdits.TargetDate.Value.Value.Year, itemEdits.TargetDate.Value.Value.Month, itemEdits.TargetDate.Value.Value.Day);
            ApproximateDateTime apDateTime = new ApproximateDateTime(apDate);
            thing.Goal = new Microsoft.Health.ItemTypes.Goal(apDateTime);
            thing.TypeOfGoal = GoalType.ReduceWeight;

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            if (bHVCreate)
            {
                accessor.NewItem(thing);

                this.FlushCache();
            }

            return thing;
        }

        internal bool IsCreateOrUpdate(WeightGoalItem itemEdits, out LifeGoalEx item)
        {
            bool bCreate = false;
            item = null;
            if (!itemEdits.IsDirty)
            {
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                item = _CreateItem(itemEdits, false);
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            if (thing != null)
            {
                _UpdateFields(thing, itemEdits);

                item = thing;
            }

            return bCreate;
        }

        public WeightGoalItem CreateOrUpdateItem(
            WeightGoalItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            LifeGoalEx thing =
                (LifeGoalEx)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    LifeGoalEx.TypeId, xpathFilter);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits, true);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(LifeGoalEx thing,
                                                WeightGoalItem itemEdits)
        {
            //Set the fields if appropriate...
            if (itemEdits.StartValue.IsDirty)
                thing.StartValue = itemEdits.StartValue.Value.ToString();

            if (itemEdits.StartDate.IsDirty)
                thing.StartDate = new HealthServiceDateTime(itemEdits.StartDate.Value.Value);

            if (itemEdits.TargetValue.IsDirty)
                thing.TargetValue = itemEdits.TargetValue.Value.ToString();

            if (itemEdits.TargetDate.IsDirty)
            {
                ApproximateDate apDate = new ApproximateDate(itemEdits.TargetDate.Value.Value.Year, itemEdits.TargetDate.Value.Value.Month, itemEdits.TargetDate.Value.Value.Day);
                ApproximateDateTime apDateTime = new ApproximateDateTime(apDate);
                thing.Goal.TargetDate = apDateTime;
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}