﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using HVWrapper;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class ExtendedProfileDataManager : LocalThingTypeSingleInstanceManager<ExtendedProfileDataManager.ExtendedProfileItem>
    {
        [Serializable]
        public class ExtendedProfileItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<bool?> IsSmokeCigarette { get; private set; }//in edit profile
            public PossibleUpdate<bool?> HasTriedToQuitSmoking { get; private set; }//in edit profile
            public PossibleUpdate<string> Allergies { get; private set; }//in edit profile
            public PossibleUpdate<string> TravelTimeToWork { get; private set; }//in edit profile
            public PossibleUpdate<string> ActivityLevel { get; private set; }
            public PossibleUpdate<string> MeansOfTransportation { get; private set; }
            public PossibleUpdate<string> AttitudeTowardsHealthCare { get; private set; }//in edit profile
            public PossibleUpdate<string> IncomeLevel { get; private set; }//in edit profile
            public PossibleUpdate<bool?> HasHealthInsurance { get; private set; }//in edit profile

            public bool IsDirty
            {
                get
                {
                    return (IsSmokeCigarette.IsDirty
                    || HasTriedToQuitSmoking.IsDirty
                    || Allergies.IsDirty
                    || TravelTimeToWork.IsDirty
                    || ActivityLevel.IsDirty
                    || MeansOfTransportation.IsDirty
                    || AttitudeTowardsHealthCare.IsDirty
                    || IncomeLevel.IsDirty
                    || HasHealthInsurance.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                IsSmokeCigarette.ResetDirtyFlag();
                HasTriedToQuitSmoking.ResetDirtyFlag();
                Allergies.ResetDirtyFlag();
                TravelTimeToWork.ResetDirtyFlag();
                ActivityLevel.ResetDirtyFlag();
                MeansOfTransportation.ResetDirtyFlag();
                AttitudeTowardsHealthCare.ResetDirtyFlag();
                IncomeLevel.ResetDirtyFlag();
                HasHealthInsurance.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public ExtendedProfileItem()
            {
                IsSmokeCigarette = new HVManager.PossibleUpdate<bool?>();
                HasTriedToQuitSmoking = new HVManager.PossibleUpdate<bool?>();
                Allergies = new HVManager.PossibleUpdate<string>();
                TravelTimeToWork = new PossibleUpdate<string>();
                ActivityLevel = new PossibleUpdate<string>();
                MeansOfTransportation = new PossibleUpdate<string>();
                AttitudeTowardsHealthCare = new PossibleUpdate<string>();
                IncomeLevel = new PossibleUpdate<string>();
                HasHealthInsurance = new PossibleUpdate<bool?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public ExtendedProfileItem(Guid gPersonGUID, Guid gRecordGUID, HealthRecordItem item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                CustomHealthTypeWrapper customWrapperForThing = (CustomHealthTypeWrapper)item;

                IsSmokeCigarette = new HVManager.PossibleUpdate<bool?>();
                HasTriedToQuitSmoking = new HVManager.PossibleUpdate<bool?>();
                Allergies = new HVManager.PossibleUpdate<string>();
                TravelTimeToWork = new PossibleUpdate<string>();
                ActivityLevel = new PossibleUpdate<string>();
                MeansOfTransportation = new PossibleUpdate<string>();
                AttitudeTowardsHealthCare = new PossibleUpdate<string>();
                IncomeLevel = new PossibleUpdate<string>();
                HasHealthInsurance = new PossibleUpdate<bool?>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();

                ExtendedProfile customThing = (ExtendedProfile)customWrapperForThing.WrappedObject;
                if (customThing == null)
                {
                    ResetDirtyFlag();
                    return;
                }

                if (customThing.IsSmokeCigarette.HasValue)
                    IsSmokeCigarette = new PossibleUpdate<bool?>(customThing.IsSmokeCigarette);

                if (customThing.HasTriedToQuitSmoking.HasValue)
                    HasTriedToQuitSmoking = new PossibleUpdate<bool?>(customThing.HasTriedToQuitSmoking);

                Allergies = new PossibleUpdate<string>(customThing.Allergies);

                TravelTimeToWork = new PossibleUpdate<string>(customThing.TravelTimeToWork);

                ActivityLevel = new PossibleUpdate<string>(customThing.ActivityLevel);

                MeansOfTransportation = new PossibleUpdate<string>(customThing.MeansOfTransportation);
                AttitudeTowardsHealthCare = new PossibleUpdate<string>(customThing.AttitudeTowardsHealthCare);
                IncomeLevel = new PossibleUpdate<string>(customThing.HouseholdIncome);
                HasHealthInsurance = new PossibleUpdate<bool?>(customThing.HasHealthInsurance);

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(customThingTypes, ExtendedProfile.XPathFilterForQuery, 1, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public ExtendedProfileDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override ExtendedProfileItem GetLatestInstance()
        {
            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                     Helper.GetAccessor(PersonGUID, RecordGUID),
                                    customThingTypes,
                                    ExtendedProfile.XPathFilterForQuery
                                    );

            //Null data result
            if (thing == null) return null;

            return new ExtendedProfileItem(PersonGUID, RecordGUID, thing);
        }

        internal bool IsCreateOrUpdate(ExtendedProfileItem itemEdits, out CustomHealthTypeWrapper item)
        {
            bool bCreate = false;
            if (!itemEdits.IsDirty)
            {
                item = null;
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            CustomHealthTypeWrapper thing =
                (CustomHealthTypeWrapper)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    customThingTypes,
                                    ExtendedProfile.XPathFilterForQuery);

            ExtendedProfile wrappedThing = null;

            if (thing != null)
                wrappedThing = (ExtendedProfile)thing.WrappedObject;


            //No instance was returned from the server
            if (wrappedThing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                wrappedThing = new ExtendedProfile();
                thing = new CustomHealthTypeWrapper(wrappedThing);
                thing.When = new HealthServiceDateTime(DateTime.Now);
            }

            //If the keys do not match; its an error
            if (!bCreate && (wrappedThing == null || !wrappedThing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(wrappedThing, itemEdits);

            item = thing;

            return bCreate;
        }

        private void _CreateItem(ExtendedProfileItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            ExtendedProfile thing = new ExtendedProfile();
            CustomHealthTypeWrapper wrapper = new CustomHealthTypeWrapper(thing);
            thing.When = new HealthServiceDateTime(DateTime.Now);

            _UpdateFields(thing, itemEdits);

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            accessor.NewItem(wrapper);

            this.FlushCache();
        }

        public ExtendedProfileItem CreateOrUpdateItem(
            ExtendedProfileItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Guid customThingTypes = new Guid(CustomHealthTypeWrapper.ApplicationCustomTypeID);

            CustomHealthTypeWrapper thing =
                (CustomHealthTypeWrapper)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    customThingTypes,
                                    ExtendedProfile.XPathFilterForQuery);

            ExtendedProfile wrappedThing = null;
            
            if (thing != null)
                wrappedThing = (ExtendedProfile)thing.WrappedObject;


            //No instance was returned from the server
            if (wrappedThing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits);

                return Item;
            }

            //If the keys do not match; its an error
            if (!wrappedThing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(wrappedThing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(ExtendedProfile wrappedThing,
                                                ExtendedProfileItem itemEdits)
        {
            if (itemEdits.IsSmokeCigarette.IsDirty)
                wrappedThing.IsSmokeCigarette = itemEdits.IsSmokeCigarette.Value;

            if (itemEdits.HasTriedToQuitSmoking.IsDirty)
                wrappedThing.HasTriedToQuitSmoking = itemEdits.HasTriedToQuitSmoking.Value;

            if (itemEdits.Allergies.IsDirty)
                wrappedThing.Allergies = itemEdits.Allergies.Value;

            if (itemEdits.TravelTimeToWork.IsDirty)
                wrappedThing.TravelTimeToWork = itemEdits.TravelTimeToWork.Value;

            if (itemEdits.ActivityLevel.IsDirty)
                wrappedThing.ActivityLevel = itemEdits.ActivityLevel.Value;

            if (itemEdits.MeansOfTransportation.IsDirty)
                wrappedThing.MeansOfTransportation = itemEdits.MeansOfTransportation.Value;

            if (itemEdits.AttitudeTowardsHealthCare.IsDirty)
                wrappedThing.AttitudeTowardsHealthCare = itemEdits.AttitudeTowardsHealthCare.Value;

            if (itemEdits.IncomeLevel.IsDirty)
                wrappedThing.HouseholdIncome = itemEdits.IncomeLevel.Value;

            if (itemEdits.HasHealthInsurance.IsDirty)
                wrappedThing.HasHealthInsurance = itemEdits.HasHealthInsurance.Value;

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) wrappedThing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) wrappedThing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}