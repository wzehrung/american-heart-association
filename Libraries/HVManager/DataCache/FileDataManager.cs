﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using Microsoft.Health.Web;

using HVWrapper.ExtendedThingTypes;


namespace HVManager
{
    [Serializable]
    public class FileDataManager : LocalThingTypeSingleInstanceManager<FileDataManager.FileItem>
    {
        [Serializable]
        public class FileItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<double?> HealthScore { get; private set; }
            public PossibleUpdate<double?> BMI { get; private set; }
            public PossibleUpdate<double?> BloodGlucose { get; private set; }
            public PossibleUpdate<double?> HbA1c { get; private set; }
            public PossibleUpdate<int?> TotalCholesterol { get; private set; }
            public PossibleUpdate<int?> SystolicBP { get; private set; }
            public PossibleUpdate<int?> DiastolicBP { get; private set; }
            public PossibleUpdate<string> Name { get; private set; }
            public PossibleUpdate<byte[]> PdfBytes { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (HealthScore.IsDirty
                    || Name.IsDirty
                    || BMI.IsDirty
                    || BloodGlucose.IsDirty
                    || HbA1c.IsDirty
                    || TotalCholesterol.IsDirty
                    || SystolicBP.IsDirty
                    || DiastolicBP.IsDirty
                    || PdfBytes.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                HealthScore.ResetDirtyFlag();
                BMI.ResetDirtyFlag();
                BloodGlucose.ResetDirtyFlag();
                HbA1c.ResetDirtyFlag();
                TotalCholesterol.ResetDirtyFlag();
                SystolicBP.ResetDirtyFlag();
                DiastolicBP.ResetDirtyFlag();
                Name.ResetDirtyFlag();
                PdfBytes.ResetDirtyFlag();
            }

            public FileItem()
            {
                HealthScore = new PossibleUpdate<double?>();
                BMI = new PossibleUpdate<double?>();
                BloodGlucose = new PossibleUpdate<double?>();
                HbA1c = new PossibleUpdate<double?>();
                TotalCholesterol = new PossibleUpdate<int?>();
                SystolicBP = new PossibleUpdate<int?>();
                DiastolicBP = new PossibleUpdate<int?>();
                Name = new HVManager.PossibleUpdate<string>();
                PdfBytes = new PossibleUpdate<byte[]>();
            }

            public FileItem(bool bIsOnlineConnection, bool bFetchPDF, Guid gPersonGUID, Guid gRecordGUID, FileEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                HealthRecordAccessor accessor = Helper.GetAccessor(bIsOnlineConnection, PersonGUID, RecordGUID);
                //OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(PersonGUID);
                //offlineConn.Authenticate();
                //HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, RecordGUID);
                HealthScore = new PossibleUpdate<double?>(item.HealthScore);
                BMI = new PossibleUpdate<double?>(item.BMI);
                BloodGlucose = new PossibleUpdate<double?>(item.BloodGlucose);
                HbA1c = new PossibleUpdate<double?>(item.HbA1c);
                TotalCholesterol = new PossibleUpdate<int?>(item.TotalCholesterol);
                SystolicBP = new PossibleUpdate<int?>(item.SystolicBP);
                DiastolicBP = new PossibleUpdate<int?>(item.DiastolicBP);
                Name = new PossibleUpdate<string>(item.Name);
                BlobStore blobstore = item.GetBlobStore(accessor);
                EffectiveDate = item.EffectiveDate;
                if (bFetchPDF && blobstore != null && blobstore.Values.Count > 0)
                {
                    Blob blob = blobstore.Values.FirstOrDefault();
                    if (blob != null)
                    {
                        using (MemoryStream _MemoryStream = new MemoryStream())
                        {
                            blob.SaveToStream(_MemoryStream);
                            byte[] byteFileData = _MemoryStream.ToArray();
                            PdfBytes = new PossibleUpdate<byte[]>(byteFileData);
                            _MemoryStream.Close();
                        }
                        //BlobStream stream = blob.GetReaderStream();
                        //if (stream != null)
                        //{
                        //    byte[] buffer = new byte[stream.Length];
                        //    Int32 bytesRead;
                        //    MemoryStream memStream = new MemoryStream();
                        //    while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                        //    {
                        //        memStream.Write(buffer, 0, bytesRead);
                        //    }
                            
                        //    PdfBytes = new PossibleUpdate<byte[]>(buffer);
                        //    memStream.Close();
                        //    memStream.Dispose();
                        //    stream.Close();
                        //    stream.Dispose();
                        //}
                    }
                }
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public FileDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public bool IsOnlineConnection
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public FileDataManager(Guid hvpersonid, Guid hvrecordid, bool bIsOnlineConnection)
            :
            base(hvpersonid, hvrecordid)
        {
            IsOnlineConnection = bIsOnlineConnection;
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(FileEx.TypeId, "/thing/data-xml/common/extension/is_my_life_check_file[.= 'true']", 1, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        public FileItem GetLatestFileItem()
        {
            HealthRecordAccessor accessor = Helper.GetAccessor(IsOnlineConnection, PersonGUID, RecordGUID);
            //OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(PersonGUID);
            //offlineConn.Authenticate();
            //HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, RecordGUID);
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   accessor,
                                   FileEx.TypeId, "/thing/data-xml/common/extension/is_my_life_check_file[.= 'true']", HealthRecordItemSections.Core | HealthRecordItemSections.Xml | HealthRecordItemSections.BlobPayload);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            FileEx fileThing = (FileEx)thing;

            return new FileItem(IsOnlineConnection, true, PersonGUID, RecordGUID, fileThing);
        }

        protected override FileItem GetLatestInstance()
        {
            HealthRecordAccessor accessor = Helper.GetAccessor(IsOnlineConnection, PersonGUID, RecordGUID);
            //OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(PersonGUID);
            //offlineConn.Authenticate();
            //HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, RecordGUID);
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   accessor,
                                   FileEx.TypeId, "/thing/data-xml/common/extension/is_my_life_check_file[.= 'true']", HealthRecordItemSections.Core | HealthRecordItemSections.Xml | HealthRecordItemSections.BlobPayload);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            FileEx fileThing = (FileEx)thing;

            return new FileItem(IsOnlineConnection, false, PersonGUID, RecordGUID, fileThing);
        }

        public void CreateItem(
            FileItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            //HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);
            //OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(PersonGUID);
            //offlineConn.Authenticate();
            //HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, RecordGUID);

            HealthRecordAccessor accessor = Helper.GetAccessor(IsOnlineConnection, PersonGUID, RecordGUID);

            FileEx file = new FileEx();
            file.Name = itemEdits.Name.Value;
            file.Size = itemEdits.PdfBytes.Value.Length;
            file.ContentType = new CodableValue("application/pdf");
            file.HealthScore = itemEdits.HealthScore.Value;
            file.BMI = itemEdits.BMI.Value;
            file.BloodGlucose = itemEdits.BloodGlucose.Value;
            file.HbA1c = itemEdits.HbA1c.Value;
            file.TotalCholesterol = itemEdits.TotalCholesterol.Value;
            file.SystolicBP = itemEdits.SystolicBP.Value;
            file.DiastolicBP = itemEdits.DiastolicBP.Value;
            file.IsMyLifeCheckFile = true;
                        
            BlobStore blobstore = file.GetBlobStore(accessor);
            blobstore.Clear();
            Blob blob = blobstore.NewBlob(string.Empty, "application/pdf");
            blob.WriteInline(itemEdits.PdfBytes.Value);

            accessor.NewItem(file);

            this.FlushCache();
        }
    }
}
