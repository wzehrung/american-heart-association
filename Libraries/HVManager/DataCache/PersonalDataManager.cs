﻿namespace HVManager
{
    using System;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using System.Text;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Smoking Data.
    /// NOTE: This class is implemented accross several files, the primary file is "SmokingDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class PersonalDataManager : LocalThingTypeSingleInstanceManager<PersonalDataManager.PersonalItem>
    {
        [Serializable]
        public class PersonalItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<string> Title { get; private set; }
            public PossibleUpdate<string> NameSuffix { get; private set; }
            public PossibleUpdate<string> FirstName { get; private set; }
            public PossibleUpdate<string> LastName { get; private set; }
            public PossibleUpdate<DateTime?> DateOfBirth { get; private set; }
            public PossibleUpdate<string> BloodType { get; private set; }
            public PossibleUpdate<string> Ethnicity { get; private set; }
            public PossibleUpdate<string> MaritalStatus { get; private set; }

            public string FullName
            {
                get
                {
                    StringBuilder sbName = new StringBuilder();
                    if (!string.IsNullOrEmpty(FirstName.Value))
                    {
                        sbName.Append(FirstName.Value);
                    }

                    if (!string.IsNullOrEmpty(LastName.Value))
                    {
                        if (sbName.Length > 0)
                        {
                            sbName.Append(" ");
                        }
                        sbName.Append(LastName.Value);
                    }

                    return sbName.ToString();
                }
            }

            public bool IsDirty
            {
                get
                {
                    return (Title.IsDirty
                    || NameSuffix.IsDirty
                    || FirstName.IsDirty
                    || LastName.IsDirty
                    || DateOfBirth.IsDirty
                    || BloodType.IsDirty
                    || Ethnicity.IsDirty
                    || MaritalStatus.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                Title.ResetDirtyFlag();
                NameSuffix.ResetDirtyFlag();
                FirstName.ResetDirtyFlag();
                LastName.ResetDirtyFlag();
                DateOfBirth.ResetDirtyFlag();
                BloodType.ResetDirtyFlag();
                Ethnicity.ResetDirtyFlag();
                MaritalStatus.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
            }

            public PersonalItem()
            {
                Title = new HVManager.PossibleUpdate<string>();
                NameSuffix = new HVManager.PossibleUpdate<string>();
                FirstName = new HVManager.PossibleUpdate<string>();
                LastName = new HVManager.PossibleUpdate<string>();
                DateOfBirth = new HVManager.PossibleUpdate<DateTime?>();
                BloodType = new HVManager.PossibleUpdate<string>();
                Ethnicity = new HVManager.PossibleUpdate<string>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                MaritalStatus = new HVManager.PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public PersonalItem(Guid gPersonGUID, Guid gRecordGUID, Personal item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                Title = new PossibleUpdate<string>();
                if (item.Name != null && item.Name.Title != null)
                    Title = new PossibleUpdate<string>(item.Name.Title.Text);

                NameSuffix = new PossibleUpdate<string>();
                if (item.Name != null && item.Name.Suffix != null)
                    NameSuffix = new PossibleUpdate<string>(item.Name.Suffix.Text);

                MaritalStatus = new PossibleUpdate<string>();
                if (item.MaritalStatus != null)
                    MaritalStatus = new PossibleUpdate<string>(item.MaritalStatus.Text);

                FirstName = new PossibleUpdate<string>();
                if (item.Name != null && item.Name.First != null)
                    FirstName = new PossibleUpdate<string>(item.Name.First);

                LastName = new PossibleUpdate<string>();
                if (item.Name != null && item.Name.Last != null)
                    LastName = new PossibleUpdate<string>(item.Name.Last);

                DateOfBirth = new PossibleUpdate<DateTime?>();
                BloodType = new PossibleUpdate<string>();
                Ethnicity = new PossibleUpdate<string>();

                //If fn, mn, ln is not available, use the full name directly on first name field.
                if (String.IsNullOrEmpty(FirstName.Value) && String.IsNullOrEmpty(LastName.Value))
                {
                    if (item.Name != null && item.Name.Full != null)
                    {
                        FirstName = new PossibleUpdate<string>(item.Name.Full);
                    }
                }

                if (item.BirthDate != null)
                {
                    DateOfBirth = new PossibleUpdate<DateTime?>(item.BirthDate.ToDateTime());
                }

                if (item.BloodType != null && !string.IsNullOrEmpty(item.BloodType.Text))
                {
                    BloodType = new PossibleUpdate<string>(item.BloodType.Text);
                }

                if (item.Ethnicity != null && !string.IsNullOrEmpty(item.Ethnicity.Text))
                {
                    Ethnicity = new PossibleUpdate<string>(item.Ethnicity.Text);
                }

                ResetDirtyFlag();
            }
        }

        public bool ShouldQuery()
        {
            return !WasItemQueriedFor;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(Personal.TypeId, null, null, DateTime.MinValue, DateTime.MaxValue);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public PersonalDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        /// <summary>
        /// Queries for a single instance of the data-type we are managing.
        /// 
        /// Returns 'NULL' of 0 data is found.  Otherwise the single most current
        /// data item is returned.
        /// </summary>
        /// <returns></returns>
        protected override PersonalItem GetLatestInstance()
        {
            HealthRecordItem thing = WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                   Helper.GetAccessor(PersonGUID, RecordGUID),
                                   Personal.TypeId, null);

            //Null data result
            if (thing == null) return null;

            //Cast it into the specific type it is.  
            //If this type is bogus, something bad/unexpected 
            //happend and an exception will correctly be thrown
            Personal personalThing = (Personal)thing;

            return new PersonalItem(PersonGUID, RecordGUID, personalThing);
        }

        internal bool IsCreateOrUpdate(PersonalItem itemEdits, out Personal item)
        {
            bool bCreate = false;
            if (!itemEdits.IsDirty)
            {
                item = null;
                return false;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Personal thing =
                (Personal)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    Personal.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                bCreate = true;
                thing = new Personal();
            }

            //If the keys do not match; its an error
            if (!bCreate && (thing == null || !thing.Key.Equals(itemEdits.ThingKey)))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            item = thing;

            return bCreate;
        }

        private void _CreateItem(PersonalItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return;

            Personal personalThing = new Personal();

            _UpdateFields(personalThing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                personalThing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            accessor.NewItem(personalThing);

            this.FlushCache();
        }

        public PersonalItem CreateOrUpdateItem(
            PersonalItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            Personal thing =
                (Personal)WCQueries.helper_PerformMSHealthQueryForMostCurrentSingleThing(
                                    accessor,
                                    Personal.TypeId, null);


            //No instance was returned from the server
            if (thing == null)
            {
                //The uncommon, but not impossible case where the server singleton Thing instance
                //has been deleted from underneith us.
                if (itemEdits.ThingKey != null)
                {
                    ErrorConditions.ThrowServerThingInstanceNotFound();
                }

                _CreateItem(itemEdits);

                return Item;
            }

            //If the keys do not match; its an error
            if (!thing.Key.Equals(itemEdits.ThingKey))
            {
                ErrorConditions.ThrowThingKeysDoNotMatch();
            }

            _UpdateFields(thing, itemEdits);

            accessor.UpdateItem(thing);

            this.FlushCache();

            return Item;
        }

        private static void _UpdateFields(Personal personalThing,
                                                PersonalItem itemEdits)
        {
            string strFullName = string.Empty;
            if (personalThing.Name == null)
            {
                personalThing.Name = new Name();
            }

            if (itemEdits.FirstName.IsDirty)
            {
                personalThing.Name.First = itemEdits.FirstName.Value;
            }

            if (itemEdits.LastName.IsDirty)
            {
                personalThing.Name.Last = itemEdits.LastName.Value;
            }

            strFullName = itemEdits.FirstName.Value + " " + itemEdits.LastName.Value;

            if (itemEdits.FirstName.IsDirty || itemEdits.LastName.IsDirty)
            {
                personalThing.Name.Full = strFullName;
            }

            if (!itemEdits.FirstName.IsDirty && !itemEdits.LastName.IsDirty && string.IsNullOrEmpty(personalThing.Name.Full))
            {
                personalThing.Name.Full = "-";
            }

            if (itemEdits.Ethnicity.IsDirty)
            {
                if (!string.IsNullOrEmpty(itemEdits.Ethnicity.Value))
                {
                    personalThing.Ethnicity = HVManager.CodedAndCodable.GetCodableValue(itemEdits.Ethnicity.Value,
                                                    VocabHelper.STR_ETHNICITY_KEY,
                                                    VocabHelper.STR_FAMILY_WC);
                }
                else
                {
                    personalThing.Ethnicity = null;
                }
            }

            if (itemEdits.MaritalStatus.IsDirty)
            {
                if (!string.IsNullOrEmpty(itemEdits.MaritalStatus.Value))
                {
                    personalThing.MaritalStatus = HVManager.CodedAndCodable.GetCodableValue(itemEdits.MaritalStatus.Value,
                                                    VocabHelper.STR_MARITAL_STATUS_KEY,
                                                    VocabHelper.STR_FAMILY_WC);
                }
                else
                {
                    personalThing.MaritalStatus = null;
                }
            }

            if (itemEdits.Title.IsDirty)
            {
                if (!string.IsNullOrEmpty(itemEdits.Title.Value))
                {
                    personalThing.Name.Title = HVManager.CodedAndCodable.GetCodableValue(itemEdits.Title.Value,
                                                    VocabHelper.STR_NAME_PREFIXES_KEY,
                                                    VocabHelper.STR_FAMILY_WC);
                }
                else
                {
                    personalThing.Name.Title = null;
                }
            }

            if (itemEdits.NameSuffix.IsDirty)
            {
                if (!string.IsNullOrEmpty(itemEdits.NameSuffix.Value))
                {
                    personalThing.Name.Suffix = HVManager.CodedAndCodable.GetCodableValue(itemEdits.NameSuffix.Value,
                                                    VocabHelper.STR_NAME_SUFFIXES_KEY,
                                                    VocabHelper.STR_FAMILY_WC);
                }
                else
                {
                    personalThing.Name.Suffix = null;
                }
            }

            if (itemEdits.BloodType.IsDirty)
            {
                if (!string.IsNullOrEmpty(itemEdits.BloodType.Value))
                {
                    personalThing.BloodType = HVManager.CodedAndCodable.GetCodableValue(itemEdits.BloodType.Value,
                                                    VocabHelper.STR_BLOOD_TYPE_KEY,
                                                    VocabHelper.STR_FAMILY_WC);
                }
                else
                {
                    personalThing.BloodType = null;
                }
            }

            if (itemEdits.DateOfBirth.IsDirty)
            {
                //Rare situation: The user wants to revoke their date of birth information
                //and make it null
                if (itemEdits.DateOfBirth.Value == null)
                {
                    personalThing.BirthDate = null;
                }
                else
                {
                    //Change the date of birth
                    personalThing.BirthDate = new HealthServiceDateTime(itemEdits.DateOfBirth.Value.Value);
                }

            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) personalThing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) personalThing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}