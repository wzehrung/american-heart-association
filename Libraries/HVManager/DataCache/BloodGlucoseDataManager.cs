﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Linq;
    using HVWrapper.ExtendedThingTypes;
    
    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "BloodGlucoseDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class BloodGlucoseDataManager : LocalThingTypeDataManager<BloodGlucoseDataManager.BGItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class BGItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<double> Value { get; private set; }
            public PossibleUpdate<string> ActionTaken { get; private set; }
            public PossibleUpdate<string> ReadingType { get; private set; }
            public PossibleUpdate<DateTime> When { get; private set; }
            public string ReadingTypeCodedValue { get; private set; }
            public PossibleUpdate<string> PolkaItemID { get; private set; }

            public bool IsDirty
            {
                get
                {
                    return (Value.IsDirty
                        || When.IsDirty
                        || ActionTaken.IsDirty
                        || ReadingType.IsDirty
                        || Note.IsDirty
                        || PolkaItemID.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                Value.ResetDirtyFlag();
                ActionTaken.ResetDirtyFlag();
                ReadingType.ResetDirtyFlag();
                When.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                PolkaItemID.ResetDirtyFlag();
            }

            public BGItem()
            {
                Value = new PossibleUpdate<double>();
                ActionTaken = new PossibleUpdate<string>();
                ReadingType = new PossibleUpdate<string>();
                When = new PossibleUpdate<DateTime>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                PolkaItemID = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public BGItem(Guid gPersonGUID, Guid gRecordGUID, BloodGlucoseEx item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                //Store the values
                this.Value = new PossibleUpdate<double>(Helper.GlocoseMmolToMgdl(item.Value.Value));
                this.ActionTaken = new PossibleUpdate<string>(item.ActionTaken);
                this.When = new PossibleUpdate<DateTime>(item.When.ToDateTime());

                string strReadingType = item.ReadingType;
                string strReadingTypeCodedValue = string.Empty;
                if (item.MeasurementContext != null)
                {
                    strReadingType = item.MeasurementContext.Text;
                    if (item.MeasurementContext.Count > 0)
                    {
                        strReadingTypeCodedValue = item.MeasurementContext[0].Value;
                    }
                }

                this.ReadingType = new PossibleUpdate<string>(strReadingType);
                this.ReadingTypeCodedValue = strReadingTypeCodedValue;

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(gPersonGUID, gRecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;
                PolkaItemID = new PossibleUpdate<string>(item.PolkaItemID);

                ResetDirtyFlag();
            }

            public override bool IsItemAcceptableByCurrentApplication()
            {
                if (float.IsInfinity((float)this.Value.Value))
                    return false;

                return true;
            }
        }

        DateTime dtStartDefault = DateTime.Today.AddDays(-180);

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(BloodGlucose.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(BloodGlucose.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public BloodGlucoseDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<BGItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<BGItem> dataItemsBG = new List<BGItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               BloodGlucoseEx.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

            IEnumerable<BloodGlucoseEx> listBG = dataItems.Cast<BloodGlucoseEx>();

            foreach (BloodGlucoseEx thing in listBG)
            {
                BGItem item = new BGItem(PersonGUID, RecordGUID, thing);
                dataItemsBG.Add(item);
            }

            return dataItemsBG;
        }

        public BloodGlucoseEx PopulateItem(BGItem itemEdits)
        {
            BloodGlucoseEx thing = new BloodGlucoseEx();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public BGItem CreateItem(BGItem itemEdits)
        {
            BloodGlucoseEx thing = new BloodGlucoseEx();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            //=============================================================
            //Save the new Thing
            //=============================================================
            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(itemEdits.When.Value))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new BGItem(PersonGUID, RecordGUID, thing);
        }

        public BGItem UpdateItem(
            BGItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            BloodGlucoseEx myThing = (BloodGlucoseEx)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            //Remove the item if it was in the cache
            RemoveItem(myThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            //Return the updated thing
            return new BGItem(PersonGUID, RecordGUID, myThing);
        }

        private void _UpdateFields(BloodGlucoseEx thing, BGItem itemEdits)
        {
            //Update only the fields indicated
            if (itemEdits.When.IsDirty) thing.When = new HealthServiceDateTime(itemEdits.When.Value);
            BloodGlucoseMeasurement objM = new BloodGlucoseMeasurement(Helper.GlucoseMgdlToMmol(itemEdits.Value.Value));

            DisplayValue displayValue = new DisplayValue();

            //Store the preferred display format
            displayValue.Units = "mg/dL";
            displayValue.Value = itemEdits.Value.Value;
            objM.DisplayValue = displayValue;

            if (itemEdits.Value.IsDirty) thing.Value = objM;

            //set Glucose Measurement Type
            string strGlucoseMeasurementType_Key = "wb";// "whole-blood";
            string strGlucoseMeasurementType = "whole-blood";
            //=============================================================
            //Create a coded-value for GlucoseMeasurementType and see if we can 
            //look up a match
            //=============================================================

            thing.GlucoseMeasurementType = HVManager.CodedAndCodable.GetCodableValue(strGlucoseMeasurementType,
                                                strGlucoseMeasurementType_Key,
                                                VocabHelper.STR_GLUCOSE_MEASUEMENT_TYPE_KEY,
                                                VocabHelper.STR_FAMILY_WC);

            if (itemEdits.ActionTaken.IsDirty) thing.ActionTaken = itemEdits.ActionTaken.Value;

            if (itemEdits.ReadingType.IsDirty)
            {
                if (!string.IsNullOrEmpty(itemEdits.ReadingType.Value))
                {
                    string strRT = itemEdits.ReadingType.Value;
                    if (itemEdits.ReadingType.Value.ToLower() == "postprandial" || itemEdits.ReadingType.Value.ToLower() == "after meal")
                    {
                        strRT = "non-fasting";
                    }

                    CodableValue codableValueGlucoseMeasurementContext = HVManager.CodedAndCodable.GetCodableValue(strRT,
                                                    VocabHelper.STR_GLUCOSE_MEASUEMENT_CONTEXT_KEY,
                                                    VocabHelper.STR_FAMILY_WC);

                    if (codableValueGlucoseMeasurementContext == null)
                    {
                        codableValueGlucoseMeasurementContext = new CodableValue();
                    }


                    codableValueGlucoseMeasurementContext.Text = itemEdits.ReadingType.Value;
                    thing.MeasurementContext = codableValueGlucoseMeasurementContext;
                }
                else
                {
                    thing.MeasurementContext = null;
                }                
            }

            if (itemEdits.PolkaItemID.IsDirty) thing.PolkaItemID = itemEdits.PolkaItemID.Value;

            if (itemEdits.PolkaItemID.IsDirty)
            {
                if (!String.IsNullOrEmpty(itemEdits.PolkaItemID.Value))
                {
                    thing.CommonData.ClientId = "POLKA-" + itemEdits.PolkaItemID.Value;
                }
                else
                {
                    thing.CommonData.ClientId = null;
                }
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}