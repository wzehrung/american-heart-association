﻿namespace HVManager
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Linq;
    using HVWrapper.ExtendedThingTypes;

    /// <summary>
    /// This class use used for managing an in-memory, per user cache for Blood Glucose Data.
    /// 
    /// NOTE: This class is implemented accross several files, the primary file is "HeightDataManager.cs".
    /// </summary>
    [Serializable]
    public partial class HeightDataManager : LocalThingTypeDataManager<HeightDataManager.HeightItem>, ILocalThingTypeDataManager
    {
        [Serializable]
        public class HeightItem : LocalThingTypeDataItem, ILocalThingTypeDataItem
        {
            public PossibleUpdate<double> HeightInMeters { get; private set; }
            public PossibleUpdate<DateTime> When { get; private set; }

            public double HeightInInches
            {
                get
                {
                    double dInches = UnitConversion.MetersToInches(HeightInMeters.Value);
                    return Math.Round(dInches % 12, 2);
                }
            }

            public double HeightInFeet
            {
                get
                {
                    double dInches = UnitConversion.MetersToInches(HeightInMeters.Value);

                    return (int)UnitConversion.InchesToFeet(dInches);
                }
            }

            public bool IsDirty
            {
                get
                {
                    return (HeightInMeters.IsDirty || When.IsDirty
                        || Note.IsDirty
                        || Source.IsDirty);
                }
            }

            public void ResetDirtyFlag()
            {
                HeightInMeters.ResetDirtyFlag();
                Source.ResetDirtyFlag();
                Note.ResetDirtyFlag();
                When.ResetDirtyFlag();
            }

            public HeightItem()
            {
                HeightInMeters = new PossibleUpdate<double>();
                When = new PossibleUpdate<DateTime>();
                Source = new PossibleUpdate<string>();
                Note = new PossibleUpdate<string>();
                ResetDirtyFlag();
            }

            public HeightItem(Guid gPersonGUID, Guid gRecordGUID, Height item)
                : base(gPersonGUID, gRecordGUID, item)
            {
                //Store the values
                HeightInMeters = new PossibleUpdate<double>(item.Value.Meters);
                this.When = new PossibleUpdate<DateTime>(item.When.ToDateTime());

                string brandData;
                string brandDetails;
                BrandLogoHelperMachine.SeekBrandDataFromMSHealthThing(gPersonGUID, gRecordGUID,
                        item,
                        out brandData,
                        out brandDetails);

                Source = new PossibleUpdate<string>(brandData);
                SourceDetails = brandDetails;

                ResetDirtyFlag();
            }
        }

        DateTime dtStartDefault = DateTime.MinValue;

        public bool ShouldQuery(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);
        }

        public bool ShouldQuery()
        {
            DateTime queryStart;
            DateTime queryEnd;

            return GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);
        }

        public HVDataTypeFilter GetHVFilter(DateTime dtStart, DateTime dtEnd)
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStart, dtEnd, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(Height.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        public override HVDataTypeFilter GetHVFilter()
        {
            DateTime queryStart;
            DateTime queryEnd;

            bool bShouldQuery = GetAdjustedDatesForQuery(dtStartDefault, DateTime.MaxValue, out queryStart, out queryEnd);

            HVDataTypeFilter dataTypeFilter = new HVDataTypeFilter(Height.TypeId, null, null, queryStart, queryEnd);
            return dataTypeFilter;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="healthRecordCache"></param>
        public HeightDataManager(Guid hvpersonid, Guid hvrecordid)
            :
            base(hvpersonid, hvrecordid)
        {
        }

        public override int MaxSizeOfCache
        {
            get
            {
                return LocalThingTypeDataManager<LocalThingTypeDataItem>.SuggestedMaxCacheSize;
            }
        }


        /// <summary>
        /// Queries HealthVault for the Things that meet the criteria below
        /// </summary>
        /// <param name="MSHealthInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="thingIDs"></param>
        /// <param name="maxNumberItemsToReturn"></param>
        /// <param name="checkForReturnItemsOverflow"></param>
        /// <param name="didResultsOverflow"></param>
        /// <returns>List<BGItem></returns>
        protected override List<HeightItem> QueryForThings(
            Nullable<DateTime> startDate,
            Nullable<DateTime> endDate,
            Collection<Guid> thingIDs,
            int maxNumberItemsToReturn,
            bool checkForReturnItemsOverflow,
            out bool didResultsOverflow)
        {

            List<HeightItem> dataItemsHeight = new List<HeightItem>();

            HealthRecordItemCollection dataItems;

            didResultsOverflow = false;
            WCQueries.helper_PerformMSHealthQuery(
               Helper.GetAccessor(PersonGUID, RecordGUID),
               Height.TypeId,
               startDate,
               endDate,
               thingIDs,
               null,
               maxNumberItemsToReturn,
               checkForReturnItemsOverflow,
               out dataItems,
               out didResultsOverflow);

            IEnumerable<Height> listBG = dataItems.Cast<Height>();

            foreach (Height thing in listBG)
            {
                HeightItem item = new HeightItem(PersonGUID, RecordGUID, thing);
                dataItemsHeight.Add(item);
            }

            return dataItemsHeight;
        }

        public Height PopulateItem(HeightItem itemEdits)
        {
            Height thing = new Height();

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            return thing;
        }

        public HeightItem CreateItem(HeightItem itemEdits)
        {
            Height thing = new Height();

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            _UpdateFields(thing, itemEdits);

            if (string.IsNullOrEmpty(itemEdits.Source.Value))
            {
                thing.CommonData.Source = HVManager.Helper.DataSourceThisApplication;
            }

            //=============================================================
            //Save the new Thing
            //=============================================================
            accessor.NewItem(thing);

            if (IsDateInsideCacheWindow(itemEdits.When.Value))
            {
                FetchAndStoreItemIfDateWindowFits(thing.Key.Id);
            }

            return new HeightItem(PersonGUID, RecordGUID, thing);
        }

        public HeightItem UpdateItem(
            HeightItem itemEdits)
        {
            if (!itemEdits.IsDirty)
                return itemEdits;

            HealthRecordAccessor accessor = Helper.GetAccessor(PersonGUID, RecordGUID);

            HealthRecordItem thing = WCQueries.helper_GetSingleThing(accessor, itemEdits.ThingKey.Id);
            if (thing == null || !thing.Key.Equals(itemEdits.ThingKey))
            {
                throw new HealthServiceException("The version stamp did not match the current thing version.");
            }

            Height myThing = (Height)thing;

            _UpdateFields(myThing, itemEdits);

            accessor.UpdateItem(myThing);

            //Remove the item if it was in the cache
            RemoveItem(myThing.Key.Id);

            //If the updated data is within the range of our data-cache, then
            //add it to the data cache.  (Otherwise, don't worry about it, since
            //it is not needed to keep our cache fully stocked with all data in
            //the date range it cares about)
            if (IsDateInsideCacheWindow(myThing.EffectiveDate))
            {
                FetchAndStoreItemIfDateWindowFits(myThing.Key.Id);
            }

            //Return the updated thing
            return new HeightItem(PersonGUID, RecordGUID, myThing);
        }

        private void _UpdateFields(Height thing, HeightItem itemEdits)
        {
            //Set the field if appropriate
            if (itemEdits.When.IsDirty)
            {
                thing.When = new HealthServiceDateTime(itemEdits.When.Value);
            }

            if (itemEdits.HeightInMeters.IsDirty)
            {
                thing.Value = new Length(itemEdits.HeightInMeters.Value);
            }

            //=============================================================
            //Store the source of the data
            //=============================================================
            if (itemEdits.Source.IsDirty) thing.CommonData.Source = itemEdits.Source.Value;
            if (itemEdits.Note.IsDirty) thing.CommonData.Note = itemEdits.Note.Value;
        }
    }
}