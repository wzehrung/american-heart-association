﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVManager
{
    [Serializable]
    public class TrackableList<T> : List<T>, IPossibleUpdate where T : IPossibleUpdate
    {
        bool m_bIsDirty = true;
        public bool IsDirty
        {
            set
            {
                m_bIsDirty = true;
            }

            get
            {
                if (m_bIsDirty)
                    return true;

                foreach (IPossibleUpdate item in this)
                {
                    if (item.IsDirty)
                        return true;
                }
                return false;

            }

        }

        public void ResetDirtyFlag()
        {
            foreach (IPossibleUpdate item in this)
            {
                item.ResetDirtyFlag();
            }

            m_bIsDirty = false;
        }

        public TrackableList()
        {

        }
    }
}
