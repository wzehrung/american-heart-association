﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVManager
{

    public static class VocabHelper
    {
        public const string STR_FAMILY_WC = "wc";
        public const string STR_ETHNICITY_KEY = "ethnicity-types";
        public const string STR_BLOOD_TYPE_KEY = "blood-types";
        public const string STR_NAME_SUFFIXES_KEY = "name-suffixes";
        public const string STR_NAME_PREFIXES_KEY = "name-prefixes";
        public const string STR_GLUCOSE_MEASUEMENT_TYPE_KEY = "glucose-measurement-type";
        public const string STR_GLUCOSE_MEASUEMENT_CONTEXT_KEY = "glucose-measurement-context";
        public const string STR_MARITAL_STATUS_KEY = "marital-status";
        public const string STR_AEROBIC_ACTIVITIES_KEY = "aerobic-activities";
        public const string STR_PERSON_TYPES_KEY = "person-types";
        public const string STR_STATES_KEY = "states";
        public const string STR_RELATIONSHIP_TYPES_KEY = "relationship-types";
        public const string STR_COUNTRY_KEY = "iso3166";
        public const string STR_FAMILY_ISO = "iso";
        public const string STR_VERSION_1_0 = "1.0";
        public const string STR_VERSION_1 = "1";

        public const string STR_EXERCISE_DETAIL_NAMES_KEY = "exercise-detail-names";
        public const string STR_EXERCISE_INTENSITIES_KEY = "intensity-one-to-five";
        public const string STR_EXERCISE_UNITS_KEY = "exercise-units";
        
        public const string STR_EXERCISE_INTERSITY = "Intensity";
        public const string STR_EXERCISE_STEPS_COUNT = "Steps_count";
        public const string STR_EXERCISE_STEPS_COUNT_VALUE = "Count";

        public const string STR_EXERCISE_INTENSITY_LEVEL_LOW = "low";
        public const string STR_EXERCISE_INTENSITY_LEVEL_HIGH = "high";
        public const string STR_EXERCISE_INTENSITY_LEVEL_MODERATE = "moderate";
        public const string STR_EXERCISE_INTENSITY_LEVEL_VERY_HIGH = "very high";
        public const string STR_EXERCISE_INTENSITY_LEVEL_VERY_LOW = "very low";

        /// <summary>
        /// Search for the  text in the vocabulary we hold for this kind of data.
        /// Returns NULL of the text does not exist in the vocabulary.  Otherwise
        /// returns the coded value
        /// </summary>
        /// <param name="exerciseText"></param>
        /// <param name="vocabulary"></param>
        /// <returns></returns>
        public static CodedValue MatchItemInVocabulary(string textToFind, string strName, string strFamily)
        {
            VocabularyKey relKey = new VocabularyKey(strName, strFamily);

            List<VocabularyItem> list = GetVocabularyItems(relKey);

            foreach (VocabularyItem vocabItem in list)
            {
                //If the text specified matches the vocabulary abbreviated, long or coded
                //value, we'll consider this a match
                if ((string.Compare(textToFind, vocabItem.DisplayText, true) == 0) ||
                   (string.Compare(textToFind, vocabItem.AbbreviationText, true) == 0) ||
                   (string.Compare(textToFind, vocabItem.Value, true) == 0))
                {
                    return new CodedValue(vocabItem.Value, relKey.Name, relKey.Family, vocabItem.Version);
                }
            }

            //No match found
            return null;
        }

        public static string GetDisplayTextFromCode(string textToFind, List<VocabularyItem> listVocab)
        {

            foreach (VocabularyItem vocabItem in listVocab)
            {
                //If the text specified matches the vocabulary abbreviated, long or coded
                //value, we'll consider this a match
                if ((string.Compare(textToFind, vocabItem.DisplayText, true) == 0) ||
                   (string.Compare(textToFind, vocabItem.AbbreviationText, true) == 0) ||
                   (string.Compare(textToFind, vocabItem.Value, true) == 0))
                {
                    return vocabItem.DisplayText;
                }
            }

            //No match found
            return string.Empty;
        }

        public static string GetListCodeFromCode(string textToFind, List<VocabularyItem> listVocab)
        {

            foreach (VocabularyItem vocabItem in listVocab)
            {
                //If the text specified matches the vocabulary abbreviated, long or coded
                //value, we'll consider this a match
                if ((string.Compare(textToFind, vocabItem.DisplayText, true) == 0) ||
                   (string.Compare(textToFind, vocabItem.AbbreviationText, true) == 0) ||
                   (string.Compare(textToFind, vocabItem.Value, true) == 0))
                {
                    return vocabItem.Value;
                }
            }

            //No match found
            return string.Empty;
        }

        public static List<VocabularyItem> GetVocabularyList(string strKey, string strFamily)
        {
            VocabularyKey stateKey = new VocabularyKey(strKey, strFamily);
            return GetVocabularyItems(stateKey);
        }

        public static List<VocabularyItem> GetVocabularyList(string strKey, string strFamily, string strVersion)
        {
            VocabularyKey stateKey = new VocabularyKey(strKey, strFamily, strVersion);
            return GetVocabularyItems(stateKey);
        }

        //public static List<VocabularyItem> GetStatesVocabulary()
        //{
        //    VocabularyKey stateKey = new VocabularyKey(STR_STATES_KEY, STR_FAMILY_WC);
        //    return GetVocabularyItems(stateKey);
        //}

        //public static List<VocabularyItem> GetRelationshipVocabulary()
        //{
        //    VocabularyKey relKey = new VocabularyKey(STR_RELATIONSHIP_TYPES_KEY, STR_FAMILY_WC);
        //    return GetVocabularyItems(relKey);
        //}

        //public static List<VocabularyItem> GetEthnicityVocabulary()
        //{
        //    VocabularyKey relKey = new VocabularyKey(STR_ETHNICITY_KEY, STR_FAMILY_WC);
        //    return GetVocabularyItems(relKey);
        //}

        //public static List<VocabularyItem> GetCountriesVocabulary()
        //{
        //    VocabularyKey relKey = new VocabularyKey("iso3166", "iso", "1.0");
        //    return GetVocabularyItems(relKey);
        //}

        //The assumption is that amongst the set of vocabulaies used insde an application 
        //basis the vocab name is going to be unique


        static Dictionary<string, Dictionary<string, List<VocabularyItem>>> m_vocabCache = new Dictionary<string, Dictionary<string, List<VocabularyItem>>>();

        private static List<VocabularyItem> GetVocabularyItems(VocabularyKey key)
        {
            if (m_vocabCache.ContainsKey(System.Threading.Thread.CurrentThread.CurrentUICulture.Name) && m_vocabCache[System.Threading.Thread.CurrentThread.CurrentUICulture.Name].ContainsKey(key.Name))
            {
                return m_vocabCache[System.Threading.Thread.CurrentThread.CurrentUICulture.Name][key.Name];
            }

            lock (m_vocabCache)
            {
                if (m_vocabCache.ContainsKey(System.Threading.Thread.CurrentThread.CurrentUICulture.Name) && m_vocabCache[System.Threading.Thread.CurrentThread.CurrentUICulture.Name].ContainsKey(key.Name))
                {
                    return m_vocabCache[System.Threading.Thread.CurrentThread.CurrentUICulture.Name][key.Name];
                }


                List<KeyValuePair<string, VocabularyItem>> vocabResult = GetFullVocabulary(key);

                if (m_vocabCache.ContainsKey(System.Threading.Thread.CurrentThread.CurrentUICulture.Name))
                {
                    m_vocabCache[System.Threading.Thread.CurrentThread.CurrentUICulture.Name].Add(key.Name, vocabResult.Select(x => x.Value).ToList());
                }
                else
                {
                    Dictionary<string,List<VocabularyItem>> list = new Dictionary<string,List<VocabularyItem>>();
                     list.Add(key.Name, vocabResult.Select(x => x.Value).ToList());
                     m_vocabCache.Add(System.Threading.Thread.CurrentThread.CurrentUICulture.Name, list);
                }
            }
            return m_vocabCache[System.Threading.Thread.CurrentThread.CurrentUICulture.Name][key.Name];
        }


        private static List<KeyValuePair<string, VocabularyItem>> GetFullVocabulary(VocabularyKey key)
        {



            Console.WriteLine("Download start and counting.....");
            List<KeyValuePair<string, VocabularyItem>> items = new List<KeyValuePair<string, VocabularyItem>>();
            ApplicationConnection conn = new ApplicationConnection(Microsoft.Health.Web.WebApplicationConfiguration.AppId, Microsoft.Health.Web.WebApplicationConfiguration.HealthServiceUrl);
            Vocabulary vocabulary = conn.GetVocabulary(key, false);


            int totalCount = 0;
            while (true)
            {
                string lastVocabValue = null;
                foreach (KeyValuePair<string, VocabularyItem> item in vocabulary)
                {
                    items.Add(item);
                    lastVocabValue = item.Key;
                }
                totalCount += vocabulary.Count;

                Console.WriteLine("....." + totalCount.ToString());

                if (!vocabulary.IsTruncated)
                    break;

                key = new VocabularyKey(key.Name, key.Family, key.Version, lastVocabValue);
                vocabulary = conn.GetVocabulary(key, false);
            }

            return items;

        }
    }
}
