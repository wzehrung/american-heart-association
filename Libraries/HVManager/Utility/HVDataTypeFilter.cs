﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVManager
{
    public class HVDataTypeFilter
    {
        public Guid TypeId;
        public string XPathFilter;
        public int? MaxItemsToReturn;
        public DateTime EffectiveDateMin;
        public DateTime EffectiveDateMax;
        public bool FetchAuditInfo;

        public HVDataTypeFilter(Guid t, string x, int? iMaxItemsToReturn, DateTime dtEffectiveDateMin, DateTime dtEffectiveDateMax)
        {
            this.TypeId = t;
            this.XPathFilter = x;
            this.MaxItemsToReturn = iMaxItemsToReturn;
            this.EffectiveDateMin = dtEffectiveDateMin;
            this.EffectiveDateMax = dtEffectiveDateMax;

        }

        public HVDataTypeFilter(Guid t, string x, int? iMaxItemsToReturn, DateTime dtEffectiveDateMin, DateTime dtEffectiveDateMax, bool bFetchAuditInfo)
        {
            this.TypeId = t;
            this.XPathFilter = x;
            this.MaxItemsToReturn = iMaxItemsToReturn;
            this.EffectiveDateMin = dtEffectiveDateMin;
            this.EffectiveDateMax = dtEffectiveDateMax;
            this.FetchAuditInfo = bFetchAuditInfo;
        }
    }
}
