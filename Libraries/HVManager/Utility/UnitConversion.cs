﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVManager
{
    /// <summary>
    /// This class manages all unit conversions
    /// </summary>
    public class UnitConversion
    {
        /// <summary>
        /// Feet to meters conversion
        /// </summary>
        /// <param name="dFeet"></param>
        /// <returns>double</returns>
        public static double FeetToMeters(double dFeet)
        {
            return dFeet * 0.3048;
        }

        /// <summary>
        /// Meters to feet conversion
        /// </summary>
        /// <param name="dMeter"></param>
        /// <returns>double</returns>
        public static double MetersToFeet(double dMeter)
        {
            return dMeter / 0.3048;
        }

        /// <summary>
        /// Inches to meters conversion
        /// </summary>
        /// <param name="dInches"></param>
        /// <returns>double</returns>
        public static double InchesToMeters(double dInches)
        {
            return dInches * 0.0254;
        }

        /// <summary>
        /// Meters to inches conversion
        /// </summary>
        /// <param name="dMeter"></param>
        /// <returns>double</returns>
        public static double MetersToInches(double dMeter)
        {
            return dMeter / 0.0254;
        }

        /// <summary>
        /// Feet to inches conversion
        /// </summary>
        /// <param name="dFeet"></param>
        /// <returns>double</returns>
        public static double FeetToInches(double dFeet)
        {
            return dFeet * 12;
        }

        /// <summary>
        /// Inches to feet conversion
        /// </summary>
        /// <param name="dInches"></param>
        /// <returns>double</returns>
        public static double InchesToFeet(double dInches)
        {
            return dInches / 12;
        }
    }
}
