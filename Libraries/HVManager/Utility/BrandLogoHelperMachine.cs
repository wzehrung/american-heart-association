﻿namespace HVManager
{
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //
    using System;
    using System.Configuration;
    using System.Web;
    using System.Web.UI;
    using Microsoft.Health.ItemTypes;
    using Microsoft.Health;
    using System.Xml.XPath;
    using System.IO;
    using System.Xml;
    using HVManager;
    using System.Text;
    /// <summary>
    /// Enables our application to get logo/brand information about Thing data.
    /// 
    /// </summary>
    public static class BrandLogoHelperMachine
    {

        /// <summary>
        /// This function is called to get the DataSource brand info
        /// from the calling application.
        /// 
        /// Typically either a WebPage or the webpage's MasterPage offers
        /// this information through the IHealthApplicationInfo interface.
        /// 
        /// If no brand information is offered, NULL is returned.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static string GetApplicationBrandInfo(Page page)
        {
            IHealthApplicationInfo iHealthAppInfo;

            //===========================================================
            //See if the calling page has Brand information to offer us
            //===========================================================
            //1st, see if the calling page has brand data
            iHealthAppInfo = page as IHealthApplicationInfo;

            //If not, go one level up to the master page
            if (iHealthAppInfo == null)
            {
                MasterPage masterPage = page.Master;
                if (masterPage != null)
                {
                    iHealthAppInfo = masterPage as IHealthApplicationInfo;
                }
            }

            //===========================================================
            //If available, get the brand data
            //===========================================================
            if (iHealthAppInfo == null) return null;

            return iHealthAppInfo.HealthDataSource;
        }



        const string logoUnknownSourceData = "UnknownSource";

        /// <summary>
        /// This function looks inside a data-item returned from the server and determines
        /// </summary>
        /// <param name="thing"></param>
        /// <param name="sourceMajor">Typically the company name, may be null</param>
        /// <param name="sourceDetails">Typically the model number, may be null</param>
        public static void SeekBrandDataFromMSHealthThing(Guid gPersonGUID, Guid gRecordGUID, HealthRecordItem thing, out string sourceMajor, out string sourceDetails)
        {

            //Default to no brand data
            sourceMajor = null;
            sourceDetails = null;

            bool bHasWpdExtension = false;

            const string WindowsPortableDevice = "WPD";

            //NOTE: Heart rate sample data is stored as a proprietary extension.  This means that we 
            //      need to know what per-manufactuer extensions to look for
            foreach (HealthRecordItemExtension thingExtension in thing.CommonData.Extensions)
            {
                if (thingExtension.Source == WindowsPortableDevice)
                {
                    bHasWpdExtension = true;
                    const string brandQuery = "//Property[@Key=\"{26D4979A-E643-4626-9E2B-736DC0C92FDC}.7\"]/Value";
                    XPathNavigator xPathNavBrand = thingExtension.ExtensionData.CreateNavigator();
                    XPathNavigator xParthSourceMajor = xPathNavBrand.SelectSingleNode(brandQuery);
                    if (xParthSourceMajor != null)
                    {
                        sourceMajor = xParthSourceMajor.InnerXml.Trim();
                        //If we have a major brand, see if there is a details item
                        if (sourceMajor != null)
                        {
                            const string modelNameQuery = "//Property[@Key=\"{26D4979A-E643-4626-9E2B-736DC0C92FDC}.8\"]/Value";
                            XPathNavigator xParthSourceMinor = xPathNavBrand.SelectSingleNode(modelNameQuery);
                            if (xParthSourceMinor != null)
                            {
                                sourceDetails = xParthSourceMinor.InnerXml.Trim();
                            }
                        }
                    }
                }

                //If we found the name of a major brand we want to use, then return
                if (!string.IsNullOrEmpty(sourceMajor))
                {
                    return;
                }
            } //keep looking in all the extensions

            if (bHasWpdExtension && String.IsNullOrEmpty(thing.CommonData.Source))
            {
                //COMMENTED OUT - NEEd TO FETCH INFO FOR EACH ITEM AND THIS IS TIME CONSUMING, WHY NOT JUST DISPLAY "Portable Device"
                //try
                //{
                //    GetWpdData(gPersonGUID, gRecordGUID, thing, out sourceMajor, out sourceDetails);
                //}
                //catch (System.Exception ex)
                //{
                //    GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
                //}

                if (String.IsNullOrEmpty(sourceMajor))
                {
                    sourceMajor = "Portable Device";
                    sourceDetails = "Portable Device";
                }
            }
            else
            {
                //If we cannot find any more detailed data, return free form text in the souce field
                sourceMajor = thing.CommonData.Source;
            }
        }

        #region New HV WPD
        /// <summary>
        /// The sample function demonstrating how to get WPD data from items uploaded to HealthVault 
        /// by v 2.x client (new behavior).
        /// It accesses first self-record in the account associated with the AuthenticatedConnection object,
        /// retrieves all items of weight type and retrieves WPD data if exist
        /// </summary>
        /// <param name="authenticatedConnection"></param>
        private static void GetWpdData(Guid gPersonGUID, Guid gRecordGUID, HealthRecordItem healthRecordItem, out string sourceMajor, out string sourceDetails)
        {
            sourceMajor = string.Empty;
            sourceDetails = string.Empty;

            string itemXml = healthRecordItem.GetItemXml();
            if (healthRecordItem.CommonData != null &&
                healthRecordItem.CommonData.Extensions != null)
            {
                // create variable to contain WPD data
                string dataWpd = string.Empty;

                // going through each Extension in HealthRecordItem and proceed if it has WPD data
                foreach (HealthRecordItemExtension healthRecordItemExtension in healthRecordItem.CommonData.Extensions)
                {
                    if (string.Compare(healthRecordItemExtension.Source, "WPD", true) == 0) // the item has WPD extension
                    {
                        HealthRecordItem healthRecordItemDataOther = Helper.GetAccessor(gPersonGUID, gRecordGUID).GetItem(healthRecordItem.Key.Id, HealthRecordItemSections.BlobPayload);

                        bool bFound = false;
                        BlobStore objBlobStore = healthRecordItemDataOther.GetBlobStore(Helper.GetAccessor(gPersonGUID, gRecordGUID));
                        
                        if (objBlobStore == null)
                            continue;

                        foreach (Blob objBlob in objBlobStore.Values)
                        {
                            byte[] b = objBlob.ReadAllBytes();
                            b = Encoding.Convert(Encoding.GetEncoding("iso-8859-1"), Encoding.UTF8, b);
                            dataWpd = Encoding.UTF8.GetString(b, 0, b.Length);
                            dataWpd = dataWpd.Substring(dataWpd.IndexOf("<"));
                            //if (objBlob != null && (string.Compare(objBlob.ContentEncoding, "base64", true) == 0 ||
                            //string.Compare(objBlob.ContentEncoding, "base-64", true) == 0))
                            //{
                            //    // converting base64 zipped items to string
                            //    dataWpd = objBlob.ReadAsString();
                            //    bFound = true;
                            //}

                            if (!string.IsNullOrEmpty(dataWpd))
                            {
                                bFound = true;
                                break;
                            }
                        }
                        
                        if(bFound)
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(dataWpd))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.InnerXml = dataWpd;
                    // do something with WPD data    
                    const string brandQuery = "//Property[@Key=\"{26D4979A-E643-4626-9E2B-736DC0C92FDC}.7\"]/Value";
                    XPathNavigator xPathNavBrand = xDoc.CreateNavigator();
                    XPathNavigator xParthSourceMajor = xPathNavBrand.SelectSingleNode(brandQuery);
                    if (xParthSourceMajor != null)
                    {
                        sourceMajor = xParthSourceMajor.InnerXml.Trim();
                        //If we have a major brand, see if there is a details item
                        if (sourceMajor != null)
                        {
                            const string modelNameQuery = "//Property[@Key=\"{26D4979A-E643-4626-9E2B-736DC0C92FDC}.8\"]/Value";
                            XPathNavigator xParthSourceMinor = xPathNavBrand.SelectSingleNode(modelNameQuery);
                            if (xParthSourceMinor != null)
                            {
                                sourceDetails = xParthSourceMinor.InnerXml.Trim();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decodes string from base 64 and decodes it using GZip decompression
        /// </summary>
        /// <param name="source"></param>
        /// <returns>Decoded from base-64 and decompressed string</returns>
        private static string Base64GZipTextToString(string source)
        {
            // verify argument
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentNullException("source", "Parameter cannot be null or empty");
            }


            // convert the string to Unicode bytes
            byte[] srcBuffer = Convert.FromBase64String(source);


            //_compressedBufferSize = srcBuffer.Length;


            using (MemoryStream memoryStream = new MemoryStream())
            {
                // compress the buffer
                memoryStream.Write(
                    srcBuffer,
                    0,
                    srcBuffer.Length);


                // reset the memory stream to read the decompressed data
                memoryStream.Position = 0;


                System.IO.Compression.GZipStream decompression =
                    new System.IO.Compression.GZipStream(
                        memoryStream,
                        System.IO.Compression.CompressionMode.Decompress);


                byte[] decompressedBytes = ReadAllBytesFromStream(decompression);


                //_decompressedBufferSize = decompressedBytes.Length;


                // reading compressed buffer back
                //decompression.Read(decompressedBytes, 0, decompressedBytes.Length);


                decompression.Close();


                return System.Text.Encoding.UTF8.GetString(decompressedBytes);
            }
        }


        private const int buffer_size = 100;


        private static byte[] ReadAllBytesFromStream(System.IO.Compression.GZipStream stream)
        {
            // Use this method is used to read all bytes from a stream.
            int buffer_size = 100;
            byte[] buffer = new byte[buffer_size];
            using (MemoryStream memoryStream = new MemoryStream())
            {
                while (true)
                {
                    int bytesRead = stream.Read(buffer, 0, buffer_size);
                    if (bytesRead == 0)
                    {
                        break;
                    }
                    memoryStream.Write(buffer, 0, bytesRead);
                }


                // reset the buffer to read the data
                memoryStream.Position = 0;


                byte[] newbuffer = new byte[memoryStream.Length];
                memoryStream.Read(newbuffer, 0, newbuffer.Length);


                return newbuffer;
            }
        }

        #endregion
    }
}