﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVManager
{
    [Serializable]
    public class EmailItem : IPossibleUpdate
    {
        public PossibleUpdate<string> Email { get; set; }
        public PossibleUpdate<string> Name { get; set; }
        public object NonPersistedData { get; set; }

        public EmailItem()
        {
            Email = new PossibleUpdate<string>();
            Name = new PossibleUpdate<string>();
            ResetDirtyFlag();
        }

        public EmailItem(string email, string name)
        {
            Email = new PossibleUpdate<string>(email);
            Name = new PossibleUpdate<string>(name);
            ResetDirtyFlag();
        }

        #region IPossibleUpdate Members

        public bool IsDirty
        {
            get { return Email.IsDirty || Name.IsDirty; }
        }


        public void ResetDirtyFlag()
        {
            Email.ResetDirtyFlag();
            Name.ResetDirtyFlag();
        }

        #endregion
    }
}
