﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health.ItemTypes;

namespace HVManager
{
    public class CodedAndCodable
    {
        public static CodableValue GetCodableValue(string strText, string strKey, string strFamily)
        {
            if (string.IsNullOrEmpty(strText))
            {
                return null;
            }
            else
            {
                //=============================================================
                //Create a coded-value for ethnicity and see if we can 
                //look up a match
                //=============================================================
                CodableValue codableValue =
                                       new CodableValue();

                //Store the text as the user specified it
                codableValue.Text = strText;

                //See if this matches an ethnicity
                CodedValue recognizedValue =
                    VocabHelper.MatchItemInVocabulary(
                                        strText,
                                        strKey, strFamily);

                //If we have got a match, add it to 
                if (recognizedValue != null)
                    codableValue.Add(recognizedValue);

                return codableValue;
            }
        }

        public static CodableValue GetCodableValue(string strText, string strValue, string strKey, string strFamily)
        {
            if (string.IsNullOrEmpty(strText))
            {
                return null;
            }
            else
            {
                //=============================================================
                //Create a coded-value for ethnicity and see if we can 
                //look up a match
                //=============================================================
                CodableValue codableValue =
                                       new CodableValue();

                //Store the text as the user specified it
                codableValue.Text = strText;

                //See if this matches an ethnicity
                CodedValue recognizedValue =
                    VocabHelper.MatchItemInVocabulary(
                                        strValue,
                                        strKey, strFamily);

                //If we have got a match, add it to 
                if (recognizedValue != null)
                    codableValue.Add(recognizedValue);

                return codableValue;
            }
        }
    }
}
