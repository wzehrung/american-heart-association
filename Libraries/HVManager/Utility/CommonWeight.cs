﻿namespace HVManager
{
    using System;
    using System.Text;

    /// <summary>
    /// A unit of weight
    /// </summary>
    /// 

    [Serializable]
    public struct CommonWeight
    {
        public enum WeightUnit
        {
            Unknown,
            Pounds,
            Kilograms
        }

        /// <summary>
        /// True if the Weight is unknown
        /// </summary>
        public bool IsUnknown
        {
            get
            {
                return (this.PreferredUnit == WeightUnit.Unknown);
            }
        }


        public double convertTo(WeightUnit unitType)
        {
            switch (unitType)
            {
                case WeightUnit.Kilograms:
                    return this.ToKilograms();

                case WeightUnit.Pounds:
                    return this.ToPounds();

                case WeightUnit.Unknown:
                default:
                    throw new Exception("An unkown weight type as been encountered.");
                    // System.Diagnostics.Debug.Assert(false); //We should never hit this code.
                    //return 0;
            }
        }

        /// <summary>
        /// Converts a weight unit to a string
        /// </summary>
        /// <param name="unitType"></param>
        /// <returns></returns>
        public static string ConvertUnitToString(WeightUnit unitType)
        {

            switch (unitType)
            {
                case WeightUnit.Unknown:
                    return "unknown";
                case WeightUnit.Kilograms:
                    return "kilograms";
                case WeightUnit.Pounds:
                    return "pounds";
                default:
                    throw new Exception("An unkown weight type as been encountered.");
                    //System.Diagnostics.Debug.Assert(false); //We should never hit this code.
                    //return "ERROR";  //We should never hit this code
            }
        }


        /// <summary>
        /// Converts a weight unit to a string
        /// </summary>
        /// <param name="unitType"></param>
        /// <returns></returns>
        public static string MSHealthConvertUnitToString(WeightUnit unitType)
        {

            switch (unitType)
            {
                case WeightUnit.Unknown:
                    return "unknown";
                case WeightUnit.Kilograms:
                    return "kg";
                case WeightUnit.Pounds:
                    return "lb";
                default:
                    throw new Exception("An unkown weight type as been encountered.");
                    //System.Diagnostics.Debug.Assert(false); //We should never hit this code.
                    //return "ERROR";  //We should never hit this code
            }
        }

        /// <summary>
        /// Tries to convert the unit text (e.g. "lbs", "lb", "pounds", "lb.") into
        /// a single standard format so that all similar unit types look the same.
        /// 
        /// Returns the original text if no noramized unit is known. 
        ///   (e.g. if the weight is specified in 'foo-bars', we will return 'foo-bars' 
        ///    because we do not know a normalized unit for it)
        /// </summary>
        /// <returns></returns>
        public static string AttemptWeightUnitTextNormalization(string unitText)
        {
            WeightUnit unit = ConvertStringToUnit(unitText);
            if (unit == WeightUnit.Unknown)
            {
                return unitText;
            }

            //Return the normalized text for the unit
            return ConvertUnitToString(unit);
        }


        /// <summary>
        /// Converts a unit text to a unit type
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static WeightUnit ConvertStringToUnit(string value)
        {
            return ConvertStringToUnit(value, true);
        }
        /// <summary>
        /// Converts a unit text to a unit type
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static WeightUnit ConvertStringToUnit(string value, bool throwErrorIfFail)
        {
            //Trip any spaces off
            value = value.Trim();

            //go to lower case
            string lcase_value = value.ToLower();

            //It's an error if we are passed in a blank string
            if (lcase_value.Length == 0)
            {
                throw new Exception("An unkown weight type as been encountered.");
            }

            //If there is a "." at the end, remove it.
            if (lcase_value[lcase_value.Length - 1] == '.')
            {
                lcase_value = lcase_value.Substring(0, lcase_value.Length - 1);
            }

            //pounds
            if (lcase_value == "lb") return WeightUnit.Pounds;
            if (lcase_value == "lbs") return WeightUnit.Pounds;
            if (lcase_value == "pound") return WeightUnit.Pounds;
            if (lcase_value == "pounds") return WeightUnit.Pounds;

            //kilograms
            if (lcase_value == "kg") return WeightUnit.Kilograms;
            if (lcase_value == "kgs") return WeightUnit.Kilograms;
            if (lcase_value == "kilogram") return WeightUnit.Kilograms;
            if (lcase_value == "kilograms") return WeightUnit.Kilograms;


            //We don't know the weight unit
            if (throwErrorIfFail)
            {
                throw new Exception("An unkown weight type as been encountered.");
            }
            return WeightUnit.Unknown;
        }

        /// <summary>
        /// What is the preferred unit of data we are working in
        /// </summary>
        public WeightUnit PreferredUnit;


        /// <summary>
        /// Gets the XML text we want to use to store the preferred unit
        /// </summary>
        public string MSHealthXMLPreferredUnitText
        {
            get
            {
                return CommonWeight.MSHealthConvertUnitToString(this.PreferredUnit);
            }
        }

        /// <summary>
        /// Get the text corresponding to the current perfered display unit
        /// </summary>
        public string PerferredUnitText
        {
            get
            {
                return CommonWeight.ConvertUnitToString(this.PreferredUnit);
            }
        }

        private double m_dataAsKilograms;

        const double kilograms_to_pounds = 2.204622623 ;//2.205;arun-added 3 at the end

        /// <summary>
        /// Parse a string (e.g. "12.8 miles" using the current culture for parsing the number
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CommonWeight FromString(string value)
        {
            return CommonWeight.FromString(value, System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Parse a string (e.g. "128.8 pounds" using the current culture for parsing the number
        /// </summary>
        /// <param name="value"></param>
        /// <param name="allowBlank">TRUE = A blank value will return a disance marked UNKNOWN.  FALSE = A blank value will throw an error</param>
        /// <returns></returns>
        public static CommonWeight FromString(string value, bool allowBlank)
        {
            return CommonWeight.FromString(value, System.Globalization.CultureInfo.CurrentCulture, allowBlank);
        }

        /// <summary>
        /// Parses a string with numberic and unit items (e.g. "400.3 feet") into a CommonWeight type
        /// </summary>
        /// <param name="value"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static CommonWeight FromString(string value, System.Globalization.CultureInfo culture)
        {
            return CommonWeight.FromString(value, culture, false);
        }

        /// <summary>
        /// Parses a string with numberic and unit items (e.g. "200.3 kg") into a CommonWeight type
        /// </summary>
        /// <param name="value"></param>
        /// <param name="culture"></param>
        /// <param name="allowBlank">TRUE = A blank value will return a disance marked UNKNOWN.  FALSE = A blank value will throw an error</param>
        /// <returns></returns>
        public static CommonWeight FromString(string value, System.Globalization.CultureInfo culture, bool allowBlank)
        {
            //Remove any whitespace
            value = value.Trim();

            if ((value == null) || (value == ""))
            {
                if (allowBlank)
                {
                    return CommonWeight.UnknownWeight;
                }
                else
                {
                    //We do not allow blank values
                    throw new Exception("Could not parse the weight string ''");
                }
            }

            bool numericFound = false;
            int indexFromRight = value.Length - 1;
            int indexEndOfNumber;


            //============================================================================
            //Search from the right end of the string, going left, and look for the first
            //digit.  This character will signify the end of the #
            //============================================================================
            while ((indexFromRight >= 0) && (numericFound == false))
            {
                //Get the current characters
                char currentChar = value[indexFromRight];
                //If it is a digit, we are done looking
                if (char.IsDigit(currentChar))
                {
                    numericFound = true;
                    indexEndOfNumber = indexFromRight;
                }
                else
                {
                    //keep moving leftward
                    indexFromRight--;
                }
            }

            //Get the numeric part
            string value_numberText = value.Substring(0, indexFromRight + 1);
            //Will throw error if double can not be parsed
            double value_Numeric = System.Double.Parse(value_numberText, culture);

            //Get the text that describes the unit type
            string value_unitText = value.Substring(indexFromRight + 1);

            //===============================================================================
            //Parse the type of the unit
            //===============================================================================
            //Will throw exception if the unit type can not be parsed
            WeightUnit WeightUnit = CommonWeight.ConvertStringToUnit(value_unitText);

            //===============================================================================
            //Based on the unit type, create the type
            //===============================================================================
            switch (WeightUnit)
            {
                case WeightUnit.Unknown:
                    return CommonWeight.UnknownWeight;

                case WeightUnit.Kilograms:
                    return CommonWeight.FromKilograms(value_Numeric);

                case WeightUnit.Pounds:
                    return CommonWeight.FromPounds(value_Numeric);

                default:
                    throw new Exception("An unkown weight type as been encountered.");
                    //System.Diagnostics.Debug.Assert(false); //We should never hit this code.
                    //return CommonWeight.UnknownWeight;  //We should never hit this code
            }
        }

        /// <summary>
        /// Creates a weight from this unit type
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CommonWeight FromPounds(double value)
        {
            //Store the value
            CommonWeight weight = new CommonWeight(value / kilograms_to_pounds);
            //Set the weight to the user's preferred weight
            weight.PreferredUnit = WeightUnit.Pounds;
            return weight;
        }

        /// <summary>
        /// Creates a weight from this unit type
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CommonWeight FromKilograms(double value)
        {
            //Store the value
            CommonWeight weight = new CommonWeight(value);
            //Set the weight to the user's preferred weight
            weight.PreferredUnit = WeightUnit.Kilograms;
            return weight;
        }


        /// <summary>
        /// Returns the weight in this unit
        /// </summary>
        /// <returns></returns>
        public double ToKilograms()
        {
            return m_dataAsKilograms;
        }

        /// <summary>
        /// Returns the weight in this unit
        /// </summary>
        /// <returns></returns>
        public double ToPounds()
        {
            return m_dataAsKilograms * kilograms_to_pounds;
        }

        /// <summary>
        /// Converts to a string in the form of the preferred type
        /// </summary>
        /// <param name="unitType"></param>
        /// <returns></returns>
        public string ToString(WeightUnit unitType)
        {
            //Copy
            CommonWeight duplicate = this;
            //Change unit
            duplicate.PreferredUnit = unitType;
            //Convert to string
            return duplicate.ToString();
        }

        /// <summary>
        /// Converts to a string in the form of the preferred unit, using the user's current culture
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.ToString(System.Globalization.CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts to a string in the form of the preferred unit
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        public string ToString(System.Globalization.CultureInfo culture)
        {
            double unitlessWeight = 0;

            switch (this.PreferredUnit)
            {

                case WeightUnit.Kilograms:
                    unitlessWeight = this.ToKilograms();
                    break;
                case WeightUnit.Pounds:
                    unitlessWeight = this.ToPounds();
                    break;
                default:
                    throw new Exception("An unkown weight type as been encountered.");
                    //System.Diagnostics.Debug.Assert(false); //We should never hit this code
                    //return "ERROR";
            }

            //Round it to 2 decimal places to clip floating point (.999999 / 0.0000001) rounding errors
            double roundedUnitlessWeight = Math.Round(unitlessWeight, 2);
            return roundedUnitlessWeight.ToString(culture) + " " + this.PerferredUnitText;

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="kilograms"></param>
        public CommonWeight(double kilograms)
        {
            this.PreferredUnit = WeightUnit.Kilograms;
            m_dataAsKilograms = kilograms;
        }

        public static CommonWeight FromUnit(double value, WeightUnit unitType)
        {

            switch (unitType)
            {
                case WeightUnit.Kilograms:
                    return CommonWeight.FromKilograms(value);

                case WeightUnit.Pounds:
                    return CommonWeight.FromPounds(value);

                case WeightUnit.Unknown:
                default:
                    throw new Exception("An unkown weight type as been encountered.");
                    //System.Diagnostics.Debug.Assert(false); //We should never hit this code.
                    //return new CommonWeight();
            }

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="kilograms"></param>
        static public CommonWeight UnknownWeight
        {
            get
            {
                CommonWeight weight = new CommonWeight(0);
                weight.PreferredUnit = WeightUnit.Unknown;
                return weight;
            }
        }

    }
}