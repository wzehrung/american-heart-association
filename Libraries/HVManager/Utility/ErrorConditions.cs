﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVManager
{
    public class ErrorConditions
    {
        public static void ThrowServerThingInstanceNotFound()
        {
            throw new Exception("Expected to find a specific object on the server.  Not found!");
        }

        public static void ThrowThingKeysDoNotMatch()
        {
            throw new Exception("Expected thing keys to match; cached thing is out of date");
        }
    }
}
