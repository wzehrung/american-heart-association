﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health.ItemTypes;

namespace HVManager
{
    [Serializable]
    public class HVApproximateDateTime
    {
        public HVApproximateDate HVApproximateDate
        {
            get;
            set;
        }

        public HVApproximateTime HVApproximateTime
        {
            get;
            set;
        }

        public HVApproximateDateTime(ApproximateDateTime dt)
        {
            if (dt == null)
                return;

            if (dt.ApproximateDate != null)
            {
                HVApproximateDate = new HVApproximateDate
                {
                    Year = dt.ApproximateDate.Year,
                    Month = dt.ApproximateDate.Month,
                    Day = dt.ApproximateDate.Day
                };
            }

            if (dt.ApproximateTime != null)
            {
                HVApproximateTime = new HVApproximateTime
                {
                    Hour = dt.ApproximateTime.Hour,
                    Minute = dt.ApproximateTime.Minute,
                    Second = dt.ApproximateTime.Second,
                    MilliSecond = dt.ApproximateTime.Millisecond
                };
            }
        }

        public HVApproximateDateTime(DateTime dt)
        {
            if (dt == null)
                return;

            HVApproximateDate = new HVApproximateDate
            {
                Year = dt.Year,
                Month = dt.Month,
                Day = dt.Day
            };

            HVApproximateTime = new HVApproximateTime
            {
                Hour = dt.Hour,
                Minute = dt.Minute,
                Second = dt.Second,
                MilliSecond = dt.Millisecond
            };
        }

        public ApproximateDateTime GetApproximateDateTime()
        {
            ApproximateDateTime dtRet = null;
            if (HVApproximateDate != null)
            {
                ApproximateDate dt = new ApproximateDate(HVApproximateDate.Year);
                dt.Month = HVApproximateDate.Month;
                dt.Day = HVApproximateDate.Day;

                dtRet = new ApproximateDateTime(dt);

                if (HVApproximateTime != null)
                {
                    ApproximateTime t = new ApproximateTime(HVApproximateTime.Hour, HVApproximateTime.Minute);
                    t.Second = HVApproximateTime.Second;
                    t.Millisecond = HVApproximateTime.MilliSecond;
                    dtRet.ApproximateTime = t;
                }
            }

            return dtRet;
        }

        public bool HasValue()
        {
            return HVApproximateDate != null && HVApproximateDate.Month.HasValue && HVApproximateDate.Day.HasValue;
        }
    }

    [Serializable]
    public class HVApproximateDate
    {
        public int Year
        {
            get;
            set;
        }

        public int? Month
        {
            get;
            set;
        }

        public int? Day
        {
            get;
            set;
        }
    }

    [Serializable]
    public class HVApproximateTime
    {
        public int Hour
        {
            get;
            set;
        }

        public int Minute
        {
            get;
            set;
        }

        public int? Second
        {
            get;
            set;
        }

        public int? MilliSecond
        {
            get;
            set;
        }
    }
}
