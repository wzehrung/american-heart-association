﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health;
using System.Xml.XPath;
using Microsoft.Health.Web;
using System.Xml;

namespace HVManager
{
    [Serializable]
    public class CurrentUserHVSetting
    {
        public string Email
        {
            get;
            set;
        }

        public bool? HasOptedForAHAEmails
        {
            get;
            set;
        }

        private const string sessionKeyCurrentUserContext = "CurrentUserContext";

        public static void SetCurrentUserSetting(AuthenticatedConnection conn, string strEmail, bool bHasOptedForAHAEmails)
        {
            CurrentUserHVSetting objUserSetting = _GetCurrentUserSession();
            if (objUserSetting != null && !string.IsNullOrEmpty(objUserSetting.Email) && objUserSetting.Email.ToLower() == strEmail.ToLower() && objUserSetting.HasOptedForAHAEmails.HasValue && objUserSetting.HasOptedForAHAEmails.Value == bHasOptedForAHAEmails)
                return;

            XmlDocument document = new XmlDocument();
            string strXml = string.Format(@"<app-settings><Email>{0}</Email><HasOptedForAHAEmails>{1}</HasOptedForAHAEmails></app-settings>", strEmail, bHasOptedForAHAEmails ? "1" : "0");

            document.LoadXml(strXml);
            conn.SetApplicationSettings(document);
            _ResetCurrentUserSession();
        }

        public static CurrentUserHVSetting GetCurrentUserSetting(AuthenticatedConnection conn)
        {
            CurrentUserHVSetting objUserSetting = _GetCurrentUserSession();
            if (objUserSetting != null)
            {
                return objUserSetting;
            }

            ApplicationSettings settings = conn.GetAllApplicationSettings();
            objUserSetting = new CurrentUserHVSetting();
            _SetCurrentUserSession(objUserSetting);
            if (settings != null && settings.XmlSettings != null)
            {
                XPathNavigator navigator = settings.XmlSettings.CreateNavigator();

                XPathNavigator emailNode = navigator.SelectSingleNode("app-settings/Email");
                if (emailNode != null)
                {
                    objUserSetting.Email = emailNode.Value;
                }

                objUserSetting.HasOptedForAHAEmails = Helper.ParseTrueFalseNull(navigator, "app-settings/HasOptedForAHAEmails");
            }

            return objUserSetting;
        }

        private static void _SetCurrentUserSession(CurrentUserHVSetting objUserSetting)
        {
            System.Web.HttpContext.Current.Session[sessionKeyCurrentUserContext] = objUserSetting;
        }

        private static CurrentUserHVSetting _GetCurrentUserSession()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[sessionKeyCurrentUserContext] != null)
            {
                return System.Web.HttpContext.Current.Session[sessionKeyCurrentUserContext] as CurrentUserHVSetting;
            }
            return null;
        }

        private static void _ResetCurrentUserSession()
        {
            System.Web.HttpContext.Current.Session[sessionKeyCurrentUserContext] = null;
        }

        public static CurrentUserHVSetting GetUserSetting(Guid hvpersonid, Guid hvrecordid)
        {
            try
            {
                OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
                offlineConn.Authenticate();

                ApplicationSettings settings = offlineConn.GetAllApplicationSettings();

                if (settings != null && settings.XmlSettings != null)
                {
                    XPathNavigator navigator = settings.XmlSettings.CreateNavigator();

                    CurrentUserHVSetting objUserSetting = new CurrentUserHVSetting();

                    XPathNavigator emailNode = navigator.SelectSingleNode("app-settings/Email");
                    if (emailNode != null)
                    {
                        objUserSetting.Email = emailNode.Value;
                    }

                    objUserSetting.HasOptedForAHAEmails = Helper.ParseTrueFalseNull(navigator, "app-settings/HasOptedForAHAEmails");

                    return objUserSetting;
                }
            }
            catch (Exception ex)
            {
                GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
                return null;
            }

            return null;
        }

        public static string GetEmailForAccount(Guid hvpersonid, Guid hvrecordid)
        {
            string strEmail = string.Empty;


            HVManager.CurrentUserHVSetting setting = HVManager.CurrentUserHVSetting.GetUserSetting(hvpersonid, hvrecordid);
            if (setting != null && !string.IsNullOrEmpty(setting.Email))
            {
                strEmail = setting.Email;
            }

            return strEmail;
        }
    }
}
