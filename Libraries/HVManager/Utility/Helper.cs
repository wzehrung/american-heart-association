﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using Microsoft.Health.Web;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using System.Collections.ObjectModel;

namespace HVManager
{
    public class Helper
    {
        public const string DataSourceThisApplication = "Heart360";

        /// <summary>
        /// Returns the Accessor object for a given PersonID and HealthRecordID using an offline connection
        /// </summary>
        /// <param name="hvpersonid"></param>
        /// <param name="hvrecordid"></param>
        /// <returns></returns>
        public static Microsoft.Health.HealthRecordAccessor GetAccessor(Guid hvpersonid, Guid hvrecordid)
        {
            Microsoft.Health.Web.HealthServicePage objCurrentPage = Microsoft.Health.Web.HealthServicePage.CurrentPage;
            if (objCurrentPage != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords.Count > 0)
            {
                return Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords[hvrecordid];
            }
            else
            {
                OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
                offlineConn.Authenticate();
                HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);
                return accessor;
            }
        }

        /// <summary>
        /// Returns the Accessor object for a given PersonID and HealthRecordID using an offline connection
        /// </summary>
        /// <param name="hvpersonid"></param>
        /// <param name="hvrecordid"></param>
        /// <returns></returns>
        public static Microsoft.Health.HealthRecordAccessor GetAccessor(bool bIsOnline, Guid hvpersonid, Guid hvrecordid)
        {
            if (bIsOnline)
            {
                Microsoft.Health.Web.HealthServicePage objCurrentPage = Microsoft.Health.Web.HealthServicePage.CurrentPage;
                if (objCurrentPage != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo != null && Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords.Count > 0)
                {
                    return Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords[hvrecordid];
                }
                else
                {
                    OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
                    offlineConn.Authenticate();
                    HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);
                    return accessor;
                }
            }
            else
            {
                OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
                offlineConn.Authenticate();
                HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);
                return accessor;
            }
        }

        /// <summary>
        /// To convert mmol/l of glucose to mg/dL, multiply by 18.
        /// http://www.faqs.org/faqs/diabetes/faq/part1/section-9.html
        /// </summary>
        /// <param name="dMmol"></param>
        /// <returns>double</returns>
        public static double GlocoseMmolToMgdl(double dMmol)
        {
            return dMmol * 18;
        }

        /// <summary>
        /// To convert mg/dL of glucose to mmol/l, divide by 18 or multiply by 0.055.
        /// http://www.faqs.org/faqs/diabetes/faq/part1/section-9.html
        /// </summary>
        /// <param name="dMgdl"></param>
        /// <returns>double</returns>
        public static double GlucoseMgdlToMmol(double dMgdl)
        {
            return dMgdl / 18;
        }

        public static double? ParseDouble(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            double valueDouble = xNav_Item.ValueAsDouble;

            return valueDouble;
        }

        public static int? ParseInt(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            int valueInt = xNav_Item.ValueAsInt;

            return valueInt;
        }

        public static DateTime? ParseDateTime(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            DateTime valueDateTime = xNav_Item.ValueAsDateTime;

            return valueDateTime;
        }

        /// <summary>
        /// Parse a bool out of the XML
        /// </summary>
        /// <param name="xPathNav"></param>
        /// <returns></returns>
        public static bool? ParseTrueFalseNullMissing(XPathNavigator xPathNav)
        {
            //If there is no node... it's null
            if (xPathNav == null) return null;

            string valueText = xPathNav.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xPathNav.ValueAsBoolean;
        }

        public static bool? SelectAndParse_TrueFalseNullMissing(
            XPathNavigator xPathNav,
            string query)
        {
            XPathNavigator xNavItem = xPathNav.SelectSingleNode(query);
            return ParseTrueFalseNullMissing(xNavItem);
        }

        /// <summary>
        /// Parses a TRUE/FALSE/NULL value
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="xPath"></param>
        /// <returns></returns>
        public static bool? ParseTrueFalseNull(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xNav_Item.ValueAsBoolean;
        }

        public static string ParseString(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return valueText;
        }
    }
}
