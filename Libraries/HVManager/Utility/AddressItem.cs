﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health.ItemTypes;

namespace HVManager
{
    [Serializable]
    public class AddressItem : IPossibleUpdate
    {
        public PossibleUpdate<string> City { get; set; }
        public PossibleUpdate<string> State { get; set; }
        public PossibleUpdate<string> Zip { get; set; }
        public TrackableList<PossibleUpdate<string>> StreetList { get; private set; }
        public PossibleUpdate<string> Country { get; set; }
        public object NonPersistedData { get; set; }

        public AddressItem()
        {
            City = new PossibleUpdate<string>();
            State = new PossibleUpdate<string>();
            Zip = new PossibleUpdate<string>();
            StreetList = new TrackableList<PossibleUpdate<string>>();
            Country = new PossibleUpdate<string>();
            ResetDirtyFlag();
        }

       

        public AddressItem(Address addr)
        {
            City = new PossibleUpdate<string>();
            State = new PossibleUpdate<string>();
            Zip = new PossibleUpdate<string>();
            StreetList = new TrackableList<PossibleUpdate<string>>();
            Country = new PossibleUpdate<string>();

            City.Value = addr.City;
            State.Value = addr.State;
            Zip.Value = addr.PostalCode;
            foreach (string street in addr.Street)
            {
                StreetList.Add(new PossibleUpdate<string>(street));
            }
            Country.Value = addr.Country;

            ResetDirtyFlag();
        }

        public Address PopulateAddress(Address add)
        {
            add.Street.Clear();
            foreach (PossibleUpdate<string> d_st in StreetList)
            {
                add.Street.Add(d_st.Value);
            }

            if (String.IsNullOrEmpty(add.City) && String.IsNullOrEmpty(City.Value))
            {
                //do nothing.. there are execptions when we manually try to set null..
            }
            else
            {
                add.City = City.Value;
            }


            if (String.IsNullOrEmpty(add.State) && String.IsNullOrEmpty(State.Value))
            {
                //do nothing.. there are execptions when we manually try to set null..
            }
            else
            {
                add.State = State.Value;
            }


            if (String.IsNullOrEmpty(add.PostalCode) && String.IsNullOrEmpty(Zip.Value))
            {
                //do nothing.. there are execptions when we manually try to set null..
            }
            else
            {
                add.PostalCode = Zip.Value;
            }

            add.Country = "US";
            return add;

        }


        #region IPossibleUpdate Members

        public bool IsDirty
        {
            get { return City.IsDirty || State.IsDirty || Zip.IsDirty || StreetList.IsDirty || Country.IsDirty; }
        }


        public void ResetDirtyFlag()
        {
            City.ResetDirtyFlag();
            State.ResetDirtyFlag();
            Zip.ResetDirtyFlag();
            StreetList.ResetDirtyFlag();
            Country.ResetDirtyFlag();
        }

        #endregion
    }
}
