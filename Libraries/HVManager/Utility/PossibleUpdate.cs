﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVManager
{
    /// <summary>
    /// This interface needs to be implemented by any property that needs to
    /// be tracked for changes
    /// </summary>
    public interface IPossibleUpdate
    {
        bool IsDirty { get; }
        void ResetDirtyFlag();
    }

    [Serializable]
    public class PossibleUpdate<T> : IPossibleUpdate
    {
        bool m_bIsDirty = true;
        T m_val;

        public PossibleUpdate()
        {
            m_val = default(T);
        }

        public PossibleUpdate(T initialValue)
        {
            m_val = initialValue;
        }

        public PossibleUpdate(T initialValue, bool bDirty)
        {
            m_val = initialValue;
            m_bIsDirty = bDirty;
        }

        public bool IsDirty
        {
            get
            {
                return m_bIsDirty;
            }
        }

        public void ResetDirtyFlag()
        {
            m_bIsDirty = false;
        }

        public T Value
        {
            get
            {
                return m_val;
            }

            set
            {
                if (m_val == null)
                {
                    if (value == null)
                    {
                        return;
                    }
                }
                else
                {
                    if (m_val.Equals(value))
                    {
                        return;
                    }
                }


                m_bIsDirty = true;
                m_val = value;
            }
        }
    }
}
