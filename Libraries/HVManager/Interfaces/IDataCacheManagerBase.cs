﻿namespace HVManager
{
    using System;
    /// <summary>
    /// Base interface that all cache management objects support
    /// </summary>
    public interface IDataCacheManagerBase
    {
        /// <summary>
        /// Clears out the contents of the cache
        /// </summary>
        void FlushCache();

        /// <summary>
        /// Returns the # of items the cache manager is currently holding
        /// </summary>
        int NumberItemsInCache
        {
            get;
        }

        /// <summary>
        /// Null if the cache manager is empty. Otherwise this returns beginning window
        /// date/time of the cache
        /// </summary>
        DateTime? CacheWindowStart
        {
            get;
        }

        /// <summary>
        /// Null if the cache manager is empty. Otherwise this returns ending window
        /// date/time of the cache
        /// </summary>
        DateTime? CacheWindowEnd
        {
            get;
        }


        /// <summary>
        /// NULL if unknown; otherwise returns the # of queries the 
        /// data manager has performed to date
        /// </summary>
        int NumberServerQueriesRun
        {
            get;
            set;
        }
    }
}