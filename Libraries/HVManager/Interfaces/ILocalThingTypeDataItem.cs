﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVManager
{
    public interface ILocalThingTypeDataItem
    {
         /// <summary>
        /// Returns the # of items the cache manager is currently holding
        /// </summary>
        bool IsDirty
        {
            get;
        }

        void ResetDirtyFlag();
    }
}
