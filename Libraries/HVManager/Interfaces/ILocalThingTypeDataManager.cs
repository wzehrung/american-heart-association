﻿namespace HVManager
{
    using System;

    /// <summary>
    /// An Interface that DataManager classes can implement
    /// that lets them be handled in objects/methods that want 
    /// collections of "non typed" (not Generic-ly type completed)
    /// objects
    /// 
    /// </summary>
    public interface ILocalThingTypeDataManager
    {
        /// <summary>
        /// True if the results of the last query could not fit in the cache
        /// </summary>
        bool CacheOverflowing
        {
            get;
        }

        /// <summary>
        /// Clears the cache
        /// </summary>
        void FlushCache();

    }
}