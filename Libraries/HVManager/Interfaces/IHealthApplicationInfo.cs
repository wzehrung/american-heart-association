﻿namespace HVManager
{
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //

    using System;

    /// <summary>
    /// This interface is implemented by either WebPages or Master Pages to give
    /// other parts of the application access to common information required
    /// when working with and branding health data
    /// </summary>
    public interface IHealthApplicationInfo
    {
        /// <summary>
        /// The text we want to appear in the "source" field of data created by this
        /// applicaiton
        /// </summary>
        string HealthDataSource
        {
            get;
        }
    }
}