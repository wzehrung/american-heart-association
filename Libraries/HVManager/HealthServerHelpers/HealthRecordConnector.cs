﻿namespace HVManager
{
    using System;
    using Microsoft.Health;
    using Microsoft.Health.Web;
    /// <summary>
    /// Summary description for HealthRecordConnector
    /// </summary>
    [Serializable]
    public class HealthRecordConnector
    {
        Guid m_healthRecordAccessorId;
        ConnectionMode m_operationMode;

        public ConnectionMode OperationMode
        {
            get
            {
                return m_operationMode;
            }
        }

        public enum ConnectionMode
        {
            realConnection,
            demoAllQueryResultsNull,
        }

        public HealthRecordAccessor HealthRecordAccessor
        {
            get
            {
                //If we have a real connection, return it
                if (m_operationMode == ConnectionMode.realConnection)
                {
                    Microsoft.Health.Web.HealthServicePage objCurrentPage = Microsoft.Health.Web.HealthServicePage.CurrentPage;
                    if (objCurrentPage != null)
                    {
                        return Microsoft.Health.Web.HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords[m_healthRecordAccessorId];
                    }
                    else
                    {
                        //to be changed - arun
                        //AHAAPI.Provider.ProviderContext objProContext = AHAAPI.Provider.SessionInfo.GetProviderContext();

                        //OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(objProContext.PersonRecordItem.PersonGUID);
                        //offlineConn.Authenticate();
                        //return new Microsoft.Health.HealthRecordAccessor(offlineConn, objProContext.PersonRecordItem.RecordGUID);
                    }
                }

                throw new Exception("There is no real connection here!");
            }
        }

        public HealthRecordConnector(Guid? healthRecordAccessorId)
        {
            if (healthRecordAccessorId == null)
            {
                m_operationMode = ConnectionMode.demoAllQueryResultsNull;
            }
            else
            {
                m_healthRecordAccessorId = healthRecordAccessorId.Value;
                m_operationMode = ConnectionMode.realConnection;
            }


        }
    }
}