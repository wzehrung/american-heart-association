﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.Health;

namespace HVManager
{
    public class WCQueries
    {
        /// <summary>
        /// Deletes an item from MSHealth & Session
        /// </summary>
        /// <param name="mshealthUserInfo"></param>
        /// <param name="thingId"></param>
        /// <returns></returns>
        public static void DeleteThing(HealthRecordAccessor record, HealthRecordItemKey thingKey)
        {
            record.RemoveItem(thingKey);
        }

        public static void helper_PerformMSHealthQuery(
            HealthRecordAccessor record,
            Guid thingTypeToSearchFor,
            Nullable<DateTime> startDate, Nullable<DateTime> endDate,
            Collection<Guid> specificThingIds,
            string xPathFilterForQuery,
            int maxItemsToReturn,
            bool checkForReturnItemsOverflow,
            out HealthRecordItemCollection dataOut,
            out bool didResultsOverflow)
        {
            //==========================================================
            //Diagnostics: Up the query count tracker
            //==========================================================
            //DataCacheDiagnostics.IncrementTotalQueryCount();

            dataOut = null;

            //==========================================================
            //setup for our query
            //==========================================================
            //NOTE: We want to "add +1" to the # of items we can return to give us 
            //      an indicator if more data exists on the server matching the query
            //      and would have been returned if the 'maxItemsToReturn' were larger.
            //
            //      We will check the results below to see how many items were returned;
            //      if the count is "> maxItemsToReturn" it means that there is "at least"
            //      one extra result that could have been returned
            HealthRecordFilter filter = new HealthRecordFilter(maxItemsToReturn + 1);
            if (checkForReturnItemsOverflow)
            {
                filter = new HealthRecordFilter(maxItemsToReturn + 1);
            }
            else
            {
                filter = new HealthRecordFilter(maxItemsToReturn);
            }


            filter.TypeIds.Add(thingTypeToSearchFor);
            if (endDate.HasValue)
            {
                filter.EffectiveDateMax = endDate.Value;
            }
            if (startDate.HasValue)
            {
                filter.EffectiveDateMin = startDate.Value;
            }

            //Are there specific Thing Ids we want to look up?
            if (specificThingIds != null)
            {
                foreach (Guid thingGuid in specificThingIds)
                {
                    filter.ItemIds.Add(thingGuid);
                }
            }

            //If we have a filter to apply to the query, do so...
            if (!string.IsNullOrEmpty(xPathFilterForQuery))
            {
                filter.XPath = xPathFilterForQuery;
            }
            
            HealthRecordSearcher searcher = record.CreateSearcher();
            searcher.Filters.Add(filter);

            //==============================================
            //Perform the query
            //==============================================
            ReadOnlyCollection<HealthRecordItemCollection> searchResultsGroup = searcher.GetMatchingItems();

            //No results
            if (searchResultsGroup.Count == 0)
            {
                didResultsOverflow = false; // No data at all = no more data
            }

            //Not expecting multiple sets of results
            System.Diagnostics.Debug.Assert(searchResultsGroup.Count < 2);

            dataOut = searchResultsGroup[0];

            //====================================================================================
            //Since we queried for up to 'maxItemsToReturn + 1' results, we can now compare
            //the count returned.  If the count returned is '> maxItemsToReturn' we know 
            //that there is at least 1 additional item that could have been returned 
            //if maxItemsToReturn was bigger.  This is typically used to see of the 
            //requested results would overflow the cache.
            //====================================================================================
            if (dataOut.Count <= maxItemsToReturn)
            {
                didResultsOverflow = false;
            }
            else
            {
                didResultsOverflow = true;
            }

        }

        public static void helper_PerformMSHealthQuery(
            HealthRecordAccessor record,
            Guid thingTypeToSearchFor,
            Nullable<DateTime> startDate, Nullable<DateTime> endDate,
            Collection<Guid> specificThingIds,
            string xPathFilterForQuery,
            int maxItemsToReturn,
            bool checkForReturnItemsOverflow,
            out HealthRecordItemCollection dataOut,
            out bool didResultsOverflow, bool bGetAuditInfo)
        {
            //==========================================================
            //Diagnostics: Up the query count tracker
            //==========================================================
            //DataCacheDiagnostics.IncrementTotalQueryCount();

            dataOut = null;

            //==========================================================
            //setup for our query
            //==========================================================
            //NOTE: We want to "add +1" to the # of items we can return to give us 
            //      an indicator if more data exists on the server matching the query
            //      and would have been returned if the 'maxItemsToReturn' were larger.
            //
            //      We will check the results below to see how many items were returned;
            //      if the count is "> maxItemsToReturn" it means that there is "at least"
            //      one extra result that could have been returned
            HealthRecordFilter filter = new HealthRecordFilter(maxItemsToReturn + 1);
            if (checkForReturnItemsOverflow)
            {
                filter = new HealthRecordFilter(maxItemsToReturn + 1);
            }
            else
            {
                filter = new HealthRecordFilter(maxItemsToReturn);
            }
            filter.View.Sections = HealthRecordItemSections.Core | HealthRecordItemSections.Xml | HealthRecordItemSections.Audits;

            filter.TypeIds.Add(thingTypeToSearchFor);
            if (endDate.HasValue)
            {
                filter.EffectiveDateMax = endDate.Value;
            }
            if (startDate.HasValue)
            {
                filter.EffectiveDateMin = startDate.Value;
            }

            //Are there specific Thing Ids we want to look up?
            if (specificThingIds != null)
            {
                foreach (Guid thingGuid in specificThingIds)
                {
                    filter.ItemIds.Add(thingGuid);
                }
            }

            //If we have a filter to apply to the query, do so...
            if (!string.IsNullOrEmpty(xPathFilterForQuery))
            {
                filter.XPath = xPathFilterForQuery;
            }

            HealthRecordSearcher searcher = record.CreateSearcher();
            searcher.Filters.Add(filter);

            //==============================================
            //Perform the query
            //==============================================
            ReadOnlyCollection<HealthRecordItemCollection> searchResultsGroup = searcher.GetMatchingItems();

            //No results
            if (searchResultsGroup.Count == 0)
            {
                didResultsOverflow = false; // No data at all = no more data
            }

            //Not expecting multiple sets of results
            System.Diagnostics.Debug.Assert(searchResultsGroup.Count < 2);

            dataOut = searchResultsGroup[0];

            //====================================================================================
            //Since we queried for up to 'maxItemsToReturn + 1' results, we can now compare
            //the count returned.  If the count returned is '> maxItemsToReturn' we know 
            //that there is at least 1 additional item that could have been returned 
            //if maxItemsToReturn was bigger.  This is typically used to see of the 
            //requested results would overflow the cache.
            //====================================================================================
            if (dataOut.Count <= maxItemsToReturn)
            {
                didResultsOverflow = false;
            }
            else
            {
                didResultsOverflow = true;
            }

        }

        /// <summary>
        /// Returns either NULL if no data of this type exists, or the single, latest
        /// instance of the data type this class is managing access to
        /// </summary>
        /// <param name="record"></param>
        /// <param name="thingType"></param>
        /// <returns></returns>
        public static HealthRecordItem helper_PerformMSHealthQueryForMostCurrentSingleThing(
            HealthRecordAccessor record,
            Guid thingType,
            string xPathFilterForQuery)
        {
            //==========================================================
            //Diagnostics: Up the query count tracker
            //==========================================================
            //DataCacheDiagnostics.IncrementTotalQueryCount();


            //We want 0 or 1 items returned
            const int maxItemsReturn = 1;

            //==========================================================
            //setup for our query
            //==========================================================
            HealthRecordFilter filter = new HealthRecordFilter(maxItemsReturn); //Max items        
            filter.TypeIds.Add(thingType);

            if (!string.IsNullOrEmpty(xPathFilterForQuery))
            {
                filter.XPath = xPathFilterForQuery;
            }

            //Look at all possible items.  (will automatically return the single most current)
            filter.EffectiveDateMax = DateTime.MaxValue;
            filter.EffectiveDateMin = DateTime.MinValue;

            HealthRecordSearcher searcher = record.CreateSearcher();
            searcher.Filters.Add(filter);

            //==============================================
            //Perform the query
            //==============================================
            ReadOnlyCollection<HealthRecordItemCollection> searchResultsGroup = searcher.GetMatchingItems();

            //No results
            if (searchResultsGroup.Count == 0) goto no_data;

            //Not expecting multiple sets of results
            System.Diagnostics.Debug.Assert(searchResultsGroup.Count <= 1);

            HealthRecordItemCollection itemsReturnedFromQuery = searchResultsGroup[0];
            //We should get either 0 or 1 items back from the query
            System.Diagnostics.Debug.Assert(itemsReturnedFromQuery.Count <= 1);

            //If 0 items, return null
            if (itemsReturnedFromQuery.Count == 0) goto no_data;

            //Return the single item
            return itemsReturnedFromQuery[0];

        no_data:
            return null;
        }

            //
            // PJB: this should be deprecated. Use the one with exceptions
            //
        public static ReadOnlyCollection<HealthRecordItemCollection> DownloadData(List<HVDataTypeFilter> typeIdWithXpathList, HealthRecordAccessor accessor)
        {
            HealthRecordSearcher searcher = accessor.CreateSearcher();

            foreach (HVDataTypeFilter x in typeIdWithXpathList)
            {
                HealthRecordFilter filter = new HealthRecordFilter();
                filter.TypeIds.Add(x.TypeId);
                if (!String.IsNullOrEmpty(x.XPathFilter))
                {
                    filter.XPath = x.XPathFilter;
                }
                if (x.MaxItemsToReturn.HasValue)
                {
                    filter.MaxItemsReturned = x.MaxItemsToReturn.Value;
                }
                filter.EffectiveDateMin = x.EffectiveDateMin;
                filter.EffectiveDateMax = x.EffectiveDateMax;
                if (x.FetchAuditInfo)
                {
                    filter.View.Sections = HealthRecordItemSections.Core | HealthRecordItemSections.Xml | HealthRecordItemSections.Audits;
                }
                searcher.Filters.Add(filter);
            }
            ReadOnlyCollection<HealthRecordItemCollection> allResults = null;
            DateTime start = DateTime.Now;
            try
            {
                allResults = searcher.GetMatchingItems();
            }
            catch (Microsoft.Health.HealthServiceException hse)
            {
                if (hse.ErrorCode == HealthServiceStatusCode.InvalidRecordState ||
                    hse.ErrorCode == HealthServiceStatusCode.AccessDenied)
                {
                    // PJB: 8.15.2014 - took out AHAAPI stuff for Project Reference reasons and because of spaghetti references.
                    // just do a throw
                    throw;

                    //if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
                    //{
                    //    AHAHelpContent.ProviderDetails.DisconnectPatientOrProvider(AHAAPI.Provider.SessionInfo.GetProviderContext().ProviderID, accessor.Id);
                    //    System.Web.HttpContext.Current.Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
                    //}
                    //else
                    //{
                    //    throw;
                    //}
                }
                else
                {
                    throw;
                }
            }
            DateTime end = DateTime.Now;

            return allResults;
        }

            //
            // PJB: this will throw an HVExceptions if there are errors. See HVWrapper/HVExceptions.cs
            //
        public static ReadOnlyCollection<HealthRecordItemCollection> RefactoredDownloadData(List<HVDataTypeFilter> typeIdWithXpathList, HealthRecordAccessor accessor)
        {
            HealthRecordSearcher searcher = accessor.CreateSearcher();

            foreach (HVDataTypeFilter x in typeIdWithXpathList)
            {
                HealthRecordFilter filter = new HealthRecordFilter();
                filter.TypeIds.Add(x.TypeId);
                if (!String.IsNullOrEmpty(x.XPathFilter))
                {
                    filter.XPath = x.XPathFilter;
                }
                if (x.MaxItemsToReturn.HasValue)
                {
                    filter.MaxItemsReturned = x.MaxItemsToReturn.Value;
                }
                filter.EffectiveDateMin = x.EffectiveDateMin;
                filter.EffectiveDateMax = x.EffectiveDateMax;
                if (x.FetchAuditInfo)
                {
                    filter.View.Sections = HealthRecordItemSections.Core | HealthRecordItemSections.Xml | HealthRecordItemSections.Audits;
                }
                searcher.Filters.Add(filter);
            }
            ReadOnlyCollection<HealthRecordItemCollection> allResults = null;
            DateTime start = DateTime.Now;
            try
            {
                allResults = searcher.GetMatchingItems();
            }
            catch (Microsoft.Health.HealthServiceException hse)
            {
                if (hse.ErrorCode == HealthServiceStatusCode.InvalidRecordState ||
                    hse.ErrorCode == HealthServiceStatusCode.AccessDenied)
                {
                    throw new HVWrapper.HVFatalException("HealthServiceStatusCode of InvalidRecordState or AccessDenied");
                }
                else
                {
                    throw new HVWrapper.HVNonfatalException(hse.Message);
                }
            }
            DateTime end = DateTime.Now;

            return allResults;
        }

        /// <summary>
        /// Gets data back for a single thing.  This does not return any XML transforms that could be performed on the data
        /// </summary>
        /// <param name="mshealthUserInfo"></param>
        /// <param name="thingId"></param>
        /// <returns></returns>
        public static HealthRecordItem helper_GetSingleThing(HealthRecordAccessor record, Guid thingId)
        {
            return record.GetItem(thingId, HealthRecordItemSections.Core | HealthRecordItemSections.Xml);
        }

        public static HealthRecordItem helper_PerformMSHealthQueryForMostCurrentSingleThing(
            HealthRecordAccessor record,
            Guid thingType,
            string xPathFilterForQuery,HealthRecordItemSections sections)
        {
            //==========================================================
            //Diagnostics: Up the query count tracker
            //==========================================================
            //DataCacheDiagnostics.IncrementTotalQueryCount();


            //We want 0 or 1 items returned
            const int maxItemsReturn = 1;

            //==========================================================
            //setup for our query
            //==========================================================
            HealthRecordFilter filter = new HealthRecordFilter(maxItemsReturn); //Max items  
            filter.View.Sections = sections;
            filter.TypeIds.Add(thingType);

            if (!string.IsNullOrEmpty(xPathFilterForQuery))
            {
                filter.XPath = xPathFilterForQuery;
            }

            //Look at all possible items.  (will automatically return the single most current)
            filter.EffectiveDateMax = DateTime.MaxValue;
            filter.EffectiveDateMin = DateTime.MinValue;

            HealthRecordSearcher searcher = record.CreateSearcher();
            searcher.Filters.Add(filter);

            //==============================================
            //Perform the query
            //==============================================
            ReadOnlyCollection<HealthRecordItemCollection> searchResultsGroup = searcher.GetMatchingItems();

            //No results
            if (searchResultsGroup.Count == 0) goto no_data;

            //Not expecting multiple sets of results
            System.Diagnostics.Debug.Assert(searchResultsGroup.Count <= 1);

            HealthRecordItemCollection itemsReturnedFromQuery = searchResultsGroup[0];
            //We should get either 0 or 1 items back from the query
            System.Diagnostics.Debug.Assert(itemsReturnedFromQuery.Count <= 1);

            //If 0 items, return null
            if (itemsReturnedFromQuery.Count == 0) goto no_data;

            //Return the single item
            return itemsReturnedFromQuery[0];

        no_data:
            return null;
        }
    }
}
