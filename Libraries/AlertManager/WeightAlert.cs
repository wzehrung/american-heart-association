﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.ComponentModel;
using System.Web.UI.WebControls;
using AHAHelpContent;
using AHACommon;
using GRCBase;

namespace AlertManager
{
    public class WeightAlertRule
    {
        public double Value
        {
            get;
            set;
        }

        public bool IsGain
        {
            get;
            set;
        }

        public int PeriodCount
        {
            get;
            set;
        }

        public PeriodType PeriodType
        {
            get;
            set;
        }

        public static string GetFormattedAlertRule(string strXml)
        {
            WeightAlertRule objAlert = ParseAlertRuleXml(strXml);

            StringBuilder sb = new StringBuilder();

            if (objAlert.IsGain)
            {
                //sb.Append("Gain of ");
                sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_GainOf").ToString());
            }
            else
            {
                ////sb.Append("Loss of ");
                sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LossOf").ToString());
            }

            sb.Append(objAlert.Value.ToString());

            if (objAlert.Value > 1)
            {
                //sb.Append(" pounds");
                sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Pounds").ToString());
            }
            else
            {
                //sb.Append(" pound");
                sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Pound").ToString());
            }


            ////sb.Append(" over the last ");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_OverTheLast").ToString());
            if (objAlert.PeriodType == PeriodType.D)
            {
                sb.Append(objAlert.PeriodCount.ToString());
                if (objAlert.PeriodCount == 1)
                {
                    //sb.Append(" day");
                    sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Day").ToString());
                }
                else
                {
                    //sb.Append(" days");
                    sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Days").ToString());
                }
            }
            else if (objAlert.PeriodType == PeriodType.W)
            {
                sb.Append(objAlert.PeriodCount.ToString());
                if (objAlert.PeriodCount == 1)
                {
                    //sb.Append(" week");
                    sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Week").ToString());
                }
                else
                {
                    sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Weeks").ToString());
                }
            }
            else
            {
                sb.Append(objAlert.PeriodCount.ToString());
                if (objAlert.PeriodCount == 1)
                {
                    //sb.Append(" month");
                    sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Month").ToString());
                }
                else
                {
                    //sb.Append(" months");
                    sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Months").ToString());
                }
            }

            if (sb.Length > 0)
            {
                return string.Format("Weight {0}", sb.ToString());
            }

            return string.Empty;
        }

        public static WeightAlertRule ParseAlertRuleXml(string strXml)
        {
            XDocument xDoc = XDocument.Parse(strXml);

            WeightAlertRule obj = new WeightAlertRule();

            obj.PeriodCount = Convert.ToInt32(xDoc.XPathSelectElement("Alert/Value").Attribute("PeriodCount").Value);
            obj.IsGain = Boolean.Parse(xDoc.XPathSelectElement("Alert/Value").Attribute("IsGain").Value);
            obj.Value = Convert.ToDouble(xDoc.XPathSelectElement("Alert/Value").Value);
            obj.PeriodType = (PeriodType)Enum.Parse(typeof(PeriodType), xDoc.XPathSelectElement("Alert/Value").Attribute("PeriodType").Value, false);

            return obj;
        }

        public static string CreateAlertRuleXml(bool bIsGain, double dValue, int iPeriodCount, PeriodType ePeriodType)
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.Weight.ToString()),
                                    new XElement("Value"
                                        , new XAttribute("IsGain", bIsGain ? true.ToString() : false.ToString())
                                        , new XAttribute("PeriodCount", iPeriodCount)
                                        , new XAttribute("PeriodType", ePeriodType.ToString())
                                    , dValue.ToString())));

            return xDoc.ToString();
        }
    }

    public class WeightAlert
    {
        public double InitialValue
        {
            get;
            set;
        }

        public double FinalValue
        {
            get;
            set;
        }

        public static string GetFormattedAlert(string strXml)
        {
            WeightAlert objAlert = ParseAlertXml(strXml);
            string strGainLoss = System.Web.HttpContext.GetGlobalResourceObject("Common", "Alert_WeightGain").ToString();
            double dVal = dVal = objAlert.FinalValue - objAlert.InitialValue; ;
            if (objAlert.FinalValue < objAlert.InitialValue)
            {
                strGainLoss = System.Web.HttpContext.GetGlobalResourceObject("Common", "Alert_WeightLoss").ToString();
                dVal = objAlert.InitialValue - objAlert.FinalValue;
            }
            ////return string.Format("{0} = {1} pounds", strGainLoss, dVal);
            return string.Format("{0} = {1} " + System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Pounds").ToString(), strGainLoss, dVal);
        }

        public static WeightAlert ParseAlertXml(string strXml)
        {
            if (string.IsNullOrEmpty(strXml))
                return null;

            XDocument xDoc = XDocument.Parse(strXml);

            WeightAlert obj = new WeightAlert();

            obj.InitialValue = Convert.ToDouble(xDoc.XPathSelectElement("Alert/InitialValue").Value);
            obj.FinalValue = Convert.ToDouble(xDoc.XPathSelectElement("Alert/FinalValue").Value);

            return obj;
        }

        public static string CreateAlertXml(double dInitialValue, double dFinalValue)
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.BloodGlucose.ToString()),
                                    new XElement("InitialValue", dInitialValue.ToString()),
                                    new XElement("FinalValue", dFinalValue.ToString())));

            return xDoc.ToString();
        }
    }

    public class WeightAlertHelper
    {
        private static DateTime _GetFromDate(int iPeriodCount, PeriodType ePeriodType)
        {
            DateTime dtFrom;

            if (ePeriodType == PeriodType.M)
            {
                dtFrom = DateTime.Today.AddMonths(-1 * iPeriodCount);
            }
            else if (ePeriodType == PeriodType.W)
            {
                dtFrom = DateTime.Today.AddDays(-1 * (iPeriodCount * 7));
            }
            else if (ePeriodType == PeriodType.D)
            {
                dtFrom = DateTime.Today.AddDays(-1 * iPeriodCount);
            }
            else
            {
                throw new InvalidProgramException(string.Format("Invalid Period type:- {0}", ePeriodType.ToString()));
            }

            return dtFrom;
        }

        public static void ProcessReadingForAlert(LogHelper logger, AHAHelpContent.Patient objPatient, Microsoft.Health.ItemTypes.Personal personalItem, List<AlertRule> listRule, IEnumerable<Microsoft.Health.ItemTypes.Weight> listWeight)
        {
            if (listRule.Count == 0 || listWeight.Count() == 0)
            {
                Helper.LOG(logger, string.Format("****No Weight Alert For for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                return;
            }

            if (!objPatient.OfflinePersonGUID.HasValue || !objPatient.OfflineHealthRecordGUID.HasValue)
            {
                Helper.LOG(logger, string.Format("****No offline person guid or offline record guid found for patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                return;
            }

            foreach (AlertRule objRule in listRule)
            {
                WeightAlertRule alertData = WeightAlertRule.ParseAlertRuleXml(objRule.AlertRuleData);
                if (alertData != null)
                {
                    listWeight = listWeight.Where(b => b.EffectiveDate > _GetFromDate(alertData.PeriodCount, alertData.PeriodType));

                    if (listWeight.Count() == 0)
                        continue;

                    listWeight = listWeight.OrderBy(i => i.EffectiveDate);

                    double kilograms_to_pounds = 2.205;

                    double dValueInitial = listWeight.First().Value.Value * kilograms_to_pounds;
                    double dValueFinal = listWeight.Last().Value.Value * kilograms_to_pounds;

                    Helper.LOG(logger, string.Format("****Initial/Final weight: {0} and alert data value: {1} for patient {2} at - {3}****", string.Format("{0}/{1}", dValueInitial, dValueFinal), alertData.Value, objPatient.UserHealthRecordID, DateTime.Now.ToString()));

                    bool bCreateAlert = (alertData.IsGain && (dValueFinal > dValueInitial) && ((dValueFinal - dValueInitial) >= alertData.Value)) ||
                        (!alertData.IsGain && (dValueFinal < dValueInitial) && ((dValueInitial - dValueFinal) >= alertData.Value));

                    if (bCreateAlert)
                    {
                        Helper.LOG(logger, string.Format("****Creating Weight Alert For for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                        Guid guid = Guid.Empty;
                        DateTime dtEffective = DateTime.Now;
                        if (alertData.IsGain)
                        {
                            guid = listWeight.Last().Key.Id;
                            dtEffective = listWeight.Last().EffectiveDate;
                        }
                        else
                        {
                            guid = listWeight.First().Key.Id;
                            dtEffective = listWeight.First().EffectiveDate;
                        }

                        AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(guid, objRule.AlertRuleID);
                        if (objExistingAlert == null)
                        {
                            Helper.LOG(logger, string.Format("****Creating Weight Alert for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                            AHAHelpContent.Alert.CreateAlert(objRule.AlertRuleID, objPatient.UserHealthRecordID, guid, WeightAlert.CreateAlertXml(dValueInitial, dValueFinal), dtEffective);
                            Helper.LOG(logger, string.Format("****End Creating Weight Alert for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));

                            AHAHelpContent.ProviderDetails objProvider = AHAHelpContent.ProviderDetails.FindByProviderID(objRule.ProviderID);

                            string strPatientName = Helper.GetPatientName(personalItem);
                            if (objProvider.NotifyNewAlertImmediately.HasValue && objProvider.NotifyNewAlertImmediately.Value && !string.IsNullOrEmpty(strPatientName))
                            {
                                Helper.LOG(logger, string.Format("****Start Sending Alert Notification to Provider {0} for patient {1} at - {2}****", objProvider.FirstName, objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                //send alert email
                                string strName = string.Format("{0} {1}", objProvider.FirstName, objProvider.LastName);
                                EmailHelper.SendAlertEmailToProvider(true, strName, objProvider.EmailForIndividualAlert, strPatientName, objProvider.LanguageLocale);
                                Helper.LOG(logger, string.Format("****End Sending Alert Notification to Provider {0} for patient {1} at - {2}****", objProvider.FirstName, objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                            }

                            if (objRule.SendMessageToPatient.HasValue && objRule.SendMessageToPatient.Value)
                            {
                                //create a db context, create db message, create hv message
                                DBContext dbContext = null;
                                bool bException = false;
                                try
                                {
                                    dbContext = DBContext.GetDBContext();
                                    Helper.LOG(logger, string.Format("****Start Creating Heart360 Message in Heart360 Mailbox for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                    //Create message entry in db
                                    int iMessageID = AHAHelpContent.Message.CreateMessage(objProvider.ProviderID, objPatient.UserHealthRecordID, AHACommon.AHADefs.MessageType.MESSAGE_TYPE_NEW, true);

                                    //Create message in HV
                                    string strMessageBody = objRule.MessageToPatient;

                                    PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;

                                    string strSubject = emailSetting.Subject_Alert_Message_Patient;

                                    HVHelper.CreateSystemToPatientMessageOnAlert(objPatient.OfflinePersonGUID.Value,
                                        objPatient.OfflineHealthRecordGUID.Value,
                                        iMessageID,
                                        true,
                                        DateTime.Now,
                                        strSubject,
                                        strMessageBody,
                                        objProvider.ProviderID);

                                    Helper.LOG(logger, string.Format("****End Creating Heart360 Message in Heart360 Mailbox for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                }
                                catch
                                {
                                    bException = true;
                                    throw;
                                }
                                finally
                                {
                                    try
                                    {
                                        if (bException)
                                        {
                                            if (dbContext != null)
                                            {
                                                dbContext.ReleaseDBContext(false);
                                            }
                                        }
                                        else
                                        {
                                            dbContext.ReleaseDBContext(true);
                                        }
                                    }
                                    catch
                                    {
                                        if (dbContext != null)
                                        {
                                            dbContext.ReleaseDBContext(false);
                                        }
                                        throw;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Helper.LOG(logger, string.Format("****This is an Existing Weight Alert For for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                        }
                        //get provider and check for notification
                    }
                }
            }
        }
    }
}
