﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.ComponentModel;
using System.Web.UI.WebControls;
using AHAHelpContent;
using GRCBase;
using AHACommon;
using System.Web.UI;
using HVManager;

namespace AlertManager
{
    public class BloodPressureAlertRule
    {
        public int? Systolic
        {
            get;
            set;
        }

        public bool? IsSystolicAbove
        {
            get;
            set;
        }

        public int? Diastolic
        {
            get;
            set;
        }

        public bool? IsDiastolicAbove
        {
            get;
            set;
        }

        public int? HeartRate
        {
            get;
            set;
        }

        public bool? IsHeartRateAbove
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }

        public static string GetFormattedAlertRule(string strXml)
        {
            BloodPressureAlertRule objAlert = ParseAlertRuleXml(strXml);

            StringBuilder sb = new StringBuilder();

            bool bHasSystolic = false;
            bool bHasDiastolic = false;

            if (objAlert.Systolic.HasValue && objAlert.IsSystolicAbove.HasValue)
            {
                // PJB 4.21.2014 - use local resource file
                //sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common","Text_Systolic").ToString());
                sb.Append(AlertManager.App_GlobalResources.Alerts.Text_Systolic);

                if (objAlert.IsSystolicAbove.Value)
                {
                    sb.AppendFormat(" > {0} ", objAlert.Systolic);
                }
                else
                {
                    sb.AppendFormat(" < {0} ", objAlert.Systolic);
                }

                bHasSystolic = true;
            }

            if (objAlert.Diastolic.HasValue && objAlert.IsDiastolicAbove.HasValue)
            {
                if (bHasSystolic)
                {
                    //sb.Append(", Diastolic");
                    // PJB 4.21.2014 - use local resource file
                    //sb.Append(", " + System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString());
                    sb.Append(", " + AlertManager.App_GlobalResources.Alerts.Text_Diastolic);
                }
                else
                {
                    // PJB 4.21.2014 - use local resource file
                    //sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString());
                    sb.Append(AlertManager.App_GlobalResources.Alerts.Text_Diastolic);
                }
                if (objAlert.IsDiastolicAbove.Value)
                {
                    sb.AppendFormat(" > {0} ", objAlert.Diastolic);
                }
                else
                {
                    sb.AppendFormat(" < {0} ", objAlert.Diastolic);
                }

                bHasDiastolic = true;
            }

            if (objAlert.HeartRate.HasValue && objAlert.IsHeartRateAbove.HasValue)
            {
                if (bHasDiastolic || bHasSystolic)
                {
                    sb.Append(", " );
                }
                // PJB 4.21.2014 - use local resource file
                //sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Heart_Rate").ToString());
                sb.Append(AlertManager.App_GlobalResources.Alerts.Text_Heart_Rate);
                

                if (objAlert.IsHeartRateAbove.Value)
                {
                    sb.AppendFormat(" > {0} ", objAlert.HeartRate);
                }
                else
                {
                    sb.AppendFormat(" < {0} ", objAlert.HeartRate);
                }
            }

            if (sb.Length > 0)
            {
                // PJB 4.21.2014 - use local resource file
               // return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Common","Text_BloodPressure").ToString()+" {0}", sb.ToString());
                return string.Format(AlertManager.App_GlobalResources.Alerts.Text_BloodPressure + " {0}", sb.ToString());
            }

            return string.Empty;
        }

        public static BloodPressureAlertRule ParseAlertRuleXml(string strXml)
        {
            XDocument xDoc = XDocument.Parse(strXml);

            BloodPressureAlertRule obj = new BloodPressureAlertRule();

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("Alert/Systolic").Attribute("IsAbove").Value))
            {
                obj.IsSystolicAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/Systolic").Attribute("IsAbove").Value);
            }

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("Alert/Diastolic").Attribute("IsAbove").Value))
            {
                obj.IsDiastolicAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/Diastolic").Attribute("IsAbove").Value);
            }

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("Alert/HeartRate").Attribute("IsAbove").Value))
            {
                obj.IsHeartRateAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/HeartRate").Attribute("IsAbove").Value);
            }

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("Alert/Systolic").Value))
            {
                obj.Systolic = Convert.ToInt32(xDoc.XPathSelectElement("Alert/Systolic").Value);
            }

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("Alert/Diastolic").Value))
            {
                obj.Diastolic = Convert.ToInt32(xDoc.XPathSelectElement("Alert/Diastolic").Value);
            }

            if (!string.IsNullOrEmpty(xDoc.XPathSelectElement("Alert/HeartRate").Value))
            {
                obj.HeartRate = Convert.ToInt32(xDoc.XPathSelectElement("Alert/HeartRate").Value);
            }

            return obj;
        }

        public static string CreateAlertRuleXml(bool? bIsSystolicAbove, int? iSystolic, bool? bIsDiastolicAbove, int? iDiastolic, bool? bIsHeartRateAbove, int? iHeartRate)
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.BloodPressure.ToString()),
                                    new XElement("Systolic"
                                        , new XAttribute("IsAbove", bIsSystolicAbove.HasValue ? bIsSystolicAbove.Value.ToString() : string.Empty)
                                        , new XAttribute("Unit", "mmHg")
                                    , iSystolic.HasValue ? iSystolic.Value.ToString() : string.Empty)

                                    , new XElement("Diastolic"
                                        , new XAttribute("IsAbove", bIsDiastolicAbove.HasValue ? bIsDiastolicAbove.Value.ToString() : string.Empty)
                                        , new XAttribute("Unit", "mmHg")
                                    , iDiastolic.HasValue ? iDiastolic.Value.ToString() : string.Empty)

                                    , new XElement("HeartRate"
                                        , new XAttribute("IsAbove", bIsHeartRateAbove.HasValue ? bIsHeartRateAbove.Value.ToString() : string.Empty)
                                        , new XAttribute("Unit", "mmHg")
                                    , iHeartRate.HasValue ? iHeartRate.Value.ToString() : string.Empty)));

            return xDoc.ToString();
        }
    }

    public class BloodPressureAlert
    {
        public int Systolic
        {
            get;
            set;
        }

        public int Diastolic
        {
            get;
            set;
        }

        public int? HeartRate
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }

        public static string GetFormattedAlert(string strXml)
        {
            BloodPressureAlert objAlert = ParseAlertXml(strXml);

            StringBuilder sb = new StringBuilder();
            // PJB 4.21.2014 - use local resource file
            //sb.AppendFormat(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodPressure").ToString()+" ");
            //sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Systolic").ToString());
            sb.AppendFormat(AlertManager.App_GlobalResources.Alerts.Text_BloodPressure + " ");
            sb.Append(AlertManager.App_GlobalResources.Alerts.Text_Systolic);

            sb.AppendFormat(" = {0} ", objAlert.Systolic);

            //sb.Append(", " + System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString());
            sb.Append(", " + AlertManager.App_GlobalResources.Alerts.Text_Diastolic);
            sb.AppendFormat(" = {0} ", objAlert.Diastolic);

            if (objAlert.HeartRate.HasValue)
            {
                //sb.Append(", " + System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Heart_Rate").ToString());
                sb.Append(", " + AlertManager.App_GlobalResources.Alerts.Text_Heart_Rate);
                sb.AppendFormat(" = {0} ", objAlert.HeartRate);
            }

            return sb.ToString();
        }

        public static BloodPressureAlert ParseAlertXml(string strXml)
        {
            XDocument xDoc = XDocument.Parse(strXml);

            BloodPressureAlert obj = new BloodPressureAlert();

            obj.Systolic = Convert.ToInt32(xDoc.XPathSelectElement("Alert/Systolic").Value);
            obj.Diastolic = Convert.ToInt32(xDoc.XPathSelectElement("Alert/Diastolic").Value);

            if (xDoc.XPathSelectElement("Alert/HeartRate") != null && !string.IsNullOrEmpty(xDoc.XPathSelectElement("Alert/HeartRate").Value))
            {
                obj.HeartRate = Convert.ToInt32(xDoc.XPathSelectElement("Alert/HeartRate").Value);
            }

            return obj;
        }

        public static string CreateAlertXml(int iSystolic, int iDiastolic, int? iHeartRate)
        {
            XDocument xDoc = new XDocument();

            if (iHeartRate.HasValue)
            {
                xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.BloodPressure.ToString()),
                                        new XElement("Systolic"
                                            , new XAttribute("Unit", "mmHg")
                                        , iSystolic.ToString())

                                        , new XElement("Diastolic"
                                            , new XAttribute("Unit", "mmHg")
                                        , iDiastolic.ToString())

                                        , new XElement("HeartRate"
                                            , new XAttribute("Unit", "mmHg")
                                        , iHeartRate.Value.ToString())));
            }
            else
            {
                xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.BloodPressure.ToString()),
                                        new XElement("Systolic"
                                            , new XAttribute("Unit", "mmHg")
                                        , iSystolic.ToString())

                                        , new XElement("Diastolic"
                                            , new XAttribute("Unit", "mmHg")
                                        , iDiastolic.ToString())
                                        ));
            }

            return xDoc.ToString();
        }
    }

    public class BloodPressureAlertHelper
    {
        public int Systolic
        {
            get;
            set;
        }

        public bool IsSystolicAbove
        {
            get;
            set;
        }

        public int Diastolic
        {
            get;
            set;
        }

        public bool IsDiastolicAbove
        {
            get;
            set;
        }

        public int HeartRate
        {
            get;
            set;
        }

        public bool IsHeartRateAbove
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }

        public static void ProcessHeart360ReadingForAlert(Guid gPersonGUID, Guid gRecordGUID, int iPatientID, string strPatientName, DateTime dtEffectiveDate, int iSystolic, int iDiastolic, int? iHeartRate, Guid gHealthRecordItemGUID)
        {
            if (dtEffectiveDate.CompareTo(DateTime.Now.AddDays(-1)) != 1)
                return;

            List<AlertRule> listRule = AlertRule.FindByPatientID(iPatientID, AHAHelpContent.TrackerDataType.BloodPressure);

            foreach (AlertRule objRule in listRule)
            {
                BloodPressureAlertRule alertData = BloodPressureAlertRule.ParseAlertRuleXml(objRule.AlertRuleData);
                if (alertData != null)
                {
                    bool bCreateAlert = ((alertData.IsSystolicAbove.HasValue && alertData.Systolic.HasValue && alertData.IsSystolicAbove.Value && iSystolic > alertData.Systolic.Value)
                                      || (alertData.IsSystolicAbove.HasValue && alertData.Systolic.HasValue && !alertData.IsSystolicAbove.Value && iSystolic < alertData.Systolic.Value)
                        || (alertData.IsDiastolicAbove.HasValue && alertData.Diastolic.HasValue && !alertData.IsDiastolicAbove.Value && iDiastolic < alertData.Diastolic.Value)
                        || (alertData.IsDiastolicAbove.HasValue && alertData.Diastolic.HasValue && alertData.IsDiastolicAbove.Value && iDiastolic > alertData.Diastolic.Value)                     
                        || (alertData.IsHeartRateAbove.HasValue && alertData.HeartRate.HasValue && iHeartRate.HasValue && alertData.IsHeartRateAbove.Value && iHeartRate > alertData.HeartRate.Value)
                        || (alertData.IsHeartRateAbove.HasValue && alertData.HeartRate.HasValue && iHeartRate.HasValue && !alertData.IsHeartRateAbove.Value && iHeartRate < alertData.HeartRate.Value));

                    AHAHelpContent.ProviderDetails objProvider = AHAHelpContent.ProviderDetails.FindByProviderID(objRule.ProviderID);

                    if (bCreateAlert)
                    {
                        AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(gHealthRecordItemGUID, objRule.AlertRuleID);

                        if (objExistingAlert == null)
                        {
                            AHAHelpContent.Alert.CreateAlert(objRule.AlertRuleID, iPatientID, gHealthRecordItemGUID, BloodPressureAlert.CreateAlertXml(iSystolic, iDiastolic, iHeartRate), DateTime.Now);
                            //send alert notification to provider if provider has subscribed to alerts as they happen

                            if (objProvider.NotifyNewAlertImmediately.HasValue && objProvider.NotifyNewAlertImmediately.Value)
                            {
                                //send alert email
                                string strName = string.Format("{0} {1}", objProvider.FirstName, objProvider.LastName);
                                EmailHelper.SendAlertEmailToProvider(false, strName, objProvider.EmailForIndividualAlert, strPatientName, objProvider.LanguageLocale);
                            }

                            if (objRule.SendMessageToPatient.HasValue && objRule.SendMessageToPatient.Value)
                            {
                                //create a db context, create db message, create hv message
                                DBContext dbContext = null;
                                bool bException = false;
                                try
                                {
                                    dbContext = DBContext.GetDBContext();
                                    //Create message entry in db
                                    int iMessageID = AHAHelpContent.Message.CreateMessage(objProvider.ProviderID, iPatientID, AHACommon.AHADefs.MessageType.MESSAGE_TYPE_NEW, true);

                                    //Create message in HV
                                    // PJB: the message body probably wants to be the customized message from Provider
                                    // objRule.MessageToPatient
                                    //
                                    string strMessageBody = _GetMessageBody(false, objProvider.LanguageLocale);
                                    PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;

                                    string strSubject = emailSetting.Subject_Alert_Message_Patient;

                                    HVManager.MessageDataManager.MessageItem objMessage = new HVManager.MessageDataManager.MessageItem();
                                    objMessage.DateSent.Value = DateTime.Now;
                                    objMessage.IsSentByProvider.Value = true;
                                    objMessage.MessageBody.Value = strMessageBody;
                                    objMessage.MessageID.Value = iMessageID;
                                    objMessage.ProviderID.Value = objProvider.ProviderID;
                                    objMessage.Subject.Value = strSubject;

                                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);

                                    Page objPage = System.Web.HttpContext.Current.Handler as Page;
                                    objMessage.Source.Value = BrandLogoHelperMachine.GetApplicationBrandInfo(objPage);
                                    mgr.MessageDataManager.CreateItem(objMessage);
                                }
                                catch
                                {
                                    bException = true;
                                    throw;
                                }
                                finally
                                {
                                    try
                                    {
                                        if (bException)
                                        {
                                            if (dbContext != null)
                                            {
                                                dbContext.ReleaseDBContext(false);
                                            }
                                        }
                                        else
                                        {
                                            dbContext.ReleaseDBContext(true);
                                        }
                                    }
                                    catch
                                    {
                                        if (dbContext != null)
                                        {
                                            dbContext.ReleaseDBContext(false);
                                        }
                                        throw;
                                    }
                                }
                            }
                        }
                    }
                    //else
                    //{
                    //    //get the alert for the health record item guid
                    //    AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(gHealthRecordItemGUID, objRule.AlertRuleID);
                    //    //if no alert for the new value, delete the existing alert, otherwise update the existing alert or create a new one if an item does not exist

                    //    if (objExistingAlert != null)
                    //    {
                    //        AHAHelpContent.Alert.DeleteAlert(objExistingAlert.AlertID);
                    //        if (objProvider.NotifyNewAlertImmediately.HasValue && objProvider.NotifyNewAlertImmediately.Value)
                    //        {
                    //            //send alert email
                    //            string strName = string.Format("{0} {1}", objProvider.FirstName, objProvider.LastName);
                    //            //send ignore previous notification for this patient email
                    //            EmailHelper.SendAlertModifiedEmailToProvider(strName, objProvider.EmailForIndividualAlert, strPatientName);
                    //        }
                    //    }
                    //}
                }
            }
        }


        private static string _getEmailTemplateDirectory(string strLocale)
        {
            if (strLocale != Locale.English)
            {
                return string.Format(AHAAppSettings.EmailTemplateDirectory + ".{0}", strLocale);
            }
            return AHAAppSettings.EmailTemplateDirectory;
        }

        private static string _GetMessageBody(bool bProcess, string strLocale)
        {
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;

            string filepath = string.Format("{0}\\{1}", _getEmailTemplateDirectory(strLocale), emailSetting.File_Alert_Message_Patient);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            if (bProcess)
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", AHAAppSettings.SiteUrl);
            }
            else
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", GRCBase.UrlUtility.ServerRootUrl);
            }

            return strContent;
        }

        public static void ProcessReadingForAlert(LogHelper logger, AHAHelpContent.Patient objPatient, Microsoft.Health.ItemTypes.Personal personalItem, List<AlertRule> listRule, IEnumerable<Microsoft.Health.ItemTypes.BloodPressure> listBloodPressure)
        {
            if (listRule.Count == 0 || listBloodPressure.Count() == 0)
            {
                Helper.LOG(logger, string.Format("****No Blood Pressure Alert For for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                return;
            }

            if (!objPatient.OfflinePersonGUID.HasValue || !objPatient.OfflineHealthRecordGUID.HasValue)
            {
                Helper.LOG(logger, string.Format("****No offline person guid or offline record guid found for patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                return;
            }

            foreach (AlertRule objRule in listRule)
            {
                BloodPressureAlertRule alertData = BloodPressureAlertRule.ParseAlertRuleXml(objRule.AlertRuleData);
                if (alertData != null)
                {
                    foreach (Microsoft.Health.ItemTypes.BloodPressure objBloodPressure in listBloodPressure)
                    {
                        int iSystolic = objBloodPressure.Systolic;
                        int iDiastolic = objBloodPressure.Diastolic;
                        int? iHeartRate = objBloodPressure.Pulse;

                        bool bCreateAlert = ((alertData.IsSystolicAbove.HasValue && alertData.Systolic.HasValue && alertData.IsSystolicAbove.Value && iSystolic > alertData.Systolic.Value)
                        || (alertData.IsDiastolicAbove.HasValue && alertData.Diastolic.HasValue && alertData.IsDiastolicAbove.Value && iDiastolic > alertData.Diastolic.Value)
                        || (alertData.IsSystolicAbove.HasValue && alertData.Systolic.HasValue && !alertData.IsSystolicAbove.Value && iSystolic < alertData.Systolic.Value)
                        || (alertData.IsDiastolicAbove.HasValue && alertData.Diastolic.HasValue && !alertData.IsDiastolicAbove.Value && iDiastolic < alertData.Diastolic.Value)
                        || (alertData.IsHeartRateAbove.HasValue && alertData.HeartRate.HasValue && iHeartRate.HasValue && alertData.IsHeartRateAbove.Value && iHeartRate > alertData.HeartRate.Value)
                        || (alertData.IsHeartRateAbove.HasValue && alertData.HeartRate.HasValue && iHeartRate.HasValue && !alertData.IsHeartRateAbove.Value && iHeartRate < alertData.HeartRate.Value));


                        if (bCreateAlert)
                        {
                            AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(objBloodPressure.Key.Id, objRule.AlertRuleID);

                            if (objExistingAlert == null)
                            {
                                Helper.LOG(logger, string.Format("****Creating Blood Pressure Alert for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                AHAHelpContent.Alert.CreateAlert(objRule.AlertRuleID, objPatient.UserHealthRecordID, objBloodPressure.Key.Id, BloodPressureAlert.CreateAlertXml(iSystolic, iDiastolic, iHeartRate), objBloodPressure.EffectiveDate);
                                Helper.LOG(logger, string.Format("****Creating Blood Pressure Alert for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));

                                AHAHelpContent.ProviderDetails objProvider = AHAHelpContent.ProviderDetails.FindByProviderID(objRule.ProviderID);

                                string strPatientName = Helper.GetPatientName(personalItem);
                                if (objProvider.NotifyNewAlertImmediately.HasValue && objProvider.NotifyNewAlertImmediately.Value && !string.IsNullOrEmpty(strPatientName))
                                {
                                    //send alert email
                                    string strName = string.Format("{0} {1}", objProvider.FirstName, objProvider.LastName);
                                    EmailHelper.SendAlertEmailToProvider(true, strName, objProvider.EmailForIndividualAlert, strPatientName, objProvider.LanguageLocale);
                                }

                                if (objRule.SendMessageToPatient.HasValue && objRule.SendMessageToPatient.Value)
                                {
                                    //create a db context, create db message, create hv message
                                    DBContext dbContext = null;
                                    bool bException = false;
                                    try
                                    {
                                        dbContext = DBContext.GetDBContext();
                                        //Create message entry in db
                                        int iMessageID = AHAHelpContent.Message.CreateMessage(objProvider.ProviderID, objPatient.UserHealthRecordID, AHACommon.AHADefs.MessageType.MESSAGE_TYPE_NEW, true);

                                        //Create message in HV
                                        string strMessageBody = _GetMessageBody(true, objProvider.LanguageLocale);

                                        PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;

                                        string strSubject = emailSetting.Subject_Alert_Message_Patient;

                                        HVHelper.CreateSystemToPatientMessageOnAlert(objPatient.OfflinePersonGUID.Value,
                                            objPatient.OfflineHealthRecordGUID.Value,
                                            iMessageID,
                                            true,
                                            DateTime.Now,
                                            strSubject,
                                            strMessageBody,
                                            objProvider.ProviderID);
                                    }
                                    catch
                                    {
                                        bException = true;
                                        throw;
                                    }
                                    finally
                                    {
                                        try
                                        {
                                            if (bException)
                                            {
                                                if (dbContext != null)
                                                {
                                                    dbContext.ReleaseDBContext(false);
                                                }
                                            }
                                            else
                                            {
                                                dbContext.ReleaseDBContext(true);
                                            }
                                        }
                                        catch
                                        {
                                            if (dbContext != null)
                                            {
                                                dbContext.ReleaseDBContext(false);
                                            }
                                            throw;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //get the alert for the health record item guid
                            //AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(objBloodPressure.Key.Id, objRule.AlertRuleID);
                            //if no alert for the new value, delete the existing alert, otherwise update the existing alert or create a new one if an item does not exist

                            //if (objExistingAlert != null)
                            //{
                            //    Helper.LOG(logger, string.Format("****Deleting Blood Pressure Alert for Patient {0} at - {1}****", iPatientID, DateTime.Now.ToString()));
                            //    AHAHelpContent.Alert.DeleteAlert(objExistingAlert.AlertID);
                            //    Helper.LOG(logger, string.Format("****End Deleting Blood Pressure Alert for Patient {0} at - {1}****", iPatientID, DateTime.Now.ToString()));
                            //}
                        }
                    }
                }
            }
        }
    }
}
