﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.ComponentModel;
using System.Web.UI.WebControls;
using Microsoft.Health.ItemTypes;
using System.Collections.ObjectModel;
using Microsoft.Health;
using AHAHelpContent;
using AHACommon;
using System.Web;

namespace AlertManager
{
    /*  <Alert Type="Weight">
            <Value IsAbove="True" Unit="Pounds">165.1</Value>
        </Alert>
      
       <Alert Type="BloodGlucose">
            <Value IsAbove="True" Unit="mg/dL">165.1</Value>
       </Alert>
     
       <Alert Type="BloodPressure">
            <Systolic IsAbove="True" Unit="mmHg">120</Systolic>
            <Diastolic IsAbove="True" Unit="mmHg">80</Diastolic>
            <HeartRate IsAbove="True" Unit="mmHg">100</HeartRate>
       </Alert>
     
        <Alert Type="Cholesterol">
            <TotalCholesterol IsAbove="True" Unit="mg/dL">240</TotalCholesterol>
            <HDL IsAbove="False" Unit="mg/dL">40</HDL>
            <LDL IsAbove="True" Unit="mg/dL">100</LDL>
            <Triglycerides IsAbove="True" Unit="mg/dL">150</Triglycerides>
       </Alert>
     
        <Alert Type="Exercise">
            <Value IsAbove="False">3</Value>
        </Alert>
    */
    public class Helper
    {
        public static ListItemCollection GetTrackerDataTypes()
        {
            ListItemCollection objList = new ListItemCollection();
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.BloodGlucose), TrackerDataType.BloodGlucose.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.BloodPressure), TrackerDataType.BloodPressure.ToString()));
            ////objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.Cholesterol), TrackerDataType.Cholesterol.ToString()));
            ////objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.Exercise), TrackerDataType.Exercise.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.Weight), TrackerDataType.Weight.ToString()));

            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_Alerts_BloodGlucose").ToString(), TrackerDataType.BloodGlucose.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_Alerts_BloodPressure").ToString(), TrackerDataType.BloodPressure.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_Alerts_Weight").ToString(), TrackerDataType.Weight.ToString()));
            return objList;
        }

        public static void  LOG(LogHelper logger, string str)
        {
           // Console.WriteLine(str);
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        public static void CreateAlertsForDate(LogHelper logger, DateTime dt)
        {
            LOG(logger, string.Format("****Fetching updated records for the day at - {0}****", DateTime.Now.ToString()));

            IList<Guid> listGuid = HVHelper.GetUpdatedRecordsForDate(dt);

            LOG(logger, string.Format("****End Fetching updated records for the day at - {0}****", DateTime.Now.ToString()));

            foreach (Guid guid in listGuid)
            {
                LOG(logger, string.Format("****Fetching Patient info {0} at - {1}****", guid, DateTime.Now.ToString()));
                AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordGUID(guid);
                LOG(logger, string.Format("****End Fetching Patient info {0} at - {1}****", guid, DateTime.Now.ToString()));

                if (objPatient == null)
                    continue;

                if (!objPatient.OfflinePersonGUID.HasValue || !objPatient.OfflineHealthRecordGUID.HasValue)
                {
                    Helper.LOG(logger, string.Format("****No offline person guid or offline record guid found for patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                    continue;
                }

                LOG(logger, string.Format("****Fetching Alert Rule  list for Patient {0} at - {1}****", guid, DateTime.Now.ToString()));
                List<AHAHelpContent.AlertRule> ruleList = AHAHelpContent.AlertRule.FindByPatientID(objPatient.UserHealthRecordID, null);
                LOG(logger, string.Format("****End Fetching Alert Rule list for Patient {0} at - {1}****", guid, DateTime.Now.ToString()));

                if (ruleList.Count == 0)
                    continue;

                LOG(logger, string.Format("****Fetching data list from HV for Patient {0} at - {1}****", guid, DateTime.Now.ToString()));

                //changed to use offline record guid
                ReadOnlyCollection<HealthRecordItemCollection> allResults = HVHelper.GetAllValuesForDate(dt, objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                LOG(logger, string.Format("****End Fetching data list from HV for Patient {0} at - {1}****", guid, DateTime.Now.ToString()));

                if (allResults.Count == 0)
                    continue;

                IEnumerable<Microsoft.Health.ItemTypes.BloodGlucose> listBloodGlucose = allResults[0].Cast<Microsoft.Health.ItemTypes.BloodGlucose>();
                IEnumerable<Microsoft.Health.ItemTypes.BloodPressure> listBloodPressure = allResults[1].Cast<Microsoft.Health.ItemTypes.BloodPressure>();
                IEnumerable<Microsoft.Health.ItemTypes.Weight> listWeight = allResults[2].Cast<Microsoft.Health.ItemTypes.Weight>();

                List<AHAHelpContent.AlertRule> ruleListBloodGlucose = ruleList.Where(r => r.Type == AHAHelpContent.TrackerDataType.BloodGlucose).ToList();
                List<AHAHelpContent.AlertRule> ruleListBloodPressure = ruleList.Where(r => r.Type == AHAHelpContent.TrackerDataType.BloodPressure).ToList();
                List<AHAHelpContent.AlertRule> ruleListWeight = ruleList.Where(r => r.Type == AHAHelpContent.TrackerDataType.Weight).ToList();
                IEnumerable<Microsoft.Health.ItemTypes.Personal> listPersonal = allResults[3].Cast<Microsoft.Health.ItemTypes.Personal>();
                Microsoft.Health.ItemTypes.Personal personalItem = null;
                if (listPersonal.Count() > 0)
                {
                    personalItem = listPersonal.FirstOrDefault();
                }

                LOG(logger, string.Format("****Processing Alert data for Patient {0} at - {1}****", guid, DateTime.Now.ToString()));

                BloodGlucoseAlertHelper.ProcessReadingForAlert(logger, objPatient, personalItem, ruleListBloodGlucose, listBloodGlucose);
                BloodPressureAlertHelper.ProcessReadingForAlert(logger, objPatient, personalItem, ruleListBloodPressure, listBloodPressure);
                WeightAlertHelper.ProcessReadingForAlert(logger, objPatient, personalItem, ruleListWeight, listWeight);

                LOG(logger, string.Format("****End Processing Alert data for Patient {0} at - {1}****", guid, DateTime.Now.ToString()));
            }
        }

        public static string GetPatientName(Microsoft.Health.ItemTypes.Personal personalItem)
        {
            if (personalItem == null)
                return null;

            else
            {
                if (personalItem.Name != null && !string.IsNullOrEmpty(personalItem.Name.First) && !string.IsNullOrEmpty(personalItem.Name.Last))
                    return string.Format("{0} {1}", personalItem.Name.First, personalItem.Name.Last);
                else if (personalItem.Name != null && !string.IsNullOrEmpty(personalItem.Name.First) && string.IsNullOrEmpty(personalItem.Name.Last))
                    return personalItem.Name.First;
                else if (personalItem.Name != null && string.IsNullOrEmpty(personalItem.Name.First) && !string.IsNullOrEmpty(personalItem.Name.Last))
                    return personalItem.Name.Last;
            }

            return null;
        }
    }
}
