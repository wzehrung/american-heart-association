﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.ComponentModel;
using System.Web.UI.WebControls;
using AHAHelpContent;
using GRCBase;
using AHACommon;
using System.Web.UI;
using HVManager;


namespace AlertManager
{
    public class BloodGlucoseAlertRule
    {
        public double Value
        {
            get;
            set;
        }

        public bool IsAbove
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }

        public static string GetFormattedAlertRule(string strXml)
        {
            BloodGlucoseAlertRule objAlert = ParseAlertRuleXml(strXml);
            if (objAlert.IsAbove)
            {
                //return string.Format("Blood Glucose > {0}", objAlert.Value);
                // PJB 4.21.2014 - use local resource file
                //return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Common", "BloodGlucoseGreaterAlert").ToString(), objAlert.Value);
                return string.Format(AlertManager.App_GlobalResources.Alerts.BloodGlucoseGreaterAlert, objAlert.Value);
            }
            else
            {
                //return string.Format("Blood Glucose < {0}", objAlert.Value);
                // PJB 4.21.2014 - use local resource file
                //return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Alerts", "BloodGlucoseLesserAlert").ToString(), objAlert.Value);
                return string.Format(AlertManager.App_GlobalResources.Alerts.BloodGlucoseLesserAlert, objAlert.Value);
            }
        }

        public static BloodGlucoseAlertRule ParseAlertRuleXml(string strXml)
        {
            if (string.IsNullOrEmpty(strXml))
                return null;

            XDocument xDoc = XDocument.Parse(strXml);

            BloodGlucoseAlertRule obj = new BloodGlucoseAlertRule();

            obj.Unit = xDoc.XPathSelectElement("Alert/Value").Attribute("Unit").Value;
            obj.IsAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/Value").Attribute("IsAbove").Value);
            obj.Value = Convert.ToDouble(xDoc.XPathSelectElement("Alert/Value").Value);

            return obj;
        }

        public static string CreateAlertRuleXml(bool bIsAbove, double dValue)
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.BloodGlucose.ToString()),
                                    new XElement("Value"
                                        , new XAttribute("IsAbove", bIsAbove ? true.ToString() : false.ToString())
                                        , new XAttribute("Unit", "mg/dL")
                                    , dValue.ToString())));

            return xDoc.ToString();
        }
    }

    public class BloodGlucoseAlert
    {
        public double Value
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }

        public static string GetFormattedAlert(string strXml)
        {
            BloodGlucoseAlert objAlert = ParseAlertXml(strXml);
            //return string.Format("Blood Glucose = {0}", objAlert.Value);
            // PJB 4.21.2014 - use local resource file
            //return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Alerts", "Text_BloodGlucose").ToString() + " = {0}", objAlert.Value);
            return string.Format(AlertManager.App_GlobalResources.Alerts.Text_BloodGlucose + " = {0}", objAlert.Value);
        }

        public static BloodGlucoseAlert ParseAlertXml(string strXml)
        {
            if (string.IsNullOrEmpty(strXml))
                return null;

            XDocument xDoc = XDocument.Parse(strXml);

            BloodGlucoseAlert obj = new BloodGlucoseAlert();

            obj.Unit = xDoc.XPathSelectElement("Alert/Value").Attribute("Unit").Value;
            obj.Value = Convert.ToDouble(xDoc.XPathSelectElement("Alert/Value").Value);

            return obj;
        }

        public static string CreateAlertXml(double dValue)
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.BloodGlucose.ToString()),
                                    new XElement("Value", new XAttribute("Unit", "mg/dL")
                                    , dValue.ToString())));

            return xDoc.ToString();
        }
    }

    public class BloodGlucoseAlertHelper
    {
        public static void ProcessHeart360ReadingForAlert(Guid gPersonGUID, Guid gRecordGUID, int iPatientID, string strPatientName, double dValue, DateTime dtEffectiveDate, Guid gHealthRecordItemGUID)
        {
            if (dtEffectiveDate.CompareTo(DateTime.Now.AddDays(-1)) != 1)
                return;

            List<AlertRule> listRule = AlertRule.FindByPatientID(iPatientID, AHAHelpContent.TrackerDataType.BloodGlucose);

            foreach (AlertRule objRule in listRule)
            {
                BloodGlucoseAlertRule alertData = BloodGlucoseAlertRule.ParseAlertRuleXml(objRule.AlertRuleData);
                if (alertData != null)
                {
                    bool bCreateAlert = ((alertData.IsAbove && dValue > alertData.Value) ||
                        (!alertData.IsAbove && dValue < alertData.Value));

                    AHAHelpContent.ProviderDetails objProvider = AHAHelpContent.ProviderDetails.FindByProviderID(objRule.ProviderID);

                    if (bCreateAlert)
                    {
                        AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(gHealthRecordItemGUID, objRule.AlertRuleID);

                        if (objExistingAlert == null)
                        {
                            AHAHelpContent.Alert.CreateAlert(objRule.AlertRuleID, iPatientID, gHealthRecordItemGUID, BloodGlucoseAlert.CreateAlertXml(dValue), DateTime.Now);
                            //send alert notification to provider if provider has subscribed to alerts as they happen

                            if (objProvider.NotifyNewAlertImmediately.HasValue && objProvider.NotifyNewAlertImmediately.Value)
                            {
                                //send alert email
                                string strName = string.Format("{0} {1}", objProvider.FirstName, objProvider.LastName);
                                EmailHelper.SendAlertEmailToProvider(false, strName, objProvider.EmailForIndividualAlert, strPatientName, objProvider.LanguageLocale);
                            }

                            if (objRule.SendMessageToPatient.HasValue && objRule.SendMessageToPatient.Value)
                            {
                                //create a db context, create db message, create hv message
                                DBContext dbContext = null;
                                bool bException = false;
                                try
                                {
                                    dbContext = DBContext.GetDBContext();
                                    //Create message entry in db
                                    int iMessageID = AHAHelpContent.Message.CreateMessage(objProvider.ProviderID, iPatientID, AHACommon.AHADefs.MessageType.MESSAGE_TYPE_NEW, true);

                                    //Create message in HV
                                    string strMessageBody = objRule.MessageToPatient;

                                    PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;

                                    string strSubject = emailSetting.Subject_Alert_Message_Patient;

                                    HVManager.MessageDataManager.MessageItem objMessage = new HVManager.MessageDataManager.MessageItem();
                                    objMessage.DateSent.Value = DateTime.Now;
                                    objMessage.IsSentByProvider.Value = true;
                                    objMessage.MessageBody.Value = strMessageBody;
                                    objMessage.MessageID.Value = iMessageID;
                                    objMessage.ProviderID.Value = objProvider.ProviderID;
                                    objMessage.Subject.Value = strSubject;

                                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);

                                    Page objPage = System.Web.HttpContext.Current.Handler as Page;
                                    objMessage.Source.Value = BrandLogoHelperMachine.GetApplicationBrandInfo(objPage);
                                    mgr.MessageDataManager.CreateItem(objMessage);
                                }
                                catch
                                {
                                    bException = true;
                                    throw;
                                }
                                finally
                                {
                                    try
                                    {
                                        if (bException)
                                        {
                                            if (dbContext != null)
                                            {
                                                dbContext.ReleaseDBContext(false);
                                            }
                                        }
                                        else
                                        {
                                            dbContext.ReleaseDBContext(true);
                                        }
                                    }
                                    catch
                                    {
                                        if (dbContext != null)
                                        {
                                            dbContext.ReleaseDBContext(false);
                                        }
                                        throw;
                                    }
                                }
                            }
                        }
                    }
                    //else
                    //{
                    //    //get the alert for the health record item guid
                    //    AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(gHealthRecordItemGUID, objRule.AlertRuleID);
                    //    //if no alert for the new value, delete the existing alert, otherwise update the existing alert or create a new one if an item does not exist

                    //    if (objExistingAlert != null)
                    //    {
                    //        AHAHelpContent.Alert.DeleteAlert(objExistingAlert.AlertID);
                    //        if (objProvider.NotifyNewAlertImmediately.HasValue && objProvider.NotifyNewAlertImmediately.Value)
                    //        {
                    //            //send alert email
                    //            string strName = string.Format("{0} {1}", objProvider.FirstName, objProvider.LastName);
                    //            //send ignore previous notification for this patient email
                    //            EmailHelper.SendAlertModifiedEmailToProvider(strName, objProvider.EmailForIndividualAlert, strPatientName);
                    //        }
                    //    }
                    //}
                }
            }
        }

        //private static string _GetMessageBody(bool bProcess)
        //{
        //    PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;

        //    string filepath = string.Format("{0}\\{1}", AHAAppSettings.EmailTemplateDirectory, emailSetting.File_Alert_Message_Patient);

        //    string strContent = IOUtility.ReadHtmlFile(filepath);

        //    if (bProcess)
        //    {
        //        strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", AHAAppSettings.SiteUrl);
        //    }
        //    else
        //    {
        //        strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", GRCBase.UrlUtility.ServerRootUrl);
        //    }

        //    return strContent;
        //}

        public static void ProcessReadingForAlert(LogHelper logger, AHAHelpContent.Patient objPatient, Microsoft.Health.ItemTypes.Personal personalItem, List<AlertRule> listRule, IEnumerable<Microsoft.Health.ItemTypes.BloodGlucose> listBloodGlucose)
        {
            if (listRule.Count == 0 || listBloodGlucose.Count() == 0)
            {
                Helper.LOG(logger, string.Format("****No Blood Glucose Alert For for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                return;
            }

            if (!objPatient.OfflinePersonGUID.HasValue || !objPatient.OfflineHealthRecordGUID.HasValue)
            {
                Helper.LOG(logger, string.Format("****No offline person guid or offline record guid found for patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                return;
            }

            foreach (AlertRule objRule in listRule)
            {
                BloodGlucoseAlertRule alertData = BloodGlucoseAlertRule.ParseAlertRuleXml(objRule.AlertRuleData);
                if (alertData != null)
                {
                    foreach (Microsoft.Health.ItemTypes.BloodGlucose objBloodGlucose in listBloodGlucose)
                    {
                        double dValue = HVManager.Helper.GlocoseMmolToMgdl(objBloodGlucose.Value.Value);
                        bool bCreateAlert = ((alertData.IsAbove && dValue > alertData.Value) ||
                            (!alertData.IsAbove && dValue < alertData.Value));

                        if (bCreateAlert)
                        {
                            AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(objBloodGlucose.Key.Id, objRule.AlertRuleID);

                            if (objExistingAlert == null)
                            {
                                Helper.LOG(logger, string.Format("****Creating Blood Glucose Alert for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                AHAHelpContent.Alert.CreateAlert(objRule.AlertRuleID, objPatient.UserHealthRecordID, objBloodGlucose.Key.Id, BloodGlucoseAlert.CreateAlertXml(dValue), objBloodGlucose.EffectiveDate);
                                Helper.LOG(logger, string.Format("****End Creating Blood Glucose Alert for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));

                                AHAHelpContent.ProviderDetails objProvider = AHAHelpContent.ProviderDetails.FindByProviderID(objRule.ProviderID);

                                string strPatientName = Helper.GetPatientName(personalItem);
                                if (objProvider.NotifyNewAlertImmediately.HasValue && objProvider.NotifyNewAlertImmediately.Value && !string.IsNullOrEmpty(strPatientName))
                                {
                                    Helper.LOG(logger, string.Format("****Start Sending Alert Notification to Provider {0} for patient {1} at - {2}****", objProvider.FirstName, objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                    //send alert email
                                    string strName = string.Format("{0} {1}", objProvider.FirstName, objProvider.LastName);
                                    EmailHelper.SendAlertEmailToProvider(true, strName, objProvider.EmailForIndividualAlert, strPatientName, objProvider.LanguageLocale);
                                    Helper.LOG(logger, string.Format("****End Sending Alert Notification to Provider {0} for patient {1} at - {2}****", objProvider.FirstName, objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                }

                                if (objRule.SendMessageToPatient.HasValue && objRule.SendMessageToPatient.Value)
                                {
                                    //create a db context, create db message, create hv message
                                    DBContext dbContext = null;
                                    bool bException = false;
                                    try
                                    {
                                        dbContext = DBContext.GetDBContext();
                                        Helper.LOG(logger, string.Format("****Start Creating Heart360 Message in Heart360 Mailbox for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                        //Create message entry in db
                                        int iMessageID = AHAHelpContent.Message.CreateMessage(objProvider.ProviderID, objPatient.UserHealthRecordID, AHACommon.AHADefs.MessageType.MESSAGE_TYPE_NEW, true);

                                        //Create message in HV
                                        string strMessageBody = objRule.MessageToPatient;

                                        PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;

                                        string strSubject = emailSetting.Subject_Alert_Message_Patient;

                                        HVHelper.CreateSystemToPatientMessageOnAlert(objPatient.OfflinePersonGUID.Value,
                                            objPatient.OfflineHealthRecordGUID.Value,
                                            iMessageID,
                                            true,
                                            DateTime.Now,
                                            strSubject,
                                            strMessageBody,
                                            objProvider.ProviderID);

                                        Helper.LOG(logger, string.Format("****End Creating Heart360 Message in Heart360 Mailbox for Patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                                    }
                                    catch
                                    {
                                        bException = true;
                                        throw;
                                    }
                                    finally
                                    {
                                        try
                                        {
                                            if (bException)
                                            {
                                                if (dbContext != null)
                                                {
                                                    dbContext.ReleaseDBContext(false);
                                                }
                                            }
                                            else
                                            {
                                                dbContext.ReleaseDBContext(true);
                                            }
                                        }
                                        catch
                                        {
                                            if (dbContext != null)
                                            {
                                                dbContext.ReleaseDBContext(false);
                                            }
                                            throw;
                                        }
                                    }
                                }
                            }
                            //get provider and check for notification
                        }
                        else
                        {
                            //get the alert for the health record item guid
                            //AHAHelpContent.Alert objExistingAlert = AHAHelpContent.Alert.FindAlertByHealthRecordItemGUIDAndAlertRuleID(objBloodGlucose.Key.Id, objRule.AlertRuleID);
                            //if no alert for the new value, delete the existing alert, otherwise update the existing alert or create a new one if an item does not exist

                            //if (objExistingAlert != null)
                            //{
                            //    Helper.LOG(logger, string.Format("****Deleting Blood Glucose Alert for Patient {0} at - {1}****", iPatientID, DateTime.Now.ToString()));
                            //    AHAHelpContent.Alert.DeleteAlert(objExistingAlert.AlertID);
                            //    Helper.LOG(logger, string.Format("****End Deleting Blood Glucose Alert for Patient {0} at - {1}****", iPatientID, DateTime.Now.ToString()));
                            //}
                        }
                    }
                }
            }
        }
    }
}
