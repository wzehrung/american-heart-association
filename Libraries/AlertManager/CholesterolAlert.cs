﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.ComponentModel;
using System.Web.UI.WebControls;
using AHAHelpContent;


namespace AlertManager
{
    public class CholesterolAlertRule
    {
        public int TotalCholesterol
        {
            get;
            set;
        }

        public bool IsTotalCholesterol
        {
            get;
            set;
        }

        public int HDL
        {
            get;
            set;
        }

        public bool IsHDLAbove
        {
            get;
            set;
        }

        public int LDL
        {
            get;
            set;
        }

        public bool IsLDLAbove
        {
            get;
            set;
        }

        public int Triglycerides
        {
            get;
            set;
        }

        public bool IsTriglyceridesAbove
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }

        public static CholesterolAlertRule ParseAlertRuleXml(string strXml)
        {
            XDocument xDoc = XDocument.Parse(strXml);

            CholesterolAlertRule obj = new CholesterolAlertRule();

            obj.IsTotalCholesterol = Boolean.Parse(xDoc.XPathSelectElement("Alert/TotalCholesterol").Attribute("IsAbove").Value);
            obj.IsHDLAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/HDL").Attribute("IsAbove").Value);
            obj.IsLDLAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/LDL").Attribute("IsAbove").Value);
            obj.IsTriglyceridesAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/Triglycerides").Attribute("IsAbove").Value);
            obj.TotalCholesterol = Convert.ToInt32(xDoc.XPathSelectElement("Alert/TotalCholesterol").Value);
            obj.HDL = Convert.ToInt32(xDoc.XPathSelectElement("Alert/HDL").Value);
            obj.LDL = Convert.ToInt32(xDoc.XPathSelectElement("Alert/LDL").Value);
            obj.Triglycerides = Convert.ToInt32(xDoc.XPathSelectElement("Alert/Triglycerides").Value);

            return obj;
        }

        public static string CreateAlertRuleXml(bool bIsTotalCholesterolAbove,
            int iTotalCholesterol,
            bool bIsHDLAbove,
            int iHDL,
            bool bIsLDLAbove,
            int iLDL,
            bool bIsTriglyceridesAbove,
            int iTriglycerides)
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.Cholesterol.ToString()),
                                    new XElement("TotalCholesterol"
                                        , new XAttribute("IsAbove", bIsTotalCholesterolAbove ? true.ToString() : false.ToString())
                                        , new XAttribute("Unit", "mg/dL")
                                    , iTotalCholesterol.ToString())

                                    , new XElement("HDL"
                                        , new XAttribute("IsAbove", bIsHDLAbove ? true.ToString() : false.ToString())
                                        , new XAttribute("Unit", "mg/dL")
                                    , iHDL.ToString())

                                    , new XElement("LDL"
                                        , new XAttribute("IsAbove", bIsLDLAbove ? true.ToString() : false.ToString())
                                        , new XAttribute("Unit", "mg/dL")
                                    , iLDL.ToString())

                                    , new XElement("Triglycerides"
                                        , new XAttribute("IsAbove", bIsTriglyceridesAbove ? true.ToString() : false.ToString())
                                        , new XAttribute("Unit", "mg/dL")
                                    , iTriglycerides.ToString())));

            return xDoc.ToString();
        }
    }
}
