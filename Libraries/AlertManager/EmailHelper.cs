﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using GRCBase;
using System.Resources;
using AlertManager.App_GlobalResources;

namespace AlertManager
{
    public class EmailHelper
    {
        private static string _getEmailTemplateDirectory(string strLocale)
        {
            if (strLocale != Locale.English)
            {
                return string.Format(AHAAppSettings.EmailTemplateDirectory + ".{0}", strLocale);
            }
            return AHAAppSettings.EmailTemplateDirectory;
        }

        public static void SendAlertEmailToProvider(bool bProcess, string strToName, string strToEmail, string strPatientName, string strLocale)
        {
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}\\{1}", _getEmailTemplateDirectory(strLocale), emailSetting.File_Alert_Provider);

            //ResourceManager rm = new System.Resources.ResourceManager("Resources.Alerts", System.Reflection.Assembly.Load("App_GlobalResources"));
            ResourceManager rm = _GetResourceManager();
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = rm.GetString("Subject_Alert_Provider", cultureInfo);
            //string subject = emailSetting.Subject_Alert_Provider;

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT##", strPatientName);
            if (bProcess)
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", AHAAppSettings.SiteUrl);
            }
            else
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", GRCBase.UrlUtility.ServerRootUrl);
            }

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(bProcess, strContent, true, strLocale);
            if (bProcess)
            {
                string strDiscliamerText = rm.GetString("Provider_Footer_Disclaimer", cultureInfo);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##DISCLAIMER##", strDiscliamerText);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##REGISTERED##", "<sup>&#174;</sup>");
            }

            //Send Email
            // PJB: DMARC fix
            //GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SystemEmail, null);
        }

        private static ResourceManager _GetResourceManager()
        {
            ResourceManager rm = new System.Resources.ResourceManager("AlertManager.App_GlobalResources.Alerts", typeof(Alerts).Assembly);
            return rm;
        }

        //public static void SendAlertModifiedEmailToProvider(string strToName, string strToEmail, string strPatientName)
        //{
        //    PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
        //    string filepath = string.Format("{0}\\{1}", _getEmailTemplateDirectory(), emailSetting.File_AlertModified);
        //    string subject = emailSetting.Subject_AlertModified;

        //    string strContent = IOUtility.ReadHtmlFile(filepath);

        //    strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT##", strPatientName);

        //    strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent);

        //    //Send Email
        //    GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
        //}
    }
}
