﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.ComponentModel;
using System.Web.UI.WebControls;
using AHAHelpContent;


namespace AlertManager
{
    public class ExerciseAlertRule
    {
        public int Value
        {
            get;
            set;
        }

        public bool IsAbove
        {
            get;
            set;
        }

        public static ExerciseAlertRule ParseAlertXml(string strXml)
        {
            XDocument xDoc = XDocument.Parse(strXml);

            ExerciseAlertRule obj = new ExerciseAlertRule();

            obj.IsAbove = Boolean.Parse(xDoc.XPathSelectElement("Alert/Value").Attribute("IsAbove").Value);
            obj.Value = Convert.ToInt32(xDoc.XPathSelectElement("Alert/Value").Value);

            return obj;
        }

        public static string CreateAlertXml(bool bIsAbove, int iTimesPerWeek)
        {
            XDocument xDoc = new XDocument();

            xDoc.Add(new XElement("Alert", new XAttribute("Type", TrackerDataType.Exercise.ToString()),
                                    new XElement("Value"
                                        , new XAttribute("IsAbove", bIsAbove ? true.ToString() : false.ToString())
                                    , iTimesPerWeek.ToString())));

            return xDoc.ToString();
        }
    }
}
