﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Health.Web;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using System.Collections.ObjectModel;

namespace AlertManager
{
    public class HVHelper
    {
        /// <summary>
        /// Returns the Accessor object for a given PersonID and HealthRecordID using an offline connection
        /// </summary>
        /// <param name="hvpersonid"></param>
        /// <param name="hvrecordid"></param>
        /// <returns></returns>
        public static Microsoft.Health.HealthRecordAccessor GetAccessor(Guid hvpersonid, Guid hvrecordid)
        {
            OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
            offlineConn.Authenticate();
            HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);
            return accessor;
        }

        public static IList<Guid> GetUpdatedRecordsForDate(DateTime dt)
        {
            OfflineWebApplicationConnection conn = new OfflineWebApplicationConnection(WebApplicationConfiguration.AppId,
               WebApplicationConfiguration.HealthServiceUrl.ToString(), Guid.Empty);

            IList<Guid> list = conn.GetUpdatedRecordsForApplication(dt);

            return list;
        }

        public static void CreateSystemToPatientMessageOnAlert(Guid gPersonGUID, 
            Guid gRecordGUID,
            int iMessageID,
            bool bIsSentByProvider,
            DateTime dtDateSent,
            string strSubject,
            string strMessageBody,
            int iProviderID)
        {
            HVWrapper.Message messageThing = new HVWrapper.Message();
            HVWrapper.CustomHealthTypeWrapper wrapper = new HVWrapper.CustomHealthTypeWrapper(messageThing);

            messageThing.When = new HealthServiceDateTime(DateTime.Now);

            messageThing.MessageID = iMessageID;
            messageThing.IsSentByProvider = bIsSentByProvider;
            messageThing.DateSent = dtDateSent;
            messageThing.Subject = strSubject;
            messageThing.MessageBody = strMessageBody;
            messageThing.ProviderID = iProviderID;
            wrapper.CommonData.Source = "Heart360";

            HealthRecordAccessor accessor = GetAccessor(gPersonGUID, gRecordGUID);

            accessor.NewItem(wrapper);
        }

        public static ReadOnlyCollection<HealthRecordItemCollection> GetAllValuesForDate(DateTime dt, Guid hvpersonid, Guid hvrecordid)
        {
            HealthRecordAccessor accessor = GetAccessor(hvpersonid, hvrecordid);
            HealthRecordSearcher searcher = accessor.CreateSearcher();

            DateTime dtEffectiveDateMin = dt;
            DateTime dtEffectiveDateMax = DateTime.Now;

            HealthRecordFilter filterBloodGlucose = new HealthRecordFilter(BloodGlucose.TypeId);
            filterBloodGlucose.EffectiveDateMin = dtEffectiveDateMin;
            filterBloodGlucose.EffectiveDateMax = dtEffectiveDateMax;

            searcher.Filters.Add(filterBloodGlucose);

            HealthRecordFilter filterBloodPressure = new HealthRecordFilter(BloodPressure.TypeId);
            filterBloodPressure.EffectiveDateMin = dtEffectiveDateMin;
            filterBloodPressure.EffectiveDateMax = dtEffectiveDateMax;

            searcher.Filters.Add(filterBloodPressure);

            HealthRecordFilter filterWeight = new HealthRecordFilter(Weight.TypeId);
            //we need to make some change if the client requests some diff period
            filterWeight.EffectiveDateMin = dtEffectiveDateMin.AddDays(-14);
            filterWeight.EffectiveDateMax = dtEffectiveDateMax;

            searcher.Filters.Add(filterWeight);

            HealthRecordFilter filterPersonal = new HealthRecordFilter(Personal.TypeId);
            searcher.Filters.Add(filterPersonal);

            ReadOnlyCollection<HealthRecordItemCollection> allResults = searcher.GetMatchingItems();

            return allResults;
        }
    }
}
