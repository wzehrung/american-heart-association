﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace GRCBase
{
    public enum GRCDateFormat
    {
        /// <summary>
        /// Date will not appear on the output
        /// </summary>
        NONE,

        /// <summary>
        /// Format date as 12/31/2007 with appropriate seperator
        /// </summary>
        [Description("MM#dd#yyyy")]
        MMDDYYYY,

        /// <summary>
        /// Format date as 12/31/07 with appropriate seperator
        /// </summary>
        [Description("MM#dd#yy")]
        MMDDYY,

        /// <summary>
        /// Format date as Dec/31/2007 with appropriate seperator
        /// </summary>
        [Description("MMM#dd#yyyy")]
        MMMDDYYYY,

        /// <summary>
        /// Format date as Dec/31/07 with appropriate seperator
        /// </summary>
        [Description("MMM#dd#yy")]
        MMMDDYY,

        /// <summary>
        /// Format date as December/31/2007 with appropriate seperator
        /// </summary>
        [Description("MMMMM#dd#yyyy")]
        MMMMMDDYYYY,

        /// <summary>
        /// Format date as December/31/07 with appropriate seperator
        /// </summary>
        [Description("MMMMM#dd#yy")]
        MMMMMDDYY,

        /// <summary>
        /// Format date as 31/12/2007 with appropriate seperator
        /// </summary>
        [Description("dd#MM#yyyy")]
        DDMMYYYY,

        /// <summary>
        /// Format date as 31/12/07 with appropriate seperator
        /// </summary>
        [Description("dd#MM#yy")]
        DDMMYY,

        /// <summary>
        /// Format date as 31/Jan/2007 with appropriate seperator
        /// </summary>
        [Description("dd#MMM#yyyy")]
        DDMMMYYYY,

        /// <summary>
        /// Format date as 31/12/07 with appropriate seperator
        /// </summary>
        [Description("dd#MMM#yy")]
        DDMMMYY,

        /// <summary>
        /// Format date as 31/December/2007 with appropriate seperator
        /// </summary>
        [Description("dd#MMMMM#yyyy")]
        DDMMMMMYYYY,

        /// <summary>
        /// Format date as 31/December/07 with appropriate seperator
        /// </summary>
        [Description("dd#MMMMM#yy")]
        DDMMMMMYY
    }

    public enum GRCDateSeperator
    {
        /// <summary>
        /// No Date seperator will be shown.
        /// </summary>
        NONE,
        /// <summary>
        /// Date seperator will be "-".
        /// </summary>
        HYPHEN,
        /// <summary>
        /// Date seperator will be "/".
        /// </summary>
        SLASH,
        /// <summary>
        /// Date seperator will be " ".
        /// </summary>
        SPACE
    }

    public enum GRCTimeFormat
    {
        /// <summary>
        /// Time will not appear on the output
        /// </summary>
        NONE,
        /// <summary>
        /// Format time as 14:30:28 (Time in 24 hour format.)
        /// </summary>
        [Description("HH:mm:ss")]
        HHmmss,
        /// <summary>
        /// Format time as 14:30 (Time in 24 hour format.)
        /// </summary>
        [Description("HH:mm")]
        HHmm,
        /// <summary>
        /// Format time as 02:30 (Time in 12 hour format.)
        /// </summary>
        [Description("hh:mm")]
        hhmm,
        /// <summary>
        /// Format time as 02:30:28 (Time in 12 hour format.)
        /// </summary>
        [Description("hh:mm:ss")]
        hhmmss,
        /// <summary>
        /// Format time as 02:30 PM (Time in 12 hour format.)
        /// </summary>
        [Description("hh:mm tt")]
        hhmmtt,
        /// <summary>
        /// Format time as 02:30:28 (Time in 12 hour format.)
        /// </summary>
        [Description("hh:mm:ss tt")]
        hhmmsstt,
        /// <summary>
        /// Format time as 02:30 (Time in 12 hour format.)
        /// </summary>
        [Description("HH:MM AM/PM")]
        HHMMAMPM
    }

    public class DateTimeHelper
    {
        public static string GetRssStringFromDateTime(DateTime dt)
        {
            //return (R) RFC1123:. . . . . . . . . Sat, 26 Jun 2004 20:11:04 GMT
            return dt.ToString("R");
        }

        public static DateTime GetDateTimeFromRssString(string rssDateString)
        {
            //return (R) RFC1123:. . . . . . . . . Sat, 26 Jun 2004 20:11:04 GMT
            return DateTime.ParseExact(rssDateString, "R", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
        }

        public static int GetMonthEndDay(DateTime dtDate)
        {
            int iMonth = dtDate.Month;
            if (iMonth == 1 || iMonth == 3 || iMonth == 5 || iMonth == 7 || iMonth == 8 || iMonth == 10 || iMonth == 12)
            {
                return 31;
            }
            else if (iMonth == 2)
            {
                if (DateTime.IsLeapYear(dtDate.Year))
                {
                    return 29;
                }
                else
                {
                    return 28;
                }
            }
            else
            {
                return 30;
            }
        }

        public static string GetMonthName(int monthnumber)
        {
            switch (monthnumber)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "Invalid";
            }
        }

        public static string GetDefaultFormattedDateTimeString(DateTime dt)
        {
            return GetFormattedDateTimeString(dt, LibrarySettings.GetDefaultDateFormat(), LibrarySettings.GetDefaultDateSeperatorFormat(), LibrarySettings.GetDefaultTimeFormat());
        }

        /// <summary>
        /// Returns formatted string for date according to the enum selected.use none value of the enum if date or time is not required.
        /// </summary>
        /// <param name="dt">DateTime object which is to be formatted.</param>
        /// <param name="DateFormatTypes">Type of GRCDateFormat, use NONE value if date part is not required.</param>
        /// <param name="DateSeperator">Type of GRCDateSeperator, use NONE value if seperator not required.</param>
        /// <param name="TimeFormatType">Type of GRCTimeFormat, use NONE value if time part not required.</param>
        /// <returns></returns>
        public static string GetFormattedDateTimeString(DateTime dt, GRCDateFormat DateFormatTypes, GRCDateSeperator DateSeperator, GRCTimeFormat TimeFormatType)
        {
            string DateFormat = string.Empty;
            string TimeFormat = string.Empty;
            StringBuilder strDateTime = new StringBuilder();

            if (DateFormatTypes != GRCDateFormat.NONE)
                DateFormat = MiscUtility.GetComponentModelPropertyDescription(DateFormatTypes);

            if (DateSeperator == GRCDateSeperator.HYPHEN)
                DateFormat = DateFormat.Replace("#", "-");
            else if (DateSeperator == GRCDateSeperator.SLASH)
                DateFormat = DateFormat.Replace("#", "/");
            else if (DateSeperator == GRCDateSeperator.NONE)
                DateFormat = DateFormat.Replace("#", "");
            else if (DateSeperator == GRCDateSeperator.SPACE)
                DateFormat = DateFormat.Replace("#", " ");

            if (TimeFormatType != GRCTimeFormat.NONE)
                TimeFormat = MiscUtility.GetComponentModelPropertyDescription(TimeFormatType);

            if (DateFormatTypes != GRCDateFormat.NONE)
                strDateTime.Append(dt.ToString(DateFormat));

            if (TimeFormatType != GRCTimeFormat.NONE)
            {
                if (DateFormatTypes != GRCDateFormat.NONE)
                    strDateTime.Append(" | ");
                if (TimeFormatType == GRCTimeFormat.HHMMAMPM)
                {
                    strDateTime.Append(String.Format("{0:t}", dt));  
                }
                else
                {
                    strDateTime.Append(dt.ToString(TimeFormat));
                }
            }
            return strDateTime.ToString();
        }

        public static string GetFormattedDateTimeDifference(DateTime dtRecent, DateTime dtOlder)
        {
            StringBuilder returnString = new StringBuilder();
            TimeSpan timeDiff = dtRecent - dtOlder;
            if (timeDiff.Days > 1)
            {
                returnString.Append(GetFormattedDateTimeString(dtOlder, LibrarySettings.GetDefaultDateFormat(), LibrarySettings.GetDefaultDateSeperatorFormat(), LibrarySettings.GetDefaultTimeFormat()));
            }
            else if (timeDiff.Days == 1)
            {
                returnString.Append("Yesterday ");
            }
            else if ((timeDiff.Hours >= 1) && (timeDiff.Days == 0))
            {
                if (timeDiff.Hours == 1)
                    returnString.Append(timeDiff.Hours.ToString() + " hour ago");
                else
                    returnString.Append(timeDiff.Hours.ToString() + " hours ago");
            }
            else if ((timeDiff.Hours < 1) && (timeDiff.Days == 0))
            {
                if (timeDiff.Minutes == 1)
                    returnString.Append(timeDiff.Minutes.ToString() + " minute ago");
                else
                    returnString.Append(timeDiff.Minutes.ToString() + " minutes ago");
            }
            return returnString.ToString();
        }

        public static string GetFormattedDateTimeDifferenceWithDateTimeFormat(DateTime dtRecent, DateTime dtOlder, GRCDateFormat DateFormatTypes, GRCDateSeperator DateSeperator, GRCTimeFormat TimeFormatType)
        {
            StringBuilder returnString = new StringBuilder();
            TimeSpan timeDiff = dtRecent - dtOlder;
            if (timeDiff.Days > 1)
            {
                returnString.Append(GetFormattedDateTimeString(dtOlder, DateFormatTypes, DateSeperator, TimeFormatType));
            }
            else if (timeDiff.Days == 1)
            {
                returnString.Append("Yesterday ");
            }
            else if ((timeDiff.Hours >= 1) && (timeDiff.Days == 0))
            {
                if (timeDiff.Hours == 1)
                    returnString.Append(timeDiff.Hours.ToString() + " hour ago");
                else
                    returnString.Append(timeDiff.Hours.ToString() + " hours ago");
            }
            else if ((timeDiff.Hours < 1) && (timeDiff.Days == 0))
            {
                if (timeDiff.Minutes == 1)
                    returnString.Append(timeDiff.Minutes.ToString() + " minute ago");
                else
                    returnString.Append(timeDiff.Minutes.ToString() + " minutes ago");
            }
            return returnString.ToString();
        }

        void f()
        {

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        /// <remarks>Added by renold</remarks>
        public static string GetFriendlyDate(DateTime src)
        {
            DateTime currDate = DateTime.Now;
            int dateDiff = (src - currDate).Days;
            if (dateDiff == -1)
            {
                return "Yesterday";
            }
            else if (dateDiff == 0)
            {
                return "Today";
            }
            else if ((dateDiff > -8) && (dateDiff < -1))
            {
                return "Last " + src.DayOfWeek.ToString();
            }
            else if (dateDiff == 1)
            {

                return src.DayOfWeek.ToString();
            }
            else
            {
                return src.ToString();
            }
        }
    }
}
