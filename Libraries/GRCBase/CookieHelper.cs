﻿using System;
using System.Web;

namespace GRCBase
{
    public class CookieHelper
    {
        public static void SetCookie(string strCookieName, string strCookieValue, DateTime? dtExpires)
        {
            HttpCookie objCookie = new HttpCookie(strCookieName, strCookieValue); ;


            if (dtExpires != null)
            {
                //HttpContext.Current.Response.Cookies[strCookieName].Expires = dtExpires.Value;
                objCookie.Expires = dtExpires.Value;
            }

            HttpContext.Current.Response.Cookies.Add(objCookie);

            //Add it to request also so that rest of the context sees it immediately and need not wait
            //for next request

            HttpContext.Current.Request.Cookies.Remove(strCookieName);
            HttpContext.Current.Request.Cookies.Add(objCookie);


        }

        public static HttpCookie GetCookie(string strCookieName)
        {
            return HttpContext.Current.Request.Cookies[strCookieName];
        }

        public static void DeleteCookie(string strCookieName)
        {
            if (HttpContext.Current.Request.Cookies[strCookieName] != null)
            {
                HttpContext.Current.Request.Cookies.Remove(strCookieName);
            }

            if (HttpContext.Current.Response.Cookies[strCookieName] != null)
            {
                HttpContext.Current.Response.Cookies.Remove(strCookieName);
                HttpContext.Current.Response.Cookies[strCookieName].Expires = DateTime.Now.AddDays(-1);
            }
        }
    }
}
