﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;

namespace GRCBase
{
    public class WebControlsHelper
    {
        public static void SelectItemInDropDownList(System.Web.UI.WebControls.DropDownList list, string val)
        {
            list.SelectedIndex = -1;
            if (list.Items.FindByValue(val) != null)
            {
                list.Items.FindByValue(val).Selected = true;
            }
        }

        public static void SelectItemsInCheckBoxList(System.Web.UI.WebControls.CheckBoxList list, string val)
        {
            if (list.Items.FindByValue(val) != null)
            {
                list.Items.FindByValue(val).Selected = true;
            }
        }

        public static void SelectMultipleItemsInCheckBoxList(System.Web.UI.WebControls.CheckBoxList list, List<int> selectedValues)
        {
            for (int i = 0; i < list.Items.Count; i++)
            {
                list.Items[i].Selected = false;
            }

            foreach (int iVal in selectedValues)
            {
                for (int i = 0; i < list.Items.Count; i++)
                {
                    if (list.Items[i].Value == iVal.ToString())
                    {
                        list.Items[i].Selected = true;
                        break;
                    }
                }
            }
        }

        public static void SelectMultipleItemsInCheckBoxList(System.Web.UI.WebControls.CheckBoxList list, List<string> selectedValues)
        {
            for (int i = 0; i < list.Items.Count; i++)
            {
                list.Items[i].Selected = false;
            }

            foreach (string strVal in selectedValues)
            {
                for (int i = 0; i < list.Items.Count; i++)
                {
                    if (list.Items[i].Value == strVal)
                    {
                        list.Items[i].Selected = true;
                        break;
                    }
                }
            }
        }

        public static void SelectMultipleItemsInCheckBoxList(System.Web.UI.WebControls.CheckBoxList list, string[] selectedValues)
        {
            for (int i = 0; i < list.Items.Count; i++)
            {
                list.Items[i].Selected = false;
            }

            foreach (string strVal in selectedValues)
            {
                for (int i = 0; i < list.Items.Count; i++)
                {
                    if (list.Items[i].Value == strVal)
                    {
                        list.Items[i].Selected = true;
                        break;
                    }
                }
            }
        }


        public static List<string> GetMultipleItemsInCheckBoxList(System.Web.UI.WebControls.CheckBoxList list)
        {
            List<string> objList = new List<string>();

            for (int i = 0; i < list.Items.Count; i++)
            {
                if (list.Items[i].Selected)
                {
                    objList.Add(list.Items[i].Value);
                }
            }

            return objList;
        }

        public static void SelectItemInRadioButtonList(System.Web.UI.WebControls.RadioButtonList list, string val)
        {
            if (val == null)
            {
                list.SelectedIndex = -1;
                return;
            }

            for (int i = 0; i < list.Items.Count; i++)
            {
                if (list.Items[i].Value == val)
                {
                    list.SelectedIndex = i;
                    return;
                }
            }

            return;
        }

        public static void SelectItemInRadioButtonList2(System.Web.UI.WebControls.RadioButtonList list, string val)
        {
            if (list.Items.FindByValue(val) != null)
            list.Items.FindByValue(val).Selected = true;
        }

        public static bool SelectItemsInASPListBox(System.Web.UI.WebControls.ListBox list, string val)
        {
            if (val == null)
            {
                list.SelectedIndex = -1;
                return false;
            }


            for (int i = 0; i < list.Items.Count; i++)
            {
                if (list.Items[i].Value.ToLower() == val.ToLower())
                {
                    list.SelectedIndex = i;
                    return true;
                }
            }

            return false;
        }

        public static void SelectItemsInListBox(System.Web.UI.WebControls.RadioButtonList list, string val)
        {
            
            if (list.Items.FindByValue(val) != null)
            {
                list.Items.FindByValue(val).Selected = true;
            }
        }

    }
}
