﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Reflection;
using System.Text.RegularExpressions;

namespace GRCBase
{
    public enum DefinedErrorID
    {
        CompileErrors = -1,
        FileNotFoundError = -2,
        ErrorOnErrorLog = -3,
    }

    public struct ErrorLogEmailSettings
    {
        public string FromName;
        public string FromEmail;
        public string Subject;
        public string MailTemplate;
        public string ToEmail;
        public string ToName;
    }

    public class ErrorLogHelper
    {
        public static string GetErrorCommonHeading()
        {
            StringBuilder strErrorHeading = new StringBuilder();
            strErrorHeading.Append("An error occurred while processing your request.\r\n");
            if (LibrarySettings.ErrorLogSettings.IsErrorLogEnabled)
            {
                strErrorHeading.Append("The error has been logged.");
            }
            strErrorHeading.Append(" If you continue to experience this error, please contact technical support.\r\n\r\n");
            return strErrorHeading.ToString();
        }

        public static int LogError_Application(Exception e)
        {
            int errorID = LogErrorToDatabase(e, "Application");
            if (HttpContext.Current != null && errorID > 0)
            {
                HttpContext.Current.ClearError();
            }
            return errorID;
        }

        public static int LogAndIgnoreException(Exception e)
        {
            return LogErrorToDatabase(e, "Ignored");
        }

        public static int LogError_Ajax(Exception e)
        {
            int errorID = LogErrorToDatabase(e, "Application");
            if (HttpContext.Current != null)
            {
                HttpContext.Current.ClearError();
            }
            return errorID;
        }

        public static int LogError_Anthem(Exception e)
        {
            int errorID = LogErrorToDatabase(e, "Anthem");
            if (HttpContext.Current != null)
            {
                HttpContext.Current.ClearError();
            }
            return errorID;
        }

        public static int LogError_ComponentArt(Exception e)
        {
            return LogErrorToDatabase(e, "Component Art");
        }

        private static string _GetMailingListForErrorModule(string errorModule)
        {
            if (errorModule.ToLower().CompareTo("billing") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportBillingEmailSetting.ToEmail;
            }
            else if (errorModule.ToLower().CompareTo("ajax") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportAjaxEmailSetting.ToEmail;
            }
            else if (errorModule.ToLower().CompareTo("anthem") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportAnthemEmailSetting.ToEmail;
            }
            else if (errorModule.ToLower().CompareTo("component art") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportComponentArtEmailSetting.ToEmail;
            }
            else if (errorModule.ToLower().CompareTo("application") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportApplicationEmailSetting.ToEmail;
            }
            else if (errorModule.ToLower().CompareTo("javascript") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportJavascriptEmailSetting.ToEmail;
            }
            return LibrarySettings.ErrorLogSettings.TechSupportDefaultEmailSetting.ToEmail;
        }

        private static string _GetMailingFromEmailForErrorModule(string errorModule)
        {
            if (errorModule.ToLower().CompareTo("billing") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportBillingEmailSetting.FromEmail;
            }
            else if (errorModule.ToLower().CompareTo("ajax") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportAjaxEmailSetting.FromEmail;
            }
            else if (errorModule.ToLower().CompareTo("anthem") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportAnthemEmailSetting.FromEmail;
            }
            else if (errorModule.ToLower().CompareTo("component art") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportComponentArtEmailSetting.FromEmail;
            }
            else if (errorModule.ToLower().CompareTo("application") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportApplicationEmailSetting.FromEmail;
            }
            else if (errorModule.ToLower().CompareTo("javascript") == 0)
            {
                return LibrarySettings.ErrorLogSettings.TechSupportJavascriptEmailSetting.FromEmail;
            }

            return LibrarySettings.ErrorLogSettings.TechSupportDefaultEmailSetting.FromEmail;
        }

        public static void _sendErrorEmail(int errorID, string errorSiteName, string errorStr, string errorModule)
        {
            string EmailFrom = _GetMailingFromEmailForErrorModule(errorStr);
            string emailTo = _GetMailingListForErrorModule(errorModule);

            if (emailTo == null || emailTo.Length == 0)
            {
                return;
            }

            string strSubject = "Error information - " + "[" + errorID + "] [" + errorModule + "] " + errorSiteName;

            foreach (string strEmail in emailTo.Split(",".ToCharArray()))
            {
                EmailHelper.SendMail(string.Empty, string.Empty, EmailFrom, strEmail, strSubject, errorStr, false);
            }
        }

        public static void LogError_Javascript(string errorTitle, string errorMsg, string errorLocation)
        {
            HttpContext Context = System.Web.HttpContext.Current;
            bool bEnable = LibrarySettings.ErrorLogSettings.IsErrorLogEmailEnabled;

            string errorSiteName = Context.Request.ServerVariables["SERVER_NAME"];

            string errorOccurredIn = UrlUtility.FullCurrentPageUrlWithQV;

            if (bEnable)
            {
                int errorID = 0;
                string errorStr = "";

                try
                {
                    errorStr = _constructEmailForJavascriptError(errorTitle, errorMsg, errorLocation);
                    string errorTemp = errorStr;
                    errorStr = System.Text.RegularExpressions.Regex.Replace(errorStr, "##ERROR_ID##", string.Empty);
                    errorID = _logErrorToDatabase(errorStr, errorMsg);
                    errorStr = errorTemp;
                    errorTemp = string.Empty;
                    if (errorID > 0)
                    {
                        errorStr = Regex.Replace(errorStr, "##ERROR_ID##", "Error ID:" + errorID.ToString() + "\r\n\r\n");
                    }
                    else
                    {
                        errorStr = Regex.Replace(errorStr, "##ERROR_ID##", string.Empty);
                    }

                    _sendErrorEmail(errorID, errorSiteName, errorStr, "Javascript");
                }
                catch (System.Exception ex)
                {
                }
            }
        }

        public static int LogErrorToDatabase(Exception e, string errorModule)
        {

            string errorSiteName = string.Empty;

            if (System.Web.HttpContext.Current != null)
                errorSiteName = UrlUtility.ServerName;
            else
                errorSiteName = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;

            int errorID = 0;

            //the errorpageurl_filenotfound

            Exception eOriginatingException = _getOriginatingException(e);

            //file not found error
            if (eOriginatingException.GetType().ToString() == "System.IO.FileNotFoundException" || eOriginatingException.TargetSite.ToString() == "Void CheckVirtualFileExists(System.Web.VirtualPath)")
            {
                return Convert.ToInt32(DefinedErrorID.FileNotFoundError);
            }

            //compile errors..

            if (LibrarySettings.ErrorLogSettings.IgnoreCompilerErrors)
            {
                if (eOriginatingException.GetType().ToString() == "System.Web.HttpCompileException")
                {
                    return Convert.ToInt32(DefinedErrorID.CompileErrors);
                }
            }

            //5 secs delay
            System.Threading.Thread.Sleep(new System.TimeSpan(0, 0, 0, 1, 0));

            string errorStr = "";


            try
            {
                errorStr = _ConstructErrorLogEmailWeb(e);
                string errorTemp = errorStr;
                errorStr = Regex.Replace(errorStr, "##ERROR_ID##", string.Empty);

                string errorMessage = e.InnerException != null ? e.InnerException.Message : e.Message;

                errorID = _logErrorToDatabase(errorStr, errorMessage);
                errorStr = errorTemp;
                errorTemp = string.Empty;
                if (errorID > 0)
                {
                    errorStr = Regex.Replace(errorStr, "##ERROR_ID##", "Error ID:" + errorID.ToString() + "\r\n\r\n");
                }
                else
                {
                    errorStr = Regex.Replace(errorStr, "##ERROR_ID##", string.Empty);
                }

                if (LibrarySettings.ErrorLogSettings.IsErrorLogEmailEnabled)
                {
                    _sendErrorEmail(errorID, errorSiteName, errorStr, errorModule);
                }
                return errorID;
            }
            catch (System.Exception ex)
            {
                return Convert.ToInt32(DefinedErrorID.ErrorOnErrorLog);
            }
            return errorID;
        }

        private static string _constructEmailForJavascriptError(string errorTitle, string errorMsg, string errorLocation)
        {
            HttpContext Context = HttpContext.Current;
            string errorSiteName = Context.Request.ServerVariables["SERVER_NAME"];
            string errorOccurredIn = Context.Request.ServerVariables["HTTP_VTI_SCRIPT_NAME"];
            if (errorOccurredIn == null)
            {
                errorOccurredIn = Context.Request.ServerVariables["URL"];
            }

            StringBuilder strEmail = new StringBuilder();
            strEmail.Append("The following Error occurred on the " + errorSiteName + " site\r\n\r\n");
            strEmail.Append("##ERROR_ID##");
            strEmail.Append("Client IP Address: " + Context.Request.UserHostAddress + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Time: " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Request Type: " + Context.Request.RequestType.ToString() + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Url: " + errorOccurredIn + "\r\n");
            strEmail.Append("\r\n\r\n");

            string httpreferrer = Context.Request.ServerVariables["HTTP_REFERER"];
            if (httpreferrer == null)
            {
                httpreferrer = "URL could not be retrieved as the Page was redirected, URL can be retrieved only if the current page is accessed using an <a> tag from the previous page.";
            }

            strEmail.Append("URL of the page that referred the request to the current page: \r\n" + httpreferrer + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Client Browser Details:\r\n");
            strEmail.Append("======================\r\n");
            strEmail.Append("User Agent : " + Context.Request.UserAgent + "\r\n");
            strEmail.Append("Browser : " + Context.Request.Browser.Browser + "\r\n");
            strEmail.Append("Version : " + Context.Request.Browser.Version + "\r\n");
            strEmail.Append("Major: " + Context.Request.Browser.MajorVersion + "\r\n");
            strEmail.Append("Minor: " + Context.Request.Browser.MinorVersion + "\r\n");
            strEmail.Append("Javascript Support: " + Context.Request.Browser.JavaScript + "\r\n");
            strEmail.Append("EcmaScriptVersion(A Major Version value greater than or equal to one implies javascript support): " + Context.Request.Browser.EcmaScriptVersion + "\r\n");
            strEmail.Append("Platform: " + Context.Request.Browser.Platform);
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Form Variables:\r\n");
            strEmail.Append("==============\r\n");

            for (int i = 0; i < Context.Request.Form.AllKeys.Length; i++)
            {
                if (Context.Request.Form.AllKeys[i] != null)
                {
                    if ("__VIEWSTATE" != Context.Request.Form.AllKeys[i].ToString())
                    {
                        if (Context.Request.Form[Context.Request.Form.AllKeys[i].ToString()] != null)
                        {
                            strEmail.Append(Context.Request.Form.AllKeys[i].ToString() + "=" + Context.Request.Form[Context.Request.Form.AllKeys[i].ToString()]);
                            strEmail.Append("\r\n");
                        }
                    }
                }
            }

            strEmail.Append("\r\n\r\n");
            strEmail.Append("Session Variables:\r\n");
            strEmail.Append("==============\r\n");

            if (Context.Session != null)
            {
                for (int i = 0; i < Context.Session.Keys.Count; i++)
                {
                    if (Context.Session[Context.Session.Keys[i].ToString()] != null)
                    {
                        strEmail.Append(Context.Session.Keys[i].ToString() + "=" + Context.Session[Context.Session.Keys[i].ToString()].ToString());
                        strEmail.Append("\r\n");
                    }
                }
            }

            strEmail.Append("\r\n\r\n");
            strEmail.Append("Exception Details:" + "\r\n");
            strEmail.Append("=================\r\n");
            strEmail.Append("Error:" + errorTitle + "\r\n\r\n");
            strEmail.Append("Message:" + errorMsg + "\r\n\r\n");
            strEmail.Append("Stack Trace\r\n");
            strEmail.Append("===========\r\n");
            strEmail.Append(errorLocation);
            strEmail.Append("\r\n\r\n");
            return strEmail.ToString();
        }

        private static string _ConstructErrorLogEmailWeb(Exception e)
        {
            HttpContext Context = HttpContext.Current;
            string errorSiteName = Context.Request.ServerVariables["SERVER_NAME"];
            string errorOccurredIn = Context.Request.ServerVariables["HTTP_VTI_SCRIPT_NAME"];
            if (errorOccurredIn == null)
            {
                errorOccurredIn = UrlUtility.FullCurrentPageUrlWithQV;
            }

            StringBuilder strEmail = new StringBuilder();
            strEmail.Append("The following Error occurred on the " + errorSiteName + " site\r\n\r\n");
            strEmail.Append("##ERROR_ID##");
            strEmail.Append("Originating Exception Message: " + _getOriginatingException(e).Message + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Client IP Address: " + Context.Request.UserHostAddress + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Server Host name: " + Context.Server.MachineName.ToString() + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Time: " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Request Type: " + Context.Request.RequestType.ToString() + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Url: " + errorOccurredIn + "\r\n");
            strEmail.Append("\r\n\r\n");

            string httpreferrer = Context.Request.ServerVariables["HTTP_REFERER"];
            if (httpreferrer == null)
            {
                httpreferrer = "URL could not be retrieved as the Page was redirected, URL can be retrieved only if the current page is accessed using an <a> tag from the previous page.";
            }

            strEmail.Append("URL of the page that referred the request to the current page: \r\n" + httpreferrer + "\r\n");
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Client Browser Details:\r\n");
            strEmail.Append("======================\r\n");
            strEmail.Append("User Agent : " + Context.Request.UserAgent + "\r\n");
            strEmail.Append("Browser : " + Context.Request.Browser.Browser + "\r\n");
            strEmail.Append("Version : " + Context.Request.Browser.Version + "\r\n");
            strEmail.Append("Major: " + Context.Request.Browser.MajorVersion + "\r\n");
            strEmail.Append("Minor: " + Context.Request.Browser.MinorVersion + "\r\n");
            strEmail.Append("Javascript Support: " + Context.Request.Browser.JavaScript + "\r\n");
            strEmail.Append("EcmaScriptVersion(A Major Version value greater than or equal to one implies javascript support): " + Context.Request.Browser.EcmaScriptVersion + "\r\n");
            strEmail.Append("Platform: " + Context.Request.Browser.Platform);
            strEmail.Append("\r\n\r\n");
            strEmail.Append("Form Variables:\r\n");
            strEmail.Append("==============\r\n");

            for (int i = 0; i < Context.Request.Form.AllKeys.Length; i++)
            {
                if (Context.Request.Form.AllKeys[i] != null)
                {
                    if ("__VIEWSTATE" != Context.Request.Form.AllKeys[i].ToString())
                    {
                        if (Context.Request.Form[Context.Request.Form.AllKeys[i].ToString()] != null)
                        {
                            strEmail.Append(Context.Request.Form.AllKeys[i].ToString() + "=" + Context.Request.Form[Context.Request.Form.AllKeys[i].ToString()]);
                            strEmail.Append("\r\n");
                        }
                    }
                }
            }

            strEmail.Append("\r\n\r\n");

            //Add Request Length, cookies and identity of thread also to log.. (BY CKR Sep 23 2007)
            try
            {

                strEmail.Append("User Identity:");
                if (Context.Request.LogonUserIdentity != null)
                {
                    strEmail.Append(Context.Request.LogonUserIdentity.Name);
                }
                else
                {
                    strEmail.Append("Context.Request.LogonUserIdentity is null");
                }

                strEmail.Append("\r\n\r\n");
                strEmail.Append("Request Length:");
                if (Context.Request.InputStream != null)
                {
                    strEmail.Append(Context.Request.InputStream.Length);
                }
                else
                {
                    strEmail.Append("Context.Request.InputStream is null");
                }

                strEmail.Append("\r\n\r\n");
                strEmail.Append("Cookies:\r\n");
                strEmail.Append("==============\r\n");

                if (Context.Request.Cookies != null)
                {
                    for (int i = 0; i < Context.Request.Cookies.Count; i++)
                    {
                        if (Context.Request.Cookies[i] != null)
                        {
                            strEmail.Append(Context.Request.Cookies[i].Name + "=" + Context.Request.Cookies[i].Value);
                            strEmail.Append("\r\n");
                        }
                    }
                }

                strEmail.Append("\r\n\r\n");

            }
            catch (System.Exception exinner)
            {
                strEmail.Append("Following error occurred while attempting to determine one of - RequestLength, UserIdentity, Cookies[[");
                strEmail.Append(exinner.Message + exinner.StackTrace);
                strEmail.Append("]]");

            }
            //End Added by CKR Spet 23 2007

            strEmail.Append("Session Variables:\r\n");
            strEmail.Append("==============\r\n");

            if (Context.Session != null)
            {
                for (int i = 0; i < Context.Session.Keys.Count; i++)
                {
                    if (Context.Session[Context.Session.Keys[i]] != null)
                    {
                        strEmail.Append(Context.Session.Keys[i].ToString() + "=" + Context.Session[Context.Session.Keys[i].ToString()].ToString());
                        strEmail.Append("\r\n");
                    }
                }
            }

            strEmail.Append("\r\n\r\n");
            strEmail.Append(GetExceptionString(e));
            strEmail.Append("\r\n\r\n");
            return strEmail.ToString();
        }

        static public string GetExceptionString(System.Exception e)
        {
            StringBuilder str = new StringBuilder();
            str.Append("Exception Details:" + "\r\n");
            str.Append("=================\r\n");
            str.Append("Message:" + e.Message + "\r\n\r\n");
            str.Append("Stack Trace\r\n");
            str.Append("===========\r\n");
            str.Append(_getExceptionStringHelper(e, 0));
            return str.ToString();
        }

        static public System.Exception _getOriginatingException(System.Exception e)
        {
            while (e.InnerException != null)
            {
                e = e.InnerException;
            }
            return e;
        }

        static private string _getExceptionStringHelper(System.Exception e, int depth)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (e.InnerException != null)
            {
                sb.Append("\r\nException:\r\n");
                sb.Append("---------------\r\n");
                sb.Append("Type:\r\n");
                sb.Append(e.GetType().ToString());
                sb.Append("\r\n");
            }
            else
            {
                sb.Append("\r\n:::::::Originating Exception:::::::\r\n");
                sb.Append("-------------------------------\r\n");
                sb.Append("Type:\r\n");
                sb.Append(e.GetType().ToString());
                sb.Append("\r\n");
            }

            sb.Append("Message:\r\n");
            sb.Append(e.Message);
            sb.Append("\r\n");
            sb.Append("Stack Trace:");
            sb.Append("\r\n");
            sb.Append(e.StackTrace);
            sb.Append("\r\n");
            if (e.InnerException != null)
            {
                sb.Append(_getExceptionStringHelper(e.InnerException, depth + 1));
                sb.Append("\r\n");
            }
            return sb.ToString();
        }

        static private int _logErrorToDatabase(string errorString, string errorMessage)
        {
            SqlDataReader reader = null;
            //if (errorString.Length > 14000)
            //{
            //    errorString = errorString.Substring(0, 13999);
            //    errorString += " [TRUNCATED FOR LOGGING]";
            //}

            string strSql = "Insert into SiteErrorLog(ErrorText,ErrorMessage) values ('" + errorString.Replace("'", "''") + "','" + errorMessage.Replace("'", "''") + "')";
            int errorID = 0;
            SqlConnection conn = new SqlConnection(LibrarySettings.DatabaseConnectionString);
            conn.Open();
            SqlCommand objCMD = new SqlCommand(strSql, conn);
            try
            {
                objCMD.ExecuteNonQuery();
                objCMD.CommandText = "select @@IDENTITY";
                reader = objCMD.ExecuteReader();
                reader.Read();
                errorID = Convert.ToInt32(reader[0].ToString());
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                objCMD.Dispose();
                conn.Close();
                objCMD = null;
                conn = null;
            }

            return errorID;
        }

        static public string GetErrorLogFromDatabase(int errorID)
        {
            SqlDataReader reader = null;
            try
            {
                reader = _DoSql("select * from SiteErrorLog where errorID=" + errorID);
                if (reader.Read())
                {
                    return reader["ErrorText"].ToString();
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return "";
        }

        static public string GetErrorMessageFromDatabase(int errorID)
        {
            SqlDataReader reader = null;

            try
            {
                reader = _DoSql("select * from SiteErrorLog where errorID=" + errorID);
                if (reader.Read())
                {
                    if (reader["ErrorMessage"] != DBNull.Value)
                    {
                        return reader["ErrorMessage"].ToString();
                    }
                    return string.Empty;
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return "";
        }


        //Intended to be used only for error logging
        public static SqlDataReader _DoSql(string sql)
        {
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            try
            {
                SqlConnection objConn = new SqlConnection(LibrarySettings.DatabaseConnectionString);
                objConn.Open();
                cmd = new SqlCommand(sql, objConn);
                reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }

                //DO NOT CLOSE CONNECTION HERE!!!
            }
            return reader;
        }
    }
}
