﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace GRCBase
{
    public class IOUtility
    {
        /// <summary>
        ///  Returns the string contained in a file
        /// </summary>
        /// <param name="FilePath">Path of file</param>
        /// <returns>string</returns>
        public static string GetStringFromFile(string FilePath)
        {
            byte[] array = new Byte[1024];
            System.Text.StringBuilder full_string;
            FileStream fs = null;
            try
            {
                fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

                full_string = new System.Text.StringBuilder();

                int read = fs.Read(array, 0, 1024);

                while (read > 0)
                {
                    string str = System.Text.ASCIIEncoding.ASCII.GetString(array, 0, read);
                    full_string.Append(str);
                    read = fs.Read(array, 0, 1024);
                }
            }
            catch (IOException ex)
            {
                throw new Exception("IOUtility : Error ", ex);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
            array = null;
            return full_string.ToString();
        }

        public static string ReadHtmlFile(string FilePath)
        {
            FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sReader = new StreamReader(fs);
            string full_string = sReader.ReadToEnd();
            return full_string;
        }
    }
}
