﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GRCBase
{
    /// <summary>
    /// Current known Credit Card Types
    /// </summary>
    public enum CreditCardType
    {
        Unknown, Visa, DinersClub, AmericanExpress, MasterCard, Solo, CarteBanc, CarteBlueue, Laser, Discover, JCB, Switch, Dankort, BankCard
    };

    /// <summary> 
    /// Class to represent the result of a credit card validation 
    /// </summary> 
    public struct CcValidationResult
    {
        #region Member Variables
        public readonly bool IsValid;
        public readonly string Message;
        #endregion

        #region Public Methods
        public CcValidationResult(bool isValid, string message)
        {
            this.IsValid = isValid; this.Message = message;
        }

        public override string ToString()
        {
            if (this.IsValid)
                return "Valid";
            else
                return "Invalid: " + this.Message;
        }
        #endregion
    }

    /// <summary>
    /// Static Class to Validate Credit Card Numbers
    /// </summary>
    public sealed class CreditCardValidator
    {
        #region Ctor
        private CreditCardValidator() { } // static only
        #endregion

        #region Private Functions
        /// <summary>
        /// Validates a card number based on Luhn's Algorithm
        /// </summary>
        /// <param name="cardNumber">the Credit Card number to check</param>
        /// <returns>valid or not</returns>
        /// <remarks>
        /// Luhn's Formula ( 4 Steps)
        /// 1. Double the value of alternating digits starting from the penultimate number until the end
        /// 2. Add the separate digits of the products from the previous step 
        /// 3. Add the unaffected digits 
        /// 4. Add the two results and divide by 10 
        ///     (Result from Step 2 + Result from Step 3) mod 10 = 0 = valid 
        /// </remarks>
        private static bool ExecuteLuhnsAlgorithm(string cardNumber)
        {
            int length = cardNumber.Length;

            if (length < 13)
                return false;
            int sum = 0;
            int offset = length % 2;
            byte[] digits = new System.Text.ASCIIEncoding().GetBytes(cardNumber);

            for (int i = 0; i < length; i++)
            {
                digits[i] -= 48;
                if (((i + offset) % 2) == 0)
                    digits[i] *= 2;

                sum += (digits[i] > 9) ? digits[i] - 9 : digits[i];
            }
            return ((sum % 10) == 0);
        }
        #endregion

        #region Public static Methods
        /// <summary>
        /// Validates a given card number based on CardType specified
        /// </summary>
        /// <param name="cardType">Credit Card Type</param>
        /// <param name="cardNumber">Card Number</param>
        /// <returns>valid or not</returns>
        public static CcValidationResult Validate(string cardNumber, int expiryMonth, int expiryYear, CreditCardType cardType)
        {
            // Check for Non-digits
            int len = cardNumber.Length;
            for (int i = 0; i < cardNumber.Length; i++)
            {
                if (!char.IsDigit(cardNumber, i))
                {
                    return new CcValidationResult(false, "Invalid credit card number non-digits not allowed");
                }
            }

            // Validate based on card type, first if tests length, second tests prefix
            switch (cardType)
            {
                case CreditCardType.MasterCard:
                    if (len != 16)
                        return new CcValidationResult(false, "Invalid credit card number");
                    if (cardNumber[0] != '5' || cardNumber[1] == '0' || Convert.ToInt32(cardNumber[1].ToString()) > 5)
                        return new CcValidationResult(false, "Card type does not match card number");
                    break;

                case CreditCardType.BankCard:
                    if (len != 16)
                        return new CcValidationResult(false, "Invalid credit card number");
                    if (cardNumber[0] != '5' || cardNumber[1] != '6' || cardNumber[2] > '1')
                        return new CcValidationResult(false, "Card type does not match card number");
                    break;

                case CreditCardType.Visa:
                    if (len != 16 && len != 13)
                        return new CcValidationResult(false, "Invalid credit card number");
                    if (cardNumber[0] != '4')
                        return new CcValidationResult(false, "Card type does not match card number");
                    break;

                case CreditCardType.AmericanExpress:
                    if (len != 15)
                        return new CcValidationResult(false, "Invalid credit card number");
                    if (cardNumber[0] != '3' || (cardNumber[1] != '4' && cardNumber[1] != '7'))
                        return new CcValidationResult(false, "Card type does not match card number");
                    break;

                case CreditCardType.Discover:
                    if (len != 16)
                        return new CcValidationResult(false, "Invalid credit card number");
                    if (cardNumber[0] != '6' || cardNumber[1] != '0' || cardNumber[2] != '1' || cardNumber[3] != '1')
                        return new CcValidationResult(false, "Card type does not match card number");
                    break;

                case CreditCardType.DinersClub:
                    if (len != 14)
                        return new CcValidationResult(false, "Invalid credit card number");
                    if (cardNumber[0] != '3' || (cardNumber[1] != '0' && cardNumber[1] != '6' && cardNumber[1] != '8')
                       || cardNumber[1] == '0' && cardNumber[2] > '5')
                        return new CcValidationResult(false, "Card type does not match card number");
                    break;

                case CreditCardType.JCB:
                    if (len != 16 && len != 15)
                        return new CcValidationResult(false, "Invalid credit card number");
                    if (cardNumber[0] != '3' || cardNumber[1] != '5')
                        return new CcValidationResult(false, "Card type does not match card number");
                    break;
            }

            DateTime expirationDate = new DateTime(expiryYear, expiryMonth, 1).AddMonths(1);
            // Check Expiration Date
            if (expirationDate < DateTime.Today.Date)
            {
                return new CcValidationResult(false, "Card has expired");
            }

            // Use Luhn Algporithm to validate
            if (ExecuteLuhnsAlgorithm(cardNumber))
                return new CcValidationResult(true, String.Empty);

            return new CcValidationResult(false, "Invalid Credit Card Number");
        }
        #endregion
    }
}