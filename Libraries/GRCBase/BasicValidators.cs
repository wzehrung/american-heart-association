﻿using System;
using System.Text.RegularExpressions;

namespace GRCBase
{
    public class BasicValidators
    {
        public static bool IsValidDateTime(DateTime dt)
        {
            try
            {
                Convert.ToDateTime(dt);
                if (dt.Day == 1 && dt.Month == 1 && dt.Year == 0001)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool IsValidDateTime(object objdt)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(objdt);
                if (dt.Day == 1 && dt.Month == 1 && dt.Year == 0001)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool IsUrl(string Url)
        {
            //string strRegex = "^(https?://)"
            //+ "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //user@
            //+ @"(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP- 199.194.52.184
            //+ "|" // allows either IP or domain
            //+ @"([0-9a-z_!~*'()-]+\.)*" // tertiary domain(s)- www.
            //+ @"([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // second level domain
            //+ "[a-z]{2,6})" // first level domain- .com or .museum
            //+ "(:[0-9]{1,4})?" // port number- :80
            //+ "((/?)|" // a slash isn't required if there is no file name
            //+ "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";

            string strRegex = @"^((ht|f)tp(s?)\:\/\/|~/|/)?([\w]+:\w+@)?([a-zA-Z]{1}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?((/?\w+/)+|/?)(\w+\.[\w]{3,4})?((\?\w+(=\w+)?)(&\w+(=\w+)?)*)?";


            Regex re = new Regex(strRegex);

            if (re.IsMatch(Url))
                return (true);
            else
                return (false);
        }
    }
}
