﻿using System;

namespace GRCBase
{
    public class CurrencyHelper
    {
        public static double RemoveCommaFromCurrencyAmount(string dblValue)
        {
            string strAmount = dblValue.Replace(",", string.Empty);
            return Convert.ToDouble(strAmount);
        }

        public static string GetFormattedCurrencyString(string floatval)
        {
            if (floatval.Length == 0)
                return string.Empty;

            double val = 0;
            try
            {
                val = Convert.ToDouble(floatval);
            }
            catch (System.Exception e)
            {
                return string.Empty;
            }
            string format = "#,##0.00;-#,##0.00;0.00";
            return val.ToString(format);
        }
    }
}
