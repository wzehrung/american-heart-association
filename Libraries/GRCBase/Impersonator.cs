﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security;
using System.Security.Permissions;

namespace GRCBase
{
    public class Impersonator
    {
        // private members for holding domain user account credentials
        private string username = String.Empty;
        private string password = String.Empty;
        private string domain = String.Empty;
        // this will hold the security context for reverting back to the client after impersonation operations are complete
        private WindowsImpersonationContext impersonationContext = null;
        private WindowsIdentity windowsIdentity = null;

        public static bool ValidateUserCredentials(string email, string password)
        {
            IntPtr handle = new IntPtr(0);
            handle = IntPtr.Zero;

            const int LOGON32_LOGON_NETWORK = 3;
            const int LOGON32_PROVIDER_DEFAULT = 0;

            // attempt to authenticate domain user account
            bool logonSucceeded = LogonUser(email, null, password, LOGON32_LOGON_NETWORK, LOGON32_PROVIDER_DEFAULT, ref handle);

            if (!logonSucceeded)
            {
                // if the logon failed, get the error code and throw an exception
                int errorCode = Marshal.GetLastWin32Error();
                throw new Exception("User logon failed. Error Number: " + errorCode);
            }
            CloseHandle(handle);
            return true;
        }

        public static Impersonator BeginImpersonate()
        {
            Impersonator objImpersonator = null;
            string userName = LibrarySettings.ImpersonationSettings.UserName;
            string domain = LibrarySettings.ImpersonationSettings.Domain;
            string password = LibrarySettings.ImpersonationSettings.Password;
            objImpersonator = new Impersonator(userName, domain, password);
            objImpersonator.Impersonate();
            return objImpersonator;
        }

        public static void EndImpersonate(Impersonator objImpersonator)
        {
            if (objImpersonator != null)
            {
                objImpersonator.Undo();
            }

        }

        public Impersonator(string username, string domain, string password)
        {
            // set the properties used for domain user account
            this.username = username;
            this.domain = domain;
            this.password = password;
        }

        private WindowsIdentity Logon()
        {
            IntPtr handle = new IntPtr(0);
            handle = IntPtr.Zero;
            const int LOGON32_LOGON_NETWORK = 3;
            const int LOGON32_PROVIDER_DEFAULT = 0;
            // attempt to authenticate domain user account
            bool logonSucceeded = LogonUser(this.username, this.domain, this.password, LOGON32_LOGON_NETWORK, LOGON32_PROVIDER_DEFAULT, ref handle);
            if (!logonSucceeded)
            {
                // if the logon failed, get the error code and throw an exception
                int errorCode = Marshal.GetLastWin32Error();
                throw new Exception("User logon failed. Error Number: " + errorCode);
            }

            // if logon succeeds, create a WindowsIdentity instance
            WindowsIdentity winIdentity = new WindowsIdentity(handle);
            // close the open handle to the authenticated account
            CloseHandle(handle);
            return winIdentity;
        }

        public void Impersonate()
        {
            // authenticates the domain user account and begins impersonating it
            this.windowsIdentity = this.Logon();
            this.impersonationContext = windowsIdentity.Impersonate();
        }

        public void Undo()
        {
            // rever back to original security context which was store in the WindowsImpersonationContext instance
            if (impersonationContext != null)
            {
                this.windowsIdentity.Dispose();
                this.impersonationContext.Undo();
                this.impersonationContext.Dispose();
            }
            else
            {
                throw new System.Exception("User is not being impersonated");
            }
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool LogonUser(string lpszUsername,
            string lpszDomain,
            string lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            ref IntPtr phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern bool CloseHandle(IntPtr handle);
    }
}
