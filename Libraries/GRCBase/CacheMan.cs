﻿using System;
using System.Web;
using System.Web.Caching;

namespace GRCBase
{
    /// <summary>
    /// Custom Cache Item
    /// </summary>
    class GRCCacheItem
    {
        public GRCCacheItem(string key, string versionStamp, object val)
        {
            this.Key = key;
            this.value = val;
            this.versionStamp = versionStamp;
        }
        public string Key;
        public object value;
        public string versionStamp;
    }

    /// <summary>
    /// Cache Manager for Versioning Cache objects
    /// </summary>
    public class CacheManager
    {
        private static string customKeyPrefix = "GRC_BASE_CACHE_ENTRY_";

        private static CacheItemRemovedCallback OnRemove = null;

        static CacheManager()
        {
            OnRemove += new CacheItemRemovedCallback(CacheDumped);
        }

        private static void CacheDumped(string k, object v, CacheItemRemovedReason r)
        {
            if ((r == CacheItemRemovedReason.Removed) ||
              (r == CacheItemRemovedReason.Underused))
            {
                System.Diagnostics.Debug.WriteLine("Removed from cache: " + k);
                return;
            }
        }

        private static System.Web.Caching.Cache _cache
        {
            get
            {
                return HttpRuntime.Cache;
            }
        }

        public static object GetItemFromCache(string key)
        {
            return GetItemFromCache(key, null, false);
        }


        /// <summary>
        /// Retrives Item from cache based on key and version stamp
        /// </summary>
        /// <param name="key"></param>
        /// <param name="versionStamp"></param>
        /// <param name="deleteFromCacheIfDifferentVersionExist"></param>
        /// <returns></returns>
        public static object GetItemFromCache(string key, string versionStamp, bool deleteFromCacheIfDifferentVersionExist)
        {
            GRCCacheItem itemInCache = (GRCCacheItem)_cache[customKeyPrefix + key];
            if (itemInCache == null)
            {
                System.Diagnostics.Debug.WriteLine("No Item cache: " + customKeyPrefix + key);
                return null;
            }

            if (itemInCache.versionStamp == versionStamp)
            {
                return itemInCache.value;
            }

            if (deleteFromCacheIfDifferentVersionExist)
            {
                _cache.Remove(customKeyPrefix + key);
            }
            return null;
        }


        public static void AddItemToCache(string key, object value)
        {
            AddItemToCache(key, null, value);
        }

        /// <summary>
        /// Adds an Item into Cache with key and version stamp
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="versionStamp">Version Stamp</param>
        /// <param name="value">Object or value</param>
        public static void AddItemToCache(string key, string versionStamp, object value)
        {
            AddItemToCache(key, versionStamp, value, LibrarySettings.CacheTimeOut);
        }

        /// <summary>
        /// Adds an item into cache with Sliding Expiration period
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="versionStamp">version Stamp</param>
        /// <param name="value">Object or value to cache</param>
        /// <param name="SlidingExpiration">With Sliding Expiration</param>
        public static void AddItemToCache(string key, string versionStamp, object value, bool SlidingExpiration)
        {
            if (SlidingExpiration == false)
            {
                AddItemToCache(key, versionStamp, value, LibrarySettings.CacheTimeOut);
                return;
            }
            GRCCacheItem itemInCache = new GRCCacheItem(key, versionStamp, value);
            if (_cache[customKeyPrefix + key] != null)
                _cache.Remove(customKeyPrefix + key);

            _cache.Insert(customKeyPrefix + key, itemInCache, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new System.TimeSpan(0, 0, LibrarySettings.CacheTimeOut, 0), CacheItemPriority.NotRemovable, null);
            return;
        }

        /// <summary>
        /// Adds an Item into Cache with key and version stamp
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="versionStamp">Version Stamp</param>
        /// <param name="value">Object or value</param>
        /// <param name="minutes">Amount in minutes</param>
        public static void AddItemToCache(string key, string versionStamp, object value, int minutes)
        {
            GRCCacheItem itemInCache = new GRCCacheItem(key, versionStamp, value);
            if (_cache[customKeyPrefix + key] != null)
            {
                _cache.Remove(customKeyPrefix + key);
            }
            _cache.Insert(customKeyPrefix + key, itemInCache, null, System.DateTime.Now.AddMinutes(minutes), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, OnRemove);
            return;
        }
    }
}
