﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls
{
    [ToolboxData("<{0}:GRCCheckBox runat=server></{0}:GRCCheckBox>")]
    [Designer("System.Web.UI.Design.WebControls.CheckBoxDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public class GRCCheckBox : CheckBox, IPostBackDataHandler
    {
        private string HiddenValueFieldName
        {
            get
            {
                return this.UniqueID + "_HidValue";
            }
        }

        private string DivArrayName
        {
            get
            {
                return this.UniqueID + "_TheImgs";
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [UrlProperty]
        [Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        public string SelectedImageUrl
        {
            get
            {
                object obj = ViewState["SelectedImageUrl"];
                return (obj == null) ? Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls.images.checkbox-active.gif") : obj.ToString();
            }
            set
            {
                ViewState["SelectedImageUrl"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [UrlProperty]
        [Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        public string NoSelectedImageUrl
        {
            get
            {
                object obj = ViewState["NoSelectedImageUrl"];
                return (obj == null) ? Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls.images.checkbox-disabled.gif") : obj.ToString();
            }
            set
            {
                ViewState["NoSelectedImageUrl"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CheckBoxTextCssClass
        {
            get
            {
                object obj = ViewState["CheckBoxTextCssClass"];
                return (obj == null) ? string.Empty : obj.ToString();
            }
            set
            {
                ViewState["CheckBoxTextCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["CHKTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CHKTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string OnClientClick
        {
            get
            {
                object obj = ViewState["OnClientClick"];
                return (obj == null) ? string.Empty : obj.ToString();
            }
            set
            {
                ViewState["OnClientClick"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["CHKTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CHKTipTextClass"] = value;
            }
        }



        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //string script = "";
            //script += "function SetGRCCBValue_"+ this.ClientID +"(theImg,HiddenValueFieldName,DivArrayName){";


            //if (this.AutoPostBack)
            //    script += Page.ClientScript.GetPostBackEventReference(this, "", false);
            //else
            //{
            //    script += "if(theImg.src.toLowerCase().indexOf('" + this.NoSelectedImageUrl + "'.toLowerCase()) != -1){";
            //    script += "theImg.src ='" + this.SelectedImageUrl + "';";
            //    script += "document.getElementById(HiddenValueFieldName).value = '1';";
            //    if (OnClientClick.Length > 0)
            //    {
            //        script += OnClientClick + "(true);";
            //    }
            //    script += "}else{";
            //    script += "theImg.src ='" + this.NoSelectedImageUrl + "';";
            //    script += "document.getElementById(HiddenValueFieldName).value = '0';";
            //    if (OnClientClick.Length > 0)
            //    {
            //        script += OnClientClick + "(false);";
            //    }
            //    script += "}";
            //    script += "for(i=0;i<DivArrayName.length;i++){";
            //    script += "var theVal=DivArrayName[i];";
            //    script += "if(theImg.getAttribute('id') != theVal)";
            //    script += "document.getElementById(theVal).src ='" + this.NoSelectedImageUrl + "';";
            //    script += "}";
            //}

            //script += "}";
            //ScriptManager.RegisterStartupScript(this,this.GetType(), "setValue_" + this.ClientID, script, true);

            //register the hidden field
            //if (Page.IsPostBack)
            //{
            //    string strVal = HttpContext.Current.Request[HiddenValueFieldName] == null ? string.Empty : HttpContext.Current.Request[HiddenValueFieldName];
            //    ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, strVal);
            //}
            //else
            //{
            //    ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, this.Checked ? "1" : "0");
            //}

            ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, this.Checked ? "1" : "0");

            //register that LoadPostdata will be called even if control's UniqueID is not on post data collection
            Page.RegisterRequiresPostBack(this);

            //this is playing for the client-side color selection :-)
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("'");
            sb.Append(this.UniqueID);
            sb.Append(this.IdSeparator);
            sb.Append("'");
            sb.Append(",");

            string s = sb.ToString().TrimEnd(',');
            ScriptManager.RegisterArrayDeclaration(this, this.UniqueID + "_TheImgs", s);
        }

        /// <summary>
        /// Raise checked-changed event
        /// </summary>
        public void RaisePostDataChangedEvent()
        {
            OnCheckedChanged(EventArgs.Empty);
        }


        /// <summary>
        /// Load data from POST (load checked from hidden input)
        /// </summary>
        /// <param name="postDataKey">Key</param>
        /// <param name="postCollection">Data</param>
        /// <returns>True when value changed</returns>
        public bool LoadPostData(string postDataKey,
            System.Collections.Specialized.NameValueCollection postCollection)
        {
            string postedValue = postCollection[postDataKey];


            if (postCollection[HiddenValueFieldName] == "1") Checked = true;
            else Checked = false;

            return true;
        }


        /// <summary>
        /// Raise check-changed event
        /// </summary>
        protected virtual void OnCheckedChanged(EventArgs e)
        {
            if (CheckedChanged != null)
                CheckedChanged(this, e);
        }

        /// <summary>
        /// Is raised when value is changed
        /// </summary>
        [Category("Action"), Description("Value changed")]
        public event EventHandler CheckedChanged;

        protected override void Render(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            if (this.TextAlign == TextAlign.Left)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CheckBoxTextCssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(this.Text);
                writer.RenderEndTag();
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.UniqueID + this.IdSeparator);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.Checked ? "1" : "0");
            writer.AddAttribute(HtmlTextWriterAttribute.Tabindex, "0");
            writer.AddAttribute("onkeypress", "SetGRCCBValue(this,'" + this.HiddenValueFieldName + "'," + this.DivArrayName + ",'" + this.SelectedImageUrl + "','" + this.NoSelectedImageUrl + "','" + this.OnClientClick + "');");
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "SetGRCCBValue(this,'" + this.HiddenValueFieldName + "'," + this.DivArrayName + ",'" + this.SelectedImageUrl + "','" + this.NoSelectedImageUrl + "','" + this.OnClientClick + "');");
            if (this.Checked)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Src, this.SelectedImageUrl);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Src, this.NoSelectedImageUrl);
            }


            if (this.TipText != null && this.TipText.Length > 0)
            {
                string showTipJS = "showtip(this,'right','" + this.TipText + "','" + this.TipTextClass + "');";
                string hideTipJS = "hidetip(this);";
                writer.AddAttribute("onmouseover", showTipJS);
                writer.AddAttribute("onmouseout", hideTipJS);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();
            if (this.TextAlign == TextAlign.Right)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CheckBoxTextCssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(this.Text);
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }
    }
}
