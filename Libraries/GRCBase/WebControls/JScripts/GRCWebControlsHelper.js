﻿function SafariFix(datePickerImageID) {
    if (datePickerImageID != null && document.getElementById(datePickerImageID) != null && navigator.appVersion.indexOf("Safari") != -1) {
        document.getElementById(datePickerImageID).style.margin = "6px 0px 0px 0px";
    }
    if (IsIE8()) {
        document.getElementById(datePickerImageID).style.margin = "6px 0px 0px 0px";
    }
}
/**
* An autosuggest textbox control.
* @class
* @scope public
*/
function AutoSuggestControl(oTextbox /*:HTMLInputElement*/, txtBoxClass, oContainerDiv, oImage, SuggestionBoxHeight, HiddenValueField,
                            oProvider /*:SuggestionProvider*/, strFunctionName, IsSourceOneDimensional, IsFunctionAjax, MinimumPrefixLength) {

    if (oTextbox == null)
        return;


    //debugger;
    /**
    * The currently selected suggestions.
    * @scope private
    */
    this.cur /*:int*/ = -1;

    /**
    * The dropdown list layer.
    * @scope private
    */
    this.layer = null;

    /**
    * Suggestion provider for the autosuggest feature.
    * @scope private.
    */
    this.provider /*:SuggestionProvider*/ = oProvider;

    this.HiddenValueField = HiddenValueField;

    /**
    * The textbox to capture.
    * @scope private
    */
    this.textbox /*:HTMLInputElement*/ = oTextbox;

    this.textbox.className = txtBoxClass;

    this.DivContainer = oContainerDiv;

    this.img = oImage;

    /*safari fix*/

    if (this.img != null && navigator.appVersion.indexOf("Safari") != -1) {
        this.img.style.margin = "6px 0px 0px 0px";
    }
    /*safari fix*/

    this.SuggestionBoxHeight = SuggestionBoxHeight;

    this.FunctionName = strFunctionName;

    this.IsFunctionAjax = IsFunctionAjax;

    this.MinimumPrefixLength = MinimumPrefixLength;

    this.IsSourceOneDimensional = IsSourceOneDimensional;

    //initialize the control
    this.init();

}

/**
* Autosuggests one or more suggestions for what the user has typed.
* If no suggestions are passed in, then no autosuggest occurs.
* @scope private
* @param aSuggestions An array of suggestion strings.
* @param bTypeAhead If the control should provide a type ahead suggestion.
*/
AutoSuggestControl.prototype.autosuggest = function(aSuggestions /*:Array*/,
                                                     bTypeAhead /*:boolean*/) {

    //make sure there's at least one suggestion
    if (aSuggestions.length > 0) {
        if (bTypeAhead) {
            this.typeAhead(aSuggestions[0]);
        }

        this.showSuggestions(aSuggestions);
    } else {
        this.hideSuggestions();
    }
};

/**
* Creates the dropdown layer to display multiple suggestions.
* @scope private
*/
AutoSuggestControl.prototype.createDropDown = function() {

    var oThis = this;

    //create the layer and assign styles
    this.layer = document.createElement("div");
    //this.layer.className = "suggestions";
    this.layer.style.border = "1px solid #4A659C";
    this.layer.style.overflowX = "hidden";
    this.layer.style.overflowY = "auto";
    this.layer.style.height = this.SuggestionBoxHeight + "px";
    this.layer.style.position = "absolute";
    this.layer.style.backgroundColor = "#FFFFFF";
    this.layer.style.visibility = "hidden";
    this.layer.style.whiteSpace = "nowrap";
    this.layer.id = this.DivContainer.id + "_cblayer";
    //this.layer.style.padding = "3px";

    //this.layer.style.width = this.textbox.offsetWidth +20 + 'px';

    if (this.FunctionName != null) {
        //this.layer.style.width = this.textbox.offsetWidth + 100 + 'px';
    }

    //when the user clicks on the a suggestion, get the text (innerHTML)
    //and place it into a textbox
    this.layer.onmousedown =
    this.layer.onmouseup =
    this.layer.onmouseover = function(oEvent) {
        oEvent = oEvent || window.event;
        oTarget = oEvent.target || oEvent.srcElement;

        if (oEvent.type == "mousedown") {
            //oThis.textbox.value = oTarget.firstChild.nodeValue;
            //oThis.hideSuggestions();
        } else if (oEvent.type == "mouseover") {
            oThis.highlightSuggestion(oTarget);
        }
        else {
            oThis.textbox.focus();
        }

    };

    this.DivContainer.appendChild(this.layer);
    //document.body.appendChild(this.layer);
};

/**
* Gets the left coordinate of the textbox.
* @scope private
* @return The left coordinate of the textbox in pixels.
*/
AutoSuggestControl.prototype.getLeft = function() /*:int*/{

    //    var oNode = this.textbox;
    //    var iLeft = 0;
    //    
    //    while(oNode.tagName != "BODY") {
    //        iLeft += oNode.offsetLeft;
    //        oNode = oNode.offsetParent;        
    //    }
    //    if(navigator.appName.indexOf("Microsoft") != -1){
    //    return iLeft;
    //    }
    //    else{
    //    return iLeft;
    //    }

    var obj = this.textbox;
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
};

/**
* Gets the top coordinate of the textbox.
* @scope private
* @return The top coordinate of the textbox in pixels.
*/
AutoSuggestControl.prototype.getTop = function() /*:int*/{
    var obj = this.textbox;
    var curtop = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;
    return curtop;
    //    var oNode = this.textbox;
    //    var iTop = 0;
    //    
    //    while(oNode.tagName != "BODY") {
    //        iTop += oNode.offsetTop;
    //        oNode = oNode.offsetParent;
    //    }
    //    if(navigator.appName.indexOf("Microsoft") != -1){
    //    return iTop + 5;
    //    }
    //    else{
    //    return iTop + 5;
    //    }
};

/**
* Handles three keydown events.
* @scope private
* @param oEvent The event object for the keydown event.
*/
AutoSuggestControl.prototype.handleKeyDown = function(oEvent /*:Event*/) {
    //alert(String.fromCharCode(oEvent.keyCode));
    //debugger;
    var iKeyCodeVal = oEvent.keyCode;

    if (iKeyCodeVal == 38) {
        this.previousSuggestion();
    }
    else if (iKeyCodeVal == 40) {
        this.nextSuggestion();
    }
    else if (iKeyCodeVal == 13) {
        this.hideSuggestions();
    }
    else {
        if (this.FunctionName == null) {
            this.ScrollIntoSuggestion(oEvent);
        }
    }
    //    switch(oEvent.keyCode) {
    //        case 38: //up arrow
    //            this.previousSuggestion();
    //            break;
    //        case 40: //down arrow 
    //            this.nextSuggestion();
    //            break;
    //        case 13: //enter
    //            this.hideSuggestions();
    //            break;
    //    }
};

AutoSuggestControl.prototype.handleImageClick = function(oEvent /*:Event*/) {

    if (this.layer.style.visibility == "hidden") {
        HideGRCMenus();
        this.textbox.focus();
        //this.nextSuggestion();

        this.provider.allSuggestions(this, false);
    }
    else {
        this.hideSuggestions();
    }
};

/**
* Handles keyup events.
* @scope private
* @param oEvent The event object for the keyup event.
*/
AutoSuggestControl.prototype.handleKeyUp = function(oEvent /*:Event*/) {
    //debugger;
    //HideGRCMenus();
    var oThis = this;
    curObj = oThis;
    var iKeyCode = oEvent.keyCode;

    //for backspace (8) and delete (46), shows suggestions without typeahead
    if (iKeyCode == 8 || iKeyCode == 46) {

        if (oThis.FunctionName != null) {
            var sTextboxValue = oThis.textbox.value;
            if (sTextboxValue.length < oThis.MinimumPrefixLength) {
                oThis.hideSuggestions();
                return;
            }
            if (oThis.IsFunctionAjax) {
                eval(oThis.FunctionName)(sTextboxValue, oThis.onSuccess, oThis.onError);
            }
            else {
                var strTempArr = eval(oThis.FunctionName)(sTextboxValue);
                oThis.provider = new StateSuggestions(strTempArr);
                oThis.provider.requestSuggestions(oThis, false);
            }
        }
        else {
            this.provider.requestSuggestions(this, false);
        }

        //make sure not to interfere with non-character keys
    } else if (iKeyCode < 32 || (iKeyCode >= 33 && iKeyCode < 46) || (iKeyCode >= 112 && iKeyCode <= 123)) {
        //ignore
    } else {
        //request suggestions from the suggestion provider with typeahead
        //arun
        if (oThis.FunctionName != null) {
            var sTextboxValue = oThis.textbox.value;
            if (sTextboxValue.length < oThis.MinimumPrefixLength) {
                oThis.hideSuggestions();
                return;
            }
            if (oThis.IsFunctionAjax) {
                eval(oThis.FunctionName)(sTextboxValue, oThis.onSuccess, oThis.onError);
            }
            else {
                var strTempArr = eval(oThis.FunctionName)(sTextboxValue);
                oThis.provider = new StateSuggestions(strTempArr);
                oThis.provider.requestSuggestions(oThis, false);
            }
        }
        else {
            this.provider.requestSuggestions(this, false);
        }
    }
};
var curObj = null;
AutoSuggestControl.prototype.onSuccess = function(result, userContext, methodName) {
    oThis = curObj;

    if (methodName == 'GetMedicationNames') {
        //Update a section of the UI   
    }

    if (userContext == 'abc') {
        //Do a specific action if it for New York   
    }

    oThis.provider = new StateSuggestions(result);
    oThis.provider.requestSuggestions(oThis, false);
};

AutoSuggestControl.prototype.onError = function(exception, userContext, methodName) {
    /*  
    We can also perform different actions like the  
    succeededCallback handler based upon the methodName and userContext  
    */
};

/**
* Hides the suggestion dropdown.
* @scope private
*/
AutoSuggestControl.prototype.hideSuggestions = function() {
    this.layer.style.visibility = "hidden";
    this.layer.parentNode.style.position = "static";
    this.cur /*:int*/ = -1;
};

/**
* Highlights the given node in the suggestions dropdown.
* @scope private
* @param oSuggestionNode The node representing a suggestion in the dropdown.
*/
AutoSuggestControl.prototype.highlightSuggestion = function(oSuggestionNode) {
    var oThis = this;

    for (var i = 0; i < this.layer.childNodes.length; i++) {
        var oNode = this.layer.childNodes[i];
        if (oNode == oSuggestionNode) {
            //oNode.className = "current"
            oNode.style.color = "#FFFFFF";
            oNode.style.backgroundColor = "#3366cc";

            oNode.onmousedown = function(oEvent) {
                oEvent = oEvent || window.event;
                oTarget = oEvent.target || oEvent.srcElement;

                if (oEvent.type == "mousedown") {
                    //alert(oTarget.firstChild.parentNode.getAttribute("ItemID"));
                    if (oThis.HiddenValueField != null) {
                        oThis.HiddenValueField.value = oTarget.firstChild.parentNode.getAttribute("ItemID");
                    }

                    oThis.textbox.value = oTarget.firstChild.nodeValue;
                    oThis.hideSuggestions();
                }
            }
        } else {

            oNode.style.color = "#000000";
            oNode.style.backgroundColor = "#FFFFFF";
        }
    }
};

/**
* Initializes the textbox with event handlers for
* auto suggest functionality.
* @scope private
*/
AutoSuggestControl.prototype.init = function() {

    //save a reference to this object
    var oThis = this;

    //debugger;
    //alert(this.textbox.getAttribute("readonly"));

    if (this.textbox.getAttribute("readonly") == false || this.textbox.getAttribute("readonly") == null) {
        //assign the onkeyup event handler
        this.textbox.onkeyup = function(oEvent) {
            //check for the proper location of the event object
            if (!oEvent) {
                oEvent = window.event;
            }

            //call the handleKeyUp() method with the event object
            oThis.handleKeyUp(oEvent);
        };
    }

    //assign onkeydown event handler
    this.textbox.onkeydown = function(oEvent) {

        //check for the proper location of the event object
        if (!oEvent) {
            oEvent = window.event;
        }

        //call the handleKeyDown() method with the event object
        oThis.handleKeyDown(oEvent);

        var bIsLayerVisible = false;
        if (oThis.layer != null && oThis.layer.style != null && oThis.layer.style.visibility == "visible") {
            bIsLayerVisible = true;
        }

        if (oEvent.keyCode == 13) {
            return false;
        }
    };

    //assign onkeydown event handler
    if (this.img != null) {
        this.img.onclick = function(oEvent) {

            //check for the proper location of the event object
            if (!oEvent) {
                oEvent = window.event;
            }

            //call the handleKeyDown() method with the event object
            oThis.handleImageClick(oEvent);
        };
    }

    //assign onblur event handler (hides suggestions)    
    this.textbox.onblur = function() {
        //oThis.hideSuggestions();
    };

    //create the suggestions dropdown
    this.createDropDown();
};

AutoSuggestControl.prototype.ScrollIntoSuggestion = function(oEvent) {
    var cSuggestionNodes = this.layer.childNodes;
    var typedChar = String.fromCharCode(oEvent.keyCode);

    for (var i = 0; i < cSuggestionNodes.length; i++) {
        var oNode = cSuggestionNodes[i];
        if (oNode.firstChild.nodeValue.charAt(0).toLowerCase() == typedChar.toLowerCase()) {
            oNode.scrollIntoView(true);
            this.highlightSuggestion(oNode);
            this.textbox.value = oNode.firstChild.nodeValue;
            this.cur = i;
            break;
        }
    }
};

/**
* Highlights the next suggestion in the dropdown and
* places the suggestion into the textbox.
* @scope private
*/
AutoSuggestControl.prototype.nextSuggestion = function() {
    var cSuggestionNodes = this.layer.childNodes;
    if (cSuggestionNodes.length > 0 && this.cur < cSuggestionNodes.length - 1) {
        var oNode = cSuggestionNodes[++this.cur];
        oNode.scrollIntoView(true);
        this.highlightSuggestion(oNode);
        this.textbox.value = oNode.firstChild.nodeValue;
    }
};

/**
* Highlights the previous suggestion in the dropdown and
* places the suggestion into the textbox.
* @scope private
*/
AutoSuggestControl.prototype.previousSuggestion = function() {
    var cSuggestionNodes = this.layer.childNodes;

    if (cSuggestionNodes.length > 0 && this.cur > 0) {
        var oNode = cSuggestionNodes[--this.cur];
        oNode.scrollIntoView(true);
        this.highlightSuggestion(oNode);
        this.textbox.value = oNode.firstChild.nodeValue;
    }
};

/**
* Selects a range of text in the textbox.
* @scope public
* @param iStart The start index (base 0) of the selection.
* @param iLength The number of characters to select.
*/
AutoSuggestControl.prototype.selectRange = function(iStart /*:int*/, iLength /*:int*/) {

    //use text ranges for Internet Explorer
    if (this.textbox.createTextRange) {
        var oRange = this.textbox.createTextRange();
        oRange.moveStart("character", iStart);
        oRange.moveEnd("character", iLength - this.textbox.value.length);
        oRange.select();

        //use setSelectionRange() for Mozilla
    } else if (this.textbox.setSelectionRange) {
        this.textbox.setSelectionRange(iStart, iLength);
    }

    //set focus back to the textbox
    this.textbox.focus();
};

/**
* Builds the suggestion layer contents, moves it into position,
* and displays the layer.
* @scope private
* @param aSuggestions An array of suggestions for the control.
*/
AutoSuggestControl.prototype.showSuggestions = function(aSuggestions /*:Array*/) {

    var oDiv = null;
    this.layer.innerHTML = "";  //clear contents of the layer
    this.layer.parentNode.style.position = "relative";

    for (var i = 0; i < aSuggestions.length; i++) {
        oDiv = document.createElement("div");
        oDiv.style.cursor = "pointer";
        oDiv.style.padding = "0px 3px";
        oDiv.style.color = "black";
        oDiv.style.fontWeight = "normal";
        oDiv.style.backgroundColor = "white";
        if (this.IsSourceOneDimensional) {
            oDiv.setAttribute("ItemID", aSuggestions[i]);
            oDiv.appendChild(document.createTextNode(aSuggestions[i]));
        }
        else {
            oDiv.setAttribute("ItemID", aSuggestions[i][1]);
            oDiv.appendChild(document.createTextNode(aSuggestions[i][0]));
        }

        this.layer.appendChild(oDiv);
    }

    this.layer.style.left = "13px";
    this.layer.style.top = this.DivContainer.offsetHeight + "px";

    //this.layer.style.left = this.getLeft() + "px";
    //this.layer.style.top = (this.getTop()+this.textbox.offsetHeight) + "px";
    if (AHAMaxZIndex) {
        this.layer.style.zIndex = AHAMaxZIndex;
        AHAMaxZIndex += 10;
    }
    else {
        this.layer.style.zIndex = 7000;
    }

    //this.layer.style.width = this.textbox.offsetWidth +20 + 'px';

    this.layer.style.visibility = "visible";

};

/**
* Inserts a suggestion into the textbox, highlighting the 
* suggested part of the text.
* @scope private
* @param sSuggestion The suggestion for the textbox.
*/
AutoSuggestControl.prototype.typeAhead = function(sSuggestion /*:String*/) {

    //check for support of typeahead functionality
    if (this.textbox.createTextRange || this.textbox.setSelectionRange) {
        var iLen = this.textbox.value.length;
        this.textbox.value = sSuggestion[1];
        this.selectRange(iLen, sSuggestion.length);
    }
};

/*********************************************************************************/

/**
* Provides suggestions for state names (USA).
* @class
* @scope public
*/
function StateSuggestions(optionArray) {
    this.states = optionArray;
}

/**
* Request suggestions for the given autosuggest control. 
* @scope protected
* @param oAutoSuggestControl The autosuggest control to provide suggestions for.
*/
StateSuggestions.prototype.requestSuggestions = function(oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/) {
    var aSuggestions = [];
    var sTextboxValue = oAutoSuggestControl.textbox.value;

    if (sTextboxValue.length > 0) {
        //search for matching states
        for (var i = 0; i < this.states.length; i++) {
            if (oAutoSuggestControl.IsSourceOneDimensional) {
                if (this.states[i].toLowerCase().indexOf(sTextboxValue.toLowerCase()) == 0) {
                    aSuggestions.push(this.states[i]);
                }
            }
            else {
                if (this.states[i][0].toLowerCase().indexOf(sTextboxValue.toLowerCase()) == 0) {
                    aSuggestions.push(this.states[i]);
                }
            }
        }
    }

    //provide suggestions to the control
    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
};

StateSuggestions.prototype.allSuggestions = function(oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/) {
    var aSuggestions = [];
    for (var i = 0; i < this.states.length; i++)
        aSuggestions.push(this.states[i]);

    //provide suggestions to the control
    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
};
/*********************************************************************************/
/*************************************START DATE PICKER********************************************/
var datePickerDivID = "datepicker";
var iFrameDivID = "datepickeriframe";

var dayArrayShort = new Array('Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa');
var dayArrayMed = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
var dayArrayLong = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
var monthArrayShort = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
var monthArrayMed = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
var monthArrayLong = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

// these variables define the date formatting we're expecting and outputting.
// If you want to use a different format by default, change the defaultDateSeparator
// and defaultDateFormat variables either here or on your HTML page.
var defaultDateSeparator = "/";        // common values would be "/" or "."
var defaultDateFormat = "mdy"    // valid values are "mdy", "dmy", and "ymd"
var dateSeparator = defaultDateSeparator;
var dateFormat = defaultDateFormat;

/**
This is the main function you'll call from the onClick event of a button.
Normally, you'll have something like this on your HTML page:

Start Date: <input name="StartDate">
<input type=button value="select" onclick="displayDatePicker('StartDate');">

That will cause the datepicker to be displayed beneath the StartDate field and
any date that is chosen will update the value of that field. If you'd rather have the
datepicker display beneath the button that was clicked, you can code the button
like this:

<input type=button value="select" onclick="displayDatePicker('StartDate', this);">

So, pretty much, the first argument (dateFieldName) is a string representing the
name of the field that will be modified if the user picks a date, and the second
argument (displayBelowThisObject) is optional and represents an actual node
on the HTML document that the datepicker should be displayed below.

In version 1.1 of this code, the dtFormat and dtSep variables were added, allowing
you to use a specific date format or date separator for a given call to this function.
Normally, you'll just want to set these defaults globally with the defaultDateSeparator
and defaultDateFormat variables, but it doesn't hurt anything to add them as optional
parameters here. An example of use is:

<input type=button value="select" onclick="displayDatePicker('StartDate', false, 'dmy', '.');">

This would display the datepicker beneath the StartDate field (because the
displayBelowThisObject parameter was false), and update the StartDate field with
the chosen value of the datepicker using a date format of dd.mm.yyyy
*/

function getMyDate(dateFieldName) {
    var myDateField = document.getElementById(dateFieldName).item(0);
    var myDateTextField = document.getElementsByName(datetextFieldName).item(0);

    myDateTextField.value = myDateField.value;

}
function displayDatePicker(dateFieldId, displayBelowThisObject, dtFormat, dtSep) {
    var targetDateField = document.getElementById(dateFieldId);
    var displayBelowThisObjectField;

    // if we weren't told what node to display the datepicker beneath, just display it
    // beneath the date field we're updating
    if (!displayBelowThisObject)
        displayBelowThisObjectField = targetDateField;
    else
        displayBelowThisObjectField = document.getElementById(displayBelowThisObject).item(0);

    // if a date separator character was given, update the dateSeparator variable
    if (dtSep)
        dateSeparator = dtSep;
    else
        dateSeparator = defaultDateSeparator;

    // if a date format was given, update the dateFormat variable
    if (dtFormat)
        dateFormat = dtFormat;
    else
        dateFormat = defaultDateFormat;

    var x = displayBelowThisObjectField.offsetLeft;
    var y = displayBelowThisObjectField.offsetTop + displayBelowThisObjectField.offsetHeight;

    // deal with elements inside tables and such
    var parent = displayBelowThisObjectField;
    while (parent.offsetParent) {
        parent = parent.offsetParent;
        x += parent.offsetLeft;
        y += parent.offsetTop;
    }

    drawDatePicker(targetDateField, x, y + 5);
}


/**
Draw the datepicker object (which is just a table with calendar elements) at the
specified x and y coordinates, using the targetDateField object as the input tag
that will ultimately be populated with a date.

This function will normally be called by the displayDatePicker function.
*/
function drawDatePicker(targetDateField, x, y) {
    var dt = getFieldDate(targetDateField.value);

    // the datepicker table will be drawn inside of a <div> with an ID defined by the
    // global datePickerDivID variable. If such a div doesn't yet exist on the HTML
    // document we're working with, add one.
    if (!document.getElementById(datePickerDivID)) {
        // don't use innerHTML to update the body, because it can cause global variables
        // that are currently pointing to objects on the page to have bad references
        //document.body.innerHTML += "<div id='" + datePickerDivID + "' class='dpDiv'></div>";
        var newNode = document.createElement("div");
        newNode.setAttribute("id", datePickerDivID);
        newNode.setAttribute("class", "dpDiv");
        newNode.setAttribute("style", "visibility: hidden;");
        document.body.appendChild(newNode);
    }

    // move the datepicker div to the proper x,y coordinate and toggle the visiblity
    var pickerDiv = document.getElementById(datePickerDivID);
    if (IsBrowserFixedPositionCompatible && IsBrowserFixedPositionCompatible() && document.getElementById('divMask') != null && document.getElementById('divMask').style.display == "block") {
        pickerDiv.style.position = "fixed";
    }
    else {
        pickerDiv.style.position = "absolute";
    }
    pickerDiv.style.left = x + "px";
    pickerDiv.style.top = y + "px";
    pickerDiv.style.visibility = (pickerDiv.style.visibility == "visible" ? "hidden" : "visible");
    pickerDiv.style.display = (pickerDiv.style.display == "block" ? "none" : "block");
    pickerDiv.style.zIndex = 10080;
    //added by arun to solve the issue of the date div closing while clicking on buttons inside it like, this month next previous etc.
    pickerDiv.onclick = function(oEvent) {
        //firefox specific
        if (oEvent != null)
            oEvent.cancelBubble = true;
        //IE specific 
        if (navigator.appName == "Microsoft Internet Explorer" && event)
            event.cancelBubble = true;
    }
    // draw the datepicker table
    refreshDatePicker(targetDateField.id, dt.getFullYear(), dt.getMonth(), dt.getDate());
}


/**
This is the function that actually draws the datepicker calendar.
*/
function refreshDatePicker(dateFieldId, year, month, day) {
    // if no arguments are passed, use today's date; otherwise, month and year
    // are required (if a day is passed, it will be highlighted later)
    var thisDay = new Date();

    if ((month >= 0) && (year > 0)) {
        thisDay = new Date(year, month, 1);
    } else {
        day = thisDay.getDate();
        thisDay.setDate(1);
    }

    // the calendar will be drawn as a table
    // you can customize the table elements with a global CSS style sheet,
    // or by hardcoding style and formatting elements below
    var crlf = "\r\n";
    var TABLE = "<table cols=7 class='dpTable'>" + crlf;
    var xTABLE = "</table>" + crlf;
    var TR = "<tr class='dpTR'>";
    var TR_title = "<tr class='dpTitleTR'>";
    var TR_days = "<tr class='dpDayTR'>";
    var TR_todaybutton = "<tr class='dpTodayButtonTR'>";
    var xTR = "</tr>" + crlf;
    var TD = "<td class='dpTD' onMouseOut='this.className=\"dpTD\";' onMouseOver=' this.className=\"dpTDHover\";' ";    // leave this tag open, because we'll be adding an onClick event
    var TD_title = "<td colspan=5 class='dpTitleTD'>";
    var TD_buttons = "<td class='dpButtonTD'>";
    var TD_todaybutton = "<td colspan=7 class='dpTodayButtonTD'>";
    var TD_days = "<td class='dpDayTD'>";
    var TD_selected = "<td class='dpDayHighlightTD' onMouseOut='this.className=\"dpDayHighlightTD\";' onMouseOver='this.className=\"dpTDHover\";' ";    // leave this tag open, because we'll be adding an onClick event
    var xTD = "</td>" + crlf;
    var DIV_title = "<div class='dpTitleText'>";
    var DIV_selected = "<div class='dpDayHighlight'>";
    var xDIV = "</div>";

    // start generating the code for the calendar table
    var html = TABLE;

    // this is the title bar, which displays the month and the buttons to
    // go back to a previous month or forward to the next month
    html += TR_title;
    html += TD_buttons + getButtonCode(dateFieldId, thisDay, -1, "&lt;") + xTD;
    html += TD_title + DIV_title + monthArrayLong[thisDay.getMonth()] + " " + thisDay.getFullYear() + xDIV + xTD;
    html += TD_buttons + getButtonCode(dateFieldId, thisDay, 1, "&gt;") + xTD;
    html += xTR;

    // this is the row that indicates which day of the week we're on
    html += TR_days;
    for (i = 0; i < dayArrayShort.length; i++)
        html += TD_days + dayArrayShort[i] + xTD;
    html += xTR;

    // now we'll start populating the table with days of the month
    html += TR;

    // first, the leading blanks
    for (i = 0; i < thisDay.getDay(); i++)
        html += TD + "&nbsp;" + xTD;

    // now, the days of the month
    do {
        dayNum = thisDay.getDate();
        TD_onclick = " onclick=\"updateDateField('" + dateFieldId + "', '" + getDateString(thisDay) + "');\">";

        if (dayNum == day)
            html += TD_selected + TD_onclick + DIV_selected + dayNum + xDIV + xTD;
        else
            html += TD + TD_onclick + dayNum + xTD;

        // if this is a Saturday, start a new row
        if (thisDay.getDay() == 6)
            html += xTR + TR;

        // increment the day
        thisDay.setDate(thisDay.getDate() + 1);
    } while (thisDay.getDate() > 1)

    // fill in any trailing blanks
    if (thisDay.getDay() > 0) {
        for (i = 6; i > thisDay.getDay(); i--)
            html += TD + "&nbsp;" + xTD;
    }
    html += xTR;

    // add a button to allow the user to easily return to today, or close the calendar
    var today = new Date();
    var todayString = "Today is " + dayArrayMed[today.getDay()] + ", " + monthArrayMed[today.getMonth()] + " " + today.getDate();
    html += TR_todaybutton + TD_todaybutton;
    html += "<button class='dpTodayButton' onClick='refreshDatePicker(\"" + dateFieldId + "\");'>This Month</button>";
    html += "&nbsp;";
    html += "<button class='dpTodayButton' onClick='updateDateField(\"" + dateFieldId + "\");'>Close</button>";
    html += xTD + xTR;

    // and finally, close the table
    html += xTABLE;

    document.getElementById(datePickerDivID).innerHTML = html;
    // add an "iFrame shim" to allow the datepicker to display above selection lists
    adjustiFrame();
}


/**
Convenience function for writing the code for the buttons that bring us back or forward
a month.
*/
function getButtonCode(dateFieldId, dateVal, adjust, label) {
    var newMonth = (dateVal.getMonth() + adjust) % 12;
    var newYear = dateVal.getFullYear() + parseInt((dateVal.getMonth() + adjust) / 12);
    if (newMonth < 0) {
        newMonth += 12;
        newYear += -1;
    }

    return "<button class='dpButton' onClick='refreshDatePicker(\"" + dateFieldId + "\", " + newYear + ", " + newMonth + ");'>" + label + "</button>";
}


/**
Convert a JavaScript Date object to a string, based on the dateFormat and dateSeparator
variables at the beginning of this script library.
*/
function getDateString(dateVal) {
    var dayString = "00" + dateVal.getDate();
    var monthString = "00" + (dateVal.getMonth() + 1);
    dayString = dayString.substring(dayString.length - 2);
    monthString = monthString.substring(monthString.length - 2);

    switch (dateFormat) {
        case "dmy":
            return dayString + dateSeparator + monthString + dateSeparator + dateVal.getFullYear();
        case "ymd":
            return dateVal.getFullYear() + dateSeparator + monthString + dateSeparator + dayString;
        case "mdy":
        default:
            return monthString + dateSeparator + dayString + dateSeparator + dateVal.getFullYear();
    }
}


/**
Convert a string to a JavaScript Date object.
*/
function getFieldDate(dateString) {
    var dateVal;
    var dArray;
    var d, m, y;

    try {
        dArray = splitDateString(dateString);
        if (dArray) {
            switch (dateFormat) {
                case "dmy":
                    d = parseInt(dArray[0], 10);
                    m = parseInt(dArray[1], 10) - 1;
                    y = parseInt(dArray[2], 10);
                    break;
                case "ymd":
                    d = parseInt(dArray[2], 10);
                    m = parseInt(dArray[1], 10) - 1;
                    y = parseInt(dArray[0], 10);
                    break;
                case "mdy":
                default:
                    d = parseInt(dArray[1], 10);
                    m = parseInt(dArray[0], 10) - 1;
                    y = parseInt(dArray[2], 10);
                    break;
            }
            dateVal = new Date(y, m, d);
        } else if (dateString) {
            dateVal = new Date(dateString);
        } else {
            dateVal = new Date();
        }
    } catch (e) {
        dateVal = new Date();
    }

    return dateVal;
}


/**
Try to split a date string into an array of elements, using common date separators.
If the date is split, an array is returned; otherwise, we just return false.
*/
function splitDateString(dateString) {
    var dArray;
    if (dateString.indexOf("/") >= 0)
        dArray = dateString.split("/");
    else if (dateString.indexOf(".") >= 0)
        dArray = dateString.split(".");
    else if (dateString.indexOf("-") >= 0)
        dArray = dateString.split("-");
    else if (dateString.indexOf("\\") >= 0)
        dArray = dateString.split("\\");
    else
        dArray = false;

    return dArray;
}

/**
Update the field with the given dateFieldName with the dateString that has been passed,
and hide the datepicker. If no dateString is passed, just close the datepicker without
changing the field value.

Also, if the page developer has defined a function called datePickerClosed anywhere on
the page or in an imported library, we will attempt to run that function with the updated
field as a parameter. This can be used for such things as date validation, setting default
values for related fields, etc. For example, you might have a function like this to validate
a start date field:

function datePickerClosed(dateField)
{
var dateObj = getFieldDate(dateField.value);
var today = new Date();
today = new Date(today.getFullYear(), today.getMonth(), today.getDate());

if (dateField.name == "StartDate") {
if (dateObj < today) {
// if the date is before today, alert the user and display the datepicker again
alert("Please enter a date that is today or later");
dateField.value = "";
document.getElementById(datePickerDivID).style.visibility = "visible";
adjustiFrame();
} else {
// if the date is okay, set the EndDate field to 7 days after the StartDate
dateObj.setTime(dateObj.getTime() + (7 * 24 * 60 * 60 * 1000));
var endDateField = document.getElementsByName ("EndDate").item(0);
endDateField.value = getDateString(dateObj);
}
}
}

*/
function updateDateField(dateFieldId, dateString) {
    var targetDateField = document.getElementById(dateFieldId);
    if (dateString)
        targetDateField.value = dateString;

    var pickerDiv = document.getElementById(datePickerDivID);
    pickerDiv.style.visibility = "hidden";
    pickerDiv.style.display = "none";

    adjustiFrame();
    targetDateField.focus();

    // after the datepicker has closed, optionally run a user-defined function called
    // datePickerClosed, passing the field that was just updated as a parameter
    // (note that this will only run if the user actually selected a date from the datepicker)
    if ((dateString) && (typeof (datePickerClosed) == "function"))
        datePickerClosed(targetDateField);
}


/**
Use an "iFrame shim" to deal with problems where the datepicker shows up behind
selection list elements, if they're below the datepicker. The problem and solution are
described at:

http://dotnetjunkies.com/WebLog/jking/archive/2003/07/21/488.aspx
http://dotnetjunkies.com/WebLog/jking/archive/2003/10/30/2975.aspx
*/
function adjustiFrame(pickerDiv, iFrameDiv) {
    return;
    // we know that Opera doesn't like something about this, so if we
    // think we're using Opera, don't even try
    var is_opera = (navigator.userAgent.toLowerCase().indexOf("opera") != -1);
    if (is_opera)
        return;

    // put a try/catch block around the whole thing, just in case
    try {
        if (!document.getElementById(iFrameDivID)) {
            // don't use innerHTML to update the body, because it can cause global variables
            // that are currently pointing to objects on the page to have bad references
            //document.body.innerHTML += "<iframe id='" + iFrameDivID + "' src='javascript:false;' scrolling='no' frameborder='0'>";
            var newNode = document.createElement("iFrame");
            newNode.setAttribute("id", iFrameDivID);
            newNode.setAttribute("src", "javascript:false;");
            newNode.setAttribute("scrolling", "no");
            newNode.setAttribute("frameborder", "0");
            document.body.appendChild(newNode);
        }

        if (!pickerDiv)
            pickerDiv = document.getElementById(datePickerDivID);
        if (!iFrameDiv)
            iFrameDiv = document.getElementById(iFrameDivID);

        try {
            iFrameDiv.style.position = "absolute";
            iFrameDiv.style.width = pickerDiv.offsetWidth;
            iFrameDiv.style.height = pickerDiv.offsetHeight;
            iFrameDiv.style.top = pickerDiv.style.top;
            iFrameDiv.style.left = pickerDiv.style.left;
            iFrameDiv.style.zIndex = pickerDiv.style.zIndex - 1;
            iFrameDiv.style.visibility = pickerDiv.style.visibility;
            iFrameDiv.style.display = pickerDiv.style.display;
        } catch (e) {
        }

    } catch (ee) {
    }

}
/*************************************END DATE PICKER********************************************/
/*************************************HIDE MENUS*************************************************/
document.onclick = HideGRCMenus;
function HideGRCMenus() {
    var cbdiv = document.getElementsByTagName("div");

    for (var i = 0; i < cbdiv.length; i++) {
        if (cbdiv[i].style != null
            && cbdiv[i].style.visibility != null
            && cbdiv[i].style.visibility == "visible"
            && cbdiv[i].id.indexOf("_cblayer") != -1) {
            cbdiv[i].style.visibility = "hidden";
            cbdiv[i].parentNode.style.position = "static";
        }
        //added by arun to fix the tooltip issue
        if (cbdiv[i].id.indexOf("grc_webcontrols_tip_div_") != -1) {
            cbdiv[i].style.visibility = "hidden";
        }
    }

    var cbdp = document.getElementsByTagName("div");

    for (var i = 0; i < cbdp.length; i++) {
        if (cbdp[i].style != null
            && cbdp[i].style.visibility != null
            && cbdp[i].style.visibility == "visible"
            && cbdp[i].id.indexOf(datePickerDivID) != -1) {
            cbdp[i].style.visibility = "hidden";
            cbdp[i].style.display = "none";
        }
    }

    var cbdpi = document.getElementsByTagName("iframe");

    for (var i = 0; i < cbdpi.length; i++) {
        if (cbdpi[i].style != null
            && cbdpi[i].style.visibility != null
            && cbdpi[i].style.visibility == "visible"
            && cbdpi[i].id.indexOf(iFrameDivID) != -1) {
            cbdpi[i].style.visibility = "hidden";
            cbdpi[i].style.display = "none";
        }
    }

}
/*************************************HIDE MENUS*************************************************/




/*************************************TIPS FOR CONTROLS*************************************************/

function getY(oElement) {
    var iReturnValue = 0;
    while (oElement != null) {
        iReturnValue += oElement.offsetTop;
        oElement = oElement.offsetParent;
    }
    return iReturnValue;
}



function getX(oElement) {
    var iReturnValue = 0;
    while (oElement != null) {
        iReturnValue += oElement.offsetLeft;
        oElement = oElement.offsetParent;
    }
    return iReturnValue;
}


function findElementPos(elem) {
    return { left: getX(elem), top: getY(elem) };
}

function showtip(textboxObj, DivPos, descText, className) {

    //console.log("showtip:" + textboxObj.id);

    var divID = createTipId(textboxObj);
    var horizonal_padding = 20;
    var vertical_padding = -40;

    var d;

    if (document.getElementById(divID) == null) {
        createTipDiv(divID, descText, className);
        d = document.getElementById(divID);
    }
    else {
        d = document.getElementById(divID);
        //d.innerHTML = descText;
        setDivText(d.id, descText);
        d.style.visibility = "visible";
    }


    changeOpac(0, d.id);
    //window.setTimeout(function(){changeOpac(0,d.id);},1);

    var elheight = (textboxObj.offsetHeight) ? textboxObj.offsetHeight : textboxObj.clientHeight;
    var elwidth = (textboxObj.offsetWidth) ? textboxObj.offsetWidth : textboxObj.clientWidth;


    var divheight = (d.offsetHeight) ? d.offsetHeight : d.clientHeight;
    var divwidth = (d.offsetWidth) ? d.offsetWidth : d.clientWidth;

    var pos = findElementPos(textboxObj);

    //alert(pos.left + ' ' + pos.top);

    if (DivPos.toLowerCase() == "top") {
        d.style.top = pos.top - height + 'px';
        d.style.left = pos.left + 'px';
    }
    else if (DivPos.toLowerCase() == "right") {
        d.style.top = pos.top + vertical_padding - (divheight - 54) + 'px';
        d.style.left = pos.left + elwidth + horizonal_padding + 'px';
    }
    else if (DivPos.toLowerCase() == "bottom") {
        d.style.top = pos.top + elheight + 'px';
        d.style.left = pos.left + 'px';
    }
    else if (DivPos.toLowerCase() == "left") {
        d.style.top = pos.top - horizonal_padding + 'px';
        d.style.left = pos.left - width + 'px';
    }


    opacity(d.id, 50, 100, 500);
    //window.setTimeout(function(){opacity(d.id,50,100,500);},1);

}


function hidetip(textboxObj) {
    // console.log("hidetip:" + textboxObj.id);
    //window.setTimeout(function() { dohidetip(textboxObj) },500);
    var divID = createTipId(textboxObj);
    var d = document.getElementById(divID);
    if (document.getElementById(divID) != null) {

        d.style.visibility = "hidden";
        //changeOpac(0,d.id);
        //window.setTimeout(function(){changeOpac(0,d.id);},1);
    }
}

function setDivText(id, description) {
    var text_div = document.getElementById(id).childNodes[1].childNodes[0];
    if (description) {
        text_div.innerHTML = description;
    } else {
        text_div.style.visiblity = "hidden";
        changeOpac(0, d.id);
    }

}

/*

<div class="ContextHelp-Container" style="width: 230px">
<div class="top">
<div>
<div>
</div>
</div>
</div>
<div class="middle">
<label>
aaaaaaaaaaaaa aaaaaaaaaaaaaa aaaaaaaaaaaa aaaaaaaaaa aaaaaaaaaaaaa
</label>
</div>
<div class="bottom">
<div>
<div>
</div>
</div>
</div>
</div>

*/

function createTipDiv(id, description, className) {
    var root_div = document.createElement('div');
    root_div.setAttribute('id', id);
    root_div.className = className;
    root_div.style.width = "230px";
    document.body.appendChild(root_div);

    var top_div = document.createElement('div');
    top_div.className = "top";
    var top_div_div = document.createElement('div');
    top_div.appendChild(top_div_div);
    var top_div_div_div = document.createElement('div');
    top_div_div.appendChild(top_div_div_div);
    root_div.appendChild(top_div);

    var middle_div = document.createElement('div');
    middle_div.className = "middle";
    var middle_div_label = document.createElement('label');
    middle_div.appendChild(middle_div_label);
    root_div.appendChild(middle_div);

    var bottom_div = document.createElement('div');
    bottom_div.className = "bottom";
    var bottom_div_div = document.createElement('div');
    bottom_div.appendChild(bottom_div_div);
    var bottom_div_div_div = document.createElement('div');
    bottom_div_div.appendChild(bottom_div_div_div);
    root_div.appendChild(bottom_div);


    if (description) {
        middle_div_label.innerHTML = description;
    } else {
        middle_div_label.style.visiblity = "hidden";
    }

}

function createTipId(textboxObj) {
    var divID = 'grc_webcontrols_tip_div_' + textboxObj.id;
    return divID;
}


function opacity(id, opacStart, opacEnd, millisec) {
    //speed for each frame
    var speed = Math.round(millisec / 100);
    var timer = 0;

    //determine the direction for the blending, if start and end are the same nothing happens
    if (opacStart > opacEnd) {
        for (i = opacStart; i >= opacEnd; i--) {
            setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
            timer++;
        }
    } else if (opacStart < opacEnd) {
        for (i = opacStart; i <= opacEnd; i++) {
            setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
            timer++;
        }
    }
}

//change the opacity for different browsers
function changeOpac(opacity, id) {
    var object = document.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + opacity + ")";
}

function shiftOpacity(id, millisec) {
    //if an element is invisible, make it visible, else make it ivisible
    if (document.getElementById(id).style.opacity == 0) {
        opacity(id, 0, 100, millisec);
    } else {
        opacity(id, 100, 0, millisec);
    }
}

function currentOpac(id, opacEnd, millisec) {
    //standard opacity is 100
    var currentOpac = 100;

    //if the element has an opacity set, get it
    if (document.getElementById(id).style.opacity < 100) {
        currentOpac = document.getElementById(id).style.opacity * 100;
    }

    //call for the function that changes the opacity
    opacity(id, currentOpac, opacEnd, millisec)
}

/*************************************TIPS FOR CONTROLS*************************************************/

function SetGRCCBValue(theImg, HiddenValueFieldName, DivArrayName, SelImageUrl, NoSelImageUrl, strOnClientClick) {
    if (theImg.src.toLowerCase().indexOf(NoSelImageUrl.toLowerCase()) != -1) {
        theImg.src = SelImageUrl;
        document.getElementById(HiddenValueFieldName).value = '1';
        //debugger;
        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(true);
        }
    }
    else {
        theImg.src = NoSelImageUrl;
        document.getElementById(HiddenValueFieldName).value = '0';
        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(false);
        }
    }

    for (i = 0; i < DivArrayName.length; i++) {
        var theVal = DivArrayName[i];
        if (theImg.getAttribute('id') != theVal)
            document.getElementById(theVal).src = NoSelImageUrl;
    }
}

function SetGRCRBLValue(theImg, HiddenValueFieldName, DivArrayName, SelImageUrl, NoSelImageUrl, strOnClientClick) {
    document.getElementById(HiddenValueFieldName).value = theImg.getAttribute('value');

    if (theImg.src.toLowerCase().indexOf(NoSelImageUrl.toLowerCase()) != -1) {
        theImg.src = SelImageUrl;
        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(true);
        }
    }
    else {
    }

    for (i = 0; i < DivArrayName.length; i++) {
        var theVal = DivArrayName[i];
        if (theImg.getAttribute('id') != theVal)
            document.getElementById(theVal).src = NoSelImageUrl;

        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(false);
        }
    }
}

/*Start Time Conversions*/

function InitializeTimeControl(HidItemID, TxtBoxID) {
    var ObjHidItem = document.getElementById(HidItemID);
    var ObjTxtBox = document.getElementById(TxtBoxID);

    if (ObjHidItem != null && ObjTxtBox != null) {
        ObjHidItem.value = GetFormattedTimeString(GetHourHalfHourAdjustedTime());
        ObjTxtBox.value = GetFormattedTimeToShow(GetHourHalfHourAdjustedTime());
    }
}

function GetHourHalfHourAdjustedTime() {
    var dt = new Date();

    var iMinutes = dt.getMinutes();
    var iHours = dt.getHours();
    if (iMinutes <= 15) {
        iMinutes = 0;
    }
    else if (iMinutes >= 45) {
        iMinutes = 0;
        if (iHours == 23) {
            iMinutes = 30;
        }
        else {
            iHours += 1;
        }
    }
    else {
        iMinutes = 30;
    }


    dt.setHours(iHours, iMinutes, 0, 0);


    return dt;
}

function GetFormattedTimeToShow(dt) {
    var dtGFTTS = new Date(dt);

    var iMinutes = dtGFTTS.getMinutes();
    var iHours = dtGFTTS.getHours();

    if (iHours == 0) {
        if (iMinutes < 10) {
            return "12" + ":0" + iMinutes + " AM";
        }
        else {
            return "12" + ":" + iMinutes + " AM";
        }
    }
    if (iHours < 10) {
        if (iMinutes < 10) {
            return "0" + iHours + ":0" + iMinutes + " AM";
        }
        else {
            return "0" + iHours + ":" + iMinutes + " AM";
        }
    }
    else if (iHours < 12) {
        if (iMinutes < 10) {
            return iHours + ":0" + iMinutes + " AM";
        }
        else {
            return iHours + ":" + iMinutes + " AM";
        }
    }
    else if (iHours == 12) {
        if (iMinutes < 10) {
            return iHours + ":0" + iMinutes + " PM";
        }
        else {
            return iHours + ":" + iMinutes + " PM";
        }
    }
    else if (iHours == 23) {
        if (iMinutes < 10) {
            return (iHours - 12) + ":0" + iMinutes + " PM";
        }
        else {
            return (iHours - 12) + ":" + iMinutes + " PM";
        }
    }
    else {
        if (iMinutes < 10) {
            return "0" + (iHours - 12) + ":0" + iMinutes + " PM";
        }
        else {
            return "0" + (iHours - 12) + ":" + iMinutes + " PM";
        }
    }
}


function GetFormattedTimeString(dt) {
    dtGFTS = new Date(dt);

    var iMinutes = dtGFTS.getMinutes();
    var iHours = dtGFTS.getHours();

    if (iHours < 10) {
        if (dt.Minute < 10) {
            return "0" + iHours + ":0" + iMinutes;
        }
        else {
            return "0" + iHours + ":" + iMinutes;
        }
    }
    else {
        if (dt.Minute < 10) {
            return iHours + ":0" + iMinutes;
        }
        else {
            return iHours + ":" + iMinutes;
        }

    }
}
/*End Time Conversions*/