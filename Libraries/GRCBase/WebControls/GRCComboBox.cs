﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls
{
    [ToolboxData("<{0}:GRCComboBox runat=server></{0}:GRCComboBox>")]
    [ParseChildren(true, "Items")]
    [ControlValueProperty("SelectedValue")]
    [SupportsEventValidation()]
    [ValidationProperty("Text")]
    [Designer("System.Web.UI.Design.WebControls.ListControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public sealed partial class GRCComboBox : DropDownList, IPostBackDataHandler
    {
        private string HiddenValueFieldName
        {
            get
            {
                return this.UniqueID + "_HidValue";
            }
        }

        private string CBOuterDivID
        {
            get
            {
                return String.Format("{0}_CBOuterDivID", this.UniqueID);
            }
        }

        private string CBImageID
        {
            get
            {
                return String.Format("{0}_CBImageID", this.UniqueID);
            }
        }

        private string CBTextBoxID
        {
            get
            {
                return String.Format("{0}_CBTextBoxID", this.UniqueID);
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["CBTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["CBTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipTextClass"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public override string Text
        {
            get
            {
                object obj = ViewState["CBSelectedText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                

                return strText;
            }
            set
            {
                ViewState["CBSelectedText"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public override string SelectedValue
        {
            get
            {
                object obj = ViewState["CBSelectedValue"];
                string strText = (obj == null) ? string.Empty : (string)obj;


                return strText;
            }
            set
            {
                ViewState["CBSelectedValue"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        [DefaultValue(true)]
        public bool CBReadOnly
        {
            get
            {
                object obj = ViewState["CBReadOnly"];
                return (obj == null) ? true : (bool)obj;
            }
            set
            {
                ViewState["CBReadOnly"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool TimeInit
        {
            get
            {
                object obj = ViewState["CBTimeInit"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["CBTimeInit"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBTextBoxCssClass
        {
            get
            {
                object obj = ViewState["CBTextBoxCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBTextBoxCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBOuterDivCssClass
        {
            get
            {
                object obj = ViewState["CBOuterDivCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBOuterDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CBPenultimateDivCssClass
        {
            get
            {
                object obj = ViewState["CBPenultimateDivCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBPenultimateDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBOuterDivWidth
        {
            get
            {
                object obj = ViewState["CBOuterDivWidth"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBOuterDivWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBPenultimateDivWidth
        {
            get
            {
                object obj = ViewState["CBPenultimateDivWidth"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBPenultimateDivWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")] 
        public int CBTextBoxWidth
        {
            get
            {
                object obj = ViewState["CBTextBoxWidth"];
                return (obj == null) ? 65 : (int)obj;
            }
            set
            {
                ViewState["CBTextBoxWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBSuggestionBoxHeight
        {
            get
            {
                object obj = ViewState["CBSuggestionBoxHeight"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBSuggestionBoxHeight"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [UrlProperty]
        [Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        public string CBImageUrl
        {
            get
            {
                object obj = ViewState["CBImageUrl"];


                return (obj == null) ? Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls.images.arrow_down.gif") : obj.ToString();

                
            }
            set
            {
                ViewState["CBImageUrl"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CBImageCssClass
        {
            get
            {
                object obj = ViewState["CBImageCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBImageCssClass"] = value;
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            ScriptManager.RegisterClientScriptResource(this, this.GetType(), "GRCBase.WebControls.JScripts.GRCWebControlsHelper.js");

            base.OnPreRender(e);


            string strMasterArrayName = this.UniqueID + "_Suggestions";


            //We need to make sure we register this array the first time the combo box is
            //made visible in the page 
            //Note: The combo box itself may be invisible (and hence OnPreRender will not 
            //be called) during first Get

            //An easy way to check if the OnPreRender already executed is to check if the
            //hidden value is already registered or not
            //if (!ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)

            if (HttpContext.Current.Request[HiddenValueFieldName] == null ||
                !ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)
            {
                StringBuilder strArray = new StringBuilder();
                int iCount = 0;
                List<string> declaredElements = new List<string>();
                foreach (ListItem objItem in this.Items)
                {
                    string arrayElement = this.UniqueID + "_element_" + iCount;
                    ScriptManager.RegisterArrayDeclaration(this, arrayElement, "\"" + GRCBase.WebUtil.JSQuoteString(objItem.Text) + "\"");
                    ScriptManager.RegisterArrayDeclaration(this, arrayElement, "\"" + GRCBase.WebUtil.JSQuoteString(objItem.Value) + "\"");

                    declaredElements.Add(arrayElement);
                    iCount++;
                }

                foreach (string declaredElement in declaredElements)
                {
                    ScriptManager.RegisterArrayDeclaration(this, strMasterArrayName, declaredElement);
                }
            }

            
            StringBuilder strScript = new StringBuilder();
            if (this.AutoPostBack)
            {
                strScript.Append(Page.ClientScript.GetPostBackEventReference(this, "", false));
            }
            else
            {
                strScript.Append("var oTextbox_" + this.UniqueID + " = new AutoSuggestControl(document.getElementById(\"" + this.CBTextBoxID + "\"),");
                strScript.Append("\"" + this.CBTextBoxCssClass + "\", document.getElementById(\"" + this.CBOuterDivID + "\"),");
                strScript.Append("document.getElementById(\"" + this.CBImageID + "\"),\"" + this.CBSuggestionBoxHeight + "\",document.getElementById(\"" + HiddenValueFieldName + "\"),");
                strScript.Append("new StateSuggestions(" + strMasterArrayName + "),null,false,null,null);\r\n");
                if (TimeInit)
                {
                    strScript.Append("InitializeTimeControl('" + this.HiddenValueFieldName + "','" + this.CBTextBoxID + "');\r\n");
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "CBScript2_" + this.UniqueID, strScript.ToString(), true);
            Page.RegisterRequiresPostBack(this);

            //register the hidden field
            if (Page.IsPostBack)
            {
                ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, this.SelectedValue);
                /*
                string strVal = HttpContext.Current.Request[HiddenValueFieldName] == null ? string.Empty : HttpContext.Current.Request[HiddenValueFieldName];
                
                ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, strVal);

                SelectedValue = strVal;
                */
                //this.Text = HttpContext.Current.Request[this.CBTextBoxID] == null ? string.Empty : HttpContext.Current.Request[this.CBTextBoxID];
            }
            else
            {
                ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, this.SelectedValue);
            }
        }

        /// <summary>
        /// Load data from POST (load checked from hidden input)
        /// </summary>
        /// <param name="postDataKey">Key</param>
        /// <param name="postCollection">Data</param>
        /// <returns>True when value changed</returns>
        public bool LoadPostData(string postDataKey,
            System.Collections.Specialized.NameValueCollection postCollection)
        {
            if (HttpContext.Current.Request[this.CBTextBoxID] != null)
            {
                this.Text = HttpContext.Current.Request[this.CBTextBoxID];
            }
            SelectedValue = HttpContext.Current.Request[HiddenValueFieldName] == null ? string.Empty : HttpContext.Current.Request[HiddenValueFieldName];
            return true;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                EnsureChildControls();
            }

            if (this.TipText != null && this.TipText.Length > 0)
            {
                string showTipJS = "showtip(this,'right','" + this.TipText + "','" + this.TipTextClass + "');";
                string hideTipJS = "hidetip(this);";
                writer.AddAttribute("onmouseover", showTipJS);
                writer.AddAttribute("onmouseout", hideTipJS);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "event.cancelBubble = true;");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBOuterDivID);
            writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBOuterDivWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBOuterDivCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBPenultimateDivWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBPenultimateDivCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);


            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBTextBoxID);
            if (CBReadOnly)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly,"readonly");
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Name, this.CBTextBoxID);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBTextBoxID);
            writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBTextBoxWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.Text);
            writer.AddAttribute(HtmlTextWriterAttribute.AutoComplete, "off");
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBImageID);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBImageCssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Src, this.CBImageUrl);
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();

            writer.RenderEndTag();

            writer.RenderEndTag();
        }
    }
}
    