﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls
{
    [ToolboxData("<{0}:GRCAutoSuggest runat=server></{0}:GRCAutoSuggest>")]
        [ValidationProperty("Text")]
    [Designer("System.Web.UI.Design.WebControls.TextBoxDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public sealed partial class GRCAutoSuggest : TextBox, IPostBackDataHandler
    {
        private string CBOuterDivID
        {
            get
            {
                return String.Format("{0}_CBOuterDivID", this.UniqueID);
            }
        }

        private string CBTextBoxID
        {
            get
            {
                return String.Format("{0}_CBTextBoxID", this.UniqueID);
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue(true)]
        public bool IsClientFunctionAjax
        {
            get
            {
                object obj = ViewState["IsClientFunctionAjax"];
                bool bVal = (obj == null) ? true : (bool)obj;
                return bVal;
            }
            set
            {
                ViewState["IsClientFunctionAjax"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string ClientSuggestionsArrayName
        {
            get
            {
                object obj = ViewState["ClientSuggestionsArrayName"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["ClientSuggestionsArrayName"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string ClientSuggestionsFunctionName
        {
            get
            {
                object obj = ViewState["ClientSuggestionsFunctionName"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["ClientSuggestionsFunctionName"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue(2)]
        public int CBMinimumPrefixLength
        {
            get
            {
                object obj = ViewState["CBMinimumPrefixLength"];
                int iLength = (obj == null) ? 2 : (int)obj;
                return iLength;
            }
            set
            {
                ViewState["CBMinimumPrefixLength"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["CBTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["CBTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipTextClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBTextBoxCssClass
        {
            get
            {
                object obj = ViewState["CBTextBoxCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBTextBoxCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBOuterDivCssClass
        {
            get
            {
                object obj = ViewState["CBOuterDivCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBOuterDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CBPenultimateDivCssClass
        {
            get
            {
                object obj = ViewState["CBPenultimateDivCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBPenultimateDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBOuterDivWidth
        {
            get
            {
                object obj = ViewState["CBOuterDivWidth"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBOuterDivWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBPenultimateDivWidth
        {
            get
            {
                object obj = ViewState["CBPenultimateDivWidth"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBPenultimateDivWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBTextBoxWidth
        {
            get
            {
                object obj = ViewState["CBTextBoxWidth"];
                return (obj == null) ? 65 : (int)obj;
            }
            set
            {
                ViewState["CBTextBoxWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBSuggestionBoxHeight
        {
            get
            {
                object obj = ViewState["CBSuggestionBoxHeight"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBSuggestionBoxHeight"] = value;
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            ScriptManager.RegisterClientScriptResource(this, this.GetType(), "GRCBase.WebControls.JScripts.GRCWebControlsHelper.js");

            base.OnPreRender(e);



            StringBuilder strScript = new StringBuilder();
            if (this.AutoPostBack)
            {
                strScript.Append(Page.ClientScript.GetPostBackEventReference(this, "", false));
            }
            else
            {
                strScript.Append("var oTextbox_" + this.UniqueID + " = new AutoSuggestControl(document.getElementById(\"" + this.CBTextBoxID + "\"),");
                strScript.Append("\"" + this.CBTextBoxCssClass + "\", document.getElementById(\"" + this.CBOuterDivID + "\"),");
                strScript.Append("null,\"" + this.CBSuggestionBoxHeight + "\",null,");
                if (ClientSuggestionsFunctionName.Length > 0)
                {
                    strScript.Append("null,\"" + ClientSuggestionsFunctionName + "\",true," + IsClientFunctionAjax.ToString().ToLower() + ","+ CBMinimumPrefixLength +");\r\n");
                }
                else
                {
                    strScript.Append("new StateSuggestions(" + ClientSuggestionsArrayName + "),null,true," + IsClientFunctionAjax.ToString().ToLower() + "," + CBMinimumPrefixLength + ");\r\n");
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "CBScript2_" + this.UniqueID, strScript.ToString(), true);
            Page.RegisterRequiresPostBack(this);
        }

        /// <summary>
        /// Load data from POST (load checked from hidden input)
        /// </summary>
        /// <param name="postDataKey">Key</param>
        /// <param name="postCollection">Data</param>
        /// <returns>True when value changed</returns>
        public bool LoadPostData(string postDataKey,
            System.Collections.Specialized.NameValueCollection postCollection)
        {
            if (HttpContext.Current.Request[this.CBTextBoxID] != null)
            {
                this.Text = HttpContext.Current.Request[this.CBTextBoxID];
            }
            return true;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                EnsureChildControls();
            }

            if (this.TipText != null && this.TipText.Length > 0)
            {
                string showTipJS = "showtip(this,'right','" + this.TipText + "','" + this.TipTextClass + "');";
                string hideTipJS = "hidetip(this);";
                writer.AddAttribute("onmouseover", showTipJS);
                writer.AddAttribute("onmouseout", hideTipJS);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "event.cancelBubble = true;");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBOuterDivID);
            writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBOuterDivWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBOuterDivCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBPenultimateDivWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBPenultimateDivCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);


            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBTextBoxID);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, this.CBTextBoxID);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBTextBoxID);
            writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBTextBoxWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.Text);
            writer.AddAttribute(HtmlTextWriterAttribute.AutoComplete, "off");
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            writer.RenderEndTag();

            writer.RenderEndTag();
        }
    }
}
