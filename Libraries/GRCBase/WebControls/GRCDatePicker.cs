﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls
{
    [ToolboxData("<{0}:GRCDatePicker runat=server></{0}:GRCDatePicker>")]
    [ValidationProperty("Text")]
    [Designer("System.Web.UI.Design.WebControls.ListControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public class GRCDatePicker : TextBox, IPostBackDataHandler
    {
        private string CBOuterDivID
        {
            get
            {
                return String.Format("{0}_CBOuterDivID", this.UniqueID);
            }
        }

        private string CBImageID
        {
            get
            {
                return String.Format("{0}_CBImageID", this.UniqueID);
            }
        }

        private string CBTextBoxID
        {
            get
            {
                return String.Format("{0}_CBTextBoxID", this.UniqueID);
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public override string Text
        {
            get
            {
                object obj = ViewState["CBSelectedValue"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }

                return strText;
            }
            set
            {
                ViewState["CBSelectedValue"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["CBTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["CBTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipTextClass"] = value;
            }
        }



        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        public DateTime? Date
        {
            get
            {
                object obj = ViewState["DBSelectedDate"];
                if (obj == null)
                {
                    return null;
                }
                try
                {
                    return Convert.ToDateTime(obj);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                ViewState["DBSelectedDate"] = value;
                // || Text.Length == 0
                if (value.HasValue)
                {
                    string strMonth = value.Value.Month.ToString();
                    if (value.Value.Month < 10)
                    {
                        strMonth = "0" + strMonth;
                    }

                    string strDay = value.Value.Day.ToString();
                    if (value.Value.Day < 0)
                    {
                        strMonth = "0" + strDay;
                    }
                    Text = strMonth + "/" + strDay + "/" + value.Value.Year;
                }
                else
                {
                    if (Text.Length > 0)
                    {
                        try
                        {
                            ViewState["DBSelectedDate"] = Convert.ToDateTime(Text);
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        [DefaultValue(true)]
        public bool CBReadOnly
        {
            get
            {
                object obj = ViewState["CBReadOnly"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["CBReadOnly"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBTextBoxCssClass
        {
            get
            {
                object obj = ViewState["CBTextBoxCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBTextBoxCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBOuterDivCssClass
        {
            get
            {
                object obj = ViewState["CBOuterDivCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBOuterDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CBPenultimateDivCssClass
        {
            get
            {
                object obj = ViewState["CBPenultimateDivCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBPenultimateDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBOuterWidth
        {
            get
            {
                object obj = ViewState["CBOuterWidth"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBOuterWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBTextBoxWidth
        {
            get
            {
                object obj = ViewState["CBTextBoxWidth"];
                return (obj == null) ? 90 : (int)obj;
            }
            set
            {
                ViewState["CBTextBoxWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBSuggestionBoxHeight
        {
            get
            {
                object obj = ViewState["CBSuggestionBoxHeight"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBSuggestionBoxHeight"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [UrlProperty]
        [Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        public string CBImageUrl
        {
            get
            {
                object obj = ViewState["CBImageUrl"];
                return (obj == null) ? Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls.images.btn_calendar.gif") : obj.ToString();
            }
            set
            {
                ViewState["CBImageUrl"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CBImageCssClass
        {
            get
            {
                object obj = ViewState["CBImageCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBImageCssClass"] = value;
            }
        }


       


        protected override void OnPreRender(EventArgs e)
        {

            


            ScriptManager.RegisterClientScriptResource(this, this.GetType(), "GRCBase.WebControls.JScripts.GRCWebControlsHelper.js");

            base.OnPreRender(e);

            //safari image position fix
            ScriptManager.RegisterStartupScript(this, this.GetType(), "JS_DatePicker" + this.ClientID, "SafariFix('"+ this.CBImageID +"');", true);

            //register that LoadPostdata will be called even if control's UniqueID is not on post data collection
            Page.RegisterRequiresPostBack(this);

            if (Page.IsPostBack)
            {
                //this.Text = HttpContext.Current.Request[this.CBTextBoxID] == null ? string.Empty : HttpContext.Current.Request[this.CBTextBoxID];
            }
        }

        /// <summary>
        /// Load data from POST (load checked from hidden input)
        /// </summary>
        /// <param name="postDataKey">Key</param>
        /// <param name="postCollection">Data</param>
        /// <returns>True when value changed</returns>
        public bool LoadPostData(string postDataKey,
            System.Collections.Specialized.NameValueCollection postCollection)
        {
            string postedValue = postCollection[postDataKey];

            this.Text = HttpContext.Current.Request[this.CBTextBoxID] == null ? string.Empty : HttpContext.Current.Request[this.CBTextBoxID];
            if (!String.IsNullOrEmpty(Text))
            {
                try
                {
                    this.Date = Convert.ToDateTime(Text);
                }
                catch
                {
                }
            }
            return true;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                EnsureChildControls();
            }


            if (this.TipText != null && this.TipText.Length > 0)
            {
                string showTipJS = "showtip(this,'right','" + this.TipText + "','" + this.TipTextClass + "');";
                string hideTipJS = "hidetip(this);";
                writer.AddAttribute("onmouseover", showTipJS);
                writer.AddAttribute("onmouseout", hideTipJS);
            }


            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "event.cancelBubble = true;");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBOuterDivID);
            //writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBOuterWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBOuterDivCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBPenultimateDivCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBTextBoxID);
            if (CBReadOnly)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Name, this.CBTextBoxID);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBTextBoxID);
            //writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.CBTextBoxWidth.ToString() + "px");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.Text);



          


            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();


            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "displayDatePicker('" + this.CBTextBoxID + "','','mdy','/');");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBImageID);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBImageCssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Src, this.CBImageUrl);
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();

            writer.RenderEndTag();

            writer.RenderEndTag();
        }
    }
}
