﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls
{
    [ToolboxData("<{0}:GRCRadioButtonList runat=server></{0}:GRCRadioButtonList>")]
    [ParseChildren(true, "Items")]
    [ControlValueProperty("SelectedValue")]
    [ValidationProperty("Text")]
    [Designer("System.Web.UI.Design.WebControls.ListControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public class GRCRadioButtonList : RadioButtonList
    {
        private string HiddenValueFieldName
        {
            get
            {
                return this.UniqueID + "_HidValue";
            }
        }

        private string DivArrayName
        {
            get
            {
                return this.UniqueID + "_TheImgs";
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [UrlProperty]
        [Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        public string SelectedImageUrl
        {
            get
            {
                object obj = ViewState["SelectedImageUrl"];
                return (obj == null) ? Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls.images.radio-active.gif") : obj.ToString();
            }
            set
            {
                ViewState["SelectedImageUrl"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [UrlProperty]
        [Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        public string NoSelectedImageUrl
        {
            get
            {
                object obj = ViewState["NoSelectedImageUrl"];
                return (obj == null) ? Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls.images.radio-disbled.gif") : obj.ToString();
            }
            set
            {
                ViewState["NoSelectedImageUrl"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string RadioButtonTextCssClass
        {
            get
            {
                object obj = ViewState["RadioButtonTextCssClass"];
                return (obj == null) ? string.Empty : obj.ToString();
            }
            set
            {
                ViewState["RadioButtonTextCssClass"] = value;
            }
        }

         [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["RADTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["RADTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["RADTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["RADTipTextClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string OnClientClick
        {
            get
            {
                object obj = ViewState["OnClientClick"];
                return (obj == null) ? string.Empty : obj.ToString();
            }
            set
            {
                ViewState["OnClientClick"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //string script = "";
            //script += "function SetGRCRBLValue(theImg,HiddenValueFieldName,DivArrayName){";
            //script += "document.getElementById(HiddenValueFieldName).value = theImg.getAttribute('value');";

            //if (this.AutoPostBack)
            //    script += Page.ClientScript.GetPostBackEventReference(this, "", false);
            //else
            //{
            //    script += "if(theImg.src.toLowerCase().indexOf('" + this.NoSelectedImageUrl + "'.toLowerCase()) != -1){";
            //    script += "theImg.src ='" + this.SelectedImageUrl + "';";
            //    if (OnClientClick.Length > 0)
            //    {
            //        script += OnClientClick + "(true);";
            //    }
            //    script += "}else{";
            //    //script += "theImg.src ='" + this.NoSelectedImageUrl + "';";
            //    script += "}";
            //    script += "for(i=0;i<DivArrayName.length;i++){";
            //    script += "var theVal=DivArrayName[i];";
            //    script += "if(theImg.getAttribute('id') != theVal)";
            //    script += "document.getElementById(theVal).src ='" + this.NoSelectedImageUrl + "';";
            //    if (OnClientClick.Length > 0)
            //    {
            //        script += OnClientClick + "(false);";
            //    }
            //    script += "}";
            //}

            //script += "}";
            //ScriptManager.RegisterStartupScript(this,this.GetType(), "setValue", script, true);

            //register the hidden field
            if (Page.IsPostBack)
            {
                string strVal = HttpContext.Current.Request[HiddenValueFieldName] == null ? string.Empty : HttpContext.Current.Request[HiddenValueFieldName];
                ScriptManager.RegisterHiddenField(this,HiddenValueFieldName, strVal);
            }
            else
            {
                ScriptManager.RegisterHiddenField(this,HiddenValueFieldName, this.SelectedValue);
            }


            //register that LoadPostdata will be called even if control's UniqueID is not on post data collection
            Page.RegisterRequiresPostBack(this);

            //this is playing for the client-side color selection :-)
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (ListItem litem in this.Items)
            {
                sb.Append("'");
                sb.Append(this.UniqueID);
                sb.Append(this.IdSeparator);
                sb.Append(litem.Value);
                sb.Append("'");
                sb.Append(",");
            }

            string s = sb.ToString().TrimEnd(',');
            ScriptManager.RegisterArrayDeclaration(this,this.UniqueID + "_TheImgs", s);

        }

        protected override bool LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            //wrap the existing loading logic to get the LiostItem value from our hidden field
            return base.LoadPostData(HiddenValueFieldName, postCollection);
        }

        protected override void RenderItem(ListItemType itemType, int repeatIndex, RepeatInfo repeatInfo, HtmlTextWriter writer)
        {
            //render the thing out as a DIV
            ListItem item = this.Items[repeatIndex];


            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            if (this.TextAlign == TextAlign.Left)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.RadioButtonTextCssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(item.Text);
                writer.RenderEndTag();
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.UniqueID + this.IdSeparator + item.Value);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, item.Value);
            writer.AddAttribute(HtmlTextWriterAttribute.Tabindex, "0");

            writer.AddAttribute("onkeypress", "SetGRCRBLValue(this,'" + this.HiddenValueFieldName + "'," + this.DivArrayName + ",'" + this.SelectedImageUrl + "','" + this.NoSelectedImageUrl + "','" + this.OnClientClick + "');");
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "SetGRCRBLValue(this,'" + this.HiddenValueFieldName + "'," + this.DivArrayName + ",'" + this.SelectedImageUrl + "','" + this.NoSelectedImageUrl + "','" + this.OnClientClick + "');");
            if (item.Selected)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Src, this.SelectedImageUrl);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Src, this.NoSelectedImageUrl);
            }



            if (this.TipText != null && this.TipText.Length > 0)
            {
                string showTipJS = "showtip(this,'right','" + this.TipText + "','" + this.TipTextClass + "');";
                string hideTipJS = "hidetip(this);";
                writer.AddAttribute("onmouseover", showTipJS);
                writer.AddAttribute("onmouseout", hideTipJS);
            }


            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();
            if (this.TextAlign == TextAlign.Right)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.RadioButtonTextCssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(item.Text);
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }
    }
}
