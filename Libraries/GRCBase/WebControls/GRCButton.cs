﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls
{
    [ToolboxData("<{0}:GRCButton runat=server></{0}:GRCButton>")]
    [ValidationProperty("Text")]
    [Designer("System.Web.UI.Design.WebControls.ListControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public class GRCButton : Button 
    {
        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["TBTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["TBTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["TBTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["TBTipTextClass"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ScriptManager.RegisterClientScriptResource(this, this.GetType(), "GRCBase.WebControls.JScripts.GRCWebControlsHelper.js");

            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                EnsureChildControls();
            }

            if (this.TipText != null && this.TipText.Length > 0)
            {
                string showTipJS = "showtip(this,'right','" + this.TipText + "','" + this.TipTextClass + "');";
                string hideTipJS = "hidetip(this);";
                writer.AddAttribute("onmouseover", showTipJS);
                writer.AddAttribute("onmouseout", hideTipJS);
            }
            base.Render(writer);
        }
    }
}
