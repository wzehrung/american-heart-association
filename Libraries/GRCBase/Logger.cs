﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using System.Runtime.CompilerServices;

namespace GRCBase
{
    /// <summary>
    /// Logger : To be used by developers for logging in proper context
    /// </summary>
    public class GRCLogger
    {
        private static ILog m_log = null;

        public GRCLogger(ILog log4NetLogger)
        {
            m_log = log4NetLogger;
        }

        /// <summary>
        /// Basic information to developer
        /// </summary>
        /// <param name="msg"></param>
        public void LogInfo(string msg)
        {
            if (m_log != null) m_log.Info(msg);
        }

        /// <summary>
        /// A warning message but not fatal or error
        /// </summary>
        /// <param name="msg"></param>
        public void LogWarn(string msg)
        {
            if (m_log != null) m_log.Warn(msg);
        }

        /// <summary>
        /// For debuggin purposes
        /// </summary>
        /// <param name="msg"></param>
        public void LogDebug(string msg)
        {
            if (m_log != null) m_log.Debug(msg);
        }

        /// <summary>
        /// Indicates an error has occurred
        /// </summary>
        /// <param name="msg"></param>
        public void LogError(string msg)
        {
            if (m_log != null) m_log.Error(msg);
        }
    }

    /// <summary>
    /// Log4Net wrapper class
    /// </summary>
    public class Logger
    {
        private static ILog m_log_db = null;
        private static ILog m_log_app = null;
        private static ILog m_log_billing = null;
        private static ILog m_log_www = null;
        private static bool m_bSettingsLoaded = false;

        static Logger()
        {
            _DoLoadSettings();
        }

        /// <summary>
        /// Used to log Database level 
        /// </summary>
        public static GRCLogger DBLogger
        {
            get
            {
                return new GRCLogger(m_log_db);
            }
        }

        /// <summary>
        /// To log application level
        /// </summary>
        public static GRCLogger AppLogger
        {
            get
            {
                return new GRCLogger(m_log_app);
            }
        }

        /// <summary>
        /// To Log Credit Card Billing 
        /// </summary>
        public static GRCLogger BillingLogger
        {
            get
            {
                return new GRCLogger(m_log_billing);
            }
        }

        /// <summary>
        /// Log Web Applications 
        /// </summary>
        public static GRCLogger WWWLogger
        {
            get
            {
                return new GRCLogger(m_log_www);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void _DoLoadSettings()
        {
            if (m_bSettingsLoaded)  //NOTE: We need to check this at start again.
                return;

            System.Xml.XmlElement logConfigElement = ConfigReader.GetXmlElement("log4net");

            if (logConfigElement != null)
            {
                log4net.Config.XmlConfigurator.Configure(logConfigElement);
                m_log_db = LogManager.GetLogger("DB");
                m_log_app = LogManager.GetLogger("APP");
                m_log_billing = LogManager.GetLogger("BILLING");
                m_log_www = LogManager.GetLogger("WWW");
                //ConfigReader.ChangeEvent += new ConfigChangeHandler(ConfigReader_ChangeEvent);
            }
            m_bSettingsLoaded = true;
        }
    }
}
