﻿using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Globalization;

namespace GRCBase
{
    public class ActionLessForm : System.Web.UI.HtmlControls.HtmlForm
    {
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            writer.WriteAttribute("name", this.Name);
            base.Attributes.Remove("name");

            writer.WriteAttribute("method", this.Method);
            base.Attributes.Remove("method");

            this.Attributes.Render(writer);

            writer.WriteAttribute("action", "");
            base.Attributes.Remove("action");

            if (base.ID != null)
                writer.WriteAttribute("id", base.ClientID);
        }
    }

    public class WebUtil
    {
        /// <summary>
        /// Encodes URL
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string UrlEncode(string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;
            return System.Web.HttpUtility.UrlEncode(s);
        }

        /// <summary>
        /// Decodes URL
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string UrlDecode(string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;
            return System.Web.HttpUtility.UrlDecode(s);
        }

        /// <summary>
        /// HTML encodes the string : makes safe for rendering to browser
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string HtmlEncode(string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;
            // TODO : Does not replace '
            return System.Web.HttpUtility.HtmlEncode(s);

        }

        /// <summary>
        /// Makes javascript code ready for html rendering
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public static string GetJavaScriptStringForHtmlRendering(string strScript)
        {

            //        \  \\ 
            //        "   \x22 
            //        '   \x27 
            //        & \x26 
            //        < \x3C 
            //        > \x3E 

            StringBuilder strjava = new StringBuilder(strScript);
            strjava = strjava.Replace("\\", "\\\\");
            strjava = strjava.Replace("\"", "\\x22");
            strjava = strjava.Replace("&", "\\x26");
            strjava = strjava.Replace(">", "\\x3E");
            strjava = strjava.Replace("<", "\\x3C");
            return strjava.ToString();
        }

        /// <summary>
        /// Removes aphostropes and quotes
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetSafeJSString(string s)
        {
            if (s.Length > 0)
            {
                return Regex.Replace(s, @"(['""\\])", @"\$1");
            }
            return s;
        }

        public static string QueryStringEncode(string s)
        {
            s = System.Web.HttpUtility.UrlEncode(s);
            return s;
        }

        public static string QueryStringDecode(string s)
        {
            s = System.Web.HttpUtility.UrlDecode(s);
            return s;
        }

        public static string CleanFileName(string filename)
        {
            string strFilename = filename;
            string BadChars = ":/?*|"; // Need to add 34,39,9
            string replaceChar = "_";
            for (int i = 0; i < BadChars.Length; i++)
                strFilename.Replace(BadChars.Substring(i, 0), replaceChar);

            return strFilename;
        }

        public static string EncodeReservedChars(string qString)
        {
            qString = qString.Replace("$", "%24");
            qString = qString.Replace("&", "%26");
            qString = qString.Replace("+", "%2B");
            qString = qString.Replace(",", "%2C");
            qString = qString.Replace("/", "%2F");
            qString = qString.Replace(":", "%3A");
            qString = qString.Replace(";", "%3B");
            qString = qString.Replace("=", "%3D");
            qString = qString.Replace("?", "%3F");
            qString = qString.Replace("@", "%40");
            return qString;
        }

        public static DataSet ConvertDataReaderToDataSet(IDataReader reader)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(ConvertDataReaderToDataTable(reader));
            return ds;
        }

        public static DataTable ConvertDataReaderToDataTable(IDataReader reader)
        {
            DataTable dt = new DataTable();
            int fieldCount = reader.FieldCount;
            for (int i = 0; i < fieldCount; i++)
                dt.Columns.Add(reader.GetName(i), reader.GetFieldType(i));

            dt.BeginLoadData();

            object[] values = null;
            while (reader.Read())
            {
                reader.GetValues(values);
                dt.LoadDataRow(values, true);

            }
            reader.Close();
            dt.EndLoadData();

            return dt;
        }

        public static StringBuilder FetchWebpage(string url, int timeout_secs, string userAgent)
        {
            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(url);
            myReq.Timeout = timeout_secs * 1000;

            if (!String.IsNullOrEmpty(userAgent))
            {
                myReq.UserAgent = userAgent;
            }

            WebResponse myResp = myReq.GetResponse();
            System.Text.StringBuilder full_response = new System.Text.StringBuilder();
            using (System.IO.Stream stream = myResp.GetResponseStream())
            {
                byte[] array = new Byte[1024];
                int read = stream.Read(array, 0, 1024);
                while (read > 0)
                {
                    string str = System.Text.ASCIIEncoding.ASCII.GetString(array, 0, read);
                    full_response.Append(str);
                    read = stream.Read(array, 0, 1024);
                }
            }
            myResp.Close();
            return full_response;
        }


        private static void AppendCharAsUnicode(StringBuilder builder, char c)
        {
            builder.Append(@"\u");
            builder.AppendFormat(CultureInfo.InvariantCulture, "{0:x4}", new object[] { (int)c });
        }



        public static string JSQuoteString(string value)
        {
            StringBuilder builder = null;
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            int startIndex = 0;
            int count = 0;
            for (int i = 0; i < value.Length; i++)
            {
                char c = value[i];
                if ((((c == '\r') || (c == '\t')) || ((c == '"') || (c == '\''))) || ((((c == '<') || (c == '>')) || ((c == '\\') || (c == '\n'))) || (((c == '\b') || (c == '\f')) || (c < ' '))))
                {
                    if (builder == null)
                    {
                        builder = new StringBuilder(value.Length + 5);
                    }
                    if (count > 0)
                    {
                        builder.Append(value, startIndex, count);
                    }
                    startIndex = i + 1;
                    count = 0;
                }
                switch (c)
                {
                    case '<':
                    case '>':
                    case '\'':
                        {
                            AppendCharAsUnicode(builder, c);
                            continue;
                        }
                    case '\\':
                        {
                            builder.Append(@"\\");
                            continue;
                        }
                    case '\b':
                        {
                            builder.Append(@"\b");
                            continue;
                        }
                    case '\t':
                        {
                            builder.Append(@"\t");
                            continue;
                        }
                    case '\n':
                        {
                            builder.Append(@"\n");
                            continue;
                        }
                    case '\f':
                        {
                            builder.Append(@"\f");
                            continue;
                        }
                    case '\r':
                        {
                            builder.Append(@"\r");
                            continue;
                        }
                    case '"':
                        {
                            builder.Append("\\\"");
                            continue;
                        }
                }
                if (c < ' ')
                {
                    AppendCharAsUnicode(builder, c);
                }
                else
                {
                    count++;
                }
            }
            if (builder == null)
            {
                return value;
            }
            if (count > 0)
            {
                builder.Append(value, startIndex, count);
            }
            return builder.ToString();
        }
    }
}
