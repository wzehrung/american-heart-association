﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Web;

namespace GRCBase
{
    public class StringHelper
    {
        /// <summary>
        ///returns empty string if null. Or returns ToString(). 
        /// </summary>
        public static string NStr(object obj)
        {
            if (obj == null)
                return "";
            return obj.ToString();
        }

        /// <summary>
        ///returns sql friendly quote escaped string
        /// </summary>
        public static string SqlEscape(string str)
        {
            if (str == null)
                return "";
            return str.Replace("'", "''");
        }

        /// <summary>
        /// HTml scrubs and then trims a string to a fixed length.
        /// </summary>
        public static string HtmlScrubAndGetShortString(string strText, int iCharacters)
        {
            return HtmlScrubAndGetShortString(strText, iCharacters, true);
        }

        /// <summary>
        /// HTml scrubs and then trims a string to a fixed length.
        /// </summary>
        public static string HtmlScrubAndGetShortString(string strText, int iCharacters, bool bAreDotsRequired)
        {
            string scrubbedString = HtmlScrubber.Clean(strText, false, true);
            return GetShortString(scrubbedString, iCharacters, true);
        }

        /// <summary>
        /// Trims a string to a fixed length.Use if '...' prefix is required.
        /// </summary>
        public static string GetShortString(string strText, int iCharacters)
        {
            return GetShortString(strText, iCharacters, true);
        }

        /// <summary>
        /// Trims a string to a fixed length.
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="iCharacters"></param>
        /// <param name="IsDotsRequired">decides whether '...' required.</param>
        public static string GetShortString(string strText, int iCharacters, bool AreDotsRequired)
        {
            if (String.IsNullOrEmpty(strText))
                return "";

            strText = strText.Trim();
            if (strText != null && strText.Length > iCharacters)
            {
                if (AreDotsRequired)
                    return strText.Substring(0, iCharacters) + "...";
                else
                    return strText;
            }
            return strText == null ? "" : strText;
        }

        /// <summary>
        /// Formats only US telephone numbers.
        /// </summary>
        public static string GetFormattedUSPhoneNumber(string strUnformattedNumber)
        {
            string strFormattedNumber = string.Empty;

            // we really only understand 10 digit phone numbers
            if (!string.IsNullOrEmpty(strUnformattedNumber) && strUnformattedNumber.Length == 10)  // Format number
            {
                strFormattedNumber += "(" + strUnformattedNumber.Substring(0, 3) + ") "
                    + strUnformattedNumber.Substring(3, 6 - 3) + "-"
                    + strUnformattedNumber.Substring(6, 10 - 6);
            }
            else  // Do not format number, because number is not parseable
            {
                strFormattedNumber = strUnformattedNumber;
            }
            return strFormattedNumber;
        }

        public static double RemoveCommaFromCurrencyAmount(string dblValue)
        {
            string strAmount = dblValue.Replace(",", string.Empty);
            return Convert.ToDouble(strAmount);
        }

        public static string HtmlEncodeAndReplaceQuotes(string str)
        {
            return HttpUtility.HtmlEncode(str).Replace("'", "&#0039;");
        }

        public static string GetFormattedCurrencyString(string floatval)
        {
            if (floatval.Length == 0)
                return string.Empty;

            double val = 0;
            try
            {
                val = Convert.ToDouble(floatval);
            }
            catch (System.Exception e)
            {
                return string.Empty;
            }
            string format = "#,##0.00;-#,##0.00;0.00";
            return val.ToString(format);
        }

        public static double HbA1cToPercentage(double dHbA1c)
        {
            return dHbA1c * 100;
        }

        public static string GetFormattedDoubleString(double dblVal, int iNumberOfDecimalPoints, bool bShowDecimalIfDecimalPartZero)
        {
            try
            {
                int i = Convert.ToInt32(dblVal);

                double dFraction = dblVal - i;

                if (bShowDecimalIfDecimalPartZero && dFraction == 0)
                    return i.ToString();

                return Math.Round(dblVal, iNumberOfDecimalPoints).ToString();
            }
            catch
            {
                return Math.Round(dblVal, iNumberOfDecimalPoints).ToString();
            }
        }

        public static bool IsInStringArray(string str, string[] strArray, bool IsCaseSensitive)
        {
            foreach (string strItem in strArray)
            {
                if (IsCaseSensitive)
                {
                    if (strItem == str)
                    {
                        return true;
                    }
                }
                else
                {
                    if (strItem.ToLower() == str.ToLower())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool IsInStringList(string str, List<string> strArray, bool IsCaseSensitive)
        {
            foreach (string strItem in strArray)
            {
                if (IsCaseSensitive)
                {
                    if (strItem == str)
                    {
                        return true;
                    }
                }
                else
                {
                    if (strItem.ToLower() == str.ToLower())
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
