﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GRCBase")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Get Real Consulting")]
[assembly: AssemblyProduct("GRCBase")]
[assembly: AssemblyCopyright("Copyright © Get Real Consulting 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8f9821ca-947b-4492-accf-0ed982b31e29")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: TagPrefix("GRCBase.WebControls", "GRC")]
[assembly: WebResource("GRCBase.WebControls.JScripts.GRCWebControlsHelper.js", "text/javascript", PerformSubstitution = true)]
[assembly: WebResource("GRCBase.WebControls.images.radio-disbled.gif", "image/gif")]
[assembly: WebResource("GRCBase.WebControls.images.radio-active.gif", "image/gif")]
[assembly: WebResource("GRCBase.WebControls.images.checkbox-active.gif", "image/gif")]
[assembly: WebResource("GRCBase.WebControls.images.checkbox-disabled.gif", "image/gif")]
[assembly: WebResource("GRCBase.WebControls.images.arrow_down.gif", "image/gif")]
[assembly: WebResource("GRCBase.WebControls.images.btn_calendar.gif", "image/gif")]
[assembly: TagPrefix("GRCBase.WebControls_V1", "GRC_V1")]
[assembly: WebResource("GRCBase.WebControls_V1.StyleSheet.css", "text/css")]
[assembly: WebResource("GRCBase.WebControls_V1.JScripts.GRCWebControlsHelper.js", "text/javascript", PerformSubstitution = true)]
[assembly: WebResource("GRCBase.WebControls_V1.images.b-arrow-down.gif", "image/gif")]
[assembly: WebResource("GRCBase.WebControls_V1.images.btn_calendar.gif", "image/gif")]
[assembly: WebResource("GRCBase.WebControls_V1.images.red-arrow-down.gif", "image/gif")]
