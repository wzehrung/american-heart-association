﻿var lowerCaseLengthArray = new Array(7, 8, 7, 8, 7, 4, 7, 7, 3, 4, 8, 3, 11, 7, 8, 8, 8, 5, 6, 4, 7, 7, 11, 7, 7, 6);
var upperCaseLengthArray = new Array(11, 10, 11, 11, 9, 9, 11, 11, 5, 6, 12, 9, 14, 12, 12, 9, 12, 10, 9, 9, 11, 11, 15, 11, 11, 9);
var lowerCaseArray = new Array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
var upperCaseArray = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

function GetCharLength(chr) {

    if (isLowerCase(chr)) {
        for (var i = 0; i < lowerCaseArray.length; i++) {
            if (chr == lowerCaseArray[i]) {
                return lowerCaseLengthArray[i];
            }
        }
    }
    else {
        for (var i = 0; i < upperCaseArray.length; i++) {
            if (chr == upperCaseArray[i]) {
                return upperCaseLengthArray[i];
            }
        }
    }

    //for space
    if (chr.replace(" ", "").length == 0)
        return 4

    //for number    
    return 8;
}

var SiteMaxZIndex = 20090;
var SuggestionsDivClassArray = null;

function AddToSuggestionsDivArray(id) {
    if (SuggestionsDivClassArray == null) {
        SuggestionsDivClassArray = new Array();
    }
    var bFound = false;
    for (var i = 0; i < SuggestionsDivClassArray.length; i++) {
        if (SuggestionsDivClassArray[i] == id) {
            bFound = true;
            break;
        }
    }

    if (!bFound) {
        SuggestionsDivClassArray[SuggestionsDivClassArray.length] = id;
    }
}

function HideAllSuggestionsDiv(id) {
    if (SuggestionsDivClassArray == null)
        return;

    for (var i = 0; i < SuggestionsDivClassArray.length; i++) {
        if (document.getElementById(SuggestionsDivClassArray[i]) != null && SuggestionsDivClassArray[i] != id && document.getElementById(SuggestionsDivClassArray[i]).style.display == "block") {
            document.getElementById(SuggestionsDivClassArray[i]).style.display = "none";
        }
    }
}
/**
* An autosuggest textbox control.
* @class
* @scope public
*/
function AutoSuggestControl(IsDisplayContainerStatic, containerid, outerscrolldivid, DivSuggestContainerID, isEnabled, oTextbox /*:HTMLInputElement*/, txtBoxClass, suggestionsOuterCss, txtBoxOnChangeFunctionName, strOnChangeFunctionParams, isReadOnly, oContainerDiv, oPenultimateDiv, oImage, SuggestionBoxHeight, HiddenValueField,
                            oProvider /*:SuggestionProvider*/, strFunctionName, IsSourceOneDimensional, IsFunctionAjax, MinimumPrefixLength, FilterResultsOnClient) {

    if (oTextbox == null)
        return;
    //debugger;

    //debugger;
    /**
    * The currently selected suggestions.
    * @scope private
    */
    this.cur /*:int*/ = -1;

    this.IsDisplayContainerStatic = IsDisplayContainerStatic;

    this.RealContainerID = DivSuggestContainerID;
    this.DivScrollContainer = outerscrolldivid;
    this.ConstOffsetLeft = -1;
    this.ConstOffsetTop = 17;

    this.isEnabled = isEnabled;

    this.DivSuggestContainerID = DivSuggestContainerID;

    /**
    * The dropdown list layer.
    * @scope private
    */
    this.layer = null;

    /**
    * Suggestion provider for the autosuggest feature.
    * @scope private.
    */
    this.provider /*:SuggestionProvider*/ = oProvider;

    this.HiddenValueField = HiddenValueField;

    this.ReadOnly = isReadOnly;

    /**
    * The textbox to capture.
    * @scope private
    */
    this.textbox /*:HTMLInputElement*/ = oTextbox;

    this.txtBoxOnChangeFunctionName = txtBoxOnChangeFunctionName;
    this.OnChangeFunctionParams = strOnChangeFunctionParams;

    this.textbox.className = txtBoxClass;

    this.CBSuggestionsOuterDivCssClass = suggestionsOuterCss;

    this.DivContainer = oContainerDiv;

    this.PenultimateDiv = oPenultimateDiv;

    this.img = oImage;

    /*safari fix*/

    if (this.img != null && navigator.appVersion.indexOf("Safari") != -1) {
        this.img.style.margin = "6px 0px 0px 0px";
    }
    /*safari fix*/

    this.SuggestionBoxHeight = SuggestionBoxHeight;

    this.FunctionName = strFunctionName;

    this.IsFunctionAjax = IsFunctionAjax;

    this.MinimumPrefixLength = MinimumPrefixLength;

    this.IsSourceOneDimensional = IsSourceOneDimensional;

    this.FilterResultsOnClient = FilterResultsOnClient;

    this.MaxItemsToShowAtATime = 5;

    this.Timer = null;

    //initialize the control
    this.init();
}

/**
* Autosuggests one or more suggestions for what the user has typed.
* If no suggestions are passed in, then no autosuggest occurs.
* @scope private
* @param aSuggestions An array of suggestion strings.
* @param bTypeAhead If the control should provide a type ahead suggestion.
*/
AutoSuggestControl.prototype.autosuggest = function(aSuggestions /*:Array*/,
                                                     bTypeAhead /*:boolean*/, bShowOnUI) {
    //added by arun onblur june 6 2009, top fix the issue with relatives where only one suggestion present did', select the node upon clicking the down arrow
    this.cur = -1;
    //make sure there's at least one suggestion
    if (aSuggestions.length > 0) {
        if (bTypeAhead) {
            this.typeAhead(aSuggestions[0]);
        }
        this.showSuggestions(aSuggestions, bShowOnUI);
    } else {
        this.hideSuggestions();
    }
};

/**
* Creates the dropdown layer to display multiple suggestions.
* @scope private
*/
AutoSuggestControl.prototype.createDropDown = function() {
    if (document.getElementById(this.RealContainerID) != null) {
        this.layer = document.getElementById(this.RealContainerID);
        return;
    }
    var oThis = this;
    //create the layer and assign styles
    this.layer = document.createElement("span");
    this.layer.className = this.CBSuggestionsOuterDivCssClass;
    this.layer.style.overflowX = "hidden";
    this.layer.style.overflowY = "auto";
    //this.layer.style.height = this.SuggestionBoxHeight + "px";
    this.layer.style.position = "absolute";
    this.layer.style.whiteSpace = "nowrap";
    this.layer.id = this.RealContainerID; //this.DivContainer.id + "_cblayer";
    //this.layer.style.padding = "3px";

    //this.layer.style.width = this.textbox.offsetWidth +20 + 'px';

    if (this.FunctionName != null) {
        //this.layer.style.width = this.textbox.offsetWidth + 100 + 'px';
    }

    //when the user clicks on the a suggestion, get the text (innerHTML)
    //and place it into a textbox
    this.layer.onmousedown =
    this.layer.onmouseup =
    this.layer.onmouseover = function(oEvent) {
        oEvent = oEvent || window.event;
        oTarget = oEvent.target || oEvent.srcElement;

        if (oEvent.type == "mousedown") {
            //oThis.textbox.value = oTarget.firstChild.nodeValue;
            //oThis.hideSuggestions();
        } else if (oEvent.type == "mouseover") {
            oThis.highlightSuggestion(oTarget);
        }
        else {
            oThis.textbox.focus();
        }

    };

    //this.layer.style.width = this.layer.offsetWidth + "px";
    //this.GetLayerContainer().appendChild(this.layer);
    this.RegisterLayer();
};

AutoSuggestControl.prototype.RegisterLayer = function() {

    if (this.layer.attachEvent) {
        this.layer.onclick = new Function("event.cancelBubble = true;");
    }
    else {
        this.layer.setAttribute("onclick", "event.cancelBubble = true;");
    }
    document.body.appendChild(this.layer);

    AddToContainerArray(this.DivScrollContainer, this.layer.id, this.textbox.id, this.ConstOffsetLeft, this.ConstOffsetTop, this.IsDisplayContainerStatic);
    AddToSuggestionsDivArray(this.layer.id);
};

/**
* Gets the left coordinate of the textbox.
* @scope private
* @return The left coordinate of the textbox in pixels.
*/
AutoSuggestControl.prototype.getLeft = function() /*:int*/{

    //    var oNode = this.textbox;
    //    var iLeft = 0;
    //    
    //    while(oNode.tagName != "BODY") {
    //        iLeft += oNode.offsetLeft;
    //        oNode = oNode.offsetParent;        
    //    }
    //    if(navigator.appName.indexOf("Microsoft") != -1){
    //    return iLeft;
    //    }
    //    else{
    //    return iLeft;
    //    }

    var obj = this.DivContainer;
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;

    if (navigator.appName == "Microsoft Internet Explorer") {
        curleft += 1;
    }
    return curleft;
};

/**
* Gets the top coordinate of the textbox.
* @scope private
* @return The top coordinate of the textbox in pixels.
*/
AutoSuggestControl.prototype.getTop = function() /*:int*/{
    var obj = this.DivContainer;
    var curtop = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;

    if (navigator.appName == "Microsoft Internet Explorer") {
        curtop += 1;
    }
    return curtop;
    //    var oNode = this.textbox;
    //    var iTop = 0;
    //    
    //    while(oNode.tagName != "BODY") {
    //        iTop += oNode.offsetTop;
    //        oNode = oNode.offsetParent;
    //    }
    //    if(navigator.appName.indexOf("Microsoft") != -1){
    //    return iTop + 5;
    //    }
    //    else{
    //    return iTop + 5;
    //    }
};

AutoSuggestControl.prototype.stopEvent = function(event /*:Event*/) {
    event = event || window.event;

    if (event) {
        if (event.stopPropagation) event.stopPropagation();
        if (event.preventDefault) event.preventDefault();

        if (typeof event.cancelBubble != "undefined") {
            event.cancelBubble = true;
            event.returnValue = false;
        }
    }

    return false;
};

/**
* Handles three keydown events.
* @scope private
* @param oEvent The event object for the keydown event.
*/
AutoSuggestControl.prototype.handleKeyDown = function (oEvent /*:Event*/) {
    if (this.layer == null) {
        this.createDropDown();
        //this.cur = -1;
        if (this.provider != null) {
            this.provider.allSuggestions(this, false, false);
        }
    }
    //alert('hello');
    HideAllSuggestionsDiv(this.layer.id);

    if (this.textbox.value == "Select") {
        this.textbox.value = '';
    }

    //debugger;
    var iKeyCodeVal = oEvent.keyCode;
    //alert(this.cur);
    if (oEvent.altKey && iKeyCodeVal == 40) {
        if (this.layer == null || this.layer.style.display == "none") {
            HideGRCMenus();
            this.textbox.focus();

            this.provider.allSuggestions(this, false, true);
        }
        else {
            this.hideSuggestions();
        }
    }
    else if (iKeyCodeVal == 38) {
        if (this.cur == -1)
            return;
        this.previousSuggestion();
        var cSuggestionNodes = this.layer.childNodes;
        if (cSuggestionNodes.length > 0 && this.cur < cSuggestionNodes.length) {
            var oNode = cSuggestionNodes[this.cur];
            oTarget = oEvent.target || oEvent.srcElement;
            if (this.HiddenValueField != null) {
                this.HiddenValueField.value = oNode.getAttribute("ItemID");
            }
        }

        this.stopEvent(oEvent);
    }
    else if (iKeyCodeVal == 40) {
        this.nextSuggestion();
        var cSuggestionNodes = this.layer.childNodes;
        if (cSuggestionNodes.length > 0 && this.cur < cSuggestionNodes.length) {
            var oNode = cSuggestionNodes[this.cur];
            oTarget = oEvent.target || oEvent.srcElement;
            if (this.HiddenValueField != null) {
                this.HiddenValueField.value = oNode.getAttribute("ItemID");
            }
        }

        this.stopEvent(oEvent);
    }
    else if (iKeyCodeVal == 13) {
        var cSuggestionNodes = this.layer.childNodes;
        if (cSuggestionNodes.length > 0 && this.cur < cSuggestionNodes.length) {
            var oNode = cSuggestionNodes[this.cur];
            oTarget = oEvent.target || oEvent.srcElement;
            if (this.HiddenValueField != null) {
                this.HiddenValueField.value = oNode.getAttribute("ItemID");
            }
        }

        if (this.txtBoxOnChangeFunctionName.length > 0) {
            if (this.OnChangeFunctionParams.length > 0) {
                eval(this.txtBoxOnChangeFunctionName)(this.OnChangeFunctionParams, oNode.getAttribute("ItemID"));
            }
            else {
                eval(this.txtBoxOnChangeFunctionName)(this.textbox);
            }
        }

        this.hideSuggestions();
    }
    else if (iKeyCodeVal == 9) {
        this.hideSuggestions();
    }
    else {
        if (this.FunctionName == null) {
            this.ScrollIntoSuggestion(oEvent);
        }
    }
    //    switch(oEvent.keyCode) {
    //        case 38: //up arrow
    //            this.previousSuggestion();
    //            break;
    //        case 40: //down arrow 
    //            this.nextSuggestion();
    //            break;
    //        case 13: //enter
    //            this.hideSuggestions();
    //            break;
    //    }
};

AutoSuggestControl.prototype.handleImageClick = function(oEvent /*:Event*/) {
    //debugger;
    if (this.layer == null || this.layer.style.display == "none") {
        HideGRCMenus();
        this.textbox.focus();
        //this.nextSuggestion();

        this.provider.allSuggestions(this, false, true);
    }
    else {
        this.hideSuggestions();
    }
};

/**
* Handles keyup events.
* @scope private
* @param oEvent The event object for the keyup event.
*/
AutoSuggestControl.prototype.handleKeyUp = function (oEvent /*:Event*/) {
    //debugger;
    //HideGRCMenus();
    var oThis = this;
    curObj = oThis;
    var iKeyCode = oEvent.keyCode;

    //for backspace (8) and delete (46), shows suggestions without typeahead
    if (iKeyCode == 8 || iKeyCode == 46) {
        if (oThis.FunctionName != null) {
            var sTextboxValue = oThis.textbox.value;
            if (sTextboxValue.length < oThis.MinimumPrefixLength) {
                oThis.hideSuggestions();
                return;
            }
            if (oThis.IsFunctionAjax) {
                this.cur = -1;
                eval(oThis.FunctionName)(sTextboxValue, oThis.onSuccess, oThis.onError);
            }
            else {
                var strTempArr = eval(oThis.FunctionName)(sTextboxValue);
                oThis.provider = new StateSuggestions(strTempArr);
                oThis.provider.requestSuggestions(oThis, false);
            }
        }
        else {
            this.provider.requestSuggestions(this, false);
        }

        //make sure not to interfere with non-character keys
    } else if (iKeyCode < 32 || (iKeyCode >= 33 && iKeyCode < 46) || (iKeyCode >= 112 && iKeyCode <= 123)) {
        //ignore
    } else {
        //request suggestions from the suggestion provider with typeahead
        //arun
        if (oThis.FunctionName != null) {
            var sTextboxValue = oThis.textbox.value;
            if (sTextboxValue.length < oThis.MinimumPrefixLength) {
                oThis.hideSuggestions();
                return;
            }
            if (oThis.IsFunctionAjax) {
                addKeyDownLoader(); // defined in front-end.js
                eval(oThis.FunctionName)(sTextboxValue, oThis.onSuccess, oThis.onError);
            }
            else {
                var strTempArr = eval(oThis.FunctionName)(sTextboxValue);
                oThis.provider = new StateSuggestions(strTempArr);
                oThis.provider.requestSuggestions(oThis, false);
            }
        }
        else {
            this.provider.requestSuggestions(this, false);
        }
    }
};
var curObj = null;
AutoSuggestControl.prototype.onSuccess = function (result, userContext, methodName) {
    oThis = curObj;

    if (methodName == 'HelloWorld') {
        //Update a section of the UI   
    }

    if (userContext == 'abc') {
        //Do a specific action if it for New York   
    }

    removeKeyDownLoader(); // defined in front-end.js

    oThis.provider = new StateSuggestions(result);
    oThis.provider.requestSuggestions(oThis, false);
};

AutoSuggestControl.prototype.onError = function(exception, userContext, methodName) {
    /*  
    We can also perform different actions like the  
    succeededCallback handler based upon the methodName and userContext  
    */

};

/**
* Hides the suggestion dropdown.
* @scope private
*/
AutoSuggestControl.prototype.hideSuggestions = function() {
    if (this.layer != null) {
        this.layer.style.display = "none";
    }
    if (document.getElementById(this.DivSuggestContainerID) == null) {
        //this.layer.parentNode.style.position = "static";
    }
    this.cur /*:int*/ = -1;
};

/**
* Highlights the given node in the suggestions dropdown.
* @scope private
* @param oSuggestionNode The node representing a suggestion in the dropdown.
*/
AutoSuggestControl.prototype.highlightSuggestion = function(oSuggestionNode) {
    var oThis = this;

    for (var i = 0; i < this.layer.childNodes.length; i++) {
        var oNode = this.layer.childNodes[i];
        if (oNode == oSuggestionNode) {
            //oNode.className = "current"
            oNode.style.color = "#FFFFFF";
            oNode.style.backgroundColor = "#08246B"; //"#3366cc";
            this.cur = i;
            oNode.onmousedown = function(oEvent) {
                oEvent = oEvent || window.event;
                oTarget = oEvent.target || oEvent.srcElement;
                if (oEvent.type == "mousedown") {
                    //alert(oTarget.firstChild.nodeValue);
                    //alert(oTarget.firstChild.parentNode.getAttribute("ItemID"));
                    if (oThis.HiddenValueField != null) {
                        oThis.HiddenValueField.value = oTarget.firstChild.parentNode.getAttribute("ItemID");
                        document.getElementById(oThis.HiddenValueField.id).value = oTarget.firstChild.parentNode.getAttribute("ItemID");
                    }
                    //alert(oThis.textbox.value);
                    document.getElementById(oThis.textbox.id).value = oTarget.firstChild.nodeValue;
                    //oThis.textbox.value = oTarget.firstChild.nodeValue;
                    if (oThis.txtBoxOnChangeFunctionName.length > 0) {
                        if (oThis.OnChangeFunctionParams.length > 0) {
                            eval(oThis.txtBoxOnChangeFunctionName)(oThis.OnChangeFunctionParams, oTarget.firstChild.parentNode.getAttribute("ItemID"));
                        }
                        else {
                            eval(oThis.txtBoxOnChangeFunctionName)(oThis.textbox);
                        }
                    }
                    oThis.hideSuggestions();
                    //alert(oTarget.firstChild.parentNode.getAttribute("ItemID"));
                    //alert(oThis.textbox.value);
                }
            }
        } else {

            oNode.style.color = "#000000";
            oNode.style.backgroundColor = "#FFFFFF";
        }
    }
};

/**
* Initializes the textbox with event handlers for
* auto suggest functionality.
* @scope private
*/
AutoSuggestControl.prototype.init = function () {

    if (!this.isEnabled)
        return;
    //save a reference to this object
    var oThis = this;

    //debugger;
    //alert(this.textbox.getAttribute("readonly"));

    if (this.textbox.getAttribute("readonly") == false || this.textbox.getAttribute("readonly") == null) {
        //assign the onkeyup event handler
        this.textbox.onkeyup = function (oEvent) {
            //check for the proper location of the event object
            if (!oEvent) {
                oEvent = window.event;
            }

            //call the handleKeyUp() method with the event object
            oThis.handleKeyUp(oEvent);
        };
    }

    //assign onkeydown event handler
    this.textbox.onkeydown = function (oEvent) {

        //check for the proper location of the event object
        if (!oEvent) {
            oEvent = window.event;
        }

        //call the handleKeyDown() method with the event object
        oThis.handleKeyDown(oEvent);

        var bIsLayerVisible = false;
        if (oThis.layer != null && oThis.layer.style != null && oThis.layer.style.display == "block") {
            bIsLayerVisible = true;
        }

        if (oEvent.keyCode == 13) {
            return false;
        }
    };

    if (this.textbox.getAttribute("readonly") != null && (this.textbox.getAttribute("readonly") == "readonly" || this.textbox.getAttribute("readonly") == true)) {
        this.textbox.style.cursor = "pointer";
    }

    if (!this.IsFunctionAjax) {
        this.textbox.onclick = function (oEvent) {

            //check for the proper location of the event object
            if (!oEvent) {
                oEvent = window.event;
            }
            //call the handleKeyDown() method with the event object
            oThis.handleImageClick(oEvent);
        };
    }

    //assign onkeydown event handler
    if (this.img != null) {
        this.img.onclick = function (oEvent) {

            //check for the proper location of the event object
            if (!oEvent) {
                oEvent = window.event;
            }
            //call the handleKeyDown() method with the event object
            oThis.handleImageClick(oEvent);
        };
    }

    //assign onblur event handler (hides suggestions)    
    this.textbox.onblur = function () {
        //oThis.hideSuggestions();
    };

    //create the suggestions dropdown
    //this.createDropDown();
};

AutoSuggestControl.prototype.ScrollIntoSuggestion = function(oEvent) {
    //alert('hello');
    if (this.layer == null) {
        this.createDropDown();
    }

    var cSuggestionNodes = this.layer.childNodes;
    var typedChar = String.fromCharCode(oEvent.keyCode);

    var currentNode = null;
    if (this.cur != -1) {
        currentNode = cSuggestionNodes[this.cur];
    }
    var currentNodeVal = currentNode != null ? currentNode.firstChild.nodeValue : "";
    var bNodeFound = false;

    if (this.cur + 1 < cSuggestionNodes.length) {
        for (var i = this.cur + 1; i < cSuggestionNodes.length; i++) {
            var oCorrectNode = cSuggestionNodes[i];
            if (oCorrectNode.firstChild.nodeValue.charAt(0).toLowerCase() == typedChar.toLowerCase()) {
                bNodeFound = true;
                break;
            }
        }
    }

    if (!bNodeFound) {
        for (var i = 0; i < cSuggestionNodes.length; i++) {
            var oCorrectNode = cSuggestionNodes[i];
            if (oCorrectNode.firstChild.nodeValue.charAt(0).toLowerCase() == typedChar.toLowerCase()) {
                bNodeFound = true;
                break;
            }
        }
    }

    if (bNodeFound && oCorrectNode.firstChild.nodeValue.charAt(0).toLowerCase() == typedChar.toLowerCase()) {
        //oNode.scrollIntoView(true);
        this.layer.scrollTop = oCorrectNode.offsetTop;
        this.highlightSuggestion(oCorrectNode);

        if (this.ReadOnly) {
            this.textbox.value = oCorrectNode.firstChild.nodeValue;
            if (this.FunctionName == null && this.HiddenValueField != null) {
                this.HiddenValueField.value = oCorrectNode.getAttribute("ItemID");
            }

            if (this.txtBoxOnChangeFunctionName.length > 0) {
                if (this.OnChangeFunctionParams.length > 0) {
                    eval(this.txtBoxOnChangeFunctionName)(this.OnChangeFunctionParams, oCorrectNode.firstChild.nodeValue);
                }
                else {
                    eval(this.txtBoxOnChangeFunctionName)(this.textbox);
                }
            }
        }

        this.cur = i;
    }
};

AutoSuggestControl.prototype.ScrollIntoTextNode = function() {
    var cSuggestionNodes = this.layer.childNodes;
    //alert(cSuggestionNodes.length);
    for (var i = 0; i < cSuggestionNodes.length; i++) {
        var oNode = cSuggestionNodes[i];
        if (oNode.firstChild.nodeValue.toLowerCase() == this.textbox.value.toLowerCase()) {
            this.layer.scrollTop = oNode.offsetTop;
            //alert(this.layer.scrollTop);
            this.highlightSuggestion(oNode);
            this.cur = i;
            //alert(this.textbox.value);
        }
    }
    clearTimeout(this.Timer);
    this.Timer = null;
}

/**
* Highlights the next suggestion in the dropdown and
* places the suggestion into the textbox.
* @scope private
*/
AutoSuggestControl.prototype.nextSuggestion = function() {
    var cSuggestionNodes = this.layer.childNodes;
    //alert(this.cur);
    //alert(cSuggestionNodes.length);
    if (cSuggestionNodes.length > 0 && this.cur < cSuggestionNodes.length - 1) {
        var oNode = cSuggestionNodes[++this.cur];
        //oNode.scrollIntoView(true);
        this.highlightSuggestion(oNode);
        //alert(oNode.offsetTop + " : " + oNode.offsetHeight + " : " + this.layer.scrollTop + " : " + this.layer.offsetHeight);
        if (!IsBrowserFixedPositionCompatible()) {
            if ((oNode.offsetTop + oNode.offsetHeight) > (this.layer.offsetHeight)) {
                var ndx = this.cur - this.MaxItemsToShowAtATime + 1;
                if (ndx > 0)
                    this.layer.scrollTop = this.layer.scrollTop + cSuggestionNodes[ndx].offsetTop;
            }
        }
        else {
            if ((oNode.offsetTop + oNode.offsetHeight) > (this.layer.scrollTop + this.layer.offsetHeight)) {
                var ndx = this.cur - this.MaxItemsToShowAtATime + 1;
                if (ndx > 0)
                    this.layer.scrollTop = cSuggestionNodes[ndx].offsetTop;
            }
        }
        this.textbox.value = oNode.firstChild.nodeValue;
        if (this.FunctionName == null && this.HiddenValueField != null) {
            this.HiddenValueField.value = oNode.getAttribute("ItemID");
        }
        this.textbox.focus();
    }
};

/**
* Highlights the previous suggestion in the dropdown and
* places the suggestion into the textbox.
* @scope private
*/
AutoSuggestControl.prototype.previousSuggestion = function() {
    var cSuggestionNodes = this.layer.childNodes;

    if (cSuggestionNodes.length > 0 && this.cur > 0) {
        var oNode = cSuggestionNodes[--this.cur];
        if (oNode != null) {
            //oNode.scrollIntoView(true);
            if (!IsBrowserFixedPositionCompatible()) {
                if (oNode.offsetTop < this.layer.scrollTop)
                    this.layer.scrollTop = this.layer.scrollTop + oNode.offsetTop;
            }
            else {
                if (oNode.offsetTop < this.layer.scrollTop)
                    this.layer.scrollTop = oNode.offsetTop;
            }
            this.highlightSuggestion(oNode);
            this.textbox.value = oNode.firstChild.nodeValue;
            if (this.FunctionName == null && this.HiddenValueField != null) {
                this.HiddenValueField.value = oNode.getAttribute("ItemID");
            }
        }
    }
};

/**
* Selects a range of text in the textbox.
* @scope public
* @param iStart The start index (base 0) of the selection.
* @param iLength The number of characters to select.
*/
AutoSuggestControl.prototype.selectRange = function(iStart /*:int*/, iLength /*:int*/) {

    //use text ranges for Internet Explorer
    if (this.textbox.createTextRange) {
        var oRange = this.textbox.createTextRange();
        oRange.moveStart("character", iStart);
        oRange.moveEnd("character", iLength - this.textbox.value.length);
        oRange.select();

        //use setSelectionRange() for Mozilla
    } else if (this.textbox.setSelectionRange) {
        this.textbox.setSelectionRange(iStart, iLength);
    }

    //set focus back to the textbox
    this.textbox.focus();
};

/**
* Builds the suggestion layer contents, moves it into position,
* and displays the layer.
* @scope private
* @param aSuggestions An array of suggestions for the control.
*/
AutoSuggestControl.prototype.showSuggestions = function(aSuggestions /*:Array*/, bShowOnUI) {

    //debugger;
    HideContainerArrayItems();
    var oDiv = null;
    //alert(this.provider.states.length);
    if (this.layer == null) {
        this.createDropDown();
    }
    this.layer.innerHTML = "";  //clear contents of the layer
    if (document.getElementById(this.DivSuggestContainerID) == null) {
        //this.layer.parentNode.style.position = "relative";
    }

    for (var i = 0; i < aSuggestions.length; i++) {
        oDiv = document.createElement("span");
        oDiv.style.cursor = "pointer";
        oDiv.id = "div_inner_" + i;
        oDiv.style.padding = "0px 3px";
        oDiv.style.color = "black";
        oDiv.style.fontWeight = "normal";
        oDiv.style.backgroundColor = "white";
        if (this.IsSourceOneDimensional) {
            oDiv.setAttribute("ItemID", aSuggestions[i]);
            oDiv.appendChild(document.createTextNode(aSuggestions[i]));
        }
        else {
            oDiv.setAttribute("ItemID", aSuggestions[i][1]);
            oDiv.appendChild(document.createTextNode(aSuggestions[i][0]));
        }

        this.layer.appendChild(oDiv);
    }


    //this.layer.style.left = "13px";
    var lMaxWidth = 0;
    var strMaxString = "";
    for (var i = 0; i < aSuggestions.length; i++) {
        if (this.IsSourceOneDimensional) {
            if (aSuggestions[i].length > lMaxWidth) {
                lMaxWidth = aSuggestions[i].length;
                strMaxString = aSuggestions[i];
            }
        }
        else {
            if (aSuggestions[i][0].length > lMaxWidth) {
                lMaxWidth = aSuggestions[i][0].length;
                strMaxString = aSuggestions[i][0];
            }
        }
    }

    var divWidth = 0;
    for (var i = 0; i < strMaxString.length; i++) {
        //debugger;
        divWidth += GetCharLength(strMaxString.charAt(i));
        //        if (isLowerCase(strMaxString.charAt(i))) {
        //            divWidth += 7;
        //        }
        //        else {
        //            divWidth += 11;
        //        }
    }


    //this.layer.style.top = this.DivContainer.offsetHeight + "px";

    var layerWidth = divWidth + 20;
    //alert(this.textbox.offsetWidth);
    if (layerWidth < this.DivContainer.offsetWidth) {
        layerWidth = this.DivContainer.offsetWidth - 2;
    }

    this.layer.style.width = layerWidth + "px";


    if (document.getElementById(this.DivSuggestContainerID) != null) {
        //this.layer.style.left = this.getLeft() + "px";
        //this.layer.style.top = (this.getTop() + this.DivContainer.offsetHeight) + "px";
    }
    if (SiteMaxZIndex) {
        this.layer.style.zIndex = SiteMaxZIndex;
        SiteMaxZIndex += 10;
    }
    else {
        this.layer.style.zIndex = 7000;
    }

    //this.layer.style.width = this.textbox.offsetWidth +20 + 'px';
    //this.layer.style.width = this.layer.style.width;
    //this.layer.style.position = "absolute";
    //this.layer.style.zIndex = 30000;
    SetLeftTopOfDiv(this.DivScrollContainer, this.layer.id, this.textbox.id, this.ConstOffsetLeft, this.ConstOffsetTop, this.IsDisplayContainerStatic);
    HideAllSuggestionsDiv(this.layer.id);


    this.layer.style.display = "block";

    var real_height = 0;
    for (var i = 0; i < this.layer.childNodes.length; i++) {
        var oNode = this.layer.childNodes[i];
        real_height += oNode.offsetHeight;
    }

    if (aSuggestions.length > this.MaxItemsToShowAtATime) {
        real_height = 0;
        for (var i = 0; i < this.MaxItemsToShowAtATime; i++) {
            var oNode = this.layer.childNodes[i];
            real_height += oNode.offsetHeight;
        }
        this.layer.style.height = String(real_height) + "px";
    }
    else {
        this.layer.style.height = String(real_height) + "px";
    }

    if (!this.IsFunctionAjax) {
        var oThis = this;
        this.Timer = setTimeout(function() { oThis.ScrollIntoTextNode(); }, 25);

        if (!bShowOnUI) {
            this.layer.style.display = "none";
            this.ScrollIntoTextNode();
        }
    }
    var oThis = this;
    setTimeout(function() { SetLeftTopOfDiv(oThis.DivScrollContainer, oThis.layer.id, oThis.textbox.id, oThis.ConstOffsetLeft, oThis.ConstOffsetTop, oThis.IsDisplayContainerStatic); }, 25);
    //alert(arun_height);
    //alert(this.layer.style.left + " : " + this.layer.style.top);
};

/**
* Inserts a suggestion into the textbox, highlighting the 
* suggested part of the text.
* @scope private
* @param sSuggestion The suggestion for the textbox.
*/
AutoSuggestControl.prototype.typeAhead = function(sSuggestion /*:String*/) {

    //check for support of typeahead functionality
    if (this.textbox.createTextRange || this.textbox.setSelectionRange) {
        var iLen = this.textbox.value.length;
        this.textbox.value = sSuggestion[1];
        this.selectRange(iLen, sSuggestion.length);
    }
};

/*********************************************************************************/

/**
* Provides suggestions for state names (USA).
* @class
* @scope public
*/
function StateSuggestions(optionArray) {
    this.states = optionArray;
}

/**
* Request suggestions for the given autosuggest control. 
* @scope protected
* @param oAutoSuggestControl The autosuggest control to provide suggestions for.
*/
StateSuggestions.prototype.requestSuggestions = function(oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/) {
    var aSuggestions = [];
    var sTextboxValue = oAutoSuggestControl.textbox.value;

    if (sTextboxValue.length > 0) {
        //search for matching states
        for (var i = 0; i < this.states.length; i++) {
            if (!oAutoSuggestControl.FilterResultsOnClient) {
                aSuggestions.push(this.states[i]);
            }
            else {
                if (oAutoSuggestControl.IsSourceOneDimensional) {
                    if (this.states[i].toLowerCase().indexOf(sTextboxValue.toLowerCase()) == 0) {
                        aSuggestions.push(this.states[i]);
                    }
                }
                else {
                    if (this.states[i][0].toLowerCase().indexOf(sTextboxValue.toLowerCase()) == 0) {
                        aSuggestions.push(this.states[i]);
                    }
                }
            }
        }
    }

    //provide suggestions to the control
    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead, true);
};

StateSuggestions.prototype.allSuggestions = function(oAutoSuggestControl /*:AutoSuggestControl*/,
                                                          bTypeAhead /*:boolean*/, bShowOnUI) {
    var aSuggestions = [];
    for (var i = 0; i < this.states.length; i++)
        aSuggestions.push(this.states[i]);

    //provide suggestions to the control
    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead, bShowOnUI);
};
/*********************************************************************************/
/*************************************START DATE PICKER********************************************/
function FloatingDivClass(OuterDivID, DivContainerID, ElementID, ConstOffsetLeft, ConstOffsetTop, IsDisplayContainerStatic) {
    this.DivContainerID = DivContainerID;
    this.ElementID = ElementID;
    this.ConstOffsetLeft = ConstOffsetLeft;
    this.ConstOffsetTop = ConstOffsetTop;
    this.OuterDivID = OuterDivID;
    this.IsDisplayContainerStatic = IsDisplayContainerStatic;
}
/// <reference name="MicrosoftAjax.js"/>

/* This notice must be untouched at all times.
Copyright (c) 2009 Arun Mohan. All rights reserved.

DatePicker.js	 v. 1.1

Created May 14, 2009 by Arun Mohan
Last modified: May 16, 2009*/

function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent)
        while (1) {
        curleft += obj.offsetLeft;
        if (!obj.offsetParent)
            break;
        obj = obj.offsetParent;
    }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
}

function findPosY(obj) {
    var curtop = 0;
    if (obj.offsetParent)
        while (1) {
        curtop += obj.offsetTop;
        if (!obj.offsetParent)
            break;
        obj = obj.offsetParent;
    }
    else if (obj.y)
        curtop += obj.y;
    return curtop;
}

var Mode_Dates = "Dates";
var Mode_Months = "Months";
var Mode_Years = "Years";

var ContainerArray = null;

function AddToContainerArray(OuterDivID, DivContainerID, ElementID, ConstOffsetLeft, ConstOffsetTop, IsDisplayContainerStatic) {
    if (ContainerArray == null) {
        ContainerArray = new Array();
    }
    var bFound = false;
    for (var i = 0; i < ContainerArray.length; i++) {
        if (ContainerArray[i].DivContainerID == DivContainerID) {
            bFound = true;
            break;
        }
    }

    if (!bFound) {
        ContainerArray[ContainerArray.length] = new FloatingDivClass(OuterDivID, DivContainerID, ElementID, ConstOffsetLeft, ConstOffsetTop, IsDisplayContainerStatic);
    }
}

function HideContainerArrayItems() {
    if (ContainerArray == null)
        return;

    for (var i = 0; i < ContainerArray.length; i++) {
        //alert(document.getElementById(ContainerArray[i].DivContainerID).style.display);
        if (document.getElementById(ContainerArray[i].DivContainerID) != null && document.getElementById(ContainerArray[i].DivContainerID).style.display == "block") {
            document.getElementById(ContainerArray[i].DivContainerID).style.display = "none";
        }
    }
}

function SetLeftTopOfDivAll() {
    if (ContainerArray == null)
        return;

    for (var i = 0; i < ContainerArray.length; i++) {
        if (document.getElementById(ContainerArray[i].DivContainerID) != null && document.getElementById(ContainerArray[i].DivContainerID).style.display == "block") {
            SetLeftTopOfDiv(ContainerArray[i].OuterDivID, ContainerArray[i].DivContainerID, ContainerArray[i].ElementID, ContainerArray[i].ConstOffsetLeft, ContainerArray[i].ConstOffsetTop, ContainerArray[i].IsDisplayContainerStatic);
        }
    }
}

window.onresize = HandleResize;
window.onscroll = HandleScroll;

function HandleScroll() {
    //AdjustLeftTopOfAllPopupDiv();
    setTimeout("SetLeftTopOfDivAll();", 120);
    //HideContainerArrayItems();
}

function HandleResize() {
    setTimeout("SetLeftTopOfDivAll();", 120);
    //HideContainerArrayItems();
}
function getBodyScrollTop() {
    if (document.documentElement.scrollTop != null && document.documentElement.scrollTop != 0)
        return document.documentElement.scrollTop;
    else if (window.pageYOffset != null && window.pageYOffset != 0)
        return window.pageYOffset;
    else if (document.body.scrollTop != null && document.body.scrollTop != 0)
        return document.body.scrollTop;
    else
        return 0;
}

function SetLeftTopOfDiv(OuterDivID, DivContainerID, ElementID, ConstOffsetLeft, ConstOffsetTop, IsDisplayContainerStatic) {
    var scTop = 0;
    var scBodyTop = 0;
    if (document.getElementById(OuterDivID) != null) {
        scTop = document.getElementById(OuterDivID).scrollTop
    }
    //mileesh //
    //!!Emergency fix . Need permanent fix//
    if (IsDisplayContainerStatic) {
        scBodyTop = getBodyScrollTop();
    }

    //debugger;
    //alert('hello');
    var strLeft = String(findPosX(document.getElementById(ElementID)) + ConstOffsetLeft) + "px";
    var strTop = String(findPosY(document.getElementById(ElementID)) - scTop + scBodyTop + ConstOffsetTop) + "px";
    //alert(strLeft + " : " + strTop);
    document.getElementById(DivContainerID).style.left = strLeft;
    document.getElementById(DivContainerID).style.top = strTop;

    //alert(document.getElementById(DivContainerID).style.left + " : " + document.getElementById(DivContainerID).style.top);
}


function Calendar(DateFormat, IsDisplayContainerStatic, calobjectid, saveddate, txtboxid, caliconid, containerid, outerdivid, defaultdate, mindate, maxdate, yeslink, nolink, yesselclassname, noselclassname, yesclassname, noclassname, hidyesnoid, onchangefunction) {
    this.DateFormat = DateFormat;
    this.CurrentDate = null;
    this.TextBoxDate = null;
    //alert(calobjectid);
    this.DefaultDate = defaultdate;
    this.DatesToDisplay = null;
    this.MonthsToDisplay = null;
    this.DatesTwoDimensional = null;
    this.MonthsToDisplay = null;
    this.YearsToDisplay = null;
    this.Mode = Mode_Dates;
    this.Rows = 5;
    this.Columns = 7;
    this.ID = calobjectid;
    this.YearRangeLastValue = null;
    if (containerid == null) {
        this.ContainerID = "DivCalendar_" + calobjectid;
    }
    else {
        this.ContainerID = containerid;
    }
    this.TextBoxID = txtboxid;
    this.CalendarIconID = caliconid;
    this.Fade = false;
    this.FadeDelay = 500;
    this.FadeStart = 30;
    this.FadeEnd = 100;
    /*CSS*/
    this.DivHeaderClassName = "cal-header";
    this.PreviousLinkClassName = "prev";
    this.NextLinkClassName = "next";
    this.DivFooterClassName = "cal-footer";
    this.CloseLinkClassName = "close right";
    this.DivOuterClassName = "cal-ctrl";
    this.DivAClassName = "cal-top-shadow";
    this.DivBClassName = "cal-content-border";
    this.DivBInnerClassName = "cal-content";
    this.DivContentClassName = "cal-table"
    this.DivCClassName = "cal-bottom-shadow";
    this.DivDatesClassName = "day";
    this.DivMonthsClassName = "month";
    this.DivYearsClassName = "year";
    this.DivWeekNamesClassName = "cal-week-head";
    this.DivClearClassName = "clear-l";
    this.DivDateNotInThisMonthClassName = "notthismonth";
    this.DivThisMonthClassName = "thismonth";
    this.DivThisYearClassName = "thisyear";
    this.DivDateTodayClassName = "today";
    this.DivDateDisabledClassName = "disabled";
    this.DivDateDisabledNotThisMonthClassName = "disabled-notthismonth"
    this.OuterDivID = outerdivid;
    this.ConstOffsetLeft = -1;
    this.ConstOffsetTop = 17;
    this.IsDisplayContainerStatic = IsDisplayContainerStatic;
    if (mindate == null) {
        this.MinDate = new Date(1900, 0, 1);
    }
    else {
        this.MinDate = mindate;
    }
    if (maxdate == null) {
        this.MaxDate = new Date(2100, 11, 31);
    }
    else {
        this.MaxDate = maxdate;
    }

    this.YesLink = yeslink;
    this.NoLink = nolink;
    this.YesSelClassName = yesselclassname;
    this.NoSelClassName = noselclassname;
    this.YesClassName = yesclassname;
    this.NoClassName = noclassname;
    this.HidYesNoId = hidyesnoid;
    this.SavedDate = saveddate.length > 0 ? saveddate : null;
    this.OnChangeFunction = onchangefunction;
    /*CSS*/
    //this.Initialize();
}

Calendar.prototype.IsValidDate = function() {
    var dt = this.GetTextBoxDate();

    if (dt != null) {
        if (this.YesLink.length > 0 && this.NoLink.length > 0) {
            OnYesNoClicked(null, true, true, this.YesLink, this.NoLink, this.YesSelClassName, this.NoSelClassName, this.YesClassName, this.NoClassName, this.HidYesNoId, this.TextBoxID, eval(this.ID));
        }

        this.ShowCalendar();
    }
};

Calendar.prototype.DisableCalendar = function() {
    var txtBox = document.getElementById(this.TextBoxID);
    var calIcon = document.getElementById(this.CalendarIconID);

    if (txtBox.attachEvent) {
        txtBox.onclick = new Function("return false;");
    }
    else {
        txtBox.setAttribute("onclick", "return false;");
    }

    if (calIcon.attachEvent) {
        calIcon.onclick = new Function("return false;");
    }
    else {
        calIcon.setAttribute("onclick", "return false;");
    }

    calIcon.style.cursor = "default";
};

Calendar.prototype.EnableCalendar = function() {
    var txtBox = document.getElementById(this.TextBoxID);
    var calIcon = document.getElementById(this.CalendarIconID);

    if (txtBox.attachEvent) {
        txtBox.onclick = new Function("ShowCalendar(event," + this.ID + ");");
    }
    else {
        txtBox.setAttribute("onclick", "ShowCalendar(event," + this.ID + ");");
    }

    if (calIcon.attachEvent) {
        calIcon.onclick = new Function("ShowCalendar(event," + this.ID + ");");
    }
    else {
        calIcon.setAttribute("onclick", "ShowCalendar(event," + this.ID + ");");
    }

    calIcon.style.cursor = "pointer";
};

Calendar.prototype.GetCalendarContainer = function() {
    if (document.getElementById(this.ContainerID) == null) {
        var DivContainer = document.createElement("div");
        if (DivContainer.attachEvent) {
            DivContainer.onclick = new Function("StopEvent(event);");
        }
        else {
            DivContainer.setAttribute("onclick", "StopEvent(event);");
        }
        DivContainer.id = this.ContainerID;
        DivContainer.style.width = "163px";
        document.body.appendChild(DivContainer);
    }
    else {
        var DivContainer = document.getElementById(this.ContainerID);
        if (DivContainer.attachEvent) {
            DivContainer.onclick = new Function("StopEvent(event);");
        }
        else {
            DivContainer.setAttribute("onclick", "StopEvent(event);");
        }
    }

    AddToContainerArray(this.OuterDivID, this.ContainerID, this.TextBoxID, this.ConstOffsetLeft, this.ConstOffsetTop, this.IsDisplayContainerStatic);

    return DivContainer;
};

Calendar.prototype.Initialize = function() {
    if (this.Mode == Mode_Dates) {
        this.Rows = 5;
        this.Columns = 7;
        this.DatesToDisplay = this.GetDatesToDisplay(this.CurrentDate.getMonth() + 1, this.CurrentDate.getFullYear());
        if (this.DatesToDisplay.length > 35) {
            this.Rows = 6;
        }

        this.DatesTwoDimensional = new Array(this.Rows * this.Columns);
        var iCell = 0;
        for (iRow = 0; iRow < this.Rows; iRow++) {
            for (iColumn = 0; iColumn < this.Columns; iColumn++) {
                this.DatesTwoDimensional[iCell] = new Array(2);
                this.DatesTwoDimensional[iRow][iColumn] = this.DatesToDisplay[iCell];
                iCell++;
            }
        }
    }
    else if (this.Mode == Mode_Months) {
        this.MonthsToDisplay = this.GetMonthsToDisplay();
        this.Rows = 4;
        this.Columns = 3;
        var iCell = 0;
        for (iRow = 0; iRow < this.Rows; iRow++) {
            for (iColumn = 0; iColumn < this.Columns; iColumn++) {
                this.DatesTwoDimensional[iCell] = new Array(2);
                this.DatesTwoDimensional[iRow][iColumn] = this.MonthsToDisplay[iCell];
                iCell++;
            }
        }
    }
    else if (this.Mode == Mode_Years) {
        this.YearsToDisplay = this.GetYearsToDisplay();
        this.Rows = 4;
        this.Columns = 3;
        var iCell = 0;
        for (iRow = 0; iRow < this.Rows; iRow++) {
            for (iColumn = 0; iColumn < this.Columns; iColumn++) {
                this.DatesTwoDimensional[iCell] = new Array(2);
                this.DatesTwoDimensional[iRow][iColumn] = this.YearsToDisplay[iCell];
                iCell++;
            }
        }
    }
};

Calendar.prototype.RemoveEmptyArrayElements = function(strArray) {
    var retArray = new Array();
    for (var i = 0; i < strArray.length; i++) {
        if (strArray[i].length > 0) {
            retArray[retArray.length] = strArray[i];
        }
    }

    return retArray;
};

Calendar.prototype.GetTextBoxDate = function() {
    var dt = null;
    var txtVal = document.getElementById(this.TextBoxID).value;

    if (txtVal.length == 0 && this.SavedDate != null && this.SavedDate.length > 0) {
        txtVal = this.SavedDate;
    }

    var bDateFound = false;
    var strSeperator = '/';
    if (txtVal.indexOf(".") != -1) {
        strSeperator = '.';
    }
    else if (txtVal.indexOf("-") != -1) {
        strSeperator = '-';
    }

    if (txtVal.length > 0) {
        if (txtVal.indexOf(strSeperator) != -1) {
            var strValArray = this.RemoveEmptyArrayElements(txtVal.split(strSeperator));
            //alert(strValArray.length);
            if (strValArray.length == 3) {
                var iMonth = strValArray[0] * 1;
                var iDay = strValArray[1] * 1;

                if (this.DateFormat == "DDMMYYYY") {
                    iMonth = strValArray[1] * 1;
                    iDay = strValArray[0] * 1;
                }

                var iYear = strValArray[2] * 1;

                var iDaysInAMonth = this.DaysInAMonth(iMonth, iYear);

                if (String(iYear).length == 4 && String(iMonth).length <= 2 && iMonth > 0 && iMonth <= 12 && this.MinDate != null && iYear >= 1900 && iYear <= 9999 && iDay <= iDaysInAMonth) {
                    dt = new Date(iYear, iMonth - 1, iDay);
                }
            }
            else if (strValArray.length == 2) {
                var iMonth = strValArray[0] * 1;
                var iYear = strValArray[1] * 1;

                if (String(iYear).length == 4 && String(iMonth).length <= 2 && iMonth > 0 && iMonth <= 12 && iYear >= 1900 && iYear <= 9999) {
                    dt = new Date(iYear, iMonth - 1, 1);
                }
            }
        }
        else {
            if (txtVal.length == 4) {
                dt = new Date(txtVal, 0, 1);
            }
        }
    }

    //document.getElementById("mydt").value = dt;
    if (dt == null || dt == "Invalid Date" || dt == "NaN") {
        bDateFound = false;
    }
    else {
        bDateFound = true;
    }

    if (bDateFound) {
        return dt;
    }
    else {
        return null;
    }
};

Calendar.prototype.ShowCalendar = function() {

    var dt = this.GetTextBoxDate();
    //alert(dt);
    if (dt == null) {
        if (this.DefaultDate != null) {
            dt = this.DefaultDate;
            dt = this.DefaultDate;
        }
        else {
            dt = new Date();
            dt = new Date();
        }
    }
    else {

    }

    this.CurrentDate = dt;
    this.TextBoxDate = dt;
    //alert(this.DefaultDate);
    //alert(dt);
    //alert(this.CurrentDate);
    this.Mode = Mode_Dates;
    this.Initialize();

    this.RenderCalendar();

    this.GetCalendarContainer().style.position = "absolute";
    this.GetCalendarContainer().style.zIndex = 30000;
    SetLeftTopOfDiv(this.OuterDivID, this.ContainerID, this.TextBoxID, this.ConstOffsetLeft, this.ConstOffsetTop, this.IsDisplayContainerStatic);
    this.GetCalendarContainer().style.display = "block";
};


Calendar.prototype.HideCalendar = function() {
    var txtVal = document.getElementById(this.TextBoxID).value;
    this.GetCalendarContainer().style.display = "none";
};

Calendar.prototype.RenderCalendar = function() {
    /*
    <div class="cal-ctrl">
    <div class="cal-top-shadow">
    </div>
    <div class="cal-content-border">
    <div class="cal-content">
    </div>
    </div>
    <div class="cal-bottom-shadow">
    </div>
    </div>
    */

    var DivOuter = document.createElement("div");
    DivOuter.className = this.DivOuterClassName;

    var DivA = document.createElement("div");
    DivA.className = this.DivAClassName;
    DivOuter.appendChild(DivA);

    var DivB = document.createElement("div");
    DivB.className = this.DivBClassName;

    var DivBInner = document.createElement("div");
    DivBInner.className = this.DivBInnerClassName;

    var DivContent = document.createElement("div");
    DivContent.className = this.DivContentClassName;

    DivBInner.appendChild(this.CreateHeader());
    if (this.Mode == Mode_Dates) {
        DivContent.appendChild(this.RenderDates());
    }
    else if (this.Mode == Mode_Months) {
        DivContent.appendChild(this.RenderMonths());
    }
    else if (this.Mode == Mode_Years) {
        DivContent.appendChild(this.RenderYears());
    }

    DivBInner.appendChild(DivContent);

    DivBInner.appendChild(this.CreateFooter());

    DivB.appendChild(DivBInner);
    DivOuter.appendChild(DivB);

    var DivC = document.createElement("div");
    DivC.className = this.DivCClassName;
    DivOuter.appendChild(DivC);

    this.GetCalendarContainer().innerHTML = "";
    this.GetCalendarContainer().appendChild(DivOuter);

    if (this.Fade) {
        Fade(this.ContainerID, this.FadeStart, this.FadeEnd, this.FadeDelay);
    }

    //    if (this.GetCalendarContainer().attachEvent) {
    //        this.GetCalendarContainer().onblur = new Function("alert('blur');eval(" + this.ID + ").HideCalendar();");
    //    }
    //    else {
    //        this.GetCalendarContainer().setAttribute("onblur", "alert('blur');eval(" + this.ID + ").HideCalendar();");
    //    }
};

Calendar.prototype.ProcessKeyDown = function(event) {
    if (event.keyCode == 9) {
        this.HideCalendar();
        return false;
    }

    return true;
};

Calendar.prototype.SetMode = function(mode) {
    this.YearRangeLastValue = null;
    this.Mode = mode;
    this.Initialize();
    this.RenderCalendar();
};

Calendar.prototype.ShowNext = function() {
    if (this.Mode == Mode_Dates) {
        this.CurrentDate.setDate(1);
        this.CurrentDate.setMonth(this.CurrentDate.getMonth() + 1);
    }
    else if (this.Mode == Mode_Months) {
        this.CurrentDate.setDate(1);
        this.CurrentDate.setYear(this.CurrentDate.getFullYear() + 1);
    }
    else if (this.Mode == Mode_Years) {
    }

    this.Initialize();
    this.RenderCalendar();
};

Calendar.prototype.ShowPrevious = function() {
    if (this.Mode == Mode_Dates) {
        this.CurrentDate.setDate(1);
        this.CurrentDate.setMonth(this.CurrentDate.getMonth() - 1);
    }
    else if (this.Mode == Mode_Months) {
        this.CurrentDate.setDate(1);
        this.CurrentDate.setYear(this.CurrentDate.getFullYear() - 1);
    }
    else if (this.Mode == Mode_Years) {
        this.YearRangeLastValue -= 20;
    }
    this.Initialize();
    this.RenderCalendar();
};

Calendar.prototype.ShowThisMonth = function() {
    this.CurrentDate = new Date();
    this.Mode = Mode_Dates;
    this.YearRangeLastValue = null;
    //this.CurrentDate.setMonth(this.CurrentDate.getMonth() - 1);
    //alert(this.CurrentDate);
    this.Initialize();
    this.RenderCalendar();
};

Calendar.prototype.OnMonthClicked = function(month) {
    this.CurrentDate.setMonth(month);
    this.Mode = Mode_Dates;
    this.Initialize();
    this.RenderCalendar();
};

Calendar.prototype.OnYearClicked = function(year) {
    this.CurrentDate.setYear(year);
    this.Mode = Mode_Months;
    this.Initialize();
    this.RenderCalendar();
};

Calendar.prototype.OnSetDate = function(date) {
    if (document.getElementById(this.TextBoxID) != null) {
        var dt = new Date(date);
        strDay = (dt.getDate()) < 10 ? '0' + String(dt.getDate()) : String(dt.getDate());
        var obj = eval(this.ID);

        strMonth = (dt.getMonth() + 1) < 10 ? '0' + String(dt.getMonth() + 1) : String(dt.getMonth() + 1);
        if (this.DateFormat == "DDMMYYYY") {
            document.getElementById(this.TextBoxID).value = strDay + "/" + strMonth + "/" + dt.getFullYear();
        }
        else {
            document.getElementById(this.TextBoxID).value = strMonth + "/" + strDay + "/" + dt.getFullYear();
        }
        this.GetCalendarContainer().style.display = "none";

        if (this.OnChangeFunction != null) {
            eval(this.OnChangeFunction);
        }
        if (this.YesLink.length > 0 && this.NoLink.length > 0) {
            OnYesNoClicked(null, true, true, this.YesLink, this.NoLink, this.YesSelClassName, this.NoSelClassName, this.YesClassName, this.NoClassName, this.HidYesNoId, this.TextBoxID, eval(this.ID));
        }
    }
}

Calendar.prototype.CreateHeader = function() {
    /*
    <div class="cal-header">
    <a href="#" class="prev" title="previous">Prev</a><a href="#" class="next" title="Next">
    Next</a> <a href="#">December, 2009</a>
    </div>
    */

    var DivHeader = document.createElement("div");
    DivHeader.className = this.DivHeaderClassName;

    var PrevLink = document.createElement("a");
    PrevLink.className = this.PreviousLinkClassName;
    PrevLink.href = "javascript:;";
    AttachCalEvent(PrevLink, "eval(" + this.ID + ").ShowPrevious();");
    //    var PrevLinkText = document.createTextNode("Prev");
    //    PrevLink.appendChild(PrevLinkText);
    DivHeader.appendChild(PrevLink);

    var NextLink = document.createElement("a");
    NextLink.className = this.NextLinkClassName;
    NextLink.href = "javascript:;";
    AttachCalEvent(NextLink, "eval(" + this.ID + ").ShowNext();");
    //    var NextLinkText = document.createTextNode("Next");
    //    NextLink.appendChild(NextLinkText);
    DivHeader.appendChild(NextLink);

    var HeadingLink = document.createElement("a");
    var strHeadingText = "";
    var strOnClick = "";
    if (this.Mode == Mode_Dates) {
        strHeadingText = monthArray[this.CurrentDate.getMonth()] + ", " + this.CurrentDate.getFullYear();
        strOnClick = "eval(" + this.ID + ").SetMode('Months');";
    }
    else if (this.Mode == Mode_Months) {
        strHeadingText = this.CurrentDate.getFullYear();
        strOnClick = "eval(" + this.ID + ").SetMode('Years');";
    }
    else if (this.Mode == Mode_Years) {
        strHeadingText = (this.YearsToDisplay[0] + 1) + " - " + (this.YearsToDisplay[this.YearsToDisplay.length - 1] - 1);
        strOnClick = "eval(" + this.ID + ").SetMode('Years');";
    }

    HeadingLink.href = "javascript:;";
    if (this.Mode != Mode_Years) {
        AttachCalEvent(HeadingLink, strOnClick);
    }
    var HeadingLinkText = document.createTextNode(strHeadingText);
    HeadingLink.appendChild(HeadingLinkText);
    DivHeader.appendChild(HeadingLink);

    return DivHeader;
};

Calendar.prototype.CreateFooter = function() {
    /*
    <div class="cal-footer">
    <a href="#" class="close right">close</a> <a href="#">This Month</a>
    </div>
    */

    var __strMonth = "This Month";
    var __strClose = "Close";

    try {
        __strMonth = str_Month;
        __strClose = strClose;
    }
    catch (e) {
    }
    
    var DivFooter = document.createElement("div");
    DivFooter.className = this.DivFooterClassName;

    var closeLink = document.createElement("a");
    closeLink.className = this.CloseLinkClassName;
    closeLink.href = "javascript:;";
    AttachCalEvent(closeLink, "eval(" + this.ID + ").HideCalendar();");
    var closeLinkText = document.createTextNode(__strClose);
    closeLink.appendChild(closeLinkText);
    DivFooter.appendChild(closeLink);

    var ThisMonthLink = document.createElement("a");
    ThisMonthLink.href = "javascript:;";
    AttachCalEvent(ThisMonthLink, "eval(" + this.ID + ").ShowThisMonth();");

    var ThisMonthLinkText = document.createTextNode(__strMonth);
    ThisMonthLink.appendChild(ThisMonthLinkText);
    DivFooter.appendChild(ThisMonthLink);

    return DivFooter;
};

/*******************************Start Dates Specific*************************************/
Calendar.prototype.GetDatesToDisplay = function(month, year) {

    var dateArray = new Array();
    for (i = 1; i <= this.DaysInAMonth(month, year); i++) {
        dateArray[i - 1] = new Date(year, month - 1, i);
    }

    var compositeArray = new Array();

    var iCount = 0;
    var prevMonthArray = this.GetPreviousMonthDates(this.GetPreviousMonthDatesCount(dateArray[0]), month, year);
    //alert(prevMonthArray.length);
    for (i = 0; i < prevMonthArray.length; i++) {
        compositeArray[iCount] = prevMonthArray[i];
        iCount++;
    }

    for (i = 0; i < dateArray.length; i++) {
        compositeArray[iCount] = dateArray[i];
        iCount++;
    }

    var bDoubleCount = false;
    //alert(42 - iCount - 1);
    if (42 - iCount - 1 >= 7) {
        bDoubleCount = true;
    }

    //alert(this.GetNextMonthDatesCount(bDoubleCount, dateArray[dateArray.length - 1]));

    var nextMonthArray = this.GetNextMonthDates(42 - iCount, month, year);
    //alert(nextMonthArray.length);
    for (i = 0; i < nextMonthArray.length; i++) {
        compositeArray[iCount] = nextMonthArray[i];
        iCount++;
    }

    return compositeArray;
};

Calendar.prototype.GetPreviousMonthDates = function(count, month, year) {
    month -= 1;
    var dateArray = new Array();
    for (i = 1; i <= this.DaysInAMonth(month, year); i++) {
        dateArray[i - 1] = new Date(year, month - 1, i);
    }

    var prevDatesArray = new Array();
    var index = 0;
    for (i = dateArray.length - count; i < dateArray.length; i++) {
        prevDatesArray[index] = dateArray[i];
        index++;
    }

    return prevDatesArray;
};

Calendar.prototype.GetNextMonthDates = function(count, month, year) {
    month += 1;
    var dateArray = new Array();
    for (i = 1; i <= this.DaysInAMonth(month, year); i++) {
        dateArray[i - 1] = new Date(year, month - 1, i);
    }

    var nextDatesArray = new Array();
    for (i = 0; i < count; i++) {
        nextDatesArray[i] = dateArray[i];
    }

    return nextDatesArray;
};


Calendar.prototype.GetPreviousMonthDatesCount = function(date) {
    return date.getDay();
};

/******************Dates Specific*************************************/

Calendar.prototype.GetNextMonthDatesCount = function(bDoubleCount, date) {
    if (bDoubleCount)
        return (13 - date.getDay());
    else
        return (6 - date.getDay());
};

Calendar.prototype.DaysInAMonth = function(month, year) {
    var dd = new Date(year, month, 0);
    return dd.getDate();
};

Calendar.prototype.RenderDates = function() {
    /*
    <div class="day">
    <div class="cal-week-head">
    <span>S</span> <span>M</span> <span>T</span> <span>W</span> <span>T</span> <span>F</span>
    <span>S</span>
    </div>
    <a href="#">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">
    5</a> <a href="#">6</a> <a href="#">7</a> <a href="#">8</a> <a href="#">9</a>
    <a href="#">10</a> <a href="#">11</a> <a href="#">12</a> <a href="#">13</a> <a href="#">
    14</a> <a href="#">15</a> <a href="#">16</a> <a href="#" class="today">17</a>
    <a href="#">18</a> <a href="#">19</a> <a href="#">20</a> <a href="#">21</a> <a href="#">
    22</a> <a href="#">23</a> <a href="#">24</a> <a href="#">25</a> <a href="#">26</a>
    <a href="#">27</a> <a href="#">28</a> <a href="#">29</a> <a href="#">30</a> <a href="#">
    31</a> <a href="#" class="notthismonth">1</a> <a href="#" class="notthismonth">2</a>
    <a href="#" class="notthismonth">3</a> <a href="#" class="notthismonth">4</a>
    <div class="clear-l">
    </div>
    </div>
    */

    var DivDates = document.createElement("div");
    DivDates.className = this.DivDatesClassName;

    var DivWeekNames = document.createElement("div");
    DivWeekNames.className = this.DivWeekNamesClassName;

    for (iColumn = 0; iColumn < this.Columns; iColumn++) {
        var spanWeekName = document.createElement("span");
        spanWeekName.innerHTML = weekday[iColumn];
        DivWeekNames.appendChild(spanWeekName);
    }

    DivDates.appendChild(DivWeekNames);
    //alert(this.MinDate);
    //alert(this.MaxDate);
    for (var i = 0; i < this.DatesToDisplay.length; i++) {
        var dateLink = document.createElement("a");
        if (this.CurrentDate.getMonth() != this.DatesToDisplay[i].getMonth()) {
            dateLink.className = this.DivDateNotInThisMonthClassName;
        }

        if (this.TextBoxDate.getDate() == this.DatesToDisplay[i].getDate() && this.TextBoxDate.getMonth() == this.DatesToDisplay[i].getMonth() && this.TextBoxDate.getFullYear() == this.DatesToDisplay[i].getFullYear()) {
            dateLink.className = this.DivDateTodayClassName;
        }

        dateLink.href = "javascript:;";
        if (Date.parse(this.DatesToDisplay[i]) >= Date.parse(this.MinDate) && Date.parse(this.DatesToDisplay[i]) <= Date.parse(this.MaxDate)) {
            AttachCalEvent(dateLink, "eval(" + this.ID + ").OnSetDate('" + this.DatesToDisplay[i] + "');");
        }
        else {
            if (this.CurrentDate.getMonth() != this.DatesToDisplay[i].getMonth()) {
                dateLink.className = this.DivDateDisabledNotThisMonthClassName;
            }
            else {
                dateLink.className = this.DivDateDisabledClassName;
            }
        }
        //AttachCalEvent(dateLink, "eval(" + this.ID + ").OnSetDate('" + this.DatesToDisplay[i] + "');");
        var dateLinkText = document.createTextNode(this.DatesToDisplay[i].getDate());
        dateLink.appendChild(dateLinkText);
        DivDates.appendChild(dateLink);
    }

    var DivClear = document.createElement("div");
    DivClear.className = this.DivClearClassName;
    DivDates.appendChild(DivClear);

    return DivDates;
}

/*******************************Months***************************************/

Calendar.prototype.GetMonthsToDisplay = function(year) {
    var monthArray = new Array("0", "1", "2", "3", "4", "5", "6", "7",
                    "8", "9", "10", "11");
    return monthArray;
};

Calendar.prototype.RenderMonths = function() {

    /*
    <div class="month">
    <a href="#">Jan</a> <a href="#">Feb</a> <a href="#">Mar</a> <a href="#">Apr</a>
    <a href="#">May</a> <a href="#" class="thismonth">Jun</a> <a href="#">Jul</a> <a
    href="#">Aug</a> <a href="#">Sep</a> <a href="#">Oct</a> <a href="#">Nov</a>
    <a href="#">Dec</a>
    <div class="clear-l">
    </div>
    </div>
    */

    var DivMonths = document.createElement("div");
    DivMonths.className = this.DivMonthsClassName;

    for (var i = 0; i < this.MonthsToDisplay.length; i++) {
        var monthLink = document.createElement("a");
        //        if (this.CurrentDate.getMonth() == this.MonthsToDisplay[i] && this.CurrentDate.getFullYear() == (new Date()).getFullYear()) {
        //            monthLink.className = this.DivThisMonthClassName;
        //        }
        monthLink.href = "javascript:;";

        AttachCalEvent(monthLink, "eval(" + this.ID + ").OnMonthClicked('" + this.MonthsToDisplay[i] + "');");
        var monthLinkText = document.createTextNode(monthArray[this.MonthsToDisplay[i]]);
        monthLink.appendChild(monthLinkText);
        DivMonths.appendChild(monthLink);
    }

    var DivClear = document.createElement("div");
    DivClear.className = this.DivClearClassName;
    DivMonths.appendChild(DivClear);

    return DivMonths;
};

/***********************************End Month***************************************/

/***********************************Start Year***************************************/

Calendar.prototype.GetYearsToDisplay = function(year) {
    var year = this.YearRangeLastValue;

    if (year == null) {
        year = this.CurrentDate.getFullYear();
    }

    while (year % 10 != 0) {
        year--;
    }

    var yearArray = new Array();
    var index = 0;
    for (i = year - 1; i <= year + 10; i++) {
        yearArray[index] = i;
        index++;
    }

    this.YearRangeLastValue = yearArray[yearArray.length - 2] + 1;

    return yearArray;
};

Calendar.prototype.RenderYears = function() {
    /*
    <div class="year">
    <a href="#">1999</a> <a href="#">2000</a> <a href="#">2001</a> <a href="#">2002</a>
    <a href="#">2003</a> <a href="#">2004</a> <a href="#">2005</a> <a href="#">2006</a>
    <a href="#" class="thisyear">2007</a> <a href="#">2008</a> <a href="#">2009</a>
    <div class="clear-l">
    </div>
    </div>  
    */

    var DivYears = document.createElement("div");
    DivYears.className = this.DivYearsClassName;

    for (var i = 0; i < this.YearsToDisplay.length; i++) {
        var YearLink = document.createElement("a");
        //        if ((new Date()).getFullYear() == this.YearsToDisplay[i]) {
        //            YearLink.className = this.DivThisYearClassName;
        //        }
        YearLink.href = "javascript:;";

        AttachCalEvent(YearLink, "eval(" + this.ID + ").OnYearClicked('" + this.YearsToDisplay[i] + "');");
        var YearLinkText = document.createTextNode(this.YearsToDisplay[i]);
        YearLink.appendChild(YearLinkText);
        DivYears.appendChild(YearLink);
    }

    var DivClear = document.createElement("div");
    DivClear.className = this.DivClearClassName;
    DivYears.appendChild(DivClear);

    return DivYears;
};

/***********************************End Year***************************************/

function ShowCalendar(e, obj) {
    HideContainerArrayItems();
    eval(obj).ShowCalendar();
}

function Fade(id, opacStart, opacEnd, millisec) {
    //speed for each frame
    var speed = Math.round(millisec / 100);
    var timer = 0;

    //determine the direction for the blending, if start and end are the same nothing happens
    if (opacStart > opacEnd) {
        for (i = opacStart; i >= opacEnd; i--) {
            setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
            timer++;
        }
    } else if (opacStart < opacEnd) {
        for (i = opacStart; i <= opacEnd; i++) {
            setTimeout("changeOpac(" + i + ",'" + id + "')", (timer * speed));
            timer++;
        }
    }
}

//change the opacity for different browsers
function changeOpac(opacity, id) {
    var object = document.getElementById(id).style;

    // IE/Win 
    object.filter = "alpha(opacity=" + opacity + ")";

    // Safari<1.2, Konqueror
    object.KhtmlOpacity = (opacity / 100);

    // Older Mozilla and Firefox
    object.MozOpacity = (opacity / 100);

    // Safari 1.2, newer Firefox and Mozilla, CSS3
    object.opacity = (opacity / 100);
}

function AttachCalEvent(domEle, jsFunction) {
    if (domEle.attachEvent) {
        domEle.onclick = new Function(jsFunction);
    }
    else {
        domEle.setAttribute("onclick", jsFunction);
    }
}
/*************************************END DATE PICKER********************************************/
/*************************************HIDE MENUS*************************************************/
document.onclick = HideGRCMenus;
function HideGRCMenus() {
    var cbdiv = document.getElementsByTagName("span");

    for (var i = 0; i < cbdiv.length; i++) {
        if (cbdiv[i].style != null
            && cbdiv[i].style.display != null
            && cbdiv[i].style.display == "block"
            && cbdiv[i].id.indexOf("_cblayer") != -1) {
            cbdiv[i].style.display = "none";
            cbdiv[i].parentNode.style.position = "static";
        }
        //added by arun to fix the tooltip issue
        if (cbdiv[i].id.indexOf("grc_webcontrols_tip_div_") != -1) {
            cbdiv[i].style.display = "none";
        }
    }

    HideContainerArrayItems();

    var cbdp = document.getElementsByTagName("div");

    //    for (var i = 0; i < cbdp.length; i++) {
    //        if (cbdp[i].style != null
    //            && cbdp[i].style.visibility != null
    //            && cbdp[i].style.visibility == "visible"
    //            && cbdp[i].id.indexOf(datePickerDivID) != -1) {
    //            cbdp[i].style.visibility = "hidden";
    //            cbdp[i].style.display = "none";
    //        }
    //    }

    var cbdpi = document.getElementsByTagName("iframe");

    for (var i = 0; i < cbdpi.length; i++) {
        if (cbdpi[i].style != null
            && cbdpi[i].style.visibility != null
            && cbdpi[i].style.visibility == "visible"
            && cbdpi[i].id.indexOf(iFrameDivID) != -1) {
            cbdpi[i].style.visibility = "hidden";
            cbdpi[i].style.display = "none";
        }
    }

}
/*************************************HIDE MENUS*************************************************/



/*************************************TIPS FOR CONTROLS*************************************************/

function SetGRCCBValue(theImg, HiddenValueFieldName, DivArrayName, SelImageUrl, NoSelImageUrl, strOnClientClick) {
    if (theImg.src.toLowerCase().indexOf(NoSelImageUrl.toLowerCase()) != -1) {
        theImg.src = SelImageUrl;
        document.getElementById(HiddenValueFieldName).value = '1';
        //debugger;
        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(true);
        }
    }
    else {
        theImg.src = NoSelImageUrl;
        document.getElementById(HiddenValueFieldName).value = '0';
        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(false);
        }
    }

    for (i = 0; i < DivArrayName.length; i++) {
        var theVal = DivArrayName[i];
        if (theImg.getAttribute('id') != theVal)
            document.getElementById(theVal).src = NoSelImageUrl;
    }
}

function SetGRCRBLValue(theImg, HiddenValueFieldName, DivArrayName, SelImageUrl, NoSelImageUrl, strOnClientClick) {
    document.getElementById(HiddenValueFieldName).value = theImg.getAttribute('value');

    if (theImg.src.toLowerCase().indexOf(NoSelImageUrl.toLowerCase()) != -1) {
        theImg.src = SelImageUrl;
        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(true);
        }
    }
    else {
    }

    for (i = 0; i < DivArrayName.length; i++) {
        var theVal = DivArrayName[i];
        if (theImg.getAttribute('id') != theVal)
            document.getElementById(theVal).src = NoSelImageUrl;

        if (strOnClientClick.length > 0) {
            eval(strOnClientClick)(false);
        }
    }
}

/*Start Time Conversions*/

function InitializeTimeControl(HidItemID, TxtBoxID) {
    var ObjHidItem = document.getElementById(HidItemID);
    var ObjTxtBox = document.getElementById(TxtBoxID);

    if (ObjHidItem != null && ObjTxtBox != null) {
        ObjHidItem.value = GetFormattedTimeString(GetHourHalfHourAdjustedTime());
        ObjTxtBox.value = GetFormattedTimeToShow(GetHourHalfHourAdjustedTime());
    }
}

function GetHourHalfHourAdjustedTime() {
    var dt = new Date();

    var iMinutes = dt.getMinutes();
    var iHours = dt.getHours();
    if (iMinutes <= 15) {
        iMinutes = 0;
    }
    else if (iMinutes >= 45) {
        iMinutes = 0;
        if (iHours == 23) {
            iMinutes = 30;
        }
        else {
            iHours += 1;
        }
    }
    else {
        iMinutes = 30;
    }


    dt.setHours(iHours, iMinutes, 0, 0);


    return dt;
}

function GetFormattedTimeToShow(dt) {
    var dtGFTTS = new Date(dt);

    var iMinutes = dtGFTTS.getMinutes();
    var iHours = dtGFTTS.getHours();

    if (iHours == 0) {
        if (iMinutes < 10) {
            return "12" + ":0" + iMinutes + " AM";
        }
        else {
            return "12" + ":" + iMinutes + " AM";
        }
    }
    else if (iHours < 10) {
        if (iMinutes < 10) {
            return "0" + iHours + ":0" + iMinutes + " AM";
        }
        else {
            return "0" + iHours + ":" + iMinutes + " AM";
        }
    }
    else if (iHours < 12) {
        if (iMinutes < 10) {
            return iHours + ":0" + iMinutes + " AM";
        }
        else {
            return iHours + ":" + iMinutes + " AM";
        }
    }
    else if (iHours == 12) {
        if (iMinutes < 10) {
            return iHours + ":0" + iMinutes + " PM";
        }
        else {
            return iHours + ":" + iMinutes + " PM";
        }
    }
    else if (iHours == 23) {
        if (iMinutes < 10) {
            return (iHours - 12) + ":0" + iMinutes + " PM";
        }
        else {
            return (iHours - 12) + ":" + iMinutes + " PM";
        }
    }
    else {
        var hNew = iHours - 12
        if (hNew < 10) {
            hNew = "0" + hNew;
        }
        if (iMinutes < 10) {
            return hNew + ":0" + iMinutes + " PM";
        }
        else {
            return hNew + ":" + iMinutes + " PM";
        }
    }
}


function GetFormattedTimeString(dt) {
    dtGFTS = new Date(dt);

    var iMinutes = dtGFTS.getMinutes();
    var iHours = dtGFTS.getHours();

    if (iHours < 10) {
        if (dt.Minute < 10) {
            return "0" + iHours + ":0" + iMinutes;
        }
        else {
            return "0" + iHours + ":" + iMinutes;
        }
    }
    else {
        if (dt.Minute < 10) {
            return iHours + ":0" + iMinutes;
        }
        else {
            return iHours + ":" + iMinutes;
        }

    }
}
/*End Time Conversions*/

function IsBrowserFixedPositionCompatible() {
    if (navigator.appName != "Microsoft Internet Explorer"
     || navigator.appVersion.indexOf("MSIE 7.") != -1
     || navigator.appVersion.indexOf("MSIE 8.") != -1
     || navigator.appVersion.indexOf("MSIE 9.") != -1
     || navigator.appVersion.indexOf("MSIE 10.") != -1) {
        return true;
    }

    return false;
}


function isLowerCase(aCharacter) {
    return (aCharacter >= 'a') && (aCharacter <= 'z')
};


function isUpperCase(aCharacter) {
    return (aCharacter >= 'A') && (aCharacter <= 'Z')
};


function StopEvent(event) {
    event = event || window.event;

    if (event) {
        if (event.stopPropagation) event.stopPropagation();
        if (event.preventDefault) event.preventDefault();

        if (typeof event.cancelBubble != "undefined") {
            event.cancelBubble = true;
            event.returnValue = false;
        }
    }

    return false;
}

/*Start Tooltip*/

function GRCToolTip() {
    this.Initialize();
}

GRCToolTip.prototype.Initialize = function() {
    //debugger;
    if (window.addEventListener) {
        window.addEventListener('load', GRCToolTipInit, false); //W3C 
    }
    else {
        window.attachEvent('onload', GRCToolTipInit); //IE 
    }
};

GRCToolTip.prototype.getWidth = function(Obj) {
    return (Obj.offsetWidth) ? Obj.offsetWidth : Obj.clientWidth;
};

GRCToolTip.prototype.getHeight = function(Obj) {
    return (Obj.offsetHeight) ? Obj.offsetHeight : Obj.clientHeight;
};

GRCToolTip.prototype.PosX = function(obj) {
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    }
    else if (obj.x)
        curleft += obj.x;
    return curleft;
};

GRCToolTip.prototype.PosY = function(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    }
    else if (obj.y)
        curtop += obj.y;
    return curtop;
};

GRCToolTip.prototype.show = function(Obj) {

    if (Obj.getAttribute("rel") && Obj) {
        calloutDivObj = document.getElementById("calloutBox");
        calloutDivContentObj = document.getElementById("calloutBoxContent");
        if (calloutDivContentObj == null)
            return;
        calloutDivContentObj.innerHTML = Obj.getAttribute("rel");
        calloutDivObj.style.display = "block";
        var direction;
        browserW = (document.body.offsetWidth) ? document.body.offsetWidth : document.documentElement.offsetWidth;
        scrollX = (document.body.scrollLeft) ? document.body.scrollLeft : document.documentElement.scrollLeft;
        if (this.PosX(Obj) + this.getWidth(Obj) + this.getWidth(calloutDivObj) - scrollX < browserW) {
            calloutDivObj.style.left = this.PosX(Obj) + this.getWidth(Obj) + "px";
            direction = "R";
        } else {
            var iLpos = (this.PosX(Obj) - this.getWidth(calloutDivObj));
            if (iLpos < 0)
                iLpos = 0;
            calloutDivObj.style.left = iLpos + "px";
            direction = "L";
        }
        scrollY = (document.body.scrollTop) ? document.body.scrollTop : document.documentElement.scrollTop;
        if ((this.PosY(Obj) - this.getHeight(calloutDivObj) - scrollY) >= 0) {
            calloutDivObj.style.top = this.PosY(Obj) - this.getHeight(calloutDivObj) + "px";
            direction += "T";
        } else {
            calloutDivObj.style.top = this.PosY(Obj) + this.getHeight(Obj) + "px";
            direction += "B";
        }

        switch (direction) {
            case "RT":
                calloutDivObj.className = "righttop-callout";
                break;
            case "RB":
                calloutDivObj.className = "rightbottom-callout";
                break;
            case "LT":
                calloutDivObj.className = "lefttop-callout";
                break;
            case "LB":
                calloutDivObj.className = "leftbottom-callout";
                break;



        }


    }
};

GRCToolTip.prototype.hide = function() {
    calloutDivObj = document.getElementById("calloutBox");
    calloutDivContentObj = document.getElementById("calloutBoxContent");
    if (calloutDivContentObj != null)
        calloutDivContentObj.innerHTML = "";
    if (calloutDivObj != null)
        calloutDivObj.style.display = "none";
};


function GRCToolTipInit() {
    //   <div id="calloutBox">
    //        <div class="callouttop">
    //        </div>
    //        <div class="calloutmid">
    //            <div id="calloutBoxContent">
    //            </div>
    //        </div>
    //        <div class="calloutbottom">
    //        </div>
    //    </div>

    if (document.getElementById("calloutBox") != null)
        return;

    var divOuter = document.createElement("div");
    divOuter.id = "calloutBox";

    var divInner1 = document.createElement("div");
    divInner1.className = "callouttop";

    var divInner2 = document.createElement("div");
    divInner2.className = "calloutmid";

    var divInner3 = document.createElement("div");
    divInner3.className = "calloutbottom";

    var divInnerMost = document.createElement("div");
    divInnerMost.id = "calloutBoxContent";

    divInner2.appendChild(divInnerMost);

    divOuter.appendChild(divInner1);
    divOuter.appendChild(divInner2);
    divOuter.appendChild(divInner3);

    document.body.appendChild(divOuter);
}

var objGRCToolTip = new GRCToolTip();

//element.addEventListener('click', function() {
//    this.style.backgroundColor = '#cc0000'
//}, false)
//element.removeEventListener('click', spyOnUser, false)
/*End Tooltip*/