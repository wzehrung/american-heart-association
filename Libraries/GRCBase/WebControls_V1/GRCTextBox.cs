﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls_V1
{
    [ToolboxData("<{0}:GRCTextBox runat=server></{0}:GRCTextBox>")]
    [ValidationProperty("Text")]
    [Designer("System.Web.UI.Design.WebControls.ListControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public class GRCTextBox : TextBox
    {
        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["TBTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["TBTipText"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string DefaultText
        {
            get
            {
                object obj = ViewState["TBDefaultText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["TBDefaultText"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "GRCWebControlsHelper", System.Web.HttpUtility.HtmlEncode(Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls_V1.JScripts.GRCWebControlsHelper.js")));

            // create the style sheet control and put it in the document header
            string csslink = "<link rel='stylesheet' type='text/css' href='" + System.Web.HttpUtility.HtmlEncode(Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls_V1.StyleSheet.css")) + "' />";
            LiteralControl include = new LiteralControl(csslink);
            include.ID = "EmbeddedCss";
            bool bFound = false;
            foreach (Control ctrl in this.Page.Header.Controls)
            {
                if (ctrl.ID == include.ID)
                {
                    bFound = true;
                }
            }

            if (!bFound)
            {
                this.Page.Header.Controls.Add(include);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                EnsureChildControls();
            }

            if (!string.IsNullOrEmpty(DefaultText))
            {
                writer.AddAttribute("onfocus", "{if(this.value == '" + DefaultText + "')this.value = '';this.style.color = '#000';}");
                writer.AddAttribute("onblur", "{if(this.value.length == 0){this.value = '" + DefaultText + "';this.style.color = '#999';}}");
            }

            if (this.Text.Length == 0)
            {
                this.Text = DefaultText;
                if (!string.IsNullOrEmpty(DefaultText))
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#999");
                }
                else
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#000");
                }
            }
            else
            {
                if (this.Text == DefaultText)
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#999");
                }
                else
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#000");
                }
            }

            if (this.TipText != null && this.TipText.Length > 0)
            {
                writer.AddAttribute("onmouseover", "objGRCToolTip.show(this);");
                writer.AddAttribute("onmouseout", "objGRCToolTip.hide();");
                writer.AddAttribute("rel", this.TipText);
            }
            base.Render(writer);
        }
    }
}
