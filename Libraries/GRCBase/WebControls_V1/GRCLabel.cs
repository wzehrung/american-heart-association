﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls_V1
{
    [ToolboxData("<{0}:GRCLabel runat=server></{0}:GRCLabel>")]
    [Designer("System.Web.UI.Design.WebControls.ListControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public class GRCLabel : Label
    {
        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["TBTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["TBTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["TBTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["TBTipTextClass"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "GRCWebControlsHelper", System.Web.HttpUtility.HtmlEncode(Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls_V1.JScripts.GRCWebControlsHelper.js")));

            // create the style sheet control and put it in the document header
            string csslink = "<link rel='stylesheet' type='text/css' href='" + System.Web.HttpUtility.HtmlEncode(Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls_V1.StyleSheet.css")) + "' />";
            LiteralControl include = new LiteralControl(csslink);
            include.ID = "EmbeddedCss";
            bool bFound = false;
            foreach (Control ctrl in this.Page.Header.Controls)
            {
                if (ctrl.ID == include.ID)
                {
                    bFound = true;
                }
            }

            if (!bFound)
            {
                this.Page.Header.Controls.Add(include);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                EnsureChildControls();
            }

            if (this.TipText != null && this.TipText.Length > 0)
            {
                writer.AddAttribute("onmouseover", "objGRCToolTip.show(this);");
                writer.AddAttribute("onmouseout", "objGRCToolTip.hide();");
                writer.AddAttribute("rel", this.TipText);
            }
            base.Render(writer);
        }
    }
}
