﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;

namespace GRCBase.WebControls_V1
{
    [ToolboxData("<{0}:GRCComboBox runat=server></{0}:GRCComboBox>")]
    [ParseChildren(true, "Items")]
    [ControlValueProperty("SelectedValue")]
    [SupportsEventValidation()]
    [ValidationProperty("Text")]
    [Designer("System.Web.UI.Design.WebControls.ListControlDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public sealed partial class GRCComboBox : DropDownList, IPostBackDataHandler
    {
        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue(true)]
        public bool FilterResultsOnClient
        {
            get
            {
                object obj = ViewState["FilterResultsOnClient"];
                bool bVal = (obj == null) ? true : (bool)obj;
                return bVal;
            }
            set
            {
                ViewState["FilterResultsOnClient"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue(true)]
        public bool IsAutoSuggest
        {
            get
            {
                object obj = ViewState["IsAutoSuggest"];
                bool bVal = (obj == null) ? false : (bool)obj;
                return bVal;
            }
            set
            {
                ViewState["IsAutoSuggest"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue(true)]
        public bool IsClientFunctionAjax
        {
            get
            {
                object obj = ViewState["IsClientFunctionAjax"];
                bool bVal = (obj == null) ? true : (bool)obj;
                return bVal;
            }
            set
            {
                ViewState["IsClientFunctionAjax"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string ClientSuggestionsArrayName
        {
            get
            {
                object obj = ViewState["ClientSuggestionsArrayName"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["ClientSuggestionsArrayName"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string ClientSuggestionsFunctionName
        {
            get
            {
                object obj = ViewState["ClientSuggestionsFunctionName"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["ClientSuggestionsFunctionName"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue(2)]
        public int CBMinimumPrefixLength
        {
            get
            {
                object obj = ViewState["CBMinimumPrefixLength"];
                int iLength = (obj == null) ? 1 : (int)obj;
                return iLength;
            }
            set
            {
                ViewState["CBMinimumPrefixLength"] = value;
            }
        }

        private string CBDDlContainerID
        {
            get
            {
                return String.Format("{0}_CBDDlContainerID", this.ClientID);
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        [DefaultValue(true)]
        public bool CBAppendToBody
        {
            get
            {
                object obj = ViewState["CBAppendToBody"];
                return (obj == null) ? true : (bool)obj;
            }
            set
            {
                ViewState["CBAppendToBody"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBDivScrollContainer
        {
            get
            {
                object obj = ViewState["CBDivScrollContainer"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBDivScrollContainer"] = value;
            }
        }

        private string DivSuggestContainerID
        {
            get
            {
                return "ddlContainer_" + this.ClientID;
            }
        }

        private string HiddenValueFieldName
        {
            get
            {
                return this.ClientID + "_HidValue";
            }
        }

        private string CBPenultimateDivID
        {
            get
            {
                return String.Format("{0}_CBPenultimateDivID", this.ClientID);
            }
        }

        private string CBOuterDivID
        {
            get
            {
                return String.Format("{0}_CBOuterDivID", this.ClientID);
            }
        }

        private string CBImageID
        {
            get
            {
                return String.Format("{0}_CBImageID", this.ClientID);
            }
        }

        private string CBTextBoxID
        {
            get
            {
                return String.Format("{0}_CBTextBoxID", this.ClientID);
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string OnChangeFunctionName
        {
            get
            {
                object obj = ViewState["OnChangeFunctionName"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["OnChangeFunctionName"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string OnChangeFunctionParams
        {
            get
            {
                object obj = ViewState["OnChangeFunctionParams"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["OnChangeFunctionParams"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipText
        {
            get
            {
                object obj = ViewState["CBTipText"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipText"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string TipTextClass
        {
            get
            {
                object obj = ViewState["CBTipTextClass"];
                string strText = (obj == null) ? string.Empty : (string)obj;
                if (strText.Length == 0)
                {
                    return string.Empty;
                }
                return strText;
            }
            set
            {
                ViewState["CBTipTextClass"] = value;
            }
        }


        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public override string Text
        {
            get
            {
                object obj = ViewState["CBSelectedText"];
                string strText = (obj == null) ? string.Empty : (string)obj;


                return strText;
            }
            set
            {
                ViewState["CBSelectedText"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public int? MaxLength
        {
            get
            {
                object obj = ViewState["MaxLength"];

                if (obj != null)
                    return Convert.ToInt32(obj);
                else return null;
            }
            set
            {
                ViewState["MaxLength"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public override string SelectedValue
        {
            get
            {
                if (!CBReadOnly && this.Items.FindByText(this.Text) == null)
                {
                    return Text;
                }

                object obj = ViewState["CBSelectedValue"];
                string strText = (obj == null) ? string.Empty : (string)obj;


                return strText;
            }
            set
            {
                ViewState["CBSelectedValue"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        [DefaultValue(true)]
        public bool CBReadOnly
        {
            get
            {
                object obj = ViewState["CBReadOnly"];
                return (obj == null) ? true : (bool)obj;
            }
            set
            {
                ViewState["CBReadOnly"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool TimeInit
        {
            get
            {
                object obj = ViewState["CBTimeInit"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["CBTimeInit"] = value;
            }
        }

        bool m_ForceLoad = false;
        public bool ForceLoad
        {
            get
            {
                return m_ForceLoad;
            }
            set
            {
                m_ForceLoad = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool IsDisplayContainerStatic
        {
            get
            {
                object obj = ViewState["CBIsDisplayContainerStatic"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["CBIsDisplayContainerStatic"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBTextBoxCssClass
        {
            get
            {
                object obj = ViewState["CBTextBoxCssClass"];
                return (obj == null) ? string.Empty : (string)obj;
            }
            set
            {
                ViewState["CBTextBoxCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBSuggestionsOuterDivCssClass
        {
            get
            {
                object obj = ViewState["CBSuggestionsOuterDivCssClass"];
                return (obj == null) ? "Custom_SuggestionsOuterDiv" : (string)obj;
            }
            set
            {
                ViewState["CBSuggestionsOuterDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(false)]
        [Themeable(false)]
        [DefaultValue("")]
        public string CBOuterDivCssClass
        {
            get
            {
                object obj = ViewState["CBOuterDivCssClass"];
                return (obj == null) ? "Custom_DropDownOuterDiv" : (string)obj;
            }
            set
            {
                ViewState["CBOuterDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CBPenultimateDivCssClass
        {
            get
            {
                object obj = ViewState["CBPenultimateDivCssClass"];
                return (obj == null) ? "Custom_DropDownPenultimateDiv" : (string)obj;
            }
            set
            {
                ViewState["CBPenultimateDivCssClass"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBOuterDivWidth
        {
            get
            {
                object obj = ViewState["CBOuterDivWidth"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBOuterDivWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBPenultimateDivWidth
        {
            get
            {
                object obj = ViewState["CBPenultimateDivWidth"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBPenultimateDivWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBTextBoxWidth
        {
            get
            {
                object obj = ViewState["CBTextBoxWidth"];
                return (obj == null) ? 65 : (int)obj;
            }
            set
            {
                ViewState["CBTextBoxWidth"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public int CBSuggestionBoxHeight
        {
            get
            {
                object obj = ViewState["CBSuggestionBoxHeight"];
                return (obj == null) ? 100 : (int)obj;
            }
            set
            {
                ViewState["CBSuggestionBoxHeight"] = value;
            }
        }

        [Bindable(true)]
        [Browsable(true)]
        [Themeable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [UrlProperty]
        [Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        public string CBImageUrl
        {
            get
            {
                object obj = ViewState["CBImageUrl"];


                return (obj == null) ? Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls_V1.images.red-arrow-down.gif") : obj.ToString();


            }
            set
            {
                ViewState["CBImageUrl"] = value;
            }
        }

        [Bindable(false)]
        [Browsable(true)]
        [Themeable(true)]
        [DefaultValue("")]
        [Category("Style")]
        public string CBImageCssClass
        {
            get
            {
                object obj = ViewState["CBImageCssClass"];
                return (obj == null) ? "Custom_DropDownImage" : (string)obj;
            }
            set
            {
                ViewState["CBImageCssClass"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "GRCWebControlsHelper", System.Web.HttpUtility.HtmlEncode(Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls_V1.JScripts.GRCWebControlsHelper.js")));

            // create the style sheet control and put it in the document header
            string csslink = "<link rel='stylesheet' type='text/css' href='" + System.Web.HttpUtility.HtmlEncode(Page.ClientScript.GetWebResourceUrl(this.GetType(), "GRCBase.WebControls_V1.StyleSheet.css")) + "' />";
            LiteralControl include = new LiteralControl(csslink);
            include.ID = "EmbeddedCss";
            bool bFound = false;
            foreach (Control ctrl in this.Page.Header.Controls)
            {
                if (ctrl.ID == include.ID)
                {
                    bFound = true;
                }
            }

            if (!bFound)
            {
                this.Page.Header.Controls.Add(include);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);


            string strMasterArrayName = this.ClientID + "_Suggestions";


            //We need to make sure we register this array the first time the combo box is
            //made visible in the page 
            //Note: The combo box itself may be invisible (and hence OnPreRender will not 
            //be called) during first Get

            //An easy way to check if the OnPreRender already executed is to check if the
            //hidden value is already registered or not
            //if (!ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)

            if (!IsAutoSuggest)
            {
                if (HttpContext.Current.Request[HiddenValueFieldName] == null ||
                    !ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)
                {
                    StringBuilder strArray = new StringBuilder();
                    int iCount = 0;
                    List<string> declaredElements = new List<string>();
                    foreach (ListItem objItem in this.Items)
                    {
                        string arrayElement = this.ClientID + "_element_" + iCount;
                        ScriptManager.RegisterArrayDeclaration(this, arrayElement, "\"" + GRCBase.WebUtil.JSQuoteString(objItem.Text) + "\"");
                        ScriptManager.RegisterArrayDeclaration(this, arrayElement, "\"" + GRCBase.WebUtil.JSQuoteString(objItem.Value) + "\"");

                        declaredElements.Add(arrayElement);
                        iCount++;
                    }

                    foreach (string declaredElement in declaredElements)
                    {
                        ScriptManager.RegisterArrayDeclaration(this, strMasterArrayName, declaredElement);
                    }

                    //var ddlView_element_0 = new Array("Select1", "1");
                    //var ddlView_element_1 = new Array("Select2", "2");
                    //var ddlView_Suggestions = new Array(ddlView_element_0, ddlView_element_1);
                }
            }

            string strM = string.Empty;
            if (ForceLoad)
            {
                string[] strArr = new string[this.Items.Count];
                int i = 0;
                foreach (ListItem objItem in this.Items)
                {
                    strArr[i] = string.Format("new Array(\"{0}\", \"{1}\")", GRCBase.WebUtil.JSQuoteString(objItem.Text), GRCBase.WebUtil.JSQuoteString(objItem.Value));
                    i++;
                }

                strM = string.Format("new Array({0})", string.Join(",", strArr));
            }

            StringBuilder strScript = new StringBuilder();
            if (this.AutoPostBack)
            {
                strScript.Append(Page.ClientScript.GetPostBackEventReference(this, "", false));
            }
            else
            {
                string strContainer = this.CBAppendToBody ? "null" : string.Format("'{0}'", this.CBDDlContainerID);

                strScript.Append("var oTextbox_" + this.ClientID + " = new AutoSuggestControl(" + IsDisplayContainerStatic.ToString().ToLower() + "," + strContainer + ",'" + this.CBDivScrollContainer + "','" + this.DivSuggestContainerID + "'," + this.Enabled.ToString().ToLower() + ",document.getElementById(\"" + this.CBTextBoxID + "\"),");
                strScript.Append("\"" + this.CBTextBoxCssClass + "\", \"" + this.CBSuggestionsOuterDivCssClass + "\", \"" + this.OnChangeFunctionName + "\", \"" + this.OnChangeFunctionParams + "\", " + this.CBReadOnly.ToString().ToLower() + ", document.getElementById(\"" + this.CBOuterDivID + "\"), document.getElementById(\"" + this.CBPenultimateDivID + "\"),");
                strScript.Append("document.getElementById(\"" + this.CBImageID + "\"),\"" + this.CBSuggestionBoxHeight + "\",document.getElementById(\"" + HiddenValueFieldName + "\"),");
                if (!IsAutoSuggest)
                {
                    if (ForceLoad)
                    {
                        strScript.Append("new StateSuggestions(" + strM + "),null,false,null,null," + FilterResultsOnClient.ToString().ToLower() + ");\r\n");
                    }
                    else
                    {
                        strScript.Append("new StateSuggestions(" + strMasterArrayName + "),null,false,null,null," + FilterResultsOnClient.ToString().ToLower() + ");\r\n");
                    }
                }
                else
                {
                    if (ClientSuggestionsFunctionName.Length > 0)
                    {
                        strScript.Append("null,\"" + ClientSuggestionsFunctionName + "\",true," + IsClientFunctionAjax.ToString().ToLower() + "," + CBMinimumPrefixLength + "," + FilterResultsOnClient.ToString().ToLower() + ");\r\n");
                    }
                    else
                    {
                        strScript.Append("new StateSuggestions(" + ClientSuggestionsArrayName + "),null,true," + IsClientFunctionAjax.ToString().ToLower() + "," + CBMinimumPrefixLength + "," + FilterResultsOnClient.ToString().ToLower() + ");\r\n");
                    }
                }

                if (TimeInit)
                {
                    strScript.Append("InitializeTimeControl('" + this.HiddenValueFieldName + "','" + this.CBTextBoxID + "');\r\n");
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "CBScript2_" + this.ClientID, strScript.ToString(), true);
            Page.RegisterRequiresPostBack(this);

            //register the hidden field
            if (Page.IsPostBack)
            {
                ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, this.SelectedValue);
                /*
                string strVal = HttpContext.Current.Request[HiddenValueFieldName] == null ? string.Empty : HttpContext.Current.Request[HiddenValueFieldName];
                
                ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, strVal);

                SelectedValue = strVal;
                */
                //this.Text = HttpContext.Current.Request[this.CBTextBoxID] == null ? string.Empty : HttpContext.Current.Request[this.CBTextBoxID];
            }
            else
            {
                ScriptManager.RegisterHiddenField(this, HiddenValueFieldName, this.SelectedValue);
            }
        }

        /// <summary>
        /// Load data from POST (load checked from hidden input)
        /// </summary>
        /// <param name="postDataKey">Key</param>
        /// <param name="postCollection">Data</param>
        /// <returns>True when value changed</returns>
        public bool LoadPostData(string postDataKey,
            System.Collections.Specialized.NameValueCollection postCollection)
        {
            if (HttpContext.Current.Request[this.CBTextBoxID] != null)
            {
                this.Text = HttpContext.Current.Request[this.CBTextBoxID];
            }
            SelectedValue = HttpContext.Current.Request[HiddenValueFieldName] == null ? string.Empty : HttpContext.Current.Request[HiddenValueFieldName];
            return true;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                EnsureChildControls();
            }

            if (this.TipText != null && this.TipText.Length > 0)
            {
                writer.AddAttribute("onmouseover", "objGRCToolTip.show(this);");
                writer.AddAttribute("onmouseout", "objGRCToolTip.hide();");
                writer.AddAttribute("rel", this.TipText);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "event.cancelBubble = true;");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBOuterDivID);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBOuterDivCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBPenultimateDivCssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBPenultimateDivID);
            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            if (CBReadOnly)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");
            }
            else
            {
                if (MaxLength.HasValue)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, MaxLength.Value.ToString());
                }
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Name, this.CBTextBoxID);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBTextBoxID);
            writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.Width.ToString());
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, this.Text);
            writer.AddAttribute(HtmlTextWriterAttribute.Tabindex, this.TabIndex.ToString());
            writer.AddAttribute(HtmlTextWriterAttribute.AutoComplete, "off");
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            if (!IsAutoSuggest)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, this.CBImageID);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CBImageCssClass);
                writer.AddAttribute(HtmlTextWriterAttribute.Src, this.CBImageUrl);
                writer.AddAttribute(HtmlTextWriterAttribute.Alt, string.Empty);
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag();
            }

            writer.RenderEndTag();

            writer.RenderEndTag();
        }

        public void SetSelectedItem(string val)
        {
            this.SelectedIndex = -1;

            ListItem item = this.Items.FindByValue(val);

            if (item != null)
            {
                this.Text = item.Text;
                this.SelectedValue = item.Value;
            }
            else
            {
                this.Text = val;
                this.SelectedValue = val;
            }
        }
    }
}
