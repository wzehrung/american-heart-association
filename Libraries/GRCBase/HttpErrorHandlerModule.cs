﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Runtime.CompilerServices;

namespace GRCBase
{
    public class HttpErrorHandlerModule : IHttpModule, System.Web.SessionState.IRequiresSessionState
    {
        public void Dispose()
        {

        }

        public virtual void Init(HttpApplication application)
        {
            application.BeginRequest += new EventHandler(BeginRequest);
            application.Error += new EventHandler(OnError);
            application.PostRequestHandlerExecute += new EventHandler(OnEndRequest);
            application.PreSendRequestHeaders += new EventHandler(OnPresendHeaders);
        }

        private bool isUrlEqual(string url1, string url2)
        {
            if (url1.Length < url2.Length)
            {
                return url2.ToLower().EndsWith(url1.ToLower());
            }

            return url1.ToLower().EndsWith(url2.ToLower());
        }

        protected virtual void OnPresendHeaders(object sender, EventArgs args)
        {
            HttpApplication application = (HttpApplication)sender;
            if (!LibrarySettings.ErrorLogSettings.IsErrorLogEnabled)
            {
                return;
            }
            string fileNotFoundUrl = LibrarySettings.ErrorLogSettings.FileNotFoundPageRelativeURL;

            if (application.Response.StatusCode == 404 && !isUrlEqual(application.Request.Url.LocalPath, fileNotFoundUrl))
            {
                if (fileNotFoundUrl == null || fileNotFoundUrl.Length == 0)
                    return;

                //Note: System.Web.HttpContext.Current is invalid (null) here..
                fileNotFoundUrl += ("?Source=" + WebUtil.UrlEncode(application.Request.Url.PathAndQuery));
                application.Response.Write(MiscUtility.GetScriptForRedirection(fileNotFoundUrl));
                application.Response.End();
            }

        }

        protected virtual void BeginRequest(object sender, EventArgs args)
        {
            if (isUrlEqual(UrlUtility.RelativeCurrentPageUrl, LibrarySettings.ErrorLogSettings.ErrorPageRelativeURL))
            {
                return;
            }

            if (DBContext.TrasactionOpenCount != 0)
            {
                throw new System.Exception(_GenerateDBContextErrorString("Open transaction count on begin request > 0. Invalid state"));
            }
        }

        protected virtual void OnError(object sender, EventArgs args)
        {
            if (!LibrarySettings.ErrorLogSettings.IsErrorLogEnabled)
            {
                return;
            }

            HttpApplication application = (HttpApplication)sender;

            if (isUrlEqual(UrlUtility.RelativeCurrentPageUrl, LibrarySettings.ErrorLogSettings.ErrorPageRelativeURL))
            {
                return;
            }

            string[] strErrorExludedUrls = LibrarySettings.ErrorLogSettings.GetErrorExludedUrls();
            bool bIsCurrentPageExcluded = false;

            string errorOccurredIn = HttpContext.Current.Request.ServerVariables["HTTP_VTI_SCRIPT_NAME"];
            foreach (string strUrl in strErrorExludedUrls)
            {
                if (strUrl != null && strUrl.Length > 0)
                {
                    if (UrlUtility.RelativeCurrentPageUrl.ToLower().EndsWith(strUrl.ToLower())
                        || (errorOccurredIn != null && errorOccurredIn.ToLower().EndsWith(strUrl.ToLower())))
                    {
                        bIsCurrentPageExcluded = true;
                        break;
                    }
                }
            }

            if (bIsCurrentPageExcluded) return;

            Exception e = System.Web.HttpContext.Current.Server.GetLastError();

            //Make sure we still have an active error to track..
            //For ex, if we have another HttpModule which tracks specific errors, 
            //that module would have cleared the errors already and the current error
            if (e == null) 
                return;

            //to ignore invalid view state - The client disconnected error when the user stops or navigates away from the current page
            //while a postback is in progress & the issue with ie8.0 viewstate eating
            if (!string.IsNullOrEmpty(e.Message) && 
                (e.Message.ToLower().IndexOf("the client disconnected") != -1
                || e.Message.ToLower().IndexOf("invalid viewstate") != -1
                || e.Message.ToLower().IndexOf("invalid character in a base-64 string") != -1))
                return;

            if (HttpContext.Current.Request.Browser != null && HttpContext.Current.Request.Browser.Browser.ToLower() == "unknown")
                return;

            if (LibrarySettings.ErrorLogSettings.PassAccessDeniedExceptions)
            {
                if (e.GetBaseException().GetType() == typeof(System.UnauthorizedAccessException))
                {
                    return;
                }
            }

            int errorID = ErrorLogHelper.LogError_Application(e);

            //known errors which can be splashed on user screen.
            if (errorID <= 0)
            {
                return;
            }

            string errorUrl = string.Format("ID={0}&Source={1}&Method={2}"
                ,BlowFish.EncryptString(errorID.ToString())
                ,WebUtil.UrlEncode(UrlUtility.RelativeCurrentPageUrlWithQV)
                ,System.Web.HttpContext.Current.Request.HttpMethod);

            errorUrl = string.Format("{0}?{1}",LibrarySettings.ErrorLogSettings.ErrorPageRelativeURL,errorUrl);

            application.Response.Write(MiscUtility.GetScriptForRedirection(errorUrl));
            application.Response.End();
        }


        protected virtual void OnEndRequest(object sender, EventArgs args)
        {
            if (isUrlEqual(UrlUtility.RelativeCurrentPageUrl, LibrarySettings.ErrorLogSettings.ErrorPageRelativeURL))
            {
                return;
            }

            if (DBContext.TrasactionOpenCount != 0)
            {
                throw new System.Exception(_GenerateDBContextErrorString("Open transaction count on end request > 0. Invalid state"));
            }
        }

        private string _GenerateDBContextErrorString(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(str);
            sb.Append("\r\n");
            sb.Append("Total Active: " + DBContext.TrasactionOpenCount + "\r\n");

            int i = 0;

            foreach (DBContext dbContext in DBContext.ActiveContextList)
            {
                sb.Append("Instance : " + i.ToString() + "\r\n");
                sb.Append("Created from : " + dbContext.InstanceStackTrace + "\r\n\r\n");
                i++;
            }

            return sb.ToString();
        }
    }
}
