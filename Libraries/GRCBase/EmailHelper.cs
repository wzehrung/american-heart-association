﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Collections.Specialized;

namespace GRCBase
{
    public class EmailHelper
    {
        public static bool SendMail(string NameFrom, string NameTo, string EmailFrom, string EmailTo, string EmailSubject, string MailBody, bool isMailFormatHTML)
        {
            return SendMailWithAttachments(NameFrom, NameTo, EmailFrom, EmailTo, EmailSubject, MailBody, isMailFormatHTML, null, null, null, null);
        }

        public static bool SendMail(string NameFrom, string NameTo, string EmailFrom, string EmailTo, string EmailSubject, string MailBody, bool isMailFormatHTML, System.Collections.Specialized.NameValueCollection CustomHeaderCollection, string strSenderEmail, string strReplyTo)
        {
            return SendMailWithAttachments(NameFrom, NameTo, EmailFrom, EmailTo, EmailSubject, MailBody, isMailFormatHTML, null, CustomHeaderCollection, strSenderEmail, strReplyTo);
        }

        public static bool SendMailWithAttachments(string NameFrom, string NameTo, string EmailFrom, string EmailTo, string EmailSubject, string MailBody, bool isMailFormatHTML, List<System.Net.Mail.Attachment> attachments)
        {
            return SendMailWithAttachments(NameFrom, NameTo, EmailFrom, EmailTo, EmailSubject, MailBody, isMailFormatHTML, attachments, null, null, null);
        }

        public static bool SendMailWithAttachments(string NameFrom, string NameTo, string EmailFrom, string EmailTo, string EmailSubject, string MailBody, bool isMailFormatHTML, List<System.Net.Mail.Attachment> attachments, System.Collections.Specialized.NameValueCollection CustomHeaderCollection, string strSenderEmail, string strReplyTo)
        {
            try
            {
                MailAddress objFromAddress = null;
                if (NameFrom.Length == 0)
                {
                    objFromAddress = new MailAddress(EmailFrom);
                }
                else
                {
                    objFromAddress = new MailAddress(EmailFrom, NameFrom);
                }

                System.Net.Mail.MailAddress objToAddress = null;
                if (NameTo.Length == 0)
                {
                    objToAddress = new MailAddress(EmailTo);
                }
                else
                {
                    objToAddress = new MailAddress(EmailTo, NameTo);
                }

                List<MailAddress> rcptList = new List<MailAddress>();
                rcptList.Add(objToAddress);

                return _DoSendMail(objFromAddress, rcptList, null, EmailSubject, MailBody, isMailFormatHTML, attachments, CustomHeaderCollection, strSenderEmail, strReplyTo);
            }
            catch (Exception ex)
            {
                // PJB: Nope, not the right way to handle this.
                //      Need to log the error.
                //throw;
            }

            return false;
        }

        public static bool SendMailMultipleWithAttachments(string EmailFrom, List<string> emailToList, List<string> emailCCList, string EmailSubject, string MailBody, bool isMailFormatHTML, List<System.Net.Mail.Attachment> attachments)
        {
            string[] emailArray = emailToList.ToArray();
            return SendMailMultipleWithAttachments(EmailFrom, emailArray, emailCCList, EmailSubject, MailBody, isMailFormatHTML, attachments);
        }

        public static bool SendMailMultipleWithAttachments(string EmailFrom, string[] emailToList, List<string> emailCCList, string EmailSubject, string MailBody, bool isMailFormatHTML, List<System.Net.Mail.Attachment> attachments)
        {
            MailAddress objFromAddress = new MailAddress(EmailFrom);
            List<MailAddress> rcptList = new List<MailAddress>();
            List<MailAddress> ccList = new List<MailAddress>();

            foreach (string toAddr in emailToList)
            {
                MailAddress objToAddress = null;
                objToAddress = new MailAddress(toAddr);
                rcptList.Add(objToAddress);
            }

            if (emailCCList != null)
            {
                foreach (string toAddr in emailCCList)
                {
                    MailAddress objCCAddress = null;
                    objCCAddress = new MailAddress(toAddr);
                    ccList.Add(objCCAddress);
                }
            }

            _DoSendMail(objFromAddress, rcptList, ccList, EmailSubject, MailBody, isMailFormatHTML, attachments, null, null, null);
            return true;
        }

        private static bool _DoSendMail(MailAddress Sender, List<MailAddress> RecipientList, List<MailAddress> CCList,
            string EmailSubject, string MailBody, bool isMailFormatHTML, List<Attachment> attachments,
            NameValueCollection CustomHeaderCollection, string strSenderEmail, string strReplyTo)
        {
            try
            {
                MailMessage objMessage = new MailMessage();
                objMessage.From = Sender;
                objMessage.Subject = EmailSubject;
                objMessage.Body = MailBody;
                if (isMailFormatHTML)
                {
                    objMessage.IsBodyHtml = true;
                }
                else
                {
                    objMessage.IsBodyHtml = false;
                }

                foreach (MailAddress mailAddr in RecipientList)
                {
                    objMessage.To.Add(mailAddr);
                }

                if (CCList != null)
                {
                    foreach (MailAddress mailAddr in CCList)
                    {
                        objMessage.CC.Add(mailAddr);
                    }
                }

                SmtpClient objSmtpClient = new SmtpClient(LibrarySettings.GetSMTPServer());


                if (attachments != null)
                {
                    foreach (Attachment attchemnt in attachments)
                    {
                        objMessage.Attachments.Add(attchemnt);
                    }
                }

                //to add custom headers
                if (CustomHeaderCollection != null)
                {
                    objMessage.Headers.Add(CustomHeaderCollection);
                }

                //this is required to modify the Return-Path(originally this is set to the from email address), which can be found in the message header and also add the "sender" property to the message header
                if (strSenderEmail != null && strSenderEmail.Length > 0)
                {
                    objMessage.Sender = new MailAddress(strSenderEmail);
                }

                //set reply to address if we need to use the objMessage.DeliveryNotificationOptions
                if (strReplyTo != null && strReplyTo.Length > 0)
                {
                    objMessage.ReplyTo = new MailAddress(strReplyTo);
                    objMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                }

                objSmtpClient.Send(objMessage);
                return true;
            }
            catch (Exception ex)
            {
                // PJB: don't do this, simply return false
                //      may want to log error somehow.
                // throw;
            }

            return false;
        }
    }
}
