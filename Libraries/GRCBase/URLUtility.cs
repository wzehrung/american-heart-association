﻿using System;
using System.Text;
using System.Collections.Specialized;
using System.Web;

namespace GRCBase
{
    public class UrlUtility
    {

        /// <summary>
        /// For http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns "http"
        /// </summary>
        public static string ProtocolPrefix
        {
            get
            {
                if (System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"] == "1")
                    return "https";
                return "http";
            }
        }

        /// <summary>
        /// For http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns www.google.co.in
        /// </summary>
        public static string ServerName
        {
            get
            {
                return System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            }
        }


        /// <summary>
        /// For http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns http://www.google.co.in
        /// </summary>
        public static string ServerRootUrl
        {
            get
            {
                string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];

                if (port == null || port == "80" || port == "443")
                    port = "";
                else
                    port = ":" + port;

                return GetHttpPrefix() + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port;
            }
        }

        public static string ServerPort
        {
            get
            {

                return GetHttpPrefix() + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            }
        }

        /// <summary>
        /// For http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns http://www.google.co.in/search.aspx
        /// </summary>
        public static string FullCurrentPageUrl
        {
            get
            {
                return GetHttpPrefix() + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + RelativeCurrentPageUrl;
            }
        }


        /// <summary>
        /// For http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns 
        /// </summary>
        public static string FullCurrentPageUrlWithQV
        {
            get
            {
                string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                if (port == null || port == "80" || port == "443")
                    port = "";
                else
                    port = ":" + port;

                if (_GetQueryStrings().Length > 0)
                    return GetHttpPrefix() + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + RelativeCurrentPageUrl + "?" + _GetQueryStrings();
                else
                    return GetHttpPrefix() + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + RelativeCurrentPageUrl;
            }
        }



        private static string _key_for_storing_relative_pageUrl_on_context = "GRC_URL_UTILITY_RELATIVE";


        /// <summary>
        /// For http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns /search.aspx
        /// </summary>
        public static string RelativeCurrentPageUrl
        {
            get
            {
                if (System.Web.HttpContext.Current.Items.Contains(_key_for_storing_relative_pageUrl_on_context))
                {
                    return System.Web.HttpContext.Current.Items[_key_for_storing_relative_pageUrl_on_context].ToString();
                }
                System.Web.HttpContext.Current.Items[_key_for_storing_relative_pageUrl_on_context] = _DoGetRelativeCurrentPageUrl();
                return System.Web.HttpContext.Current.Items[_key_for_storing_relative_pageUrl_on_context].ToString();
            }
        }



        public static void RefreshContextCache()
        {
            System.Web.HttpContext.Current.Items.Remove(_key_for_storing_relative_pageUrl_on_context);
        }


        private static string _DoGetRelativeCurrentPageUrl()
        {
            if (System.Web.HttpContext.Current.Items.Contains("Layouts"))
            {
                //System.Uri contextUri = System.Web.HttpContext.Current.Items["Microsoft.SharePoint.Administrtion.ContextUri"] as System.Uri;
                //string pathAndQuery = System.Web.HttpContext.Current.Request.RawUrl;
                string pathAndQuery = System.Web.HttpContext.Current.Items["HTTP_VTI_SCRIPT_NAME"].ToString();

                int endOfUrl = pathAndQuery.IndexOf("?");
                if (endOfUrl > 0)
                {
                    return pathAndQuery.ToString().Substring(0, endOfUrl);
                }
                return pathAndQuery.ToString();
            }
            return System.Web.HttpContext.Current.Request.ServerVariables["URL"];
        }






        #region ServerVariables
        public static string ClientIPAddress
        {
            get
            {
                return System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
        }

        public static string HTTP_REFERER
        {
            get
            {
                return System.Web.HttpContext.Current.Request.ServerVariables["HTTP_REFERER"];
            }
        }

        public static string HTTP_USER_AGENT
        {
            get
            {
                return System.Web.HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            }
        }
        #endregion

        /// <summary>
        /// For http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns /search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// </summary>
        public static string RelativeCurrentPageUrlWithQV
        {
            get
            {
                if (_GetQueryStrings().Length > 0)
                    return RelativeCurrentPageUrl + "?" + _GetQueryStrings();
                else
                    return RelativeCurrentPageUrl;
            }
        }


        /// <summary>
        /// http://www.google.co.in/search.aspx?hl=en&q=asp.net+datetime+format+list&meta=
        /// returns hl=en&q=asp.net+datetime+format+list&meta=
        /// </summary>
        public static string QueryString
        {
            get
            {
                return _GetQueryStrings();
            }
        }

        public static string _GetQueryStrings()
        {
            string urlRequest = string.Empty;
            if (System.Web.HttpContext.Current.Request.ServerVariables["QUERY_STRING"] != null && System.Web.HttpContext.Current.Request.ServerVariables["QUERY_STRING"].Length > 0)
            {
                urlRequest = System.Web.HttpContext.Current.Request.ServerVariables["QUERY_STRING"];
            }
            return urlRequest;
        }

        public static NameValueCollection GetQueryStringCollection()
        {

            string queryPart = System.Web.HttpContext.Current.Request.Url.Query;

            NameValueCollection queryCollection = HttpUtility.ParseQueryString(queryPart);

            return queryCollection;
        }

        public static NameValueCollection GetQueryStringCollection(string fullURL)
        {
            Uri myUrl = new Uri(fullURL);
            string queryPart = myUrl.Query;

            NameValueCollection queryCollection = HttpUtility.ParseQueryString(queryPart);

            return queryCollection;
        }

        /// <summary>
        /// Only plain text can be used here to generate anchor.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        /// 
        public static string GenerateHtmlAnchor(string url, string text)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<a ");
            sb.Append("href=\"");
            sb.Append(url);
            sb.Append("\">");
            sb.Append(text);
            sb.Append("</a>");
            return sb.ToString();
        }

        public static string GenerateHtmlAnchorOpensInNewWindow(string url, string text)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<a ");
            sb.Append("href=\"");
            sb.Append(url + "\" target=\"_blank\"");
            sb.Append(">");
            sb.Append(text);
            sb.Append("</a>");
            return sb.ToString();
        }

        public static string GenerateImageTagForImageURL(string imageURL)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<img ");
            sb.Append("src=\"");
            sb.Append(imageURL);
            sb.Append("\"/>");
            return sb.ToString();
        }

        public static string GenerateHtmlAnchorWithCssClass(string url, string text, string cssclass)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<a ");

            if (cssclass != null && cssclass.Length > 0)
                sb.Append("class=\"" + cssclass + "\" ");

            sb.Append("href=\"");
            sb.Append(HttpUtility.UrlEncodeUnicode(url));
            sb.Append("\">");
            sb.Append(text);
            sb.Append("</a>");
            return sb.ToString();
        }


        public static string GenerateHtmlAnchorWithImage(string url, string imageSrc)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<a href=\"");
            sb.Append(url);
            sb.Append("\"><img border=\"0\" src=\"");
            sb.Append(imageSrc);
            sb.Append("\"></a>");
            return sb.ToString();
        }

        public static string GenerateHtmlAnchorWithOnclick(string url, string text, bool bIsImage, string onclick)
        {
            if (!bIsImage)
            {
                text = StringHelper.HtmlEncodeAndReplaceQuotes(text);
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("<a onclick=\"" + onclick.Replace("\"", "'") + "\" href=\"");
            sb.Append(url);
            sb.Append("\">");
            sb.Append(text);
            sb.Append("</a>");
            return sb.ToString();
        }

        public static string GenerateHtmlAnchorWithOnclickInNewWindow(string url, string text, bool bIsImage, string onclick)
        {
            if (!bIsImage)
            {
                text = StringHelper.HtmlEncodeAndReplaceQuotes(text);
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("<a target=\"_blank\" onclick=\"" + onclick.Replace("\"", "'") + "\" href=\"");
            sb.Append(url);
            sb.Append("\">");
            sb.Append(text);
            sb.Append("</a>");
            return sb.ToString();
        }

        public static string GetHttpPrefix()
        {
            if (System.Web.HttpContext.Current.Request.IsSecureConnection)
                return "https://";

            return "http://";
        }
    }
}
