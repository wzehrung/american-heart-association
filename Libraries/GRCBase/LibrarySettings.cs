﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GRCBase
{
    public class LibrarySettings
    {

        public static string DatabaseConnectionString
        {
            get
            {
                return ConfigReader.GetValue("DBConnStr");
            }
        }


        /// <summary>
        /// The Cache expiration time
        /// </summary>
        public static int CacheTimeOut
        {
            get
            {
                string cachevalue = ConfigReader.GetValue("CacheManTimeOutValue");
                if (cachevalue == null)
                    return 20;
                else
                    return System.Convert.ToInt32(cachevalue);
            }
        }


        public class ImpersonationSettings
        {
            public static string UserName
            {
                get
                {
                    return ConfigReader.GetValue("BasicAuthenticationUsername");
                }
            }

            public static string Domain
            {
                get
                {
                    return ConfigReader.GetValue("BasicAuthenticationDomain");
                }
            }

            public static string Password
            {
                get
                {
                    return ConfigReader.GetValue("BasicAuthenticationPassword");
                }
            }

        }



        public static GRCDateFormat GetDefaultDateFormat()
        {
            string strDateFormatString = ConfigReader.GetValue("DefaultDateFormat");

            switch (strDateFormatString)
            {
                case "NONE": return GRCDateFormat.NONE;
                case "MMDDYYYY": return GRCDateFormat.MMDDYYYY;
                case "MMDDYY": return GRCDateFormat.MMDDYY;
                case "MMMDDYYYY": return GRCDateFormat.MMMDDYYYY;
                case "MMMDDYY": return GRCDateFormat.MMMDDYY;
                case "MMMMMDDYYYY": return GRCDateFormat.MMMMMDDYYYY;
                case "MMMMMDDYY": return GRCDateFormat.MMMMMDDYY;
                case "DDMMYYYY": return GRCDateFormat.DDMMYYYY;
                case "DDMMYY": return GRCDateFormat.DDMMYY;
                case "DDMMMYYYY": return GRCDateFormat.DDMMMYYYY;
                case "DDMMMYY": return GRCDateFormat.DDMMMYY;
                case "DDMMMMMYYYY": return GRCDateFormat.DDMMMMMYYYY;
                default: throw new Exception("Invalid date format.");
            }
        }

        public static GRCDateSeperator GetDefaultDateSeperatorFormat()
        {
            string strDateSeperatorFormatString = ConfigReader.GetValue("DefaultDateSeperator");

            switch (strDateSeperatorFormatString)
            {
                case "NONE": return GRCDateSeperator.NONE;
                case "HYPHEN": return GRCDateSeperator.HYPHEN;
                case "SLASH": return GRCDateSeperator.SLASH;
                default: throw new Exception("Invalid date seperator format.");
            }

        }

        public static GRCTimeFormat GetDefaultTimeFormat()
        {
            string strTimeFormatString = ConfigReader.GetValue("DefaultTimeFormat");

            switch (strTimeFormatString)
            {
                case "NONE": return GRCTimeFormat.NONE;
                case "HHmmss": return GRCTimeFormat.HHmmss;
                case "HHmm": return GRCTimeFormat.HHmm;
                case "hhmm": return GRCTimeFormat.hhmm;
                case "hhmmss": return GRCTimeFormat.hhmmss;
                case "hhmmtt": return GRCTimeFormat.hhmmtt;
                case "hhmmsstt": return GRCTimeFormat.hhmmsstt;
                default: throw new Exception("Invalid time format.");
            }
        }


        public static string GetSMTPServer()
        {
            return ConfigReader.GetValue("SMTPServer");
        }


        public class ErrorLogSettings
        {
            public static bool IsErrorLogEnabled
            {
                get
                {
                    string strEnableErrorLog = ConfigReader.GetValue("EnableErrorLog");
                    if (strEnableErrorLog != null && strEnableErrorLog == "1")
                        return true;
                    return false;
                }
            }


            public static bool IsErrorLogEmailEnabled
            {
                get
                {
                    string strErrorLogEmail = ConfigReader.GetValue("EnableErrorLogEmail");
                    if (strErrorLogEmail != null && strErrorLogEmail == "1")
                        return true;
                    return false;
                }
            }

            public static string ErrorPageRelativeURL
            {
                get
                {
                    return ConfigReader.GetValue("ErrorPageRelativePath");
                }
            }


            public static string FileNotFoundPageRelativeURL
            {
                get
                {
                    return ConfigReader.GetValue("FileNotFoundPageRelativeURL");
                }
            }

            /** PJB: not used for patient or provider portal **/
            public static string ItemNotFoundPageRelativeUrl
            {
                get
                {
                    return ConfigReader.GetValue("ItemNotFoundPageRelativeURL");
                }
            }


            public static bool PassAccessDeniedExceptions
            {
                get
                {
                    string strPassAccessDeniedExceptions = ConfigReader.GetValue("PassAccessDeniedExceptions");
                    if (strPassAccessDeniedExceptions != null && strPassAccessDeniedExceptions == "1")
                        return true;
                    return false;
                }
            }


            public static bool IgnoreCompilerErrors
            {
                get
                {
                    if (ConfigReader.GetValue("IgnoreCompilerErrors") == "1")
                        return true;
                    return false;
                }
            }

            public static string[] GetErrorExludedUrls()
            {
                string errorExcludedUrls = ConfigReader.GetValue("ErrorExcludedPages");

                if (errorExcludedUrls == null)
                    return new string[0];

                return errorExcludedUrls.Split(",".ToCharArray());
            }


            public static ErrorLogEmailSettings TechSupportBillingEmailSetting
            {
                get
                {
                    ErrorLogEmailSettings emailsetting = new ErrorLogEmailSettings();
                    emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportBillingEmailTemplate");
                    emailsetting.Subject = ConfigReader.GetValue("TechSupportBillingEmailSubject");
                    emailsetting.FromEmail = ConfigReader.GetValue("TechSupportBillingEmailBillingFrom");
                    emailsetting.FromName = ConfigReader.GetValue("TechSupportBillingEmailFromName");
                    emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Billing");
                    emailsetting.ToName = ConfigReader.GetValue("TechSupportBillingEmailToName");
                    return emailsetting;
                }
            }

            public static ErrorLogEmailSettings TechSupportAjaxEmailSetting
            {
                get
                {
                    ErrorLogEmailSettings emailsetting = new ErrorLogEmailSettings();
                    emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportAjaxEmailTemplate");
                    emailsetting.Subject = ConfigReader.GetValue("TechSupportAjaxEmailSubject");
                    emailsetting.FromEmail = ConfigReader.GetValue("TechSupportAjaxEmailBillingFrom");
                    emailsetting.FromName = ConfigReader.GetValue("TechSupportAjaxEmailFromName");
                    emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Ajax");
                    emailsetting.ToName = ConfigReader.GetValue("TechSupportAjaxEmailToName");
                    return emailsetting;
                }
            }

            public static ErrorLogEmailSettings TechSupportAnthemEmailSetting
            {
                get
                {
                    ErrorLogEmailSettings emailsetting = new ErrorLogEmailSettings();
                    emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportAnthemEmailTemplate");
                    emailsetting.Subject = ConfigReader.GetValue("TechSupportAnthemEmailSubject");
                    emailsetting.FromEmail = ConfigReader.GetValue("TechSupportAnthemEmailFrom");
                    emailsetting.FromName = ConfigReader.GetValue("TechSupporAnthemEmailFromName");
                    emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Anthem");
                    emailsetting.ToName = ConfigReader.GetValue("TechSupportAnthemEmailToName");
                    return emailsetting;
                }
            }

            public static ErrorLogEmailSettings TechSupportComponentArtEmailSetting
            {
                get
                {
                    ErrorLogEmailSettings emailsetting = new ErrorLogEmailSettings();
                    emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportComponentArtEmailTemplate");
                    emailsetting.Subject = ConfigReader.GetValue("TechSupportComponentArtEmailSubject");
                    emailsetting.FromEmail = ConfigReader.GetValue("TechSupportComponentArtEmailFrom");
                    emailsetting.FromName = ConfigReader.GetValue("TechSupporComponentArtEmailFromName");
                    emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_ComponentArt");
                    emailsetting.ToName = ConfigReader.GetValue("TechSupportComponentArtEmailToName");
                    return emailsetting;
                }
            }

            public static ErrorLogEmailSettings TechSupportApplicationEmailSetting
            {
                get
                {
                    ErrorLogEmailSettings emailsetting = new ErrorLogEmailSettings();
                    emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportApplicationEmailTemplate");
                    emailsetting.Subject = ConfigReader.GetValue("TechSupportApplicationEmailSubject");
                    emailsetting.FromEmail = ConfigReader.GetValue("TechSupportApplicationEmailFrom");
                    emailsetting.FromName = ConfigReader.GetValue("TechSupporApplicationEmailFromName");
                    emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Application");
                    emailsetting.ToName = ConfigReader.GetValue("TechSupportApplicationEmailToName");
                    return emailsetting;
                }
            }

            public static ErrorLogEmailSettings TechSupportJavascriptEmailSetting
            {
                get
                {
                    ErrorLogEmailSettings emailsetting = new ErrorLogEmailSettings();
                    emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportJavascriptEmailTemplate");
                    emailsetting.Subject = ConfigReader.GetValue("TechSupportJavascriptEmailSubject");
                    emailsetting.FromEmail = ConfigReader.GetValue("TechSupportJavascriptEmailFrom");
                    emailsetting.FromName = ConfigReader.GetValue("TechSupporJavascriptEmailFromName");
                    emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Javascript");
                    emailsetting.ToName = ConfigReader.GetValue("TechSupportJavascriptEmailToName");
                    return emailsetting;
                }
            }

            public static ErrorLogEmailSettings TechSupportDefaultEmailSetting
            {
                get
                {
                    ErrorLogEmailSettings emailsetting = new ErrorLogEmailSettings();
                    emailsetting.MailTemplate = ConfigReader.GetValue("TechSupportDefaultEmailTemplate");
                    emailsetting.Subject = ConfigReader.GetValue("TechSupportDefaultEmailSubject");
                    emailsetting.FromEmail = ConfigReader.GetValue("TechSupportDefaultEmailFrom");
                    emailsetting.FromName = ConfigReader.GetValue("TechSupporDefaultEmailFromName");
                    emailsetting.ToEmail = ConfigReader.GetValue("TechSupportEmail_Default");
                    emailsetting.ToName = ConfigReader.GetValue("TechSupportDefaultEmailToName");
                    return emailsetting;
                }
            }
        }
    }
}
