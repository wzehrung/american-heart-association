﻿using System;
using System.Data;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using System.Web;
using System.Data.SqlClient;

namespace GRCBase
{
    public class SQLHelper
    {
        public SQLHelper()
        {

        }


        /// <summary>
        /// Do not use this method.
        /// </summary>
        /// <param name="sql">sql query</param>
        /// <returns></returns>
        private static SqlDataReader _DoSql(string sql)
        {
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            try
            {
                SqlConnection objConn = new SqlConnection(LibrarySettings.DatabaseConnectionString);
                objConn.Open();
                cmd = new SqlCommand(sql, objConn);
                reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch (System.Exception e)
            {
                throw;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                    cmd = null;
                }

                //DO NOT CLOSE CONNECTION HERE!!!
            }
            return reader;
        }

        /// <summary>
        /// Executes a select query.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static SqlDataReader DoSql(DBContext dbContext, string sql)
        {
            dbContext.ResetForNextQuery();
            SqlDataReader reader = null;
            try
            {
                dbContext.ContextSqlCommand.CommandText = sql;
                reader = dbContext.ContextSqlCommand.ExecuteReader();
            }
            catch (System.Exception e)
            {
                throw;
            }
            return reader;
        }

        /// <summary>
        /// Executes insert or update query and does not return records.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="sql"></param>
        public static void DoSqlStatement(DBContext dbContext, string sql)
        {
            dbContext.ResetForNextQuery();
            try
            {
                dbContext.ContextSqlCommand.CommandText = sql;
                dbContext.ContextSqlCommand.ExecuteNonQuery();
            }
            catch (System.Exception e)
            {
                throw;
            }
            return;
        }
    }

    /// <summary>
    /// Summary description for GRCBaseException.
    /// </summary>
    public class GRCBaseException : System.Exception
    {
        public GRCBaseException(string msg)
            : base(msg)
        {

        }

        public GRCBaseException()
        {
        }

        public GRCBaseException(System.Exception inner)
            : base("An Error Occurred. See Inner Exception for details", inner)
        {
        }
    }

    public class DBContext
    {

        //Thread static variables

        [ThreadStatic]
        private static SqlCommand m_ContextTransactionCommand;

        [ThreadStatic]
        private static SqlConnection m_ContextTransactionConnection;

        [ThreadStatic]
        private static SqlTransaction m_ContextTransaction;

        [ThreadStatic]
        private static bool m_bContextTransactionInProgress;

        [ThreadStatic]
        private static int m_ActiveTransactionOpenCount;

        [ThreadStatic]
        private static System.Collections.Generic.List<DBContext> m_activeDbContextList;


        //Member variables
        private bool m_bStartedNewTransaction = false;

        private bool m_bReleased = false;

        private string m_strStackOnCreation = "";

        private void _Log(string str)
        {
            Logger.DBLogger.LogInfo("[DBCONTEXT] " + str + " Count=" + m_ActiveTransactionOpenCount + " Threadid = " + System.Threading.Thread.CurrentThread.ManagedThreadId);
        }

        public static void CloseReader(SqlDataReader reader)
        {
            try
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            catch
            {
                //Ignore any erros when we try to dispose/close
            }
        }


        public DBContext(bool bStartedNewTransaction)
        {
            try
            {
                m_ActiveTransactionOpenCount++;

                m_strStackOnCreation = new StackTrace().ToString();

                if (bStartedNewTransaction)
                {
                    //_Log("Starting new transaction");
                }
                else
                {
                    //_Log("Appending to existing transaction");
                }

                m_bStartedNewTransaction = bStartedNewTransaction;
                ResetForNextQuery();

                if (m_activeDbContextList == null)
                    m_activeDbContextList = new System.Collections.Generic.List<DBContext>();

                m_activeDbContextList.Add(this);
            }
            catch
            {
                //Any exceptions here is cleaned up here and the exception passed to caller
                try
                {
                    m_ActiveTransactionOpenCount--;
                    if (m_activeDbContextList != null)
                    {
                        m_activeDbContextList.Remove(this);
                    }
                }
                catch
                {
                    //Ignore any exceptions here since these doenst have any memory or handles
                }

                m_bReleased = true;
                throw;
            }
        }


        public static int TrasactionOpenCount
        {
            get
            {
                return m_ActiveTransactionOpenCount;
            }

        }


        public static System.Collections.Generic.List<DBContext> ActiveContextList
        {
            get
            {
                return m_activeDbContextList;
            }
        }

        public SqlCommand ContextSqlCommand
        {
            get
            {
                return m_ContextTransactionCommand;
            }
        }


        public string InstanceStackTrace
        {
            get
            {
                return m_strStackOnCreation;
            }

        }


        /// <summary>
        /// Starts a new transation or get current transation.
        /// </summary>
        /// <returns></returns>
        public static DBContext GetDBContext()
        {
            if (m_bContextTransactionInProgress)
            {
                return new DBContext(false);
            }

            try
            {


                m_ContextTransactionConnection = new SqlConnection(LibrarySettings.DatabaseConnectionString);
                m_ContextTransactionConnection.Open();
                m_ContextTransaction = m_ContextTransactionConnection.BeginTransaction();
                m_ContextTransactionCommand = new SqlCommand();
                m_ContextTransactionCommand.CommandTimeout = 600;
                m_ContextTransactionCommand.Connection = m_ContextTransactionConnection;
                m_ContextTransactionCommand.Transaction = m_ContextTransaction;
                m_bContextTransactionInProgress = true;



            }
            catch
            {
                //Reset the flag that we just set (to default)
                m_bContextTransactionInProgress = false;

                if (m_ContextTransaction != null)
                {
                    try
                    {
                        m_ContextTransaction.Dispose();
                        m_ContextTransaction = null;
                    }
                    catch
                    {
                        //Ignore any erros when we try to dispose transaction object
                    }
                }

                if (m_ContextTransactionConnection != null)
                {
                    try
                    {
                        m_ContextTransactionConnection.Close();
                        m_ContextTransactionConnection.Dispose();
                        m_ContextTransactionConnection = null;
                    }
                    catch
                    {
                        //Ignore any exceptions when we try to close 
                        //since, an exception here means that the connection was 
                        //not in open state
                    }
                }

                if (m_ContextTransactionCommand != null)
                {
                    try
                    {
                        m_ContextTransactionCommand.Dispose();
                        m_ContextTransactionCommand = null;
                    }
                    catch
                    {
                        //Ignore any errors disposing the command object
                    }
                }

                //IMPORTANT: While we ignore excecptions that arise when we attempt
                //to cleanup, never ignore the original exeception. Throw the original exception 
                //back to caller anyway

                throw;
            }

            return new DBContext(true);


        }

        /// <summary>
        /// Commits or rollbacks current transaction depends on the input.
        /// </summary>
        /// <param name="bCommit"></param>
        public void ReleaseDBContext(bool bCommit)
        {
            if (m_bReleased)
                return;

            m_bReleased = true;

            m_ActiveTransactionOpenCount--;

            if (m_activeDbContextList != null)
                m_activeDbContextList.Remove(this);

            if (m_bContextTransactionInProgress)
            {
                if (!m_bStartedNewTransaction)  //If we dint start a new trans, we dont do commit..
                    return;

                try
                {
                    if (bCommit)
                    {
                        m_ContextTransaction.Commit();
                    }
                    else
                    {
                        m_ContextTransaction.Rollback();
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {

                    //Attempt to cleanup Transaction object
                    try
                    {
                        m_ContextTransaction.Dispose();

                    }
                    catch { }


                    //Attempt to cleanup Connection object
                    try
                    {
                        m_ContextTransactionConnection.Close();
                        m_ContextTransactionConnection.Dispose();
                    }
                    catch { }

                    //Attempt to cleanup Command object
                    try
                    {
                        m_ContextTransactionCommand.Dispose();
                    }
                    catch { }


                    m_ContextTransactionConnection = null;
                    m_ContextTransactionCommand = null;
                    m_ContextTransaction = null;
                    m_bContextTransactionInProgress = false;
                }

            }
            else
            {
                //Unexpected!  
                throw new System.Exception("There is no context transaction in progress");
            }


            return;
        }


        public void ResetForNextQuery()
        {
            if (m_bContextTransactionInProgress)
            {
                m_ContextTransactionCommand.CommandText = "";
                m_ContextTransactionCommand.CommandType = CommandType.Text;
                m_ContextTransactionCommand.Parameters.Clear();
                return;
            }
            throw new System.Exception("There is no context transaction in progress");
        }
    }
}
