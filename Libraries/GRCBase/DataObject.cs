﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace GRCBase
{
    public class DataObjectEventArgs : System.EventArgs
    {
        DataObject m_dataObject;
        public DataObjectEventArgs(DataObject dataObject)
        {
            m_dataObject = dataObject;
        }
    }

    /// <summary>
    /// This class should be implemented for each DataObject derived class.</summary>
    public abstract class PrimaryKey
    {
        public abstract string WhereClause
        {
            get;
        }
    }

    /// <summary>
    /// This is an abstract class which has to be dervied for each Database object
    /// that is a part of the system
    /// </summary>
    public abstract class DataObject
    {
        /// <summary>
        /// Only allow dervied classes to construct the DataObject
        ///</summary>
        protected DataObject()
        {
        }

        /// <summary>
        /// If the dervied class contains embedded DataObject or DataObjectList
        /// members, this method should be overriden and should return an ArrayList of 
        /// such members.
        /// Whenever updating the derived class to database, each such member will
        /// also be checked for updates and will be updated automatically by the 
        /// DataObject.UpdateChanges() method
        /// </summary>

        protected virtual ArrayList ListMembers
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// This property should be implemented in the derived class.
        /// It should return the PrimaryKey for the instance
        /// </summary>
        public abstract PrimaryKey Key
        {
            get;
            set;
        }

        /// <summary>
        /// This method should be implemented in the derived class.
        /// It should initialize an instance from database based on the PrimaryKey passed in
        /// The parameter objCmd should be used for all sql queries to the 
        /// database during the initialization.
        /// </summary>
        protected abstract bool internalLoad(PrimaryKey key, SqlCommand objCmd);

        /// <summary>
        /// This method should be implemented in the derived class.
        /// It should create database a row corresponding to the DataObject and
        /// return the PrimaryKey for the new row inserted
        /// The parameter objCmd should be used for all sql queries to the 
        /// database during this process.
        /// </summary>
        protected abstract PrimaryKey internalInsert(SqlCommand objCmd);

        /// <summary>
        /// This method should be implemented in the derived class.
        /// It should delete database a row corresponding to the DataObject.
        /// The parameter objCmd should be used for all sql queries to the 
        /// database during this process.
        /// </summary>
        protected abstract void internalDelete(SqlCommand objCmd);

        /// <summary>
        /// This method should be implemented in the derived class.
        /// It should update the DataObject to the database.
        /// The parameter objCmd should be used for all sql queries to the 
        /// database during this process.
        /// </summary>
        protected abstract void internalUpdate(SqlCommand objCmd);


        /// <summary>
        /// This method should be implemented in the derived class if DataBinding is
        /// to be used in it. Overriding this method is optional.
        /// DataBinding refers to initializing an instance of DataObject to the
        /// values present from another object. The other object will usually be objects
        /// like OleDbDataReader, OleDbDataRow etc.
        /// The parameter objSource should be used to initialize the DataObject
        /// </summary>
        protected virtual void internalOnDataBind(object objSource)
        {
            //base class implementation. 
            Logger.DBLogger.LogError("Derived class should override this method if DataBinding is used");
            throw new System.Exception("Derived class should override this method if DataBinding is used");
        }

        private bool m_bInitialized;
        private bool m_bDeleted;
        private bool m_bIsDirty;


        /// <summary>
        /// Whenever the state of the DataObject is modified and needs updation to 
        /// the database, this property has to be set
        /// </summary>

        public bool IsUpdated
        {
            set
            {
                m_bIsDirty = value;
            }
            get
            {
                return m_bIsDirty;
            }
        }

        /// <summary>
        /// Call this methed to create a datarow on the Database corresponding to
        /// the instance.
        /// </summary>
        protected PrimaryKey Create()
        {
            DBContext dbContext = DBContext.GetDBContext();
            try
            {
                dbContext.ResetForNextQuery();
                Key = internalInsert(dbContext.ContextSqlCommand);
                Logger.DBLogger.LogInfo("Inserted row for " + this.GetType().ToString() + " object to database [WhereClause: " + this.Key.WhereClause + "]");
                dbContext.ReleaseDBContext(true);
                m_bInitialized = true;
            }
            catch (Exception x)
            {
                dbContext.ReleaseDBContext(false);
                throw;
            }
            finally
            {

            }
            m_bIsDirty = false;
            return Key;
        }

        /// <summary>
        //Call this method to initialize an instance from a collection
        /// </summary>
        public void DataBind(IEnumerable collection)
        {
            if (this.m_bInitialized == true || this.m_bDeleted == true)
            {
                Logger.DBLogger.LogError("DataBind : Attempt to Bind on a object that is already initialized or deleted");
                throw new System.Exception("Attempt to Bind on a object that is already initialized or deleted");
            }

            try
            {
                internalOnDataBind(collection);
            }
            catch (Exception x)
            {
                throw;
            }
            Logger.DBLogger.LogInfo("Loaded " + this.GetType().ToString() + " from DB [" + this.Key.WhereClause + "]");
            //SQLHelper.DBTrace(TraceLevel.Verbose, "Loaded " + this.GetType().ToString() + " from DB [" + this.Key.WhereClause + "]");
            m_bInitialized = true;
            return;
        }

        /// <summary>
        /// Call this method to initialize an instance by PrimaryKey
        /// </summary>

        public bool Load(PrimaryKey key)
        {
            if (this.m_bInitialized || this.m_bDeleted)
            {
                Logger.DBLogger.LogError("Attempt to Load on a object that is already initialized or deleted");
                throw new System.Exception("Attempt to Load on a object that is already initialized or deleted");
            }

            DBContext dbContext = DBContext.GetDBContext();
            try
            {
                dbContext.ResetForNextQuery();

                m_bInitialized = internalLoad(key, dbContext.ContextSqlCommand);

                if (m_bInitialized)
                {
                    Logger.DBLogger.LogInfo("Loaded " + this.GetType().ToString() + " from DB [" + key.WhereClause + "]");
                }
                else
                {
                    Logger.DBLogger.LogInfo("DNE: " + this.GetType().ToString() + " object from database [WhereClause: " + key.WhereClause + "]");
                }
            }
            catch (Exception x)
            {
                throw;
            }
            finally
            {
                dbContext.ReleaseDBContext(true); //We always commit load !?
            }
            return m_bInitialized;
        }

        /// <summary>
        /// Call this method to delete database rows corresponding to an instance
        /// </summary>

        public void Delete()
        {
            DBContext dbContext = DBContext.GetDBContext();

            try
            {
                dbContext.ResetForNextQuery();
                internalDelete(dbContext.ContextSqlCommand);
                Logger.DBLogger.LogInfo("Deleted row for " + this.GetType().ToString() + " object from database [WhereClause: " + this.Key.WhereClause + "]");

                dbContext.ReleaseDBContext(true);
            }
            catch (Exception x)
            {
                dbContext.ReleaseDBContext(false);
                throw;
            }
            finally
            {

            }
            this.m_bDeleted = true;
        }

        /// <summary>
        /// Call this method whenever some changes has been made to the DataObject
        /// and needs updation to database
        /// Note: This method will recursively call the UpdateChanges() method on
        /// all the embeded DataObject members and DataObjectList members. (As 
        /// returned by ListMembers property
        /// </summary>

        public void UpdateChanges()
        {
            if (m_bDeleted)
            {
                Logger.DBLogger.LogError("Attempt to CommitChanges on a Row that is deleted");

                throw new System.Exception("Attempt to CommitChanges on a Row that is deleted");
            }

            if (!m_bInitialized)
            {
                Logger.DBLogger.LogError("Attempt to CommitChanges on a Row that is not initialized");
                throw new System.Exception("Attempt to CommitChanges on a Row that is not initialized");
            }

            ArrayList objVisitedList = new ArrayList();
            if (!IsDirtyRecursive(objVisitedList))
            {
                return;
            }

            //Some thing in the chain is dirty and we need to update!
            DBContext dbContext = DBContext.GetDBContext();
            ArrayList objUpdatedList = null;
            try
            {
                objUpdatedList = new ArrayList();
                objVisitedList = new ArrayList();

                DoUpdates(dbContext, objUpdatedList, objVisitedList);

                dbContext.ReleaseDBContext(true);

            }
            catch (System.Exception e)
            {
                dbContext.ReleaseDBContext(false);

                if (objUpdatedList != null)
                {
                    foreach (DataObject obj in objUpdatedList)
                    {
                        //set it back...
                        obj.m_bIsDirty = true;
                    }
                }

                throw;
            }
            finally
            {

            }
            return;
        }


        private bool IsDirtyRecursive(ArrayList objVisitedList)
        {
            foreach (object objAlready in objVisitedList)
            {
                if (objAlready == this)
                {
                    return false;	//if loop; return false for now..
                }
            }

            objVisitedList.Add(this);

            if (this.m_bIsDirty)
            {
                return true;
            }

            ArrayList arrayListMembers = ListMembers;
            if (arrayListMembers == null)
            {
                return false;
            }

            //Now update the contained objects and objects in list objects with this DataObject
            for (int i = 0; i < arrayListMembers.Count; i++)
            {
                if (arrayListMembers[i] == null)
                {
                    //This usually should not happen.. but better sheilded..
                    continue;
                }

                if (arrayListMembers[i] is DataObjectList)
                {
                    DataObjectList objDataList = (DataObjectList)arrayListMembers[i];
                    foreach (DataObject obj in objDataList)
                    {
                        if (obj.IsDirtyRecursive(objVisitedList))
                        {
                            return true;
                        }
                    }

                }
                else if (arrayListMembers[i] is DataObject)
                {
                    DataObject objData = (DataObject)arrayListMembers[i];
                    if (objData.IsDirtyRecursive(objVisitedList))
                    {
                        return true;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.Assert(false, "Invalid object type passed");
                }
            }
            return false;
        }

        private void DoUpdates(DBContext dbContext, ArrayList objUpdatedList, ArrayList objVisitedList)
        {
            foreach (object objAlready in objVisitedList)
            {
                if (objAlready == this)
                {
                    return;
                }
            }

            objVisitedList.Add(this);

            if (this.m_bIsDirty)
            {
                dbContext.ResetForNextQuery();

                Logger.DBLogger.LogInfo("Updating " + this.GetType().ToString() + " to DB [" + this.Key.WhereClause + "]");

                internalUpdate(dbContext.ContextSqlCommand);
                objUpdatedList.Add(this);
                m_bIsDirty = false;
            }

            ArrayList arrayListMembers = ListMembers;

            if (arrayListMembers == null)
            {
                return;
            }

            //Now update the contained objects and objects in list objects with this DataObject
            for (int i = 0; i < arrayListMembers.Count; i++)
            {
                if (arrayListMembers[i] == null)
                {
                    //This usually should not happen.. but better sheilded..
                    continue;
                }

                if (arrayListMembers[i] is DataObjectList)
                {
                    DataObjectList objDataList = (DataObjectList)arrayListMembers[i];
                    foreach (DataObject obj in objDataList)
                    {
                        obj.DoUpdates(dbContext, objUpdatedList, objVisitedList);
                    }

                }
                else if (arrayListMembers[i] is DataObject)
                {
                    DataObject objData = (DataObject)arrayListMembers[i];
                    objData.DoUpdates(dbContext, objUpdatedList, objVisitedList);
                }
                else
                {
                    System.Diagnostics.Debug.Assert(false, "Invalid object type passed");
                }
            }
        }

    }

    public class DataObjectList : ArrayList
    {
        private SqlCommand m_command;
        public SqlCommand TransactionCommand
        {
            set
            {
                m_command = value;
            }
        }

        public override int Add(object obj)
        {
            if (!obj.GetType().IsSubclassOf(typeof(DataObject)))
            {
                Logger.DBLogger.LogError("Cannot add non DataObject types to DataObjectList");

                throw new System.Exception("Cannot add non DataObject types to DataObjectList");
            }
            return base.Add(obj);
        }

    }
}
