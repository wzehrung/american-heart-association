﻿using System;
using System.Web.UI;

namespace GRCBase
{
    public abstract class GRCCachedWebControlBase : UserControl
    {
        private bool m_bCachingEnabled = false;
        public GRCCachedWebControlBase(bool bEnabled)
        {
            m_bCachingEnabled = bEnabled;

        }

        public bool IsCached
        {
            get
            {
                return m_bCachingEnabled;
            }
        }

        protected override void OnInit(System.EventArgs e)
        {
            if (!IsCached)
            {
                CachePolicy.Duration = TimeSpan.FromSeconds(0);
            }
            base.OnInit(e);
        }
    }
}
