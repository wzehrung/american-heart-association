﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using System.Data;

namespace GRCBase
{
    public class MiscUtility
    {
        public static string GetScriptForRedirection(string strUrl)
        {
            return string.Format("<script type=\"text/javascript\">function RedirectToParentWindow(strUrl) {{if (window.opener != null) {{ window.opener.location.replace(strUrl); return; }} top.location.href = strUrl;}}RedirectToParentWindow(\"{0}\");</script>", strUrl);
        }

        public static string GetComponentModelPropertyDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes =
                  (DescriptionAttribute[])fi.GetCustomAttributes(
                  typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();

        }

        public static DataTable SortDataTable(DataTable tableToSort, int sortByColumnIndex, bool isDescending)
        {
            DataTable sortedTable = new DataTable();
            foreach (DataColumn col in tableToSort.Columns)
            {
                sortedTable.Columns.Add(col.ColumnName, col.DataType);
            }

            try
            {
                string order = " ASC";
                if (isDescending)
                {
                    order = " DESC";
                }
                tableToSort.DefaultView.Sort = tableToSort.Columns[sortByColumnIndex].ColumnName + order;

                foreach (DataRowView drView in tableToSort.DefaultView)
                {
                    DataRow newRow = sortedTable.NewRow();
                    foreach (DataColumn col in tableToSort.Columns)
                    {
                        newRow[col.ColumnName] = drView[col.ColumnName];
                    }
                    sortedTable.Rows.Add(newRow);
                }
            }
            catch (Exception eg)
            {
                throw;
            }
            return sortedTable;
        }

        public static bool IsRequestHttpGetMethod()
        {
            return (System.Web.HttpContext.Current.Request.HttpMethod == "GET");
        }

        public static void DbgWrite(string str)
        {
            System.Diagnostics.Debug.WriteLine(str);
        }

        public static bool CheckBrowser(string str)
        {
            string detect = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToLower();
            if (detect.IndexOf(str) > 0)
            {
                return true;
            }
            return false;
        }

        public static bool IsBrowserSafari()
        {
            if (GetBrowser().CompareTo("Safari") == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsBrowserNetscape()
        {
            if (GetBrowser().CompareTo("Netscape Navigator") == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetBrowser()
        {
            string browser = string.Empty;
            if (CheckBrowser("konqueror"))
            {
                browser = "Konqueror";
                //OS = "Linux";
            }
            else if (CheckBrowser("safari"))
            {
                browser = "Safari";
            }
            else if (CheckBrowser("omniweb"))
            {
                browser = "OmniWeb";
            }
            else if (CheckBrowser("opera"))
            {
                browser = "Opera";
            }
            else if (CheckBrowser("webtv"))
            {
                browser = "WebTV";
            }
            else if (CheckBrowser("icab"))
            {
                browser = "iCab";
            }
            else if (CheckBrowser("msie"))
            {
                browser = "Internet Explorer";
            }
            else if (!CheckBrowser("compatible"))
            {
                browser = "Netscape Navigator";
                //version = detect.charAt(8);
            }
            else
            {
                browser = "An unknown browser";
            }

            //if (!version) version = detect.charAt(place + thestring.length);

            /*
            if (!OS)
            {
                if (checkIt('linux')) OS = "Linux";
                else if (checkIt('x11')) OS = "Unix";
                else if (checkIt('mac')) OS = "Mac"
                                             else if (checkIt('win')) OS = "Windows"
                                                                          else OS = "an unknown operating system";
            }
            */
            return browser;
        }


    }
}
