﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace GRCBase
{
    public class XmlUtility
    {
        public static string GetXMLDataFromNode(XmlNode node, string childNodeName)
        {
            if (node.SelectSingleNode(childNodeName) != null && node.SelectSingleNode(childNodeName).InnerXml.Length > 0)
                return node.SelectSingleNode(childNodeName).InnerXml;
            return string.Empty;
        }

        public static string GetDataFromNode(XmlNode node)
        {
            if (node != null && node.InnerText.Length > 0)
                return node.InnerText;
            return string.Empty;
        }

        public static string GetDataFromChildNode(XmlNode node, string childNodeName)
        {
            if (node.SelectSingleNode(childNodeName) != null && node.SelectSingleNode(childNodeName).InnerText.Length > 0)
                return node.SelectSingleNode(childNodeName).InnerText;
            return string.Empty;
        }

        public static string GetAttributeDataFromNode(XmlNode node, string nodeAttributeName)
        {
            if (node.Attributes != null)
            {
                if (node.Attributes[nodeAttributeName] != null && node.Attributes[nodeAttributeName].InnerText.Length > 0)
                    return node.Attributes[nodeAttributeName].InnerText;
            }

            return string.Empty;
        }

        public static int GetChildNodeIntValue(XmlNode node, string childNodeName)
        {
            return Convert.ToInt32(node.SelectSingleNode(childNodeName).InnerText);
        }

        public static string GetChildNodeStringValue(XmlNode node, string childNodeName)
        {
            if (node.SelectSingleNode(childNodeName) != null)
                return node.SelectSingleNode(childNodeName).InnerText;
            return string.Empty;
        }


        public static string GetAttributeStringValue(XmlNode node, string attributeName)
        {
            if (node.Attributes[attributeName] != null)
                return node.Attributes[attributeName].InnerText;
            return string.Empty;
        }

        public static int? GetAttributeIntValue(XmlNode node, string attributeName)
        {
            if (node.Attributes[attributeName] != null)
                return Convert.ToInt32(node.Attributes[attributeName].InnerText);
            return null;
        }

        public static string AddCDATATagToString(string unsafeString)
        {
            return "<![CDATA[" + unsafeString + "]]>";
        }

        public static string RemoveCDATATagFromString(string safeString)
        {
            safeString = safeString.Replace("<![CDATA[", string.Empty);
            safeString = safeString.Replace("]]>", string.Empty);
            return safeString;
        }

        public static string GetStringValue(XmlNode xNode, string attribName, string defVal)
        {
            string retVal;

            if (xNode != null && xNode.Attributes[attribName] != null)
            {
                retVal = xNode.Attributes[attribName].Value;
            }
            else
            {
                retVal = defVal;
            }

            return retVal;
        }

        public static bool GetBoolValue(XmlNode xNode, string attribName, string desiredValue, bool defVal)
        {
            bool retVal;

            if (xNode != null && xNode.Attributes[attribName] != null)
            {
                retVal = xNode.Attributes[attribName].Value.ToLower() == desiredValue ? true : false;
            }
            else
            {
                retVal = defVal;
            }

            return retVal;
        }

        public static XmlDocument DoLoadXML(string xmlString)
        {
            XmlDocument myDoc = new XmlDocument();
            try
            {
                myDoc.Load(new StringReader(xmlString));
                return myDoc;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Loads the xml from a given path
        /// </summary>
        /// <param name="filePath">path where xml file resides</param>
        /// <returns>XmlDocument</returns>
        public static XmlDocument DoLoadFile(string filePath)
        {
            XmlDocument myDoc = new XmlDocument();
            try
            {
                myDoc.Load(filePath);
                return myDoc;
            }
            catch
            {
                throw;
            }
        }
    }
}
