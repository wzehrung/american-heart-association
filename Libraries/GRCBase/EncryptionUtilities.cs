﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace GRCBase.EncryptionUtilities
{
    public class BlowFish
    {
        public BlowFish(string key)
        {

        }

        public static string Encrypt(string key, string instr)
        {
            byte[] key_bytes = BlowfishECB.getAsciiBytes(key);
            byte[] b1 = BlowfishECB.getAsciiBytes(instr);
            BlowfishECB crypt = new BlowfishPKCS5(key_bytes);
            byte[] encrypted = crypt.encrypt(b1);
            encrypted = BinConverter.bytesToBinHex(encrypted);
            string str_encrypted = BlowfishECB.getAsciiString(encrypted);

            return str_encrypted;
        }

        public static string Decrypt(string key, string encrypted_string)
        {
            byte[] key_bytes = BlowfishECB.getAsciiBytes(key);
            BlowfishECB crypt = new BlowfishPKCS5(key_bytes);
            byte[] encrypted_bytes = BlowfishECB.getAsciiBytes(encrypted_string);
            byte[] encrypted_bytes2 = BinConverter.binHexToBytes(encrypted_bytes);
            byte[] decrypted = crypt.decrypt(encrypted_bytes2);
            return BlowfishECB.getAsciiString(decrypted);
        }
    }

    public class Scrambler
    {
        public static byte[] ConvertStringToByteArray(string stringToConvert)
        {
            return (new UnicodeEncoding()).GetBytes(stringToConvert);
        }

        public static byte[] ScrambleKey()
        {
            return Scrambler.ConvertStringToByteArray("encoding");
        }

        // Initialization vector management for scrambling support
        public static byte[] ScrambleIV()
        {
            return Scrambler.ConvertStringToByteArray("decoding");
        }

        public static string Scramble(string message)
        {
            UTF8Encoding textConverter = new UTF8Encoding();
            RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();

            //Convert the data to a byte array.
            byte[] toEncrypt = textConverter.GetBytes(message);

            //Get an encryptor.
            ICryptoTransform encryptor = rc2CSP.CreateEncryptor(Scrambler.ScrambleKey(), Scrambler.ScrambleIV());

            //Encrypt the data.
            MemoryStream msEncrypt = new MemoryStream();
            CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

            //Write all data to the crypto stream and flush it.
            // Encode length as first 4 bytes
            byte[] length = new byte[4];
            length[0] = (byte)(message.Length & 0xFF);
            length[1] = (byte)((message.Length >> 8) & 0xFF);
            length[2] = (byte)((message.Length >> 16) & 0xFF);
            length[3] = (byte)((message.Length >> 24) & 0xFF);
            csEncrypt.Write(length, 0, 4);
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
            csEncrypt.FlushFinalBlock();

            //Get encrypted array of bytes.
            byte[] encrypted = msEncrypt.ToArray();

            // Convert to Base64 string
            string b64 = Convert.ToBase64String(encrypted);

            // Protect against URLEncode/Decode problem
            string b64mod = b64.Replace('+', '@');

            // Return a URL encoded string
            return HttpUtility.UrlEncode(b64mod);
        }

        public static string Descramble(string scrambledMessage)
        {
            UTF8Encoding textConverter = new UTF8Encoding();
            RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
            // URL decode , replace and convert from Base64
            string b64mod = HttpUtility.UrlDecode(scrambledMessage);
            // Replace '@' back to '+' (avoid URLDecode problem)
            string b64 = b64mod.Replace('@', '+');
            // Base64 decode
            byte[] encrypted = Convert.FromBase64String(b64);

            //Get a decryptor that uses the same key and IV as the encryptor.
            ICryptoTransform decryptor = rc2CSP.CreateDecryptor(Scrambler.ScrambleKey(), Scrambler.ScrambleIV());

            //Now decrypt the previously encrypted message using the decryptor
            // obtained in the above step.
            MemoryStream msDecrypt = new MemoryStream(encrypted);
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

            byte[] fromEncrypt = new byte[encrypted.Length - 4];

            //Read the data out of the crypto stream.
            byte[] length = new byte[4];
            csDecrypt.Read(length, 0, 4);
            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
            int len = (int)length[0] | (length[1] << 8) | (length[2] << 16) | (length[3] << 24);

            //Convert the byte array back into a string.
            return textConverter.GetString(fromEncrypt).Substring(0, len);
        }
    }
}
