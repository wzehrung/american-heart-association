﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GRCBase
{
    public class PhoneHelper
    {
        /// <summary>
        /// Formats only US telephone numbers.
        /// </summary>
        public static string GetFormattedUSPhoneNumber(string strUnformattedNumber)
        {
            if (string.IsNullOrEmpty(strUnformattedNumber))
                return string.Empty;

            string strFormattedNumber = string.Empty;

            // we really only understand 10 digit phone numbers
            if (strUnformattedNumber.Length == 10)  // Format number
            {
                strUnformattedNumber = strUnformattedNumber.Replace("(", string.Empty).Replace("(", string.Empty).Replace("-", string.Empty);

                strFormattedNumber += "(" + strUnformattedNumber.Substring(0, 3) + ") "
                    + strUnformattedNumber.Substring(3, 6 - 3) + "-"
                    + strUnformattedNumber.Substring(6, 10 - 6);
            }
            else  // Do not format number, because number is not parseable
            {
                strFormattedNumber = strUnformattedNumber;
            }
            return strFormattedNumber;
        }
    }
}
