﻿
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Xml;
    using System.Xml.XPath;

    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;

namespace HVWrapper
{
    /// <summary>
    /// Summary description for Message
    /// </summary>
    public class Message : HealthRecordItemCustomBase
    {

        public int? MessageID;
        public DateTime? DateSent;
        public bool? IsSentByProvider;
        public string Subject;
        public string MessageBody;
        public int? ProviderID;


        private const string format_appid = "microsoft-pre-schema";
        private const string format_tag = "heart360-message-data";

        //Important: This XML XPath query must be an exact match to the XML on the server, or we will never get our data
        public const string XPathFilterForQuery = "/thing/data-xml/app-specific/CustomType/app-specific[format-tag = \"" + format_tag + "\"]";

        const string xmlnode_MessageID = "message-id";
        const string xmlnode_DateSent = "date-sent";
        const string xmlnode_IsSentByProvider = "is-sent-by-provider";
        const string xmlnode_Subject = "message-subject";
        const string xmlnode_MessageBody = "message-body";
        const string xmlnode_ProviderID = "provider-id";

        /// <summary> 
        /// Populates the data for the zone from the XML.
        /// </summary>
        /// 
        public override void ParseXml(IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.SelectSingleNode("app-specific");

            if (navigator == null)
            {
                throw new ArgumentNullException("null navigator");
            }

            this.DateSent
                = ParseHelper.ParseDateTime(navigator, xmlnode_DateSent);

            this.IsSentByProvider
               = ParseHelper.ParseTrueFalseNull(navigator, xmlnode_IsSentByProvider);

            this.MessageID = ParseHelper.ParseInt(navigator, xmlnode_MessageID);

            this.Subject = ParseHelper.ParseString(navigator, xmlnode_Subject);

            this.MessageBody = ParseHelper.ParseString(navigator, xmlnode_MessageBody);

            this.ProviderID = ParseHelper.ParseInt(navigator, xmlnode_ProviderID);
        }

        /// <summary> 
        /// Writes the zone information to the specified XML writer.
        /// </summary>
        public override void WriteXml(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("null writer");
            }

            writer.WriteStartElement("app-specific");

            {
                writer.WriteStartElement("format-appid");
                writer.WriteValue(format_appid);
                writer.WriteEndElement();

                writer.WriteStartElement("format-tag");
                writer.WriteValue(format_tag);
                writer.WriteEndElement();

                When.WriteXml("when", writer);

                writer.WriteStartElement("summary");
                writer.WriteValue("");
                writer.WriteEndElement();

                //=====================================================
                writer.WriteStartElement(xmlnode_MessageID);
                if (this.MessageID.HasValue)
                {
                    writer.WriteValue(this.MessageID.Value.ToString());
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_IsSentByProvider);
                if (this.IsSentByProvider.HasValue)
                {
                    writer.WriteValue(this.IsSentByProvider.Value);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_DateSent);
                if (this.DateSent.HasValue)
                {
                    writer.WriteValue(this.DateSent.Value);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_Subject);
                if (!string.IsNullOrEmpty(this.Subject))
                {
                    writer.WriteValue(this.Subject);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_MessageBody);
                if (!string.IsNullOrEmpty(this.MessageBody))
                {
                    writer.WriteValue(this.MessageBody);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_ProviderID);
                if (this.ProviderID.HasValue)
                {
                    writer.WriteValue(this.ProviderID.Value);
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// A string representation of the object.
        /// </summary>
        public override string ToString()
        {
            //UNDONE: Should print out all the field values.
            return "Patient Provider Messaging.";
        }


    }
}