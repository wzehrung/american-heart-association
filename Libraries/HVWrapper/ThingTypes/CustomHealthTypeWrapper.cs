
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //

    using System;
    using System.Data;
    using System.Configuration;
    /**
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Web.UI.HtmlControls;
     **/
    using System.Reflection;  
    using System.Xml;
    using System.Xml.Serialization;
    using System.Xml.XPath;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper
{
    /// <summary>
    /// Summary description for CustomHealthTypeWrapper
    /// </summary>
    public class CustomHealthTypeWrapper : HealthRecordItem
    {
        const string NullObjectTypeName = "NULLOBJECT";

        private HealthServiceDateTime m_when;
        private HealthRecordItemCustomBase m_wrappedObject = null;

        /// <summary>
        /// Don't use this one, use the one that takes the custom object instance as a parameter
        /// </summary>
        /// <remarks>
        /// This one has to be here so that the API layer can deserialize the object...
        /// </remarks>
        public CustomHealthTypeWrapper()
            : this(null)
        {
        }

        public CustomHealthTypeWrapper(HealthRecordItemCustomBase wrappedInstance)
            : base(new Guid(ApplicationCustomTypeID))
        {
            m_wrappedObject = wrappedInstance;

            // Test for null here because the deserialization code needs to create this object first...
            if (wrappedInstance != null)
            {
                wrappedInstance.Wrapper = this;
            }
        }

        public HealthServiceDateTime When
        {
            get { return m_when; }
            set { m_when = value; }
        }

        public HealthRecordItemCustomBase WrappedObject
        {
            get { return m_wrappedObject; }
            set { m_wrappedObject = value; }
        }

        public object CreateObjectByName(string typeName)
        {
                // PJB - Since we moved the custom types from AHAAPI project/namespace to HVWrapper project/namespace,
                //       we have to convert any references of AHAAPI to HVWrapper.
            string  typeNameToUse = (typeName.StartsWith("AHAAPI")) ? typeName.Replace("AHAAPI", "HVWrapper") : typeName;
            Type    type = Type.GetType(typeNameToUse );
            object  o = null;

            if (type != null)
            {
                // Validate that the object inherits from HealthRecordItemCustomBase
                Type baseType = type.BaseType;
                while (true)
                {
                    if (baseType == null)
                    {
                        throw new ApplicationException("Custom type not derived from HealthRecordItemCustomBase");
                    }

                    if (baseType == typeof(HealthRecordItemCustomBase))
                    {
                        break;
                    }
                    baseType = baseType.BaseType;
                }

                o = Activator.CreateInstance(type);
            }

            return o;
        }

        public override void WriteXml(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("null writer");
            }

            writer.WriteStartElement("app-specific");
            {
                writer.WriteStartElement("format-appid");
                writer.WriteValue("Custom");
                writer.WriteEndElement();

                string wrappedTypeName = NullObjectTypeName;
                if (m_wrappedObject != null)
                {
                    Type type = m_wrappedObject.GetType();
                    wrappedTypeName = type.FullName;
                }
                writer.WriteStartElement("format-tag");
                writer.WriteValue(wrappedTypeName);
                writer.WriteEndElement();

                m_when.WriteXml("when", writer);

                writer.WriteStartElement("summary");
                writer.WriteValue("");
                writer.WriteEndElement();

                if (m_wrappedObject != null)
                {
                    writer.WriteStartElement("CustomType");
                    m_wrappedObject.WriteXml(writer);
                    writer.WriteEndElement();
                }
            }

            writer.WriteEndElement();
        }

        /// <summary> 
        /// Populates the data for the flow zone from the XML.
        /// </summary>
        /// 
        protected override void ParseXml(IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.SelectSingleNode("app-specific");

            if (navigator == null)
            {
                throw new ArgumentNullException("null navigator");
            }

            XPathNavigator when = navigator.SelectSingleNode("when");
            m_when = new HealthServiceDateTime();
            m_when.ParseXml(when);

            XPathNavigator formatAppid = navigator.SelectSingleNode("format-appid");
            string appid = formatAppid.Value;

            XPathNavigator formatTag = navigator.SelectSingleNode("format-tag");
            string wrappedTypeName = formatTag.Value;

            if (wrappedTypeName == "AHAAppProfile")
            {
                wrappedTypeName = "HVWrapper.H360AppProfile";
            }
            if (wrappedTypeName == "AHAAPI.Message")    // PJB: we need to be backwards compatible with old messages from v5 AHAAPI days.
            {
                wrappedTypeName = "HVWrapper.Message";
            }

            if (wrappedTypeName != NullObjectTypeName)
            {
                m_wrappedObject = (HealthRecordItemCustomBase)CreateObjectByName(wrappedTypeName);

                if (m_wrappedObject != null)
                {
                    m_wrappedObject.Wrapper = this;
                    XPathNavigator customType = navigator.SelectSingleNode("CustomType");
                    if (customType != null)
                    {
                        m_wrappedObject.ParseXml(customType);
                    }
                }
            }
        }

        // Required so we can call it directly
        public void ParseXmlPublic(IXPathNavigable typeSpecificXml)
        {
            ParseXml(typeSpecificXml);
        }


        /// <summary>
        /// The custom type-id we want to use as the parent object for all
        /// self-defined types in our tree
        /// </summary>
        public const string ApplicationCustomTypeID = "a5033c9d-08cf-4204-9bd3-cb412ce39fc0";


        /// <summary>
        /// Thread synconization lock
        /// </summary>
        private static object lock_RegisterCustomDataType = new object();

        /// <summary>
        /// True if the the custom data type hander has alrady been registered
        /// </summary>
        private static bool s_isCustomDataTypeHanderRegistered = false;

        /// <summary>
        /// Registered the custom data type handler.
        /// 
        /// Does nothign if the handler has already been registered
        /// </summary>
        public static void RegisterCustomDataTypeHandler_OnceOnly()
        {
            //Thread safe: If we are already registered, then exit
            if (s_isCustomDataTypeHanderRegistered) return;

            //Serialize the the registration process
            lock (lock_RegisterCustomDataType)
            {
                //If someone got in right before us, exit.
                if (s_isCustomDataTypeHanderRegistered) return;
                RegisterCustomDataType();
            }
        }

        public static void RegisterCustomDataType()
        {
            ItemTypeManager.RegisterTypeHandler(new Guid(ApplicationCustomTypeID), typeof(CustomHealthTypeWrapper), true);
            s_isCustomDataTypeHanderRegistered = true;
        }

    }
}