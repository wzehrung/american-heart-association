
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Xml;
    using System.Xml.XPath;

    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;

namespace HVWrapper
{
    /// <summary>
    /// Summary description for AppProfile
    /// </summary>
    public abstract class AppProfile : HealthRecordItemCustomBase
    {
        public bool? HasUserAgreedToStoreDataInDB;
        public bool? HasUserAgreedToTermsAndConditions;
        public bool? HasUserAgreedToNewTermsAndConditions;

        //------------------------------------------------------------
        //Persistence constants
        //------------------------------------------------------------

        private const string format_appid = "microsoft-pre-schema";
        const string xmlnode_has_user_agreed_to_db_terms = "has-user-agreed-to-db-terms";
        const string xmlnode_has_user_agreed_to_terms_and_cond = "has-user-agreed-to-terms-and-cond";
        const string xmlnode_has_user_agreed_to_new_terms_and_cond = "has-user-agreed-to-new-terms-and-cond";
        
        /// <summary>
        /// The format tag that will be used in XPathFilterForQuery
        /// </summary>
        protected abstract string FormatTag
        {
            get;
        }

        /// <summary> 
        /// Populates the data for the zone from the XML.
        /// </summary>
        /// 
        public override void ParseXml(IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.SelectSingleNode("app-specific");

            if (navigator == null)
            {
                throw new ArgumentNullException("null navigator");
            }


            this.HasUserAgreedToStoreDataInDB = helper_SelectAndParse_TrueFalseNullMissing(
                                                navigator,
                                                xmlnode_has_user_agreed_to_db_terms);

            this.HasUserAgreedToTermsAndConditions = helper_SelectAndParse_TrueFalseNullMissing(
                                                navigator,
                                                xmlnode_has_user_agreed_to_terms_and_cond);

            this.HasUserAgreedToNewTermsAndConditions = helper_SelectAndParse_TrueFalseNullMissing(
                                               navigator,
                                               xmlnode_has_user_agreed_to_new_terms_and_cond);


            //See if any derrived classes have data they want to extract
            ParseAdditionalXml(navigator);
        }


        /// <summary>
        /// Derriving classes can override and de-persist their own extra data here...
        /// </summary>
        protected virtual void ParseAdditionalXml(XPathNavigator navigator)
        {
        }

        /// <summary>
        /// Derriving classes can override and persist their own extra data here...
        /// </summary>
        protected virtual void WriteAdditionalXml(XmlWriter writer)
        {
        }

        /// <summary> 
        /// Writes the zone information to the specified XML writer.
        /// </summary>
        public override void WriteXml(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("null writer");
            }

            writer.WriteStartElement("app-specific");


            writer.WriteStartElement("format-appid");
            writer.WriteValue(format_appid);
            writer.WriteEndElement();

            writer.WriteStartElement("format-tag");
            writer.WriteValue(FormatTag);
            writer.WriteEndElement();

            When.WriteXml("when", writer);

            writer.WriteStartElement("summary");
            writer.WriteValue("");
            writer.WriteEndElement();
            //==================================================================

            //---------------------------------------
            helper_writeOptionalBoolElement(
                writer,
                xmlnode_has_user_agreed_to_db_terms,
                this.HasUserAgreedToStoreDataInDB);

            helper_writeOptionalBoolElement(
                writer,
                xmlnode_has_user_agreed_to_terms_and_cond,
                this.HasUserAgreedToTermsAndConditions);

            helper_writeOptionalBoolElement(
              writer,
              xmlnode_has_user_agreed_to_new_terms_and_cond,
              this.HasUserAgreedToNewTermsAndConditions);

            //Write any elements derrived classes want to write out
            WriteAdditionalXml(writer);
        }


        protected void helper_writeOptionalBoolElement(
            XmlWriter writer,
            string xmlElementName,
            bool? value)
        {
            writer.WriteStartElement(xmlElementName);
            if (value != null)
            {
                writer.WriteValue(value);
            }
            writer.WriteEndElement();

        }

        /// <summary>
        /// Parse a bool out of the XML
        /// </summary>
        /// <param name="xPathNav"></param>
        /// <returns></returns>
        protected bool? helper_ParseTrueFalseNullMissing(XPathNavigator xPathNav)
        {
            //If there is no node... it's null
            if (xPathNav == null) return null;

            string valueText = xPathNav.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xPathNav.ValueAsBoolean;
        }

        protected bool? helper_SelectAndParse_TrueFalseNullMissing(
            XPathNavigator xPathNav,
            string query)
        {
            XPathNavigator xNavItem = xPathNav.SelectSingleNode(query);
            return helper_ParseTrueFalseNullMissing(xNavItem);
        }
    }
}