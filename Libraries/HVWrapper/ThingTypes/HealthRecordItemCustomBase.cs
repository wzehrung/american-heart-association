
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //
    using System;
    using System.Data;
    using System.Configuration;
/**
using System.Web;
    
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
**/

    using System.Xml.XPath;
    using System.Xml;
     
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;


namespace HVWrapper
{
    /// <summary>
    /// Summary description for HealthRecordItemCustomBase
    /// </summary>
    public class HealthRecordItemCustomBase
    {
        protected CustomHealthTypeWrapper m_wrapper;

        public CustomHealthTypeWrapper Wrapper
        {
            get { return m_wrapper; }
            set { m_wrapper = value; }
        }

        public HealthRecordItemKey Key
        {
            get { return m_wrapper.Key; }
        }

        public DateTime EffectiveDate
        {
            get { return m_wrapper.EffectiveDate; }
        }

        public CommonItemData CommonData
        {
            get { return m_wrapper.CommonData; }
        }

        public HealthServiceDateTime When
        {
            get { return m_wrapper.When; }
            set { m_wrapper.When = value; }
        }

        public HealthRecordItemCustomBase()
        {
        }

        public virtual void ParseXml(IXPathNavigable typeSpecificXml)
        {
        }

        public virtual void WriteXml(XmlWriter writer)
        {
        }
    }
}