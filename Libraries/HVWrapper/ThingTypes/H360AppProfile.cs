
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Xml;
    using System.Xml.XPath;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;

namespace HVWrapper
{
    /// <summary>
    /// An application specific profile page
    /// </summary>
    public class H360AppProfile : AppProfile
    {
        private static string format_tag = "health-aha-app-profile";
        public static readonly string XPathFilterForQuery = "/thing/data-xml/app-specific/CustomType/app-specific[format-tag = \"" + format_tag + "\"]";

        protected override string FormatTag
        {
            get
            {
                return format_tag;
            }
        }
    }
}