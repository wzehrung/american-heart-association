
    //
    // Copyright (c) Microsoft Corporation.  All rights reserved.
    //
    //
    // Use of this sample source code is subject to the terms of the Microsoft 
    // license agreement under which you licensed this sample source code and is 
    // provided AS-IS.  If you did not accept the terms of the license agreement, you 
    // are not authorized to use this sample source code.  For the terms of the 
    // license, please see the license agreement between you and Microsoft.
    //
    //
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Xml;
    using System.Xml.XPath;

    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;

namespace HVWrapper
{
    /// <summary>
    /// Summary description for BPZoneCriteria
    /// </summary>
    public class ExtendedProfile : HealthRecordItemCustomBase
    {

        public string HouseholdIncome;
        public bool? HasHealthInsurance;
        public string ActivityLevel;
        public bool? IsSmokeCigarette;
        public int? NumberOfCigarettes;
        public double? CostPerPackOfCigarettes;
        public bool? HasTriedToQuitSmoking;
        public string TravelTimeToWork;
        public string MeansOfTransportation;
        public string AttitudeTowardsHealthCare;
        public string Allergies;


        private const string format_appid = "microsoft-pre-schema";
        private const string format_tag = "health-extended-user-profile";

        //Important: This XML XPath query must be an exact match to the XML on the server, or we will never get our data
        public const string XPathFilterForQuery = "/thing/data-xml/app-specific/CustomType/app-specific[format-tag = \"" + format_tag + "\"]";

        const string xmlnode_householdIncome = "household-income";
        const string xmlnode_activityLevel = "activity-level";
        const string xmlnode_hasHealthInsurance = "has-health-insurance";
        const string xmlnode_IsSmokeCigarette = "is-smoke-cigarette";
        const string xmlnode_NumberOfCigarettes = "number-of-cigarette";
        const string xmlnode_CostPerPackOfCigarettes = "cost-per-pack-of-cigarettes";
        const string xmlnode_HasTriedToQuitSmoking = "has-tried-to-quit-smoking";
        const string xmlnode_TravelTimeToWork = "travel-time-to-work";
        const string xmlnode_MeansOfTransportation = "means-of-transportation";
        const string xmlnode_AttitudeTowardsHealthCare = "attitude-towards-health-care";
        const string xmlnode_Allergies = "allergies";

        /// <summary> 
        /// Populates the data for the zone from the XML.
        /// </summary>
        /// 
        public override void ParseXml(IXPathNavigable typeSpecificXml)
        {
            XPathNavigator navigator = typeSpecificXml.CreateNavigator();
            navigator = navigator.SelectSingleNode("app-specific");

            if (navigator == null)
            {
                throw new ArgumentNullException("null navigator");
            }

            this.HasHealthInsurance
                = helper_ParseTrueFalseNull(navigator, xmlnode_hasHealthInsurance);

            this.ActivityLevel
                = helper_ParseString(navigator, xmlnode_activityLevel);

            this.HouseholdIncome
                = helper_ParseString(navigator, xmlnode_householdIncome);

            this.IsSmokeCigarette
                = helper_ParseTrueFalseNull(navigator, xmlnode_IsSmokeCigarette);

            this.NumberOfCigarettes
                = helper_ParseInt(navigator, xmlnode_NumberOfCigarettes);

            this.CostPerPackOfCigarettes
                = helper_ParseDouble(navigator, xmlnode_CostPerPackOfCigarettes);

            this.HasTriedToQuitSmoking
                = helper_ParseTrueFalseNull(navigator, xmlnode_HasTriedToQuitSmoking);

            this.TravelTimeToWork
                = helper_ParseString(navigator, xmlnode_TravelTimeToWork);

            this.MeansOfTransportation
                = helper_ParseString(navigator, xmlnode_MeansOfTransportation);

            this.AttitudeTowardsHealthCare
                = helper_ParseString(navigator, xmlnode_AttitudeTowardsHealthCare);

            this.Allergies
                = helper_ParseString(navigator, xmlnode_Allergies);
        }


/**
        private int? helper_ParseOneToFiveValueOrNull(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            int valueInt = xNav_Item.ValueAsInt;

            if (valueInt < 1) HVErrorConditions.ThrowCouldNotParseOneToFiveValue();
            if (valueInt > 5) HVErrorConditions.ThrowCouldNotParseOneToFiveValue();
            return valueInt;
        }
**/

        private int? helper_ParseInt(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            int valueInt = xNav_Item.ValueAsInt;

            return valueInt;
        }

        private double? helper_ParseDouble(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            double valueDouble = xNav_Item.ValueAsDouble;

            return valueDouble;
        }


        /// <summary>
        /// Parses a TRUE/FALSE/NULL value
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="xPath"></param>
        /// <returns></returns>
        private bool? helper_ParseTrueFalseNull(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xNav_Item.ValueAsBoolean;
        }

        private string helper_ParseString(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return valueText;
        }

        /// <summary> 
        /// Writes the zone information to the specified XML writer.
        /// </summary>
        public override void WriteXml(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("null writer");
            }

            writer.WriteStartElement("app-specific");

            {
                writer.WriteStartElement("format-appid");
                writer.WriteValue(format_appid);
                writer.WriteEndElement();

                writer.WriteStartElement("format-tag");
                writer.WriteValue(format_tag);
                writer.WriteEndElement();

                When.WriteXml("when", writer);

                writer.WriteStartElement("summary");
                writer.WriteValue("");
                writer.WriteEndElement();

                //=====================================================
                writer.WriteStartElement(xmlnode_householdIncome);
                if (this.HouseholdIncome != null)
                {
                    writer.WriteValue(
                        this.HouseholdIncome);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_hasHealthInsurance);
                if (this.HasHealthInsurance != null)
                {
                    writer.WriteValue(this.HasHealthInsurance.Value);
                }
                writer.WriteEndElement();


                writer.WriteStartElement(xmlnode_activityLevel);
                if (this.ActivityLevel != null)
                {
                    writer.WriteValue(this.ActivityLevel);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_IsSmokeCigarette);
                if (this.IsSmokeCigarette != null)
                {
                    writer.WriteValue(this.IsSmokeCigarette.Value);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_NumberOfCigarettes);
                if (this.NumberOfCigarettes != null)
                {
                    writer.WriteValue(this.NumberOfCigarettes.Value);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_CostPerPackOfCigarettes);
                if (this.CostPerPackOfCigarettes != null)
                {
                    writer.WriteValue(this.CostPerPackOfCigarettes.Value);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_HasTriedToQuitSmoking);
                if (this.HasTriedToQuitSmoking != null)
                {
                    writer.WriteValue(this.HasTriedToQuitSmoking.Value);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_TravelTimeToWork);
                if (this.TravelTimeToWork != null)
                {
                    writer.WriteValue(this.TravelTimeToWork);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_MeansOfTransportation);
                if (this.MeansOfTransportation != null)
                {
                    writer.WriteValue(this.MeansOfTransportation);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_AttitudeTowardsHealthCare);
                if (this.AttitudeTowardsHealthCare != null)
                {
                    writer.WriteValue(this.AttitudeTowardsHealthCare);
                }
                writer.WriteEndElement();

                writer.WriteStartElement(xmlnode_Allergies);
                if (this.Allergies != null)
                {
                    writer.WriteValue(this.Allergies);
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// A string representation of the object.
        /// </summary>
        public override string ToString()
        {
            //UNDONE: Should print out all the field values.
            return "Extended user profile information.";
        }


    }
}