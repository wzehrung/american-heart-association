﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HVWrapper
{
    //
    // If this exception is thrown, then the application should logout the existing user.
    // This gets thrown on a Microsoft.Health.HealthServiceException hse where:
    //      hse.ErrorCode == HealthServiceStatusCode.InvalidRecordState
    //      hse.ErrorCode == HealthServiceStatusCode.AccessDenied
    //
    [Serializable()]
    public class HVFatalException : Exception, ISerializable
    {
        public HVFatalException() : base() { }
        public HVFatalException(string message) : base(message) { }
        public HVFatalException(string message, System.Exception inner) : base(message, inner) { }
        public HVFatalException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable()]
    public class HVNonfatalException : Exception, ISerializable
    {
        public HVNonfatalException() : base() { }
        public HVNonfatalException(string message) : base(message) { }
        public HVNonfatalException(string message, System.Exception inner) : base(message, inner) { }
        public HVNonfatalException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

}
