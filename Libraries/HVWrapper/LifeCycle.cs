﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Health;
using Microsoft.Health.Web;

namespace HVWrapper
{
    public class LifeCycle
    {
        public static void Initialize()
        {
            Microsoft.Health.ItemTypes.ItemTypeRegistrar.RegisterAssemblyHealthRecordItemTypes();

            //Microsoft.Health.ItemTypes.Old.ItemTypeOldRegistrar.RegisterAssemblyHealthRecordItemTypes();

            //-----------------------------------------------
            //Register the custom type handler
            //-----------------------------------------------
            CustomHealthTypeWrapper.RegisterCustomDataTypeHandler_OnceOnly();

            //-----------------------------------------------
            //Register the extensions for different Thing Types
            //-----------------------------------------------
            HVWrapper.ExtendedThingTypes.ExtendedThingTypeRegistration.RegisterAllExtendedTypes();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.File.TypeId, typeof(HVWrapper.ExtendedThingTypes.FileEx), true);
            HVWrapper.ExtendedThingTypes.FileExtension.RegisterExtensionHandler();
        }
    }
}
