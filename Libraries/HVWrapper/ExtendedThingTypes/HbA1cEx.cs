﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    internal class HbA1cExtension : HealthRecordItemExtension
    {
        public const string HbA1cExtensionSourceName = "bpmc.heart.org.HbA1c";

        string m_PolkaItemID;
        public string PolkaItemID
        {
            set
            {
                m_PolkaItemID = value;
            }
            get
            {
                return m_PolkaItemID;
            }
        }

        string m_ReadingType;
        public string ReadingType
        {
            set
            {
                m_ReadingType = value;
            }
            get
            {
                return m_ReadingType;
            }
        }

        public HbA1cExtension()
        {
        }

        public HbA1cExtension(string readingType, string strPolkaItemID)
            : base(HbA1cExtensionSourceName)
        {
            m_ReadingType = readingType;
            m_PolkaItemID = strPolkaItemID;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();

            XPathNavigator readingTypeNode = navigator.SelectSingleNode("extension/reading_type");
            if (readingTypeNode != null)
            {
                m_ReadingType = readingTypeNode.Value;
            }

            XPathNavigator polkaItemIDNode = navigator.SelectSingleNode("extension/polka_item_id");
            if (polkaItemIDNode != null)
            {
                m_PolkaItemID = polkaItemIDNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("reading_type");
            if (this.ReadingType != null)
            {
                writer.WriteValue(
                    this.ReadingType);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("polka_item_id");
            if (this.PolkaItemID != null)
            {
                writer.WriteValue(
                    this.PolkaItemID);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(HbA1cExtensionSourceName, typeof(HbA1cExtension), true);
        }
    }
        
    public class HbA1cEx : Microsoft.Health.ItemTypes.HbA1C
    {
        public HbA1cEx()
        {
        }

        private HbA1cExtension m_HbA1cExtension;
        private bool m_HbA1cExtensionLoaded = false;

        private HbA1cExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                HbA1cExtension hbA1cExtension = extension as HbA1cExtension;

                if(hbA1cExtension != null)
                {
                    m_HbA1cExtensionLoaded = true;
                    return hbA1cExtension;
                }
            }
            m_HbA1cExtensionLoaded = true;
            return null;
        }

        public string ReadingType
        {
            get
            {
                if (!m_HbA1cExtensionLoaded)
                {
                    m_HbA1cExtension = GetExtendedData();
                }

                if (m_HbA1cExtension == null)
                {
                    return null;
                }
                return m_HbA1cExtension.ReadingType;
            }
            set
            {
                if (!m_HbA1cExtensionLoaded)
                {
                    m_HbA1cExtension = GetExtendedData();
                }

                if (m_HbA1cExtension == null)
                {
                    m_HbA1cExtension = new HbA1cExtension(value, null);
                    this.CommonData.Extensions.Add(m_HbA1cExtension);
                }
                else
                {
                    m_HbA1cExtension.ReadingType = value;
                }
            }
        }

        public string PolkaItemID
        {
            get
            {
                if (!m_HbA1cExtensionLoaded)
                {
                    m_HbA1cExtension = GetExtendedData();
                }

                if (m_HbA1cExtension == null)
                {
                    return null;
                }
                return m_HbA1cExtension.PolkaItemID;
            }
            set
            {
                if (!m_HbA1cExtensionLoaded)
                {
                    m_HbA1cExtension = GetExtendedData();
                }

                if (m_HbA1cExtension == null)
                {
                    m_HbA1cExtension = new HbA1cExtension(null, value);
                    this.CommonData.Extensions.Add(m_HbA1cExtension);
                }
                else
                {
                    m_HbA1cExtension.PolkaItemID = value;
                }
            }
        }
    }
}
