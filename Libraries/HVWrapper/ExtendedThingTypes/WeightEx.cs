﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    internal class WeightExtension : HealthRecordItemExtension
    {
        public const string WeightExtensionSourceName = "bpmc.heart.org.weight";

        string m_PolkaItemID;
        public string PolkaItemID
        {
            set
            {
                m_PolkaItemID = value;
            }
            get
            {
                return m_PolkaItemID;
            }
        }

        public WeightExtension()
        {
        }

        public WeightExtension(string strPolkaItemID)
            : base(WeightExtensionSourceName)
        {
            m_PolkaItemID = strPolkaItemID;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            XPathNavigator polkaItemIDNode = navigator.SelectSingleNode("extension/polka_item_id");
            if (polkaItemIDNode != null)
            {
                m_PolkaItemID = polkaItemIDNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("polka_item_id");
            if (this.PolkaItemID != null)
            {
                writer.WriteValue(
                    this.PolkaItemID);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(WeightExtensionSourceName, typeof(WeightExtension), true);
        }
    }


    public class WeightEx : Microsoft.Health.ItemTypes.Weight
    {
        public WeightEx()
        {

        }

        private WeightExtension m_WeightExtension;
        private bool m_WeightExtensionLoaded = false;

        private WeightExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                WeightExtension WeightExtension = extension as WeightExtension;
                if (WeightExtension != null)
                {
                    m_WeightExtensionLoaded = true;
                    return WeightExtension;
                }
            }
            m_WeightExtensionLoaded = true;
            return null;
        }

        public string PolkaItemID
        {
            get
            {
                if (!m_WeightExtensionLoaded)
                {
                    m_WeightExtension = GetExtendedData();
                }

                if (m_WeightExtension == null)
                {
                    return null;
                }
                return m_WeightExtension.PolkaItemID;
            }
            set
            {
                if (!m_WeightExtensionLoaded)
                {
                    m_WeightExtension = GetExtendedData();
                }

                if (m_WeightExtension == null)
                {
                    m_WeightExtension = new WeightExtension(value);
                    this.CommonData.Extensions.Add(m_WeightExtension);
                }
                else
                {
                    m_WeightExtension.PolkaItemID = value;
                }
            }
        }
    }
}
