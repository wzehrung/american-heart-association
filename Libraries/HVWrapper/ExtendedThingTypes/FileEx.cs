﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;

using Microsoft.Health;

namespace HVWrapper.ExtendedThingTypes
{
    public class FileExtension : HealthRecordItemExtension
    {
        public const string FileExtensionSourceName = "bpmc.heart.org.file";

        bool? m_IsMyLifeCheckFile;
        public bool? IsMyLifeCheckFile
        {
            set
            {
                m_IsMyLifeCheckFile = value;
            }
            get
            {
                return m_IsMyLifeCheckFile;
            }
        }

        double? m_HealthScore;
        public double? HealthScore
        {
            set
            {
                m_HealthScore = value;
            }
            get
            {
                return m_HealthScore;
            }
        }

        double? m_BMI;
        public double? BMI
        {
            set
            {
                m_BMI = value;
            }
            get
            {
                return m_BMI;
            }
        }

        double? m_BloodGlucose;
        public double? BloodGlucose
        {
            set
            {
                m_BloodGlucose = value;
            }
            get
            {
                return m_BloodGlucose;
            }
        }

        double? m_HbA1c;
        public double? HbA1c
        {
            set
            {
                m_HbA1c = value;
            }
            get
            {
                return m_HbA1c;
            }
        }

        int? m_TotalCholesterol;
        public int? TotalCholesterol
        {
            set
            {
                m_TotalCholesterol = value;
            }
            get
            {
                return m_TotalCholesterol;
            }
        }

        int? m_SystolicBP;
        public int? SystolicBP
        {
            set
            {
                m_SystolicBP = value;
            }
            get
            {
                return m_SystolicBP;
            }
        }

        int? m_DiastolicBP;
        public int? DiastolicBP
        {
            set
            {
                m_DiastolicBP = value;
            }
            get
            {
                return m_DiastolicBP;
            }
        }

        public FileExtension()
        {
        }

        public FileExtension(bool? bIsMyLifeCheckFile
            , double? iHealthScore
            , double? dBMI
            , double? dBloodGlucose
            , double? dHbA1c
            , int? iTotalCholesterol
            , int? iSystolicBP
            , int? iDiastolicBP
            )
            : base(FileExtensionSourceName)
        {
            m_IsMyLifeCheckFile = bIsMyLifeCheckFile;
            m_HealthScore = iHealthScore;
            m_BMI = dBMI;
            m_BloodGlucose = dBloodGlucose;
            m_HbA1c = dHbA1c;
            m_TotalCholesterol = iTotalCholesterol;
            m_SystolicBP = iSystolicBP;
            m_DiastolicBP = iDiastolicBP;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            m_IsMyLifeCheckFile = ParseHelper.ParseTrueFalseNull(navigator, "extension/is_my_life_check_file");

            this.HealthScore = ParseHelper.ParseDouble(navigator, "extension/health_score");

            this.BMI = ParseHelper.ParseDouble(navigator, "extension/bmi");
            this.BloodGlucose = ParseHelper.ParseDouble(navigator, "extension/blood_glucose");
            this.HbA1c = ParseHelper.ParseDouble(navigator, "extenstion/hba1c");
            this.TotalCholesterol = ParseHelper.ParseInt(navigator, "extension/total_cholesterol");
            this.SystolicBP = ParseHelper.ParseInt(navigator, "extension/systolic");
            this.DiastolicBP = ParseHelper.ParseInt(navigator, "extension/diastolic");
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("is_my_life_check_file");
            if (this.m_IsMyLifeCheckFile.HasValue)
            {
                writer.WriteValue(this.m_IsMyLifeCheckFile.Value);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("health_score");
            if (this.HealthScore.HasValue)
            {
                writer.WriteValue(this.HealthScore.Value);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("bmi");
            if (this.BMI.HasValue)
            {
                writer.WriteValue(this.BMI.Value);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("blood_glucose");
            if (this.BloodGlucose.HasValue)
            {
                writer.WriteValue(this.BloodGlucose.Value);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("hba1c");
            if (this.HbA1c.HasValue)
            {
                writer.WriteValue(this.HbA1c.Value);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("total_cholesterol");
            if (this.TotalCholesterol.HasValue)
            {
                writer.WriteValue(this.TotalCholesterol.Value);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("systolic");
            if (this.SystolicBP.HasValue)
            {
                writer.WriteValue(this.SystolicBP.Value);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("diastolic");
            if (this.DiastolicBP.HasValue)
            {
                writer.WriteValue(this.DiastolicBP.Value);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(FileExtensionSourceName, typeof(FileExtension), true);
        }
    }


    public class FileEx : Microsoft.Health.ItemTypes.File
    {
        public FileEx()
        {

        }

        private FileExtension m_FileExtension;
        private bool m_FileExtensionLoaded = false;

        private FileExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                FileExtension fileExtension = extension as FileExtension;
                if (fileExtension != null)
                {
                    m_FileExtensionLoaded = true;
                    return fileExtension;
                }
            }
            m_FileExtensionLoaded = true;
            return null;
        }

        public bool? IsMyLifeCheckFile
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.IsMyLifeCheckFile;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(value, null, null, null, null, null, null, null);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.IsMyLifeCheckFile = value;
                }
            }
        }

        public double? HealthScore
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.HealthScore;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(null, value, null, null, null, null, null, null);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.HealthScore = value;
                }
            }
        }

        public double? BMI
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.BMI;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(null, null, value, null, null, null, null, null);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.BMI = value;
                }
            }
        }

        public double? BloodGlucose
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.BloodGlucose;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(null, null, null, value, null, null, null, null);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.BloodGlucose = value;
                }
            }
        }

        public double? HbA1c
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.HbA1c;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(null, null, null, null, value, null, null, null);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.HbA1c = value;
                }
            }
        }


        public int? TotalCholesterol
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.TotalCholesterol;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(null, null, null, null, null, value, null, null);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.TotalCholesterol = value;
                }
            }
        }

        public int? SystolicBP
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.SystolicBP;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(null, null, null, null, null, null, value, null);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.SystolicBP = value;
                }
            }
        }

        public int? DiastolicBP
        {
            get
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    return null;
                }
                return m_FileExtension.DiastolicBP;
            }
            set
            {
                if (!m_FileExtensionLoaded)
                {
                    m_FileExtension = GetExtendedData();
                }

                if (m_FileExtension == null)
                {
                    m_FileExtension = new FileExtension(null, null, null, null, null, null, null, value);
                    this.CommonData.Extensions.Add(m_FileExtension);
                }
                else
                {
                    m_FileExtension.DiastolicBP = value;
                }
            }
        }
    }
}
