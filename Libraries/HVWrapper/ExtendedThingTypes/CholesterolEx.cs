﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    internal class CholesterolExtension : HealthRecordItemExtension
    {
        public const string CholesterolExtensionSourceName = "bpmc.heart.org.cholesterol";

        string m_PolkaItemID;
        public string PolkaItemID
        {
            set
            {
                m_PolkaItemID = value;
            }
            get
            {
                return m_PolkaItemID;
            }
        }

        string m_TestLocation;
        public string TestLocation
        {
            set
            {
                m_TestLocation = value;
            }
            get
            {
                return m_TestLocation;
            }
        }

        public CholesterolExtension()
        {
        }

        public CholesterolExtension(string testlocation, string strPolkaItemID)
            : base(CholesterolExtensionSourceName)
        {
            m_TestLocation = testlocation;
            m_PolkaItemID = strPolkaItemID;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            XPathNavigator testlocationNode = navigator.SelectSingleNode("extension/test_location");
            if (testlocationNode != null)
            {
                m_TestLocation = testlocationNode.Value;
            }

            XPathNavigator polkaItemIDNode = navigator.SelectSingleNode("extension/polka_item_id");
            if (polkaItemIDNode != null)
            {
                m_PolkaItemID = polkaItemIDNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("test_location");
            if (this.TestLocation != null)
            {
                writer.WriteValue(
                    this.TestLocation);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("polka_item_id");
            if (this.PolkaItemID != null)
            {
                writer.WriteValue(
                    this.PolkaItemID);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(CholesterolExtensionSourceName, typeof(CholesterolExtension), true);
        }
    }


    public class CholesterolEx : Microsoft.Health.ItemTypes.CholesterolProfileV2    // CholesterolProfile
    {       
        public CholesterolEx()
        {

        }

        private const double c_mgdL2mmolL = 0.0259 ;                 // 1 mg/dL ~= 0.0259 mmol/L for HDL, LDL, TC
        private const double c_mgdL2mmolL_Triglycerides = 0.0113 ;    // 1 mg/dL ~= 0.0113 mmol/L for Triglycerides
        private const double c_mmolL2mgdL = 38.6100 ;
        private const double c_mmolL2mgdL_Triglycerides = 88.4956 ;
        public const int NO_VALUE = -1;

        private static bool ConcentrationMeasurementHasValue(Microsoft.Health.ItemTypes.ConcentrationMeasurement _cm)
        {
            return (_cm == null) ? false : true;
        }

            // this is used to convert a mmol/L concentration measurement (not Triglycerides!) to Heart360 mg/dL value
            // the given concentration measurement should not be null!
        private static int ConcentrationMeasurementTomgdL(Microsoft.Health.ItemTypes.ConcentrationMeasurement _cm, bool _isTriglycerides = false )
        {
            return (!_isTriglycerides) ? (int)(_cm.Value * c_mmolL2mgdL) : (int)(_cm.Value * c_mmolL2mgdL_Triglycerides);
        }


        private CholesterolExtension m_CholesterolExtension;
        private bool m_CholesterolExtensionLoaded = false;

        private CholesterolExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                CholesterolExtension cholesterolExtension = extension as CholesterolExtension;
                if (cholesterolExtension != null)
                {
                    m_CholesterolExtensionLoaded = true;
                    return cholesterolExtension;
                }
            }
            m_CholesterolExtensionLoaded = true;
            return null;
        }

        public string TestLocation
        {
            get
            {
                if (!m_CholesterolExtensionLoaded)
                {
                    m_CholesterolExtension = GetExtendedData();
                }

                if (m_CholesterolExtension == null)
                {
                    return null;
                }
                return m_CholesterolExtension.TestLocation;
            }
            set
            {
                if (!m_CholesterolExtensionLoaded)
                {
                    m_CholesterolExtension = GetExtendedData();
                }

                if (m_CholesterolExtension == null)
                {
                    m_CholesterolExtension = new CholesterolExtension(value, null);
                    this.CommonData.Extensions.Add(m_CholesterolExtension);
                }
                else
                {
                    m_CholesterolExtension.TestLocation = value;
                }
            }
        }

        public string PolkaItemID
        {
            get
            {
                if (!m_CholesterolExtensionLoaded)
                {
                    m_CholesterolExtension = GetExtendedData();
                }

                if (m_CholesterolExtension == null)
                {
                    return null;
                }
                return m_CholesterolExtension.PolkaItemID;
            }
            set
            {
                if (!m_CholesterolExtensionLoaded)
                {
                    m_CholesterolExtension = GetExtendedData();
                }

                if (m_CholesterolExtension == null)
                {
                    m_CholesterolExtension = new CholesterolExtension(null, value);
                    this.CommonData.Extensions.Add(m_CholesterolExtension);
                }
                else
                {
                    m_CholesterolExtension.PolkaItemID = value;
                }
            }
        }
   
            //
            // PJB: Here are a bunch of getters and setters for LDL, HDL, Total Cholesterol and Triglycerides.
            //
        public bool LdlHasValue
        {
            get
            {
                return (this.LDL == null) ? false : true;
            }
        }
        public int LdlInMgdlUnits
        {
            get
            {
                return (this.LDL != null) ? (int)(this.LDL.Value * c_mmolL2mgdL): NO_VALUE ;
            }
            set
            {
                if (this.LDL == null)
                    this.LDL = new ConcentrationMeasurement();
                this.LDL.Value = c_mgdL2mmolL * value;
            }
        }
        public bool HdlHasValue
        {
            get
            {
                return (this.HDL == null) ? false : true;
            }
        }
        public int HdlInMgdlUnits
        {
            get
            {
                return (this.HDL != null) ? (int)(this.HDL.Value * c_mmolL2mgdL) : NO_VALUE;
            }
            set
            {
                if (this.HDL == null)
                    this.HDL = new ConcentrationMeasurement();
                this.HDL.Value = c_mgdL2mmolL * value;
            }
        }
        public bool TotalCholesterolHasValue
        {
            get
            {
                return (this.TotalCholesterol == null) ? false : true;
            }
        }
        public int TotalCholesterolInMgdlUnits
        {
            get
            {
                return (this.TotalCholesterol != null) ? (int)(this.TotalCholesterol.Value * c_mmolL2mgdL) : NO_VALUE;
            }
            set
            {
                if (this.TotalCholesterol == null)
                    this.TotalCholesterol = new ConcentrationMeasurement();
                this.TotalCholesterol.Value = c_mgdL2mmolL * value;
            }
        }
        public bool TriglycerideHasValue
        {
            get
            {
                return (this.Triglyceride == null) ? false : true;
            }
        }
        public int TriglycerideInMgdlUnits
        {
            get
            {
                return (this.Triglyceride != null) ? (int)(this.Triglyceride.Value * c_mmolL2mgdL_Triglycerides) : NO_VALUE;
            }
            set
            {
                if (this.Triglyceride == null)
                    this.Triglyceride = new ConcentrationMeasurement();
                this.Triglyceride.Value = c_mgdL2mmolL_Triglycerides * value;
            }
        }
    }
}
