﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    public enum GoalType { Unknown, ReduceWeight, ReduceBloodPressure, ReduceCholestrol, ReduceBloodGlucose, Exercise };

    

    internal class LifeGoalExtension : HealthRecordItemExtension
    {
        public const string LifeGoalExtensionSourceName = "bpmc.heart.org.lifegoal";
        
        GoalType m_typeOfGoal;
        public GoalType TypeOfGoal
        {
            set
            {
                m_typeOfGoal = value;
            }
            get
            {
                return m_typeOfGoal;
            }
        }

        string m_startValue;
        public string StartValue
        {
            set
            {
                m_startValue = value;
            }

            get
            {
                return m_startValue;
            }
        }

        HealthServiceDateTime m_startDate;
        public HealthServiceDateTime StartDate
        {
            set
            {
                m_startDate = value;
            }

            get
            {
                return m_startDate;
            }
        }

        string m_targetValue;
        public string TargetValue
        {
            set
            {
                m_targetValue = value;
            }

            get
            {
                return m_targetValue;
            }
        }


        public LifeGoalExtension()
        {
        }

        public LifeGoalExtension(GoalType goalType, string startValue, HealthServiceDateTime dtStartDate, string targetValue)
                       : base(LifeGoalExtensionSourceName)
        {
            m_startValue = startValue;
            m_startDate = dtStartDate;
            m_targetValue = targetValue;
            m_typeOfGoal = goalType;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            XPathNavigator goaltypeNode = navigator.SelectSingleNode("extension/goal_type" );
            if (goaltypeNode != null)
            {
                m_typeOfGoal = (GoalType)Enum.Parse(typeof(GoalType), goaltypeNode.Value);
            }

            XPathNavigator startValueNode = navigator.SelectSingleNode("extension/start_value");
            if (startValueNode != null)
            {
                m_startValue = startValueNode.Value;
            }

            XPathNavigator statDateNode = navigator.SelectSingleNode("extension/start_date");
            if (statDateNode != null)
            {
                m_startDate = new HealthServiceDateTime();
                m_startDate.ParseXml(statDateNode);
            }

            XPathNavigator targetValueNode = navigator.SelectSingleNode("extension/target_value");
            if (targetValueNode != null)
            {
                m_targetValue = targetValueNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            if (m_typeOfGoal == GoalType.Unknown)
            {
                throw new ArgumentException("Goal Type not set");
            }


            if (m_targetValue.Trim().Length == 0)
            {
                throw new ArgumentException("Target value not set for goal");
            }

            writer.WriteStartElement("goal_type");
            writer.WriteString(m_typeOfGoal.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("start_value");
            if (m_startValue != null)
            {
                writer.WriteString(m_startValue.ToString());
            }
            writer.WriteEndElement();

            if (m_startDate != null)
            {
                m_startDate.WriteXml("start_date", writer);
            }
            
            writer.WriteStartElement("target_value");
            writer.WriteString(m_targetValue.ToString());
            writer.WriteEndElement();
        }

        private DateTime? helper_ParseDateTime(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null)
                return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;

            if (string.IsNullOrEmpty(valueText))
                return null;

            if (string.IsNullOrEmpty(valueText.Trim()))
                return null;

            DateTime valueDateTime = xNav_Item.ValueAsDateTime;

            return valueDateTime;
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(LifeGoalExtensionSourceName, typeof(LifeGoalExtension), true);
        }

    }



    public class LifeGoalEx : Microsoft.Health.ItemTypes.LifeGoal
    {
        public LifeGoalEx()
        {

        }


        private LifeGoalExtension m_lifeGoalExtension;
        private bool m_lifeGoalExtensionLoaded = false;
        private LifeGoalExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                LifeGoalExtension lifeGoalExtension = extension as LifeGoalExtension;
                if (lifeGoalExtension != null)
                {
                    m_lifeGoalExtensionLoaded = true;
                    return lifeGoalExtension;
                }
            }
            m_lifeGoalExtensionLoaded = true;
            return null;
        }


        public GoalType TypeOfGoal
        {
            get
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    return GoalType.Unknown;
                }
                return m_lifeGoalExtension.TypeOfGoal;
            }

            set
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    m_lifeGoalExtension = new LifeGoalExtension(value,null,null,null);
                    this.CommonData.Extensions.Add(m_lifeGoalExtension);
                }
                else
                {
                    m_lifeGoalExtension.TypeOfGoal = value;
                }
            }
        }

        public string StartValue
        {
            get
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    return null;
                }
                return m_lifeGoalExtension.StartValue;
            }

            set
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    m_lifeGoalExtension = new LifeGoalExtension(GoalType.Unknown, value, null, null);
                    this.CommonData.Extensions.Add(m_lifeGoalExtension);
                }
                else
                {
                    m_lifeGoalExtension.StartValue = value;
                }
            }
        }

        public HealthServiceDateTime StartDate
        {
            get
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    return null;
                }
                return m_lifeGoalExtension.StartDate;
            }

            set
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    m_lifeGoalExtension = new LifeGoalExtension(GoalType.Unknown, null,value, null);
                    this.CommonData.Extensions.Add(m_lifeGoalExtension);
                }
                else
                {
                    m_lifeGoalExtension.StartDate = value;
                }
            }
        }
        
        public string TargetValue
        {
            get
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    return null;
                }
                return m_lifeGoalExtension.TargetValue;
            }

            set
            {
                if (!m_lifeGoalExtensionLoaded)
                {
                    m_lifeGoalExtension = GetExtendedData();
                }

                if (m_lifeGoalExtension == null)
                {
                    m_lifeGoalExtension = new LifeGoalExtension(GoalType.Unknown, null,null,value);
                    this.CommonData.Extensions.Add(m_lifeGoalExtension);
                }
                else
                {
                    m_lifeGoalExtension.TargetValue = value;
                }
            }
        }

    }
}
