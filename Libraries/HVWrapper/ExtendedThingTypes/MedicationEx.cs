﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    internal class MedicationExtension : HealthRecordItemExtension
    {
        public const string MedicationExtensionSourceName = "bpmc.heart.org.medication";

        string m_PolkaItemID;
        public string PolkaItemID
        {
            set
            {
                m_PolkaItemID = value;
            }
            get
            {
                return m_PolkaItemID;
            }
        }

        string m_medicationType;
        public string MedicationType
        {
            set
            {
                m_medicationType = value;
            }
            get
            {
                return m_medicationType;
            }
        }

        string m_dosage;
        public string Dosage
        {
            set
            {
                m_dosage = value;
            }
            get
            {
                return m_dosage;
            }
        }

        HealthServiceDateTime m_DateEntered;
        public HealthServiceDateTime DateEntered
        {
            set
            {
                m_DateEntered = value;
            }

            get
            {
                return m_DateEntered;
            }
        }

        public MedicationExtension()
        {
        }

        public MedicationExtension(string medicationtype, string dosage, HealthServiceDateTime dtDateEntered, string strPolkaItemID)
            : base(MedicationExtensionSourceName)
        {
            m_medicationType = medicationtype;
            m_dosage = dosage;
            m_DateEntered = dtDateEntered;
            m_PolkaItemID = strPolkaItemID;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            XPathNavigator medicationtypeNode = navigator.SelectSingleNode("extension/medication_type");
            if (medicationtypeNode != null)
            {
                m_medicationType = medicationtypeNode.Value;
            }

            XPathNavigator dosageNode = navigator.SelectSingleNode("extension/dosage");
            if (medicationtypeNode != null)
            {
                m_dosage = dosageNode.Value;
            }

            XPathNavigator dateEnteredNode = navigator.SelectSingleNode("extension/date_entered");
            if (dateEnteredNode != null)
            {
                m_DateEntered = new HealthServiceDateTime();
                m_DateEntered.ParseXml(dateEnteredNode);
            }

            XPathNavigator polkaItemIDNode = navigator.SelectSingleNode("extension/polka_item_id");
            if (polkaItemIDNode != null)
            {
                m_PolkaItemID = polkaItemIDNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("medication_type");
            if (this.MedicationType != null)
            {
                writer.WriteValue(
                    this.MedicationType);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("dosage");
            if (this.Dosage != null)
            {
                writer.WriteValue(
                    this.Dosage);
            }
            writer.WriteEndElement();

            if (m_DateEntered != null)
            {
                m_DateEntered.WriteXml("date_entered", writer);
            }

            writer.WriteStartElement("polka_item_id");
            if (this.PolkaItemID != null)
            {
                writer.WriteValue(
                    this.PolkaItemID);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(MedicationExtensionSourceName, typeof(MedicationExtension), true);
        }

    }



    public class MedicationEx : Microsoft.Health.ItemTypes.Medication
    {
        public MedicationEx()
        {

        }

        private MedicationExtension m_MedicationExtension;
        private bool m_MedicationExtensionLoaded = false;

        private MedicationExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                MedicationExtension medicationExtension = extension as MedicationExtension;
                if (medicationExtension != null)
                {
                    m_MedicationExtensionLoaded = true;
                    return medicationExtension;
                }
            }
            m_MedicationExtensionLoaded = true;
            return null;
        }

        public string MedicationType
        {
            get
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    return null;
                }
                return m_MedicationExtension.MedicationType;
            }
            set
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    m_MedicationExtension = new MedicationExtension(value, null, null, null);
                    this.CommonData.Extensions.Add(m_MedicationExtension);
                }
                else
                {
                    m_MedicationExtension.MedicationType = value;
                }
            }
        }

        public string Dosage
        {
            get
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    return null;
                }
                return m_MedicationExtension.Dosage;
            }
            set
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    m_MedicationExtension = new MedicationExtension(null, value, null, null);
                    this.CommonData.Extensions.Add(m_MedicationExtension);
                }
                else
                {
                    m_MedicationExtension.Dosage = value;
                }
            }
        }

        public HealthServiceDateTime DateEntered
        {
            get
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    return null;
                }
                return m_MedicationExtension.DateEntered;
            }

            set
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    m_MedicationExtension = new MedicationExtension(null, null, value, null);
                    this.CommonData.Extensions.Add(m_MedicationExtension);
                }
                else
                {
                    m_MedicationExtension.DateEntered = value;
                }
            }
        }

        public string PolkaItemID
        {
            get
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    return null;
                }
                return m_MedicationExtension.PolkaItemID;
            }

            set
            {
                if (!m_MedicationExtensionLoaded)
                {
                    m_MedicationExtension = GetExtendedData();
                }

                if (m_MedicationExtension == null)
                {
                    m_MedicationExtension = new MedicationExtension(null, null, null, value);
                    this.CommonData.Extensions.Add(m_MedicationExtension);
                }
                else
                {
                    m_MedicationExtension.PolkaItemID = value;
                }
            }
        }
    }
}
