﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    internal class BloodPressureExtension : HealthRecordItemExtension
    {
        public const string BloodPressureExtensionSourceName = "bpmc.heart.org.bloodpressure";

        string m_PolkaItemID;
        public string PolkaItemID
        {
            set
            {
                m_PolkaItemID = value;
            }
            get
            {
                return m_PolkaItemID;
            }
        }

        public BloodPressureExtension()
        {
        }

        public BloodPressureExtension(string strPolkaItemID)
            : base(BloodPressureExtensionSourceName)
        {
            m_PolkaItemID = strPolkaItemID;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            XPathNavigator polkaItemIDNode = navigator.SelectSingleNode("extension/polka_item_id");
            if (polkaItemIDNode != null)
            {
                m_PolkaItemID = polkaItemIDNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("polka_item_id");
            if (this.PolkaItemID != null)
            {
                writer.WriteValue(
                    this.PolkaItemID);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(BloodPressureExtensionSourceName, typeof(BloodPressureExtension), true);
        }
    }


    public class BloodPressureEx : Microsoft.Health.ItemTypes.BloodPressure
    {
        public BloodPressureEx()
        {

        }

        private BloodPressureExtension m_BloodPressureExtension;
        private bool m_BloodPressureExtensionLoaded = false;

        private BloodPressureExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                BloodPressureExtension BloodPressureExtension = extension as BloodPressureExtension;
                if (BloodPressureExtension != null)
                {
                    m_BloodPressureExtensionLoaded = true;
                    return BloodPressureExtension;
                }
            }
            m_BloodPressureExtensionLoaded = true;
            return null;
        }

        public string PolkaItemID
        {
            get
            {
                if (!m_BloodPressureExtensionLoaded)
                {
                    m_BloodPressureExtension = GetExtendedData();
                }

                if (m_BloodPressureExtension == null)
                {
                    return null;
                }
                return m_BloodPressureExtension.PolkaItemID;
            }
            set
            {
                if (!m_BloodPressureExtensionLoaded)
                {
                    m_BloodPressureExtension = GetExtendedData();
                }

                if (m_BloodPressureExtension == null)
                {
                    m_BloodPressureExtension = new BloodPressureExtension(value);
                    this.CommonData.Extensions.Add(m_BloodPressureExtension);
                }
                else
                {
                    m_BloodPressureExtension.PolkaItemID = value;
                }
            }
        }
    }
}
