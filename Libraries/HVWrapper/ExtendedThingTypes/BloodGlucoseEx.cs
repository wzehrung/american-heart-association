﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    internal class BloodGlucoseExtension : HealthRecordItemExtension
    {
        public const string BloodGlucoseExtensionSourceName = "bpmc.heart.org.bloodglucose";

        string m_PolkaItemID;
        public string PolkaItemID
        {
            set
            {
                m_PolkaItemID = value;
            }
            get
            {
                return m_PolkaItemID;
            }
        }

        string m_ActionTaken;
        public string ActionTaken
        {
            set
            {
                m_ActionTaken = value;
            }
            get
            {
                return m_ActionTaken;
            }
        }

        string m_ReadingType;
        public string ReadingType
        {
            set
            {
                m_ReadingType = value;
            }
            get
            {
                return m_ReadingType;
            }
        }

        public BloodGlucoseExtension()
        {
        }

        public BloodGlucoseExtension(string actionTaken, string readingType, string strPolkaItemID)
            : base(BloodGlucoseExtensionSourceName)
        {
            m_ActionTaken = actionTaken;
            m_ReadingType = readingType;
            m_PolkaItemID = strPolkaItemID;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            XPathNavigator actionTakenNode = navigator.SelectSingleNode("extension/action_taken");
            if (actionTakenNode != null)
            {
                m_ActionTaken = actionTakenNode.Value;
            }

            XPathNavigator readingTypeNode = navigator.SelectSingleNode("extension/reading_type");
            if (readingTypeNode != null)
            {
                m_ReadingType = readingTypeNode.Value;
            }

            XPathNavigator polkaItemIDNode = navigator.SelectSingleNode("extension/polka_item_id");
            if (polkaItemIDNode != null)
            {
                m_PolkaItemID = polkaItemIDNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("action_taken");
            if (this.ActionTaken != null)
            {
                writer.WriteValue(
                    this.ActionTaken);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("reading_type");
            if (this.ReadingType != null)
            {
                writer.WriteValue(
                    this.ReadingType);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("polka_item_id");
            if (this.PolkaItemID != null)
            {
                writer.WriteValue(
                    this.PolkaItemID);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(BloodGlucoseExtensionSourceName, typeof(BloodGlucoseExtension), true);
        }
    }


    public class BloodGlucoseEx : Microsoft.Health.ItemTypes.BloodGlucose
    {
        public BloodGlucoseEx()
        {

        }

        private BloodGlucoseExtension m_BloodGlucoseExtension;
        private bool m_BloodGlucoseExtensionLoaded = false;

        private BloodGlucoseExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                BloodGlucoseExtension bloodGlucoseExtension = extension as BloodGlucoseExtension;
                if (bloodGlucoseExtension != null)
                {
                    m_BloodGlucoseExtensionLoaded = true;
                    return bloodGlucoseExtension;
                }
            }
            m_BloodGlucoseExtensionLoaded = true;
            return null;
        }

        public string ActionTaken
        {
            get
            {
                if (!m_BloodGlucoseExtensionLoaded)
                {
                    m_BloodGlucoseExtension = GetExtendedData();
                }

                if (m_BloodGlucoseExtension == null)
                {
                    return null;
                }
                return m_BloodGlucoseExtension.ActionTaken;
            }
            set
            {
                if (!m_BloodGlucoseExtensionLoaded)
                {
                    m_BloodGlucoseExtension = GetExtendedData();
                }

                if (m_BloodGlucoseExtension == null)
                {
                    m_BloodGlucoseExtension = new BloodGlucoseExtension(value, null, null);
                    this.CommonData.Extensions.Add(m_BloodGlucoseExtension);
                }
                else
                {
                    m_BloodGlucoseExtension.ActionTaken = value;
                }
            }
        }

        public string ReadingType
        {
            get
            {
                if (!m_BloodGlucoseExtensionLoaded)
                {
                    m_BloodGlucoseExtension = GetExtendedData();
                }

                if (m_BloodGlucoseExtension == null)
                {
                    return null;
                }
                return m_BloodGlucoseExtension.ReadingType;
            }
            set
            {
                if (!m_BloodGlucoseExtensionLoaded)
                {
                    m_BloodGlucoseExtension = GetExtendedData();
                }

                if (m_BloodGlucoseExtension == null)
                {
                    m_BloodGlucoseExtension = new BloodGlucoseExtension(null, value, null);
                    this.CommonData.Extensions.Add(m_BloodGlucoseExtension);
                }
                else
                {
                    m_BloodGlucoseExtension.ReadingType = value;
                }
            }
        }

        public string PolkaItemID
        {
            get
            {
                if (!m_BloodGlucoseExtensionLoaded)
                {
                    m_BloodGlucoseExtension = GetExtendedData();
                }

                if (m_BloodGlucoseExtension == null)
                {
                    return null;
                }
                return m_BloodGlucoseExtension.PolkaItemID;
            }
            set
            {
                if (!m_BloodGlucoseExtensionLoaded)
                {
                    m_BloodGlucoseExtension = GetExtendedData();
                }

                if (m_BloodGlucoseExtension == null)
                {
                    m_BloodGlucoseExtension = new BloodGlucoseExtension(null, null, value);
                    this.CommonData.Extensions.Add(m_BloodGlucoseExtension);
                }
                else
                {
                    m_BloodGlucoseExtension.PolkaItemID = value;
                }
            }
        }
    }
}
