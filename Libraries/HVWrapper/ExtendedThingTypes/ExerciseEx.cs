﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

namespace HVWrapper.ExtendedThingTypes
{
    internal class ExerciseExtension : HealthRecordItemExtension
    {
        public const string ExerciseExtensionSourceName = "bpmc.heart.org.exercise";

        string m_PolkaItemID;
        public string PolkaItemID
        {
            set
            {
                m_PolkaItemID = value;
            }
            get
            {
                return m_PolkaItemID;
            }
        }

        public ExerciseExtension()
        {
        }

        public ExerciseExtension(string strPolkaItemID)
            : base(ExerciseExtensionSourceName)
        {
            m_PolkaItemID = strPolkaItemID;
        }

        protected override void ParseXml(System.Xml.XPath.IXPathNavigable extensionData)
        {
            XPathNavigator navigator = extensionData.CreateNavigator();
            XPathNavigator polkaItemIDNode = navigator.SelectSingleNode("extension/polka_item_id");
            if (polkaItemIDNode != null)
            {
                m_PolkaItemID = polkaItemIDNode.Value;
            }
        }

        protected override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("polka_item_id");
            if (this.PolkaItemID != null)
            {
                writer.WriteValue(
                    this.PolkaItemID);
            }
            writer.WriteEndElement();
        }

        public static void RegisterExtensionHandler()
        {
            ItemTypeManager.RegisterExtensionHandler(ExerciseExtensionSourceName, typeof(ExerciseExtension), true);
        }
    }


    public class ExerciseEx : Microsoft.Health.ItemTypes.Exercise
    {
        public ExerciseEx()
        {

        }

        private ExerciseExtension m_ExerciseExtension;
        private bool m_ExerciseExtensionLoaded = false;

        private ExerciseExtension GetExtendedData()
        {
            foreach (HealthRecordItemExtension extension in this.CommonData.Extensions)
            {
                ExerciseExtension ExerciseExtension = extension as ExerciseExtension;
                if (ExerciseExtension != null)
                {
                    m_ExerciseExtensionLoaded = true;
                    return ExerciseExtension;
                }
            }
            m_ExerciseExtensionLoaded = true;
            return null;
        }

        public string PolkaItemID
        {
            get
            {
                if (!m_ExerciseExtensionLoaded)
                {
                    m_ExerciseExtension = GetExtendedData();
                }

                if (m_ExerciseExtension == null)
                {
                    return null;
                }
                return m_ExerciseExtension.PolkaItemID;
            }
            set
            {
                if (!m_ExerciseExtensionLoaded)
                {
                    m_ExerciseExtension = GetExtendedData();
                }

                if (m_ExerciseExtension == null)
                {
                    m_ExerciseExtension = new ExerciseExtension(value);
                    this.CommonData.Extensions.Add(m_ExerciseExtension);
                }
                else
                {
                    m_ExerciseExtension.PolkaItemID = value;
                }
            }
        }
    }
}
