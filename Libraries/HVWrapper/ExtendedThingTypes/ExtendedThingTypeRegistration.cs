﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;


namespace HVWrapper.ExtendedThingTypes
{
    public class ExtendedThingTypeRegistration
    {
        public static void RegisterAllExtendedTypes()
        {
            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.LifeGoal.TypeId, typeof(HVWrapper.ExtendedThingTypes.LifeGoalEx), true);
            LifeGoalExtension.RegisterExtensionHandler();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.Medication.TypeId, typeof(HVWrapper.ExtendedThingTypes.MedicationEx), true);
            MedicationExtension.RegisterExtensionHandler();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId, typeof(HVWrapper.ExtendedThingTypes.CholesterolEx), true);
            CholesterolExtension.RegisterExtensionHandler();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.BloodGlucose.TypeId, typeof(HVWrapper.ExtendedThingTypes.BloodGlucoseEx), true);
            BloodGlucoseExtension.RegisterExtensionHandler();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.BloodPressure.TypeId, typeof(HVWrapper.ExtendedThingTypes.BloodPressureEx), true);
            BloodPressureExtension.RegisterExtensionHandler();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.Exercise.TypeId, typeof(HVWrapper.ExtendedThingTypes.ExerciseEx), true);
            ExerciseExtension.RegisterExtensionHandler();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.Weight.TypeId, typeof(HVWrapper.ExtendedThingTypes.WeightEx), true);
            WeightExtension.RegisterExtensionHandler();

            ItemTypeManager.RegisterTypeHandler(Microsoft.Health.ItemTypes.HbA1C.TypeId, typeof(HVWrapper.ExtendedThingTypes.HbA1cEx), true);
            HbA1cExtension.RegisterExtensionHandler();
        }
    }
}
