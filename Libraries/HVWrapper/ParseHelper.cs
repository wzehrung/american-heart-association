﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.XPath;

namespace HVWrapper
{
    public class ParseHelper
    {
        public static double? ParseDouble(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            double valueDouble = xNav_Item.ValueAsDouble;

            return valueDouble;
        }

        public static int? ParseInt(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            int valueInt = xNav_Item.ValueAsInt;

            return valueInt;
        }

        public static DateTime? ParseDateTime(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            DateTime valueDateTime = xNav_Item.ValueAsDateTime;

            return valueDateTime;
        }

        /// <summary>
        /// Parse a bool out of the XML
        /// </summary>
        /// <param name="xPathNav"></param>
        /// <returns></returns>
        public static bool? ParseTrueFalseNullMissing(XPathNavigator xPathNav)
        {
            //If there is no node... it's null
            if (xPathNav == null) return null;

            string valueText = xPathNav.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xPathNav.ValueAsBoolean;
        }

        public static bool? SelectAndParse_TrueFalseNullMissing(
            XPathNavigator xPathNav,
            string query)
        {
            XPathNavigator xNavItem = xPathNav.SelectSingleNode(query);
            return ParseTrueFalseNullMissing(xNavItem);
        }

        /// <summary>
        /// Parses a TRUE/FALSE/NULL value
        /// </summary>
        /// <param name="navigator"></param>
        /// <param name="xPath"></param>
        /// <returns></returns>
        public static bool? ParseTrueFalseNull(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return xNav_Item.ValueAsBoolean;
        }

        public static string ParseString(XPathNavigator navigator, string xPath)
        {
            XPathNavigator xNav_Item = navigator.SelectSingleNode(xPath);
            //If hte navigator is NULL the value is NULL
            if (xNav_Item == null) return null;

            //See whats inside the node
            string valueText = xNav_Item.Value;
            if (string.IsNullOrEmpty(valueText)) return null;
            if (string.IsNullOrEmpty(valueText.Trim())) return null;

            return valueText;
        }
    }
}
