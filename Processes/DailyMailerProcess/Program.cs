﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GRCBase;
using AHACommon;

namespace DailyMailerProcess
{
    class Program
    {
        static LogHelper logger = null;

        public static void LOG(string str)
        {
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        static void Main(string[] args)
        {
            string strDailyMailerErrorEmail = AHACommon.AHAAppSettings.DailyMailerErrorEmail;

            // Offset is configurable. Typically -1 in production
            string      sOffsetInDays = GRCBase.ConfigReader.GetValue("DailyMailerDayOffset");
            int         iOffsetInDays = (!string.IsNullOrEmpty(sOffsetInDays)) ? int.Parse(sOffsetInDays) : 0;

            DateTime    dtProcessRunForDate = DateTime.Today.AddDays(iOffsetInDays);   // -1

            char[]      delimiterChars = { ',' } ;  // for provider list override
            try
            {
                logger = new LogHelper("DailyMailer");

                if (args.Length > 0)
                {
                    dtProcessRunForDate = Convert.ToDateTime(args[0]).Date;
                }

                LOG(string.Format("****Start Daily Mailer Process for Date - {0} at - {1}****", dtProcessRunForDate.ToString(), DateTime.Now.ToString()));

                LOG(string.Format("****DismissAlertsOlderThanNWeeks at - {0}****", DateTime.Now.ToString()));
                // PJB: this executes a stored procedure that for every provider/volunteer, it will automatically dismiss alerts which are 
                // older than the value for this setting (e.g. 1 week, 2 weeks, 3 weeks, never). The setting is configured per-user.
                AHAHelpContent.Alert.DismissAlertsOlderThanNWeeks();
                LOG(string.Format("****End DismissAlertsOlderThanNWeeks at - {0}****", DateTime.Now.ToString()));

                LOG(string.Format("****Start Fetching list of providers for Daily Digest at - {0}****", DateTime.Now.ToString()));
                List<AHAHelpContent.ProviderDetails> listProviders = AHAHelpContent.ProviderDetails.FindProvidersForDailyDigest();
                LOG(string.Format("****End Fetching list -  {0}(count) of providers for Daily Digest at - {1}****", listProviders.Count, DateTime.Now.ToString()));


                string csvProviderList = GRCBase.ConfigReader.GetValue("ProviderUsernameList");
                
                // PJB: make the comparison logic not deal with capitalization.
                string[]   providerOverride = (!string.IsNullOrEmpty(csvProviderList)) ? csvProviderList.ToLower().Split( delimiterChars ) : null;


                foreach (AHAHelpContent.ProviderDetails objProvider in listProviders)
                {
                    try
                    {
                        List<string> listPatientSummary = new List<string>();

                        bool skipThis = false;

                        if (providerOverride != null)
                        {
                            bool found = false;
                            foreach (string provUserName in providerOverride)
                            {
                                // PJB: we will do a comparison not taking into account capitalization,
                                // because that is what SQL does.
                                if (objProvider.UserName.ToLower().Equals(provUserName))
                                {
                                    found = true;
                                }
                            }
                            if (!found)
                                skipThis = true;
                        }

                        if (!skipThis)
                        {
                            // PJB: this will return the number of (non-dismissed) alerts that occured on the given date.
                            int iAlertCount = AHAHelpContent.Alert.GetAlertCountByProviderIDAndDate(objProvider.ProviderID, dtProcessRunForDate);
                            int iMessageCount = 0;
                            if (AHAAppSettings.IsMessagingEnabled)
                            {
                                iMessageCount = AHAHelpContent.Message.GetMessageCountByProviderIDAndDate(objProvider.ProviderID, dtProcessRunForDate);
                            }

                            if (iAlertCount == 0 && iMessageCount == 0)
                            {
                                continue;
                            }

                            //log in DB & send email
                            LOG(string.Format("****Start Sending Daily Summary for provider {0} for Daily Digest at - {1}****", objProvider.Email, DateTime.Now.ToString()));
                            EmailHelper.SendSumamryEmailToProvider(objProvider.FormattedName, objProvider.EmailForDailySummary, iAlertCount, iMessageCount, objProvider.LanguageLocale);
                            LOG(string.Format("****End Sending Daily Summary for provider {0} for Daily Digest at - {1}****", objProvider.Email, DateTime.Now.ToString()));
                        }
                    }
                    catch (System.Exception e)
                    {
                        string MailBody = "An error occurred while sending daily email to provider id - " + objProvider.ProviderID;
                        MailBody += "\r\n\r\n";
                        MailBody += e.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += e.StackTrace;

                        GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.AHAAppSettings.SystemEmail, strDailyMailerErrorEmail, "Error occurred while sending daily email to provider", MailBody, false);

                        LOG(MailBody);
                    }
                }

                LOG(string.Format("****End Daily Mailer Process at - {0}****", DateTime.Now.ToString()));
            }
            catch (System.Exception e)
            {
                string MailBody = "An error occurred while running daily mailer (send mail process). Please run the process again";
                MailBody += "\r\n\r\n";
                MailBody += e.Message;
                MailBody += "\r\n\r\n";
                MailBody += e.StackTrace;

                GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.AHAAppSettings.SystemEmail, strDailyMailerErrorEmail, "Error during the Daily Mailer process", MailBody, false);

                LOG(MailBody);
            }
        }
    }
}
