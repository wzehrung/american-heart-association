﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Resources;
using AHACommon;
using GRCBase;


namespace DailyMailerProcess
{
    public class EmailHelper
    {
        private static string _getEmailTemplateDirectory(string strLocale)
        {
            if (strLocale != Locale.English)
            {
                return string.Format(AHAAppSettings.EmailTemplateDirectory + ".{0}", strLocale);
            }
            return AHAAppSettings.EmailTemplateDirectory;
        }

        public static void SendSumamryEmailToProvider(string strToName, string strToEmail, int iAlertCount, int iMessageCount, string strLocale)
        {
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}\\{1}", _getEmailTemplateDirectory(strLocale), emailSetting.File_Summary_Email_Provider);

            string subject = DailyMailerProcess.ResourceFiles.DailyMailer.Subject_Summary_Email_Provider;
            string strContent = IOUtility.ReadHtmlFile(filepath);

            string strAlertMessg = string.Empty;
            string sbAPlusM = string.Empty;

            if (iAlertCount > 0 && iMessageCount > 0)
            {
                sbAPlusM = DailyMailerProcess.ResourceFiles.DailyMailer.Text_AlertsAndMssgs;
            }

            if (iAlertCount > 0 && iMessageCount > 0)
            {
                strAlertMessg = string.Format(DailyMailerProcess.ResourceFiles.DailyMailer.BothAlertAndMessg, iAlertCount.ToString(), iMessageCount.ToString());
            }
            else if (iAlertCount > 0)
            {
                strAlertMessg = string.Format(DailyMailerProcess.ResourceFiles.DailyMailer.AlertOnly, iAlertCount.ToString());
            }
            else if (iMessageCount > 0)
            {
                strAlertMessg = string.Format(DailyMailerProcess.ResourceFiles.DailyMailer.MessageOnly, iMessageCount.ToString());
            }

            if (iAlertCount == 0 && iMessageCount > 0)
            {
                sbAPlusM = DailyMailerProcess.ResourceFiles.DailyMailer.Text_Mssgs;
            }
            else if (iAlertCount > 0 && iMessageCount == 0)
            {
                sbAPlusM = DailyMailerProcess.ResourceFiles.DailyMailer.Text_Alerts;
            }

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##ALERT_MESSAGE##", strAlertMessg);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##ALERTS_AND_MESSAGES##", sbAPlusM);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_LINK##", AppSettings.Url.ProviderPortal );

            strContent = AHACommon.ConfigurableEmailTemplateHelper.GetCompleteHtmlForEmail(true, strContent, strLocale);

            string strDisclaimer = System.Text.RegularExpressions.Regex.Replace( ResourceFiles.DailyMailer.Provider_Footer_Disclaimer, "##REGISTERED##", "<sup>&#174;</sup>");
            strContent = AHACommon.ConfigurableEmailTemplateHelper.HtmlEmailSetDisclaimer(strContent, strDisclaimer);


            //Send Email
            
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
        }
    }
}
