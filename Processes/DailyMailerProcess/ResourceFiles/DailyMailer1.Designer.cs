﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DailyMailerProcess.ResourceFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class DailyMailer {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DailyMailer() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DailyMailerProcess.ResourceFiles.DailyMailer", typeof(DailyMailer).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} alert(s).
        /// </summary>
        public static string AlertOnly {
            get {
                return ResourceManager.GetString("AlertOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} alert(s) and {1} message(s).
        /// </summary>
        public static string BothAlertAndMessg {
            get {
                return ResourceManager.GetString("BothAlertAndMessg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} message(s).
        /// </summary>
        public static string MessageOnly {
            get {
                return ResourceManager.GetString("MessageOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heart360##REGISTERED## is not intended to provide medical advice or treatment and is not a substitute from your professional medical judgment based on the individual medical needs of your patient. The information provided by your patient can help you work with your patient to map a successful path to heart health.&lt;br /&gt;&lt;br /&gt;You understand that data entered by your patient into Heart360##REGISTERED## is self-reported and may contain errors, may be out-dated, or may be later changed by the patient.&lt;br /&gt;&lt;br  [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Provider_Footer_Disclaimer {
            get {
                return ResourceManager.GetString("Provider_Footer_Disclaimer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heart360: Daily summary of alerts.
        /// </summary>
        public static string Subject_Summary_Email_Provider {
            get {
                return ResourceManager.GetString("Subject_Summary_Email_Provider", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to alerts.
        /// </summary>
        public static string Text_Alerts {
            get {
                return ResourceManager.GetString("Text_Alerts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to alerts and messages.
        /// </summary>
        public static string Text_AlertsAndMssgs {
            get {
                return ResourceManager.GetString("Text_AlertsAndMssgs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to messages.
        /// </summary>
        public static string Text_Mssgs {
            get {
                return ResourceManager.GetString("Text_Mssgs", resourceCulture);
            }
        }
    }
}
