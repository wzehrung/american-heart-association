﻿using System;
using System.Collections.Generic;
using System.Text;
using GRCBase;

//
// PJB (3.5.2014) - this was refactored from AHACommon/AHAAppSettings.cs.
// PJB (6.27.2014) - taken from provider portal
//
namespace DailyMailerProcess.AppSettings
{
    /****************************************************************
     *
     * URLS
     * 
     ****************************************************************/

    public class Url
    {

        /// <summary>
        /// Provider Portal
        /// </summary>
        public static string ProviderPortal
        {
            get
            {
                return ConfigReader.GetValue("ProviderPortal");
            }
        }


        /// <summary>
        /// Error page url
        /// </summary>
        public static string EmailAssetUrl
        {
            get
            {
                return ConfigReader.GetValue("EmailAssetUrl");
            }
        }

        public static string AHA
        {
            get
            {
                return ConfigReader.GetValue("url.AHA");
            }
        }

        public static string Heart360
        {
            get
            {
                return ConfigReader.GetValue("url.Heart360");
            }
        }

        public static string Donate
        {
            get
            {
                return ConfigReader.GetValue("url.Donate");
            }
        }

        public static string Facebook
        {
            get
            {
                return ConfigReader.GetValue("url.Facebook");
            }
        }

        public static string Twitter { get { return ConfigReader.GetValue("url.Twitter"); } }

        public static string Heart360PrivacyPolicy { get { return ConfigReader.GetValue("url.H360PrivacyPolicy"); } }
        public static string Heart360Copyright { get { return ConfigReader.GetValue("url.H360Copyright"); } }
        public static string Heart360EthicsPolicy { get { return ConfigReader.GetValue("url.H360EthicsPolicy"); } }
        public static string Heart360ConflictOfInterest { get { return ConfigReader.GetValue("url.H360ConflictOfInterest"); } }
        public static string Heart360LinkingPolicy { get { return ConfigReader.GetValue("url.H360LinkingPolicy"); } }
        public static string Heart360Diversity { get { return ConfigReader.GetValue("url.H360Diversity"); } }
        public static string Heart360Careers { get { return ConfigReader.GetValue("url.H360Careers"); } }

    }

    /****************************************************************
     *
     * DIRECTORIES
     * 
     ****************************************************************/

    public class Directory
    {
        /// <summary>
        /// This function is used to get the information about EmailTemplateDirectory.
        /// </summary>
        public static string EmailTemplate
        {
            get
            {
                return ConfigReader.GetValue("EmailTemplateDirectory");
            }
        }
    }

    /****************************************************************
     *
     * SETTINGS
     * 
     ****************************************************************/

    public class Setting
    {
        /// <summary>
        /// This function is used to get the information about VersionNumber.
        /// </summary>
        public static string VersionNumber
        {
            get
            {
                return ConfigReader.GetValue("VersionNumber");
            }
        }
    }

    /****************************************************************
     *
     * SETTINGS
     * 
     ****************************************************************/
    public class Email
    {
        /// <summary>
        /// This function is used to get the information about which is the SMTPServer to be used.
        /// </summary>
        public static string SMTPServer
        {
            get
            {
                return ConfigReader.GetValue("SMTPServer");
            }
        }

        /// <summary>
        /// This function is used to know if Messaging is enabled or not.
        /// </summary>
        public static bool IsMessagingEnabled
        {
            get
            {
                return ConfigReader.GetValue("Is_Messaging_Enabled") == "1" ? true : false;
            }
        }

        /// <summary>
        /// SystemEmail
        /// </summary>
        public static string System
        {
            get
            {
                return ConfigReader.GetValue("SystemEmail");
            }
        }

        /// <summary>
        /// FeedbackToEmail
        /// </summary>
        public static string Feedback
        {
            get
            {
                return ConfigReader.GetValue("FeedbackToEmail");
            }
        }

        /// <summary>
        /// SenderEmail
        /// </summary>
        public static string Sender
        {
            get
            {
                return ConfigReader.GetValue("SenderEmail");
            }
        }

        public static string TemplateDailySummary
        {
            get
            {
                return ConfigReader.GetValue("File_Daily_Summary");
            }
        }
    }
}
