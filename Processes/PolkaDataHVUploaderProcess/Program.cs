﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using AHAHelpContent;
using System.Collections.ObjectModel;
using Microsoft.Health.Web;
using Microsoft.Health;

namespace PolkaDataHVUploaderProcess
{
    class Program
    {
        static LogHelper logger = null;

        public static void LOG(string str)
        {
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        static void Main(string[] args)
        {
            string strPolkaDataHVUploaderProcessErrorEmail = GRCBase.ConfigReader.GetValue("ProcessErrorEmail");
            try
            {
                logger = new LogHelper("PolkaDataHVUploaderProcess");


                LOG(string.Format("****Start PolkaDataHVUploader Process  at - {0}****", DateTime.Now.ToString()));

                LOG(string.Format("****Start initialization of Health Vault Wrapper at - {0}****", DateTime.Now.ToString()));
                // This initializes our use of Health Vault. 
                HVWrapper.LifeCycle.Initialize();
                LOG(string.Format("****End initialization of Health Vault Wrapper at -  {0}****", DateTime.Now.ToString()));

               LOG(string.Format("****Start fetch patient list at - {0}****", DateTime.Now.ToString()));
                List<Patient> listPatient = AHAHelpContent.Patient.GetPatientsForPolkaDataProcessingV2();
                LOG(string.Format("****Total Patient Count = {0} at - {1}****", listPatient.Count, DateTime.Now.ToString()));
                foreach (Patient objPatient in listPatient)
                {
                    try
                    {
                        if (!objPatient.OfflinePersonGUID.HasValue || !objPatient.OfflineHealthRecordGUID.HasValue)
                            continue;

    //                    if (!(objPatient.UserHealthRecordID == 38900) && (!(objPatient.UserHealthRecordID == 38915)))
    //continue;
                        //if (objPatient.PolkaID != "TESTER")
                        //    continue;

                        LOG(string.Format("****Start processing patient {0} at - {1}****", objPatient.PersonalItemGUID, DateTime.Now.ToString()));
                        PolkaDataManager.Heart360PolkaManager.ProcessFor3CiUser(objPatient.OfflinePersonGUID.Value, false, objPatient);
                        LOG(string.Format("****End processing patient {0} at - {1}****", objPatient.PersonalItemGUID, DateTime.Now.ToString()));
                    }
                    catch (Exception ex)
                    {
                        string MailBody = "An error occurred while processing patient " + objPatient.UserHealthRecordID + "  for PolkaDataHVUploaderProcess.";
                        MailBody += "\r\n\r\n";
                        MailBody += ex.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += ex.StackTrace;

                        LOG(MailBody);
                    }
                }
                LOG(string.Format("****End fetch patient list -  {0}(count) at - {1}****", listPatient.Count, DateTime.Now.ToString()));

                LOG(string.Format("****End PolkaDataHVUploader Process  at - {0}****", DateTime.Now.ToString()));
            }
            catch (System.Exception e)
            {
                string MailBody = "An error occurred while running PolkaDataHVUploaderProcess. Please run the process again.";
                MailBody += "\r\n\r\n";
                MailBody += e.Message;
                MailBody += "\r\n\r\n";
                MailBody += e.StackTrace;

                LOG(MailBody);

                GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, strPolkaDataHVUploaderProcessErrorEmail, "Error during the Polka Data HV Uploader Process", MailBody, false);

                // PJB: not sure we need this, nor if this is good standard practice
                // throw;
            }
        }

        public static Microsoft.Health.HealthRecordAccessor GetAccessor(Guid hvpersonid, Guid hvrecordid)
        {
            OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
            offlineConn.Authenticate();
            HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);
            return accessor;
        }

        public static void GetHVItems(Guid hvpersonid, Guid hvrecordid)
        {
            HealthRecordAccessor accessor = GetAccessor(hvpersonid, hvrecordid);
            HealthRecordSearcher searcher = accessor.CreateSearcher();

            HealthRecordFilter filterBloodGlucose = new HealthRecordFilter(Microsoft.Health.ItemTypes.BloodGlucose.TypeId);
            filterBloodGlucose.EffectiveDateMin = DateTime.MinValue;
            filterBloodGlucose.EffectiveDateMax = DateTime.MaxValue;
            searcher.Filters.Add(filterBloodGlucose);

            string strXsl = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?>
<xsl:stylesheet version=""1.0""
	xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" xmlns:wc=""urn:com.microsoft.wc.methods.response.GetThings3"">
<xsl:template match=""/"">
  <root>
    <xsl:for-each select=""//response/wc:info/group/thing"">
        <xsl:if test=""data-xml/common/extension/polka_item_id[text()= '102' or text()= '4']"">
    <thing>
      <thing-id><xsl:value-of select=""thing-id"" /></thing-id>
      <type-id><xsl:value-of select=""type-id"" /></type-id>
      <polka-id><xsl:value-of select=""data-xml/common/extension/polka_item_id"" /></polka-id>
    </thing>
    </xsl:if>
    </xsl:for-each>
  </root>
</xsl:template>
</xsl:stylesheet>";

            string str = searcher.GetTransformedItems(strXsl);
        }
    }
}
