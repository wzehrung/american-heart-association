﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using System.Resources;

namespace Heart360CampaignEmailProcess
{
    public class BPTriggerHelper
    {
        public static void ProcessBPTriggerEmail(LogHelper logger, int? iCampaignID, int iWeeks, string strCampaignUrl)
        {
            string      processErrorEmail = GRCBase.ConfigReader.GetValue("ProcessErrorEmail") ;

            Helper.LOG(logger, string.Format("****Start BP Trigger processs  at - {0}****", DateTime.Now.ToString()));

            Helper.LOG(logger, string.Format("****Start Fetching list of active users for bp trigger for last {0} weeks at - {1}****", iWeeks.ToString(), DateTime.Now.ToString()));
            List<AHAHelpContent.AHAUser> listAHAUsers = AHAHelpContent.AHAUser.FindUsersForAHABPTriggerEmails(iWeeks, iCampaignID);
            Helper.LOG(logger, string.Format("****End Fetching list of active users for bp trigger for last {0} weeks at - {1}****", iWeeks.ToString(), DateTime.Now.ToString()));
            ResourceManager rm = Helper.GetResourceManager();

            string strSubject = string.Empty;


            if (listAHAUsers.Count > 0)
            {
                List<AHAHelpContent.AHAUser> listGroup0 = listAHAUsers.Where(a => a.Group1 == 1).ToList();
                List<AHAHelpContent.AHAUser> listGroup1 = listAHAUsers.Where(a => a.Group2 == 1).ToList();
                List<AHAHelpContent.AHAUser> listGroup2 = listAHAUsers.Where(a => a.Group3 == 1).ToList();

                foreach (AHAHelpContent.AHAUser objAHAUser in listGroup0)
                {
                    try
                    {
                        HVManager.CurrentUserHVSetting objSettings = HVManager.CurrentUserHVSetting.GetUserSetting(objAHAUser.PersonGUID, Guid.Empty);

                        if (objSettings == null || (objSettings != null && string.IsNullOrEmpty(objSettings.Email)))
                            continue;

                        System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objAHAUser.LanguageLocale);
                        string strEmailType = Heart360EmailType.BPTriggerLevel0.ToString();
                        strSubject = rm.GetString("Subject_BP_Level0", cultureInfo);

                        //log in DB & send email
                        Helper.LOG(logger, string.Format("****Start Sending Level 0 notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                        //Helper.SendMailToUser(false, string.Empty, objSettings.Email, AHAAppSettings.Heart360CampaignEmailProcessConfig.File_BP_Trigger_Excellent, strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        EmailHelper.SendMailToUser(false, string.Empty, objSettings.Email, GRCBase.ConfigReader.GetValue("File_BP_Trigger_Excellent"), strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        AHAHelpContent.EmailDespatchDetails.CreateDespatchDetails(objAHAUser.UserID, null, iCampaignID, strEmailType);
                        Helper.LOG(logger, string.Format("****End Sending Level 0 notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                    }
                    catch (System.Exception e)
                    {
                        string MailBody = "An error occurred while sending Level 0 notification to user id - " + objAHAUser.UserID.ToString();
                        MailBody += "\r\n\r\n";
                        MailBody += e.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += e.StackTrace;

                        GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, processErrorEmail, "Error occurred while sending inactive notification email to user", MailBody, false);

                        Helper.LOG(logger, MailBody);
                    }
                }

                foreach (AHAHelpContent.AHAUser objAHAUser in listGroup1)
                {
                    try
                    {
                        HVManager.CurrentUserHVSetting objSettings = HVManager.CurrentUserHVSetting.GetUserSetting(objAHAUser.PersonGUID, Guid.Empty);

                        if (objSettings == null || (objSettings != null && string.IsNullOrEmpty(objSettings.Email)))
                            continue;

                        System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objAHAUser.LanguageLocale);
                        string strEmailType = Heart360EmailType.BPTriggerLevel1.ToString();
                        strSubject = rm.GetString("Subject_BP_Level1", cultureInfo);

                        //log in DB & send email
                        Helper.LOG(logger, string.Format("****Start Sending Level 1 notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                        //Helper.SendMailToUser(false, string.Empty, objSettings.Email, AHAAppSettings.Heart360CampaignEmailProcessConfig.File_BP_Trigger_NeedsImprovement, strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        EmailHelper.SendMailToUser(false, string.Empty, objSettings.Email, GRCBase.ConfigReader.GetValue("File_BP_Trigger_NeedsImprovement"), strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        AHAHelpContent.EmailDespatchDetails.CreateDespatchDetails(objAHAUser.UserID, null, iCampaignID, strEmailType);
                        Helper.LOG(logger, string.Format("****End Sending Level 1 notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                    }
                    catch (System.Exception e)
                    {
                        string MailBody = "An error occurred while sending Level 1 notification to user id - " + objAHAUser.UserID.ToString();
                        MailBody += "\r\n\r\n";
                        MailBody += e.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += e.StackTrace;

                        GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, processErrorEmail, "Error occurred while sending inactive notification email to user", MailBody, false);

                        Helper.LOG(logger, MailBody);
                    }
                }

                foreach (AHAHelpContent.AHAUser objAHAUser in listGroup2)
                {
                    try
                    {
                        HVManager.CurrentUserHVSetting objSettings = HVManager.CurrentUserHVSetting.GetUserSetting(objAHAUser.PersonGUID, Guid.Empty);

                        if (objSettings == null || (objSettings != null && string.IsNullOrEmpty(objSettings.Email)))
                            continue;

                        System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objAHAUser.LanguageLocale);
                        string strEmailType = Heart360EmailType.BPTriggerLevel2.ToString();
                        strSubject = rm.GetString("Subject_BP_Level2", cultureInfo);

                        //log in DB & send email
                        Helper.LOG(logger, string.Format("****Start Sending Level 2 notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                        //Helper.SendMailToUser(false, string.Empty, objSettings.Email, AHAAppSettings.Heart360CampaignEmailProcessConfig.File_BP_Trigger_Warning, strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        EmailHelper.SendMailToUser(false, string.Empty, objSettings.Email, GRCBase.ConfigReader.GetValue("File_BP_Trigger_Warning"), strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        AHAHelpContent.EmailDespatchDetails.CreateDespatchDetails(objAHAUser.UserID, null, iCampaignID, strEmailType);
                        Helper.LOG(logger, string.Format("****End Sending Level 2 notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                    }
                    catch (System.Exception e)
                    {
                        string MailBody = "An error occurred while sending Level 2 notification to user id - " + objAHAUser.UserID.ToString();
                        MailBody += "\r\n\r\n";
                        MailBody += e.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += e.StackTrace;

                        GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, processErrorEmail, "Error occurred while sending inactive notification email to user", MailBody, false);

                        Helper.LOG(logger, MailBody);
                    }
                }

                Helper.LOG(logger, string.Format("****End BP Trigger processs - at - {0}****", DateTime.Now.ToString()));
            }
        }
    }
}
