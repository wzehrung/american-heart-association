﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;

using AHACommon;
using GRCBase;
using Heart360CampaignEmailProcess.App_GlobalResources;



namespace Heart360CampaignEmailProcess
{
    public class EmailHelper
    {
        private static string _getEmailTemplateDirectory(string strLocale)
        {

            return (strLocale != Locale.English) ? string.Format(MinimumAppSettings.EmailTemplateDirectory + ".{0}", strLocale) :
                                                    MinimumAppSettings.EmailTemplateDirectory ;
        }

        private static ResourceManager _GetResourceManager()
        {
            ResourceManager rm = new System.Resources.ResourceManager("Heart360CampaignEmailProcess.App_GlobalResources.EmailProcess", typeof(EmailProcess).Assembly);
            //ResourceManager rm = new System.Resources.ResourceManager("ContentExpirationNotificationProcess.App_GlobalResources.ContentExpiration", typeof(ContentExpiration).Assembly);
            return rm;
        }


        public static void SendMailToUser(bool bIsProvider, string strToName, string strToEmail, string strFileName, string strSubject, string strLocale, string strCampaignUrl)
        {
            string filepath = string.Format("{0}\\{1}", _getEmailTemplateDirectory(strLocale), strFileName);

            string strMailContent = IOUtility.ReadHtmlFile(filepath);

            strMailContent = AHACommon.ConfigurableEmailTemplateHelper.GetCompleteHtmlForEmail(true, strMailContent, strLocale);
            strMailContent = System.Text.RegularExpressions.Regex.Replace(strMailContent, "##CAMPAIGN_URL##", strCampaignUrl);


            // get some text strings out of the resource file
            ResourceManager rm = _GetResourceManager();
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string strDisclaimer = rm.GetString( ((bIsProvider) ? "Provider_Footer_Disclaimer" : "Patient_Footer_Disclaimer"), cultureInfo);

            strMailContent = AHACommon.ConfigurableEmailTemplateHelper.HtmlEmailSetDisclaimer(strMailContent, strDisclaimer);

            //Send Email
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHACommon.MinimumAppSettings.SystemEmail, strToEmail, strSubject, strMailContent, true, null, AHACommon.MinimumAppSettings.SystemEmail, null);
        }

/**
        public static void SendContentExpirationNotificationToAdmin(string strToName, string strToEmail, string strContent, DateTime dtExpiringDate, string strLocale)
        {
            ResourceManager rm = _GetResourceManager();

            // get some text strings out of the resource file
            System.Globalization.CultureInfo    cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string                              strDiscliamerText = rm.GetString("Patient_Footer_Disclaimer", cultureInfo);

            // get the filepath to the email template
            string filepath = string.Format("{0}\\{1}", _getEmailTemplateDirectory(strLocale), GRCBase.ConfigReader.GetValue("File_Content_Expiration" ) );
            string strMailContent = IOUtility.ReadHtmlFile(filepath);

            string subject = System.Text.RegularExpressions.Regex.Replace( rm.GetString("Email_Subject", cultureInfo), "##ExpiringDate##", dtExpiringDate.ToShortDateString());

            strMailContent = System.Text.RegularExpressions.Regex.Replace(strMailContent, "##Content##", strContent);
            strMailContent = AHACommon.ConfigurableEmailTemplateHelper.GetCompleteHtmlForEmail(true, strMailContent, strLocale);

            AHACommon.ConfigurableEmailTemplateHelper.HtmlEmailSetDisclaimer( strMailContent, 
            

            strMailContent = System.Text.RegularExpressions.Regex.Replace(strMailContent, "##DISCLAIMER##", strDiscliamerText);
            strMailContent = System.Text.RegularExpressions.Regex.Replace(strMailContent, "##REGISTERED##", "<sup>&#174;</sup>");
            //Send Email
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHACommon.MinimumAppSettings.SystemEmail, strToEmail, subject, strMailContent, true, null, AHACommon.MinimumAppSettings.SystemEmail, null);
        }
**/
       
    }
}
