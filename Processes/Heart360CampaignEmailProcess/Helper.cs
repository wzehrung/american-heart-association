﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using System.Resources;
using GRCBase;

namespace Heart360CampaignEmailProcess
{
    public enum Heart360EmailType
    {
        InactivePatient14Days,
        InactivePatient30Days,
        InactiveProvider14Days,
        InactiveProvider30Days,
        InactivePatientDefault,
        InactiveProviderDefault,
        AppointmentReminderToPatient,
        BPTriggerLevel0,
        BPTriggerLevel1,
        BPTriggerLevel2,
    }

    public class Helper
    {
        public static void LOG(LogHelper logger, string str)
        {
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        public static ResourceManager GetResourceManager()
        {
            ResourceManager rm = new System.Resources.ResourceManager("Heart360CampaignEmailProcess.App_GlobalResources.EmailProcess", typeof(Heart360CampaignEmailProcess.App_GlobalResources.EmailProcess).Assembly);
            return rm;
        }

/**
        private static string _getEmailTemplateDirectory(string strLocale)
        {
            if (strLocale != Locale.English)
            {
                return string.Format(AHAAppSettings.EmailTemplateDirectory + ".{0}", strLocale);
            }
            return AHAAppSettings.EmailTemplateDirectory;
        }

        public static void SendMailToUser(bool bIsProvider, string strToName, string strToEmail, string strFileName, string strSubject, string strLocale, string strCampaignUrl)
        {
            string filepath = string.Format("{0}\\{1}", _getEmailTemplateDirectory(strLocale), strFileName);

            string strMailContent = IOUtility.ReadHtmlFile(filepath);

            strMailContent = GetCompleteHtmlForEmail(true, strMailContent, bIsProvider, strLocale);
            strMailContent = System.Text.RegularExpressions.Regex.Replace(strMailContent, "##CAMPAIGN_URL##", strCampaignUrl);

            //Send Email
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, strSubject, strMailContent, true, null, AHAAppSettings.SenderEmail, null);
        }


        

        public static string SiteUrlForImage(bool bProcess)
        {
            if (bProcess)
            {
                return AHACommon.AHAAppSettings.SiteUrlNoHttps;
            }
            else
            {
                return GRCBase.UrlUtility.ServerRootUrl.Replace("https://", "http://");
            }
        }

        public static string GetEmailTemplateDirectory(bool bProcess, string strLocale)
        {
            if (strLocale != Locale.English)
            {
                return string.Format(AHAAppSettings.EmailTemplateDirectory + ".{0}", strLocale);
            }
            return AHAAppSettings.EmailTemplateDirectory;
        }

        public static string GetEmailHeader(bool bProcess, string strLocale)
        {
            string strFilePath = string.Format("{0}\\Header.htm", GetEmailTemplateDirectory(bProcess, strLocale));

            string strHeader = IOUtility.ReadHtmlFile(strFilePath);

            strHeader = System.Text.RegularExpressions.Regex.Replace(strHeader, "##SiteUrl##", SiteUrlForImage(bProcess));

            return strHeader;
        }

        public static string GetEmailFooter(bool bProcess, bool bIsForProvider, string strLocale)
        {
            string strFilePath = string.Format("{0}\\Footer.htm", GetEmailTemplateDirectory(bProcess, strLocale));
            string strFooter = IOUtility.ReadHtmlFile(strFilePath);

            ResourceManager rm = GetResourceManager();
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);

            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##SiteUrl##", SiteUrlForImage(bProcess));
            if (bIsForProvider)
            {
                strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##DISCLAIMER##", rm.GetString("Provider_Footer_Disclaimer", cultureInfo));
            }
            else
            {
                strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##DISCLAIMER##", rm.GetString("Patient_Footer_Disclaimer", cultureInfo));
            }
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##REGISTERED##", "<sup>&#174;</sup>");
            return strFooter;
        }

        public static string GetCompleteHtmlForEmail(bool bProcess, string strContent, bool bIsForProvider, string strLocale)
        {
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##SiteUrl##", SiteUrlForImage(bProcess));

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{0}{1}{2}", GetEmailHeader(bProcess, strLocale), strContent, GetEmailFooter(bProcess, bIsForProvider, strLocale));

            return sb.ToString();
        }
 **/
    }
}
