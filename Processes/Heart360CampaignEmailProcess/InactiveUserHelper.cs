﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using System.Resources;

namespace Heart360CampaignEmailProcess
{
    public class InactiveUserHelper
    {
        public static void ProcessInactiveEmailForPatients(LogHelper logger, int? iCampaignID, int intNumInActiveDays, string strFileName, string strCampaignUrl)
        {
            Helper.LOG(logger, string.Format("****Start " + intNumInActiveDays + " days  InActive User Notification Process  at - {0}****", DateTime.Now.ToString()));

            Helper.LOG(logger, string.Format("****Start Fetching list of users who have not logged in for last {0} days at - {1}****", intNumInActiveDays.ToString(), DateTime.Now.ToString()));
            List<AHAHelpContent.AHAUser> listAHAUsers = AHAHelpContent.AHAUser.FindAllUsersNotLoggedInForLastNDays(intNumInActiveDays, iCampaignID);
            Helper.LOG(logger, string.Format("****End Fetching list of users who have not logged in for last {0} days at - {1}****", intNumInActiveDays.ToString(), DateTime.Now.ToString()));
            ResourceManager rm = Helper.GetResourceManager();

            string strSubject = string.Empty;


            if (listAHAUsers.Count > 0)
            {
                foreach (AHAHelpContent.AHAUser objAHAUser in listAHAUsers)
                {
                    try
                    {
                        HVManager.CurrentUserHVSetting objSettings = HVManager.CurrentUserHVSetting.GetUserSetting(objAHAUser.PersonGUID, Guid.Empty);

                        if (objSettings == null || (objSettings != null && string.IsNullOrEmpty(objSettings.Email)))
                            continue;

                        System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objAHAUser.LanguageLocale);
                        string strEmailType = string.Empty;
                        if (intNumInActiveDays == 14)
                        {
                            strEmailType = Heart360EmailType.InactivePatient14Days.ToString();
                            strSubject = rm.GetString("Subject_14Day_Inactive_Patient", cultureInfo);
                        }
                        else if (intNumInActiveDays == 30)
                        {
                            strEmailType = Heart360EmailType.InactivePatient30Days.ToString();
                            strSubject = rm.GetString("Subject_30Day_Inactive_Patient", cultureInfo);
                        }
                        else
                        {
                            strEmailType = Heart360EmailType.InactivePatientDefault.ToString();
                            strSubject = rm.GetString("Subject_Inactive_Patient_Default", cultureInfo);
                        }

                        //log in DB & send email
                        Helper.LOG(logger, string.Format("****Start Sending InActive notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));

                        EmailHelper.SendMailToUser(false, string.Empty, objSettings.Email, strFileName, strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        //Helper.SendMailToUser(false, string.Empty, objSettings.Email, strFileName, strSubject, objAHAUser.LanguageLocale, strCampaignUrl);
                        AHAHelpContent.EmailDespatchDetails.CreateDespatchDetails(objAHAUser.UserID, null, iCampaignID, strEmailType);
                        Helper.LOG(logger, string.Format("****End Sending InActive notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                    }
                    catch (System.Exception e)
                    {
                        string MailBody = "An error occurred while sending inactive notification to user id - " + objAHAUser.UserID.ToString();
                        MailBody += "\r\n\r\n";
                        MailBody += e.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += e.StackTrace;

                        GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, GRCBase.ConfigReader.GetValue("ProcessErrorEmail"), "Error occurred while sending inactive notification email to user", MailBody, false);

                        Helper.LOG(logger, MailBody);
                    }
                }

                Helper.LOG(logger, string.Format("****End " + intNumInActiveDays + "  days InActive user Notification Process - at - {0}****", DateTime.Now.ToString()));
            }
        }

        public static void ProcessInactiveEmailForProviders(LogHelper logger, int? iCampaignID, int intNumInActiveDays, string strFileName, string strCampaignUrl)
        {
            Helper.LOG(logger, string.Format("****Start InActive Provider Notification Process  at - {0}****", DateTime.Now.ToString()));

            Helper.LOG(logger, string.Format("****Start Fetching list of providers who have not logged in for last {0} days at - {1}****", intNumInActiveDays.ToString(), DateTime.Now.ToString()));
            List<AHAHelpContent.ProviderDetails> listProviderDetails = AHAHelpContent.ProviderDetails.ProviderNotLoggedInForLastNDays(intNumInActiveDays, iCampaignID);
            Helper.LOG(logger, string.Format("****End Fetching list of providers who have not logged in for last {0} days at - {1}****", intNumInActiveDays.ToString(), DateTime.Now.ToString()));


            if (listProviderDetails.Count > 0)
            {
                ResourceManager rm = Helper.GetResourceManager();

                string strSubject = string.Empty;

                foreach (AHAHelpContent.ProviderDetails objProviderDetails in listProviderDetails)
                {
                    try
                    {
                        System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objProviderDetails.LanguageLocale);
                        string strEmailType = string.Empty;
                        if (intNumInActiveDays == 14)
                        {
                            strEmailType = Heart360EmailType.InactivePatient14Days.ToString();
                            strSubject = rm.GetString("Subject_14Day_Inactive_Patient", cultureInfo);
                        }
                        else if (intNumInActiveDays == 30)
                        {
                            strEmailType = Heart360EmailType.InactivePatient30Days.ToString();
                            strSubject = rm.GetString("Subject_30Day_Inactive_Patient", cultureInfo);
                        }
                        else
                        {
                            strEmailType = Heart360EmailType.InactiveProviderDefault.ToString();
                            strSubject = rm.GetString("Subject_Inactive_Provider_Default", cultureInfo);
                        }
                        //log in DB & send email
                        Helper.LOG(logger, string.Format("****Start Sending InActive notification to provider {0}  at - {1}****", objProviderDetails.Email, DateTime.Now.ToString()));
                        EmailHelper.SendMailToUser(true, string.Empty, objProviderDetails.Email, strFileName, strSubject, objProviderDetails.LanguageLocale, strCampaignUrl);
                        AHAHelpContent.EmailDespatchDetails.CreateDespatchDetails(null, objProviderDetails.ProviderID, iCampaignID, strEmailType);
                        Helper.LOG(logger, string.Format("****End Sending InActive notification to provider {0}  at - {1}****", objProviderDetails.Email, DateTime.Now.ToString()));
                    }
                    catch (System.Exception e)
                    {
                        string MailBody = "An error occurred while sending inactive notification to provider id - " + objProviderDetails.ProviderID;
                        MailBody += "\r\n\r\n";
                        MailBody += e.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += e.StackTrace;

                        GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, GRCBase.ConfigReader.GetValue("ProcessErrorEmail"), "Error occurred while sending inactive notification email to priovder", MailBody, false);

                        Helper.LOG(logger, MailBody);
                    }
                }

                Helper.LOG(logger, string.Format("****End InActive Provider Notification Process - at - {0}****", DateTime.Now.ToString()));
            }
        }
    }
}
