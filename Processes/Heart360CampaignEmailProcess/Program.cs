﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using AHAHelpContent;
using System.Resources;

namespace Heart360CampaignEmailProcess
{
    class Program
    {
        static LogHelper logger = null;
        const int _14Days = 14;
        const int _30Days = 30;
        const int _4Months = 4;
        const int _6Weeks = 1;

        public static void LOG(string str)
        {
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Syntax: Heart360CampaignEmailProcess <switch>");
                Console.WriteLine("Switch: BPT | IU14 | IU30 | IUD | IP14 | IP30 | IPD | AR");
                return;
            }

            logger = new LogHelper("Heart360CampaignEmailProcess");

            LOG(string.Format("****Start initialization of Health Vault Wrapper at - {0}****", DateTime.Now.ToString()));
            // This initializes our use of Health Vault. The Group Manager project uses this
            HVWrapper.LifeCycle.Initialize();
            LOG(string.Format("****End initialization of Health Vault Wrapper at -  {0}****", DateTime.Now.ToString()));

            string strHeart360CampaignEmailProcessCampaignUrlSuffix = GRCBase.ConfigReader.GetValue("Heart360CampaignEmailProcessCampaignUrlSuffix");

            Campaign objCampaign = Campaign.FindCampaignByURL(strHeart360CampaignEmailProcessCampaignUrlSuffix,Language.GetDefaultLanguage().LanguageID);

            string strCampaignUrl = string.Format("{0}/{1}", AHACommon.AHAAppSettings.SiteUrlNoHttps, ((objCampaign != null) ? objCampaign.CampaignURL : string.Empty) );

            string strSwitch = args[0].ToUpper();

            switch (strSwitch)
            {
                case "BPT":
                    ThrowCampignNotFoundException(objCampaign);
                    BPTriggerHelper.ProcessBPTriggerEmail(logger, objCampaign.CampaignID, _6Weeks, strCampaignUrl);
                    break;
                case "IU14":
                    ThrowCampignNotFoundException(objCampaign);
                    InactiveUserHelper.ProcessInactiveEmailForPatients(logger, objCampaign.CampaignID, _14Days, GRCBase.ConfigReader.GetValue("File_14Day_Inactive_Patient"), strCampaignUrl);
                    break;
                case "IU30":
                    ThrowCampignNotFoundException(objCampaign);
                    InactiveUserHelper.ProcessInactiveEmailForPatients(logger, objCampaign.CampaignID, _30Days, GRCBase.ConfigReader.GetValue("File_30Day_Inactive_Patient"), strCampaignUrl);
                    break;
                case "IUD":
                    {
                        int nbInactiveDays = Convert.ToInt32(GRCBase.ConfigReader.GetValue("User_NumberOfInActiveDays"));
                        InactiveUserHelper.ProcessInactiveEmailForPatients(logger, null, nbInactiveDays, GRCBase.ConfigReader.GetValue("File_InActive_Patient_Notification"), strCampaignUrl);
                    }
                    break;
                case "IP14":
                    ThrowCampignNotFoundException(objCampaign);
                    InactiveUserHelper.ProcessInactiveEmailForProviders(logger, objCampaign.CampaignID, _14Days, GRCBase.ConfigReader.GetValue("File_14Day_Inactive_Provider"), strCampaignUrl);
                    break;
                case "IP30":
                    ThrowCampignNotFoundException(objCampaign);
                    InactiveUserHelper.ProcessInactiveEmailForProviders(logger, objCampaign.CampaignID, _30Days, GRCBase.ConfigReader.GetValue("File_30Day_Inactive_Provider"), strCampaignUrl);
                    break;
                case "IPD":
                    {
                        int nbInactiveDays = Convert.ToInt32(GRCBase.ConfigReader.GetValue("Provider_NumberOfInActiveDays"));
                        InactiveUserHelper.ProcessInactiveEmailForProviders(logger, null, nbInactiveDays, GRCBase.ConfigReader.GetValue("File_InActive_Provider_Notification"), strCampaignUrl);
                    }
                    break;
                case "AR":
                    ThrowCampignNotFoundException(objCampaign);
                    AppointmentReminderHelper.ProcessAppointmentReminderEmail(logger, objCampaign.CampaignID, _4Months, GRCBase.ConfigReader.GetValue("File_AppointmentReminder"), strCampaignUrl);
                    break;
            }

            //string subject = rm.GetString("Subject_InActive_Notification", cultureInfo);
        }

        private static void ThrowCampignNotFoundException(Campaign objCampaign)
        {
            if (objCampaign == null)
                throw new Exception("A campaign must be specified to run this process.");
        }
    }
}
