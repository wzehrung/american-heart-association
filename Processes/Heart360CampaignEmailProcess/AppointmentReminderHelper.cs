﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using System.Resources;

namespace Heart360CampaignEmailProcess
{
    public class AppointmentReminderHelper
    {
        public static void ProcessAppointmentReminderEmail(LogHelper logger, int? iCampaignID, int intNumInMonths, string strFileName, string strCampaignUrl)
        {
            Helper.LOG(logger, string.Format("****Start AppointmentReminder Process  at - {0}****", DateTime.Now.ToString()));

            Helper.LOG(logger, string.Format("****Start Fetching list of users who have signed up for last {0} months at - {1}****", intNumInMonths.ToString(), DateTime.Now.ToString()));
            List<AHAHelpContent.AHAUser> listAHAUsers = AHAHelpContent.AHAUser.FindAllUsersSignedUpForLastNMonths(intNumInMonths, iCampaignID);
            Helper.LOG(logger, string.Format("****End Fetching list of users who have signed up for last {0} months at - {1}****", intNumInMonths.ToString(), DateTime.Now.ToString()));
            ResourceManager rm = Helper.GetResourceManager();

            string strSubject = string.Empty;


            if (listAHAUsers.Count > 0)
            {
                foreach (AHAHelpContent.AHAUser objAHAUser in listAHAUsers)
                {
                    try
                    {
                        HVManager.CurrentUserHVSetting objSettings = HVManager.CurrentUserHVSetting.GetUserSetting(objAHAUser.PersonGUID, Guid.Empty);

                        if (string.IsNullOrEmpty(objSettings.Email))
                            continue;

                        System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objAHAUser.LanguageLocale);
                        strSubject = rm.GetString("Subject_AppointmentReminder", cultureInfo);
                        string strEmailType = Heart360EmailType.AppointmentReminderToPatient.ToString();

                        //log in DB & send email
                        Helper.LOG(logger, string.Format("****Start Sending InActive notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                        EmailHelper.SendMailToUser(false, string.Empty, objSettings.Email, strFileName, strSubject, objAHAUser.LanguageLocale,strCampaignUrl);
                        AHAHelpContent.EmailDespatchDetails.CreateDespatchDetails(objAHAUser.UserID,null, iCampaignID, strEmailType);
                        Helper.LOG(logger, string.Format("****End Sending InActive notification to user {0}  at - {1}****", objSettings.Email, DateTime.Now.ToString()));
                    }
                    catch (System.Exception e)
                    {
                        string MailBody = "An error occurred while sending appointment reminder to user id - " + objAHAUser.UserID.ToString();
                        MailBody += "\r\n\r\n";
                        MailBody += e.Message;
                        MailBody += "\r\n\r\n";
                        MailBody += e.StackTrace;

                        GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, "", "Error occurred while sending inactive provider notification email to priovder", MailBody, false);

                        Helper.LOG(logger, MailBody);
                    }
                }

                Helper.LOG(logger, string.Format("****End AppointmentReminder Process - at - {0}****", DateTime.Now.ToString()));
            }
        }
    }
}
