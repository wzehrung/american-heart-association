﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using AlertManager;
using AHACommon;


namespace AlertGenerationProcess
{
    class Program
    {
        static LogHelper logger = null;

        public static void LOG(string str)
        {
           // Console.WriteLine(str);
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        static void Main(string[] args)
        {
            string strAlertGenerationProcessErrorEmail = GRCBase.ConfigReader.GetValue( "ProcessErrorEmail") ; // AHACommon.AHAAppSettings.AlertGenerationProcessErrorEmail;
            DBContext dbContext = null;
            bool bException = false;
            bool bCreateAlertFailed = false;

            try
            {
                DateTime dtProcessRunForDate = DateTime.Now.AddDays(-1);

                logger = new LogHelper("AlertGeneration");
                LOG(string.Format("****Start Alert Generation Process at - {0}****", DateTime.Now.ToString()));

                LOG(string.Format("****Start initialization of Health Vault Wrapper at - {0}****", DateTime.Now.ToString()));
                // This initializes our use of Health Vault. The Group Manager project uses this
                HVWrapper.LifeCycle.Initialize();
                LOG(string.Format("****End initialization of Health Vault Wrapper at -  {0}****", DateTime.Now.ToString()));

                dbContext = DBContext.GetDBContext();

                LOG(string.Format("****Start CreateAlertsForDate at - {0}****", DateTime.Now.ToString()));
                AlertManager.Helper.CreateAlertsForDate(logger, dtProcessRunForDate);
                LOG(string.Format("****End CreateAlertsForDate at - {0}****", DateTime.Now.ToString()));
            }
            catch (System.Exception e)
            {
                bException = true;
                bCreateAlertFailed = true;
                string MailBody = "An error occurred during Alert Generation Process.";
                MailBody += "\r\n\r\n";
                MailBody += e.Message;
                MailBody += "\r\n\r\n";
                MailBody += e.StackTrace;

                GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, strAlertGenerationProcessErrorEmail, "Error creating Alerts during the Alert Generation process", MailBody, false);

                LOG(MailBody);

                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }

            if (bCreateAlertFailed)
            {
                LOG(string.Format("****Alert Process Failed at - {0}****", DateTime.Now.ToString()));
                return;
            }

            LOG(string.Format("****End Alert Generation Process at - {0}****", DateTime.Now.ToString()));
            //Console.ReadLine();
        }
    }
}
