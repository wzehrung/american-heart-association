﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using Microsoft.Health.Web;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;

using AHAHelpContent;
using GroupManager;

namespace GroupMembershipProcess
{
    public class HVHelper
    {
        /// <summary>
        /// Returns the Accessor object for a given PersonID and HealthRecordID using an offline connection
        /// </summary>
        /// <param name="hvpersonid"></param>
        /// <param name="hvrecordid"></param>
        /// <returns></returns>
        public static Microsoft.Health.HealthRecordAccessor GetAccessor(Guid hvpersonid, Guid hvrecordid)
        {
            OfflineWebApplicationConnection offlineConn = new OfflineWebApplicationConnection(hvpersonid);
            offlineConn.Authenticate();
            HealthRecordAccessor accessor = new HealthRecordAccessor(offlineConn, hvrecordid);
            return accessor;
        }

        public static IList<Guid> GetUpdatedRecordsForTheDay()
        {
            OfflineWebApplicationConnection conn = new OfflineWebApplicationConnection(WebApplicationConfiguration.AppId,
               WebApplicationConfiguration.HealthServiceUrl.ToString(), Guid.Empty);

            IList<Guid> list = conn.GetUpdatedRecordsForApplication(DateTime.Today.AddDays(-1));

            return list;
        }

       /// <summary>
        /// ReadOnlyCollection<HealthRecordItemCollection>
       /// </summary>
       /// <param name="iLastNDays"></param>
       /// <param name="hvpersonid"></param>
       /// <param name="hvrecordid"></param>
       /// <returns></returns>
        //public static Dictionary<Guid, List<HealthRecordItem>> GetAllValuesForLastNDays(int iLastNDays, Guid hvpersonid, Guid hvrecordid)
        //{
        //    HealthRecordAccessor accessor = GetAccessor(hvpersonid, hvrecordid);
        //    HealthRecordSearcher searcher = accessor.CreateSearcher();

        //    iLastNDays = -1 * iLastNDays;

        //    HealthRecordFilter filterBloodGlucose = new HealthRecordFilter(Microsoft.Health.ItemTypes.BloodGlucose.TypeId);
        //    filterBloodGlucose.EffectiveDateMin = DateTime.Now.AddDays(iLastNDays);
        //    filterBloodGlucose.EffectiveDateMax = DateTime.Now;
        //    searcher.Filters.Add(filterBloodGlucose);

        //    HealthRecordFilter filterBloodPressure = new HealthRecordFilter(Microsoft.Health.ItemTypes.BloodPressure.TypeId);
        //    filterBloodPressure.EffectiveDateMin = DateTime.Now.AddDays(iLastNDays);
        //    filterBloodPressure.EffectiveDateMax = DateTime.Now;
        //    searcher.Filters.Add(filterBloodPressure);

        //    HealthRecordFilter filterWeight = new HealthRecordFilter(Weight.TypeId);
        //    filterWeight.EffectiveDateMin = DateTime.Now.AddDays(iLastNDays);
        //    filterWeight.EffectiveDateMax = DateTime.Now;
        //    searcher.Filters.Add(filterWeight);

        //    HealthRecordFilter filterC = new HealthRecordFilter(CholesterolProfile.TypeId);
        //    filterC.EffectiveDateMin = DateTime.Now.AddDays(iLastNDays);
        //    filterC.EffectiveDateMax = DateTime.Now;
        //    searcher.Filters.Add(filterC);

        //    HealthRecordFilter filterBasic = new HealthRecordFilter(BasicV2.TypeId);
        //    searcher.Filters.Add(filterBasic);

        //    HealthRecordFilter filterPersonal = new HealthRecordFilter(Personal.TypeId);
        //    searcher.Filters.Add(filterPersonal);

        //    HealthRecordFilter filterHeight = new HealthRecordFilter(Height.TypeId);
        //    filterHeight.EffectiveDateMin = DateTime.MinValue;
        //    filterHeight.EffectiveDateMax = DateTime.MaxValue;
        //    filterHeight.MaxItemsReturned = 1;
        //    searcher.Filters.Add(filterHeight);


        //    ReadOnlyCollection<HealthRecordItemCollection> allResults = searcher.GetMatchingItems();

        //    Dictionary<Guid, List<HealthRecordItem>> dictItems = new Dictionary<Guid, List<HealthRecordItem>>();

        //    foreach (HealthRecordItemCollection coll in allResults)
        //    {
        //        foreach (HealthRecordItem item in coll)
        //        {
        //            if (!dictItems.ContainsKey(item.TypeId))
        //            {
        //                dictItems.Add(item.TypeId, new List<HealthRecordItem> { item });
        //            }
        //            else
        //            {
        //                dictItems[item.TypeId].Add(item);
        //            }
        //        }
        //    }

        //    return dictItems;
        //}

        public static Dictionary<Guid, List<HealthRecordItem>> GetPatientDataFromHVBasedOnFilter(List<HealthRecordFilter> filterList, Guid hvpersonid, Guid hvrecordid)
        {
            if (filterList.Count == 0)
                return new Dictionary<Guid, List<HealthRecordItem>>();

            HealthRecordAccessor accessor = GetAccessor(hvpersonid, hvrecordid);
            HealthRecordSearcher searcher = accessor.CreateSearcher();

            foreach (HealthRecordFilter filter in filterList)
            {
                searcher.Filters.Add(filter);
            }

            ReadOnlyCollection<HealthRecordItemCollection> allResults = searcher.GetMatchingItems();

            Dictionary<Guid, List<HealthRecordItem>> dictItems = new Dictionary<Guid, List<HealthRecordItem>>();

            foreach (HealthRecordItemCollection coll in allResults)
            {
                foreach (HealthRecordItem item in coll)
                {
                    if (!dictItems.ContainsKey(item.TypeId))
                    {
                        dictItems.Add(item.TypeId, new List<HealthRecordItem> { item });
                    }
                    else
                    {
                        dictItems[item.TypeId].Add(item);
                    }
                }
            }

            return dictItems;
        }

        public static List<HealthRecordFilter> GetAllHVFiltersForGroupRuleList(List<PatientGroup> listPatientGroup)
        {
            List<HealthRecordFilter> filterList = new List<HealthRecordFilter>();

            Dictionary<Guid, List<Rule>> dictRule = new Dictionary<Guid, List<Rule>>();

            bool bHasAgeRule = false;
            bool bHasGenderRule = false;
            bool bHasBMIRule = false;

            foreach (PatientGroup objPatientGroup in listPatientGroup)
            {
                GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objPatientGroup.GroupRuleXml);
                if (objGroupRule != null)
                {
                    if (objGroupRule.Age != null && objGroupRule.Age.IsAbove.HasValue)
                    {
                        bHasAgeRule = true;
                    }

                    if (objGroupRule.Gender != null && objGroupRule.Gender.IsMale.HasValue)
                    {
                        bHasGenderRule = true;
                    }

                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.BMI)
                        {
                            bHasBMIRule = true;
                        }
                        Guid guid = _GetTypeIdBasedOnRuleType(objRule.RuleType);
                        if (guid == Guid.Empty)
                            continue;
                        if (!dictRule.ContainsKey(guid))
                        {
                            dictRule.Add(guid, new List<Rule> { objRule });
                        }
                        else
                        {
                            dictRule[guid].Add(objRule);
                        }
                    }
                }
            }

            foreach (Guid guid in dictRule.Keys)
            {
                if (guid == Guid.Empty)
                    continue;

                DateTime dtEffectiveMin = DateTime.MaxValue;
                int? iLastNReadings = null;

                HealthRecordFilter filter = new HealthRecordFilter(guid);
                
                foreach (Rule objRule in dictRule[guid])
                {
                    CommonRuleManager objRuleManager = new CommonRuleManager(objRule);
                    if (objRuleManager.DateFrom < dtEffectiveMin)
                    {
                        dtEffectiveMin = objRuleManager.DateFrom;
                    }

                    if (!iLastNReadings.HasValue && objRule.LastNReadings.HasValue)
                    {
                        iLastNReadings = objRule.LastNReadings;
                    }

                    if (iLastNReadings.HasValue
                            && objRule.LastNReadings.HasValue
                            && objRule.LastNReadings > iLastNReadings.Value)
                    {
                        iLastNReadings = objRule.LastNReadings;
                    }
                }

                if (dtEffectiveMin == DateTime.MaxValue)
                {
                    dtEffectiveMin = DateTime.MinValue;
                }

                if (dictRule[guid].Where(r => !r.LastNReadings.HasValue).FirstOrDefault() != null)
                {
                    iLastNReadings = null;
                }
                else
                {
                    filter.MaxItemsReturned = iLastNReadings.Value;
                }

                filter.EffectiveDateMin = dtEffectiveMin;
                filter.EffectiveDateMax = DateTime.Now.AddDays(1);

                filterList.Add(filter);
            }

            if (bHasAgeRule)
            {
                HealthRecordFilter filterPersonal = new HealthRecordFilter(Personal.TypeId);
                filterList.Add(filterPersonal);
            }

            if (bHasGenderRule)
            {
                HealthRecordFilter filterBasic = new HealthRecordFilter(BasicV2.TypeId);
                filterList.Add(filterBasic);
            }

            if (bHasBMIRule)
            {
                HealthRecordFilter filterHeight = new HealthRecordFilter(Height.TypeId);
                filterHeight.EffectiveDateMin = DateTime.MinValue;
                filterHeight.EffectiveDateMax = DateTime.MaxValue;
                filterHeight.MaxItemsReturned = 1;
                filterList.Add(filterHeight);
            }

            return filterList;
        }

        private static Guid _GetTypeIdBasedOnRuleType(RuleType ruleType)
        {
            switch (ruleType)
            {
                case RuleType.Systolic:
                case RuleType.Diastolic:
                    return Microsoft.Health.ItemTypes.BloodPressure.TypeId;

                case RuleType.TotalCholesterol:
                case RuleType.HDL:
                case RuleType.LDL:
                case RuleType.Triglyceride:
                    return Microsoft.Health.ItemTypes.CholesterolProfileV2.TypeId;

                case RuleType.BloodGlucose:
                    return Microsoft.Health.ItemTypes.BloodGlucose.TypeId;

                case RuleType.Weight:
                case RuleType.BMI:
                    return Microsoft.Health.ItemTypes.Weight.TypeId;

                case RuleType.ActivePatient:
                    return Guid.Empty;
                case RuleType.InactivePatient:
                    return Guid.Empty;
                case RuleType.PatientConnection:
                    return Guid.Empty;
                default:
                    return Guid.Empty;
            }
        }
    }
}
