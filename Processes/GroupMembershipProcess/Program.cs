﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.Health;
using GroupManager;
using AHAHelpContent;
using AHACommon;
using GRCBase;


namespace GroupMembershipProcess
{
    class Program
    {
        static LogHelper logger = null;

        public static void LOG(string str)
        {
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        static void Main(string[] args)
        {
            /**********************************************/
            //Proc_GetPatientsWithProviderGroupRules
            //Proc_FindGroupsWithProviderGroupRulesForPatient(int iPatientID)
            //foreach patient
            //for each group, get patient id list, add to group
            /**********************************************/
            string strDailyGroupMembershipProcessErrorEmail = GRCBase.ConfigReader.GetValue("ProcessErrorEmail");

            try
            {
                logger = new LogHelper("DailyGroupMembershipProcess");
                LOG(string.Format("****Start DailyGroupMembershipProcess Process at - {0}****", DateTime.Now.ToString()));

                LOG(string.Format("****Start initialization of Health Vault Wrapper at - {0}****", DateTime.Now.ToString()));
                // This initializes our use of Health Vault. The Group Manager project uses this
                HVWrapper.LifeCycle.Initialize();
                LOG(string.Format("****End initialization of Health Vault Wrapper at -  {0}****", DateTime.Now.ToString()));


                LOG(string.Format("****Start fetch patient list at - {0}****", DateTime.Now.ToString()));
                List<Patient> listPatient = AHAHelpContent.Patient.FindPatientsWithProviderGroupRules();
                LOG(string.Format("****End fetch patient list -  {0}(count) at - {1}****", listPatient.Count, DateTime.Now.ToString()));

                //int iLastNDays = 60;

                Dictionary<int, GroupInfo> finalList = new Dictionary<int, GroupInfo>();

                foreach (Patient objPatient in listPatient)
                {
                    try
                    {
                        LOG(string.Format("****Start fetch group list for patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                        List<PatientGroup> listPatientGroup = AHAHelpContent.PatientGroup.FindGroupsWithProviderGroupRulesForPatient(objPatient.UserHealthRecordID);
                        LOG(string.Format("****End fetch group list -  {0}(count) for patient {1} at - {2}****", listPatientGroup.Count, objPatient.UserHealthRecordID, DateTime.Now.ToString()));

                        if (listPatientGroup.Count == 0)
                            continue;

                        LOG(string.Format("****Start fetch hv data for patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));
                        //Dictionary<Guid, List<HealthRecordItem>> allPatientResults = HVHelper.GetAllValuesForLastNDays(iLastNDays, objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                        

                        List<HealthRecordFilter> filterList = HVHelper.GetAllHVFiltersForGroupRuleList(listPatientGroup);
                        Dictionary<Guid, List<HealthRecordItem>> allPatientResults = HVHelper.GetPatientDataFromHVBasedOnFilter(filterList, objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                        //break;
                        foreach (PatientGroup objPatientGroup in listPatientGroup)
                        {
                            GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objPatientGroup.GroupRuleXml);
                            if (objGroupRule != null)
                            {
                                if (!finalList.ContainsKey(objPatientGroup.GroupID))
                                {
                                    finalList.Add(objPatientGroup.GroupID, new GroupInfo { ProviderID = objPatientGroup.ProviderID, PatientList = new List<int>(), RemoveOption = objPatientGroup.RemoveOption });
                                }
                                objPatient.ProviderConnectionDate = objPatientGroup.ProviderConnectionDate;
                                bool bDoesQualify = GroupRuleManager.DoesPatientQualifyForRule(objPatient, allPatientResults, objGroupRule);
                                if (bDoesQualify)
                                {
                                    finalList[objPatientGroup.GroupID].PatientList.Add(objPatient.UserHealthRecordID);
                                }
                            }
                        }
                        LOG(string.Format("****Start fetch hv data for patient {0} at - {1}****", objPatient.UserHealthRecordID, DateTime.Now.ToString()));

                    }
                    catch (Exception ex)
                    {
                        Microsoft.Health.HealthServiceException hse = ex as Microsoft.Health.HealthServiceException;
                        if (hse != null
                            && (hse.ErrorCode == HealthServiceStatusCode.InvalidRecordState 
                            || hse.ErrorCode == HealthServiceStatusCode.AccessDenied
                            || hse.ErrorCode == HealthServiceStatusCode.InvalidPersonOrGroupId))
                        {
                            //to trap
                            //The operation cannot be performed on the specified record because it is not active.
                            //don't do anything
                        }
                        else
                        {
                            string MailBody = "An error occurred while processing patient " + objPatient.UserHealthRecordID + "  for daily group membership process.";
                            MailBody += "\r\n\r\n";
                            MailBody += ex.Message;
                            MailBody += "\r\n\r\n";
                            MailBody += ex.StackTrace;

                            LOG(MailBody);

                            //GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.AHAAppSettings.SystemEmail, strDailyGroupMembershipProcessErrorEmail, "Error processing patient " + objPatient.UserHealthRecordID + " the Daily group membership process", MailBody, false);
                        }
                    }
                }

                DBContext dbContext = null;
                bool bException = false;
                try
                {
                    dbContext = DBContext.GetDBContext();
                    //add patient to group
                    LOG(string.Format("****Start add patient to group (total groups - {0}) process at - {1}****", finalList.Keys.Count, DateTime.Now.ToString()));
                    foreach (int key in finalList.Keys)
                    {
                        if (finalList[key].RemoveOption == PatientGroup.GroupPatientRemoveOption.A)
                        {
                            StringBuilder sb = new StringBuilder();

                            sb.Append("****Option = A\tPatientList = " + string.Join(",", finalList[key].PatientList.Select(i => i.ToString()).ToArray()));
                            sb.Append("\tGroupID = " + key);
                            sb.Append("\tProviderID = " + finalList[key].ProviderID + "\t");
                            LOG(string.Format("{0} at - {1}****", sb.ToString(), DateTime.Now.ToString()));
                            AHAHelpContent.PatientGroup.AddPatientToGroup(finalList[key].PatientList, key, finalList[key].ProviderID);
                        }
                        else if (finalList[key].RemoveOption == PatientGroup.GroupPatientRemoveOption.D && finalList[key].PatientList != null && finalList[key].PatientList.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();

                            sb.Append("****Option = A\tPatientList = " + string.Join(",", finalList[key].PatientList.Select(i => i.ToString()).ToArray()));
                            sb.Append("\tGroupID = " + key);
                            sb.Append("\tProviderID = " + finalList[key].ProviderID + "\t");
                            LOG(string.Format("{0} at - {1}****", sb.ToString(), DateTime.Now.ToString()));
                            AHAHelpContent.PatientGroup.AddPatientToGroup(finalList[key].PatientList, new List<int> { key }, finalList[key].ProviderID);
                        }
                    }
                    LOG(string.Format("****End add patient group process at - {0}****", DateTime.Now.ToString()));
                }
                catch (System.Exception e)
                {
                    bException = true;
                    string MailBody = "An error occurred while running daily group memebrship process (send mail process). Please run the process again.";
                    MailBody += "\r\n\r\n";
                    MailBody += e.Message;
                    MailBody += "\r\n\r\n";
                    MailBody += e.StackTrace;

                    GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.AHAAppSettings.SystemEmail, strDailyGroupMembershipProcessErrorEmail, "Error during the Daily group membership process", MailBody, false);

                    LOG(MailBody);

                    throw;
                }
                finally
                {
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }

                LOG(string.Format("****End DailyGroupMembershipProcess Process at - {0}****", DateTime.Now.ToString()));
            }
            catch (System.Exception e)
            {
                string MailBody = "An error occurred while running daily group memebrship process (send mail process). Please run the process again.";
                MailBody += "\r\n\r\n";
                MailBody += e.Message;
                MailBody += "\r\n\r\n";
                MailBody += e.StackTrace;

                LOG(MailBody);

                GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.AHAAppSettings.SystemEmail, strDailyGroupMembershipProcessErrorEmail, "Error during the Daily group membership process", MailBody, false);

                throw;
            }
        }
    }
}
