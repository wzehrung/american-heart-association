﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AHACommon;

namespace EmailListGenerationProcess
{
    public class CSVHelper
    {
        private static string _FormatString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            else if (str.IndexOf(",") == -1)
                return str;
            else

                return string.Format("\"{0}\"", str);
        }

        private static void _CreateCSV(LogHelper logger, string strFolderName, string strFileName, string strInfo)
        {
            if (!AHACommon.Helper.DoesCurrentUserHaveFullAccess(false, AHACommon.AHAAppSettings.EmailListDownloadPath))
            {
                Helper.LOG(logger, string.Format("Current user does not have full control on the directory {0}", AHACommon.AHAAppSettings.EmailListDownloadPath));
                return;
            }

            string strDirPath = string.Format("{0}\\{1}", AHACommon.AHAAppSettings.EmailListDownloadPath, strFolderName);
            if (!Directory.Exists(strDirPath))
            {
                Directory.CreateDirectory(strDirPath);
            }

            string strFilePath = string.Format("{0}\\{1}.csv", strDirPath, strFileName);

            // Delete the file if it exists.
            if (File.Exists(strFilePath))
            {
                File.Delete(strFilePath);
            }

            // Create the file.
            using (FileStream fs = File.Create(strFilePath, 1024))
            {
                Byte[] byteInfo = new UTF8Encoding(true).GetBytes(strInfo);
                // Add some information to the file.
                fs.Write(byteInfo, 0, byteInfo.Length);
            }
        }

        public static void ProcessAndCreateCSV(LogHelper logger, string strFolderName, string strFileName, string strEmailList)
        {
            _CreateCSV(logger, strFolderName, strFileName, strEmailList);
        }
    }
}
