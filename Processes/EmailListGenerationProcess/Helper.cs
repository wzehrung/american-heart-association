﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.AccessControl;
using AHACommon;

namespace EmailListGenerationProcess
{
    public class Helper
    {
        public static void LOG(LogHelper logger, string str)
        {
            Console.WriteLine(str);
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

    }
}
