﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AHACommon;
using AHAHelpContent;
using System.Security.AccessControl;

namespace EmailListGenerationProcess
{
    class Program
    {
        static LogHelper logger = null;

        static void Main(string[] args)
        {
            try
            {
                string strEmailListDownloadPath = AHACommon.AHAAppSettings.EmailListDownloadPath;
                logger = new LogHelper("EmailListProcess");
                if (!AHACommon.Helper.DoesCurrentUserHaveFullAccess(false, strEmailListDownloadPath))
                {
                    Helper.LOG(logger, string.Format("Current user -{0}\\{1} does not have full control on the directory {2}", Environment.UserDomainName, Environment.UserName, strEmailListDownloadPath));
                    return;
                }

                Helper.LOG(logger,string.Format("****Start initialization of Health Vault Wrapper at - {0}****", DateTime.Now.ToString()));
                // This initializes our use of Health Vault.
                HVWrapper.LifeCycle.Initialize();
                Helper.LOG(logger,string.Format("****End initialization of Health Vault Wrapper at -  {0}****", DateTime.Now.ToString()));

                DateTime dtDate = DateTime.Today.AddDays(-1);

                Helper.LOG(logger, string.Format("****Start EmailListProcess for date {0} at - {1}****", dtDate.ToString(), DateTime.Now.ToString()));

                List<AHAHelpContent.ProviderDetails> listProvider = null;
                List<AHAHelpContent.AHAUser> listAHAEmailUser = null;


                List<EmailScheduler> listEmailScheduler = EmailScheduler.GetAllEmailListSchedulersForDate(dtDate, AHAHelpContent.Language.GetDefaultLanguage().LanguageID, false);

                if (listEmailScheduler.Count == 0)
                {
                    Helper.LOG(logger, string.Format("****No EmailListProcess scheduled for date {0} at - {1}****", dtDate.ToString(), DateTime.Now.ToString()));
                }

                foreach (EmailScheduler objEmailScheduler in listEmailScheduler)
                {
                    Helper.LOG(logger, string.Format("****Start Processing scheduler {0} at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                    StringBuilder sbEmail = new StringBuilder();
                    string strFolderName = string.Format("{0}-{1}-{2}", objEmailScheduler.ScheduledDate.Year, objEmailScheduler.ScheduledDate.Month, objEmailScheduler.ScheduledDate.Day);
                    string strFileName = AHACommon.Helper.GetFileNameForEmailList(objEmailScheduler.IsProviderList, objEmailScheduler.SchedulerID, objEmailScheduler.Title);
                    if (!objEmailScheduler.IsProviderList)
                    {
                        listAHAEmailUser = AHAHelpContent.AHAUser.FindAllUsersForAHAEmails(objEmailScheduler.CampaignID);

                        Helper.LOG(logger, string.Format("****Start fetching emails for scheduler {0} from HV at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                        foreach (AHAHelpContent.AHAUser objUser in listAHAEmailUser)
                        {
                            string strEmail = HVManager.CurrentUserHVSetting.GetEmailForAccount(objUser.PersonGUID, Guid.Empty);
                            string strConnect = AHAHelpContent.AHAUser.FindAllProvidersUserTypeByPatientID(objUser.SelfHealthRecordID);

                            if (!string.IsNullOrEmpty(strEmail))
                            {
                                sbEmail.AppendLine(strEmail + "," + strConnect);
                            }
                        }

                        if (sbEmail != null && sbEmail.Length > 0)
                        {
                            Helper.LOG(logger, string.Format("****Start creating csv for scheduler {0} at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                            CSVHelper.ProcessAndCreateCSV(logger, strFolderName, strFileName, sbEmail.ToString());
                            AHAHelpContent.EmailScheduler.MarkEmailListSchedulerDownloaded(objEmailScheduler.SchedulerID, true);
                            Helper.LOG(logger, string.Format("****End creating csv for scheduler {0} at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                        }
                        else
                        {
                            Helper.LOG(logger, string.Format("****No Emails found for scheduler {0}  at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                        }

                        Helper.LOG(logger, string.Format("****End fetching emails for scheduler {0} from HV at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                        Helper.LOG(logger, string.Format("Patient Count: {0}", listAHAEmailUser.Count));
                    }
                    else
                    {
                        listProvider = AHAHelpContent.ProviderDetails.FindAllProvidersForAHAEmails(null);
                        //if (listProvider != null && listProvider.Count > 0)
                        //{
                        //    sbEmail.Append(string.Join("\n", listProvider.Select(p => p.Email).ToArray()));
                        //    sbEmail.Append(","+listProvider
                        //}

                        foreach (ProviderDetails ps in listProvider)
                        {
                            string strEmail = ps.Email + "," + ps.FirstName + "," + ps.LastName + "," + ps.PracticeName + "," + ps.Campaign + "," + ps.Affiliate + "," + ps.Market;
                            if (!string.IsNullOrEmpty(strEmail))
                            {
                                sbEmail.AppendLine(strEmail);
                            }
                        }
                        if (sbEmail != null && sbEmail.Length > 0)
                        {
                            Helper.LOG(logger, string.Format("****Start creating csv for scheduler {0} at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                            CSVHelper.ProcessAndCreateCSV(logger, strFolderName, strFileName, sbEmail.ToString());
                            AHAHelpContent.EmailScheduler.MarkEmailListSchedulerDownloaded(objEmailScheduler.SchedulerID, true);
                            Helper.LOG(logger, string.Format("****End creating csv for scheduler {0} at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                        }
                        else
                        {
                            Helper.LOG(logger, string.Format("****No Emails found for scheduler {0}  at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                        }
                        Helper.LOG(logger, string.Format("Provider Count:{0}", listProvider.Count));
                    }
                    Helper.LOG(logger, string.Format("****End Processing scheduler {0} at - {1}****", objEmailScheduler.SchedulerID, DateTime.Now.ToString()));
                }

                Helper.LOG(logger, string.Format("****End EmailListProcess for date {0} at - {1}****", dtDate.ToString(), DateTime.Now.ToString()));
            }
            catch (System.Exception e)
            {
                string MailBody = "An error occurred while running EmailListProcess. Please run the process again";
                MailBody += "\r\n\r\n";
                MailBody += e.Message;
                MailBody += "\r\n\r\n";
                MailBody += e.StackTrace;

                Helper.LOG(logger, MailBody);

                throw;
            }            //Console.ReadLine();
        }
    }
}
