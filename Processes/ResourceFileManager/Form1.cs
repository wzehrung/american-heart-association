﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO ;
using System.Text.RegularExpressions;

namespace ResourceFileManager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.Text = "Resource File Manager";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = "Specified folder or file does not contain a resource file.";

            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                if (Directory.Exists(textBox1.Text))
                {
                    processFolder();
                }
                else if (File.Exists(textBox1.Text))
                {



                }
            }
        }

            // This is the button to open the folder browser dialog
        private void button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
                folderBrowserDialog1.SelectedPath = textBox1.Text;

            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }

        }

        private void processFolder()
        {
            string[] files = Directory.GetFiles(textBox1.Text);

            textBox2.Text = string.Empty ;

            foreach (string s in files)
            {
                if (s.EndsWith(".resx"))
                {
                    System.Resources.ResXResourceReader reader = new System.Resources.ResXResourceReader(s);
                    int keycount = 0;
                    int wordcount = 0;
                    foreach (System.Collections.DictionaryEntry de in reader)
                    {
                        keycount++;

                        MatchCollection collection = Regex.Matches(de.Value.ToString(), @"[\S]+");
                        wordcount += collection.Count;
                        /**
                        if (((string)de.Key).EndsWith(".Text"))
                        {
                            System.Diagnostics.Debug.WriteLine(string.Format("{0}: {1}", de.Key, de.Value));
                        }
                         */
                    }

                    textBox2.Text += s + " = " + keycount.ToString() + " " + wordcount.ToString() + Environment.NewLine + Environment.NewLine ;
                }
            }
        }

    }
}
