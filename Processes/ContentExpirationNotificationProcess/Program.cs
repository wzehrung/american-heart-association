﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;
using AHACommon;

namespace ContentExpirationNotificationProcess
{
    class Program
    {
        static LogHelper logger = null;

        public static void LOG(string str)
        {
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        static void Main(string[] args)
        {
            string strContentExpNotificationErrorEmail = GRCBase.ConfigReader.GetValue("ProcessErrorEmail");
            int daysOffset = Convert.ToInt32(GRCBase.ConfigReader.GetValue("DaysBefore_ContentExpirationNotification"));
            DateTime dtProcessRunForDate = DateTime.Today.AddDays( daysOffset );
            //string strContent = string.Empty;
            StringBuilder sbContent = new StringBuilder();

            try
            {
                logger = new LogHelper("ContentExpirationNotificationMailer");
                
                LOG(string.Format("****Start Content Expiration Notification Process for Date - {0} at - {1}****", dtProcessRunForDate.ToString(), DateTime.Now.ToString()));

                LOG(string.Format("****Start Fetching list of contents expiring on - {0} at - {1}****", dtProcessRunForDate.ToString(), DateTime.Now.ToString()));
                List<AHAHelpContent.Content> listContent = AHAHelpContent.Content.GetContentsExpiringOn(dtProcessRunForDate);
                LOG(string.Format("****End Fetching list of contents -  {0}(count) contents expires on - {1} at - {2}****", listContent.Count, dtProcessRunForDate.ToString(), DateTime.Now.ToString()));

                LOG(string.Format("****Start Fetching list of resources expiring on - {0} at - {1}****", dtProcessRunForDate.ToString(), DateTime.Now.ToString()));
                List<AHAHelpContent.Resource> listResource = AHAHelpContent.Resource.FindAllResourcesExpiringOn(dtProcessRunForDate);
                LOG(string.Format("****End Fetching list of resources -  {0}(count) resources expires on - {1} at - {2}****", listResource.Count, dtProcessRunForDate.ToString(), DateTime.Now.ToString()));

                LOG(string.Format("****Start Fetching list of admin users at - {0}****", DateTime.Now.ToString()));
                List<AHAHelpContent.AdminUser> listAdminUsers = AHAHelpContent.AdminUser.GetAllActiveAdminUsers();
                LOG(string.Format("****End Fetching list of admin users -  {0}(count) users at - {1}****", listAdminUsers.Count, DateTime.Now.ToString()));

                if (listContent.Count > 0)
                {
                    sbContent.Append(string.Format("Below are the list of contents expiring on {0}: <br /><br />", dtProcessRunForDate.ToShortDateString()));
                    
                    foreach (AHAHelpContent.Content objContent in listContent)
                    {
                        sbContent.Append(objContent.Title + "<br />");
                    }
                    sbContent.Append("<br /><br /><br />");
                }

                if (listResource.Count > 0)
                {
                    sbContent.Append(string.Format("Below are the list of resources expiring on {0}: <br /><br />", dtProcessRunForDate.ToShortDateString()));

                    foreach (AHAHelpContent.Resource objResource in listResource)
                    {
                        sbContent.Append(objResource.ContentTitle + "<br />");
                    }
                    sbContent.Append("<br />");
                }

                if ((listContent.Count > 0 || listResource.Count > 0) && listAdminUsers.Count > 0)
                {
                    foreach (AHAHelpContent.AdminUser objUser in listAdminUsers)
                    {
                        try
                        {
                            LOG(string.Format("****Start Sending content expiration notification to user {0} at - {1}****", objUser.Email, DateTime.Now.ToString()));
                            EmailHelper.SendContentExpirationNotificationToAdmin(objUser.Name, objUser.Email, sbContent.ToString(), dtProcessRunForDate, Locale.English);
                            LOG(string.Format("****End Sending content expiration notification to user {0} at - {1}****", objUser.Email, DateTime.Now.ToString()));
                        }
                        catch (System.Exception e)
                        {
                            string MailBody = "An error occurred while sending content expiration notification to admin user id - " + objUser.AdminUserID;
                            MailBody += "\r\n\r\n";
                            MailBody += e.Message;
                            MailBody += "\r\n\r\n";
                            MailBody += e.StackTrace;

                            GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, strContentExpNotificationErrorEmail, "Error occurred while sending content expiration notification email to admin user", MailBody, false);

                            LOG(MailBody);
                        }
                    }
                    LOG(string.Format("****End Content Expiration Notification Process for Date - {0} at - {1}****", dtProcessRunForDate.ToString(), DateTime.Now.ToString()));
                }
            }
            catch (System.Exception e)
            {
                string MailBody = "An error occurred while running content expiration notification mailer (send mail process). Please run the process again";
                MailBody += "\r\n\r\n";
                MailBody += e.Message;
                MailBody += "\r\n\r\n";
                MailBody += e.StackTrace;

                GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, strContentExpNotificationErrorEmail, "Error during the content expiration notification mailer process", MailBody, false);

                LOG(MailBody);
            }
        }
    }
}
