﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using AHACommon;

namespace RxTermUpdateProcess
{
    class Program
    {
        static LogHelper logger = null;

        public static void LOG(string str)
        {
            logger.WriteTrace(LogHelper.MailLogTraceLevel.ERROR, str);
        }

        static void Main(string[] args)
        {
            string strRxTermUpdateProcessErrorEmail = GRCBase.ConfigReader.GetValue("ProcessErrorEmail");

            try
            {
                logger = new LogHelper("RxTermUpdateProcess");

                LOG(string.Format("****Starting RxTermUpdateProcess at - {0}****", DateTime.Now.ToString()));

                using (DBManagerService db = new DBManagerService())
                {
                    LOG(string.Format("****Start Fetching RxTerms from NIH at - {0}****", DateTime.Now.ToString()));
                    string[] RxTermList = db.getDisplayTerms();
                    int iCount = RxTermList.Count();
                    LOG(string.Format("****End Fetching RxTerms ({0})from NIH at - {1}****", iCount, DateTime.Now.ToString()));
                    if (iCount > 0)
                    {
                        string str = string.Empty;
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            try
                            {
                                XmlSerializer serializer = new XmlSerializer(RxTermList.GetType(), new XmlRootAttribute("RxTermList"));
                                serializer.Serialize(memoryStream, RxTermList);
                                string strXmlizedString = _UTF8ByteArrayToString(memoryStream.ToArray());
                                LOG(string.Format("****Start Updating RxTerms in local DB at - {0}****", DateTime.Now.ToString()));
                                AHAHelpContent.RxTerm.UpdateRxTerms(strXmlizedString);
                                LOG(string.Format("****End Updating RxTerms in local DB at - {0}****", DateTime.Now.ToString()));
                            }
                            catch
                            {
                                throw;
                            }
                            finally
                            {
                                memoryStream.Close();
                                memoryStream.Dispose();
                            }
                        }
                    }
                }

                LOG(string.Format("****Ending RxTermUpdateProcess at - {0}****", DateTime.Now.ToString()));
            }
            catch (System.Exception e)
            {
                string MailBody = "An error occurred while running RxTermUpdateProcess. Please run the process again";
                MailBody += "\r\n\r\n";
                MailBody += e.Message;
                MailBody += "\r\n\r\n";
                MailBody += e.StackTrace;
                if (e.InnerException != null)
                {
                    if (!string.IsNullOrEmpty(e.InnerException.Message))
                    {
                        MailBody += "\r\n\r\n";
                        MailBody += e.InnerException.Message;
                    }
                    if (!string.IsNullOrEmpty(e.InnerException.StackTrace))
                    {
                        MailBody += "\r\n\r\n";
                        MailBody += e.InnerException.StackTrace;
                    }
                }

                GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, AHACommon.MinimumAppSettings.SystemEmail, strRxTermUpdateProcessErrorEmail, "Error during the RxTermUpdateProcess", MailBody, false);

                LOG(MailBody);
            }
        }

        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        private static String _UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }
    }
}
//try
//{
//    XmlSerializer serializer = new XmlSerializer(RxTermList.GetType(), new XmlRootAttribute("RxTermList"));

//    XmlWriterSettings writerSettings = new XmlWriterSettings();
//    writerSettings.OmitXmlDeclaration = true;
//    StringWriter stringWriter = new StringWriter();
//    using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, writerSettings))
//    {
//        serializer.Serialize(xmlWriter, RxTermList);
//    }

//    LOG(string.Format("****Start Updating RxTerms in local DB at - {0}****", DateTime.Now.ToString()));
//    //AHAHelpContent.RxTerm.UpdateRxTerms(stringWriter.ToString());
//    LOG(string.Format("****End Updating RxTerms in local DB at - {0}****", DateTime.Now.ToString()));
//}
//catch
//{
//    throw;
//}