﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Heart360Web.SignUp" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

	<div id="main-content" class="row">

		<div id="easy-steps" class="full">

			<header class="full spt">

				<div class="small-12 large-9 columns">
					<h2><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_Title")%></h2>
				</div>

				<div class="small-12 large-3 columns">
					<div class="get-started row">
                        <asp:LinkButton ID="btnGetStarted" CssClass="red-button ie-gradient get-started small-10 small-centered large-10 columns" OnClick="CreateAcc_Click" runat="server"><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_GetStarted")%></asp:LinkButton>
					</div>
				</div>

			</header>

			<section class="process full">

				<div id="step-1" class="steps small-12 large-3 columns">
					<div class="process-graphic"></div>
					<div class="process-message">
						<h5><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_SignUp")%></h5>
						<%=GetGlobalResourceObject("Visitor", "Visitor_Steps_SignUp_Text")%>
					</div>
				</div>

				<div id="step-2" class="steps small-12 large-3 columns">
					<div class="process-graphic"></div>
					<div class="process-message">
						<h5><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_Create_HealthVault")%></h5>
						<p><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_Create_HealthVault_Text")%></p>
					</div>
					<footer class="full">
						<a href="http://www.healthvault.com" class="fl"></a>
					</footer>
				</div>

				<div id="step-3" class="steps small-12 large-3 columns">
					<div class="process-graphic"></div>
					<div class="process-message">
						<h5><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_Allow_Heart360")%></h5>
						<p><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_Allow_Heart360_Text")%></p>
					</div>
				</div>

				<div id="step-4" class="steps small-12 large-3 columns">
					<div class="process-graphic"></div>
					<div class="process-message">
						<h5><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_Finish")%></h5>
						<p><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_Finish_Text")%></p>
					</div>
				</div>

			</section>

            <header class="full spt">

				<div class="small-12 large-9 columns">
					&nbsp;
				</div>
            
				<div class="small-12 large-3 columns">
					<div class="get-started row">
                        <asp:LinkButton ID="LinkButton1" CssClass="red-button ie-gradient get-started small-10 small-centered large-10 columns" OnClick="CreateAcc_Click" runat="server"><%=GetGlobalResourceObject("Visitor", "Visitor_Steps_GetStarted")%></asp:LinkButton>
					</div>
				</div>            

            </header>

			<footer class="full">



			</footer>

		</div>

	</div>

    <br class="hide-for-medium-down" />
    <hr class="row hide-for-medium-down spb" />
    <br class="hide-for-medium-down" />

</asp:Content>