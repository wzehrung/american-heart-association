﻿var DivJSPresent = null;
var DivNoJS = null;
var DivStart = null;
var DivTakeReading = null;
var DivProgress = null;
var DivReTakeReading = null;
var DivResult = null;
var spanProgressText = null;
var spanErrorMessage = null;
var spanResultBP = null;
var spanResultPulseRate = null;
var spanResultTime = null;
var hidSystolic = null;
var hidDiastolic = null;
var hidPulseRate = null;
var hidDateTime = null;
var strDisplayNone = "DisplayNone";
var strDisplayBlock = "DisplayBlock";
var __Heart360BpDeviceDriver = null;
var __URLDomain = "http://localhost:8765";
var __URL_GetLatestReading = "{0}/Heart360BPService/GetLatestReading?callback=?";
var __URL_StartMeasurement = "{0}/Heart360BPService/StartMesurement?str={1}&callback=?";
var __URL_IsServiceInstalled = "{0}/Heart360BPService/IsServiceInstalled?callback=?";
var _BPObjects = new Array();
String.prototype.format = function() {
    var pattern = /\{\d+\}/g;
    var args = arguments;
    return this.replace(pattern, function(capture) { return args[capture.match(/\d+/)]; });
}
function GetFormattedDateTimeString(dt) {
    dt = new Date(dt);
    var iDate = dt.getDate();
    var strDate = iDate;
    if (iDate < 10) {
        strDate = "0{0}".format(iDate);
    }

    var iHH = dt.getHours();
    var strHH = iHH;
    if (iHH < 10) {
        strHH = "0{0}".format(iHH);
    }

    var iMM = dt.getMinutes();
    var strMM = iMM;
    if (iMM < 10) {
        strMM = "0{0}".format(iMM);
    }
    
    if (strLocale == "es-MX") {
        return "{0} de {1} de {2}, {3}".format(strDate, monthname[(dt.getMonth() + 1)], dt.getFullYear(), GetFormattedTime(dt));
    }
    else {
        return "{0} {1} {2} {3}".format(monthname[(dt.getMonth() + 1)].substring(0, 3), strDate, dt.getFullYear(), GetFormattedTime(dt));
    }
}
function GetFormattedTime(dtDateTime) {
    hour = dtDateTime.getHours();
    var ap = STR_SMALL_AM;
    if (hour > 11) { ap = STR_SMALL_PM; }
    if (hour > 12) { hour = hour - 12; }
    if (hour == 0) { hour = 12; }

    minute = dtDateTime.getMinutes();
    if (minute < 10) {
        minute = '0' + minute;
    }

    return hour + ":" + minute + " " + ap;
}
function OnLoad() {
    DivStart = document.getElementById("DivStart");
    DivTakeReading = document.getElementById("DivTakeReading");
    DivProgress = document.getElementById("DivProgress");
    DivReTakeReading = document.getElementById("DivReTakeReading");
    DivResult = document.getElementById("DivResult");
    spanProgressText = document.getElementById("spanProgressText");
    spanErrorMessage = document.getElementById("spanErrorMessage");
    spanResultBP = document.getElementById("spanResultBP");
    spanResultPulseRate = document.getElementById("spanResultPulseRate");
    spanResultTime = document.getElementById("spanResultTime");

    DivJSPresent = document.getElementById("DivJSPresent");
    DivNoJS = document.getElementById("DivNoJS");

    DivStart.className = strDisplayBlock;
    __Heart360BpDeviceDriver = new Heart360BpDeviceDriver();

    DivNoJS.style.display = "none";
    DivJSPresent.style.display = "block";

}

function Heart360BpDeviceDriver() {
    this.IsServiceInstalledTimer = null;
    this.IsServiceInstalledStarted = false;
    this.IsServiceInstalledTimerInterval = 1000;
    this.IsServiceInstalledTimerTotalAttempts = 0;
    this.IsServiceInstalledTimerMaxAttempts = 10;

    this.StartMeasurementTimer = null;
    this.StartMeasurementStarted = false;
    this.StartMeasurementTimerInterval = 1000;
    this.StartMeasurementTimerTotalAttempts = 0;
    this.StartMeasurementTimerMaxAttempts = 1;

    this.Systolic = null;
    this.Diastolic = null;
    this.PulseRate = null;
    this.BPDateTime = null;

    this.ProgressTimer = null;
    this.ProgressPollingInMilliSeconds = 100;
    this.FinalProgressPollingInMilliSeconds = 10;
    this.TotalProgressPolling = 0;
    this.MaxTimeTakenForBPReadingInMilliSeconds = 1000 * 70; //55 seconds

    this.WaitTimer = null;
    this.WaitTimerInMilliSeconds = 1000;
    this.WaitSeconds = null;
    this.WaitText = null;
}

Heart360BpDeviceDriver.prototype.ClearAll = function() {
    __Heart360BpDeviceDriver = new Heart360BpDeviceDriver();
    clearTimeout(this.IsServiceInstalledTimer);
    clearTimeout(this.StartMeasurementTimer);
    clearTimeout(this.ProgressTimer);
    clearTimeout(this.WaitTimer);
    hidSystolic.value = "";
    hidDiastolic.value = "";
    hidPulseRate.value = "";
    this.TotalProgressPolling = 0;
    this.WaitSeconds = null;
    this.WaitText = null;
    spanErrorMessage.innerHTML = "";
    this.StartMeasurementTimerTotalAttempts = 0;
    for (var i = 0; i < _BPObjects.length; i++) {
        _BPObjects[i].ClearAllTimers();
    }
    _BPObjects = new Array();
};

Heart360BpDeviceDriver.prototype.ClearAllTimers = function() {
    clearTimeout(this.IsServiceInstalledTimer);
    clearTimeout(this.StartMeasurementTimer);
    clearTimeout(this.ProgressTimer);
    clearTimeout(this.WaitTimer);
    this.TotalProgressPolling = 0;
};

Heart360BpDeviceDriver.prototype.GetProgress = function(milliseconds) {
    var strVal = (100 / this.MaxTimeTakenForBPReadingInMilliSeconds) * milliseconds;
    //alert(strVal);
    return strVal;
};

Heart360BpDeviceDriver.prototype.GetCurrentProgress = function() {
    return parseInt(this.GetProgress(this.TotalProgressPolling * this.ProgressPollingInMilliSeconds));
};

Heart360BpDeviceDriver.prototype.CompleteProgress = function() {
    var oThis = this; 
    this.ProgressTimer = setTimeout(function() {
        oThis.TotalProgressPolling++;
        var iCurrentProgress = oThis.GetCurrentProgress();
        //alert(iCurrentProgress);
        oThis.SetProgressText(strBpProgress.format(iCurrentProgress));
        if (iCurrentProgress >= 100) {
            clearTimeout(oThis.ProgressTimer);
            oThis.TotalProgressPolling = 0;
            oThis.ShowResult();
            return;
        }
        oThis.CompleteProgress();
    }, oThis.FinalProgressPollingInMilliSeconds);
};

Heart360BpDeviceDriver.prototype.InitializeStartWaitTimer = function(txt, iSeconds) {
    this.ClearAll();
    this.HideAllWindows();
    this.SetErrorText(strPleaseWait);
    this.ShowWindow(DivReTakeReading);
    this.WaitText = txt;
    this.StartWaitTimer(iSeconds);
};

Heart360BpDeviceDriver.prototype.StartWaitTimerProperties = function() {
    this.WaitSeconds--;
    if (this.WaitSeconds == 0) {
        clearTimeout(this.WaitTimer);
        this.Start();
        return;
    }
    this.SetErrorText(this.WaitText.format(this.WaitSeconds));
    this.StartWaitTimer(this.WaitSeconds);
};

Heart360BpDeviceDriver.prototype.StartWaitTimer = function(iSeconds) {
    this.WaitSeconds = iSeconds;
    var oThis = this;
    _BPObjects[_BPObjects.length] = oThis;
    this.WaitTimer = setTimeout(function() {
        oThis.StartWaitTimerProperties();
    }, this.WaitTimerInMilliSeconds);
};

Heart360BpDeviceDriver.prototype.StartProgressTimer = function() {
    var oThis = this;
    this.ProgressTimer = setTimeout(function() {
        oThis.TotalProgressPolling++;
        var iCurrentProgress = oThis.GetCurrentProgress();
        oThis.SetProgressText(strBpProgress.format(iCurrentProgress));
        if (iCurrentProgress == 90) {
            clearTimeout(oThis.ProgressTimer);
            return;
        }
        oThis.StartProgressTimer();
    }, oThis.ProgressPollingInMilliSeconds);
};

Heart360BpDeviceDriver.prototype.SaveReading = function() {
    hidSystolic.value = this.Systolic;
    hidDiastolic.value = this.Diastolic;
    hidPulseRate.value = this.PulseRate;
    hidDateTime.value = "{0}|{1}|{2}|{3}|{4}|{5}".format(this.BPDateTime.getFullYear(), (this.BPDateTime.getMonth() + 1), this.BPDateTime.getDate(), this.BPDateTime.getHours(), this.BPDateTime.getMinutes(), this.BPDateTime.getSeconds());
    return true;
};

Heart360BpDeviceDriver.prototype.Start = function() {
    this.ClearAll();
    this.HideAllWindows();
    this.ShowWindow(DivTakeReading);
    //alert(this.WaitTimer);
};

Heart360BpDeviceDriver.prototype.TakeReading = function() {
    this.ClearAll();
    this.HideAllWindows();
    this.ShowWindow(DivProgress);
    this.SetProgressText(strStarting);
    this.StartSetp1();
};

Heart360BpDeviceDriver.prototype.StartSetp1 = function() {
    var oThis = this;
    this.IsServiceInstalledTimer = setTimeout(function() {
        if (oThis.IsServiceInstalledTimerTotalAttempts >= oThis.IsServiceInstalledTimerMaxAttempts) {
            clearTimeout(oThis.IsServiceInstalledTimer);
            oThis.IsServiceInstalledTimerTotalAttempts = 0;
            oThis.ServiceNotInstalled();
            return;
        }
        oThis.IsServiceInstalledTimerTotalAttempts++;
        oThis.SetProgressText(strCheckingServiceAttempt.format(oThis.IsServiceInstalledTimerTotalAttempts));
        oThis.StartIsServiceInstalled();

    }, oThis.IsServiceInstalledTimerInterval);
};

Heart360BpDeviceDriver.prototype.StartIsServiceInstalled = function() {
    var oThis = this;
    clearTimeout(oThis.IsServiceInstalledTimer);
    //alert('hello1');
    $.get(__URL_IsServiceInstalled.format(__URLDomain),
    				{},
    				function(retVal) {
    				    oThis.IsServiceInstalledTimerTotalAttempts = 0;
    				    if (retVal.IsServiceInstalledResult.HasError) {
    				        oThis.AppError(strAppError);
    				        return;
    				    }
    				    else {
    				        oThis.IsServiceInstalledStarted = true;
    				        if (!retVal.IsServiceInstalledResult.IsServiceInstalled) {
    				            oThis.ServiceNotInstalled();
    				            return;
    				        }
    				        else {
    				            if (oThis.StartMeasurementTimerTotalAttempts > 0)
    				                return;
    				            oThis.SetProgressText(strStartConnectingToDevice);
    				            oThis.StartSetp2();
    				            return;
    				        }
    				    }
    				    // make sure to tell jQuery this is a JSONP call
    				}, 'jsonp');

    if (!oThis.IsServiceInstalledStarted) {
        oThis.StartSetp1();
    }
};

Heart360BpDeviceDriver.prototype.ServiceNotInstalled = function() {
    this.ClearAll();
    this.HideAllWindows();
    this.SetErrorText(strServiceNotInstalled);
    this.ShowWindow(DivReTakeReading);
};

Heart360BpDeviceDriver.prototype.StartSetp2 = function() {
    var oThis = this;
    clearTimeout(oThis.StartMeasurementTimer);
    this.StartMeasurementTimer = setTimeout(function() {
        oThis.StartMeasurementTimerTotalAttempts++;
        oThis.StartMesurement();

    }, oThis.StartMeasurementTimerInterval);
};

Heart360BpDeviceDriver.prototype.StartMesurement = function() {
    this.ClearAllTimers();
    this.TotalProgressPolling = 0;
    this.WaitText = null;
    spanErrorMessage.innerHTML = "";
    this.StartProgressTimer();
    if (!this.StartMeasurementStarted) {
        this.SetProgressText(strConnectingToDeviceAttempt.format(this.StartMeasurementTimerTotalAttempts));
    }
    var oThis = this;
    //alert('hello2');a
    //alert(__URL_StartMeasurement);
    $.get(__URL_StartMeasurement.format(__URLDomain,strLocale),
    				{},
    				function(retVal) {
    				    //alert(retVal.StartMesurementResult.Status);
    				    oThis.StartMeasurementStarted = true;
    				    if (retVal.StartMesurementResult.HasError) {
    				        oThis.AppError(strAppError);
    				        return;
    				    }
    				    else {
    				        oThis.StartMeasurementTimerTotalAttempts = 0;
    				        if (retVal.StartMesurementResult.Status == "0") {
    				            oThis.DeviceNotDetected();
    				            return;
    				        }
    				        else if (retVal.StartMesurementResult.Status == "1") {
    				            oThis.WaitForDeviceToGetReady(parseInt(retVal.StartMesurementResult.ProposedWaitSeconds));
    				            return;
    				        }
    				        else if (retVal.StartMesurementResult.Status == "2") {
    				            oThis.UnexpectedError(parseInt(retVal.StartMesurementResult.ProposedWaitSeconds));
    				            return;
    				        }
    				        else if (retVal.StartMesurementResult.Status == "3") {
    				            oThis.MeasurementError(parseInt(retVal.StartMesurementResult.ProposedWaitSeconds), retVal.StartMesurementResult.MesurementErrorSubCodeDescription);
    				            return;
    				        }
    				        else if (retVal.StartMesurementResult.Status == "4") {
    				            oThis.Systolic = retVal.StartMesurementResult.Systolic;
    				            oThis.Diastolic = retVal.StartMesurementResult.Diastolic;
    				            oThis.PulseRate = retVal.StartMesurementResult.PulseRate;
    				            oThis.BPDateTime = new Date();
    				            __Heart360BpDeviceDriver.Systolic = retVal.StartMesurementResult.Systolic;
    				            __Heart360BpDeviceDriver.Diastolic = retVal.StartMesurementResult.Diastolic;
    				            __Heart360BpDeviceDriver.PulseRate = retVal.StartMesurementResult.PulseRate;
    				            __Heart360BpDeviceDriver.BPDateTime = new Date();
    				            clearTimeout(oThis.ProgressTimer);
    				            oThis.CompleteProgress();
    				            return;
    				        }
    				    }
    				    // make sure to tell jQuery this is a JSONP call
    				}, 'jsonp');
};

Heart360BpDeviceDriver.prototype.WaitForDeviceToGetReady = function(iSeconds) {
    this.InitializeStartWaitTimer(strDeviceNotReady, iSeconds);
};

Heart360BpDeviceDriver.prototype.UnexpectedError = function(iSeconds) {
    this.InitializeStartWaitTimer(strUnExpectedError, iSeconds);
};

Heart360BpDeviceDriver.prototype.MeasurementError = function(iSeconds, strError) {
    var strErrMsg = strWaitForNSeconds;
    if (strError.length > 0) {
        strError = strErrorWhileTakingMeasurementWithMsg.format(strError);
    }
    else {
        strError = strErrorWhileTakingMeasurement;
    }
    this.InitializeStartWaitTimer(strError + strErrMsg, iSeconds);
};

Heart360BpDeviceDriver.prototype.DeviceNotDetected = function() {
    this.ClearAll();
    this.HideAllWindows();
    this.ShowWindow(DivReTakeReading);
    this.SetErrorText(strDeviceNotDetected);
};

Heart360BpDeviceDriver.prototype.StartMeasurementAttemptOverFlow = function() {
    this.ClearAll();
    this.HideAllWindows();
    this.ShowWindow(DivReTakeReading);
    this.SetErrorText(strMonitorServiceTimeout);
};

Heart360BpDeviceDriver.prototype.ShowResult = function() {
    this.HideAllWindows();
    this.ShowWindow(DivResult);
    spanResultBP.innerHTML = "<span class=\"{0}\">{1}</span>/<span class=\"{2}\">{3}</span>".format(this.GetColorClass(this.Systolic, true), this.Systolic, this.GetColorClass(this.Diastolic, false), this.Diastolic);
    spanResultPulseRate.innerHTML = this.PulseRate;
    spanResultTime.innerHTML = GetFormattedDateTimeString(this.BPDateTime);
    this.SetProgressText('');
    clearTimeout(this.IsServiceInstalledTimer);
    clearTimeout(this.StartMeasurementTimer);
    clearTimeout(this.ProgressTimer);
};

/*
63C022 - green
FEFB67 - yellow
F3A531 - orange
EB672A - orange-red
D83023 - red
*/
Heart360BpDeviceDriver.prototype.GetColorClass = function(iValue, bIsSystolic) {
    if (bIsSystolic) {
        if (iValue < 120) {
            return "bp-mon-green";
        }
        else if (iValue >= 120 && iValue <= 139) {
            return "bp-mon-yellow";
        }
        else if (iValue >= 140 && iValue <= 159) {
            return "bp-mon-orange";
        }
        else if (iValue >= 160 && iValue <= 180) {
            return "bp-mon-orange-red";
        }
        else {
            return "bp-mon-red";
        }
    }
    else {
        if (iValue < 80) {
            return "bp-mon-green";
        }
        else if (iValue >= 80 && iValue <= 89) {
            return "bp-mon-yellow";
        }
        else if (iValue >= 90 && iValue <= 99) {
            return "bp-mon-orange";
        }
        else if (iValue >= 100 && iValue <= 110) {
            return "bp-mon-orange-red";
        }
        else {
            return "bp-mon-red";
        }
    }
};

Heart360BpDeviceDriver.prototype.SetProgressText = function(str) {
    spanProgressText.innerHTML = str;
};

Heart360BpDeviceDriver.prototype.SetErrorText = function(str) {
    spanErrorMessage.innerHTML = str;
};

Heart360BpDeviceDriver.prototype.AppError = function(str) {
    this.HideAllWindows();
    this.ShowWindow(DivReTakeReading);
    this.SetErrorText(str);
    this.ClearAll();
};

Heart360BpDeviceDriver.prototype.ShowWindow = function(div) {
    div.className = strDisplayBlock;
};

Heart360BpDeviceDriver.prototype.HideAllWindows = function() {
    DivStart.className = strDisplayNone;
    DivTakeReading.className = strDisplayNone;
    DivProgress.className = strDisplayNone;
    DivReTakeReading.className = strDisplayNone;
    DivResult.className = strDisplayNone;
};