﻿var strAppError = "Parece que hay un problema, ¡realice la lectura nuevamente!";
var strBpProgress = "La lectura de la presión arterial se ha completado en un {0} por ciento"; //"{0} de la lectura de la presión arterial completa";
var strStarting = "Comenzar...";
var strPleaseWait = "Espere...";
var strCheckingServiceAttempt = "Verificando servicio...intento ({0})";
var strStartConnectingToDevice = "Conectando al dispositivo...";
var strServiceNotInstalled = "El servicio de monitorización de la presión arterial de Heart360 no está funcionando, asegúrese de que el servicio esté funcionando y realice la lectura nuevamente.";
var strConnectingToDeviceAttempt = "Conectando al dispositivo...intento ({0})";
var strWaitingForDevice = "Esperando a que el dispositivo se active ...intento ({0})";
var strDeviceNotDetected = "El servicio de monitorización de la presión arterial de Heart360 no pudo detectar el dispositivo, asegúrese de que el dispositivo esté conectado correctamente y realice la lectura otra vez.";
var strMonitorServiceTimeout = "El servicio de monitorización de la presión arterial de Heart360 no respondió a tiempo. Realice la lectura otra vez";

var strDeviceNotReady = "El dispositivo no está listo. Espere {0} segundos y realice la lectura nuevamente.";
var strUnExpectedError = "Ocurrió un error inesperado. Espere {0} segundos y realice la lectura nuevamente.";
var strWaitForNSeconds = "Espere {0} segundos y realice la lectura nuevamente.";
var strErrorWhileTakingMeasurementWithMsg = "Ocurrió un error [{0}] durante la medición.";
var strErrorWhileTakingMeasurement = "Ocurrió un error al realizar la medición. ";

var weekday = new Array("S", "M", "T", "W", "T", "F", "S");

var monthArray = new Array("Ene", "Feb", "Mar", "Abr", "Mayo", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");

var weekday_names = new Array(7);
weekday_names[0] = "Domingo";
weekday_names[1] = "Lunes";
weekday_names[2] = "Martes";
weekday_names[3] = "Miércoles";
weekday_names[4] = "Jueves";
weekday_names[5] = "Viernes";
weekday_names[6] = "Sábado";

var monthname = new Array(12);
monthname[1] = "Enero";
monthname[2] = "Febrero";
monthname[3] = "Marzo";
monthname[4] = "Abril";
monthname[5] = "Mayo";
monthname[6] = "Junio";
monthname[7] = "Julio";
monthname[8] = "Agosto";
monthname[9] = "Septiembre";
monthname[10] = "Octubre";
monthname[11] = "Noviembre";
monthname[12] = "diciembre";

var STR_SMALL_AM = "a.m.";
var STR_SMALL_PM = "p.m.";
var strProcessing = "Procesando...";
var strMLCDownload = "Espere mientras descargamos su informe Marcando los 7 pasos para mi salud™...";
var strDays = "días";
var strDay = "día";
var strClose = "Cerrar";
var str_Month = "Este Mes";
var STR_SHOW_GRAPH = "MOSTRAR GRÁFICO";
var STR_HIDE_GRAPH = "OCULTAR GRÁFICO";