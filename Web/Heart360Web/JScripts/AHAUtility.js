﻿function OpenMLCWindow(strUrl) {
    var newWin = window.open(strUrl, "MLCWINDOW", "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=950,height=550");
    newWin.moveTo((screen.availWidth - 950) / 2, (screen.availHeight - 550) / 2);
    //window.close();
    return false;
}
/*Start Update Panel - Progress Helper*/
function _CreateFloatingContainerForMask(id) {
    //alert("hai");
    var tOuter = document.createElement("table");
    tOuter.id = 'table_' + id;
    tOuter.className = "tableAjaxLoader";

    tbodyOuter = document.createElement("tbody");

    var tRow = document.createElement("tr");
    var tCell = document.createElement("td");

    var img = document.createElement("img");
    img.src = "/images/ajax-loader.gif";

    tCell.appendChild(img);
    tRow.appendChild(tCell);

    tbodyOuter.appendChild(tRow);
    tOuter.appendChild(tbodyOuter);

    return tOuter;
}

function ShowHideDivTrackerMask(idDivToMark, bShow) {
    //alert(idDivToMark);

    var floatingContainer = null;
    var divToMask = document.getElementById(idDivToMark);
    if (divToMask == null) {
        throw "no element with id = " + idDivToMark + " found!";
        return false;
    }

    if (!bShow) {
        /*    floatingContainer = document.getElementById('table_' + idDivToMark);
        if (floatingContainer != null) {
        divToMask.removeChild(floatingContainer);
        }*/
    }
    else {
        floatingContainer = _CreateFloatingContainerForMask(idDivToMark);
        floatingContainer.style.left = findPosX(divToMask) + "px";
        floatingContainer.style.top = findPosY(divToMask) + "px";
        floatingContainer.style.width = divToMask.offsetWidth + "px";
        floatingContainer.style.height = divToMask.offsetHeight + "px";
        floatingContainer.style.display = "";
        divToMask.appendChild(floatingContainer);
    }
}
/*End Update Panel - Progress*/

function adjustMyFrameHeight(obj) {
    var iframeHeight = 200;

    if (obj.contentDocument && obj.contentDocument.body.offsetHeight)
        iframeHeight = obj.contentDocument.getElementById('iframeDivContentArea').offsetHeight;

    else if (obj.Document && obj.Document.body.offsetHeight)
        iframeHeight = obj.Document.getElementById('iframeDivContentArea').offsetHeight;
    else if (obj.Document && obj.Document.body.scrollHeight)
        iframeHeight = obj.Document.body.getElementById('iframeDivContentArea').scrollHeight;
    if (obj.addEventListener)
        obj.addEventListener("load", readjustIframe, false)
    else if (obj.attachEvent) {
        obj.detachEvent("onload", readjustIframe)
        obj.attachEvent("onload", readjustIframe)
    }

    obj.height = iframeHeight + 20 + "px";

}

function readjustIframe(loadevt) {
    var crossevt = (window.event) ? event : loadevt
    var iframeroot = (crossevt.currentTarget) ? crossevt.currentTarget : crossevt.srcElement
    if (iframeroot)
        adjustMyFrameHeight(iframeroot.id);
}

function setItToCenter(obj) {
    try {
        var oW = obj.clientWidth;
        var bW = getPageSize()[2];

        var oH = obj.clientHeight;
        var bH = getPageSize()[3];


        var vscroll = document.documentElement.scrollTop;

        var screensize = (document.body.clientHeight) ? document.body.clientHeight : document.body.scrollHeight;


        if ((oH + 50) > screensize || !IsBrowserFixedPositionCompatible()) {

            obj.style.position = "absolute";
            obj.style.top = vscroll + 50 + "px";
        } else {
            obj.style.position = "fixed";
            obj.style.top = ((bH - oH) / 2) + "px";
        }



        obj.style.left = ((bW - oW) / 2) + "px";

        dimmer.resize();


        //obj.style.display = "block";
    }
    catch (e) {
    }
}

function showpopup(DivId) {
    //alert(DivId);
    var obj = document.getElementById(DivId);
    obj.style.display = "block";
    dimmer.show();
}

function hidepopup(DivId) {
    document.getElementById(DivId).style.display = "none";
    dimmer.hide();
}
var isDimmer = false;
var isPopup = false;
var currentPopupObj;
dimmer = {
    style: ["top:0px", "left:0px", "position:absolute", "zIndex:9998", "backgroundColor:#bbb", "opacity:.5", "filter:alpha(opacity=50)", "width:100%"],
    show: function() {
        var tDimmer = document.getElementById("dynamicdimmer");
        if (!tDimmer) {
            oDimmer = document.createElement('div');
            for (var i = 0; i < this.style.length; i++) {
                var selector = this.style[i].split(":");
                oDimmer.style[selector[0]] = selector[1];
            }
            oDimmer.id = "dynamicdimmer";
            (document.body.firstChild) ? document.body.insertBefore(oDimmer, document.body.firstChild) : document.body.appendChild(oDimmer); isDimmer = true;
            isDimmer = true;
            this.resize();

        }

    },
    hide: function() {
        var tDimmer = document.getElementById("dynamicdimmer");
        if (tDimmer) document.body.removeChild(tDimmer);
        isDimmer = false;
    },
    resize: function() {
        var tDimmer = document.getElementById("dynamicdimmer");
        if (tDimmer) {
            var viewHeight = (document.documentElement.scrollHeight) ? document.documentElement.scrollHeight : document.documentElement.clientHeight;
            tDimmer.style.height = viewHeight + "px";
        }
    }
};





/*Start Z-Index Manager*/
var AHAMaxZIndex = 7000;
var PageScrollY = 0;
/*End Z-Index Manager*/

/*
This function is used for auto tab functionality in date control.
Parameters - textbox control, length of the textbox,event.
*/
var isNN = (navigator.appName.indexOf("Netscape") != -1);
function autoTab(input, len, e) {
    var keyCode = (isNN) ? e.which : e.keyCode;
    var filter = (isNN) ? [0, 8, 9] : [0, 8, 9, 16, 17, 18, 37, 38, 39, 40, 46];
    if (input.value.length >= len && !containsElement(filter, keyCode)) {
        input.value = input.value.slice(0, len);
        input.form[(getIndex(input) + 1) % input.form.length].focus();
    }
    function containsElement(arr, ele) {
        var found = false, index = 0;
        while (!found && index < arr.length)
            if (arr[index] == ele)
            found = true;
        else
            index++;
        return found;
    }
    function getIndex(input) {
        var index = -1, i = 0, found = false;
        while (i < input.form.length && index == -1)
            if (input.form[i] == input) index = i;
        else i++;
        return index;
    }
    return true;
}

function GetUserDate() {
    var dt = new Date();
    //debugger;
    return dt.getDate() + ":" + (dt.getMonth() + 1) + ":" + dt.getFullYear();
}

function OpenPrinterFriendlyPage(url) {
    window.open(url, '_print', 'height=750,width=770,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no,top=100,left=100');
    return false;
}

function OpenTourPage(url, strWindowName) {
    //alert(url.replace(new RegExp(/\//gi), "_"));
    //var strName = url.replace(new RegExp(/\//gi), "");
    window.open(url, strWindowName, 'height=655,width=935,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no,top=100,left=100');
    return false;
}

function HideEditDiv() {
    var element = document.getElementById("divPopupContainer");
    element.innerHTML = "";
    element.style.display = "none";
    document.getElementById('divMask').style.display = "none";
    return false;
}

function HideHeightDiv() {
    var element = document.getElementById("divPopupContainer");
    element.innerHTML = "";
    element.style.display = "none";
    document.getElementById('divMask').style.display = "none";
    return false;
}

function IsBrowserFixedPositionCompatible() {
    if (navigator.appName != "Microsoft Internet Explorer"
     || navigator.appVersion.indexOf("MSIE 7.") != -1
     || navigator.appVersion.indexOf("MSIE 8.") != -1
     || navigator.appVersion.indexOf("MSIE 9.") != -1
     || navigator.appVersion.indexOf("MSIE 10.") != -1) {
        return true;
    }

    return false;
}

function IsIE8() {
    if (navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE 8.") != -1) {
        return true;
    }

    return false;
}

function ShowHeightDiv(width) {
    var element = document.getElementById("divPopupContainer");
    element.innerHTML = "";
    var height = 120;
    if (IsBrowserFixedPositionCompatible()) {
        element.style.position = "fixed";
        document.getElementById('divMask').style.position = "fixed";
    }
    element.style.display = "block";
    element.style.left = (getPageSize()[0] / 2) - (width / 2) + "px";
    element.style.top = (getPageSize()[3] / 2) - (height / 2) + "px";
    element.style.width = width + 'px';
    element.style.height = height + 'px';
    document.getElementById("DivHeight").style.display = "block";
    element.appendChild(document.getElementById("DivHeight"));


    document.getElementById('divMask').style.display = "block";
    //document.getElementById('divMask').style.height = getPageSize()[1] + 'px';
}

function HideSaveButton(bHide, btnSave, btnCancel) {
    var Save = document.getElementById(btnSave);
    var Cancel = document.getElementById(btnCancel);
    var waitdiv = document.getElementById('divWait');
    if (bHide) {
        Save.style.visibility = "hidden";
        Cancel.style.visibility = "hidden";
        waitdiv.style.display = "block";
    }
    else {
        Save.style.visibility = "visible";
        Cancel.style.visibility = "visible";
        waitdiv.style.display = "none";
    }
}

function ShowEditDiv(divEdit, width) {
    document.getElementById('divMask').style.display = "block";
    var element = document.getElementById("divPopupContainer");
    if (IsBrowserFixedPositionCompatible()) {
        element.style.position = "fixed";
        document.getElementById('divMask').style.position = "fixed";
    }
    element.innerHTML = "";
    element.style.display = "block";
    element.style.left = (getPageSize()[0] / 2) - (width / 2) + "px";
    element.style.width = width + 'px';
    document.getElementById(divEdit).style.display = "block";
    try {
        element.appendChild(document.getElementById(divEdit));
    }
    catch (e) {
    }

    //document.getElementById('divMask').style.height = getPageSize()[1] + 'px';
}

function OnConfirmDelete(val) {
    return confirm(val);
}

function OnConfirmDeleteWithProcessing(val, btnId, divId) {
    if (confirm(val)) {
        ShowHideButton(btnId, divId, false);
        return true;
    }
    else {
        return false;
    }
}


function ShowHideTrackerMask(bShow) {
    ShowHideDivMask('DivTacker', bShow);
}

function ShowHideDivMask(divIdToMask, bShow) {
    var DivMaskTracker = document.getElementById('DivMaskTracker');
    var divToMask = document.getElementById(divIdToMask);

    if (divToMask == null)
        return;


    if (bShow) {
        DivMaskTracker.style.left = (divToMask.offsetLeft + 200) + "px";
        DivMaskTracker.style.top = (divToMask.offsetTop + 90) + "px";
        DivMaskTracker.style.width = divToMask.offsetWidth + 20 + "px";
        DivMaskTracker.style.height = divToMask.offsetHeight + 40 + "px";
        DivMaskTracker.className = "popupmask";
        DivMaskTracker.style.display = "block";
    }
    else {
        DivMaskTracker.style.display = "none";
        DivMaskTracker.className = "";
    }
}

function ShowHideDivTranslucentMask(divIdToMask, divIdForMasking, bShow) {
    var divToMask = document.getElementById(divIdToMask);
    var divForMasking = document.getElementById(divIdForMasking);
    //debugger;
    //alert(divToMask.offsetLeft + " : " + divToMask.offsetTop);
    if (bShow) {
        //divTranslucent.style.left = (d.offsetLeft) + "px";
        //divTranslucent.style.top = (d.offsetTop) + "px";
        divForMasking.style.width = divToMask.offsetWidth + "px";
        divForMasking.style.height = divToMask.offsetHeight + "px";
        divForMasking.className = "translucentmask";
        divForMasking.style.display = "block";
    }
    else {
        divForMasking.style.display = "none";
        divForMasking.className = "";
    }
}

function ShowHideButton(IdToHide, IdToShow, bShow) {
    if (bShow) {
        document.getElementById(IdToHide).style.display = "block";
        document.getElementById(IdToHide).disabled = false;
        document.getElementById(IdToShow).style.display = "none";
    }
    else {
        document.getElementById(IdToHide).style.display = "none";
        document.getElementById(IdToShow).style.display = "block";
    }
}

function getPageSize() {

    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) {	// all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if (yScroll < windowHeight) {
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    if (navigator.appName == "Netscape" && getPageYScroll() > 0) {
        pageWidth -= 18;
    }

    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;
}

function getPageYScroll() {

    var yScroll;

    if (self.pageYOffset) {
        yScroll = self.pageYOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
        yScroll = document.documentElement.scrollTop;
    } else if (document.body) {// all other Explorers
        yScroll = document.body.scrollTop;
    }

    return yScroll;
}


/****************Start Progress Div****************************************************/
var nRequestProcessingCount = 0;
function ShowRequestProcessing(strProgressText) {
    nRequestProcessingCount++;
    var rpDiv = document.getElementById('divProgressBar');
    rpDiv.style.display = "block";
    rpDiv.style.width = getPageSize()[0] + "px"; //window.screen.width - 20 + "px";
    if (IsBrowserFixedPositionCompatible()) {
        rpDiv.style.position = "fixed";
    }
    rpDiv.innerHTML = strProgressText;
}

function HideRequestProcessing() {
    nRequestProcessingCount--;
    if (nRequestProcessingCount == 0) {
        var rpDiv = document.getElementById('divProgressBar');
        rpDiv.style.display = "none";
        rpDiv.innerHTML = "";
    }
}

function RefreshSiteDataPatientForUrl(url) {
    ShowRequestProcessing(strSiteDataRefreshing);
    var strRedirect = '/Patient/RefreshSiteData.aspx?loc=' + URLEncode(url);
    window.location.replace(strRedirect);
    return false;
}

function RefreshSiteDataPatient() {
    ShowRequestProcessing(strSiteDataRefreshing);
    var strRedirect = '/Patient/RefreshSiteData.aspx?loc=' + URLEncode(window.location);
    window.location.replace(strRedirect);
    return false;
}

function RefreshSiteDataProvider() {
    ShowRequestProcessing(strSiteDataRefreshing);
    var strRedirect = '/Provider/RefreshSiteData.aspx?loc=' + URLEncode(window.location);
    //alert(strRedirect);
    window.location.replace(strRedirect);
    return false;
}

/****************End Progress Div****************************************************/



/*************** Graph Plot Functions ***********************************************/


function getXOffset(e) {
    if (typeof e.offsetX != 'undefined')
        return e.offsetX;
    else if (typeof e.pageX != 'undefined')
        return e.pageX - e.target.offsetLeft;
}

function getYOffset(e) {
    if (typeof e.offsetY != 'undefined')
        return e.offsetY;
    else if (typeof e.pageY != 'undefined')
        return e.pageY - e.target.offsetTop;
}

function on_image_over(e) {
    e = (e) ? e : window.event;
    //document.getElementById('mytext').value = getXOffset(e) + ' : ' + getYOffset(e);
}

function findPos(obj) {
    var curleft = 0;
    var curtop = 0;
    var border = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {

            curleft += obj.offsetLeft - obj.scrollLeft;
            curtop += obj.offsetTop - obj.scrollTop;
            var position = '';
            if (obj.style && obj.style.position) position = obj.style.position.toLowerCase();
            if ((position == 'absolute') || (position == 'relative')) break;
            while (obj.parentNode != obj.offsetParent) {
                obj = obj.parentNode;
                curleft -= obj.scrollLeft;
                curtop -= obj.scrollTop;
            }
            obj = obj.offsetParent;
        }
    }
    else {
        if (obj.x)
            curleft += obj.x;
        if (obj.y)
            curtop += obj.y;
    }
    return { left: curleft, top: curtop };
}

function CreateGraphDiv(id) {
    var div = document.createElement("div");
    div.id = id;
    div.className = "bubble";

    setTimeout(function() {
        document.body.appendChild(div);
    }, 100);
}

function showGraphDiv(toggle, bubblediv, imageid, x1, y1, h, w, str) {
    var obj = document.getElementById(bubblediv);
    if (obj == null)
        return;
    if (toggle) {
        var imagediv = document.getElementById(imageid);
        var pos = findPos(imagediv);

        if (bNewSite) {
            setTimeout(function() {
                obj.style.display = "block";


                if (navigator.appVersion.indexOf("MSIE 7.") != -1) {
                    obj.style.left = (x1 + 130) + "px";
                    obj.style.top = (pos.top + y1) - (obj.offsetHeight + 60) - 250 + "px";
                }
                else {
                    obj.style.left = (x1 + 140) + "px";
                    obj.style.top = (pos.top + y1) - (obj.offsetHeight + 60) + "px";
                }
                obj.style.display = "block";
            }, 1);
        }
        else {
            setTimeout(function() {
                obj.style.display = "block";
                obj.style.left = (x1 + 50) + "px";
                obj.style.top = (pos.top + y1 - 200) + "px";
            }, 1);
        }
        obj.style.width = 80 + "px";
        //obj.style.height = 20 + "px";
        //alert(y1 + " : " + pos.top + " : " + h);

        if (obj.offsetHeight != null && obj.offsetHeight > 100) {
            if (!bNewSite) {
                setTimeout(function() {
                    obj.style.top = (pos.top + y1 - 250) + "px";
                }, 2);
            }
        }

        if (obj.offsetHeight != null && obj.offsetHeight < 50) {
            if (!bNewSite) {
                setTimeout(function() {
                if (navigator.appVersion.indexOf("MSIE 7.") != -1) {
                        obj.style.left = (x1 + 140) + "px";
                        obj.style.top = (pos.top + y1) - 100 + "px";
                    }
                    else {
                        obj.style.left = (x1 + 150) + "px";
                        obj.style.top = (pos.top + y1) + "px";
                    }
                }, 2);
            }
        }
        obj.innerHTML = str;

        //obj.style.left = 100;
        //obj.style.top= 100;
        //obj.innerHTML = str;
    }
    else {
        obj.style.display = "none";
    }


}



/*************** Graph Plot Functions ***********************************************/



function TextLimit(field, maxlen) {
    var txt = document.getElementById(field);
    if (txt.value.length > maxlen + 1) {
        txt.value = txt.value.substring(0, maxlen);
        return false;
    }
    return;
}

/********************START HIT BOX**************************************************/
function signInClicked() {
    _hbLink('GetStartedSignIn');
    return true;
}

function registerClicked() {
    _hbLink('GetStartedRegister');
    return true;
}
/********************END HIT BOX**************************************************/

/********************START JS ERROR LOG**************************************************/
window.onerror = AHATrapError;

function AHATrapError(msg, url, ln) {
    if (!TrapJSErrors)
        return;

    var error = new Error(msg);

    error.location = url + ', line: ' + ln; // add custom property

    Heart360Web.Services.AHAJSUtility.LogJavascriptError(error.name, error.message, error.location, OnErrorLogSuccess, OnErrorLogFailure);

    return true;
}

function OnErrorLogSuccess(result, userContext, methodName) {
}

function OnErrorLogFailure(exception, userContext, methodName) {
    /*  
    We can also perform different actions like the  
    succeededCallback handler based upon the methodName and userContext  
    */
}
/********************END JS ERROR LOG**************************************************/

function ChangeContent(bNext) {
    if (bNext)
        myLytebox.changeContent(myLytebox.activeFrame + 1);
    else
        myLytebox.changeContent(myLytebox.activeFrame - 1);
}

function EndTour() {
    myLytebox.end();
}

function Signup(bIsScientificSession) {
    if (bIsScientificSession) {
        window.location = "../../ScientificSession/prereg.aspx";
    }
    else {
        window.location = "../../visitor/prereg.aspx";
    }
}

/*Provider Additions*/

function ShowGenericConfirmationDiv(divid, width, height) {
    //debugger;   
    var element = document.getElementById("divPopupContainer");

    if (IsBrowserFixedPositionCompatible()) {
        element.style.position = "fixed";
        document.getElementById('divMask').style.position = "fixed";
    }
    element.style.display = "block";
    element.style.left = (getPageSize()[0] / 2) - (width / 2) + "px";
    element.style.top = (getPageSize()[3] / 2) - (height / 2) + "px";
    element.style.width = width + 'px';
    element.style.height = height + 'px';

    element.appendChild(document.getElementById(divid));

    document.getElementById(divid).style.display = "block";
    document.getElementById('divMask').style.display = "block";

    return false;
}

function HideGenericConfirmationDiv(divid) {
    var element = document.getElementById("divPopupContainer");
    var childDiv = document.getElementById(divid);
    childDiv.style.display = "none";
    element.removeChild(childDiv);
    document.body.appendChild(childDiv);
    element.style.display = "none";
    document.getElementById('divMask').style.display = "none";

    return false;
}
/*End Provider Additions*/

/* Mail Specific */

function OnMailSelectAll(item) {
    var bchecked = false;
    if (item.checked)
        bchecked = true;

    for (var i = 0; i < document.forms[0].elements.length; i++) {
        var ele = document.forms[0].elements[i];
        if (ele.type == "checkbox" && ele.id.indexOf("chkSelect") != -1) {
            ele.checked = bchecked;
        }
    }
}

function OnMailSelectAllUnchecked(item, chkSelectAll) {
    if (!item.checked) {
        document.getElementById(chkSelectAll).checked = false;
        return;
    }

    var bIfAllChecked = true;
    for (var i = 0; i < document.forms[0].elements.length; i++) {
        var ele = document.forms[0].elements[i];
        if (ele.type == "checkbox" && ele.id.indexOf("chkSelect") != -1 && !ele.checked) {
            bIfAllChecked = false;
        }
    }

    if (bIfAllChecked)
        document.getElementById(chkSelectAll).checked = true;
}

/* End Mail Specific */
/*URL Encode*/
function URLEncode(clearString) {
    var output = '';
    var x = 0;
    clearString = clearString.toString();
    var regex = /(^[a-zA-Z0-9_.]*)/;
    while (x < clearString.length) {
        var match = regex.exec(clearString.substr(x));
        if (match != null && match.length > 1 && match[1] != '') {
            output += match[1];
            x += match[1].length;
        } else {
            if (clearString[x] == ' ')
                output += '+';
            else {
                var charCode = clearString.charCodeAt(x);
                var hexVal = charCode.toString(16);
                output += '%' + (hexVal.length < 2 ? '0' : '') + hexVal.toUpperCase();
            }
            x++;
        }
    }
    return output;
}

/*End URl Encode*/

/*Start Risk Level Indicator*/


levelBar = function() {
    this.containerObj = document.getElementById(arguments[0]);
    this.levelDiv = document.createElement('div');
    this.levelDiv.className = "leavelDiv";
    this.containerObj.appendChild(this.levelDiv);
    this.greenToRed = false;

    this.goalDiv = document.createElement('div');
    this.goalDiv.className = "goalDiv";
    this.goalDiv.style.width = "0%";
    this.levelDiv.appendChild(this.goalDiv);
    this.goalMarkDiv = document.createElement('div');
    this.goalMarkDiv.className = "goalMarkDiv";
    this.goalDiv.appendChild(this.goalMarkDiv);

    this.goalLineDiv = document.createElement('div');
    this.goalLineDiv.className = "goalLineDiv";
    this.goalDiv.appendChild(this.goalLineDiv);



    this.levelTable = document.createElement('table');
    this.levelTable.cellPadding = "0";
    this.levelTable.cellSpacing = "0";
    this.levelTable.border = "0";
    this.levelTable.className = "levelTable";
    this.levelDiv.appendChild(this.levelTable);
    var levelTableRow = this.levelTable.insertRow(-1);
    for (var i = 1; i < arguments.length; i++) {
        var s = arguments[i].toString();
        var argumentsElemnts = s.split(",");
        var cell = levelTableRow.insertCell(-1);
        cell.innerHTML = argumentsElemnts[0].toString();
        cell.style.width = argumentsElemnts[1];
        if (i == (arguments.length - 1)) {
            cell.className = "levelTableLastTd";
        }
    }

    this.readingDiv = document.createElement('div');
    this.readingDiv.className = "readingDiv";
    this.readingDiv.style.width = "0%";
    this.levelDiv.appendChild(this.readingDiv);
    this.readingMarkDiv = document.createElement('div');
    this.readingMarkDiv.className = "readingMarkDiv";
    this.readingDiv.appendChild(this.readingMarkDiv);
    this.readingLineDiv = document.createElement('div');
    this.readingLineDiv.className = "readingLineDiv";
    this.readingDiv.appendChild(this.readingLineDiv);


};

levelBar.prototype.setReading = function(val) {
    this.readingDiv.style.display = "block";
    this.readingDiv.style.width = val;

    if (this.greenToRed) {
        this.levelDiv.className = "leavelDiv leavelgreentored";
    }

}
levelBar.prototype.setGoal = function(val) {
    this.goalDiv.style.display = "block";
    this.goalDiv.style.width = val;


}
physicalActivity = function() {
    this.containerObj = document.getElementById(arguments[0]);
    this.monthTable = document.createElement('table');
    this.monthTable.cellPadding = "0";
    this.monthTable.cellSpacing = "0";
    this.monthTable.border = "0";
    this.monthTable.className = "monthTable";
    this.containerObj.appendChild(this.monthTable);
    var monthUpperTableRow = this.monthTable.insertRow(-1);
    var monthUpperTableCell = monthUpperTableRow.insertCell(-1);
    monthUpperTableCell.className = "monthBarChartLeft";

    var maxLabelHolder = document.createElement('div');
    monthUpperTableCell.appendChild(maxLabelHolder);
    maxLabelHolder.className = "maxLabelHolder";
    maxLabelHolder.innerHTML = "&nbsp;";
    var maxLabel = document.createElement('div');
    maxLabelHolder.appendChild(maxLabel);
    maxLabel.innerHTML = "7 " + strDays;
    maxLabel.className = "monthMaxLabel";

    this.weekBar = new Array();

    for (var i = 0; i < 4; i++) {
        var monthUpperTableGraphCell = monthUpperTableRow.insertCell(-1);
        monthUpperTableGraphCell.className = "monthGraphHolder";
        monthUpperTableGraphCell.rowSpan = 2;
        this.weekBar.push(monthUpperTableGraphCell);
    }

    var monthBottomTableRow = this.monthTable.insertRow(-1);
    var monthBottomTableCell = monthBottomTableRow.insertCell(-1);
    monthBottomTableCell.className = "monthBarChartLeft monthBarCharttbottom";

    var maxLabelHolder = document.createElement('div');
    monthBottomTableCell.appendChild(maxLabelHolder);
    maxLabelHolder.className = "maxLabelHolder";
    maxLabelHolder.innerHTML = "&nbsp;";
    var minLabel = document.createElement('div');
    maxLabelHolder.appendChild(minLabel);
    minLabel.innerHTML = "0 " + strDays;
    minLabel.className = "monthMinLabel";

    var monthBottomTextTableRow = this.monthTable.insertRow(-1);
    var firstCellBottomTextCell = monthBottomTextTableRow.insertCell(-1);
    firstCellBottomTextCell.innerHTML = "&nbsp;";

    var s = arguments[1].toString();
    var labelElements = s.split(",");



    for (var i = 0; i < 4; i++) {
        var monthBottomTextLabel = monthBottomTextTableRow.insertCell(-1);
        monthBottomTextLabel.className = "monthBottomLabel";
        monthBottomTextLabel.innerHTML = labelElements[i];

    }

}
physicalActivity.prototype.setWeek = function() {

    for (var i = 0; i < arguments.length; i++) {

        this.weekBar[i].innerHTML = " ";

        var weekBarDiv = document.createElement('Div');
        weekBarDiv.className = "weekBarDiv";
        this.weekBar[i].appendChild(weekBarDiv);



        var weekBarTable = document.createElement('Table');
        weekBarDiv.appendChild(weekBarTable);
        weekBarTable.cellPadding = "0";
        weekBarTable.cellSpacing = "0";
        weekBarTable.border = "0";
        weekBarTable.className = "weekBarTable";
        var weekBarTableRow = weekBarTable.insertRow(-1);
        var weekBarTableCell = weekBarTableRow.insertCell(-1);
        weekBarTableCell.innerHTML = arguments[i] + (arguments[i] == "1" ? " " + strDay : " " + strDays);



        var weekBarDivMarker = document.createElement('Div');
        weekBarDivMarker.className = "weekBarDivMarker";
        weekBarTableCell.appendChild(weekBarDivMarker);

        weekBarDivMarker.style.height = (arguments[i] * 14.3) + "%";

        var weekBarDivMarkerTable = document.createElement('Table');
        weekBarDivMarker.appendChild(weekBarDivMarkerTable);
        weekBarDivMarkerTable.cellPadding = "0";
        weekBarDivMarkerTable.cellSpacing = "0";
        weekBarDivMarkerTable.border = "0";
        weekBarDivMarkerTable.className = "weekBarDivMarkerTable";
        var weekBarTableRow = weekBarDivMarkerTable.insertRow(-1);
        var weekBarTableCell = weekBarTableRow.insertCell(-1);
        weekBarTableCell.innerHTML = arguments[i] + (arguments[i] == "1" ? " " + strDay : " " + strDays);






    }
}
/*End risk level indicator*/
/*Start Organize Dashboard*/
function SymmaryType(position, summtype, divid, bIsVisible) {
    this.Type = summtype;
    this.Position = position;
    this.DivId = divid;
    this.IsVisible = bIsVisible;
}

function SummaryCtrl(bIsConfig, gPersonGUID, gRecordGUID) {
    this.ArrSummary = new Array();
    this.IsConfig = bIsConfig;
    this.PersonGUID = gPersonGUID;
    this.RecordGUID = gRecordGUID;
    this.HasUserMadeChanges = false;
}

SummaryCtrl.prototype.GetXmlForSummary = function() {
    var strXml = "";
    for (var i = 0; i < this.ArrSummary.length; i++) {
        if (i == 0) {
            strXml += "<SummaryOptions>";
        }

        strXml += "<Summary><Type>" + this.ArrSummary[i].Type + "</Type><Position>" + this.ArrSummary[i].Position + "</Position><IsVisible>" + this.ArrSummary[i].IsVisible + "</IsVisible></Summary>";
    }

    if (strXml.length > 0) {
        strXml += "</SummaryOptions>";
    }

    return strXml;
};

SummaryCtrl.prototype.ChangePos = function(summtype, newPosition) {
    for (var i = 0; i < this.ArrSummary.length; i++) {
        if (this.ArrSummary[i].Type == summtype) {
            this.ArrSummary[i].Position = newPosition;
            this.SummaryShowHide(newPosition, this.ArrSummary[i].IsVisible, false);
            break;
        }
    }
};

SummaryCtrl.prototype.GetJSONForSummary = function() {
    //var myFirstJSON = { "firstName": "John", "lastName": "Doe", "age": 23 };
    var strXml = "";

    for (var i = 0; i < this.ArrSummary.length; i++) {
        if (i == 0) {
            str = "\"" + this.ArrSummary[i].Type + "\"" + ":" + "\"" + this.ArrSummary[i].Position + "\"";
        }
        else {
            str += "," + "\"" + this.ArrSummary[i].Type + "\"" + ":" + "\"" + this.ArrSummary[i].Position + "\"";
        }
    }

    if (str.length > 0) {
        str = '{' + str + '}'
    }
    return JSON.parse(str);
};

SummaryCtrl.prototype.ShowHideSummaryType = function() {
};

SummaryCtrl.prototype.RenderSummary = function() {
    //alert(this.ArrSummary.length);
    for (var i = 0; i < this.ArrSummary.length; i++) {
        var divTarget = document.getElementById("Div" + this.ArrSummary[i].Position);
        if (divTarget != null) {
            divTarget.appendChild(document.getElementById(this.ArrSummary[i].DivId));
        }

        if (!this.IsConfig && !this.ArrSummary[i].IsVisible) {
            var summaryRow = document.getElementById("Row" + this.ArrSummary[i].Position);
            summaryRow.style.display = "none";
        }

        if (this.IsConfig) {
            //eval("span_" + this.ArrSummary[i].Position).setValue(this.ArrSummary[i].IsVisible);
            //alert(eval("span_" + this.ArrSummary[i].Position).isYes);
            if (this.ArrSummary[i].IsVisible) {
                document.getElementById("linkShow" + this.ArrSummary[i].Position).className = "btn-hide-show active";
                document.getElementById("linkHide" + this.ArrSummary[i].Position).className = "btn-hide-show";
            }
            else {
                document.getElementById("linkShow" + this.ArrSummary[i].Position).className = "btn-hide-show";
                document.getElementById("linkHide" + this.ArrSummary[i].Position).className = "btn-hide-show active";
            }
        }
    }
};

SummaryCtrl.prototype.SummaryMoveUpDown = function(position, bIsDown) {
    var div1 = null;
    var div2 = null;
    var divSource = document.getElementById("Div" + position);
    if (divSource != null) {
        div1 = divSource.getElementsByTagName('div')[0]; //divSource.firstChild;
        //alert(divSource.getElementsByTagName('div')[0]);
    }
    var divDest = null;
    var iOldPosition = position;
    var iNewPosition = 0;
    if (bIsDown) {
        iNewPosition = position + 1;
        divDest = document.getElementById("Div" + (position + 1));
    }
    else {
        iNewPosition = position - 1;
        divDest = document.getElementById("Div" + (position - 1));
    }

    if (divDest != null) {
        div2 = divDest.getElementsByTagName('div')[0]; //divDest.firstChild;
    }

    if (div1 != null && div2 != null) {
        //debugger;
        //alert(div1.innerHTML);
        var type1 = div1.getAttribute("rel");
        var type2 = div2.getAttribute("rel");
        //alert(type1 + " : " + type2);
        divSource.appendChild(div2);
        divDest.appendChild(div1);
        this.ChangePos(type1, iNewPosition);
        this.ChangePos(type2, iOldPosition);
    }

    //alert(arrSummary["Pos1"] + "\r\n" + arrSummary["Pos2"] + "\r\n" + arrSummary["Pos3"] + "\r\n" + arrSummary["Pos4"] + "\r\n" + arrSummary["Pos5"] + "\r\n" + arrSummary["Pos6"]);
    //PageMethods.UpdateTrackerSortOrder(this.GetXmlForSummary(), this.PersonGUID, this.RecordGUID);
    this.HasUserMadeChanges = true;
};

SummaryCtrl.prototype.SummaryShowHide = function(position, bIsShow, bUpdateToServer) {
    for (var i = 0; i < this.ArrSummary.length; i++) {
        if (this.ArrSummary[i].Position == position) {
            this.ArrSummary[i].IsVisible = bIsShow;
            //eval("span_" + this.ArrSummary[i].Position).setValue(bIsShow);
            if (this.ArrSummary[i].IsVisible) {
                document.getElementById("linkShow" + this.ArrSummary[i].Position).className = "btn-hide-show active";
                document.getElementById("linkHide" + this.ArrSummary[i].Position).className = "btn-hide-show";
            }
            else {
                document.getElementById("linkShow" + this.ArrSummary[i].Position).className = "btn-hide-show";
                document.getElementById("linkHide" + this.ArrSummary[i].Position).className = "btn-hide-show active";
            }
            break;
        }
    }
    //PageMethods.UpdateTrackerSortOrder(this.GetXmlForSummary(), this.PersonGUID, this.RecordGUID);
    this.HasUserMadeChanges = true;
};

SummaryCtrl.prototype.SaveDashborad = function() {
    if (this.HasUserMadeChanges) {
        PageMethods.UpdateTrackerSortOrder(this.GetXmlForSummary(), this.PersonGUID, this.RecordGUID);
    }
    this.ClosePage();
    return false;
};
SummaryCtrl.prototype.ClosePage = function() {
    if (this.HasUserMadeChanges) {
        setTimeout(function() { window.parent.RefreshPage(); }, 1000);
    }
    else {
        var linkClose = window.parent.GetCloseButton();
        linkClose.click();
    }
    return false;
};
/*End Organize Dashboard*/
/*Start MLC*/
// JScript source code
analogMeter = function() {
    this.containerObj = document.getElementById(arguments[0]);
    this.analogMeter = document.createElement('div');
    this.analogMeter.className = "analogMeter";
    this.containerObj.appendChild(this.analogMeter);
    this.analogMeterPointer = document.createElement('div');
    this.analogMeterPointer.className = "analogMeterPointer";
    this.analogMeter.appendChild(this.analogMeterPointer);
    this.analogMeterShadowValue = document.createElement('div');
    this.analogMeterShadowValue.className = "analogMeterShadowValue";
    this.analogMeter.appendChild(this.analogMeterShadowValue);
    this.analogMeterShadowValue.innerHTML = "0.0";
    this.analogMeterValue = document.createElement('div');
    this.analogMeterValue.className = "analogMeterValue";
    this.analogMeter.appendChild(this.analogMeterValue);
    this.analogMeterValue.innerHTML = "0.0";
    this.analogMeterValue.style.border = "none";
    this.analogMeterCenter = document.createElement('div');
    this.analogMeterCenter.className = "analogMeterCenter";
    this.analogMeter.appendChild(this.analogMeterCenter);
    this.reading = 0;
    this.waitAnimation = false;
};
analogMeter.prototype.setValue = function(val) {

    if (val > 10) val = 10;
    if (val < 0) val = 0;
    var currentReading = parseInt(val * 80 * 10);


    if (this.waitAnimation) {
        clearInterval(this.slideIntervalHandle);

    }
    this.movePointerTo(currentReading);

};
analogMeter.prototype.movePointerTo = function(val) {
    var oInstance = this;
    var startPoint = this.reading;
    var speed = 5;
    this.slideIntervalHandle = setInterval(function() {
        var pointAdjustMent = 0;
        if (oInstance.reading > 4000) {
            pointAdjustMent++;
        }
        if (oInstance.reading > 7600) {
            pointAdjustMent++;
        }
        oInstance.waitAnimation = true;
        var bgString = (-1 * startPoint - pointAdjustMent) + "px 0px";
        oInstance.analogMeterPointer.style.backgroundPosition = bgString;
        if (startPoint == val) {
            clearInterval(oInstance.slideIntervalHandle);
            oInstance.reading = val;
            oInstance.waitAnimation = false;
        }
        else {
            if (val > oInstance.reading) {
                startPoint += 80;
            } else {
                startPoint -= 80;
            }
            oInstance.reading = startPoint;
        }
        var displayValue = (oInstance.reading / 800);
        if ((displayValue * 10 % 10) == 0 && displayValue != 10) {
            displayValue = displayValue + ".0";
        }
        oInstance.analogMeterValue.innerHTML = displayValue;
        oInstance.analogMeterShadowValue.innerHTML = displayValue;
    }, speed);
}
/*End MLC*/
/*Start Show Hide for Organize BD*/
// JScript source code

yesnobutton = function() {
    this.containerObj = document.getElementById(arguments[0]);
    this.position = arguments[1];

    this.yesnocontainerHolder = document.createElement('div');
    this.yesnocontainerHolder.className = "yesnocontainerHolder";
    this.containerObj.appendChild(this.yesnocontainerHolder);


    this.slider = document.createElement('div');
    this.slider.className = "yesnoslider";
    this.yesnocontainerHolder.appendChild(this.slider);
    this.isYes = true;

    var contentTable = document.createElement('table');
    this.slider.appendChild(contentTable);
    contentTable.cellPadding = "0";
    contentTable.cellSpacing = "0";
    contentTable.border = "0";
    contentTable.width = "100%";
    contentTable.className = "yesnocontentTable";
    var showRow = contentTable.insertRow(-1);
    var Cell = showRow.insertCell(-1);
    Cell.className = "yestext";
    Cell.innerHTML = arguments[2];
    Cell = showRow.insertCell(-1);
    Cell.innerHTML = "&nbsp;";
    Cell = showRow.insertCell(-1);
    Cell.className = "notext";
    Cell.innerHTML = arguments[3];

    this.currentX = null;
    this.waitingForAnimation = false;
    var cover = document.createElement('div');
    cover.className = "yesnocover";
    this.yesnocontainerHolder.appendChild(cover);
    var oInstance = this;

    this.containerObj.onclick = function() {
        if (oInstance.isYes) {
            oInstance.isYes = false;
        } else {
            oInstance.isYes = true;
        }

        oInstance.toggle(true);

    }

}

yesnobutton.prototype.toggle = function(bOnClick) {
    var oInstance = this;
    var speed = 1;
    var startPoint = this.currentX;
    if (oInstance.waitingForAnimation) {
        clearInterval(oInstance.slideIntervalHandle);
    }
    this.slideIntervalHandle = setInterval(function() {
        oInstance.waitingForAnimation = true;
        if (!oInstance.isYes) {
            if (startPoint <= 0) {
                clearInterval(oInstance.slideIntervalHandle);
                oInstance.waitingForAnimation = false;
            } else {
                startPoint -= 2;
            }
        } else {
            if (startPoint >= 56) {

                clearInterval(oInstance.slideIntervalHandle);
                oInstance.waitingForAnimation = false;
            } else {
                startPoint += 2;
            }
        }
        var bgString = (-1 * startPoint) + "px";
        oInstance.slider.style.left = bgString;
        oInstance.currentX = startPoint;
    }, speed);

    if (bOnClick && _SummaryCtrl != null) {
        _SummaryCtrl.SummaryShowHide(oInstance.position, oInstance.isYes, true);
    }
}
yesnobutton.prototype.setValue = function(boolVal) {
    if (boolVal) {
        this.isYes = true;
        if (this.currentX == null) {
            this.currentX = 0;
        }

    } else {
        this.isYes = false;


    }
    this.toggle(false);


}
/*End Show Hide for Organize BD*/
/*Start Error Specific*/
function RedirectToAppropriatePage(strUrl) {
    if (window.parent != null) { window.parent.location.replace(strUrl); return; } if (window.opener != null) { window.opener.location.replace(strUrl); return; } window.location.replace(strUrl);
}
/*End Error Specific*/
function ExpandCollapseTrackerGraph(linkExpandCollapse, hidTrackerTypeID, hidIsGraphVisibleID, DivGraphID) {
    var hidTrackerType = document.getElementById(hidTrackerTypeID);
    var hidIsGraphVisible = document.getElementById(hidIsGraphVisibleID);
    var DivGraph = document.getElementById(DivGraphID);

    if (hidIsGraphVisible.value == "0") {
        linkExpandCollapse.innerHTML = STR_HIDE_GRAPH;
        hidIsGraphVisible.value = "1";
        DivGraph.style.display = "block";
        linkExpandCollapse.className = "aha-collapse";
    }
    else {
        linkExpandCollapse.innerHTML = STR_SHOW_GRAPH;
        hidIsGraphVisible.value = "0";
        DivGraph.style.display = "none";
        linkExpandCollapse.className = "aha-expand";
    }
    //alert(hidTrackerType.value);
    PageMethods.UpdateTrackerDetails(hidTrackerType.value, hidIsGraphVisible.value);
}


function SendTweet(txt) {
    var tweetUrl = "http://twitter.com/home?status=";
    var argument = "titlebar=no,scrollbars=yes,directories=no,menubar=no,toolbar=no,width=800,height=450";
    var tweetwindow = window.open(tweetUrl + txt, "", argument);
    return false;
}

function SendToFacebook(fbWallUrl) {
    var argument = "titlebar=no,scrollbars=yes,directories=no,menubar=no,toolbar=no,width=800,height=450";
    var tweetwindow = window.open(fbWallUrl, "", argument);
    return false;
}


function SetInGRCCalendar(calid, dt) {
    if (window.addEventListener) {
        window.addEventListener('load', function() {
            eval(calid).OnSetDate(dt);
        }, false);
    }
    else if (window.attachEvent) {
        window.attachEvent('onload', function() {
            eval(calid).OnSetDate(dt);
        });
    }
}

function SetIframeUrl(strIframeId, strUrl, bShowInHistory) {
    if (bShowInHistory) {
        document.getElementById(strIframeId).src = strUrl;
    }
    else {
        document.getElementById(strIframeId).contentWindow.location.replace(strUrl);
    }
}

var currentDropDownObj = null;
function showdropdownmenu(obj) {


    obj.style.clip = "rect(auto auto auto auto)";
    obj.removeAttribute("style");
    currentDropDownObj = obj;


}
function hidedropdownmenu(obj) {
    setTimeout(function () { if (obj !== currentDropDownObj) obj.style.clip = "rect(auto auto 25px auto)"; }, 50);
    currentDropDownObj = null;
}