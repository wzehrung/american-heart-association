//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class EMR {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal EMR() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.EMR", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        internal static string Button_Back {
            get {
                return ResourceManager.GetString("Button_Back", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Continue.
        /// </summary>
        internal static string Button_Continue {
            get {
                return ResourceManager.GetString("Button_Continue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start.
        /// </summary>
        internal static string Button_Start {
            get {
                return ResourceManager.GetString("Button_Start", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        internal static string Button_Submit {
            get {
                return ResourceManager.GetString("Button_Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By clicking on this box I agree to the terms and conditions.
        /// </summary>
        internal static string Checkbox_Agree {
            get {
                return ResourceManager.GetString("Checkbox_Agree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The organizational identity code or unique access code do not match our records.  Please reenter the information and click continue or check with the clinic or hospital which provided you with access instructions and confirm your codes..
        /// </summary>
        internal static string Error_Lookup {
            get {
                return ResourceManager.GetString("Error_Lookup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the answer to your secret question.
        /// </summary>
        internal static string Text_AnswerSecretQuestion {
            get {
                return ResourceManager.GetString("Text_AnswerSecretQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Congratulations, your Heart360 account is ready to start receiving your personal health record information from your hospital or clinic.  When information is received it will not be available for viewing in your Heart360 account is approximately 4 hours..
        /// </summary>
        internal static string Text_Connection_Successful {
            get {
                return ResourceManager.GetString("Text_Connection_Successful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clinic Connect establishes a connection between Heart360 and a hospital&apos;s or clinic’s medical information system.  If you have received an email or hardcopy from your clinic or hospital with instructions for connecting, please  select the button below.  For more information on this service or to inquire if your clinic or hospital is connected to Heart360 please send an email to:  &lt;email address&gt;.
        /// </summary>
        internal static string Text_EMR_Overview {
            get {
                return ResourceManager.GetString("Text_EMR_Overview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the information given to you by your clinic:.
        /// </summary>
        internal static string Text_Enter_Information {
            get {
                return ResourceManager.GetString("Text_Enter_Information", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;PLACEHOLDER&gt;CLINIC CONNECT ESTABLISHES A CONNECTION BETWEEN HEART360 AND A HOSPITAL&apos;S OR CLINIC’S MEDICAL INFORMATION SYSTEM.  IF YOU HAVE RECEIVED AN EMAIL OR HARDCOPY FROM YOUR CLINIC OR HOSPITAL WITH INSTRUCTIONS FOR CONNECTING, PLEASE  SELECT THE BUTTON BELOW.  FOR MORE INFORMATION ON THIS SERVICE OR TO INQUIRE IF YOUR CLINIC OR HOSPITAL IS CONNECTED TO HEART360 PLEASE SEND AN EMAIL TO:  &lt;EMAIL ADDRESS&gt;.
        /// </summary>
        internal static string Text_TermsAndConditions {
            get {
                return ResourceManager.GetString("Text_TermsAndConditions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clinic Connect.
        /// </summary>
        internal static string Title_ClinicConnect {
            get {
                return ResourceManager.GetString("Title_ClinicConnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection Date.
        /// </summary>
        internal static string Title_ConnectionDate {
            get {
                return ResourceManager.GetString("Title_ConnectionDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection Key:.
        /// </summary>
        internal static string Title_ConnectionKey {
            get {
                return ResourceManager.GetString("Title_ConnectionKey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your current connections:.
        /// </summary>
        internal static string Title_CurrentConnections {
            get {
                return ResourceManager.GetString("Title_CurrentConnections", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your current EMR records:.
        /// </summary>
        internal static string Title_CurrentRecords {
            get {
                return ResourceManager.GetString("Title_CurrentRecords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Organization.
        /// </summary>
        internal static string Title_Organization {
            get {
                return ResourceManager.GetString("Title_Organization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Organization ID:.
        /// </summary>
        internal static string Title_OrganizationID {
            get {
                return ResourceManager.GetString("Title_OrganizationID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EMR.
        /// </summary>
        internal static string Title_Record {
            get {
                return ResourceManager.GetString("Title_Record", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record Date.
        /// </summary>
        internal static string Title_RecordDate {
            get {
                return ResourceManager.GetString("Title_RecordDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The answer to your secret question does not match.  Please reenter the information and accept the T&amp;C or contact the clinic or hospital which provide you with access instructions to confirm the required response to your secret question.
        /// </summary>
        internal static string Validation_Challenge_Mismatch {
            get {
                return ResourceManager.GetString("Validation_Challenge_Mismatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You must answer your secret question to continue.
        /// </summary>
        internal static string Validation_Challenge_Required {
            get {
                return ResourceManager.GetString("Validation_Challenge_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are already connected to this organization.
        /// </summary>
        internal static string Validation_Keys_AlreadyConnected {
            get {
                return ResourceManager.GetString("Validation_Keys_AlreadyConnected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Both the connection key and organization ID must be entered.
        /// </summary>
        internal static string Validation_Keys_Empty {
            get {
                return ResourceManager.GetString("Validation_Keys_Empty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The organizational identity code or unique access code do not match our records.  Please reenter the information and click continue or check with the clinic or hospital which provided you with access instructions and confirm your codes.
        /// </summary>
        internal static string Validation_Keys_Mismatch {
            get {
                return ResourceManager.GetString("Validation_Keys_Mismatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select the terms and conditions check box, reenter your response to your secret question, and select the submit button.
        /// </summary>
        internal static string Validation_Terms_Accept {
            get {
                return ResourceManager.GetString("Validation_Terms_Accept", resourceCulture);
            }
        }
    }
}
