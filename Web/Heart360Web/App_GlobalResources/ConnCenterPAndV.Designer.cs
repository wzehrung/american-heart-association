//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ConnCenterPAndV {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ConnCenterPAndV() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.ConnCenterPAndV", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heart360 Patient/Participant Disconnection Notice.
        /// </summary>
        internal static string Email_Subject_Disconnect_Provider {
            get {
                return ResourceManager.GetString("Email_Subject_Disconnect_Provider", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invitation from ##PATIENT_NAME##.
        /// </summary>
        internal static string Email_Subject_Invite_Provider {
            get {
                return ResourceManager.GetString("Email_Subject_Invite_Provider", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Agreeing to the terms of use and clicking on the grant access link will enable your
        ///                                    provider view your health information. You can view the provider profile before
        ///                                    granting access. You can also deny access to your health record if you wish..
        /// </summary>
        internal static string Patient_AcceptInvitation_AgreeToTerms {
            get {
                return ResourceManager.GetString("Patient_AcceptInvitation_AgreeToTerms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BY ACCEPTING THIS INVITATION, YOU ARE ALLOWING THE PERSONS WHO SENT YOU THIS INVITATION
        ///                                            TO HAVE ACCESS TO YOUR PERSONAL AND MEDICAL INFORMATION IN YOUR
        ///                                            ##RegisteredMicrosoft##
        ///                                            ##TrademarkedHealthvault##
        ///                                            ACCOUNT RELATED TO THE AHA’S
        ///                                            Heart360&lt;sup&gt;&amp;#174;&lt;/sup&gt;
        ///                             [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Patient_AcceptInvitation_TermsOfUse {
            get {
                return ResourceManager.GetString("Patient_AcceptInvitation_TermsOfUse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connect with Patient/Participant.
        /// </summary>
        internal static string Patient_ConnectPatient {
            get {
                return ResourceManager.GetString("Patient_ConnectPatient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please sign in or register to use Heart360&lt;sup&gt;&amp;#174;&lt;/sup&gt; in order to complete the process
        ///                                        of accepting your patient/participant invitation..
        /// </summary>
        internal static string Patient_ConnectPatient_GuidingText {
            get {
                return ResourceManager.GetString("Patient_ConnectPatient_GuidingText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invitation code is either invalid or already used.
        /// </summary>
        internal static string Patient_Invalid_Code {
            get {
                return ResourceManager.GetString("Patient_Invalid_Code", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are already connected to this provider/volunteer..
        /// </summary>
        internal static string Patient_Invitation_AlreadyConnected {
            get {
                return ResourceManager.GetString("Patient_Invitation_AlreadyConnected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;p&gt;
        ///                &lt;br /&gt;
        ///                Your patient,
        ///                ##ThankYouName##, has invited you to join
        ///                Heart360&lt;sup&gt;&amp;#174;&lt;/sup&gt;. This is a free online cardiovascular
        ///                health management tool from the American Heart Association.&lt;/p&gt;
        ///            &lt;p&gt;
        ///                Heart360&lt;sup&gt;&amp;#174;&lt;/sup&gt;
        ///                helps patients with tools that help them easily monitor and track their blood pressure,
        ///                blood glucose, cholesterol, weight, physical activ [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Patient_Invitation_PrintInvite {
            get {
                return ResourceManager.GetString("Patient_Invitation_PrintInvite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BY INVITING YOUR PROVIDER TO THE AHA’S
        ///                    Heart360&lt;sup&gt;&amp;#174;&lt;/sup&gt;
        ///                    TOOL, YOU ARE ALLOWING THE PERSON YOU HAVE &lt;u&gt;IDENTIFIED&lt;/u&gt; TO HAVE ACCESS TO YOUR
        ///                    PERSONAL AND MEDICAL INFORMATION IN YOUR
        ///                    ##RegisteredMicrosoft##
        ///                    ##TrademarkedHealthvault##
        ///                    ACCOUNT RELATED TO
        ///                    Heart360&lt;sup&gt;&amp;#174;&lt;/sup&gt;. DO NOT CLICK “SUBMIT” UNLESS
        ///                    YOU HAVE CONFIRMED THE NAME  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Patient_Invitation_TermsOfUse {
            get {
                return ResourceManager.GetString("Patient_Invitation_TermsOfUse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please specify an invitation code.
        /// </summary>
        internal static string Patient_Unspecified_Code {
            get {
                return ResourceManager.GetString("Patient_Unspecified_Code", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please specify at least one search field..
        /// </summary>
        internal static string Patient_Unspecified_SearchField {
            get {
                return ResourceManager.GetString("Patient_Unspecified_SearchField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grant Provider/Volunteer Access.
        /// </summary>
        internal static string Provider_ConnectPatient {
            get {
                return ResourceManager.GetString("Provider_ConnectPatient", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you are an existing user please click sign in to login and complete the invitation
        ///                                    process or if you are a new user click register to register for Heart360&lt;sup&gt;&amp;#174;&lt;/sup&gt;
        ///                                    and complete the invitation process..
        /// </summary>
        internal static string Provider_ConnectPatient_GuidingText {
            get {
                return ResourceManager.GetString("Provider_ConnectPatient_GuidingText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter provider/volunteer code.
        /// </summary>
        internal static string Provider_EnterProviderCode {
            get {
                return ResourceManager.GetString("Provider_EnterProviderCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Provider/Volunteer code.
        /// </summary>
        internal static string Provider_ProviderCode {
            get {
                return ResourceManager.GetString("Provider_ProviderCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PROVIDERS &amp;amp; VOLUNTEERS.
        /// </summary>
        internal static string Text_Accordion_Title {
            get {
                return ResourceManager.GetString("Text_Accordion_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I AGREE TO THE DISCLAIMER.
        /// </summary>
        internal static string Text_Agree_Disclaimer {
            get {
                return ResourceManager.GetString("Text_Agree_Disclaimer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your provider/volunteer may have given you an invitation code, please enter it here..
        /// </summary>
        internal static string Text_ByCode_Description {
            get {
                return ResourceManager.GetString("Text_ByCode_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invite your provider/volunteer by email..
        /// </summary>
        internal static string Text_ByEmail_Description {
            get {
                return ResourceManager.GetString("Text_ByEmail_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Have an email sent to you when your provider accepts your invitation..
        /// </summary>
        internal static string Text_ByEmail_Receipt {
            get {
                return ResourceManager.GetString("Text_ByEmail_Receipt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your provider/volunteer may already be using Heart360. Enter their details to search for them..
        /// </summary>
        internal static string Text_ByName_Description {
            get {
                return ResourceManager.GetString("Text_ByName_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invite your provider/volunteer by printing an invitation..
        /// </summary>
        internal static string Text_ByPrint_Description {
            get {
                return ResourceManager.GetString("Text_ByPrint_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CONNECT.
        /// </summary>
        internal static string Text_Connect {
            get {
                return ResourceManager.GetString("Text_Connect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If you did not find your provider, close window and try an alternative search.
        /// </summary>
        internal static string Text_ConnectionsPopup_Footer {
            get {
                return ResourceManager.GetString("Text_ConnectionsPopup_Footer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CONGRATULATIONS.
        /// </summary>
        internal static string Text_ConnectionsPopup_Title_Congrats {
            get {
                return ResourceManager.GetString("Text_ConnectionsPopup_Title_Congrats", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CONNECT TO MY PROVIDER OR VOLUNTEER.
        /// </summary>
        internal static string Text_ConnectionsPopup_Title_Connect {
            get {
                return ResourceManager.GetString("Text_ConnectionsPopup_Title_Connect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You can connect directly and securely with your healthcare provider or volunteer to allow them to review your health information online. Once connected, your provider or volunteer will be able to review your latest readings and send messages directly to you..
        /// </summary>
        internal static string Text_Connections_Overview {
            get {
                return ResourceManager.GetString("Text_Connections_Overview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Once you have printed and given the invitation to your provider, they will need to follow the instructions detailed on the invitation before you will be connected..
        /// </summary>
        internal static string Text_Connections_Overview_ByPrint {
            get {
                return ResourceManager.GetString("Text_Connections_Overview_ByPrint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your connection request has been sent to your provider/volunteer.  They will need to accept the request before you will see them listed under Providers &amp; Volunteers..
        /// </summary>
        internal static string Text_Connections_Request {
            get {
                return ResourceManager.GetString("Text_Connections_Request", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have successfully disconnected from.
        /// </summary>
        internal static string Text_DeleteProviderSuccess_Description {
            get {
                return ResourceManager.GetString("Text_DeleteProviderSuccess_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DISCONNECTED.
        /// </summary>
        internal static string Text_DeleteProviderSuccess_Title {
            get {
                return ResourceManager.GetString("Text_DeleteProviderSuccess_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You want to disconnect from.
        /// </summary>
        internal static string Text_DeleteProvider_Description {
            get {
                return ResourceManager.GetString("Text_DeleteProvider_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ARE YOU SURE?.
        /// </summary>
        internal static string Text_DeleteProvider_Title {
            get {
                return ResourceManager.GetString("Text_DeleteProvider_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DENY PROVIDER_ACCESS.
        /// </summary>
        internal static string Text_DenyProviderAccess {
            get {
                return ResourceManager.GetString("Text_DenyProviderAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DISCLAIMER.
        /// </summary>
        internal static string Text_Disclaimer {
            get {
                return ResourceManager.GetString("Text_Disclaimer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DO NOT ALLOW ACCESS.
        /// </summary>
        internal static string Text_DoNotAllowAccess {
            get {
                return ResourceManager.GetString("Text_DoNotAllowAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter Invitation Code.
        /// </summary>
        internal static string Text_EnterInvitationCode {
            get {
                return ResourceManager.GetString("Text_EnterInvitationCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ERROR.
        /// </summary>
        internal static string Text_Error {
            get {
                return ResourceManager.GetString("Text_Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A first name is required.
        /// </summary>
        internal static string Text_FirstNameRequired {
            get {
                return ResourceManager.GetString("Text_FirstNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An internal error occured that prevented this operation from completing..
        /// </summary>
        internal static string Text_GenericErrorDescription {
            get {
                return ResourceManager.GetString("Text_GenericErrorDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GRANT ACCESS.
        /// </summary>
        internal static string Text_GrantAccess {
            get {
                return ResourceManager.GetString("Text_GrantAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GRANT PROVIDER ACCESS.
        /// </summary>
        internal static string Text_GrantProviderAccess {
            get {
                return ResourceManager.GetString("Text_GrantProviderAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid provider/volunteer code or invitation code.
        /// </summary>
        internal static string Text_InvalidCode {
            get {
                return ResourceManager.GetString("Text_InvalidCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The email address is not correctly formatted..
        /// </summary>
        internal static string Text_InvalidEmailFormat {
            get {
                return ResourceManager.GetString("Text_InvalidEmailFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to InvitationID is either invalid or used!.
        /// </summary>
        internal static string Text_InvalidID {
            get {
                return ResourceManager.GetString("Text_InvalidID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invitation Code.
        /// </summary>
        internal static string Text_InvitationCode {
            get {
                return ResourceManager.GetString("Text_InvitationCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invitation code is invalid or already used.
        /// </summary>
        internal static string Text_InvitationCodeAlreadyUsed {
            get {
                return ResourceManager.GetString("Text_InvitationCodeAlreadyUsed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your connection request has been sent to your provider/volunteer.
        /// </summary>
        internal static string Text_Invitation_Sent {
            get {
                return ResourceManager.GetString("Text_Invitation_Sent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INVITE.
        /// </summary>
        internal static string Text_Invite {
            get {
                return ResourceManager.GetString("Text_Invite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INVITE A PROVIDER OR VOLUNTEER.
        /// </summary>
        internal static string Text_Invite_PV {
            get {
                return ResourceManager.GetString("Text_Invite_PV", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A last name is required.
        /// </summary>
        internal static string Text_LastNameRequired {
            get {
                return ResourceManager.GetString("Text_LastNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have not granted access to a new provider, No changes have been made to your
        ///                                    provider.
        /// </summary>
        internal static string Text_PatientInvitation_DeniedAccess {
            get {
                return ResourceManager.GetString("Text_PatientInvitation_DeniedAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No changes have been made to your provider/volunteer.&lt;br /&gt;
        ///                                    The pending request will remain in your alerts.
        /// </summary>
        internal static string Text_PatientInvitation_RemindLater {
            get {
                return ResourceManager.GetString("Text_PatientInvitation_RemindLater", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PREVIEW.
        /// </summary>
        internal static string Text_Preview {
            get {
                return ResourceManager.GetString("Text_Preview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PROCEED.
        /// </summary>
        internal static string Text_Proceed {
            get {
                return ResourceManager.GetString("Text_Proceed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I HAVE READ AND AGREE TO THE TERMS AND CONDITIONS LISTED ABOVE.
        /// </summary>
        internal static string Text_Read_And_Agree {
            get {
                return ResourceManager.GetString("Text_Read_And_Agree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to REGISTER.
        /// </summary>
        internal static string Text_Register {
            get {
                return ResourceManager.GetString("Text_Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to REMIND ME LATER.
        /// </summary>
        internal static string Text_RemindMeLater {
            get {
                return ResourceManager.GetString("Text_RemindMeLater", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SIGN IN.
        /// </summary>
        internal static string Text_SignIn {
            get {
                return ResourceManager.GetString("Text_SignIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have successfully granted access to.
        /// </summary>
        internal static string Text_SuccessfullyGranted {
            get {
                return ResourceManager.GetString("Text_SuccessfullyGranted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The email addresses do not match.
        /// </summary>
        internal static string Text_UnmatchedEmails {
            get {
                return ResourceManager.GetString("Text_UnmatchedEmails", resourceCulture);
            }
        }
    }
}
