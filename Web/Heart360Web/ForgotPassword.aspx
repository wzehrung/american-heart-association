﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PatientPopup.master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Heart360Web.ForgotPassword" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <!-- class was modal-wrap cpopuptracker page-blue -->
    <div id="divPopupTrackerStyle" runat="server" visible="false" >
        <div class="row" >
            <div class="large-12 small-12 columns spt">
                <asp:LinkButton ID="btnLinkClose" CssClass="close-modal fr spr" runat="server" Text="CLOSE" />
            </div>
        </div>
    </div>

    <div class="full">
	    <div class="large-12 small-12 columns">
		    <h3>Forgot Password</h3>
	    </div>

	    <div class="large-12 small-12 columns spb">
		    <p><label id="lblDescription" runat="server"><b>To update your password and login to Heart360 you will need to update your password using HealthVault. 
            This can be done by clicking the Continue button below and then selecting the "Can't access your account?" link on the HealthVault sign in page.</b></label></p>
	    </div>

        <div class="large-6 small-6 columns fr" >
            <asp:Button ID="btnOk" CssClass="gray-button full" OnClick="btnOk_Click" runat="server" Text="<%$Resources:Common,Text_Continue%>" />
        </div>
    </div>

</asp:Content>