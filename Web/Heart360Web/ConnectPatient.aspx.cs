﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using AHAHelpContent;
using AHAAPI;

namespace Heart360Web
{
    public partial class ConnectPatient : System.Web.UI.Page
    {
        #region Private Members

        private enum View
        {
            EnterInvitationCode,
            Visitor,
            Invalid,
        }

        private void _SwitchView(View view)
        {
            mvAcceptInvitation.ActiveViewIndex = (int)view;
        }

        private PatientProviderInvitation CurrentInvitation
        {
            get;
            set;
        }

        private AHAHelpContent.ProviderDetails CurrentProvider
        {
            get;
            set;
        }

        private void _UpdateInvitedProvider()
        {
            bool bAlreadyConnected = ProviderDetails.IsValidPatientForProvider(this.CurrentInvitation.PatientID.Value, this.CurrentProvider.ProviderID);

            PatientProviderInvitationDetails objAvailableDetails = CurrentInvitation.PendingInvitationDetails.First();

            objAvailableDetails.AcceptedProviderID = this.CurrentProvider.ProviderID;

            if (bAlreadyConnected)
                objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Blocked;
            else
            {
                //Marks the existing invitations, sent by this patient to the current provider, as blocked
                PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(this.CurrentInvitation.PatientID.Value, this.CurrentProvider.ProviderID, false);
             
                objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Pending;
            }

            objAvailableDetails.Update();

            Response.Redirect(AHACommon.AHAAppSettings.ProviderPortal);
        }

        private void _ProcessInvitation()
        {
            if (this.CurrentInvitation != null)
            {
                AHAAPI.Provider.ProviderContext objContext = AHAAPI.Provider.SessionInfo.GetProviderContext();

                if (objContext == null || !AHAAPI.Provider.SessionInfo.GetProviderContext().IsLoggedIn)
                {
                    _SwitchView(View.Visitor);
                    return;
                }

                if (!CurrentInvitation.IsSendByProvider)
                {
                    _UpdateInvitedProvider();
                }
                else
                    _SwitchView(View.Invalid);
            }
            else
            {
                _SwitchView(View.Invalid);
            }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            AHAAPI.Provider.ProviderContext objContext = AHAAPI.Provider.SessionInfo.GetProviderContext();

            if (objContext == null || !AHAAPI.Provider.SessionInfo.GetProviderContext().IsLoggedIn)
            {
                this.MasterPageFile = "~/MasterPages/Visitor.Master";
            }
            else
            {
                this.CurrentProvider = AHAHelpContent.ProviderDetails.FindByProviderID(objContext.ProviderID);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AHAAPI.Provider.ProviderContext objContext = AHAAPI.Provider.SessionInfo.GetProviderContext();

            string strInvitationCode = Request["invitationid"];
            if (!string.IsNullOrEmpty(strInvitationCode))
            {
                this.CurrentInvitation = PatientProviderInvitation.FindPendingInvitationByInvitationCode(strInvitationCode);
                _ProcessInvitation();
            }
            else
            {
                _SwitchView(View.EnterInvitationCode);
                if (IsPostBack)
                {
                    this.CurrentInvitation = PatientProviderInvitation.FindPendingInvitationByInvitationCode(txtInvitationCode.Text);
                }
            }

        }

        protected void OnSignIn(object sender, EventArgs e)
        {
            //encode the invitation ID into the query string and redirect to the provider portal SignIn page
            Response.Redirect(AHACommon.AHAAppSettings.ProviderPortal + AHACommon.AHAAppSettings.ProviderLogin 
                + "&invitationid=" + this.CurrentInvitation.InvitationCode);
            Response.End();
        }

        protected void OnRegister(object sender, EventArgs e)
        {
            //encode the invitation ID into the query string and redirect to the provider portal SignUp page
            Response.Redirect(AHACommon.AHAAppSettings.ProviderPortal + AHACommon.AHAAppSettings.ProviderRegister 
                + "&invitationid=" + this.CurrentInvitation.InvitationCode);
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            Page.Validate("Invite");
            if (!Page.IsValid)
                return;

            _ProcessInvitation();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        #endregion

        #region Validators

        protected void OnValidateCode(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = true;
            if (!string.IsNullOrEmpty(txtInvitationCode.Text))
            {
                this.CurrentInvitation = PatientProviderInvitation.FindPendingInvitationByInvitationCode(txtInvitationCode.Text);
                if (this.CurrentInvitation == null || this.CurrentInvitation.IsSendByProvider)
                {
                    e.IsValid = false;
                }
            }
        }

        #endregion
    }
}
