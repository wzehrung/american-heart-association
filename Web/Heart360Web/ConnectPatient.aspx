﻿<%@ Page Language="C#" Title="Connect to Patient" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" CodeBehind="ConnectPatient.aspx.cs" Inherits="Heart360Web.ConnectPatient" %>

<%@ Register Src="~/Controls/Provider/GuidingText.ascx" TagName="GuidingText" TagPrefix="ProviderCtrls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div id="mainContent" class="row spb" >
        <section class="full info-stats" >
            <div class="title" >
                <h4><%=GetGlobalResourceObject("ConnCenterPAndV", "Patient_ConnectPatient")%> </h4>
            </div>
        
            <div class="large-12 small-12 columns" >
                <asp:ValidationSummary 
                    HeaderText="<%$ Resources:Common, Text_ValidationMessage %>" 
                    ValidationGroup="Invite"
                    Enabled="true" 
                    ID="ValidationSummary1" 
                    runat="server" 
                    DisplayMode="BulletList"
                    ShowSummary="true" 
                    ShowMessageBox="true" 
                    CssClass="validation-error" />
            </div>
            <asp:MultiView ID="mvAcceptInvitation" runat="server">
                <asp:View ID="vEmptyInvitationCode" runat="server">
                    <div class="large-12 small-12 columns" >
                        <p>
                            <ProviderCtrls:GuidingText ID="guidingtext" runat="server" ZoneTitle="ConnectWithPatient" />
                        </p>
                        <div class="row spt">
                            <div class="large-3 small-12 columns" ></div>
                            <div class="large-6 small-12 columns" >
                                <label class="text-left" for="invitecode"><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_InvitationCode")%></label>
                                <asp:TextBox ID="txtInvitationCode" runat="server" MaxLength="6" />
                                <asp:RequiredFieldValidator ID="rfvIC" runat="server" ControlToValidate="txtInvitationCode"
                                                            Display="None" EnableClientScript="false" ValidationGroup="Invite" ErrorMessage="<%$ Resources:ConnCenterPAndV, Text_EnterInvitationCode %>" />
                                <asp:CustomValidator ID="custValidEmail" runat="server" Display="None" EnableClientScript="false"
                                                         ValidationGroup="Invite" OnServerValidate="OnValidateCode" ErrorMessage="<%$ Resources:ConnCenterPAndV, Patient_Invalid_Code %>" />
                            </div>
                            <div class="large-3 small-12 columns" ></div>
                        </div>

                        <div class="row spt">
                            <div class="large-3 small-3 columns fl" >
                                <asp:Button ID="btnProceed" CssClass="gray-button full" OnClick="btnProceed_Click" runat="server" Text="<%$Resources:ConnCenterPAndV,Text_Proceed %>" />
                            </div>
                            <div class="large-3 small-3 columns fl" >
                                <asp:Button ID="btnViewConfirmCancel" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_Cancel%>" />
                            </div>          
                        </div>
                    </div>              
                </asp:View>
                <asp:View ID="vVisitor" runat="server">
                    <div class="large-12 small-12 columns" >
                        <p><%=GetGlobalResourceObject("ConnCenterPAndV", "Patient_ConnectPatient_GuidingText")%></p>
                        <div class="row spt">
                            <div class="large-3 small-3 columns fl" >
                                <asp:LinkButton ID="lnkSignIn" runat="server" OnClick="OnSignIn" CssClass="gray-button full"><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_SignIn")%></asp:LinkButton>
                            </div>
                            <div class="large-3 small-3 columns fl" >
                                <asp:LinkButton ID="lnkRegister" runat="server" OnClick="OnRegister" CssClass="gray-button full"><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_Register")%></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="vInvalidID" runat="server">
                     <div class="large-12 small-12 columns" >
                        <p><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_InvalidID")%></p>
                    </div>
                </asp:View>
            </asp:MultiView>
        </section>           
    </div>
</asp:Content>
