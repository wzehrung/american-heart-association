﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAHelpContent;
using AHAAPI;

namespace Heart360Web
{
    public enum ConnectProviderMode
    {
        Email,
        Print
    }

    public partial class ConnectProvider : H360PageHelper
    {
        protected override bool LogOnRequired
        {
            get
            {
                return false;
            }
        }

        #region Private Members

        private enum View
        {
            EnterInvitationCode,
            Visitor,
            Invalid,
        }

        private void _SwitchView(View view)
        {
            mvAcceptInvitation.ActiveViewIndex = (int)view;
        }

        private PatientProviderInvitation CurrentInvitation
        {
            get;
            set;
        }

        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }

        private ConnectProviderMode Mode
        {
            get;
            set;
        }

        private void _UpdateInvitedPatient()
        {
            bool bAlreadyConnected = ProviderDetails.IsValidPatientForProvider(this.CurrentPatient.UserHealthRecordID, this.CurrentInvitation.ProviderID.Value);

            PatientProviderInvitationDetails objAvailableDetails = CurrentInvitation.PendingInvitationDetails.First();

            objAvailableDetails.AcceptedPatientID = this.CurrentPatient.UserHealthRecordID;

            if (!bAlreadyConnected)
            {
                //Marks the existing invitations, sent by this provider to the current patient, as blocked
                PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(this.CurrentPatient.UserHealthRecordID, this.CurrentInvitation.ProviderID.Value, true);

                objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Pending;
            }
            else
                objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Blocked;

            objAvailableDetails.Update();

            Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_HOME);
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!AHAAPI.SessionInfo.IsUserLoggedIn)
            {
                this.MasterPageFile = "~/MasterPages/Visitor.Master";
            }
            else
            {
                UserIdentityContext objContext = SessionInfo.GetUserIdentityContext();

                this.CurrentPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objContext.PatientID.Value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //
            // PJB: begin existing code from V5.....  (this is not the cleanest written code).....
            //
            if (!string.IsNullOrEmpty(Request["invitationid"]))
                this.guidingtext.ZoneTitle = AHACommon.AHADefs.ContentType.GrantProviderAccessEmail;

            string strInvitationCode = Request["invitationid"];
            Mode = ConnectProviderMode.Print;
            if (!string.IsNullOrEmpty(strInvitationCode))
            {
                txtInvitationCode.Text = strInvitationCode;
                Mode = ConnectProviderMode.Email;
            }

            if (!IsPostBack)
            {
                divInvitationCodeEntry.Style.Add(HtmlTextWriterStyle.Display, "none");
                _SwitchView(View.EnterInvitationCode);
            }
            else
            {
                strInvitationCode = txtInvitationCode.Text;
            }

            if (!string.IsNullOrEmpty(strInvitationCode))
            {
                this.CurrentInvitation = PatientProviderInvitation.FindPendingInvitationByInvitationCode(strInvitationCode);

                if (this.CurrentInvitation == null || !this.CurrentInvitation.IsSendByProvider)
                {
                    _SwitchView(View.Invalid);
                    return;
                }
            }

        }

        protected void OnSignIn(object sender, EventArgs e)
        {
            //
            // NOTE: this PatientInvitation Session info will be "processed" by the Patient's default web page on Patient portal.
            //       That, specifically, is Dashboard.aspx (after the redirects)
            //
            AHAAPI.SessionInfo.SetPatientInvitationSession(new PatientInvitation
            {
                InvitationCode = Mode == ConnectProviderMode.Email ? this.CurrentInvitation.InvitationCode : string.Empty,
                ProviderCode = Mode == ConnectProviderMode.Print ? txtProviderCode.Text : string.Empty,
                IsPatientAccept = true
            });

            Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_HOME);
            Response.End();
        }

        protected void OnRegister(object sender, EventArgs e)
        {
            AHAAPI.SessionInfo.SetPatientInvitationSession(new PatientInvitation
            {
                InvitationCode = Mode == ConnectProviderMode.Email ? this.CurrentInvitation.InvitationCode : string.Empty,
                ProviderCode = Mode == ConnectProviderMode.Print ? txtProviderCode.Text : string.Empty,
                IsPatientAccept = true
            });

            Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_SIGNUP);
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            _SwitchView(View.EnterInvitationCode);
            Page.Validate("Invite");
            if (!Page.IsValid)
            {
                return;
            }

            if (CurrentInvitation == null && Mode == ConnectProviderMode.Email)
            {
                _SwitchView(View.Invalid);
                return;
            }

            if (!AHAAPI.SessionInfo.IsUserLoggedIn)
            {
                // PJB: not sure why this was needed in V5
                //this.content.Attributes["class"] = "content";

                _SwitchView(View.Visitor);
                return;
            }

            if (Mode == ConnectProviderMode.Email)
            {
                _UpdateInvitedPatient();
            }
            else
            {
                bool bIsPossible = UIHelper.ProcessProviderCodeOnlyTypeInvitationForPatient(txtProviderCode.Text.Trim(), this.CurrentPatient.UserHealthRecordID, this.PersonGUID, this.RecordGUID);
                if (bIsPossible)
                {
                    Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_HOME);
                    Response.End();
                }
                else
                {
                    _SwitchView(View.Invalid);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        #endregion

        #region Validators

        protected void OnValidateCodes(object sender, ServerValidateEventArgs e)
        {
            CustomValidator validator = sender as CustomValidator;

            e.IsValid = true;

            if (Mode == ConnectProviderMode.Email)
            {
                if (!string.IsNullOrEmpty(txtInvitationCode.Text.Trim()))
                {
                    this.CurrentInvitation = PatientProviderInvitation.FindPendingInvitationByInvitationCode(txtInvitationCode.Text);

                    if (this.CurrentInvitation == null || !this.CurrentInvitation.IsSendByProvider)
                    {
                        e.IsValid = false;
                        validator.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_InvitationCodeAlreadyUsed").ToString();
                        return;
                    }

                    if (!string.IsNullOrEmpty(txtProviderCode.Text.Trim()))
                    {
                        e.IsValid = true;

                        if (ProviderDetails.FindByProviderID(this.CurrentInvitation.ProviderID.Value).ProviderCode != txtProviderCode.Text)
                        {
                            e.IsValid = false;
                            validator.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_InvalidCode").ToString();

                        }
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtProviderCode.Text.Trim()))
                {
                    e.IsValid = true;

                    if (ProviderDetails.FindByProviderCode(txtProviderCode.Text) == null)
                    {
                        e.IsValid = false;
                        validator.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_InvalidCode").ToString();

                    }
                }
            }
        }

        #endregion
    }
}