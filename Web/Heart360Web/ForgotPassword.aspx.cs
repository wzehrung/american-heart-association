﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        private string mCloseUrl = string.Empty;       // If non-null, this page is not displayed in an iframe

        public string CloseMeButton
        {
            get { return "btnPpeSummaryProxyOk" ; }
        }
        public string CancelMeButton
        {
            get { return "btnPpeSummaryCancel"; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {

            if (Request.Params["closeurl"] != null)
            {
                mCloseUrl = Request.Params["closeurl"];
            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(mCloseUrl))
                {
                    divPopupTrackerStyle.Attributes.Add("class", "popup-wrap page-red");
                    divPopupTrackerStyle.Visible = true;
                    btnLinkClose.PostBackUrl = mCloseUrl;
                }
            }
        }

        public void CancelPopup()
        {
            if (string.IsNullOrEmpty(mCloseUrl))    // contents in an iframe
            {
                if (!Page.ClientScript.IsStartupScriptRegistered("CANCEL ME"))
                {
                    string script = "window.parent.document.getElementById('" + this.CancelMeButton + "').click();";
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "CANCEL ME", script, true);
                }
            }
            else  // contents in browser
            {
                Response.Redirect(mCloseUrl);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {

            mCloseUrl = AHACommon.AHAAppSettings.PatientForgotPasswordURL;

            if (string.IsNullOrEmpty(mCloseUrl))    // contents in an iframe
            {
                if (!Page.ClientScript.IsStartupScriptRegistered("CLOSE ME"))
                {
                    string script = "window.parent.document.getElementById('" + this.CloseMeButton + "').click();";
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "CLOSE ME", script, true);
                }
            }
            else  // contents in browser
            {
                Response.Redirect(AHACommon.AHAAppSettings.PatientForgotPasswordURL);
            }

        }
    }
}