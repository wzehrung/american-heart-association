﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AHAAPI;
using System.Text.RegularExpressions;

namespace Heart360Web
{
    public partial class FileNotFound : H360PageHelper
    {
        protected override bool LogOnRequired
        {
            get
            {
                return false;
            }
        }

        protected void Page_PreInit(object sender, System.EventArgs e)
        {
            if (SessionInfo.IsUserLoggedIn)
            {
                this.MasterPageFile = "~/MasterPages/Patient.Master";
            }
            //else if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
            //{
            //    this.MasterPageFile = "~/MasterPages/Provider.Master";
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string strLitErrorText = GetGlobalResourceObject("Common", "Error_Message_Persists").ToString();
            strLitErrorText = Regex.Replace(strLitErrorText, "##supportmail##", AHACommon.AHAAppSettings.SupportEmail);

            litError.Text = strLitErrorText;
            string strLitLinkText = GetGlobalResourceObject("Common", "Error_Message_Details").ToString();
            strLitLinkText = Regex.Replace(strLitLinkText, "##requestsource##", Request["Source"]);

            litLinkText.Text = strLitLinkText;
            System.Web.HttpContext.Current.Response.StatusCode = 404;

            if (!SessionInfo.IsUserLoggedIn && !AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
            {
                divContent.Attributes.Add("class", "content");
            }
        }
    }
}