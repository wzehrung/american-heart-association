﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" CodeBehind="TermsOfUse.aspx.cs" Inherits="Heart360Web.TermsOfUse" %>

<asp:Content ID="ContentMain" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div id="main-content" class="row terms">

        <div class="termsofuse-content">
            <%=GetGlobalResourceObject("Common", "Text_TermsOfUse")%> 
        </div>

        <div class="large-12 small-12 columns">
            <p><%=strFooter%></p>
        </div>

    </div>

</asp:Content>