﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using AHAHelpContent;

namespace Heart360Web
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class ImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            string header = context.Request.Headers["If-Modified-Since"];

            if (header != null)
            {
                context.Response.StatusCode = 304;
                context.Response.SuppressContent = true;
                return;
            }

            if (!string.IsNullOrEmpty(context.Request["GID"]))
            {
                AHAHelpContent.GrantSupport objGrantSupport = AHAHelpContent.GrantSupport.GetGrantSupportByID(Convert.ToInt32(context.Request["GID"]), AHAAPI.H360Utility.GetCurrentLanguageID());
                if (objGrantSupport.Logo != null)
                {
                    context.Response.Clear();
                    context.Response.Buffer = false;
                    context.Response.Cache.SetExpires(DateTime.Now.AddDays(30));
                    context.Response.Cache.SetMaxAge(new TimeSpan(30, 0, 0, 0));
                    context.Response.Cache.SetLastModified(DateTime.Now);
                    context.Response.Cache.SetCacheability(HttpCacheability.Public);
                    context.Response.Cache.SetValidUntilExpires(false);

                    context.Response.AddHeader("Content-Length", objGrantSupport.Logo.Length.ToString());
                    context.Response.ContentType = objGrantSupport.ContentType;
                    context.Response.AddHeader("content-disposition", "inline; filename=Grant_Provider_" + objGrantSupport.Version);

                    context.Response.BinaryWrite(objGrantSupport.Logo);
                    context.Response.End();
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}