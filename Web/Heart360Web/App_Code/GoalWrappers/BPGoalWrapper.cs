﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Heart360Web
{
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using AHAAPI;
    using System.Collections.Generic;

    public enum BPGoalStatus
    {
        Achieved,
        InProgress,
        Expired,
        NotSet
    }

    public class BPGoalWrapper
    {
        public static BPGoalStatus BPGoalStatus
        {
            get
            {
                if (HVHelper.HVManagerPatientBase.BPGoalDataManager.Item != null)
                {
                    HVManager.BloodPressureDataManager.BPItem objBPItem = BloodPressureWrapper.GetLatestBP();
                    if (objBPItem == null)
                    {
                        return BPGoalStatus.InProgress;
                    }
                    else
                    {
                        if (objBPItem.Systolic.Value <= HVHelper.HVManagerPatientBase.BPGoalDataManager.Item.TargetSystolic.Value && objBPItem.Diastolic.Value <= HVHelper.HVManagerPatientBase.BPGoalDataManager.Item.TargetDiastolic.Value)
                        {
                            return BPGoalStatus.Achieved;
                        }
                        else
                        {
                            return BPGoalStatus.InProgress;
                        }
                    }
                }

                return BPGoalStatus.NotSet;
            }
        }
    }
}
