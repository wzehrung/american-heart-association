﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Heart360Web
{
    using HVManager;
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Collections.Generic;

    public enum ExerciseGoalStatus
    {
        Achieved,
        InProgress,
        NotSet
    }

    public class ExerciseGoalWrapper
    {
        public static ExerciseGoalStatus ExerciseGoalStatus
        {
            get
            {
                ExerciseDataManager.ExerciseItem objItem = ExerciseDataWrapper.GetLatestExerciseData();
                if (objItem == null)
                {
                    return ExerciseGoalStatus.NotSet;
                }

                int iSuccessfulDayCount = 0;

                //past 7 days including today
                DateTime startDate = DateTime.Today.AddDays(-6);
                DateTime endDate = DateTime.Today;

                LinkedList<ExerciseDataManager.ExerciseItem> objMasterList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataBetweenDates(startDate, endDate);

                DateTime dtTemp = endDate;
                while (dtTemp >= startDate)
                {
                    List<ExerciseDataManager.ExerciseItem> objList = objMasterList.Where(i => i.EffectiveDate.Date.CompareTo(dtTemp.Date) == 0).ToList();

                    if (_HasGoalAttainedOnDate(objList))
                    {
                        iSuccessfulDayCount++;
                    }

                    dtTemp = dtTemp.AddDays(-1);
                }

                if (iSuccessfulDayCount >= 5)
                {
                    return ExerciseGoalStatus.Achieved;
                }
                else
                {
                    return ExerciseGoalStatus.InProgress;
                }
            }
        }


        private static bool _HasGoalAttainedOnDate(List<ExerciseDataManager.ExerciseItem> objList)
        {
            double dTotalDuration = 0;
            foreach (ExerciseDataManager.ExerciseItem objItem in objList)
            {
                if (objItem.Duration.Value.HasValue)
                {
                    dTotalDuration += objItem.Duration.Value.Value;
                }
            }

            if (dTotalDuration >= 30)
                return true;

            return false;
        }
    }
}
