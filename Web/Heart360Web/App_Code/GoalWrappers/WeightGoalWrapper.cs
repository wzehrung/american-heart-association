﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Heart360Web
{
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using HVManager;
    using System.Collections.Generic;

    public enum WeightGoalStatus
    {
        Achieved,
        InProgress,
        Expired,
        NotSet
    }

    public class WeightGoalWrapper
    {
        public static WeightGoalStatus WeightGoalStatus
        {
            get
            {
                if (HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item != null)
                {
                    if (HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item.TargetDate.Value < DateTime.Now)
                    {
                        return WeightGoalStatus.Expired;
                    }
                    else
                    {
                        WeightDataManager.WeightItem objWeightDataItem = MyWeightWrapper.GetLatestWeight();
                        if (objWeightDataItem == null)
                        {
                            return WeightGoalStatus.InProgress;
                        }
                        else
                        {
                            if (Math.Round(objWeightDataItem.CommonWeight.Value.ToPounds(), 2) <= HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item.TargetValue.Value)
                            {
                                return WeightGoalStatus.Achieved;
                            }
                            else
                            {
                                return WeightGoalStatus.InProgress;
                            }
                        }
                    }
                }

                return WeightGoalStatus.NotSet;
            }
        }
    }
}
