﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Heart360Web
{
    using Microsoft.Health;
    using Microsoft.Health.ItemTypes;
    using System.Collections.Generic;

    public enum CholesterolGoalStatus
    {
        Achieved,
        InProgress,
        Expired,
        NotSet
    }

    public class CholesterolGoalWrapper
    {
        public static CholesterolGoalStatus CholesterolGoalStatus
        {
            get
            {
                if (HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item != null)
                {
                    HVManager.CholesterolDataManager.CholesterolItem objCholesterolItem = CholestrolWrapper.GetLatestCholesterol();
                    if (objCholesterolItem == null)
                    {
                        return CholesterolGoalStatus.InProgress;
                    }
                    else
                    {
                        if (objCholesterolItem.HDL.Value.HasValue && objCholesterolItem.HDL.Value <= HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item.TargetHDL.Value
                            && objCholesterolItem.LDL.Value.HasValue && objCholesterolItem.LDL.Value <= HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item.TargetLDL.Value
                            && objCholesterolItem.TotalCholesterol.Value.HasValue && objCholesterolItem.TotalCholesterol.Value <= HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item.TargetTotalCholesterol.Value
                            && objCholesterolItem.Triglycerides.Value.HasValue && objCholesterolItem.Triglycerides.Value <= HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item.TargetTriglyceride.Value)
                        {
                            return CholesterolGoalStatus.Achieved;
                        }
                        else
                        {
                            return CholesterolGoalStatus.InProgress;
                        }
                    }
                }

                return CholesterolGoalStatus.NotSet;
            }
        }
    }
}
