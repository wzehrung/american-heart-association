﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Drawing;
using NPlot;
using AHAAPI;
using System.Text;
using System.Collections;


namespace Heart360Web.Graph
{

    /// <summary>
    /// This is used to store the mouse over popup data. The image map for the
    /// graph uses these instances
    /// </summary>
    public class GraphPoint
    {
        public float x;
        public float y;
        public string data;
    }


    /// <summary>
    /// This abstracts one reading that will be represented in the graph
    /// </summary>
    public class GraphInputReading
    {
        private DateTime _dateOfReading;
        private float _readingValue;

        public float ReadingValue
        {
            get
            {
                return _readingValue;
            }

            set
            {
                _readingValue = value;
            }
        }


        public DateTime DateOfReading
        {
            get
            {
                return _dateOfReading;
            }

            set
            {
                _dateOfReading = value;
            }
        }


        private string _displayValue;
        public void SetDisplayValueForGraph(string displayValue)
        {
            _displayValue = displayValue;
        }

        public string DisplayValue
        {
            get
            {
                if (_displayValue != null)
                    return _displayValue;

                return _readingValue.ToString();
            }
        }

    }


    /// <summary>
    /// This represents if the shaded region is above threshold (for ex systolic) or below threshold for ex (HDL)
    /// </summary>
    public enum ThresholdDirection { ShadeHigher, ShadeLower };


    /// <summary>
    /// This class represents a shaded threshold region in the graph
    /// </summary>
    public class ThresholdRegion
    {
        //public static Color DefaultShadingColor = Color.FromArgb(0xDC, 0xEA, 0xF5); //DCEAF5 is default
        public static Color DefaultShadingColor = Color.FromArgb(100, 0xFF, 0x99, 0xA7);

        public ThresholdDirection m_direction;
        public float m_thresholdValue;
        public Color m_shadingColor;
        public string m_regionText;
        public string m_legendText;

        public ThresholdRegion(ThresholdDirection direction, float value, Color color, string regionText, string legendText)
        {
            m_direction = direction;
            m_thresholdValue = value;
            m_shadingColor = color;
            m_regionText = regionText;
            m_legendText = legendText;
        }
    }

    /// <summary>
    /// These are the various graph types Heart360 supports.
    /// </summary>
    public enum GraphType
    {
        Glucose,
        PressureSystolic,
        PressureDiastolic,
        TotalCholesterol,
        LDL,
        HDL,
        Triglycerides,
        Weight,
        CombinedBP,
        CombinedCholesterol,
        Exercise,
        SPRGlucose,
        SPRCombinedBP,
        SPRCombinedCholesterol,
        SPRExercise,
        SPRWeight,
        AllInOne
    };


    [Flags]
    public enum GraphGroupType
    {
        GlucoseGroup = 1,
        PressureGroup = 2,
        CholesterolGroup = 4,
        WeightGroup = 8
    };


    /// <summary>
    /// This class represents the colors for different Graph Types
    /// </summary>
    public class AHAGraphColor
    {
        //public static Color Systolic = Color.FromArgb(0xAE, 0xAC, 0xFF);
        //public static Color SystolicGoal = Color.FromArgb(0xAE, 0xAC, 0xFF);
        //public static Color SystolicGoalShading = Color.FromArgb(0xE1, 0xE0, 0xFF);

        //public static Color Diastolic = Color.FromArgb(0xF6, 0xB0, 0xF6);
        //public static Color DiastolicGoal = Color.FromArgb(0xF6, 0xB0, 0xF6);
        //public static Color DiastolicGoalShading = Color.FromArgb(0xFD, 0xDE, 0xFD);

        //changed by arun to make the bp graph darker for aha, uat nov 10, 2009

        public static Color Systolic = Color.FromArgb(0x9E, 0xAC, 0xFF);
        public static Color SystolicGoal = Color.FromArgb(0x9E, 0xAC, 0xFF);
        public static Color SystolicGoalShading = Color.FromArgb(0xE1, 0xE0, 0xFF);

        public static Color Diastolic = Color.FromArgb(0xFE, 0x54, 0xA9);
        public static Color DiastolicGoal = Color.FromArgb(0xFE, 0x54, 0xA9);
        public static Color DiastolicGoalShading = Color.FromArgb(0xFD, 0xDE, 0xFD);

        //public static Color TotalCholestrol = Color.FromArgb(0xBD, 0xE0, 0xAF);
        //public static Color TotalCholestrolGoal = Color.FromArgb(0xBD, 0xE0, 0xAF);
        public static Color TotalCholestrol = ColorTranslator.FromHtml("#f890ff");
        public static Color TotalCholestrolGoal = ColorTranslator.FromHtml("#F88FFF");
        public static Color TotalCholestrolGoalShading = Color.FromArgb(0xE0, 0xF7, 0xD7);

        //public static Color HDL = Color.FromArgb(0xD8, 0xA3, 0x7F);
        //public static Color HDLGoal = Color.FromArgb(0xD8, 0xA3, 0x7F);
        public static Color HDL = ColorTranslator.FromHtml("#5EC1FF");
        public static Color HDLGoal = ColorTranslator.FromHtml("#5EC1FF");
        public static Color HDLGoalShading = Color.FromArgb(0xF9, 0xE4, 0xD6);

        //public static Color LDL = Color.FromArgb(0xFF, 0xF3, 0x69);
        //public static Color LDLGoal = Color.FromArgb(0xFF, 0xF3, 0x69);
        public static Color LDL = ColorTranslator.FromHtml("#606BFF");
        public static Color LDLGoal = ColorTranslator.FromHtml("#606BFF");
        public static Color LDLGoalShading = Color.FromArgb(0xF7, 0xF3, 0xC3);

        //public static Color Triglyceride = Color.FromArgb(0x69, 0xF3, 0xFF);
        //public static Color TriglycerideGoal = Color.FromArgb(0x69, 0xF3, 0xFF);
        //public static Color TriglycerideGoalShading = Color.FromArgb(0xD2, 0xF6, 0xF9);
        public static Color Triglyceride = ColorTranslator.FromHtml("#7D20DF");
        public static Color TriglycerideGoal = ColorTranslator.FromHtml("#7D20DF");
        public static Color TriglycerideGoalShading = ColorTranslator.FromHtml("#F0E1FF");

        public static Color Weight = Color.FromArgb(0xAB, 0xB2, 0x8F);
        public static Color WeightGoal = Color.FromArgb(0xAB, 0xB2, 0x8F);
        public static Color WeightGoalShading = Color.FromArgb(0xE0, 0xDD, 0xBE);
        //public static Color WeightObese = Color.FromArgb(0xFE, 0xFE, 0xDE);
        public static Color WeightObese = ColorTranslator.FromHtml("#EEF09C");
        public static Color Weight_OverWeight = Color.FromArgb(100, 0xFF, 0x99, 0xA7);

        public static Color Glucose = Color.FromArgb(0x96, 0x69, 0xA6);

        //public static Color PhysicalActivity = Color.FromArgb(0x33, 0x66, 0x33);   
        public static Color PhysicalActivity = Color.FromArgb(180, 0x73, 0x95, 0xBF);
        public static Color PatientPhysicalActivity = ColorTranslator.FromHtml("#f79226");
        public static Color PhysicalActivityGoal = Color.FromArgb(180, 0x73, 0x95, 0xBF);
        public static Color PatientPhysicalActivityGoal = ColorTranslator.FromHtml("#0e6119");
        public static Color PhysicalActivityGoalShading = Color.FromArgb(100, 0xFF, 0x99, 0xA7);
        public static Color PatientPhysicalActivityGoalShading = ColorTranslator.FromHtml("#d5e1d7");
    }


    /// <summary>
    /// This class abstracts one type of graph data. For ex: LDL.
    /// Note that a single graph can contain multiple instances of AHAGraphData. For ex a CombinedCholesterol graph
    /// contains 3 instances of AHAGraphData for LDL, HDL, Total Cholesterol
    /// </summary>
    public class AHAGraphData
    {
        public AHAPlotGoalData m_goalData;
        public List<GraphInputReading> m_readingList;
        public string m_graphDataPlotLabel;
        public string m_goalGraphPlotLabel;
        public Color m_color;
        public string m_readingUnits;
        public bool m_considerForYaxisRemapping;
        public double m_graphLineThickness;
        public List<DateTime> m_XAxisLabels;


        public AHAGraphData()
        {
            m_goalData = null;
            m_readingList = new List<GraphInputReading>();
            m_goalGraphPlotLabel = "";
            m_color = System.Drawing.Color.Black;
            m_readingUnits = "";
            m_considerForYaxisRemapping = true;
            m_graphLineThickness = 2.0f;
        }




        public static void CalculateMaxMinValues(bool isForYaxis, List<AHAGraphData> graphs, ref double min, ref double max)
        {
            double maxReading = 0;
            double minReading = 0;

            if (graphs.SelectMany(x => x.m_readingList).Count() > 0)
            {
                maxReading = graphs.SelectMany(x => x.m_readingList).Max(y => y.ReadingValue);
                minReading = graphs.SelectMany(x => x.m_readingList).Min(y => y.ReadingValue);
            }

            foreach (AHAGraphData graphData in graphs)
            {
                if (graphData.m_goalData != null)
                {
                    if (graphData.m_goalData.GoalLineType == GoalLineType.Sloped)
                    {
                        maxReading = Math.Max(maxReading, Math.Max(graphData.m_goalData.GoalStartValue, graphData.m_goalData.GoalEndValue));
                        minReading = Math.Min(minReading, Math.Min(graphData.m_goalData.GoalStartValue, graphData.m_goalData.GoalEndValue));
                    }
                    else
                    {
                        maxReading = Math.Max(maxReading, graphData.m_goalData.GoalEndValue);
                        minReading = Math.Min(minReading, graphData.m_goalData.GoalEndValue);
                    }
                }
            }






            max = maxReading;
            min = minReading;

            if (isForYaxis)
            {
                //increment/decrement maxValue and minValue by 3% so the graph points well inside
                //the range. 
                max = 1.03 * max;
                min = .97 * min;

                //Make sure the top and bottom values on yaxis are non fractional
                min = Math.Floor(min);
                max = Math.Ceiling(max);
            }

        }
    }








    /// <summary>
    /// Heart360 supports BarChart for Excercise and Line and point plots for other types
    /// </summary>
    public enum GraphRenderingMode { LineAndPoint, BarChart };


    /// <summary>
    /// This class wraps all the data to draw a graph.  Note that this is a private constructor and instance of this class can be
    /// only created by calling the static ConstructPlotData method.
    /// </summary>
    public class AHAPlotData
    {
        public GraphType m_GraphType;
        public GraphRenderingMode m_graphRenderingMode;
        public List<AHAGraphData> m_graphsToPlot;
        public string m_xaxisLabel;
        public string m_yaxisLabel;
        public AHACommon.DataDateReturnType m_DateRange;


        //There can be 2 ThresholdRegions for a graph (shaded region)
        //Leave these null if there are no shaded regions in the graph 
        //We currently need 2 ThresholdRegion only in Weight graph
        public ThresholdRegion m_thresholdRegion_1;
        public ThresholdRegion m_thresholdRegion_2;


        //For excerise we need the y axis to start from 0
        public bool m_bForceYAxisMinToZero;

        public int m_distanceBetweenYaxisPoints;

        public bool bShowTimeOfReadingOnMouseOverDiv;

        public int m_graphWidth;
        public int m_graphHeight;


        private AHAPlotData()
        {
            m_bForceYAxisMinToZero = false;
            m_graphRenderingMode = GraphRenderingMode.LineAndPoint;
            bShowTimeOfReadingOnMouseOverDiv = true;
            m_distanceBetweenYaxisPoints = 0; //Autocalculate
            m_graphWidth = AHAGraphGenerator.defaultGraphWidth;
            m_graphHeight = AHAGraphGenerator.defaultGraphHeight;
        }


        public int GetBarWidth()
        {
            if (m_GraphType == GraphType.Exercise)
            {
                if (this.m_DateRange == AHACommon.DataDateReturnType.Last6Months)
                {
                    return 10;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.Last9Months)
                {
                    return 5;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.LastYear)
                {
                    return 4;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.Last2Years)
                {
                    return 3;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.Last3Months)
                {
                    return 30;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.LastMonth)
                {
                    return 9;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.LastWeek)
                {
                    return 30;
                }
                return 2;
            }
            else
            {
                if (this.m_DateRange == AHACommon.DataDateReturnType.Last6Months)
                {
                    return 1;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.Last3Months)
                {
                    return 3;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.LastMonth)
                {
                    return 9;
                }
                else if (this.m_DateRange == AHACommon.DataDateReturnType.LastWeek)
                {
                    return 30;
                }
                return 2;
            }

        }
        private static AHAPlotData _CreatePlotDataForSPRBG(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_DateRange = dateRange;

            plotData.m_yaxisLabel = string.Format("{0} (mg/dL)", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodGlucose"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            //graphData.m_color = System.Drawing.Color.FromArgb(0x33, 0x66, 0x33);
            graphData.m_color = AHAGraphColor.Glucose;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodGlucose").ToString();

            graphData.m_readingUnits = "mg/dL";

            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForSPRBG(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForSPRBG(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            List<PatientGuids> listGuids = UIHelper.GetPatientGuidList(AHAAPI.H360RequestContext.GetContext().PatientList);

            List<StandardPracticeReportHelper.GraphItem> objBG = StandardPracticeReportHelper.GetBloodGlucoseList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            return plotData;
        }

        private static AHAPlotData _CreatePlotDataForSPRBP(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_DateRange = dateRange;

            plotData.m_yaxisLabel = string.Format("{0} mmHg", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodPressure"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.Systolic;

            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Systolic").ToString();

            graphData.m_readingUnits = "mmHg";


            graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.Diastolic;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString();
            //Now Added
            graphData.m_readingUnits = "mmHg";
            //End Now Added

            return plotData;
        }


        private static AHAPlotData _PopulatePlotDataForForSPRBP(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForSPRBP(dateRange);
            AHAGraphData graphData = plotData.m_graphsToPlot[0];

            List<PatientGuids> listGuids = UIHelper.GetPatientGuidList(AHAAPI.H360RequestContext.GetContext().PatientList);

            List<StandardPracticeReportHelper.GraphItem> objBG = StandardPracticeReportHelper.GetSystolicPressureList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            graphData = plotData.m_graphsToPlot[1];
            objBG = StandardPracticeReportHelper.GetDiastolicPressureList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            return plotData;

        }

        private static AHAPlotData _CreatePlotDataForSPRCholesterol(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_DateRange = dateRange;

            plotData.m_yaxisLabel = string.Format("{0} (mg/dL)", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Cholesterol"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.TotalCholestrol;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TC").ToString();

            graphData.m_readingUnits = string.Format("mg/dL[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TC"));


            graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.HDL;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_HDL").ToString();

            graphData.m_readingUnits = string.Format("mg/dL[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_HDL"));


            graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.LDL;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LDL").ToString();

            graphData.m_readingUnits = string.Format("mg/dL[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LDL"));


            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForSPRCholesterol(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForSPRCholesterol(dateRange);
            AHAGraphData graphData = plotData.m_graphsToPlot[0];

            List<PatientGuids> listGuids = UIHelper.GetPatientGuidList(AHAAPI.H360RequestContext.GetContext().PatientList);

            List<StandardPracticeReportHelper.GraphItem> objBG = StandardPracticeReportHelper.GetTotalCholesterolList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            graphData = plotData.m_graphsToPlot[1];
            objBG = StandardPracticeReportHelper.GetHDLCholesterolList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            graphData = plotData.m_graphsToPlot[2];
            objBG = StandardPracticeReportHelper.GetLDLCholesterolList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            return plotData;

        }
        private static AHAPlotData _CreatePlotDataForSPRWeight(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
            plotData.m_DateRange = dateRange;

            plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Weight").ToString();
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.Weight;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Weight").ToString();

            graphData.m_readingUnits = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Pounds").ToString();

            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForSPRWeight(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForSPRWeight(dateRange);
            AHAGraphData graphData = plotData.m_graphsToPlot[0];

            List<PatientGuids> listGuids = UIHelper.GetPatientGuidList(AHAAPI.H360RequestContext.GetContext().PatientList);


            List<StandardPracticeReportHelper.GraphItem> objBG = StandardPracticeReportHelper.GetWeightList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            return plotData;
        }
        private static AHAPlotData _CreatePlotDataForSPRPhysicalActivity(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
            plotData.m_DateRange = dateRange;

            plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Duration").ToString(); ;
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.PhysicalActivity;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Duration").ToString(); ;

            graphData.m_readingUnits = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Minutes_Short").ToString(); ;

            plotData.m_graphRenderingMode = GraphRenderingMode.BarChart;
            plotData.m_bForceYAxisMinToZero = true;//We need y axis to start from zero in this case
            plotData.bShowTimeOfReadingOnMouseOverDiv = false; //Dont show time of day while rendering...
            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForSPRPhysicalActivity(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForSPRPhysicalActivity(dateRange);
            AHAGraphData graphData = plotData.m_graphsToPlot[0];

            List<PatientGuids> listGuids = UIHelper.GetPatientGuidList(AHAAPI.H360RequestContext.GetContext().PatientList);

            List<StandardPracticeReportHelper.GraphItem> objBG = StandardPracticeReportHelper.GetPhysicalActivityList(dateRange, listGuids);
            if (objBG != null && objBG.Count > 0)
            {
                foreach (StandardPracticeReportHelper.GraphItem data in objBG)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            return plotData;
        }

        private static AHAPlotData _ConstructPlotDataForSPR(GraphType graphType, AHACommon.DataDateReturnType dateRange)
        {
            switch (graphType)
            {
                case GraphType.SPRGlucose:
                    return _PopulatePlotDataForSPRBG(dateRange);
                case GraphType.SPRCombinedBP:
                    return _PopulatePlotDataForForSPRBP(dateRange);
                case GraphType.SPRCombinedCholesterol:
                    return _PopulatePlotDataForSPRCholesterol(dateRange);
                case GraphType.SPRWeight:
                    return _PopulatePlotDataForSPRWeight(dateRange);
                case GraphType.SPRExercise:
                    return _PopulatePlotDataForSPRPhysicalActivity(dateRange);
            }

            return null;
        }

        private static AHAPlotData _CreatePlotDataForBG(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.Glucose;

            plotData.m_DateRange = dateRange;
            plotData.m_yaxisLabel = string.Format("{0} mg/dL", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodGlucose"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.Glucose;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodGlucose").ToString();

            graphData.m_readingUnits = "mg/dL";

            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForBG(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForBG(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.BloodGlucoseDataManager.BGItem> objBG = mgr.BloodGlucoseDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objBG != null && objBG.Count > 0)
                {
                    foreach (HVManager.BloodGlucoseDataManager.BGItem data in objBG)
                    {
                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value;
                        reading.ReadingValue = (float)data.Value.Value;
                        graphData.m_readingList.Add(reading);
                    }
                }
            }
            return plotData;
        }



        private static AHAPlotData _PopulateAllInOnePlotDataForBG(List<PatientGuids> listGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForBG(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (listGuids != null && listGuids.Count == 1)
            {
                PatientGuids objGuids = listGuids[0];
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                foreach (HVManager.BloodGlucoseDataManager.BGItem data in mgr.BloodGlucoseDataManager.GetDataBetweenDates(dateRange))
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.When.Value;
                    reading.ReadingValue = (float)data.Value.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            else if (listGuids != null && listGuids.Count > 1)
            {
                List<StandardPracticeReportHelper.GraphItem> listGraphItem = StandardPracticeReportHelper.GetBloodGlucoseList(dateRange, listGuids);
                foreach (StandardPracticeReportHelper.GraphItem data in listGraphItem)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            return plotData;
        }

        private static AHAPlotData _CreatePlotDataForPressureSystolic(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
            plotData.m_GraphType = GraphType.PressureSystolic;
            plotData.m_DateRange = dateRange;
            plotData.m_graphsToPlot = new List<AHAGraphData>();

            AHAGraphData graphData = new AHAGraphData();
            //graphData.m_color = System.Drawing.Color.FromArgb(0x79, 0x76, 0xFE);
            graphData.m_color = AHAGraphColor.Systolic;
            plotData.m_graphsToPlot.Add(graphData);
            plotData.m_distanceBetweenYaxisPoints = 10;

            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Systolic").ToString();
            graphData.m_readingUnits = string.Format("[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Systolic").ToString());
            plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Systolic").ToString();
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            return plotData;
        }


        private static AHAPlotData _PopulatePlotDataForPressureSystolic(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForPressureSystolic(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.BPGoalDataManager.BPGoalItem objGoal_B = mgr.BPGoalDataManager.Item;
                if (objGoal_B != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.SystolicGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                    //BP Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    //plotData.m_goalData.m_goalStartValue = (float)bpGoalWrapper.StartDiastolic.Value.Value;
                    graphData.m_goalData.GoalEndValue = (float)objGoal_B.TargetSystolic.Value;

                    //For BP, the threshold is as specified by Goal
                    //plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion(ThresholdDirection.ShadeHigher, graphData.m_goalData.m_goalEndValue, Graph.ThresholdRegion.DefaultShadingColor, "", "Above my goal");
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.SystolicGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString() );
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_SystolicGoal").ToString(); //We dont want a legend for goal line

                }
            }

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.BloodPressureDataManager.BPItem> objBP = mgr.BloodPressureDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objBP != null && objBP.Count > 0)
                {
                    foreach (HVManager.BloodPressureDataManager.BPItem data in objBP)
                    {
                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value;
                        reading.ReadingValue = (float)data.Systolic.Value;
                        graphData.m_readingList.Add(reading);
                    }
                }
            }

            return plotData;
        }

        private static AHAPlotData _PopulateAllInOnePlotDataForPressureSystolic(List<PatientGuids> listGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForPressureSystolic(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (listGuids != null && listGuids.Count == 1)
            {
                PatientGuids objGuids = listGuids[0];
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                HVManager.BPGoalDataManager.BPGoalItem goalItem = mgr.BPGoalDataManager.Item;
                if (goalItem != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.SystolicGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                    //BP Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    //plotData.m_goalData.m_goalStartValue = (float)bpGoalWrapper.StartDiastolic.Value.Value;
                    graphData.m_goalData.GoalEndValue = (float)goalItem.TargetSystolic.Value;

                    //For BP, the threshold is as specified by Goal
                    //plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion(ThresholdDirection.ShadeHigher, graphData.m_goalData.m_goalEndValue, Graph.ThresholdRegion.DefaultShadingColor, "", "Above my goal");
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.SystolicGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString() );
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_SystolicGoal").ToString(); //We dont want a legend for goal line
                }

                foreach (HVManager.BloodPressureDataManager.BPItem data in mgr.BloodPressureDataManager.GetDataBetweenDates(dateRange))
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.When.Value;
                    reading.ReadingValue = (float)data.Systolic.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            else if (listGuids != null && listGuids.Count > 1)
            {
                List<StandardPracticeReportHelper.GraphItem> listGraphItem = StandardPracticeReportHelper.GetSystolicPressureList(dateRange, listGuids);
                foreach (StandardPracticeReportHelper.GraphItem data in listGraphItem)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            return plotData;
        }

        private static AHAPlotData _CreatePlotDataForPressureDiastolic(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.PressureDiastolic;

            plotData.m_DateRange = dateRange;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();

            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.Diastolic;
            plotData.m_graphsToPlot.Add(graphData);

            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString();

            graphData.m_readingUnits = string.Format("[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString());
            plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString();
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));


            return plotData;
        }



        private static AHAPlotData _PopulatePlotDataForPressureDiastolic(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForPressureDiastolic(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.BPGoalDataManager.BPGoalItem objGoal_B = mgr.BPGoalDataManager.Item;
                if (objGoal_B != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.DiastolicGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                    //BP Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    //plotData.m_goalData.m_goalStartValue = (float)bpGoalWrapper.StartDiastolic.Value.Value;
                    graphData.m_goalData.GoalEndValue = (float)objGoal_B.TargetDiastolic.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.DiastolicGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString() );
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_DiastolicGoal").ToString(); //We dont want a legend for goal line

                }
            }


            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.BloodPressureDataManager.BPItem> objBP = mgr.BloodPressureDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objBP != null && objBP.Count > 0)
                {
                    foreach (HVManager.BloodPressureDataManager.BPItem data in objBP)
                    {
                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value;
                        reading.ReadingValue = (float)data.Diastolic.Value;
                        graphData.m_readingList.Add(reading);
                    }
                }
            }

            return plotData;
        }

        private static AHAPlotData _PopulateAllInOnePlotDataForPressureDiastolic(List<PatientGuids> listGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForPressureDiastolic(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (listGuids != null && listGuids.Count == 1)
            {
                PatientGuids objGuids = listGuids[0];
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                HVManager.BPGoalDataManager.BPGoalItem goalItem = mgr.BPGoalDataManager.Item;
                if (goalItem != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.DiastolicGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;
                    //plotData.m_goalData.m_goalStartDate = bpGoalWrapper.StartDate.Value.Value;
                    //plotData.m_goalData.m_goalEndDate = bpGoalWrapper.TargetDate.Value.Value;

                    //BP Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    //plotData.m_goalData.m_goalStartValue = (float)bpGoalWrapper.StartDiastolic.Value.Value;
                    graphData.m_goalData.GoalEndValue = (float)goalItem.TargetDiastolic.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.DiastolicGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString() );
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_DiastolicGoal").ToString();  //We dont want a legend for goal line
                }

                foreach (HVManager.BloodPressureDataManager.BPItem data in mgr.BloodPressureDataManager.GetDataBetweenDates(dateRange))
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.When.Value;
                    reading.ReadingValue = (float)data.Diastolic.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            else if (listGuids != null && listGuids.Count > 1)
            {
                List<StandardPracticeReportHelper.GraphItem> listGraphItem = StandardPracticeReportHelper.GetDiastolicPressureList(dateRange, listGuids);
                foreach (StandardPracticeReportHelper.GraphItem data in listGraphItem)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            return plotData;
        }


        private static AHAPlotData _CreatePlotDataForTotalCholesterol(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.TotalCholesterol;

            plotData.m_DateRange = dateRange;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.TotalCholestrol;
            plotData.m_graphsToPlot.Add(graphData);
            plotData.m_distanceBetweenYaxisPoints = 10;
            plotData.bShowTimeOfReadingOnMouseOverDiv = false;



            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TC").ToString();
            graphData.m_readingUnits = string.Format("mg/dL[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TC").ToString());

            plotData.m_yaxisLabel = string.Format("{0} mg/dL", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TC").ToString());
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));

            return plotData;

        }

        private static AHAPlotData _PopulatePlotDataForTotalCholesterol(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForTotalCholesterol(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.CholesterolGoalDataManager.CholesterolGoalItem objGoal_C = mgr.CholesterolGoalDataManager.Item;
                if (objGoal_C != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.TotalCholestrolGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;
                    //plotData.m_goalData.m_goalStartDate = bpGoalWrapper.StartDate.Value.Value;
                    //plotData.m_goalData.m_goalEndDate = bpGoalWrapper.TargetDate.Value.Value;

                    //Cholestrol Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    graphData.m_goalData.GoalEndValue = (float)objGoal_C.TargetTotalCholesterol.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.TotalCholestrolGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString());
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TotalGoal").ToString();
                }
            }

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.CholesterolDataManager.CholesterolItem> objC = mgr.CholesterolDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objC != null && objC.Count > 0)
                {
                    foreach (HVManager.CholesterolDataManager.CholesterolItem data in objC)
                    {
                        if (data.TotalCholesterol.Value.HasValue)
                        {
                            Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                            reading.DateOfReading = data.When.Value;
                            reading.ReadingValue = (float)data.TotalCholesterol.Value.Value;
                            graphData.m_readingList.Add(reading);
                        }
                    }
                }
            }
            return plotData;

        }

        private static AHAPlotData _PopulateAllInOnePlotDataForTotalCholesterol(List<PatientGuids> listGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForTotalCholesterol(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (listGuids != null && listGuids.Count == 1)
            {
                PatientGuids objGuids = listGuids[0];
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                HVManager.CholesterolGoalDataManager.CholesterolGoalItem goalItem = mgr.CholesterolGoalDataManager.Item;
                if (goalItem != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.TotalCholestrolGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                    //Cholestrol Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    graphData.m_goalData.GoalEndValue = (float)goalItem.TargetTotalCholesterol.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.TotalCholestrolGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString());
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TotalGoal").ToString();
                }

                foreach (HVManager.CholesterolDataManager.CholesterolItem data in mgr.CholesterolDataManager.GetDataBetweenDates(dateRange))
                {
                    if (data.TotalCholesterol.Value.HasValue)
                    {
                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value;
                        reading.ReadingValue = (float)data.TotalCholesterol.Value.Value;
                        graphData.m_readingList.Add(reading);
                    }
                }
            }
            else if (listGuids != null && listGuids.Count > 1)
            {
                List<StandardPracticeReportHelper.GraphItem> listGraphItem = StandardPracticeReportHelper.GetTotalCholesterolList(dateRange, listGuids);
                foreach (StandardPracticeReportHelper.GraphItem data in listGraphItem)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            return plotData;

        }



        private static AHAPlotData _CreatePlotDataForLDL(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.LDL;
            plotData.m_DateRange = dateRange;
            plotData.m_distanceBetweenYaxisPoints = 10;


            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.LDL;
            plotData.m_graphsToPlot.Add(graphData);
            plotData.bShowTimeOfReadingOnMouseOverDiv = false;


            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LDL").ToString();
            graphData.m_readingUnits = string.Format("mg/dL[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LDL").ToString());
            plotData.m_yaxisLabel = string.Format("{0} mg/dL", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LDL").ToString());
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForLDL(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForLDL(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.CholesterolGoalDataManager.CholesterolGoalItem objGoal_C = mgr.CholesterolGoalDataManager.Item;
                if (objGoal_C != null && objGoal_C.TargetLDL.Value.HasValue )
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.LDLGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;
                    //plotData.m_goalData.m_goalStartDate = bpGoalWrapper.StartDate.Value.Value;
                    //plotData.m_goalData.m_goalEndDate = bpGoalWrapper.TargetDate.Value.Value;

                    //Cholestrol Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    graphData.m_goalData.GoalEndValue = (float)objGoal_C.TargetLDL.Value.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.LDLGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString());
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LDLGoal").ToString();
                }
            }

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.CholesterolDataManager.CholesterolItem> objC = mgr.CholesterolDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objC != null && objC.Count > 0)
                {
                    foreach (HVManager.CholesterolDataManager.CholesterolItem data in objC)
                    {
                        if (data.LDL.Value.HasValue)
                        {
                            Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                            reading.DateOfReading = data.When.Value;
                            reading.ReadingValue = (float)data.LDL.Value.Value;
                            graphData.m_readingList.Add(reading);
                        }
                    }
                }
            }
            return plotData;


        }


        private static AHAPlotData _PopulateAllInOnePlotDataForLDL(List<PatientGuids> listGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForLDL(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (listGuids != null && listGuids.Count == 1)
            {
                PatientGuids objGuids = listGuids[0];
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);


                HVManager.CholesterolGoalDataManager.CholesterolGoalItem goalItem = mgr.CholesterolGoalDataManager.Item;
                if (goalItem != null && goalItem.TargetLDL.Value.HasValue )
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.LDLGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                    //Cholestrol Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    graphData.m_goalData.GoalEndValue = (float)goalItem.TargetLDL.Value.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.LDLGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString());
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_LDLGoal").ToString();
                }

                foreach (HVManager.CholesterolDataManager.CholesterolItem data in mgr.CholesterolDataManager.GetDataBetweenDates(dateRange))
                {
                    if (data.LDL.Value.HasValue)
                    {
                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value;
                        reading.ReadingValue = (float)data.LDL.Value.Value;
                        graphData.m_readingList.Add(reading);
                    }
                }
            }
            else if (listGuids != null && listGuids.Count > 1)
            {
                List<StandardPracticeReportHelper.GraphItem> listGraphItem = StandardPracticeReportHelper.GetLDLCholesterolList(dateRange, listGuids);
                foreach (StandardPracticeReportHelper.GraphItem data in listGraphItem)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            return plotData;


        }

        private static AHAPlotData _CreatePlotDataForHDL(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
            plotData.m_GraphType = GraphType.HDL;

            plotData.m_DateRange = dateRange;
            plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.HDL;
            plotData.m_graphsToPlot.Add(graphData);

            plotData.bShowTimeOfReadingOnMouseOverDiv = false;
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_HDL").ToString();
            graphData.m_readingUnits = string.Format("mg/dL[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_HDL"));
            plotData.m_yaxisLabel = string.Format("{0} mg/dL", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_HDL"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            return plotData;

        }

        private static AHAPlotData _PopulatePlotDataForHDL(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForHDL(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.CholesterolGoalDataManager.CholesterolGoalItem objGoal_C = mgr.CholesterolGoalDataManager.Item;
                if (objGoal_C != null && objGoal_C.TargetHDL.Value.HasValue )
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.HDLGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;
                    //plotData.m_goalData.m_goalStartDate = bpGoalWrapper.StartDate.Value.Value;
                    //plotData.m_goalData.m_goalEndDate = bpGoalWrapper.TargetDate.Value.Value;

                    //Cholestrol Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    graphData.m_goalData.GoalEndValue = (float)objGoal_C.TargetHDL.Value.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeLower, graphData.m_goalData.GoalEndValue,
                                                                                AHAGraphColor.HDLGoalShading, "", 
                                                                                System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BelowMyGoal").ToString() );
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_HDLGoal").ToString(); ;
                }
            }

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.CholesterolDataManager.CholesterolItem> objC = mgr.CholesterolDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objC != null && objC.Count > 0)
                {
                    foreach (HVManager.CholesterolDataManager.CholesterolItem data in objC)
                    {
                        if (data.HDL.Value.HasValue)
                        {
                            Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                            reading.DateOfReading = data.When.Value;
                            reading.ReadingValue = (float)data.HDL.Value.Value;
                            graphData.m_readingList.Add(reading);
                        }
                    }
                }
            }
            return plotData;

        }

        private static AHAPlotData _PopulateAllInOnePlotDataForHDL(List<PatientGuids> listGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForHDL(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (listGuids != null && listGuids.Count == 1)
            {
                PatientGuids objGuids = listGuids[0];
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                HVManager.CholesterolGoalDataManager.CholesterolGoalItem goalItem = mgr.CholesterolGoalDataManager.Item;
                if (goalItem != null && goalItem.TargetHDL.Value.HasValue )
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.HDLGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                    //Cholestrol Goal does not have start and end dates
                    graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                    graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                    graphData.m_goalData.GoalEndValue = (float)goalItem.TargetHDL.Value.Value;

                    //For BP, the threshold is as specified by Goal
                    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeLower, graphData.m_goalData.GoalEndValue,
                                                                              AHAGraphColor.HDLGoalShading, "", 
                                                                              System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BelowMyGoal").ToString() );
                    graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_HDLGoal").ToString();
                }

                foreach (HVManager.CholesterolDataManager.CholesterolItem data in mgr.CholesterolDataManager.GetDataBetweenDates(dateRange))
                {
                    if (data.HDL.Value.HasValue)
                    {
                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value;
                        reading.ReadingValue = (float)data.HDL.Value.Value;
                        graphData.m_readingList.Add(reading);
                    }
                }
            }
            else if (listGuids != null && listGuids.Count > 1)
            {
                List<StandardPracticeReportHelper.GraphItem> listGraphItem = StandardPracticeReportHelper.GetHDLCholesterolList(dateRange, listGuids);
                foreach (StandardPracticeReportHelper.GraphItem data in listGraphItem)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }
            return plotData;

        }

        private static AHAPlotData _CreatePlotDataForTriglycerides(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
            plotData.m_GraphType = GraphType.Triglycerides;
            plotData.m_DateRange = dateRange;

            //Since the data can be for high "ranges" in y axis, let us do it 
            //autocalculate mode
            //plotData.m_distanceBetweenYaxisPoints = 10;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.Triglyceride;
            plotData.m_graphsToPlot.Add(graphData);
            plotData.bShowTimeOfReadingOnMouseOverDiv = false;

            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Triglycerides_Plain").ToString(); ;
            graphData.m_readingUnits = string.Format("mg/dL[{0}]", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Triglycerides_Plain"));
            plotData.m_yaxisLabel = string.Format("{0} mg/dL", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Triglycerides_Plain"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));

            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForTriglycerides(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForTriglycerides(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.CholesterolGoalDataManager.CholesterolGoalItem objGoal_C = mgr.CholesterolGoalDataManager.Item;
                if (objGoal_C != null)
                {
                    if (objGoal_C != null && objGoal_C.TargetTriglyceride.Value.HasValue)
                    {
                        graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                        graphData.m_goalData.GoalLineColor = AHAGraphColor.TriglycerideGoal;
                        graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                        //Cholestrol Goal does not have start and end dates
                        graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                        graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                        graphData.m_goalData.GoalEndValue = (float)objGoal_C.TargetTriglyceride.Value.Value;

                        //The threshold is as specified by Goal
                        plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                                  AHAGraphColor.TriglycerideGoalShading, "", 
                                                                                  System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString());
                        graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TriGoal").ToString(); ;
                    }
                }
            }

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.CholesterolDataManager.CholesterolItem> objC = mgr.CholesterolDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objC != null && objC.Count > 0)
                {
                    foreach (HVManager.CholesterolDataManager.CholesterolItem data in objC)
                    {
                        if (data.Triglycerides.Value.HasValue)
                        {
                            Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                            reading.DateOfReading = data.When.Value;
                            reading.ReadingValue = (float)data.Triglycerides.Value.Value;
                            graphData.m_readingList.Add(reading);
                        }
                    }
                }
            }
            return plotData;

        }


        private static AHAPlotData _PopulateAllInOnePlotDataForTriglycerides(bool isSingleUserMode, PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForTriglycerides(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.CholesterolGoalDataManager.CholesterolGoalItem objGoal_C = mgr.CholesterolGoalDataManager.Item;
                if (objGoal_C != null)
                {
                    if (objGoal_C != null && objGoal_C.TargetTriglyceride.Value.HasValue)
                    {
                        graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                        graphData.m_goalData.GoalLineColor = AHAGraphColor.TriglycerideGoal;
                        graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

                        //Cholestrol Goal does not have start and end dates
                        graphData.m_goalData.GoalStartDate = DateTime.MinValue;
                        graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

                        graphData.m_goalData.GoalEndValue = (float)objGoal_C.TargetTriglyceride.Value.Value;

                        //The threshold is as specified by Goal
                        plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion( ThresholdDirection.ShadeHigher, graphData.m_goalData.GoalEndValue,
                                                                                    AHAGraphColor.TriglycerideGoalShading, "", 
                                                                                    System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_AboveMyGoal").ToString());
                        graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_TriGoal").ToString(); ;
                    }
                }
            }
            return plotData;

        }

        private static AHAPlotData _CreatePlotDataForWeight(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.Weight;

            plotData.m_DateRange = dateRange;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = AHAGraphColor.Weight;
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Weight").ToString();
            graphData.m_goalGraphPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_MyGoalWeight").ToString();
            graphData.m_readingUnits = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Pounds").ToString();
            plotData.m_yaxisLabel = string.Format("{0} lbs", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_MyWeight"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            return plotData;
        }
        private static AHAPlotData _PopulatePlotDataForWeight(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForWeight(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);
                HVManager.WeightGoalDataManager.WeightGoalItem objGoal_W = mgr.WeightGoalDataManager.Item;
                if (objGoal_W != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.WeightGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Sloped;
                    graphData.m_goalData.GoalStartDate = objGoal_W.StartDate.Value.Value;
                    graphData.m_goalData.GoalEndDate = objGoal_W.TargetDate.Value.Value;
                    graphData.m_goalData.GoalStartValue = (float)Convert.ToDouble(objGoal_W.StartValue.Value);
                    graphData.m_goalData.GoalEndValue = (float)objGoal_W.TargetValue.Value;
                }
            }

            float? overWeightVal = (float?)UserSummary.GetWeightThresholdInPoundsForCurrentRecord(objPatGuids, UserWeightRangeType.OverWeight);
            float? obeseWeightVal = (float?)UserSummary.GetWeightThresholdInPoundsForCurrentRecord(objPatGuids, UserWeightRangeType.Obese);

            if (overWeightVal.HasValue)
            {
                plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion(ThresholdDirection.ShadeHigher, overWeightVal.Value, AHAGraphColor.Weight_OverWeight, "", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_OverWeight").ToString());
                //Color nextThresholdColor = Color.FromArgb(0xFE, 0xFE, 0xDE);
                Color nextThresholdColor = AHAGraphColor.WeightObese;
                plotData.m_thresholdRegion_2 = new Graph.ThresholdRegion(ThresholdDirection.ShadeHigher, obeseWeightVal.Value, nextThresholdColor, "", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Obese").ToString());
            }

            if (objPatGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.WeightDataManager.WeightItem> objW = mgr.WeightDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objW != null && objW.Count > 0)
                {
                    foreach (HVManager.WeightDataManager.WeightItem data in objW)
                    {
                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value;
                        reading.ReadingValue = (float)data.CommonWeight.Value.ToPounds();
                        graphData.m_readingList.Add(reading);
                    }
                }
            }

            return plotData;

        }

        private static AHAPlotData _PopulateAllInOnePlotDataForWeight(List<PatientGuids> listGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForWeight(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];
            if (listGuids != null && listGuids.Count == 1)
            {
                PatientGuids objGuids = listGuids[0];
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                HVManager.WeightGoalDataManager.WeightGoalItem goalItem = mgr.WeightGoalDataManager.Item;
                if (goalItem != null)
                {
                    graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
                    graphData.m_goalData.GoalLineColor = AHAGraphColor.WeightGoal;
                    graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Sloped;
                    graphData.m_goalData.GoalStartDate = goalItem.StartDate.Value.Value;
                    graphData.m_goalData.GoalEndDate = goalItem.TargetDate.Value.Value;
                    graphData.m_goalData.GoalStartValue = (float)goalItem.StartValue.Value;
                    graphData.m_goalData.GoalEndValue = (float)goalItem.TargetValue.Value;
                }

                foreach (HVManager.WeightDataManager.WeightItem data in mgr.WeightDataManager.GetDataBetweenDates(dateRange))
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.When.Value;
                    reading.ReadingValue = (float)data.CommonWeight.Value.ToPounds();
                    graphData.m_readingList.Add(reading);
                }
            }
            else if (listGuids != null && listGuids.Count > 1)
            {
                List<StandardPracticeReportHelper.GraphItem> listGraphItem = StandardPracticeReportHelper.GetWeightList(dateRange, listGuids);
                foreach (StandardPracticeReportHelper.GraphItem data in listGraphItem)
                {
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.Date;
                    reading.ReadingValue = (float)data.Value;
                    graphData.m_readingList.Add(reading);
                }
            }

            return plotData;

        }

        private static AHAPlotData _PopulatePlotDataForCombinedBP(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {

            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.CombinedBP;

            AHAPlotData plotData_Systolic = _PopulatePlotDataForPressureSystolic(objPatGuids, dateRange);
            AHAPlotData plotData_Diastolic = _PopulatePlotDataForPressureDiastolic(objPatGuids, dateRange);


            plotData.m_graphsToPlot = new List<AHAGraphData>();

            plotData.m_graphsToPlot.Add(plotData_Systolic.m_graphsToPlot[0]);
            plotData.m_graphsToPlot.Add(plotData_Diastolic.m_graphsToPlot[0]);
            plotData.m_DateRange = dateRange;


            plotData.m_yaxisLabel = string.Format("{0} mmHg", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodPressure").ToString());
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_distanceBetweenYaxisPoints = 10;

            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForCombinedCholesterol(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {

            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.CombinedCholesterol;

            AHAPlotData plotData_HDL = _PopulatePlotDataForHDL(objPatGuids, dateRange);
            AHAPlotData plotData_LDL = _PopulatePlotDataForLDL(objPatGuids, dateRange);
            AHAPlotData plotData_Total = _PopulatePlotDataForTotalCholesterol(objPatGuids, dateRange);


            plotData.m_graphsToPlot = new List<AHAGraphData>();
            plotData.m_graphsToPlot.Add(plotData_Total.m_graphsToPlot[0]);
            plotData.m_graphsToPlot.Add(plotData_LDL.m_graphsToPlot[0]);
            plotData.m_graphsToPlot.Add(plotData_HDL.m_graphsToPlot[0]);
            plotData.bShowTimeOfReadingOnMouseOverDiv = false;

            //Make the graph Height 50% more than default height since Total Cholestrol values
            //are way higher
            plotData.m_graphHeight = (int)(plotData.m_graphHeight * 1.5);


            plotData.m_DateRange = dateRange;


            plotData.m_yaxisLabel = string.Format("{0} mg/dL", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Cholesterol"));
            plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
            plotData.m_distanceBetweenYaxisPoints = 10;

            return plotData;

        }


        private static bool _ContainsGraphGroup(GraphGroupType instanceToCheck, GraphGroupType combined)
        {
            return ((combined & instanceToCheck) == instanceToCheck);
        }


        private static AHAPlotData _PopulateAllInOnePlotData(List<PatientGuids> listGuids, GraphGroupType graphsToPlot, GraphGroupType groupForYaxis, AHACommon.DataDateReturnType dateRange)
        {

            //yaxis should be for a specific group and cannot be a combination
            if (!(groupForYaxis == GraphGroupType.CholesterolGroup ||
                groupForYaxis == GraphGroupType.GlucoseGroup ||
                groupForYaxis == GraphGroupType.PressureGroup ||
                groupForYaxis == GraphGroupType.WeightGroup))
            {
                throw new System.Exception("Multiple data types specified for y-axis");
            }

            if (!_ContainsGraphGroup(groupForYaxis, graphsToPlot))
            {
                throw new System.Exception("The yaxis data type is not a part of groups to plot");
            }


            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
            plotData.m_graphsToPlot = new List<AHAGraphData>();
            plotData.m_GraphType = GraphType.AllInOne;
            plotData.m_DateRange = dateRange;
            Dictionary<GraphGroupType, List<AHAGraphData>> graphDataDict = new Dictionary<GraphGroupType, List<AHAGraphData>>();
            int intLegendCount = 0;

            if (_ContainsGraphGroup(GraphGroupType.PressureGroup, graphsToPlot))
            {
                AHAPlotData plotData_Systolic = _PopulateAllInOnePlotDataForPressureSystolic(listGuids, dateRange);
                AHAPlotData plotData_Diastolic = _PopulateAllInOnePlotDataForPressureDiastolic(listGuids, dateRange);

                plotData.m_graphsToPlot.Add(plotData_Systolic.m_graphsToPlot[0]);

                plotData.m_graphsToPlot.Add(plotData_Diastolic.m_graphsToPlot[0]);

                if (GraphGroupType.PressureGroup == groupForYaxis)
                {

                    plotData.m_yaxisLabel = string.Format("{0} mmHg", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodPressure").ToString());
                    plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
                    plotData.m_distanceBetweenYaxisPoints = 10;
                }

                List<AHAGraphData> listOfGraphs = new List<AHAGraphData>();
                listOfGraphs.Add(plotData_Systolic.m_graphsToPlot[0]);
                listOfGraphs.Add(plotData_Diastolic.m_graphsToPlot[0]);
                graphDataDict.Add(GraphGroupType.PressureGroup, listOfGraphs);

            }


            if (_ContainsGraphGroup(GraphGroupType.WeightGroup, graphsToPlot))
            {
                //AHAPlotData plotWeight = _PopulatePlotDataForWeight(null, dateRange);
                AHAPlotData plotWeight = _PopulateAllInOnePlotDataForWeight(listGuids, dateRange);
                plotData.m_graphsToPlot.Add(plotWeight.m_graphsToPlot[0]);

                if (GraphGroupType.WeightGroup == groupForYaxis)
                {
                    plotData.m_yaxisLabel = string.Format("{0} lbs", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_MyWeight").ToString());
                    plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));

                }

                List<AHAGraphData> listOfGraphs = new List<AHAGraphData>();
                listOfGraphs.Add(plotWeight.m_graphsToPlot[0]);
                graphDataDict.Add(GraphGroupType.WeightGroup, listOfGraphs);
            }


            if (_ContainsGraphGroup(GraphGroupType.CholesterolGroup, graphsToPlot))
            {
                AHAPlotData plotTotalCholestrol = _PopulateAllInOnePlotDataForTotalCholesterol(listGuids, dateRange);
                plotData.m_graphsToPlot.Add(plotTotalCholestrol.m_graphsToPlot[0]);

                AHAPlotData plotHDL = _PopulateAllInOnePlotDataForHDL(listGuids, dateRange);
                plotData.m_graphsToPlot.Add(plotHDL.m_graphsToPlot[0]);

                AHAPlotData plotLDL = _PopulateAllInOnePlotDataForLDL(listGuids, dateRange);
                plotData.m_graphsToPlot.Add(plotLDL.m_graphsToPlot[0]);

                if (GraphGroupType.CholesterolGroup == groupForYaxis)
                {
                    //Make the graph Height 50% more than default height since Total Cholestrol values
                    //are way higher
                    plotData.m_graphHeight = (int)(plotData.m_graphHeight * 1.5);

                    plotData.m_yaxisLabel = string.Format("{0} mg/dL", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Cholesterol"));
                    plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
                    plotData.m_distanceBetweenYaxisPoints = 10;
                }

                List<AHAGraphData> listOfGraphs = new List<AHAGraphData>();
                listOfGraphs.Add(plotTotalCholestrol.m_graphsToPlot[0]);
                listOfGraphs.Add(plotHDL.m_graphsToPlot[0]);
                listOfGraphs.Add(plotLDL.m_graphsToPlot[0]);
                graphDataDict.Add(GraphGroupType.CholesterolGroup, listOfGraphs);
            }

            if (_ContainsGraphGroup(GraphGroupType.GlucoseGroup, graphsToPlot))
            {
                AHAPlotData plotGlucose = _PopulateAllInOnePlotDataForBG(listGuids, dateRange);
                plotData.m_graphsToPlot.Add(plotGlucose.m_graphsToPlot[0]);

                if (GraphGroupType.GlucoseGroup == groupForYaxis)
                {
                    plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodGlucose").ToString();
                    plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
                    plotData.m_thresholdRegion_1 = null;
                    plotData.m_thresholdRegion_2 = null;
                    plotData.m_distanceBetweenYaxisPoints = 10;
                }

                List<AHAGraphData> listOfGraphs = new List<AHAGraphData>();
                listOfGraphs.Add(plotGlucose.m_graphsToPlot[0]);
                graphDataDict.Add(GraphGroupType.GlucoseGroup, listOfGraphs);
            }


            //Remap data...
            double maxYaxis = 0;
            double minYaxis = 0;


            GraphGroupType finallySelectedYaxisType = groupForYaxis;


            //If there are readings during the date range for the currently chosen data type for Yaxis
            //the calculate the max and min values of Yaxis based on the readings
            if (graphDataDict[groupForYaxis].SelectMany(x => x.m_readingList).Count() != 0 || graphDataDict[groupForYaxis].Any(x => x.m_goalData != null))
            {
                AHAGraphData.CalculateMaxMinValues(true, graphDataDict[groupForYaxis], ref minYaxis, ref maxYaxis);
            }
            else
            {
                //Select the next data type which has some reading and calculate 
                //the max and min values of Yaxis based on the readings
                foreach (GraphGroupType graphType in graphDataDict.Keys)
                {
                    if (graphDataDict[graphType].SelectMany(x => x.m_readingList).Count() != 0 || graphDataDict[graphType].Any(x => x.m_goalData != null))
                    {
                        AHAGraphData.CalculateMaxMinValues(true, graphDataDict[graphType], ref minYaxis, ref maxYaxis);
                        finallySelectedYaxisType = graphType;
                        break;
                    }
                }
            }

            foreach (GraphGroupType graphType in graphDataDict.Keys.Where(x => x != finallySelectedYaxisType))
            {
                foreach (AHAGraphData gd in graphDataDict[graphType])
                {
                    gd.m_considerForYaxisRemapping = false;
                }

            }


            foreach (GraphGroupType graphType in graphDataDict.Keys.Where(x => x != groupForYaxis))
            {
                foreach (AHAGraphData gd in graphDataDict[graphType])
                {
                    gd.m_graphLineThickness = 1.0f;
                }
            }


            AHAGraphGenerator.GetYaxisLabelCountAndAdjustMinMaxVals(plotData.m_bForceYAxisMinToZero, minYaxis, maxYaxis, ref minYaxis, ref maxYaxis);


            int flag = 1;
            while ((((int)GraphGroupType.PressureGroup |
                    (int)GraphGroupType.GlucoseGroup |
                    (int)GraphGroupType.CholesterolGroup |
                    (int)GraphGroupType.WeightGroup) | flag) > 0)
            {

                GraphGroupType currentType = (GraphGroupType)flag;
                if (_ContainsGraphGroup(currentType, graphsToPlot) && currentType != groupForYaxis)
                {
                    //remap data
                    List<AHAGraphData> listOfGraphsToRemap = graphDataDict[currentType];


                    // Finding the current group's max and min reading
                    double currentGraphMinReading = 0;
                    double currentGraphMaxReading = 0;
                    AHAGraphData.CalculateMaxMinValues(false, listOfGraphsToRemap, ref currentGraphMinReading, ref currentGraphMaxReading);


                    foreach (AHAGraphData currentGraphToRemap in listOfGraphsToRemap)
                    {

                        foreach (GraphInputReading currentReading in currentGraphToRemap.m_readingList)
                        {
                            currentReading.SetDisplayValueForGraph(currentReading.ReadingValue.ToString());

                            if (currentGraphMaxReading == currentGraphMinReading)
                            {
                                double newVal = minYaxis + (maxYaxis - minYaxis) / 2;
                                currentReading.ReadingValue = (float)newVal;
                            }
                            else
                            {
                                double newVal = minYaxis + (currentReading.ReadingValue - currentGraphMinReading) / (currentGraphMaxReading - currentGraphMinReading) * (maxYaxis - minYaxis);
                                currentReading.ReadingValue = (float)newVal;
                            }

                        }

                        if (currentGraphToRemap.m_goalData != null)
                        {
                            //Now Added
                            currentGraphToRemap.m_goalData.SetGoalStartDisplayValueForGraph(currentGraphToRemap.m_goalData.GoalStartValue.ToString());
                            currentGraphToRemap.m_goalData.SetGoalEndDisplayValueForGraph(currentGraphToRemap.m_goalData.GoalEndValue.ToString());
                            //End Now Added

                            if (currentGraphMaxReading == currentGraphMinReading)
                            {
                                currentGraphToRemap.m_goalData.GoalStartValue = (float)(minYaxis + (maxYaxis - minYaxis) / 2);
                                currentGraphToRemap.m_goalData.GoalEndValue = (float)(minYaxis + (maxYaxis - minYaxis) / 2);
                            }
                            else
                            {
                                double newVal = minYaxis + (currentGraphToRemap.m_goalData.GoalStartValue - currentGraphMinReading) / (currentGraphMaxReading - currentGraphMinReading) * (maxYaxis - minYaxis);
                                currentGraphToRemap.m_goalData.GoalStartValue = (float)newVal;

                                newVal = minYaxis + (currentGraphToRemap.m_goalData.GoalEndValue - currentGraphMinReading) / (currentGraphMaxReading - currentGraphMinReading) * (maxYaxis - minYaxis);
                                currentGraphToRemap.m_goalData.GoalEndValue = (float)newVal;
                            }
                        }
                    }
                }
                flag = flag << 1;
            }

            //increase height of graph by 50 pixel  according to the number of legends to show

            if ((intLegendCount += graphDataDict.SelectMany(e => e.Value).Count()) > 0)
            {
                //intLegendCount += graphDataDict.SelectMany(e => e.Value).Count();
                intLegendCount += graphDataDict.SelectMany(e => e.Value).Count(e => e.m_goalData != null);
                if (intLegendCount > 4)
                {
                    plotData.m_graphHeight = plotData.m_graphHeight + (50 * ((intLegendCount - 1) / 4));
                }
            }

            return plotData;
        }


        //private static AHAPlotData _CreatePlotDataForExercise(AHACommon.DataDateReturnType dateRange)
        //{
        //    Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
        //    plotData.m_GraphType = GraphType.Exercise;

        //    plotData.bShowTimeOfReadingOnMouseOverDiv = false; //Dont show time of day while rendering...
        //    plotData.m_bForceYAxisMinToZero = true;//We need y axis to start from zero in this case
        //    plotData.m_graphRenderingMode = GraphRenderingMode.BarChart;
        //    plotData.m_DateRange = dateRange;

        //    plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common1", "Text_Duration").ToString();
        //    plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), LanguageResourceHelper.GetDateLabelForDateRange(dateRange));
        //    plotData.m_thresholdRegion_1 = null;
        //    plotData.m_thresholdRegion_2 = null;

        //    plotData.m_graphsToPlot = new List<AHAGraphData>();
        //    AHAGraphData graphData = new AHAGraphData();
        //    //graphData.m_color = System.Drawing.Color.FromArgb(180, 0x73, 0x95, 0xBF);
        //    graphData.m_color = AHAGraphColor.PhysicalActivity;
        //    plotData.m_graphsToPlot.Add(graphData);
        //    graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common1", "Text_Duration").ToString();

        //    graphData.m_readingUnits = System.Web.HttpContext.GetGlobalResourceObject("Common1", "Text_Minutes").ToString();


        //    //For Exercise, the threshold is 30 as specified by AHA
        //    Color shadingColor = AHAGraphColor.PhysicalActivityGoalShading;
        //    plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion(ThresholdDirection.ShadeLower, 30, shadingColor, "", System.Web.HttpContext.GetGlobalResourceObject("Common1", "Text_BelowMyGoal").ToString());
        //    return plotData;
        //}

        //private static AHAPlotData _PopulatePlotDataForExercise(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        //{
        //    Graph.AHAPlotData plotData = _CreatePlotDataForExercise(dateRange);
        //    Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

        //    if (objPatGuids != null)
        //    {
        //        HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

        //        List<HVManager.ExerciseDataManager.ExerciseItem> objEx = mgr.ExerciseDataManager.GetDataBetweenDates(dateRange).ToList();

        //        if (objEx != null && objEx.Count > 0)
        //        {
        //            Graph.GraphInputReading lastReadingAdded = null;

        //            foreach (HVManager.ExerciseDataManager.ExerciseItem data in objEx)
        //            {
        //                if (!data.Duration.Value.HasValue)
        //                {
        //                    continue;
        //                }

        //                if (lastReadingAdded != null && lastReadingAdded.DateOfReading.Date == data.When.Value.Date)
        //                {
        //                    lastReadingAdded.ReadingValue += (float)data.Duration.Value.Value;
        //                    continue;
        //                }

        //                Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
        //                reading.DateOfReading = data.When.Value.Date;
        //                reading.ReadingValue = (float)data.Duration.Value.Value;

        //                graphData.m_readingList.Add(reading);
        //                lastReadingAdded = reading;
        //            }
        //        }
        //    }
        //    return plotData;
        //}

        private static AHAPlotData _CreatePlotDataForExercise(AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();
            plotData.m_GraphType = GraphType.Exercise;

            plotData.bShowTimeOfReadingOnMouseOverDiv = false; //Dont show time of day while rendering...
            plotData.m_bForceYAxisMinToZero = true;//We need y axis to start from zero in this case
            plotData.m_graphRenderingMode = GraphRenderingMode.BarChart;
            plotData.m_DateRange = dateRange;

            //plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common1", "Text_Duration").ToString();
            plotData.m_yaxisLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_WeeklyDurationInMinutes").ToString(); 
            if (dateRange == AHACommon.DataDateReturnType.Last6Months ||
                dateRange == AHACommon.DataDateReturnType.Last9Months ||
                dateRange == AHACommon.DataDateReturnType.LastYear ||
                dateRange == AHACommon.DataDateReturnType.Last2Years)
            {
                plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), "MM/yy");
            }
            else
            {
                plotData.m_xaxisLabel = string.Format("{0} ({1})", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Date"), AHACommon.DateTimeHelper.GetShortDateFormatForWeb());
            }
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            //graphData.m_color = System.Drawing.Color.FromArgb(180, 0x73, 0x95, 0xBF);
            graphData.m_color = AHAGraphColor.PatientPhysicalActivity;
            plotData.m_graphsToPlot.Add(graphData);
            //graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common1", "Text_Duration").ToString();
            graphData.m_graphDataPlotLabel = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_WeeklyDuration").ToString();
            graphData.m_readingUnits = System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Minutes").ToString();


            graphData.m_goalData = new Heart360Web.Graph.AHAPlotGoalData();
            graphData.m_goalData.GoalLineColor = AHAGraphColor.PatientPhysicalActivityGoal;
            graphData.m_goalData.GoalLineType = Heart360Web.Graph.GoalLineType.Straight;

            //BP Goal does not have start and end dates
            graphData.m_goalData.GoalStartDate = DateTime.MinValue;
            graphData.m_goalData.GoalEndDate = DateTime.MaxValue;

            //plotData.m_goalData.m_goalStartValue = (float)bpGoalWrapper.StartDiastolic.Value.Value;
            graphData.m_goalData.GoalEndValue = 120;

            //For Exercise, the threshold is 30 as specified by AHA
            //Color shadingColor = AHAGraphColor.PhysicalActivityGoalShading;
            //plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion(ThresholdDirection.ShadeLower, 30, shadingColor, "", System.Web.HttpContext.GetGlobalResourceObject("Common1", "Text_BelowMyGoal").ToString());

            //For Exercise, the threshold is 120 as specified by AHA
            Color shadingColor = AHAGraphColor.PatientPhysicalActivityGoalShading;
            plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion(ThresholdDirection.ShadeLower, 120, shadingColor, "", System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BelowMyGoal").ToString());
            return plotData;
        }

        private static AHAPlotData _PopulatePlotDataForExercise(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            Graph.AHAPlotData plotData = _CreatePlotDataForExercise(dateRange);
            Graph.AHAGraphData graphData = plotData.m_graphsToPlot[0];

            Dictionary<DateTime, Dictionary<DateTime, ExerciseWeekData>> dicExerciseGraphData = UserExercise.GetGraphDataForExercise(objPatGuids, dateRange);
            graphData.m_XAxisLabels = new List<DateTime>();

            if (dateRange == AHACommon.DataDateReturnType.Last3Months)
            {
                //Added for giving some spacing to the left side
                //6 days subtracted from the start date and  inserted in to m_XAxisLabels  for some spacing on left hand side of the graph
                graphData.m_XAxisLabels.Add(dicExerciseGraphData.Values.SelectMany(e => e.Keys).Min().AddDays(-6));
            }

            foreach (KeyValuePair<DateTime, Dictionary<DateTime, ExerciseWeekData>> value in dicExerciseGraphData)
            {
                if (dateRange == AHACommon.DataDateReturnType.Last6Months ||
                    dateRange == AHACommon.DataDateReturnType.Last9Months ||
                    dateRange == AHACommon.DataDateReturnType.LastYear ||
                    dateRange == AHACommon.DataDateReturnType.Last2Years)
                {
                    // for these date ranges we need to plot the outter keys as X axis labels
                    graphData.m_XAxisLabels.Add(value.Key);
                }

                foreach (KeyValuePair<DateTime, ExerciseWeekData> val in value.Value)
                {
                    if (dateRange == AHACommon.DataDateReturnType.Last3Months)
                    {
                        // for '3 months' date range we need to plot the inner keys(inner keys represents the end of the week to plot) as X axis labels
                        //inner keys represent the end date of a week we need to plot
                        graphData.m_XAxisLabels.Add(val.Key);
                    }
                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = val.Value.EndDate;
                    reading.ReadingValue = (float)val.Value.TotalMinutes;
                    StringBuilder sbData = new StringBuilder();
                    sbData.Append(reading.ReadingValue);
                    sbData.Append(" ");
                    sbData.Append(graphData.m_readingUnits);
                    sbData.Append("<br>");
                    string dtStartDateString = GRCBase.DateTimeHelper.GetFormattedDateTimeString(val.Value.StartDate, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                    string dtEndDateString = GRCBase.DateTimeHelper.GetFormattedDateTimeString(val.Value.EndDate, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                    sbData.Append(string.Format(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_ExerciseGraphToolTip").ToString(), dtStartDateString, dtEndDateString));
                    reading.SetDisplayValueForGraph(sbData.ToString());
                    graphData.m_readingList.Add(reading);
                }
            }
            //6 days added to the end date and inserted in to m_XAxisLabels  for some spacing on right hand side of the graph
            if( dicExerciseGraphData.Count > 0 )
                graphData.m_XAxisLabels.Add(dicExerciseGraphData.Values.SelectMany(e => e.Keys).Max().AddDays(6));
            return plotData;
        }


#if false
        private static AHAPlotData _ConstructPlotDataForExercise(PatientGuids objPatGuids, AHACommon.DataDateReturnType dateRange)
        {
            
            Graph.AHAPlotData plotData = new Heart360Web.Graph.AHAPlotData();

            plotData.m_GraphType = GraphType.Exercise;

            plotData.bShowTimeOfReadingOnMouseOverDiv = false; //Dont show time of day while rendering...
            plotData.m_bForceYAxisMinToZero = true;//We need y axis to start from zero in this case
            plotData.m_graphRenderingMode = GraphRenderingMode.BarChart;
            plotData.m_DateRange = dateRange;
            //plotData.m_readingUnits = "minutes";
            plotData.m_yaxisLabel = "Duration";
            plotData.m_xaxisLabel = "Date";
            plotData.m_thresholdRegion_1 = null;
            plotData.m_thresholdRegion_2 = null;

            plotData.m_graphsToPlot = new List<AHAGraphData>();
            AHAGraphData graphData = new AHAGraphData();
            graphData.m_color = System.Drawing.Color.FromArgb(180, 0x73, 0x95, 0xBF);
            plotData.m_graphsToPlot.Add(graphData);
            graphData.m_graphDataPlotLabel = "Duration";
            //Now Added
            graphData.m_readingUnits = "minutes";
            //End Now Added

            //For Exercise, the threshold is 30 as specified by AHA
            Color shadingColor = ThresholdRegion.DefaultShadingColor;
            plotData.m_thresholdRegion_1 = new Graph.ThresholdRegion(ThresholdDirection.ShadeLower, 30, shadingColor, "", "Below my goal");

            if (objPatGuids!=null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatGuids.PersonID, objPatGuids.RecordID);

                List<HVManager.ExerciseDataManager.ExerciseItem> objEx = mgr.ExerciseDataManager.GetDataBetweenDates(dateRange).ToList();

                if (objEx != null && objEx.Count > 0)
                {
                    Graph.GraphInputReading lastReadingAdded = null;

                    foreach (HVManager.ExerciseDataManager.ExerciseItem data in objEx)
                    {
                        if (!data.Duration.Value.HasValue)
                        {
                            continue;
                        }

                        if (lastReadingAdded != null && lastReadingAdded.DateOfReading.Date == data.When.Value.Date)
                        {
                            lastReadingAdded.ReadingValue += (float)data.Duration.Value.Value;
                            continue;
                        }

                        Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                        reading.DateOfReading = data.When.Value.Date;
                        reading.ReadingValue = (float)data.Duration.Value.Value;

                        graphData.m_readingList.Add(reading);
                        lastReadingAdded = reading;
                    }
                }
            }
            else
            {
                LinkedList<AerobicExerciseDataManager.AerobicItem> aerobicData = ExerciseDataWrapper.GetDataBetweenDates(dateRange);

                Graph.GraphInputReading lastReadingAdded = null;

                foreach (AerobicExerciseDataManager.AerobicItem data in aerobicData)
                {
                    if (data.DurationInMinutes == null || !data.DurationInMinutes.HasValue)
                    {
                        continue;
                    }

                    if (lastReadingAdded != null && lastReadingAdded.DateOfReading.Date == data.When.Date)
                    {
                        lastReadingAdded.ReadingValue += (float)data.DurationInMinutes.Value;
                        continue;
                    }

                    Graph.GraphInputReading reading = new Heart360Web.Graph.GraphInputReading();
                    reading.DateOfReading = data.When.Date;
                    reading.ReadingValue = (float)data.DurationInMinutes.Value;

                    graphData.m_readingList.Add(reading);
                    lastReadingAdded = reading;
                }
            }
            return plotData;
        }

#endif
        public static AHAPlotData ConstructPlotData(List<AHAHelpContent.Patient> objPatients, GraphGroupType graphsToPlot, GraphGroupType groupForYaxis, AHACommon.DataDateReturnType dateRange)
        {
            List<PatientGuids> listGuids = UIHelper.GetPatientGuidList(objPatients);
            return _PopulateAllInOnePlotData(listGuids, graphsToPlot, groupForYaxis, dateRange);
        }

        public static AHAPlotData ConstructPlotData(PatientGuids objPatGuids, GraphType graphType, AHACommon.DataDateReturnType dateRange)
        {
            switch (graphType)
            {
                case GraphType.Glucose:
                    return _PopulatePlotDataForBG(objPatGuids, dateRange);
                case GraphType.PressureSystolic:
                    return _PopulatePlotDataForPressureSystolic(objPatGuids, dateRange);
                case GraphType.PressureDiastolic:
                    return _PopulatePlotDataForPressureDiastolic(objPatGuids, dateRange);
                case GraphType.TotalCholesterol:
                    return _PopulatePlotDataForTotalCholesterol(objPatGuids, dateRange);
                case GraphType.LDL:
                    return _PopulatePlotDataForLDL(objPatGuids, dateRange);
                case GraphType.HDL:
                    return _PopulatePlotDataForHDL(objPatGuids, dateRange);
                case GraphType.Triglycerides:
                    return _PopulatePlotDataForTriglycerides(objPatGuids, dateRange);
                case GraphType.Weight:
                    return _PopulatePlotDataForWeight(objPatGuids, dateRange);
                case GraphType.CombinedBP:
                    return _PopulatePlotDataForCombinedBP(objPatGuids, dateRange);
                case GraphType.CombinedCholesterol:
                    return _PopulatePlotDataForCombinedCholesterol(objPatGuids, dateRange);
                case GraphType.Exercise:
                    return _PopulatePlotDataForExercise(objPatGuids, dateRange);
                //case GraphType.PatientExercise:
                //    return _PopulatePlotDataForPatientExercise(objPatGuids, dateRange);
                //return _PopulatePlotDataForExercise(objPatGuids, dateRange);
                case GraphType.SPRGlucose:
                    return _PopulatePlotDataForSPRBG(dateRange);
                case GraphType.SPRCombinedBP:
                    return _PopulatePlotDataForForSPRBP(dateRange);
                case GraphType.SPRCombinedCholesterol:
                    return _PopulatePlotDataForSPRCholesterol(dateRange);
                case GraphType.SPRWeight:
                    return _PopulatePlotDataForSPRWeight(dateRange);
                case GraphType.SPRExercise:
                    return _PopulatePlotDataForSPRPhysicalActivity(dateRange);
            }

            throw new System.Exception("Unknown graph type to plot");
        }

    }


    /// <summary>
    /// Represents the type of a goal line. For Weight it is slopped and for graphs like Blood pressure, it is Straight
    /// </summary>
    public enum GoalLineType
    {
        Sloped,
        Straight
    }

    /// <summary>
    /// The various attributes for a Goal is represented here
    /// </summary>
    public class AHAPlotGoalData
    {
        private Color _goalLineColor;
        public Color GoalLineColor
        {
            get
            {
                return _goalLineColor;
            }
            set
            {
                _goalLineColor = value;
            }
        }

        private DateTime _goalStartDate;
        public DateTime GoalStartDate
        {
            get
            {
                return _goalStartDate;
            }
            set
            {
                _goalStartDate = value;
            }
        }

        private DateTime _goalEndDate;
        public DateTime GoalEndDate
        {
            get
            {
                return _goalEndDate;
            }
            set
            {
                _goalEndDate = value;
            }
        }

        private float _goalStartValue;
        public float GoalStartValue
        {
            get
            {
                return _goalStartValue;
            }
            set
            {
                _goalStartValue = value;
            }
        }

        private float _goalEndValue;
        public float GoalEndValue
        {
            get
            {
                return _goalEndValue;
            }
            set
            {
                _goalEndValue = value;
            }
        }

        private string _goalStartDisplayValue;
        public string GoalStartDisplayValue
        {
            get
            {
                if (_goalStartDisplayValue != null)
                    return _goalStartDisplayValue;
                return _goalStartValue.ToString();
            }
        }

        private GoalLineType _goalLineType;
        public GoalLineType GoalLineType
        {
            get
            {
                return _goalLineType;
            }
            set
            {
                _goalLineType = value;
            }
        }

        public void SetGoalStartDisplayValueForGraph(string displayValue)
        {
            _goalStartDisplayValue = displayValue;
        }


        private string _goalEndDisplayValue;
        public string GoalEndDisplayValue
        {
            get
            {
                if (_goalEndDisplayValue != null)
                    return _goalEndDisplayValue;
                return _goalEndValue.ToString();
            }
        }
        public void SetGoalEndDisplayValueForGraph(string displayValue)
        {
            _goalEndDisplayValue = displayValue;
        }



        public AHAPlotGoalData()
        {
            _goalStartDate = DateTime.Now.Date;
            _goalEndDate = DateTime.Now.Date;
            _goalStartValue = 0;
            _goalEndValue = 0;
            _goalLineColor = System.Drawing.Color.Blue;
        }
    }


    /// <summary>
    /// This class implements the funtionality of plotting the graph on a surface
    /// Pay specific attention to the DrawGraph method
    /// </summary>
    public class AHAGraphGenerator
    {
        /// <summary>
        /// Contains values when the graph is being drawn
        /// </summary>
        public class GraphContextData
        {
            public DateTime goalStartDate = DateTime.Now;      //This will contain the goal start date in the current graph's context
            public DateTime goalEndDate = DateTime.Now;        //This will contain the goal end date in the current graph's context
            public float goalStartValue = 0;                   //This will contain the goal start value in the current graph's context
            public float goalEndValue = 0;                     //This will contain the goal start value in the current graph's context


            public float[] GL_X1X2;     //To store goal line plot data
            public float[] GL_Y1Y2;     //To store goal line plot data
            public float[] GP_X1X2;     //To store goal point plot data
            public float[] GP_Y1Y2;     //To store goal point plot data
            public float[] DL_X1X2;     //To store data line plot and point plot data
            public float[] DL_Y1Y2;     //To store data line plot and point plot data

            public bool bDrawGoalLine = false;
        }


        private static string fontFamily = "Arial";

        //The dimensions of the graph we will plot
        public const int defaultGraphWidth = 598;
        public const int defaultGraphHeight = 300;
        //The dimensions of the graph we will plot
        public const int reportGraphWidth = 600;
        public const int reportGraphHeight = 400;


        public DateTime currentEndDate = DateTime.Now.Date;//The End date on graph will be stored here while plotting
        public DateTime currentStartDate = DateTime.Now.Date;//The Start date on graph will be stored here while plotting


        double minValue = double.MaxValue;          //The min y value will be stored in these as the plot will progress
        double maxValue = double.MinValue;          //The max y value will be stored in these as the plot will progress
        int nPointCount = 0;                        //The number of points on x axis will be calulated and stored here




        public List<GraphContextData> m_ContextDataList = new List<GraphContextData>();

        public AHAPlotData m_plotData = null;              //Will hold the plot values
        IPlotSurface2D m_plotSurface;               //The Surface on which we will draw the graph

        public AHAGraphGenerator(AHAPlotData plotData, IPlotSurface2D plotSurface)
        {
            m_plotData = plotData;
            m_plotSurface = plotSurface;

        }

        /// <summary>
        /// Returns the threshold in the max/min range of the current yaxis 
        /// </summary>
        /// <param name="threshold"></param>
        /// <returns>Threshold in the range of graph</returns>
        private float _getAdjustedThreshold(float threshold)
        {
            if (threshold < m_plotSurface.YAxis1.WorldMin)
            {
                return (float)m_plotSurface.YAxis1.WorldMin;
            }

            if (threshold > m_plotSurface.YAxis1.WorldMax)
            {
                return (float)m_plotSurface.YAxis1.WorldMax;
            }

            return threshold;
        }

        /// <summary>
        /// For each AHAGraphData instantiate a GraphContextData object
        /// </summary>
        private void PopulateContextData()
        {
            foreach (AHAGraphData graphData in m_plotData.m_graphsToPlot)
            {
                GraphContextData contextData = new GraphContextData();
                m_ContextDataList.Add(contextData);
            }
        }


        /// <summary>
        /// Calculates and sets the goalStartDate, goalEndDate, goalStartValue, goalEndValue 
        /// as it should be plotted in the current graph. 
        /// </summary>
        /// <returns>If the goal date overlaps with graph range, returns true. Else returns false</returns>
        private bool _NormalizeGoalValues(AHAGraphData graphData, GraphContextData contextData)
        {
            //If the goal starts later or ends earlier than current graph range, we dont have to plot anything for goal
            if (contextData.goalStartDate > currentEndDate || contextData.goalEndDate < currentStartDate)
            {
                return false;
            }

            //If the goal begins earlier than current graph range, find the propotional start value 
            if (contextData.goalStartDate < currentStartDate)
            {

                if (graphData.m_goalData.GoalLineType == GoalLineType.Sloped)
                {
                    double perDayDelta = ((contextData.goalEndValue - contextData.goalStartValue) / (contextData.goalEndDate - contextData.goalStartDate).Days);
                    contextData.goalStartValue = (float)(contextData.goalStartValue + perDayDelta * (currentStartDate - contextData.goalStartDate).Days);
                }
                else
                {
                    contextData.goalStartValue = graphData.m_goalData.GoalEndValue;
                }
                contextData.goalStartDate = currentStartDate;
            }

            //If the goal ends later than current graph range, find the propotional end value
            if (contextData.goalEndDate > currentEndDate)
            {
                if (graphData.m_goalData.GoalLineType == GoalLineType.Sloped)
                {
                    double perDayDelta = ((contextData.goalEndValue - contextData.goalStartValue) / (contextData.goalEndDate - contextData.goalStartDate).Days);
                    contextData.goalEndValue = (float)(contextData.goalEndValue - perDayDelta * (contextData.goalEndDate - currentEndDate).Days);
                }
                else
                {
                    contextData.goalEndValue = graphData.m_goalData.GoalEndValue;
                }

                contextData.goalEndDate = currentEndDate;
            }

            return true;
        }

        /// <summary>
        /// Gets the x axis point on which a given date time reading maps to.
        /// </summary>
        /// <param name="nPointCount">Number of points on xaxis</param>
        /// <param name="dtStart">Start date on xaxis</param>
        /// <param name="dtEnd">End date on xaxis</param>
        /// <param name="readingDt">The reading datetime for which xaxis point needs to be determined</param>
        /// <returns>The xaxis point on which the readingDt maps to</returns>
        private float _getGraphPoint(int nPointCount, DateTime dtStart, DateTime dtEnd, DateTime readingDt)
        {
            double res = (readingDt.Date - dtStart).Days;
            double perMinute = 1.0 / (24.0 * 60.0);
            res += readingDt.TimeOfDay.TotalMinutes * perMinute;
            return (float)res;
        }


        /// <summary>
        /// Calculate the end date of the graph to be plotted
        /// </summary>
        /// <returns></returns>
        private DateTime _CalculateEndDateForGraph()
        {
            return DateTime.Now.Date.AddDays(1);
        }

        /// <summary>
        /// Calculate the start date of the graph to be plotted
        /// </summary>
        /// <returns></returns>
        private DateTime _CalculateStartDateForGraph()
        {
            DateTime currentDate = DateTime.Now.Date;
            if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last3Months)
            {
                return currentDate.AddDays(-90);
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last6Months)
            {
                return currentDate.AddDays(-180);
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.LastMonth)
            {
                return currentDate.AddDays(-30);
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.LastWeek)
            {
                return currentDate.AddDays(-7);
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last9Months)
            {
                return currentDate.AddDays(-270);
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.LastYear)
            {
                return currentDate.AddYears(-1);
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last2Years)
            {
                return currentDate.AddYears(-2);
            }
            throw new System.Exception("Invalid Date range");
        }




        /// <summary>
        /// This function sets the basis styles for the graph and adds the legend object
        /// </summary>
        private void _InitializeSurface()
        {
            m_plotSurface.Clear();
            m_plotSurface.AutoScaleAutoGeneratedAxes = false;
            m_plotSurface.PlotBackColor = Color.White;
            m_plotSurface.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            //Generating Grid for Graph and styling
            Grid WtGrid = new Grid();

            if (m_plotData.m_graphRenderingMode == GraphRenderingMode.LineAndPoint)
            {
                WtGrid.VerticalGridType = Grid.GridType.Coarse;
                WtGrid.HorizontalGridType = Grid.GridType.Coarse;
                WtGrid.MajorGridPen = new Pen(Color.LightGray, 1.5f);
                WtGrid.MinorGridPen = new Pen(Color.LightGray, 0.2f);
                m_plotSurface.Add(WtGrid, 0);
            }
            else
            {
                if (m_plotData.m_GraphType == GraphType.Exercise)
                {
                    WtGrid.VerticalGridType = Grid.GridType.Coarse;
                    WtGrid.HorizontalGridType = Grid.GridType.Coarse;

                    WtGrid.MajorGridPen = new Pen(Color.LightGray, 1.5f);
                    //WtGrid.MajorGridPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                    WtGrid.MinorGridPen = new Pen(Color.LightGray, 0.2f);
                    m_plotSurface.Add(WtGrid, 0);
                }
                else
                {
                    WtGrid.VerticalGridType = Grid.GridType.Coarse;
                    WtGrid.HorizontalGridType = Grid.GridType.None;

                    WtGrid.MajorGridPen = new Pen(Color.LightGray, 1.5f);
                    WtGrid.MajorGridPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                    WtGrid.MinorGridPen = new Pen(Color.LightGray, 0.2f);
                    m_plotSurface.Add(WtGrid, 110);
                }
            }




            //generating and styling the X-Axis for Date
            LabelAxis WtXAxis = new LabelAxis();
            m_plotSurface.XAxis1 = WtXAxis;
            m_plotSurface.XAxis1.AxisColor = Color.LightGray;
            m_plotSurface.XAxis1.TickTextColor = Color.Black;
            m_plotSurface.XAxis1.TickTextNextToAxis = false;
            m_plotSurface.XAxis1.FlipTicksLabel = true;
            m_plotSurface.XAxis1.TickTextFont = new Font(new FontFamily(fontFamily), 10.0f);
            m_plotSurface.XAxis1.TicksLabelOffSetX = -4;
            m_plotSurface.XAxis1.TicksLabelOffSetY = 8;
            m_plotSurface.XAxis1.OnlyTicksText = true;

            m_plotSurface.XAxis1.LabelOffset = 15;
            m_plotSurface.XAxis1.Label = m_plotData.m_xaxisLabel;
            m_plotSurface.XAxis1.LabelFont = new Font(new FontFamily(fontFamily), 12.0f, FontStyle.Bold);
            m_plotSurface.XAxis1.LabelColor = Color.Black;

            m_plotSurface.Padding = 10;  //Otherwise the xaxis labels get cut off on right end

            //generating and styling the Y-Axis for weight
            LabelAxis WtYAxis = new LabelAxis();
            m_plotSurface.YAxis1 = WtYAxis;
            m_plotSurface.YAxis1.AxisColor = Color.LightGray;
            m_plotSurface.YAxis1.TickTextColor = Color.Black;
            m_plotSurface.YAxis1.TickTextFont = new Font(new FontFamily(fontFamily), 10.0f);
            m_plotSurface.YAxis1.Label = m_plotData.m_yaxisLabel;
            m_plotSurface.YAxis1.LabelFont = new Font(new FontFamily(fontFamily), 12.0f, FontStyle.Bold);
            m_plotSurface.YAxis1.LabelColor = Color.Black;
            m_plotSurface.YAxis1.LabelOffset = 5;

            //We are not using axis 2
            m_plotSurface.XAxis2 = null;
            m_plotSurface.YAxis2 = null;

            //Consturct Legent object and assign it to graph
            NPlot.Legend npLegend = new Legend();
            //May need to change the below code
            //npLegend.AttachTo(NPlot.PlotSurface2D.XAxisPosition.Top, NPlot.PlotSurface2D.YAxisPosition.Right);
            npLegend.AttachTo(NPlot.PlotSurface2D.XAxisPosition.Bottom, NPlot.PlotSurface2D.YAxisPosition.Right);
            npLegend.NumberItemsHorizontally = -1;
            npLegend.NumberItemsVertically = 2;

            npLegend.Font = new Font(fontFamily, 10.0f);
            npLegend.XOffset = 0;
            //npLegend.YOffset = -8;
            npLegend.YOffset = 45;

            npLegend.AutoScaleText = false;
            npLegend.VerticalEdgePlacement = NPlot.Legend.Placement.Inside;
            npLegend.HorizontalEdgePlacement = NPlot.Legend.Placement.Outside;
            npLegend.BorderStyle = NPlot.LegendBase.BorderType.None;
            npLegend.BorderColor = Color.White;
            npLegend.BackgroundColor = Color.White;

            m_plotSurface.Legend = npLegend;

            //Fake a Point plot so that it appears in legend as "above threshold" zone indicator

            if (m_plotData.m_GraphType == GraphType.Exercise)
            {
                PointPlot dummyPointPlot = new PointPlot();


                Marker dummyMarker = new Marker(Marker.MarkerType.FilledSquare, 19, m_plotData.m_graphsToPlot[0].m_color);
                dummyPointPlot.Marker = dummyMarker;

                dummyPointPlot.ShowInLegend = true;
                dummyPointPlot.Label = m_plotData.m_graphsToPlot[0].m_graphDataPlotLabel;
                m_plotSurface.Add(dummyPointPlot, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left, -1000);
            }

            if (m_plotData.m_thresholdRegion_1 != null)
            {
                PointPlot dummyPointPlot = new PointPlot();


                Marker dummyMarker = new Marker(Marker.MarkerType.FilledSquare, 19, m_plotData.m_thresholdRegion_1.m_shadingColor);
                dummyPointPlot.Marker = dummyMarker;

                dummyPointPlot.ShowInLegend = true;
                dummyPointPlot.Label = m_plotData.m_thresholdRegion_1.m_legendText;
                m_plotSurface.Add(dummyPointPlot, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left, -1000);
            }

            if (m_plotData.m_thresholdRegion_2 != null)
            {
                PointPlot dummyPointPlot = new PointPlot();
                Marker dummyMarker = new Marker(Marker.MarkerType.FilledSquare, 19, m_plotData.m_thresholdRegion_2.m_shadingColor);
                dummyPointPlot.Marker = dummyMarker;
                dummyPointPlot.ShowInLegend = true;
                dummyPointPlot.Label = m_plotData.m_thresholdRegion_2.m_legendText;
                m_plotSurface.Add(dummyPointPlot, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left, -1000);
            }
        }

        /// <summary>
        /// Set the labels for xaxis based on date range
        /// </summary>
        private void _DoLabelXaxis(DateTime endDate)
        {
            LabelAxis WtXAxis = m_plotSurface.XAxis1 as LabelAxis;





            //Set the labels for the x axis selectively based on what interval we are going to plot
            if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last3Months)
            {
                //Label Every 10 days 9 times
                for (int i = 0; i < 9; i++)
                {
                    double labelPosition = nPointCount - (i * 10) - 1;
                    WtXAxis.AddLabel(endDate.AddDays(-i * 10).ToString(AHACommon.DateTimeHelper.GetShortDateFormatForWeb()), labelPosition);
                }
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last9Months)
            {
                //Label Every 30 days 9 times
                for (int i = 0; i < 9; i++)
                {
                    double labelPosition = nPointCount - (i * 30) - 1;
                    WtXAxis.AddLabel(endDate.AddDays(-i * 30).ToString(AHACommon.DateTimeHelper.GetShortDateFormatForWeb()), labelPosition);
                }
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.LastYear)
            {
                //Label Every 30 days 12 times(May need to change this code)
                for (int i = 0; i < 12; i++)
                {
                    double labelPosition = nPointCount - (i * 30) - 1;
                    WtXAxis.AddLabel(endDate.AddDays(-i * 30).ToString("MM/yy"), labelPosition);
                }
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last2Years)
            {
                //Label Every 60 days 12 times(May need to change this code)
                for (int i = 0; i < 12; i++)
                {
                    double labelPosition = nPointCount - (i * 60) - 1;
                    WtXAxis.AddLabel(endDate.AddDays(-i * 60).ToString("MM/yy"), labelPosition);
                }
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last6Months)
            {
                //Label Every 30 days 6 times
                for (int i = 0; i < 6; i++)
                {
                    double labelPosition = nPointCount - (i * 30) - 1;
                    WtXAxis.AddLabel(endDate.AddDays(-i * 30).ToString(AHACommon.DateTimeHelper.GetShortDateFormatForWeb()), labelPosition);
                }
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.LastMonth)
            {
                //Label Every 5 days
                for (int i = 0; i < 6; i++)
                {
                    double labelPosition = nPointCount - (i * 5) - 1;
                    WtXAxis.AddLabel(endDate.AddDays(-i * 5).ToString(AHACommon.DateTimeHelper.GetShortDateFormatForWeb()), labelPosition);
                }
            }
            else if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.LastWeek)
            {
                //Label Every day
                for (int i = 0; i < nPointCount; i++)
                {
                    double labelPosition = nPointCount - i - 1;
                    WtXAxis.AddLabel(endDate.AddDays(-i).ToString(AHACommon.DateTimeHelper.GetShortDateFormatForWeb()), labelPosition);
                }
            }
        }

        /// <summary>
        /// Set the labels for xaxis(Exercise)
        /// </summary>
        private void _DoLabelXaxisForExercise()
        {
            LabelAxis WtXAxis = m_plotSurface.XAxis1 as LabelAxis;
            int intXAxisLabelCount = 0;
            if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last3Months)
            {
                //For last 3 months we plot x axis labels in weekly intervals
                int intLabelPosition = 7;
                foreach (DateTime dt in m_plotData.m_graphsToPlot[0].m_XAxisLabels)
                {
                    if (intXAxisLabelCount == 0)
                    {
                        //no need to plot the first value because it is inserted in m_XAxisLabels by us for some spacing on left hand side of the graph
                        intXAxisLabelCount++;
                        continue;
                    }
                    else if (intXAxisLabelCount == (m_plotData.m_graphsToPlot[0].m_XAxisLabels.Count - 1))
                    {
                        //no need to plot the last value because it is inserted in m_XAxisLabels by us for some spacing on right hand side of the graph
                        break;
                    }
                    else
                    {
                        intXAxisLabelCount++;
                        WtXAxis.AddLabel(dt.ToString(AHACommon.DateTimeHelper.GetShortDateFormatForWeb()), intLabelPosition);
                        intLabelPosition += 7;
                    }
                }
            }
            else
            {
                //For data other than 'last 3 months' we plot x axis labels in monthly intervals
                
                DateTime dtPreviousPlottedDate = m_plotData.m_graphsToPlot[0].m_XAxisLabels[0];
                int intPreviousLabelPosition = 0;
                foreach (DateTime dt in m_plotData.m_graphsToPlot[0].m_XAxisLabels)
                {
                    int labelPosition = 0;
                    if (intXAxisLabelCount == 0)
                    {
                        intXAxisLabelCount++;
                        labelPosition = 0;
                        WtXAxis.AddLabel(dt.ToString("MM/yy"), labelPosition);
                        dtPreviousPlottedDate = dt;
                        intPreviousLabelPosition = 0;
                    }
                    else if (intXAxisLabelCount == (m_plotData.m_graphsToPlot[0].m_XAxisLabels.Count - 1))
                    {
                        //no need to plot the last value because it is inserted in m_XAxisLabels by us for some spacing on right hand side of the graph
                        break;
                    }
                    else
                    {
                        intXAxisLabelCount++;
                        TimeSpan ts = new TimeSpan(dt.Ticks - dtPreviousPlottedDate.Ticks);
                        labelPosition = intPreviousLabelPosition + ts.Days;
                        WtXAxis.AddLabel(dt.ToString("MM/yy"), labelPosition);
                        dtPreviousPlottedDate = dt;
                        intPreviousLabelPosition = labelPosition;
                    }
                }
            }
        }


        public static int GetYaxisLabelCountAndAdjustMinMaxVals(bool forceYaxisToZero, double minVal, double maxVal, ref double resultMin, ref double resultMax)
        {

            int nYAxisLabelCount = 5;
            if (forceYaxisToZero)
            {
                minVal = 0;
            }
            else
            {
                //Down the min value so that the values are divisible by nYAxisLabelCount
                minVal = minVal - ((int)(maxVal - minVal)) % nYAxisLabelCount;
            }

            if (minVal < 0)
                minVal = 0;

            //Make sure the min value is a multiple of 10
            if (minVal % 10 != 0)
            {
                minVal = (((int)minVal) / 10) * 10;
            }

            if (maxVal % 10 != 0)
            {
                maxVal = (((int)maxVal) / 10) * 10 + 10;
            }


            if ((maxVal - minVal) / 10 < nYAxisLabelCount)
            {
                nYAxisLabelCount = (int)(maxVal - minVal) / 10;
            }

            resultMin = minVal;
            resultMax = maxVal;
            return nYAxisLabelCount;
        }


        /// <summary>
        /// Draw yaxis labels and do the FINAL adjust on minValue and maxValue on yaxis respectively
        /// </summary>
        private void _DoLabelYaxis()
        {
            LabelAxis WtYAxis = m_plotSurface.YAxis1 as LabelAxis;

            int nYAxisLabelCount = GetYaxisLabelCountAndAdjustMinMaxVals(m_plotData.m_bForceYAxisMinToZero, minValue, maxValue, ref minValue, ref maxValue);

            //Draw nYAxisLabelCount  axis labels
            double distanceBetweenPoints = 10;


            if (m_plotData.m_distanceBetweenYaxisPoints > 0)
            {
                distanceBetweenPoints = m_plotData.m_distanceBetweenYaxisPoints;
            }
            else
            {
                //Autocalculate
                distanceBetweenPoints = (maxValue - minValue) / (float)nYAxisLabelCount;
                if (distanceBetweenPoints % 10 != 0)
                {
                    if (distanceBetweenPoints % 10 < 3)
                    {
                        distanceBetweenPoints = distanceBetweenPoints - (distanceBetweenPoints % 10);
                    }
                    else
                    {
                        distanceBetweenPoints = distanceBetweenPoints + (10 - distanceBetweenPoints % 10);
                    }
                }
            }

            int labelFrequency = 1;  //Label all points in yaxis
            int totalYaxisPoints = (int)(maxValue - minValue) / (int)distanceBetweenPoints;
            if (totalYaxisPoints < 3)
            {
                totalYaxisPoints = 3;
            }


            //labelFrequency = totalYaxisPoints / 5;
            //if (labelFrequency == 0)
            //{
            //    labelFrequency = 1;
            //}



            for (int i = 0; i < totalYaxisPoints + 1; i++)
            {
                double labelPosition = minValue + distanceBetweenPoints * i;

                if (i % labelFrequency == 0)
                {
                    WtYAxis.AddLabel(labelPosition.ToString("0"), Math.Abs(labelPosition));
                }
                else
                {
                    WtYAxis.AddLabel("", Math.Abs(labelPosition));
                }

            }
        }



        private LinePlot _CreateLinePlot(AHAGraphData graphData, GraphContextData contextData)
        {
            LinePlot LinePlotGraph = new LinePlot();
            LinePlotGraph.Pen = new Pen(graphData.m_color, (float)graphData.m_graphLineThickness);
            LinePlotGraph.ShowInLegend = true;
            LinePlotGraph.Label = graphData.m_graphDataPlotLabel;
            return LinePlotGraph;

        }


        private PointPlot _CreatePointPlot(AHAGraphData graphData, GraphContextData contextData)
        {
            PointPlot PointPlotGraph = new PointPlot();

            Marker marker = new Marker(Marker.MarkerType.FilledCircle, 4, graphData.m_color);
            PointPlotGraph.Marker = marker;
            PointPlotGraph.ShowInLegend = false;

            return PointPlotGraph;

        }


        private BarPlot _CreateBarPlot(AHAGraphData graphData, GraphContextData contextData)
        {
            BarPlot BarPlotGraph = new BarPlot();

            BarPlotGraph.BarWidth = m_plotData.GetBarWidth();
            if (m_plotData.m_GraphType == GraphType.Exercise)
            {
                BarPlotGraph.ShowInLegend = false;
            }
            else
            {
                BarPlotGraph.ShowInLegend = true;
            }
            BarPlotGraph.Label = graphData.m_graphDataPlotLabel;
            BarPlotGraph.BorderColor = graphData.m_color;
            BarPlotGraph.FillBrush = new RectangleBrushes.Solid(graphData.m_color);
            return BarPlotGraph;

        }






        /// <summary>
        /// This plots the line and the point graph. 
        /// </summary>
        private void _PlotGraphLineAndPoint(AHAGraphData graphData, GraphContextData contextData)
        {
            //Instantiate a LinePlot and PointPlotGraph
            LinePlot LinePlotGraph = null;
            PointPlot PointPlotGraph = null;

            BarPlot BarPlotGraph = null;

            if (this.m_plotData.m_graphRenderingMode == GraphRenderingMode.LineAndPoint)
            {
                LinePlotGraph = _CreateLinePlot(graphData, contextData);
                PointPlotGraph = _CreatePointPlot(graphData, contextData);
            }
            else
            {
                BarPlotGraph = _CreateBarPlot(graphData, contextData);
            }


            //Copy source data to an array 
            contextData.DL_X1X2 = new float[graphData.m_readingList.Count];
            contextData.DL_Y1Y2 = new float[graphData.m_readingList.Count];
            int count = 0;
            foreach (GraphInputReading reading in graphData.m_readingList)
            {
                contextData.DL_X1X2[count] = _getGraphPoint(nPointCount, currentStartDate, currentEndDate, reading.DateOfReading);
                contextData.DL_Y1Y2[count] = (float)reading.ReadingValue;
                count++;
            }


            if (LinePlotGraph != null)
            {
                LinePlotGraph.AbscissaData = contextData.DL_X1X2;
                LinePlotGraph.OrdinateData = contextData.DL_Y1Y2;
                m_plotSurface.Add(LinePlotGraph, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left, 1000);
            }


            if (PointPlotGraph != null)
            {
                PointPlotGraph.AbscissaData = contextData.DL_X1X2;
                PointPlotGraph.OrdinateData = contextData.DL_Y1Y2;
                m_plotSurface.Add(PointPlotGraph, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left, 1000);
            }


            if (BarPlotGraph != null)
            {

                float[] bottom = new float[contextData.DL_Y1Y2.Length];
                for (int i = 0; i < contextData.DL_Y1Y2.Length; i++)
                {
                    bottom[i] = 0.0F;
                }

                BarPlotGraph.AbscissaData = contextData.DL_X1X2;
                BarPlotGraph.OrdinateDataTop = contextData.DL_Y1Y2;
                BarPlotGraph.OrdinateDataBottom = bottom;
                m_plotSurface.Add(BarPlotGraph, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left, 100);
            }
        }

        /// <summary>
        /// Plots the goal Line
        /// </summary>
        private void _PlotGoalLine(AHAGraphData graphData, GraphContextData contextData)
        {
            LinePlot LinePlotGoal = new LinePlot();
            LinePlotGoal.Pen = new Pen(graphData.m_goalData.GoalLineColor, 3.0f);

            //Now Added
            if (m_plotData.m_GraphType == GraphType.Exercise)
            {
                LinePlotGoal.Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            }
            else
            {
                LinePlotGoal.Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            }
            //End Now Added

            if (!String.IsNullOrEmpty(graphData.m_goalGraphPlotLabel))
            {
                LinePlotGoal.ShowInLegend = true;
                LinePlotGoal.Label = graphData.m_goalGraphPlotLabel;

                float fGoalLabelX = 0;
                float fGoalLabelY = 0;
                if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last3Months)
                {
                    fGoalLabelY = contextData.goalStartValue + 8;
                }
                else
                {
                    fGoalLabelY = contextData.goalStartValue + 15;
                }
                if (m_plotData.m_GraphType == GraphType.Weight)
                {
                    if (m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last3Months
                        || m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last6Months
                        || m_plotData.m_DateRange == AHACommon.DataDateReturnType.Last9Months
                        )
                    {
                        fGoalLabelY = contextData.goalStartValue + 8;
                    }
                    fGoalLabelX = (float)(contextData.goalStartDate - currentStartDate).Days;
                }
                //need to change below code
                TextItem goalLabel = new TextItem(new PointD(fGoalLabelX, fGoalLabelY), graphData.m_goalGraphPlotLabel);
                goalLabel.TextColor = ColorTranslator.FromHtml("#666666");
                m_plotSurface.Add(goalLabel);
            }
            else
            {
                LinePlotGoal.ShowInLegend = false;
            }

            contextData.GL_X1X2 = new float[2];
            contextData.GL_Y1Y2 = new float[2];
            contextData.GL_X1X2[0] = (float)(contextData.goalStartDate - currentStartDate).Days;
            contextData.GL_X1X2[1] = (float)(contextData.goalEndDate - currentStartDate).Days;
            contextData.GL_Y1Y2[0] = contextData.goalStartValue;
            contextData.GL_Y1Y2[1] = contextData.goalEndValue;
            LinePlotGoal.AbscissaData = contextData.GL_X1X2;
            LinePlotGoal.OrdinateData = contextData.GL_Y1Y2;
            m_plotSurface.Add(LinePlotGoal);




            //If the goal start and end dates are within the range of the graph,
            //show their end points as points

            PointPlot PointPlotGraph = new PointPlot();
            Marker marker = new Marker(Marker.MarkerType.FilledSquare, 6, graphData.m_goalData.GoalLineColor);
            PointPlotGraph.Marker = marker;
            PointPlotGraph.ShowInLegend = false;

            int pointCount = 0;

            //Is the start date in range?
            if (contextData.goalStartDate == graphData.m_goalData.GoalStartDate)
            {
                pointCount++;
            }

            if (contextData.goalEndDate == graphData.m_goalData.GoalEndDate)
            {
                pointCount++;
            }

            if (pointCount > 0)
            {
                contextData.GP_X1X2 = new float[pointCount];
                contextData.GP_Y1Y2 = new float[pointCount];

                int index = 0;

                if (contextData.goalStartDate == graphData.m_goalData.GoalStartDate)
                {
                    contextData.GP_X1X2[index] = (float)(contextData.goalStartDate - currentStartDate).Days;
                    contextData.GP_Y1Y2[index] = contextData.goalStartValue;
                    index++;
                }

                if (contextData.goalEndDate == graphData.m_goalData.GoalEndDate)
                {
                    contextData.GP_X1X2[index] = (float)(contextData.goalEndDate - currentStartDate).Days;
                    contextData.GP_Y1Y2[index] = contextData.goalEndValue;
                }

                PointPlotGraph.AbscissaData = contextData.GP_X1X2;
                PointPlotGraph.OrdinateData = contextData.GP_Y1Y2;


                m_plotSurface.Add(PointPlotGraph, PlotSurface2D.XAxisPosition.Bottom, PlotSurface2D.YAxisPosition.Left);
            }
        }

        private void _ShadeAndLabelThresholdZonesHelper(ThresholdDirection direction, float thresholdValue, int zindex, Color colorToShade, string aboveLimitText, string belowLimitText)
        {
            LinePlot LinePlotThreshold_1 = new LinePlot();
            LinePlotThreshold_1.Pen = new Pen(Color.DarkGray, 1.0f);
            LinePlotThreshold_1.ShowInLegend = false;

            LinePlot LinePlotThreshold_2 = new LinePlot();
            LinePlotThreshold_2.Pen = new Pen(Color.White, 1.0f);
            LinePlotThreshold_2.ShowInLegend = false;

            //Plot LinePlotThreshold_2 as a horizontal line in yaxis on specified threshold value
            {
                float[] X1X2 = new float[2];
                float[] Y1Y2 = new float[2];
                X1X2[0] = (float)m_plotSurface.XAxis1.WorldMin;
                X1X2[1] = (float)m_plotSurface.XAxis1.WorldMax;
                Y1Y2[0] = _getAdjustedThreshold(thresholdValue);
                Y1Y2[1] = _getAdjustedThreshold(thresholdValue);
                LinePlotThreshold_1.AbscissaData = X1X2;
                LinePlotThreshold_1.OrdinateData = Y1Y2;
            }

            //Plot LinePlotThreshold_2 as a horizontal line in yaxis max or min value as specified by direction
            {
                float[] X1X2 = new float[2];
                float[] Y1Y2 = new float[2];
                X1X2[0] = (float)m_plotSurface.XAxis1.WorldMin;
                X1X2[1] = (float)m_plotSurface.XAxis1.WorldMax;

                if (direction == ThresholdDirection.ShadeHigher)
                {
                    Y1Y2[0] = (float)m_plotSurface.YAxis1.WorldMax;
                    Y1Y2[1] = (float)m_plotSurface.YAxis1.WorldMax;
                }
                else
                {
                    Y1Y2[0] = (float)m_plotSurface.YAxis1.WorldMin;
                    Y1Y2[1] = (float)m_plotSurface.YAxis1.WorldMin;
                }
                LinePlotThreshold_2.AbscissaData = X1X2;
                LinePlotThreshold_2.OrdinateData = Y1Y2;
            }

            //Add these lines to plot surface
            m_plotSurface.Add(LinePlotThreshold_1, -50);
            m_plotSurface.Add(LinePlotThreshold_2, -50);

            bool bShowBelowLimitText = false;
            bool bShowAboveLimitText = false;
            NPlot.PointD aboveLimitPoint = new PointD((float)m_plotSurface.XAxis1.WorldMin + .25f, _getAdjustedThreshold(thresholdValue) + 100); ;
            NPlot.PointD belowLimitPoint = new PointD((float)m_plotSurface.XAxis1.WorldMin + .25f, _getAdjustedThreshold(thresholdValue) - 100); ;

            if (_getAdjustedThreshold(thresholdValue) == (float)m_plotSurface.YAxis1.WorldMax)
            {
                //All readings are in normal section
                bShowBelowLimitText = true;
                belowLimitPoint = new PointD((float)m_plotSurface.XAxis1.WorldMin + .25f, ((float)m_plotSurface.YAxis1.WorldMin + (float)m_plotSurface.YAxis1.WorldMax) / 2);
            }
            else if (_getAdjustedThreshold(thresholdValue) == (float)m_plotSurface.YAxis1.WorldMin)
            {
                //All readings are in abovenormal section
                bShowAboveLimitText = true;
                aboveLimitPoint = new PointD((float)m_plotSurface.XAxis1.WorldMin + .25f, ((float)m_plotSurface.YAxis1.WorldMin + (float)m_plotSurface.YAxis1.WorldMax) / 2);
            }
            else
            {
                //has both normal and abovenormal section
                bShowAboveLimitText = true;
                bShowBelowLimitText = true;
                aboveLimitPoint = new PointD((float)m_plotSurface.XAxis1.WorldMin + .25f, (_getAdjustedThreshold(thresholdValue) + (float)m_plotSurface.YAxis1.WorldMax) / 2);
                belowLimitPoint = new PointD((float)m_plotSurface.XAxis1.WorldMin + .25f, ((float)m_plotSurface.YAxis1.WorldMin + _getAdjustedThreshold(thresholdValue)) / 2);
            }

            //Create a filled region b/w above lines
            NPlot.FilledRegion filledRegion = new FilledRegion(LinePlotThreshold_1, LinePlotThreshold_2);
            filledRegion.RectangleBrush = new RectangleBrushes.Solid(colorToShade);

            m_plotSurface.Add(filledRegion, zindex);

            if (bShowAboveLimitText)
            {
                NPlot.TextItem txtAboveNormal = new TextItem(aboveLimitPoint, aboveLimitText);
                m_plotSurface.Add(txtAboveNormal);
            }

            if (bShowBelowLimitText)
            {
                NPlot.TextItem txtNormal = new TextItem(belowLimitPoint, belowLimitText);
                m_plotSurface.Add(txtNormal);
            }
        }



        /// <summary>
        /// This does the actual drawing of the graph
        /// </summary>
        /// <returns></returns>
        public bool DrawGraph()
        {
            if (m_plotData == null)
                return false;

            if (m_plotData.m_graphsToPlot.Count == 0)
                return false;


            bool bHasDataToPlot = false;

            foreach (AHAGraphData graphData in m_plotData.m_graphsToPlot)
            {
                if (graphData.m_readingList.Count > 0)
                {
                    bHasDataToPlot = true;
                    break;
                }
            }


            if (!bHasDataToPlot)
                return false;

            //Intialize the plot styles
            _InitializeSurface();

            PopulateContextData();

            if (m_plotData.m_GraphType == GraphType.Exercise)
            {
                currentEndDate = m_plotData.m_graphsToPlot[0].m_XAxisLabels.Last();
                currentStartDate = m_plotData.m_graphsToPlot[0].m_XAxisLabels.First();
            }
            else
            {
                //Calculate the start date and end date to plot
                currentEndDate = _CalculateEndDateForGraph();
                currentStartDate = _CalculateStartDateForGraph();
            }
            nPointCount = (currentEndDate - currentStartDate).Days + 1; //number of points to plot, both days inclusive

            int i = 0;
            foreach (AHAGraphData graphData in m_plotData.m_graphsToPlot)
            {
                GraphContextData contextData = m_ContextDataList[i++];

                if (graphData.m_goalData != null)
                {
                    contextData.goalStartDate = graphData.m_goalData.GoalStartDate;
                    contextData.goalEndDate = graphData.m_goalData.GoalEndDate;

                    if (graphData.m_goalData.GoalLineType == GoalLineType.Sloped)
                    {
                        contextData.goalStartValue = (float)graphData.m_goalData.GoalStartValue;
                        contextData.goalEndValue = (float)graphData.m_goalData.GoalEndValue;
                    }
                    else
                    {
                        contextData.goalStartValue = (float)graphData.m_goalData.GoalEndValue;
                        contextData.goalEndValue = (float)graphData.m_goalData.GoalEndValue;
                    }

                    contextData.bDrawGoalLine = true;
                }
                else
                {
                    contextData.bDrawGoalLine = false;
                }

                //If a goal line needs to be plotted, find the points on which it needs to be plotted and recalculate min and max values
                if (contextData.bDrawGoalLine)
                {
                    contextData.bDrawGoalLine = _NormalizeGoalValues(graphData, contextData);
                }
            }


            //Label xaxis based on the date range
            if (m_plotData.m_GraphType == GraphType.Exercise)
            {
                _DoLabelXaxisForExercise();
            }
            else
            {
                _DoLabelXaxis(currentEndDate);
            }

            i = 0;
            foreach (AHAGraphData graphData in m_plotData.m_graphsToPlot)
            {
                GraphContextData contextData = m_ContextDataList[i++];
                //Draw line and points on the graph 
                _PlotGraphLineAndPoint(graphData, contextData);
            }

            //Calculates the max and min values based on the graphs to plot
            AHAGraphData.CalculateMaxMinValues(true, m_plotData.m_graphsToPlot.Where(g => g.m_considerForYaxisRemapping == true).ToList(),
                ref minValue, ref maxValue);



            //Label yaxis based on the values and update min and max values
            //so that n number of labels are exactly plotted on y axis
            _DoLabelYaxis();


            i = 0;
            foreach (AHAGraphData graphData in m_plotData.m_graphsToPlot)
            {
                GraphContextData contextData = m_ContextDataList[i];

                //Draw Goal line
                if (contextData.bDrawGoalLine)
                {
                    _PlotGoalLine(graphData, contextData);
                }

                i++;
            }

            //Adding and substracting so that the first and last label are displayed correctly.
            //If this is not done, first and last lables are not shown
            m_plotSurface.XAxis1.WorldMin = -0.01;
            m_plotSurface.XAxis1.WorldMax = nPointCount - 1 + .01;

            m_plotSurface.YAxis1.WorldMin = minValue - 0.01;
            m_plotSurface.YAxis1.WorldMax = maxValue + 0.01;

            if (m_plotData.m_thresholdRegion_1 != null)
            {
                _ShadeAndLabelThresholdZonesHelper(m_plotData.m_thresholdRegion_1.m_direction, m_plotData.m_thresholdRegion_1.m_thresholdValue, -100, m_plotData.m_thresholdRegion_1.m_shadingColor, m_plotData.m_thresholdRegion_1.m_regionText, "");
            }

            if (m_plotData.m_thresholdRegion_2 != null)
            {
                _ShadeAndLabelThresholdZonesHelper(m_plotData.m_thresholdRegion_2.m_direction, m_plotData.m_thresholdRegion_2.m_thresholdValue, -90, m_plotData.m_thresholdRegion_2.m_shadingColor, m_plotData.m_thresholdRegion_2.m_regionText, "");
            }



            int legendCount = 0;
            for (i = 0; i < m_plotSurface.Drawables.Count; i++)
            {

                NPlot.IPlot plot = m_plotSurface.Drawables[i] as NPlot.IPlot;
                if (plot == null)
                    continue;

                if (plot.ShowInLegend)
                {
                    legendCount++;
                }
            }


            switch (legendCount)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                    m_plotSurface.Legend.NumberItemsHorizontally = -1;
                    m_plotSurface.Legend.NumberItemsVertically = 1;
                    break;
                //case 4:
                //    m_plotSurface.Legend.NumberItemsHorizontally = 2;
                //    m_plotSurface.Legend.NumberItemsVertically = 2;
                //    break;
                case 5:
                case 6:
                case 7:
                case 8:
                    m_plotSurface.Legend.NumberItemsHorizontally = 4;
                    m_plotSurface.Legend.NumberItemsVertically = 2;
                    break;
                case 9:
                case 10:
                case 11:
                case 12:
                    m_plotSurface.Legend.NumberItemsHorizontally = 4;
                    m_plotSurface.Legend.NumberItemsVertically = 3;
                    break;
                case 13:
                case 14:
                case 15:
                case 16:
                    m_plotSurface.Legend.NumberItemsHorizontally = 4;
                    m_plotSurface.Legend.NumberItemsVertically = 4;
                    break;
            }



            //if (legendCount == 6)
            //{
            //    m_plotSurface.Legend.NumberItemsHorizontally = 3;
            //    m_plotSurface.Legend.NumberItemsVertically = 2;
            //}
            //else
            //{
            //    m_plotSurface.Legend.NumberItemsHorizontally = -1;
            //    m_plotSurface.Legend.NumberItemsVertically = 1;
            //}





            return true;
        }

    }
}
