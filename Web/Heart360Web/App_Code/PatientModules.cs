﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Web
{
    public class PatientPortal
    {
        public enum MODULE_ID : int
        {
            MODULE_ID_UNKNOWN = 0,
            MODULE_ID_CONNECTION_CENTER = 1,
            MODULE_ID_PROFILE = 2,
            MODULE_ID_BLOOD_PRESSURE = 3,
            MODULE_ID_WEIGHT = 4,
            MODULE_ID_PHYSICAL_ACTIVITY = 5,
            MODULE_ID_BLOOD_GLUCOSE = 6,
            MODULE_ID_CHOLESTEROL = 7,
            MODULE_ID_MEDICATIONS = 8,
            MODULE_ID_LIFE_CHECK = 9,
            MODULE_ID_COUNT = 10     // for range checking
        } ;

        public static MODULE_ID GetModuleIdFromInt(int _modId)
        {
            MODULE_ID result = MODULE_ID.MODULE_ID_UNKNOWN;

            switch (_modId)
            {
                case (int)MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    result = MODULE_ID.MODULE_ID_BLOOD_GLUCOSE;
                    break;

                case (int)MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    result = MODULE_ID.MODULE_ID_BLOOD_PRESSURE;
                    break;

                case (int)MODULE_ID.MODULE_ID_CHOLESTEROL:
                    result = MODULE_ID.MODULE_ID_CHOLESTEROL;
                    break;

                case (int)MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    result = MODULE_ID.MODULE_ID_CONNECTION_CENTER;
                    break;

                case (int)MODULE_ID.MODULE_ID_LIFE_CHECK:
                    result = MODULE_ID.MODULE_ID_LIFE_CHECK;
                    break;

                case (int)MODULE_ID.MODULE_ID_MEDICATIONS:
                    result = MODULE_ID.MODULE_ID_MEDICATIONS;
                    break;

                case (int)MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    result = MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY;
                    break;

                case (int)MODULE_ID.MODULE_ID_PROFILE:
                    result = MODULE_ID.MODULE_ID_PROFILE;
                    break;

                case (int)MODULE_ID.MODULE_ID_WEIGHT:
                    result = MODULE_ID.MODULE_ID_WEIGHT;
                    break;

                default:
                    break;
            }
            return result;
        }
    }
}