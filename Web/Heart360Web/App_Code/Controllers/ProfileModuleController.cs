﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Web
{
    public class ProfileModuleController : ModuleControllerInterface
    {
        public RiskLevelHelper.RiskLevelResultTypeEnum GetRiskLevel()
        {
            return RiskLevelHelper.RiskLevelResultTypeEnum.None ;
        }

        private bool DoTheyHaveAnAddress
        {       
            get
            {
                bool    hasAddr = false ;

                if( !string.IsNullOrEmpty( CurrentUser.Address.StreetList[0].Value ) )
                {
                    // If there is a dash as the first character, it means they don't have an address (they removed it from Heart360 profile).
                    hasAddr = (CurrentUser.Address.StreetList[0].Value.Trim().StartsWith("-") ) ? false : true ;
                }
                return hasAddr;
            }
        }

        public bool InitializeSummary( Heart360Web.Controls.Patient.ModuleSummary sumView )
        {
            bool retval = false;
            
            try
            {
                int nbPossible = 0;
                int nbCompleted = 0;

                sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NEUTRAL;

                //
                // The profile contains three groups of information: personal, emergency contact, and health history
                //

                this.GetPersonalInfoNumbers(ref nbPossible, ref nbCompleted);
                this.GetEmergenyContactNumbers(ref nbPossible, ref nbCompleted); 
                this.GetHealthHistoryNumbers( ref nbPossible, ref nbCompleted);

                double pcent = ((double)nbCompleted / (double)nbPossible) * 100.0 ;
                sumView.PrimaryText = string.Format("<span class=\"completion-data\">{0} %</span>", GRCBase.StringHelper.GetFormattedDoubleString(pcent, 0, false));
            }
            catch
            {
                sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NONE;
                sumView.PrimaryText = " ";
            }

            return retval;
        }

        public bool InitializeDetail(Heart360Web.Controls.Patient.ModuleDetails detView)
        {
            return false;
        }

        private void GetPersonalInfoNumbers(ref int nbPossible, ref int nbCompleted)
        {
            //
            // Personal Information: taken from AHAWeb\UserCntrls\Patient\Profile\PersonalInfo.ascx.cs
            //

            HVManager.PersonalDataManager.PersonalItem obj = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
            if (obj == null)
            {
                Guid guidPersonal = HVHelper.GetPersonalItemGUID(true);
                if (guidPersonal != Guid.Empty)
                {
                    obj = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
                }
            }
            HVManager.BasicDataManager.BasicItem objBasic = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
            HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem = HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item;

            
            
            nbPossible++;
            if (!String.IsNullOrEmpty(obj.FirstName.Value))
                nbCompleted++;

            nbPossible++;
            if (!String.IsNullOrEmpty(obj.LastName.Value))
                nbCompleted++;

            nbPossible++;
            if (!String.IsNullOrEmpty(obj.Ethnicity.Value))
                nbCompleted++;

            //nbPossible++;
            //if (!String.IsNullOrEmpty(obj.MaritalStatus.Value))
            //    nbCompleted++;

            nbPossible++;
            if(!String.IsNullOrEmpty(CurrentUser.ContactPhone))
                nbCompleted++;

            nbPossible++;
            if (!String.IsNullOrEmpty(CurrentUser.MobilePhone))
                nbCompleted++;

            nbPossible += 4;    // there are 4 possibles dealing with 

            ////if (CurrentUser.Address.StreetList.Count > 0)
            ////    nbCompleted++;

            if (CurrentUser.Address != null)
            {
                //if (!String.IsNullOrEmpty(sAddress))
                //    nbCompleted++;
                if (CurrentUser.Address.StreetList.Count > 0 && DoTheyHaveAnAddress )
                {
                    nbCompleted++;
                }
                if (!String.IsNullOrEmpty(CurrentUser.Address.City.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(CurrentUser.Address.State.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(CurrentUser.Address.Country.Value))
                    nbCompleted++;
            }

            nbPossible += 3;
            if (objBasic != null)
            {
                if (objBasic.GenderOfPerson.Value.HasValue)
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objBasic.PostalCode.Value))
                    nbCompleted++;
                if (objBasic.YearOfBirth.Value > 1890)
                    nbCompleted++;
            }

            /** Don't worry about these values
            nbPossible += 2;
            if (epItem != null)
            {
                if (!String.IsNullOrEmpty(epItem.IncomeLevel.Value))
                    nbCompleted++ ;
                if (epItem.HasHealthInsurance.Value.HasValue)
                    nbCompleted++ ;
            }
            **/
            HVManager.PersonalContactDataManager.ContactItem objContact = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;

            nbPossible++;
            if (objContact != null && objContact.Phone.Value != null)
                nbCompleted++;

        }

        private void GetEmergenyContactNumbers(ref int nbPossible, ref int nbCompleted)
        {
            //
            // Emergency Contact Information: taken from AHAWeb\UserCntrls\Patient\Profile\EmergencyContact.ascx.cs
            //
            HVManager.EmergencyContactDataManager.EmergencyContactItem objEC = HVHelper.HVManagerPatientBase.EmergencyContactDataManager.Item;

            nbPossible += 8;
            if (objEC != null)
            {
                if (!String.IsNullOrEmpty(objEC.FirstName.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objEC.LastName.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objEC.StreetAddress.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objEC.City.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objEC.State.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objEC.Zip.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objEC.Country.Value))
                    nbCompleted++;
                if (!String.IsNullOrEmpty(objEC.Phone.Value))
                    nbCompleted++;
            }
        }

        private void GetHealthHistoryNumbers(ref int nbPossible, ref int nbCompleted)
        {
            //
            // Health History Information: taken from AHAWeb\UserCntrls\Patient\Profile\HealthHistory.ascx.cs
            //
            HVManager.PersonalDataManager.PersonalItem personalItem = null;
            HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem = null;
            HVManager.CardiacDataManager.CardiacItem cardiacItem = null;
            HVManager.HeightDataManager.HeightItem heightItem = null;
            HVManager.WeightDataManager.WeightItem weightItem = null;
            HVManager.BloodPressureDataManager.BPItem bpItem = null;

            try
            {
                personalItem = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
                epItem = HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item;
                cardiacItem = HVHelper.HVManagerPatientBase.CardiacDataManager.Item;
                heightItem = HeightWrapper.GetLatestHeightItem();
                weightItem = MyWeightWrapper.GetLatestWeight();
                bpItem = BloodPressureWrapper.GetLatestBP();
            }
            catch { }

            nbPossible++ ;
            if (heightItem != null)
                nbCompleted++ ;

            //nbPossible++;
            //if (weightItem != null)
            //    nbCompleted++ ;

            nbPossible++;
            if (bpItem != null)
                nbCompleted++ ;

            //nbPossible += 4;
            //if (epItem != null)
            //{
            //    // txtAllergies.Text = epItem.Allergies.Value;

            //    if (epItem.HasTriedToQuitSmoking.Value.HasValue)
            //        nbCompleted++ ;

            //    if (epItem.IsSmokeCigarette.Value.HasValue)
            //        nbCompleted++ ;

            //    if (!String.IsNullOrEmpty(epItem.TravelTimeToWork.Value))
            //        nbCompleted++;

            //    if (!String.IsNullOrEmpty(epItem.AttitudeTowardsHealthCare.Value))
            //        nbCompleted++ ;
            //}

            nbPossible += 7;
            if (cardiacItem != null)
            {
                if (cardiacItem.IsOnHypertensionMedication.Value.HasValue)
                    nbCompleted++ ;
        
                if (cardiacItem.HasRenalFailureBeenDiagnosed.Value.HasValue)
                    nbCompleted++;

                if (cardiacItem.HasDiabetesBeenDiagnosed.Value.HasValue)
                    nbCompleted++;

                if (cardiacItem.HasPersonalHeartDiseaseHistory.Value.HasValue)
                    nbCompleted++;

                if (cardiacItem.IsOnHypertensionDiet.Value.HasValue)
                    nbCompleted++;

                if (cardiacItem.HasFamilyHeartDiseaseHistory.Value.HasValue)
                    nbCompleted++;

                if (cardiacItem.HasFamilyStrokeHistory.Value.HasValue)
                    nbCompleted++;
            }

            nbPossible++;
            if (personalItem != null)
            {
                if (!String.IsNullOrEmpty(personalItem.BloodType.Value))
                    nbCompleted++;
            }

        }
    }
}