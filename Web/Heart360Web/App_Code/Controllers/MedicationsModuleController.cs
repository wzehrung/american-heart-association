﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AHAAPI;

namespace Heart360Web
{
    public class MedicationsModuleController : ModuleControllerInterface
    {
        private string                              mDetailAbbreviation = "M";  // taken from RightRail.ascx
            // App_Code/DataWrappers/UserMedication is an empty class (worthless as is)
        //private UserMedication                      mObjMed = null;

        public RiskLevelHelper.RiskLevelResultTypeEnum GetRiskLevel()
        {
            return RiskLevelHelper.RiskLevelResultTypeEnum.None ;
        }

        public bool InitializeSummary(Heart360Web.Controls.Patient.ModuleSummary sumView)
        {
            bool retval = false;

            if (sumView == null)
                return retval;

            try
            {
                // taken from AHAWeb\UserCtrls\Patient\AllTrackerSummaryCtrl.ascx.cs

                HVManager.AllItemManager                                    mgr = HVHelper.HVManagerPatientBase;
                LinkedList<HVManager.MedicationDataManager.MedicationItem>  ascendingList = mgr.MedicationDataManager.GetDataBetweenDates(AHACommon.DataDateReturnType.AllData);
                int                                                         countOfExistingMeds = 0;

                    // Loop over result set and count existing meds...
                foreach (HVManager.MedicationDataManager.MedicationItem medicationItem in ascendingList)
                {
                    DateTime? dtDiscontinued = null;

                    if (medicationItem.HasDiscontinued
                        && medicationItem.DateDiscontinued.Value.HVApproximateDate != null
                        && medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                        && medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.HasValue)
                    {
                        dtDiscontinued = new DateTime(medicationItem.DateDiscontinued.Value.HVApproximateDate.Year, medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.Value, medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.Value);
                    }

                    if ((!medicationItem.HasDiscontinued
                        || (dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() > DateTime.Now.ToUniversalTime())))
                    {
                        countOfExistingMeds++;
                    }
                }


                sumView.PrimaryText = string.Format("{0}", countOfExistingMeds );

                retval = true;
            }
            catch
            {
                sumView.PrimaryText = " ";
            }

            // Note: there is no risk level for medications
            sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NEUTRAL;
            sumView.SecondaryTextNoColor = ".";    // Correct styling issue where there is no secondary text

            return retval;
        }

            // This is the method required by ModuleControllerInterface, but since our Detail View is custom,
            // we don't use this.
        public bool InitializeDetail(Heart360Web.Controls.Patient.ModuleDetails detView)
        {
            return false;
        }

            // This method initializes our custom detail view.
        public bool InitializeDetail(Heart360Web.Controls.Patient.Medications.MedicationDetails detView)
        {
            bool retval = false;

            if (detView == null)
                return retval;

            try
            {
                // History Section
                detView.AddHistoryUserControl( (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS );

                    // Learn more section, no tip of the day
                List<AHAHelpContent.Content> listLearnMore = Heart360Web.App_Code.VirtualCoach.Trigger.TriggerManager.GetCategoryTipsForDisplay(mDetailAbbreviation);
                detView.SetLearnMoreContents(listLearnMore);


                // Actions I can take section
                List<AHAHelpContent.MLCActionICanTake> listActionsICanTake = AHAHelpContent.MLCActionICanTake.GetActionICanTakeBasedOnTypeAndReading(null, AHAHelpContent.TrackerDataType.Medication, H360Utility.GetCurrentLanguageID());
                detView.SetActionsContents(listActionsICanTake);

                retval = true;
            }
            catch
            {

            }
            return retval;
        }
    }
}