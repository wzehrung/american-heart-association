﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AHAAPI;

namespace Heart360Web
{
    public class BloodPressureModuleController :  ModuleControllerInterface
    {
        private string                                      mDetailAbbreviation = "H";  // taken from RightRail.ascx
        private UserBP                                      mObjBP = null;
        Heart360Web.RiskLevelHelper.RiskLevelResult         mObjResult = null;

        public RiskLevelHelper.RiskLevelResultTypeEnum GetRiskLevel()
        {
            return ( mObjBP != null && mObjBP.LatestValue != null && mObjResult != null ) ? mObjResult.RiskLevelResultType : RiskLevelHelper.RiskLevelResultTypeEnum.None ;
        }

        public bool InitializeSummary( Heart360Web.Controls.Patient.ModuleSummary sumView )
        {
            bool retval = false;

            if (sumView == null)
                return retval;

            try
            {
                // taken from AHAWeb\UserCtrls\Patient\AllTrackerSummaryCtrl.ascx.cs
                // also look at AHAWeb\UserCtrls\Patient\HealthRiskIndicator.ascx.cs
                if (mObjBP == null)
                {
                    mObjBP = new UserBP();
                    mObjBP.HasGoal = false;
                }

                if (mObjBP != null && mObjBP.LatestValue != null)
                {

                    sumView.PrimaryText = string.Format("<span class=\"diastolic-data\">{0}</span><span class=\"reading-bar\"></span><span class=\"systolic-data\">{1}</span>", mObjBP.LatestValue[1], mObjBP.LatestValue[0] );

                    if( mObjResult == null )
                        mObjResult = mObjBP.GetRiskLevelLatestReading();
                    RiskLevelHelper.RiskLevelResultTypeEnum     rlte = mObjResult.RiskLevelResultType;

                    if (rlte == RiskLevelHelper.RiskLevelResultTypeEnum.Warning)
                    {
                        sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_WARNING;
                    }
                    else if (rlte == RiskLevelHelper.RiskLevelResultTypeEnum.NeedsImprovement)
                    {
                        sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT;
                    }
                    else      // assuming the best.... 
                    {
                        sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_EXCELLENT;
                    }

                    retval = true;
                }
                else
                {
                    sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NONE;
                    sumView.PrimaryText = " ";
                }
            }
            catch
            {
                sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NONE;
                sumView.PrimaryText = " ";

                //mSummaryView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_WARNING;
                //mSummaryView.PrimaryText = "210/185";

            }

            return retval;
        }

        public bool InitializeDetail(Heart360Web.Controls.Patient.ModuleDetails detView)
        {
            bool retval = false;

            if (detView == null)
                return retval;

            try
            {
                int? riskLevel = null;

                if (mObjBP == null)
                {
                    mObjBP = new UserBP();
                    mObjBP.HasGoal = false;
                }

                if (mObjResult == null)
                    mObjResult = mObjBP.GetRiskLevelLatestReading();
                if (mObjResult != null)
                    riskLevel = (int)mObjResult.RiskLevelResultType;


                // If the riskLevel is unknown, that means the user hasn't added a reading yet.
                bool hasReading = (riskLevel == null || riskLevel == (int)RiskLevelHelper.RiskLevelResultTypeEnum.None) ? false : true;

                // Learn more section
                if (hasReading)
                {
                    // Learn more section
                    Heart360Web.VirtualCoach.SpeechBubblePeriodicData virtualCoach = Heart360Web.VirtualCoach.BPPageHelper.GetSpeechBubble();
                    detView.SetTipOfTheDay(virtualCoach.TipOfTheDay);
                }
                else
                {
                    detView.SetTipOfTheDay(string.Empty);
                }

                // History Section
                detView.AddHistoryUserControl((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE);

                if (hasReading)
                {
                    List<AHAHelpContent.Content> listLearnMore = Heart360Web.App_Code.VirtualCoach.Trigger.TriggerManager.GetCategoryTipsForDisplay(mDetailAbbreviation);
                    detView.SetLearnMoreContents(listLearnMore);
                }
                else
                {
                    detView.SetLearnMoreContents(H360Utility.GetCurrentLanguageID());
                }

                // Actions I can take section
                List<AHAHelpContent.MLCActionICanTake> listActionsICanTake = AHAHelpContent.MLCActionICanTake.GetActionICanTakeBasedOnTypeAndReading(riskLevel, AHAHelpContent.TrackerDataType.BloodPressure, H360Utility.GetCurrentLanguageID());
                detView.SetActionsContents(listActionsICanTake);                    
                retval = true;
            }
            catch
            {

            }


            return retval;
        }
    }


}