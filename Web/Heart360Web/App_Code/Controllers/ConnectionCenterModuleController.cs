﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Web
{

    public class ConnectionCenterModuleController : ModuleControllerInterface
    {
        public RiskLevelHelper.RiskLevelResultTypeEnum GetRiskLevel()
        {
            return RiskLevelHelper.RiskLevelResultTypeEnum.None ;
        }

        public bool InitializeSummary( Heart360Web.Controls.Patient.ModuleSummary sumView )
        {
            AHAAPI.UserIdentityContext  uic = AHAAPI.SessionInfo.GetUserIdentityContext() ;
            bool                        retval = false ;
            int                         iAnnouncements = 0 ;
            int                         iMessageCount = 0 ;
            int                         iPendingCount = 0 ;

            // Probably best to code this where we take into consideration that GetUserIdentityContext() could fail.
            if (uic != null)
            {
                int patientId = uic.PatientID.Value;

                // Get count of Announcements (resources sent by doctor)
                iAnnouncements = AHAHelpContent.Resource.GetAnnouncementCountForPatient(patientId);

                // Get count of Messages
                iMessageCount = AHAHelpContent.Message.GetNumberOfUnreadMessagesForUser(new AHAHelpContent.UserTypeDetails()
                                {
                                    UserID = patientId,
                                    UserType = AHAHelpContent.UserType.Patient
                                }, AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX);

                // Get count of Pending Invitations 
                List<AHAHelpContent.PatientProviderInvitation> objInvitationList = AHAHelpContent.PatientProviderInvitation.FindPendingInvitationsByAcceptedPatient(
                                                                       patientId,
                                                                        AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Pending);
                iPendingCount = objInvitationList.Count;

                retval = true;
            }

            sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NEUTRAL;
            sumView.PrimaryText = string.Format("<span class='announce'>{0}</span><span class='messages'>{1}</span><span class='pending'>{2}</span>", iAnnouncements.ToString(), iMessageCount.ToString(), iPendingCount.ToString());

            return retval;
        }

        public bool InitializeDetail(Heart360Web.Controls.Patient.ModuleDetails detView)
        {
            return false;
        }
    }

}