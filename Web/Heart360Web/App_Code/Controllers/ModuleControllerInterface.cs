﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Heart360Web.Controls.Patient;
using Heart360Web;

namespace Heart360Web
{
        // this is really Data Access Layer functionality
    public interface ModuleControllerInterface
    {
        bool                                        InitializeSummary( ModuleSummary sumView );
        bool                                        InitializeDetail(ModuleDetails detView );
            // This function can't be called until after InitializeSummary() has been called!
        RiskLevelHelper.RiskLevelResultTypeEnum     GetRiskLevel() ;

    }
}
