﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AHAAPI;

namespace Heart360Web
{
    public class MyLifeCheckModuleController : ModuleControllerInterface
    {
        public RiskLevelHelper.RiskLevelResultTypeEnum GetRiskLevel()
        {
            return RiskLevelHelper.RiskLevelResultTypeEnum.None ;
        }

        public bool InitializeSummary( Heart360Web.Controls.Patient.ModuleSummary sumView )
        {
            bool retval = false;
            try
            {
                double dMLC = 0.0;
                HVManager.FileDataManager.FileItem fileItem = HVHelper.HVManagerPatientBase.FileDataManager.Item;

                if (fileItem != null && fileItem.HealthScore.Value.HasValue)
                {
                    dMLC = Math.Round(fileItem.HealthScore.Value.Value, 1);


                    //bool bShouldShowMLCAlert = RiskLevelHelper.ShouldShowMLCAlert();
                    //if (bShouldShowMLCAlert)
                    //{

                    //}
                    retval = true;
                }

                sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NEUTRAL;
                sumView.PrimaryText = string.Format("{0}", dMLC.ToString());
            }
            catch
            {

            }

            return retval;
        }

        public bool InitializeDetail(Heart360Web.Controls.Patient.ModuleDetails detView)
        {
            bool retval = false;

            if (detView == null)
                return retval;

            return retval;
        }
    }
}