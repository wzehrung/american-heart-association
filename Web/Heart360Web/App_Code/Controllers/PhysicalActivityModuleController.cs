﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AHAAPI;

namespace Heart360Web
{
    public class PhysicalActivityModuleController : ModuleControllerInterface
    {
        private string                              mDetailAbbreviation = "P";  // taken from RightRail.ascx
        private UserExercise                        mObjUserExercise = null;
        Heart360Web.RiskLevelHelper.RiskLevelResult mObjResult = null;

        public RiskLevelHelper.RiskLevelResultTypeEnum GetRiskLevel()
        {
            return ( mObjUserExercise != null && mObjUserExercise.LatestValue != null && mObjResult != null ) ? mObjResult.RiskLevelResultType : RiskLevelHelper.RiskLevelResultTypeEnum.None ;
        }

        public bool InitializeSummary( Heart360Web.Controls.Patient.ModuleSummary sumView )
        {
            bool retval = false;
            try
            {
                // taken from AHAWeb\UserCtrls\Patient\AllTrackerSummaryCtrl.ascx.cs
                // also look at AHAWeb\UserCtrls\Patient\HealthRiskIndicator.ascx.cs
                if (mObjUserExercise == null)
                {
                    mObjUserExercise = new UserExercise();
                    mObjUserExercise.HasGoal = false;
                }

                if (mObjUserExercise != null && mObjUserExercise.LatestValue != null)
                {
                    sumView.PrimaryText = string.Format("<span class=\"fitness-data\">{0}</span>", GRCBase.StringHelper.GetFormattedDoubleString(mObjUserExercise.WeeklyTotal, 2, false)); // WZ 5/19/2014 Removed per AHA ("Min"): , Resources.Common.Text_Minutes_Short

                    // RiskLevelHelper.RiskLevelTypeEnum.Exercise,
                    if (mObjResult == null)
                        mObjResult = mObjUserExercise.GetRiskLevelLatestReading();
                    RiskLevelHelper.RiskLevelResultTypeEnum rlte = mObjResult.RiskLevelResultType;

                    if (rlte == RiskLevelHelper.RiskLevelResultTypeEnum.Warning)
                    {
                        sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_WARNING;
                    }
                    else if (rlte == RiskLevelHelper.RiskLevelResultTypeEnum.NeedsImprovement)
                    {
                        sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT;
                    }
                    else      // assuming the best.... 
                    {
                        sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_EXCELLENT;
                    }
                    retval = true;
                }


            }
            catch
            {
                sumView.SummaryState = Controls.Patient.ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NONE;
                sumView.PrimaryText = " ";
            }

            return retval;
        }

        public bool InitializeDetail(Heart360Web.Controls.Patient.ModuleDetails detView)
        {
            bool retval = false;

            if (detView == null)
                return retval;

            try
            {
                #region chnaged for Learn More clause
                int? riskLevel = null;

                if (mObjUserExercise == null)
                {
                    mObjUserExercise = new UserExercise();
                    mObjUserExercise.HasGoal = false;
                }

                if (mObjResult == null)
                    mObjResult = mObjUserExercise.GetRiskLevelLatestReading();
                if (mObjResult != null)
                    riskLevel = (int)mObjResult.RiskLevelResultType;

                // If the riskLevel is unknown, that means the user hasn't added a reading yet.
                bool hasReading = (riskLevel == null || riskLevel == (int)RiskLevelHelper.RiskLevelResultTypeEnum.None) ? false : true;

                // Learn more section
                if (hasReading)
                {
                    Heart360Web.VirtualCoach.SpeechBubblePeriodicData virtualCoach = Heart360Web.VirtualCoach.ExercisePageHelper.GetSpeechBubble();
                    detView.SetTipOfTheDay(virtualCoach.TipOfTheDay);
                }
                else
                {
                    detView.SetTipOfTheDay(string.Empty);
                }

                // History Section
                detView.AddHistoryUserControl( (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY );

                if (hasReading)
                {
                    List<AHAHelpContent.Content> listLearnMore = Heart360Web.App_Code.VirtualCoach.Trigger.TriggerManager.GetCategoryTipsForDisplay(mDetailAbbreviation);
                    detView.SetLearnMoreContents(listLearnMore);
                }
                else
                {
                    detView.SetLearnMoreContents(H360Utility.GetCurrentLanguageID());
                }

                #endregion 


                // Actions I can take section
                List<AHAHelpContent.MLCActionICanTake> listActionsICanTake = AHAHelpContent.MLCActionICanTake.GetActionICanTakeBasedOnTypeAndReading(riskLevel, AHAHelpContent.TrackerDataType.Exercise, H360Utility.GetCurrentLanguageID());
                detView.SetActionsContents(listActionsICanTake);

                retval = true;
            }
            catch
            {

            }


            return retval;
        }

    }
}