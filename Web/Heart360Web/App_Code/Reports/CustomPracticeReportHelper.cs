﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace Heart360Web
{
    public enum CustomPracticeReportType
    {
        [Description("BP Systolic")]
        Systolic,
        [Description("BP Diastolic")]
        Diastolic,
        [Description("Cholesterol Total")]
        TotalCholesterol,
        [Description("Cholesterol HDL")]
        HDL,
        [Description("Cholesterol LDL")]
        LDL,
        [Description("Blood Glucose")]
        BloodGlucose,
        [Description("Weight")]
        Weight
    }

    /// <summary>
    ///  List<AHAHelpContent.Patient> listReturn1 = CustomPracticeReportHelper.GetPatientsAboveOrBelowThreshold(CustomPracticeReportType.Systolic, 120, true, DateTime.Today.AddDays(-180), DateTime.Now);
    ///  List<AHAHelpContent.Patient> listReturn2 = CustomPracticeReportHelper.GetPatientsAboveOrBelowThresholdXTimes(CustomPracticeReportType.Systolic, 120, true, DateTime.Today.AddDays(-180), DateTime.Now, 1);
    ///  List<AHAHelpContent.Patient> listReturn3 = CustomPracticeReportHelper.GetPatientsForThresholdChanged(CustomPracticeReportType.Systolic, DateTime.Today.AddDays(-180), DateTime.Now, 21);
    /// </summary>
    public class CustomPracticeReportHelper
    {
        public class PatientReport
        {
            public Guid PersonGUID
            {
                get;
                set;
            }

            public Guid RecordGUID
            {
                get;
                set;
            }

            public string MaxValueThatQualified
            {
                get;
                set;
            }
        }

        public enum ReportFunction
        {
            AboveOrBelowThreshold,
            ThresholdChanged,
            AboveOrBelowThresholdXTimes
        }

        public static ListItemCollection GetCustomPracticeReportTypes()
        {
            ListItemCollection objList = new ListItemCollection();
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(CustomPracticeReportType.Systolic), CustomPracticeReportType.Systolic.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(CustomPracticeReportType.Diastolic), CustomPracticeReportType.Diastolic.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(CustomPracticeReportType.TotalCholesterol), CustomPracticeReportType.TotalCholesterol.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(CustomPracticeReportType.HDL), CustomPracticeReportType.HDL.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(CustomPracticeReportType.LDL), CustomPracticeReportType.LDL.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(CustomPracticeReportType.BloodGlucose), CustomPracticeReportType.BloodGlucose.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(CustomPracticeReportType.Weight), CustomPracticeReportType.Weight.ToString()));

            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_CustomPracticeReport_BPSystolic").ToString(), CustomPracticeReportType.Systolic.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_CustomPracticeReport_BPDiastolic").ToString(), CustomPracticeReportType.Diastolic.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_CustomPracticeReport_TotalCholesterol").ToString(), CustomPracticeReportType.TotalCholesterol.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_CustomPracticeReport_HDLCholesterol").ToString(), CustomPracticeReportType.HDL.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_CustomPracticeReport_LDLCholesterol").ToString(), CustomPracticeReportType.LDL.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_CustomPracticeReport_BloodGlucose").ToString(), CustomPracticeReportType.BloodGlucose.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_CustomPracticeReport_Weight").ToString(), CustomPracticeReportType.Weight.ToString()));
            return objList;
        }

        public static List<PatientReport> GetPatientsAboveOrBelowThreshold(CustomPracticeReportType eType, double dThreshold, bool bIsAbove, AHACommon.DataDateReturnType dataDateReturnType)
        {
            List<PatientReport> listReturn = new List<PatientReport>();
            string strMaxValue = string.Empty;

            List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listPatient.ForEach(delegate(AHAHelpContent.Patient objPatient)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                switch (eType)
                {
                    case CustomPracticeReportType.Systolic:
                        LinkedList<HVManager.BloodPressureDataManager.BPItem> listBPS = mgr.BloodPressureDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.BloodPressureDataManager.BPItem> modBPSList = listBPS.Where(item => (bIsAbove && item.Systolic.Value > dThreshold) || (!bIsAbove && item.Systolic.Value < dThreshold));
                        if (modBPSList.Count() > 0)
                        {
                            strMaxValue = modBPSList.Max(i => i.Systolic.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        //foreach (HVManager.BloodPressureDataManager.BPItem item in listBPS)
                        //{
                        //    if (bIsAbove && item.Systolic.Value > dThreshold)
                        //    {
                        //        listReturn.Add(objPatient);
                        //        break;
                        //    }
                        //    else if (!bIsAbove && item.Systolic.Value < dThreshold)
                        //    {
                        //        listReturn.Add(objPatient);
                        //        break;
                        //    }
                        //}
                        break;
                    case CustomPracticeReportType.Diastolic:
                        LinkedList<HVManager.BloodPressureDataManager.BPItem> listBPD = mgr.BloodPressureDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.BloodPressureDataManager.BPItem> modBPDList = listBPD.Where(item => (bIsAbove && item.Diastolic.Value > dThreshold) || (!bIsAbove && item.Diastolic.Value < dThreshold));
                        if (modBPDList.Count() > 0)
                        {
                            strMaxValue = modBPDList.Max(i => i.Diastolic.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.TotalCholesterol:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listTC = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> modTCList = listTC.Where(item => (bIsAbove && item.TotalCholesterol.Value.HasValue && item.TotalCholesterol.Value.Value > dThreshold) || (!bIsAbove && item.TotalCholesterol.Value.HasValue && item.TotalCholesterol.Value.Value < dThreshold));
                        if (modTCList.Count() > 0)
                        {
                            strMaxValue = modTCList.Max(i => i.TotalCholesterol.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.HDL:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listHDL = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> modHDLList = listHDL.Where(item => (bIsAbove && item.HDL.Value.HasValue && item.HDL.Value.Value > dThreshold) || (!bIsAbove && item.HDL.Value.HasValue && item.HDL.Value.Value < dThreshold));
                        if (modHDLList.Count() > 0)
                        {
                            strMaxValue = modHDLList.Max(i => i.HDL.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.LDL:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listLDL = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> modLDLList = listLDL.Where(item => (bIsAbove && item.LDL.Value.HasValue && item.LDL.Value.Value > dThreshold) || (!bIsAbove && item.LDL.Value.HasValue && item.LDL.Value.Value < dThreshold));
                        if (modLDLList.Count() > 0)
                        {
                            strMaxValue = modLDLList.Max(i => i.LDL.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.BloodGlucose:
                        LinkedList<HVManager.BloodGlucoseDataManager.BGItem> listBG = mgr.BloodGlucoseDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> modBGList = listBG.Where(item => (bIsAbove && item.Value.Value > dThreshold) || (!bIsAbove && item.Value.Value < dThreshold));
                        if (modBGList.Count() > 0)
                        {
                            strMaxValue = modBGList.Max(i => i.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.Weight:
                        LinkedList<HVManager.WeightDataManager.WeightItem> listWeight = mgr.WeightDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.WeightDataManager.WeightItem> modWeightList = listWeight.Where(item => (bIsAbove && item.CommonWeight.Value.ToPounds() > dThreshold) || (!bIsAbove && item.CommonWeight.Value.ToPounds() < dThreshold));
                        if (modWeightList.Count() > 0)
                        {
                            strMaxValue = modWeightList.Max(i => i.CommonWeight.Value.ToPounds()).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                }
            });

            return listReturn;
        }

        public static List<PatientReport> GetPatientsForThresholdChanged(CustomPracticeReportType eType, AHACommon.DataDateReturnType dataDateReturnType, double dChangedAmount)
        {
            List<PatientReport> listReturn = new List<PatientReport>();

            List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listPatient.ForEach(delegate(AHAHelpContent.Patient objPatient)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                switch (eType)
                {
                    case CustomPracticeReportType.Systolic:
                        LinkedList<HVManager.BloodPressureDataManager.BPItem> listBPS = mgr.BloodPressureDataManager.GetDataBetweenDates(dataDateReturnType);
                        int iBPSMin = 0;
                        int iBPSMax = 0;

                        if (listBPS.Count > 0)
                        {
                            iBPSMin = listBPS.Min(item => item.Systolic.Value);
                            iBPSMax = listBPS.Max(item => item.Systolic.Value);
                        }

                        //foreach (HVManager.BloodPressureDataManager.BPItem item in listBPS)
                        //{
                        //    if (item.Systolic.Value < iBPSMin)
                        //    {
                        //        iBPSMin = item.Systolic.Value;
                        //    }

                        //    if (item.Systolic.Value > iBPSMax)
                        //    {
                        //        iBPSMax = item.Systolic.Value;
                        //    }
                        //}

                        if (iBPSMax - iBPSMin >= dChangedAmount)
                        {
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = iBPSMax.ToString() });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.Diastolic:
                        LinkedList<HVManager.BloodPressureDataManager.BPItem> listBPD = mgr.BloodPressureDataManager.GetDataBetweenDates(dataDateReturnType);
                        int iBPDMin = 0;
                        int iBPDMax = 0;

                        if (listBPD.Count > 0)
                        {
                            iBPDMin = listBPD.Min(item => item.Diastolic.Value);
                            iBPDMax = listBPD.Max(item => item.Diastolic.Value);
                        }

                        if (iBPDMax - iBPDMin >= dChangedAmount)
                        {
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = iBPDMax.ToString() });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.TotalCholesterol:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listTC = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        int iTCMin = 0;
                        int iTCMax = 0;

                        if (listTC.Count > 0 && listTC.Where(item => item.TotalCholesterol.Value.HasValue).Count() > 0)
                        {
                            iTCMin = listTC.Min(i => i.TotalCholesterol.Value.Value);
                            iTCMax = listTC.Max(i => i.TotalCholesterol.Value.Value);
                        }

                        if (iTCMax - iTCMin >= dChangedAmount)
                        {
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = iTCMax.ToString() });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.HDL:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listHDL = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        int iHDLMin = 0;
                        int iHDLMax = 0;

                        if (listHDL.Count > 0 && listHDL.Where(item => item.HDL.Value.HasValue).Count() > 0)
                        {
                            iHDLMin = listHDL.Min(i => i.HDL.Value.Value);
                            iHDLMax = listHDL.Max(i => i.HDL.Value.Value);
                        }

                        if (iHDLMax - iHDLMin >= dChangedAmount)
                        {
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = iHDLMax.ToString() });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.LDL:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listLDL = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);

                        int iLDLMin = 0;
                        int iLDLMax = 0;

                        if (listLDL.Count > 0 && listLDL.Where(item => item.LDL.Value.HasValue).Count() > 0)
                        {
                            iLDLMin = listLDL.Min(i => i.LDL.Value.Value);
                            iLDLMax = listLDL.Max(i => i.LDL.Value.Value);
                        }

                        if (iLDLMax - iLDLMin >= dChangedAmount)
                        {
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = iLDLMax.ToString() });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.BloodGlucose:
                        LinkedList<HVManager.BloodGlucoseDataManager.BGItem> listBG = mgr.BloodGlucoseDataManager.GetDataBetweenDates(dataDateReturnType);
                        double iBGMin = 0;
                        double iBGMax = 0;

                        if (listBG.Count > 0)
                        {
                            iBGMin = listBG.Min(item => item.Value.Value);
                            iBGMax = listBG.Max(item => item.Value.Value);
                        }

                        if (iBGMax - iBGMin >= dChangedAmount)
                        {
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = iBGMax.ToString() });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.Weight:
                        LinkedList<HVManager.WeightDataManager.WeightItem> listWeight = mgr.WeightDataManager.GetDataBetweenDates(dataDateReturnType);
                        double iWMin = 0;
                        double iWMax = 0;

                        if (listWeight.Count > 0)
                        {
                            iWMin = listWeight.Min(item => item.CommonWeight.Value.ToPounds());
                            iWMax = listWeight.Max(item => item.CommonWeight.Value.ToPounds());
                        }

                        if (iWMax - iWMin >= dChangedAmount)
                        {
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = iWMax.ToString() });
                            break;
                        }
                        break;
                }
            });

            return listReturn;
        }

        public static List<PatientReport> GetPatientsAboveOrBelowThresholdXTimes(CustomPracticeReportType eType, double dThreshold, bool bIsAbove, AHACommon.DataDateReturnType dataDateReturnType, int iTimes)
        {
            List<PatientReport> listReturn = new List<PatientReport>();

            string strMaxValue = string.Empty;

            List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listPatient.ForEach(delegate(AHAHelpContent.Patient objPatient)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                switch (eType)
                {
                    case CustomPracticeReportType.Systolic:
                        LinkedList<HVManager.BloodPressureDataManager.BPItem> listBPS = mgr.BloodPressureDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.BloodPressureDataManager.BPItem> modBPSList = listBPS.Where(item => (bIsAbove && item.Systolic.Value > dThreshold) || (!bIsAbove && item.Systolic.Value < dThreshold));

                        int iBPSTimes = modBPSList.Count();

                        if (iBPSTimes >= iTimes)
                        {
                            strMaxValue = modBPSList.Max(i => i.Systolic.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.Diastolic:
                        LinkedList<HVManager.BloodPressureDataManager.BPItem> listBPD = mgr.BloodPressureDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.BloodPressureDataManager.BPItem> modBPDList = listBPD.Where(item => (bIsAbove && item.Diastolic.Value > dThreshold) || (!bIsAbove && item.Diastolic.Value < dThreshold));

                        int iBPDTimes = modBPDList.Count();

                        if (iBPDTimes >= iTimes)
                        {
                            strMaxValue = modBPDList.Max(i => i.Diastolic.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.TotalCholesterol:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listTC = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> modTCList = listTC.Where(item => (bIsAbove && item.TotalCholesterol.Value.HasValue && item.TotalCholesterol.Value.Value > dThreshold) || (!bIsAbove && item.TotalCholesterol.Value.HasValue && item.TotalCholesterol.Value.Value < dThreshold));

                        int iTCTimes = modTCList.Count();

                        if (iTCTimes >= iTimes)
                        {
                            strMaxValue = modTCList.Max(i => i.TotalCholesterol.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.HDL:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listHDL = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> modHDLList = listHDL.Where(item => (bIsAbove && item.HDL.Value.HasValue && item.HDL.Value.Value > dThreshold) || (!bIsAbove && item.HDL.Value.HasValue && item.HDL.Value.Value < dThreshold));

                        int iHDLTimes = modHDLList.Count();

                        if (iHDLTimes >= iTimes)
                        {
                            strMaxValue = modHDLList.Max(i => i.HDL.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.LDL:
                        LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listLDL = mgr.CholesterolDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.CholesterolDataManager.CholesterolItem> modLDLList = listLDL.Where(item => (bIsAbove && item.LDL.Value.HasValue && item.LDL.Value.Value > dThreshold) || (!bIsAbove && item.LDL.Value.HasValue && item.LDL.Value.Value < dThreshold));

                        int iLDLTimes = modLDLList.Count();

                        if (iLDLTimes >= iTimes)
                        {
                            strMaxValue = modLDLList.Max(i => i.HDL.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.BloodGlucose:
                        LinkedList<HVManager.BloodGlucoseDataManager.BGItem> listBG = mgr.BloodGlucoseDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> modBGList = listBG.Where(item => (bIsAbove && item.Value.Value > dThreshold) || (!bIsAbove && item.Value.Value < dThreshold));

                        int iBGTimes = modBGList.Count();

                        if (iBGTimes >= iTimes)
                        {
                            strMaxValue = modBGList.Max(i => i.Value.Value).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                    case CustomPracticeReportType.Weight:
                        LinkedList<HVManager.WeightDataManager.WeightItem> listWeight = mgr.WeightDataManager.GetDataBetweenDates(dataDateReturnType);
                        IEnumerable<HVManager.WeightDataManager.WeightItem> modWeightList = listWeight.Where(item => (bIsAbove && item.CommonWeight.Value.ToPounds() > dThreshold) || (!bIsAbove && item.CommonWeight.Value.ToPounds() < dThreshold));

                        int iWTimes = modWeightList.Count();

                        if (iWTimes >= iTimes)
                        {
                            strMaxValue = modWeightList.Max(i => i.CommonWeight.Value.ToPounds()).ToString();
                            listReturn.Add(new PatientReport { PersonGUID = objPatient.OfflinePersonGUID.Value, RecordGUID = objPatient.OfflineHealthRecordGUID.Value, MaxValueThatQualified = strMaxValue });
                            break;
                        }
                        break;
                }
            });

            return listReturn;
        }
    }
}
