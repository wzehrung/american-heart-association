﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HVManager;

namespace Heart360Web
{
    public class UserBP : UserHealthDataDetails
    {
        public UserBP()
        {
            //HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataForLast2Year();
            TrackerType = AHAAPI.TrackerType.BloodPressure;
            TrackerTypeString = GRCBase.MiscUtility.GetComponentModelPropertyDescription(AHAAPI.TrackerType.BloodPressure);
            IEnumerable<BloodPressureDataManager.BPItem> objItemList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetLatestNItems(2).Reverse();
            BPGoalDataManager.BPGoalItem objGoalItem = HVHelper.HVManagerPatientBase.BPGoalDataManager.Item;
            BloodPressureDataManager.BPItem objItemLatest = null;
            BloodPressureDataManager.BPItem objItemSecond = null;

            if (objItemList.Count() > 0)
            {
                objItemLatest = objItemList.ElementAt(0);
                LatestValue = new object[] { objItemLatest.Systolic.Value, objItemLatest.Diastolic.Value };
            }

            if (objItemList.Count() > 1)
            {
                objItemSecond = objItemList.ElementAt(1);
            }

            if (objGoalItem != null && objGoalItem.TargetSystolic != null && objGoalItem.TargetDiastolic != null)
            {
                HasGoal = true;
                LatestGoal = new object[] { objGoalItem.TargetSystolic.Value, objGoalItem.TargetDiastolic.Value };
            }

            if (objItemLatest != null && objItemSecond != null)
            {
                if (objItemLatest.Systolic.Value > objItemSecond.Systolic.Value
                    && objItemLatest.Diastolic.Value > objItemSecond.Diastolic.Value)
                {
                    LastReadingChange = LastReadingChange.Increased;
                }
                else if (objItemLatest.Systolic.Value == objItemSecond.Systolic.Value
                    && objItemLatest.Diastolic.Value == objItemSecond.Diastolic.Value)
                {
                    LastReadingChange = LastReadingChange.NotChanged;
                }
                else
                {
                    LastReadingChange = LastReadingChange.Decreased;
                }
            }
            else
            {
                LastReadingChange = LastReadingChange.NotChanged;
            }

            HasThreeBoxes = true;
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading()
        {
            if (LatestValue != null)
            {
                RiskLevelHelper.RiskLevelBloodPressure objRiskLevelBloodPressure = new RiskLevelHelper.RiskLevelBloodPressure(Convert.ToInt32(LatestValue[0]), Convert.ToInt32(LatestValue[1]));
                return objRiskLevelBloodPressure.GetRiskLevelResult();
            }
            return null;            
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal()
        {
            if (LatestGoal != null)
            {
                RiskLevelHelper.RiskLevelBloodPressure objRiskLevelBloodPressureGoal = new RiskLevelHelper.RiskLevelBloodPressure(Convert.ToInt32(LatestGoal[0]), Convert.ToInt32(LatestGoal[1]));
                return objRiskLevelBloodPressureGoal.GetRiskLevelResult();
            }
            return null;  
        }
    }
}
