﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using AHAAPI;
using System.Collections.Generic;

namespace Heart360Web
{
    /// <summary>
    /// This class manages all access to Cholestrol data from the UI
    /// </summary>
    public class CholestrolWrapper
    {
        /// <summary>
        /// Gets the latest item
        /// </summary>        /// 
        /// <returns>HVManager.CholesterolDataManager.CholesterolItem</returns>
        public static HVManager.CholesterolDataManager.CholesterolItem GetLatestCholesterol()
        {
            LinkedList<HVManager.CholesterolDataManager.CholesterolItem> objList = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetLatestNItems(1);

            if (objList.Count > 0)
                return objList.First.Value;

            return null;
        }
    }
}
