﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using AHAAPI;
using System.Collections.Generic;

namespace Heart360Web
{
    /// <summary>
    /// This class manages all access to Blood Pressure data from the UI
    /// </summary>
    public class BloodPressureWrapper
    {
        /// <summary>
        /// Gets the latest item
        /// </summary>        /// 
        /// <returns>HVManager.BloodPressureDataManager.BPItem</returns>
        public static HVManager.BloodPressureDataManager.BPItem GetLatestBP()
        {
            LinkedList<HVManager.BloodPressureDataManager.BPItem> objList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetLatestNItems(1);

            if (objList.Count > 0)
                return objList.First.Value;

            return null;
        }
    }
}
