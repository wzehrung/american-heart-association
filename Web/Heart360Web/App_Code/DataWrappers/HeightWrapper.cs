﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using AHAAPI;
using System.Collections.Generic;

namespace Heart360Web
{
    public class HeightWrapper
    {
        /// <summary>
        /// Gets the latest item
        /// </summary>        /// 
        /// <returns>HVManager.HeightDataManager.HeightItem</returns>
        public static HVManager.HeightDataManager.HeightItem GetLatestHeightItem()
        {
            LinkedList<HVManager.HeightDataManager.HeightItem> objList = HVHelper.HVManagerPatientBase.HeightDataManager.GetLatestNItems(1);
            if (objList != null && objList.Count > 0)
            {
                return objList.First.Value;
            }

            return null;
        }

        public static bool IsUserHeightDefined()
        {
            bool retstat = false;
            try
            {
                LinkedList<HVManager.HeightDataManager.HeightItem> objList = HVHelper.HVManagerPatientBase.HeightDataManager.GetLatestNItems(1);
                if (objList != null && objList.Count > 0)
                    retstat = true;
            }
            catch
            {

            }
            return retstat ;
        }

        public static bool SaveHeightInMeters(double _meters)
        {
            bool retstat = false;

            try
            {
                    //Update only if user has consented
                if (AHAAPI.SessionInfo.GetUserIdentityContext() != null && AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
                {
                    HVManager.HeightDataManager.HeightItem objItem = new HVManager.HeightDataManager.HeightItem();
                    objItem.HeightInMeters.Value = _meters;
                    objItem.When.Value = DateTime.Now;
                    HVManager.HeightDataManager.HeightItem objNewItem = HVHelper.HVManagerPatientBase.HeightDataManager.CreateItem(objItem);

                    //DB Update
                    //TODO:'comments' temporarily given as null and 'date measured' to DateTime.Now - to be discussed with Arun
                    
                    AHAHelpContent.HeightDetails.CreateOrChangeHeight(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID, objNewItem.ThingKey.Id, objNewItem.ThingKey.VersionStamp, _meters, null, objNewItem.When.Value);
                    retstat = true;
                }

            }
            catch { }

            return retstat;
        }
    }
}
