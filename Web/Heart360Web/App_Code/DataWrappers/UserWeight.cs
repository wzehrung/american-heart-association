﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HVManager;

namespace Heart360Web
{
    public class UserWeight : UserHealthDataDetails
    {
        double? dBMI;
        double? dBMIGoal;

        public UserWeight()
        {
            //HVHelper.HVManagerPatientBase.WeightDataManager.GetDataForLast2Year();
            TrackerType = AHAAPI.TrackerType.Weight;
            TrackerTypeString = GRCBase.MiscUtility.GetComponentModelPropertyDescription(AHAAPI.TrackerType.Weight);
            IEnumerable<WeightDataManager.WeightItem> objItemList = HVHelper.HVManagerPatientBase.WeightDataManager.GetLatestNItems(2).Reverse();
            WeightGoalDataManager.WeightGoalItem objGoalItem = HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item;
            WeightDataManager.WeightItem objItemLatest = null;
            WeightDataManager.WeightItem objItemSecond = null;

            if (objItemList.Count() > 0)
            {
                objItemLatest = objItemList.ElementAt(0);
                LatestValue = new object[] { objItemLatest.CommonWeight.Value.ToPounds() };
            }

            if (objItemList.Count() > 1)
            {
                objItemSecond = objItemList.ElementAt(1);
            }

            if (objItemLatest != null && objItemSecond != null)
            {
                if (objItemLatest.CommonWeight.Value.ToPounds() > objItemSecond.CommonWeight.Value.ToPounds())
                {
                    LastReadingChange = LastReadingChange.Increased;
                }
                else if (objItemLatest.CommonWeight.Value.ToPounds() == objItemSecond.CommonWeight.Value.ToPounds())
                {
                    LastReadingChange = LastReadingChange.NotChanged;
                }
                else
                {
                    LastReadingChange = LastReadingChange.Decreased;
                }
            }
            else
            {
                LastReadingChange = LastReadingChange.NotChanged;
            }

            if (objGoalItem != null && objGoalItem.TargetValue != null)
            {
                HasGoal = true;
                LatestGoal = new object[] { objGoalItem.TargetValue.Value };
            }

            if (objItemLatest != null)
            {
                dBMI = UserSummary.GetBMIForCurrentRecordForWeight(PatientGuids.CurrentPatientGuids, objItemLatest.CommonWeight.Value.ToPounds());
            }

            if (objGoalItem != null && objGoalItem.TargetValue != null)
            {
                dBMIGoal = UserSummary.GetBMIForCurrentRecordForWeight(PatientGuids.CurrentPatientGuids, objGoalItem.TargetValue.Value);
            }

            HasThreeBoxes = true;
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading()
        {
            if (dBMI.HasValue)
            {
                RiskLevelHelper.RiskLevelBMI objRiskLevelItem = new RiskLevelHelper.RiskLevelBMI(dBMI.Value);
                return objRiskLevelItem.GetRiskLevelResult();
            }
            return null;            
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal()
        {
            if (dBMIGoal.HasValue)
            {
                RiskLevelHelper.RiskLevelBMI objRiskLevelGoal = new RiskLevelHelper.RiskLevelBMI(dBMIGoal.Value);
                return objRiskLevelGoal.GetRiskLevelResult();
            }
            return null;  
        }
    }
}
