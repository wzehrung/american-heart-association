﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using AHAAPI;
using System.Collections.Generic;

namespace Heart360Web
{
    public class MyWeightWrapper
    {
        /// <summary>
        /// Gets the latest item
        /// </summary>        /// 
        /// <returns>HVManager.WeightDataManager.WeightItem</returns>
        public static HVManager.WeightDataManager.WeightItem GetLatestWeight()
        {
            LinkedList<HVManager.WeightDataManager.WeightItem> objList = HVHelper.HVManagerPatientBase.WeightDataManager.GetLatestNItems(1);

            if (objList.Count > 0)
                return objList.First.Value;

            return null;
        }
        
    }
}
