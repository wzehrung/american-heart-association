﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using AHAAPI;

namespace Heart360Web
{
    public enum LastReadingChange
    {
        [Description("Increased")]
        Increased,
        [Description("Decreased")]
        Decreased,
        [Description("Not Changed")]
        NotChanged
    }

    public abstract class UserHealthDataDetails
    {
        public virtual bool HideBadge
        {
            get;
            set;
        }
        public virtual bool HasGoal
        {
            get;
            set;
        }
        public virtual bool HasThreeBoxes
        {
            get;
            set;
        }
        public virtual object[] LatestValue
        {
            get;
            set;
        }
        public virtual object[] LatestGoal
        {
            get;
            set;
        }
        public virtual TrackerType TrackerType
        {
            get;
            set;
        }
        public virtual string TrackerTypeString
        {
            get;
            set;
        }
        public virtual LastReadingChange LastReadingChange
        {
            get;
            set;
        }

        public string LastReadingChangeString
        {
            get
            {
                return GRCBase.MiscUtility.GetComponentModelPropertyDescription(LastReadingChange);
            }
        }
        public abstract Heart360Web.RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading();
        public abstract Heart360Web.RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal();
    }
}
