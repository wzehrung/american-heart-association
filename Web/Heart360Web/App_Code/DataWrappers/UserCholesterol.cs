﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HVManager;

namespace Heart360Web
{
    public class UserCholesterol : UserHealthDataDetails
    {
        public UserCholesterol()
        {
            //HVHelper.HVManagerPatientBase.CholesterolDataManager.GetDataForLast2Year();
            TrackerType = AHAAPI.TrackerType.Cholesterol;
            TrackerTypeString = GRCBase.MiscUtility.GetComponentModelPropertyDescription(AHAAPI.TrackerType.Cholesterol);
            IEnumerable<CholesterolDataManager.CholesterolItem> objItemList = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetLatestNItems(2).Reverse();
            CholesterolGoalDataManager.CholesterolGoalItem objGoalItem = HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item;
            CholesterolDataManager.CholesterolItem objItemLatest = null;
            CholesterolDataManager.CholesterolItem objItemSecond = null;

            if (objItemList.Count() > 0)
            {
                objItemLatest = objItemList.ElementAt(0);
                LatestValue = new object[] { objItemLatest.TotalCholesterol.Value, objItemLatest.HDL.Value, objItemLatest.LDL.Value, objItemLatest.Triglycerides.Value };
            }

            if (objItemList.Count() > 1)
            {
                objItemSecond = objItemList.ElementAt(1);
            }

            if (objGoalItem != null)
            {
                HasGoal = true;
                LatestGoal = new object[] { objGoalItem.TargetTotalCholesterol.Value, objGoalItem.TargetHDL.Value, objGoalItem.TargetLDL.Value, objGoalItem.TargetTriglyceride.Value };
            }

            if (objItemLatest != null && objItemSecond != null)
            {
                if (objItemLatest.TotalCholesterol.Value.HasValue
                    && objItemLatest.TotalCholesterol.Value > objItemSecond.TotalCholesterol.Value
                    && objItemLatest.HDL.Value.HasValue
                    && objItemLatest.HDL.Value > objItemSecond.HDL.Value
                    && objItemLatest.LDL.Value.HasValue
                    && objItemLatest.LDL.Value > objItemSecond.LDL.Value
                    && objItemLatest.Triglycerides.Value.HasValue
                    && objItemLatest.Triglycerides.Value > objItemSecond.Triglycerides.Value
                    )
                {
                    LastReadingChange = LastReadingChange.Increased;
                }
                else if (
                    objItemLatest.TotalCholesterol.Value.HasValue
                    && objItemLatest.TotalCholesterol.Value == objItemSecond.TotalCholesterol.Value
                    && objItemLatest.HDL.Value.HasValue
                    && objItemLatest.HDL.Value == objItemSecond.HDL.Value
                    && objItemLatest.LDL.Value.HasValue
                    && objItemLatest.LDL.Value == objItemSecond.LDL.Value
                    && objItemLatest.Triglycerides.Value.HasValue
                    && objItemLatest.Triglycerides.Value == objItemSecond.Triglycerides.Value
                    )
                {
                    LastReadingChange = LastReadingChange.NotChanged;
                }
                else
                {
                    LastReadingChange = LastReadingChange.Decreased;
                }
            }
            else
            {
                LastReadingChange = LastReadingChange.NotChanged;
            }

            HasThreeBoxes = false;
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading()
        {
            if (LatestValue != null && LatestValue[0] != null)
            {
                RiskLevelHelper.RiskLevelCholesterol objRiskLevelItem = new RiskLevelHelper.RiskLevelCholesterol(
                    Convert.ToInt32(LatestValue[0]), 
                    Convert.ToInt32(LatestValue[1]),
                    Convert.ToInt32(LatestValue[2]),
                    Convert.ToInt32(LatestValue[3]),
                    true);
                return objRiskLevelItem.GetRiskLevelResult();
            }
            return null;  
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal()
        {
            if (LatestGoal != null)
            {
                RiskLevelHelper.RiskLevelCholesterol objRiskLevelGoal = new RiskLevelHelper.RiskLevelCholesterol(
                   Convert.ToInt32(LatestGoal[0]),
                    Convert.ToInt32(LatestGoal[1]),
                    Convert.ToInt32(LatestGoal[2]),
                    Convert.ToInt32(LatestGoal[3]),
                    true);
                return objRiskLevelGoal.GetRiskLevelResult();
            }
            return null;  
        }
    }
}
