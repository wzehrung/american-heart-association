﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text.pdf;

namespace Heart360Web
{
    /// <summary>
    /// This class manages all access to Blood Glucose data from the UI
    /// </summary>
    public class BloodGlucoseWrapper
    {
        /// <summary>
        /// Gets the latest item
        /// </summary>        /// 
        /// <returns>HVManager.BloodGlucoseDataManager.BGItem</returns>
        public static HVManager.BloodGlucoseDataManager.BGItem GetLatestBloodGlucose()
        {
            LinkedList<HVManager.BloodGlucoseDataManager.BGItem> objList = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetLatestNItems(1);

            if (objList.Count > 0)
                return objList.First.Value;

            return null;
        }

        public static HVManager.HbA1cDataManager.HbA1cItem GetLatestHbA1c()
        {
            LinkedList<HVManager.HbA1cDataManager.HbA1cItem> objList = HVHelper.HVManagerPatientBase.HbA1cDataManager.GetLatestNItems(1);

            //string strValue = objList.First.Value;

            if (objList.Count > 0)
                return objList.First.Value;

            return null;
        }

        public static string GetAverageBloodGlucoseForPeriod(bool bIsProvider, AHACommon.DataDateReturnType dataDateReturnType)
        {
            StringBuilder sb = new StringBuilder();
            double dOverallAverage = 0;
            double dFastingAverage = 0;
            double dPPAverage = 0;
            int iFastingCount = 0;
            int iPPCount = 0;

            Guid gPersonGUID;
            Guid gRecordGUID;

            if (bIsProvider)
            {
                AHAAPI.Provider.ProviderBase page = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;
                gPersonGUID = page.PersonID.Value;
                gRecordGUID = page.RecordID.Value;
            }
            else
            {
                gPersonGUID = PatientGuids.CurrentPatientGuids.PersonID;
                gRecordGUID = PatientGuids.CurrentPatientGuids.RecordID;
            }

            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);

            LinkedList<HVManager.BloodGlucoseDataManager.BGItem> list = mgr.BloodGlucoseDataManager.GetDataBetweenDates(dataDateReturnType);

            IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> listNew = list.Where(i => i.EffectiveDate >= DateTime.Today.AddDays(-30));

            if (listNew.Count() > 0)
            {
                dOverallAverage = listNew.Average(item => item.Value.Value);
            }

            IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> listFasting = listNew.Where(item => !string.IsNullOrEmpty(item.ReadingType.Value) && (item.ReadingType.Value.ToLower() == "fasting" || item.ReadingTypeCodedValue.ToLower() == "fasting"));
            IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> listPP = listNew.Where(item => !string.IsNullOrEmpty(item.ReadingType.Value) && (item.ReadingType.Value.ToLower() == "postprandial" || item.ReadingType.Value.ToLower() == "after meal" || item.ReadingTypeCodedValue.ToLower() == "non-fasting"));

            iFastingCount = listFasting.Count();
            iPPCount = listPP.Count();

            if (listFasting.Count() > 0)
            {
                dFastingAverage = listFasting.Average(item => item.Value.Value);
            }

            if (listPP.Count() > 0)
            {
                dPPAverage = listPP.Average(item => item.Value.Value);
            }

            if (dOverallAverage > 0)
            {
                sb.AppendFormat(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BG_OverallAverage").ToString(), (int)dOverallAverage);
            }

            if (iFastingCount > 0)
            {
                sb.Append("<br/>");
                sb.AppendFormat(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BG_FastingAverage").ToString(), iFastingCount, iFastingCount == 1 ? string.Empty : "s", (int)dFastingAverage);
            }

            if (iPPCount > 0)
            {
                sb.Append("<br/>");
                sb.AppendFormat(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BG_AfterMealAverage").ToString(), iPPCount, iPPCount == 1 ? string.Empty : "s", (int)dPPAverage);
            }

            return sb.ToString();
        }

        public static PdfPTable GetAverageBloodGlucoseForPeriodForPDF(PatientGuids objGuids, AHACommon.DataDateReturnType dataDateReturnType)
        {
            double dOverallAverage = 0;
            double dFastingAverage = 0;
            double dPPAverage = 0;
            int iFastingCount = 0;
            int iPPCount = 0;

            if (objGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                LinkedList<HVManager.BloodGlucoseDataManager.BGItem> list = mgr.BloodGlucoseDataManager.GetDataBetweenDates(dataDateReturnType);

                IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> listNew = list.Where(i => i.EffectiveDate >= DateTime.Today.AddDays(-30));

                if (listNew.Count() > 0)
                {
                    dOverallAverage = listNew.Average(item => item.Value.Value);
                }

                IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> listFasting = listNew.Where(item => !string.IsNullOrEmpty(item.ReadingType.Value) && (item.ReadingType.Value.ToLower() == "fasting" || item.ReadingTypeCodedValue.ToLower() == "fasting"));
                IEnumerable<HVManager.BloodGlucoseDataManager.BGItem> listPP = listNew.Where(item => !string.IsNullOrEmpty(item.ReadingType.Value) && (item.ReadingType.Value.ToLower() == "postprandial" || item.ReadingType.Value.ToLower() == "after meal" || item.ReadingTypeCodedValue.ToLower() == "non-fasting"));

                iFastingCount = listFasting.Count();
                iPPCount = listPP.Count();

                if (listFasting.Count() > 0)
                {
                    dFastingAverage = listFasting.Average(item => item.Value.Value);
                }

                if (listPP.Count() > 0)
                {
                    dPPAverage = listPP.Average(item => item.Value.Value);
                }
            }

            PdfPTable tblBGAv = new PdfPTable(2);
            bool bDataExists = false;
            if (dOverallAverage > 0)
            {
                tblBGAv.AddCell(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BG_OverallAverageShort").ToString());
                tblBGAv.AddCell(((int)dOverallAverage).ToString());
                bDataExists = true;
            }

            if (iFastingCount > 0)
            {
                tblBGAv.AddCell(string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BG_FastingAverageShort").ToString(), iFastingCount, iFastingCount == 1 ? string.Empty : "s"));
                tblBGAv.AddCell(((int)dFastingAverage).ToString());
                bDataExists = true;
            }

            if (iPPCount > 0)
            {
                tblBGAv.AddCell(string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BG_AfterMealAverageShort").ToString(), iPPCount, iPPCount == 1 ? string.Empty : "s"));
                tblBGAv.AddCell(((int)dPPAverage).ToString());
                bDataExists = true;
            }

            if (!bDataExists)
            {
                return null;
            }

            return tblBGAv;
        }
    }
}
