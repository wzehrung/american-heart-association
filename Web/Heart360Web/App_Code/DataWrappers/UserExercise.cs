﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HVManager;
using System.Collections.Specialized;
using AHACommon;
using AHAAPI;

namespace Heart360Web
{
    public class ExerciseWeekData
    {
        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public double TotalMinutes
        {
            get;
            set;
        }
    }

    public class UserExercise : UserHealthDataDetails
    {
        public NameValueCollection Last4Weeeks
        {
            get;
            set;
        }

        public bool HasDataInLast4Weeks
        {
            get;
            set;
        }

        public double WeeklyTotal
        {
            get;
            set;
        }

        public UserExercise()
        {
            //HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataForLast2Year();
            HideBadge = true;
            TrackerType = AHAAPI.TrackerType.PhysicalActivity;
            TrackerTypeString = GRCBase.MiscUtility.GetComponentModelPropertyDescription(AHAAPI.TrackerType.PhysicalActivity);
            IEnumerable<ExerciseDataManager.ExerciseItem> objItemList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetLatestNItems(1).Reverse();
            ExerciseGoalDataManager.ExerciseGoalItem objGoalItem = HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.Item;
            ExerciseDataManager.ExerciseItem objItemLatest = null;

            if (objGoalItem != null && objGoalItem.Duration.Value.HasValue && !string.IsNullOrEmpty(objGoalItem.Intensity.Value))
            {
                HasGoal = true;

                string strIntensity = objGoalItem.Intensity.Value;
                string strIntensityLower = objGoalItem.Intensity.Value.ToLower();
                if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.High.ToString().ToLower()) ||
                    strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.VeryHigh.ToString().ToLower()))
                {
                    strIntensity = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Vigorous", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                }
                else if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.Low.ToString().ToLower()) ||
                    strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.VeryLow.ToString().ToLower()))
                {
                    strIntensity = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Light", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                }
                else if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.Moderate.ToString().ToLower()))
                {
                    strIntensity = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Moderate", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                }
                else if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.None.ToString().ToLower()))
                {
                    strIntensity = string.Empty;
                }

                LatestGoal = new object[] { objGoalItem.Duration.Value.Value, strIntensity };
            }


            if (objItemList.Count() > 0)
            {
                objItemLatest = objItemList.ElementAt(0);
                LatestValue = new object[] { objItemLatest.When.Value, objItemLatest.Activity.Value, objItemLatest.Duration.Value };
            }

            DateTime dtLastWeekStart = DateTime.Today.AddDays(-7);

            LinkedList<ExerciseDataManager.ExerciseItem> objExerciseList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataBetweenDates(dtLastWeekStart, DateTime.Today.Date);
            foreach (ExerciseDataManager.ExerciseItem item in objExerciseList)
            {
                if (item.Duration.Value.HasValue)
                {
                    WeeklyTotal += item.Duration.Value.Value;
                }
            }

            //DateTime dtEnd = DateTime.Today.Date;

            //DateTime dtStart = dtEnd.AddDays(-28);
            //LinkedList<ExerciseDataManager.ExerciseItem> objExerciseList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataBetweenDates(dtStart, dtEnd);

            //DateTime dtTemp = dtEnd;
            //NameValueCollection listDate = new NameValueCollection();
            //while (dtTemp >= dtStart)
            //{
            //    List<ExerciseDataManager.ExerciseItem> objList = objExerciseList.Where(i => i.EffectiveDate.Date.CompareTo(dtTemp.Date) == 0).ToList();

            //    if (_HasGoalAttainedOnDate(objList))
            //    {
            //        listDate.Add(dtTemp.ToShortDateString(), "1");
            //    }
            //    else
            //    {
            //        listDate.Add(dtTemp.ToShortDateString(), "0");
            //    }

            //    dtTemp = dtTemp.AddDays(-1);
            //}
            //dtTemp = dtEnd;
            //int iCount = 1;
            //string strWeek1Start = string.Empty;
            //string strWeek2Start = string.Empty;
            //string strWeek3Start = string.Empty;
            //string strWeek4Start = string.Empty;
            //string strWeek1End = string.Empty;
            //string strWeek2End = string.Empty;
            //string strWeek3End = string.Empty;
            //string strWeek4End = string.Empty;
            //int strWeek1Count = 0;
            //int strWeek2Count = 0;
            //int strWeek3Count = 0;
            //int strWeek4Count = 0;
            //while (dtTemp >= dtStart)
            //{
            //    if (iCount == 1)
            //    {
            //        strWeek4End = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount == 7)
            //    {
            //        strWeek4Start = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount <= 7)
            //    {
            //        if (listDate[dtTemp.ToShortDateString()] == "1")
            //        {
            //            strWeek4Count++;
            //        }
            //    }
            //    if (iCount == 8)
            //    {
            //        strWeek3End = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount == 14)
            //    {
            //        strWeek3Start = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount > 7 && iCount <= 14)
            //    {
            //        if (listDate[dtTemp.ToShortDateString()] == "1")
            //        {
            //            strWeek3Count++;
            //        }
            //    }
            //    if (iCount == 15)
            //    {
            //        strWeek2End = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount == 21)
            //    {
            //        strWeek2Start = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount > 14 && iCount <= 21)
            //    {
            //        if (listDate[dtTemp.ToShortDateString()] == "1")
            //        {
            //            strWeek2Count++;
            //        }
            //    }
            //    if (iCount == 22)
            //    {
            //        strWeek1End = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount == 28)
            //    {
            //        strWeek1Start = AHACommon.DateTimeHelper.GetMonthDayDateStringForWeb(dtTemp.Month, dtTemp.Day);
            //    }
            //    if (iCount > 21 && iCount <= 28)
            //    {
            //        if (listDate[dtTemp.ToShortDateString()] == "1")
            //        {
            //            strWeek1Count++;
            //        }
            //    }
            //    dtTemp = dtTemp.AddDays(-1);
            //    iCount++;
            //}

            //Last4Weeeks = new NameValueCollection();
            //Last4Weeeks.Add(string.Format("{0} - {1}", strWeek1Start, strWeek1End), strWeek1Count.ToString());
            //Last4Weeeks.Add(string.Format("{0} - {1}", strWeek2Start, strWeek2End), strWeek2Count.ToString());
            //Last4Weeeks.Add(string.Format("{0} - {1}", strWeek3Start, strWeek3End), strWeek3Count.ToString());
            //Last4Weeeks.Add(string.Format("{0} - {1}", strWeek4Start, strWeek4End), strWeek4Count.ToString());

            //HasDataInLast4Weeks = strWeek1Count > 0 || strWeek2Count > 0 || strWeek3Count > 0 || strWeek4Count > 0;
        }

        private static bool _HasGoalAttainedOnDate(List<ExerciseDataManager.ExerciseItem> objList)
        {
            double dTotalDuration = 0;
            foreach (ExerciseDataManager.ExerciseItem objItem in objList)
            {
                if (objItem.Duration.Value.HasValue)
                {
                    dTotalDuration += objItem.Duration.Value.Value;
                }
            }

            if (dTotalDuration >= 30)
                return true;

            return false;
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelLatestReading()
        {
            if (LatestValue != null)
            {
                RiskLevelHelper.RiskLevelExercise objRiskLevelExercise = new RiskLevelHelper.RiskLevelExercise(WeeklyTotal);
                return objRiskLevelExercise.GetRiskLevelResult();
            }
            return null;
        }

        public override RiskLevelHelper.RiskLevelResult GetRiskLevelResultGoal()
        {
            if (LatestGoal != null)
            {
                RiskLevelHelper.RiskLevelExercise objRiskLevelExerciseGoal = new RiskLevelHelper.RiskLevelExercise(Convert.ToInt32(LatestGoal[0]));
                return objRiskLevelExerciseGoal.GetRiskLevelResult();
            }
            return null;
        }

        public static Dictionary<DateTime, Dictionary<DateTime, ExerciseWeekData>> GetGraphDataForExercise(PatientGuids gPatientHandler, DataDateReturnType eDataDateReturnType)
        {
            Dictionary<DateTime, Dictionary<DateTime, ExerciseWeekData>> listRet = new Dictionary<DateTime, Dictionary<DateTime, ExerciseWeekData>>();

            DateTime dtStart = DateTime.Today;
            DateTime dtTemp = DateTime.Today;
            DateTime dtEnd = DateTime.Today;
            Dictionary<DateTime, ExerciseWeekData> listWeek = null;

            switch (eDataDateReturnType)
            {
                case DataDateReturnType.Last3Months:
                    dtStart = DateTime.Today;
                    dtTemp = dtStart;
                    while (dtTemp.DayOfWeek != DayOfWeek.Saturday)
                    {
                        dtTemp = dtTemp.AddDays(1);
                    }

                    dtStart = dtTemp;
                    dtEnd = dtStart;
                    dtStart = dtStart.AddDays(-90);
                    dtTemp = dtStart;
                    listWeek = _PopulateListWeek(gPatientHandler, dtTemp, dtEnd);
                    listRet.Add(dtTemp, listWeek);
                    break;
                case DataDateReturnType.Last6Months:
                    listRet = _populateMonthYearDataForExercise(gPatientHandler, DataDateReturnType.Last6Months, 1);
                    break;

                case DataDateReturnType.Last9Months:
                    listRet = _populateMonthYearDataForExercise(gPatientHandler, DataDateReturnType.Last9Months, 1);
                    break;

                case DataDateReturnType.LastYear:
                    listRet = _populateMonthYearDataForExercise(gPatientHandler, DataDateReturnType.LastYear, 1);
                    break;

                case DataDateReturnType.Last2Years:
                    listRet = _populateMonthYearDataForExercise(gPatientHandler, DataDateReturnType.Last2Years, 2);
                    break;
            }

            return listRet;


        }

        /// <summary>
        /// compute the end date by going till the next saturday from this months last day
        /// go back n munber of months or years and then find the next sunday for the start date
        /// and compute the total duration for the computed weeks
        /// </summary>
        /// <param name="eDataDateReturnType"></param>
        /// <param name="iMonthFrequencyForEndDate"></param>
        /// <returns></returns>
        private static Dictionary<DateTime, Dictionary<DateTime, ExerciseWeekData>> _populateMonthYearDataForExercise(PatientGuids gPatientHandler, DataDateReturnType eDataDateReturnType, int iMonthFrequencyForEndDate)
        {
            Dictionary<DateTime, Dictionary<DateTime, ExerciseWeekData>> listRet = new Dictionary<DateTime, Dictionary<DateTime, ExerciseWeekData>>();

            DateTime dtStart = DateTime.Today;
            DateTime dtTemp = dtStart.AddMonths(1);
            dtTemp = new DateTime(dtTemp.Year, dtTemp.Month, 1);
            DateTime dtCurrentMonthStart = dtTemp;
            dtTemp = dtTemp.AddDays(-1);

            dtStart = dtTemp;

            while (dtTemp.DayOfWeek != DayOfWeek.Saturday)
            {
                dtTemp = dtTemp.AddDays(1);
            }
            DateTime dtEnd = dtTemp;


            int iWeekListForGraph = 0;
            switch (eDataDateReturnType)
            {
                case DataDateReturnType.Last6Months:
                    dtStart = dtCurrentMonthStart.AddDays(-180);
                    iWeekListForGraph = 6;
                    break;
                case DataDateReturnType.Last9Months:
                    dtStart = dtCurrentMonthStart.AddDays(-270);
                    iWeekListForGraph = 9;
                    break;
                case DataDateReturnType.LastYear:
                    dtStart = dtCurrentMonthStart.AddYears(-1);
                    iWeekListForGraph = 12;
                    break;
                case DataDateReturnType.Last2Years:
                    dtStart = dtCurrentMonthStart.AddYears(-2);
                    iWeekListForGraph = 12;
                    break;

            }
            dtTemp = dtStart;
            while (dtTemp.DayOfWeek != DayOfWeek.Saturday)
            {
                dtTemp = dtTemp.AddDays(1);
            }
            dtTemp = dtTemp.AddDays(1);
            Dictionary<DateTime, ExerciseWeekData> listWeek = new Dictionary<DateTime, ExerciseWeekData>();

            int iCount = 0;
            while (iCount < iWeekListForGraph)
            {
                DateTime dtEndTemp = new DateTime(dtTemp.Year, dtTemp.Month, 1).AddMonths(iMonthFrequencyForEndDate).AddDays(-1);
                while (dtEndTemp.DayOfWeek != DayOfWeek.Saturday)
                {
                    dtEndTemp = dtEndTemp.AddDays(1);
                }

                listWeek = _PopulateListWeek(gPatientHandler, dtTemp, dtEndTemp);
                DateTime dtToPlot = new DateTime(dtTemp.Year, dtTemp.Month, 1);

                listRet.Add(dtToPlot, listWeek);
                iCount++;
                dtTemp = dtEndTemp.AddDays(1);
            }

            return listRet;
        }

        private static Dictionary<DateTime, ExerciseWeekData> _PopulateListWeek(PatientGuids gPatientHandler, DateTime dtStart, DateTime dtEnd)
        {
            DateTime dtTemp = dtStart;
            Dictionary<DateTime, ExerciseWeekData> listWeek = new Dictionary<DateTime, ExerciseWeekData>();
            while (dtTemp.Date <= dtEnd.Date)
            {
                DateTime dtEndTemp = dtTemp.AddDays(6);
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPatientHandler.PersonID, gPatientHandler.RecordID);
                LinkedList<ExerciseDataManager.ExerciseItem> objExerciseList = mgr.ExerciseDataManager.GetDataBetweenDates(dtTemp, dtEndTemp);
                double dVal = objExerciseList.Where(d => d.Duration.Value.HasValue).Sum(e => e.Duration.Value.Value);
                listWeek.Add(dtEndTemp, new ExerciseWeekData { StartDate = dtTemp, EndDate = dtEndTemp, TotalMinutes = dVal });
                dtTemp = dtTemp.AddDays(7);
            }

            return listWeek;
        }
    }
}
