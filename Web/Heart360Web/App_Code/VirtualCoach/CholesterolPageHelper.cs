﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using HVManager;
using AHAHelpContent;
using AHACommon;
using System.Text;

namespace Heart360Web.VirtualCoach
{
    public class CholesterolPageHelper
    {
        const string WELCOME_MSG_GOAL_NOT_SET = "Welcome {0}";
        const string WELCOME_MSG_GOAL_SET = "Welcome back {0}";
        const string GOAL_NOT_SET = "This is the Heart360<sup>&#174;</sup> Cholesterol Management Tool. Please set a goal if you would like me to help you reduce Cholesterol.";
        const string GOAL_ACHIEVED = "Great job, you have reached your goal! Please click the Edit Goal link if you would like to set a new goal.";
        const string GOAL_EXPIRED = "Your old goal has passed its end date. Please click the Edit Goal link if you would like to set a new goal.";
        const string GOAL_IN_PROGRESS = "Keep on working towards that goal!";

        const double AHA_TotalCHolesterol_Threshold = 240;
        const double AHA_HDL_Threshold = 40;
        const double AHA_LDL_Threshold = 160;
        const string DATA_ALERT_STRING = "Your [{0}] cholesterol level is outside of the goal you have set for it. You may wish to consult with your physician about options to achieve your goal.";

        public static SpeechBubblePeriodicData GetSpeechBubble()
        {
            SpeechBubblePeriodicData objSpeechBubblePeriodicData = new SpeechBubblePeriodicData();

            objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_SET, CurrentUser.FirstName);

            CholesterolGoalStatus cholesterolGoalStatus = CholesterolGoalWrapper.CholesterolGoalStatus;

            if (cholesterolGoalStatus == CholesterolGoalStatus.NotSet)
            {
                objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_NOT_SET, CurrentUser.FirstName);
                objSpeechBubblePeriodicData.SpeechText = GOAL_NOT_SET;
            }
            else if (cholesterolGoalStatus == CholesterolGoalStatus.Achieved)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_ACHIEVED;
            }
            else if (cholesterolGoalStatus == CholesterolGoalStatus.InProgress)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_IN_PROGRESS;
            }
            else if (cholesterolGoalStatus == CholesterolGoalStatus.Expired)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_EXPIRED;
            }

            objSpeechBubblePeriodicData.TipOfTheDay = ThingTips.GetTipForTheDay(AHADefs.ThingTipTypes.Cholesterol.ToString(), AHAAPI.H360Utility.GetCurrentLanguageID());

            bool bAlertUser = false;
            StringBuilder sbAlertVal = new StringBuilder();
            CholesterolDataManager.CholesterolItem objCholesterolItem = CholestrolWrapper.GetLatestCholesterol();
            CholesterolGoalDataManager.CholesterolGoalItem objCholesterolGoal = HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item;
            if (objCholesterolItem != null && objCholesterolGoal != null)
            {
                if (objCholesterolItem.TotalCholesterol.Value.HasValue
                    && objCholesterolItem.TotalCholesterol.Value > objCholesterolGoal.TargetTotalCholesterol.Value)
                {
                    bAlertUser = true;
                    sbAlertVal.Append("Total");
                }

                if (objCholesterolItem.HDL.Value.HasValue
                    && objCholesterolItem.HDL.Value.Value < objCholesterolGoal.TargetHDL.Value)
                {
                    bAlertUser = true;
                    if (sbAlertVal.Length > 0)
                    {
                        sbAlertVal.Append("/");
                    }
                    sbAlertVal.Append("HDL");
                }

                if (objCholesterolItem.LDL.Value.HasValue
                    && objCholesterolItem.LDL.Value.Value > objCholesterolGoal.TargetLDL.Value)
                {
                    bAlertUser = true;
                    if (sbAlertVal.Length > 0)
                    {
                        sbAlertVal.Append("/");
                    }
                    sbAlertVal.Append("LDL");
                }

                objSpeechBubblePeriodicData.LastReadingDate = objCholesterolItem.When.Value;
            }

            if (bAlertUser)
            {
                objSpeechBubblePeriodicData.DataAlertText = string.Format(DATA_ALERT_STRING, sbAlertVal.ToString());
                objSpeechBubblePeriodicData.AlertUser = true;
            }



            return objSpeechBubblePeriodicData;
        }
    }
}
