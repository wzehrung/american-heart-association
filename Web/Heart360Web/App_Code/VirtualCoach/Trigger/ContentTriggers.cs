﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using System.Collections.Generic;
using HVManager;

namespace Heart360Web.VirtualCoach.Trigger
{
    /// <summary>
    /// Interface to be implemented by a Content trigger clases reponsible for determining
    /// if a particular content category is eligible for the tips section. 
    /// </summary>
    public interface IContentRuleTrigger
    {
        bool ShouldFire();
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to hypertension
    /// </summary>
    public class HyperTensionTrigger : IContentRuleTrigger
    {
        /// For the hypertension related tips to be displayed, the conditions that are to be satisfied are
        /// Systolic > 139 or
        /// Diastolic > 89
        const int systolicMax = 139;
        const int diastolicMax = 89;
        public bool ShouldFire()
        {
            BloodPressureDataManager.BPItem objBpItem = BloodPressureWrapper.GetLatestBP();

            if (objBpItem != null)
            {
                return (objBpItem.Systolic.Value > systolicMax || objBpItem.Diastolic.Value > diastolicMax);
            }
            return false;
        }
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to diabetes
    /// </summary>
    public class DiabetesTrigger : IContentRuleTrigger
    {
        /// For the diabetes related tips to be displayed, the conditions that are to be satisfied are
        /// # of glucose entries > 0
        const int NumberOfEntriesMax = 0;
        public bool ShouldFire()
        {
            BloodGlucoseDataManager.BGItem objBgItem = BloodGlucoseWrapper.GetLatestBloodGlucose();
            return (objBgItem != null) ? true : false;
        }
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to obesity
    /// </summary>
    public class ObesityTrigger : IContentRuleTrigger
    {
        const int BMIMax = 30;
        public bool ShouldFire()
        {
            double? dblBMI = UserSummary.GetWeightThresholdInPoundsForCurrentRecord(null, UserWeightRangeType.OverWeight);

            if (dblBMI != null)
            {
                return (dblBMI.Value >= BMIMax);
            }
            return false;
        }
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to cholesterol
    /// </summary>
    public class CholesterolTrigger : IContentRuleTrigger
    {
        const int TotalCholestrolMax = 240;

        public bool ShouldFire()
        {
            CholesterolDataManager.CholesterolItem objCholesterolItem = CholestrolWrapper.GetLatestCholesterol();
            if (objCholesterolItem != null)
            {
                if (objCholesterolItem.TotalCholesterol.Value.HasValue)
                {
                    return (objCholesterolItem.TotalCholesterol.Value.Value > TotalCholestrolMax);
                }
            }
            return false;
        }
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to medication
    /// </summary>
    public class MedicationTrigger : IContentRuleTrigger
    {
        public bool ShouldFire()
        {
            return false;
        }
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to smoking
    /// </summary>
    public class SmokingTrigger : IContentRuleTrigger
    {
        const int CigarettesMax = 0;
        public bool ShouldFire()
        {
            if (HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item != null)
            {
                if (HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item.IsSmokeCigarette != null && HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item.IsSmokeCigarette.Value == true)
                {
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to physical activity
    /// </summary>
    public class PhysicalActivityTrigger : IContentRuleTrigger
    {
        public bool ShouldFire()
        {
            return false;
        }
    }

    /// <summary>
    /// This class is to determine if the person needs to be shown the tips
    /// specific to diet
    /// </summary>
    public class DietTrigger : IContentRuleTrigger
    {
        public bool ShouldFire()
        {
            return false;
        }
    }
}
