﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Reflection;
using System.Collections.Generic;
using AHAHelpContent;

namespace Heart360Web.App_Code.VirtualCoach.Trigger
{
    public class TriggerManager
    {
        public static List<AHAHelpContent.Content> GetCategoryTipsForDisplay()
        {
            string strCategories = GetAllTriggerBasedCategoriesForCurrentUser();
            return AHAHelpContent.Content.GetContentInformation(strCategories, AHAAPI.H360Utility.GetCurrentLanguageID());
        }

        public static List<AHAHelpContent.Content> GetCategoryTipsForDisplay(string strAbbreviation)
        {
            return AHAHelpContent.Content.GetContentInformationForSpecificType(strAbbreviation, AHAAPI.H360Utility.GetCurrentLanguageID());
        }

        private static string GetAllTriggerBasedCategoriesForCurrentUser()
        {
            string strCategories = string.Empty;
            List<ContentRules> objContentRulesList = ContentRules.GetAllContentRules(AHAAPI.H360Utility.GetCurrentLanguageID());
            foreach (ContentRules objContentRule in objContentRulesList)
            {
                if (IsCategorySelectedForVirtualCoach(objContentRule))
                {
                    strCategories += objContentRule.Abbreviation;
                    strCategories += ",";
                }
            }
            strCategories = strCategories.TrimEnd(',');
            return strCategories;
        }

        private static bool IsCategorySelectedForVirtualCoach(ContentRules objContentRule)
        {
            string namespacePrefix = "Heart360Web.VirtualCoach.Trigger";
            string typeName = namespacePrefix + "." + objContentRule.ClassName;

            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            object triggerCheckObject = currentAssembly.CreateInstance(typeName);
            Type typeOfTriggerCheckObject = triggerCheckObject.GetType();
            Type ifaceType = typeOfTriggerCheckObject.GetInterface(namespacePrefix + "." + "IContentRuleTrigger");
            MethodInfo shouldFireMethod = ifaceType.GetMethod("ShouldFire");
            return (bool)shouldFireMethod.Invoke(triggerCheckObject, null);
        }
    }
}

