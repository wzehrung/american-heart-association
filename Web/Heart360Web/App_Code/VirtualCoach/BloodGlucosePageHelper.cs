﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using HVManager;
using AHAHelpContent;
using AHACommon;

namespace Heart360Web.VirtualCoach
{
    public class BloodGlucosePageHelper
    {
        const string WELCOME_MSG = "Welcome {0}";
        const double AHA_Low_Threshold = 60;
        const double AHA_High_Threshold = 300;
        const string DATA_ALERT_STRING_LOW = "Your blood glucose appears to be low. This may be an indicator that you are hypoglycemic. You may wish to consult your physician.";
        const string DATA_ALERT_STRING_HIGH = "Your blood glucose appears to be high. You may wish to consult your physician.";
        const string DIABETES_LINK = "&nbsp;<a target=\"_blank\" href=\"http://www.americanheart.org/presenter.jhtml?identifier=3044757\">About Diabetes</a>";

        public static SpeechBubblePeriodicData GetSpeechBubble()
        {
            SpeechBubblePeriodicData objSpeechBubblePeriodicData = new SpeechBubblePeriodicData();

            objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG, CurrentUser.FirstName);

            objSpeechBubblePeriodicData.TipOfTheDay = ThingTips.GetTipForTheDay(AHADefs.ThingTipTypes.BloodGlucose.ToString(), AHAAPI.H360Utility.GetCurrentLanguageID());

            BloodGlucoseDataManager.BGItem objBGItem = BloodGlucoseWrapper.GetLatestBloodGlucose();
            if (objBGItem != null)
            {
                if (objBGItem.Value.Value < AHA_Low_Threshold)
                {
                    objSpeechBubblePeriodicData.DataAlertText = DATA_ALERT_STRING_LOW + DIABETES_LINK;
                    objSpeechBubblePeriodicData.AlertUser = true;
                }
                else if (objBGItem.Value.Value > AHA_High_Threshold)
                {
                    objSpeechBubblePeriodicData.DataAlertText = DATA_ALERT_STRING_HIGH + DIABETES_LINK;
                    objSpeechBubblePeriodicData.AlertUser = true;
                }

                objSpeechBubblePeriodicData.LastReadingDate = objBGItem.When.Value;
            }

            return objSpeechBubblePeriodicData;
        }
    }
}
