﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using HVManager;
using AHAHelpContent;
using AHACommon;

namespace Heart360Web.VirtualCoach
{
    public class WeightPageHelper
    {
        const string WELCOME_MSG_GOAL_NOT_SET = "Welcome {0}";
        const string WELCOME_MSG_GOAL_SET = "Welcome back {0}";
        const string GOAL_NOT_SET = "This is the Heart360<sup>&#174;</sup> Weight Management Tool. Please set a goal if you would like me to help you lose weight.";
        const string GOAL_ACHIEVED = "Great job, you have reached your goal! Please click the Edit Goal link if you would like to set a new goal.";
        const string GOAL_EXPIRED = "Your old goal has passed its end date. Please click the Edit Goal link if you would like to set a new goal.";
        const string GOAL_IN_PROGRESS = "Keep on working towards that goal!";

        public static SpeechBubblePeriodicData GetSpeechBubble()
        {
            SpeechBubblePeriodicData objSpeechBubblePeriodicData = new SpeechBubblePeriodicData();

            WeightGoalStatus weightGoalStatus = WeightGoalWrapper.WeightGoalStatus;


            objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_SET, CurrentUser.FirstName);
            if (weightGoalStatus == WeightGoalStatus.NotSet)
            {
                objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_NOT_SET, CurrentUser.FirstName);
                objSpeechBubblePeriodicData.SpeechText = GOAL_NOT_SET;
            }
            else if (weightGoalStatus == WeightGoalStatus.Achieved)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_ACHIEVED;
            }
            else if (weightGoalStatus == WeightGoalStatus.InProgress)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_IN_PROGRESS;
            }
            else if (weightGoalStatus == WeightGoalStatus.Expired)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_EXPIRED;
            }

            objSpeechBubblePeriodicData.TipOfTheDay = ThingTips.GetTipForTheDay(AHADefs.ThingTipTypes.Weight.ToString(), AHAAPI.H360Utility.GetCurrentLanguageID());

            return objSpeechBubblePeriodicData;
        }
    }
}
