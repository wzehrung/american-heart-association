﻿
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Heart360Web.VirtualCoach
{
    public class SpeechBubblePeriodicData
    {
        string m_WelcomeText;
        string m_SpeechText;
        string m_TipOfTheDay;
        string m_DataAlertText;
        bool m_AlertUser;
        DateTime? m_LastReadingDate;

        public string WelcomeText
        {
            get
            {
                return m_WelcomeText;
            }
            set
            {
                m_WelcomeText = value;
            }
        }

        public string SpeechText
        {
            get
            {
                return m_SpeechText;
            }
            set
            {
                m_SpeechText = value;
            }
        }

        public string TipOfTheDay
        {
            get
            {
                return m_TipOfTheDay;
            }
            set
            {
                m_TipOfTheDay = value;
            }
        }

        public string DataAlertText
        {
            get
            {
                return m_DataAlertText;
            }
            set
            {
                m_DataAlertText = value;
            }
        }

        public bool AlertUser
        {
            get
            {
                return m_AlertUser;
            }
            set
            {
                m_AlertUser = value;
            }
        }

        public DateTime? LastReadingDate
        {
            get
            {
                return m_LastReadingDate;
            }
            set
            {
                m_LastReadingDate = value;
            }
        }
    }
}
