﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Text;
using System.Collections.Generic;

namespace Heart360Web.VirtualCoach
{
    public class HomePageHelper
    {
        const int DAYS_BW_LOGIN_AND_READING = 7;
        public static List<string> GetAlerts()
        {
            List<string> objList = new List<string>();
            
            SpeechBubblePeriodicData objBloodGlucosePageHelper = BloodGlucosePageHelper.GetSpeechBubble();
            SpeechBubblePeriodicData objBPPageHelper = BPPageHelper.GetSpeechBubble();
            SpeechBubblePeriodicData objCholesterolPageHelper = CholesterolPageHelper.GetSpeechBubble();
            SpeechBubblePeriodicData objExercisePageHelper = ExercisePageHelper.GetSpeechBubble();

            TimeSpan timespanBG = new TimeSpan();
            if(objBloodGlucosePageHelper.LastReadingDate.HasValue)
            {
                timespanBG = DateTime.Now - objBloodGlucosePageHelper.LastReadingDate.Value;
            }

            TimeSpan timespanBP = new TimeSpan();
            if(objBPPageHelper.LastReadingDate.HasValue)
            {
                timespanBP = DateTime.Now - objBPPageHelper.LastReadingDate.Value;
            }

            TimeSpan timespanCholesterol = new TimeSpan();
            if(objCholesterolPageHelper.LastReadingDate.HasValue)
            {
                timespanCholesterol = DateTime.Now - objCholesterolPageHelper.LastReadingDate.Value;
            }

            TimeSpan timespanExercise = new TimeSpan();
            if(objExercisePageHelper.LastReadingDate.HasValue)
            {
                timespanExercise = DateTime.Now - objExercisePageHelper.LastReadingDate.Value;
            }

            if (timespanBG.Days < DAYS_BW_LOGIN_AND_READING && objBloodGlucosePageHelper.AlertUser)
            {
                objList.Add(objBloodGlucosePageHelper.DataAlertText);
            }

            if (timespanBP.Days < DAYS_BW_LOGIN_AND_READING && objBPPageHelper.AlertUser)
            {
                objList.Add(objBPPageHelper.DataAlertText);
            }

            if (timespanCholesterol.Days < DAYS_BW_LOGIN_AND_READING && objCholesterolPageHelper.AlertUser)
            {
                objList.Add(objCholesterolPageHelper.DataAlertText);
            }

            if (timespanExercise.Days < DAYS_BW_LOGIN_AND_READING && objExercisePageHelper.AlertUser)
            {
                objList.Add(objExercisePageHelper.DataAlertText);
            }

            return objList;
        }
    }
}
