﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AHAHelpContent;
using AHACommon;
using System.Collections.Generic;
using HVManager;

namespace Heart360Web.VirtualCoach
{
    public class BPPageHelper
    {
        const string WELCOME_MSG_GOAL_NOT_SET = "Welcome {0}";
        const string WELCOME_MSG_GOAL_SET = "Welcome back {0}";
        const string GOAL_NOT_SET = "This is the Heart360<sup>&#174;</sup> Blood Pressure Management Tool. Please set a goal if you would like me to help you reduce Blood Pressure.";
        const string GOAL_ACHIEVED = "Great job, you have reached your goal! Please click the Edit Goal link if you would like to set a new goal.";
        const string GOAL_EXPIRED = "Your old goal has passed its end date. Please click the Edit Goal link if you would like to set a new goal.";
        const string GOAL_IN_PROGRESS = "Keep on working towards that goal!";

        const string DATA_ALERT_STRING = "Your blood pressure has changed significantly in a short period of time. You may wish to consult your physician.";
        const int ALER_MAX_MIN_DIFFERENCE = 15;

        public static SpeechBubblePeriodicData GetSpeechBubble()
        {
            SpeechBubblePeriodicData objSpeechBubblePeriodicData = new SpeechBubblePeriodicData();

            BPGoalStatus bpGoalStatus = BPGoalWrapper.BPGoalStatus;

            objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_SET, CurrentUser.FirstName);
            if (bpGoalStatus == BPGoalStatus.NotSet)
            {
                objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_NOT_SET, CurrentUser.FirstName);
                objSpeechBubblePeriodicData.SpeechText = GOAL_NOT_SET;
            }
            else if (bpGoalStatus == BPGoalStatus.Achieved)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_ACHIEVED;
            }
            else if (bpGoalStatus == BPGoalStatus.InProgress)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_IN_PROGRESS;
            }
            else if (bpGoalStatus == BPGoalStatus.Expired)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_EXPIRED;
            }

            objSpeechBubblePeriodicData.TipOfTheDay = ThingTips.GetTipForTheDay(AHADefs.ThingTipTypes.BloodPressure.ToString(), AHAAPI.H360Utility.GetCurrentLanguageID());

            LinkedList<BloodPressureDataManager.BPItem> objList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataBetweenDates(DateTime.Today, DateTime.Now);
            int iMinSystolic = 1000;
            int iMaxSystolic = 0;
            int iMinDiastolic = 1000;
            int iMaxDiastolic = 0;
            foreach (BloodPressureDataManager.BPItem objBPItem in objList)
            {
                if (objBPItem.Systolic.Value < iMinSystolic)
                {
                    iMinSystolic = objBPItem.Systolic.Value;
                }

                if (objBPItem.Diastolic.Value < iMinDiastolic)
                {
                    iMinDiastolic = objBPItem.Diastolic.Value;
                }

                if (objBPItem.Systolic.Value > iMaxSystolic)
                {
                    iMaxSystolic = objBPItem.Systolic.Value;
                }

                if (objBPItem.Diastolic.Value > iMaxDiastolic)
                {
                    iMaxDiastolic = objBPItem.Diastolic.Value;
                }
            }

            if (iMaxSystolic - iMinSystolic > ALER_MAX_MIN_DIFFERENCE
                || iMaxDiastolic - iMinDiastolic > ALER_MAX_MIN_DIFFERENCE)
            {
                objSpeechBubblePeriodicData.DataAlertText = DATA_ALERT_STRING;
                objSpeechBubblePeriodicData.AlertUser = true;
            }

            BloodPressureDataManager.BPItem objBPLatest = BloodPressureWrapper.GetLatestBP();
            if (objBPLatest != null)
            {
                objSpeechBubblePeriodicData.LastReadingDate = objBPLatest.When.Value;
            }

            return objSpeechBubblePeriodicData;
        }
    }
}
