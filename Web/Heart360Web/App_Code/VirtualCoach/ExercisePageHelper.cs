﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using HVManager;
using AHAHelpContent;
using AHACommon;

namespace Heart360Web.VirtualCoach
{
    public class ExercisePageHelper
    {
        const string WELCOME_MSG_GOAL_NOT_SET = "Welcome {0}";
        const string WELCOME_MSG_GOAL_SET = "Welcome back {0}";
        const string GOAL_NOT_SET = "The American Heart Association recommends 5 days of physical activity of 30 minutes at moderate intensity for good heart health. This tool tracks your progress each week against his goal.";
        const string GOAL_ACHIEVED = "Great job, you have reached your goal for this week! Keep up the great work!";
        const string GOAL_IN_PROGRESS = "Keep on working towards that goal of being physically active five times each week for 30 minutes or more!";

        const string DATA_ALERT_STRING = "The American Heart Association recommends 5 days of physical activity of 30 minutes or more per week to maintain cardiovascular health. Consider adding more physical activity to your weekly schedule.";

        public static SpeechBubblePeriodicData GetSpeechBubble()
        {
            SpeechBubblePeriodicData objSpeechBubblePeriodicData = new SpeechBubblePeriodicData();

            objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_SET, CurrentUser.FirstName);

            ExerciseGoalStatus exerciseGoalStatus = ExerciseGoalWrapper.ExerciseGoalStatus;

            if (exerciseGoalStatus == ExerciseGoalStatus.NotSet)
            {
                objSpeechBubblePeriodicData.WelcomeText = string.Format(WELCOME_MSG_GOAL_NOT_SET, CurrentUser.FirstName);
                objSpeechBubblePeriodicData.SpeechText = GOAL_NOT_SET;
            }
            else if (exerciseGoalStatus == ExerciseGoalStatus.Achieved)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_ACHIEVED;
            }
            else if (exerciseGoalStatus == ExerciseGoalStatus.InProgress)
            {
                objSpeechBubblePeriodicData.SpeechText = GOAL_IN_PROGRESS;
            }

            objSpeechBubblePeriodicData.TipOfTheDay = ThingTips.GetTipForTheDay(AHADefs.ThingTipTypes.Exercise.ToString(), AHAAPI.H360Utility.GetCurrentLanguageID());

            if (exerciseGoalStatus != ExerciseGoalStatus.Achieved)
            {
                objSpeechBubblePeriodicData.DataAlertText = DATA_ALERT_STRING;
                objSpeechBubblePeriodicData.AlertUser = true;
            }

            ExerciseDataManager.ExerciseItem objItem = ExerciseDataWrapper.GetLatestExerciseData();
            if (objItem != null)
            {
                objSpeechBubblePeriodicData.LastReadingDate = objItem.When.Value;
            }

            return objSpeechBubblePeriodicData;
        }
    }
}
