﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using System.Collections.ObjectModel;
using AHAAPI;

namespace Heart360Web
{
    public class HVHelper
    {
        public static HVManager.AllItemManager HVManagerPatientBase
        {
            get
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId((System.Web.HttpContext.Current.Handler as H360PageHelper).PersonGUID, (System.Web.HttpContext.Current.Handler as H360PageHelper).RecordGUID);
                return mgr;
            }
        }

        public static bool HasPatientGivenOfflineAccessToApp(Guid hvpersonid, Guid hvrecordid)
        {
            HealthRecordAccessor accessor = HVManager.Helper.GetAccessor(hvpersonid, hvrecordid);

            IList<Guid> idlist = new List<Guid>();
            idlist.Add(BloodPressure.TypeId);
            idlist.Add(Personal.TypeId);

            Collection<HealthRecordItemTypePermission> list = accessor.QueryPermissions(idlist);

            bool bHasOfflineAccess = false;

            foreach (HealthRecordItemTypePermission item in list)
            {
                if (item.OfflineAccessPermissions == HealthRecordItemPermissions.All)
                {
                    bHasOfflineAccess = true;
                    break;
                }
            }

            return bHasOfflineAccess;
        }

        /// <summary>
        /// This functions returns the personal object item guid of the selected record
        /// </summary>
        /// <returns>Guid</returns>
        public static Guid GetPersonalItemGUID(bool bRedirect)
        {
            HVManager.PersonalDataManager.PersonalItem objPI = HVManagerPatientBase.PersonalDataManager.Item;
            if (objPI == null)
            {
                HVManagerPatientBase.PersonalDataManager.FlushCache();
                objPI = HVManagerPatientBase.PersonalDataManager.Item;
                if (objPI == null)
                {
                    HVManager.PersonalDataManager.PersonalItem item =  new HVManager.PersonalDataManager.PersonalItem();
                    item.FirstName.Value = HVManagerPatientBase.Name;
                    HVManager.PersonalDataManager.PersonalItem objPersonalItem = HVManagerPatientBase.PersonalDataManager.CreateOrUpdateItem(item);
                    HVManagerPatientBase.PersonalDataManager.FlushCache();
                    objPI = HVManagerPatientBase.PersonalDataManager.Item;
                    if (objPI == null)
                    {
                        if (bRedirect)
                        {
                            System.Web.HttpContext.Current.Response.Redirect(string.Format("~/NoPersonalInfo.aspx?Guid={0}", Guid.NewGuid().ToString()), true);
                        }
                        else
                        {
                            return Guid.Empty;
                        }
                    }
                    else
                    {
                        SessionInfo.ClearUserIdentityContext();
                        System.Web.HttpContext.Current.Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
                    }
                }
                //throw new Exception(string.Format("Personal info not found! Please add your personal info by going to {0}.", Microsoft.Health.Web.WebApplicationConfiguration.ShellUrl));
            }

            return objPI.ThingID; ;
        }
    }
}
