﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Web
{
    /// <summary>
    /// This is the patient info
    /// </summary>
    public class PatientInfo
    {
        public int UserHealthRecordID
        {
            get;
            set;
        }

        public Guid PersonGUID
        {
            get;
            set;
        }

        public Guid RecordGUID
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public int TotalAlerts
        {
            get;
            set;
        }

        public DateTime? DateJoined
        {
            get;
            set;
        }

        public DateTime? LastActive
        {
            get;
            set;
        }

        public DateTime? DateOfBirth
        {
            get;
            set;
        }

        public bool HasAlertRule
        {
            get;
            set;
        }

        public double? BloodGlucose
        {
            get;
            set;
        }

        public string BloodGlucoseReadingType
        {
            get;
            set;
        }

        public DateTime BloodGlucoseEffectiveDate
        {
            get;
            set;
        }

        public int? Systolic
        {
            get;
            set;
        }

        public int? Diastolic
        {
            get;
            set;
        }

        public DateTime BloodPressureEffectiveDate
        {
            get;
            set;
        }

        public double? Weight
        {
            get;
            set;
        }

        public int? Cholesterol
        {
            get;
            set;
        }

        public DateTime CholesterolEffectiveDate
        {
            get;
            set;
        }

        public DateTime? ProviderConnectionDate
        {
            get;
            set;
        }

        public static List<PatientInfo> GetPopulatedPatientInfo(List<AHAHelpContent.Patient> lstPatients)
        {
            List<PatientInfo> lstPatientInfo = new List<PatientInfo>();

            lstPatients.ForEach(delegate(AHAHelpContent.Patient objPatient)
            {
                PatientInfo objPatientInfo = new PatientInfo();
                objPatientInfo.UserHealthRecordID = objPatient.UserHealthRecordID;
                objPatientInfo.PersonGUID = objPatient.OfflinePersonGUID.Value;
                objPatientInfo.RecordGUID = objPatient.OfflineHealthRecordGUID.Value;
                objPatientInfo.HasAlertRule = objPatient.HasAlertRule;
                objPatientInfo.DateJoined = objPatient.DateJoined;
                objPatientInfo.ProviderConnectionDate = objPatient.ProviderConnectionDate;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                //commented by arun - no need to cache data - MS had issue on March 7, 2011 where a user had lots of data in HV for the exercise data type
                //mgr.DownloadDataBetweenDates(new List<string> 
                //{ 
                //    typeof(HVManager.BasicDataManager).ToString(), 
                //    typeof(HVManager.BloodGlucoseDataManager).ToString(), 
                //    typeof(HVManager.BloodPressureDataManager).ToString(),
                //    typeof(HVManager.CholesterolDataManager).ToString(), 
                //    typeof(HVManager.ExerciseDataManager).ToString(), 
                //    typeof(HVManager.HeightDataManager).ToString(),
                //    typeof(HVManager.MedicationDataManager).ToString(), 
                //    typeof(HVManager.PersonalDataManager).ToString(), 
                //    typeof(HVManager.PersonalContactDataManager).ToString(), 
                //    typeof(HVManager.WeightDataManager).ToString() 
                //}, DateTime.Today.AddYears(-2), DateTime.MaxValue);


                if (mgr.PersonalDataManager.Item != null && !String.IsNullOrEmpty(mgr.PersonalDataManager.Item.FirstName.Value))
                    objPatientInfo.FirstName = mgr.PersonalDataManager.Item.FirstName.Value;
                else
                    objPatientInfo.FirstName = "-";

                if (mgr.PersonalDataManager.Item != null && !String.IsNullOrEmpty(mgr.PersonalDataManager.Item.LastName.Value))
                    objPatientInfo.LastName = mgr.PersonalDataManager.Item.LastName.Value;
                else
                    objPatientInfo.LastName = "-";

                objPatientInfo.FullName = string.Format("{0} {1}", objPatientInfo.FirstName, objPatientInfo.LastName);
                if (mgr.PersonalDataManager.Item == null)
                {
                    objPatientInfo.FullName = mgr.Name;
                }

                if (mgr.PersonalDataManager.Item != null && mgr.PersonalDataManager.Item.DateOfBirth.Value.HasValue)
                    objPatientInfo.DateOfBirth = mgr.PersonalDataManager.Item.DateOfBirth.Value.Value;

                objPatientInfo.LastActive = mgr.LastActiveDate;
                if (mgr.BloodGlucoseDataManager.GetLatestNItems(1).Count > 0)
                {
                    objPatientInfo.BloodGlucose = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.Value.Value;
                    objPatientInfo.BloodGlucoseReadingType = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.ReadingType.Value;
                    objPatientInfo.BloodGlucoseEffectiveDate = mgr.BloodGlucoseDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                }

                if (mgr.BloodPressureDataManager.GetLatestNItems(1).Count > 0)
                {
                    objPatientInfo.Systolic = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.Systolic.Value;
                    objPatientInfo.Diastolic = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.Diastolic.Value;
                    objPatientInfo.BloodPressureEffectiveDate = mgr.BloodPressureDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                }

                if (mgr.CholesterolDataManager.GetLatestNItems(1).Count > 0)
                {
                    objPatientInfo.Cholesterol = mgr.CholesterolDataManager.GetLatestNItems(1).First.Value.TotalCholesterol.Value;
                    objPatientInfo.CholesterolEffectiveDate = mgr.CholesterolDataManager.GetLatestNItems(1).First.Value.EffectiveDate;
                }

                objPatientInfo.TotalAlerts = objPatient.TotalAlerts;
                lstPatientInfo.Add(objPatientInfo);
            });

            return lstPatientInfo;
        }
    }

    public class PatientGuids
    {
        public Guid PersonID
        {
            get;
            set;
        }


        public Guid RecordID
        {
            get;
            set;
        }

        public static PatientGuids CurrentPatientGuids
        {
            get
            {
                return new PatientGuids { PersonID = (System.Web.HttpContext.Current.Handler as H360PageHelper).PersonGUID, RecordID = (System.Web.HttpContext.Current.Handler as H360PageHelper).RecordGUID };
            }
        }
    };
}
