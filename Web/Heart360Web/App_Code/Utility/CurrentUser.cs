﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AHAHelpContent;
using Heart360Web;
using HVManager;

namespace Heart360Web
{
    public class CurrentUser
    {
        public static int? YearOfBirth
        {
            get
            {
                HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;
                HVManager.PersonalDataManager.PersonalItem item = mgr.PersonalDataManager.Item;
                if (item != null && item.DateOfBirth.Value.HasValue)
                {
                    return item.DateOfBirth.Value.Value.Year;
                }

                return null;
            }
        }

        public static string FirstName
        {
            get
            {
                HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;
                HVManager.PersonalDataManager.PersonalItem item = mgr.PersonalDataManager.Item;
                if (item != null && !string.IsNullOrEmpty(item.FirstName.Value))
                {
                    return item.FirstName.Value;
                }

                return mgr.Name;
            }
        }

        public static string LastName
        {
            get
            {
                HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;
                HVManager.PersonalDataManager.PersonalItem item = mgr.PersonalDataManager.Item;
                if (item != null)
                {
                    return item.LastName.Value;
                }

                return mgr.Name;
            }
        }

        public static string FullName
        {
            get
            {
                HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;
                HVManager.PersonalDataManager.PersonalItem item = mgr.PersonalDataManager.Item;
                if (item != null)
                {
                    return item.FullName;
                }

                return mgr.Name;
            }
        }

        public static string PrimaryPhone
        {
            get
            {
                //HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;
                //HVManager.PersonalContactDataManager.ContactItem item = mgr.PersonalContactDataManager.Item;
                HVManager.PersonalContactDataManager.ContactItem contactItem = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;

                if (contactItem == null) return null;


                return contactItem.Phone.Value;
            }
        }

        public static string Email
        {
            get
            {
                HVManager.PersonalContactDataManager.ContactItem contactItem = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;

                if (contactItem == null) return null;

                if (contactItem.EmailList.Count == 0)
                    return null;

                return contactItem.EmailList[0].Email.Value;
            }
        }

        public static AddressItem Address
        {
            get
            {
                HVManager.PersonalContactDataManager.ContactItem contactItem = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;

                if (contactItem == null) return null;

                if (contactItem.AddressList.Count == 0)
                    return null;

                return contactItem.AddressList[0];
            }
        }

        public static string ContactPhone
        {
            get
            {
                AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordGUID((System.Web.HttpContext.Current.Handler as H360PageHelper).RecordGUID);
                PhoneNumbers objPhoneNumbers = PhoneNumbers.GetPhoneNumbers(objPatient.UserHealthRecordID);
                return objPhoneNumbers.PhoneContact;
            }
        }

        public static string MobilePhone
        {
            get
            {
                AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordGUID((System.Web.HttpContext.Current.Handler as H360PageHelper).RecordGUID);
                PhoneNumbers objPhoneNumbers = PhoneNumbers.GetPhoneNumbers(objPatient.UserHealthRecordID);
                return objPhoneNumbers.PhoneMobile;
            }
        }
    }
}
