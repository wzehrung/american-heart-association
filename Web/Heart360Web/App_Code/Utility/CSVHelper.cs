﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GRCBase;
using AHACommon;
using AHAHelpContent;
using System.Resources;
using System.Text;

namespace Heart360Web
{
    public static class CSVHelper
    {
        static StringBuilder sb;

        private static void getCSVHeader()
        {
            sb = new StringBuilder();
            
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Name"));
            sb.Append(",");

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_DOB"));
            sb.Append(",");

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_DateJoined"));
            sb.Append(",");

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Provider2", "Provider_Last_Active"));
            sb.Append(",");

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodPressure"));
            sb.Append(",");

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_BloodGlucose"));
            sb.Append(",");

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Cholesterol"));
            sb.Append(",");

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Provider2", "Provider_Alerts"));
            sb.Append('\n');
        }

        public static string getCSVStringContent(List<PatientInfo> lstPatientInfo)
        {
            getCSVHeader();

            foreach (PatientInfo objPatientInfo in lstPatientInfo)
            {
                sb.Append(objPatientInfo.FullName.Replace(",",""));
                sb.Append(",");
                if (objPatientInfo.DateOfBirth.HasValue)
                {
                    sb.Append(objPatientInfo.DateOfBirth.Value.ToShortDateString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }
                if (objPatientInfo.ProviderConnectionDate.HasValue)
                {

                    sb.Append(objPatientInfo.ProviderConnectionDate.Value.ToShortDateString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }

                if (objPatientInfo.LastActive.HasValue)
                {
                    sb.Append(objPatientInfo.LastActive.Value.ToShortDateString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }

                if ((!string.IsNullOrEmpty(objPatientInfo.Systolic.ToString()) && (!string.IsNullOrEmpty(objPatientInfo.Diastolic.ToString()))))
                {

                    sb.Append(objPatientInfo.Systolic.ToString() + "/" + objPatientInfo.Diastolic.ToString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }

                if (!string.IsNullOrEmpty(objPatientInfo.BloodGlucose.ToString()))
                {
                    sb.Append(objPatientInfo.BloodGlucose.ToString() + " mg/dL");
                    sb.Append(",");
                }
                else
                {
                    sb.Append(objPatientInfo.BloodGlucose.ToString());
                    sb.Append(",");
                }
                if (objPatientInfo.Cholesterol != null && objPatientInfo.Cholesterol > 0)
                {
                    sb.Append(objPatientInfo.Cholesterol.ToString() + " mg/dL");
                    sb.Append(",");
                }
                else
                {
                    sb.Append(objPatientInfo.Cholesterol.ToString());
                    sb.Append(",");
                }

                if (objPatientInfo.TotalAlerts > 0)
                {
                    sb.Append(objPatientInfo.TotalAlerts.ToString());
                    sb.Append('\n');
                }
                else
                {
                    sb.Append("");
                    sb.Append('\n');
                }


            }
            return sb.ToString();


        }


    }
}
