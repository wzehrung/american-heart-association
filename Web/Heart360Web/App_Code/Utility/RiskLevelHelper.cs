﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GroupManager;
using System.ComponentModel;
using AHAAPI;
using HVManager;

namespace Heart360Web
{
    public class RiskLevelHelper
    {
        public static bool ShouldShowMLCAlert()
        {
            UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
            if (userIdentityContext != null)
            {
                HVManager.FileDataManager.FileItem fileItem = HVHelper.HVManagerPatientBase.FileDataManager.Item;
                bool bLookForDBValues = true;
                DateTime dtLastUpdated = DateTime.Now;
                double? dBMI = null;
                double? dBloodGlucose = null;
                int? iTotalCholesterol = null;
                int? iSystolic = null;
                int? iDiastolic = null;

                double? dHbA1c = null;

                if (fileItem != null
                    && fileItem.HealthScore.Value.HasValue
                    && fileItem.BMI.Value.HasValue
                    && fileItem.BloodGlucose.Value.HasValue
                    && fileItem.HbA1c.Value.HasValue
                    && fileItem.TotalCholesterol.Value.HasValue
                    && fileItem.SystolicBP.Value.HasValue
                    && fileItem.DiastolicBP.Value.HasValue
                    )
                {
                    bLookForDBValues = false;
                    dtLastUpdated = fileItem.EffectiveDate;
                    dBMI = fileItem.BMI.Value;
                    dBloodGlucose = fileItem.BloodGlucose.Value;
                    dHbA1c = fileItem.HbA1c.Value;
                    iTotalCholesterol = fileItem.TotalCholesterol.Value;
                    iSystolic = fileItem.SystolicBP.Value;
                    iDiastolic = fileItem.DiastolicBP.Value;
                }

                AHAHelpContent.MyLifeCheckItem objMLCItem = AHAHelpContent.MyLifeCheckItem.GetLatestMLCItem(userIdentityContext.PersonalItemGUID);
                if (bLookForDBValues && objMLCItem != null)
                {
                    dtLastUpdated = objMLCItem.UpdatedDate;
                    dBMI = objMLCItem.BMI;
                    dBloodGlucose = objMLCItem.BloodGlucose;
                    iTotalCholesterol = objMLCItem.TotalCholesterol;
                    iSystolic = objMLCItem.SystolicBP;
                    iDiastolic = objMLCItem.DiastolicBP;
                }

                if (bLookForDBValues && objMLCItem == null)
                {
                    return true;
                }

                //bp 
                IEnumerable<BloodPressureDataManager.BPItem> objBPList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetLatestNItems(1).Reverse();
                BloodPressureDataManager.BPItem objBPItem = null;
                if (objBPList.Count() > 0)
                {
                    objBPItem = objBPList.First();
                }

                if (objBPItem != null)
                {
                    TimeSpan tsBP = objBPItem.EffectiveDate.Subtract(dtLastUpdated);
                    if (tsBP.TotalHours > 24)
                    {
                        RiskLevelHelper.RiskLevelBloodPressure objRiskLevelBloodPressure = new RiskLevelHelper.RiskLevelBloodPressure(objBPItem.Systolic.Value, objBPItem.Diastolic.Value);
                        RiskLevelHelper.RiskLevelResult objRiskBPLatest = objRiskLevelBloodPressure.GetRiskLevelResult();

                        RiskLevelHelper.RiskLevelBloodPressure objRiskLevelBloodPressureMLC = new RiskLevelHelper.RiskLevelBloodPressure(iSystolic.Value, iDiastolic.Value);
                        RiskLevelHelper.RiskLevelResult objRiskBPLatestMLC = objRiskLevelBloodPressureMLC.GetRiskLevelResult();

                        if (objRiskBPLatest.RiskLevelResultType != objRiskBPLatestMLC.RiskLevelResultType)
                        {
                            return true;
                        }
                    }
                }

                //cholesterol
                IEnumerable<CholesterolDataManager.CholesterolItem> objCholesterolList = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetLatestNItems(1).Reverse();
                CholesterolDataManager.CholesterolItem objCholesterolItem = null;
                if (objCholesterolList.Count() > 0)
                {
                    objCholesterolItem = objCholesterolList.First();
                }

                if (objCholesterolItem != null && objCholesterolItem.TotalCholesterol.Value.HasValue)
                {
                    TimeSpan tsCho = objCholesterolItem.EffectiveDate.Subtract(dtLastUpdated);
                    if (tsCho.TotalHours > 24)
                    {
                        RiskLevelHelper.RiskLevelCholesterol objRiskLevelCho = new RiskLevelHelper.RiskLevelCholesterol(objCholesterolItem.TotalCholesterol.Value.Value, null, null, null, true);
                        RiskLevelHelper.RiskLevelResult objRiskChoLatest = objRiskLevelCho.GetRiskLevelResult();

                        RiskLevelHelper.RiskLevelCholesterol objRiskLevelChoMLC = new RiskLevelHelper.RiskLevelCholesterol(iTotalCholesterol.Value, null, null, null, true);
                        RiskLevelHelper.RiskLevelResult objRiskChoLatestMLC = objRiskLevelChoMLC.GetRiskLevelResult();

                        if (objRiskChoLatest.RiskLevelResultType != objRiskChoLatestMLC.RiskLevelResultType)
                        {
                            return true;
                        }
                    }
                }

                //BMI
                IEnumerable<WeightDataManager.WeightItem> objWeightList = HVHelper.HVManagerPatientBase.WeightDataManager.GetLatestNItems(1).Reverse();
                WeightDataManager.WeightItem objWeightItem = null;
                if (objWeightList.Count() > 0)
                {
                    objWeightItem = objWeightList.First();
                }

                if (objWeightItem != null)
                {
                    TimeSpan tsWeight = objWeightItem.EffectiveDate.Subtract(dtLastUpdated);
                    if (tsWeight.TotalHours > 24)
                    {
                        double? dBMIHV = UserSummary.GetBMIForCurrentRecordForWeight(PatientGuids.CurrentPatientGuids, objWeightItem.CommonWeight.Value.ToPounds());

                        if (dBMI.HasValue)
                        {
                            RiskLevelHelper.RiskLevelBMI objRiskLevelBMI = new RiskLevelHelper.RiskLevelBMI(dBMIHV.Value);
                            RiskLevelHelper.RiskLevelResult objRiskBMILatest = objRiskLevelBMI.GetRiskLevelResult();

                            RiskLevelHelper.RiskLevelBMI objRiskLevelBMIMLC = new RiskLevelHelper.RiskLevelBMI(dBMI.Value);
                            RiskLevelHelper.RiskLevelResult objRiskBMILatestMLC = objRiskLevelBMIMLC.GetRiskLevelResult();

                            if (objRiskBMILatest.RiskLevelResultType != objRiskBMILatestMLC.RiskLevelResultType)
                            {
                                return true;
                            }
                        }
                    }
                }

                //Blood Lucose
                IEnumerable<BloodGlucoseDataManager.BGItem> objBGList = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetLatestNItems(1).Reverse();
                BloodGlucoseDataManager.BGItem objBGItem = null;
                if (objBGList.Count() > 0)
                {
                    objBGItem = objBGList.First();
                }

                if (objBGItem != null)
                {
                    TimeSpan tsBG = objBGItem.EffectiveDate.Subtract(dtLastUpdated);
                    if (tsBG.TotalHours > 24)
                    {
                        RiskLevelHelper.RiskLevelBloodGlucose objRiskLevelBG = new RiskLevelHelper.RiskLevelBloodGlucose(objBGItem.Value.Value);
                        RiskLevelHelper.RiskLevelResult objRiskBGLatest = objRiskLevelBG.GetRiskLevelResult();

                        RiskLevelHelper.RiskLevelBloodGlucose objRiskLevelBGMLC = new RiskLevelHelper.RiskLevelBloodGlucose(dBloodGlucose.Value);
                        RiskLevelHelper.RiskLevelResult objRiskBGLatestMLC = objRiskLevelBGMLC.GetRiskLevelResult();

                        if (objRiskBGLatest.RiskLevelResultType != objRiskBGLatestMLC.RiskLevelResultType)
                        {
                            return true;
                        }
                    }
                }

                // HbA1c
                IEnumerable<HbA1cDataManager.HbA1cItem> objHbA1cList = HVHelper.HVManagerPatientBase.HbA1cDataManager.GetLatestNItems(1).Reverse();
                HbA1cDataManager.HbA1cItem objHbA1cItem = null;

                if (objHbA1cList.Count() > 0)
                {
                    objHbA1cItem = objHbA1cList.First();
                }



            }
            return false;
        }

        public class HealthRiskIndicatorHelper
        {
            public RiskLevelTypeEnum RiskLevelTypeEnumVal
            {
                get;
                set;
            }

            public object[] ValueList
            {
                get;
                set;
            }

            public object ValueToUse
            {
                get;
                set;
            }
        }

        public enum RiskLevelTypeEnum
        {
            BP,
            BG,
            HbA1c,
            Cholesterol,
            BMI,
            Exercise
        }
        public enum RiskLevelResultTypeEnum
        {
            [Description("Excellent")]
            Excellent = 3,
            [Description("Needs Improvement")]
            NeedsImprovement = 2,
            [Description("Warning")]
            Warning = 1,
            [Description("None")]       // PJB: Needed to add this for non-active trackers, means it's not being tracked
            None = 0
        }
        /// <summary>
        /// these represen the 3 risk levels on the indicator (3 boxes), 
        /// some times 2 in that case only first & last are relevant
        /// </summary>
        public enum RiskLevelEnum
        {
            First,
            Middle,
            Last
        }

        /// <summary>
        /// the result based on an input
        /// </summary>
        public class RiskLevelResult
        {
            public double RiskLevelPercentage
            {
                get;
                set;
            }

            public RiskLevelResultTypeEnum RiskLevelResultType
            {
                get;
                set;
            }
        }

        /// <summary>
        /// base class for all risk level data types
        /// </summary>
        public abstract class RiskLevelDataBase
        {
            public abstract RiskLevelResult GetRiskLevelResult();
            public abstract RiskLevelEnum RiskLevelEnumType
            {
                get;
            }
            public abstract RiskLevelEnum RiskLevelBasedOnActualLevels
            {
                get;
            }

            internal double iPercentageL0 = 0;
            internal double iPercentageL1 = 33.3;
            internal double iPercentageL2 = 50;
            internal double iPercentageL3 = 66.6;
            internal double iPercentageL4 = 100;

            //
            // PJB: in V6 these need to be css class names
            //
            internal string strColorExcellent_Provider = "notify-green";    // "#196100";
            internal string strColorNeedsImprovement_Provider = "notify-orange";    // "#DB6024";
            internal string strColorWarning_Provider = "notify-red";    // "#B20800";

            internal string strColorExcellent = "notify-green";    // "#196100";
            internal string strColorNeedsImprovement = "notify-orange";    // "#DB6024";
            internal string strColorWarning = "notify-red";    // "#B20800";

            public virtual string ColorReading1
            {
                get
                {
                    return string.Empty;
                }
            }
            public virtual string ColorReading2
            {
                get
                {
                    return string.Empty;
                }
            }
            public virtual string ColorReading3
            {
                get
                {
                    return string.Empty;
                }
            }
            public virtual string ColorReading4
            {
                get
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// risk level calculator for bp
        /// </summary>
        public class RiskLevelBloodPressure : RiskLevelDataBase
        {
            public int Systolic
            {
                get;
                set;
            }

            public int Diastolic
            {
                get;
                set;
            }

            int iL0 = 100;
            int iL1 = 120;
            int iL2 = 140;
            int iL3 = 170;
            bool m_IsProvider = false;

            public RiskLevelBloodPressure(int iSystolic, int iDiastolic)
            {
                Systolic = iSystolic;
                Diastolic = iDiastolic;
            }

            public RiskLevelBloodPressure(int iSystolic, int iDiastolic, bool bIsProvider)
            {
                Systolic = iSystolic;
                Diastolic = iDiastolic;
                m_IsProvider = bIsProvider;
            }

            public override string ColorReading1
            {
                get
                {
                    //middle - systolic 120-139 and diastolic between 80-89.
                    //last - systolic 139 and/or Diastolic above 89.

                    GroupInfo objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.Hypertension);
                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.Systolic)
                        {
                            SystolicRuleManager srm = new SystolicRuleManager(objRule);
                            if (srm.DoesReadingQualifyForRule(Systolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorWarning_Provider : strColorWarning;
                    }

                    objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.PreHypertension);
                    objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.Systolic)
                        {
                            SystolicRuleManager srm = new SystolicRuleManager(objRule);
                            if (srm.DoesReadingQualifyForRule(Systolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override string ColorReading2
            {
                get
                {
                    //middle - systolic 120-139 and diastolic between 80-89.
                    //last - systolic 139 and/or Diastolic above 89.

                    GroupInfo objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.Hypertension);
                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.Diastolic)
                        {
                            DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
                            if (drm.DoesReadingQualifyForRule(Diastolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorWarning_Provider : strColorWarning;
                    }

                    objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.PreHypertension);
                    objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.Diastolic)
                        {
                            DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
                            if (drm.DoesReadingQualifyForRule(Diastolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override RiskLevelEnum RiskLevelEnumType
            {
                get
                {
                    //middle - systolic 120-139 and diastolic between 80-89.
                    //last - systolic 139 and/or Diastolic above 89.

                    GroupInfo objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.Hypertension);
                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.Systolic)
                        {
                            SystolicRuleManager srm = new SystolicRuleManager(objRule);
                            if (srm.DoesReadingQualifyForRule(Systolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                        else if (objRule.RuleType == RuleType.Diastolic)
                        {
                            DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
                            if (drm.DoesReadingQualifyForRule(Diastolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return RiskLevelEnum.Last;
                    }

                    objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.PreHypertension);
                    objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.Systolic)
                        {
                            SystolicRuleManager srm = new SystolicRuleManager(objRule);
                            if (srm.DoesReadingQualifyForRule(Systolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                        else if (objRule.RuleType == RuleType.Diastolic)
                        {
                            DiastolicRuleManager drm = new DiastolicRuleManager(objRule);
                            if (drm.DoesReadingQualifyForRule(Diastolic))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return RiskLevelEnum.Middle;
                    }

                    return RiskLevelEnum.First;
                }
            }

            public override RiskLevelResult GetRiskLevelResult()
            {
                RiskLevelResult objResult = new RiskLevelResult();
                RiskLevelEnum eRiskCategory = RiskLevelEnumType;
                RiskLevelEnum eRiskCategoryActual = RiskLevelBasedOnActualLevels;
                if (eRiskCategory != eRiskCategoryActual)
                {
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        objResult.RiskLevelPercentage = iPercentageL1;
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                    }
                    else if (eRiskCategory == RiskLevelEnum.Middle)
                    {
                        objResult.RiskLevelPercentage = iPercentageL1;
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                    }
                    else if (eRiskCategory == RiskLevelEnum.Last)
                    {
                        objResult.RiskLevelPercentage = iPercentageL3;
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Warning;
                    }
                }
                else
                {
                    //for a given category, we map the difference b/w the final & initial point (for a block) to a given percentage,  
                    //e.g 110 - initial , 130 - final, difference = 20, this maps to 33%
                    //do a given value - 120, maps to (33/(130 - 110)) * (120 - 110) = 16.5
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        int iVal = Systolic;
                        if (iVal < iL0)
                        {
                            iVal = iL0;
                        }
                        objResult.RiskLevelPercentage = iPercentageL1 * (iVal - iL0) / (iL1 - iL0);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                    }
                    else if (eRiskCategory == RiskLevelEnum.Middle)
                    {
                        int iVal = Systolic;
                        if (iVal < iL1)
                        {
                            iVal = iL1;
                        }
                        else if (iVal > iL2)
                        {
                            iVal = iL2;
                        }
                        //objResult.RiskLevelPercentage = iPercentageL3 * (iVal - iL0) / (iL2 - iL0);
                        objResult.RiskLevelPercentage = iPercentageL1 + iPercentageL1 * (iVal - iL1) / (iL2 - iL1);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                    }
                    else
                    {
                        int iVal = Systolic;
                        if (iVal > iL3)
                        {
                            iVal = iL3;
                        }
                        //objResult.RiskLevelPercentage = iPercentageL4 * (iVal - iL0) / (iL3 - iL0);
                        objResult.RiskLevelPercentage = iPercentageL3 + iPercentageL1 * (iVal - iL2) / (iL3 - iL2);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Warning;
                    }
                }

                return objResult;
            }

            public override RiskLevelEnum RiskLevelBasedOnActualLevels
            {
                get
                {
                    if (Systolic < iL1)
                    {
                        return RiskLevelEnum.First;
                    }
                    else if (Systolic > iL2)
                    {
                        return RiskLevelEnum.Last;
                    }
                    else
                    {
                        return RiskLevelEnum.Middle;
                    }
                }
            }
        }

        /// <summary>
        /// risk level calculator for cholesterol
        /// </summary>
        public class RiskLevelCholesterol : RiskLevelDataBase
        {
            public int TotalCholesterol
            {
                get;
                set;
            }

            public int? LDL
            {
                get;
                set;
            }

            public int? HDL
            {
                get;
                set;
            }

            public int? Triglyceride
            {
                get;
                set;
            }

            public bool IsMale
            {
                get;
                set;
            }

            int iL0 = 99;
            int iL1 = 199;
            int iL2 = 299;

            bool m_IsProvider = false;

            public RiskLevelCholesterol(int iTotalCholesterol, int? iHDL, int? iLDL, int? iTriglyceride, bool bIsMale)
            {
                TotalCholesterol = iTotalCholesterol;
                LDL = iLDL;
                HDL = iHDL;
                Triglyceride = iTriglyceride;
                IsMale = bIsMale;
            }

            public RiskLevelCholesterol(int iTotalCholesterol, int? iHDL, int? iLDL, int? iTriglyceride, bool bIsMale, bool bIsProvider)
            {
                TotalCholesterol = iTotalCholesterol;
                LDL = iLDL;
                HDL = iHDL;
                Triglyceride = iTriglyceride;
                IsMale = bIsMale;
                m_IsProvider = bIsProvider;
            }

            public override string ColorReading1
            {
                get
                {
                    //male - Total cholesterol > 199 and/or LDL >128 and/or HDL <40 and/or Triglycerides >150.
                    //femate -Total cholesterol > 199 and/or LDL >128 and/or HDL <50 and/or triglycerides >150.
                    GroupInfo objGroupInfo = null;

                    if (IsMale)
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolMale);
                    }
                    else
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolFemale);
                    }

                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.TotalCholesterol)
                        {
                            TotalCholesterolRuleManager trm = new TotalCholesterolRuleManager(objRule);
                            if (trm.DoesReadingQualifyForRule(TotalCholesterol))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override string ColorReading2
            {
                get
                {
                    //male - Total cholesterol > 199 and/or LDL >128 and/or HDL <40 and/or Triglycerides >150.
                    //femate -Total cholesterol > 199 and/or LDL >128 and/or HDL <50 and/or triglycerides >150.
                    GroupInfo objGroupInfo = null;

                    if (IsMale)
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolMale);
                    }
                    else
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolFemale);
                    }

                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.HDL)
                        {
                            HDLRuleManager hrm = new HDLRuleManager(objRule);
                            if (HDL.HasValue && hrm.DoesReadingQualifyForRule(HDL.Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override string ColorReading3
            {
                get
                {
                    //male - Total cholesterol > 199 and/or LDL >128 and/or HDL <40 and/or Triglycerides >150.
                    //femate -Total cholesterol > 199 and/or LDL >128 and/or HDL <50 and/or triglycerides >150.
                    GroupInfo objGroupInfo = null;

                    if (IsMale)
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolMale);
                    }
                    else
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolFemale);
                    }

                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.LDL)
                        {
                            LDLRuleManager lrm = new LDLRuleManager(objRule);
                            if (LDL.HasValue && lrm.DoesReadingQualifyForRule(LDL.Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override string ColorReading4
            {
                get
                {
                    //male - Total cholesterol > 199 and/or LDL >128 and/or HDL <40 and/or Triglycerides >150.
                    //femate -Total cholesterol > 199 and/or LDL >128 and/or HDL <50 and/or triglycerides >150.
                    GroupInfo objGroupInfo = null;

                    if (IsMale)
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolMale);
                    }
                    else
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolFemale);
                    }

                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.Triglyceride)
                        {
                            TriglycerideRuleManager trm = new TriglycerideRuleManager(objRule);
                            if (Triglyceride.HasValue && trm.DoesReadingQualifyForRule(Triglyceride.Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override RiskLevelEnum RiskLevelEnumType
            {
                get
                {
                    //male - Total cholesterol > 199 and/or LDL >128 and/or HDL <40 and/or Triglycerides >150.
                    //femate -Total cholesterol > 199 and/or LDL >128 and/or HDL <50 and/or triglycerides >150.
                    GroupInfo objGroupInfo = null;

                    if (IsMale)
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolMale);
                    }
                    else
                    {
                        objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.CholesterolFemale);
                    }

                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.TotalCholesterol)
                        {
                            TotalCholesterolRuleManager trm = new TotalCholesterolRuleManager(objRule);
                            if (trm.DoesReadingQualifyForRule(TotalCholesterol))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                        else if (objRule.RuleType == RuleType.HDL)
                        {
                            HDLRuleManager hrm = new HDLRuleManager(objRule);
                            if (HDL.HasValue && hrm.DoesReadingQualifyForRule(HDL.Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                        else if (objRule.RuleType == RuleType.LDL)
                        {
                            LDLRuleManager lrm = new LDLRuleManager(objRule);
                            if (LDL.HasValue && lrm.DoesReadingQualifyForRule(LDL.Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                        else if (objRule.RuleType == RuleType.Triglyceride)
                        {
                            TriglycerideRuleManager trm = new TriglycerideRuleManager(objRule);
                            if (Triglyceride.HasValue && trm.DoesReadingQualifyForRule(Triglyceride.Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return RiskLevelEnum.Last;
                    }

                    return RiskLevelEnum.First;
                }
            }

            public override RiskLevelResult GetRiskLevelResult()
            {
                RiskLevelResult objResult = new RiskLevelResult();
                RiskLevelEnum eRiskCategory = RiskLevelEnumType;
                RiskLevelEnum eRiskCategoryActual = RiskLevelBasedOnActualLevels;
                if (eRiskCategory != eRiskCategoryActual)
                {
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                        objResult.RiskLevelPercentage = iPercentageL1;
                    }
                    else
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                        objResult.RiskLevelPercentage = iPercentageL3;
                    }
                }
                else
                {
                    //for a given category, we map the difference b/w the final & initial point (for a block) to a given percentage,  
                    //e.g 110 - initial , 130 - final, difference = 20, this maps to 33%
                    //do a given value - 120, maps to (33/(130 - 110)) * (120 - 110) = 16.5
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        int iVal = TotalCholesterol;
                        if (iVal < iL0)
                        {
                            iVal = iL0;
                        }
                        objResult.RiskLevelPercentage = iPercentageL2 * (iVal - iL0) / (iL1 - iL0);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                    }
                    else
                    {
                        int iVal = TotalCholesterol;
                        if (iVal > iL2)
                        {
                            objResult.RiskLevelPercentage = iPercentageL4;
                        }
                        else
                        {
                            //objResult.RiskLevelPercentage = iPercentageL4 * (iVal - iL0) / (iL2 - iL0);
                            //objResult.RiskLevelPercentage = iPercentageL4 * iVal / iL2;
                            objResult.RiskLevelPercentage = 50 + iPercentageL2 * (iVal - iL1) / (iL2 - iL1);
                        }
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                    }
                }

                return objResult;
            }

            public override RiskLevelEnum RiskLevelBasedOnActualLevels
            {
                get
                {
                    if (TotalCholesterol <= iL1)
                    {
                        return RiskLevelEnum.First;
                    }
                    else
                    {
                        return RiskLevelEnum.Last;
                    }
                }
            }
        }

        public class RiskLevelHbA1c : RiskLevelDataBase
        {
            public double Value
            {
                get;
                set;
            }

            int iL0 = 70;
            int iL1 = 125;
            int iL2 = 300;

            public RiskLevelHbA1c(double iValue)
            {
                Value = iValue;
            }

            public override RiskLevelEnum RiskLevelEnumType
            {
                get
                {
                    bool bQualified = false;

                    if (bQualified)
                    {
                        return RiskLevelEnum.Last;
                    }

                    return RiskLevelEnum.First;
                }
            }

            public override RiskLevelResult GetRiskLevelResult()
            {
                RiskLevelResult objResult = new RiskLevelResult();

                return objResult;
            }

            public override RiskLevelEnum RiskLevelBasedOnActualLevels
            {
                get
                {
                    if (Value <= iL1)
                    {
                        return RiskLevelEnum.First;
                    }
                    else
                    {
                        return RiskLevelEnum.Last;
                    }
                }
            }
        }

        /// <summary>
        /// risk level calculator for glucose
        /// </summary>
        public class RiskLevelBloodGlucose : RiskLevelDataBase
        {
            public double Value
            {
                get;
                set;
            }

            int iL0 = 70;
            int iL1 = 125;
            int iL2 = 300;

            bool m_IsProvider = false;

            public RiskLevelBloodGlucose(double iValue)
            {
                Value = iValue;
            }

            public RiskLevelBloodGlucose(double iValue, bool bIsProvider)
            {
                Value = iValue;
                m_IsProvider = bIsProvider;
            }

            public override string ColorReading1
            {
                get
                {
                    //last - blood glucose reading is > 125.
                    GroupInfo objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.Diabetes);
                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.BloodGlucose)
                        {
                            BloodGlucoseRuleManager brm = new BloodGlucoseRuleManager(objRule);
                            if (brm.DoesReadingQualifyForRule(Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override RiskLevelEnum RiskLevelEnumType
            {
                get
                {
                    //last - blood glucose reading is > 125.
                    GroupInfo objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.Diabetes);
                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.BloodGlucose)
                        {
                            BloodGlucoseRuleManager brm = new BloodGlucoseRuleManager(objRule);
                            if (brm.DoesReadingQualifyForRule(Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return RiskLevelEnum.Last;
                    }

                    return RiskLevelEnum.First;
                }
            }

            public override RiskLevelResult GetRiskLevelResult()
            {
                RiskLevelResult objResult = new RiskLevelResult();
                RiskLevelEnum eRiskCategory = RiskLevelEnumType;
                RiskLevelEnum eRiskCategoryActual = RiskLevelBasedOnActualLevels;
                if (eRiskCategory != eRiskCategoryActual)
                {
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                        objResult.RiskLevelPercentage = iPercentageL1;
                    }
                    else
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                        objResult.RiskLevelPercentage = iPercentageL3;
                    }
                }
                else
                {
                    //for a given category, we map the difference b/w the final & initial point (for a block) to a given percentage,  
                    //e.g 110 - initial , 130 - final, difference = 20, this maps to 33%
                    //do a given value - 120, maps to (33/(130 - 110)) * (120 - 110) = 16.5
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        double iVal = Value;
                        if (iVal < iL0)
                        {
                            iVal = iL0;
                        }
                        objResult.RiskLevelPercentage = iPercentageL2 * (iVal - iL0) / (iL1 - iL0);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                    }
                    else
                    {
                        double iVal = Value;
                        if (iVal > iL2)
                        {
                            objResult.RiskLevelPercentage = iPercentageL4;
                        }
                        else
                        {
                            //objResult.RiskLevelPercentage = 50 + iPercentageL4 * (iVal - iL1) / (iL2 - iL1);
                            //objResult.RiskLevelPercentage = iPercentageL4 * iVal / iL2;
                            objResult.RiskLevelPercentage = 50 + iPercentageL2 * (iVal - iL1) / (iL2 - iL1);
                        }
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                    }
                }

                return objResult;
            }

            public override RiskLevelEnum RiskLevelBasedOnActualLevels
            {
                get
                {
                    if (Value <= iL1)
                    {
                        return RiskLevelEnum.First;
                    }
                    else
                    {
                        return RiskLevelEnum.Last;
                    }
                }
            }
        }

        /// <summary>
        /// risk level calculator for bmi
        /// </summary>
        public class RiskLevelBMI : RiskLevelDataBase
        {
            public double Value
            {
                get;
                set;
            }

            int iL0 = 15;
            int iL1 = 25;
            int iL2 = 29;
            int iL3 = 40;
            bool m_IsProvider = false;

            public RiskLevelBMI(double iValue)
            {
                Value = iValue;
            }

            public RiskLevelBMI(double iValue, bool bIsProvider)
            {
                Value = iValue;
                m_IsProvider = bIsProvider;
            }

            public override string ColorReading1
            {
                get
                {
                    //middle - systolic 120-139 and diastolic between 80-89.
                    //last - systolic 139 and/or Diastolic above 89.

                    GroupInfo objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.BMIObese);
                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.BMI)
                        {
                            BMIRuleManager brm = new BMIRuleManager(objRule);
                            if (brm.DoesReadingQualifyForRule(Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorWarning_Provider : strColorWarning;
                    }

                    objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.BMIOverweight);
                    objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.BMI)
                        {
                            BMIRuleManager brm = new BMIRuleManager(objRule);
                            if (brm.DoesReadingQualifyForRule(Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return m_IsProvider ? strColorNeedsImprovement_Provider : strColorNeedsImprovement;
                    }

                    return m_IsProvider ? strColorExcellent_Provider : strColorExcellent;
                }
            }

            public override RiskLevelEnum RiskLevelEnumType
            {
                get
                {
                    //middle - systolic 120-139 and diastolic between 80-89.
                    //last - systolic 139 and/or Diastolic above 89.

                    GroupInfo objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.BMIObese);
                    GroupRule objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bool bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.BMI)
                        {
                            BMIRuleManager brm = new BMIRuleManager(objRule);
                            if (brm.DoesReadingQualifyForRule(Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return RiskLevelEnum.Last;
                    }

                    objGroupInfo = Heart360Global.Main.GetGroupByTypeID(GroupType.BMIOverweight);
                    objGroupRule = GroupRule.ParseGroupRuleXml(objGroupInfo.GroupRule);
                    bQualified = false;
                    foreach (Rule objRule in objGroupRule.RuleList)
                    {
                        if (objRule.RuleType == RuleType.BMI)
                        {
                            BMIRuleManager brm = new BMIRuleManager(objRule);
                            if (brm.DoesReadingQualifyForRule(Value))
                            {
                                bQualified = true;
                                break;
                            }
                        }
                    }

                    if (bQualified)
                    {
                        return RiskLevelEnum.Middle;
                    }

                    return RiskLevelEnum.First;
                }
            }

            public override RiskLevelResult GetRiskLevelResult()
            {
                RiskLevelResult objResult = new RiskLevelResult();
                RiskLevelEnum eRiskCategory = RiskLevelEnumType;
                RiskLevelEnum eRiskCategoryActual = RiskLevelBasedOnActualLevels;
                if (eRiskCategory != eRiskCategoryActual)
                {
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                        objResult.RiskLevelPercentage = iPercentageL1;
                    }
                    else if (eRiskCategory == RiskLevelEnum.Middle)
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                        objResult.RiskLevelPercentage = iPercentageL1;
                    }
                    else if (eRiskCategory == RiskLevelEnum.Last)
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Warning;
                        objResult.RiskLevelPercentage = iPercentageL3;
                    }
                }
                else
                {
                    //for a given category, we map the difference b/w the final & initial point (for a block) to a given percentage,  
                    //e.g 110 - initial , 130 - final, difference = 20, this maps to 33%
                    //do a given value - 120, maps to (33/(130 - 110)) * (120 - 110) = 16.5
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        double iVal = Value;
                        if (iVal < iL0)
                        {
                            iVal = iL0;
                        }
                        //objResult.RiskLevelPercentage = iPercentageL1 * (iVal - iL0) / (iL1 - iL0);
                        objResult.RiskLevelPercentage = iPercentageL1 * (iVal - iL0) / (iL1 - iL0);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                    }
                    else if (eRiskCategory == RiskLevelEnum.Middle)
                    {
                        double iVal = Value;
                        if (iVal < iL1)
                        {
                            iVal = iL1;
                        }
                        else if (iVal > iL2)
                        {
                            iVal = iL2;
                        }
                        //objResult.RiskLevelPercentage = iPercentageL3 * (iVal - iL0) / (iL2 - iL0);
                        objResult.RiskLevelPercentage = iPercentageL1 + iPercentageL1 * (iVal - iL1) / (iL2 - iL1);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                    }
                    else
                    {
                        double iVal = Value;
                        if (iVal > iL3)
                        {
                            iVal = iL3;
                        }
                        //objResult.RiskLevelPercentage = iPercentageL4 * (iVal - iL0) / (iL3 - iL0);
                        objResult.RiskLevelPercentage = iPercentageL3 + iPercentageL1 * (iVal - iL2) / (iL3 - iL2);
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Warning;
                    }
                }

                return objResult;
            }

            public override RiskLevelEnum RiskLevelBasedOnActualLevels
            {
                get
                {
                    if (Value < iL1)
                    {
                        return RiskLevelEnum.First;
                    }
                    else if (Value > iL2)
                    {
                        return RiskLevelEnum.Last;
                    }
                    else
                    {
                        return RiskLevelEnum.Middle;
                    }
                }
            }
        }

        /// <summary>
        /// risk level calculator for exercise
        /// </summary>
        public class RiskLevelExercise : RiskLevelDataBase
        {
            public double Value
            {
                get;
                set;
            }

            int iL0 = 0;
            int iL1 = 120;
            int iL2 = 240;

            public RiskLevelExercise(double iDuration)
            {
                Value = iDuration;
            }

            public override RiskLevelEnum RiskLevelEnumType
            {
                get
                {
                    // Middle = needs improvement, First = excellent
                    return (Value < iL1) ? RiskLevelEnum.Middle : RiskLevelEnum.First ;

                    //return (Value < iL1) ? RiskLevelEnum.Last : ((Value > iL2) ? RiskLevelEnum.First : RiskLevelEnum.Middle);
                }
            }

            public override RiskLevelResult GetRiskLevelResult()
            {
                RiskLevelResult objResult = new RiskLevelResult();
                RiskLevelEnum eRiskCategory = RiskLevelEnumType;
                RiskLevelEnum eRiskCategoryActual = RiskLevelBasedOnActualLevels;
                if (eRiskCategory != eRiskCategoryActual)
                {
                    if (eRiskCategory == RiskLevelEnum.First)
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                        objResult.RiskLevelPercentage = iPercentageL3;
                    }
                    else // if (eRiskCategory == RiskLevelEnum.Middle)
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                        objResult.RiskLevelPercentage = iPercentageL1;
                    }
                }
                else
                {
                    //for a given category, we map the difference b/w the final & initial point (for a block) to a given percentage,  
                    //e.g 110 - initial , 130 - final, difference = 20, this maps to 33%
                    //do a given value - 120, maps to (33/(130 - 110)) * (120 - 110) = 16.5
                    double iVal = Value;
                    if (iVal < iL1)    // Needs improvement
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.NeedsImprovement;
                        objResult.RiskLevelPercentage = iPercentageL1;
                    }
                    else
                    {
                        objResult.RiskLevelResultType = RiskLevelResultTypeEnum.Excellent;
                        objResult.RiskLevelPercentage = iPercentageL3;
                    }
                }

                return objResult;
            }

            public override RiskLevelEnum RiskLevelBasedOnActualLevels
            {
                get
                {
                    return (Value < iL1) ? RiskLevelEnum.Middle : RiskLevelEnum.First ;
                }
            }
        }
    }
}
