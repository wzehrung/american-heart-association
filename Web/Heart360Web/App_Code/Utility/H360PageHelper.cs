﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AHAHelpContent;
using AHAAPI;
using Microsoft.Health.Web;
using Microsoft.Health;
using GRCBase;

namespace Heart360Web
{
    /// <summary>
    /// All application pages where the user is logged-in should derrive from this page
    /// </summary>
    public abstract class H360PageHelper : HealthServicePage
    {
        /// <summary>
        /// Note: This variable is loaded during the 1st OnPreLoad event
        /// of a Health Server page.  If it is needed prior to that,
        /// it would have to be manually set by calling the ApplicaitonConnection
        /// constructor and supplying it with the application-id (not hard).
        /// 
        /// The current implementation should serve most purposes since we 
        /// don't need any dictionary vocabularies prior to the user viewing
        /// a logged-in webpage.
        /// </summary>
        //private static ApplicationConnection s_DictionaryConnection;

        private static ApplicationConnection s_DictionaryConnection
        {
            get
            {
                return Microsoft.Health.Web.WebApplicationUtilities.DictionaryConnection;
            }
            set
            {
                s_DictionaryConnection = value;
            }
        }

        public Guid PersonGUID
        {
            get
            {
                return this.PersonInfo.PersonId;
            }
        }

        public Guid RecordGUID
        {
            get
            {
                return this.PersonInfo.SelectedRecord.Id;
            }
        }


        public bool UpdatedOfflinePersonGUID
        {
            get;
            set;
        }

        public UserIdentityContext UserIdentityContextDetails
        {
            get
            {
                return SessionInfo.GetUserIdentityContext();
            }
        }

        AHAHelpContent.AHAUser __AHAUser = null;
        AHAHelpContent.AHAUser AHAUser
        {
            get
            {
                if (__AHAUser == null)
                {
                    return AHAHelpContent.AHAUser.GetAHAUserByPersonGUID(this.PersonInfo.PersonId);
                }

                return __AHAUser;
            }
        }

        AHAHelpContent.Patient __Patient = null;
        public AHAHelpContent.Patient Patient
        {
            get
            {
                if (__Patient == null)
                {
                    __Patient = AHAHelpContent.Patient.FindByUserHealthRecordGUID(this.PersonInfo.SelectedRecord.Id);
                }

                return __Patient;
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            if (s_DictionaryConnection == null)
            {
                s_DictionaryConnection = this.DictionaryConnection;
            }
        }

        protected void Page_PreInit(object sender, System.EventArgs e)
        {
            //if the query parameter 'PrinterFriendly' is not empty and is equal to 1 then 'PrinterFriendly.Master' is used
            if (!string.IsNullOrEmpty(Request["PrinterFriendly"]) && (Request["PrinterFriendly"] == "1"))
            {
                this.MasterPageFile = "~/MasterPages/PrinterFriendly.Master";
            }
        }

        /// <summary>
        /// Called before we do a page load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);

            string strFullUrl = GRCBase.UrlUtility.FullCurrentPageUrl.ToLower();

            if (strFullUrl.EndsWith("error.aspx")
                || strFullUrl.EndsWith("filenotfound.aspx")
                || this.PersonInfo == null)
            {
                return;
            }

            //Make sure we are holding a dictionary connection
            if (s_DictionaryConnection == null)
            {
                s_DictionaryConnection = this.DictionaryConnection;
            }

            //===================================================================
            //This block of code should only get executed once, when the user logs in
            //===================================================================
            //We need to make sure we've got a unique user id cached so
            //that we can store files in a subdirectory with this name
            UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
            if (userIdentityContext == null)
            {
                //SessionInfo.ClearLoggedInUserState();
                DBContext dbContext = null;
                bool bException = false;
                try
                {
                    dbContext = DBContext.GetDBContext();

                    //If the user selects another record and then logs out, 
                    //the next time the user logs in, we make sure that his selected record is the same as the one
                    //that he selected before logging out.
                    //a try catch is here because the user can potentially delete this record from elsewhere.
                    if (AHAUser != null && AHAUser.CurrentSelectedRecordGUID.HasValue && AHAUser.CurrentSelectedRecordGUID.Value != this.PersonInfo.SelectedRecord.Id && AHAAPI.SessionInfo.GetBPCenterItemFromSession() == null)
                    {
                        try
                        {
                            Guid guidPreviousSelectedRecordId = this.PersonInfo.SelectedRecord.Id;
                            this.SetSelectedRecord(this.PersonInfo.AuthorizedRecords[AHAUser.CurrentSelectedRecordGUID.Value]);
                            Guid guidPersonal = HVHelper.GetPersonalItemGUID(false);
                            if (guidPersonal.Equals(Guid.Empty))
                            {
                                this.SetSelectedRecord(this.PersonInfo.AuthorizedRecords[guidPreviousSelectedRecordId]);
                            }
                        }
                        catch (Exception ex)
                        {
                            //GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
                        }
                    }

                    ChangeLoggedInUserAndSelectedRecord();

                    //See if there is any derrived class behavior
                    _OnUserAppLogIn();
                }
                catch
                {
                    bException = true;
                    SessionInfo.ClearLoggedInUserState();
                    throw;
                }
                finally
                {
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
            }

            //Make sure that the expected user is the one logged in...
            bool successValidatingUser = ValidateExistingUserContext();
            if (!successValidatingUser)
            {
                System.Diagnostics.Debug.Assert(false, "Unexpected user context");
            }

            //If the user has not agreed to the user agreement / terms of use we show them the new terms screen
            userIdentityContext = SessionInfo.GetUserIdentityContext();
            string strRequestURLLower = Request["URL"].ToLower();
            if (userIdentityContext != null && !userIdentityContext.HasUserAgreedToNewTermsAndConditions 
                && !strRequestURLLower.Contains(WebAppNavigation.H360.PAGE_NEW_TERMS.ToLower())) //allow loading of terms screen!
            {
                string strHttpReferer = Request["HTTP_REFERER"];
                if (!string.IsNullOrEmpty(strHttpReferer) && strHttpReferer.ToLower().IndexOf("ss=1") != -1)
                {
                    HttpContext.Current.Response.Redirect(WebAppNavigation.H360.PAGE_NEW_TERMS + "?ss=1");
                }
                else
                {
                    // PJB: Now with third party login, terms and conditions may not necessarily be on Patient Page Consent
                    AHAAPI.ThirdPartyContext tpc = AHAAPI.SessionInfo.GetThirdPartyContext();
                    if (tpc == null)
                    {
                        HttpContext.Current.Response.Redirect(WebAppNavigation.H360.PAGE_NEW_TERMS);
                    }
                }
            }
            object a = AHAUser;
            if (userIdentityContext != null && !userIdentityContext.LanguageID.HasValue)
            {
                userIdentityContext.LanguageID = AHAUser.DefaultLanguageID;
                userIdentityContext.LanguageLocale = AHAUser.LanguageLocale;
                LanguageHelper.SetLocaleAndCulture(userIdentityContext.LanguageLocale);
            }
        }



        /// <summary>
        /// This is called by our base class when the user first logs in.
        /// If the user has not agreed to the consent & terms of use we show them the consent screen
        ///other wise if the loggedin use is a patient, we take the use to the patient home page
        /// </summary>
        private void _OnUserAppLogIn()
        {
            HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;

            /// The user info needs to be added to the AHAUser table.
            /// For this we call the CreateAHAUser function which inserts the user record to table if the 
            /// user doesn't exist otherwise it would not do anything.
            /// PersonGUID --> SessionInfo.GetUserIdentityContext().UserId
            /// SelfRecordGUID --> SessionInfo.GetUserIdentityContext().SelfRecordId
            ///objSelfPersonalItemGUID - > self record personal object GUID
            ///objPersonalItemGUID - > selected record personal object GUID
            System.Guid? objSelfPersonalItemGUID = null;


            UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
            System.Guid? objPersonalItemGUID = userIdentityContext.PersonalItemGUID;
            //even though we can have a selfrecord guid as a non-empty value, 
            //we set the self personal object guid as null when passing these values to
            // the createAHAUser function, because to create a healthrecord,
            //we can either pass both the self record guid & personal item guid as null or
            //both the values as non-null because in the db personal item GUID has more
            //importance then the health record guid and the personal item guid does not vary across applications
            System.Guid? objSelfRecordGUID = null;

            if (userIdentityContext.SelfRecordId.HasValue && userIdentityContext.RecordId == userIdentityContext.SelfRecordId.Value)
            {
                objSelfPersonalItemGUID = objPersonalItemGUID;
                objSelfRecordGUID = userIdentityContext.SelfRecordId;
            }

            AHAUserLoginDetails.LoginDate structLogin = AHAUserLoginDetails.GetUserLastLoggedInDate(userIdentityContext.UserId);
            if (structLogin.NumberOfLogins > 0)
            {
                userIdentityContext.IsUserFirstTimeLoggedIn = false;
            }
            else
            {
                userIdentityContext.IsUserFirstTimeLoggedIn = true;
            }

            AHAUser au = AHAUser.CreateAHAUser(objSelfPersonalItemGUID, userIdentityContext.UserId, objSelfRecordGUID, AHAAPI.H360RequestContext.GetContext().SelectedLanguage.LanguageID);

            

            //Sometimes the selected record may not be the same as the self record id, if they are different we need to create the healthrecord row for the selected record
            AHAHelpContent.Patient.CreateHealthRecord(objPersonalItemGUID, userIdentityContext.RecordId, userIdentityContext.UserId);

            //check if offline is configured for this user & healthrecord

            if (!UpdatedOfflinePersonGUID)
            {
                bool bHasGivenOfflineAccess = HVHelper.HasPatientGivenOfflineAccessToApp(userIdentityContext.UserId, userIdentityContext.RecordId);
                if (bHasGivenOfflineAccess)
                {
                    //update personguid for that healthrecord
                    AHAHelpContent.Patient.UpdateOfflinePersonGuid(objPersonalItemGUID.Value, userIdentityContext.RecordId, userIdentityContext.UserId);
                }
            }

            /// We need to track the date/time when the user has logged into the appplication as well as
            /// the number of times the user has logged into the application.
            /// We track this by inserting an entry into the AHAUserLoginDetails table each time the user logs in.
            //
            // PJB: Check to see if we need to specify a host device with new user
            AHAAPI.ThirdPartyContext tpc = SessionInfo.GetThirdPartyContext();
            if (tpc != null)
            {
                AHAUser.SetHostDevice(au.PersonGUID, tpc.DeviceName, tpc.DeviceId);
                AHAUserLoginDetails.CreateAHAUserLoginDetails(userIdentityContext.UserId, tpc.DeviceName, tpc.DeviceId );
            }
            else
            {
                AHAUserLoginDetails.CreateAHAUserLoginDetails(userIdentityContext.UserId, string.Empty, string.Empty );
            }

            if (Patient != null)
            {
                userIdentityContext.PatientID = Patient.UserHealthRecordID;
            }

            userIdentityContext.HasUserConsented = AHAUser.Consent;
            userIdentityContext.HasUserAgreedToTermsAndConditions = AHAUser.TermsAndConditions;
            userIdentityContext.HasUserAgreedToNewTermsAndConditions = AHAUser.NewTermsAndConditions;
            userIdentityContext.HasCompletedProfile = AHAUser.IsPersonalInfoCompleted;
            Campaign tempCampaign = Campaign.FindCampaignByPatientID((int)userIdentityContext.PatientID, AHAAPI.H360RequestContext.GetContext().SelectedLanguage.LanguageID);
            if (tempCampaign != null && tempCampaign.IsActive)
            {
                userIdentityContext.CampaignID = tempCampaign.CampaignID;
                userIdentityContext.CampaignUrl = tempCampaign.CampaignURL;
            }

            //check for new user login via campaign URL
            int? iCampaignID = H360Utility.GetCurrentCampaignID();
            if (iCampaignID.HasValue && userIdentityContext.IsUserFirstTimeLoggedIn)
            {
                AHAHelpContent.Campaign.UpdateCampaignForPatient(userIdentityContext.PersonalItemGUID, iCampaignID.Value);
            }
            //H360Utility.ResetCampaignSession();
        }

        /// <summary>
        /// Called when a page is served and either:
        /// (1) the user 1st logs in, or 
        /// (2)when the user has just changed the active record to point to a new user
        /// </summary>
        private void ChangeLoggedInUserAndSelectedRecord()
        {
            bool bIsUserFirstTimeLoggedIn = false;
            bool bHasUserAgreedToTermsAndConditions = false;
            bool bHasUserAgreedToNewTermsAndConditions = false;
            bool bHasUserConsented = false;
            int? iCampaignID = null;
            string sCampaignUrl = null;

            UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
            if (userIdentityContext != null)
            {
                bIsUserFirstTimeLoggedIn = userIdentityContext.IsUserFirstTimeLoggedIn;
                bHasUserAgreedToTermsAndConditions = userIdentityContext.HasUserAgreedToTermsAndConditions;
                bHasUserConsented = userIdentityContext.HasUserConsented;
                bHasUserAgreedToNewTermsAndConditions = userIdentityContext.HasUserAgreedToNewTermsAndConditions;

                if (userIdentityContext.CampaignID != null)
                {
                    iCampaignID = (int)userIdentityContext.CampaignID;
                }
                sCampaignUrl = userIdentityContext.CampaignUrl;
            }

            SessionInfo.SetUserIdentityContext(
                  this.PersonInfo.PersonId,
                  this.PersonInfo.SelectedRecord.Id,
                  UserSelfRecordID,
                  HVHelper.GetPersonalItemGUID(true),
                  bHasUserAgreedToTermsAndConditions,
                  false,
                  bHasUserConsented,
                  bHasUserAgreedToNewTermsAndConditions,
                  iCampaignID,
                  sCampaignUrl);

            userIdentityContext = SessionInfo.GetUserIdentityContext();
            userIdentityContext.IsUserFirstTimeLoggedIn = bIsUserFirstTimeLoggedIn;

            if (AHAUser != null)
            {
                AHAHelpContent.Patient.CreateHealthRecord(userIdentityContext.PersonalItemGUID, userIdentityContext.RecordId, userIdentityContext.UserId);
                bool bHasGivenOfflineAccess = HVHelper.HasPatientGivenOfflineAccessToApp(userIdentityContext.UserId, userIdentityContext.RecordId);
                if (bHasGivenOfflineAccess)
                {
                    //update personguid for that healthrecord
                    AHAHelpContent.Patient.UpdateOfflinePersonGuid(userIdentityContext.PersonalItemGUID, userIdentityContext.RecordId, userIdentityContext.UserId);
                    UpdatedOfflinePersonGUID = true;
                }
            }

            if (!userIdentityContext.IsUserFirstTimeLoggedIn)
            {
                userIdentityContext.HasUserAgreedToTermsAndConditions = true;
                userIdentityContext.HasUserConsented = true;
                userIdentityContext.HasUserAgreedToNewTermsAndConditions = true;
            }
        }

        /// <summary>
        /// This function verifies that the logged in user is who think they are
        /// </summary>
        /// <returns>
        /// True = the user was who we thought they were, and either the selected record matched or we were able to change records
        ///</returns>
        private bool ValidateExistingUserContext()
        {
            UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();

            bool isUserAsExpected = false;

            if (userIdentityContext != null)
                isUserAsExpected = userIdentityContext.ValidateUserIdentity(this.PersonInfo.PersonId);

            //If the user is not who we expected; we should immediatly clear out all session state
            //and start over again.
            //
            //This could happen if, while a user is logged in, they go "back" in the browser and 
            //log in again with another account.  This is somewhat a degenerate case, but worth 
            //checking for.
            if (!isUserAsExpected)
            {
                MSHealthSignOff("");
            }

            //If the record has changed, we should clear the Image and Data caches.
            bool isRecordAsExpected = false;

            if (userIdentityContext != null)
                isRecordAsExpected = SessionInfo.GetUserIdentityContext().ValidateRecord(
                                        this.PersonInfo.SelectedRecord.Id);

            if (!isRecordAsExpected)
            {
                //Dump the cached record data
                //LocalCacheForHealthRecord.DropCurrentUserDataCache();

                //set db sync to false
                //SessionInfo.SetDBSynchronized(false);
                //Update the session state data that corresponds to our user
                ChangeLoggedInUserAndSelectedRecord();

                userIdentityContext = SessionInfo.GetUserIdentityContext();

                if (Patient != null)
                {
                    userIdentityContext.PatientID = Patient.UserHealthRecordID;
                }
            }

            return isUserAsExpected;
        }

        /// <summary>
        /// Gets the self record id for the user
        /// </summary>
        internal Guid? UserSelfRecordID
        {
            get
            {
                //A person may not have a self record id,but he should have a select record id
                Guid? gSelfRecordId = null;
                try
                {
                    HealthRecordInfo objHealthRecordInfo = this.PersonInfo.GetSelfRecord();
                    if (objHealthRecordInfo != null)
                    {
                        gSelfRecordId = objHealthRecordInfo.Id;
                    }
                }
                catch
                {
                    //don't do anything
                }
                return gSelfRecordId;
            }
        }

        /// <summary>
        /// Does a complete sign-off.  Throws out all the applicaiton state
        /// and asks the MSHealth server to revoke any logged in access.
        /// </summary>
        /// <param name="callingPage"></param>
        public void MSHealthSignOff(string pActionUrl)
        {

            //=========================================================
            //Throw out all the application state we are holding
            //=========================================================
            SessionInfo.ClearLoggedInUserState();

            //=================================================================
            //Execute the sign-off
            //=================================================================
            this.SignOut(pActionUrl);
        }
    }
}
