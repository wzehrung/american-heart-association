﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Web
{
    public class PolkaDataProcessor
    {
        public static void Process()
        {
            lock (HVHelper.HVManagerPatientBase)
            {
                //check if the sync operation has already been performed or not.
                if (HVHelper.HVManagerPatientBase.IsPolkaDataProcessed)
                    return;

                AHAHelpContent.Patient objPatient = (System.Web.HttpContext.Current.Handler as H360PageHelper).Patient;
                if (objPatient.OfflinePersonGUID.HasValue && objPatient.OfflineHealthRecordGUID.HasValue)
                {
                    HVHelper.HVManagerPatientBase.IsPolkaDataProcessed = true;
                    return;
                }
                
             
                PolkaDataManager.Heart360PolkaManager.ProcessForUser((System.Web.HttpContext.Current.Handler as H360PageHelper).PersonGUID, true, objPatient);

                HVHelper.HVManagerPatientBase.IsPolkaDataProcessed = true;
            }
        }
    }
}
