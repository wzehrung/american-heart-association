﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Heart360Web
{
    [XmlRoot("SummaryOptions")]
    public class SummaryOptionList
    {
        private List<SummaryOption> list;

        public SummaryOptionList()
        {
            list = new List<SummaryOption>();
        }

        [XmlElement("Summary")]
        public List<SummaryOption> Items
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
            }
        }
    }

    public class SummaryOption
    {
        public string Type
        {
            get;
            set;
        }

        public string Position
        {
            get;
            set;
        }

        public string IsVisible
        {
            get;
            set;
        }

        /// <summary>
        /// Deserialize List
        /// </summary>
        /// <param name="strXml"></param>
        /// <returns></returns>
        public static SummaryOptionList GetObject(string strXml)
        {
            if (string.IsNullOrEmpty(strXml))
                return null;

            SummaryOptionList list = null;

            XmlSerializer serializer = new XmlSerializer(typeof(SummaryOptionList));
            using (StringReader reader = new StringReader(strXml))
            {
                list = (SummaryOptionList)serializer.Deserialize(reader);
                reader.Close();
            }

            return list;
        }

        /// <summary>
        /// Serialize List
        /// </summary>
        /// <returns></returns>
        public static string GetXml(SummaryOptionList list)
        {
            if (list == null || list.Items.Count == 0)
                return null;

            XmlSerializer serializer = new XmlSerializer(list.GetType());

            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.OmitXmlDeclaration = true;
            string strXML = string.Empty;
            using (StringWriter stringWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, writerSettings))
                {
                    serializer.Serialize(xmlWriter, list);
                    xmlWriter.Close();
                }

                strXML = stringWriter.ToString();
                stringWriter.Close();
            }
            return strXML;
        }
    }
}
