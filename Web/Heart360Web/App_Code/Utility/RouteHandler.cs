﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Compilation;
using System.Web.UI;

namespace Heart360Web
{

    /// <summary>
    /// Implements IHttpHandlers and contains uniqueName
    /// </summary>
    public interface IDisplay : IHttpHandler
    {

    }

    /// <summary>
    /// Summary description for RouteHandler
    /// </summary>
    public class RouteHandler : IRouteHandler
    {
        public RouteHandler()
        {

        }
        public RouteHandler(string virtualPath)
        {
            _virtualPath = virtualPath;
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            var display = BuildManager.CreateInstanceFromVirtualPath(
                            _virtualPath, typeof(Page)) as IDisplay;
            return display;
        }

        string _virtualPath;
    }
}
