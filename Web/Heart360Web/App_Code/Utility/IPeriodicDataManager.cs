﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using AHAAPI;
using AHACommon;

namespace Heart360Web
{
    /// <summary>
    /// Representation of a collection of Data
    /// </summary>
    public enum DataViewType
    {
        Table,
        Chart,
        TableAndChart,
        CombinedChart,
        TableAndCombinedChart
    }

    /// <summary>
    /// Interface to be implemented by a data presentation page reponsible for interacting
    /// with the Data range control, Data Add Control and the Data View control
    /// </summary>
    public interface ITrackerDetails
    {
        string GetGraphDivID
        {
            get;
        }

        void ShowHideGraphDiv(bool bIsVisible);
    }

    /// <summary>
    /// Interface to be implemented by a data presentation page reponsible for interacting
    /// with the Data range control, Data Add Control and the Data View control
    /// </summary>
    public interface IPeriodicDataManager
    {
        void LoadItems();

        void FocusTabOne();

        void OnAddRecord();

        void OnCreatePDF();

        DataDateReturnType DateRange
        {
            get;
        }

        TrackerType TrackerType
        {
            get;
        }

        DataViewType DataViewType
        {
            get;
        }
    }

    /// <summary>
    /// Interface to be implemented by a data presentation page reponsible for interacting
    /// with the Data range control, Data Add Control and the Data View control
    /// </summary>
    public interface IDataList
    {
        void OnAddRecord();

        void LoadItems(AHACommon.DataDateReturnType DateRange);
    }

    /// <summary>
    /// Interface to be implemented by a data presentation page reponsible for interacting
    /// with the Data range control, Data Add Control and the Data View control
    /// </summary>
    public interface IDataGraph
    {
        void LoadItems();
    }

    /// <summary>
    /// Interface to be implemented by a data presentation page reponsible for interacting
    /// with the Data range control, Data Add Control and the Data View control
    /// </summary>
    public interface IMedicationDataManager
    {
        void LoadItems();

        DataDateReturnType DateRange
        {
            get;
        }

        void LoadEditData(string strItemID, string strVersion);

    }

    public interface IProviderSearchPage
    {
        string SearchText
        {
            get;
        }
    }

    public interface ISpecialMaster
    {
        void MarkAsSpecial();
    }

    public interface IMailHome
    {
        event MailFolderChangedEventHandler MailFolderChanged;

        event MailActionChangedEventHandler MailActionChanged;

        event MessageShowEventHandler MessageShow;

        string CurrentFolderType
        {
            get;
            set;
        }

        string CurrentMessageID
        {
            get;
            set;
        }

        void ShowHideMessageList(bool bShow);
        void SetActionResult(string strMessage);
        void NotifyMailFolderChanged(string strFolderType);
        void NotifyMailActionChanged(string strMailActionType, string strMessageType);
        void NotifyMessageShow(int iMessageID);
        void ShowHideDeleteButton(bool bShow);

        bool IsMessagingEnabledForPatient { get; set; }
    }


}
