﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.Health;
using Microsoft.Health.Web;
using Microsoft.Health.ItemTypes;
using System.IO;
using System.Collections;
using System.Drawing;
using ImageLib;


namespace Heart360Web
{
    public class PersonalImageHelper
    {
        private static object personalImagesLock = new object();

        public static AHAAPI.Provider.ImageItem GetPersonalImageBytes(HealthRecordAccessor objHRI)
        {
            AHAAPI.Provider.ImageItem objImageItem = null;
            byte[] imageBytes = null;

            try
            {
                objImageItem = AHAAPI.Provider.SessionInfo.GetPatientImage(objHRI.Id);
                if (objImageItem != null)
                {
                    return objImageItem;
                }

                lock (personalImagesLock)
                {
                    if (objImageItem != null)
                    {
                        return objImageItem;
                    }

                    if (objImageItem == null)
                    {
                        HealthRecordItemCollection collection = objHRI.GetItemsByType(PersonalImage.TypeId, HealthRecordItemSections.BlobPayload);

                        PersonalImage image = null;
                        bool bHasHVImage = false;

                        if (collection.Count != 0)
                        {
                            image = collection[0] as PersonalImage;

                            using (Stream currentImageStream = image.ReadImage())
                            {
                                if (currentImageStream != null)
                                {
                                    imageBytes = _getResizedImage(currentImageStream);
                                    bHasHVImage = true;
                                }
                            }
                        }

                        if (imageBytes == null)
                        {
                            imageBytes = _OutputDefaultImage();
                        }

                        objImageItem = new AHAAPI.Provider.ImageItem { ImageByteArray = imageBytes, HasHVImage = bHasHVImage, RecordID = objHRI.Id };
                        AHAAPI.Provider.SessionInfo.AddPatientImageToList(objImageItem);
                    }
                }
            }
            catch (HealthServiceCredentialTokenExpiredException ctex)
            {
                throw;
            }
            catch (Exception hsadex)
            {
                objImageItem = new AHAAPI.Provider.ImageItem { ImageByteArray = _OutputDefaultImage(), HasHVImage = false, RecordID = objHRI.Id };
            }

            return objImageItem;
        }

        private static byte[] _getResizedImage(Stream stream)
        {
            int iWidth = 50;
            int iHeight = 50;

            System.Drawing.Image portrait = System.Drawing.Image.FromStream(stream);
            System.Drawing.Image bm = ImageHelper.Resample(portrait, "fill(50%,50%)", iWidth, iHeight);
            ImageConverter converter = new ImageConverter();
            byte[] imageByteArray = (byte[])converter.ConvertTo(bm, typeof(byte[]));
            return imageByteArray;

        }

        private static byte[] _OutputDefaultImage()
        {
            using (Stream currentImageStream = System.IO.File.OpenRead(System.Web.HttpContext.Current.Server.MapPath("~/images/NoUserImage.gif")))
            {
                byte[] imageBytes = _getResizedImage(currentImageStream);
                return imageBytes;
            }
        }
    }
}
