﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Heart360Web
{
    public class FolderChangedEventArgs : EventArgs
    {
        public string FolderType
        {
            get;
            set;
        }
    }

    public class MailActionChangedEventArgs : EventArgs
    {
        public string MailActionType
        {
            get;
            set;
        }

        public string MessageType
        {
            get;
            set;
        }
    }

    public class MessageEventArgs : EventArgs
    {
        public int MessageID
        {
            get;
            set;
        }
    }

    public delegate void MailFolderChangedEventHandler(object sender, FolderChangedEventArgs e);
    public delegate void MailActionChangedEventHandler(object sender, MailActionChangedEventArgs e);
    public delegate void MessageShowEventHandler(object sender, MessageEventArgs e);

}
