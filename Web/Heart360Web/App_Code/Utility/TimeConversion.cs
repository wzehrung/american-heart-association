﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Heart360Web
{
    public enum TimeRange
    { 
        Morning,
        Afternoon,
        Evening,
        Night
    }

    /*
     *  Morning -   6:00 AM to 10:00 AM
     *  Afternoon - 10:00 AM to 4:00 PM
     *  Evening -   4:00 PM to 8:00PM
     *  Night   -   8:00 PM to 6:00 AM
     */

    public class TimeConversion
    {
        static int iMorningStartHour = 6;
        static int iAfternoonStartHour = 10;
        static int iEveningStartHour = 16;
        static int iNightStartHour = 20;

        public static TimeRange GetTimeRangeByDateTime(DateTime dt)
        {
            if (dt.Hour >= iMorningStartHour && dt.Hour < iAfternoonStartHour)
                return TimeRange.Morning;
            else if (dt.Hour >= iAfternoonStartHour && dt.Hour < iEveningStartHour)
                return TimeRange.Afternoon;
            else if (dt.Hour >= iEveningStartHour && dt.Hour < iNightStartHour)
                return TimeRange.Evening;
            else
                return TimeRange.Night;
        }

        public static DateTime GetDateTimeByTimeRange(TimeRange timeRange)
        {
            return GetDateTimeByTimeRange(DateTime.Now, timeRange);
        }

        public static DateTime GetDateTimeByTimeRange(DateTime dt, TimeRange timeRange)
        {
            if (timeRange == TimeRange.Morning)
            {
                return new DateTime(dt.Year, dt.Month, dt.Day, iMorningStartHour, 0, 0);            
            }
            else if (timeRange == TimeRange.Afternoon)
            {
                return new DateTime(dt.Year, dt.Month, dt.Day, iAfternoonStartHour, 0, 0);
            }
            else if (timeRange == TimeRange.Evening)
            {
                return new DateTime(dt.Year, dt.Month, dt.Day, iEveningStartHour, 0, 0);
            }
            else 
            {
                return new DateTime(dt.Year, dt.Month, dt.Day, iNightStartHour, 0, 0);
            }
        }

        public static ListItemCollection GetAllTimeOfDay()
        {
            ListItemCollection itemColl = new ListItemCollection();
            
            ListItem item = new ListItem("Morning", "Morning");
            itemColl.Add(item);

            item = new ListItem("Afternoon", "Afternoon");
            itemColl.Add(item);

            item = new ListItem("Evening", "Evening");
            itemColl.Add(item);

            item = new ListItem("Night", "Night");
            itemColl.Add(item);

            return itemColl;
        }

        public static ListItemCollection GetTimeList()
        {
            ListItemCollection itemColl = new ListItemCollection();
            for (int i = 0; i < 24; i++)
            {
                if (i < 10)
                {
                    itemColl.Add(new ListItem("0" + i.ToString() + ":00", "0" + i.ToString() + ":00"));
                    itemColl.Add(new ListItem("0" + i.ToString() + ":30", "0" + i.ToString() + ":30"));
                }
                else
                {
                    itemColl.Add(new ListItem(i.ToString() + ":00", i.ToString() + ":00"));
                    itemColl.Add(new ListItem(i.ToString() + ":30", i.ToString() + ":30"));
                }
            }
            return itemColl;
        }

        public static ListItemCollection getTimeInAMPMFormat()
        {
            ListItemCollection itemColl = new ListItemCollection();
            int j = 0;
            for (int i = 0; i < 12; i++)
            {
                if (i == 0)
                {
                    itemColl.Add(new ListItem("12:00 AM", "00:00"));
                    itemColl.Add(new ListItem("12:30 AM", "00:30"));
                }
                else if (i < 10)
                {
                    itemColl.Add(new ListItem("0" + i.ToString() + ":00 AM", "0" + j.ToString() + ":00"));
                    itemColl.Add(new ListItem("0" + i.ToString() + ":30 AM", "0" + j.ToString() + ":30"));
                }
                else
                {
                    itemColl.Add(new ListItem(i.ToString() + ":00 AM", j.ToString() + ":00"));
                    itemColl.Add(new ListItem(i.ToString() + ":30 AM", j.ToString() + ":30"));
                }
                j++;
            }

            for (int i = 0; i < 12; i++)
            {
                if (i == 0)
                {
                    itemColl.Add(new ListItem("12:00 PM", "12:00"));
                    itemColl.Add(new ListItem("12:30 PM", "12:30"));
                }
                else if(i<10)
                {
                    itemColl.Add(new ListItem("0" + i.ToString() + ":00 PM", j.ToString() + ":00"));
                    itemColl.Add(new ListItem("0" + i.ToString() + ":30 PM", j.ToString() + ":30"));
                }
                else
                {
                    itemColl.Add(new ListItem(i.ToString() + ":00 PM", j.ToString() + ":00"));
                    itemColl.Add(new ListItem(i.ToString() + ":30 PM", j.ToString() + ":30"));
                }
                j++;
            }
                
            return itemColl;
        }

        public static ListItemCollection getTimeInAMPMFormatWholeHours()
        {
            ListItemCollection itemColl = new ListItemCollection();
            int j = 0;
            for (int i = 0; i < 12; i++)
            {
                if (i == 0)
                {
                    itemColl.Add(new ListItem("12:00 AM", "00:00"));
                }
                else if (i < 10)
                {
                    itemColl.Add(new ListItem(" " + i.ToString() + ":00 AM", "0" + j.ToString() + ":00"));
                }
                else
                {
                    itemColl.Add(new ListItem(i.ToString() + ":00 AM", j.ToString() + ":00"));
                }
                j++;
            }

            for (int i = 0; i < 12; i++)
            {
                if (i == 0)
                {
                    itemColl.Add(new ListItem("12:00 PM", "12:00"));
                }
                else if (i < 10)
                {
                    itemColl.Add(new ListItem(" " + i.ToString() + ":00 PM", j.ToString() + ":00"));
                }
                else
                {
                    itemColl.Add(new ListItem(i.ToString() + ":00 PM", j.ToString() + ":00"));
                }
                j++;
            }

            return itemColl;
        }

        public static string GetFormattedTimeToShow(DateTime dt)
        {
            if(dt.Hour==0)
            {
                if (dt.Minute < 10)
                {
                    return "12" + ":0" + dt.Minute.ToString() + " AM";
                }
                else
                {
                    return "12" + ":" + dt.Minute.ToString() + " AM";
                }
            }
            if (dt.Hour < 10)
            {
                if (dt.Minute < 10)
                {
                    return "0" + dt.Hour.ToString() + ":0" + dt.Minute.ToString() + " AM";
                }
                else
                {
                    return "0" + dt.Hour.ToString() + ":" + dt.Minute.ToString() + " AM";
                }
            }
            else if (dt.Hour < 12)
            {
                if (dt.Minute < 10)
                {
                    return dt.Hour.ToString() + ":0" + dt.Minute.ToString() + " AM";
                }
                else
                {
                    return dt.Hour.ToString() + ":" + dt.Minute.ToString() + " AM";
                }
            }
            else if (dt.Hour == 12)
            {
                if (dt.Minute < 10)
                {
                    return dt.Hour.ToString() + ":0" + dt.Minute.ToString() + " PM";
                }
                else
                {
                    return dt.Hour.ToString() + ":" + dt.Minute.ToString() + " PM";
                }
            }
            else if (dt.Hour == 23)
            {
                if (dt.Minute < 10)
                {
                    return (dt.Hour - 12).ToString() + ":0" + dt.Minute.ToString() + " PM";
                }
                else
                {
                    return (dt.Hour - 12).ToString() + ":" + dt.Minute.ToString() + " PM";
                }
            }
            else
            {
                if (dt.Minute < 10)
                {
                    return "0" + (dt.Hour - 12).ToString() + ":0" + dt.Minute.ToString() + " PM";
                }
                else
                {
                    return "0" + (dt.Hour - 12).ToString() + ":" + dt.Minute.ToString() + " PM";
                }
            }
        }

        public static string GetFormattedTimeString(DateTime dt)
        {
            if (dt.Hour < 10)
            {
                if (dt.Minute < 10)
                {
                    return "0" + dt.Hour.ToString() + ":0" + dt.Minute.ToString();
                }
                else
                {
                    return "0" + dt.Hour.ToString() + ":" + dt.Minute.ToString();
                }
            }
            else
            {
                if (dt.Minute < 10)
                {
                    return dt.Hour.ToString() + ":0" + dt.Minute.ToString();
                }
                else
                {
                    return dt.Hour.ToString() + ":" + dt.Minute.ToString();
                }

            }
        }

        public static DateTime GetHourHalfHourAdjustedTime(DateTime dt)
        {
            int iMinutes = dt.Minute;
            int iHours = dt.Hour;
            if (iMinutes <= 15)
            {
                iMinutes = 0;
            }
            else if (iMinutes >= 45)
            {
                iMinutes = 0;
                if (iHours == 23)
                {
                    iMinutes = 30;
                }
                else
                {
                    iHours += 1;
                }
            }
            else
            {
                iMinutes = 30;
            }

            return new DateTime(dt.Year,
                dt.Month,
                dt.Day,
                iHours,
                iMinutes,
                0);
        }
    }

        // This is a class to standardize DateTime validation across the system
        // All dates to be validated (a string) must be parsable by DateTime and must be on or after 1/1/1900
        // the given _date string is not tested for null or empty
        // _cv and _v are assumed to have been initialized to true (valid)
    public class DateTimeValidation
    {
            //
            // status will be returned in _v, any error message in _cv
        public static void ValidateDateBeforeNow(string _date, CustomValidator _cv, ServerValidateEventArgs _v)
        {
            ValidateDateBeforeDate(_date, DateTime.Now, _cv, _v);
        }

        public static void ValidateDateAfterNow(string _date, CustomValidator _cv, ServerValidateEventArgs _v)
        {
            ValidateDateAfterDate(_date, DateTime.Now, _cv, _v);
        }

        public static void ValidateDateBeforeDate(string _date, DateTime _endDate, CustomValidator _cv, ServerValidateEventArgs _v)
        {

            DateTime result;

            // Check to see if the value is valid
            if (!DateTime.TryParse(_date, out result))
            {
                _v.IsValid = false;
                _cv.ErrorMessage = Resources.Tracker.Validation_Text_Valid_Date;
            }
            else if (result > _endDate)
            {
                _v.IsValid = false;
                _cv.ErrorMessage = Resources.Tracker.Validation_Text_FutureDate;
            }
            else if (result.Year < 1900)
            {
                _v.IsValid = false;
                _cv.ErrorMessage = Resources.Tracker.Validation_Text_Minimum_Date;
            }

        }


        public static void ValidateDateAfterDate(string _date, DateTime _beforeDate, CustomValidator _cv, ServerValidateEventArgs _v)
        {
            DateTime result;

            // Check to see if the value is valid
            if (!DateTime.TryParse(_date, out result))
            {
                _v.IsValid = false;
                _cv.ErrorMessage = Resources.Tracker.Validation_Text_Valid_Date;
            }
            else if (result <= _beforeDate)
            {
                _v.IsValid = false;
                _cv.ErrorMessage = Resources.Tracker.Validation_Text_GoalDates;
            }
            else if (result.Year < 1900)
            {
                _v.IsValid = false;
                _cv.ErrorMessage = Resources.Tracker.Validation_Text_Minimum_Date;
            }

        }
    }
}
