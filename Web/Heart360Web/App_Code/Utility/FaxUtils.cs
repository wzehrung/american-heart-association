﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AHACommon;
using GRCBase;
using eFaxDeveloper.Outbound;

namespace Heart360Web
{
    /// <summary>
    /// place this in aha.config when we are doing the fax stuff
    /// <!--Fax File Settings-->
    ///<add key="File_Invitation_To_Patient" value="InvitationFaxToPatient.htm"/>
    ///<add key="File_Invitation_To_Provider" value="InvitationFaxToProvider.htm"/>
    ///<!--End Fax File Settings-->
    ///<!--Fax Settings-->
    ///<add key="Fax_AccountID" value="8666807594"/>
    ///<add key="Fax_UserName" value="aha456"/>
    ///<add key="Fax_Password" value="112233"></add>
    ///<!--End Fax Settings-->
    /// </summary>
    public class FaxUtils
    {
        private static string _getFaxTemplateDirectory()
        {
            return System.Web.HttpContext.Current.Server.MapPath("~/FaxTemplates/");
        }

        public static string GetInvitationContentForProvider()
        {
            string strContent;
            string strFilePath = _getFaxTemplateDirectory();
            PatientProviderFaxSettings faxSetting = AHAAppSettings.PatientProviderFaxSettingsConfig;
            strFilePath += faxSetting.File_Invitation_To_Provider;
            
            //faxSetting.File_Invitation_To_Provider;
            strContent = IOUtility.ReadHtmlFile(strFilePath);

            return strContent;
        }

        public static string GetInvitationContentForPatient()
        {
            string strContent;
            string strFilePath = _getFaxTemplateDirectory();
            PatientProviderFaxSettings faxSetting = AHAAppSettings.PatientProviderFaxSettingsConfig;
            strFilePath += faxSetting.File_Invitation_To_Patient;
            //faxSetting.File_Invitation_To_Patient;
            strContent = IOUtility.ReadHtmlFile(strFilePath);

            return strContent;
        }

        public static bool SendFax(string strFaxNumber, string strContent)
        {
            bool isSuccess = false;
            IOutboundClient api = new OutboundClient();
            DocumentBundler docs = new DocumentBundler();
            FaxSettings faxSetting = AHAAppSettings.FaxSettingsConfig;
            api.SetAccountID(faxSetting.AccountID);
            api.SetUserName(faxSetting.UserName);
            api.SetPassword(faxSetting.Password);
            
            api.SetDispositionMethod("none");
            api.SetRecipientFax(strFaxNumber);
            docs.Add(strContent, "htm");
            api.SetDocuments(docs);
            api.SetXMLResponse(true);
            try
            {
                api.PostRequest();
            }
            catch
            {
                throw;
            }
            //Status Code "1" - Success, "2" - Failure
            if (api.GetStatusCode() == "1")
            {
                isSuccess = true;
            }
                      
            
            return isSuccess;
        }
    }
}
