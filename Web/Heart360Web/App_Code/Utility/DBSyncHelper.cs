﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using AHAAPI;
using AHAHelpContent;

namespace Heart360Web
{
    public class DBSyncHelper
    {
        /// <summary>
        /// This function helps the AHA DB to hold the latest values from HealthVault.
        /// Basicaly it fetches the data between the last sync date maintained in the DB and today
        /// </summary>
        public static void PerformSync()
        {
            string phoneNumber = "0";
            string zipCode = "0";   
            //check if the sync operation has already been performed or not.
            if (HVHelper.HVManagerPatientBase.IsDBSynchronized)
                return;

            //if the user has not consented, we don't have to fetch any data 
            if (!SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
            {
                //set that the sync has already been performed.
                HVHelper.HVManagerPatientBase.IsDBSynchronized = true;
                return;
            }

            Guid objPersonalItemGUID = SessionInfo.GetUserIdentityContext().PersonalItemGUID;
            //Guid objPersonalItemGUID = SessionInfo.GetUserIdentityContext().RecordId;

            DateTime? dtLastSyncDate = AHAHelpContent.ExtendedProfile.GetLastSyncDate(objPersonalItemGUID);
            if (dtLastSyncDate.HasValue)
            {
                //to make sure that we don't miss any data here
                dtLastSyncDate = dtLastSyncDate.Value.AddDays(-1);
            }
            else
            {
                //since we don't have any last sync date, perform fetch fo the last six months
                dtLastSyncDate = DateTime.Today.AddDays(-180);
            }

            //to make sure that we don't miss any data here
            DateTime dtSyncEndDate = DateTime.Now.AddDays(1);

            //doanload all data relevant to the patient
            HVHelper.HVManagerPatientBase.DownloadDataBetweenDates(new List<string> 
                { 
                    typeof(HVManager.BasicDataManager).ToString(), 
                    typeof(HVManager.EmergencyContactDataManager).ToString(),
                    typeof(HVManager.PersonalDataManager).ToString(), 
                    typeof(HVManager.PersonalContactDataManager).ToString(), 
                    typeof(HVManager.ExtendedProfileDataManager).ToString(),
                    typeof(HVManager.CardiacDataManager).ToString(), 
                    typeof(HVManager.BloodGlucoseDataManager).ToString(),                     
                    typeof(HVManager.BloodPressureDataManager).ToString(),                    
                    typeof(HVManager.CholesterolDataManager).ToString(), 
                    typeof(HVManager.ExerciseDataManager).ToString(), 
                    typeof(HVManager.MedicationDataManager).ToString(),    
                    typeof(HVManager.HeightDataManager).ToString(),
                    typeof(HVManager.WeightDataManager).ToString()
                    
                }, dtLastSyncDate.Value/*DateTime.Today.AddYears(-2)*/, DateTime.MaxValue);

            //fetch all singleton data
            HVManager.PersonalDataManager.PersonalItem personalItem = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
            HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem = HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item;
            HVManager.CardiacDataManager.CardiacItem cardiacItem = HVHelper.HVManagerPatientBase.CardiacDataManager.Item;
            HVManager.BasicDataManager.BasicItem basicItem = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
            HVManager.PersonalContactDataManager.ContactItem contactItem = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;

            //fetch all weight values between sync dates
            LinkedList<HVManager.HeightDataManager.HeightItem> hList = HVHelper.HVManagerPatientBase.HeightDataManager.GetDataBetweenDates(dtLastSyncDate.Value, dtSyncEndDate);

            //fetch all weight values between sync dates
            LinkedList<HVManager.WeightDataManager.WeightItem> wList = HVHelper.HVManagerPatientBase.WeightDataManager.GetDataBetweenDates(dtLastSyncDate.Value, dtSyncEndDate);

            //fetch all BP values between sync dates
            LinkedList<HVManager.BloodPressureDataManager.BPItem> bpList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataBetweenDates(dtLastSyncDate.Value, dtSyncEndDate);

            //fetch all Cholesterol values between sync dates
            LinkedList<HVManager.CholesterolDataManager.CholesterolItem> cList = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetDataBetweenDates(dtLastSyncDate.Value, dtSyncEndDate);

            //fetch all Blood glucose between sync dates
            LinkedList<HVManager.BloodGlucoseDataManager.BGItem> bgList = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetDataBetweenDates(dtLastSyncDate.Value, dtSyncEndDate);

            //fetch all medication values between sync dates
            LinkedList<HVManager.MedicationDataManager.MedicationItem> mList = HVHelper.HVManagerPatientBase.MedicationDataManager.GetDataBetweenDates(dtLastSyncDate.Value, dtSyncEndDate);

            //Todo: aerobicexercisedatamanager and exercisedatawrapper used. To be discussed and confirmed
            //fetch all exercise values between sync dates 
            LinkedList<HVManager.ExerciseDataManager.ExerciseItem> eList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataBetweenDates(dtLastSyncDate.Value, dtSyncEndDate);


            double? dTravelTime = null;
            //todo: condition to be verified
            if (epItem != null && !string.IsNullOrEmpty(epItem.TravelTimeToWork.Value))
            {
                if (epItem.TravelTimeToWork.Value.Length > 0)
                {
                    dTravelTime = Convert.ToDouble(epItem.TravelTimeToWork.Value);
                }
            }

            XDocument xDoc = new XDocument();
            XElement elemType = new XElement("AHASyncData");
            XElement elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
            elemType.Add(elemPersonalItemGUID);

            elemType.Add(new XElement("ExtendedProfile",
                    new XElement("HasFamilyHeartDiseaseHistory", cardiacItem != null ? cardiacItem.HasFamilyHeartDiseaseHistory.Value.HasValue ? cardiacItem.HasFamilyHeartDiseaseHistory.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("HasFamilyStrokeHistory", cardiacItem != null ? cardiacItem.HasFamilyStrokeHistory.Value.HasValue ? cardiacItem.HasFamilyStrokeHistory.Value.HasValue.ToString() : string.Empty : string.Empty),
                    new XElement("IsOnHypertensionDiet", cardiacItem != null ? cardiacItem.IsOnHypertensionDiet.Value.HasValue ? cardiacItem.IsOnHypertensionDiet.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("IsOnHypertensionMedication", cardiacItem != null ? cardiacItem.IsOnHypertensionMedication.Value.HasValue ? cardiacItem.IsOnHypertensionMedication.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("HasRenalFailureBeenDiagnosed", cardiacItem != null ? cardiacItem.HasRenalFailureBeenDiagnosed.Value.HasValue ? cardiacItem.HasRenalFailureBeenDiagnosed.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("HasDiabetesBeenDiagnosed", cardiacItem != null ? cardiacItem.HasDiabetesBeenDiagnosed.Value.HasValue ? cardiacItem.HasDiabetesBeenDiagnosed.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("HasPersonalHeartDiseaseHistory", cardiacItem != null ? cardiacItem.HasPersonalHeartDiseaseHistory.Value.HasValue ? cardiacItem.HasPersonalHeartDiseaseHistory.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("HasPersonalStrokeHistory", cardiacItem != null ? cardiacItem.HasPersonalStrokeHistory.Value.HasValue ? cardiacItem.HasPersonalStrokeHistory.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("ActivityLevel", epItem != null ? epItem.ActivityLevel.Value : string.Empty),
                    new XElement("BloodType", personalItem != null ? personalItem.BloodType.Value : string.Empty),
                    new XElement("Allergies", epItem != null ? epItem.Allergies.Value : string.Empty),
                    new XElement("TravelTime", dTravelTime.HasValue ? dTravelTime.Value.ToString() : string.Empty),
                    new XElement("MeansOfTransportation", epItem != null ? epItem.MeansOfTransportation.Value : string.Empty),
                    new XElement("AttitudeTowardsHealthCare", epItem != null ? epItem.AttitudeTowardsHealthCare.Value : string.Empty),
                    new XElement("IsSmokeCigarette", epItem != null ? epItem.IsSmokeCigarette.Value.HasValue ? epItem.IsSmokeCigarette.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("HasTriedToQuitSmoking", epItem != null ? epItem.HasTriedToQuitSmoking.Value.HasValue ? epItem.HasTriedToQuitSmoking.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("Country", CurrentUser.Address != null ? CurrentUser.Address.Country.Value : string.Empty),
                    new XElement("GenderOfPerson", basicItem != null ? basicItem.GenderOfPerson.Value.HasValue ? basicItem.GenderOfPerson.Value.Value.ToString() : string.Empty : string.Empty),
                    new XElement("Ethnicity", personalItem != null ? personalItem.Ethnicity.Value : string.Empty),
                    new XElement("MaritalStatus", personalItem != null ? personalItem.MaritalStatus.Value : string.Empty),
                    new XElement("YearOfBirth", basicItem != null ? basicItem.YearOfBirth.Value.ToString() : string.Empty)
                    //new XElement("YearOfBirth", CurrentUser.YearOfBirth.HasValue ? CurrentUser.YearOfBirth.Value.ToString() : string.Empty)
                    ));

            if (hList.Count > 0)
            {
                elemType.Add(hList.Select(objHeightDataItem => new XElement(new XElement("Height",
                                   new XElement("ThingID", objHeightDataItem.ThingID.ToString()),
                                   new XElement("VersionStamp", objHeightDataItem.VersionStamp.ToString()),
                                   new XElement("HeightValue", objHeightDataItem.HeightInMeters.Value),
                                   new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objHeightDataItem.When.Value))
                                   ))));
            }

            if (wList.Count > 0)
            {
                elemType.Add(wList.Select(objWeightDataItem => new XElement(new XElement("Weight",
                    new XElement("ThingID", objWeightDataItem.ThingID.ToString()),
                    new XElement("VersionStamp", objWeightDataItem.VersionStamp.ToString()),
                    new XElement("WeightValue", objWeightDataItem.CommonWeight.Value.ToPounds().ToString()),
                    new XElement("Source", objWeightDataItem.Source.Value),
                    new XElement("Note", objWeightDataItem.Note.Value),
                    new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objWeightDataItem.When.Value))
                    ))));
            }

            if (bpList.Count > 0)
            {
                elemType.Add(bpList.Select(objBPItem => new XElement(new XElement("BloodPressure",
                        new XElement("ThingID", objBPItem.ThingID.ToString()),
                        new XElement("VersionStamp", objBPItem.VersionStamp.ToString()),
                        new XElement("Systolic", objBPItem.Systolic.Value.ToString()),
                        new XElement("Diastolic", objBPItem.Diastolic.Value.ToString()),
                        new XElement("Pulse", objBPItem.HeartRate.Value.HasValue ? objBPItem.HeartRate.Value.Value.ToString() : string.Empty),
                        new XElement("Source", objBPItem.Source.Value),
                        new XElement("Note", objBPItem.Note.Value),
                        new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objBPItem.When.Value))
                        ))));
            }

            if (cList.Count > 0)
            {
                elemType.Add(cList.Select(objCholesterolItem => new XElement(new XElement("Cholesterol",
                       new XElement("ThingID", objCholesterolItem.ThingID.ToString()),
                    new XElement("VersionStamp", objCholesterolItem.VersionStamp.ToString()),
                    new XElement("TotalCholesterol", objCholesterolItem.TotalCholesterol.Value.HasValue ? objCholesterolItem.TotalCholesterol.Value.ToString() : string.Empty),
                    new XElement("LDL", objCholesterolItem.LDL.Value.HasValue ? objCholesterolItem.LDL.Value.ToString() : string.Empty),
                    new XElement("HDL", objCholesterolItem.HDL.Value.HasValue ? objCholesterolItem.HDL.Value.ToString() : string.Empty),
                    new XElement("Triglyceride", objCholesterolItem.Triglycerides.Value.HasValue ? objCholesterolItem.Triglycerides.Value.ToString() : string.Empty),
                    new XElement("Note", objCholesterolItem.Note.Value),
                    new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objCholesterolItem.When.Value))
                        ))));
            }

            if (bgList.Count > 0)
            {
                elemType.Add(bgList.Select(objBGItem => new XElement(new XElement("BloodGlucose",
                     new XElement("ThingID", objBGItem.ThingID.ToString()),
                    new XElement("VersionStamp", objBGItem.VersionStamp.ToString()),
                    new XElement("Value", objBGItem.Value.Value.ToString()),
                    new XElement("ActionTaken", objBGItem.ActionTaken.Value),
                    new XElement("ReadingType", objBGItem.ReadingType.Value),
                    new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objBGItem.When.Value))
                        ))));
            }

            if (mList.Count > 0)
            {
                elemType.Add(mList.Select(objMedicationItem => new XElement(new XElement("Medication",
                    new XElement("ThingID", objMedicationItem.ThingID.ToString()),
                    new XElement("VersionStamp", objMedicationItem.VersionStamp.ToString()),
                    new XElement("MedicationName", objMedicationItem.Name.Value),
                    new XElement("MedicationType", objMedicationItem.MedicationType.Value),
                    new XElement("Dosage", objMedicationItem.Dosage.Value),
                    new XElement("Strength", objMedicationItem.Strength.Value),
                    new XElement("Frequency", objMedicationItem.Frequency.Value),
                    new XElement("DateDiscontinued", (objMedicationItem.DateDiscontinued.Value != null
                    && objMedicationItem.DateDiscontinued.Value.HasValue()
                    && objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                    && objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Day.HasValue) ? LanguageResourceHelper.GetDateTimeForDB(new DateTime(objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Year, objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Month.Value, objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Day.Value)) : string.Empty),
                    new XElement("Note", objMedicationItem.Note.Value)
                        ))));
            }

            if (eList.Count > 0)
            {
                elemType.Add(eList.Select(objExerciseItem => new XElement(new XElement("Exercise",
                    new XElement("ThingID", objExerciseItem.ThingID.ToString()),
                    new XElement("VersionStamp", objExerciseItem.VersionStamp.ToString()),
                    new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objExerciseItem.When.Value)),
                    new XElement("Activity", objExerciseItem.Activity.Value),
                    new XElement("Intensity", objExerciseItem.Intensity.Value),
                    new XElement("DurationInMinutes", objExerciseItem.Duration.Value.HasValue ? objExerciseItem.Duration.Value.Value.ToString() : string.Empty),
                    new XElement("Note", objExerciseItem.Note.Value),
                    new XElement("NumberOfSteps", objExerciseItem.NumberOfSteps.Value.HasValue ? objExerciseItem.NumberOfSteps.Value.Value.ToString() : string.Empty)
                        ))));
            }

            xDoc.Add(elemType);

            if (basicItem != null)
            {
                if (basicItem.PostalCode != null)
                {
                    zipCode = basicItem.PostalCode.Value;
                }
            }

            if (contactItem != null)
            {
                if (contactItem.Phone != null)
                {
                    phoneNumber = contactItem.Phone.Value;
                }
      
                //if (contactItem.AddressList != null)
                //{
                //    if (contactItem.AddressList.Count > 0)
                //    {
                //        if (contactItem.AddressList[0].Zip != null)
                //        {
                //            zipCode = contactItem.AddressList[0].Zip.Value;
                //        }
                //    }
                //}
            }
            AHAHelpContent.Misc.SyncData(xDoc, true, phoneNumber, zipCode);
            xDoc = null;

            HVHelper.HVManagerPatientBase.IsDBSynchronized = true;
        }



        public static void PerformSyncForWizardItems(AHAAPI.WizardItemType eWizardItemType, DateTime dateOfItemsToBeUpdated)
        {
            //if the user has not consented, we don't have to fetch any data 
            if (!SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
            {
                return;
            }

            Guid objPersonalItemGUID = SessionInfo.GetUserIdentityContext().PersonalItemGUID;


            XDocument xDoc = null;
            XElement elemType = null;
            XElement elemPersonalItemGUID = null;
            switch (eWizardItemType)
            {
                case WizardItemType.Height:
                    LinkedList<HVManager.HeightDataManager.HeightItem> hList = HVHelper.HVManagerPatientBase.HeightDataManager.GetDataBetweenDates(dateOfItemsToBeUpdated, dateOfItemsToBeUpdated);
                    if (hList.Count > 0)
                    {
                        xDoc = new XDocument();
                        elemType = new XElement("AHASyncData");
                        elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
                        elemType.Add(elemPersonalItemGUID);
                        elemType.Add(hList.Select(objHeightDataItem => new XElement(new XElement("Height",
                                   new XElement("ThingID", objHeightDataItem.ThingID.ToString()),
                                   new XElement("VersionStamp", objHeightDataItem.VersionStamp.ToString()),
                                   new XElement("HeightValue", objHeightDataItem.HeightInMeters.Value),
                                   new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objHeightDataItem.When.Value))
                                   ))));
                        xDoc.Add(elemType);
                        AHAHelpContent.Misc.SyncData(xDoc, false, "0","0");
                        xDoc = null;
                    }
                    break;
                case WizardItemType.BloodGlucose:
                    LinkedList<HVManager.BloodGlucoseDataManager.BGItem> bgList = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetDataBetweenDates(dateOfItemsToBeUpdated, dateOfItemsToBeUpdated);
                    if (bgList.Count > 0)
                    {
                        xDoc = new XDocument();
                        elemType = new XElement("AHASyncData");
                        elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
                        elemType.Add(elemPersonalItemGUID);
                        elemType.Add(bgList.Select(objBGItem => new XElement(new XElement("BloodGlucose",
                             new XElement("ThingID", objBGItem.ThingID.ToString()),
                            new XElement("VersionStamp", objBGItem.VersionStamp.ToString()),
                            new XElement("Value", objBGItem.Value.Value.ToString()),
                            new XElement("ActionTaken", objBGItem.ActionTaken.Value),
                            new XElement("ReadingType", objBGItem.ReadingType.Value),
                            new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objBGItem.When.Value))
                                ))));
                        xDoc.Add(elemType);
                        AHAHelpContent.Misc.SyncData(xDoc, false, "0","0");
                        xDoc = null;
                    }
                    break;
                case WizardItemType.BloodPressure:
                    LinkedList<HVManager.BloodPressureDataManager.BPItem> bpList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataBetweenDates(dateOfItemsToBeUpdated, dateOfItemsToBeUpdated);
                    if (bpList.Count > 0)
                    {
                        xDoc = new XDocument();
                        elemType = new XElement("AHASyncData");
                        elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
                        elemType.Add(elemPersonalItemGUID);
                        elemType.Add(bpList.Select(objBPItem => new XElement(new XElement("BloodPressure",
                                new XElement("ThingID", objBPItem.ThingID.ToString()),
                                new XElement("VersionStamp", objBPItem.VersionStamp.ToString()),
                                new XElement("Systolic", objBPItem.Systolic.Value.ToString()),
                                new XElement("Diastolic", objBPItem.Diastolic.Value.ToString()),
                                new XElement("Pulse", objBPItem.HeartRate.Value.HasValue ? objBPItem.HeartRate.Value.Value.ToString() : string.Empty),
                                new XElement("Source", objBPItem.Source.Value),
                                new XElement("Note", objBPItem.Note.Value),
                                new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objBPItem.When.Value))
                                ))));
                        xDoc.Add(elemType);
                        AHAHelpContent.Misc.SyncData(xDoc, false, "0","0");
                        xDoc = null;
                    }
                    break;
                case WizardItemType.Cholesterol:
                    LinkedList<HVManager.CholesterolDataManager.CholesterolItem> cList = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetDataBetweenDates(dateOfItemsToBeUpdated, dateOfItemsToBeUpdated);
                    if (cList.Count > 0)
                    {
                        xDoc = new XDocument();
                        elemType = new XElement("AHASyncData");
                        elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
                        elemType.Add(elemPersonalItemGUID);
                        elemType.Add(cList.Select(objCholesterolItem => new XElement(new XElement("Cholesterol",
                               new XElement("ThingID", objCholesterolItem.ThingID.ToString()),
                            new XElement("VersionStamp", objCholesterolItem.VersionStamp.ToString()),
                            new XElement("TotalCholesterol", objCholesterolItem.TotalCholesterol.Value.HasValue ? objCholesterolItem.TotalCholesterol.Value.ToString() : string.Empty),
                            new XElement("LDL", objCholesterolItem.LDL.Value.HasValue ? objCholesterolItem.LDL.Value.ToString() : string.Empty),
                            new XElement("HDL", objCholesterolItem.HDL.Value.HasValue ? objCholesterolItem.HDL.Value.ToString() : string.Empty),
                            new XElement("Triglyceride", objCholesterolItem.Triglycerides.Value.HasValue ? objCholesterolItem.Triglycerides.Value.ToString() : string.Empty),
                            new XElement("Note", objCholesterolItem.Note.Value),
                            new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objCholesterolItem.When.Value))
                                ))));
                        xDoc.Add(elemType);
                        AHAHelpContent.Misc.SyncData(xDoc, false, "0","0");
                        xDoc = null;
                    }

                    break;
                case WizardItemType.Medication:
                    LinkedList<HVManager.MedicationDataManager.MedicationItem> mList = HVHelper.HVManagerPatientBase.MedicationDataManager.GetDataBetweenDates(dateOfItemsToBeUpdated, dateOfItemsToBeUpdated);
                    if (mList.Count > 0)
                    {
                        xDoc = new XDocument();
                        elemType = new XElement("AHASyncData");
                        elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
                        elemType.Add(elemPersonalItemGUID);
                        elemType.Add(mList.Select(objMedicationItem => new XElement(new XElement("Medication",
                            new XElement("ThingID", objMedicationItem.ThingID.ToString()),
                            new XElement("VersionStamp", objMedicationItem.VersionStamp.ToString()),
                            new XElement("MedicationName", objMedicationItem.Name.Value),
                            new XElement("MedicationType", objMedicationItem.MedicationType.Value),
                            new XElement("Dosage", objMedicationItem.Dosage.Value),
                            new XElement("Strength", objMedicationItem.Strength.Value),
                            new XElement("Frequency", objMedicationItem.Frequency.Value),
                            new XElement("DateDiscontinued", (objMedicationItem.DateDiscontinued.Value != null
                            && objMedicationItem.DateDiscontinued.Value.HasValue()
                            && objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                            && objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Day.HasValue) ? LanguageResourceHelper.GetDateTimeForDB(new DateTime(objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Year, objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Month.Value, objMedicationItem.DateDiscontinued.Value.HVApproximateDate.Day.Value)) : string.Empty),
                            new XElement("Note", objMedicationItem.Note.Value)
                                ))));
                        xDoc.Add(elemType);
                        AHAHelpContent.Misc.SyncData(xDoc, false, "0","0");
                        xDoc = null;
                    }

                    break;
                case WizardItemType.PhysicalActivity:
                    LinkedList<HVManager.ExerciseDataManager.ExerciseItem> eList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataBetweenDates(dateOfItemsToBeUpdated, dateOfItemsToBeUpdated);
                    if (eList.Count > 0)
                    {
                        xDoc = new XDocument();
                        elemType = new XElement("AHASyncData");
                        elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
                        elemType.Add(elemPersonalItemGUID);
                        elemType.Add(eList.Select(objExerciseItem => new XElement(new XElement("Exercise",
                            new XElement("ThingID", objExerciseItem.ThingID.ToString()),
                            new XElement("VersionStamp", objExerciseItem.VersionStamp.ToString()),
                            new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objExerciseItem.When.Value)),
                            new XElement("Activity", objExerciseItem.Activity.Value),
                            new XElement("Intensity", objExerciseItem.Intensity.Value),
                            new XElement("DurationInMinutes", objExerciseItem.Duration.Value.HasValue ? objExerciseItem.Duration.Value.Value.ToString() : string.Empty),
                            new XElement("Note", objExerciseItem.Note.Value),
                            new XElement("NumberOfSteps", objExerciseItem.NumberOfSteps.Value.HasValue ? objExerciseItem.NumberOfSteps.Value.Value.ToString() : string.Empty)
                                ))));
                        xDoc.Add(elemType);
                        AHAHelpContent.Misc.SyncData(xDoc, false, "0","0");
                        xDoc = null;
                    }
                    break;
                case WizardItemType.Weight:
                    LinkedList<HVManager.WeightDataManager.WeightItem> wList = HVHelper.HVManagerPatientBase.WeightDataManager.GetDataBetweenDates(dateOfItemsToBeUpdated, dateOfItemsToBeUpdated);

                    if (wList.Count > 0)
                    {
                        xDoc = new XDocument();
                        elemType = new XElement("AHASyncData");
                        elemPersonalItemGUID = new XElement("PersonalItemGUID", objPersonalItemGUID);
                        elemType.Add(elemPersonalItemGUID);
                        elemType.Add(wList.Select(objWeightDataItem => new XElement(new XElement("Weight",
                            new XElement("ThingID", objWeightDataItem.ThingID.ToString()),
                            new XElement("VersionStamp", objWeightDataItem.VersionStamp.ToString()),
                            new XElement("WeightValue", objWeightDataItem.CommonWeight.Value.ToPounds().ToString()),
                            new XElement("Source", objWeightDataItem.Source.Value),
                            new XElement("Note", objWeightDataItem.Note.Value),
                            new XElement("When", LanguageResourceHelper.GetDateTimeForDB(objWeightDataItem.When.Value))
                            ))));
                        xDoc.Add(elemType);
                        AHAHelpContent.Misc.SyncData(xDoc, false, "0","0");
                        xDoc = null;
                    }
                    break;
            }
        }
    }
}
