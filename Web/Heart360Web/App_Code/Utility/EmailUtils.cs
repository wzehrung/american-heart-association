﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GRCBase;
using AHACommon;
using AHAHelpContent;
using System.Resources;

namespace Heart360Web
{
    public class EmailUtils
    {
        public static string GetEmailTemplateDirectory(string strLocale)
        {
            if (strLocale != Locale.English)
            {
                return System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates.{0}/", strLocale));
            }

            return System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/");
        }        

        public static void SendNewMessageEmail(string strToName, string strToEmail, string strLocale)
        {
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);

            string filepath = string.Format("{0}\\{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_New_Message_Provider);
            string subject = Resources.Common.ResourceManager.GetString("Subject_New_Message_Provider", cultureInfo);
            
            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", GRCBase.UrlUtility.ServerRootUrl);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            //Send Email
            // PJB: DMARC fix
            //GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SystemEmail, null);
        }

/** No longer needed in patient portal
        public static void SendWelcomeEmailToProvider(string strFromEmail, string strFromName, string strToName, string strToEmail, string strLocale, string strProviderCode)
        {
            
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_Register_Provider);
            //string subject = emailSetting.Subject_Register_Provider;
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Register_Provider", cultureInfo);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_CODE##", strProviderCode);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", GRCBase.UrlUtility.ServerRootUrl);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            //Send Email
            string fromEmail = strFromEmail;
            string fromName = strFromName;

            GRCBase.EmailHelper.SendMail(strFromName, strToName, strFromEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
        }

        public static void SendForgotPasswordEmail(string strToName, string strToEmail, string strLink, string strLocale)
        {
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_Forgot_Password);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Forgot_Password", cultureInfo);
            //string subject = emailSetting.Subject_Forgot_Password;

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", strLink);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            // PJB: DMARC fix
            // GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SystemEmail, null);
        }

        public static void SendForgotUsernameEmail(string strToName, string strToEmail, string strUsername, string strLocale)
        {
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_Forgot_Username);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Forgot_Username", cultureInfo);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##USERNAME##", strUsername);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
        }

        public static void SendInvitationToPatient(AHAHelpContent.PatientProviderInvitation objInvitation, ProviderDetails objProvider)
        {
            AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(objContext.SelectedLanguage.LanguageLocale), emailSetting.File_Invite_Patient);

            foreach (PatientProviderInvitationDetails objDetails in objInvitation.InvitationDetails)
            {
                System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objContext.SelectedLanguage.LanguageLocale);
                string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Patient", cultureInfo);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", objDetails.Name);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);

                string strContent = IOUtility.ReadHtmlFile(filepath);

                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", objDetails.Name);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_CODE##", objProvider.ProviderCode);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##INVITATION_URL##", AHAAPI.GlobalPaths.GetInvitationLink(objInvitation.InvitationCode, true));

                strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, objContext.SelectedLanguage.LanguageLocale);

                //Send Email
                GRCBase.EmailHelper.SendMail(objProvider.FirstName, objDetails.Name, objProvider.Email, objDetails.Email, subject, strContent, true, null, objProvider.Email, null);
            }
        }

        public static void SendInvitationToPatient(AHAHelpContent.PatientProviderInvitationDetails objInvitationDetails, ProviderDetails objProvider, string strInvitationCode)
        {
            AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(objContext.SelectedLanguage.LanguageLocale), emailSetting.File_Invite_Patient);

            //string subject = System.Text.RegularExpressions.Regex.Replace(emailSetting.Subject_Invite_Patient, "##PATIENT_NAME##", objInvitationDetails.Name);
            //subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objContext.SelectedLanguage.LanguageLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Patient", cultureInfo);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", objInvitationDetails.Name);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", objInvitationDetails.Name);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_CODE##", objProvider.ProviderCode);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##INVITATION_URL##", AHAAPI.GlobalPaths.GetInvitationLink(strInvitationCode, true));

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, objContext.SelectedLanguage.LanguageLocale);

            //Send Email
            GRCBase.EmailHelper.SendMail(objProvider.FirstName, objInvitationDetails.Name, objProvider.Email, objInvitationDetails.Email, subject, strContent, true, null, objProvider.Email, null);
        }
**/
        public static void SendInvitationToProvider(AHAHelpContent.PatientProviderInvitation objInvitation, string strPatientName, string strEmail)
        {
            AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(objContext.SelectedLanguage.LanguageLocale), emailSetting.File_Invite_Provider);

            foreach (PatientProviderInvitationDetails objDetails in objInvitation.InvitationDetails)
            {

                System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objContext.SelectedLanguage.LanguageLocale);
                string subject = Resources.ConnCenterPAndV.ResourceManager.GetString("Email_Subject_Invite_Provider", cultureInfo);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objDetails.Name);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strPatientName);

                string strContent = IOUtility.ReadHtmlFile(filepath);

                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strPatientName);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objDetails.Name);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##INVITATION_URL##", AHAAPI.GlobalPaths.GetInvitationLink(objInvitation.InvitationCode, false));

                // PJB: DMARC fix
                //string strFromMail = string.IsNullOrEmpty(strEmail) ? AHAAppSettings.SystemEmail : strEmail;
                string strFromMail = AHAAppSettings.SystemEmail;

                strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, objContext.SelectedLanguage.LanguageLocale);

                GRCBase.EmailHelper.SendMail(strPatientName,
                    objDetails.Name,
                    strFromMail,
                    objDetails.Email,
                    subject,
                    strContent,
                    true, null,
                    strFromMail,
                    null);
            }
        }

/** This is for the provider portal
        public static void SendInvitationConfirmationToPatient(ProviderDetails objProvider, string strFName, string strEmail)
        {
            AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(objContext.SelectedLanguage.LanguageLocale), emailSetting.File_Invitation_Confirmation_Patient);

            //string subject = System.Text.RegularExpressions.Regex.Replace(emailSetting.Subject_Invitation_Confirmation_Patient, "##PROVIDER_NAME##", objProvider.FormattedName);
            //subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strFName);

            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objContext.SelectedLanguage.LanguageLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Confirmation_Patient", cultureInfo);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strFName);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strFName);

            if (objProvider.Salutation == null)
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", "");
            }
            else
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", objProvider.Salutation);
            }
           
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, objContext.SelectedLanguage.LanguageLocale);

            //Send Email
            GRCBase.EmailHelper.SendMail(objProvider.FirstName, strFName, objProvider.Email, strEmail, subject, strContent, true, null, objProvider.Email, null);
        }
**/
        public static void SendInvitationConfirmationToProvider(ProviderDetails objProvider, string strPatientName, string strPatientEmail, string strLocale)
        {
            //AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_Invitation_Confirmation_Provider);

            ////string subject = System.Text.RegularExpressions.Regex.Replace(emailSetting.Subject_Invitation_Confirmation_Provider, "##PROVIDER_NAME##", objProvider.FormattedName);
            ////subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strPatientName);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Confirmation_Provider", cultureInfo);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strPatientName);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strPatientName);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            //Send Email
            /** PJB: DMARC fix
            string fromMail = string.Empty;

            if (string.IsNullOrEmpty(strPatientEmail))
            {
                fromMail = AHAAppSettings.SystemEmail;
            }
            else
            {
                fromMail = strPatientEmail;
            }
            **/
            string fromMail = AHAAppSettings.SystemEmail;

            GRCBase.EmailHelper.SendMail(strPatientName, objProvider.FirstName, fromMail, objProvider.Email, subject, strContent, true, null, fromMail, null);
        }
/** for provider portal
        public static void SendDisconnectPatientEmail(string strToName, string strToEmail, string strProviderName, string strProviderSalutation)
        {
            AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(objContext.SelectedLanguage.LanguageLocale), emailSetting.File_Disconnect_Patient);
            //ResourceManager rm = new System.Resources.ResourceManager("Resources.Common", System.Reflection.Assembly.Load("App_GlobalResources"));
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objContext.SelectedLanguage.LanguageLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Disconnect_Patient", cultureInfo);
            //string subject = emailSetting.Subject_Disconnect_Patient;

            string strContent = IOUtility.ReadHtmlFile(filepath);

            if (strProviderSalutation == null)
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", "");
            }
            else
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", strProviderSalutation);
            }

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", strProviderName);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, objContext.SelectedLanguage.LanguageLocale);

            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
        }
**/


        public static void SendDisconnectProviderEmail(string strToName, string strToEmail, string strPatientName, string strLocale)
        {
            //AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_Disconnect_Provider);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.ConnCenterPAndV.ResourceManager.GetString("Email_Subject_Disconnect_Provider", cultureInfo);
            //string subject = emailSetting.Subject_Disconnect_Provider;

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strPatientName);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", strToName);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            // PJB: DMARC fix
            //GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SenderEmail, null);
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AHAAppSettings.SystemEmail, strToEmail, subject, strContent, true, null, AHAAppSettings.SystemEmail, null);
        }

        public static void SendFeedbackEmail(string strSubject, string strBody, string strResponseEmail)
        {
            string strFromEmail = AHAAppSettings.SystemEmail;

            string strToEmail = AHAAppSettings.FeedbackToEmail;

            string strReplyTo = null;
            if (!String.IsNullOrEmpty(strResponseEmail))
                strReplyTo = strResponseEmail;

            strBody = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strBody, false, Locale.English);

            List<AHAHelpContent.Feedback> objRecipients = AHAHelpContent.Feedback.GetFeedbackRecipients();

            if (objRecipients != null)
            {
                for (int i = 0; i < objRecipients.Count; i++)
                {
                    // PJB: DMARC fix
                    // GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, strFromEmail, objRecipients[i].AdminEmailAddress.ToString(), strSubject, strBody, true, null, AHAAppSettings.SenderEmail, strReplyTo);
                    GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, strFromEmail, objRecipients[i].AdminEmailAddress.ToString(), strSubject, strBody, true, null, strFromEmail, strReplyTo);
                }
            }           

            // WZ: Original:
            //GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, strFromEmail, strToEmail, strSubject, strBody, true, null, AHAAppSettings.SenderEmail, strReplyTo);
        }
    }
}
