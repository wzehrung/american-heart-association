﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AHAAPI;
using AHAHelpContent;

namespace Heart360Web
{
    public class PatientGroupItemComparer : IComparer<AHAHelpContent.PatientGroup>
    {
        private bool m_bAscending;
        private string m_FieldName;

        public PatientGroupItemComparer(string strFieldName, bool bAscending)
        {
            m_bAscending = bAscending;
            m_FieldName = strFieldName;
        }

        public int Compare(AHAHelpContent.PatientGroup x, AHAHelpContent.PatientGroup y)
        {
            if (m_FieldName.ToLower() == "groupname")
            {
                return (m_bAscending ? 1 : -1) * x.Name.CompareTo(y.Name);
            }
            else if(m_FieldName.ToLower() == "lastactive")
            {
                return (m_bAscending ? 1 : -1) * x.Name.CompareTo(y.Name);
            }
            else// if (m_FieldName.ToLower() == "alerts")
            {
                return (m_bAscending ? 1 : -1) * x.TotalAlerts.CompareTo(y.TotalAlerts);
            }
        }
    }

    public class PatientItemComparer : IComparer<PatientInfo>
    {
        private bool m_bAscending;
        private string m_FieldName;

        public PatientItemComparer(string strFieldName, bool bAscending)
        {
            m_bAscending = bAscending;
            m_FieldName = strFieldName;
        }

        public int Compare(PatientInfo x, PatientInfo y)
        {
            if (m_FieldName.ToLower() == "name")
            {
                return (m_bAscending ? 1 : -1) * x.FullName.CompareTo(y.FullName);
            }
            else if (m_FieldName.ToLower() == "dob")
            {
                return (m_bAscending ? 1 : -1) * Nullable.Compare(x.DateOfBirth,y.DateOfBirth);
            }
            else if (m_FieldName.ToLower() == "datejoined")
            {
                return (m_bAscending ? 1 : -1) * Nullable.Compare(x.DateJoined, y.DateJoined);
            }
            else if (m_FieldName.ToLower() == "patientconnectiondate")
            {
                return (m_bAscending ? 1 : -1) * Nullable.Compare(x.ProviderConnectionDate, y.ProviderConnectionDate);
            }
            else if (m_FieldName.ToLower() == "lastactive")
            {
                return (m_bAscending ? 1 : -1) * Nullable.Compare(x.LastActive,y.LastActive);
            }
            else if (m_FieldName.ToLower() == "bloodpressure")
            {
                return (m_bAscending ? 1 : -1) * Nullable.Compare(x.Systolic, y.Systolic);
            }
            else if (m_FieldName.ToLower() == "bloodglucose")
            {
                return (m_bAscending ? 1 : -1) * Nullable.Compare(x.BloodGlucose, y.BloodGlucose);
            }
            else if (m_FieldName.ToLower() == "cholesterol")
            {
                return (m_bAscending ? 1 : -1) * Nullable.Compare(x.Cholesterol, y.Cholesterol);
            }
            else
            {
                return (m_bAscending ? 1 : -1) * x.TotalAlerts.CompareTo(y.TotalAlerts);
            }
        }
    }

    public class InvitationDetailsComparer : IComparer<PatientProviderInvitationDetails>
    {
        string m_Expression;
        bool m_Ascending;

        public InvitationDetailsComparer(string strExpression, bool bAscending)
        {
            this.m_Expression = strExpression;
            this.m_Ascending = bAscending;
        }

        #region IComparer<PatientProviderInvitationDetails> Members

        public int Compare(PatientProviderInvitationDetails x, PatientProviderInvitationDetails y)
        {
            switch (this.m_Expression)
            {
                case "last":
                    return (this.m_Ascending ? 1 : -1) * x.LastName.CompareTo(y.LastName);
                case "first":
                    return (this.m_Ascending ? 1 : -1) * x.FirstName.CompareTo(y.FirstName);
                case "type":
                    return (this.m_Ascending ? 1 : -1) * x.IsPrintInvitation.CompareTo(y.IsPrintInvitation);
                case "date":
                    return (this.m_Ascending ? 1 : -1) * x.DateInserted.CompareTo(y.DateInserted);
            }

            return 0;
        }

        #endregion
    }

    public class AlertRuleItemComparer : IComparer<AHAHelpContent.AlertRule>
    {
        private bool m_bAscending;
        private string m_FieldName;

        public AlertRuleItemComparer(string strFieldName, bool bAscending)
        {
            m_bAscending = bAscending;
            m_FieldName = strFieldName;
        }

        public int Compare(AHAHelpContent.AlertRule x, AHAHelpContent.AlertRule y)
        {
            if (m_FieldName.ToLower() == "who")
            {
                return (m_bAscending ? 1 : -1) * x.Type.CompareTo(y.Type);
            }
            return 0;
        }
    }

    public class AlertItemComparer : IComparer<AHAHelpContent.Alert>
    {
        private bool m_bAscending;
        private string m_FieldName;

        public AlertItemComparer(string strFieldName, bool bAscending)
        {
            m_bAscending = bAscending;
            m_FieldName = strFieldName;
        }

        public int Compare(AHAHelpContent.Alert x, AHAHelpContent.Alert y)
        {
            if (m_FieldName.ToLower() == "date")
            {
                return (m_bAscending ? 1 : -1) * x.ItemEffectiveDate.CompareTo(y.ItemEffectiveDate);
            }
            else if (m_FieldName.ToLower() == "first")
            {
                AHAHelpContent.Patient objPatientX = AHAHelpContent.Patient.FindByUserHealthRecordID(x.PatientID);
                AHAHelpContent.Patient objPatientY = AHAHelpContent.Patient.FindByUserHealthRecordID(y.PatientID);
                HVManager.AllItemManager mgrX = HVManager.AllItemManager.GetItemManagersForRecordId(objPatientX.OfflinePersonGUID.Value, objPatientX.OfflineHealthRecordGUID.Value);
                HVManager.AllItemManager mgrY = HVManager.AllItemManager.GetItemManagersForRecordId(objPatientY.OfflinePersonGUID.Value, objPatientY.OfflineHealthRecordGUID.Value);
                return (m_bAscending ? 1 : -1) * mgrX.PersonalDataManager.Item.FirstName.Value.CompareTo(mgrY.PersonalDataManager.Item.FirstName.Value);
            }
            else if (m_FieldName.ToLower() == "last")
            {
                AHAHelpContent.Patient objPatientX = AHAHelpContent.Patient.FindByUserHealthRecordID(x.PatientID);
                AHAHelpContent.Patient objPatientY = AHAHelpContent.Patient.FindByUserHealthRecordID(y.PatientID);
                HVManager.AllItemManager mgrX = HVManager.AllItemManager.GetItemManagersForRecordId(objPatientX.OfflinePersonGUID.Value, objPatientX.OfflineHealthRecordGUID.Value);
                HVManager.AllItemManager mgrY = HVManager.AllItemManager.GetItemManagersForRecordId(objPatientY.OfflinePersonGUID.Value, objPatientY.OfflineHealthRecordGUID.Value);
                return (m_bAscending ? 1 : -1) * mgrX.PersonalDataManager.Item.LastName.Value.CompareTo(mgrY.PersonalDataManager.Item.LastName.Value);
            }
            return 0;
        }
    }
}
