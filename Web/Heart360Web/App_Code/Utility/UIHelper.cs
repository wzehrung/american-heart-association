﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using AHAAPI;
using AHACommon;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using AHAHelpContent;
using System.Resources;
//using Heart360Web.Patient;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using GRCBase;

namespace Heart360Web
{
    public class ListItemsComparer : IComparer<AHAHelpContent.ListItem>
    {
        private bool m_bAscending;

        public ListItemsComparer(bool bAscending)
        {
            m_bAscending = bAscending;
        }

        public int Compare(AHAHelpContent.ListItem x, AHAHelpContent.ListItem y)
        {
            return (m_bAscending ? 1 : -1) * x.ListItemName.CompareTo(y.ListItemName);
        }
    }

    public class ColorCodeHelper
    {
        public static string GetTotalCholesterolColorCode(HVManager.CholesterolDataManager.CholesterolItem item)
        {
            if ((item.EffectiveDate.CompareTo(DateTime.Now.AddDays(-30)) == 1) && item.TotalCholesterol.Value.HasValue)
            {
                if (item.TotalCholesterol.Value > UIHelper.TOTAL_CHOLESTEROL_THRESHOLD)
                {
                    return "needsImprovement";
                }
                else
                {
                    return "excellent";
                }
            }
            return string.Empty;
        }

        public static string GetHDLColorCode(HVManager.CholesterolDataManager.CholesterolItem item, string strGender)
        {
            if ((item.EffectiveDate.CompareTo(DateTime.Now.AddDays(-30)) == 1) && item.HDL.Value.HasValue)
            {
                if (strGender == "Male")
                {
                    if (item.HDL.Value < UIHelper.HDL_MAN_THRESHOLD)
                    {
                        return "needsImprovement";
                    }
                    else
                    {
                        return "excellent";
                    }
                }
                else
                {
                    if (item.HDL.Value < UIHelper.HDL_WOMAN_THRESHOLD)
                    {
                        return "needsImprovement";
                    }
                    else
                    {
                        return "excellent";
                    }
                }
            }
            return string.Empty;
        }

        public static string GetLDLColorCode(HVManager.CholesterolDataManager.CholesterolItem item)
        {
            if ((item.EffectiveDate.CompareTo(DateTime.Now.AddDays(-30)) == 1) && item.LDL.Value.HasValue)
            {
                if (item.LDL.Value > UIHelper.LDL_THRESHOLD)
                {
                    return "needsImprovement";
                }
                else
                {
                    return "excellent";
                }
            }
            return string.Empty;
        }

        public static string GetBloodGlucoseColorCode(HVManager.BloodGlucoseDataManager.BGItem item)
        {
            if (item.EffectiveDate.CompareTo(DateTime.Now.AddDays(-30)) == 1 && item.ReadingType != null && !String.IsNullOrEmpty(item.ReadingType.Value) && item.ReadingType.Value == "Fasting")
            {
                if (item.Value.Value > UIHelper.FASTING_GLUCOSE_THRESHOLD)
                {
                    return "needsImprovement";
                }
                else
                {
                    return "excellent";
                }
            }

            return string.Empty;
        }


        public static string GetSystolicColorCode(HVManager.BloodPressureDataManager.BPItem item)
        {
            if (item.EffectiveDate.CompareTo(DateTime.Now.AddDays(-30)) == 1)
            {
                if (item.Systolic.Value > UIHelper.SYSTOLIC_THRESHOLD)
                {
                    return "needsImprovement";
                }
                else
                {
                    return "excellent";
                }
            }

            return string.Empty;
        }

        public static string GetDiastolicColorCode(HVManager.BloodPressureDataManager.BPItem item)
        {
            if (item.EffectiveDate.CompareTo(DateTime.Now.AddDays(-30)) == 1)
            {
                if (item.Diastolic.Value > UIHelper.DIASTOLIC_THRESHOLD)
                {
                    return "needsImprovement";
                }
                else
                {
                    return "excellent";
                }
            }

            return string.Empty;
        }
    }
    public class UIHelper
    {
        public const int SYSTOLIC_THRESHOLD = 139;
        public const int DIASTOLIC_THRESHOLD = 89;
        public const int TOTAL_CHOLESTEROL_THRESHOLD = 199;
        public const int HDL_MAN_THRESHOLD = 40;
        public const int HDL_WOMAN_THRESHOLD = 50;
        public const int LDL_THRESHOLD = 128;
        public const int FASTING_GLUCOSE_THRESHOLD = 125;

        public static int CalculateTotalCholesterol(int intLdl, int intHDL, int intTriglycerides)
        {
            int intTC = intHDL + intLdl + (intTriglycerides / 5);
            return intTC;
        }

        public static void ScrollToTopOfThePageAfterAjaxPostback()
        {
            ScriptManager.RegisterStartupScript((System.Web.HttpContext.Current.Handler as Page), (System.Web.HttpContext.Current.Handler as Page).GetType(), "JS_Valid", "setTimeout(\"window.scrollTo(0,0)\", 0);", true);
        }

        public static List<PatientGuids> GetPatientGuidList(List<AHAHelpContent.Patient> listPatient)
        {
            if (listPatient == null)
                return new List<PatientGuids>();

            return listPatient.Select(i => new PatientGuids { PersonID = i.OfflinePersonGUID.Value, RecordID = i.OfflineHealthRecordGUID.Value }).ToList();
        }

        public static ListItemCollection GetAllRulesDataTypes()
        {
            ListItemCollection objList = new ListItemCollection();
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_BPSystolic").ToString(), GroupManager.RuleType.Systolic.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_BPSystolicDiastolic").ToString(), GroupManager.RuleType.Diastolic.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_CholesterolTotal").ToString(), GroupManager.RuleType.TotalCholesterol.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_CholesterolHDL").ToString(), GroupManager.RuleType.HDL.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_CholesterolLDL").ToString(), GroupManager.RuleType.LDL.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_Triglycerides").ToString(), GroupManager.RuleType.Triglyceride.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_BloodGlucose").ToString(), GroupManager.RuleType.BloodGlucose.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_Weight").ToString(), GroupManager.RuleType.Weight.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_BMI").ToString(), GroupManager.RuleType.BMI.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_ActivePatient").ToString(), GroupManager.RuleType.ActivePatient.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_InActivePatient").ToString(), GroupManager.RuleType.InactivePatient.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(System.Web.HttpContext.GetGlobalResourceObject("Provider", "Provider_RuleDataTypes_PatientConnection").ToString(), GroupManager.RuleType.PatientConnection.ToString()));
            return objList;
        }

        public static bool IsScientificSessionsPage()
        {
            if (GRCBase.UrlUtility.FullCurrentPageUrl.ToLower().IndexOf("/scientificsession/") != -1)
                return true;
            return false;
        }

        public static string GetScientificSessionsQueryString()
        {
            return "ss=1";
        }

        public static DateTime? GetUserDate()
        {
            try
            {
                if (System.Web.HttpContext.Current.Request["hidUserDate"] != null)
                {
                    string[] strDateArr = System.Web.HttpContext.Current.Request["hidUserDate"].Split(':');
                    if (strDateArr.Length != 3)
                    {
                        return null;
                    }
                    else
                    {
                        return new DateTime(Convert.ToInt32(strDateArr[2]),
                            Convert.ToInt32(strDateArr[1]),
                            Convert.ToInt32(strDateArr[0]));
                    }
                }
            }
            catch (Exception e)
            {
                GRCBase.ErrorLogHelper.LogAndIgnoreException(e);
            }

            return null;
        }

        public static void ShowEditDiv(Page pg, string divEditId)
        {
            ScriptManager.RegisterStartupScript(pg, pg.GetType(), "JS_EditDiv", "ShowEditDiv('" + divEditId + "',400);", true);
        }

        public static void HideEditDiv(Page pg)
        {
            ScriptManager.RegisterStartupScript(pg, pg.GetType(), "JS_EditDiv", "HideEditDiv();", true);
        }

        public static bool IsPrinterFriendlyPage()
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request["PrinterFriendly"])
                    && System.Web.HttpContext.Current.Request["PrinterFriendly"] == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string EncloseWithinHtmlTag(string str, string strHtmlTag)
        {
            return String.Format("<{0}>" + str + "</{0}>", strHtmlTag); ;
        }

        public static string EncloseWithinHtmlTag(string str, string strHtmlTag, string strStyleClassName)
        {
            return String.Format("<{0} class=\"{1}\">" + str + "</{0}>", strHtmlTag, strStyleClassName); ;
        }

        public static string StyleAndReturnAlertMessage(string strAlert, bool bHomePage)
        {
            if (string.IsNullOrEmpty(strAlert))
                return string.Empty;

            if (bHomePage)
                return string.Format("<span class=\"NormalItalics\">{0}</span>", strAlert);

            return string.Format("<span class=\"NormalBoldRed\">Notification:</span>&nbsp;<span class=\"NormalItalics\">{0}</span>", strAlert);
        }

        public static List<AHAHelpContent.ListItem> FindListItemsFromHVVocByListCode(string strName)
        {
            List<AHAHelpContent.ListItem> objListItems = new List<AHAHelpContent.ListItem>();
            List<VocabularyItem> vocab = null;
            switch (strName)
            {
                case AHACommon.AHADefs.VocabDefs.Ethnicity:
                    vocab = HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_ETHNICITY_KEY, HVManager.VocabHelper.STR_FAMILY_WC);
                    if (vocab != null)
                    {
                        foreach (VocabularyItem vocabItem in vocab)
                        {
                            objListItems.Add(new AHAHelpContent.ListItem { ListItemName = vocabItem.DisplayText, ListItemCode = vocabItem.Value });
                        }
                    }
                    break;
                case AHACommon.AHADefs.VocabDefs.NameSuffixes:
                    vocab = HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_NAME_SUFFIXES_KEY, HVManager.VocabHelper.STR_FAMILY_WC);
                    if (vocab != null)
                    {
                        foreach (VocabularyItem vocabItem in vocab)
                        {
                            objListItems.Add(new AHAHelpContent.ListItem { ListItemName = vocabItem.DisplayText, ListItemCode = vocabItem.Value });
                        }
                    }
                    break;
                case AHACommon.AHADefs.VocabDefs.NamePrefixes:
                    vocab = HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_NAME_PREFIXES_KEY, HVManager.VocabHelper.STR_FAMILY_WC);
                    if (vocab != null)
                    {
                        foreach (VocabularyItem vocabItem in vocab)
                        {
                            objListItems.Add(new AHAHelpContent.ListItem { ListItemName = vocabItem.DisplayText, ListItemCode = vocabItem.Value });
                        }
                    }
                    break;
                case AHACommon.AHADefs.VocabDefs.RelationshipStatus:
                    vocab = HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_MARITAL_STATUS_KEY, HVManager.VocabHelper.STR_FAMILY_WC);
                    if (vocab != null)
                    {
                        foreach (VocabularyItem vocabItem in vocab)
                        {
                            objListItems.Add(new AHAHelpContent.ListItem { ListItemName = vocabItem.DisplayText, ListItemCode = vocabItem.Value });
                        }
                    }
                    break;
                case AHACommon.AHADefs.VocabDefs.States:
                    vocab = HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_STATES_KEY, HVManager.VocabHelper.STR_FAMILY_WC);
                    if (vocab != null)
                    {
                        foreach (VocabularyItem vocabItem in vocab)
                        {
                            objListItems.Add(new AHAHelpContent.ListItem { ListItemName = vocabItem.DisplayText, ListItemCode = vocabItem.Value });
                        }
                    }
                    break;
                case AHACommon.AHADefs.VocabDefs.Countries:
                    vocab = HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_COUNTRY_KEY, HVManager.VocabHelper.STR_FAMILY_ISO, HVManager.VocabHelper.STR_VERSION_1_0);
                    if (vocab != null)
                    {
                        foreach (VocabularyItem vocabItem in vocab)
                        {
                            objListItems.Add(new AHAHelpContent.ListItem { ListItemName = vocabItem.DisplayText, ListItemCode = vocabItem.Value });
                        }
                    }
                    break;
                case AHACommon.AHADefs.VocabDefs.BloodType:
                    vocab = HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_BLOOD_TYPE_KEY, HVManager.VocabHelper.STR_FAMILY_WC);
                    if (vocab != null)
                    {
                        foreach (VocabularyItem vocabItem in vocab)
                        {
                            objListItems.Add(new AHAHelpContent.ListItem { ListItemName = vocabItem.DisplayText, ListItemCode = vocabItem.Value });
                        }
                    }
                    break;
            }

            objListItems.Sort(new ListItemsComparer(true));
            return objListItems;
        }

        /// <summary>
        /// Returns hexadecimal string of MD5 hashed string
        /// http://msdn.microsoft.com/en-us/library/system.security.cryptography.md5cryptoserviceprovider.aspx
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetHashString(string input)
        {
            try
            {
                // Create a new instance of the MD5CryptoServiceProvider object.
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();

            }
            catch (Exception e)
            {
                throw new Exception("An Error occurred:-" + e.Message);
            }
        }

        //public static bool IsGUID(string expression)
        //{
        //    if (expression != null)
        //    {
        //        Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");

        //        return guidRegEx.IsMatch(expression);
        //    }
        //    return false;
        //}

        public const string STRING_INVALID_PASSWORD = "Password must have at least 1 number, 1 special character, and more than 6 characters.";
        public static bool IsVaidProviderPassword(string strPassword)
        {
            if (string.IsNullOrEmpty(strPassword))
                return false;

            Match passwordstrong = Regex.Match(strPassword, "(?=^.{6,}$)(?=.*\\d)(?=.*\\W+)(?![.\n]).*$");
            if (!passwordstrong.Success)
            {
                return false;
            }

            return true;
        }

        public static DateTime GetTramsformedDate(DataDateReturnType type)
        {
            switch (type)
            {
                case DataDateReturnType.LastWeek:
                    return DateTime.Now.AddDays(-7);

                case DataDateReturnType.LastMonth:
                    return DateTime.Now.AddMonths(-1);

                case DataDateReturnType.Last3Months:
                    return DateTime.Now.AddMonths(-3);

                case DataDateReturnType.Last6Months:
                    return DateTime.Now.AddMonths(-6);
            }

            return DateTime.MinValue;
        }

        public static string GetAge(DateTime? dtBirthYear)
        {
            if (dtBirthYear.HasValue)
            {
                TimeSpan timespan = DateTime.Now.ToUniversalTime().Subtract(dtBirthYear.Value.ToUniversalTime());
                float fOneYear = 365.25f;
                if (timespan.Days < fOneYear * 2)
                {
                    int iMonths = timespan.Days / 30;
                    return string.Format("{0} month{1}", iMonths.ToString(), iMonths == 1 ? string.Empty : "s");
                }
                else
                {
                    int iYears = (int)(timespan.Days / fOneYear);
                    return iYears.ToString();
                }
            }
            return string.Empty;
        }

        public static string GetBloodGlucoseReadingType(string str)
        {
            if (!string.IsNullOrEmpty(str) && str.ToLower().CompareTo("postprandial") == 0)
            {
                return "After Meal";
            }

            return str;
        }


        public static string RenderLink(string strContent)
        {
            //Construct Links
            if (!string.IsNullOrEmpty(strContent))
            {
                strContent = Regex.Replace(strContent,
                    "\\[(L|l)(I|i)(N|n)(K|k)(\\s)*(T|t)(E|e)(X|x)(T|t)(\\s)*=(\\s)*'.*'(\\s)*(U|u)(R|r)(L|l)(\\s)*=(\\s)*'.*'(\\s)*\\]",
                    delegate(Match match)
                    {
                        string strMatchText = match.Value;
                        strMatchText = Regex.Replace(strMatchText, "\\[(L|l)(I|i)(N|n)(K|k)(\\s)*", string.Empty);
                        strMatchText = Regex.Replace(strMatchText, "(\\s)*\\]", string.Empty);

                        //Remove unwanted space
                        strMatchText = Regex.Replace(strMatchText, "(\\s)*=(\\s)*", "=");
                        strMatchText = Regex.Replace(strMatchText, "'(\\s)+", "',");

                        string strText = string.Empty;
                        string strHref = string.Empty;

                        strMatchText.Split((",".ToCharArray())).ToList().ForEach(delegate(string strAttributes)
                        {
                            if (strAttributes.ToLower().Contains("text"))
                                strText = Regex.Replace(strAttributes, "(T|t)(E|e)(X|x)(T|t)(\\s)*=(\\s)*", string.Empty).Replace("'", string.Empty);

                            if (strAttributes.ToLower().Contains("url"))
                                strHref = Regex.Replace(strAttributes, "(U|u)(R|r)(L|l)(\\s)*=(\\s)*", string.Empty).Replace("'", string.Empty);
                        });

                        if (strHref.ToLower().StartsWith("#"))
                        {
                            return string.Format("<a href=\"{0}\">{1}</a>", strHref, strText);
                        }
                        else
                        {
                            if (!strHref.ToLower().StartsWith("http://"))
                            {
                                strHref = strHref.Insert(0, "http://");
                            }
                        }

                        return string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", strHref, strText);
                    });
            }

            return strContent;
        }

        public static void ProcessForRedirectionToWizardPage(int iCurrentStep)
        {
            AHAAPI.UserIdentityContext objUserIdentity = AHAAPI.SessionInfo.GetUserIdentityContext();
            WizardDetails objWizardDetails = WizardDetails.FindWizardDetailsForRecord(objUserIdentity.PersonalItemGUID);
            if (objWizardDetails != null)
            {
                if (objWizardDetails != null
                    && objWizardDetails.WizardStepIndex.HasValue
                    && objWizardDetails.WizardStepIndex.Value != iCurrentStep)
                {
                    //RedirectToWizardPage(objWizardDetails.WizardStepIndex.Value, objWizardDetails.WizardStepName);
                }
            }
        }

        /** PJB: out of scope for V6
        public static void RedirectToWizardPage(int iStep, string strWizardStepName)
        {
            AHAAPI.TrackerType eTrackerType = (AHAAPI.TrackerType)Enum.Parse(typeof(AHAAPI.TrackerType), strWizardStepName, true);
            string strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_STEP1;
            switch (iStep)
            {
                case 0:
                    strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_START;
                    break;
                case 1:
                    strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_STEP1;
                    break;
                case 2:
                    strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_STEP2;
                    break;
                case 3:
                    strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_STEP3;
                    break;
                case 4:
                    strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_STEP4;
                    break;
                case 5:
                    strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_STEP5;
                    break;
                case 6:
                    strUrl = WebAppNavigation.H360.PAGE_PATIENT_WIZARD_STEP6;
                    break;
            }

            System.Web.HttpContext.Current.Response.Redirect(string.Format(strUrl, eTrackerType), true);
        }
         **/

        //public static string GetTrackerTypeDescription(TrackerType type)
        //{
        //    string strRK = String.Format("Text_{0}", type);

        //    //this will also work
        //    //ResourceManager rm = new System.Resources.ResourceManager("Resources.Patient", System.Reflection.Assembly.Load("App_GlobalResources"));
        //    //string strlocalizedDescription = Resources.Common.ResourceManager.GetString(strRK);

        //    //if (strlocalizedDescription == null)
        //    //{
        //    //    return GRCBase.MiscUtility.GetComponentModelPropertyDescription(type);
        //    //}
        //    //else
        //    //{
        //    //    return strlocalizedDescription;
        //    //}
        
        //}

        //public static string GetLastReadingChangeTypeDescription(LastReadingChange type)
        //{
        //    string strRK = String.Format("Text_{0}", type);

        //    //this will also work
        //    //ResourceManager rm = new System.Resources.ResourceManager("Resources.Patient", System.Reflection.Assembly.Load("App_GlobalResources"));
        //    string strlocalizedDescription = Resources.Common1.ResourceManager.GetString(strRK);

        //    if (strlocalizedDescription == null)
        //    {
        //        return GRCBase.MiscUtility.GetComponentModelPropertyDescription(type);
        //    }
        //    else
        //    {
        //        return strlocalizedDescription;
        //    }
        //}

        //public static string GetRiskLevelResultTypeEnumTypeDescription(Heart360Web.RiskLevelHelper.RiskLevelResultTypeEnum type)
        //{
        //    string strRK = String.Format("Text_Risk_{0}", type);

        //    //this will also work
        //    //ResourceManager rm = new System.Resources.ResourceManager("Resources.Patient", System.Reflection.Assembly.Load("App_GlobalResources"));
        //    string strlocalizedDescription = Resources.Common1.ResourceManager.GetString(strRK);

        //    if (strlocalizedDescription == null)
        //    {
        //        return GRCBase.MiscUtility.GetComponentModelPropertyDescription(type);
        //    }
        //    else
        //    {
        //        return strlocalizedDescription;
        //    }
        //}

        public static List<SummaryOption> GetDashboardSortOrderListForCurrentUser(Guid gPersonGUID, Guid gHealthRecordGUID)
        {
            string strXml = AHAHelpContent.AHAUser.FetchDashboardSortOrderForAHAuserHealthRecord(gPersonGUID, gHealthRecordGUID);
            SummaryOptionList list = null;
            if (!string.IsNullOrEmpty(strXml))
            {
                list = SummaryOption.GetObject(strXml);
            }

            List<int> listIgnoredPositions = new List<int>();
            if (list != null)
            {
                List<int> listAllottedPositions = new List<int>();
                List<SummaryOption> listItemsWithNoOrSamePosition = new List<SummaryOption>();
                foreach (SummaryOption objSummaryOption in list.Items)
                {
                    int iValid = -1;
                    bool bIsValid = Int32.TryParse(objSummaryOption.Position, out iValid);
                    if (!bIsValid)
                    {
                        list.Items.Where(i => i.Type == objSummaryOption.Type).SingleOrDefault().Position = string.Empty;
                    }
                }

                foreach (SummaryOption objSummaryOption in list.Items)
                {
                    if (string.IsNullOrEmpty(objSummaryOption.Position))
                    {
                        continue;
                    }

                    if (!listAllottedPositions.Contains(Convert.ToInt32(objSummaryOption.Position)))
                    {
                        listAllottedPositions.Add(Convert.ToInt32(objSummaryOption.Position));
                    }
                    else
                    {
                        if (!listIgnoredPositions.Contains(Convert.ToInt32(objSummaryOption.Position)))
                        {
                            listIgnoredPositions.Add(Convert.ToInt32(objSummaryOption.Position));
                        }
                    }
                }
                listAllottedPositions = listAllottedPositions.Where(i => !listIgnoredPositions.Contains(i) && i > 0 && i < 7).OrderBy(i => i).ToList();
                listItemsWithNoOrSamePosition = list.Items.Where(i =>
                    string.IsNullOrEmpty(i.Position)
                    || (Convert.ToInt32(i.Position) > 6 && Convert.ToInt32(i.Position) < 1)
                    || !listAllottedPositions.Contains(Convert.ToInt32(i.Position))
                    ).ToList();

                bool bHasPositionChanged = false;
                if (listItemsWithNoOrSamePosition != null && listItemsWithNoOrSamePosition.Count > 0)
                {
                    foreach (SummaryOption objSummaryOption in listItemsWithNoOrSamePosition)
                    {
                        int iPos = _getNewPosition(listAllottedPositions);
                        listAllottedPositions.Add(iPos);
                        listAllottedPositions = listAllottedPositions.OrderBy(i => i).ToList();
                        list.Items.Where(i => i.Type == objSummaryOption.Type).SingleOrDefault().Position = iPos.ToString();
                    }
                    bHasPositionChanged = true;
                }
                if (bHasPositionChanged)
                {
                    strXml = SummaryOption.GetXml(list);
                    AHAHelpContent.AHAUser.ModifyAHAuserHealthRecordDashBoard(gPersonGUID, gHealthRecordGUID, strXml);
                }
                return list.Items;
            }

            return null;
        }

        private static int _getNewPosition(List<int> listAllottedPositions)
        {
            for (int i = 0; i < 6; i++)
            {
                if ((i + 1) <= listAllottedPositions.Count
                    && listAllottedPositions[i] == (i + 1))
                {
                    continue;
                }
                else
                {
                    return (i + 1);
                }
            }

            throw new Exception("An error occurred while allotting new dashboard position");
        }

        public static void OrganizeDashBoard(bool bOrganizeDashBoardMode, Control objControl, Guid gPersonGUID, Guid gHealthRecordGUID)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("var _SummaryCtrl = new SummaryCtrl({0},'{1}','{2}');", bOrganizeDashBoardMode.ToString().ToLower(), gPersonGUID, gHealthRecordGUID);
            sb.Append("var arrSummary = new Array();");
            List<SummaryOption> list = UIHelper.GetDashboardSortOrderListForCurrentUser(gPersonGUID, gHealthRecordGUID);
            if (list != null)
            {
                SummaryOption objBP = list.Where(i => i.Type == "BP").SingleOrDefault();
                SummaryOption objBG = list.Where(i => i.Type == "BG").SingleOrDefault();
                SummaryOption objCholesterol = list.Where(i => i.Type == "Cholesterol").SingleOrDefault();
                SummaryOption objWeight = list.Where(i => i.Type == "Weight").SingleOrDefault();
                SummaryOption objMedication = list.Where(i => i.Type == "Medication").SingleOrDefault();
                SummaryOption objExercise = list.Where(i => i.Type == "Exercise").SingleOrDefault();

                sb.AppendFormat("_SummaryCtrl.ArrSummary[0] = new SymmaryType({0}, \"BP\", \"Div1Child\", {1});", objBP.Position, objBP.IsVisible);
                sb.AppendFormat("_SummaryCtrl.ArrSummary[1] = new SymmaryType({0}, \"BG\", \"Div2Child\", {1});", objBG.Position, objBG.IsVisible);
                sb.AppendFormat("_SummaryCtrl.ArrSummary[2] = new SymmaryType({0}, \"Cholesterol\", \"Div3Child\", {1});", objCholesterol.Position, objCholesterol.IsVisible);
                sb.AppendFormat("_SummaryCtrl.ArrSummary[3] = new SymmaryType({0}, \"Weight\", \"Div4Child\", {1});", objWeight.Position, objWeight.IsVisible);
                sb.AppendFormat("_SummaryCtrl.ArrSummary[4] = new SymmaryType({0}, \"Medication\", \"Div5Child\", {1});", objMedication.Position, objMedication.IsVisible);
                sb.AppendFormat("_SummaryCtrl.ArrSummary[5] = new SymmaryType({0}, \"Exercise\", \"Div6Child\", {1});", objExercise.Position, objExercise.IsVisible);
            }
            else
            {
                sb.Append("_SummaryCtrl.ArrSummary[0] = new SymmaryType(1, \"BP\", \"Div1Child\", true);");
                sb.Append("_SummaryCtrl.ArrSummary[1] = new SymmaryType(2, \"BG\", \"Div2Child\", true);");
                sb.Append("_SummaryCtrl.ArrSummary[2] = new SymmaryType(3, \"Cholesterol\", \"Div3Child\", true);");
                sb.Append("_SummaryCtrl.ArrSummary[3] = new SymmaryType(4, \"Weight\", \"Div4Child\", true);");
                sb.Append("_SummaryCtrl.ArrSummary[4] = new SymmaryType(5, \"Medication\", \"Div5Child\", true);");
                sb.Append("_SummaryCtrl.ArrSummary[5] = new SymmaryType(6, \"Exercise\", \"Div6Child\", true);");
            }

            sb.AppendFormat("_SummaryCtrl.RenderSummary();");
            if (bOrganizeDashBoardMode)
            {
                sb.AppendFormat("RegisterEventForClose();");
            }

            ScriptManager.RegisterStartupScript(objControl, objControl.Page.GetType(), "JS_OD", sb.ToString(), true);
        }

        public static bool ProcessProviderCodeOnlyTypeInvitationForPatient(string strProviderCode, int iPatientID, Guid gPersonGUID, Guid gRecordID)
        {
            ProviderDetails objProviderDetails = ProviderDetails.FindByProviderCode(strProviderCode);
            if (objProviderDetails != null)
            {
                bool bAlreadyConnected = ProviderDetails.IsValidPatientForProvider(iPatientID, objProviderDetails.ProviderID);

                if (!bAlreadyConnected)
                {
                    DBContext dbContext = null;
                    bool bException = false;
                    try
                    {
                        dbContext = DBContext.GetDBContext();

                        HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordID);

                        string strFirstName = mgr.Name;
                        string strLastName = mgr.Name;

                        HVManager.PersonalDataManager.PersonalItem item = mgr.PersonalDataManager.Item;
                        if (item != null && !string.IsNullOrEmpty(item.FirstName.Value))
                        {
                            strFirstName = item.FirstName.Value;
                        }

                        if (item != null && !string.IsNullOrEmpty(item.LastName.Value))
                        {
                            strLastName = item.LastName.Value;
                        }

                        PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(iPatientID, objProviderDetails.ProviderID, true);


                        PatientProviderInvitation CurrentInvitation = PatientProviderInvitation.CreatePatientInvitation(
                                            strFirstName,
                                            strLastName,
                                            string.Empty,
                                            objProviderDetails.ProviderID,
                                            false);


                        PatientProviderInvitationDetails objAvailableDetails = CurrentInvitation.PendingInvitationDetails.First();
                        objAvailableDetails.AcceptedPatientID = iPatientID;
                        objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.ProviderCodeOnly;
                        objAvailableDetails.Update();
                    }
                    catch
                    {
                        bException = true;
                        throw;
                    }
                    finally
                    {
                        try
                        {
                            if (bException)
                            {
                                if (dbContext != null)
                                {
                                    dbContext.ReleaseDBContext(false);
                                }
                            }
                            else
                            {
                                dbContext.ReleaseDBContext(true);
                            }
                        }
                        catch
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                            throw;
                        }
                    }
                    return true;
                }
            }

            return false;
        }

    }
}
