﻿using System;
using System.Linq;
using System.Collections.Generic;
using AHAHelpContent;
using HVManager;

namespace Heart360Web
{
    public class MessageInfo : Message
    {
        private string m_Subject;
        private string m_Body;
        private bool _SubjectAndBodyLoaded = false;

        private UserTypeDetails CurrentUserDetails
        {
            get;
            set;
        }

        private string CurrentFolderType
        {
            get;
            set;
        }

        private void _LoadSubjectAndBody()
        {
            if (!_SubjectAndBodyLoaded)
            {
                if (this.CurrentUserDetails.UserType == UserType.Provider)
                {
                    HVManager.MessageDataManager.MessageItem objMessageItem = MessageWrapper.GetMessageByIdForProvider(this.CurrentUserDetails.UserID, this.MessageID);
                    if (objMessageItem == null)
                    {
                        //had an issue where a provider was able to send messages to all patients/grop when the ptovider 
                        //had no patiens, in such cases, the message is null and the patient id is 0
                        //the fix is to NOT CHEck for that message agaian becasue the patient id is 0 and we do not have a OfflinePersonGUID
                        if (this.PatientID > 0)
                        {
                            AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(this.PatientID);
                            objMessageItem = MessageWrapper.FetchMessageAgainByIdForProvider(objPatient.OfflinePersonGUID.Value,
                                                    objPatient.OfflineHealthRecordGUID.Value,
                                                    this.CurrentUserDetails.UserID,
                                                    this.MessageID);

                        }
                    }

                    m_Subject = objMessageItem != null ? objMessageItem.Subject.Value : "-No Subject-";
                    m_Body = objMessageItem != null ? objMessageItem.MessageBody.Value.Replace("\n", "<br>") : string.Empty;
                }
                else
                {
                    MessageDataManager.MessageItem objMessageItem = MessageWrapper.GetMessageByIdForPatient(this.MessageID);

                    if (objMessageItem == null)
                    {
                        objMessageItem = MessageWrapper.FetchMessageAgainByIdForPatient(this.MessageID);
                    }

                    m_Subject = objMessageItem != null ? objMessageItem.Subject.Value : "-No Subject-";

                    m_Body = objMessageItem != null ? objMessageItem.MessageBody.Value.Replace("\n", "<br>") : string.Empty;
                }
            }
        }

        // PJB: had a case where user "billtester" had a message from a provider that was not (no longer?) in the database.
        // So handling that case and returning "Unknown" as FormattedName
        private string ProviderFormattedName
        {
            get
            {

                ProviderDetails pd = AHAHelpContent.ProviderDetails.FindByProviderID(this.ProviderID);
                return (pd != null) ? pd.FormattedName : "Unknown";
            }
        }

        public string From
        {
            get
            {
                if (this.CurrentFolderType == AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX)
                {
                    if (this.CurrentUserDetails.UserType == UserType.Patient)
                    {
                        return ProviderFormattedName;
                    }
                    else
                    {
                        AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(this.PatientID);

                        HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                        HVManager.PersonalDataManager.PersonalItem objPersonalInfo = mgr.PersonalDataManager.Item;

                        return string.Format("{0} {1}", objPersonalInfo.FirstName.Value, objPersonalInfo.LastName.Value);
                    }
                }
                else
                {
                    if (this.CurrentUserDetails.UserType == UserType.Patient)
                    {
                        return CurrentUser.FullName;
                    }
                    else
                    {
                        return ProviderFormattedName;
                    }
                }
            }
        }

        public string To
        {
            get
            {
                if (this.CurrentFolderType == AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX)
                {
                    if (this.CurrentUserDetails.UserType == UserType.Patient)
                    {
                        return CurrentUser.FullName;
                    }
                    else
                    {
                        return ProviderFormattedName;
                    }
                }
                else
                {
                    if (this.CurrentUserDetails.UserType == UserType.Patient)
                    {
                        return ProviderFormattedName;
                    }
                    else
                    {
                        if (this.GroupID.HasValue)
                        {
                            AHAHelpContent.PatientGroup objGroup = AHAHelpContent.PatientGroup.FindByGroupID(this.GroupID.Value);
                            
                            if (objGroup.IsDefault)
                                return "All Patients";

                            return objGroup.Name;
                        }

                        AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(this.PatientID);

                        HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                        HVManager.PersonalDataManager.PersonalItem objPersonalInfo = mgr.PersonalDataManager.Item;

                        return string.Format("{0} {1}", objPersonalInfo.FirstName.Value, objPersonalInfo.LastName.Value);                        
                    }
                }
            }
        }

        public string Subject
        {
            get
            {
                _LoadSubjectAndBody();

                return m_Subject;
            }
            set
            {
                this.m_Subject = value;
            }

        }

        public string MessageBody
        {
            get
            {
                _LoadSubjectAndBody();

                return m_Body;
            }
            set
            {
                this.m_Body = value;
            }
        }

        public static List<MessageInfo> PopulateMessages(List<Message> objMessageList, UserTypeDetails utdCurrentUserDetails, string strCurrentFolderType)
        {
            List<MessageInfo> objMessageInfoList = new List<MessageInfo>();
            objMessageList.ForEach(delegate(Message objMessage)
            {
                objMessageInfoList.Add(new MessageInfo()
                {
                    CurrentUserDetails = utdCurrentUserDetails,
                    CurrentFolderType = strCurrentFolderType,
                    IsFlaged = objMessage.IsFlaged,
                    IsRead = objMessage.IsRead,
                    MessageID = objMessage.MessageID,
                    MessageType = objMessage.MessageType,
                    PatientID = objMessage.PatientID,
                    ProviderID = objMessage.ProviderID,
                    SentDate = objMessage.SentDate,
                    GroupID = objMessage.GroupID
                });
            });

            return objMessageInfoList;
        }

        public static MessageInfo GetMessageInfoObject(Message objMessage, UserTypeDetails utdCurrentUserDetails, string strCurrentFolderType)
        {
            MessageInfo objMessageInfo = new MessageInfo()
            {
                CurrentUserDetails = utdCurrentUserDetails,
                CurrentFolderType = strCurrentFolderType,
                IsFlaged = objMessage.IsFlaged,
                IsRead = objMessage.IsRead,
                MessageID = objMessage.MessageID,
                MessageType = objMessage.MessageType,
                PatientID = objMessage.PatientID,
                ProviderID = objMessage.ProviderID,
                SentDate = objMessage.SentDate,
                GroupID = objMessage.GroupID
            };

            return objMessageInfo;
        }
    }
}
