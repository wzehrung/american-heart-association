﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using AHAAPI;

namespace Heart360Web
{
    [XmlRoot("TrackerDetails")]
    public class TrackerItemList
    {
        private List<TrackerItem> list;

        public TrackerItemList()
        {
            list = new List<TrackerItem>();
        }

        [XmlElement("Tracker")]
        public List<TrackerItem> Items
        {
            get
            {
                return list;
            }
            set
            {
                list = value;
            }
        }
    }

    public class TrackerItem
    {
        public string Type
        {
            get;
            set;
        }

        public int IsGraphVisible
        {
            get;
            set;
        }

        /// <summary>
        /// Deserialize List
        /// </summary>
        /// <param name="strXml"></param>
        /// <returns></returns>
        public static TrackerItemList GetObject(string strXml)
        {
            if (string.IsNullOrEmpty(strXml))
                return null;

            TrackerItemList list = null;

            XmlSerializer serializer = new XmlSerializer(typeof(TrackerItemList));
            using (StringReader reader = new StringReader(strXml))
            {
                list = (TrackerItemList)serializer.Deserialize(reader);
                reader.Close();
            }

            return list;
        }

        /// <summary>
        /// Serialize List
        /// </summary>
        /// <returns></returns>
        public static string GetXml(TrackerItemList list)
        {
            if (list == null || list.Items.Count == 0)
                return null;

            XmlSerializer serializer = new XmlSerializer(list.GetType());

            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.OmitXmlDeclaration = true;
            string strXML = string.Empty;
            using (StringWriter stringWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, writerSettings))
                {
                    serializer.Serialize(xmlWriter, list);
                    xmlWriter.Close();
                }

                strXML = stringWriter.ToString();
                stringWriter.Close();
            }
            return strXML;
        }
    }

    public class TrackerCustomization
    {
        public static void UpdateTrackerDetails(AHAAPI.TrackerType eTrackerType, int iIsGraphVisible)
        {
            UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
            TrackerItemList list = null;
            bool bItemFound = false;
            string strTrackerType = eTrackerType.ToString();
            if (userIdentityContext != null)
            {
                string strTrackerDetailsXml = AHAHelpContent.AHAUser.FetchTrackerDetailsForAHAuserHealthRecord(userIdentityContext.UserId, userIdentityContext.RecordId);
                if (!string.IsNullOrEmpty(strTrackerDetailsXml))
                {
                    list = TrackerItem.GetObject(strTrackerDetailsXml);
                    if (list != null && list.Items != null && list.Items.Count > 0)
                    {
                        TrackerItem item = list.Items.Where(i => i.Type == strTrackerType).SingleOrDefault();
                        if (item != null)
                        {
                            bItemFound = true;
                            list.Items.Where(i => i.Type == strTrackerType).SingleOrDefault().IsGraphVisible = iIsGraphVisible;
                        }
                    }
                }

                if (list == null)
                {
                    list = new TrackerItemList();
                }

                if (!bItemFound)
                {
                    TrackerItem item = new TrackerItem { IsGraphVisible = iIsGraphVisible, Type = strTrackerType };
                    list.Items.Add(item);
                }

                AHAHelpContent.AHAUser.ModifyAHAuserHealthRecordTrackerDetails(userIdentityContext.UserId, userIdentityContext.RecordId, TrackerItem.GetXml(list));
            }
        }
    }
}
