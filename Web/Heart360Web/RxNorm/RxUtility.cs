﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;

namespace Heart360Web.RxNorm
{
    public static class RxUtility
    {
        //private static DataTable tableTerms = new DataTable();
        //private static bool bIsDataTableLoaded = false;


        //private static void _EnsureTableLoaded()
        //{
        //    if (bIsDataTableLoaded)
        //        return;

        //    lock (tableTerms)
        //    {
        //        if (bIsDataTableLoaded)
        //            return;

        //        tableTerms.Columns.Add("Term", typeof(string));

        //        //using (DBManagerService db = new DBManagerService())
        //        //{
        //        //    string[] terms = db.getDisplayTerms();
        //        //    foreach (string term in terms)
        //        //    {
        //        //        DataRow r = tableTerms.NewRow();
        //        //        r["Term"] = term;
        //        //        tableTerms.Rows.Add(r);
        //        //    }
        //        //}

        //        loadTableFromFile();

        //        //saveTableToFile();

        //        bIsDataTableLoaded = true;
        //    }
        //}


        //private static void saveTableToFile()
        //{
        //    using (StreamWriter sw = new StreamWriter(System.Web.HttpContext.Current.Server.MapPath("~/RxNorm/RxTerms.txt"), false))
        //    {
        //        foreach (DataRow row in tableTerms.Rows)
        //        {
        //            sw.WriteLine(row["Term"].ToString());
        //        }
        //    }
        //}

        //private static void loadTableFromFile()
        //{
        //    using (StreamReader sr = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/RxNorm/RxTerms.txt")))
        //    {
        //        string line = sr.ReadLine();
        //        while (!sr.EndOfStream)
        //        {
        //            DataRow r = tableTerms.NewRow();
        //            r["Term"] = line;
        //            tableTerms.Rows.Add(r);
        //            line = sr.ReadLine();
        //        }
        //    }
        //}

        public static string[] GetAutoSuggest(string name)
        {
            //_EnsureTableLoaded();
            var rows = AHAHelpContent.RxTerm.GetAllRxTerms().Select("Term like '" + name + "%'");
            return rows.Select(x => x["Term"].ToString()).ToArray();
        }

        public static string[] GetSpellingSuggestions(string prefix)
        {
            using (DBManagerService db = new DBManagerService())
            {
                return db.getSpellingSuggestions(prefix);
            }
        }

        public static bool DoesExactRxMatchExist(string rxText)
        {
            //_EnsureTableLoaded();
            var rows = AHAHelpContent.RxTerm.GetAllRxTerms().Select("Term = '" + rxText.Replace("'","''") + "'");
            return rows.Count() > 0;
        }
    }
}
