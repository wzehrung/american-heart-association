
using AHAAPI;
using System.Text;
using Microsoft.Health.Web;
using System.Web;
using System;

/********************************************************************++

Copyright (c) Microsoft Corporation. All rights reserved.

************************************************************************/

/// <summary>
/// We have set the following in the Web.Config:
/// <add key="NonProductionActionUrlRedirectOverride" value="Redirect.aspx"/>
/// This means that any time we go to the shell, it will redirect back to this 
/// page once it handles our request. The shell will attach a target parameter
/// in the URL that we can use to determine which page in our application we
/// want to redirect to. Microsoft.Health.Web.HealthServiceActionPage (which
/// this page extends) automatically handles processing the target param
/// in the URL. It uses keys (WCPage_ActionXXX) set in the Web.Config file 
/// to look up where to redirect based on the target param.
/// </summary>
public partial class Redirect : Microsoft.Health.Web.HealthServiceActionPage
{
    //We don't want this page to require log on because when we sign out, 
    //we still want this page to read the WCPage_ActionSignOut key in the
    //Web.Config and redirect us to the proper page on sign out
    protected override bool LogOnRequired
    {
        get
        {
            return false;
        }
    }

    public static string ActionQueryStringValue = "actionqs";
    public static string TargetQueryStringValue = "target";

    protected override void OnActionPrivacy(string action, string actionQueryString)
    {
        //base.OnActionPrivacy(action, actionQueryString);
        Response.Redirect(AHACommon.AHAAppSettings.FooterLinks.UseOfPersonalInformationLink);
        Response.End();
    }

    protected override void OnActionServiceAgreement(string action, string actionQueryString)
    {
        //base.OnActionServiceAgreement(action, actionQueryString);
        Response.Redirect(AHACommon.AHAAppSettings.TermsOfUse);         // <<<< TERMS OF USE
        Response.End();
    }

    protected override void OnActionSelectedRecordChanged(string action, string actionQueryString)
    {
        if (IsLoggedIn)
        {
            RefreshAndPersist();
        }
        else
        {
            //strange cases - unable to load person info from wctoken??
            StringBuilder sb = new StringBuilder(128);
            sb.Append("appid=");
            sb.Append(WebApplicationConfiguration.AppId.ToString());
            sb.Append("&actionqs=");
            sb.Append(HttpUtility.UrlEncode("/Pages/Patient/Dashboard.aspx"));
            WebApplicationUtilities.RedirectToShellUrl(Context,
                "APPAUTH",
                sb.ToString());
        }

        AHAAPI.PatientInvitation obj = AHAAPI.SessionInfo.GetPatientInvitationSession();
        if (obj != null && !obj.IsPatientAccept)
        {
            //redirect to accept invitation page
            Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_INVITATION_INVITE);
        }

        string redirectUrl = Request[ActionQueryStringValue];

        if (IsValidDomainUrl(redirectUrl))
        {
            _ProcessDefaultRedirect(redirectUrl);
            return;
        }

        // PJB - 12.10.2013 - PAGE_AUTHORIZE_MORE_RECORDS is a web page that does not exist in V6! 
        /**
        string targetLocation = WebAppNavigation.H360.PAGE_AUTHORIZE_MORE_RECORDS;
        
        if(!SessionInfo.IsUserLoggedIn)
            targetLocation = WebAppNavigation.H360.PAGE_VISITOR_DEFAULT;
         **/

        string targetLocation = WebAppNavigation.H360.PAGE_VISITOR_DEFAULT; // PJB: this is the new default

        Response.Redirect(targetLocation);
    }

    protected override void OnActionSignOut(string action, string actionQueryString)
    {
        if (string.IsNullOrEmpty(actionQueryString))
        {
            Response.Redirect(WebAppNavigation.H360.PAGE_VISITOR_DEFAULT);
            return;
        }

        if (Uri.IsWellFormedUriString(actionQueryString, UriKind.RelativeOrAbsolute))
        {
            if (IsValidDomainUrl(actionQueryString))
            {
                Response.Redirect(actionQueryString);
                return;
            }
        }

        Response.Redirect(WebAppNavigation.H360.PAGE_VISITOR_DEFAULT);
    }

    protected override void OnActionApplicationAuthorizationSuccessful(string action,
     string actionQueryString)
    {
        AHAAPI.PatientInvitation obj = AHAAPI.SessionInfo.GetPatientInvitationSession();
        if (obj != null)
        {
            if (obj.IsPatientAccept)
            {
                //redirect to accept invitation page
            }
            else
            {
                Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_INVITATION_INVITE);
            }
        }

        string redirectUrl = Request[ActionQueryStringValue];
        if (IsValidDomainUrl(redirectUrl))
        {
            _ProcessDefaultRedirect(redirectUrl);
            return;
        }

        string targetLocation = WebAppNavigation.H360.PAGE_PATIENT_HOME;
        Response.Redirect(targetLocation);
    }

    protected override void OnActionApplicationAuthorizationRejected(string action, string actionQueryString)
    {
        //we are redirecting the user to default home if the target=AppAuthReject
        string redirectUrl = Request[ActionQueryStringValue];
        if (IsValidDomainUrl(redirectUrl))
        {
            if (!SessionInfo.IsUserLoggedIn)
                redirectUrl = WebAppNavigation.H360.PAGE_VISITOR_DEFAULT;
            _ProcessDefaultRedirect(redirectUrl);
            return;
        }

        string targetLocation = WebAppNavigation.H360.PAGE_VISITOR_DEFAULT;
        Response.Redirect(targetLocation);
    }

    private void _ProcessDefaultRedirect(string urlToRedirect)
    {
        // PJB: handle new camp query parameter. Assume it is the only one!!!
        int indexofquery = urlToRedirect.IndexOf('?');
        if (indexofquery != -1 && urlToRedirect.IndexOf("campid=", indexofquery) != -1)
        {
            int campid = int.Parse(urlToRedirect.Substring(indexofquery + 8));
            AHAAPI.H360Utility.SetCampaignSession(campid);
            urlToRedirect = urlToRedirect.Substring(0, indexofquery);   // we are done with query parameter.
        }


        Response.Redirect(urlToRedirect);
    }

    /// <summary>
    /// Checks for Validity of a URL in parent domain
    /// </summary>
    /// <param name="url">URL</param>
    /// <returns>?</returns>
    public bool IsValidDomainUrl(string url)
    {
        if (string.IsNullOrEmpty(url))
            return false;

        if (url.StartsWith("http://", System.StringComparison.OrdinalIgnoreCase) ||
            url.StartsWith("https://", System.StringComparison.OrdinalIgnoreCase))
        {
            try
            {
                System.Uri uri = new System.Uri(url);
                // Uri.Host is always lower case
                return (string.Equals(
                    uri.Host,
                    Request.Url.Host));
            }
            catch
            {
                // Malformed? Either way, swallow
                return false;
            }
        }
        else if (url.StartsWith("/"))
            return true;
        
        return false;
    }    
}