﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;

namespace Heart360Web
{
    public partial class SignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /** PJB: this is not used
        protected void SignIn_Click(object sender, EventArgs e)
        {
            Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);
        }
        **/

        protected void CreateAcc_Click(object sender, EventArgs e)
        {
            string  dest = AHAAPI.WebAppNavigation.H360.PAGE_CONSENT_NO_APP;
            int?    campaignId = AHAAPI.H360Utility.GetCurrentCampaignID() ;

            if (campaignId != null)
            {
                dest = dest + "?campid=" + campaignId.ToString();
            }

            AHAAPI.H360Utility.RedirectToHVCreateAccountPage( dest );
            Response.End();

            //if (UIHelper.IsScientificSessionsPage())
            //{
            //    AHAAPI.H360Utility.RedirectToHVCreateAccountPage(AHAAPI.WebAppNavigation.H360.PAGE_CONSENT_NO_APP + "?" + UIHelper.GetScientificSessionsQueryString());
            //    Response.End();
            //}
            //else
            //{
            //    AHAAPI.H360Utility.RedirectToHVCreateAccountPage(WebAppNavigation.H360.PAGE_PATIENT_HOME_NO_APP);
            //    Response.End();
            //}
        }
    }
}