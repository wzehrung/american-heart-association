﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthGraph.ascx.cs" Inherits="Heart360Web.Controls.Common.HealthGraph" %>
<%@ Register Assembly="NPlot" Namespace="NPlot.Web" TagPrefix="GraphCtrl" %>
<%@ Register Assembly="Heart360Web" Namespace="Heart360Web.Controls.Common" TagPrefix="GraphCtrl" %>

<div id="divGraph"  runat="server" >
    <GraphCtrl:AHAPlotSurface2D ID="PlotSurface2D1" runat="server" />
</div>
<map id="<%=PlotSurface2D1.ImageMapName%>" name="<%=PlotSurface2D1.ImageMapName%>">
    <asp:Repeater runat="server" ID="repImageMap" OnItemDataBound="repImageMap_OnItemDataBound">
        <ItemTemplate>
            <asp:Literal runat="server" ID="litAreaText"></asp:Literal>
        </ItemTemplate>
    </asp:Repeater>
</map>
