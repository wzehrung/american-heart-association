﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AHAFooter.ascx.cs" Inherits="Heart360Web.Controls.Common.AHAFooter" %>


	<footer class="footer row">

<script>
    var footerHeader = 'Contact Us';
    // ***** Initialize javascript object (hash) ***** //
    var footerContactUs = new Object();
    // ***** create a hash key (associative arrays) and assign a new Array as its value ***** //
    footerContactUs['Address'] = new Array("7272 Greenville Ave.", "Dallas, Tx 75231");
    footerContactUs['Customer Service'] = new Array("1-800-AHA-USA-1", "1-800-242-8721");
    /* required parameter (both) */
    footerVendorBrandImg = '';
    footerVendorBrandHref = '';
    /* optional parameter */
    footerVendorBrandTitle = '';
    footerSealHTML = '<strong>*Red Dress &#0153; DHHS, Go Red &#0153; AHA ; National Wear Red Day&#0174; is a registered trademark</strong>';

</script>
<script src="https://static.heart.org/aha_footer.js" type="text/javascript"></script>

<div id="divFooter" runat="server" visible="false">
        <!-- ## BEGIN Section ## -->
		<div class="full">

            <h4 class="master-btn hide-gt-tablet">
                American Heart Association
                <span class="white-arrow hide-gt-tablet"></span>
            </h4>

            <ul class="master-colapse">
                <li>
                    <!-- # BEGIN About # -->
			        <section class="small-12 large-4 columns about">
				        <h5>
                            <a href="http://www.heart.org/HEARTORG/General/About-Us---American-Heart-Association_UCM_305422_SubHomePage.jsp">About Us</a>
                            <span class="gray-arrow hide-gt-tablet"></span>
                        </h5>
				        <ul>
					        <li>
						        <p>Our mission is to build healthier lives, free of cardiovascular diseases and stroke. That single purpose drives all we do. The need for our work is beyond question. <a title="About American Heart Association" href="http://www.heart.org/HEARTORG/General/About-Us---American-Heart-Association_UCM_305422_SubHomePage.jsp">More</a></p>
					        </li>
				        </ul>
			        </section>
                    <!-- # END About # -->

                    <!-- # BEGIN Causes # -->
			        <section class="small-12 large-3 columns causes">
				        <h5>
                            <a href="http://www.heart.org/HEARTORG/Causes/Causes_UCM_001128_SubHomePage.jsp">Our Causes</a>
                            <span class="gray-arrow hide-gt-tablet"></span>
                        </h5>
				        <ul>
					        <li>
						        <a title="Go Red for Women" href="http://www.goredforwomen.org/">Go Red For Women</a>
					        </li>
					        <li>
						        <a title="Go Red Corazon" href="http://www.goredcorazon.org">Go Red Por Tu Corazón</a>
					        </li>
					        <li>
						        <a title="My Heart My Life" href="http://www.heart.org/myheartmylife">My Heart My Life</a>
					        </li>
					        <li>
						        <a title="Power to End Stroke" href="http://www.powertoendstroke.org">Power To End Stroke</a>
					        </li>
					        <li>
				                <h5 class="spb">
					                <a title="Heart Attack, Stroke and Cardiac Arrest Warning Signs" href="http://www.heart.org/HEARTORG/Conditions/Conditions_UCM_305346_SubHomePage.jsp">The Warning Signs</a>
				                </h5>
					        </li>
					        <li>
				                <h5>
					                <a title="Browse the Heart and Stroke Encyclopedia" href="http://www.heart.org/HEARTORG/Conditions/The-Heart-and-Stroke-Encyclopedia_UCM_445688_SubHomePage.jsp">Heart and Stroke Encyclopedia</a>
				                </h5>
                            </li>
				        </ul>
			        </section>
                    <!-- # END Causes # -->

                    <!-- # BEGIN Sites # -->
			        <section class="small-12 large-3 columns sites">
				        <h5>
                            <a href="#">Our Sites</a>
                            <span class="gray-arrow hide-gt-tablet"></span>
                        </h5>
				        <ul>
					        <li>
						        <a title="American Heart Association" href="http://www.heart.org/HEARTORG">American Heart Association</a>
					        </li>
					        <li>
						        <a title="American Stroke Association" href="http://www.strokeassociation.org/STROKEORG">American Stroke Association</a>
					        </li>
					        <li>
						        <a title="link to My Life Check" href="http://www.heart.org/mylifecheck/">My Life Check</a>
					        </li>
					        <li>
						        <a title="link to Heart360" href="http://www.heart360.org">Heart360</a>
					        </li>
					        <li>
						        <a title="Everyday Choices" href="http://www.everydaychoices.org/">Everyday Choices</a>
					        </li>
					        <li>
						        <a title="My.AmericanHeart for Professionals" href="http://my.americanheart.org/">My.AmericanHeart for Professionals</a>
					        </li>
					        <li>
						        <a title="Scientific Sessions" href="http://www.scientificsessions.org/">Scientific Sessions</a>
					        </li>
					        <li>
						        <a title="Stroke Conference" href="http://www.strokeconference.org/">Stroke Conference</a>
					        </li>
					        <li>
						        <a title="You're The Cure" href="http://www.yourethecure.org">You're The Cure</a>
					        </li>
					        <li>
						        <a title="Global Programs" href="http://www.heart.org/HEARTORG/General/Global-Strategies-Pages_UCM_312090_SubHomePage.jsp">Global Programs</a>
					        </li>
					        <li>
						        <a title="Shop Heart" href="http://www.shopheart.org">Shop Heart</a>
					        </li>
					        <li>
						        <a title="CEO Nancy Brown" href="https://volunteer.heart.org/Pages/SiteHomePage.aspx">CEO Nancy Brown</a>
					        </li>
				        </ul>
			        </section>
                    <!-- # END Sites # -->

                    <!-- # BEGIN Contact Us # -->
			        <section class="small-12 large-2 columns contact">
				        <h5>
                            <a href="http://www.heart.org/HEARTORG/General/Contact-Us_UCM_308813_Article.jsp">Contact Us</a>
                            <span class="gray-arrow hide-gt-tablet"></span>
                        </h5>
				        <ul>
					        <li class="spb">
						        <h6>Address</h6>
						       
							        <p>7272 Greenville Ave.<br />
							           Dallas, TX 75231</p>
						        
					        </li>
					        <li>
						        <h6>Customer Service</h6>
							        <p>1-800-AHA-USA-1<br />
							           1-800-242-8721<br />
							           1-888-474-VIVE</p>
					        </li>
					        <li>
						        <h5><a href="http://www.heart.org/HEARTORG/localization/chooseState.jsp">Local Info</a></h5>
						        <h6 class="rss">
							        <a href="http://www.heart.org/HEARTORG/General/RSSFeed-page-datafile_UCM_003860_RSSFeed.jsp">RSS</a>
						        </h6>
					        </li>
				        </ul>
			        </section>
		            <!-- # END Contact Us # -->
                </li>
            </ul>
        
        </div>
        <br class="hide-for-medium-down" />
        <hr class="row hide-for-medium-down spb" />
        <br class="hide-for-medium-down" />
        <!-- ## END Section ## -->

        <!-- ## BEGIN Section ## -->
		<div class="full">
			<section class="full fl legal">
                <h5 class="hide-gt-tablet">
                    <span class="white-arrow hide-gt-tablet"></span>
                </h5>
                <ul>
                    <li>                   
				        <p class="text-center">
					        <span class="full">
						        <a onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Getting Healthy']);" title="Getting Healthy" href="http://www.heart.org/HEARTORG/GettingHealthy/GettingHealthy_UCM_001078_SubHomePage.jsp">Getting Healthy</a>
						        |
						        <a onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Conditions']);" title="Conditions" href="http://www.heart.org/HEARTORG/Conditions/Conditions_UCM_001087_SubHomePage.jsp">Conditions</a>
						        |
						        <a onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Healthcare / Research']);" title="Healthcare / Research" href="http://www.heart.org/HEARTORG/HealthcareResearch/Healthcare-Research_UCM_001093_SubHomePage.jsp">Healthcare / Research</a>
						        |
						        <a onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Caregiver']);" title="Caregiver" href="http://www.heart.org/HEARTORG/Caregiver/Caregiver_UCM_001103_SubHomePage.jsp">Caregiver</a>
						        |
						        <a onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Educator']);" title="Educator" href="http://www.heart.org/HEARTORG/Educator/Educator_UCM_001113_SubHomePage.jsp">Educator</a>
						        |
						        <a onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'CPR & ECC']);" title="CPR & ECC" href="http://www.heart.org/HEARTORG/CPRAndECC/CPR_UCM_001118_SubHomePage.jsp">CPR & ECC</a>
						        |
						        <a title="Shop" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Shop']);" href="http://shopheart.org">Shop</a>
						        |
						        <a title="Causes" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Causes']);" href="http://www.heart.org/HEARTORG/Causes/Causes_UCM_001128_SubHomePage.jsp">Causes</a>
						        |
						        <a title="Advocate" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Advocate']);" href="http://www.heart.org/HEARTORG/Advocate/Advocate_UCM_001133_SubHomePage.jsp">Advocate</a>
						        |
						        <a title="Giving" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Giving']);" href="http://www.heart.org/HEARTORG/Giving/Giving_UCM_001137_SubHomePage.jsp">Giving</a>
						        |
						        <a title="News" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'News']);" href="http://newsroom.heart.org">News</a>
						        |
						        <a title="Volunteer" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Volunteer']);" href="http://www.heart.org/HEARTORG/volunteer/volunteerForm.jsp">Volunteer</a>
						        |
						        <a title="Donate" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Donate']);" href="https://donate.americanheart.org/ecommerce/donation/donation_info.jsp?campaignId=101&amp;site=Heart&amp;itemId=prod20008">Donate</a>
						        </span>
						        <span class="full">
						        <a title="Privacy Policy" href="http://www.heart.org/HEARTORG/General/Privacy-Policy_UCM_300371_Article.jsp">Privacy Policy</a>
						        |
						        <a title="Copyright & DMCA Info" href="http://www.heart.org/HEARTORG/General/Copyright-Notice_UCM_300378_Article.jsp">Copyright & DMCA Info</a>
						        |
						        <a title="Ethics Policy" href="http://www.heart.org/HEARTORG/General/Ethics-Policy_UCM_300430_Article.jsp">Ethics Policy</a>
						        |
						        <a title="Conflict of Interest Policy" href="http://www.heart.org/HEARTORG/General/Conflict-of-Interest-Policy_UCM_300435_Article.jsp">Conflict of Interest Policy</a>
						        |
						        <a title="Linking Policy" href="http://www.heart.org/HEARTORG/General/American-Heart-Association-and-American-Stroke-Association-Linking-Policy_UCM_303551_Article.jsp">Linking Policy</a>
						        |
						        <a title="Diversity" href="http://www.heart.org/HEARTORG/General/Life-at-the-American-Heart-Association_UCM_303457_SubHomePage.jsp">Diversity</a>
						        |
						        <a title="Careers" href="http://www.heart.org/HEARTORG/General/Careers_UCM_303455_SubHomePage.jsp">Careers</a>
					        </span>
					        <span class="full hide-for-medium-down">&copy;<asp:Literal ID="litCopyrightDate" runat="server" /> American Heart Association, Inc. All rights reserved. Unauthorized use prohibited.</span>
					        <span class="full hide-for-medium-down">The American Heart Association is a qualified 501(c)(3) tax-exempt organization.</span>
				        </p>
                    </li>
                </ul>
			</section>
		</div>
        <!-- ## END Section ## -->

        <div class="hide-gt-tablet full">
            <div class="mobile-copyright">
                <p class="copyright small-12 large-4 columns fl">&copy;<asp:Literal ID="litCopyrightDateMobile" runat="server" /> American Heart Association, Inc. All rights reserved. Unauthorized use prohibited. The American Heart Association is a qualified 501(c)(3) tax-exempt organization.</p>
                <p class="disclaimer small-12 large-8 columns fl">Heart360® is not intended to provide medical advice or treatment. Only your healthcare provider can do that. The American Heart Association recommends that you consult your healthcare provider before starting a physical activity program or making a major lifestyle change. The results of this assessment can help you work with your healthcare provider to map a successful path to heart health.</p>
            </div>
        </div>


</div>
		
	</footer>

	
    <asp:PlaceHolder ID="PlaceHolder1"  runat="server">

        <script type="text/javascript" src="<%=AppVirtualPath%>/js/vendor/jquery.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/vendor/custom.modernizr.js"></script>
        <script type="text/javascript" src="<%=AppVirtualPath%>/js/vendor/Heart360.js"></script>

	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation.min.js"></script>	
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.alerts.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.clearing.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.dropdown.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.forms.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.orbit.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.reveal.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.section.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.tooltips.js"></script>
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/foundation/foundation.topbar.js"></script>
	
	    <script type="text/javascript">
	        $(document).foundation();
	    </script>
    	
	    <script type="text/javascript" src="<%=AppVirtualPath%>/js/front-end.js"></script>
	
    </asp:PlaceHolder>