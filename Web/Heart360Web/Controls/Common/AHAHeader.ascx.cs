﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// For search
using System.IO;
using System.Net;
using System.Text;

using AHAAPI;
using AHACommon;

namespace Heart360Web.Controls.Common
{
    public partial class AHAHeader : System.Web.UI.UserControl
    {
        #region public properties

        bool m_HideDonate = true;
        public bool HideDonate
        {
            get
            {
                return m_HideDonate;
            }
            set
            {
                m_HideDonate = value;
            }
        }

        bool m_HideSearch = true;
        public bool HideSearch
        {
            get
            {
                return m_HideSearch;
            }
            set
            {
                m_HideSearch = value;
            }
        }

        bool m_HidePersonas = true;
        public bool HidePersonas
        {
            get
            {
                return m_HidePersonas;
            }
            set
            {
                m_HidePersonas = value;
            }
        }

        public string HOME = string.Empty;

        public string AHA_SCRIPT_HEADER = string.Empty;
        public string AHA_HEADER_TYPE = string.Empty;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                _PopulateFeedbackTypes();
            }

            // WZ: AHA wants the feedback icon to display regardless if a user is logged in or not
            // Kept the code here in case they change their minds

            // Display the feedback icon only if the user is logged in
            //AHAAPI.UserIdentityContext objUserIdentity = AHAAPI.SessionInfo.GetUserIdentityContext();

            //if (objUserIdentity != null)
            //{
            //    btnFeedback.Visible = true;
            //    pnlFeedback.Visible = true;
            //}
            //else
            //{
            //    btnFeedback.Visible = false;
            //    pnlFeedback.Visible = false;
            //    // Removes whitespace between search and Facebook icons
            //    liSocial.Attributes.Add("style", "width: 80px;");
            //}

            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name == Locale.Spanish)
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_HEADER = "https://static.heart.org/es_aha_header.js";
                }
                else
                {
                    AHA_SCRIPT_HEADER = "http://static.heart.org/es_aha_header.js";
                }
                AHA_HEADER_TYPE = "headerType=\"aha-es\";";
            }
            else
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_HEADER = "https://static.heart.org/aha_header.js";
                }
                else
                {
                    AHA_SCRIPT_HEADER = "http://static.heart.org/aha_header.js";
                }
            }
            //<%=AHACommon.AHAAppSettings.IsSSLEnabled ? "https://static.heart.org/aha_header.js" : "http://static.heart.org/aha_header.js"%>

            string searchurl = Request.Url.Scheme + "://" + Request.Url.Authority + "/Pages/Common/Search.aspx";
            searchIframe.Attributes.Add("src", searchurl);
           
        }

        private void _PopulateFeedbackTypes()
        {
            List<AHAHelpContent.Feedback> objFeedbackTypes = AHAHelpContent.Feedback.GetFeedbackTypes(true);

            ddlFeedbackTypes.DataSource = objFeedbackTypes;
            ddlFeedbackTypes.DataValueField = "FeedbackTypeID";
            ddlFeedbackTypes.DataTextField = "FeedbackTypeName";
            ddlFeedbackTypes.DataBind();
        }

        protected void btnLinkClose_OnClick(object sender, EventArgs e)
        {
            mpeFeedback.Dispose();
            mpeFeedback.Hide();
            txtFeedbackText.Text = string.Empty;
            txtEmailAddress.Text = string.Empty;
            lblMessage.Text = string.Empty;
            lnkSend.Visible = true;
            divFeedbackType.Visible = true;
            divComments.Visible = true;
            divCheckbox.Visible = true;
            lnkOK.Visible = false;
        }

        protected void lnkSend_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Send");
            if (!Page.IsValid)
                return;

            bool successful = true;

            divMessage.Visible = true;

            string strSubject = "Heart360" + ": Feedback-" + ddlFeedbackTypes.SelectedItem.Text;

            StringBuilder sb = new StringBuilder();
            sb.Append(txtFeedbackText.Text.Trim());

            string strReplyTo = string.Empty;

            if (chkResponse.Checked)
            {
                if (txtEmailAddress.Text.Trim().Length > 0)
                {
                    sb.Append("<br /><br />");
                    sb.Append("I would like a response to my feedback.");
                    sb.Append("<br />");
                    sb.Append("E-mail : " + txtEmailAddress.Text.Trim());
                    strReplyTo = txtEmailAddress.Text.Trim();

                    try
                    {
                        EmailUtils.SendFeedbackEmail(strSubject, sb.ToString(), strReplyTo);
                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = lblMessage.Text += ex.ToString();
                        throw;
                    }
                }
            }

            try
            {
                AHAHelpContent.Feedback.FeedbackInsert(sb.ToString(), Convert.ToInt32(ddlFeedbackTypes.SelectedValue));
            }
            catch (Exception)
            {
                successful = false;
            }

            if (successful)
            {
                lblMessage.Text = "Your feedback has been sent!";
                lnkOK.Visible = true;
                lnkSend.Visible = false;
                divFeedbackType.Visible = false;
                divComments.Visible = false;
                divCheckbox.Visible = false;
            }           
        }

        #region Validators

        protected void OnValidateType(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (ddlFeedbackTypes.SelectedValue == "0")
            {
                e.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_FeedbackType").ToString();
                return;
            }

            return;
        }

        protected void OnValidateComments(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtFeedbackText.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_Comments").ToString();
                return;
            }

            return;
        }

        protected void OnValidateEmail(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (chkResponse.Checked)
            {
                if (txtEmailAddress.Text.Trim().Length == 0)
                {
                    e.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_Email").ToString();
                    return;
                }
                else if (!GRCBase.EmailSyntaxValidator.Valid(txtEmailAddress.Text.Trim(), true))
                {
                    e.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_ValidEmail").ToString();
                    return;
                }
            }

            return;
        }

        #endregion
    }
}