﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.UserCtrls.Common
{
    public partial class GoogleAnalytics : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!AHACommon.AHAAppSettings.IsGoogleAnalyticsEnabled)
            {
                this.Visible = false;
            }
        }
    }
}