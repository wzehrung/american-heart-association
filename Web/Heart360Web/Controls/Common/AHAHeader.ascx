﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AHAHeader.ascx.cs" Inherits="Heart360Web.Controls.Common.AHAHeader" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

    <style type="text/css">
        .pageBackground 
        {
            background-color: Transparent;
	        background: url('../../images/modal-bg.png');
	        background-position: center;
	        padding: 0;
	        border: none;
        }
        .popupStyle
        {
            border-top: 10px solid #D91C24;
            background: linear-gradient(to top, white 17%, rgb(242, 242, 242) 47%, white 80%) repeat scroll 0% 0% transparent !important;
            background-color: white;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.4);
            height: auto;
            
            left: 50%;
            margin-left: -20%;
            padding: 0.5em 1em 1em 1em;
            position: absolute;
            top: 10%;
            width: 310px;
            z-index: 3002;
            overflow: visible;
        }
        .h3 
        {
            font-size: 1.375em;
            margin-bottom: 0.375em;
        }
        .closePopup
        {
            margin-top: 5px !important;
            width: 100%;
            text-align: right;
        }
        .closePopupText
        {
            color: #666;
            cursor: pointer;
            font-size: 1em;
            font-weight: 500;
            line-height: 1;
        }
        .boxes
        {
            margin-top: 20px !important;
            width: 100%;
        }
        .messageSuccess
        {
            text-align: center;
            padding: 10px;
            font-size: 1.25em;
            font-weight: 600;
            color: green;
        }
    </style>

<!--[if (gt IE 9)|!(IE)]><!-->
<style type="text/css">
.popupStyle 
{   
    background: white none;
}
</style>
<!--<![endif]-->

<div id="top-navs-wrap">

    <!-- AHA Logo -->
	<section id="logo" class="small-12 large-5 columns fl">
		<h1><a href='<%=GetGlobalResourceObject("Common", "URL_Heart360")%>'><%=GetGlobalResourceObject("Common", "Header_Text_AHA")%></a></h1>
	</section>

    <!-- ## BEGIN meta-social-wrap -->
	<div id="meta-social-wrap" class="small-12 large-7 columns fr">

		<section id="utility" class="small-12 large-12">
            <a id="search-icon" class="last hide-port-tablet hide-gt-tablet phone" href="#"></a>

			<ul>
				<li class="last">
					<span class="donate-button"><a href='<%=GetGlobalResourceObject("Common", "URL_Donate")%>'><%=GetGlobalResourceObject("Common", "Header_Text_Donate")%></a></span>
				</li>

                <li class="social-media" ID="liSocial" runat="server">
                    <a class="twitter fr" href='<%=GetGlobalResourceObject("Common", "URL_Twitter")%>'></a>
                    <a class="facebook fr" href='<%=GetGlobalResourceObject("Common", "URL_Facebook")%>'></a>
                    <a class="feedback fr" id="btnFeedback" runat="server" href="#" visible="true"></a>
                </li>

				<li class="first hide-lt-tablet">
                    <iframe id="searchIframe" runat="server" class="no-scrollbar" scrolling="no" frameborder="0" height="100" width="100" ></iframe>          
				</li>
			</ul>				
		</section>

	</div>
    <!-- ## END meta-social-wrap -->

</div>

<asp:Panel ID="pnlFeedback" runat="server" Visible="true" style="display: none;">

    <asp:UpdatePanel ID="upFeedback" runat="server">
        <ContentTemplate>

            <div class="popupStyle">

                <div id="divPopupTrackerStyle" runat="server" visible="true">
                    <div class="closePopup">
                        <asp:LinkButton ID="btnLinkClose" OnClick="btnLinkClose_OnClick" CssClass="closePopupText" runat="server" Text="CLOSE" />
                    </div>
                </div>

                <div class="full">

                    <div class="full">
                        <h3>Send Us Your Feedback</h3>
                    </div>

                    <div>
                        <asp:ValidationSummary
                            ID="valSummary"
                            ValidationGroup="Send"
                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                            Enabled="true"
                            CssClass="validation-error"
                            DisplayMode="BulletList"
                            ShowSummary="true"
                            runat="server" /> 
                    </div>

                    <div class="boxes" runat="server" id="divMessage" visible="false">
                        <div class="messageSuccess">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </div>
                    </div>

                    <div class="boxes" style="height: 4em;" runat="server" id="divFeedbackType">
                        <b><%=GetGlobalResourceObject("Common", "Feedback_SendUsFeedbackType")%></b><span class="red">*</span><br />
                        <asp:DropDownList ID="ddlFeedbackTypes" runat="server" CssClass="dropdowns" Width="100%"></asp:DropDownList>
                    </div>

                    <div class="boxes" style="height: 4em;" runat="server" id="divComments">
                        <b><%=GetGlobalResourceObject("Common", "Feedback_SendUsFeedbackComments")%></b><span class="red">*</span>
                        <asp:TextBox ID="txtFeedbackText" runat="server" TextMode="MultiLine" Rows="6" MaxLength="1024"></asp:TextBox>
                    </div>

                    <div class="boxes" runat="server" id="divCheckbox">
                        <asp:CheckBox ID="chkResponse" runat="server" TextAlign="Right" /><span style="padding-left: 10px;"><%=GetGlobalResourceObject("Common", "Feedback_Response")%></span><br />
                        <b><%=GetGlobalResourceObject("Common", "Text_Email")%></b><br />
                        <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="boxWidths"></asp:TextBox>
                    </div>

                    <div class="g-recaptcha" data-sitekey="6LeXuxYTAAAAAJ7tRwhFMx3Gx1F-QXeh69gGVyEK"></div>

                    <div class="boxes">
                        <asp:LinkButton runat="server" ID="lnkSend" OnClick="lnkSend_OnClick" CssClass="gray-button full"><em><strong><%=GetGlobalResourceObject("Common", "Feedback_SendUsFeedbackSubmit")%></strong></em></asp:LinkButton>
                        <asp:LinkButton runat="server" ID="lnkOK" OnClick="btnLinkClose_OnClick" CssClass="gray-button full" Visible="false"><em><strong><%=GetGlobalResourceObject("Common", "Text_OK")%></strong></em></asp:LinkButton>
                    </div>

                </div>

            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Panel>
    <asp:CustomValidator ValidationGroup="Send" EnableClientScript="false" ID="custValidType"
        runat="server" Display="None" OnServerValidate="OnValidateType">
    </asp:CustomValidator>
    <asp:CustomValidator ValidationGroup="Send" EnableClientScript="false" ID="custValidComments"
        runat="server" Display="None" OnServerValidate="OnValidateComments"></asp:CustomValidator>
    <asp:CustomValidator ValidationGroup="Send" EnableClientScript="false" ID="custValidEmail"
        runat="server" Display="None" OnServerValidate="OnValidateEmail">
    </asp:CustomValidator>
    



<%--
    <asp:RegularExpressionValidator ID="regexVal" 
        runat="server" ControlToValidate="txtFeedbackText" 
        ValidationExpression="<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)</\1>" 
        ValidationGroup="send" ErrorMessage="no!">
    </asp:RegularExpressionValidator>
--%>
<asp:ModalPopupExtender
    ID="mpeFeedback"
    runat="server"
    TargetControlID="btnFeedback"
    PopupControlID="pnlFeedback" CancelControlID="btnLinkClose" OkControlID="btnLinkClose" BackgroundCssClass="pageBackground">
</asp:ModalPopupExtender>
