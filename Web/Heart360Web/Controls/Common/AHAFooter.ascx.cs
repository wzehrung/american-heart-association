﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHACommon;

namespace Heart360Web.Controls.Common
{
    public partial class AHAFooter : System.Web.UI.UserControl
    {
        protected string strFooter = String.Empty;
        protected string AppVirtualPath = string.Empty;
        public string AHA_SCRIPT_FOOTER = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Heart360Global.AHAPage objPage = Heart360Global.Main.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);
            if (objPage != null)
            {
            //    if (objPage.IsProviderPage)
            //    {
            //        strFooter = LanguageResourceHelper.Disclaimer_Provider_HTML;
            //    }
            //    else
            //    {
            //        strFooter = LanguageResourceHelper.Disclaimer_Patient_HTML;
            //    }
            }

            litCopyrightDate.Text = DateTime.Now.Year.ToString();
            litCopyrightDateMobile.Text = DateTime.Now.Year.ToString();

            if (HttpRuntime.AppDomainAppVirtualPath != "/")
            {
                AppVirtualPath = HttpRuntime.AppDomainAppVirtualPath;
            }

            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name == Locale.Spanish)
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_FOOTER = "https://static.heart.org/es_aha_footer.js";
                }
                else
                {
                    AHA_SCRIPT_FOOTER = "http://static.heart.org/es_aha_footer.js";
                }
            }
            else
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_FOOTER = "https://static.heart.org/aha_footer.js";
                }
                else
                {
                    AHA_SCRIPT_FOOTER = "http://static.heart.org/aha_footer.js";
                }
            }
        }
    }
}