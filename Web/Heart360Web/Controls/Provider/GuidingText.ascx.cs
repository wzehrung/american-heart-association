﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using AHAAPI;

namespace Heart360Web.Controls.Provider
{
    public partial class GuidingText : System.Web.UI.UserControl
    {
        #region Public Members

        public AHACommon.AHADefs.ContentType ZoneTitle
        {
            get;
            set;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AHAHelpContent.ProviderContent objContent = AHAHelpContent.ProviderContent.FindContentByZoneTitle(this.ZoneTitle.ToString(), AHAAPI.H360Utility.GetCurrentLanguageID());
                if (objContent != null)
                {
                    if (!string.IsNullOrEmpty(objContent.ContentTitle))
                    {
                        if (string.IsNullOrEmpty(objContent.ContentTitleUrl))
                        {
                            hZoneTitle.InnerText = Regex.Replace(objContent.ContentTitle, "##TrademarkedHeart360##", H360Utility.GetTrademarkedHeart360());
                        }
                        else
                        {
                            hZoneTitle.InnerHtml = string.Format("<a href=\"{0}\">{1}</a>", objContent.ContentTitleUrl, Regex.Replace(objContent.ContentTitle, "##TrademarkedHeart360##", H360Utility.GetTrademarkedHeart360()));
                        }
                    }
                    else
                    {
                        hZoneTitle.Visible = false;
                    }

                    string strContent = objContent.ContentText;

                    if (!string.IsNullOrEmpty(strContent))
                    {
                        strContent = Regex.Replace(strContent, "##TrademarkedHeart360##", H360Utility.GetTrademarkedHeart360());

                        if (Page is AHAAPI.Provider.ProviderBase)
                        {
                            AHAAPI.Provider.ProviderBase objPage = Page as AHAAPI.Provider.ProviderBase;

                            if (objPage.RecordID.HasValue && objPage.PersonID.HasValue)
                            {
                                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPage.PersonID.Value, objPage.RecordID.Value);

                                HVManager.PersonalDataManager.PersonalItem objPersonal = mgr.PersonalDataManager.Item;

                                if (objPersonal != null)
                                {
                                    strContent = Regex.Replace(strContent, "##PATIENTNAME##", objPersonal.FullName);
                                }
                                else
                                {
                                    //fetch from manager
                                    strContent = Regex.Replace(strContent, "##PATIENTNAME##", mgr.Name);
                                }
                            }

                            H360RequestContext objRequestContext = H360RequestContext.GetContext();

                            if (objRequestContext.ProviderDetails != null)
                            {
                                strContent = Regex.Replace(strContent, "##PROVIDERNAME##", objRequestContext.ProviderDetails.FormattedName);
                            }
                        }

                        litGuidingText.Text = Regex.Replace(UIHelper.RenderLink(strContent), "\r\n", "<br />");
                    }                    
                }
            }
        }

      
    }
}