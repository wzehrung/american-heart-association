﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleDeleteReading.ascx.cs" Inherits="Heart360Web.Controls.Patient.ModuleDeleteReading" %>

<div class="full">
    <!-- ## Delete Reading Title ## -->
	<div class="large-12 small-12 columns">
		<h3><asp:Literal ID="litTitle" runat="server"></asp:Literal></h3>
	</div>

	<div class="large-12 small-12 columns spb">
		<p><label id="lblDescription" runat="server"><b><%=GetGlobalResourceObject("Tracker", "Popup_Confirmation_Delete")%></b></label></p>
	</div>

    <div class="large-6 small-6 columns" >
        <asp:Button ID="btnOk" CssClass="gray-button full" OnClick="btnOk_Click" runat="server" Text="<%$Resources:Common,Text_OK %>" />
    </div>
    <div class="large-6 small-6 columns" >
        <asp:Button ID="btnCancel" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_Cancel%>" />
    </div>
</div>