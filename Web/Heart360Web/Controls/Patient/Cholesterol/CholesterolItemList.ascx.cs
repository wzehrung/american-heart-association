﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;
using HVManager;
using AHACommon;

namespace Heart360Web.Controls.Patient.Cholesterol
{
    public partial class CholesterolItemList : System.Web.UI.UserControl, IDataList
    {
        // Givens: all interaction with the list (Edit, Delete) is done through a popup with no postback.
        //         This control does not change/manage current date range
        //
        // How it works: we always render the control on Page_Load!
        //               ModuleDetails manages ViewState["DataRange"]

        #region Private Members

    /**
        AHACommon.DataDateReturnType CurrentDateRange
        {
            get
            {
                if (ViewState["DataRange"] != null)
                    return (AHACommon.DataDateReturnType)ViewState["DataRange"];
                else
                    return AHACommon.DataDateReturnType.Last3Months;
            }
            set
            {
                if (ViewState["DataRange"] == null)
                {
                    ViewState.Add("DataRange", value);
                }
                else
                {
                    ViewState["DataRange"] = value;
                }
            }
        }
**/
        private bool? ISGenderMale
        {
            get
            {
                if (ViewState["ISGenderMale"] != null)
                    return (bool)ViewState["ISGenderMale"];
                else
                    return null;
            }
            set
            {
                if (ViewState["ISGenderMale"] == null)
                {
                    ViewState.Add("ISGenderMale", value);
                }
                else
                {
                    ViewState["ISGenderMale"] = value;
                }
            }
        }

        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "date";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return false;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }

        #endregion

        #region Public Properties

        IPeriodicDataManager m_IPeriodicDataManager = null;
        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    CurrentDateRange = m_IPeriodicDataManager.DateRange;

                //this.Gender = "Unknown";
                HVManager.BasicDataManager.BasicItem objBI = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
                if (objBI != null && objBI.GenderOfPerson.Value.HasValue)
                {
                    if (objBI.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Female)
                        this.ISGenderMale = false;
                    else if (objBI.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Male)
                        this.ISGenderMale = true;
                }

            //}
            
            //linkRefreshSiteData.OnClientClick = string.Format("return RefreshSiteDataPatientForUrl('{0}');", AHAAPI.WebAppNavigation.H360.PAGE_TRACKER_BP);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            _bindDataToGridView();
        }


        private void _bindDataToGridView()
        {
            DataDateReturnType ddrt = m_IPeriodicDataManager.DateRange;
            LinkedList<CholesterolDataManager.CholesterolItem> ascendingList = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetDataBetweenDates(ddrt);

            LinkedList<CholesterolDataManager.CholesterolItem> descendingList = new LinkedList<CholesterolDataManager.CholesterolItem>();
            foreach (CholesterolDataManager.CholesterolItem cholesterolItem in ascendingList.Reverse())
            {
                descendingList.AddLast(cholesterolItem);
            }


            rptHistory.DataSource = descendingList;
            rptHistory.DataBind();

            // NOTE: If there is no history, there will just be an empty table.
            /**
                        gvList.DataKeyNames = new string[] { "ThingID", "VersionStamp" };
                        gvList.DataSource = desendingList;
                        gvList.DataBind();

                        if (descendingList.Count == 0)
                        {
                            DivNoData.Visible = true;
                            DivData.Visible = false;
                        }
                        else
                        {
                            DivNoData.Visible = false;
                            DivData.Visible = true;
                        }
             **/
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CholesterolDataManager.CholesterolItem item = e.Item.DataItem as CholesterolDataManager.CholesterolItem;

                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litTC = e.Item.FindControl("litTC") as Literal;     // can have risk styling
                Literal litHDL = e.Item.FindControl("litHDL") as Literal;   // can have risk styling
                Literal litLDL = e.Item.FindControl("litLDL") as Literal;   // can have risk styling
                Literal litTri = e.Item.FindControl("litTriglycerides") as Literal; // can have risk styling
                Literal litSource = e.Item.FindControl("litSource") as Literal;


                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.SLASH, GRCBase.GRCTimeFormat.NONE);

                //
                // Compute a risk level for this entry
                //
                RiskLevelHelper.RiskLevelCholesterol cholesterolRiskLevel = null;
                if ((AHAAppSettings.ColorCoding_EffectiveDays == null) || item.EffectiveDate.CompareTo(DateTime.Now.AddDays(AHAAppSettings.ColorCoding_EffectiveDays.Value)) == 1)
                {
                    cholesterolRiskLevel = new RiskLevelHelper.RiskLevelCholesterol((item.TotalCholesterol.Value.HasValue ? item.TotalCholesterol.Value.Value : 0)
                        , (item.HDL.Value.HasValue ? item.HDL.Value.Value : 0)
                        , (item.LDL.Value.HasValue ? item.LDL.Value.Value : 0)
                        , (item.Triglycerides.Value.HasValue ? item.Triglycerides.Value.Value : 0), ISGenderMale.HasValue ? ISGenderMale.Value : true);

                }


                if (item.TotalCholesterol.Value.HasValue)
                {
                    if (cholesterolRiskLevel != null)
                        litTC.Text = "<td class=\"" + cholesterolRiskLevel.ColorReading1 + "\">" + item.TotalCholesterol.Value.ToString() + "</td>";
                    else
                        litTC.Text = "<td>" + item.TotalCholesterol.Value.ToString() + "</td>";
                }
                else
                {
                    litTC.Text = "<td></td>";
                }

                if (item.HDL.Value.HasValue)
                {
                    if (cholesterolRiskLevel != null && ISGenderMale.HasValue)
                        litHDL.Text = "<td class=\"" + cholesterolRiskLevel.ColorReading2 + "\">" + item.HDL.Value.ToString() + "</td>";
                    else
                        litHDL.Text = "<td>" + item.HDL.Value.ToString() + "</td>";
                }
                else
                {
                    litHDL.Text = "<td></td>";
                }

                if (item.LDL.Value.HasValue)
                {
                    if (cholesterolRiskLevel != null)
                        litLDL.Text = "<td class=\"" + cholesterolRiskLevel.ColorReading3 + "\">" + item.LDL.Value.ToString() + "</td>";
                    else
                        litLDL.Text = "<td>" + item.LDL.Value.ToString() + "</td>";
                }
                else
                {
                    litLDL.Text = "<td></td>";
                }

                if (item.Triglycerides.Value.HasValue)
                {
                    if (cholesterolRiskLevel != null)
                        litTri.Text = "<td class=\"" + cholesterolRiskLevel.ColorReading4 + "\">" + item.Triglycerides.Value.ToString() + "</td>";
                    else
                        litTri.Text = "<td>" + item.Triglycerides.Value.ToString() + "</td>";
                }
                else
                {
                    litTri.Text = "<td></td>";
                }

                litSource.Text = item.TestLocation.Value;

                //
                // Hook up Actions
                //
                ImageButton actEdit = e.Item.FindControl("actEditor") as ImageButton;
                ImageButton actTrash = e.Item.FindControl("actTrash") as ImageButton;
                string      itemKey = item.ThingID.ToString();

                if (actEdit != null)
                {
                    string editUrl = "PopUp.aspx?reading=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL).ToString() + "&elid=" + itemKey;
                    actEdit.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL);
                }
                if (actTrash != null)
                {
                    // delete requires both ThingID and VersionStamp
                    itemKey += "," + item.VersionStamp.ToString();
                    string trashUrl = "PopUp.aspx?delete=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL).ToString() + "&elid=" + itemKey;
                    actTrash.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL);
                }
            }
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        public void LoadItems(DataDateReturnType DateRange)
        {
            SortAscending = false;
            SortExpression = string.Empty;

            //CurrentDateRange = DateRange;

            //_bindDataToGridView();
        }
    }
}