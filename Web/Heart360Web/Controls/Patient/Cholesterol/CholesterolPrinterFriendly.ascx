﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CholesterolPrinterFriendly.ascx.cs" Inherits="Heart360Web.Controls.Patient.Cholesterol.CholesterolPrinterFriendly" %>

<%@ Register Src="~/Controls/Patient/ModuleItemGraph.ascx" TagName="BPGraph" TagPrefix="printerFriendlyInnerCtrl" %>

<h3><%=GetGlobalResourceObject("Reports", "Text_Heading_MyCholesterolRecord")%></h3>
<printerFriendlyInnerCtrl:BPGraph ID="GraphCtrl" runat="server" />
<div class="full history-table-wrap" >
    <asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="false" runat="server">
        <HeaderTemplate>
    	         <table>
		            <thead>
			            <tr>
				            <th width="20%"><%=GetGlobalResourceObject("Reports", "Text_Date")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_TCWithUnits")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_HDLWithUnits")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_LDLWithUnits")%></th>
				            <th width="25%"><%=GetGlobalResourceObject("Reports", "Text_TriglycerideWithUnits")%></th>
				            <th width="25%"><%=GetGlobalResourceObject("Reports", "Text_TestLocation")%></th>
			            </tr>
		            </thead>
                    <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
			    <td><asp:Literal ID="litDate" runat="server"/></td>
			    <td><asp:Literal ID="litTC" runat="server"/></td>
			    <td><asp:Literal ID="litHDL" runat="server"/></td>
			    <td><asp:Literal ID="litLDL" runat="server"/></td>
			    <td><asp:Literal ID="litTriglyceride" runat="server"/></td>
			    <td><asp:Literal ID="litLocation" runat="server"/></td>
		    </tr>
        </ItemTemplate>
        <FooterTemplate>
                    </tbody>
	            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>

