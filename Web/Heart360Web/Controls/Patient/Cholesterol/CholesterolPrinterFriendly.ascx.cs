﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using HVManager;

namespace Heart360Web.Controls.Patient.Cholesterol
{
    public partial class CholesterolPrinterFriendly : System.Web.UI.UserControl
    {
        IPeriodicDataManager m_IPeriodicDataManager = null;

        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
                GraphCtrl.IPeriodicDataManager = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GraphCtrl.ModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL;
            GraphCtrl.Visible = true;
            GraphCtrl.UseDivTrigger = false;

            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            LinkedList<CholesterolDataManager.CholesterolItem> asendingList = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetDataBetweenDates(m_IPeriodicDataManager.DateRange);

            LinkedList<CholesterolDataManager.CholesterolItem> desendingList = new LinkedList<CholesterolDataManager.CholesterolItem>();
            foreach (CholesterolDataManager.CholesterolItem cholesterolItem in asendingList.Reverse())
            {
                desendingList.AddLast(cholesterolItem);
            }

            rptHistory.DataSource = desendingList;
            rptHistory.DataBind();
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CholesterolDataManager.CholesterolItem item = e.Item.DataItem as CholesterolDataManager.CholesterolItem;

                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litTC = e.Item.FindControl("litTC") as Literal;
                Literal litHDL = e.Item.FindControl("litHDL") as Literal;
                Literal litLDL = e.Item.FindControl("litLDL") as Literal;
                Literal litTriglyceride = e.Item.FindControl("litTriglyceride") as Literal;
                Literal litLocation = e.Item.FindControl("litLocation") as Literal;


                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                if (item.TotalCholesterol.Value.HasValue)
                    litTC.Text = item.TotalCholesterol.Value.Value.ToString();
                if (item.HDL.Value.HasValue)
                   litHDL.Text = item.HDL.Value.Value.ToString();
                if (item.LDL.Value.HasValue)
                    litLDL.Text = item.LDL.Value.Value.ToString();
                if (item.Triglycerides.Value.HasValue)
                    litTriglyceride.Text = item.Triglycerides.Value.Value.ToString();
                litLocation.Text = item.TestLocation.Value;
            }
        }
    }
}