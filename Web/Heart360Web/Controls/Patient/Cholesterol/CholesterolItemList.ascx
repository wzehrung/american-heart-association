﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CholesterolItemList.ascx.cs" Inherits="Heart360Web.Controls.Patient.Cholesterol.CholesterolItemList" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>

<asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="true" runat="server">
    <HeaderTemplate>
    	     <table class="paginated">
		        <thead>
			        <tr>
				        <th class="tt-date">DATE</th>
				        <th class="ch-total">TOTAL CHOLESTEROL (mg/dL)</th>
				        <th class="ch-hdl">HDL (mg/dL)</th>
				        <th class="ch-ldl">LDL (mg/dL)</th>
				        <th class="ch-triglycerides">TRIGLYCERIDES (mg/dL)</th>
				        <th class="ch-reading">READING SOURCE</th>
				        <th class="tt-icon"></th>
                        <th class="tt-icon"></th>
			        </tr>
		        </thead>
                <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
			<td><asp:Literal ID="litDate" runat="server"/></td>
			<asp:Literal ID="litTC" runat="server"/><!-- has to include td tag -->
			<asp:Literal ID="litHDL" runat="server"/><!-- has to include td tag -->
			<asp:Literal ID="litLDL" runat="server"/><!-- has to include td tag -->
			<asp:Literal ID="litTriglycerides" runat="server"/><!-- has to include td tag -->
			<td><asp:Literal ID="litSource" runat="server"/></td>
			<td>
				<asp:ImageButton ID="actEditor" CssClass="add-note fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Edit CH reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
            <td>
                <asp:ImageButton ID="actTrash" CssClass="trash fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Delete CH reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
		</tr>
    </ItemTemplate>
    <FooterTemplate>
                </tbody>
	        </table>
    </FooterTemplate>
</asp:Repeater>


