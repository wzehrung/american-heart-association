﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Heart360Web;
using HVManager;

namespace Heart360Web.Controls.Patient
{
    //
    // In a Model-View-Controller pattern, this is a View component
    //

    public partial class ModuleSummary : System.Web.UI.UserControl
    {
            // This is an enum to specify the "state" of the ModuleSummary.
            // The state will assign the proper color scheme and sometimes the secondary text.
        public enum SUMMARY_STATE
        {
            SUMMARY_STATE_NONE,                 // grey scheme (default)
            SUMMARY_STATE_NEUTRAL,              // blue scheme, 
            SUMMARY_STATE_EXCELLENT,            // green scheme, secondary text assigned
            SUMMARY_STATE_NEEDS_IMPROVEMENT,    // orange scheme, secondary text assigned
            SUMMARY_STATE_WARNING               // red scheme, secondary text assigned
        } ;

            // This enumeration is used to assign the proper css classes
        public enum SUMMARY_LOCATION
        {
            SUMMARY_LOCATION_DASHBOARD = 0,
            SUMMARY_LOCATION_MODULE,               // it is in the side-bar of (non-current) module
            SUMMARY_LOCATION_MODULE_CURRENT        // it is the current module
        } ;

        private Heart360Web.PatientPortal.MODULE_ID                 mModuleID;
        private bool                                                mEnableAddButton;
        private bool                                                mRefresh;
        private string                                              mNavigationUrl;
        private string                                              mTrackerType;
        private SUMMARY_STATE                                       mSummaryState;
        private SUMMARY_LOCATION                                    mSummaryLocation;
            // the module controller wants to potentially be referenced by both the Summary and Detail views.
            // so this can be new'd either internally or externally
        private Heart360Web.ModuleControllerInterface               mModuleController;

        public ModuleSummary() : base()
        {
            mSummaryState = ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NONE;
            mSummaryLocation = ModuleSummary.SUMMARY_LOCATION.SUMMARY_LOCATION_DASHBOARD;
            mNavigationUrl = string.Empty;
            mEnableAddButton = true;
            mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
            mTrackerType = string.Empty;
            mModuleController = null;
        }

        //
        // Attributes
        //

        public int ModuleIdAsInt      // need this so (html/asp) markup can specify modules
        {
            set
            {
                mModuleID = Heart360Web.PatientPortal.GetModuleIdFromInt(value);
            }
            get
            {
                return (int)mModuleID;
            }
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

            // Note that TrackerType will determine the icon of the ModuleSummary, via CSS
        public string TrackerType
        {
            get { return mTrackerType; } // string sTracker = litTrackerBegin.Text; return sTracker; }
            set { mTrackerType = value; }   //  litTracker.Text = value; }
        }

        public string Title
        {
            set { litTitle.Text = value; }
        }

        public string PrimaryText
        {
            set { litTrackerData.Text = value;  }
        }

        public string HbA1cValue
        {
            set { litHbA1c.Text = value; }
        }

        public string SecondaryTextNoColor
        {
            set { litTrackerReadingA.Text = value;  }
        }
        public string SecondaryTextColored
        {
            set { litTrackerReadingB.Text = value; }
        }

        public bool EnableAddButton
        {
            get { return mEnableAddButton; }
            set { mEnableAddButton = value; }
        }

        public bool RefreshControl
        {
            get { return mRefresh; }
            set { mRefresh = value; }
        }

        public string NavigationUrl
        {
            get { return mNavigationUrl; }
            set { mNavigationUrl = value; }
        }

        public SUMMARY_STATE SummaryState
        {
            get { return mSummaryState; }
            set
            {
                switch (value)
                {
                    case SUMMARY_STATE.SUMMARY_STATE_NONE:
                        mSummaryState = value;
                        SecondaryTextNoColor = ".";      // we need at least one space character for css styling
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_NEUTRAL:
                        mSummaryState = value;
                        // SecondaryText is set by instance
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_EXCELLENT:
                        mSummaryState = value;
                        SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_LastReading").ToString() + ": ";
                        SecondaryTextColored = "<span class=\"status has-tip tip-left\" data-tooltip=\"\" title=\"" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Color_Indication_Green").ToString() + "\">" +
                                               GetGlobalResourceObject("Tracker", "Tracker_Text_Excellent").ToString() + "[?]</span>" ;
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT:
                        mSummaryState = value;
                        SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_LastReading").ToString() + ": ";
                        SecondaryTextColored = "<span class=\"status has-tip tip-left\" data-tooltip=\"\" title=\"" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Color_Indication_Orange").ToString() + "\">" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Text_NeedsImprovement").ToString() + "[?]</span>";
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_WARNING:
                        mSummaryState = value;
                        SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_LastReading").ToString() + ": ";
                        SecondaryTextColored = "<span class=\"status has-tip tip-left\" data-tooltip=\"\" title=\"" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Color_Indication_Red").ToString() + "\">" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Text_Warning").ToString() + "[?]</span>";
                        break;

                    default:
                        break;

                }
            }
        }

        public SUMMARY_LOCATION SummaryLocation
        {
            get { return mSummaryLocation; }
            set { mSummaryLocation = value; }
        }

        public Heart360Web.ModuleControllerInterface ModuleController
        {
            set { mModuleController = value; }
            get { return mModuleController; }
        }
        
            // This is the name of the css color code class that Push Design and Tectura agreed to use.
            // It is used, for example, to color the border around the icon.
        private string CssColorCode
        {
            get
            {
                string  retval = string.Empty ;

                switch (mSummaryState)
                {
                    case SUMMARY_STATE.SUMMARY_STATE_NEUTRAL:
                        retval = "neutral-blue";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_EXCELLENT:
                        retval = "excellent-green";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT:
                        retval = "needsimprovement-orange";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_WARNING:
                        retval = "warning-red";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_NONE:
                    default:
                        break; // use default return value
                }
                return retval;
            }
        }

        private string CssClass
        {
            get
            {
                string retval = string.Empty;
                switch (mSummaryLocation)
                {
                    case SUMMARY_LOCATION.SUMMARY_LOCATION_DASHBOARD:
                        retval = "tile " + CssColorCode + " small-12 large-4 columns";
                        break;

                    case SUMMARY_LOCATION.SUMMARY_LOCATION_MODULE:
                        retval = "tile " + CssColorCode + " large-12 small-12 columns";
                        break;

                    case SUMMARY_LOCATION.SUMMARY_LOCATION_MODULE_CURRENT:
                        retval = "tile-large " + CssColorCode + " full";
                        break;

                    default:
                        break;
                }
                return retval;
            }
        }

        private bool IsGoalDefined
        {
            // Only BP, CH, WT and PA have goals
            get
            {
                bool retstat = false;
                switch (this.mModuleID)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                        if( HVHelper.HVManagerPatientBase.BPGoalDataManager.Item != null )
                            retstat = true ;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        if (HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item != null)
                            retstat = true;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                        if (HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.Item != null)
                            retstat = true;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                        if (HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item != null)
                            retstat = true;
                        break;

                    default:
                        break;
                }
                return retstat;
            }
        }

        // Get the current goal to display on the tracker
        private void _LoadBPGoal()
        {
            BPGoalDataManager.BPGoalItem bpGoal = HVHelper.HVManagerPatientBase.BPGoalDataManager.Item;

            if (bpGoal != null)
            {
                if (bpGoal.TargetSystolic != null)
                    litGoal1.Text = "<span class=\"systolic-data\">" + bpGoal.TargetSystolic.Value.ToString() + "</span>";

                if (bpGoal.TargetDiastolic != null)
                    litGoal2.Text = "<span class=\"diastolic-data\">" + bpGoal.TargetDiastolic.Value.ToString() + "</span>";
            }
            else
            {
                // PJB: JM needs a special class because of the | separator between systolic and diastolic
                litGoal1.Text = "<span class=\"no-data\">" + GetGlobalResourceObject("Tracker", "Tracker_Text_Goal_None").ToString() + "</span>";
            }

            spanGoals.Visible = true;
        }

        private void _LoadCHGoal()
        {
            CholesterolGoalDataManager.CholesterolGoalItem ChGoal = HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item;

            if (ChGoal != null)
            {
                if (ChGoal.TargetTotalCholesterol != null)
                    litGoal1.Text = "<span class=\"cholesterol-data\">" + ChGoal.TargetTotalCholesterol.Value.ToString() + "</span>";
            }
            else
            {
                litGoal1.Text = "<span class=\"cholesterol-data\">" + GetGlobalResourceObject("Tracker", "Tracker_Text_Goal_None").ToString() + "</span>";
            }

            spanGoals.Visible = true;
        }

        private void _LoadPAGoal()
        {
            ExerciseGoalDataManager.ExerciseGoalItem ExGoal = HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.Item;

            if (ExGoal != null)
            {
                if (ExGoal.Duration != null && ExGoal.Duration.Value.HasValue)
                {
                    litGoal1.Text = "<span class=\"fitness-data\">" + ExGoal.Duration.Value.Value.ToString() + "</span>";
                }
            }
            else
            {
                litGoal1.Text = "<span class=\"fitness-data\">" + GetGlobalResourceObject("Tracker", "Tracker_Text_Goal_None").ToString() + "</span>";
            }

            spanGoals.Visible = true;
        }

        private void _LoadWGoal()
        {
            HVManager.WeightGoalDataManager.WeightGoalItem WtGoal = HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item;

            if (WtGoal != null)
            {
                litGoal1.Text = "<span class=\"weight-data\">" + GRCBase.StringHelper.GetFormattedDoubleString(WtGoal.TargetValue.Value, 2, false) + "</span>";
            }
            else
            {
                litGoal1.Text = "<span class=\"weight-data\">" + GetGlobalResourceObject("Tracker", "Tracker_Text_Goal_None").ToString() + "</span>";
            }

            spanGoals.Visible = true;
        }

        private void _LoadA1c()
        {
            spanA1c.Visible = true;

            HVManager.HbA1cDataManager.HbA1cItem objItem = BloodGlucoseWrapper.GetLatestHbA1c();

            if (objItem != null)
            {
                litHbA1c.Text = AHAAPI.H360Utility.HbA1cToPercentage(objItem.Value.Value).ToString() + "%";
            }
            else
            {
                litHbA1c.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_HbA1c_None").ToString(); ;
            }
        }

        //
        // Events
        //
/** Not sure we need these
        public delegate void ModuleSummaryEventHandler( object sender, EventArgs e ) ;

        public event ModuleSummaryEventHandler ModuleClicked;
        public event ModuleSummaryEventHandler ModuleAddButtonClicked;
**/

        //
        // Functions
        //

        public void _RefreshProfileData()
        {
            this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyProfile").ToString();
            this.NavigationUrl = "/Pages/Patient/Profile.aspx";
            this.SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_Completed").ToString();
            this.EnableAddButton = false;
            mModuleController = new ProfileModuleController();
            mModuleController.InitializeSummary(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string addReadingUrl = string.Empty;
            string addGoalUrl = string.Empty;

            switch (mModuleID)
            {
                    //-------------------------------------------------------------------
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    //-------------------------------------------------------------------
                    #region initialize connection center properties for
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyConnectionCenter").ToString();
                    this.NavigationUrl = "/Pages/Patient/ConnectionCenter.aspx";
                    this.EnableAddButton = false;
                    // construct a controller if we have not been assigned one
                    if (mModuleController == null)
                    {
                        // --------------------------------------------------------------------------------------------
                        // use icon-xxxxxxxx class for My connection Center on Dashboard.aspx 20130802-HHC
                        // --------------------------------------------------------------------------------------------
                        this.SecondaryTextNoColor = "<span class=\"first col-1\"><span data-tooltip=\"\" class=\"icon-announce has-tip tip-left\" title=\"Announcements\"></span></span>" +
                                                    "<span class=\"col-2\"><span data-tooltip=\"\" class=\"icon-messages has-tip tip-left\" title=\"Messages\"></span></span>" +
                                                    "<span class=\"last col-3\"><span data-tooltip=\"\" class=\"icon-invites has-tip tip-left\" title=\"Invitations\"></span></span>";
                        mModuleController = new ConnectionCenterModuleController();

                         /** "<span data-tooltip=\"\" class=\"has-tip tip-left\" title=\"" + **/
                    }
                    else
                    {
                        // ------------------------------------------------------------------------------------------
                        // use icon-xxxxxxxx-lrg class for My connection Center connectioncenter.aspx 20130802-HHC 
                        // ------------------------------------------------------------------------------------------
                        this.SecondaryTextNoColor = "<span class=\"first col-1\"><span data-tooltip=\"\" class=\"icon-announce-lrg has-tip tip-left\" title=\"Announcements\"></span></span>" +
                                                    "<span class=\"col-2\"><span data-tooltip=\"\" class=\"icon-messages-lrg has-tip tip-left\" title=\"Messages\"></span></span>" +
                                                    "<span class=\"last col-3\"><span data-tooltip=\"\" class=\"icon-invites-lrg has-tip tip-left\" title=\"Invitations\"></span></span>";
                    }
                    #endregion 
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyProfile").ToString();
                    this.NavigationUrl = "/Pages/Patient/Profile.aspx";
                    this.SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_Completed").ToString();
                    this.EnableAddButton = false;
                    if (mModuleController == null)  // construct a controller if we have not been assigned one
                        mModuleController = new ProfileModuleController();

                        // TODO: next lines should be handled by mModuleController
                    //this.SummaryState = SUMMARY_STATE.SUMMARY_STATE_NEUTRAL;
                    //this.PrimaryText = "60%";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyBloodPressure").ToString();
                    this.NavigationUrl = "/Pages/Patient/BloodPressure.aspx";
                    addReadingUrl = "PopUp.aspx?reading=1&modid=" + ModuleIdAsInt.ToString();
                    addGoalUrl = "Popup.aspx?goal=1&modid=" + ModuleIdAsInt.ToString();
                    _LoadBPGoal();
                    if (mModuleController == null)  // construct a controller if we have not been assigned one
                        mModuleController = new BloodPressureModuleController();
                    this.hidModuleID.Value = ModuleID.ToString();
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyWeight").ToString();
                    this.NavigationUrl = "/Pages/Patient/Weight.aspx";
                    addReadingUrl = "PopUp.aspx?reading=1&modid=" + ModuleIdAsInt.ToString();
                    addGoalUrl = "Popup.aspx?goal=1&modid=" + ModuleIdAsInt.ToString();
                    _LoadWGoal();
                    if (mModuleController == null)  // construct a controller if we have not been assigned one
                        mModuleController = new WeightModuleController();
                        // TODO: next lines should be handled by mModuleController
                    //this.SummaryState = SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT;
                    //this.PrimaryText = "275";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyPhysicalActivity").ToString();
                    this.NavigationUrl = "/Pages/Patient/PhysicalActivity.aspx";
                    addReadingUrl = "/Pages/Patient/PopUp.aspx?reading=1&modid=" + ModuleIdAsInt.ToString();
                    addGoalUrl = "Popup.aspx?goal=1&modid=" + ModuleIdAsInt.ToString();
                    _LoadPAGoal();
                    if (mModuleController == null)  // construct a controller if we have not been assigned one
                        mModuleController = new PhysicalActivityModuleController();
                        // TODO: next lines should be handled by mModuleController
                    //this.SummaryState = SUMMARY_STATE.SUMMARY_STATE_EXCELLENT;
                    //this.PrimaryText = "120";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyBloodGlucose").ToString();
                    this.NavigationUrl = "/Pages/Patient/BloodGlucose.aspx";
                    addReadingUrl = "PopUp.aspx?reading=1&modid=" + ModuleIdAsInt.ToString();
                    // Using the add goal functionality for adding an A1c
                    addGoalUrl = "Popup.aspx?hba1c=add&modid=" + ModuleIdAsInt.ToString();
                    // Note: there is no goal for blood glucose
                    // Need this as a blank placeholder so the readings are vertically aligned
                    // spanBlank.Visible = true; 

                    _LoadA1c();

                    if (mModuleController == null)  // construct a controller if we have not been assigned one
                        mModuleController = new BloodGlucoseModuleController();
                    
                        // TODO: next lines should be handled by mModuleController
                    //this.SummaryState = SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT;
                    //this.PrimaryText = "180";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyCholesterol").ToString();
                    this.NavigationUrl = "/Pages/Patient/Cholesterol.aspx";
                    addReadingUrl = "PopUp.aspx?reading=1&modid=" + ModuleIdAsInt.ToString();
                    addGoalUrl = "Popup.aspx?goal=1&modid=" + ModuleIdAsInt.ToString();
                    _LoadCHGoal();
                    if (mModuleController == null)  // construct a controller if we have not been assigned one
                        mModuleController = new CholesterolModuleController();
                        // TODO: next lines should be handled by mModuleController
                    //SummaryState = SUMMARY_STATE.SUMMARY_STATE_EXCELLENT;
                    //litTrackerData.Text = "120";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyMedications").ToString();
                    this.NavigationUrl = "/Pages/Patient/Medications.aspx";
                    // this.SecondaryText = GetGlobalResourceObject("Tracker", "Tracker_Text_AddMedications").ToString();
                    addReadingUrl = "PopUp.aspx?reading=1&modid=" + ModuleIdAsInt.ToString(); // "MedicationsAdd.aspx";
                    if (mModuleController == null)
                        mModuleController = new MedicationsModuleController();
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    this.Title = GetGlobalResourceObject("Tracker", "Tracker_Title_MyLifeCheck").ToString();
                    this.NavigationUrl = AHACommon.AHAAppSettings.MyLifeCheckURL.ToString();

                    HVManager.FileDataManager.FileItem fileItem = HVHelper.HVManagerPatientBase.FileDataManager.Item;

                    if (fileItem != null && fileItem.HealthScore.Value.HasValue)
                    {
                        this.SecondaryTextNoColor = "<a href=" + AHACommon.AHAAppSettings.MyLifeCheckURL.ToString() + ">" +
                            GetGlobalResourceObject("Tracker", "Tracker_Text_MLC_CheckAgain").ToString() + "</a> | ";
                        this.linkMLCReport.Visible = true;
                    }
                    else
                    {
                        this.SecondaryTextNoColor = 
                            GetGlobalResourceObject("Tracker", "Tracker_Text_MLC_GetStarted").ToString();
                    }

                    this.EnableAddButton = false;
                    this.SummaryState = SUMMARY_STATE.SUMMARY_STATE_NEUTRAL;
                    //this.PrimaryText = string.Empty;

                    if (mModuleController == null)  // construct a controller if we have not been assigned one
                        mModuleController = new MyLifeCheckModuleController(); 
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                default:
                    this.PrimaryText = "NO DATA";
                    this.SecondaryTextNoColor = "no data";
                    break;
            }

            // If there is a module controller, initialize it
            if (mModuleController != null)
            {
                mModuleController.InitializeSummary( this );
            }

            // Need to dynamically assign the id to <article> so it will render the appropriate icon
            litTrackerBegin.Text = "<article id='" + TrackerType + "' class='" + CssClass + "' >";
            litTrackerEnd.Text = "</article>";

            // Set the URLs for each tracker based on the value given to the control on the dashboard page
            aTrackerLink1.HRef = mNavigationUrl;
            aTrackerLink2.HRef = mNavigationUrl;

            // Do the Add Reading and Add Goal html
            if (mEnableAddButton)
            {
                string addTitle = (this.mModuleID != Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS) ?
                                        GetGlobalResourceObject("Tracker", "Tracker_Text_AddReading").ToString() :
                                         GetGlobalResourceObject("Tracker", "Tracker_Text_AddMedication").ToString();

                popupReading.Visible = true;
                if (mSummaryLocation != SUMMARY_LOCATION.SUMMARY_LOCATION_MODULE_CURRENT)
                {
                    popupReading.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetSummaryPopupOnClientClickContent(addReadingUrl, mModuleID);
                }
                else
                {
                    addReadingUrl += "&closesummary=0";
                    popupReading.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(addReadingUrl, mModuleID);
                }
                popupReading.Text = addTitle + " <b>+</b>";

                if (mSummaryLocation == SUMMARY_LOCATION.SUMMARY_LOCATION_MODULE_CURRENT && !string.IsNullOrEmpty(addGoalUrl))
                {
                    string goalText = string.Empty;

                    if (mModuleID == Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE)
                    {
                        goalText = GetGlobalResourceObject("Tracker", "Tracker_Text_AddA1c").ToString();
                    }
                    else
                    {
                        goalText = (IsGoalDefined) ? GetGlobalResourceObject("Tracker", "Tracker_Text_EditGoal").ToString() :
                                    GetGlobalResourceObject("Tracker", "Tracker_Text_AddGoal").ToString();
                    }

                    addGoalUrl += "&closesummary=0";
                    popupGoal.Text = goalText + " <b>+</b>";
                    popupGoal.Visible = true ;
                    popupGoal.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(addGoalUrl, mModuleID);
                }
            }
        }

        protected void DownloadMLCReport(object sender, EventArgs e)
        {
            HttpContext.Current.Server.ScriptTimeout = 300;
            HVManager.FileDataManager objFileDataManager = new HVManager.FileDataManager((Page as H360PageHelper).PersonGUID, (Page as H360PageHelper).RecordGUID);
            HVManager.FileDataManager.FileItem objFileItem = objFileDataManager.GetLatestFileItem();
            if (objFileItem != null)
            {
                string strFileName = objFileItem.Name.Value;
                Response.AddHeader("Content-disposition", "attachment; filename=" + strFileName);
                Response.ContentType = "application/pdf";
                Response.Clear();
                Response.OutputStream.Write(objFileItem.PdfBytes.Value, 0, objFileItem.PdfBytes.Value.Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
                Response.End();
            }
            else
            {
                objFileDataManager.FlushCache();
                Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
            }
        }

        public void ibIcon_OnClick(object sender, EventArgs e)
        {
            /**
            if (ModuleClicked != null )
                ModuleClicked(this, e);
            **/
        }
    }
}