﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleDetails.ascx.cs" Inherits="Heart360Web.Controls.Patient.ModuleDetails" %>

<%@ Register Src="~/Controls/Patient/ModuleAddReading.ascx" TagName="AddReadingBase" TagPrefix="Module" %>

            <!-- ## BEGIN LEARN MORE ## -->
			<section class="my-actions info-stats full">
                <div class="accordion">
					<div class="toggle-btn">
                        <h4><asp:Literal ID="litTitleLearnMore" runat="server" /><span class="gray-arrow"></span></h4>
                    </div>
                    <div class="content">
                        <asp:UpdatePanel ID="updatePanelActions" UpdateMode="Conditional" runat="server" >
                        <ContentTemplate>                      
                        <p><asp:Literal ID="liTipOfTheDay" runat="server"></asp:Literal></p>
                        <!-- in the following spans, the appropriate circle-check class is added programatically --> 
                        <ul>
					        <li id="liLearnMore1" runat="server" >
						        <span id="spanLearnMore1" runat="server"></span>
                                <p><a runat="server" id="lnkLearnMore1" target="_blank"></a></p>
					        </li>
					        <li id="liLearnMore2" runat="server" >
						        <span id="spanLearnMore2" runat="server"></span>
                                <p><a runat="server" id="lnkLearnMore2" target="_blank"></a></p>
					        </li>
					        <li id="liLearnMore3" runat="server" >
						        <span id="spanLearnMore3" runat="server"></span>
                                <p><a runat="server" id="lnkLearnMore3" target="_blank"></a></p>
					        </li>
					        <li id="liLearnMore4" runat="server" >
						        <span id="spanLearnMore4" runat="server"></span>
                                <p><a runat="server" id="lnkLearnMore4" target="_blank"></a></p>
					        </li>
					        <li id="liLearnMore5" runat="server" >
						        <span id="spanLearnMore5" runat="server"></span>
                                <p><a runat="server" id="lnkLearnMore5" target="_blank"></a></p>
					        </li>
				        </ul>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
			</section>
            <!-- ## END LEARN MORE ## -->

            <!-- ## BEGIN MY HISTORY ## -->
			<section class="my-history info-stats full">
				<div class="accordion">
					<div id="dtMyHistory" runat="server" class="toggle-btn">
						<h4><asp:Literal ID="litTitleHistory" runat="server" /><span class="gray-arrow"></span></h4>
					</div>
                    <div class="content">
                        <asp:UpdatePanel ID="updatePanelHistory" UpdateMode="Conditional" runat="server" >
                            <ContentTemplate>
                                <div class="full history-table-wrap">
                                    <asp:PlaceHolder ID="phHistoryTable" runat="server" />
                                </div>
                                <asp:PlaceHolder ID="phGraph" runat="server" />
                                <div class="full my-status-controls">
						            <div class="large-1 hide-for-small columns"></div>
							        <div class="large-7 large-offset-4 small-12 columns">
								        <div class="row">
                                            <div class="large-5 small-5 columns">
                                                <asp:DropDownList ID="ddlDateRange" runat="server" CssClass="fr gray-button full" AutoPostBack="true" OnSelectedIndexChanged="ddlDateRange_SelectedChanged" EnableViewState="true" >
                                                    <asp:ListItem Text="3 Months" Value="3" />
                                                    <asp:ListItem Text="6 Months" Value="6" />
                                                    <asp:ListItem Text="9 Months" Value="9" />
                                                    <asp:ListItem Text="1 Year" Value="12" />
                                                    <asp:ListItem Text="2 Years" Value="24" />
                                                </asp:DropDownList>
									        </div>
									        <div class="large-5 small-5 columns">
                                                <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="updatePanelHistory" runat="server" DisplayAfter="500">
                                                    <ProgressTemplate>
                                                        <asp:Image ID="Image1" runat="server" alt="*" ImageAlign="Middle" ImageUrl="~/images/ajax-loader.gif" />       
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>	
                                                <asp:Button ID="btnRefreshData" runat="server" CssClass="gray-button full" Text="<%$Resources:Tracker,Tracker_Text_RefreshData%>"  OnClick="OnRefreshData_Click" />
									        </div>
										
									        <div class="large-2 small-2 columns">
										        <div class="row">
											        <div class="large-6 small-6 columns">
                                                        <asp:LinkButton CssClass="pdf" runat="server" OnClick="OnPDF_Click"/>
											        </div>
											        <div class="large-6 small-6 columns">
												        <a href="#" class="print" runat="server" id='btnPrintReport'></a>
											        </div>
										        </div>
									        </div>
								        </div>
							        </div>
						        </div>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
				</div>
			</section>
            <!-- ## END MY HISTORY ## -->

            <!-- ## BEGIN ACTIONS I CAN TAKE ## -->
            <section class="learn-about info-stats full">
                <div class="accordion">
					<div class="toggle-btn">
						<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Title_Actions")%><span class="gray-arrow"></span></h4>
					</div>
					<div class="content">
                        <asp:UpdatePanel ID="updatePanelLearnMore" UpdateMode="Conditional" runat="server" >
                        <ContentTemplate>                     
                        <!-- The actions section uses an ASP Repeater in the code behind -->
                        <asp:Repeater ID="rptActions" OnItemDataBound="rptActions_ItemDataBound" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <span id="spanCircleCheck" runat="server" ></span>
                                    <p><asp:Literal ID="litActionText" runat="server"></asp:Literal></p>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        </ContentTemplate>                   
                        </asp:UpdatePanel>
					</div>
				</div>
			</section>
            <!-- ## END ACTIONS I CAN TAKE ## -->			

            <!-- ## BEGIN UPLOAD MY READINGS ## -->
            <section class="compatiblity info-stats full">
				<div class="accordion">
					<div class="toggle-btn">
						<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Title_Upload_Methods")%><span class="gray-arrow"></span></h4>
					</div>
					<div class="content">
						<ul>
                            <li>
								<span class="circle-check"></span>
								<p><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Devices_Description")%></p>
                                <a href="https://account.healthvault.com/Directory?target=Devices&amp;SearchType=ViewAll" target="_blank"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_LearnMore")%></a>
							</li>
                            <li>
								<span class="circle-check"></span>
								<p><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Upload_SMS")%></p>
                                <a href="<%=ConnectionCenterSms%>" target="_self"><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Upload_SMS_Link")%></a>
							</li>
                            <li id="liCampaignOnly" runat="server">
								<span class="circle-check"></span>
								<p><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Upload_IVR")%></p>
                                <a href="<%=ConnectionCenterIvr%>" target="_self"><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Upload_IVR_Link")%></a>
							</li>
							
							
						</ul>
					</div>
				</div>
			</section>
            <!-- ## END UPLOAD MY READINGS ## -->

            <!-- ## BEGIN A1c ## -->
            <section class="compatiblity info-stats full spb" runat="server" visible="false" id="sectionA1c">
				<div class="accordion">
					<div class="toggle-btn">
						<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Title_A1c")%><span class="gray-arrow"></span></h4>
					</div>
					<div class="content">

                        <asp:UpdatePanel ID="updatePanelHbA1c" UpdateMode="Conditional" runat="server" >
                            <ContentTemplate>

                                <div class="full history-table-wrap">
                                    <asp:PlaceHolder ID="phA1cTable" runat="server" />
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>							

					</div>
				</div>
			</section>
            <!-- ## END A1c ## -->

            <%--<div id="detailPopupDelete" class="shadowbox deleteitem">
				<div class="modal-wrap deleteitem">
                    <span>
                        <asp:Button ID="popupBtnDeleteOk" runat="server" Text="Ok" OnClientClick="javascript:document.getElementById('popupBtnClose').click(); return true;" />
                        <asp:Button ID="popupBtnDeleteCancel" runat="server" Text="Cancel" OnClientClick="javascript:document.getElementById('popupBtnClose').click(); return false;" />
                    </span>
					<a id="popupBtnClose" class="close-modal">CLOSE</a>
				</div>
			</div>

            <div id="detailPopupEdit" class="shadowbox edititem">
				<div class="modal-wrap edititem">
                    <Module:AddReadingBase ID="AddReadingBase" runat="server" />	
					<a class="close-modal">CLOSE</a>
				</div>
			</div>--%>
