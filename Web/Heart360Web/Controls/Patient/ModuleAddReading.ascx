﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleAddReading.ascx.cs" Inherits="Heart360Web.Controls.Patient.ModuleAddReading" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
        <div class="full">
            <!-- ## Add Reading Title ## -->
	        <div class="full">
		        <h3><asp:Literal ID="litTitle" runat="server"></asp:Literal></h3>
	        </div>

            <!-- ## Add Reading Validation Area, one summary for each tracker  ## -->
            <div class="large-12 small-12 columns spb" >
                <asp:ValidationSummary ID="vsBG" ValidationGroup="vgBG"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

                <asp:ValidationSummary ID="vsBP" ValidationGroup="vgBP"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

                <!-- this validation is used when the control is not used as an add reading popup (e.g. new user) -->
                <asp:ValidationSummary ID="vsBPExternal" ValidationGroup="vgBPExternal"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

                <asp:ValidationSummary ID="vsCH" ValidationGroup="vgCH"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

                <asp:ValidationSummary ID="vsCHPostCalculate" ValidationGroup="vgCHPostCalculate"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

                <asp:ValidationSummary ID="vsPA" ValidationGroup="vgPA"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

                <asp:ValidationSummary ID="vsW" ValidationGroup="vgW"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

                <asp:ValidationSummary ID="vsA1c" ValidationGroup="vgA1c"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

            </div>

            <!-- ## BEGIN Blood Pressure ## -->
            <div id="divBloodPressure" visible="false" runat="server">
                <div class="row">
	                <div class="large-3 small-12 columns">
		                <label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,BPItemForm_HeartRate_Tip %>" ><%=GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodPressure_HeartRate")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtHeartRate" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                        <asp:CustomValidator ID="vBPHeartRate" runat="server" EnableClientScript="false" ValidationGroup="vgBP" Display="None" OnServerValidate="ValidateBPHeartRate"></asp:CustomValidator>
	                </div>

                </div>
			
                <div class="row">
	                <div class="large-3 small-12 columns">
		                <label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,BPItemForm_Systolic_Tip %>" ><%=GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodPressure_Systolic")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
		                <asp:TextBox ID="txtSystolic" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                        <asp:CustomValidator ID="vBPSystolic" runat="server" EnableClientScript="false" ValidationGroup="vgBP" Display="None" OnServerValidate="ValidateBPSystolic"></asp:CustomValidator>
	                </div>
                </div>

                <div class="row">
	                <div class="large-3 small-12 columns">
		                <label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,BPItemForm_Diastolic_Tip %>" ><%=GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodPressure_Diastolic")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
		                <asp:TextBox ID="txtDiastolic" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                        <asp:CustomValidator ID="vBPDiastolic" runat="server" EnableClientScript="false" ValidationGroup="vgBP" Display="None" OnServerValidate="ValidateBPDiastolic"></asp:CustomValidator>
	                </div>
                </div>  

                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,BPItemForm_IrregularHeartbeat_Tip %>" ><%=GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodPressure_IrregularHeartbeat")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlIrregularHeartbeat" CssClass="dropdowns" runat="server"></asp:DropDownList>
                    </div>
                </div>

                <asp:CustomValidator ID="vBPExternal" runat="server" EnableClientScript="false" ValidationGroup="vgBPExternal" Display="None" OnServerValidate="ValidateBPExternal"></asp:CustomValidator>    
            </div>
            <!-- ## END Blood Pressure ## -->      

            <!-- ## BEGIN Weight ## -->
            <div id="divWeight" visible="false" runat="server">
                <div class="row" >
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Weight")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtWeight" CssClass="required" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:CustomValidator ID="vWWeight" runat="server" EnableClientScript="false" ValidationGroup="vgW" Display="None" OnServerValidate="ValidateWWeight"></asp:CustomValidator>
                    </div>
                </div>
                <div id="divWeightHeight" class="row" visible="false" runat="server">
                    <div class="large-4 small-12 columns">
                        <label id="lblWeight" class="text-left" runat="server"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Height")%>:</label>
                    </div>

                    <div class="large-2 small-12 columns">
                        <asp:TextBox ID="tbHeightA" CssClass="required" runat="server" MaxLength="1"></asp:TextBox>
                    </div>
                    <div class="large-2 small-12 columns">
                        <label class="text-left" >feet</label>
                    </div>
                    
                    <div class="large-2 small-12 columns">
                        <asp:TextBox ID="tbHeightB" CssClass="required" runat="server" MaxLength="2"></asp:TextBox>
                    </div>
                    <div class="large-2 small-12 columns">
                        <label class="text-left" >inches</label>
                    </div>
                    <asp:CustomValidator ID="vWHeight" runat="server" EnableClientScript="false" ValidationGroup="vgW" Display="None" OnServerValidate="ValidateWHeight"></asp:CustomValidator>
                </div>
            </div>
            <!-- ## END Weight ## -->
            
            <!-- ## BEGIN Physical Activity ## -->
            <div id="divActivity" visible="false" runat="server">
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Activity")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlActivityType" CssClass="required" runat="server"></asp:DropDownList>
                        <asp:CustomValidator ID="vPAActivity" runat="server" EnableClientScript="false" ValidationGroup="vgPA" Display="None" OnServerValidate="ValidatePAActivity"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Duration")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtDuration" CssClass="required" runat="server" MaxLength="6"></asp:TextBox>
                        <asp:CustomValidator ID="vPADuration" runat="server" EnableClientScript="false" ValidationGroup="vgPA" Display="None" OnServerValidate="ValidatePADuration"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Steps")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtSteps" CssClass="required" runat="server"></asp:TextBox>
                        <asp:CustomValidator ID="vPASteps" runat="server" EnableClientScript="false" ValidationGroup="vgPA" Display="None" OnServerValidate="ValidatePASteps"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Intensity")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlIntensity" CssClass="required" runat="server"></asp:DropDownList>
                        <asp:CustomValidator ID="vPAIntensity" runat="server" EnableClientScript="false" ValidationGroup="vgPA" Display="None" OnServerValidate="ValidatePAIntensity"></asp:CustomValidator>
                    </div>
                </div>
            </div>
            <!-- ## END Physical Activity ## -->
            
            <!-- ## BEGIN Blood Glucose ## -->
            <div id="divBloodGlucose" visible="false" runat="server">
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_BloodGlucose")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtBloodGlucose" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                        <asp:CustomValidator ID="vBGBg" runat="server" EnableClientScript="false" ValidationGroup="vgBG" Display="None" OnServerValidate="ValidateBGBloodGlucose"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_ReadingType")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlReadingType" CssClass="dropdowns" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Action")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlAction" CssClass="dropdowns" runat="server"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <!-- ## END Blood Glucose ## -->

            <!-- ## BEGIN A1c ## -->
            <div id="divA1c" visible="false" runat="server">
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,Text_A1c_Tip %>"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_A1c")%>:</label>
                    </div>
                    <div class="large-3 small-12 columns" style="float:left;">
                        <asp:TextBox ID="txtA1c" CssClass="required" runat="server" MaxLength="2"></asp:TextBox>
                        <asp:CustomValidator ID="vA1c" runat="server" EnableClientScript="false" ValidationGroup="vgA1c" OnServerValidate="ValidateA1c" Display="None"></asp:CustomValidator>                    
                    </div>
                </div>
            </div>

            <asp:FilteredTextBoxExtender 
                ID="FilteredTextBoxExtender_HbA1c" 
                runat="server" 
                Enabled="true" 
                TargetControlID="txtA1c" 
                FilterType="Numbers"
                ValidChars="/" >
            </asp:FilteredTextBoxExtender>
            <!-- ## END A1c ## -->
            
             <!-- ## BEGIN Cholesterol ## -->
            <div id="divCholesterol" visible="false" runat="server">
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_HDL")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtHDL" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                        <asp:CustomValidator ID="vCHHdl" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHHdl"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_LDL")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtLDL" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                        <asp:CustomValidator ID="vCHLdl" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHLdl"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Triglycerides")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtTriglycerides" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                        <asp:CustomValidator ID="vCHTrig" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHTrig"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row spb">
                    <div class="large-3 small-3 columns"><label class="text-left" ></label></div>
                    <div class="large-9 small-9 columns">
                        <asp:Button ID="btnCalculate" runat="server" CssClass="gray-button full" Text="<%$Resources:Common,Text_Calculate %>" OnClick="OnRecalculateTC" CommandName="Add" />
                        <asp:CustomValidator ID="vCHTc" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHTotalC"></asp:CustomValidator>
                        <asp:CustomValidator ID="vGHTc2" runat="server" EnableClientScript="false" ValidationGroup="vgCHPostCalculate" Display="None" OnServerValidate="ValidateCHPostTotalC"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 small-12 columns">
                        <label class="text-left" > <%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_TotalCholesterol")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtTotalCholesterol" CssClass="required" runat="server" MaxLength="3"></asp:TextBox>
                    </div>
                </div>
            </div>
            <!-- ## END Cholesterol ## -->

            <!-- ## BEGIN Common Data Entry ## -->
            <div id="divCommonData" visible="false" runat="server" >
                <div class="row">
                    <div class="large-3 small-12 columns">
		                <p><label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,Text_DateOfReading_Tip %>" ><%=GetGlobalResourceObject("Tracker", "Tracker_Text_AddReading_Date")%>:</label></p>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:CalendarExtender ID="calDatePicker" TargetControlID="txtDate" PopupButtonID="btnCalendar" CssClass="popup_cal" runat="server" />
		                <asp:TextBox ID="txtDate" CssClass="required" runat="server"></asp:TextBox>
                        
                        <asp:ImageButton ID="btnCalendar" CssClass="calendar-popup-button" runat="server" BorderStyle="None" AlternateText="Calendar popup" style="border:none !important;" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                        
                        
                    </div>
                </div>

                <div id="divCommonDataTime" class="row" runat="server">
                    <div class="large-3 small-12 columns">
		                <label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,Text_TimeOfReading_Tip %>"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_AddReading_Time")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
		                <asp:DropDownList TabIndex="5" ID="ddlTime" runat="server" CssClass="dropdowns" />
                    </div>
                </div>

                <div id="divCommonDataReadingSource" class="row" runat="server">
                    <div class="large-3 small-12 columns">
		                <label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,Text_ReadingSource_Tip %>" ><%=GetGlobalResourceObject("Tracker", "Tracker_Text_AddReading_Source")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList TabIndex="6" ID="ddlReadingSource" runat="server" CssClass="dropdowns" />
                    </div>
                </div>
			
                <div id="divComments" class="row" runat="server">
                    <div class="large-12 small-12 columns">
		                <p><label runat="server" class="text-left has-tip tip-left" data-tooltip="" title="<%$Resources:Tracker,Text_Comments_Tip %>" ><%=GetGlobalResourceObject("Tracker", "Tracker_Text_AddReading_Comments")%>:</label>
		                <textarea id="txtComments" rows="4" class="textarea" name="comments" runat="server"></textarea></p>
                    </div>
                </div>
			
                <div class="save row spt">
                    <div class="large-12 small-12 columns">
                        <asp:Button ID="btnSave" CssClass="gray-button full" runat="server" onclick="btnSave_Click" />
                    </div>
                </div>
            </div>
            <asp:CustomValidator ID="vBPCommon" runat="server" EnableClientScript="false" ValidationGroup="vgBP" Display="None" OnServerValidate="ValidateDateTime"></asp:CustomValidator>
            <asp:CustomValidator ID="vBGCommon" runat="server" EnableClientScript="false" ValidationGroup="vgBG" Display="None" OnServerValidate="ValidateDateTime"></asp:CustomValidator>
            <asp:CustomValidator ID="vCHCommon" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateDateTime"></asp:CustomValidator>
            <asp:CustomValidator ID="vPACommon" runat="server" EnableClientScript="false" ValidationGroup="vgPA" Display="None" OnServerValidate="ValidateDateTime"></asp:CustomValidator>
            <asp:CustomValidator ID="vWCommon" runat="server" EnableClientScript="false" ValidationGroup="vgW" Display="None" OnServerValidate="ValidateDateTime"></asp:CustomValidator>
            <asp:CustomValidator ID="vBPExternalCommon" runat="server" EnableClientScript="false" ValidationGroup="vgBPExternal" Display="None" OnServerValidate="ValidateDateTime"></asp:CustomValidator>
            <!-- ## END Common Data Entry ## -->

             <asp:Label ID="libError" runat="server"></asp:Label>
        </div>

        <%-- Filters are used to only allow numbers --%>
        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender1"
            runat="server"
            Enabled="true"
            TargetControlID="txtHeartRate"
            FilterType="Numbers">
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender2"
            runat="server"
            Enabled="true"
            TargetControlID="txtSystolic"
            FilterType="Numbers">
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender3"
            runat="server"
            Enabled="true"
            TargetControlID="txtDiastolic"
            FilterType="Numbers">
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender4"
            runat="server"
            Enabled="true"
            TargetControlID="txtWeight"
            FilterType="Numbers, Custom"
            ValidChars="." >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender5"
            runat="server"
            Enabled="true"
            TargetControlID="tbHeightA"
            FilterType="Numbers" >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender6"
            runat="server"
            Enabled="true"
            TargetControlID="tbHeightB"
            FilterType="Numbers, Custom" ValidChars="." >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender7"
            runat="server"
            Enabled="true"
            TargetControlID="txtBloodGlucose"
            FilterType="Numbers" >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender8"
            runat="server"
            Enabled="true"
            TargetControlID="txtHDL"
            FilterType="Numbers" >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender9"
            runat="server"
            Enabled="true"
            TargetControlID="txtLDL"
            FilterType="Numbers" >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender10"
            runat="server"
            Enabled="true"
            TargetControlID="txtTriglycerides"
            FilterType="Numbers" >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender11"
            runat="server"
            Enabled="true"
            TargetControlID="txtTotalCholesterol"
            FilterType="Numbers" >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender12"
            runat="server"
            Enabled="true"
            TargetControlID="txtSteps"
            FilterType="Numbers" >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender
            ID="FilteredTextBoxExtender13"
            runat="server"
            Enabled="true"
            TargetControlID="txtDuration"
            FilterType="Numbers, Custom" ValidChars="." >
        </asp:FilteredTextBoxExtender>

        <asp:FilteredTextBoxExtender 
            ID="FilteredTextBoxExtender14" 
            runat="server" 
            Enabled="true" 
            TargetControlID="txtDate" 
            FilterType="Numbers,Custom"
            ValidChars="/" >
        </asp:FilteredTextBoxExtender>



<asp:HiddenField ID="hidCalculate" runat="server" Value="0" />