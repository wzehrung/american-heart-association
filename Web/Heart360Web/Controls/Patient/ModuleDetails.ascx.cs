﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;


namespace Heart360Web.Controls.Patient
{
    public partial class ModuleDetails : System.Web.UI.UserControl, IPeriodicDataManager
    {
        public delegate void RefreshDataCompletedCallback();

            // these are urls for Other Ways to Add Readings accordion
        protected string ConnectionCenterSms = string.Empty;
        protected string ConnectionCenterIvr = string.Empty;

        private Heart360Web.PatientPortal.MODULE_ID           mModuleID;
        private Heart360Web.ModuleControllerInterface   mModuleController;
        private RefreshDataCompletedCallback            mRefreshDataCallback = null ;

        public ModuleDetails() : base()
        {
            mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
            mModuleController = null;
        }

        public int ModuleIdAsInt      // need this so (html/asp) markup can specify modules
        {
            set
            {
                mModuleID = Heart360Web.PatientPortal.GetModuleIdFromInt(value);
            }
            get
            {
                return (int)mModuleID;
            }
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        public Heart360Web.ModuleControllerInterface ModuleController
        {
            get { return mModuleController; }
            set { mModuleController = value; }
        }

            // This is for hooking up Asynchronous Update Triggers or for calling Update() of UpdatePanel directly
        public UpdatePanel MyHistoryUpdatePanel
        {
            get { return this.updatePanelHistory; }
        }

        public UpdatePanel MyActionsUpdatePanel
        {
            get { return this.updatePanelActions; }
        }

        public UpdatePanel MyLearnMoreUpdatePanel
        {
            get { return this.updatePanelLearnMore; }
        }

        public UpdatePanel MyHbA1cUpdatePanel
        {
            get { return this.updatePanelHbA1c; }
        }

        public RefreshDataCompletedCallback RefreshDataCallback
        {
            set { mRefreshDataCallback = value; }
        }


        protected void Page_Init(object sender, EventArgs e)
        {

            switch (mModuleID)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    litTitleLearnMore.Text = GetGlobalResourceObject("Tracker", "Tracker_Details_Title_LearnMore").ToString();
                    litTitleHistory.Text = GetGlobalResourceObject("Tracker", "Tracker_Details_Title_History").ToString();
                    //litTitleGraph.Text = GetGlobalResourceObject("Tracker", "Tracker_Details_Title_Graph").ToString();
                    //litTitleActions.Text = GetGlobalResourceObject("Tracker", "Tracker_Details_Title_Actions").ToString();
                    //litTitleDevices.Text = GetGlobalResourceObject("Tracker", "Tracker_Details_Title_Devices").ToString();
                    //AddReadingBase.ModuleID = mModuleID;
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                default:
                    break;
            }

            switch (mModuleID)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    if (mModuleController == null)
                    {
                        mModuleController = new BloodPressureModuleController();
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    if (mModuleController == null)
                    {
                        mModuleController = new BloodGlucoseModuleController();
                        sectionA1c.Visible = true; // Only relevant for this tracker
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    if (mModuleController == null)
                    {
                        mModuleController = new CholesterolModuleController();
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    if (mModuleController == null)
                    {
                        mModuleController = new PhysicalActivityModuleController();
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    if (mModuleController == null)
                    {
                        mModuleController = new WeightModuleController();
                    }
                    break;

                default:
                    break;
            }

            this.ConnectionCenterSms = /* HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + */ GRCBase.ConfigReader.GetValue("ConnectionCenterReminderSMS");
            this.ConnectionCenterIvr = /* HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + */ GRCBase.ConfigReader.GetValue("ConnectionCenterReminderIVR");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // We need to add the history tables and graphs regardless of post back
            if (mModuleController != null)
                mModuleController.InitializeDetail(this);

            if (!IsPostBack)
            {
                // We need to determine if the user is in a campaign and if not, we need to hide
                // an entry in 'Other Ways to Upload My Reading' accordion.
                AHAHelpContent.Patient objInCampaign = AHAHelpContent.Patient.IsPatientInACampaign(Convert.ToInt32(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID));

                if (objInCampaign.PatientID == null)
                {
                    this.liCampaignOnly.Visible = false;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // don't think we need '~/Pages/Patient/'
         
            string  ocJavascript = "window.open( 'PrinterFriendly.aspx?modid=" + ModuleIdAsInt.ToString() + "&daterange=" + DateRange.ToString() + "', '','scrollbars=yes,status=no,toolbar=no,menubar=no,location=no'); return false ;" ;
            btnPrintReport.Attributes.Add("onclick", ocJavascript);
        }

        protected override object SaveViewState()
        {
            // We need to see if there are any open accordions
            string openacc = string.Empty;
            string classNames = dtMyHistory.Attributes["class"];

            if (classNames.Contains("dt-active"))
                openacc = "mh";     // My History

            // If there is an open accordion, assign it to view state
            if (!string.IsNullOrEmpty(openacc))
            {
                ViewState["CurrentAccordion"] = openacc;
            }
            else if( ViewState["CurrentAccordion"] != null )
            {
                ViewState["CurrentAccordion"] = "" ;
            }

            return base.SaveViewState();
        }

        protected void ddlDateRange_SelectedChanged( object sender, EventArgs e )
        {
            int     newIdx = this.ddlDateRange.SelectedIndex ;
            int     months = int.Parse(this.ddlDateRange.SelectedValue);

            switch (months)
            {
                case 3:
                    DateRange = AHACommon.DataDateReturnType.Last3Months;
                    break;

                case 6:
                    DateRange = AHACommon.DataDateReturnType.Last6Months;
                    break;

                case 9:
                    DateRange = AHACommon.DataDateReturnType.Last9Months;
                    break;

                case 12:
                    DateRange = AHACommon.DataDateReturnType.LastYear;
                    break;

                case 24:
                    DateRange = AHACommon.DataDateReturnType.Last2Years;
                    break;

                default:
                    break;
            }
        }

            //
            // First section from the top: Actions I can take
            //
        public void SetActionsContents(List<AHAHelpContent.MLCActionICanTake> actionList)
        {
            if (actionList != null && actionList.Count > 0)
            {
                rptActions.DataSource = actionList;
                rptActions.DataBind();
            }
        }

        protected void rptActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AHAHelpContent.MLCActionICanTake objActionICanTake = e.Item.DataItem as AHAHelpContent.MLCActionICanTake;
                Literal litActionText = e.Item.FindControl("litActionText") as Literal;
                litActionText.Text = objActionICanTake.ActionText;

                System.Web.UI.HtmlControls.HtmlControl sc = e.Item.FindControl("spanCircleCheck") as System.Web.UI.HtmlControls.HtmlControl;
                if (sc != null)
                    sc.Attributes.Add("class", GetCircleCheckName());
            }
        }

            //
            // Second section from the top: My History
            //

            // NOTE: There is some screwy runtime compilation error if we use the real enumeration.
            // Phil Brock, Andrew Forman and JM from Push have all looked into it deeply.....
        public void AddHistoryUserControl( int modID )      // Heart360Web.PatientPortal.MODULE_ID modID)
        {
            bool addGrid = false;

            try
            {
                switch (modID)
                {
                    case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                        {
                            Heart360Web.Controls.Patient.BloodPressure.BPItemList bpil = (Heart360Web.Controls.Patient.BloodPressure.BPItemList)LoadControl("~/Controls/Patient/BloodPressure/BPItemList.ascx");

                            // call methods of bpil to initialize it
                            bpil.IPeriodicDataManager = this;
                            bpil.LoadItems(this.DateRange);

                            phHistoryTable.Controls.Add(bpil);

                            addGrid = true;
                        }
                        break;

                    case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                        {
                            Heart360Web.Controls.Patient.BloodGlucose.BGItemList bgil = (Heart360Web.Controls.Patient.BloodGlucose.BGItemList)LoadControl("~/Controls/Patient/BloodGlucose/BGItemList.ascx");

                            // call methods of bpil to initialize it
                            bgil.IPeriodicDataManager = this;
                            bgil.LoadItems(this.DateRange);

                            phHistoryTable.Controls.Add(bgil);

                            // A1c
                            Heart360Web.Controls.Patient.BloodGlucose.A1cItemList bgA1c = (Heart360Web.Controls.Patient.BloodGlucose.A1cItemList)LoadControl("~/Controls/Patient/BloodGlucose/A1cItemList.ascx");
                            bgA1c.IPeriodicDataManager = this;
                            bgA1c.LoadItems(this.DateRange);
                            phA1cTable.Controls.Add(bgA1c);

                            addGrid = true;
                        }
                        break;

                    case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                        {
                            Heart360Web.Controls.Patient.Cholesterol.CholesterolItemList cil = (Heart360Web.Controls.Patient.Cholesterol.CholesterolItemList)LoadControl("~/Controls/Patient/Cholesterol/CholesterolItemList.ascx");

                            // call methods of cil to initialize it
                            cil.IPeriodicDataManager = this;
                            cil.LoadItems(this.DateRange);

                            phHistoryTable.Controls.Add(cil);

                            addGrid = true;
                        }
                        break;


                    case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                        {
                            Heart360Web.Controls.Patient.PhysicalActivity.PAItemList pail = (Heart360Web.Controls.Patient.PhysicalActivity.PAItemList)LoadControl("~/Controls/Patient/PhysicalActivity/PAItemList.ascx");

                            // call methods of pail to initialize it
                            pail.IPeriodicDataManager = this;
                            pail.LoadItems(this.DateRange);

                            phHistoryTable.Controls.Add(pail);

                            addGrid = true;
                        }
                        break;


                    case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        {
                            Heart360Web.Controls.Patient.Weight.WeightItemList wil = (Heart360Web.Controls.Patient.Weight.WeightItemList)LoadControl("~/Controls/Patient/Weight/WeightItemList.ascx");

                            wil.IPeriodicDataManager = this;
                            wil.LoadItems(this.DateRange);
                            phHistoryTable.Controls.Add(wil);

                            addGrid = true;
                        }
                        break;

                    default:
                        break;
                }

                if (addGrid)
                {
                    Heart360Web.Controls.Patient.ModuleItemGraph mig = (Heart360Web.Controls.Patient.ModuleItemGraph)LoadControl("~/Controls/Patient/ModuleItemGraph.ascx");

                    mig.ModuleID = Heart360Web.PatientPortal.GetModuleIdFromInt(modID); 
                    mig.IPeriodicDataManager = this;

                    phGraph.Controls.Add(mig);
                }
            }
            catch (Exception exc)
            {
                string msg = exc.Message;
            }
            finally
            {
                int iii = 0;
            }
        }

        private string GetCircleCheckName()
        {
            string retstat = "default-circle-check";

            switch (this.mModuleController.GetRiskLevel())
            {
                case RiskLevelHelper.RiskLevelResultTypeEnum.Excellent:
                    retstat = "green-circle-check";
                    break;

                case RiskLevelHelper.RiskLevelResultTypeEnum.NeedsImprovement:
                    retstat = "orange-circle-check";
                    break;

                case RiskLevelHelper.RiskLevelResultTypeEnum.Warning:
                    retstat = "red-circle-check";
                    break;

                case RiskLevelHelper.RiskLevelResultTypeEnum.None:
                default:
                    // already defined
                    break;
            }
            return retstat;
        }

            //
            // Fourth section from the top: Learn More
            //
        public void SetLearnMoreContents( List<AHAHelpContent.Content> contentList)
        {
            string riskCircleCheckName = GetCircleCheckName();

            liLearnMore1.Visible = false;
            liLearnMore2.Visible = false;
            liLearnMore3.Visible = false;
            liLearnMore4.Visible = false;
            liLearnMore5.Visible = false;

            // Up to five links
            int nIndex = 0;
            foreach (AHAHelpContent.Content objContent in contentList )
            {
                switch (nIndex)
                {
                    case 0:
                        lnkLearnMore1.HRef = contentList[0].URL;
                        lnkLearnMore1.InnerText = contentList[0].DidYouKnowLinkTitle;
                        spanLearnMore1.Attributes.Add("class", riskCircleCheckName);
                        liLearnMore1.Visible = true;
                        break;
                    case 1:
                        lnkLearnMore2.HRef = contentList[1].URL;
                        lnkLearnMore2.InnerText = contentList[1].DidYouKnowLinkTitle;
                        spanLearnMore2.Attributes.Add("class", riskCircleCheckName);
                        liLearnMore2.Visible = true;
                        break;
                    case 2:
                        lnkLearnMore3.HRef = contentList[2].URL;
                        lnkLearnMore3.InnerText = contentList[2].DidYouKnowLinkTitle;
                        spanLearnMore3.Attributes.Add("class", riskCircleCheckName);
                        liLearnMore3.Visible = true;
                        break;
                    case 3:
                        lnkLearnMore4.HRef = contentList[3].URL;
                        lnkLearnMore4.InnerText = contentList[3].DidYouKnowLinkTitle;
                        spanLearnMore4.Attributes.Add("class", riskCircleCheckName);
                        liLearnMore4.Visible = true;
                        break;
                    case 4:
                        lnkLearnMore5.HRef = contentList[4].URL;
                        lnkLearnMore5.InnerText = contentList[4].DidYouKnowLinkTitle;
                        spanLearnMore5.Attributes.Add("class", riskCircleCheckName);
                        liLearnMore5.Visible = true;
                        break;

                    default:
                        break;
                }
                nIndex++;
            }
        }

        public void SetLearnMoreContents( int _languageID )
        {
                // get the string to display out of a resource file

            liLearnMore1.Visible = true;
            liLearnMore2.Visible = false;
            liLearnMore3.Visible = false;
            liLearnMore4.Visible = false;
            liLearnMore5.Visible = false;
            lnkLearnMore1.HRef = string.Empty;
            lnkLearnMore1.InnerText = GetGlobalResourceObject("Tracker", "Tracker_Text_LearnMoreDefault").ToString();
            spanLearnMore1.Attributes.Add("class", GetCircleCheckName());
            liLearnMore1.Visible = true;
        }

        public void SetTipOfTheDay( string stod )
        {
            if( !string.IsNullOrEmpty(stod ) )
                liTipOfTheDay.Text = UIHelper.RenderLink(stod );
        }

 #region IPeriodicDataManager Members

        public void LoadItems()
        {
            //ucBPItemGraph1.LoadItems(DateRange);
            //ucBPItemList1.LoadItems(DateRange);
            //OutterUpdatePanel.Update();
        }

        public void FocusTabOne()
        {
            throw new NotImplementedException();
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        protected void OnRefreshData_Click(object sender, EventArgs e)
        {
            HVHelper.HVManagerPatientBase.FlushCache();

            if (mRefreshDataCallback != null)
                mRefreshDataCallback();
//            Response.Redirect(Request.RawUrl);
        }

        protected void OnPDF_Click(object sender, EventArgs e)
        {
            OnCreatePDF();
        }

        public void OnCreatePDF()
        {
            switch (mModuleID)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    PDFHelper.CreateReport(PatientGuids.CurrentPatientGuids, PDFType.BloodPressure, DateRange, DataViewType.TableAndCombinedChart);
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    PDFHelper.CreateReport(PatientGuids.CurrentPatientGuids, PDFType.BloodGlucose, DateRange, DataViewType.TableAndChart);
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    PDFHelper.CreateReport(PatientGuids.CurrentPatientGuids, PDFType.Cholesterol, DateRange, DataViewType.TableAndCombinedChart);
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    PDFHelper.CreateReport(PatientGuids.CurrentPatientGuids, PDFType.Exercise, DateRange, DataViewType.TableAndChart);
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    PDFHelper.CreateReport(PatientGuids.CurrentPatientGuids, PDFType.Weight, DateRange, DataViewType.TableAndChart);
                    break;

                default:
                    break;
            }
        }

        public AHACommon.DataDateReturnType DateRange
        {
            /**
            get
            {
                int months = int.Parse(this.ddlDateRange.SelectedValue);
                AHACommon.DataDateReturnType retstat = AHACommon.DataDateReturnType.Last3Months;

                switch (months)
                {
                    case 3:
                        retstat = AHACommon.DataDateReturnType.Last3Months;
                        break;

                    case 6:
                        retstat = AHACommon.DataDateReturnType.Last6Months;
                        break;

                    case 9:
                        retstat = AHACommon.DataDateReturnType.Last9Months;
                        break;

                    case 12:
                        retstat = AHACommon.DataDateReturnType.LastYear;
                        break;

                    case 24:
                        retstat = AHACommon.DataDateReturnType.Last2Years;
                        break;

                    default:
                        break;
                }
                return retstat;
            }
            **/
            get
            {
                if (ViewState["DataRange"] != null)
                    return (AHACommon.DataDateReturnType)ViewState["DataRange"];
                else
                    return AHACommon.DataDateReturnType.Last3Months;
            }

            set
            {
                if (ViewState["DataRange"] == null)
                {
                    ViewState.Add("DataRange", value);
                }
                else
                {
                    ViewState["DataRange"] = value;
                }
            }
        }

        public TrackerType TrackerType
        {
            get 
            {
                switch (mModuleID)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                        return AHAAPI.TrackerType.BloodGlucose;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                        return AHAAPI.TrackerType.Cholesterol;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                        return AHAAPI.TrackerType.PhysicalActivity;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        return AHAAPI.TrackerType.Weight;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    default:
                        return AHAAPI.TrackerType.BloodPressure;
                }
            }
        }

        public DataViewType DataViewType
        {
            get { return DataViewType.TableAndCombinedChart; }
        }

#endregion

    }
}