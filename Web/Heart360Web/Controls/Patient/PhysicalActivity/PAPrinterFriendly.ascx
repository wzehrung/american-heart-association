﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PAPrinterFriendly.ascx.cs" Inherits="Heart360Web.Controls.Patient.PhysicalActivity.PAPrinterFriendly" %>

<%@ Register Src="~/Controls/Patient/ModuleItemGraph.ascx" TagName="BPGraph" TagPrefix="printerFriendlyInnerCtrl" %>

<h3><%=GetGlobalResourceObject("Reports", "Text_Heading_MyActivityRecord")%></h3>
<printerFriendlyInnerCtrl:BPGraph ID="GraphCtrl" runat="server" />
<div class="full history-table-wrap" >
    <asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="false" runat="server">
        <HeaderTemplate>
    	         <table>
		            <thead>
			            <tr>
				            <th width="20%"><%=GetGlobalResourceObject("Reports", "Text_Date")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_Activity")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_DurationInMinutes")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_Intensity")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_Steps")%></th>
                            <th width="40%"><%=GetGlobalResourceObject("Reports", "Text_Comments")%></th>
			            </tr>
		            </thead>
                    <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
			    <td><asp:Literal ID="litDate" runat="server"/></td>
			    <td><asp:Literal ID="litActivity" runat="server"/></td>
			    <td><asp:Literal ID="litDuration" runat="server"/></td>
			    <td><asp:Literal ID="litIntensity" runat="server"/></td>
			    <td><asp:Literal ID="litSteps" runat="server"/></td>
                <td><asp:Literal ID="litComments" runat="server"/></td>
		    </tr>
        </ItemTemplate>
        <FooterTemplate>
                    </tbody>
	            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>

