﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;
using HVManager;
using AHACommon;
using AHAAPI;

namespace Heart360Web.Controls.Patient.PhysicalActivity
{
    public partial class PAItemList : System.Web.UI.UserControl, IDataList
    {
        #region Private Members

        const string _vgAdd = "Add";
        const string _vgEdit = "Edit";

        // Givens: all interaction with the list (Edit, Delete) is done through a popup with no postback.
        //         This control does not change/manage current date range
        //
        // How it works: we always render the control on Page_Load!
        //               ModuleDetails manages ViewState["DataRange"]
/**
        AHACommon.DataDateReturnType CurrentDateRange
        {
            get
            {
                if (ViewState["DataRange"] != null)
                    return (AHACommon.DataDateReturnType)ViewState["DataRange"];
                else
                    return AHACommon.DataDateReturnType.Last3Months;
            }
            set
            {
                if (ViewState["DataRange"] == null)
                {
                    ViewState.Add("DataRange", value);
                }
                else
                {
                    ViewState["DataRange"] = value;
                }
            }
        }
**/
        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "date";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return false;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }

        #endregion

        #region Public Properties

        IPeriodicDataManager m_IPeriodicDataManager = null;
        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            /**
            if (!Page.IsPostBack)
            {
                CurrentDateRange = m_IPeriodicDataManager.DateRange;
                _bindDataToGridView();
            }
            **/
            //_bindDataToGridView();

            //linkRefreshSiteData.OnClientClick = string.Format("return RefreshSiteDataPatientForUrl('{0}');", AHAAPI.WebAppNavigation.H360.PAGE_TRACKER_BP);
        }

        protected override void OnPreRender( EventArgs e)
        {
            base.OnPreRender(e);

            _bindDataToGridView();
        }


        private void _bindDataToGridView()
        {
            DataDateReturnType ddrt = m_IPeriodicDataManager.DateRange;
            LinkedList<ExerciseDataManager.ExerciseItem> ascendingList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataBetweenDates(ddrt);

            LinkedList<ExerciseDataManager.ExerciseItem> descendingList = new LinkedList<ExerciseDataManager.ExerciseItem>();
            foreach (ExerciseDataManager.ExerciseItem exItem in ascendingList.Reverse())
            {
                descendingList.AddLast(exItem);
            }


            rptHistory.DataSource = descendingList;
            rptHistory.DataBind();

            // NOTE: If there is no history, there will just be an empty table.
            /**
                        gvList.DataKeyNames = new string[] { "ThingID", "VersionStamp" };
                        gvList.DataSource = desendingList;
                        gvList.DataBind();

                        if (descendingList.Count == 0)
                        {
                            DivNoData.Visible = true;
                            DivData.Visible = false;
                        }
                        else
                        {
                            DivNoData.Visible = false;
                            DivData.Visible = true;
                        }
             **/
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ExerciseDataManager.ExerciseItem item = e.Item.DataItem as ExerciseDataManager.ExerciseItem;

                //
                // Hook up data
                //
                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litActivity = e.Item.FindControl("litActivity") as Literal;
                Literal litDuration = e.Item.FindControl("litDuration") as Literal;
                Literal litIntensity = e.Item.FindControl("litIntensity") as Literal;
                Literal litSteps = e.Item.FindControl("litSteps") as Literal;
                Literal litComment = e.Item.FindControl("litComment") as Literal;


                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.SLASH, GRCBase.GRCTimeFormat.NONE);
                litActivity.Text = item.Activity.Value;

                if (item.Duration.Value.HasValue)
                    litDuration.Text = GRCBase.StringHelper.GetFormattedDoubleString(item.Duration.Value.Value, 2, false);

                switch (item.Intensity.Value)
                {
                    case Microsoft.Health.ItemTypes.RelativeRating.High: litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Vigorous", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.Low: litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Light", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.Moderate: litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Moderate", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.None: litIntensity.Text = string.Empty;
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.VeryHigh: litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Vigorous", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.VeryLow: litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Light", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    default: litIntensity.Text = string.Empty;
                        break;
                }

                if (item.NumberOfSteps.Value.HasValue)
                {
                    litSteps.Text = item.NumberOfSteps.Value.ToString();
                }

                if (!string.IsNullOrEmpty(item.Note.Value))
                {
                    if (item.Note.Value.Length > 18)
                    {
                        // comment is truncated and entire comment is put in a tooltip
                        litComment.Text = "<span data-tooltip=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + item.Note.Value + "\">" +
                                            GRCBase.StringHelper.GetShortString(item.Note.Value, 18, true) + "</span>";
                    }
                    else
                    {
                        // entire comment fits in cell.
                        litComment.Text = item.Note.Value;
                    }
                }

                //
                // Hook up Actions
                //
                ImageButton actEdit = e.Item.FindControl("actEditor") as ImageButton;
                ImageButton actTrash = e.Item.FindControl("actTrash") as ImageButton;
                string      itemKey = item.ThingID.ToString();


                if (actEdit != null)
                {
                    string editUrl = "PopUp.aspx?reading=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY).ToString() + "&elid=" + itemKey;
                    actEdit.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY);
                }
                if (actTrash != null)
                {
                    // delete requires both ThingID and VersionStamp
                    itemKey += "," + item.VersionStamp.ToString();
                    string trashUrl = "PopUp.aspx?delete=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY).ToString() + "&elid=" + itemKey;
                    actTrash.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY);
                }
            }
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        public void LoadItems(DataDateReturnType DateRange)
        {
            SortAscending = false;
            SortExpression = string.Empty;

            //CurrentDateRange = DateRange;

            //_bindDataToGridView();
        }
    }
}