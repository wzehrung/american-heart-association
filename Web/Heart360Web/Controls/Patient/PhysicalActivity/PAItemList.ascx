﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PAItemList.ascx.cs" Inherits="Heart360Web.Controls.Patient.PhysicalActivity.PAItemList" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>

<asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="true" runat="server">
    <HeaderTemplate>
    	     <table class="paginated">
		        <thead>
			        <tr>
				        <th class="tt-date">DATE</th>
				        <th class="pa-activity">ACTIVITY</th>
				        <th class="pa-duration">DURATION</th>
				        <th class="pa-intensity">INTENSITY</th>
				        <th class="pa-steps">STEPS</th>
				        <th class="pa-comments">COMMENTS</th>
				        <th class="tt-icon"></th>
                        <th class="tt-icon"></th>
			        </tr>
		        </thead>
                <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
			<td><asp:Literal ID="litDate" runat="server"/></td>
			<td><asp:Literal ID="litActivity" runat="server"/></td>
			<td><asp:Literal ID="litDuration" runat="server"/></td>
			<td><asp:Literal ID="litIntensity" runat="server"/></td>
			<td><asp:Literal ID="litSteps" runat="server"/></td>
			<td><asp:Literal ID="litComment" runat="server"/></td>
			<td>
				<asp:ImageButton ID="actEditor" CssClass="add-note fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Edit PA reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
            <td>
                <asp:ImageButton ID="actTrash" CssClass="trash fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Delete PA reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
		</tr>
    </ItemTemplate>
    <FooterTemplate>
                </tbody>
	        </table>
    </FooterTemplate>
</asp:Repeater>

