﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;
using AHAAPI;
using HVManager;

namespace Heart360Web.Controls.Patient.PhysicalActivity
{
    public partial class PAPrinterFriendly : System.Web.UI.UserControl
    {
        IPeriodicDataManager m_IPeriodicDataManager = null;

        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
                GraphCtrl.IPeriodicDataManager = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GraphCtrl.ModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY;
            GraphCtrl.Visible = true;
            GraphCtrl.UseDivTrigger = false;

            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            LinkedList<HVManager.ExerciseDataManager.ExerciseItem> asendingList = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataBetweenDates(m_IPeriodicDataManager.DateRange);

            LinkedList<HVManager.ExerciseDataManager.ExerciseItem> desendingList = new LinkedList<HVManager.ExerciseDataManager.ExerciseItem>();
            foreach (HVManager.ExerciseDataManager.ExerciseItem exerciseItem in asendingList.Reverse())
            {
                desendingList.AddLast(exerciseItem);
            }

            rptHistory.DataSource = desendingList;
            rptHistory.DataBind();
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HVManager.ExerciseDataManager.ExerciseItem item = e.Item.DataItem as HVManager.ExerciseDataManager.ExerciseItem ;

                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litActivity = e.Item.FindControl("litActivity") as Literal;
                Literal litDuration = e.Item.FindControl("litDuration") as Literal;
                Literal litIntensity = e.Item.FindControl("litIntensity") as Literal;
                Literal litSteps = e.Item.FindControl("litSteps") as Literal;
                Literal litComments = e.Item.FindControl("litComments") as Literal;


                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                litActivity.Text = item.Activity.Value;
                if (item.Duration.Value.HasValue)
                    litDuration.Text = GRCBase.StringHelper.GetFormattedDoubleString(item.Duration.Value.Value, 2, false);
                switch (item.Intensity.Value)
                {
                    case Microsoft.Health.ItemTypes.RelativeRating.VeryHigh:
                    case Microsoft.Health.ItemTypes.RelativeRating.High:
                        litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Vigorous", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.Low:
                    case Microsoft.Health.ItemTypes.RelativeRating.VeryLow:
                        litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Light", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.Moderate:
                        litIntensity.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Moderate", AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.None:
                    default:
                        litIntensity.Text = string.Empty;
                        break;
                }
                if (item.NumberOfSteps.Value.HasValue)
                    litSteps.Text = item.NumberOfSteps.Value.Value.ToString();
                litComments.Text = item.Note.Value;
            }
        }
    }
}