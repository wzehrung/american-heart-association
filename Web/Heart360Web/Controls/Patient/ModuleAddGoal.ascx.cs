﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;
using HVManager;
using AHAAPI;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using Microsoft.Health.Web;
using AjaxControlToolkit;

using Heart360Web;

namespace Heart360Web.Controls.Patient
{
    public partial class ModuleAddGoal : System.Web.UI.UserControl
    {
        private Heart360Web.PatientPortal.MODULE_ID   mModuleID;
        private bool                                  mEnableAddButton;

        public ModuleAddGoal() : base()
        {
            mEnableAddButton = true;
            mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
        }

        public int ModuleIdAsInt      // need this so (html/asp) markup can specify modules
        {
            set
            {
                mModuleID = Heart360Web.PatientPortal.GetModuleIdFromInt(value);
            }
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        private bool IsEditMode
        {
            get;
            set;
        }

        

        protected void Page_Load(object sender, EventArgs e)
        {

            if (AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
            {
                btnSave.Text = GetGlobalResourceObject("Common", "Text_Save").ToString();
                btnDelete.Text = GetGlobalResourceObject("Common", "Text_Delete").ToString();

                // Get the correct module to load
                switch (ModuleID)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                        if (HVHelper.HVManagerPatientBase.BPGoalDataManager.Item != null)
                        {
                            IsEditMode = true;
                            if( !Page.IsPostBack )
                                _LoadBPData();
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddGoal_BloodPressure").ToString() :
                                                                GetGlobalResourceObject("Tracker", "Tracker_Title_EditGoal_BloodPressure").ToString();
                        divBloodPressure.Visible = true;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        if( HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item != null )
                        {
                            IsEditMode = true;
                            if (!Page.IsPostBack)
                                _LoadWData();
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddGoal_Weight").ToString() :
                                                                GetGlobalResourceObject("Tracker", "Tracker_Title_EditGoal_Weight").ToString();
                        divWeight.Visible = true;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                        if( !Page.IsPostBack )
                            _LoadExerciseIntensityDDL();
                        if( HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.Item != null )
                        {
                            IsEditMode = true;
                            if (!Page.IsPostBack)
                                _LoadPAData();
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddGoal_PhysicalActivity").ToString() :
                                                                GetGlobalResourceObject("Tracker", "Tracker_Title_EditGoal_PhysicalActivity").ToString() ;
                        divActivity.Visible = true;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                        if( HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item != null )
                        {
                            IsEditMode = true;
                            if (!Page.IsPostBack)
                                _LoadCData();
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddGoal_Cholesterol").ToString() :
                                                                GetGlobalResourceObject("Tracker", "Tracker_Title_EditGoal_Cholesterol").ToString();
                        this.hidCalculate.Value = "0";
                        divCholesterol.Visible = true;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                    default:
                        this.litTitle.Text = "INVALID MODULE ID";
                        btnSave.Visible = false;
                        break;
                }

                // Turn off the Save button
                if (mEnableAddButton == false)
                {
                    btnSave.Enabled = false;
                    btnSave.Visible = false;
                }
            }
            else
            {
                // TODO: put a text message
            }
        
        }

            // only called when editing a goal
        private void _LoadBPData()
        {
            BPGoalDataManager.BPGoalItem bpGoal = HVHelper.HVManagerPatientBase.BPGoalDataManager.Item;

            if (bpGoal.TargetSystolic != null)
                txtSystolic.Text = bpGoal.TargetSystolic.Value.ToString();

            if (bpGoal.TargetDiastolic != null)
                txtDiastolic.Text = bpGoal.TargetDiastolic.Value.ToString();
        }

            // only called when editing a goal
        private void _LoadCData()
        {
            CholesterolGoalDataManager.CholesterolGoalItem ChGoal = HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item;

            if (ChGoal.TargetTotalCholesterol != null)
                txtTotalCholesterol.Text = ChGoal.TargetTotalCholesterol.Value.ToString();

            if (ChGoal.TargetHDL != null)
                txtHDL.Text = ChGoal.TargetHDL.Value.ToString();

            if (ChGoal.TargetLDL != null)
                txtLDL.Text = ChGoal.TargetLDL.Value.ToString();

            if (ChGoal.TargetTriglyceride.Value.HasValue)
                txtTriglycerides.Text = ChGoal.TargetTriglyceride.Value.ToString();

        }

            // only called when editing a goal
        private void _LoadPAData()
        {
            ExerciseGoalDataManager.ExerciseGoalItem ExGoal = HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.Item;

            if (ExGoal.Duration != null && ExGoal.Duration.Value.HasValue)
            {
                txtDuration.Text = ExGoal.Duration.Value.Value.ToString();
            }


            string strIntensity = string.Empty;

            if (!string.IsNullOrEmpty(ExGoal.Intensity.Value))
            {
                strIntensity = ExGoal.Intensity.Value;
                string strIntensityLower = ExGoal.Intensity.Value.ToLower();
                if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.High.ToString().ToLower()) ||
                    strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.VeryHigh.ToString().ToLower()))
                {
                    strIntensity = "Vigorous";
                }
                else if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.Low.ToString().ToLower()) ||
                    strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.VeryLow.ToString().ToLower()))
                {
                    strIntensity = "Light";
                }
                else if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.Moderate.ToString().ToLower()))
                {
                    strIntensity = "Moderate";
                }
                else if (strIntensityLower.Equals(Microsoft.Health.ItemTypes.RelativeRating.None.ToString().ToLower()))
                {
                    strIntensity = string.Empty;
                }
            }

            ddlIntensity.SelectedValue = strIntensity ;

        }
        // only called when editing a goal
        private void _LoadWData()
        {
            HVManager.WeightDataManager.WeightItem myWt = MyWeightWrapper.GetLatestWeight();

            HVManager.WeightGoalDataManager.WeightGoalItem WtGoal = HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item;

            if (WtGoal != null)
            {
                txtTargetWeight.Text = GRCBase.StringHelper.GetFormattedDoubleString(WtGoal.TargetValue.Value, 2, false);
                txtStartWeight.Text = GRCBase.StringHelper.GetFormattedDoubleString(WtGoal.StartValue.Value, 2, false);
                calexStartDate.SelectedDate = WtGoal.StartDate.Value;
                calexTargetDate.SelectedDate = WtGoal.TargetDate.Value;
            }
        }

        // Call the appropriate save function based on the module ID
        protected void btnSave_Click(object sender, EventArgs e)
        {

                // Perform the save doing per tracker validation
            bool    successfulSave = false ;

            switch (ModuleID)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    Page.Validate("vgBP");
                    if (Page.IsValid)
                    {
                        _SaveBloodPressure();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    Page.Validate("vgW");
                    if (Page.IsValid)
                    {
                        _SaveWeight();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    Page.Validate("vgPA");
                    if (Page.IsValid)
                    {
                        _SavePhysicalActivity();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    Page.Validate("vgCH");
                    if (Page.IsValid)
                    {
                        _SaveCholesterol();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                default:
                    break;
            }

            // We need to click a hidden button to close the popup
            if (successfulSave)
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            switch (ModuleID)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    HVHelper.HVManagerPatientBase.BPGoalDataManager.DeleteItem();
                    break;
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.DeleteItem();
                    break;
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    HVHelper.HVManagerPatientBase.WeightGoalDataManager.DeleteItem();
                    break;
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.DeleteItem();
                    break;
            }

            ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
        }

        public void _SaveBloodPressure()
        {

            BPGoalDataManager.BPGoalItem bpGoal = HVHelper.HVManagerPatientBase.BPGoalDataManager.Item;

            if (bpGoal != null)
            {
                if (AHAAPI.H360Utility.IsUseNewValue(bpGoal.TargetSystolic.Value, Convert.ToInt32(txtSystolic.Text)))
                {
                    bpGoal.TargetSystolic.Value = Convert.ToInt32(txtSystolic.Text);
                }

                if (AHAAPI.H360Utility.IsUseNewValue(bpGoal.TargetDiastolic.Value, Convert.ToInt32(txtDiastolic.Text)))
                {
                    bpGoal.TargetDiastolic.Value = Convert.ToInt32(txtDiastolic.Text);
                }
                HVHelper.HVManagerPatientBase.BPGoalDataManager.CreateOrUpdateItem(bpGoal);
            }
            else
            {
                bpGoal = new BPGoalDataManager.BPGoalItem();

                bpGoal.TargetDiastolic.Value = Convert.ToInt32(txtDiastolic.Text);
                bpGoal.TargetSystolic.Value = Convert.ToInt32(txtSystolic.Text);

                bpGoal = HVHelper.HVManagerPatientBase.BPGoalDataManager.CreateOrUpdateItem(bpGoal);
            }

            //DB Update
            AHAHelpContent.BloodPressureGoal.CreateOrChangeBloodPressureGoal(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                    bpGoal.ThingKey.Id, bpGoal.ThingKey.VersionStamp,
                    bpGoal.TargetSystolic.Value, bpGoal.TargetDiastolic.Value);

        }


        // Assuming that all weight values have been validated
        private void _SaveWeight()
        {
            HealthRecordItemKey                             keyWeightGoal = null;
            HVManager.WeightGoalDataManager.WeightGoalItem  WtGoal = HVHelper.HVManagerPatientBase.WeightGoalDataManager.Item;
            DateTime startDate = DateTime.Parse(txtStartDate.Text.Trim());
            DateTime targetDate = DateTime.Parse(txtTargetDate.Text.Trim());

            if (WtGoal != null)
            {
                if (AHAAPI.H360Utility.IsUseNewValue(WtGoal.StartValue.Value, Convert.ToDouble(txtStartWeight.Text)))
                    WtGoal.StartValue.Value = Convert.ToDouble(txtStartWeight.Text);
                if (AHAAPI.H360Utility.IsUseNewValue(WtGoal.TargetValue.Value, Convert.ToDouble(txtTargetWeight.Text)))
                    WtGoal.TargetValue.Value = Convert.ToDouble(txtTargetWeight.Text);

                WtGoal.StartDate.Value = startDate;
                WtGoal.TargetDate.Value = targetDate;
                
                HVManager.WeightGoalDataManager.WeightGoalItem objItemUpdated = HVHelper.HVManagerPatientBase.WeightGoalDataManager.CreateOrUpdateItem(WtGoal);
                keyWeightGoal = objItemUpdated.ThingKey;
            }
            else
            {
                HVManager.WeightGoalDataManager.WeightGoalItem weighttGoal = new HVManager.WeightGoalDataManager.WeightGoalItem();
                weighttGoal.StartValue.Value = Convert.ToDouble(txtStartWeight.Text);
                weighttGoal.TargetValue.Value = Convert.ToDouble(txtTargetWeight.Text);
                weighttGoal.StartDate.Value = startDate ;
                weighttGoal.TargetDate.Value = targetDate ;
                HVManager.WeightGoalDataManager.WeightGoalItem objItemNew = HVHelper.HVManagerPatientBase.WeightGoalDataManager.CreateOrUpdateItem(weighttGoal);
                keyWeightGoal = objItemNew.ThingKey;
            }

            //DB Update
            AHAHelpContent.WeightGoal.CreateOrChangeWeightGoal(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                keyWeightGoal.Id, keyWeightGoal.VersionStamp,
                Convert.ToDouble(txtStartWeight.Text), startDate,
                Convert.ToDouble(txtTargetWeight.Text), targetDate);

        }


        private void _LoadExerciseIntensityDDL()
        {
            ddlIntensity.DataSource = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
            ddlIntensity.DataTextField = "ListItemName";
            ddlIntensity.DataValueField = "ListItemCode";
            ddlIntensity.DataBind();

            ddlIntensity.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlIntensity.SelectedItem.Value = (string.Empty);
        }

        private void _SavePhysicalActivity()
        {
            ExerciseGoalDataManager.ExerciseGoalItem ExGoal = HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.Item;
            RelativeRating intensity = RelativeRating.Low;
            switch (ddlIntensity.SelectedValue)
            {
                case "Moderate": intensity = RelativeRating.Moderate;
                    break;
                case "Vigorous": intensity = RelativeRating.VeryHigh;
                    break;
            }
            if (ExGoal != null)
            {
                if (!string.IsNullOrEmpty(ExGoal.Intensity.Value) && (ExGoal.Intensity.Value.ToLower() != intensity.ToString().ToLower()))
                {
                    ExGoal.Intensity.Value = intensity.ToString();
                }

                if (AHAAPI.H360Utility.IsUseNewValue(GRCBase.StringHelper.GetFormattedDoubleString(ExGoal.Duration.Value.Value, 2, false), txtDuration.Text.Trim()))
                {
                    ExGoal.Duration.Value = Convert.ToDouble(txtDuration.Text.Trim());
                }
            }
            else
            {
                ExGoal = new ExerciseGoalDataManager.ExerciseGoalItem();
                ExGoal.Duration.Value = Convert.ToDouble(txtDuration.Text.Trim());
                ExGoal.Intensity.Value = intensity.ToString();
            }
            ExGoal = HVHelper.HVManagerPatientBase.ExerciseGoalDataManager.CreateOrUpdateItem(ExGoal);

            //DB Update
            AHAHelpContent.ExerciseGoal.CreateOrChangeExerciseGoal(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                        ExGoal.ThingKey.Id, ExGoal.ThingKey.VersionStamp,
                        ExGoal.Intensity.Value, ExGoal.Duration.Value.HasValue ? ExGoal.Duration.Value.Value : 0);
        }

        protected void ValidatePADuration(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtDuration.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Duration").ToString();
            }
            else
            {
                try
                {
                    double dbl = Convert.ToDouble(txtDuration.Text.Trim());
                    if (!(dbl > 0.0))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_DurationGtZero").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Duration").ToString();
                }
            }
        }


        protected void OnRecalculateTC(object sender, EventArgs e)
        {

            // we need to validate the inputs: HDL, LDL, and Triglycerides.
            // also make sure that total cholesterol is not validated.

            this.hidCalculate.Value = "1";

            Page.Validate("vgCH");
            if (!Page.IsValid)
                return;

            // If we are here, there is something input into HDL, LDL and Trigs.
            int dblLDL = 0;
            int dblHDL = 0;
            int dblTriglycerides = 0;

            try
            {
                if (!String.IsNullOrEmpty(txtLDL.Text.Trim()))
                {
                    dblLDL = Convert.ToInt32(txtLDL.Text.Trim());
                }

                if (!String.IsNullOrEmpty(txtHDL.Text.Trim()))
                {
                    dblHDL = Convert.ToInt32(txtHDL.Text.Trim());
                }

                if (!String.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
                {
                    dblTriglycerides = Convert.ToInt32(txtTriglycerides.Text.Trim());
                }

                int TC = dblHDL + dblLDL + (dblTriglycerides / 5);

                txtTotalCholesterol.Text = TC.ToString();
            }
            catch
            {
                // this shouldn't happen as all inputs would have been validated
            }

            // Now make sure the computed value is valid
            Page.Validate("vgCHPostCalculate");

        }

        private void _SaveCholesterol()
        {
            CholesterolGoalDataManager.CholesterolGoalItem ChGoal = HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.Item;

            if (ChGoal != null)
            {
                // Note: only TC is required! HDL, LDL and Triglycerides may be empty.
                if (AHAAPI.H360Utility.IsUseNewValue(ChGoal.TargetTotalCholesterol.Value, Convert.ToInt32(txtTotalCholesterol.Text)))
                {
                    ChGoal.TargetTotalCholesterol.Value = Convert.ToInt32(txtTotalCholesterol.Text);
                }

                if (!string.IsNullOrEmpty(txtHDL.Text.Trim()))
                {
                    ChGoal.TargetHDL.Value = Convert.ToInt32(txtHDL.Text.Trim());
                }
                else if (ChGoal.TargetHDL.Value.HasValue )
                {
                    ChGoal.TargetHDL.Value = null;
                }

                if (!string.IsNullOrEmpty(txtLDL.Text.Trim()))
                {
                    ChGoal.TargetLDL.Value = Convert.ToInt32(txtLDL.Text.Trim());
                }
                else if (ChGoal.TargetLDL.Value.HasValue)
                {
                    ChGoal.TargetLDL.Value = null;
                }

                if (!string.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
                {
                    ChGoal.TargetTriglyceride.Value = Convert.ToInt32(txtTriglycerides.Text.Trim());
                }
                else if (ChGoal.TargetTriglyceride.Value.HasValue)
                {
                    ChGoal.TargetTriglyceride.Value = null;
                }

                HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.CreateOrUpdateItem(ChGoal);
            }
            else
            {
                ChGoal = new CholesterolGoalDataManager.CholesterolGoalItem();

                ChGoal.TargetTotalCholesterol.Value = Convert.ToInt32(txtTotalCholesterol.Text);
                if (!string.IsNullOrEmpty(txtHDL.Text.Trim()))
                    ChGoal.TargetHDL.Value = Convert.ToInt32(txtHDL.Text.Trim());
                else
                    ChGoal.TargetHDL.Value = null;

                if (!string.IsNullOrEmpty(txtLDL.Text.Trim()))
                    ChGoal.TargetLDL.Value = Convert.ToInt32(txtLDL.Text.Trim());
                else
                    ChGoal.TargetLDL.Value = null;

                if (!string.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
                    ChGoal.TargetTriglyceride.Value = Convert.ToInt32(txtTriglycerides.Text.Trim());
                else
                    ChGoal.TargetTriglyceride.Value = null;

                HVHelper.HVManagerPatientBase.CholesterolGoalDataManager.CreateOrUpdateItem(ChGoal);
            }
        }


        protected void ValidateBPSystolic(object sender, ServerValidateEventArgs v)
        {
            int iSystolic = 0;
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;


            if (String.IsNullOrEmpty(txtSystolic.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Systolic").ToString();
            }
            else
            {
                try
                {
                    iSystolic = Convert.ToInt32(txtSystolic.Text.Trim());

                    if (iSystolic <= 0)
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Systolic").ToString();
                    }
                    else
                    {
                        // Make sure Systolic is greater than distolic
                        if (!String.IsNullOrEmpty(txtDiastolic.Text.Trim()))
                        {
                            try
                            {
                                int iDiastolic = Convert.ToInt32(txtDiastolic.Text.Trim());
                                if (iSystolic < iDiastolic)
                                {
                                    v.IsValid = false;
                                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_SystolicGtDiastolic").ToString();
                                }
                            }
                            catch { }   // this will be handled in diastolic validation
                        }

                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Systolic").ToString();
                }
            }
        }

        protected void ValidateBPDiastolic(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtDiastolic.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Diastolic").ToString();
            }
            else
            {
                try
                {
                    int iDiastolic = Convert.ToInt32(txtDiastolic.Text.Trim());
                    if (iDiastolic <= 0)
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Diastolic").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Diastolic").ToString();
                }
            }
        }

        protected void ValidateWStartDate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Check to see if the value is valid
            string      sdate = txtStartDate.Text.Trim() ;

            if (String.IsNullOrEmpty(sdate))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Date").ToString();
            }
            else
            {
                DateTimeValidation.ValidateDateBeforeNow( sdate, valid, v ) ;
            }
        }

        protected void ValidateWTargetDate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string sdate = txtTargetDate.Text.Trim();

            // Check to see if the value is valid
            if (String.IsNullOrEmpty(sdate))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Date").ToString();
            }
            else
            {
                string      startString = txtStartDate.Text.Trim();
                DateTime    startDate;

                if (!String.IsNullOrEmpty(startString) && DateTime.TryParse(startString, out startDate))
                {
                    DateTimeValidation.ValidateDateAfterDate(sdate, startDate, valid, v);
                }
                else
                {
                    DateTimeValidation.ValidateDateAfterNow(sdate, valid, v);
                }
            }
        }

        protected void ValidateWStartWeight(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtStartWeight.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight_Value").ToString();
                return;
            }

            try
            {
                double dbl = Convert.ToDouble(txtStartWeight.Text.Trim());
                /// Added to check that the weight entry is always greater than or equal to 1.
                if (dbl < 1 || dbl > 999)
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight").ToString();
                    return;
                }
            }
            catch
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight").ToString();
                return;
            }
        }

        protected void ValidateWTargetWeight(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtTargetWeight.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight_Value").ToString();
                return;
            }

            try
            {
                double dbl = Convert.ToDouble(txtTargetWeight.Text.Trim());
                /// Added to check that the weight entry is always greater than or equal to 1.
                if (dbl < 1 || dbl > 999)
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight").ToString();
                    return;
                }
            }
            catch
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight").ToString();
                return;
            }
        }

        protected void ValidateCHHdl(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Note: this value is not required, but if the user specified a value, it needs to be validated
            if (!String.IsNullOrEmpty(txtHDL.Text.Trim()))
            {
                try
                {
                    int ival = Convert.ToInt32(txtHDL.Text.Trim());
                    if ((ival < 1) || (ival > 100000))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_HDL").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_HDL").ToString();
                }
            }
        }
        protected void ValidateCHLdl(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Note: this value is not required, but if the user specified a value, it needs to be validated
            if (!String.IsNullOrEmpty(txtLDL.Text.Trim()))
            {
                try
                {
                    int ival = Convert.ToInt32(txtLDL.Text.Trim());
                    if ((ival < 1) || (ival > 100000))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_LDL").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_LDL").ToString();
                }
            }
        }
        protected void ValidateCHTrig(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Note: this value is not required, but if the user specified a value, it needs to be validated
            if (!String.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
            {
                try
                {
                    int ival = Convert.ToInt32(txtTriglycerides.Text.Trim());
                    if ((ival < 1) || (ival > 100000))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_Triglyceride").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_Triglyceride").ToString();
                }
            }
        }

        protected void ValidateCHTotalC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (hidCalculate.Value.Equals("1"))  // they clicked on calculate button, validating before calculation
            {
                // we need to check if all three HDL, LDL and Triglycerides values are defined.
                if (string.IsNullOrEmpty(txtHDL.Text.Trim()) || string.IsNullOrEmpty(txtLDL.Text.Trim()) || string.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Total_Reqs").ToString();
                }
            }
            else
            {
                // user may have input a TC value computed from a device
                if (string.IsNullOrEmpty(this.txtTotalCholesterol.Text.Trim()))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_Total").ToString();
                }
                else
                {
                    try
                    {
                        int ival = Convert.ToInt32(txtTotalCholesterol.Text.Trim());
                        if ((ival < 1) || (ival > 100000))
                        {
                            v.IsValid = false;
                            valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_Total").ToString();
                        }
                    }
                    catch
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_Total").ToString();
                    }
                }

            }
        }

        protected void ValidateCHPostTotalC(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = true;

            if (string.IsNullOrEmpty(this.txtTotalCholesterol.Text.Trim()))     // error was thrown
            {
                v.IsValid = false;
            }
            else
            {
                int ival = Convert.ToInt32(txtTotalCholesterol.Text.Trim());
                if ((ival < 1) || (ival > 100000))
                    v.IsValid = false;
            }

            if (!v.IsValid)
            {
                CustomValidator valid = (CustomValidator)sender;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Total_Notice_Recheck").ToString();
            }
        }
    }
}