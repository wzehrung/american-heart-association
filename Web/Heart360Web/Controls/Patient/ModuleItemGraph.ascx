﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleItemGraph.ascx.cs" Inherits="Heart360Web.Controls.Patient.ModuleItemGraph" %>
<%@ Register Src="~/Controls/Common/HealthGraph.ascx" TagName="HealthGraph" TagPrefix="uc1" %>

<div class="full my-graph">
    <div id="divOptionalTrigger" class="trigger" runat="server">
	    <h4>Graph<span class="gray-arrow"></span></h4>
	</div>
	<div class="next-content">
        <!-- Cholesterol requires two graphs -->
        <uc1:HealthGraph ID="HealthGraph1" runat="server" />
        <uc1:HealthGraph ID="HealthGraph2" runat="server" />
    </div>
</div>
