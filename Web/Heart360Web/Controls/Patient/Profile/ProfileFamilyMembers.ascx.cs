﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using Microsoft.Health;
using Microsoft.Health.Web;

namespace Heart360Web.Controls.Patient.Profile
{
    public partial class ProfileFamilyMembers : System.Web.UI.UserControl
    {
        public class ActiveRecordChangedEventArgs
        {
            public readonly Guid ActiveRecordId;
            public ActiveRecordChangedEventArgs(Guid activeRecordId)
            {
                ActiveRecordId = activeRecordId;
            }
        }

        public PersonInfo PersonInfo;

        public delegate void ActiveRecordChangedHandler(object sender, ActiveRecordChangedEventArgs e);
        public event ActiveRecordChangedHandler ActiveRecordChanged;

        public string strHeart360 = string.Empty;

        /// <summary>
        /// Generates a URL that calls back to this page, and specifies the selected record 
        /// as a web-request parameter.
        /// 
        /// This function gets called by data-binding code in the *.ascx file when
        /// the list of possible records the user can switch to is enumerated
        /// </summary>
        /// <param name="recordId"></param> 
        /// <returns></returns>
        public string UrlForRecordSelected(HealthRecordInfo healthRecordInfo)
        {
            string filePath = this.Request.FilePath;
            string url = filePath +
                "?" +
                WebAppNavigation.QueryParam_SelectRecordId + "=" + healthRecordInfo.Id.ToString();

            if (!string.IsNullOrEmpty(Request["invitationid"]))
            {
                url += "&invitationid=";
                url += Request["invitationid"];
            }

            return url;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            strHeart360 = H360Utility.GetTrademarkedHeart360();
            Guid? guidSelected = WebAppNavigation.ParseSelectedRecordId(this.Page);
            if (guidSelected.HasValue)
            {
                if ((Page as HealthServicePage).PersonInfo.AuthorizedRecords.ContainsKey(guidSelected.Value)
                    && ActiveRecordChanged != null)
                {
                    ActiveRecordChanged(this, new ActiveRecordChangedEventArgs(guidSelected.Value));
                }
            }
            LoadAuthorizedRecords();

            if (string.IsNullOrEmpty(Request["invitationid"]))
            {
                //btnCancel.Visible = false;
            }
        }

        private void SetCurrentRecord(Guid healthInfoGuid)
        {
            Dictionary<Guid, HealthRecordInfo> dvp = HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords;
            if (dvp.ContainsKey(healthInfoGuid))
            {
                (Page as HealthServicePage).SetSelectedRecord(dvp[healthInfoGuid]);
            }
        }

        /// <summary>
        /// Load the records that the logged in user has authorization
        /// </summary>
        private void LoadAuthorizedRecords()
        {
            listFamily.DataSource = HealthServicePage.CurrentPage.PersonInfo.AuthorizedRecords;
            listFamily.DataBind();
        }

        /// <summary>
        /// Event that occurs when a data record is bound to a row in the gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void gvList_RowDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem)
                return;

            ListViewDataItem dataItem = (ListViewDataItem)e.Item;

            KeyValuePair<Guid, HealthRecordInfo> kvp = (KeyValuePair<Guid, HealthRecordInfo>)dataItem.DataItem;

            HealthRecordInfo currentRecord = kvp.Value;

            Label lblName = e.Item.FindControl("lblName") as Label;
            HtmlAnchor linkSelect = e.Item.FindControl("linkSelect") as HtmlAnchor;
            HtmlControl iSelect = e.Item.FindControl("iCurrent") as HtmlControl;
             

            bool bIsCurrent = ((Page as HealthServicePage).PersonInfo.SelectedRecord.Id == currentRecord.Id);

            if (linkSelect.Visible = !bIsCurrent)
            {
                linkSelect.HRef = UrlForRecordSelected(currentRecord);
                linkSelect.Attributes.CssStyle.Value = "blue-circle-check";
            }
            //else
            //{
            //    linkSelect.Attributes.CssStyle.Value = "green-circle-check";
            //}
            iSelect.Visible = bIsCurrent;

            lblName.Text = currentRecord.DisplayName;
        }
        


        protected void btnAuthorize_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder(128);
            sb.Append("appid=");
            sb.Append(WebApplicationConfiguration.AppId.ToString());
            sb.Append("&actionqs=");
            sb.Append(HttpUtility.UrlEncode(Request.Url.PathAndQuery));
            (Page as HealthServicePage).RedirectToShellUrl(
                "APPAUTH",
                sb.ToString());
        }
    }
}