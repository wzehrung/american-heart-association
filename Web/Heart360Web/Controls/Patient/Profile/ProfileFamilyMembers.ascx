﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileFamilyMembers.ascx.cs" Inherits="Heart360Web.Controls.Patient.Profile.ProfileFamilyMembers" %>

<!-- ## BEGIN Edit Family Members -->           
<section class="family-members info-stats full">
	<div class="accordion">
		<div class="toggle-btn dt-active">
			<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Title_SelectAddFamilyMembers")%><span class="gray-arrow"></span></h4>
		</div>
		<div style="display: none;" class="content">

			<div class="row">
				<div class="large-6 small-12 columns">
					<p><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Text_AddDeleteFamilyMembers")%></p>
				</div>
				<div class="large-6 small-12 columns">

                    <asp:UpdatePanel ID="upFamilyMembers" UpdateMode="Always" runat="server">
                        <ContentTemplate>

                            <asp:ListView
                                ID="listFamily"
                                EnableViewState="false" 
                                OnItemDataBound="gvList_RowDataBound"
                                runat="server">
                                <LayoutTemplate>
                                    <ul class="full family-members">                       
								        <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                                    </ul>
                                </LayoutTemplate>
                        
                                <EmptyDataTemplate>
                                    <asp:Literal runat="server" Text="<%$Resources:Common,Text_NoRecords %>"></asp:Literal>
                                </EmptyDataTemplate>

                                <ItemTemplate>
                                    <li class="full"><span class="full member"><asp:Label ID="lblName" runat="server"></asp:Label><a id="linkSelect" class="fr" runat="server"><u><%= GetGlobalResourceObject("Tracker","Tracker_Text_Select") %></u></a></span><span id="iCurrent" class="blue-circle-check fr" visible="false" runat="server"></span></li>
                                </ItemTemplate>
                    
                            </asp:ListView>

                            <asp:UpdateProgress ID="uprFamilyMembers" AssociatedUpdatePanelID="upFamilyMembers" runat="server">
                                <ProgressTemplate>
                                    ><%=GetGlobalResourceObject("Tracker", "Message_Profile_FamilyMembers")%>
                                </ProgressTemplate>
                            </asp:UpdateProgress>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:LinkButton ID="btnAddMember" CssClass="gray-button full" OnClick="btnAuthorize_Click" runat="server"><%= GetGlobalResourceObject("Tracker", "Tracker_Profile_AddFamilyMember")%><span class="plus"><%= GetGlobalResourceObject("Common", "Symbol_Plus")%></span></asp:LinkButton>

				</div>
			</div>

		</div>
	</div>
</section>
<!-- ## BEGIN Edit Family Members -->  