﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BGItemList.ascx.cs" Inherits="Heart360Web.Controls.Patient.BloodGlucose.BGItemList" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>

<asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" OnItemCommand="rptHistory_ItemCommandTest" EnableViewState="true" runat="server">
    <HeaderTemplate>
    	     <table  class="paginated bg-table">
		        <thead>
			        <tr>
				        <th class="tt-date">DATE</th>
				        <th class="tt-time">TIME</th>
				        <th class="bg-bg">BG <em>(mg/dL)</em></th>
				        <th class="bg-reading">READING TYPE</th>
				        <th class="bg-action">ACTION I TOOK</th>
				        <th class="tt-comments">COMMENTS</th>
				        <th class="tt-icon"></th>
                        <th class="tt-icon"></th>
			        </tr>
		        </thead>
                <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
			<td><asp:Literal ID="litDate" runat="server"/></td>
			<td><asp:Literal ID="litTime" runat="server"/></td>
			<asp:Literal ID="litBG" runat="server"/> <!-- td added programmatically with risk color -->
			<td><asp:Literal ID="litSource" runat="server"/></td>
			<td><asp:Literal ID="litAction" runat="server"/></td>
			<td><asp:Literal ID="litComment" runat="server"/></td>
			<td>
				<asp:ImageButton ID="actEditor" CssClass="add-note fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Edit BG reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
            <td>
                <asp:ImageButton ID="actTrash" CssClass="trash fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Delete BG reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
		</tr>
    </ItemTemplate>
    <FooterTemplate>
                </tbody>
	        </table>
    </FooterTemplate>
</asp:Repeater>
