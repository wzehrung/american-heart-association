﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;
using HVManager;
using AHACommon;
using AHAAPI;

namespace Heart360Web.Controls.Patient.BloodGlucose
{
    public partial class BGItemList : System.Web.UI.UserControl, IDataList
    {
        #region Private Members

        const string _vgAdd = "Add";
        const string _vgEdit = "Edit";

        // Givens: all interaction with the list (Edit, Delete) is done through a popup with no postback.
        //         This control does not change/manage current date range
        //
        // How it works: we always render the control on Page_Load!
        //               ModuleDetails manages ViewState["DataRange"]
/**
        AHACommon.DataDateReturnType CurrentDateRange
        {
            get
            {
                if (ViewState["DataRange"] != null)
                    return (AHACommon.DataDateReturnType)ViewState["DataRange"];
                else
                    return AHACommon.DataDateReturnType.Last3Months;
            }
            set
            {
                if (ViewState["DataRange"] == null)
                {
                    ViewState.Add("DataRange", value);
                }
                else
                {
                    ViewState["DataRange"] = value;
                }
            }
        }
**/
        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "date";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return false;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }

        #endregion

        #region Public Properties

        IPeriodicDataManager m_IPeriodicDataManager = null;
        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            //linkRefreshSiteData.OnClientClick = string.Format("return RefreshSiteDataPatientForUrl('{0}');", AHAAPI.WebAppNavigation.H360.PAGE_TRACKER_BP);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            _bindDataToGridView();
        }


        private void _bindDataToGridView()
        {
            DataDateReturnType ddrt = m_IPeriodicDataManager.DateRange;
            LinkedList<BloodGlucoseDataManager.BGItem> ascendingList = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetDataBetweenDates(ddrt);

            LinkedList<BloodGlucoseDataManager.BGItem> descendingList = new LinkedList<BloodGlucoseDataManager.BGItem>();
            foreach (BloodGlucoseDataManager.BGItem bgItem in ascendingList.Reverse())
            {
                descendingList.AddLast(bgItem);
            }


            rptHistory.DataSource = descendingList;
            rptHistory.DataBind();

            // NOTE: If there is no history, there will just be an empty table.
            /**
                        gvList.DataKeyNames = new string[] { "ThingID", "VersionStamp" };
                        gvList.DataSource = desendingList;
                        gvList.DataBind();

                        if (descendingList.Count == 0)
                        {
                            DivNoData.Visible = true;
                            DivData.Visible = false;
                        }
                        else
                        {
                            DivNoData.Visible = false;
                            DivData.Visible = true;
                        }
             **/
        }

        protected void rptHistory_ItemCommandTest(object sender, RepeaterCommandEventArgs e)
        {

            int iii = 0;
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BloodGlucoseDataManager.BGItem item = e.Item.DataItem as BloodGlucoseDataManager.BGItem;

                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litTime = e.Item.FindControl("litTime") as Literal;
                Literal litBG = e.Item.FindControl("litBG") as Literal;
                Literal litAction = e.Item.FindControl("litAction") as Literal;
                Literal litReadingSource = e.Item.FindControl("litSource") as Literal;
                Literal litComment = e.Item.FindControl("litComment") as Literal;


                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.SLASH, GRCBase.GRCTimeFormat.NONE);
                litTime.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, GRCBase.GRCDateFormat.NONE, GRCBase.GRCDateSeperator.NONE, GRCBase.GRCTimeFormat.HHMMAMPM);

                if ((AHAAppSettings.ColorCoding_EffectiveDays == null) || item.EffectiveDate.CompareTo(DateTime.Now.AddDays(AHAAppSettings.ColorCoding_EffectiveDays.Value)) == 1)
                {
                    RiskLevelHelper.RiskLevelBloodGlucose bgRiskLevelIndicator = new RiskLevelHelper.RiskLevelBloodGlucose(item.Value.Value);
                    litBG.Text = "<td class=\"" + bgRiskLevelIndicator.ColorReading1 + "\">" + GRCBase.StringHelper.GetFormattedDoubleString(item.Value.Value, 2, false) + "</td>";
                }
                else
                {
                    litBG.Text = "<td>" + GRCBase.StringHelper.GetFormattedDoubleString(item.Value.Value, 2, false) + "</td>";
                }

                if (!string.IsNullOrEmpty(item.ActionTaken.Value))
                    litAction.Text = item.ActionTaken.Value;    // was .ToString() ;
                if (!string.IsNullOrEmpty(item.ReadingType.Value))
                {
                    switch (item.ReadingType.Value.ToLower())
                    {
                        case "postprandial":
                            litReadingSource.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("postprandial", AHADefs.VocabDefs.ReadingType, H360Utility.GetCurrentLanguageID());
                            break;
                        case "fasting":
                            litReadingSource.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("fasting", AHADefs.VocabDefs.ReadingType, H360Utility.GetCurrentLanguageID());
                            break;
                        default:
                            litReadingSource.Text = item.ReadingType.Value;
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(item.Note.Value))
                {
                    if (item.Note.Value.Length > 18)
                    {
                        // comment is truncated and entire comment is put in a tooltip
                        litComment.Text = "<span data-tooltip=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + item.Note.Value + "\">" +
                                            GRCBase.StringHelper.GetShortString(item.Note.Value, 18, true) + "</span>";
                    }
                    else
                    {
                        // entire comment fits in cell.
                        litComment.Text = item.Note.Value;
                    }
                }

                //
                // Hook up Actions
                //
                ImageButton actEdit = e.Item.FindControl("actEditor") as ImageButton;
                ImageButton actTrash = e.Item.FindControl("actTrash") as ImageButton;
                string      itemKey = item.ThingID.ToString();

                if (actEdit != null)
                {
                    string editUrl = "PopUp.aspx?reading=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE).ToString() + "&elid=" + itemKey;
                    actEdit.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE);
                }
                if (actTrash != null)
                {
                    // delete requires both ThingID and VersionStamp
                    itemKey += "," + item.VersionStamp.ToString();
                    string trashUrl = "PopUp.aspx?delete=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE).ToString() + "&elid=" + itemKey;
                    actTrash.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE);
                }
            }
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        public void LoadItems(DataDateReturnType DateRange)
        {
            SortAscending = false;
            SortExpression = string.Empty;

            //CurrentDateRange = DateRange;

            //_bindDataToGridView();
        }
    }
}