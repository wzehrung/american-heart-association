﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="A1cItemList.ascx.cs" Inherits="Heart360Web.Controls.Patient.BloodGlucose.A1cItemList" %>

<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>

<asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" OnItemCommand="rptHistory_ItemCommandTest" EnableViewState="true" runat="server">
    <HeaderTemplate>
    	     <table  class="paginated bg-table"  style="margin-top: 4px;">
		        <thead>
			        <tr>
				        <th class="tt-date"><%=GetGlobalResourceObject("Tracker", "Table_Header_Date") %></th>
				        <th class="tt-time"><%=GetGlobalResourceObject("Tracker", "Table_Header_Time") %></th>
				        <th class="bg-bg"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_A1c") %> <em></em></th>
				        <th class="bg-reading"><%=GetGlobalResourceObject("Tracker", "Table_Header_ReadingSource") %></th>
				        <th class="tt-comments"><%=GetGlobalResourceObject("Tracker", "Table_Header_Comments") %></th>
				        <th class="tt-icon"></th>
                        <th class="tt-icon"></th>
			        </tr>
		        </thead>
                <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
			<td><asp:Literal ID="litDate" runat="server"/></td>
			<td><asp:Literal ID="litTime" runat="server"/></td>
			<asp:Literal ID="litA1c" runat="server"/> <!-- td added programmatically with risk color ## todo later -->
			<td><asp:Literal ID="litSource" runat="server"/></td>
			<td><asp:Literal ID="litComment" runat="server"/></td>
			<td>
				<asp:ImageButton ID="actEditor" CssClass="add-note fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Edit BG reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
            <td>
                <asp:ImageButton ID="actTrash" CssClass="trash fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Delete BG reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
		</tr>
    </ItemTemplate>
    <FooterTemplate>
                </tbody>
	        </table>
    </FooterTemplate>
</asp:Repeater>