﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using HVManager;
using AHACommon;
using AHAAPI;

namespace Heart360Web.Controls.Patient.BloodGlucose
{
    public partial class BGPrinterFriendly : System.Web.UI.UserControl
    {
        IPeriodicDataManager m_IPeriodicDataManager = null;

        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
                GraphCtrl.IPeriodicDataManager = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GraphCtrl.ModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE;
            GraphCtrl.Visible = true;
            GraphCtrl.UseDivTrigger = false;

            string strAvBG = BloodGlucoseWrapper.GetAverageBloodGlucoseForPeriod(false, m_IPeriodicDataManager.DateRange);
            if (strAvBG.Length == 0)
            {
                litAverageBloodGlucose.Visible = false;
            }
            else
            {
                litAverageBloodGlucose.Text = strAvBG;
            }

            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            LinkedList<BloodGlucoseDataManager.BGItem> asendingList = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetDataBetweenDates(m_IPeriodicDataManager.DateRange);

            LinkedList<BloodGlucoseDataManager.BGItem> desendingList = new LinkedList<BloodGlucoseDataManager.BGItem>();
            foreach (BloodGlucoseDataManager.BGItem glucoseItem in asendingList.Reverse())
            {
                desendingList.AddLast(glucoseItem);
            }

            rptHistory.DataSource = desendingList;
            rptHistory.DataBind();
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BloodGlucoseDataManager.BGItem item = e.Item.DataItem as BloodGlucoseDataManager.BGItem;

                Literal litDateTime = e.Item.FindControl("litDateTime") as Literal;
                Literal litGlucose = e.Item.FindControl("litGlucose") as Literal;
                Literal litReadingType = e.Item.FindControl("litReadingType") as Literal;
                Literal litActionTaken = e.Item.FindControl("litActionTaken") as Literal;


                litDateTime.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.HHMMAMPM);
                litGlucose.Text = GRCBase.StringHelper.GetFormattedDoubleString(item.Value.Value, 2, false);
                litActionTaken.Text = item.ActionTaken.Value;
                if (!string.IsNullOrEmpty(item.ReadingType.Value))
                {
                    switch (item.ReadingType.Value.ToLower())
                    {
                        case "postprandial":
                            litReadingType.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("postprandial", AHADefs.VocabDefs.ReadingType, H360Utility.GetCurrentLanguageID());
                            break;
                        case "fasting":
                            litReadingType.Text = AHAHelpContent.ListItem.FindListItemNameByListItemCode("fasting", AHADefs.VocabDefs.ReadingType, H360Utility.GetCurrentLanguageID());
                            break;
                        default:
                            litReadingType.Text = item.ReadingType.Value;
                            break;
                    }
                }
            }
        }
    }
}