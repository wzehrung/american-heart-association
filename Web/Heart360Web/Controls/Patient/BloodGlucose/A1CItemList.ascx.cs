﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;
using HVManager;
using AHACommon;
using AHAAPI;

namespace Heart360Web.Controls.Patient.BloodGlucose
{
    public partial class A1cItemList : System.Web.UI.UserControl, IDataList
    {
        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "date";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return false;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }

        IPeriodicDataManager m_IPeriodicDataManager = null;
        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            _bindDataToGridView();
        }


        private void _bindDataToGridView()
        {
            DataDateReturnType ddrt = m_IPeriodicDataManager.DateRange;
            LinkedList<HbA1cDataManager.HbA1cItem> ascendingList = HVHelper.HVManagerPatientBase.HbA1cDataManager.GetDataBetweenDates(ddrt);

            LinkedList<HbA1cDataManager.HbA1cItem> descendingList = new LinkedList<HbA1cDataManager.HbA1cItem>();
            foreach (HbA1cDataManager.HbA1cItem acItem in ascendingList.Reverse())
            {
                descendingList.AddLast(acItem);
            }

            rptHistory.DataSource = descendingList;
            rptHistory.DataBind();
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HbA1cDataManager.HbA1cItem item = e.Item.DataItem as HbA1cDataManager.HbA1cItem;

                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litTime = e.Item.FindControl("litTime") as Literal;
                Literal litA1c = e.Item.FindControl("litA1c") as Literal;
                Literal litReadingSource = e.Item.FindControl("litSource") as Literal;
                Literal litComment = e.Item.FindControl("litComment") as Literal;

                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.SLASH, GRCBase.GRCTimeFormat.NONE);
                litTime.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, GRCBase.GRCDateFormat.NONE, GRCBase.GRCDateSeperator.NONE, GRCBase.GRCTimeFormat.HHMMAMPM);

                // WZ - Putting this here for possible later implementation
                //if ((AHAAppSettings.ColorCoding_EffectiveDays == null) || item.EffectiveDate.CompareTo(DateTime.Now.AddDays(AHAAppSettings.ColorCoding_EffectiveDays.Value)) == 1)
                //{
                //    RiskLevelHelper.RiskLevelBloodGlucose bgRiskLevelIndicator = new RiskLevelHelper.RiskLevelBloodGlucose(item.Value.Value);
                //    litA1c.Text = "<td class=\"" + bgRiskLevelIndicator.ColorReading1 + "\">" + GRCBase.StringHelper.GetFormattedDoubleString(item.Value.Value, 2, false) + "</td>";
                //}
                //else
                //{
                //    litA1c.Text = "<td>" + GRCBase.StringHelper.GetFormattedDoubleString(item.Value.Value, 2, false) + "</td>";
                //}

                // "Convert" the double value (0.95) to percentage (95)
                litA1c.Text = "<td>" + AHAAPI.H360Utility.HbA1cToPercentage(item.Value.Value) + "%</td>";

                litReadingSource.Text = item.Source.Value;

                if (!string.IsNullOrEmpty(item.Note.Value))
                {
                    if (item.Note.Value.Length > 18)
                    {
                        // comment is truncated and entire comment is put in a tooltip
                        litComment.Text = "<span data-tooltip=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + item.Note.Value + "\">" +
                                            GRCBase.StringHelper.GetShortString(item.Note.Value, 18, true) + "</span>";
                    }
                    else
                    {
                        // entire comment fits in cell.
                        litComment.Text = item.Note.Value;
                    }
                }

                //
                // Hook up Actions
                //
                ImageButton actEdit = e.Item.FindControl("actEditor") as ImageButton;
                ImageButton actTrash = e.Item.FindControl("actTrash") as ImageButton;
                string itemKey = item.ThingID.ToString();

                if (actEdit != null)
                {
                    string editUrl = "PopUp.aspx?closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE).ToString() + "&hba1c=edit&elid=" + itemKey;
                    actEdit.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE);
                }
                if (actTrash != null)
                {
                    // delete requires both ThingID and VersionStamp
                    itemKey += "," + item.VersionStamp.ToString();
                    string trashUrl = "PopUp.aspx?delete=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE).ToString() + "&hba1c=delete&elid=" + itemKey;
                    actTrash.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE);
                }
            }
        }

        protected void rptHistory_ItemCommandTest(object sender, RepeaterCommandEventArgs e)
        {

            int iii = 0;
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        public void LoadItems(DataDateReturnType DateRange)
        {
            SortAscending = false;
            SortExpression = string.Empty;
        }
    }
}