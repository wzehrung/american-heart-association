﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BGPrinterFriendly.ascx.cs" Inherits="Heart360Web.Controls.Patient.BloodGlucose.BGPrinterFriendly" %>

<%@ Register Src="~/Controls/Patient/ModuleItemGraph.ascx" TagName="BGGraph" TagPrefix="printerFriendlyInnerCtrl" %>

<h3><%=GetGlobalResourceObject("Reports", "Text_Heading_MyBgRecord")%></h3>
<printerFriendlyInnerCtrl:BGGraph ID="GraphCtrl" runat="server" />
<asp:Literal ID="litAverageBloodGlucose" runat="server"></asp:Literal>
<div class="full history-table-wrap spt" >
    <asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="false" runat="server">
        <HeaderTemplate>
    	         <table>
		            <thead>
			            <tr>
				            <th width="20%"><%=GetGlobalResourceObject("Reports", "Text_DateTime")%></th>
				            <th width="20%"><%=GetGlobalResourceObject("Reports", "Text_BGWithUnits")%></th>
				            <th width="30%"><%=GetGlobalResourceObject("Reports", "Text_ReadingType")%></th>
				            <th width="30%"><%=GetGlobalResourceObject("Reports", "Text_ActionITook")%></th>
			            </tr>
		            </thead>
                    <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
			    <td><asp:Literal ID="litDateTime" runat="server"/></td>
			    <td><asp:Literal ID="litGlucose" runat="server"/></td>
			    <td><asp:Literal ID="litReadingType" runat="server"/></td>
			    <td><asp:Literal ID="litActionTaken" runat="server"/></td>
		    </tr>
        </ItemTemplate>
        <FooterTemplate>
                    </tbody>
	            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
