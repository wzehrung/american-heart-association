﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using AHACommon;
using HVManager;
using AHAAPI;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using Microsoft.Health.Web;
using AjaxControlToolkit;

using Heart360Web;

//using GRCBase.WebControls_V1; // For the date picker control...NEED to get away from using this

namespace Heart360Web.Controls.Patient
{
    public partial class ModuleAddReading : System.Web.UI.UserControl
    {
        private Heart360Web.PatientPortal.MODULE_ID     mModuleID;
        private bool                                    mEnableTitle;
        private bool                                    mEnableAddButton;
        private bool                                    mEnableComments;
        private string                                  mEditKeys ;         // one or more comma separated values representing the "keys" to the database item to edit.
        private string                                  iIrrHearbeat;

        public ModuleAddReading()
            : base()
        {
            mEnableAddButton = true;
            mEnableTitle = true;
            mEnableComments = true;
            mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
            mEditKeys = string.Empty ;
        }

        // Need to track if we are adding an A1c reading
        // so we don't show the add BG reading fields
        public string AddorEditHbA1c
        {   
            get
            {
                string value = string.Empty;

                if ( Request.QueryString["hba1c"] != null)
                {
                    string query = Request.QueryString["hba1c"].ToString();

                    switch (query)
                    {
                        case "add":
                            value = "add";
                            break;
                        
                        case "edit":
                            value = "edit";
                            break;

                        default :
                            value = null;
                            break;
                    }
                }
                return value;
            }
        }

        public int ModuleIdAsInt      // need this so (html/asp) markup can specify modules
        {
            set
            {
                mModuleID = Heart360Web.PatientPortal.GetModuleIdFromInt(value);
            }
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        public bool EnableAddButton
        {
            get { return mEnableAddButton; }
            set { mEnableAddButton = value; }
        }

        public bool EnableTitle
        {
            get { return mEnableTitle; }
            set { mEnableTitle = value; }
        }

        public bool EnableComments
        {
            get { return mEnableComments; }
            set { mEnableComments = value; }
        }

        public DateTime DateTime
        {
            get
            {
                // Get the time from the time dropdown
                string strTime = ddlTime.SelectedValue;               
                string[] strTimeArr = strTime.Split(":".ToCharArray());

                string strDate = txtDate.Text;
                DateTime dt = Convert.ToDateTime(strDate);

                DateTime dtNewDate = (this.divCommonDataTime.Visible) ?
                                        new DateTime(dt.Year, dt.Month, dt.Day, Convert.ToInt32(strTimeArr[0]), Convert.ToInt32(strTimeArr[1]), 0) :
                                        new DateTime(dt.Year, dt.Month, dt.Day);

                return dtNewDate;
            }
        }

        public string EditKeys
        {
            set { mEditKeys = value;  }
        }

        private bool IsEditMode
        {
            get;
            set;
        }

        public bool? IrregularHeartbeat
        {
            get
            {
                iIrrHearbeat = ddlIrregularHeartbeat.SelectedItem.Value;
                bool? bValue = null;
                if (iIrrHearbeat == "3" )
                {
                    bValue = false;
                }
                else if (iIrrHearbeat == "2")
                {
                    bValue = true;
                }

                return bValue;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            
            // We need to see if this is an edit session (logic dictated by existance of edit keys
            if (!string.IsNullOrEmpty(mEditKeys))
            {

                // TODO: try and get the data item associated with the keys.
                // if we are successful, we are in edit mode.
                IsEditMode = true;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // This bit of logic is left over (but required) from V5. Put it up front (here) instead of sprinkled throughout the saves
            if (AHAAPI.SessionInfo.GetUserIdentityContext() != null) // && AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
            {
                string param1 = string.Empty;

                btnSave.Text = GetGlobalResourceObject("Common", "Text_Save").ToString();

                //
                // NOTE: we only want to load drop downs when it's not a post back!
                //       if we do it during post-back we wipe out (view state) the values user has choosen.
                //

                // Load generic stuff only if not post back
                if (!IsPostBack)
                {
                    _LoadReadingSourceDDL();
                    _LoadTimeOfDay(ddlTime);
                    _LoadIrregularHeartbeatTypeDDL();
                    // Set the default date to the current one
                    txtDate.Text = DateTime.Now.ToString("d");
                }

                // Get the correct module to load
                switch (ModuleID)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                        if (IsEditMode && !Page.IsPostBack)
                        {
                            _LoadIrregularHeartbeatTypeDDL();

                            if (!_SetUIForBPEdit())
                                IsEditMode = false;
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodPressure").ToString() :
                                                             GetGlobalResourceObject("Tracker", "Tracker_Title_EditReading_BloodPressure").ToString();
                        divBloodPressure.Visible = true;
                        divCommonData.Visible = true;

                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        if (IsEditMode && !Page.IsPostBack)
                        {
                            if (!_SetUIForWEdit())
                                IsEditMode = false;
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_Weight").ToString() :
                                                             GetGlobalResourceObject("Tracker", "Tracker_Title_EditReading_Weight").ToString() ;

                        divWeight.Visible = true;
                        divCommonData.Visible = true;

                        // Now see if the user has previously entered their height.
                        if (!HeightWrapper.IsUserHeightDefined())
                        {
                            divWeightHeight.Visible = true;
                        }
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                        if ( !Page.IsPostBack)
                        {
                            _LoadActivityTypeDDL();
                            _LoadExerciseIntensityDDL();
                            if (IsEditMode)
                            {
                                if (!_SetUIForPAEdit())
                                    IsEditMode = false;
                            }
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_PhysicalActivity").ToString() :
                                                             GetGlobalResourceObject("Tracker", "Tracker_Title_EditReading_PhysicalActivity").ToString() ;

                        divActivity.Visible = true;
                        divCommonData.Visible = true;
                        divCommonDataTime.Visible = false;
                        divCommonDataReadingSource.Visible = false;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:

                        if (Request.QueryString["hba1c"] == "add" || Request.QueryString["hba1c"] == "edit")
                        {
                            if (!Page.IsPostBack)
                            {
                                if (IsEditMode)
                                {
                                    if (!_SetUIForHbA1cEdit())
                                        IsEditMode = false;
                                }
                            }

                            this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_A1c").ToString() :
                                                                 GetGlobalResourceObject("Tracker", "Tracker_Title_EditReading_A1c").ToString();

                            divA1c.Visible = true;
                            divCommonDataReadingSource.Visible = false;
                        }
                        else
                        {
                            if (!Page.IsPostBack)
                            {
                                _LoadBGReadingTypeDDL();
                                _LoadBGActionITook();

                                if (IsEditMode)
                                {
                                    if (!_SetUIForBGEdit())
                                        IsEditMode = false;
                                }
                            }
                            this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodGlucose").ToString() :
                                                                 GetGlobalResourceObject("Tracker", "Tracker_Title_EditReading_BloodGlucose").ToString();

                            divBloodGlucose.Visible = true;
                        }

                            divCommonData.Visible = true;
                            divCommonDataReadingSource.Visible = true;

                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                        if (IsEditMode && !Page.IsPostBack)
                        {
                            if (!_SetUIForCEdit())
                                IsEditMode = false;
                        }
                        this.litTitle.Text = (!IsEditMode) ? GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_Cholesterol").ToString() :
                                                             GetGlobalResourceObject("Tracker", "Tracker_Title_EditReading_Cholesterol").ToString() ;
                        this.hidCalculate.Value = "0";

                        divCholesterol.Visible = true;
                        divCommonData.Visible = true;
                        divCommonDataTime.Visible = false;
                        divComments.Visible = false;
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                    default:
                        this.litTitle.Text = "INVALID MODULE ID";
                        break;
                }

                // Turn off the Save button
                if (mEnableAddButton == false)
                {
                    btnSave.Enabled = false;
                    btnSave.Visible = false;
                }

                // Turn off the Title
                if (mEnableTitle == false)
                {
                    litTitle.Visible = false;
                }

                // Turn off the Comments section
                if (mEnableComments == false)
                {
                    divComments.Visible = false;
                }
            }
            else
            {
                // TODO: Add a text error message
            }
        }

        // Call the appropriate save function based on the module ID
        protected void btnSave_Click(object sender, EventArgs e)
        {
                // Perform the save
            bool    successfulSave = false ;

            switch (ModuleID)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    Page.Validate("vgBP");
                    if (Page.IsValid)
                    {
                        _SaveBloodPressure();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    Page.Validate("vgW");
                    if (Page.IsValid)
                    {
                        _SaveWeight();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    Page.Validate("vgPA");
                    if (Page.IsValid)
                    {
                        _SavePhysicalActivity();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:

                    if (AddorEditHbA1c == "add" || AddorEditHbA1c == "edit")
                    {
                        Page.Validate("vgA1c");
                        if (Page.IsValid)
                        {
                            _SaveA1c();
                            successfulSave = true;
                        }
                    }
                    else
                    {
                        Page.Validate("vgBG");
                        if (Page.IsValid)
                        {
                            _SaveBloodGlucose();
                            successfulSave = true;
                        }
                    }

                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    Page.Validate("vgCH");
                    if (Page.IsValid)
                    {
                        _SaveCholesterol();
                        successfulSave = true;
                    }
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                default:
                    break;
            }

            if (successfulSave)
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }
        }

        // Populate the Reading Source dropdown
        private void _LoadReadingSourceDDL()
        {
            ddlReadingSource.DataSource = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.ReadingSource, H360Utility.GetCurrentLanguageID());
            ddlReadingSource.DataTextField = "ListItemName";
            ddlReadingSource.DataValueField = "ListItemCode";

            ddlReadingSource.DataBind();

            ddlReadingSource.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));
        }

        private void _LoadTimeOfDay(DropDownList ddlTimeOfDay)
        {
            ListItemCollection itemColl = TimeConversion.getTimeInAMPMFormat();

            ddlTimeOfDay.DataSource = itemColl;
            ddlTimeOfDay.DataTextField = "Text";
            ddlTimeOfDay.DataValueField = "Value";

            ddlTimeOfDay.DataBind();

            ddlTimeOfDay.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            // Set the default selected time to the current one rounded down to nearest half hour
            DateTime RoundDown = DateTime.Parse(DateTime.Now.ToShortTimeString());
            RoundDown = RoundDown.AddMinutes(-RoundDown.Minute);
            ddlTimeOfDay.SelectedValue = RoundDown.ToString("HH:mm");
        }

        #region Common
/**
        private bool ValidateDate(CustomValidator _cv)
        {
            bool retstat = true;

            if (string.IsNullOrEmpty(txtDate.Text.Trim()))
            {
                retstat = false;
                _cv.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Date_Value").ToString();
            }
            else
            {
                DateTime result;
                if (!DateTime.TryParse(txtDate.Text.Trim(), out result))
                {
                    retstat = false;
                   _cv.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Date_Invalid").ToString();
                }
                else if (result > DateTime.Now.Date)
                {
                    retstat = false;
                    _cv.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Date_Future").ToString();
                }
            }

            return retstat;
        }
**/
        private bool ValidateTimeOfDay(CustomValidator _cv)
        {
            bool retstat = true;

            if (string.IsNullOrEmpty(ddlTime.SelectedValue))
            {
                retstat = false;
                _cv.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_TOD_Value").ToString();
            }

            return retstat;
        }

        #endregion

        # region Blood Pressure

        public void _SaveBloodPressure()
        {
            // This has been written to minimize code duplication between save new and update existing

            try
            {
                BloodPressureDataManager.BPItem objBPItem = null;

                if (!IsEditMode)   // Save a new one!
                {
                    objBPItem = new BloodPressureDataManager.BPItem();
                }
                else
                {
                    Guid bpguid = new Guid(mEditKeys);
                    objBPItem = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataWithThingId(bpguid);
                }

                //
                // Assign the data from UI
                //
                objBPItem.When.Value = DateTime;

                if (string.IsNullOrEmpty(txtHeartRate.Text.Trim()))
                {
                    objBPItem.HeartRate.Value = null;
                }
                else
                {
                    objBPItem.HeartRate.Value = Convert.ToInt32(txtHeartRate.Text.Trim());
                }

                objBPItem.Systolic.Value = Convert.ToInt32(txtSystolic.Text.Trim());
                objBPItem.Diastolic.Value = Convert.ToInt32(txtDiastolic.Text.Trim());
                objBPItem.Note.Value = txtComments.Value;
                objBPItem.IrregularHeartbeat.Value = IrregularHeartbeat;

                if (string.IsNullOrEmpty(ddlReadingSource.SelectedItem.Text))
                {
                    objBPItem.Source.Value = BrandLogoHelperMachine.GetApplicationBrandInfo(this.Page);
                }
                else
                {
                    objBPItem.Source.Value = ddlReadingSource.SelectedItem.Value;
                }

                //
                // Save/update record
                //
                BloodPressureDataManager.BPItem objFinalItem = null;

                if (!IsEditMode)
                {
                    objFinalItem = HVHelper.HVManagerPatientBase.BloodPressureDataManager.CreateItem(objBPItem);
                }
                else
                {
                    HVHelper.HVManagerPatientBase.BloodPressureDataManager.UpdateItem(objBPItem);
                    objFinalItem = objBPItem;
                }

                //DB Update
                AHAHelpContent.BloodPressure.CreateOrChangeBloodPressure(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                    objFinalItem.ThingKey.Id, 
                    objFinalItem.ThingKey.VersionStamp,
                    objFinalItem.Systolic.Value, 
                    objFinalItem.Diastolic.Value,
                    objFinalItem.HeartRate.Value, 
                    objFinalItem.Source.Value,
                    objFinalItem.Note.Value, 
                    objFinalItem.When.Value,
                    objFinalItem.IrregularHeartbeat.Value);


                AlertManager.BloodPressureAlertHelper.ProcessHeart360ReadingForAlert(PatientGuids.CurrentPatientGuids.PersonID, PatientGuids.CurrentPatientGuids.RecordID, AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value,
                    CurrentUser.FullName, objFinalItem.When.Value, objFinalItem.Systolic.Value, objFinalItem.Diastolic.Value,
                        objFinalItem.HeartRate.Value, objFinalItem.ThingKey.Id);
            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }       
        }

        private bool _SetUIForBPEdit()
        {
            bool retval = false;
            try
            {
                Guid bpguid = new Guid(mEditKeys);
                BloodPressureDataManager.BPItem item = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataWithThingId(bpguid);

                calDatePicker.SelectedDate = item.When.Value;

                //ddlTime.Text = TimeConversion.GetFormattedTimeToShow(item.When.Value);
                //ddlTime.SelectedValue = TimeConversion.GetFormattedTimeString(item.When.Value);
                ddlTime.SelectedValue = TimeConversion.GetFormattedTimeString(item.When.Value);
                if (item.HeartRate.Value.HasValue)
                {
                    txtHeartRate.Text = item.HeartRate.Value.Value.ToString();
                }
                txtDiastolic.Text = item.Diastolic.Value.ToString();
                txtSystolic.Text = item.Systolic.Value.ToString();
                txtComments.Value = item.Note.Value;

                ddlReadingSource.SelectedValue = item.Source.Value;


                //if (item.h)
                //{
                    string strIrregularHeartbeat = string.Empty;
                    bool? bValue = item.IrregularHeartbeat.Value;

                    if (bValue == true)
                    {
                        strIrregularHeartbeat = "2"; // Yes
                    }
                    else if (bValue == false)
                    {
                        strIrregularHeartbeat = "3"; // No
                    }
                    else
                    {
                        strIrregularHeartbeat = "1";  // Don't Know
                    }

                    ddlIrregularHeartbeat.SelectedValue = strIrregularHeartbeat;
                //}

                

                retval = true;
            }
            catch (Exception e)
            {
                // WHAT TO DO HERE?
            }
            return retval;
        }

        #endregion

        # region Weight

        private bool _SetUIForWEdit()
        {
            bool retval = false;

            try
            {
                Guid wguid = new Guid(mEditKeys);

                HVManager.WeightDataManager.WeightItem item = HVHelper.HVManagerPatientBase.WeightDataManager.GetDataWithThingId(wguid);

                calDatePicker.SelectedDate = item.When.Value;

                //ddlTime.Text = TimeConversion.GetFormattedTimeToShow(item.When.Value);
                //ddlTime.SelectedValue = TimeConversion.GetFormattedTimeString(item.When.Value);
                ddlTime.SelectedValue = TimeConversion.GetFormattedTimeString(item.When.Value);

                ddlReadingSource.SelectedValue = item.Source.Value;
                txtWeight.Text = GRCBase.StringHelper.GetFormattedDoubleString(item.CommonWeight.Value.ToPounds(), 2, false);

                txtComments.Value = item.Note.Value;
                retval = true;
            }
            catch (Exception e)
            {
                // WHAT TO DO HERE?
            }

            return retval;
        }

            // Assuming that all weight values have been validated
        private void _SaveWeight()
        {
            HVManager.WeightDataManager.WeightItem objWItem = null;

            try
            {
                if (!IsEditMode)
                {
                    if (divWeightHeight.Visible)
                    {
                        double dFeetToMeter = HVManager.UnitConversion.FeetToMeters((double)Convert.ToInt32(tbHeightA.Text.Trim()));
                        double dInchesToMeter = HVManager.UnitConversion.InchesToMeters(Convert.ToDouble(tbHeightB.Text.Trim()));
                        double dMeters = dFeetToMeter + dInchesToMeter;

                        HeightWrapper.SaveHeightInMeters(dMeters);
                    }

                    objWItem = new HVManager.WeightDataManager.WeightItem();
                }
                else
                {
                    Guid wguid = new Guid(mEditKeys);
                    objWItem = HVHelper.HVManagerPatientBase.WeightDataManager.GetDataWithThingId(wguid);
                }

                objWItem.CommonWeight.Value = HVManager.CommonWeight.FromPounds(Convert.ToDouble(txtWeight.Text.Trim()));
                objWItem.When.Value = DateTime;
                objWItem.Note.Value = txtComments.Value.Trim();
                objWItem.Source.Value = ddlReadingSource.SelectedValue;

                //
                // Save/update record
                //
                HVManager.WeightDataManager.WeightItem itemFinal = null;

                if (!IsEditMode)
                {
                    itemFinal = HVHelper.HVManagerPatientBase.WeightDataManager.CreateItem(objWItem);
                }
                else
                {
                    itemFinal = HVHelper.HVManagerPatientBase.WeightDataManager.UpdateItem(objWItem);
                }

                //DB Update
                AHAHelpContent.WeightDetails.CreateOrChangeWeight( AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                                                                    itemFinal.ThingKey.Id, itemFinal.ThingKey.VersionStamp,
                                                                    Convert.ToDouble(txtWeight.Text.Trim()), 
                                                                    ddlReadingSource.SelectedValue,
                                                                    txtComments.Value.Trim(),
                                                                    DateTime);
            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }
        }

        #endregion

        # region Physical Activity

        private void _LoadActivityTypeDDL()
        {
            ddlActivityType.DataSource = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.ActivityType, H360Utility.GetCurrentLanguageID());
            ddlActivityType.DataTextField = "ListItemName";
            ddlActivityType.DataValueField = "ListItemCode";
            ddlActivityType.DataBind();

            ddlActivityType.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlActivityType.SelectedItem.Value = (string.Empty);
        }

        private void _LoadExerciseIntensityDDL()
        {
            ddlIntensity.DataSource = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.ExerciseIntensity, H360Utility.GetCurrentLanguageID());
            ddlIntensity.DataTextField = "ListItemName";
            ddlIntensity.DataValueField = "ListItemCode";
            ddlIntensity.DataBind();

            ddlIntensity.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlIntensity.SelectedItem.Value = (string.Empty);
        }

        private void _LoadIrregularHeartbeatTypeDDL() // WHZ 3-26-15
        {
            ddlIrregularHeartbeat.DataSource = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.IrregularHeartbeat, H360Utility.GetCurrentLanguageID());
            ddlIrregularHeartbeat.DataTextField = "ListItemName";
            ddlIrregularHeartbeat.DataValueField = "ListItemCode";
            ddlIrregularHeartbeat.DataBind();

            ddlIrregularHeartbeat.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlIrregularHeartbeat.SelectedItem.Value = (string.Empty);
        }

        private bool _SetUIForPAEdit()
        {
            bool retval = false;

            try
            {
                Guid paguid = new Guid(mEditKeys);
                HVManager.ExerciseDataManager.ExerciseItem item = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataWithThingId(paguid);

                calDatePicker.SelectedDate = item.When.Value;

                // Physical Activity has no Time, just a Date

                ddlActivityType.SelectedValue = item.Activity.Value;
                txtDuration.Text = (item.Duration.Value.HasValue) ? item.Duration.Value.ToString() : string.Empty;
                txtSteps.Text = (item.NumberOfSteps.Value.HasValue) ? item.NumberOfSteps.Value.ToString() : string.Empty;

                string strIntensity = string.Empty ;

                switch (item.Intensity.Value)
                {
                    case Microsoft.Health.ItemTypes.RelativeRating.High: 
                    case Microsoft.Health.ItemTypes.RelativeRating.VeryHigh: 
                        strIntensity = "Vigorous";
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.Low: 
                    case Microsoft.Health.ItemTypes.RelativeRating.VeryLow:
                        strIntensity = "Light";
                        break;
                    case Microsoft.Health.ItemTypes.RelativeRating.Moderate: 
                        strIntensity = "Moderate";
                        break;
                    
                    case Microsoft.Health.ItemTypes.RelativeRating.None:
                    default:
                        break;
                }
                ddlIntensity.SelectedValue = strIntensity;


                txtComments.Value = item.Note.Value;
                retval = true;
            }
            catch (Exception e)
            {
                // WHAT TO DO HERE?
            }

            return retval;
        }

        private void _SavePhysicalActivity()
        {
            RelativeRating intensity = RelativeRating.Low;
            int? iSteps = null;
            switch (ddlIntensity.SelectedValue)
            {
                case "Moderate": intensity = RelativeRating.Moderate;
                    break;
                case "Vigorous": intensity = RelativeRating.VeryHigh;
                    break;
            }

            HVManager.ExerciseDataManager.ExerciseItem objPAItem = null;

            try
            {
                if (!IsEditMode)
                {
                    objPAItem = new HVManager.ExerciseDataManager.ExerciseItem();
                }
                else
                {
                    Guid paguid = new Guid(mEditKeys);
                    objPAItem = HVHelper.HVManagerPatientBase.ExerciseDataManager.GetDataWithThingId(paguid);
                }

                objPAItem.When.Value = DateTime;
                objPAItem.Activity.Value = ddlActivityType.SelectedValue;
                objPAItem.Duration.Value = Convert.ToDouble(txtDuration.Text.Trim());
                objPAItem.Intensity.Value = intensity;
                objPAItem.Source.Value = ddlReadingSource.SelectedValue;

                if (!String.IsNullOrEmpty(txtSteps.Text.Trim()))
                {
                    iSteps = Convert.ToInt32(txtSteps.Text.Trim());
                    objPAItem.NumberOfSteps.Value = iSteps;
                }
                else if( IsEditMode && objPAItem.NumberOfSteps.Value.HasValue )
                {
                    objPAItem.NumberOfSteps.Value = null ;
                }

                objPAItem.Note.Value = txtComments.Value.Trim();

                // Save record
                HVManager.ExerciseDataManager.ExerciseItem finalObj = (!IsEditMode) ? HVHelper.HVManagerPatientBase.ExerciseDataManager.CreateItem( objPAItem ) :
                                                                                      HVHelper.HVManagerPatientBase.ExerciseDataManager.UpdateItem( objPAItem ) ;

                //DB Update
                AHAHelpContent.Exercise.CreateOrChangeExercise( AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID, 
                                                                finalObj.ThingKey.Id, finalObj.ThingKey.VersionStamp, 
                                                                DateTime, 
                                                                ddlActivityType.SelectedValue, 
                                                                intensity.ToString(), 
                                                                Convert.ToDouble(txtDuration.Text), 
                                                                txtComments.Value.Trim(), 
                                                                iSteps, 
                                                                ddlReadingSource.SelectedValue);
            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }
        }

        #endregion

        # region Blood Glucose

        private void _LoadBGActionITook()
        {
            ddlAction.DataSource = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.ActionITook, H360Utility.GetCurrentLanguageID());
            ddlAction.DataTextField = "ListItemName";
            ddlAction.DataValueField = "ListItemCode";
            ddlAction.DataBind();

            ddlAction.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlAction.SelectedItem.Value = (string.Empty);
        }

        private void _LoadBGReadingTypeDDL()
        {
            ddlReadingType.DataSource = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.ReadingType, H360Utility.GetCurrentLanguageID());
            ddlReadingType.DataTextField = "ListItemName" ;
            ddlReadingType.DataValueField = "ListItemCode";
            ddlReadingType.DataBind();

            ddlReadingType.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlReadingType.SelectedItem.Value = (string.Empty);
        }

        private bool _SetUIForBGEdit()
        {
            bool retval = false;

            try
            {
                Guid                            bgguid = new Guid(mEditKeys);
                BloodGlucoseDataManager.BGItem  item = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetDataWithThingId(bgguid);

                calDatePicker.SelectedDate = item.When.Value;

                //ddlTime.Text = TimeConversion.GetFormattedTimeToShow(item.When.Value);
                //ddlTime.SelectedValue = TimeConversion.GetFormattedTimeString(item.When.Value);
                ddlTime.SelectedValue = TimeConversion.GetFormattedTimeString(item.When.Value);

                txtBloodGlucose.Text = item.Value.Value.ToString();
                ddlAction.SelectedValue = item.ActionTaken.Value;
                ddlReadingType.SelectedValue = item.ReadingType.Value;

                txtComments.Value = item.Note.Value;
                retval = true;
            }
            catch (Exception e)
            {
                // WHAT TO DO HERE?
            }

            return retval;
        }

        private bool _SetUIForHbA1cEdit()
        {
            bool retval = false;

            try
            {
                Guid bgguid = new Guid(mEditKeys);
                
                HbA1cDataManager.HbA1cItem item = HVHelper.HVManagerPatientBase.HbA1cDataManager.GetDataWithThingId(bgguid);

                calDatePicker.SelectedDate = item.When.Value;

                ddlTime.SelectedValue = TimeConversion.GetFormattedTimeString(item.When.Value);

                txtA1c.Text = AHAAPI.H360Utility.HbA1cToPercentage(item.Value.Value).ToString();
                ddlReadingSource.SelectedValue = item.Source.Value;
                txtComments.Value = item.Note.Value;

                retval = true;
            }
            catch (Exception e)
            {
                // WHAT TO DO HERE?
            }

            return retval;
        }

        private void _SaveA1c()
        {
            HbA1cDataManager.HbA1cItem item = null;

            try
            {
                if (!IsEditMode)
                {
                    item = new HbA1cDataManager.HbA1cItem();
                }
                else
                {
                    Guid ac1guid = new Guid(mEditKeys);
                    item = HVHelper.HVManagerPatientBase.HbA1cDataManager.GetDataWithThingId(ac1guid);
                }

                // Basically converting the whole number (percent) to a decimal
                string strValue = "." + txtA1c.Text;

                // Convert value to double/float. Example: 0.12
                item.Value.Value = Convert.ToDouble(strValue);
                item.When.Value = DateTime;
                item.Note.Value = txtComments.Value.Trim();
                item.Source.Value = ddlReadingSource.SelectedValue;

                HbA1cDataManager.HbA1cItem itemCreated = (!IsEditMode) ?
                    HVHelper.HVManagerPatientBase.HbA1cDataManager.CreateItem(item) :
                    HVHelper.HVManagerPatientBase.HbA1cDataManager.UpdateItem(item) ;

                //DB Update
                AHAHelpContent.BloodGlucose.CreateOrChangeHbA1c(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                    itemCreated.ThingKey.Id, itemCreated.ThingKey.VersionStamp,
                    itemCreated.Value.Value, 
                    itemCreated.When.Value, itemCreated.Note.Value, ddlReadingSource.SelectedValue);

            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }     
        }

        private void _SaveBloodGlucose()
        {
            BloodGlucoseDataManager.BGItem item = null ;

            try
            {
                if (!IsEditMode)
                {
                    item = new BloodGlucoseDataManager.BGItem();
                }
                else
                {
                    Guid bgguid = new Guid(mEditKeys);
                    item = HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.GetDataWithThingId(bgguid);
                }

                item.ActionTaken.Value = ddlAction.SelectedValue.Trim();
                item.When.Value = DateTime;
                item.Value.Value = Convert.ToDouble(txtBloodGlucose.Text.Trim());
                item.ReadingType.Value = ddlReadingType.SelectedValue.Trim();

                item.Note.Value = txtComments.Value.Trim();
                item.Source.Value = ddlReadingSource.SelectedValue;

                BloodGlucoseDataManager.BGItem itemCreated = (!IsEditMode) ? 
                                                                HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.CreateItem(item) :
                                                                HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.UpdateItem( item ) ;

                //DB Update
                AHAHelpContent.BloodGlucose.CreateOrChangeBloodGlucose( AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                    itemCreated.ThingKey.Id, itemCreated.ThingKey.VersionStamp,
                    itemCreated.Value.Value, itemCreated.ActionTaken.Value,
                    itemCreated.ReadingType.Value, itemCreated.When.Value, itemCreated.Note.Value, ddlReadingSource.SelectedValue);

                AlertManager.BloodGlucoseAlertHelper.ProcessHeart360ReadingForAlert( PatientGuids.CurrentPatientGuids.PersonID, 
                                                                                     PatientGuids.CurrentPatientGuids.RecordID, 
                                                                                     AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value,
                                                                                     CurrentUser.FullName, 
                                                                                     itemCreated.Value.Value, 
                                                                                     itemCreated.When.Value, 
                                                                                     itemCreated.ThingKey.Id);
            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }      
        }

        #endregion

        # region Cholesterol

        protected void OnRecalculateTC(object sender, EventArgs e)
        {
            // we need to validate the inputs: HDL, LDL, and Triglycerides.
            // also make sure that total cholesterol is not validated.

            this.hidCalculate.Value = "1";

            Page.Validate("vgCH");
            if (!Page.IsValid)
                return;

            // If we are here, there is something input into HDL, LDL and Trigs.
            int dblLDL = 0;
            int dblHDL = 0;
            int dblTriglycerides = 0;

            try
            {
                if (!String.IsNullOrEmpty(txtLDL.Text.Trim()))
                {
                    dblLDL = Convert.ToInt32(txtLDL.Text.Trim());
                }

                if (!String.IsNullOrEmpty(txtHDL.Text.Trim()))
                {
                    dblHDL = Convert.ToInt32(txtHDL.Text.Trim());
                }

                if (!String.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
                {
                    dblTriglycerides = Convert.ToInt32(txtTriglycerides.Text.Trim());
                }

                int TC = dblHDL + dblLDL + (dblTriglycerides / 5);

                txtTotalCholesterol.Text = TC.ToString();
            }
            catch
            {
                // this shouldn't happen as all inputs would have been validated
            }

            // Now make sure the computed value is valid
            Page.Validate("vgCHPostCalculate");

        }

        private bool _SetUIForCEdit()
        {
            bool retval = false;

            try
            {
                Guid cguid = new Guid(mEditKeys);
                CholesterolDataManager.CholesterolItem item = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetDataWithThingId(cguid);

                calDatePicker.SelectedDate = item.When.Value;

                if (item.TotalCholesterol.Value.HasValue)
                    txtTotalCholesterol.Text = item.TotalCholesterol.Value.Value.ToString();

                if (item.HDL.Value.HasValue)
                    txtHDL.Text = item.HDL.Value.Value.ToString();

                if (item.LDL.Value.HasValue)
                    txtLDL.Text = item.LDL.Value.Value.ToString();

                if (item.Triglycerides.Value.HasValue)
                    txtTriglycerides.Text = item.Triglycerides.Value.Value.ToString();

                ddlReadingSource.SelectedValue = item.TestLocation.Value;
                retval = true;
            }
            catch (Exception e)
            {
                // WHAT TO DO HERE?
            }
            return retval;
        }

        private void _SaveCholesterol()
        {
            CholesterolDataManager.CholesterolItem item = null ;

            try
            {
                if (!IsEditMode)
                {
                    item = new CholesterolDataManager.CholesterolItem();
                }
                else
                {
                    Guid cguid = new Guid(mEditKeys);
                    item = HVHelper.HVManagerPatientBase.CholesterolDataManager.GetDataWithThingId(cguid);
                }

                item.When.Value = DateTime;
                item.TotalCholesterol.Value = Convert.ToInt32(txtTotalCholesterol.Text.Trim());

                if (String.IsNullOrEmpty(txtLDL.Text.Trim()))
                {
                    item.LDL.Value = null;
                }
                else
                {
                    item.LDL.Value = Convert.ToInt32(txtLDL.Text.Trim());
                }

                if (String.IsNullOrEmpty(txtHDL.Text.Trim()))
                {
                    item.HDL.Value = null;
                }
                else
                {
                    item.HDL.Value = Convert.ToInt32(txtHDL.Text.Trim());
                }

                if (String.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
                {
                    item.Triglycerides.Value = null;
                }
                else
                {
                    item.Triglycerides.Value = Convert.ToInt32(txtTriglycerides.Text.Trim());
                }

                if (string.IsNullOrEmpty(ddlReadingSource.SelectedValue))
                {
                    item.TestLocation.Value = null;
                }
                else
                {
                    item.TestLocation.Value = ddlReadingSource.SelectedValue;
                }


                CholesterolDataManager.CholesterolItem finalItem = (!IsEditMode) ? 
                                                                    HVHelper.HVManagerPatientBase.CholesterolDataManager.CreateItem(item) :
                                                                    HVHelper.HVManagerPatientBase.CholesterolDataManager.UpdateItem(item) ;

                //DB Update
                AHAHelpContent.Cholesterol.CreateOrChangeCholesterol( AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                                                                      finalItem.ThingKey.Id, finalItem.ThingKey.VersionStamp,
                                                                      finalItem.TotalCholesterol.Value, 
                                                                      finalItem.LDL.Value,
                                                                      finalItem.HDL.Value, finalItem.Triglycerides.Value,
                                                                      finalItem.TestLocation.Value, finalItem.When.Value );
            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }
        }


        #endregion

        public bool PerformValidationForExternalUse()
        {
            bool retval = true;

            // make sure its external use and only implemented for blood pressure
            if (!mEnableAddButton && mModuleID ==  Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE)
            {
                Page.Validate("vgBPExternal");
                retval = Page.IsValid;
            }

            return retval;
        }

        #region VALIDATORS

        private void HeartRateValidationHelper(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            int             iHeartRate = 0;
            try
            {
                iHeartRate = Convert.ToInt32(txtHeartRate.Text.Trim());
            }
            catch
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_HeartRate").ToString();
            }

            if (iHeartRate <= 0)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_HeartRate").ToString();
            }

        }

        protected void ValidateBPHeartRate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (!String.IsNullOrEmpty(txtHeartRate.Text.Trim()))
            {
                HeartRateValidationHelper(sender, v);
            }

            // 9.18.2013 - Heart Rate is not required.
            /**
            else
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_HeartRate").ToString();
            }
             */
        }

        protected void ValidateBPSystolic(object sender, ServerValidateEventArgs v)
        {
            int             iSystolic = 0;
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;


            if (String.IsNullOrEmpty(txtSystolic.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Systolic").ToString();
            }
            else
            {
                try
                {
                    iSystolic = Convert.ToInt32(txtSystolic.Text.Trim());

                    if (iSystolic <= 0)
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Systolic").ToString();
                    }
                    else
                    {
                        // Make sure Systolic is greater than distolic
                        if (!String.IsNullOrEmpty(txtDiastolic.Text.Trim()))
                        {
                            try
                            {
                                int iDiastolic = Convert.ToInt32(txtDiastolic.Text.Trim());
                                if (iSystolic < iDiastolic)
                                {
                                    v.IsValid = false;
                                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_SystolicGtDiastolic").ToString();
                                }
                            }
                            catch { }   // this will be handled in diastolic validation
                        }

                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Systolic").ToString();
                }
            }
        }

        protected void ValidateBPDiastolic(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtDiastolic.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Diastolic").ToString();
            }
            else
            {
                try
                {
                    int iDiastolic = Convert.ToInt32(txtDiastolic.Text.Trim());
                    if (iDiastolic <= 0)
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Diastolic").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Diastolic").ToString();
                }
            }
        }

        protected void ValidateBPExternal(object sender, ServerValidateEventArgs v)
        {
            // Potentially validating Heart Rate, Systolic and Diastolic. None are required.
            
            v.IsValid = true;

            // A. If there is a Heart Rate value, it has to be valid.
            if (!String.IsNullOrEmpty(txtHeartRate.Text.Trim()))
            {
                HeartRateValidationHelper(sender, v);
            }

            if (v.IsValid && ( !string.IsNullOrEmpty(txtSystolic.Text.Trim()) || !string.IsNullOrEmpty(txtDiastolic.Text.Trim()) ) )
            {
                // B. There either has to be both Systolic and Diastolic values, or neither.
                if (string.IsNullOrEmpty(txtSystolic.Text.Trim()) || string.IsNullOrEmpty(txtDiastolic.Text.Trim()))
                {
                    CustomValidator valid = (CustomValidator)sender;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_SystolicAndDiastolic").ToString();
                    v.IsValid = false;
                }
                else
                {
                    ValidateBPSystolic(sender, v);
                    if( v.IsValid )
                        ValidateBPDiastolic(sender, v);
                }
            }
        }

        protected void ValidateWWeight(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtWeight.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight_Value").ToString();
                return;
            }

            try
            {
                double dbl = Convert.ToDouble(txtWeight.Text.Trim());
                /// Added to check that the weight entry is always greater than or equal to 1.
                if (dbl < 1 || dbl > 999)
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight").ToString();
                    return;
                }
            }
            catch
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Weight").ToString();
                return;
            }
        }

        protected void ValidateWHeight(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (divWeightHeight.Visible)
            {
                List<string> strList = new List<string>();

                if (String.IsNullOrEmpty(tbHeightA.Text.Trim()))
                {
                    strList.Add(GetGlobalResourceObject("Tracker", "Tracker_Profile_Feet").ToString());
                }
                else
                {
                    try
                    {
                        int iObjHeightInFeet = Convert.ToInt32(tbHeightA.Text);
                        if (iObjHeightInFeet <= 0)
                        { strList.Add(GetGlobalResourceObject("Tracker", "Tracker_Profile_Feet").ToString()); }
                    }
                    catch
                    {
                        strList.Add(GetGlobalResourceObject("Tracker", "Tracker_Profile_Feet").ToString());
                    }
                }

                if (String.IsNullOrEmpty(tbHeightB.Text.Trim()))
                {
                    strList.Add(GetGlobalResourceObject("Tracker", "Tracker_Profile_Inches").ToString());
                }
                else
                {
                    try
                    {
                        double dbObjHeightInches = Convert.ToDouble(tbHeightB.Text);
                        if ((dbObjHeightInches >= 12.00) || (dbObjHeightInches < 0.0))
                        {
                            strList.Add(GetGlobalResourceObject("Tracker", "Tracker_Profile_Inches").ToString());
                        }
                    }
                    catch
                    {
                        strList.Add(GetGlobalResourceObject("Tracker", "Tracker_Profile_Inches").ToString());
                    }
                }

                if (strList.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append(GetGlobalResourceObject("Tracker", "Validation_Text_Weight_ValidHeight").ToString());
                    foreach (string str in strList)
                    {
                        sb.Append(str + ",");
                    }
                    valid.ErrorMessage = sb.ToString().TrimEnd(',');
                    v.IsValid = false;
                }
            }
        }

        protected void ValidatePAActivity(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (string.IsNullOrEmpty( this.ddlActivityType.SelectedValue))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_ActivityType").ToString();
            }
        }

        protected void ValidatePADuration(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtDuration.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Duration").ToString();
            }
            else
            {
                try
                {
                    double dbl = Convert.ToDouble(txtDuration.Text.Trim());
                    if (!(dbl > 0.0))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_DurationGtZero").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Duration").ToString();
                }
            }
        }

        protected void ValidatePASteps(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Note: this field is not required, but if they did enter a value, it has to be valid
            if (!string.IsNullOrEmpty(txtSteps.Text))
            {
                int iSteps;
                bool bIsValidStep = int.TryParse(txtSteps.Text, out iSteps);
                if (!bIsValidStep || iSteps < 0)
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_StepsNonNegative").ToString();
                }
            }
        }

        protected void ValidatePAIntensity(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (string.IsNullOrEmpty(ddlIntensity.SelectedValue))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Intensity").ToString();
            }
        }

        protected void ValidateBGBloodGlucose(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtBloodGlucose.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_BloodGlucose").ToString();
            }
            else
            {
                try
                {
                    double dblGlucoseValue = Convert.ToDouble(txtBloodGlucose.Text.Trim());
                    if (!(dblGlucoseValue > 0))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_BloodGlucose").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_BloodGlucose").ToString();
                }
            }
        }

        protected void ValidateA1c(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtA1c.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_HbA1c_Specify").ToString();
            }
            else if (Convert.ToInt16(txtA1c.Text) < Convert.ToInt16(GetGlobalResourceObject("Tracker", "Validation_Text_HbA1c_Value_Lower_Limit")))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_HbA1c_Valid").ToString();
            }
            else if (Convert.ToInt16(txtA1c.Text) > Convert.ToInt16(GetGlobalResourceObject("Tracker", "Validation_Text_HbA1c_Value_Upper_Limit")))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_HbA1c_Valid").ToString();
            }
        }

        protected void ValidateCHHdl(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Note: this value is not required, but if the user specified a value, it needs to be validated
            if (!String.IsNullOrEmpty(txtHDL.Text.Trim()))
            {
                try
                {
                    int ival = Convert.ToInt32(txtHDL.Text.Trim());
                    if ((ival < 1) || (ival > 100000))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_HDL").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_HDL").ToString();
                }
            }
        }
        
        protected void ValidateCHLdl(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Note: this value is not required, but if the user specified a value, it needs to be validated
            if (!String.IsNullOrEmpty(txtLDL.Text.Trim()))
            {
                try
                {
                    int ival = Convert.ToInt32(txtLDL.Text.Trim());
                    if ((ival < 1) || (ival > 100000))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_LDL").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_LDL").ToString();
                }
            }
        }
        
        protected void ValidateCHTrig(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // Note: this value is not required, but if the user specified a value, it needs to be validated
            if (!String.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
            {
                try
                {
                    int ival = Convert.ToInt32(txtTriglycerides.Text.Trim());
                    if ((ival < 1) || (ival > 100000))
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_Triglyceride").ToString();
                    }
                }
                catch
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_Triglyceride").ToString();
                }
            }
        }
        
        protected void ValidateCHTotalC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (hidCalculate.Value.Equals("1"))  // they clicked on calculate button, validating before calculation
            {
                // we need to check if all three HDL, LDL and Triglycerides values are defined.
                if (string.IsNullOrEmpty(txtHDL.Text.Trim()) || string.IsNullOrEmpty(txtLDL.Text.Trim()) || string.IsNullOrEmpty(txtTriglycerides.Text.Trim()))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Total_Reqs").ToString();
                }
            }
            else
            {
                // user may have input a TC value computed from a device
                if (string.IsNullOrEmpty(this.txtTotalCholesterol.Text.Trim()))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_Total").ToString();
                }
                else
                {
                    try
                    {
                        int ival = Convert.ToInt32(txtTotalCholesterol.Text.Trim());
                        if ((ival < 1) || (ival > 100000))
                        {
                            v.IsValid = false;
                            valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Range_Total").ToString();
                        }
                    }
                    catch
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Integer_Total").ToString();
                    }
                }

            }
        }

        protected void ValidateCHPostTotalC(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = true;

            if (string.IsNullOrEmpty(this.txtTotalCholesterol.Text.Trim()))     // error was thrown
            {
                v.IsValid = false;
            }
            else
            {
                int ival = Convert.ToInt32(txtTotalCholesterol.Text.Trim());
                if ((ival < 1) || (ival > 100000))
                    v.IsValid = false;
            }

            if (!v.IsValid)
            {
                CustomValidator valid = (CustomValidator)sender;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Tracker_Validate_Cholesterol_Total_Notice_Recheck").ToString();
            }
        }

        protected void ValidateDateTime(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string  sdate = txtDate.Text.Trim() ;
            if (String.IsNullOrEmpty(sdate))     // first make sure there is a date specified
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Date").ToString();
            }
            else
            {
                DateTimeValidation.ValidateDateBeforeNow(sdate, valid, v);
            }


            //
            // Now validate time
            //

            if( v.IsValid && String.IsNullOrEmpty(ddlTime.SelectedValue))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Time").ToString();
            }
        }

        #endregion
    }
}