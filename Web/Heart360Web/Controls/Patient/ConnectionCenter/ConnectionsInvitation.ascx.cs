﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text ;

using AHACommon;
using AHAHelpContent;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class ConnectionsInvitation : System.Web.UI.UserControl
    {
        private bool        mSendConfirmation = false;
        private string      mSalutation;
        private string      mFirstName;
        private string      mLastName;

        private string SalutationAndFirst
        {
            get 
            { 
                return (string.IsNullOrEmpty(mSalutation)) ? string.Empty : (mSalutation + " "); 
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            mSendConfirmation = false;
            mSalutation = mFirstName = mLastName = string.Empty;

            // get the data from query parameters
            if (Request.Params["invitation"] != null)
            {
                string      csv = GRCBase.BlowFish.DecryptString(Request.Params["invitation"]);
                string[]    svals = csv.Split('|') ;

                // svals should now be salutation|0, fname, lname, boolAsString

                if (svals.Length == 4)
                {
                    if (!svals[0].Equals("0"))
                        mSalutation = svals[0];
                    mFirstName = svals[1];
                    mLastName = svals[2];
                    mSendConfirmation = (Convert.ToInt32(svals[3]) > 0) ? true : false;
                }

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Heart360Web.H360PageHelper ph = (Heart360Web.H360PageHelper)this.Page;
                Guid hrGUID = ph.RecordGUID;
                AHAHelpContent.Patient pat = AHAHelpContent.Patient.FindByUserHealthRecordGUID(hrGUID);

                //Create invitation entry
                PatientProviderInvitation objInvitation = PatientProviderInvitation.CreateProviderInvitation(SalutationAndFirst,
                                                                                                                    mLastName, null,
                                                                                                                    pat.UserHealthRecordID,
                                                                                                                    mSendConfirmation);

                litToName.Text = SalutationAndFirst + " " + mLastName;
                litUrl.Text = string.Format("{0}/ConnectPatient.aspx", GRCBase.UrlUtility.ServerName);
                litInvitationCode.Text = objInvitation.InvitationCode;

                string strPrintInviteMessage = GetGlobalResourceObject("ConnCenterPAndV", "Patient_Invitation_PrintInvite").ToString();
                strPrintInviteMessage = System.Text.RegularExpressions.Regex.Replace(strPrintInviteMessage, "##ThankYouName##", CurrentUser.FullName);
                strPrintInviteMessage = System.Text.RegularExpressions.Regex.Replace(strPrintInviteMessage, "##Heart360Trademark##", AHAAPI.H360Utility.GetTrademarkedHeart360());
                litPrintInviteMessage.Text = strPrintInviteMessage;

            }
            catch
            {
                // Turn off Print button.
                this.divInvitation.Visible = false;
            }
        }
    }
}