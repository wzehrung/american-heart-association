﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Announcements.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.Announcements" %>

<section class="my-announcements info-stats full">
    <div class="accordion">

        <div class="toggle-btn">
            <h4><span class="icon-announce"></span><%=GetGlobalResourceObject("Tracker", "Title_Announcements")%><a id="aAnnouncementCount" href="tooltip" runat="server"></a><span class="gray-arrow"></span></h4>
        </div>

        <div class="content" style="display: block;">
        
            <ul class="inner-accordion">

                <asp:Repeater ID="repAnnouncements" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>

                        <li>
							<div class="trigger-wrap">
								<h4 class="inner-trigger"><%# Eval("AnnouncementTitle") %><span class="blue-arrow"></span></h4>
                                <p class="announcement-name"><%# Eval("ProviderName")%></p>
								<p class="announcement-date"><%# Eval("CreatedDate")%></p>
							</div>
                            <!-- Note that AnnouncementText is already html formatted, so no need to wrap it in a p tag -->
							<div style="display: none;" class="inner-content"><%# Eval("AnnouncementText")%></div>
						</li>

                    </ItemTemplate>
                </asp:Repeater>

                <li>
                
                    <div id="divDefault" runat="server" visible="false">
                        <span><%=GetGlobalResourceObject("Tracker", "Text_Announcements_Default")%></span>
                    </div>                
                
                </li>

            </ul>

        </div>

    </div>

</section>