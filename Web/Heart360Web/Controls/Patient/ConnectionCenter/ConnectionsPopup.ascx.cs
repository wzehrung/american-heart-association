﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAHelpContent;
using AHAAPI;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class ConnectionsPopup : System.Web.UI.UserControl
    {
            // sometimes with a popup that only has an OK button, we need to force a refresh back on the main page.
            // we do this by setting the CommandArgument of the btnViewOkOk to OK_FORCE_CLOSE
        private string OK_NORMAL_HIDE = string.Empty;
        private string OK_FORCE_CLOSE = "close";

        private string  mEditKeys;               // one or more pipe separated values representing the "keys" to what needs to be done.

        public ConnectionsPopup()
            : base()
        {
            mEditKeys = string.Empty;
        }

        public string EditKeys
        {
            set { mEditKeys = value; }
        }

        private AHAHelpContent.Patient GetPatient()
        {
            return AHAHelpContent.Patient.FindByUserHealthRecordGUID((Page as H360PageHelper).RecordGUID);
            /**
            Heart360Web.H360PageHelper ph = (Heart360Web.H360PageHelper)this.Page;
            Guid hrGUID = ph.RecordGUID;
            return AHAHelpContent.Patient.FindByUserHealthRecordGUID(hrGUID);
            **/
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(mEditKeys))
                {
                    string[] keys = mEditKeys.Split('|');

                    this.btnViewConfirmOk.CommandArgument = OK_NORMAL_HIDE;
                    this.btnViewOkOk.CommandArgument = OK_NORMAL_HIDE;

                    if (keys.Length > 0)
                    {
                        if (keys[0].Equals("congrats"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Congrats").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_Connections_Request").ToString();
                            mvViews.ActiveViewIndex = 1;

                        }
                        else if (keys[0].Equals("connect"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Connect").ToString();
                            this.divNoSearchResults.Visible = false;
                            mvViews.ActiveViewIndex = 0;
                            LoadSearchResults(keys);
                        }
                        else if (keys[0].Equals("connectEmpty"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Connect").ToString();
                            this.divSearchResults.Visible = false;
                            mvViews.ActiveViewIndex = 0;
                        }
                        else if (keys[0].Equals("grant"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_GrantProviderAccess").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_SuccessfullyGranted").ToString() + " " +
                                                             ((keys.Length > 1) ? keys[1] : "unknown");
                            mvViews.ActiveViewIndex = 1;
                            btnViewOkOk.CommandArgument = OK_FORCE_CLOSE;
                        }
                        else if (keys[0].Equals("deny"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_DenyProviderAccess").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_PatientInvitation_DeniedAccess").ToString();
                            mvViews.ActiveViewIndex = 1;
                        }
                        else if (keys[0].Equals("remindLater"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_RemindMeLater").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_PatientInvitation_RemindLater").ToString();
                            mvViews.ActiveViewIndex = 1;
                        }
                        else if (keys[0].Equals("delete") && keys.Length > 1)
                        {
                            // keys = delete | providerID | providerFullName
                            string providerName = (keys.Length > 2) ? keys[2] : "unknown";
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_DeleteProvider_Title").ToString();
                            this.litViewConfirmDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_DeleteProvider_Description").ToString() +
                                                                  " " + providerName;

                            this.btnViewConfirmOk.CommandArgument = mEditKeys;    // put the providerId here so we can use it on button click
                            mvViews.ActiveViewIndex = 2;


                        }
                        else if (keys[0].Equals("error"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_Error").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_GenericErrorDescription").ToString();
                            mvViews.ActiveViewIndex = 1;
                        }
                    }
                }
            }
        }

        private void LoadSearchResults(string[] _keys)
        {
            string firstname = string.Empty;
            string lastname = string.Empty;
            string orgname = string.Empty;
            string phonenb = string.Empty;
            string city = string.Empty;
            int?   stateId = null ;
            string zip = string.Empty ;
            string email = string.Empty ;

            for (int i = 1; i < _keys.Length; i++)
            {
                string[] kvpair = _keys[i].Split('=');

                if (kvpair.Length == 2)
                {
                    if (kvpair[0].Equals("fn"))
                        firstname = kvpair[1];
                    else if (kvpair[0].Equals("ln"))
                        lastname = kvpair[1];
                    else if (kvpair[0].Equals("on"))
                        orgname = kvpair[1];
                    else if (kvpair[0].Equals("cy"))
                        city = kvpair[1];
                    else if (kvpair[0].Equals("st"))
                        stateId = Convert.ToInt32(kvpair[1]);
                    else if (kvpair[0].Equals("zp"))
                        zip = kvpair[1];
                    else if (kvpair[0].Equals("ph"))
                        phonenb = kvpair[1];
                    else if (kvpair[0].Equals("em"))
                        email = kvpair[1];
                }
            }

            List<ProviderDetails> listProviders = ProviderDetails.SearchProvider( firstname, lastname, orgname,
                                                                                    phonenb, city, stateId, 
                                                                                    H360Utility.GetCurrentLanguageID(), zip, email  ) ;

            if (listProviders.Count > 0)
            {
                repeaterSearchResults.DataSource = listProviders;
                repeaterSearchResults.DataBind();
            }
            else
            {
                repeaterSearchResults.Visible = false;
            }

        }

        protected void OnProviderDetailsDataBind(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ProviderDetails objProviderDetails = e.Item.DataItem as ProviderDetails;

                Literal litProviderName = e.Item.FindControl("litProviderName") as Literal;
                Literal litProviderOrg = e.Item.FindControl("litProviderOrg") as Literal;
                Literal litProviderAddr = e.Item.FindControl("litProviderAddr") as Literal;
                Literal litProviderCityStateZip = e.Item.FindControl("litProviderCityStateZip") as Literal;
                Button  btnConnect = e.Item.FindControl("btnConnect") as Button;
                Label   lblAlreadyConnected = e.Item.FindControl("lblAlreadyConnected") as Label;


                if (litProviderName != null)
                    litProviderName.Text = objProviderDetails.FormattedName;
                if (litProviderOrg != null)
                    litProviderOrg.Text = objProviderDetails.PracticeName;
                if (litProviderAddr != null)
                    litProviderAddr.Text = objProviderDetails.PracticeAddress;
                if (litProviderCityStateZip != null)
                    litProviderCityStateZip.Text = objProviderDetails.City + " " + objProviderDetails.StateName + " " + objProviderDetails.Zip;

                if (ProviderDetails.IsValidPatientForProvider( this.GetPatient().UserHealthRecordID, objProviderDetails.ProviderID))
                {
                    // they are already connected
                    btnConnect.Visible = false;
                }
                else
                {
                    lblAlreadyConnected.Visible = false;
                    btnConnect.CommandArgument = objProviderDetails.ProviderID.ToString();
                }
            }
        }

        protected void OnConnectClick(object sender, EventArgs e)
        {
            Button                  btn = sender as Button;
            int                     provID = Convert.ToInt32(btn.CommandArgument);
            AHAHelpContent.Patient  pat = GetPatient();

            // If the patient is not already in the Providers group
            if (!ProviderDetails.IsValidPatientForProvider(pat.UserHealthRecordID, provID))
            {
                GRCBase.DBContext dbContext = null;
                bool bException = false;
                try
                {
                    dbContext = GRCBase.DBContext.GetDBContext();

                    PatientProviderInvitation objInvitation = PatientProviderInvitation.CreateProviderInvitation(
                                CurrentUser.FirstName,
                                CurrentUser.LastName,
                                string.Empty,
                                pat.UserHealthRecordID,
                                false);

                    PatientProviderInvitationDetails objAvailableDetails = objInvitation.PendingInvitationDetails.First();

                    objAvailableDetails.AcceptedProviderID = provID;

                    //Marks the existing invitations, sent by this patient to the current provider, as blocked
                    //cannot user objInvitation.PatientID.Value as patient id is null because whilr creating an invitation patient id is passed as null
                    PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(pat.UserHealthRecordID, provID, false);

                    objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Pending;

                    objAvailableDetails.Update();
                }
                catch
                {
                    bException = true;
                    // throw;
                }
                finally
                {
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        // throw;
                    }
                }

                // Now display another View of popup
                if (!bException)
                {
                    // display congrats
                    this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Congrats").ToString();
                    this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_Invitation_Sent").ToString();
                    mvViews.ActiveViewIndex = 1;
                    btnViewOkOk.CommandArgument = OK_FORCE_CLOSE;   // need the tabs to go back to Terms and Conditions
                }
                else
                {
                    // error screen
                    this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_Error").ToString();
                    this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_GenericErrorDescription").ToString();
                    mvViews.ActiveViewIndex = 1;
                    btnViewOkOk.CommandArgument = OK_NORMAL_HIDE;
                }
                
            }
            else
            {
                // What to do here ??

            }
            

        }

            //
            // This is from btnViewConfirmOk
        protected void btnOk_Click(object sender, EventArgs e)
        {
            bool closeMe = true;

            // If we are successful, we have to show another view and when that view closes,
            // we need to signal the main page to update. We use btnViewConfirmCancel.CommandArgument
            // to specify that
            btnViewOkOk.CommandArgument = OK_NORMAL_HIDE;    // no main page update

            try
            {
                Button btn = (Button)sender;    // always btnViewConfirmOk
                if( !string.IsNullOrEmpty( btn.CommandArgument ) )
                {
                    string[]                        keys = btn.CommandArgument.Split('|');
                    int                             providerID = Convert.ToInt32(keys[1]);
                    AHAHelpContent.ProviderDetails  obj = AHAHelpContent.ProviderDetails.FindByProviderID( providerID );
                    string                          providerName = (keys.Length > 2) ? keys[2] : "<Unknown>";

                    if (obj != null)
                    {
                        AHAHelpContent.ProviderDetails.DisconnectPatientOrProvider(obj.ProviderID, AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value);
                        EmailUtils.SendDisconnectProviderEmail(obj.FormattedName, obj.Email, CurrentUser.FullName, obj.LanguageLocale);

                        this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_DeleteProviderSuccess_Title").ToString();
                        this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_DeleteProviderSuccess_Description").ToString() +
                                                            providerName ;
                        mvViews.ActiveViewIndex = 1;
                        btnViewOkOk.CommandArgument = OK_FORCE_CLOSE;
                    }
                    else
                    {
                        this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_Error").ToString();
                        this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_GenericErrorDescription").ToString();
                        mvViews.ActiveViewIndex = 1;
                    }
                    closeMe = false;
                }
            }
            catch (Exception ee)
            {
                closeMe = false;
                this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_Error").ToString();
                this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_GenericErrorDescription").ToString();
                mvViews.ActiveViewIndex = 1;
            }

            if (closeMe)
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            if (string.IsNullOrEmpty(btn.CommandArgument))
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).CancelPopup();
            }
            else
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }
        }
    }
}