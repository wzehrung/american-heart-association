﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConnectionsInvitation.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsInvitation" %>

<div id="divInvitation" runat="server" class="printinvite">
    <p> </p>
    <b>
        <asp:Literal ID="litToName" runat="server" /></b>,
        <asp:Literal ID="litPrintInviteMessage" runat="server"></asp:Literal>
    <p>
        <b><asp:Literal ID="litUrl" runat="server" /></b>
    </p>
    <p>
        <%=GetGlobalResourceObject("ConnCenterPAndV", "Text_InvitationCode")%>:&nbsp;<b><asp:Literal ID="litInvitationCode" runat="server" /></b>
    </p>
</div>
