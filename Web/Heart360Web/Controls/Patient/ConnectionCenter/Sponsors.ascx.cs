﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAHelpContent;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class Sponsors : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _BindRepeater();
            }
        }

        private void _BindRepeater()
        {
            rptSponsors.DataSource = AHAHelpContent.GrantSupport.GetAllProviderGrantSupport(AHAAPI.H360Utility.GetCurrentLanguageID());
            rptSponsors.DataBind();
        }

        protected void rptSponsors_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AHAHelpContent.GrantSupport objItem = e.Item.DataItem as AHAHelpContent.GrantSupport;
                Image grantLogo = e.Item.FindControl("grantLogo") as Image;
                Literal litDesc = e.Item.FindControl("litDesc") as Literal;

                grantLogo.ImageUrl = string.Format("/ImageHandler.ashx?GID={0}&Version={1}", objItem.GrantSupportID, objItem.Version);
                litDesc.Text = UIHelper.RenderLink(objItem.Description);
            }
        }
    }
}