﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using AHAHelpContent;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class RemindMePopup : System.Web.UI.UserControl
    {
        // sometimes with a popup that only has an OK button, we need to force a refresh back on the main page.
        // we do this by setting the CommandArgument of the btnViewOkOk to OK_FORCE_CLOSE
        private string OK_NORMAL_HIDE = string.Empty;
        private string OK_FORCE_CLOSE = "close";

       private string  mEditKeys;               // one or more pipe separated values representing the "keys" to what needs to be done.

        public RemindMePopup()
            : base()
        {
            mEditKeys = string.Empty;
        }

        public string EditKeys
        {
            set { mEditKeys = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(mEditKeys))
                {
                    string[] keys = mEditKeys.Split('|');

                    this.btnViewConfirmOk.CommandArgument = OK_NORMAL_HIDE;
                    this.btnViewOkOk.CommandArgument = OK_NORMAL_HIDE;

                    if (keys.Length > 0)
                    {
                        if (keys[0].Equals("setupsms"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Congrats").ToString();
                            this.litSmsSetupDescription.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_Congratulations").ToString();
                            mvViews.ActiveViewIndex = 2;
                        }
                        else if (keys[0].Equals("deletesms") )
                        {
                            if( keys.Length > 2)
                            {
                                // keys = deletesms | country-code | phone number
                                string providerName = (keys.Length > 2) ? keys[2] : "unknown";
                                this.litTitle.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_Note").ToString();
                                this.litViewConfirmDescription.Text = GetGlobalResourceObject("RemindMe", "SMS_Text_Note").ToString() ;
                                this.btnViewConfirmOk.CommandArgument = mEditKeys;    // put the providerId here so we can use it on button click
                                mvViews.ActiveViewIndex = 1;
                            }
                        }
                        else if (keys[0].Equals("congrats") )
                        {
                            if( keys.Length > 1)
                            {
                                this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Congrats").ToString();
                                this.litViewOkDescription.Text = keys[1];
                                mvViews.ActiveViewIndex = 0;
                            }
                        }
                        else if (keys[0].Equals("deletereminder") )
                        {
                            if( keys.Length > 2)
                            {
                                this.litTitle.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_PleaseConfirm").ToString();
                                this.litViewConfirmDescription.Text = GetGlobalResourceObject("RemindMe", "Message_Delete_Confirm").ToString() + " " + keys[2] ;
                                this.btnViewConfirmOk.CommandArgument = mEditKeys;    // put the Id here so we can use it on button click
                                mvViews.ActiveViewIndex = 1;
                            }
                        }
                        else if( keys[0].Equals("setupivr") )
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Congrats").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("RemindMe", "IVR_Message_PINSaved").ToString();
                            mvViews.ActiveViewIndex = 0;
                        }
                        else if (keys[0].Equals("enableivr"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Title_Congrats").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("RemindMe", "IVR_Text_Success").ToString();
                            mvViews.ActiveViewIndex = 0;
                        }
                        else if (keys[0].Equals("disableivr"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_Note").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("RemindMe", "IVR_Text_Disabled").ToString();
                            mvViews.ActiveViewIndex = 0;
                        }

/******************
                        else if (keys[0].Equals("error"))
                        {
                            this.litTitle.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_Error").ToString();
                            this.litViewOkDescription.Text = GetGlobalResourceObject("ConnCenterPAndV", "Text_GenericErrorDescription").ToString();
                            mvViews.ActiveViewIndex = 0;
                        }
   **********/
                    }
                }
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            if (string.IsNullOrEmpty(btn.CommandArgument))
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).CancelPopup();
            }
            else
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Button  btn = (Button)sender;
            string  errStr = " Invalid parameters.";
            bool    successful = false;
                
            if (!string.IsNullOrEmpty(btn.CommandArgument))
            {
                string[] keys = btn.CommandArgument.Split('|');

                if (keys.Length == 3)
                {
                    if (keys[0].Equals("deletesms"))
                    {
                        try
                        {
                            string sTimeZone = string.Empty;
                            UserIdentityContext objContext = SessionInfo.GetUserIdentityContext();

                            AHAHelpContent.Patient patient = AHAHelpContent.Patient.FindByUserHealthRecordID(objContext.PatientID.Value);
                            // This will delete the SMS number AND the mobile number in the profile page
                            AHAHelpContent.PhoneNumbers.CreateOrChangeSMSNumber(patient.UserHealthRecordID, keys[1], keys[2], "0000", sTimeZone, "true");

                            // Let the popup handle how we close
                            successful = true;
                            ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
                        }
                        catch (Exception ex)
                        {
                            errStr = " " + ex.ToString();
                        }
                    }
                    else if (keys[0].Equals("deletereminder"))
                    {
                        try
                        {
                            PatientReminderSchedule.DeleteReminderSchedule(Convert.ToInt32(keys[1]));

                            // Let the popup handle how we close
                            successful = true;
                            ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
                        }
                        catch (Exception ex)
                        {
                            errStr = " " + ex.ToString();
                        }


                    }
                }
                if (!successful)
                {
                    this.litTitle.Text = GetGlobalResourceObject("RemindMe", "Title_Error").ToString();
                    this.litSmsSetupDescription.Text = GetGlobalResourceObject("RemindMe", "Description_Error").ToString() + errStr ;
                    mvViews.ActiveViewIndex = 2;
                }
            }

        }

        protected void btnSmsSetup_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            if (string.IsNullOrEmpty(btn.CommandArgument))
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).CancelPopup();
            }
            else
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }


        }
    }
}