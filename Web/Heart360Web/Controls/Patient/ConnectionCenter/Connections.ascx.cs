﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;
using AHAAPI;
using System.Text.RegularExpressions;
using AHAHelpContent;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class Connections : System.Web.UI.UserControl
    {
            // these are constants to be used as values for hidTabState
        private const int TABSTATE_ERROR = -2;
        private const int TABSTATE_COMPLETED = -1;
        private const int TABSTATE_START = 0;
        private const int TABSTATE_ACCEPTED = 1;
        private const int TABSTATE_INVITING = 2;

            // these are constants to be used as values for hidPendingState
        private const int PENDINGSTATE_VALIDATION_ERROR = -1;
        private const int PENDINGSTATE_DEFAULT = 0;

        private bool mArePending;       // this is used internally to flag whether or not to display the 'Remind Me Later' button in Pending list

        public UpdatePanel GetUpdatePanel()
        {
            return this.upanelInvite;
        }

        private AHAHelpContent.Patient GetPatient()
        {
            return AHAHelpContent.Patient.FindByUserHealthRecordGUID((Page as H360PageHelper).RecordGUID);
            /**
            Heart360Web.H360PageHelper ph = (Heart360Web.H360PageHelper)this.Page;
            Guid hrGUID = ph.RecordGUID;
            return AHAHelpContent.Patient.FindByUserHealthRecordGUID(hrGUID);
            **/
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // hidTabState.Value = TABSTATE_START.ToString() ;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidTabState.Value = TABSTATE_START.ToString();
                hidPendingState.Value = PENDINGSTATE_DEFAULT.ToString();
                LoadStateDDL();
                LoadSalutationDDLs();
            }

        }

        private void LoadStateDDL()
        {
            List<AHAHelpContent.ListItem> objStateList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.States, H360Utility.GetCurrentLanguageID());
            //objStateList.Insert(0, new AHAHelpContent.ListItem { ListItemName = GetGlobalResourceObject("Common", "Text_SelectOne").ToString(), ListItemID = 0 });
            objStateList.Insert(0, new AHAHelpContent.ListItem { ListItemName = "State", ListItemID = 0 });

            ddlState.DataSource = objStateList;
            ddlState.DataTextField = "ListItemName";
            ddlState.DataValueField = "ListItemID";
            ddlState.DataBind();
        }

        private void LoadSalutationDDLs()
        {
            ListItemCollection objSalutationList = new ListItemCollection();
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem("Salutation", ""));
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem("Dr.", "Dr"));
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem("MD.", "MD"));
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem("RN", "RN"));
/**
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject("Common", "Text_SelectOne").ToString(), ""));
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject("Common", "Text_Dr").ToString(), "Dr"));
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject("Common", "Text_MD").ToString(), "MD"));
            objSalutationList.Add(new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject("Common", "Text_RN").ToString(), "RN"));
**/
            ddlEmailSalutation.DataSource = objSalutationList;
            ddlEmailSalutation.DataTextField = "Text";
            ddlEmailSalutation.DataValueField = "Value";
            ddlEmailSalutation.DataBind();

            ddlPrintSalutation.DataSource = objSalutationList;
            ddlPrintSalutation.DataTextField = "Text";
            ddlPrintSalutation.DataValueField = "Value";
            ddlPrintSalutation.DataBind();
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            //
            // Everything dealing with the list of P&V
            //

            int     pendingState = Convert.ToInt32( hidPendingState.Value ) ;

            if (!Page.IsPostBack || pendingState == PENDINGSTATE_DEFAULT)
            {
                List<ProviderDetails> listProviders = AHAHelpContent.ProviderDetails.FindProviderByPatientID(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value);

                repeaterProviders.DataSource = listProviders;
                repeaterProviders.DataBind();


                // V5 had different lists for Pending and Remind Me Later. In V6 we will combine them
                List<PatientProviderInvitation> objInvitationList = PatientProviderInvitation.FindPendingInvitationsByAcceptedPatient(
                                                                            AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value,
                                                                            PatientProviderInvitationDetails.InvitationStatus.Pending);

                if (objInvitationList.Count > 0)
                {
                    mArePending = true;     // used during data binding
                    repeaterPending.DataSource = objInvitationList;
                    repeaterPending.DataBind();
                }
                else
                {
                    List<PatientProviderInvitation> objRemindList = PatientProviderInvitation.FindPendingInvitationsByAcceptedPatient(
                                                                            AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value,
                                                                            PatientProviderInvitationDetails.InvitationStatus.RemindLater);

                    mArePending = false;    // used during data binding
                    repeaterPending.DataSource = objRemindList;
                    repeaterPending.DataBind();
                }
            }

            //
            // Everything dealing with Invite a Provider or Volunteer
            //

            // TODO: put this somewhere else once we construct the contents of each tab individually by which one is current
            string strTermsOfUse = GetGlobalResourceObject("ConnCenterPAndV", "Patient_Invitation_TermsOfUse").ToString();
            strTermsOfUse = Regex.Replace(strTermsOfUse, "##Heart360Trademark##", AHAAPI.H360Utility.GetTrademarkedHeart360());
            strTermsOfUse = Regex.Replace(strTermsOfUse, "##RegisteredMicrosoft##", AHAAPI.H360Utility.GetRegisteredMicrosoft());
            strTermsOfUse = Regex.Replace(strTermsOfUse, "##TrademarkedHealthvault##", AHAAPI.H360Utility.GetTrademarkedHealthVault());
            litTermsOfUse.Text = strTermsOfUse;

            int     tabstate = Convert.ToInt32( this.hidTabState.Value ) ;

            switch( tabstate )
            {
                case TABSTATE_ERROR:
                    this.hidTabState.Value = TABSTATE_INVITING.ToString() ;
                    break ;

                case TABSTATE_COMPLETED:
                    this.hidTabState.Value = TABSTATE_START.ToString() ;   // for next time :-)
                    tabpTerms.Enabled = true;
                    tabpByName.Enabled = tabpByCode.Enabled = tabpByEmail.Enabled = tabpByPrint.Enabled = false;
                    tabcInvite.ActiveTabIndex = 0;
                    break ;

                case TABSTATE_START:    // Terms and Conditions tab
                    tabpTerms.Enabled = true;
                    tabpByName.Enabled = tabpByCode.Enabled = tabpByEmail.Enabled = tabpByPrint.Enabled = false;
                    tabcInvite.ActiveTabIndex = 0;
                    break ;

                case TABSTATE_INVITING:
                case TABSTATE_ACCEPTED:
                    tabpTerms.Enabled = false;
                    tabpByName.Enabled = true;
                    tabpByCode.Enabled = true;
                    tabpByEmail.Enabled = true;
                    tabpByPrint.Enabled = true;

                    if (tabstate == TABSTATE_ACCEPTED) // Terms and Conditions were accepted
                    {
                        tabcInvite.ActiveTabIndex = 1;      // default to BY NAME
                        hidTabState.Value = TABSTATE_INVITING.ToString() ;
                    }
                    break;

                default:
                    break;
            }
        }


        protected void OnProvidersDataBind(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal litName = e.Item.FindControl("litProviderAcceptedName") as Literal;
                Literal litPhone = e.Item.FindControl("litProviderAcceptedPhone") as Literal ;
                Literal litSpecialty = e.Item.FindControl("litProviderAcceptedSpecialty") as Literal;
                Literal litPharmaPartner = e.Item.FindControl("litProviderAcceptedPharmaPartner") as Literal;
                Literal litOrgName = e.Item.FindControl("litProviderAcceptedOrgName") as Literal;
                Literal litContactPhone = e.Item.FindControl("litProviderAcceptedContactPhone") as Literal;
                Literal litContactFax = e.Item.FindControl("litProviderAcceptedContactFax") as Literal;
                Literal litOrgAddr = e.Item.FindControl("litProviderAcceptedOrgAddr") as Literal;
                Literal litEmail = e.Item.FindControl("litProviderAcceptedEmail") as Literal;
                Literal litWebsite = e.Item.FindControl("litProviderAcceptedWebsite") as Literal;
                Literal litCity = e.Item.FindControl("litProviderAcceptedCity") as Literal;
                Literal litState = e.Item.FindControl("litProviderAcceptedState") as Literal;
                Literal litZip = e.Item.FindControl("litProviderAcceptedZip") as Literal;
                Literal litMessage = e.Item.FindControl("litProviderMessage") as Literal;


                ProviderDetails objProvider = e.Item.DataItem as ProviderDetails;

                //litName.Text = string.Format("<a href=\"{0}?id={1}\">{2}</a>", AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_PROVIDER, GRCBase.BlowFish.EncryptString(objProvider.ProviderID.ToString()), objProvider.FormattedName);
                litName.Text = objProvider.FormattedName;
                if( !string.IsNullOrEmpty(objProvider.PracticePhone) )
                {
                    litPhone.Text = litContactPhone.Text = GRCBase.PhoneHelper.GetFormattedUSPhoneNumber( objProvider.PracticePhone ) ;
                }
                    
                litSpecialty.Text = objProvider.Speciality;
                litPharmaPartner.Text = objProvider.PharmacyPartner;
                litOrgName.Text = objProvider.PracticeName;
                if (!string.IsNullOrEmpty(objProvider.PracticeFax))
                    litContactFax.Text = GRCBase.PhoneHelper.GetFormattedUSPhoneNumber(objProvider.PracticeFax);
                litOrgAddr.Text = objProvider.PracticeAddress;
                litEmail.Text = objProvider.PracticeEmail;
                litWebsite.Text = objProvider.PracticeUrl;
                litCity.Text = objProvider.City;
                litState.Text = AHAHelpContent.ListItem.FindByListItemID(objProvider.StateID, H360Utility.GetCurrentLanguageID()).ListItemName;
                litZip.Text = objProvider.Zip;

                litMessage.Text = "<textarea style='resize:none;' readonly=\"readonly\">" + objProvider.ProfileMessage + "</textarea>" ;

                //
                // Hook up Actions
                //

                ImageButton actDelete = e.Item.FindControl("actProviderAcceptedTrash") as ImageButton;
                string itemKey = "delete|" + objProvider.ProviderID.ToString() + "|" + objProvider.FormattedName ;

                if (actDelete != null)
                {
                    // What does delete require??     delete requires both ThingID and VersionStamp
                    //itemKey += "," + item.VersionStamp.ToString();
                    string trashUrl = "PopUp.aspx?delete=2&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER).ToString() + "&elid=" + itemKey;
                    actDelete.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER);
                }
            }
        }

        protected void OnPendingDataBind(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // this is taken from AHAWeb\UserCtrls\Patient\RightRail.ascx.cs
                PatientProviderInvitation objPPI = e.Item.DataItem as PatientProviderInvitation;

                // PJB: 7/23/2014 somewhere in here has been throwing an error, probably with corrupt data.
                // Needed to add some defensive programming.
                AHAHelpContent.ProviderDetails provDet = (objPPI.ProviderID.HasValue) ? AHAHelpContent.ProviderDetails.FindByProviderID( objPPI.ProviderID.Value ) : null ;

                if (provDet != null)
                {
                    Literal litName = e.Item.FindControl("litProviderPendingName") as Literal;
                    Literal litPhone = e.Item.FindControl("litProviderPendingPhone") as Literal;

                    litName.Text = provDet.FormattedName;
                    if (!string.IsNullOrEmpty(provDet.PracticePhone))
                    {
                        litPhone.Text = GRCBase.PhoneHelper.GetFormattedUSPhoneNumber(provDet.PracticePhone);
                    }

                    //
                    // Hook up actions
                    //
                    Button btnGrant = e.Item.FindControl("btnProviderPendingGrantAccess") as Button;
                    Button btnDeny = e.Item.FindControl("btnProviderPendingNoAccess") as Button;
                    Button btnLater = e.Item.FindControl("btnProviderPendingRemindMeLater") as Button;

                    if (!mArePending)
                        btnLater.Visible = false;

                    PatientProviderInvitationDetails objAvailableDetails = objPPI.PendingInvitationDetails.First(); // CurrentInvitation.PendingInvitationDetails.First();

                    btnGrant.CommandArgument = btnDeny.CommandArgument = btnLater.CommandArgument = objAvailableDetails.InvitationDetailsID.ToString(); // objPPI.InvitationCode;


                    //
                    // Hook up validation
                    //

                    ValidationSummary vs = e.Item.FindControl("valSummaryNewTerms") as ValidationSummary;
                    CustomValidator cv = e.Item.FindControl("validNewTermsFields") as CustomValidator;

                    if (vs != null && cv != null)
                    {
                        string valgroup = "prov" + e.Item.ItemIndex.ToString();
                        vs.ValidationGroup = valgroup;
                        cv.ValidationGroup = valgroup;
                    }
                }
            }

        }

        protected void ValidateNewTermsFields(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;

            CheckBox cb = valid.NamingContainer.FindControl("ckbProviderPendingAgree") as CheckBox;

            if( cb != null && cb.Checked )
            {
                v.IsValid = true;
            }
            else
            {
                v.IsValid = false;
                valid.ErrorMessage = "Please check the checkbox if you wish to proceed";
            }
        }

        protected void btnGrantAccess_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            // Do validation first

            ValidationSummary vs = btn.NamingContainer.FindControl("valSummaryNewTerms") as ValidationSummary;

            if (vs != null)
            {
                Page.Validate(vs.ValidationGroup);
                if (!Page.IsValid)
                {
                    hidPendingState.Value = PENDINGSTATE_VALIDATION_ERROR.ToString();
                    return;
                }
            }
            hidPendingState.Value = PENDINGSTATE_DEFAULT.ToString();

            // this is taken from V5 AHAWeb\Patient\Invitation\AcceptInvitation.aspx.cs
            string sInvitationCode = btn.CommandArgument ;
            int iInvitationCode = Convert.ToInt32(sInvitationCode);

            PatientProviderInvitationDetails ppid = AHAHelpContent.PatientProviderInvitationDetails.FindByInvitationDetailsID(iInvitationCode);
            if (ppid != null ) // && ppid.Status == AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Pending)
            {
                AHAHelpContent.Patient      pat = GetPatient() ;
                PatientProviderInvitation   ppi = AHAHelpContent.PatientProviderInvitation.FindByInvitationID( ppid.InvitationID );
                ProviderDetails             objProvider = ProviderDetails.FindByProviderID(ppi.ProviderID.Value);
             
                ppid.AcceptedPatientID = pat.UserHealthRecordID;
                ppid.Update();

                    // all this taken from _GrandAccess() method of AcceptInvitation.aspx.cs
                bool showSuccessPopup = false;

                // Check to see if they are already connected
                if (!ProviderDetails.IsValidPatientForProvider( pat.UserHealthRecordID,  ppi.ProviderID.Value) )
                {
                    // they are not already connected.
                    //update personguid for that healthrecord
                    UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
                    AHAHelpContent.Patient.UpdateOfflinePersonGuid(userIdentityContext.PersonalItemGUID, userIdentityContext.RecordId, userIdentityContext.UserId);

                    if (ppid.Status == PatientProviderInvitationDetails.InvitationStatus.ProviderCodeOnly)
                    {
                        GRCBase.DBContext dbContext = null;
                        bool bException = false;
                        try
                        {
                            dbContext = GRCBase.DBContext.GetDBContext();

                            //Mark all pending invitations b/w these entities as blocked
                            PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked( pat.UserHealthRecordID, ppi.ProviderID.Value, null);

                            PatientProviderInvitation objInvitation = PatientProviderInvitation.CreateProviderInvitation(
                                        CurrentUser.FirstName,
                                        CurrentUser.LastName,
                                        string.Empty,
                                        pat.UserHealthRecordID,
                                        false);

                            PatientProviderInvitationDetails objAvailableDetails = objInvitation.PendingInvitationDetails.First();

                            objAvailableDetails.AcceptedProviderID = ppi.ProviderID.Value;
                            objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Pending;
                            objAvailableDetails.Update();
            
                            showSuccessPopup = true;    //_SwitchView(View.ConnectSuccessProviderCodeOnly);         
                        }
                        catch
                        {
                            bException = true;
                            //throw;    // PJB commented this out
                        }
                        finally
                        {
                            try
                            {
                                if (bException)
                                {
                                    if (dbContext != null)
                                    {
                                        dbContext.ReleaseDBContext(false);
                                    }
                                }
                                else
                                {
                                    dbContext.ReleaseDBContext(true);
                                }
                            }
                            catch
                            {
                                if (dbContext != null)
                                {
                                    dbContext.ReleaseDBContext(false);
                                }
                                // throw;   // PJB commented this out
                            }
                        }
                    }
                    else
                    {
                        ConnectPatientAndProvider( ppi );
                        AssociatePatientToCampaign( ppi );
                        MarkInvitationAccepted( ppi, ppid );

                        EmailUtils.SendInvitationConfirmationToProvider(objProvider, CurrentUser.FirstName, CurrentUser.Email, objProvider.LanguageLocale);

                        showSuccessPopup = true ;       //_SwitchView(View.Confirmation);
                    }

                    // AHAAPI.SessionInfo.ResetPatientInvitationSession();
                }
                else
                {
                    MarkInvitationAccepted( ppi, ppid ) ;

                    // they were already connected, but show success anyway
                    showSuccessPopup = true ;   //_SwitchView(View.Invalid);
                }

                // either a success or error popup
                string popupKeys = (showSuccessPopup) ? ("grant|" + objProvider.FormattedName) : "error" ;
                DisplayPopup(popupKeys);
                
            }
            else
            {
                // somehow the invitation was not pending.....
                DisplayPopup( "error" ) ;
            }
        }

        protected void btnNoAccess_Click(object sender, EventArgs e)
        {
            Button  btn = (Button)sender ;
            string  sInvitationCode = btn.CommandArgument ;
            int     iInvitationCode = Convert.ToInt32(sInvitationCode);

            PatientProviderInvitationDetails ppid = AHAHelpContent.PatientProviderInvitationDetails.FindByInvitationDetailsID(iInvitationCode);

            if (ppid != null)
            {
                ppid.Status = PatientProviderInvitationDetails.InvitationStatus.Blocked;
                ppid.Update();

                DisplayPopup("deny");
            }
            else
            {
                DisplayPopup("error");
            }

            hidPendingState.Value = PENDINGSTATE_DEFAULT.ToString();
        }

        protected void btnRemindMeLater_Click(object sender, EventArgs e)
        {
            Button  btn = (Button)sender ;
            string  sInvitationCode = btn.CommandArgument ;
            int     iInvitationCode = Convert.ToInt32(sInvitationCode);

            PatientProviderInvitationDetails ppid = AHAHelpContent.PatientProviderInvitationDetails.FindByInvitationDetailsID(iInvitationCode);

            if (ppid != null)
            {
                ppid.Status = PatientProviderInvitationDetails.InvitationStatus.RemindLater;
                ppid.Update();

                DisplayPopup("remindLater");
            }
            else
            {
                DisplayPopup("error");
            }
            hidPendingState.Value = PENDINGSTATE_DEFAULT.ToString();
        }

        private void ConnectPatientAndProvider( PatientProviderInvitation _currInvitation )
        {
            List<int> patientlist = new List<int>();
            patientlist.Add( GetPatient().UserHealthRecordID);

            PatientGroup.AddPatientToGroup(patientlist, null, _currInvitation.ProviderID.Value);
        }

        private void MarkInvitationAccepted( PatientProviderInvitation _currInvitation, PatientProviderInvitationDetails _currInvitationDetails )
        {
            _currInvitationDetails.DateAccepted = DateTime.Now;
            _currInvitationDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Accepted;
            _currInvitationDetails.Update();

            //Mark all pending invitations b/w these entities as blocked
            PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(GetPatient().UserHealthRecordID, _currInvitation.ProviderID.Value, null);
        }

        private void AssociatePatientToCampaign(PatientProviderInvitation _currInvitation )
        {
            AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByProviderID( _currInvitation.ProviderID.Value, AHAAPI.H360Utility.GetCurrentLanguageID());

            if (objCampaign == null)
            {
            }
            else
            {
                HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;
                //System.Guid? objSelfPersonalItemGUID = null;

                UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
                System.Guid? objPersonalItemGUID = userIdentityContext.PersonalItemGUID;

                AHAHelpContent.Campaign.UpdateCampaignForPatient(userIdentityContext.PersonalItemGUID, objCampaign.CampaignID);
            }

        }

        private void DisplayPopup(string _keys)
        {
            string editUrl = "PopUp.aspx?connect=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER).ToString() + "&elid=" + _keys;
            string script = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER);

            // Script Manager doesn't want the "return" in the script.
            string script2 = script.Substring(6);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "SHOW POPUP", script2, true); 
        }

        protected void ckbAgree_Changed(object sender, EventArgs e)
        {
            btnTermsContinue.Enabled = ckbAgree.Checked;
        }

        protected void btnTermsContinue_Click(object sender, EventArgs e)
        {
            Page.Validate("Terms");
            if (!Page.IsValid)
                return;

            this.hidTabState.Value = TABSTATE_ACCEPTED.ToString();  // "1";
        }

        protected void btnByNameSearch_Click(object sender, EventArgs e)
        {
            Page.Validate("ByName");
            if (!Page.IsValid)
            {
                this.hidTabState.Value = TABSTATE_ERROR.ToString();
                return;
            }

            //
            // Before we send this to the popup, let's make sure we have at least one result
            //


            //
            // Now ship search to popup
            //
            int?                    stateId = null;   if (ddlState.SelectedIndex > 0) stateId = Convert.ToInt32(ddlState.SelectedValue);
            List<ProviderDetails>   listProviders = ProviderDetails.SearchProvider( tboxFirstName.Text, 
                                                                                  tboxLastName.Text, 
                                                                                  tboxOrgName.Text,
                                                                                  tboxPhone.Text,
                                                                                  tboxCity.Text, 
                                                                                  stateId,
                                                                                  H360Utility.GetCurrentLanguageID(),
                                                                                  tboxZip.Text,
                                                                                  tboxEmail.Text);

            if (listProviders.Count > 0)    // found something
            {
                string keys = "connect";
                if (!string.IsNullOrEmpty(tboxFirstName.Text))
                    keys += ("|" + "fn=" + tboxFirstName.Text);
                if (!string.IsNullOrEmpty(tboxLastName.Text))
                    keys += ("|" + "ln=" + tboxLastName.Text);
                if (!string.IsNullOrEmpty(tboxOrgName.Text))
                    keys += ("|" + "on=" + tboxOrgName.Text);
                if (!string.IsNullOrEmpty(tboxCity.Text))
                    keys += ("|" + "cy=" + tboxCity.Text);
                if (ddlState.SelectedIndex > 0)
                    keys += ("|" + "st=" + ddlState.SelectedValue);
                if (!string.IsNullOrEmpty(tboxZip.Text))
                    keys += ("|" + "zp=" + tboxZip.Text);
                if (!string.IsNullOrEmpty(tboxPhone.Text))
                    keys += ("|" + "ph=" + tboxPhone.Text);
                if (!string.IsNullOrEmpty(tboxEmail.Text))
                    keys += ("|" + "em=" + tboxEmail.Text);

                //
                // We don't know if they will select one of the found providers,
                // but we have to force another terms and conditions.
                //
                InvitationCompleted();

                this.DisplayPopup(keys);
            }
            else
            {
                this.DisplayPopup("connectEmpty");
            }


        }

        protected void btnByNameClear_Click(object sender, EventArgs e)
        {
            tboxFirstName.Text = String.Empty;
            tboxLastName.Text = String.Empty;
            tboxOrgName.Text = String.Empty;
            tboxCity.Text = String.Empty;
            ddlState.SelectedIndex = 0;
//            ddlState.SelectedValue = "0";
//            ddlState.Text = GetGlobalResourceObject("Common", "Text_SelectOne").ToString();
            tboxZip.Text = String.Empty;
            tboxPhone.Text = String.Empty;
            tboxEmail.Text = String.Empty;

            hidTabState.Value = "2";
        }

        protected void ValidateByNameZip(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(tboxZip.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = (GetGlobalResourceObject("Tracker", "Validation_Text_ZipCode").ToString());

                return;
            }
        }

        protected void ValidateByNameSearchFields( object sender, ServerValidateEventArgs v )
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = false;

            //
            // Make sure that at least one search field has something
            //
            if (!string.IsNullOrEmpty(tboxFirstName.Text) && !string.IsNullOrEmpty(tboxFirstName.Text.Trim()))
            {
                v.IsValid = true;
            }
            if (!string.IsNullOrEmpty(tboxLastName.Text) && !string.IsNullOrEmpty(tboxLastName.Text.Trim()))
            {
                v.IsValid = true;
            }
            if (!string.IsNullOrEmpty(tboxOrgName.Text) && !string.IsNullOrEmpty(tboxOrgName.Text.Trim()))
            {
                v.IsValid = true;
            }
            if (!string.IsNullOrEmpty(tboxCity.Text) && !string.IsNullOrEmpty(tboxCity.Text.Trim()))
            {
                v.IsValid = true;
            }
            if( ddlState.SelectedIndex > 0 )
            {
                v.IsValid = true;
            }
            if (!string.IsNullOrEmpty(tboxZip.Text) && !string.IsNullOrEmpty(tboxZip.Text.Trim()))
            {
                v.IsValid = true;
            }
            if (!string.IsNullOrEmpty(tboxPhone.Text) && !string.IsNullOrEmpty(tboxPhone.Text.Trim()))
            {
                v.IsValid = true;
            }
            if (!string.IsNullOrEmpty(tboxEmail.Text) && !string.IsNullOrEmpty(tboxEmail.Text.Trim()))
            {
                v.IsValid = true;
            }



            if (!v.IsValid)
            {
                valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Patient_Unspecified_SearchField").ToString();
            }

        }

        private bool _ConnectWithProvider(int iProviderID)
        {
            AHAHelpContent.Patient pat = GetPatient();

            bool bIsValidProviderConnection = ProviderDetails.IsValidPatientForProvider( pat.UserHealthRecordID, iProviderID);
            //Create invitation entry
            if (!bIsValidProviderConnection)
            {
                GRCBase.DBContext dbContext = null;
                bool bException = false;
                try
                {
                    dbContext = GRCBase.DBContext.GetDBContext();

                    PatientProviderInvitation objInvitation = PatientProviderInvitation.CreateProviderInvitation(
                                CurrentUser.FirstName,
                                CurrentUser.LastName,
                                string.Empty,
                                pat.UserHealthRecordID,
                                false);

                    PatientProviderInvitationDetails objAvailableDetails = objInvitation.PendingInvitationDetails.First();

                    objAvailableDetails.AcceptedProviderID = iProviderID;

                    //Marks the existing invitations, sent by this patient to the current provider, as blocked
                    //cannot user objInvitation.PatientID.Value as patient id is null because whilr creating an invitation patient id is passed as null
                    PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(pat.UserHealthRecordID, iProviderID, false);

                    objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Pending;

                    objAvailableDetails.Update();
                }
                catch
                {
                    bException = true;
                    throw;
                }
                finally
                {
                    try
                    {
                        if (bException)
                        {
                            if (dbContext != null)
                            {
                                dbContext.ReleaseDBContext(false);
                            }
                        }
                        else
                        {
                            dbContext.ReleaseDBContext(true);
                        }
                    }
                    catch
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                        throw;
                    }
                }
            }


            return !bIsValidProviderConnection;
        }

        protected void btnByCodeConnect_Click(object sender, EventArgs e)
        {
            // Note: validation checks to make sure that there is a provider associated with the specified code
            Page.Validate("ByCode");
            if (!Page.IsValid)
            {
                this.hidTabState.Value = TABSTATE_ERROR.ToString();
                return;
            }

            // Perform business logic.
            ProviderDetails     objProviderDetails = ProviderDetails.FindByProviderCode( this.tboxCode.Text.Trim() );
            bool                bConnected = false;

            if (objProviderDetails != null)
            {
                bConnected = _ConnectWithProvider(objProviderDetails.ProviderID);
            }

            // Display popup showing results
            if (bConnected)
            {
                InvitationCompleted();
                DisplayPopup("congrats");  // email and code use the same popup on success
            }
            else
            {
                DisplayPopup("error");
            }
        }

        private void InvitationCompleted()
        {
            this.accInvitation.SelectedIndex = -1;
            this.hidTabState.Value = TABSTATE_COMPLETED.ToString() ;              // flag to PreRender that we don't initialize tabs

            ckbAgree.Checked = false;
            switch( tabcInvite.ActiveTabIndex )
            {
                case 1: // by name
                    tboxFirstName.Text = string.Empty ;
                    tboxLastName.Text = string.Empty ;
                    tboxOrgName.Text = string.Empty ;
                    tboxCity.Text = string.Empty ;
                    if (ddlState.Items.Count > 0)
                    {
                        ddlState.SelectedIndex = 0;
//                        ddlState.SelectedValue = "0";
//                        ddlState.Text = GetGlobalResourceObject("Common", "Text_SelectOne").ToString();
                    }
                    tboxZip.Text = string.Empty ;
                    tboxPhone.Text = string.Empty ;
                    tboxEmail.Text = string.Empty ;
                    break ;

                case 2: // by code
                    tboxCode.Text = string.Empty ;
                    break ;

                case 3: // by email
                    if (ddlEmailSalutation.Items.Count > 0)
                        ddlEmailSalutation.SelectedIndex = 0;
                    tboxEmailFname.Text = string.Empty ;
                    tboxEmailLname.Text = string.Empty ;
                    tboxEmailAddr.Text = string.Empty ;
                    tboxEmailAddrConfirm.Text = string.Empty ;
                    break ;
                    
                case 4: // by print
                    if (ddlPrintSalutation.Items.Count > 0)
                        ddlPrintSalutation.SelectedIndex = 0;
                    tboxPrintFname.Text = string.Empty ;
                    tboxPrintLname.Text = string.Empty ;
                    break ;

                default:
                    break ;
            }
        }


        protected void ValidateByCodeCode(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = false;

            if (String.IsNullOrEmpty(tboxCode.Text.Trim()))
            {
                valid.ErrorMessage = (GetGlobalResourceObject("ConnCenterPAndV", "Patient_Unspecified_Code").ToString());
            }
            else
            {
                ProviderDetails objProviderDetails = ProviderDetails.FindByProviderCode(tboxCode.Text.Trim());

                if (objProviderDetails != null)
                {
                    AHAHelpContent.Patient p = GetPatient();
                    bool bIsValidProviderConnection = ProviderDetails.IsValidPatientForProvider(p.UserHealthRecordID, objProviderDetails.ProviderID);

                    if (!bIsValidProviderConnection)
                    {
                        v.IsValid = true;
                    }
                    else
                    {
                        valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Patient_Invitation_AlreadyConnected").ToString();
                    }
                }
                else
                {
                    valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Patient_Invalid_Code").ToString();
                }
            }
        }

        protected void btnByEmailInvite_Click(object sender, EventArgs e)
        {
            Page.Validate("ByEmail");
            if (!Page.IsValid)
            {
                this.hidTabState.Value = TABSTATE_ERROR.ToString();
                return;
            }

            //
            // Create invitation in database
            //
            AHAHelpContent.Patient pat = GetPatient();

            string                      fname = (ddlEmailSalutation.SelectedIndex > 0) ? tboxEmailFname.Text : (ddlEmailSalutation.SelectedValue + " " + tboxEmailFname.Text);
            PatientProviderInvitation   objInvitation = PatientProviderInvitation.CreateProviderInvitation(fname,
                                                                                                          tboxEmailLname.Text,
                                                                                                          tboxEmailAddr.Text,
                                                                                                          pat.UserHealthRecordID,
                                                                                                          false );


            //Send mail to provider            
            EmailUtils.SendInvitationToProvider(objInvitation, CurrentUser.FullName, CurrentUser.Email);

            //
            // Now close up iteration of invitation
            //
            InvitationCompleted();

            //
            // Display popup
            //
            DisplayPopup("congrats");

        }

        protected void ValidateTermsFields(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;

            if (ckbAgree.Checked)
            {
                v.IsValid = true;
            }
            else
            {
                v.IsValid = false;
                valid.ErrorMessage = "Please check the checkbox if you wish to proceed";
            }
        }

        protected void ValidateByEmailAddrConfirm(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = false;

            string  addr1 = this.tboxEmailAddr.Text.Trim() ;
            string  addr2 = this.tboxEmailAddrConfirm.Text.Trim() ;

            if (!string.IsNullOrEmpty(addr1) && !string.IsNullOrEmpty( addr2) && addr1.Equals( addr2 ) )
            {
                // now make sure the email addr is formatted correctly
                if (GRCBase.EmailSyntaxValidator.Valid(addr1, true))
                {
                    v.IsValid = true;
                }
                else
                {
                    valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_InvalidEmailFormat").ToString();
                }
            }
            else
            {
                valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_UnmatchedEmails").ToString();
            }
        }

        protected void ValidateByEmailLname(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string lname = this.tboxEmailLname.Text.Trim();

            if (string.IsNullOrEmpty(lname) || lname.Length < 2 )
            {
                v.IsValid = false ;
                valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_LastNameRequired").ToString();
            }
        }

        protected void ValidateByEmailFname(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string fname = this.tboxEmailFname.Text.Trim();

            if (string.IsNullOrEmpty(fname) || fname.Length < 2)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_FirstNameRequired").ToString();
            }
        }


        protected void ValidateByPrintFname(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string fname = this.tboxPrintFname.Text.Trim();

            if (string.IsNullOrEmpty(fname) || fname.Length < 2)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_FirstNameRequired").ToString();
            }
        }

        protected void ValidateByPrintLname(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string lname = this.tboxPrintLname.Text.Trim();

            if (string.IsNullOrEmpty(lname) || lname.Length < 2)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("ConnCenterPAndV", "Text_LastNameRequired").ToString();
            }
        }

        protected void btnByPrintPreview_Click(object sender, EventArgs e)
        {
            Page.Validate("ByPrint");
            if (!Page.IsValid)
            {
                this.hidTabState.Value = TABSTATE_ERROR.ToString();
                return;
            }

            // We need to display an invitation page that the user can print.
            // Use PrinterFriendly.aspx to handle that.

                // pipe-delimited values
            string  sal = (this.ddlPrintSalutation.SelectedIndex > 0) ? ddlPrintSalutation.SelectedValue : "0" ;
            string  invitationParams = sal + "|" + tboxPrintFname.Text + "|" + tboxPrintLname.Text + "|0" ;
            string  url = "PrinterFriendly.aspx?modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER).ToString() +
                          "&invitation=" + GRCBase.BlowFish.EncryptString(invitationParams);
            string script = "window.open('" + url + "', '_printInvite');";

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OPEN BYPRINT", script, true); 

            //
            // Now close up this iteration of invitation
            //
            InvitationCompleted();

        }

    }
}