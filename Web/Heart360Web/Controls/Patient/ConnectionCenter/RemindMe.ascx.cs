﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAHelpContent;
using AHAAPI;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class RemindMe : System.Web.UI.UserControl
    {
        public UpdatePanel GetUpdatePanel()
        {
            return this.upanelRemindMe;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string sActiveAccordian = Request.QueryString["active"];

                if (!String.IsNullOrEmpty(sActiveAccordian))
                {
                    // PJB - If you want to have an accordion be automatically opened,
                    // then you need to add the "pre-selected" class to the div tag that has toggle-button
                    switch (sActiveAccordian)
                    {
                        case "rem":
                        case "sms":
                        case "ivr":
                            {
                                string classval = Dt1.Attributes["class"];
                                classval += " pre-selected";
                                Dt1.Attributes["class"] = classval;
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }
}