﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IVR_Master.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.IVR.IVR_Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<li>
    <div id="divIVRSetup" runat="server" class="asp-container-start">
     
        <ul style="margin: 0px;" class="asp-container-ul">

            <li style="margin: 0px;" class="asp-container-li">

                <asp:UpdatePanel ID="upIVR" runat="server" UpdateMode="Conditional">
                                        
                    <ContentTemplate>

	                    <asp:Accordion ID="accIVRSetup" runat="server"
                            RequireOpenedPane="false"
                            Visible="false"
                            AutoSize="None"
                            SelectedIndex="-1"
                            CssClass="accordionChild"  
                            HeaderCssClass="accordionChildHeader"
                            HeaderSelectedCssClass="accordionChildHeaderSelected"
                            ContentCssClass="accordionChildContent"
                            FadeTransitions="false"
                            FramesPerSecond="30"
                            TransitionDuration="300">

                            <Panes>

                                <asp:AccordionPane ID="accPaneIVRSetup" runat="server" CssClass="philIsThisEmptyDiv" HeaderCssClass="philTryThis" >

                                    <Header>
                                        <!-- ## BEGIN IVR SETUP ## -->
                                        <div class="trigger-wrap">
                                            <div class="inner-trigger">  
                                                <span class="sms-edit-list-title"><asp:Literal ID="litIVRSetupTitle" runat="server"></asp:Literal></span>
                                                <span class="blue-arrow"></span>
                                            </div>
                                        </div>

                                    </Header>

                                    <Content>
                    
                                        <asp:ValidationSummary 
                                            ID="valSummary"
                                            ValidationGroup="valPIN"
                                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                            Enabled="true"
                                            CssClass="validation-error full"
                                            DisplayMode="BulletList"
                                            ShowSummary="true"
                                            runat="server" />   

			                            <div class="row">

                                            <div class="large-12 small-12 columns" >
                                                <asp:UpdateProgress ID="uprIVRSetup" AssociatedUpdatePanelID="upIVR" runat="server" DisplayAfter="500" >
                                                    <ProgressTemplate>
                                                        <div class="large-1 large-centered small-2 small-centered columns">
                                                            <asp:Image ID="Image1" runat="server" AlternateText="Animated progress gif" ImageAlign="Middle" ImageUrl="~/images/ajax-loader.gif" />       
                                                        </div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <%=GetGlobalResourceObject("RemindMe", "IVR_Text_YouCanUse") %>
                                        </div>

                                        <div class="row" style="margin-top: .75em;">

                                            <div class="large-4 small-12 columns">
                                                <p><%=GetGlobalResourceObject("RemindMe", "IVR_Title_CreatePIN")%></p>
                                            </div>

						                    <div class="large-2 small-12 columns">
                                                <asp:TextBox ID="txtIVRPin" runat="server" CssClass="full" MaxLength="4"></asp:TextBox>
						                    </div>

						                    <div class="large-6 small-12 columns">
                                                &nbsp;
						                    </div>

                                        </div>

                                        <div class="row">

                                            <div class="large-8 small-12 columns">
                                                <asp:Label ID="Label1" runat="server"></asp:Label>&nbsp;
				                            </div>

				                            <div class="large-4 small-12 columns">
                                                <asp:Button ID="btnSavePIN" runat="server" CssClass="gray-button full" OnClick="OnClick_SavePIN" Text="<%$Resources:RemindMe,IVR_Button_SavePIN%>" />
                                                <asp:Button ID="Button1" runat="server" width="1" Height="1" BackColor="White" BorderStyle="None" />
				                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="large-4 small-12 columns">
					                            <p><%=GetGlobalResourceObject("RemindMe", "IVR_Title_IVRWillCall")%></p>
                                            </div>

		                                    <div class="large-4 small-12 columns">
			                                    <h2 class="ivr-number" style="margin: 0px 0px 5px 0px; line-height: 1em;"><asp:Literal ID="litPhoneContactSetup" runat="server"></asp:Literal></h2>
		                                    </div>

                                        </div>

                                        <div class="row">

				                            <div class="large-12 small-12 columns">
					                            <p class="full"><%=GetGlobalResourceObject("RemindMe", "IVR_Text_TollFree")%></p>
					                            <p class="full"><%=GetGlobalResourceObject("RemindMe", "IVR_Text_ToCallIn") %></p>
				                            </div>

                                        </div>


                                        <!-- ## END IVR SETUP ## -->

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender1"
    runat="server"
    Enabled="true"
    TargetControlID="txtIVRPin"
    FilterType="Numbers">
</asp:FilteredTextBoxExtender>

<asp:CustomValidator
    ID="validPIN"
    runat="server"
    Display="None"
    EnableClientScript="false"
    ValidationGroup="valPIN"
    OnServerValidate="ValidatePin">
</asp:CustomValidator>

                                    </Content>

                                </asp:AccordionPane>                               

                                <asp:AccordionPane ID="accPaneIVRReminder" runat="server" CssClass="philIsThisEmptyDiv" HeaderCssClass="philTryThis" >

                                    <Header>
                                        <!-- ## BEGIN IVR REMINDER ## -->
                                        <div class="trigger-wrap">
                                            <div class="inner-trigger">  
                                                <span class="sms-edit-list-title"><asp:Literal ID="litIVRReminder" runat="server"></asp:Literal></span>
                                                <span class="blue-arrow"></span>
                                            </div>
                                        </div>

                                    </Header>

                                    <Content>

                                        <asp:UpdatePanel ID="upIVRReminder" runat="server" UpdateMode="Conditional" >

                                            <ContentTemplate>

                                                <asp:ValidationSummary 
                                                    ID="valSummaryReminder"
                                                    ValidationGroup="valReminder"
                                                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                    Enabled="true"
                                                    CssClass="validation-error full"
                                                    DisplayMode="BulletList"
                                                    ShowSummary="true"
                                                    runat="server" />  

                                                <div class="row"> 

                                                    <div class="large-12 small-12 columns" >
                                                        <asp:UpdateProgress ID="uprIVRReminder" AssociatedUpdatePanelID="upIVRReminder" runat="server">
                                                            <ProgressTemplate>
                                                                <div class="large-1 large-centered small-2 small-centered columns">
                                                                    <asp:Image ID="Image1" runat="server" AlternateText="Animated progress gif" ImageAlign="Middle" ImageUrl="~/images/ajax-loader.gif" />       
                                                                </div>
                                                            </ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="large-12 small-12 columns">
                                                        <div class="row">

		                                                    <div class="large-12 small-12 columns">
			                                                    <p class="full"><%=GetGlobalResourceObject("RemindMe", "IVR_Text_EnableIVR")%></p>
		                                                    </div>
    
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

		                                            <div class="large-8 small-12 columns">
			                                            <p>The contact number in your <a href="Profile.aspx">Profile</a> the IVR will call is: </p>
		                                            </div>
    
		                                            <div class="large-4 small-12 columns">
			                                            <h2 class="ivr-number" style="margin: 0px 0px 5px 0px; line-height: 1em;"><asp:Literal ID="litPhoneContactReminder" runat="server"></asp:Literal></h2>
		                                            </div>

                                                </div>

                                                <div class="row">

		                                            <div class="large-8 small-12 columns">
			                                            <p class="full"><%=GetGlobalResourceObject("RemindMe", "IVR_Text_WantReminder")%></p>
		                                            </div>

                                                    <div class="large-4 small-12 columns">

                                                        <table class="radButtons">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButton ID="rbEnableIVR_Yes" runat="server" OnCheckedChanged="OnCheckedChanged_EnableIVR" GroupName="rbEnableIVR" AutoPostBack="true" />                                 
                                                                    </td>
                                                                    <td>
                                                                        <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_Yes")%>                           
                                                                    <td>
                                                                        <asp:RadioButton ID="rbEnableIVR_No" runat="server" OnCheckedChanged="OnCheckedChanged_EnableIVR" GroupName="rbEnableIVR" AutoPostBack="true" />
                                                                    </td>
                                                                    <td>
                                                                        <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_No")%>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="large-12 small-12 columns" style="border-top: 1px solid #cccccc; margin-top: 1em; padding-top: 1em;">

                                                        <div class="large-6 small-12 columns">
                                                            <p class="full"><%=GetGlobalResourceObject("RemindMe", "IVR_Title_TimeToCall")%></p>
		                                                </div>

                                                        <div class="large-2 small-12 columns">
                                                            <p><%=GetGlobalResourceObject("RemindMe", "Title_Time")%></p>
                                                        </div>

		                                                <div class="large-4 small-12 columns">
                                                            <asp:DropDownList ID="ddlTimeOfDay" runat="server" CssClass="dropdowns" />
		                                                </div>

                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="large-6 small-12 columns">
                                                        &nbsp;
		                                            </div>
                
                                                    <div class="large-2 small-12 columns">
                                                        <p><%=GetGlobalResourceObject("RemindMe", "Title_Day")%></p>
                                                    </div>

		                                            <div class="large-4 small-12 columns">
                                                        <asp:DropDownList ID="ddlDays" runat="server" CssClass="dropdowns">
                                                            <asp:ListItem Value="" Text="Select:"></asp:ListItem>
                                                            <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                            <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                            <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                            <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                                                            <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                            <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                            <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                        </asp:DropDownList>
		                                            </div>

                                                </div>

                                                <div class="row">

                                                    <div class="large-6 small-12 columns">
                                                        &nbsp;
		                                            </div>

                                                    <div class="large-2 small-12 columns">
                                                        <p><%=GetGlobalResourceObject("RemindMe", "Title_TimeZone")%></p>
                                                    </div>

                                                    <div class="large-4 small-12 columns">
                                                        <asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="dropdowns">
                                                            <asp:ListItem Value="" Text="Select:"></asp:ListItem>
                                                            <asp:ListItem Value="US/Eastern" text="Eastern"></asp:ListItem>
                                                            <asp:ListItem Value="US/Central" text="Central"></asp:ListItem>
                                                            <asp:ListItem Value="US/Mountain" text="Mountain"></asp:ListItem>
                                                            <asp:ListItem Value="US/Mountain (AZ no daylight savings)" text="Mountain (AZ no daylight savings)"></asp:ListItem>
                                                            <asp:ListItem Value="US/Pacific" text="Pacific"></asp:ListItem>
                                                        </asp:DropDownList>
		                                            </div>

                                                </div>

                                                <div class="row">

		                                            <div class="large-8 small-12 columns">
			                                            &nbsp;
		                                            </div>

		                                            <div class="large-4 small-12 columns">
			                                            <asp:Button ID="btnSave" runat="server" CssClass="gray-button full" OnClick="OnClick_SaveReminder" Text="<%$Resources:RemindMe,SMS_Button_Save%>" />
                                                        <asp:Button ID="btnHidden" runat="server" width="1" Height="1" BackColor="White" BorderStyle="None" />
		                                            </div>

                                                </div>

<asp:CustomValidator
    ID="validTime"
    runat="server"
    Display="None"
    EnableClientScript="false"
    ValidationGroup="valReminder"
    OnServerValidate="ValidateReminderTime">
</asp:CustomValidator>

<asp:CustomValidator
    ID="validDay"
    runat="server"
    Display="None"
    EnableClientScript="false"
    ValidationGroup="valReminder"
    OnServerValidate="ValidateReminderDay">
</asp:CustomValidator>

<asp:CustomValidator
    ID="validTimeZone"
    runat="server"
    Display="None"
    EnableClientScript="false"
    ValidationGroup="valReminder"
    OnServerValidate="ValidateReminderTimeZone">
</asp:CustomValidator>

                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                            <!-- ## END IVR Reminder ## -->

                                    </Content>

                                </asp:AccordionPane>

                            </Panes>

                        </asp:Accordion>                                                     
      
                    </ContentTemplate>
    
                </asp:UpdatePanel>

            </li>

        </ul>

    </div>

    <asp:HiddenField ID="hidIVRPIN" runat="server" />
    <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>

</li>

