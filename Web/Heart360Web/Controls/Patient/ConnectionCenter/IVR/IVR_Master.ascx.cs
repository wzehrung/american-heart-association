﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using AHAHelpContent;

namespace Heart360Web.Controls.Patient.ConnectionCenter.IVR
{
    public partial class IVR_Master : System.Web.UI.UserControl
    {
        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }

        public string PhoneContact
        {
            get
            {
                AHAHelpContent.PhoneNumbers objPhoneNumbers = AHAHelpContent.PhoneNumbers.GetPhoneNumbers(CurrentPatient.UserHealthRecordID);
                string sPhoneContact = string.Empty;

                if (objPhoneNumbers.PhoneContact != null)
                {
                    sPhoneContact = objPhoneNumbers.PhoneContact;

                    if (sPhoneContact.StartsWith("1") == true)
                    {
                        sPhoneContact = sPhoneContact.Remove(0, 1);
                    }
                }

                string retval = sPhoneContact;

                return retval;
            }
        }

        private void DisplayPopup(string _keys)
        {
            string editUrl = "PopUp.aspx?connect=2&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER).ToString() + "&elid=" + _keys;
            string script = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER);

            // Script Manager doesn't want the "return" in the script.
            string script2 = script.Substring(6);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "SHOW POPUP", script2, true);
        }

        protected void Page_init(object sender, EventArgs e)
        {
            _LoadTimeOfDay(ddlTimeOfDay);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserIdentityContext objContext = SessionInfo.GetUserIdentityContext();

            this.CurrentPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objContext.PatientID.Value);

            IVRReminders objIVR = IVRReminders.GetIVRScheduleByPatient(Convert.ToInt32(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID));

            if (!String.IsNullOrEmpty(PhoneContact) || PhoneContact.Length == 10)
            {
                litPhoneContactSetup.Text = "(" + PhoneContact.Substring(0, 3) + ")&nbsp;" + PhoneContact.Substring(3, 3) + "-" + PhoneContact.Substring(6, 4);
                litPhoneContactReminder.Text = "(" + PhoneContact.Substring(0, 3) + ")&nbsp;" + PhoneContact.Substring(3, 3) + "-" + PhoneContact.Substring(6, 4);
                litIVRReminder.Text = GetGlobalResourceObject("RemindMe", "IVR_Title_Reminder").ToString();
            }
            else
            {
                litPhoneContactSetup.Text = GetGlobalResourceObject("RemindMe", "IVR_Text_NoNumber").ToString();
                litPhoneContactReminder.Text = GetGlobalResourceObject("RemindMe", "IVR_Text_NoNumber").ToString();
                txtIVRPin.Enabled = false;
                btnSavePIN.Visible = false;
            }

            if (!Page.IsPostBack)
            {
                _GetControls();

                if (objIVR != null)
                {
                    if (!String.IsNullOrEmpty(objIVR.PIN))
                    {
                        hidIVRPIN.Value = objIVR.PIN;
                        txtIVRPin.Text = objIVR.PIN;
                    }

                    if (!String.IsNullOrEmpty(objIVR.PhoneNumber) && !String.IsNullOrEmpty(PhoneContact) )
                    {
                        litPhoneContactReminder.Text = "(" + PhoneContact.Substring(0, 3) + ")&nbsp;" + PhoneContact.Substring(3, 3) + "-" + PhoneContact.Substring(6, 4);
                    }

                    if (!String.IsNullOrEmpty(objIVR.Time))
                    {
                        ddlTimeOfDay.SelectedValue = objIVR.Time;
                    }

                    if (!String.IsNullOrEmpty(objIVR.Day))
                    {
                        ddlDays.SelectedValue = objIVR.Day;
                    }

                    if (!String.IsNullOrEmpty(objIVR.TimeZone))
                    {
                        ddlTimeZone.SelectedValue = objIVR.TimeZone;
                    }

                    if (objIVR.IVREnabled == true)
                    {
                        rbEnableIVR_Yes.Checked = true;
                        ddlTimeOfDay.Enabled = true;
                        ddlDays.Enabled = true;
                        ddlTimeZone.Enabled = true;
                    }
                    else
                    {
                        rbEnableIVR_No.Checked = true;
                    }
                }
                else
                {
                    rbEnableIVR_No.Checked = true;
                }
            }
        }

        protected void OnCheckedChanged_EnableIVR(object sender, EventArgs e)
        {
            if (rbEnableIVR_Yes.Checked == true)
            {
                ddlTimeOfDay.Enabled = true;
                ddlDays.Enabled = true;
                ddlTimeZone.Enabled = true;
            }
            else
            {
                ddlTimeOfDay.Enabled = false;
                ddlDays.Enabled = false;
                ddlTimeZone.Enabled = false;
            }
        }

        private void _GetControls()
        {
            // IVR
            try
            {
                // Check to see if a patient is in a campaign, if so load the IVR control
                AHAHelpContent.Patient objInCampaign = AHAHelpContent.Patient.IsPatientInACampaign(Convert.ToInt32(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID));

                // If the user is in a campaign, show the IVR stuff
                if (objInCampaign.PatientID != null)
                {
                    // Load the IVR control and show the IVR accordion
                    accIVRSetup.Visible = true;

                    // Get user's reminder
                    IVRReminders objIVR = IVRReminders.GetIVRScheduleByPatient(Convert.ToInt32(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID));

                    if (objIVR != null)
                    {
                        // If they have setup a PIN
                        if (!String.IsNullOrEmpty(objIVR.PIN))
                        {
                            accPaneIVRReminder.Visible = true;
                            litIVRSetupTitle.Text = (GetGlobalResourceObject("RemindMe", "IVR_Title_ChangePIN").ToString());
                            litIVRReminder.Text = (GetGlobalResourceObject("RemindMe", "IVR_Title_Reminder").ToString());
                        }
                        else
                        {
                            litIVRSetupTitle.Text = (GetGlobalResourceObject("RemindMe", "IVR_Title_Setup").ToString());
                            accPaneIVRReminder.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = lblErrorMessage.Text + "<br/><br/>IVR Error:<br/>" + ex.ToString();
            }
        }

        protected void OnClick_SavePIN(object sender, EventArgs e)
        {
            Page.Validate("valPIN");
            if (!Page.IsValid)
                return;
            try
            {
                string strPhone = PhoneContact;
                strPhone = "1" + strPhone;

                ListItemCollection itemColl = TimeConversion.getTimeInAMPMFormatWholeHours();

                string dfltTOD = itemColl[0].Value;


                AHAHelpContent.IVRReminders.CreateOrUpdateIVRSchedule(Convert.ToInt32(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID),
                    txtIVRPin.Text, dfltTOD, "Monday", "Eastern", Convert.ToBoolean(true), strPhone);

                accPaneIVRReminder.Visible = true;
                accIVRSetup.SelectedIndex = 1;
                litIVRSetupTitle.Text = (GetGlobalResourceObject("RemindMe", "IVR_Title_ChangePIN").ToString());

                DisplayPopup("setupivr");
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.ToString();
            }
        }

        protected void OnClick_SaveReminder(object sender, EventArgs e)
        {
            bool bEnableIVR = false;

            Page.Validate("valReminder");
            if (!Page.IsValid)
                return;

            try
            {
                if (rbEnableIVR_Yes.Checked == true)
                {
                    bEnableIVR = true;
                }
                else
                {
                    bEnableIVR = false;
                }

                AHAHelpContent.IVRReminders.CreateOrUpdateIVRSchedule(Convert.ToInt32(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID),
                    txtIVRPin.Text, ddlTimeOfDay.SelectedValue.ToString(), ddlDays.SelectedValue.ToString(), ddlTimeZone.SelectedValue.ToString(), bEnableIVR, "1" + PhoneContact);

                //lblDays.Text = ddlDays.SelectedItem.Text;
                //lblReminderTime.Text = ddlTimeOfDay.SelectedItem.Text;

                DisplayPopup((bEnableIVR) ? "enableivr" : "disableivr");
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.ToString();
            }
        }

        private void _LoadTimeOfDay(DropDownList ddlTimeOfDay)
        {
            ListItemCollection itemColl = TimeConversion.getTimeInAMPMFormatWholeHours();

            ddlTimeOfDay.DataSource = itemColl;
            ddlTimeOfDay.DataTextField = "Text";
            ddlTimeOfDay.DataValueField = "Value";

            ddlTimeOfDay.DataBind();

            ddlTimeOfDay.Items.Insert(0, new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            // Set the default selected time to the current one rounded up to nearest hour
            DateTime RoundUp = DateTime.Parse(DateTime.Now.ToShortTimeString());
            RoundUp = RoundUp.AddMinutes(RoundUp.Hour);
            ddlTimeOfDay.SelectedValue = RoundUp.ToString("HH:mm");
        }

        #region Validators

        protected void ValidatePin(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (String.IsNullOrEmpty(txtIVRPin.Text))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_PIN_Invalid").ToString();

                return;
            }
            else if (txtIVRPin.Text.Trim().Length < 4)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_PIN_Incomplete").ToString();

                return;
            }
        }

        protected void ValidateReminderTime(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // No need to validate if they are disabling IVR
            if (rbEnableIVR_No.Checked == false)
            {
                if (String.Equals(ddlTimeOfDay.SelectedItem.Text, GetGlobalResourceObject("Common", "Text_Select").ToString()))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_TimeOfDay").ToString();

                    return;
                }
            }
        }

        protected void ValidateReminderDay(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // No need to validate if they are disabling IVR
            if (rbEnableIVR_No.Checked == false)
            {
                if (String.Equals(ddlDays.SelectedItem.Text, GetGlobalResourceObject("Common", "Text_Select").ToString()))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_Day").ToString();

                    return;
                }
            }
        }

        protected void ValidateReminderTimeZone(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            // No need to validate if they are disabling IVR
            if (rbEnableIVR_No.Checked == false)
            {
                if (String.Equals(ddlTimeZone.SelectedItem.Text, GetGlobalResourceObject("Common", "Text_Select").ToString()))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_TimeZone").ToString();

                    return;
                }
            }
        }

        #endregion
    }
}