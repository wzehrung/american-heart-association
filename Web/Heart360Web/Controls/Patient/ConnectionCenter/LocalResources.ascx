﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocalResources.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.LocalResources" %>

<section class="local-resources info-stats full">
	<div class="accordion">
		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("Tracker", "Title_LocalResources")%><span class="gray-arrow"></span></h4>
		</div>
		<div class="content" style="display: none;" >
            <p><%=GetGlobalResourceObject("Tracker", "Title_SubTitle_LocalResources")%></p>

            <asp:UpdatePanel ID="upLocalResources" runat="server">
                <ContentTemplate>
    
                    <asp:DropDownList ID="ddlLocal" OnSelectedIndexChanged="OnTopicChangedLocal" AutoPostBack="true" CssClass="dropdowns" runat="server"></asp:DropDownList>

                    <div id="divContentLocal" runat="server"></div>
    
                    <a id="learnMoreLocal" runat="server" class="gray-button" target="_blank"><%=GetGlobalResourceObject("Tracker", "Text_LearnMore")%></a>

                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:Literal id="litNoResources" runat="server" visible="false" Text="<%$ Resources:Tracker, Text_NoLocalResources %>"></asp:Literal>

		</div>
	</div>
</section>