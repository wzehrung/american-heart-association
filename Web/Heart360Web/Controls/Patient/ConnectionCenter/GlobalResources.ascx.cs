﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class GlobalResources : System.Web.UI.UserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            List<AHAHelpContent.Resource> lstTopics = AHAHelpContent.Resource.FindAllResources(false, AHAAPI.H360Utility.GetCurrentLanguageID());
            //List<AHAHelpContent.Resource> lstTopics = AHAHelpContent.Resource.FindAllNonExpiredResources(false);
            if (lstTopics.Count > 0)
            {
                ddlGlobal.DataSource = lstTopics;
                ddlGlobal.DataTextField = "ContentTitle";
                ddlGlobal.DataValueField = "ResourceID";
                ddlGlobal.DataBind();
                AHAHelpContent.Resource objResource = ((AHAHelpContent.Resource)lstTopics[0]);
                ddlGlobal.Text = objResource.ContentTitle;
                divContent.InnerHtml = UIHelper.RenderLink(objResource.ContentText);
                learnMore.HRef = objResource.ContentTitleUrl;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {



        }

        protected void OnTopicChanged(object sender, EventArgs e)
        {
            AHAHelpContent.Resource objResource = AHAHelpContent.Resource.FindByResourceID(Convert.ToInt32(ddlGlobal.SelectedValue), AHAAPI.H360Utility.GetCurrentLanguageID());
            divContent.InnerHtml = UIHelper.RenderLink(objResource.ContentText);
            learnMore.HRef = objResource.ContentTitleUrl;
        }
    }
}