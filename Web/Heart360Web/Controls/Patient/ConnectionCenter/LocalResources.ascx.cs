﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class LocalResources : System.Web.UI.UserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            int patientID = 0;
            if (AHAAPI.SessionInfo.GetUserIdentityContext() != null)
                patientID = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value;

            List<AHAHelpContent.Resource> lstLocal = AHAHelpContent.Resource.FindLocalResourcesForPatient(false, AHAAPI.H360Utility.GetCurrentLanguageID(), patientID);
            if (lstLocal.Count > 0)
            {
                ddlLocal.DataSource = lstLocal;
                ddlLocal.DataTextField = "ContentTitle";
                ddlLocal.DataValueField = "ResourceID";
                ddlLocal.DataBind();
                AHAHelpContent.Resource objResource = ((AHAHelpContent.Resource)lstLocal[0]);
                ddlLocal.Text = objResource.ContentTitle;
                divContentLocal.InnerHtml = UIHelper.RenderLink(objResource.ContentText);
                learnMoreLocal.HRef = objResource.ContentTitleUrl;
                upLocalResources.Visible = true;
            }
            else
            {
                litNoResources.Visible = true;
                upLocalResources.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnTopicChangedLocal(object sender, EventArgs e)
        {
            int patientID = 0;
            if (AHAAPI.SessionInfo.GetUserIdentityContext() != null)
                patientID = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value;

            AHAHelpContent.Resource objResource = AHAHelpContent.Resource.FindByResourceID(Convert.ToInt32(ddlLocal.SelectedValue), AHAAPI.H360Utility.GetCurrentLanguageID());

            divContentLocal.InnerHtml = objResource.ContentText; //"WHY U NO WORK?!";// UIHelper.RenderLink(objResource.ContentText);
            learnMoreLocal.HRef = objResource.ContentTitleUrl;
        }
    }
}