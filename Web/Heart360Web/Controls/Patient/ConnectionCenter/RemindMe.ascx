﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemindMe.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.RemindMe" %>

<%@ Register Src="~/Controls/Patient/ConnectionCenter/SMS/SMS_Master.ascx" TagName="Reminders" TagPrefix="SMS" %>
<%@ Register Src="~/Controls/Patient/ConnectionCenter/IVR/IVR_Master.ascx" TagName="IVRMaster" TagPrefix="IVR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<section id="my-reminders" class="info-stats full">

    <div class="accordion">
    
        <div id="Dt1" class="toggle-btn" runat="server">
            <h4><%=GetGlobalResourceObject("RemindMe","Title_RemindMe") %><span class="gray-arrow"></span></h4>
        </div>

        <div class="content" style="display: block;" >
            <asp:UpdatePanel ID="upanelRemindMe" UpdateMode="Conditional" runat="server" >
                <ContentTemplate>

                    <ul class="inner-accordion">   
               
                        <SMS:Reminders ID="cntrlReminders" runat="server"></SMS:Reminders>
                  
                        <IVR:IVRMaster ID="cntrlIVR" runat="server"></IVR:IVRMaster>           

                    </ul>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    
    </div>

</section>