﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sponsors.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.Sponsors" %>

<section class="local-resources info-stats full">
	<div class="accordion">
		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("Tracker", "Title_Sponsors")%><span class="gray-arrow"></span></h4>
		</div>
		<div class="content" style="display: none;" >

            <asp:Repeater ID="rptSponsors" runat="server" OnItemDataBound="rptSponsors_ItemDataBound">

                <HeaderTemplate>
                    <table border="0" cellpadding="10" cellspacing="10">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Image ID="grantLogo" runat="server" />
                        </td>
                        <td>
                            <asp:Literal ID="litDesc" runat="server"></asp:Literal><br />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>

            </asp:Repeater>

		</div>
	</div>
</section>