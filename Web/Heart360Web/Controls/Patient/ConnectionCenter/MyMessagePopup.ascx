﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyMessagePopup.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.MyMessagePopup" %>

<div class="full">
    <!-- ## Delete Reading Title ## -->
	<div class="full">
		<h3><asp:Literal ID="litTitle" runat="server"></asp:Literal></h3>
	</div>

    <div class="row">
	    <div class="large-4 small-12 columns">
		    <label id="lblDescription" runat="server"><%=GetGlobalResourceObject("Tracker", "Popup_Confirmation_Delete")%>
            <br /> <br />
            </label>   
	        <asp:Literal ID="LitMessageID" runat="server"></asp:Literal><br />
            <asp:Literal ID="LitFromFolder" runat="server" Visible="false"></asp:Literal><br />
            <asp:Literal ID="LitMsgID" runat="server" Visible="false"></asp:Literal><br />
	    </div>
    </div>

    <div class="row spt">
        <div class="large-3 small-3 columns fl" >
            <asp:Button ID="btnOk" CssClass="gray-button full" OnClick="btnOk_Click" runat="server" Text="<%$Resources:Common,Text_OK %>" />
        </div>
        <div class="large-3 small-3 columns fl" >
            <asp:Button ID="btnCancel" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_Cancel%>" />
        </div>
    </div>
</div>
