﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class Announcements : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int patientID = 0;
            if (AHAAPI.SessionInfo.GetUserIdentityContext() != null)
                patientID = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value;

            List<AHAHelpContent.Resource> lstAnnouncements = AHAHelpContent.Resource.GetAnnouncements(patientID, AHAAPI.H360Utility.GetCurrentLanguageID());

            if (lstAnnouncements.Count > 0)
            {
                repAnnouncements.DataSource = lstAnnouncements;
                repAnnouncements.DataBind();
            }
            else
            {
                divDefault.Visible = true;
            }
        }

        protected void OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {

            }
        }

        //protected void rptActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        AHAHelpContent.MLCActionICanTake objActionICanTake = e.Item.DataItem as AHAHelpContent.MLCActionICanTake;
        //        Literal litActionText = e.Item.FindControl("litActionText") as Literal;
        //        litActionText.Text = objActionICanTake.ActionText;

        //        System.Web.UI.HtmlControls.HtmlControl sc = e.Item.FindControl("spanCircleCheck") as System.Web.UI.HtmlControls.HtmlControl;
        //        if (sc != null)
        //            sc.Attributes.Add("class", GetCircleCheckName());
        //    }
        //}

    }
}