﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using AHACommon;
using AHAHelpContent;
using Microsoft.Health;
using Microsoft.Health.Web;
using HVManager;
using Heart360Web;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class MyMessages : System.Web.UI.UserControl
    {
        #region declaration constant variables
        private const string TRACKER_KEY = "Tracker";
        private const string TRACKER_KEY_DATA_TEXT_FIELD = "Tracker_Text_DDL_Provider_DataTextField";
        private const string TRACKER_KEY_DATA_VALUE_FIELD = "Tracker_Text_DDL_Provider_DataValueField";
        private const string TRACKER_KEY_SELECTONE = "Tracker_Text_SelectOne";
        private const string TRACKER_KEY_VALIDATE_PROVIDER = "Validation_Text_ComposeProviders";
        private const string TRACKER_KEY_VALIDATE_SUBJECT = "Validation_Text_ComposeSubject";
        private const string TRACKER_KEY_VALIDATE_MESSAGE = "Validation_Text_ComposeMessage";
        private const string TRACKER_KEY_SNET_MESSAEG_ALERT = "Tracker_Sent_Message_Alert";
        private const string TRACKER_KEY_ALERT_PROVIDER = "Tracker_Error_Provider";
        private const string TRACKER_KEY_ALERT_SUBJECT = "Tracker_Alert_Subject";
        private const string TRACKER_KEY_ALERT_MSG_BODY = "Tracker_Alert_Message";

        private const string COMPOSE_RE = "RE: ";
        private const string COMPOSE_HTML_BR = "<br>";
        private const string COMPOSE_LINE_BRAKE = "\n";
        private const string COMPOSE_NO_MESSAGE = "No message.";
        private const string COMPOSE_NO_SUBJECT = "No subject";
        private const string COMPOSE_MESSAGE = "Message : ";
        private const string COMPOSE_FROM = "From: ";
        private const string COMPOSE_TO = "To: ";
        private const string COMPOSE_SUBJECT = "Subject: ";
        private const string COMPOSE_SENT_DATE = "Date: ";
        private const string COMPOSE_MSG_START = "> ";

        private const string UC_LIT_FROM = "LitFrom";
        private const string UC_LIT_TO = "LitTo";
        private const string UC_LIT_SUBJECT = "litSubject";
        private const string UC_LIT_SENTDATE = "LitSentDate";
        private const string UC_LIT_MESSAGE = "LitMessageBody";
        private const string UC_IMG_BTN_DELETE = "imgBtnDelete";
        private const string UC_IMG_BTN_REPLY = "imgBtnReply";
        #endregion 
        
        #region declaration variables
        private string m_subject = string.Empty;
        private string m_provider = string.Empty;
        private string m_message = string.Empty;
        
        // User Type 
        private AHAHelpContent.UserType UserType
        {
            get;
            set;
        }
        // return current user details
        private AHAHelpContent.UserTypeDetails CurrentUserDetails
        {
            get
            {
                // patient information 
                if (this.UserType == AHAHelpContent.UserType.Patient)
                {
                    return new AHAHelpContent.UserTypeDetails
                    {
                        UserID = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value,
                        UserType = AHAHelpContent.UserType.Patient
                    };
                }
                else
                {
                    // provider information 
                    return new AHAHelpContent.UserTypeDetails
                    {
                        UserID = AHAAPI.Provider.SessionInfo.GetProviderContext().ProviderID,
                        UserType = AHAHelpContent.UserType.Provider
                    };
                }
            }
        }
        // the current patient
        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }
        #endregion

        #region declaration events

        public string CurrentFolderType
        {
            get
            {
                return hidFolderType.Value;
            }
            set
            {
                hidFolderType.Value = value;
            }
        }
        public string CurrentMessageID
        {
            get
            {
                return hidMessageID.Value;
            }
            set
            {
                hidMessageID.Value = value;
            }
        }

        #endregion 

        /// <summary>
        ///  Page load 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            //objPage = Page as IMailHome;
            // ----------------------------------------------------
            // if this is not page postback, perform following
            // ----------------------------------------------------
            //if (!Page.IsPostBack)
            //{
            //    #region if not page postback

            //    // -------------------------------------------------------
            //    // calling routine to initialize compose message section 
            //    // -------------------------------------------------------
            //    //_InitializeForm();
            //    #endregion 
            //}

            if (!Page.IsPostBack)
            {
                string  js = "document.getElementById('" + divTabCompose.ClientID.ToString() + "').click();" ;
                divTabCompose.Attributes.Add("onclick", js );
            }
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            RefreshTabpanel( mvMsgBoxes.ActiveViewIndex) ;  //    tabMsg.ActiveTabIndex);



        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="activeTab"></param>
        private void RefreshTabpanel(int activeTab)
        {
            string tabClass = "large-3 small-3 tab-btn";
            switch (activeTab)
            {
                case 0: //message inbox
                    this.CurrentFolderType = AHADefs.FolderType.MESSAGE_FOLDER_INBOX;
                    _BindMessages(AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX, null, false);
                    
                    divTabInbox.Attributes["class"] = tabClass + " active-btn";
                    divTabCompose.Attributes["class"] = tabClass;
                    divTabSentbox.Attributes["class"] = tabClass;
                    divTabTrashbox.Attributes["class"] = tabClass;
                    break;

                case 1:  //compose 
                    //if (!Page.IsPostBack)
                    //{
                    ddlProviders.Items.Clear();
                    _loadProviderDDL();

                    divTabInbox.Attributes["class"] = tabClass;
                    divTabCompose.Attributes["class"] = tabClass + " active-btn";
                    divTabSentbox.Attributes["class"] = tabClass;
                    divTabTrashbox.Attributes["class"] = tabClass;
                    //}
                    break;

                case 2: //sent messages
                    this.CurrentFolderType = AHADefs.FolderType.MESSAGE_FOLDER_SENT_MESSAGES;
                    _BindMessages(AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_SENT_MESSAGES, null, false);

                    divTabInbox.Attributes["class"] = tabClass;
                    divTabCompose.Attributes["class"] = tabClass;
                    divTabSentbox.Attributes["class"] = tabClass + " active-btn";
                    divTabTrashbox.Attributes["class"] = tabClass;
                    break;

                case 3: //trash can
                    this.CurrentFolderType = AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN;
                    _BindMessages(AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN, null, false);

                    divTabInbox.Attributes["class"] = tabClass;
                    divTabCompose.Attributes["class"] = tabClass;
                    divTabSentbox.Attributes["class"] = tabClass;
                    divTabTrashbox.Attributes["class"] = tabClass + " active-btn";
                    break;

                default:
                    break;
            }
        }
        /// <summary>
        ///  extract and loading provider's dropdownlist
        /// </summary>
        /// 
        private void _loadProviderDDL()
        {

            // ----------------------------------------------
            //  init ddlproviders dropdownlist 
            // ----------------------------------------------
            _initProviderDDL();
            // --------------------------------------------------------
            // clear the dropdown list then fill the providers name
            // --------------------------------------------------------
            #region loading dropdown list 
            ddlProviders.DataSource = AHAHelpContent.ProviderDetails.FindProviderByPatientID(this.CurrentUserDetails.UserID).Where(PD => PD.AllowPatientsToSendMessages.HasValue && PD.AllowPatientsToSendMessages.Value);
            ddlProviders.DataTextField = GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_DATA_TEXT_FIELD).ToString();
            ddlProviders.DataValueField = GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_DATA_VALUE_FIELD).ToString();
            ddlProviders.DataBind();
            ddlProviders.Enabled = true;

            #endregion 
        }
        /// <summary>
        ///  Initialize ddlProviders dropdowmlist 
        /// </summary>
        private void _initProviderDDL()
        {
            // --------------------------------------------------------------------
            //  Insert the default item "Select One" as the top row selection 
            // --------------------------------------------------------------------
            ddlProviders.Items.Insert(0, new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_SELECTONE).ToString(), string.Empty));
        }
        /// <summary>
        /// initilize form.
        /// </summary>
        private void _InitializeForm()
        {
            // -----------------------------------------------------
            //  this routine is used ONLY for init compose controls
            // -----------------------------------------------------
            txtSubject.Text = string.Empty;
            txtMailBody.Text = string.Empty;
            ddlProviders.Items.Clear();
            _loadProviderDDL();
        }
        /// <summary>
        /// send message routine for patients or participants 
        /// </summary>
        private void _sendMessage()
        {
            try
            {
                #region sending message for patient routine.

                #region validation 
/** these will have been validated by the asp validators
                if (ddlProviders.SelectedItem.Value == string.Empty)
                {
                    throw new ApplicationException(GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_ALERT_PROVIDER).ToString());
                }
                if (txtMailBody.Text.Length == 0)
                {
                    throw new ApplicationException(GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_ALERT_MSG_BODY).ToString());
                }
                if (txtSubject.Text.Length == 0)
                {
                    throw new ApplicationException(GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_ALERT_SUBJECT).ToString());
                }
 **/
                #endregion 

                #region extract provider id & message id
                int iToUserID = (ddlProviders.SelectedItem != null) ? Convert.ToInt32(ddlProviders.SelectedItem.Value) : -1 ;

                //Create message entry in db
                int iMessageID = AHAHelpContent.Message.CreateMessage(this.CurrentUserDetails.UserID, iToUserID, AHACommon.AHADefs.MessageType.MESSAGE_TYPE_NEW, false);
                #endregion 

                #region create a new message in the Helth Vault
                // ---------------------------------------------------
                // Create  a new message in Health Vault
                // ---------------------------------------------------
                MessageDataManager.MessageItem objMessage = new MessageDataManager.MessageItem();
                objMessage.DateSent.Value = DateTime.Now;
                objMessage.IsSentByProvider.Value = false;
                objMessage.MessageBody.Value = (txtMailBody.Text.Trim().Length > 0) ? txtMailBody.Text : COMPOSE_NO_MESSAGE;
                objMessage.MessageID.Value = iMessageID;
                objMessage.ProviderID.Value = iToUserID;
                objMessage.Subject.Value = (txtSubject.Text.Trim().Length > 0) ? txtSubject.Text.Trim() : COMPOSE_NO_SUBJECT;
                MessageWrapper.CreateMessage(objMessage, PatientGuids.CurrentPatientGuids.PersonID, PatientGuids.CurrentPatientGuids.RecordID);
               
                #endregion 

                #region notifying provider
                // ----------------------------------------------------------------------
                // Notify provider if he enabled notification option
                // ----------------------------------------------------------------------
                AHAHelpContent.ProviderDetails objProviderDetails = AHAHelpContent.ProviderDetails.FindByProviderID(iToUserID);
                if (objProviderDetails.NotifyNewMessageImmediately.HasValue &&
                    objProviderDetails.NotifyNewMessageImmediately.Value)
                {
                    //Notification Enabled
                    EmailUtils.SendNewMessageEmail(objProviderDetails.FirstName, objProviderDetails.EmailForIndividualMessage, objProviderDetails.LanguageLocale);
                }
                _InitializeForm();
                PopulateAlertMessage(GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_SNET_MESSAEG_ALERT).ToString());
                #endregion 

                #endregion
            }
            catch(Exception ex)
            {
                #region populate exception message.
                PopulateAlertMessage(ex);
                #endregion

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iMessageID"></param>
        private void MarkMessageAsRead(int iMessageID)
        {
            // --------------------------------------------------
            // set this message IsRead to true
            // --------------------------------------------------
            AHAHelpContent.Message.MarkMessageAsReadOrUnread(this.CurrentUserDetails, iMessageID, true);
        }
        /// <summary>
        ///  calling sent message function to create message.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSubmit(object sender, EventArgs e)
        {
            // Validate the message parameters
            Page.Validate("vgCompose");
            if (!Page.IsValid)
                return;

            //sent button execution 
            _sendMessage();

        }
        /// <summary>
        ///  return message count
        /// </summary>
        /// <returns></returns>
        public string GetMessageCount()
        {
            // ----------------------------------------------------------------------------
            // this routine querying the count of unread messages and post to the connection page 
            // ---------------------------------------------------------------------------
            int iMessageCount = AHAHelpContent.Message.GetNumberOfUnreadMessagesForUser(new AHAHelpContent.UserTypeDetails()
            {
                UserID = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value,
                UserType = AHAHelpContent.UserType.Patient
            }, AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX);

            return iMessageCount.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidSubject(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = true;      
            if (string.IsNullOrEmpty(txtSubject.Text))
            {
                v.IsValid = false;
                ((CustomValidator)sender).ErrorMessage = GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_VALIDATE_SUBJECT).ToString();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidMessage(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = true;    
            if (string.IsNullOrEmpty(txtMailBody.Text))
            {
                v.IsValid = false;
                ((CustomValidator)sender).ErrorMessage = GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_VALIDATE_MESSAGE).ToString();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void validateProvider(object source, ServerValidateEventArgs v)
        {
            v.IsValid = true;
            
            if (string.IsNullOrEmpty(ddlProviders.SelectedValue))
            {
                v.IsValid = false;
                ((CustomValidator)source).ErrorMessage = GetGlobalResourceObject(TRACKER_KEY, TRACKER_KEY_VALIDATE_PROVIDER).ToString();
            }
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="strFolderType"></param>
        /// <param name="strSort"></param>
        /// <param name="bAscending"></param>
        private void _BindMessages(string strFolderType, string strSort, bool bAscending)
        {
            #region binding messages for message folder routine
            List<AHAHelpContent.Message> myMessageList =
                AHAHelpContent.Message.GetAllMessagesByFolderForUser(this.CurrentUserDetails, strFolderType);

            List<Heart360Web.MessageInfo> myMessageInfoList = this.PopulateMessageInfo(myMessageList, strFolderType);
            myMessageInfoList = myMessageInfoList.OrderBy(m => m.SentDate).ToList();
            myMessageInfoList.Reverse();
           
   
            // -----------------------------------------------------
            //  Get message list
            // -----------------------------------------------------            
            //iMessageCount = myMessageList.Count;
            switch(strFolderType)
            {
                    // -----------------------------------------------------
                case AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX:
                    // -----------------------------------------------------               
                    repMessages.DataSource = myMessageInfoList;
                    repMessages.DataBind();
                    break;
                    // -----------------------------------------------------------
                case AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_SENT_MESSAGES:
                    // -----------------------------------------------------------
                    repSent.DataSource = myMessageInfoList;
                    repSent.DataBind();
                    break;
                case AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN:
                    repTrash.DataSource = myMessageInfoList;
                    repTrash.DataBind();
                    break;
                default:
                    break;
            }
            #endregion 

        }
        /// <summary>
        /// 
        /// </summary>
        private List<MessageInfo> PopulateMessageInfo(List<AHAHelpContent.Message> oMsg, string strCurrentFolder)
        {
            #region populate messageinfo with subject and message body
            // ------------------------------------------------------------
            // populate message and replace line brake.
            // ------------------------------------------------------------
            List<MessageInfo> lstMsgInfo = new List<MessageInfo>();
            
            foreach (AHAHelpContent.Message oMessage in oMsg)
            {
                AHAHelpContent.Message objMessage =
                    AHAHelpContent.Message.GetMessageDetails(this.CurrentUserDetails, strCurrentFolder, oMessage.MessageID);

                MessageInfo objMessageInfo = MessageInfo.GetMessageInfoObject(objMessage, this.CurrentUserDetails, strCurrentFolder);
                lstMsgInfo.Add(objMessageInfo);
            }
            #endregion 
            return lstMsgInfo;
        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repMessages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // message inboox repeater 
            BindRepeater(e);
   
        }
        private void BindRepeater(RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                #region bind data element for each message item
                MessageInfo MessageItem = e.Item.DataItem as MessageInfo;
                Literal LitFrom = e.Item.FindControl(UC_LIT_FROM + this.CurrentFolderType) as Literal;  // may be null
                Literal LitTo = e.Item.FindControl(UC_LIT_TO + this.CurrentFolderType) as Literal;      // may be null
                Literal litSubject = e.Item.FindControl(UC_LIT_SUBJECT + this.CurrentFolderType) as Literal;
                Literal litSentDate = e.Item.FindControl(UC_LIT_SENTDATE + this.CurrentFolderType) as Literal;
                Literal litMessageBody = e.Item.FindControl(UC_LIT_MESSAGE + this.CurrentFolderType) as Literal;

                if (this.CurrentFolderType == AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX && !MessageItem.IsRead)
                {
                    LitFrom.Text = "<span class=\"msg-unread\">" + COMPOSE_FROM + MessageItem.From.ToString() + "</span>";
                    litSubject.Text = "<span class=\"msg-unread\">" + ((MessageItem.Subject.Trim().Length > 0) ? COMPOSE_SUBJECT + MessageItem.Subject.Trim() : COMPOSE_SUBJECT + COMPOSE_NO_SUBJECT) + "</span>";

                    System.Web.UI.HtmlControls.HtmlGenericControl trigger = e.Item.FindControl("triggerReadMail") as System.Web.UI.HtmlControls.HtmlGenericControl;
                    if (trigger != null)
                    {
                        //string msgdata = MessageItem.MessageID.ToString() + "|" + CurrentUserDetails.UserID.ToString() ;
                        trigger.Attributes.Add("onclick", "messageUpdater(" + MessageItem.MessageID.ToString() + "," + CurrentUserDetails.UserID.ToString() + ",\"" + trigger.ClientID + "\");");
                    }
                }
                else
                {
                    if( LitTo != null )
                        LitTo.Text = "<span>" + COMPOSE_TO + MessageItem.To.ToString() + "</span>";
                    if( LitFrom != null )
                        LitFrom.Text = "<span>" + COMPOSE_FROM + MessageItem.From.ToString() + "</span>" ;
                    litSubject.Text = "<span>" + ((MessageItem.Subject.Trim().Length > 0) ? COMPOSE_SUBJECT + MessageItem.Subject.Trim() : COMPOSE_SUBJECT + COMPOSE_NO_SUBJECT) + "</span>" ;
                }
                litSentDate.Text = COMPOSE_SENT_DATE + MessageItem.SentDate.ToString();
                litMessageBody.Text = (MessageItem.MessageBody.Trim().Length > 0) ? MessageItem.MessageBody.ToString() : COMPOSE_NO_MESSAGE;
                
                ImageButton imgBtnDelete = e.Item.FindControl(UC_IMG_BTN_DELETE + this.CurrentFolderType) as ImageButton;
                imgBtnDelete.CommandArgument = MessageItem.MessageID.ToString();
                // --------------------------------------------------------------------------------------------
                //   Reply feature is available from inbox folder or sent message folder, but not Trash can
                // --------------------------------------------------------------------------------------------
                ImageButton imgBtnReply = e.Item.FindControl(UC_IMG_BTN_REPLY + this.CurrentFolderType) as ImageButton;
                imgBtnReply.CommandArgument = MessageItem.MessageID.ToString();
                if (this.CurrentFolderType == AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN)
                {
                    imgBtnReply.Visible = false;
                }
                string msgKeys = this.CurrentFolderType + "," + MessageItem.MessageID.ToString();
                

                if (imgBtnDelete != null)
                {
                    // -------------------------------------------------------------------------------------------------------------
                    // set up on client click for deleting a message, below is for triggering a popup form for delete confirmation 
                    // the message keys are : current folder type and message ID with common separated
                    // -------------------------------------------------------------------------------------------------------------
                    string trashUrl = "PopUp.aspx?delete=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER).ToString() + "&elid=" + msgKeys;
                    imgBtnDelete.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent( trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER);
                }
                #endregion
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repSent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // message inboox repeater 
            BindRepeater(e);
        }

        protected void repTrash_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // message trash can repeater 
            BindRepeater(e);
        }

        protected void tabMsg_ActiveTabChanged(object sender, EventArgs e)
        {
            int xxxx = 0;
        }
        /// <summary>
        ///  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnClick_ReplySent(object sender, EventArgs e)
        {
            #region reply a message from the Sent Message Folder
//            tabMsg.ActiveTabIndex = 1;
            mvMsgBoxes.ActiveViewIndex = 1;
            this.CurrentFolderType = AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_SENT_MESSAGES;
            ImageButton btnReply = sender as ImageButton;
            string strMsgID = btnReply.CommandArgument.Trim();
            _PopulateReplyMessage(this.CurrentFolderType, Convert.ToInt16(strMsgID));
            #endregion 

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected void OnClick_ReplyTrash(object sender, EventArgs e)
        {
            #region reply a message in the trash can
            //tabMsg.ActiveTabIndex = 1;
            mvMsgBoxes.ActiveViewIndex = 1;
            this.CurrentFolderType = AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN;
            ImageButton btnReply = sender as ImageButton;
            string strMsgID = btnReply.CommandArgument.Trim();
            _PopulateReplyMessage(this.CurrentFolderType, Convert.ToInt16(strMsgID));
            #endregion 
        }
        /// <summary>
        ///  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnClick_Reply(object sender, EventArgs e)
        {
            #region reply a message in the inbox folder
            // --------------------------------------------------------------------------------------------
            // extract message ID and populate active message, and set up compose tab as the active tab. 
            // ---------------------------------------------------------------------------------------------

            //tabMsg.ActiveTabIndex = 1;
            mvMsgBoxes.ActiveViewIndex = 1;
            this.CurrentFolderType = AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX;
            ImageButton btnReply = sender as ImageButton;
            string strMsgID = btnReply.CommandArgument.Trim();
            _PopulateReplyMessage(this.CurrentFolderType, Convert.ToInt16(strMsgID));
            #endregion 

        }
        /// <summary>
        /// 
        /// </summary>
        protected void OnClick_Cancel(object sender, EventArgs e)
        {
            //when click on Cancel button refresh the compose panel
            _InitializeForm();
        }

        protected void OnClick_TabInbox(object sender, EventArgs e)
        {
            mvMsgBoxes.ActiveViewIndex = 0;
        }
        protected void OnClick_TabCompose(object sender, EventArgs e)
        {
            mvMsgBoxes.ActiveViewIndex = 1;
        }
        protected void OnClick_TabSentbox(object sender, EventArgs e)
        {
            mvMsgBoxes.ActiveViewIndex = 2;
        }
        protected void OnClick_TabTrashbox(object sender, EventArgs e)
        {
            mvMsgBoxes.ActiveViewIndex = 3;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCurrentFolderType"></param>
        /// <param name="iMessageID"></param>
        private void _PopulateReplyMessage(string strCurrentFolderType, int iReplyMessageID)
        {
            // --------------------------------------------------------------
            // populate reply message in the compose panel 
            // --------------------------------------------------------------
            try
            {
                #region extract message content: From, Subject and Existing message body
                // ----------------------------------------------------------
                // extract current message by folder by message ID
                // ----------------------------------------------------------
                AHAHelpContent.Message objMessage =
                    AHAHelpContent.Message.GetMessageDetails(this.CurrentUserDetails, strCurrentFolderType, iReplyMessageID);

                MessageInfo objMessageInfo = MessageInfo.GetMessageInfoObject(objMessage, this.CurrentUserDetails, strCurrentFolderType);
                // initialize Compose form 
                _InitializeForm();
                // ----------------------------------------------------------------------------------
                // set up compose panel cpomtrols: providers dropdownlist, subject and message body.
                // ----------------------------------------------------------------------------------
                txtSubject.Text = COMPOSE_RE + objMessageInfo.Subject;
                // ----------------------------------------------------------------------------------
                // construct previouse message body 
                // ----------------------------------------------------------------------------------
                txtMailBody.Text = COMPOSE_MSG_START + COMPOSE_LINE_BRAKE + COMPOSE_LINE_BRAKE + COMPOSE_FROM + objMessageInfo.From + COMPOSE_LINE_BRAKE
                    + COMPOSE_TO + objMessageInfo.To + COMPOSE_LINE_BRAKE
                    + COMPOSE_SUBJECT + COMPOSE_RE + objMessageInfo.Subject + COMPOSE_LINE_BRAKE
                    + COMPOSE_SENT_DATE + objMessageInfo.SentDate + COMPOSE_LINE_BRAKE
                    + COMPOSE_MSG_START + COMPOSE_LINE_BRAKE 
                    + objMessageInfo.MessageBody.Replace(COMPOSE_HTML_BR, COMPOSE_LINE_BRAKE );
                // ----------------------------------------------------------------------------------
                // populate providers dropdownlist and selected provider
                // ----------------------------------------------------------------------------------

                int iSelectIndex = 0;
                string strProvider = string.Empty;
                switch (this.CurrentFolderType)
                {
                    case AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX:
                        strProvider = objMessageInfo.From.ToString();
                        break;
                    case AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_SENT_MESSAGES:
                        strProvider = objMessageInfo.To.ToString();
                        break;
                    case AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN:
                        strProvider = objMessageInfo.To.ToString();
                        break;
                    default:
                        break;
                }
                bool bProviderFound = false;
                foreach (System.Web.UI.WebControls.ListItem item in ddlProviders.Items)
                {
                    if (item.Text.ToUpper() == strProvider.ToUpper())
                    {
                        ddlProviders.SelectedIndex = iSelectIndex;
                        bProviderFound = true;
                    }
                    iSelectIndex++;
                }
                // -------------------------------------------------------------------------
                // this is only fo rthe message in the trash can only
                // -------------------------------------------------------------------------
                if ((!bProviderFound) & (this.CurrentFolderType == AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN))
                {
                    strProvider = objMessageInfo.From.ToString();
                    foreach (System.Web.UI.WebControls.ListItem item in ddlProviders.Items)
                    {
                        if (item.Text.ToUpper() == strProvider.ToUpper())
                        {
                            ddlProviders.SelectedIndex = iSelectIndex;
                            bProviderFound = true;
                        }
                        iSelectIndex++;
                    }
                }
                #endregion 
            }
            catch (Exception ex)
            {
                PopulateAlertMessage(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        private void PopulateAlertMessage(Exception ex)
        {
            
            litAlert.Text = (ex.InnerException != null) ? ex.Message + "/" + ex.InnerException.Message : ex.Message;
            Timer1.Enabled = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errMsg"></param>
        private void PopulateAlertMessage(string strMsg)
        {
            litAlert.Text = strMsg;
            Timer1.Enabled = true;
        }
        /// <summary>
        /// 
        /// </summary>
        public UpdatePanel UpdateMyMessagePanel()
        {
            // ------------------------------------------
            // return update panel control 
            // ------------------------------------------
            return UpdateMyMessage;

        }

        protected void OnTick_Timer1(object sender, EventArgs e)
        {
            litAlert.Text = string.Empty;
            Timer1.Enabled = false;
        }
    }
}