﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalResources.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.GlobalResources" %>

<section class="global-resources info-stats full">
	<div class="accordion">

		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("Tracker", "Title_GlobalResources")%><span class="gray-arrow"></span></h4>
		</div>

		<div class="content" style="display: none;">
            <p><%=GetGlobalResourceObject("Tracker", "Title_SubTitle_GlobalResources")%></p>

            <asp:UpdatePanel ID="upGlobalResources" runat="server" UpdateMode="Conditional">

                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlGlobal" EventName="SelectedIndexChanged" />
                </Triggers>

                <ContentTemplate>
    
                    <asp:DropDownList ID="ddlGlobal" CssClass="dropdowns" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnTopicChanged"></asp:DropDownList>

                    <div id="divContent" runat="server"></div>
    
                    <a id="learnMore" runat="server" class="gray-button" target="_blank"><%=GetGlobalResourceObject("Tracker", "Text_LearnMore")%></a>

                </ContentTemplate>
            </asp:UpdatePanel>

		</div>
	</div>
</section>