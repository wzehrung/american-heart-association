﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RemindMePopup.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.RemindMePopup" %>

<div class="full">
    <!-- ## Title ## -->
	<div class="large-12 small-12 columns spb">
		<h4 class="full go-blue text-center dspb"><asp:Literal ID="litTitle" runat="server"></asp:Literal></h4>
    </div>

    <asp:MultiView ID="mvViews" runat="server" >
       <asp:View ID="viewOk" runat="server" >
            <div class="row">
	            <div class="large-12 small-12 columns">
		            <label runat="server"><asp:Literal ID="litViewOkDescription" runat="server"></asp:Literal></label>
	            </div>
            </div>
            <div class="row spt">
                <div class="large-6 large-centered small-12 columns">
				    <asp:Button ID="btnViewOkOk" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_OK %>" />
			    </div>
		    </div>
        </asp:View>
        <asp:View ID="viewConfirm" runat="server" >
            <div class="row">
	            <div class="large-12 small-12 columns">
		            <label id="Label2" runat="server"><asp:Literal ID="litViewConfirmDescription" runat="server"></asp:Literal></label>
	            </div>
            </div>
            <div class="row spt">
                <div class="large-3 small-3 columns fl" >
                    <asp:Button ID="btnViewConfirmOk" CssClass="gray-button full" OnClick="btnOk_Click" runat="server" Text="<%$Resources:Common,Text_OK %>" />
                </div>
                <div class="large-3 small-3 columns fl" >
                    <asp:Button ID="btnViewConfirmCancel" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_Cancel%>" />
                </div>          
            </div>
        </asp:View>
        <asp:View ID="viewSmsSetup" runat="server" >
            <div class="row">
	            <div class="large-12 small-12 columns">
		            <label runat="server"><asp:Literal ID="litSmsSetupDescription" runat="server"></asp:Literal></label>
	            </div>
            </div>
            <div class="row">
                <div class="large-12 small-12 columns">
				    <asp:Button CssClass="gray-button full" OnClick="btnSmsSetup_Click" runat="server" Text="<%$Resources:RemindMe,SMS_Button_FirstReminder %>" />
			    </div>
		    </div>
        </asp:View>
    </asp:MultiView>

</div>