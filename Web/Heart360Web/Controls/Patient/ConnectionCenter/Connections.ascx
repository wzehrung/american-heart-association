﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Connections.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.Connections" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<section id="my-providers" class="info-stats full">
	<div class="accordion">
		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_Accordion_Title")%><span class="gray-arrow"></span></h4>
		</div>
		<div class="content">

            <asp:UpdatePanel ID="upanelInvite" UpdateMode="Conditional" runat="server" >
            <ContentTemplate>
			<ul class="inner-accordion">
                <asp:Repeater ID="repeaterProviders" runat="server" OnItemDataBound="OnProvidersDataBind">
                    <ItemTemplate>
                        <li class="provider accepted main-trigger">
						    <div class="trigger-wrap">
							    <div class="inner-trigger">
								    <span class="confirmed-contact"></span>
                                    <span class="provider-contact-name"><asp:Literal ID="litProviderAcceptedName" runat="server"></asp:Literal></span>
								    <span class="contact-number"><asp:Literal ID="litProviderAcceptedPhone" runat="server"></asp:Literal></span>
                                    <span class="blue-arrow"></span>
                                </div>
								<span><asp:ImageButton ID="actProviderAcceptedTrash" runat="server" BorderStyle="None" style="border:none !important;" CssClass="trash" AlternateText="Delete provider" ImageUrl="~/images/1x1.png" CausesValidation="false" /></span>                                  
						    </div>
						    <div class="inner-content">
							    <div class="full">
								    <div class="row spt spb">
									    <div class="large-10 large-offset-2 small-12 columns">
										    <ul class="first">
											    <li>
												    <span>Specialty: </span><span><asp:Literal ID="litProviderAcceptedSpecialty" runat="server" ></asp:Literal></span>
											    </li>
											    <li>
												    <span>Pharmacy Partner: </span><span><asp:Literal ID="litProviderAcceptedPharmaPartner" runat="server" ></asp:Literal></span>
											    </li>
											    <li>
												    <span>Organization Name: </span><span><asp:Literal ID="litProviderAcceptedOrgName" runat="server" ></asp:Literal></span>
											    </li>
										    </ul>
									    </div>
								    </div>
								    <div class="row spb">
									    <div class="large-10 large-offset-2 small-12 columns">
										    <ul class="first">
											    <li>
												    <span>Contact Phone: </span><span><asp:Literal ID="litProviderAcceptedContactPhone" runat="server"></asp:Literal></span>
											    </li>
											    <li>
												    <span>Contact Fax: </span><span><asp:Literal ID="litProviderAcceptedContactFax" runat="server"></asp:Literal></span>
											    </li>
											    <li>
												    <span>Organization Address: </span><span><asp:Literal ID="litProviderAcceptedOrgAddr" runat="server"></asp:Literal></span>
											    </li>
											    <li>
												    <span>Email: </span><span><asp:Literal ID="litProviderAcceptedEmail" runat="server" ></asp:Literal></span>
											    </li>
											    <li>
												    <span>Website: </span><span><asp:Literal ID="litProviderAcceptedWebsite" runat="server" ></asp:Literal></span>
											    </li>
										    </ul>
									    </div>
								    </div>
								    <div class="row spb">
									    <div class="large-10 large-offset-2 small-12 columns">
										    <ul class="first">
											    <li>
												    <span>City: </span><span><asp:Literal ID="litProviderAcceptedCity" runat="server" ></asp:Literal></span>
											    </li>
											    <li>
												    <span>State: </span><span><asp:Literal ID="litProviderAcceptedState" runat="server" ></asp:Literal></span>
											    </li>
											    <li>
												    <span>Zip Code: </span><span><asp:Literal ID="litProviderAcceptedZip" runat="server" ></asp:Literal></span>
											    </li>
										    </ul>
									    </div>
								    </div>
                                    <div class="row spb">
									    <div class="large-10 large-offset-2 small-12 columns">
                                            <ul class="first">
                                                <li>
                                                    <span>Message: </span><span><asp:Literal ID="litProviderMessage" runat="server" ></asp:Literal></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
							    </div>
						    </div>
					    </li>
                    </ItemTemplate>
                </asp:Repeater>

                <asp:Repeater ID="repeaterPending" runat="server" OnItemDataBound="OnPendingDataBind" >
                    <ItemTemplate>
                        <li class="provider pending">
							<div class="trigger-wrap">
								<div class="inner-trigger">
									<span class="pending-contact"></span>
                                    <span class="provider-contact-name"><asp:Literal ID="litProviderPendingName" runat="server" ></asp:Literal></span>
									<span class="contact-number"><asp:Literal ID="litProviderPendingPhone" runat="server" ></asp:Literal></span>
                                    <span class="blue-arrow"></span>
								</div>
							</div>
							<div class="inner-content">
								<div class="full">
									<div class="row"><!-- this was a form, perhaps we can remove it -->
										<div class="large-12 small-12 columns">
											<p><%=GetGlobalResourceObject("ConnCenterPAndV", "Patient_AcceptInvitation_AgreeToTerms")%></p>
										</div>
										<div class="large-12 small-12 columns">
											<label for="terms"><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_Disclaimer")%></label>
										</div>
                                        <div class="large-12 small-12 columns spb" >
                                            <asp:ValidationSummary ID="valSummaryNewTerms" ValidationGroup="Terms"
                                                HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                Enabled="true"
                                                CssClass="validation-error"
                                                DisplayMode="BulletList"
                                                ShowSummary="true"
                                                runat="server" /> 
                                        </div>
										<div class="large-12 small-12 columns">
											<p style="border-style: solid; border-width:thin;"><%=GetGlobalResourceObject("ConnCenterPAndV", "Patient_AcceptInvitation_TermsOfUse")%></p>
										</div>
										<div class="large-12 small-12 columns spb">
                                            <asp:CheckBox ID="ckbProviderPendingAgree" runat="server" Text="<%$Resources:ConnCenterPAndV,Text_Agree_Disclaimer %>" CssClass="full cbox" /> 
										</div>
										<div class="large-4 small-12 columns fl spb">
                                            <asp:Button id="btnProviderPendingGrantAccess" runat="server" CssClass="gray-button full" OnClick="btnGrantAccess_Click" Text="<%$Resources:ConnCenterPAndV,Text_GrantAccess %>" />											
										</div>
										<div class="large-4 small-12 columns fl spb">
											<asp:Button id="btnProviderPendingNoAccess" runat="server" CssClass="gray-button full" OnClick="btnNoAccess_Click" Text="<%$Resources:ConnCenterPAndV,Text_DoNotAllowAccess %>" />
										</div>
										<div class="large-4 small-12 columns fl spb">
											<asp:Button id="btnProviderPendingRemindMeLater" runat="server" CssClass="gray-button full" OnClick="btnRemindMeLater_Click" Text="<%$Resources:ConnCenterPAndV,Text_RemindMeLater %>" />
										</div>
                                        <asp:CustomValidator ID="validNewTermsFields" runat="server" EnableClientScript="false" ValidationGroup="" Display="None" OnServerValidate="ValidateNewTermsFields"></asp:CustomValidator>
									</div>
								</div>
							</div>
						</li>
				    </ItemTemplate>
                </asp:Repeater>
                
                <li id="invite-provider">
                    <div id="divInvitation" runat="server" class="asp-container-start">
                        <!-- ## BEGIN ## -->

                        <ul style="margin: 0px;" class="asp-container-ul">
                            <li style="margin: 0px;" class="asp-container-li">
	                            <asp:Accordion ID="accInvitation" runat="server"
                                    RequireOpenedPane="false"
                                    Visible="true"
                                    AutoSize="None"
                                    SelectedIndex="-1"
                                    CssClass="accordionChild"  
                                    HeaderCssClass="accordionChildHeader"
                                    HeaderSelectedCssClass="accordionChildHeaderSelected"
                                    ContentCssClass="accordionChildContent"
                                    FadeTransitions="true"
                                    FramesPerSecond="30"
                                    TransitionDuration="300">
                                    <Panes>
                                        <asp:AccordionPane ID="accPaneInvitation" runat="server" >
                                            <Header>
	                                            <div class="trigger-wrap">
		                                            <div class="accordionChildHeaderText">
                                                        <span class="add-contact"></span>
                                                        <span class="provider-contact-name"><%=GetGlobalResourceObject("ConnCenterPAndV","Text_Invite_PV") %></span>
                                                        <span class="blue-arrow"></span>                                                   
                                                    </div>
	                                            </div>
                                            </Header>
                                            <Content>
                                                <asp:UpdatePanel ID="upanelTab" UpdateMode="Conditional" runat="server" >
                                                <ContentTemplate>
                                                    <div class="full spb" >

                                                        <div class="infoBox">
                                                            <%=GetGlobalResourceObject("ConnCenterPAndV", "Text_Connections_Overview")%>
                                                        </div>

                                                        <asp:TabContainer ID="tabcInvite" runat="server" ActiveTabIndex="0" >
                                                            <asp:TabPanel ID="tabpTerms" runat="server"  >
                                                                <HeaderTemplate>
                                                                    <span class="tab-title">TERMS &amp; CON</span> 
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <section class="tab-content-inv terms-con active-content">
											                            <div class="full">
												                            <div class="row">
													                            <div class="large-12 small-12 columns">
														                            <h3>TERMS AND CONDITIONS</h3>
													                            </div>
                                                                                <div class="large-12 small-12 columns spb" >
                                                                                    <asp:ValidationSummary ID="valSummaryTerms" ValidationGroup="Terms"
                                                                                        HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                                                        Enabled="true"
                                                                                        CssClass="validation-error"
                                                                                        DisplayMode="BulletList"
                                                                                        ShowSummary="true"
                                                                                        runat="server" /> 
                                                                                </div>
													                            <div class="providers-terms-and-con large-12 small-12 columns">
														                            <p style="border-style: solid; border-width:thin;"><asp:Literal ID="litTermsOfUse" runat="server" /></p>
													                            </div>
													                            <div class="large-12 small-12 columns spb">
                                                                                    <asp:CheckBox ID="ckbAgree" runat="server" Text="<%$Resources:ConnCenterPAndV,Text_Read_And_Agree %>" CssClass="full cbox" />   
													                            </div>
													                            <div class="large-4 small-12 columns fl spb">
                                                                                    <asp:Button id="btnTermsContinue" runat="server" CssClass="gray-button full" OnClick="btnTermsContinue_Click" Text="<%$Resources:Common,Text_Continue %>" />
													                            </div>
												                            </div>
											                            </div>	
										                            </section>
                                                                    <asp:CustomValidator ID="validTermsFields" runat="server" EnableClientScript="false" ValidationGroup="Terms" Display="None" OnServerValidate="ValidateTermsFields"></asp:CustomValidator>
                                                                </ContentTemplate>
                                                            </asp:TabPanel>
                                                            <asp:TabPanel ID="tabpByName" runat="server" >
                                                                <HeaderTemplate>
                                                                    <span class="tab-title">BY SEARCH</span> 
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <section class="tab-content-inv by-name active-content">
											                            <div class="full">
												                            <div class="row">
													                            <div class="large-12 small-12 columns">
														                            <h3><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_ByName_Description")%></h3>
													                            </div>
                                                                                <div class="large-12 small-12 columns" >
                                                                                    <asp:UpdateProgress ID="UpdateProgressBySearch" AssociatedUpdatePanelID="upanelTab" runat="server" DisplayAfter="500" >
                                                                                        <ProgressTemplate>
                                                                                            <div class="large-1 large-centered small-2 small-centered columns">
                                                                                                <asp:Image ID="Image1" runat="server" ImageAlign="Middle" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                                                            </div>
                                                                                        </ProgressTemplate>
                                                                                    </asp:UpdateProgress>
                                                                                </div>
                                                                                <div class="large-12 small-12 columns spb" >
                                                                                    <asp:ValidationSummary ID="valSummaryByName" ValidationGroup="ByName"
                                                                                        HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                                                        Enabled="true"
                                                                                        CssClass="validation-error"
                                                                                        DisplayMode="BulletList"
                                                                                        ShowSummary="true"
                                                                                        runat="server" /> 
                                                                                </div>
													                            <div class="large-12 small-12 columns">
														                            <div class="full-name row">
															                            <div class="large-6 small-12 columns">
                                                                                            <asp:TextBox ID="tboxFirstName" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="weFirstName" runat="server" TargetControlID="tboxFirstName" WatermarkText="First Name" />
															                            </div>
															                            <div class="large-6 columns">
                                                                                            <asp:TextBox ID="tboxLastName" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="weLastName" runat="server" TargetControlID="tboxLastName" WatermarkText="Last Name" />
															                            </div>
														                            </div>
													                            </div>
													                            <div class="large-12 small-12 columns">
														                            <div class="organization-name row">
															                            <div class="large-6 small-12 columns">
                                                                                            <asp:TextBox ID="tboxOrgName" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="weOrgName" runat="server" TargetControlID="tboxOrgName" WatermarkText="Organization Name" />
															                            </div>
														                            </div>
													                            </div>
													                            <div class="large-12 small-12 columns">
														                            <div class="my-area row">
															                            <div class="large-6 small-12 columns">
                                                                                            <asp:TextBox ID="tboxCity" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="weCity" runat="server" TargetControlID="tboxCity" WatermarkText="City" />
															                            </div>
															                            <div class="large-6 small-12 columns">
																                            <div class="large-6 small-12 columns">
                                                                                                    <asp:DropDownList ID="ddlState" runat="server" CssClass="dropdowns full" /> 
																                            </div>
																                            <div class="large-6 small-12 columns">

                                                                                                    <asp:TextBox ID="tboxZip" CssClass="required full" runat="server" />
                                                                                                    <asp:TextBoxWatermarkExtender ID="weZip" runat="server" TargetControlID="tboxZip" WatermarkText="Zip" />
																	                           
																                            </div>
															                            </div>
														                            </div>
													                            </div>
													                            <div class="large-12 small-12 columns">
														                            <div class="my-contact-info row">
															                            <div class="large-6 small-12 columns">
                                                                                            <asp:TextBox ID="tboxPhone" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="wePhone" runat="server" TargetControlID="tboxPhone" WatermarkText="Contact Phone" />
															                            </div>
															                            <div class="large-6 columns">
                                                                                            <asp:TextBox ID="tboxEmail" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="weEmail" runat="server" TargetControlID="tboxEmail" WatermarkText="Email" />
															                            </div>
														                            </div>
													                            </div>
													                            <div class="large-12 small-12 columns fl spb spt">
														                            <div class="row">
															                            <div class="large-6 small-12 columns">
                                                                                            <asp:Button ID="btnByNameSearch" CssClass="gray-button full" runat="server" OnClick="btnByNameSearch_Click" Text="<%$Resources:Common,Text_Search %>" />
															                            </div>
															                            <div class="large-6 small-12 columns">
                                                                                            <asp:Button ID="btnByNameClear" CssClass="gray-button full" runat="server" OnClick="btnByNameClear_Click" Text="<%$Resources:Common,Text_Clear %>" />
															                            </div>
														                            </div>
													                            </div>															
												                            </div>
											                            </div>	
										                            </section>
                                                                    <asp:CustomValidator ID="validByNameSearchFields" runat="server" EnableClientScript="false" ValidationGroup="ByName" Display="None" OnServerValidate="ValidateByNameSearchFields"></asp:CustomValidator>
                                                                </ContentTemplate>
                                                            </asp:TabPanel>
                                                            <asp:TabPanel ID="tabpByCode" runat="server" >
                                                                <HeaderTemplate>
                                                                    <span class="tab-title">BY CODE</span> 
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <section class="tab-content-inv by-code">
											                            <div class="full">
												                            <div class="row">
													                            <div class="large-12 small-12 columns">
														                            <h3><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_ByCode_Description")%></h3>
													                            </div>
                                                                                <div class="large-12 small-12 columns" >
                                                                                    <asp:UpdateProgress ID="UpdateProgressByCode" AssociatedUpdatePanelID="upanelTab" runat="server" DisplayAfter="500" >
                                                                                        <ProgressTemplate>
                                                                                            <div class="large-1 large-centered small-2 small-centered columns">
                                                                                                <asp:Image ID="Image1" runat="server" ImageAlign="Middle" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                                                            </div>
                                                                                        </ProgressTemplate>
                                                                                    </asp:UpdateProgress>
                                                                                </div>
                                                                                <div class="large-12 small-12 columns spb" >
                                                                                    <asp:ValidationSummary ID="valSummaryByCode" ValidationGroup="ByCode"
                                                                                        HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                                                        Enabled="true"
                                                                                        CssClass="validation-error"
                                                                                        DisplayMode="BulletList"
                                                                                        ShowSummary="true"
                                                                                        runat="server" /> 
                                                                                </div>
													                            <div class="large-6 small-12 columns fl dspb">
                                                                                    <asp:TextBox ID="tboxCode" CssClass="match-button required full" runat="server" />
                                                                                    <asp:TextBoxWatermarkExtender ID="weCode" runat="server" TargetControlID="tboxCode" WatermarkText="Enter A Provider/Volunteer Code" />
													                            </div>
													                            <div class="large-6 small-12 columns fl spb">
                                                                                    <asp:Button ID="btnByCodeConnect" runat="server" CssClass="gray-button full" Text="<%$Resources:ConnCenterPAndV,Text_Connect %>" OnClick="btnByCodeConnect_Click" />
													                            </div>															
												                            </div>
											                            </div>	
										                            </section>
                                                                    <asp:CustomValidator ID="validByCodeCode" runat="server" EnableClientScript="false" ValidationGroup="ByCode" Display="None" OnServerValidate="ValidateByCodeCode"></asp:CustomValidator>
                                                                </ContentTemplate>
                                                            </asp:TabPanel>
                                                            <asp:TabPanel ID="tabpByEmail" runat="server" >
                                                                <HeaderTemplate>
                                                                    <span class="tab-title">BY EMAIL</span> 
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <section class="tab-content-inv by-email">
													                    <div class="full">
															                <div class="large-12 small-12 columns">
																                <h3><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_ByEmail_Description")%></h3>
															                </div>
                                                                            <div class="large-12 small-12 columns" >
                                                                                <asp:UpdateProgress ID="UpdateProgressByEmail" AssociatedUpdatePanelID="upanelTab" runat="server" DisplayAfter="500" >
                                                                                    <ProgressTemplate>
                                                                                        <div class="large-1 large-centered small-2 small-centered columns">
                                                                                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                                                        </div>
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </div>
                                                                            <div class="large-12 small-12 columns spb" >
                                                                                <asp:ValidationSummary ID="valSummaryByEmail" ValidationGroup="ByEmail"
                                                                                        HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                                                        Enabled="true"
                                                                                        CssClass="validation-error"
                                                                                        DisplayMode="BulletList"
                                                                                        ShowSummary="true"
                                                                                        runat="server" /> 
                                                                            </div>
															                <div class="large-12 small-12 columns fl">
																                <div class="row">
																	                <div class="large-6 small-12 fl">
																		               
																			                <div class="large-4 small-12 columns">
                                                                                                <asp:DropDownList ID="ddlEmailSalutation" runat="server" CssClass="dropdowns full" />
																			                </div>
																			                <div class="large-8 small-12 columns">
                                                                                                <asp:TextBox ID="tboxEmailFname" CssClass="required full" runat="server" />
                                                                                                <asp:TextBoxWatermarkExtender ID="weEmailFname" runat="server" TargetControlID="tboxEmailFname" WatermarkText="First Name" />
                                                                                                <asp:CustomValidator ID="cvByEmailFname" runat="server" EnableClientScript="false" ValidationGroup="ByEmail" Display="None" OnServerValidate="ValidateByEmailFname"></asp:CustomValidator>	
																			                </div>
																		               
																	                </div>
																	                <div class="large-6 small-12 columns fl">
                                                                                        <asp:TextBox ID="tboxEmailLname" CssClass="required full" runat="server" />
                                                                                        <asp:TextBoxWatermarkExtender ID="weEmailLname" runat="server" TargetControlID="tboxEmailLname" WatermarkText="Last Name" />
                                                                                        <asp:CustomValidator ID="cvByEmailLname" runat="server" EnableClientScript="false" ValidationGroup="ByEmail" Display="None" OnServerValidate="ValidateByEmailLname"></asp:CustomValidator>											                                                 
																	                </div>
                                                                                </div>
                                                                                <div class="large-12 small-12 columns">
																	                <div class="large-6 small-12 columns fl">
                                                                                        <asp:TextBox ID="tboxEmailAddr" CssClass="required full" runat="server" />
                                                                                        <asp:TextBoxWatermarkExtender ID="weEmailAddr" runat="server" TargetControlID="tboxEmailAddr" WatermarkText="Email" />
																	                </div>
                                                                                </div>
                                                                                <div class="large-12 small-12 columns">
																	                <div class="large-6 small-12 columns fl">
                                                                                        <asp:TextBox ID="tboxEmailAddrConfirm" CssClass="required full" runat="server" />
                                                                                        <asp:TextBoxWatermarkExtender ID="weEmailAddrConfirm" runat="server" TargetControlID="tboxEmailAddrConfirm" WatermarkText="Confirm Email" />
																	                </div>
                                                                                </div>
															                </div>
															                <div class="large-6 small-12 columns fl spb">
                                                                                 <div class="large-12 small-12 columns">
                                                                                    <div class="row">
                                                                                        <asp:Button ID="btnEmailInvite" runat="server" CssClass="gray-button full" Text="<%$Resources:ConnCenterPAndV,Text_Invite %>" OnClick="btnByEmailInvite_Click" />
															                        </div>
                                                                                 </div>
                                                                            </div>
                                                                            <asp:CustomValidator ID="cvByEmailConfirmAddr" runat="server" EnableClientScript="false" ValidationGroup="ByEmail" Display="None" OnServerValidate="ValidateByEmailAddrConfirm"></asp:CustomValidator>												
													                    </div>	
												                    </section>
                                                                </ContentTemplate>
                                                            </asp:TabPanel>
                                                            <asp:TabPanel ID="tabpByPrint" runat="server" >
                                                                <HeaderTemplate>
                                                                    <span class="tab-title">BY PRINT</span> 
                                                                </HeaderTemplate>
                                                                <ContentTemplate>
                                                                    <section class="tab-content-inv by-print">
													                    <div class="full">
															                <div class="large-12 small-12 columns">
																                <h3><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_ByPrint_Description")%></h3>
															                </div>
                                                                             <div class="large-12 small-12 columns spb" >
                                                                                <asp:ValidationSummary ID="valSummaryByPrint" ValidationGroup="ByPrint"
                                                                                        HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                                                        Enabled="true"
                                                                                        CssClass="validation-error"
                                                                                        DisplayMode="BulletList"
                                                                                        ShowSummary="true"
                                                                                        runat="server" /> 
                                                                            </div>
															                <div class="large-12 small-12 columns fl">
																                <div class="row">
																	                <div class="large-6 small-12 columns fl">
																			            <div class="large-4 small-12 fl">
                                                                                            <asp:DropDownList ID="ddlPrintSalutation" runat="server" CssClass="dropdowns full" />
																			            </div>
																			            <div class="large-8 small-12 columns fl">
                                                                                            <asp:TextBox ID="tboxPrintFname" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="wePrintFname" runat="server" TargetControlID="tboxPrintFname" WatermarkText="First Name" />
                                                                                            <asp:CustomValidator ID="cvByPrintFname" runat="server" EnableClientScript="false" ValidationGroup="ByPrint" Display="None" OnServerValidate="ValidateByPrintFname"></asp:CustomValidator>
																			            </div>
																	                </div>
																	                <div class="large-6 small-12 columns fl">
                                                                                            <asp:TextBox ID="tboxPrintLname" CssClass="required full" runat="server" />
                                                                                            <asp:TextBoxWatermarkExtender ID="wePrintLname" runat="server" TargetControlID="tboxPrintLname" WatermarkText="Last Name" />
                                                                                            <asp:CustomValidator ID="cvByPrintLname" runat="server" EnableClientScript="false" ValidationGroup="ByPrint" Display="None" OnServerValidate="ValidateByPrintLname"></asp:CustomValidator>	
                                                                                    </div>
																                </div>
															                </div>
															                <div class="large-12 small-12 columns fl spb">
																                <div class="row">
																	                <div class="large-6 small-12 columns fl">
                                                                                        <asp:Button ID="btnPrintPreview" runat="server" CssClass="gray-button full" Text="<%$Resources:ConnCenterPAndV,Text_Preview %>" OnClick="btnByPrintPreview_Click" />
																	                </div>
																                </div>

                                                                                <div class="infoBoxByPrint">
                                                                                    <%=GetGlobalResourceObject("ConnCenterPAndV", "Text_Connections_Overview_ByPrint")%>
                                                                                </div>
															                </div>															
													                    </div>	
												                    </section>
                                                                </ContentTemplate>
                                                            </asp:TabPanel>
                                                        </asp:TabContainer>
                                                    </div>
                                                    <asp:HiddenField ID="hidTabState" runat="server" EnableViewState="true" />
                                                </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </Content>
                                        </asp:AccordionPane>
                                    </Panes>
                                </asp:Accordion>
                            </li>
                        </ul>
                        <!-- ## END ## -->
                    </div>           
                </li>
			</ul>
            <asp:HiddenField ID="hidPendingState" runat="server" EnableViewState="true" />
            </ContentTemplate>
            </asp:UpdatePanel>
		</div>
	</div>
    
</section>