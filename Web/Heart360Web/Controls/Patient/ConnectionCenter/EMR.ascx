﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EMR.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.EMRControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<section id="my-emr" class="info-stats full">
	<div class="accordion">

		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("EMR", "Title_ClinicConnect")%><span class="gray-arrow"></span></h4>
		</div>

		<div class="content" style="display: block;">

            <asp:UpdatePanel ID="upanelClinicConnect" UpdateMode="Conditional" runat="server"  >

                <ContentTemplate>

                    <ul class="inner-accordion">

                        <li id="invite-provider">

                            <div id="divInvitation" runat="server" class="asp-container-start">

                                <div class="full spb" >

                                    <div class="large-12 small-12 columns spb" >
                                        <asp:ValidationSummary 
                                            ID="valSummary01"
                                            ValidationGroup="ValidateKeys"
                                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                            Enabled="true"
                                            CssClass="validation-error"
                                            DisplayMode="BulletList"
                                            ShowSummary="true"
                                            runat="server" />   

                                        <asp:ValidationSummary 
                                            ID="valSummary02"
                                            ValidationGroup="ValidateChallenge"
                                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                            Enabled="true"
                                            CssClass="validation-error"
                                            DisplayMode="BulletList"
                                            ShowSummary="true"
                                            runat="server" />  

                                        <asp:ValidationSummary 
                                            ID="valSummary03"
                                            ValidationGroup="ValidateTerms"
                                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                            Enabled="true"
                                            CssClass="validation-error"
                                            DisplayMode="BulletList"
                                            ShowSummary="true"
                                            runat="server" />  
                                    </div>

                                    <div id="divConnection" runat="server" class="asp-container-start">
                                        <asp:MultiView ID="mvEMR" runat="server" ActiveViewIndex="0">

                                            <!-- ## BEGIN START ## -->
                                            <asp:View ID="viewStart" runat="server">
                                                <div class="infoBox" style="margin-top: 10px;">
                                                    <%=GetGlobalResourceObject("EMR", "Text_EMR_Overview")%>
                                                </div>
                                                <asp:Button ID="btnStart" runat="server" Text="<%$Resources:EMR,Button_Start %>" CssClass="gray-button full" OnClick="btnStart_Click" />
                                            </asp:View>
                                            <!-- ## END START ## -->

	                                        <!-- ## BEGIN STEP 1: Enter in Connection Key and Organization ID codes ## -->
                                            <asp:View ID="viewStep1" runat="server">

                                                <p><strong><%=GetGlobalResourceObject("EMR", "Text_Enter_Information")%></strong></p><br />

                                                <div class="row" >
                                                    <div class="large-6 small-6 columns" >
                                                        <p><%=GetGlobalResourceObject("EMR", "Title_ConnectionKey")%></p>
                                                        <asp:TextBox ID="txtConnectionKey" MaxLength="16" Width="100%" runat="server"></asp:TextBox>
                                                    </div>
                                                    <div class="large-6 small-6 columns" >
                                                        <p><%=GetGlobalResourceObject("EMR", "Title_OrganizationID")%></p>
                                                        <asp:TextBox ID="txtOrganizationID" MaxLength="16" Width="100%" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <asp:Button ID="btnSubmitKeys" runat="server" Text="<%$Resources:EMR,Button_Continue %>" CssClass="gray-button full" OnClick="btnSubmitKeys_Click" />

                                            </asp:View>
                                            <!-- ## END STEP 1 ## -->

                                            <!-- ## BEGIN STEP 2: Enter answer to secret question and accept terms ## -->
                                            <asp:View ID="viewStep2" runat="server">

                                                <p>
                                                    Connection Key:&nbsp;<strong><asp:Label runat="server" ID="lblRegistrationKey"></asp:Label></strong><br />
                                                    Organization ID:&nbsp;<strong><asp:Label runat="server" ID="lblOrganizationID"></asp:Label></strong><br />
                                                    Organization Name:&nbsp;<strong><asp:Label runat="server" ID="lblOrganizationName"></asp:Label></strong><br />
                                                    Request Date:&nbsp;<strong><asp:Label runat="server" ID="lblRequestDate"></asp:Label></strong>
                                                </p>
                                        
                                                <p><strong><%=GetGlobalResourceObject("EMR", "Text_AnswerSecretQuestion")%></strong></p><br />
                                                <p>Question: <strong><asp:Label runat="server" ID="lblQuestion" /></p></strong>
                                                <asp:TextBox ID="txtAnswer" MaxLength="32" Width="100%" runat="server"></asp:TextBox>

                                                <div class="large-12 small-12 columns">
                                                    <p style="border-style: solid; border-width:thin; line-height: 21px; padding:4px;"><%=GetGlobalResourceObject("EMR", "Text_TermsAndConditions")%></p>
                                                </div>

                                                <div class="large-12 small-12 columns spb">
                                                    <asp:CheckBox ID="chkAgree" runat="server" Text="<%$Resources:EMR,Checkbox_Agree %>" CssClass="full cbox" />
                                                </div>
                                                
                                                <div class="large-4 small-12 columns fl spb">
                                                        <asp:Button ID="btnSubmit" runat="server" Text="<%$Resources:EMR,Button_Submit %>" CssClass="gray-button full" OnClick="btnSubmit_Click" />
                                                </div>

                                                <div class="large-4 small-12 columns fl spb">
                                                    <asp:Button ID="btnBackToStep1" runat="server" Text="<%$Resources:EMR,Button_Back %>" CssClass="gray-button full" OnClick="btnBacktoStep1_Click" />
                                                </div>
                                            </asp:View>
                                            <!-- ## BEGIN STEP 2 ## -->

                                            <!-- ## BEGIN STEP 3: Connection successful ## -->
                                            <asp:View ID="viewStep3" runat="server">
                                                <div class="infoBox" style="margin-top: 10px;">
                                                    <%=GetGlobalResourceObject("EMR", "Text_Connection_Successful")%>
                                                </div>
                                            </asp:View>
                                            <!-- ## BEGIN STEP 3 ## -->

                                        </asp:MultiView>
                                    </div>           

                                </div>

                                <div class="large-12 small-12 columns" >
                                    <asp:UpdateProgress ID="upConnect" AssociatedUpdatePanelID="upanelClinicConnect" runat="server">
                                        <ProgressTemplate>
                                            <div class="row">
                                                <div class="large-4 small-12 columns">
                                                    <asp:Image ID="Image1" runat="server" ImageAlign="right" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                </div>
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>   
                                </div>

                            </div>           
                        </li>
			        </ul>

                </ContentTemplate>
            
            </asp:UpdatePanel>

            <!-- ### Current Connections ### -->
            <asp:UpdatePanel ID="upanelEMRConnections" UpdateMode="Conditional" runat="server" Visible="true"  ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div class="row" >
                        <div class="large-12 small-12 columns" >
                            <div class="infoBox" style="margin-top: 10px;">
                                <%=GetGlobalResourceObject("EMR", "Title_CurrentConnections")%>
                            </div>
                        </div>

                        <div class="large-12 small-12 columns" >
                            <asp:Repeater ID="rptEMRConnections" EnableViewState="true" runat="server" OnItemDataBound="rptEMRConnections_ItemDataBound">
                                <HeaderTemplate>
    	                                 <table style="width: 100%">
		                                    <thead>
			                                    <tr>
				                                    <th class="tt-date"><%=GetGlobalResourceObject("EMR", "Title_Organization")%></th>
				                                    <th class="tt-time"><%=GetGlobalResourceObject("EMR", "Title_ConnectionDate")%></th>
			                                    </tr>
		                                    </thead>
                                            <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
			                            <td><asp:Literal ID="litClinicName" runat="server" ></asp:Literal></td>
			                            <td><asp:Literal ID="litConnectionDate" runat="server" ></asp:Literal></td>
		                            </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                            </tbody>
	                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <!-- ### Current Records ### -->
            <asp:UpdatePanel ID="upanelRecords" UpdateMode="Conditional" runat="server" Visible="true"  ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div class="row" >
                        <div class="large-12 small-12 columns" >
                            <div class="infoBox" style="margin-top: 10px;">
                                <%=GetGlobalResourceObject("EMR", "Title_CurrentRecords")%>
                            </div>
                        </div>

                        <div class="large-12 small-12 columns" >
                            <asp:Repeater ID="rptRecords" EnableViewState="true" runat="server" OnItemDataBound="rptEMRRecords_ItemDataBound">
                                <HeaderTemplate>
    	                                 <table style="width: 100%">
		                                    <thead>
			                                    <tr>
				                                    <th class="tt-date"><%=GetGlobalResourceObject("EMR", "Title_Organization")%></th>
				                                    <th class="tt-time"><%=GetGlobalResourceObject("EMR", "Title_RecordDate")%></th>
                                                    <th class="tt-time"><%=GetGlobalResourceObject("EMR", "Title_Record")%></th>
                                                    <th></th>
			                                    </tr>
		                                    </thead>
                                            <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
			                            <td><asp:Literal ID="litRecordClinicName" runat="server" ></asp:Literal></td>
			                            <td><asp:Literal ID="litRecordDate" runat="server" ></asp:Literal></td>
                                        <td><asp:LinkButton ID="popupEMR" CssClass="add-this add-reading" data-name="generic" runat="server"></asp:LinkButton></td>
                                        <td>

                                            <asp:Panel ID="pnlXmlDocument" runat="server" Visible="true" style="display: none;">

                                                <div class="popupStyle">

                                                    <div id="divPopupTrackerStyle" runat="server" visible="true">
                                                        <div class="closePopup">
                                                            <asp:LinkButton ID="btnLinkClose" OnClick="btnLinkClose_OnClick" CssClass="closePopupText" runat="server" Text="CLOSE" />
                                                        </div>
                                                    </div>

                                                    <div class="full">

                                                        <asp:Xml ID="xmlDocument" runat="server"></asp:Xml>

                                                    </div>

                                                </div>

                                            </asp:Panel>

                                            <asp:ModalPopupExtender
                                                ID="mpeDisplayEMR"
                                                runat="server" 
                                                PopupControlID="pnlXmlDocument" TargetControlID="popupEMR" CancelControlID="btnLinkClose" OkControlID="btnLinkClose" BackgroundCssClass="pageBackground">
                                            </asp:ModalPopupExtender>

                                        </td>
		                            </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                            </tbody>
	                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:CustomValidator
                ValidationGroup="ValidateChallenge"
                ID="validAnswer"
                runat="server"
                Display="None"
                EnableClientScript="false"
                OnServerValidate="ValidateChallengeAnswer">
            </asp:CustomValidator>

            <asp:CustomValidator
                ValidationGroup="ValidateKeys"
                ID="validKeys"
                runat="server"
                Display="None"
                EnableClientScript="false"
                OnServerValidate="ValidateConnectionKeys">
            </asp:CustomValidator> 

            <asp:CustomValidator
                ValidationGroup="ValidateChallenge"
                ID="validAccept"
                runat="server"
                Display="None"
                EnableClientScript="false"
                OnServerValidate="ValidateAcceptTerms">
            </asp:CustomValidator>   

		</div>
	</div>
    
</section>

<asp:HiddenField ID="hidPatientID" runat="server" EnableViewState="true" />