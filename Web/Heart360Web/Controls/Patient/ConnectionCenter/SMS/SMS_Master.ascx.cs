﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAHelpContent;
using AHAAPI ;

namespace Heart360Web.Controls.Patient.ConnectionCenter.SMS
{
    public partial class SMS_Master : System.Web.UI.UserControl
    {
        //
        // Private helper functions
        //
        #region private helper functions

        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }

        private int NumberOfScheduledReminders
        {
            get
            {
                PatientReminder objPatientReminder = AHAHelpContent.PatientReminder.GetPatientReminders(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID);
                List<PatientReminderSchedule> objSchedules = PatientReminderSchedule.GetPatientReminderSchedules(objPatientReminder.ReminderID);

                return objSchedules.Count;
            }
        }

        private PatientReminder getPatientReminder()
        {
            PatientReminder objPatientReminder = AHAHelpContent.PatientReminder.GetPatientReminders(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID);
            return objPatientReminder;
        }

        private AHAHelpContent.PhoneNumbers getPhoneNumbers()
        {
            return AHAHelpContent.PhoneNumbers.GetPhoneNumbers(CurrentPatient.UserHealthRecordID);
        }

        private string GetPhoneMobileCurrent( AHAHelpContent.PhoneNumbers objPhoneNumbers )
        {
            return (objPhoneNumbers.PhoneMobile != null) ? objPhoneNumbers.PhoneMobile : string.Empty ;
        }

        private string GetPhoneSMSCurrent( AHAHelpContent.PhoneNumbers objPhoneNumbers )
        {
            string sPhoneSMS = string.Empty;

            if (objPhoneNumbers.PhoneSMS != null)
            {
                sPhoneSMS = objPhoneNumbers.PhoneSMS;
                sPhoneSMS = sPhoneSMS.Remove(0, 1);     // Remove the 1 (country code) at the beginning
            }
            return sPhoneSMS;
        }

        private string SpecifiedMobilePhone
        {
            get
            {
                string retval = txtPhoneMobile_AreaCode.Text + txtPhoneMobile_Prefix.Text + txtPhoneMobile_Suffix.Text;
                return retval;
            }
        }

        private string PhoneAreaCode(string phonenb)
        {
            return (!string.IsNullOrEmpty(phonenb)) ? phonenb.Substring(0, 3) : string.Empty;
        }
        private string PhonePrefix(string phonenb)
        {
            return (!string.IsNullOrEmpty(phonenb)) ? phonenb.Substring(3, 3) : string.Empty;
        }
        private string PhoneSuffix(string phonenb)
        {
            return (!string.IsNullOrEmpty(phonenb)) ? phonenb.Substring(6, 4) : string.Empty;
        }

        private void ConfigureTrashButton(bool _enable)
        {
            if (_enable)
            {
                // Assign trashcan event
                // deletesms specifies delete, 1 = country code
                string itemKey = "deletesms|1|" + SpecifiedMobilePhone;
                string trashUrl = "PopUp.aspx?connect=2&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER).ToString() + "&elid=" + itemKey;
                btnSMSDelete.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER);

                btnSMSDelete.Visible = true;
                litStep1Title.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_Step1Edit").ToString();
            }
            else
            {
                btnSMSDelete.Visible = false;
                litStep1Title.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_Step1").ToString();
            }

        }

        private void DisplayPopup(string _keys)
        {
            string editUrl = "PopUp.aspx?connect=2&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER).ToString() + "&elid=" + _keys;
            string script = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER);

            // Script Manager doesn't want the "return" in the script.
            string script2 = script.Substring(6);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "SHOW POPUP", script2, true);
        }

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hidSMSNeedToConfigureReminders.Value = "1";
                
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // we need to do this regardless of whether or not its a post-back
                UserIdentityContext objContext = SessionInfo.GetUserIdentityContext();
                this.CurrentPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objContext.PatientID.Value);

                if (!Page.IsPostBack)
                {
                    // Add elements to time zone drop down
                    int currLang = (AHAAPI.SessionInfo.GetUserIdentityContext().LanguageID.HasValue) ? AHAAPI.SessionInfo.GetUserIdentityContext().LanguageID.Value : 1;  // English

                    System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem(AHAHelpContent.TimeZoneNames.GetTimeZoneForUX(AHAHelpContent.TimeZone.US_ALASKA, currLang),
                                                                                                     AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_ALASKA));
                    ddlTimeZoneSetup.Items.Add(li);
                    ddlTimeZoneSetup.Items.Add(new System.Web.UI.WebControls.ListItem(
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneForUX(AHAHelpContent.TimeZone.US_CENTRAL, currLang),
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_CENTRAL)));
                    ddlTimeZoneSetup.Items.Add(new System.Web.UI.WebControls.ListItem(
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneForUX(AHAHelpContent.TimeZone.US_EASTERN, currLang),
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_EASTERN)));
                    ddlTimeZoneSetup.Items.Add(new System.Web.UI.WebControls.ListItem(
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneForUX(AHAHelpContent.TimeZone.US_HAWAII, currLang),
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_HAWAII)));
                    ddlTimeZoneSetup.Items.Add(new System.Web.UI.WebControls.ListItem(
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneForUX(AHAHelpContent.TimeZone.US_MOUNTAIN, currLang),
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_MOUNTAIN)));
                    ddlTimeZoneSetup.Items.Add(new System.Web.UI.WebControls.ListItem(
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneForUX(AHAHelpContent.TimeZone.US_MOUNTAIN_AZ, currLang),
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_MOUNTAIN_AZ)));
                    ddlTimeZoneSetup.Items.Add(new System.Web.UI.WebControls.ListItem(
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneForUX(AHAHelpContent.TimeZone.US_PACIFIC, currLang),
                                                                        AHAHelpContent.TimeZoneNames.GetTimeZoneDbName(AHAHelpContent.TimeZone.US_PACIFIC)));

                        // Determine which of our accordions need to be displayed.

                    // Get all the users' phone numbers
                    AHAHelpContent.PhoneNumbers objPhoneNumbers = getPhoneNumbers();

                    ConfigureSetup( objPhoneNumbers );

                    // Show these controls if the user has a mobile number
                    if (objPhoneNumbers.PhoneSMS != null && objPhoneNumbers.IsPinValidated)
                    {
                        string currMobile = GetPhoneMobileCurrent(objPhoneNumbers);
                        /**
                        litSMSTitle.Text = GetGlobalResourceObject("RemindMe", "Title_MyMobile").ToString()
                        + " (" + currMobile.Substring(0, 3) + ")&nbsp;" + currMobile.Substring(3, 3) + "-" + currMobile.Substring(6, 4)
                        + " | " + objPhoneNumbers.TimeZone.ToString();
                         **/
                    }
                    else
                    {
                        //litSMSTitle.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_Setup").ToString();
                    }
                }
        
            }
            catch (Exception ex)
            {
                // lblErrorMessage.Text = "SMS Error:<br/>" + ex.ToString();

                divSetupSMS.Visible = false;
                accPaneSmsReminders.Visible = false;
                accPaneSmsAddReminder.Visible = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //
            // This occurs after any post-backs, so we know if there are reminders, etc..
            //
            AHAHelpContent.PhoneNumbers objPhoneNumbers = getPhoneNumbers();


            if (objPhoneNumbers.PhoneSMS != null )
            {
                ConfigureTrashButton(true);

                string smsphone = GetPhoneSMSCurrent(objPhoneNumbers);

                litSMSTitle.Text = GetGlobalResourceObject("RemindMe", "Title_MyMobile").ToString()
                        + " (" + PhoneAreaCode(smsphone) + ")&nbsp;" + PhonePrefix( smsphone) + "-" + PhoneSuffix( smsphone )
                        + " | " + objPhoneNumbers.TimeZone.ToString();

                if (objPhoneNumbers.IsPinValidated)
                {
                    // Show Current Reminders if there are any
                    if (NumberOfScheduledReminders > 0)
                    {
                        if (hidSMSNeedToConfigureReminders.Value.Equals("1"))
                        {
                            ConfigureReminders();
                            hidSMSNeedToConfigureReminders.Value = "0";
                        }
                        else if (hidSMSNeedToConfigureReminders.Value.Equals("2"))
                        {
                            hidSMSNeedToConfigureReminders.Value = "1";
                        }
                        accPaneSmsReminders.Visible = true;
                    }
                    else
                    {
                         accPaneSmsReminders.Visible = false;
                    }

                    // Show Add Reminder
                    accPaneSmsAddReminder.Visible = true;
                }
                else
                {
                    accPaneSmsReminders.Visible = false;
                    accPaneSmsAddReminder.Visible = false;

                    
                }
            }
            else
            {
                ConfigureTrashButton(false);
                litSMSTitle.Text = GetGlobalResourceObject("RemindMe", "SMS_Title_Setup").ToString();
                if (mvSMS.ActiveViewIndex > 1)      // if a Confirm Step3 or trash can
                    mvSMS.SetActiveView(viewStep1);

                accPaneSmsReminders.Visible = false;
                accPaneSmsAddReminder.Visible = false;
            }



        }

        protected void ConfigureSetup(AHAHelpContent.PhoneNumbers objPhoneNumbers )
        {
            string smsPhone = GetPhoneMobileCurrent(objPhoneNumbers);

            // If there is a SMS phone number 
            if ( !String.IsNullOrEmpty(smsPhone) )
            {
                txtPhoneMobile_AreaCode.Text = PhoneAreaCode( smsPhone );
                txtPhoneMobile_Prefix.Text = PhonePrefix( smsPhone );
                txtPhoneMobile_Suffix.Text = PhoneSuffix( smsPhone );
                ddlTimeZoneSetup.SelectedValue = objPhoneNumbers.TimeZone ;

                divStep1TermsRequired.Visible = false;
                divStep1TermsPreviouslyAccepted.Visible = true;

                if (objPhoneNumbers.IsPinValidated)
                {
                    // set multi view to step 1
                }
                else
                {
                    // set multi view to step 3
                }

//                ConfigureTrashButton(true);
            }
            else
            {
//                ConfigureTrashButton(false);

                divStep1TermsRequired.Visible = true;
                divStep1TermsPreviouslyAccepted.Visible = false;

                string mobilePhone = GetPhoneMobileCurrent(objPhoneNumbers);

                // If they have entered a phone number in profile...
                if (!String.IsNullOrEmpty(mobilePhone))
                {
                    txtPhoneMobile_AreaCode.Text = PhoneAreaCode( mobilePhone ) ;
                    txtPhoneMobile_Prefix.Text = PhonePrefix( mobilePhone );
                    txtPhoneMobile_Suffix.Text = PhoneSuffix( mobilePhone ) ;
                    ddlTimeZoneSetup.SelectedValue = objPhoneNumbers.TimeZone;

                    litPhoneNumber2.Text = "(" + PhoneAreaCode(mobilePhone) + ")&nbsp;" + PhonePrefix(mobilePhone) + "-" + PhoneSuffix(mobilePhone) ;
                    litTimeZone2.Text = ddlTimeZoneSetup.SelectedValue.ToString();
                }
            }
        }

        protected void ConfigureReminders()
        {
            Repeater_DataBind();
        }

        protected void Repeater_DataBind()
        {
            List<PatientReminderSchedule> objSchedules = PatientReminderSchedule.GetPatientReminderSchedules(getPatientReminder().ReminderID);
            PatientReminder objReminder = getPatientReminder();

            repReminders.DataSource = objSchedules;
            repReminders.DataBind();
        }

        protected void Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string sDays = string.Empty;

            PatientReminderSchedule di = e.Item.DataItem as PatientReminderSchedule;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal litTitle = (Literal)e.Item.FindControl("litReminderTitle");

                string titleDays = string.Empty ;
                int nbDays = 0;
                if (di.Monday)
                {
                    titleDays = "Mon";
                    nbDays++;
                }
                if (di.Tuesday)
                {
                    titleDays = (string.IsNullOrEmpty(titleDays)) ? "Tue" : titleDays + ",Tue";
                    nbDays++;
                }
                if (di.Wednesday)
                {
                    titleDays = (string.IsNullOrEmpty(titleDays)) ? "Wed" : titleDays + ",Wed";
                    nbDays++;
                }
                if (di.Thursday)
                {
                    titleDays = (string.IsNullOrEmpty(titleDays)) ? "Thu" : titleDays + ",Thu";
                    nbDays++;
                }
                if (di.Friday)
                {
                    titleDays = (string.IsNullOrEmpty(titleDays)) ? "Fri" : titleDays + ",Fri";
                    nbDays++;
                }
                if (di.Saturday)
                {
                    titleDays = (string.IsNullOrEmpty(titleDays)) ? "Sat" : titleDays + ",Sat";
                    nbDays++ ;
                }
                if (di.Sunday )
                {
                    titleDays = (string.IsNullOrEmpty(titleDays)) ? "Sun" : titleDays + ",Sun";
                    nbDays++ ;
                }
                if (nbDays == 7)
                    titleDays = "Everyday";

                litTitle.Text = di.ReminderCategory + ": " + di.DisplayTime + " " + titleDays ;

                /**
                 ** TODO: perhaps add a tooltip
                 **
                 if (item.Note.Value.Length > 18)
                    {
                        // comment is truncated and entire comment is put in a tooltip
                        litComment.Text = "<span data-tooltip=\"\" tip-bottom=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + item.Note.Value + "\">" +
                                            GRCBase.StringHelper.GetShortString(item.Note.Value, 18, true) + "</span>";
                 **/
                /**
                <span class="sms-list-reminder-title"><%# DataBinder.Eval(Container.DataItem, "Message")%></span>
                <span class="sms-list-reminder-date"><asp:Literal ID="litReminderDays" runat="server"></asp:Literal>&nbsp;|&nbsp;
                 * <%#DataBinder.Eval(Container.DataItem, "DisplayTime")%>&nbsp;
                 * <%#DataBinder.Eval(Container.DataItem, "TimeZone")%>&nbsp;|&nbsp;
                 * <%=GetGlobalResourceObject("RemindMe","SMS_Text_LastSent") %>&nbsp;</span>
                 **/


                Heart360Web.Controls.Patient.ConnectionCenter.SMS.SMS_Reminder cntrlReminder = (Heart360Web.Controls.Patient.ConnectionCenter.SMS.SMS_Reminder)e.Item.FindControl("cntrlReminder");

                if (cntrlReminder != null)
                {
                    cntrlReminder.SetEditData(di);
                }

                // Now setup Save and Cancel buttons
                Button btn = (Button)e.Item.FindControl("btnEditReminderSave");

                if (btn != null)
                {
                    btn.CommandArgument = di.ReminderScheduleID.ToString();
                    btn.CommandName = cntrlReminder.ID ;
                }

                ImageButton ibtn = (ImageButton)e.Item.FindControl("btnEditReminderDelete");
                if (ibtn != null)
                {
                    ibtn.CommandArgument = di.ReminderScheduleID.ToString();
                    ibtn.CommandName = cntrlReminder.GetTitle();
                }
            }
        }

        #region button handlers

        protected void OnClick_CancelStep1(object sender, EventArgs e)
        {
            // Reset step 1 parameters

            // close accordion

        }

        protected void OnClick_Step2(object sender, EventArgs e)
        {
            Page.Validate("SMSSetupStep1");
            if (!Page.IsValid)
                return;

            litPhoneNumber1.Text = "(" + SpecifiedMobilePhone.Substring(0, 3) + ")&nbsp;" + SpecifiedMobilePhone.Substring(3, 3) + "-" + SpecifiedMobilePhone.Substring(6, 4); ;
            litTimeZone1.Text = ddlTimeZoneSetup.SelectedValue.ToString();
            mvSMS.SetActiveView(viewStep2);
        }

        protected void OnClick_Back(object sender, EventArgs e)
        {
            // This should only be used for going from Step2 back to Step1
            // After Step2 the SMS phone has been defined and it would need to be deleted :-)
            mvSMS.SetActiveView(viewStep1);
        }

        protected void OnClick_Step3(object sender, EventArgs e)
        {
            try
            {
                //Saving to database code goes here
                String sMobileNumber = PatientReminder.FormatPhoneNumberfor3Ci(SpecifiedMobilePhone);
                PatientReminder objPatientReminder = getPatientReminder();

                string sPin = AHAHelpContent.PatientReminder.CreatePIN();

                PatientReminder.UpdateReminderPIN(objPatientReminder.ReminderID, objPatientReminder.PatientID, sPin);

                //to be removed after 3Ci API call has been coded
                //lblPIN.Text = sPin;
                _3CiDataManagerSend.SendAPI obj3CiSend = new _3CiDataManagerSend.SendAPI();
                //String sMobileNumber = objPatientReminder.MobileNumber.Replace("-", "");

                sMobileNumber = sMobileNumber.Length == 10 ? "1" + sMobileNumber : sMobileNumber;

                obj3CiSend.sendPIN(sMobileNumber, sPin, objPatientReminder.PatientID.ToString());

                //Add or update reminder setup info
                if (objPatientReminder.ReminderID == 0)
                {
                    // Create new reminder with default pin
                    objPatientReminder = AHAHelpContent.PatientReminder.CreateReminder(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID, sMobileNumber, sPin, ddlTimeZoneSetup.SelectedValue, chkTerms.Checked, false);
                }
                else
                {
                    objPatientReminder.MobileNumber = sMobileNumber;
                    objPatientReminder.TimeZone = ddlTimeZoneSetup.SelectedValue;
                    objPatientReminder.Terms = chkTerms.Checked;

                    objPatientReminder = AHAHelpContent.PatientReminder.UpdateReminder(objPatientReminder);
                }

                // Setting the country codes to 1 for now until we "internationalize"
                int iMobileCountryCodeID = 1;

                string sTimeZone = string.Empty;

                if (!String.IsNullOrEmpty(SpecifiedMobilePhone))
                {
                    AHAHelpContent.PhoneNumbers objPhoneNumbers = getPhoneNumbers() ;

                    if (String.IsNullOrEmpty(objPhoneNumbers.TimeZone))
                    {
                        sTimeZone = string.Empty;
                    }
                    else
                    {
                        sTimeZone = ddlTimeZoneSetup.SelectedValue;
                    }

                    // int iUserHealthRecordID, int iPhoneMobileCountryCodeID, string sPhoneMobile, sTimeZone
                    AHAHelpContent.PhoneNumbers.CreateOrChangeSMSNumber(CurrentPatient.UserHealthRecordID, Convert.ToString(iMobileCountryCodeID), SpecifiedMobilePhone, sPin, sTimeZone, "false");
                }
                else
                {
                    AHAHelpContent.PhoneNumbers.CreateOrChangeSMSNumber(CurrentPatient.UserHealthRecordID, Convert.ToString(iMobileCountryCodeID), SpecifiedMobilePhone, "0000", sTimeZone, "true");
                }

                litPhoneNumber2.Text = "(" + SpecifiedMobilePhone.Substring(0, 3) + ")&nbsp;" + SpecifiedMobilePhone.Substring(3, 3) + "-" + SpecifiedMobilePhone.Substring(6, 4);
                litTimeZone2.Text = ddlTimeZoneSetup.SelectedValue.ToString();

                ConfigureTrashButton(true);
                mvSMS.SetActiveView(viewStep3);
            }
            catch (Exception ex)
            {
                // TODO: show an error popup

                //lblErrorMessage.Text = ex.ToString();
            }
        }

        protected void OnClick_Confirm(object sender, EventArgs e)
        {
            Page.Validate("ValidatePIN");
            if (!Page.IsValid)
                return;
            try
            {
                PatientReminder objPatientReminder = getPatientReminder();

                if (objPatientReminder.PIN == txtPin.Text.Trim())
                {
                    PatientReminder.UpdateReminderPINValidated(objPatientReminder.ReminderID, objPatientReminder.PatientID, true);

                    //Send pin confirmation message
                    _3CiDataManagerSend.SendAPI obj3CiSend = new _3CiDataManagerSend.SendAPI();
                    obj3CiSend.pinConfirm(objPatientReminder.MobileNumber.ToString(), objPatientReminder.PatientID.ToString());
                }
            }
            catch (Exception ex)
            {
                lblStep3Message.Visible = true;
                lblStep3Message.Text = ex.ToString();
            }
            finally
            {
                // PJB: I am not sure we want to do this even if an exception was thrown??
                ConfigureSetup( getPhoneNumbers() );
                mvSMS.SetActiveView(viewStep1);         // put the steps back to 1
                accSmsSetup.SelectedIndex = -1;         // close the (inner) SMS Setup accordion

                hidSMSNeedToConfigureReminders.Value = "1";
                upSMS.Update();                         // need to do this to get the other inner accordions to display

                DisplayPopup("setupsms");
            }
        }

        protected void OnClick_AddReminderSave(object sender, EventArgs e)
        {
            Page.Validate( cntrlAddReminder.GetValidationGroup() );
            if (!Page.IsValid)
                return;

            if (cntrlAddReminder.SaveValues())
            {
                string  reminderTitle = cntrlAddReminder.GetTitle() ;

                hidSMSNeedToConfigureReminders.Value = "1";
                cntrlAddReminder.ResetValues();

                DisplayPopup("congrats|" + "Your reminder has been set for " + reminderTitle);
            }
            else
            {
                DisplayPopup("error");
                // TODO: display an error popup
            }
        }

        protected void OnClick_AddReminderCancel(object sender, EventArgs e)
        {
            // TODO: reset all values of cntrl
            cntrlAddReminder.ResetValues();

            this.accSmsSetup.SelectedIndex = -1;    // close the accordion

        }

        protected void OnClick_EditReminderSave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
//            int scheduleID = Int32.Parse(btn.CommandArgument);

            Control     smscontrol = btn.NamingContainer.FindControl(btn.CommandName);

            if (smscontrol != null)
            {
                Heart360Web.Controls.Patient.ConnectionCenter.SMS.SMS_Reminder reminder = (Heart360Web.Controls.Patient.ConnectionCenter.SMS.SMS_Reminder)smscontrol;

                Page.Validate(reminder.GetValidationGroup());
                if (!Page.IsValid)
                    return;

                if (reminder.SaveValues())
                {
                    string reminderTitle = reminder.GetTitle();

                    hidSMSNeedToConfigureReminders.Value = "1";
                    upScheduledReminders.Update();

                    DisplayPopup("congrats|" + "Your reminder has been changed to " + reminderTitle);
                }
                else
                {
                    // TODO: error popup
                }
            }
        }


        protected void OnClick_EditReminderCancel(object sender, EventArgs e)
        {
            // performing a redisplay will reset values
            hidSMSNeedToConfigureReminders.Value = "1";
            upScheduledReminders.Update();
        }


        protected void OnClick_EditReminderDelete(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;

            if (btn != null)
            {
                hidSMSNeedToConfigureReminders.Value = "2";
                DisplayPopup("deletereminder|" + btn.CommandArgument + "|" + btn.CommandName );
            }

        }

        #endregion

        #region validation

        protected void ValidatePIN(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            PatientReminder objPatientReminder = getPatientReminder();

            if (String.IsNullOrEmpty(txtPin.Text))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_PIN_Invalid").ToString();

                return;
            }
            else if (txtPin.Text.Trim().Length < 4)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_PIN_Incomplete").ToString();

                return;
            }
            else if (objPatientReminder.PIN != txtPin.Text.Trim())
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_PIN_Mismatch").ToString();

                return;
            }
        }

        protected void ValidateSmsStep1AreaCode(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string ac = txtPhoneMobile_AreaCode.Text.Trim();
            if (string.IsNullOrEmpty(ac) || ac.Length != 3 || !Char.IsDigit(ac[0]) || !Char.IsDigit(ac[1]) || !Char.IsDigit(ac[2]))
            {
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_AreaCode").ToString();
                v.IsValid = false;
            }
        }

        protected void ValidateSmsStep1Prefix(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string ac = txtPhoneMobile_Prefix.Text.Trim();
            if (string.IsNullOrEmpty(ac) || ac.Length != 3 || !Char.IsDigit(ac[0]) || !Char.IsDigit(ac[1]) || !Char.IsDigit(ac[2]))
            {
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_Prefix").ToString();
                v.IsValid = false;
            }
        }

        protected void ValidateSmsStep1Suffix(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string ac = txtPhoneMobile_Suffix.Text.Trim();
            if (string.IsNullOrEmpty(ac) || ac.Length != 4 || !Char.IsDigit(ac[0]) || !Char.IsDigit(ac[1]) || !Char.IsDigit(ac[2]) || !Char.IsDigit(ac[3]))
            {
                valid.ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_Suffix").ToString();
                v.IsValid = false;
            }
        }

        protected void ValidateSmsStep1Terms(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = true;

            if (divStep1TermsRequired.Visible && !chkTerms.Checked)
            {
                ((CustomValidator)sender).ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_Terms").ToString();
                v.IsValid = false;
            }
            /**
            // PJB: this comes from ExtendedProfile.cs and the database. I guess the logic is if they have accepted the terms
            // in the past then they don't have to do it this time.
            AHAHelpContent.PhoneNumbers objTerms = AHAHelpContent.PhoneNumbers.GetPhoneNumbers(CurrentPatient.UserHealthRecordID);
                if (objTerms.IsTermsChecked == true || chkTerms.Checked == true)
                {
**/
        }

        #endregion

        // PJB: This gets called when the value gets changed client side, by script.
        // Not sure who is doing this, perhaps the Update panel.....
        // Anyway, if the value is changed to zero, then it was non-null previously,
        // so we will set it back to "1". This fixes display refresh issues with deleting reminders
        // when the popups are in an iframe (not a separate page).
        protected void HiddenValueChanged(Object sender, EventArgs e)
        {
            string hvs = hidSMSNeedToConfigureReminders.Value;

            if (hvs.Equals("0"))
                hidSMSNeedToConfigureReminders.Value = "1";
        }
    }
}