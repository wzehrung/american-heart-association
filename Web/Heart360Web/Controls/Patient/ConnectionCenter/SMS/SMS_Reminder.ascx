﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SMS_Reminder.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.SMS.SMS_Reminder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


        <div class="large-12 small-12 columns spb" >
            <asp:ValidationSummary ID="valReminder" 
                ValidationGroup="SMSReminder"
                HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                Enabled="true"
                CssClass="validation-error full"
                DisplayMode="BulletList"
                ShowSummary="true"
                runat="server" /> 
        </div>
		<div class="large-12 small-12 columns">
			<label><%=GetGlobalResourceObject("RemindMe","SMS_Title_ChooseCategory") %></label>
		</div>

		<div class="large-4 small-12 columns">        
			<asp:DropDownList ID="ddlCategory" runat="server" CssClass="dropdowns" OnSelectedIndexChanged="OnSelectedIndexChanged_Category" AutoPostBack="true" />
		</div>

		<div class="large-8 small-12 columns">
            &nbsp;
		</div>

		<div class="large-12 small-12 columns">
			<label ><%=GetGlobalResourceObject("RemindMe","SMS_Title_ReminderSay") %></label>
		</div>

		<div class="large-12 small-12 columns">
            <asp:TextBox ID="txtReminderText" runat="server" MaxLength="160"></asp:TextBox>
		</div>

		<div class="large-12 small-12 columns">
			<label ><%=GetGlobalResourceObject("RemindMe", "SMS_Title_TimeOfReminder")%></label>
        </div>

		<div class="large-4 small-12 columns">
            <asp:DropDownList ID="ddlTimeOfDay" runat="server" CssClass="dropdowns" />
		</div>

		<div class="large-8 small-12 columns">
            &nbsp;
		</div>

        <div class="large-12 small-12 columns">
			<label ><%=GetGlobalResourceObject("RemindMe", "SMS_Title_DayOfReminder")%></label>
		</div>

		<div class="large-12 small-12 columns">
            <asp:CheckBoxList ID="cblDays" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" Width="100%" CssClass="checkboxlistReminders">
                <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
            </asp:CheckBoxList>
            <span>Select</span> <asp:LinkButton ID="lnkDaysAll" runat="server" OnClick="lnkDaysAll_Click" >All</asp:LinkButton>&nbsp;|&nbsp;<asp:LinkButton ID="lnkDaysNone" runat="server" OnClick="lnkDaysNone_Click" >None</asp:LinkButton>
		</div>

        

        <asp:HiddenField ID="hidScheduleReminderId" runat="server" />

        <asp:CustomValidator
            ID="cvReminderCategory"
            runat="server"
            EnableClientScript="false"
            ValidationGroup="SMSReminder"
            Display="None"
            OnServerValidate="ValidateCategory">
        </asp:CustomValidator>
        <asp:CustomValidator
            ID="cvReminderMessage"
            runat="server"
            EnableClientScript="false"
            ValidationGroup="SMSReminder"
            Display="None"
            OnServerValidate="ValidateMessage">
        </asp:CustomValidator>
        <asp:CustomValidator
            ID="cvReminderTOD"
            runat="server"
            EnableClientScript="false"
            ValidationGroup="SMSReminder"
            Display="None"
            OnServerValidate="ValidateTimeOfDay">
        </asp:CustomValidator>
        <asp:CustomValidator
            ID="cvReminderDays"
            runat="server"
            EnableClientScript="false"
            ValidationGroup="SMSReminder"
            Display="None"
            OnServerValidate="ValidateDays">
        </asp:CustomValidator>
        


