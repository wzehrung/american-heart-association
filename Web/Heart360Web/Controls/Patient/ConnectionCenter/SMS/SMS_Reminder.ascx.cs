﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;     // for appSettings
using AHAHelpContent;
using AHAAPI;

namespace Heart360Web.Controls.Patient.ConnectionCenter.SMS
{
    public partial class SMS_Reminder : System.Web.UI.UserControl
    {
        // if _isSave = true, Save button, else Cancel button
        public delegate void FunctionButtonCallback(bool _isSave, bool _isSuccess, string _saveTitle);

        public FunctionButtonCallback FunctionCallback
        {
            get;
            set;
        }

        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }

        private PatientReminder getPatientReminder()
        {
            PatientReminder objPatientReminder = AHAHelpContent.PatientReminder.GetPatientReminders(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID);
            return objPatientReminder;
        }

        private string SMSNumber
        {
            get
            {
                // PHONE NUMBERS
                AHAHelpContent.PhoneNumbers objPhoneNumbers = AHAHelpContent.PhoneNumbers.GetPhoneNumbers(Convert.ToInt32(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID));

                return (!string.IsNullOrEmpty(objPhoneNumbers.PhoneSMS)) ? objPhoneNumbers.PhoneSMS : string.Empty;
            }
        }

        private AHAHelpContent.PhoneNumbers getPhoneNumbers()
        {
            return AHAHelpContent.PhoneNumbers.GetPhoneNumbers(CurrentPatient.UserHealthRecordID);
        }

        private string CurrentTimeZone
        {
            get
            {
                AHAHelpContent.PhoneNumbers objTimeZone = getPhoneNumbers();
                return (objTimeZone != null) ? objTimeZone.TimeZone : string.Empty;
            }
        }

        private bool Perform3CiFunctionality
        {
            get
            {
                bool doit = true;

                try
                {
                    string configval = ConfigurationManager.AppSettings["InterfaceWith3Ci"];

                    if (!string.IsNullOrEmpty(configval))
                    {
                        doit = bool.Parse(configval);
                    }
                }
                catch { }

                return doit;
            }


        }

            // When used with the data repeater in SMS_Master,
            // this will be called after Page_Init() and Page_Load()!

        public void SetEditData( PatientReminderSchedule scheduledReminder )
        {
            hidScheduleReminderId.Value = scheduledReminder.ReminderScheduleID.ToString();

            ddlCategory.SelectedValue = scheduledReminder.ReminderCategoryID.ToString() + scheduledReminder.ReminderGroupID;

            txtReminderText.Text = scheduledReminder.Message;
            ddlTimeOfDay.SelectedValue = scheduledReminder.Time;

            string sDays = string.Empty;

            // Now do all checkboxes
            if (scheduledReminder.Monday == true)
            {
                sDays = "Monday";
                cblDays.Items.FindByValue("Monday").Selected = true;
            }

            if (scheduledReminder.Tuesday == true)
            {
                sDays = sDays + ", Tuesday";
                cblDays.Items.FindByValue("Tuesday").Selected = true;
            }

            if (scheduledReminder.Wednesday == true)
            {
                sDays = sDays + ", Wednesday";
                cblDays.Items.FindByValue("Wednesday").Selected = true;
            }

            if (scheduledReminder.Thursday == true)
            {
                sDays = sDays + ", Thursday";
                cblDays.Items.FindByValue("Thursday").Selected = true;
            }

            if (scheduledReminder.Friday == true)
            {
                sDays = sDays + ", Friday";
                cblDays.Items.FindByValue("Friday").Selected = true;
            }

            if (scheduledReminder.Saturday == true)
            {
                sDays = sDays + ", Saturday";
                cblDays.Items.FindByValue("Saturday").Selected = true;
            }

            if (scheduledReminder.Sunday == true)
            {
                sDays = sDays + ", Sunday";
                cblDays.Items.FindByValue("Sunday").Selected = true;
            }

            if (sDays.IndexOf(", ", 0) != -1)
            {
                sDays = sDays.Remove(0, 2);
            }

            //
            // Now do all the validations. Since there can be multiple instances of this control,
            // we need to change the validation group to be a unique name
            //

            string validationGroup = "ValidateEditReminder" + hidScheduleReminderId.Value;
            this.valReminder.ValidationGroup = validationGroup;
            this.cvReminderCategory.ValidationGroup = validationGroup;
            this.cvReminderMessage.ValidationGroup = validationGroup;
            this.cvReminderDays.ValidationGroup = validationGroup;
            this.cvReminderTOD.ValidationGroup = validationGroup;
        }

        public string GetValidationGroup()
        {
            return valReminder.ValidationGroup;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _LoadTimeOfDay(ddlTimeOfDay);
            _LoadReminderCategories();

            hidScheduleReminderId.Value = "null";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // we need to do this regardless of whether or not its a post-back
            UserIdentityContext objContext = SessionInfo.GetUserIdentityContext();
            this.CurrentPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objContext.PatientID.Value);

            if (!Page.IsPostBack)
            {
                
            }
        }

        private void _LoadTimeOfDay(DropDownList ddlTimeOfDay)
        {
            ListItemCollection itemColl = TimeConversion.getTimeInAMPMFormatWholeHours();

            ddlTimeOfDay.DataSource = itemColl;
            ddlTimeOfDay.DataTextField = "Text";
            ddlTimeOfDay.DataValueField = "Value";

            ddlTimeOfDay.DataBind();

            ddlTimeOfDay.Items.Insert(0, new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            // Set the default selected time to the current one rounded up to nearest hour
            DateTime RoundUp = DateTime.Parse(DateTime.Now.ToShortTimeString());
            RoundUp = RoundUp.AddMinutes(RoundUp.Hour);
            //ddlTimeOfDay.SelectedValue = RoundUp.ToString("HH:mm");
        }

        private void _LoadReminderCategories()
        {
            List<ReminderCategories> objCategories = ReminderCategories.GetReminderCategories();

            ddlCategory.DataSource = objCategories;
            ddlCategory.DataTextField = "ReminderCategoryName";
            ddlCategory.DataValueField = "ReminderCategoryIDGroupID";

            ddlCategory.DataBind();

            ddlCategory.Items.Insert(0, new System.Web.UI.WebControls.ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));
        }

        protected void OnSelectedIndexChanged_Category(object sender, EventArgs e)
        {
            string sSelected = ddlCategory.SelectedValue.Substring(0, 1);

            switch (sSelected)
            {
                case "1":
                    txtReminderText.Text = GetGlobalResourceObject("RemindMe", "SMS_Message_Default_BloodPressure").ToString();
                    break;
                case "2":
                    txtReminderText.Text = GetGlobalResourceObject("RemindMe", "SMS_Message_Default_Cholesterol").ToString();
                    break;
                case "3":
                    txtReminderText.Text = GetGlobalResourceObject("RemindMe", "SMS_Message_Default_BloodGlucose").ToString();
                    break;
                case "4":
                    txtReminderText.Text = GetGlobalResourceObject("RemindMe", "SMS_Message_Default_Medication").ToString();
                    break;
                case "5":
                    txtReminderText.Text = GetGlobalResourceObject("RemindMe", "SMS_Message_Default_Weight").ToString();
                    break;
                case "6":
                    txtReminderText.Text = GetGlobalResourceObject("RemindMe", "SMS_Message_Default_PhysicalActivity").ToString();
                    break;
            }
        }

        protected void OnClick_EditReminderSave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            

            Page.Validate( btn.CommandArgument);
            if (!Page.IsValid)
                return;

            if (SaveValues())
            {
                if (FunctionCallback != null)
                    FunctionCallback(true, true, this.GetTitle() );
            }
            else
            {
                if (FunctionCallback != null)
                    FunctionCallback(true, false, string.Empty);
            }
        }

        protected void OnClick_EditReminderCancel(object sender, EventArgs e)
        {
            if (FunctionCallback != null)
                FunctionCallback(false, true, string.Empty);
        }

        // Days Check All
        protected void lnkDaysAll_Click(object sender, EventArgs e)
        {
            foreach (System.Web.UI.WebControls.ListItem li in cblDays.Items)
            {
                li.Selected = true;
            }
        }

        // Days Uncheck All
        protected void lnkDaysNone_Click(object sender, EventArgs e)
        {
            foreach (System.Web.UI.WebControls.ListItem li in cblDays.Items)
            {
                li.Selected = false;
            }
        }

        public void ResetValues()
        {
            // TODO: reset all values to defaults, this shouldn't happen in edit mode.
            _LoadTimeOfDay(ddlTimeOfDay);
            _LoadReminderCategories();
            cblDays.SelectedIndex = -1;

            txtReminderText.Text = "";
        }

        public string GetTitle()
        {
            string sDays = string.Empty;
            int nbDays = 0;
            if ( this.cblDays.Items[0].Selected)
            {
                sDays = "Mon";
                nbDays++;
            }
            if (this.cblDays.Items[1].Selected)
            {
                sDays = (string.IsNullOrEmpty(sDays)) ? "Tue" : sDays + ",Tue";
                nbDays++;
            }
            if (this.cblDays.Items[2].Selected)
            {
                sDays = (string.IsNullOrEmpty(sDays)) ? "Wed" : sDays + ",Wed";
                nbDays++;
            }
            if (this.cblDays.Items[3].Selected)
            {
                sDays = (string.IsNullOrEmpty(sDays)) ? "Thu" : sDays + ",Thu";
                nbDays++;
            }
            if (this.cblDays.Items[4].Selected)
            {
                sDays = (string.IsNullOrEmpty(sDays)) ? "Fri" : sDays + ",Fri";
                nbDays++;
            }
            if (this.cblDays.Items[5].Selected)
            {
                sDays = (string.IsNullOrEmpty(sDays)) ? "Sat" : sDays + ",Sat";
                nbDays++;
            }
            if (this.cblDays.Items[6].Selected)
            {
                sDays = (string.IsNullOrEmpty(sDays)) ? "Sun" : sDays + ",Sun";
                nbDays++;
            }
            if (nbDays == 7)
                sDays = "Everyday";

            string retval = ddlCategory.SelectedItem + ": " + ddlTimeOfDay.SelectedValue + " " + sDays;

            return retval;
        }

        public bool SaveValues()
        {
            bool retval = false;

            try
            {
                //Saving to database code goes here
                //String sMobileNumber = PatientReminder.FormatPhoneNumberfor3Ci(txtPhoneNumber.Text.ToString());
                PatientReminder objPatientReminder = AHAHelpContent.PatientReminder.GetPatientReminders(AHAAPI.SessionInfo.GetUserIdentityContext().PatientID);

                bool bMonday = false, bTuesday = false, bWednesday = false, bThursday = false, bFriday = false, bSaturday = false, bSunday = false;

                foreach (System.Web.UI.WebControls.ListItem li in cblDays.Items)
                {
                    if (li.Selected)
                    {
                        switch (li.Value.Trim().ToLower())
                        {
                            case "monday":
                                bMonday = true;
                                break;
                            case "tuesday":
                                bTuesday = true;
                                break;
                            case "wednesday":
                                bWednesday = true;
                                break;
                            case "thursday":
                                bThursday = true;
                                break;
                            case "friday":
                                bFriday = true;
                                break;
                            case "saturday":
                                bSaturday = true;
                                break;
                            case "sunday":
                                bSunday = true;
                                break;
                        }
                    }
                }

                // The values are concatenated with categoryID and groupID since we need them both to
                // create a schedule
                // Just need the actual categoryID
                string sCatID = ddlCategory.SelectedValue.Substring(0, 1);

                // Get the groupID which is the last 5 digits
                string sGroupID = ddlCategory.SelectedValue.Substring(1, 5);

                int scheduleReminderID = (!hidScheduleReminderId.Value.Equals("null")) ? Int32.Parse(hidScheduleReminderId.Value) : -1;
                if (scheduleReminderID == -1)   // save a new one
                {

                    List<PatientReminderSchedule> objPatientSchedule = PatientReminderSchedule.CreateReminderSchedule(objPatientReminder.ReminderID, txtReminderText.Text, bMonday, bTuesday, bWednesday, bThursday, bFriday,
                                                            bSaturday, bSunday, ddlTimeOfDay.SelectedValue.ToString(), ddlTimeOfDay.SelectedItem.Text, Convert.ToInt32(sCatID));

                    if (Perform3CiFunctionality)
                    {
                        _3CiDataManagerSend.SendAPI obj3CiSend = new _3CiDataManagerSend.SendAPI();
                        obj3CiSend.addPersonTo3CiGroup(SMSNumber, sGroupID, objPatientReminder.PatientID.ToString());
                    }
                }
                else
                {
                    List<PatientReminderSchedule> objPatientSchedule = PatientReminderSchedule.UpdateReminderSchedule(
                                                                                scheduleReminderID, objPatientReminder.ReminderID,
                                                                                txtReminderText.Text, 
                                                                                bMonday, bTuesday, bWednesday, bThursday, bFriday, bSaturday, bSunday, 
                                                                                ddlTimeOfDay.SelectedValue.ToString(), ddlTimeOfDay.SelectedItem.Text, 
                                                                                Convert.ToInt32(sCatID));

                }

                retval = true;
            }
            catch (Exception ex)
            {
                // TODO: What to do here?????
                //lblErrorMessage.Text = ex.ToString();
            }

            return retval;
        }

        protected void ValidateCategory(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = ( !string.IsNullOrEmpty(ddlCategory.SelectedValue)) ? true : false;

            if (!v.IsValid)
            {
                ((CustomValidator)sender).ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_Category").ToString();
            }
        }

        protected void ValidateMessage(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = (string.IsNullOrEmpty(txtReminderText.Text.Trim())) ? false : true;
            if (!v.IsValid)
            {
                ((CustomValidator)sender).ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_Message").ToString();
            }
        }

        protected void ValidateTimeOfDay(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = (!string.IsNullOrEmpty(ddlTimeOfDay.SelectedValue)) ? true : false ;

            if (!v.IsValid)
            {
                ((CustomValidator)sender).ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_TimeOfDay").ToString();
            }
        }

        protected void ValidateDays(object sender, ServerValidateEventArgs v)
        {
            v.IsValid = false;

            for( int i = 0; i < cblDays.Items.Count; i++ )
            {
                if( cblDays.Items[i].Selected )
                {
                    v.IsValid = true;
                    break ;
                }
            }

            if (!v.IsValid)
            {
                ((CustomValidator)sender).ErrorMessage = GetGlobalResourceObject("RemindMe", "Validation_SMS_Days").ToString();
            }
        }
    }
}