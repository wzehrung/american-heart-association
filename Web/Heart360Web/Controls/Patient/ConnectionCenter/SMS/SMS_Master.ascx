﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SMS_Master.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.SMS.SMS_Master" %>

<%@ Register Src="~/Controls/Patient/ConnectionCenter/SMS/SMS_Reminder.ascx" TagName="Reminder" TagPrefix="SMS" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<li>
    <div id="divSetupSMS" runat="server" class="asp-container-start"> 
        <ul style="margin: 0px;" class="asp-container-ul">
            <li style="margin: 0px;" class="asp-container-li">
                <asp:UpdatePanel ID="upSMS" runat="server" UpdateMode="Conditional" >         
                <ContentTemplate>
                <asp:HiddenField ID="hidSMSNeedToConfigureReminders" runat="server" OnValueChanged="HiddenValueChanged" />
	            <asp:Accordion ID="accSmsSetup" runat="server"
                    RequireOpenedPane="false"
                    Visible="true"
                    AutoSize="None"
                    SelectedIndex="-1"
                    CssClass="accordionChild"  
                    HeaderCssClass="accordionChildHeader"
                    HeaderSelectedCssClass="accordionChildHeaderSelected"
                    ContentCssClass="accordionChildContent"
                    FadeTransitions="false"
                    FramesPerSecond="30"
                    TransitionDuration="300">
                    <Panes>
                        <asp:AccordionPane ID="accPaneSmsSetup" runat="server" >
                            <Header>
	                            <div class="trigger-wrap">
		                            <div class="accordionChildHeaderText">
                                        <span class="sms-edit-list-title"><asp:Literal ID="litSMSTitle" runat="server"></asp:Literal></span>
						                <asp:ImageButton ID="btnSMSDelete" runat="server" BorderStyle="None" style="border:none !important;" CssClass="trash fr" AlternateText="Delete reminder" ImageUrl="~/images/1x1.png" CausesValidation="false" /> 
                                        <span id="spanArrow" runat="server" class="blue-arrow"></span>
                                    </div>
	                            </div>       
                            </Header>
                            <Content>
                                <div class="row" >
                                    <div class="large-12 small-12 columns" >
                                        <asp:UpdateProgress ID="UpdateProgressSetup" AssociatedUpdatePanelID="upSMS" runat="server" DisplayAfter="500" >
                                            <ProgressTemplate>
                                                <div class="large-1 large-centered small-2 small-centered columns">
                                                    <asp:Image ID="Image1" runat="server" AlternateText="Animated progress gif" ImageAlign="Middle" ImageUrl="~/images/ajax-loader.gif" />       
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </div>
                                    <div class="large-12 small-12 columns" >
                                        <asp:MultiView ID="mvSMS" runat="server" ActiveViewIndex="0">

			                                <!--## BEGIN STEP 1 ##-->
                                            <asp:View ID="viewStep1" runat="server">
				                                <div id="mobile-step-1" class="full">
					                                <div class="row">
                                                        <div class="large-12 small-12 columns spb" >
                                                            <asp:ValidationSummary ID="valSetupStep1" 
                                                                ValidationGroup="SMSSetupStep1"
                                                                HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                                Enabled="true"
                                                                CssClass="validation-error full"
                                                                DisplayMode="BulletList"
                                                                ShowSummary="true"
                                                                runat="server" /> 
                                                        </div>
						                                <div id="divStep1NewText" runat="server" class="large-12 small-12 columns spb">
							                                <p><%=GetGlobalResourceObject("RemindMe", "SMS_Text_Step1")%></p>
                                                        </div>
                                                        <div class="large-12 small-12 columns spb">
                                                            <h4><asp:Literal ID="litStep1Title" runat="server"></asp:Literal></h4>
						                                </div>

                                                        <div class="large-12 small-12 columns">
                                                            <div class="row">

						                                        <div class="large-6 small-12 columns">
							                                        <label ><%=GetGlobalResourceObject("RemindMe", "SMS_Title_EnterNumber")%></label>
						                                            <div class="row">

							                                            <div class="large-4 small-4 columns">
                                                                            <asp:TextBox ID="txtPhoneMobile_AreaCode" runat="server" MaxLength="3"></asp:TextBox>
                                                                        </div>
                                                                        <div class="large-4 small-4 columns">
                                                                            <asp:TextBox ID="txtPhoneMobile_Prefix" runat="server" MaxLength="3"></asp:TextBox>
                                                                        </div>
                                                                        <div class="large-4 small-4 columns">
                                                                            <asp:TextBox ID="txtPhoneMobile_Suffix" runat="server" MaxLength="4"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="large-6 small-12 columns">
                                                                    <label ><%=GetGlobalResourceObject("RemindMe", "Title_TimeZone")%></label>
                                                                    <div class="row">

					                                                    <div class="large-12 small-12 columns">
                                                                            <asp:DropDownList runat="server" ID="ddlTimeZoneSetup" CssClass="dropdowns">
                                                                            </asp:DropDownList>
					                                                    </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

						                                <div class="large-12 small-12 columns">
							                                <label ><%=GetGlobalResourceObject("RemindMe", "Title_TermsAndConditions")%></label>
						                                </div>
						                                <div class="large-12 small-12 columns">
							                                <div class="termsbox">
                                                                <%=GetGlobalResourceObject("RemindMe", "SMS_Text_Terms")%>
                                                            </div>
						                                </div>
						                                <div id="divStep1TermsRequired" runat="server" class="large-12 small-12 columns spb">
                                                            <asp:CheckBox ID="chkTerms" CssClass="fl" runat="server" />
							                                <label class="check-text" ><%=GetGlobalResourceObject("RemindMe", "SMS_Text_Terms_Agree")%></label>
						                                </div>
                                                        <div id="divStep1TermsPreviouslyAccepted" runat="server" class="large-12 small-12 columns spb" visible="false" >
                                                            <p>You have previously accepted the terms.</p>
                                                        </div>
                                                        <div class="large-12 small-12 columns spb" >
						                                    <div class="large-4 small-12 columns fl">
                                                                <asp:Button ID="btnContinueStep2" runat="server" CssClass="gray-button full" OnClick="OnClick_Step2" Text="<%$Resources:RemindMe,SMS_Button_ContinueToStep2%>" />
						                                    </div>
                                                            <div class="large-4 small-12 columns fl">
                                                                <asp:Button ID="btnCancelStep1" runat="server" CssClass="gray-button full" OnClick="OnClick_CancelStep1" Text="<%$Resources:Common,Text_Cancel%>" Visible="false" />
						                                    </div>
                                                        </div>

					                                </div>
				                                </div>

                                                <asp:CustomValidator
                                                    ID="cvStep1AreaCode"
                                                    runat="server"
                                                    EnableClientScript="false"
                                                    ValidationGroup="SMSSetupStep1"
                                                    Display="None"
                                                    OnServerValidate="ValidateSmsStep1AreaCode">
                                                </asp:CustomValidator>
                                                <asp:CustomValidator
                                                    ID="cvStep1Prefix"
                                                    runat="server"
                                                    EnableClientScript="false"
                                                    ValidationGroup="SMSSetupStep1"
                                                    Display="None"
                                                    OnServerValidate="ValidateSmsStep1Prefix">
                                                </asp:CustomValidator>
                                                <asp:CustomValidator
                                                    ID="cvStep1Suffix"
                                                    runat="server"
                                                    EnableClientScript="false"
                                                    ValidationGroup="SMSSetupStep1"
                                                    Display="None"
                                                    OnServerValidate="ValidateSmsStep1Suffix">
                                                </asp:CustomValidator>
                                                <asp:CustomValidator
                                                    ID="cvStep1Terms"
                                                    runat="server"
                                                    EnableClientScript="false"
                                                    ValidationGroup="SMSSetupStep1"
                                                    Display="None"
                                                    OnServerValidate="ValidateSmsStep1Terms">
                                                </asp:CustomValidator>

                                            </asp:View>
			                                <!--## END STEP 1 ##-->
                
                                            <!--## BEGIN STEP 2 ##-->
                                            <asp:View ID="viewStep2" runat="server">

				                                <div id="mobile-step-2" class="full">
					                                <div class="row">

                                                        <div class="large-12 small-12 columns">
                                                            <h4><%=GetGlobalResourceObject("RemindMe", "SMS_Title_Step2")%></h4>
                                                        </div>
						                                <div class="large-12 small-12 columns spb">
							                                <p><%=GetGlobalResourceObject("RemindMe","SMS_Text_SendTest") %></p>
							                                <span class="test-confirm-blue"><%=GetGlobalResourceObject("RemindMe","SMS_Text_UsingPhoneNumber") %> <asp:Literal ID="litPhoneNumber1" runat="server"></asp:Literal>&nbsp;|&nbsp;<asp:Literal ID="litTimeZone1" runat="server"></asp:Literal></span>
						                                </div>

						                                <div class="large-3 small-12 columns fl spb">
                                                            <asp:Button ID="btnBack1" runat="server" CssClass="gray-button full" OnClick="OnClick_Back" Text="<%$Resources:RemindMe,SMS_Button_BackToStep1%>" />
						                                </div>
						                                <div class="large-5 small-12 columns fl spb">
                                                            <asp:Button ID="btnContinueStep3" runat="server" CssClass="gray-button full" OnClick="OnClick_Step3" Text="<%$Resources:RemindMe,SMS_Button_SendPin%>" />
						                                </div>

					                                </div>
				                                </div>

                                            </asp:View>
			                                <!--## END STEP 2 ##-->								

                                            <!--## BEGIN STEP 3 ##-->
                                            <asp:View ID="viewStep3" runat="server">

                                                <asp:ValidationSummary 
                                                    ID="valSummary"
                                                    ValidationGroup="ValidatePIN"
                                                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                    Enabled="true"
                                                    CssClass="validation-error"
                                                    DisplayMode="BulletList"
                                                    ShowSummary="true"
                                                    runat="server" />   

				                                <div id="mobile-step-3" class="full">
					                                <div class="row">

						                                <div class="large-12 small-12 columns spb">
							                                <h4><%=GetGlobalResourceObject("RemindMe", "SMS_Title_Step3")%></h4>
							                                <span class="test-confirm-blue"><%=GetGlobalResourceObject("RemindMe","SMS_Text_UsingPhoneNumber") %> <asp:Literal ID="litPhoneNumber2" runat="server"></asp:Literal>&nbsp;|&nbsp;<asp:Literal ID="litTimeZone2" runat="server"></asp:Literal></span></span>
							                                <p><%=GetGlobalResourceObject("RemindMe","SMS_Text_EnterPin") %></p>
						                                </div>

						                                <div class="large-12 small-12 columns dspb">
							                                <div class="row">

								                                <div class="large-1 small-12 columns text-right fl">
									                                <label class="pin" ><%=GetGlobalResourceObject("RemindMe","SMS_Title_Pin") %></label>
								                                </div>	
								                                <div class="large-5 small-12 columns fl">
									                                <asp:TextBox ID="txtPin" runat="server" MaxLength="4"></asp:TextBox>
								                                </div>

							                                </div>
						                                </div>
														
						                                <div class="large-12 small-12 columns">
							                                <div class="row">
                                                                <div class="large-12 small-12 columns fl spb">
                                                                    <asp:Label ID="lblStep3Message" runat="server" Visible="false"></asp:Label>
						                                        </div>

								                                <div class="large-5 small-12 columns fl spb">
                                                                    <asp:Button ID="btnConfirm" runat="server" CssClass="gray-button full" OnClick="OnClick_Confirm" Text="<%$Resources:RemindMe,SMS_Button_Confirm%>" />                                             
								                                </div>
							                
                                                            </div>
						                                </div>

					                                </div>
				                                </div>
                        
                                                <asp:FilteredTextBoxExtender
                                                    ID="FilteredTextBoxExtender1"
                                                    runat="server"
                                                    Enabled="true"
                                                    TargetControlID="txtPin"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>

                                            </asp:View>
                                            <!-- ## END STEP 3 ## -->

                                            <!--## BEGIN EDIT ##-->
                                            
                                        </asp:MultiView>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender4"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneMobile_AreaCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender5"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneMobile_Prefix"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender6"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneMobile_Suffix"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:CustomValidator
                                            ID="validPIN"
                                            runat="server"
                                            EnableClientScript="false"
                                            ValidationGroup="ValidatePIN"
                                            Display="None"
                                            OnServerValidate="ValidatePIN">
                                        </asp:CustomValidator>
                                    </div>
                                </div>
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="accPaneSmsReminders" runat="server" CssClass="remind-me-edit-reminders" >
                            <Header>
                                <div class="trigger-wrap">
                                    <div class="accordionChildHeaderText">
                                        <span class="sms-edit-list-title"><%=GetGlobalResourceObject("RemindMe","SMS_Title_SMSReminders") %></span>
                                        <span class="blue-arrow"></span>
                                    </div>
                                </div>
                            </Header>
                            <Content>
                                <div class="row">    
                                    <asp:UpdatePanel ID="upScheduledReminders" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" >
                                    <ContentTemplate>

                                        <asp:Repeater ID="repReminders" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <ul class="inner-accordion">
                                                <li>
					                                <div class="trigger-wrap">
						                                <div class="inner-trigger">     
                                                            <span class="sms-list-reminder-title"><asp:Literal ID="litReminderTitle" runat="server"></asp:Literal></span>
                                                            <span class="blue-arrow"></span>
                                                        </div>
						                                <asp:ImageButton ID="btnEditReminderDelete" runat="server" BorderStyle="None" style="border:none !important;" CssClass="trash" AlternateText="Delete reminder" ImageUrl="~/images/1x1.png" CausesValidation="false" OnClick="OnClick_EditReminderDelete" />                                    
                                                    </div>
					                                <div class="inner-content">
                                                        <asp:UpdatePanel ID="upReminderRepeater" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="large-12 small-12 columns" >
                                                                <asp:UpdateProgress AssociatedUpdatePanelID="upReminderRepeater" runat="server" DisplayAfter="500" >
                                                                    <ProgressTemplate>
                                                                        <div class="large-1 large-centered small-2 small-centered columns">
                                                                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                                        </div>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                            </div>
                                                            <SMS:Reminder ID="cntrlReminder" runat="server"></SMS:Reminder>
                                                            <div class="large-12 small-12 columns fl spt">
                                                                <div class="row">
		                                                            <div class="large-4 small-12 columns fl">
			                                                            <asp:Button ID="btnEditReminderSave" runat="server" CssClass="gray-button full" OnClick="OnClick_EditReminderSave" Text="<%$Resources:RemindMe,SMS_Button_Save%>" />
		                                                            </div>

		                                                            <div class="large-4 small-12 columns fl">
			                                                            <asp:Button ID="btnEditReminderCancel" runat="server" CssClass="gray-button full" OnClick="OnClick_EditReminderCancel" Text="<%$Resources:RemindMe,SMS_Button_Cancel%>"/>
		                                                            </div>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>  
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </li>
                                            </ul>
                                        </ItemTemplate>
                                        </asp:Repeater> 
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </Content>
                        </asp:AccordionPane>
                        <asp:AccordionPane ID="accPaneSmsAddReminder" runat="server" CssClass="remind-me-add-reminders" >
                            <Header>
                                <div class="trigger-wrap">
                                    <div class="accordionChildHeaderText">
                                        <span class="sms-edit-list-title"><%=GetGlobalResourceObject("RemindMe","SMS_Title_AddReminder") %></span>
                                        <span class="blue-arrow"></span>  
                                    </div>             
                                </div>
                            </Header>
                            <Content>
                                <div class="row" >
                                    <div class="large-12 small-12 columns" >
                                        <asp:UpdateProgress ID="UpdateProgressAddReminder" AssociatedUpdatePanelID="upSMS" runat="server" DisplayAfter="500" >
                                            <ProgressTemplate>
                                                <div class="large-1 large-centered small-2 small-centered columns">
                                                    <asp:Image ID="Image1" runat="server" ImageAlign="Middle" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </div>
                                    <SMS:Reminder ID="cntrlAddReminder" runat="server"></SMS:Reminder>
                                    <div class="large-12 small-12 columns fl spt">
                                        <div class="row">
		                                    <div class="large-4 small-12 columns fl">
			                                    <asp:Button ID="btnAddReminderSave" runat="server" CssClass="gray-button full" OnClick="OnClick_AddReminderSave" Text="<%$Resources:RemindMe,SMS_Button_Save%>" />
		                                    </div>

		                                    <div class="large-4 small-12 columns fl">
			                                    <asp:Button ID="btnAddReminderCancel" runat="server" CssClass="gray-button full" OnClick="OnClick_AddReminderCancel" Text="<%$Resources:RemindMe,SMS_Button_Cancel%>" />
		                                    </div>

                                        </div>
                                    </div>
                                </div>
                            </Content>
                        </asp:AccordionPane>
                    </Panes>
                </asp:Accordion>
                </ContentTemplate>       
                </asp:UpdatePanel>
            </li>
        </ul>
    </div>
</li>


