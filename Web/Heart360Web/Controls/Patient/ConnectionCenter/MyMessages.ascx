﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyMessages.ascx.cs"
    Inherits="Heart360Web.Controls.Patient.ConnectionCenter.MyMessages" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxTool" %>
<section id="my-messages" class="info-stats full">
	<div class="accordion">
        <div class="toggle-btn">
			<h4><span class="icon-messages"></span><%=GetGlobalResourceObject("Tracker", "Tracker_Text_MyMessages")%><span class="gray-arrow"></span></h4>
        </div>
		<div class="content">
           
            <asp:UpdatePanel ID="UpdateMyMessage" UpdateMode="Conditional" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Timer1" />
                </Triggers>
                
                <ContentTemplate>
           
                    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdateMyMessage" runat="server" DisplayAfter="500">
                        <ProgressTemplate>
                            <div class="large-1 large-centered small-2 small-centered columns">
                                <asp:Image ID="Image1" runat="server" ImageAlign="Middle" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
  
			<div class="nested-tabs" >
                <asp:Timer ID="Timer1" runat="server" OnTick="OnTick_Timer1" Interval="5000" Enabled="False"></asp:Timer>
                
                    
                    <div class="row tabs-nav">
                        <div id="divTabInbox" runat="server"  class="large-3 small-3 tab-btn" >
                            <span id="spanTabMessages" runat="server" class="msg-unread" >
                                <span class="icon-messages" onclick="document.getElementById('<%=btnTabInbox.ClientID %>').click();"></span>
						        <span class="tab-title" onclick="document.getElementById('<%=btnTabInbox.ClientID %>').click();"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_Messages")%> [<%=GetMessageCount()%>]</span>                                     
                                <asp:Button ID="btnTabInbox" runat="server" OnClick="OnClick_TabInbox" style="display:none;" />
                            </span>
                        </div>
                        <div id="divTabCompose" runat="server" class="large-3 small-3 tab-btn" >
                            <span class="add-note" onclick="document.getElementById('<%=btnTabCompose.ClientID %>').click();"></span>
							<span class="tab-title" onclick="document.getElementById('<%=btnTabCompose.ClientID %>').click();"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_ComposeMessage")%></span>
                            <asp:Button ID="btnTabCompose" runat="server" OnClick="OnClick_TabCompose" style="display:none;" />
                        </div>
                        <div id="divTabSentbox" runat="server" class="large-3 small-3 tab-btn" >
                            <span class="icon-send-message" onclick="document.getElementById('<%=btnTabSentbox.ClientID %>').click();"></span>
							<span class="tab-title" onclick="document.getElementById('<%=btnTabSentbox.ClientID %>').click();"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_SentMessages")%></span>
                            <asp:Button ID="btnTabSentbox" runat="server" OnClick="OnClick_TabSentbox" style="display:none;" />
                        </div>
                        <div id="divTabTrashbox" runat="server" class="large-3 small-3 tab-btn" >
                            <span class="trash" onclick="document.getElementById('<%=btnTabTrashbox.ClientID %>').click();"></span>
							<span class="tab-title" onclick="document.getElementById('<%=btnTabTrashbox.ClientID %>').click();" ><%=GetGlobalResourceObject("Tracker", "Tracker_Text_Trash")%></span>
                            <asp:Button ID="btnTabTrashbox" runat="server" OnClick="OnClick_TabTrashbox" style="display:none;" />
                        </div>
                    
                    </div>

                    
                    <asp:MultiView ID="mvMsgBoxes" runat="server" ActiveViewIndex="0" >
                    
                        <asp:View ID="viewMsgInbox" runat="server" >
                                    <!-- Display message in the inbox -->
				                    <section class="inbox spb">
                                        <div class="full messages-table-container">
                                        <!-- Display message in the inbox-->                        
                                            <asp:Repeater ID="repMessages" runat="server" OnItemDataBound="repMessages_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table class="paginated">
                                                        <thead>
                                                            <tr>
                                                                <th width="100%"></th>
                                                            </tr>
                                                        </thead>
                                                    <tbody>
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                    <tr>
                                                    <td>
                                                    <ul class="inner-accordion">
                                                    <li>                          
                                                    <div class="trigger-wrap">
                                                        <h4 id="triggerReadMail" runat="server" class="inner-trigger" >
                                                            <asp:Literal ID="LitFromI" runat="server"></asp:Literal><br />
                                                            <asp:Literal ID="litSubjectI" runat="server"></asp:Literal>
                                                            <span class="blue-arrow"></span>
                                                        </h4>                               
                                                        <span class="message-date full"><asp:Literal ID="LitSentDateI" runat="server"></asp:Literal></span>
							                            <asp:ImageButton ID="imgBtnDeleteI" runat="server" BorderStyle="None" style="border:none !important;" AlternateText="Send message to trash" CssClass="trash" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                                                        <asp:ImageButton ID="imgBtnReplyI" runat="server" BorderStyle="None" style="border:none !important;" AlternateText="Reply to message" CssClass="add-note" ImageUrl="~/images/1x1.png" OnClick="OnClick_Reply" CausesValidation="false" />                            
                                                    </div>
                                                    <div class="inner-content"><asp:Literal ID="LitMessageBodyI" runat="server"></asp:Literal></div>
                                                    </li>
                                                    </ul>
                                                    </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                        </tbody>
                                                    </table>                                                
                                                </FooterTemplate>
                                            </asp:Repeater>                         
					                    </div>	
				                    </section>
                        </asp:View>
                        <asp:View ID="viewMsgCompose" runat="server" >                      
                                    <section class="compose spb">
		        		    	        <div class="full">
                                            <div class="large-12 small-12 columns spb" >
                                                <asp:ValidationSummary ID="valSummaryCompose" ValidationGroup="vgCompose"
                                                        HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                                        Enabled="true"
                                                        CssClass="validation-error"
                                                        DisplayMode="BulletList"
                                                        ShowSummary="true"
                                                        runat="server" /> 
                                            </div>
							                <div class="large-2 small-12 columns text-right">
								                <label >TO:</label>
							                </div>
							                <div class="large-10 small-12 columns">
								                <asp:DropDownList ID="ddlProviders" runat="server" AppendDataBoundItems="True" ></asp:DropDownList>
                                                <asp:CustomValidator ID="validateComposeProvider" runat="server" ValidationGroup="vgCompose" EnableClientScript="false" Display="None" OnServerValidate="validateProvider" ></asp:CustomValidator>
							                </div>
							                <div class="large-2 small-12 columns text-right">
								                <label >SUBJECT:</label>
							                </div>	
							                <div class="large-10 small-12 columns">
                                                <asp:TextBox ID="txtSubject" runat="server" MaxLength="100" ></asp:TextBox>
                                                <asp:CustomValidator ID="validateSubject" runat="server" ValidationGroup="vgCompose" EnableClientScript="false" Display="None" OnServerValidate="ValidSubject" ></asp:CustomValidator>
							                </div>
							                <div class="large-10 large-offset-2 small-12 columns">
                                                <asp:TextBox ID="txtMailBody" TextMode="MultiLine" runat="server" Rows="13"></asp:TextBox>
                                                <asp:CustomValidator ID="validComposeMessage" runat="server" ValidationGroup="vgCompose" EnableClientScript="false" Display="None" OnServerValidate="ValidMessage" ></asp:CustomValidator>
							                </div>
							                <div class="large-2 large-offset-2 small-12 columns fl">
                                                <asp:LinkButton ID="btnSend" runat="server" CssClass="gray-button full" onclick="OnSubmit">Send</asp:LinkButton>
							                </div>
							                <div class="large-2 small-12 columns fl">
                                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="gray-button full" onclick="OnClick_Cancel">Cancel</asp:LinkButton>
							                </div>
                                            <br />
                                            <div class="large-2 small-12 text-left">
                                                <asp:Literal ID="litAlert" runat="server"></asp:Literal>
							                </div>
                                            
					                    </div>	
			    	                </section>
                        </asp:View>
                        <asp:View ID="viewMsgSentBox" runat="server" >  
 	            			        <section class="sent">
                                        <div class="full messages-table-container">
                                        <!-- Display messages have been sent -->
                                            <asp:Repeater ID="repSent" runat="server" OnItemDataBound="repSent_ItemDataBound">
                                                 <HeaderTemplate>
                                                    <table class="paginated">
                                                        <thead>
                                                            <tr>
                                                                <th width="100%"></th>
                                                            </tr>
                                                        </thead>
                                                    <tbody>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                    <td>
                                                    <ul class="inner-accordion">
                                                    <li>                          
                                                    <div class="trigger-wrap">
                                                    <h4 class="inner-trigger">
                                                    <asp:Literal ID="LitToS" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="litSubjectS" runat="server"></asp:Literal>
                                                    <span class="blue-arrow"></span></h4>                          
                                                    <span class="message-date full"><asp:Literal ID="LitSentDateS" runat="server"></asp:Literal></span>
                                                    <asp:ImageButton ID="imgBtnDeleteS" runat="server" BorderStyle="None" style="border:none !important;" AlternateText="Send message to trash" CssClass="trash" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                                                    <asp:ImageButton ID="imgBtnReplyS" runat="server" BorderStyle="None" style="border:none !important;" AlternateText="Reply to message" CssClass="add-note" ImageUrl="~/images/1x1.png" OnClick="OnClick_ReplySent" CausesValidation="false" />
                                                    </div>
                                                    <div class="inner-content"><asp:Literal ID="LitMessageBodys" runat="server"></asp:Literal></div>
                                                    </li>
                                                    </ul>
                                                    </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                        </tbody>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
		            		        </section>
                        </asp:View>
                        <asp:View ID="viewMsgTrashBox" runat="server" >
                                    <!-- Messages in the trash can -->
				                    <section class="trashbox">
                                        <div class="full messages-table-container">
                                        <!-- Display messages has been trashed -->
                                        <asp:Repeater ID="repTrash" runat="server" OnItemDataBound="repTrash_ItemDataBound">
                                            <HeaderTemplate>
                                                <table class="paginated">
                                                    <thead>
                                                        <tr>
                                                            <th width="100%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>                            
                                                    <tr>
                                                    <td>
                                                    <ul class="inner-accordion">
                                                    <li>                          
                                                    <div class="trigger-wrap">
                                                    <h4 class="inner-trigger">
                                                    <asp:Literal ID="LitToT" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="LitFromT" runat="server"></asp:Literal><br />
                                                    <asp:Literal ID="litSubjectT" runat="server"></asp:Literal>
                                                    <span class="blue-arrow"></span></h4>                               
                                                    <span class="message-date full"><asp:Literal ID="LitSentDateT" runat="server"></asp:Literal></span>
							                        <asp:ImageButton ID="imgBtnDeleteT" runat="server" BorderStyle="None" style="border:none !important;" AlternateText="Permanently delete message" CssClass="trash" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                                                    <asp:ImageButton ID="imgBtnReplyT" runat="server" BorderStyle="None" style="border:none !important;" AlternateText="Reply to message" CssClass="add-note" ImageUrl="~/images/1x1.png" OnClick="OnClick_ReplyTrash" CausesValidation="false"  />
                                                    </div>
                                                    <div class="inner-content"><asp:Literal ID="LitMessageBodyT" runat="server"></asp:Literal></div>
                                                    </li>
                                                    </ul>
                                                    </td>
                                                    </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                    </tbody>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        </div>
				                    </section>
                       </asp:View>
                   </asp:MultiView>
                </ContentTemplate>
                </asp:UpdatePanel>
                <!-- -->
			</div>
            <asp:HiddenField ID="hidFolderType" runat="server" />
            <asp:HiddenField ID="hidMessageID" runat="server" />
		</div>
	</div>
</section>
