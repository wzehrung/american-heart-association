﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using AHAHelpContent;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class EMRControl : System.Web.UI.UserControl
    {
        public string TempPath
        {
            get
            {
                string strTempPath = AHACommon.AHAAppSettings.TempFilesURL;
                return strTempPath;
                
            }
        }

        public UpdatePanel GetUpdatePanel()
        {
            return this.upanelClinicConnect;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hidPatientID.Value = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value.ToString();
                rptEMRConnections_DataBind();
                rptEMRRecords_DataBind();
            }

        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            mvEMR.SetActiveView(viewStep1);
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            mvEMR.SetActiveView(viewStep2);
        }

        protected void btnBacktoStep1_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            mvEMR.SetActiveView(viewStep1);
        }

        protected void btnBacktoStep2_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            mvEMR.SetActiveView(viewStep2);
        }

        // Validate the ConnectionKey and OrganizationID
        protected void btnSubmitKeys_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            Page.Validate("ValidateKeys");
            if (!Page.IsValid)
            {
                return;
            }

            try
            {
                EMRConnections objRequest = EMRConnections.GetConnectionRequest(txtConnectionKey.Text, txtOrganizationID.Text);

                if (objRequest != null)
                {
                    mvEMR.SetActiveView(viewStep2);

                    lblRegistrationKey.Text = objRequest.ConnectionKey;
                    lblOrganizationID.Text = objRequest.OrganizationID;
                    lblOrganizationName.Text = objRequest.OrganizationName;
                    lblQuestion.Text = objRequest.SecretQuestion;
                    lblRequestDate.Text = objRequest.RequestDate.ToString();
                }
            }
            catch (Exception ex)
            {
                
            }

        }

        // Validate the challenge question and answer and then make the connection
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            Page.Validate("ValidateChallenge");
            if (!Page.IsValid)
            {
                return;
            }

            int iConnectionID = 0;

            EMRConnections objRequestA = EMRConnections.GetConnectionRequest(txtConnectionKey.Text, txtOrganizationID.Text);

            if (objRequestA != null)
            {
                iConnectionID = objRequestA.ConnectionID;
            }

            int iPatientID = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value;

            EMRConnections objRequest = EMRConnections.UpdateConnection(true, iConnectionID, iPatientID);

            if (objRequest != null)
            {
                mvEMR.SetActiveView(viewStep3);
            }            
        }

        protected void rptEMRConnections_DataBind()
        {
            List<EMRConnections> objConnections = EMRConnections.GetConnections(Convert.ToInt32(hidPatientID.Value));

            rptEMRConnections.DataSource = objConnections;
            rptEMRConnections.DataBind();
        }

        protected void rptEMRConnections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EMRConnections di = e.Item.DataItem as EMRConnections;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal litClinicName = e.Item.FindControl("litClinicName") as Literal;
                Literal litConnectionDate = e.Item.FindControl("litConnectionDate") as Literal;

                litClinicName.Text = di.OrganizationName;
                litConnectionDate.Text = di.ConnectDate.ToString();
            }
        }

        protected void rptEMRRecords_DataBind()
        {
            List<EMRRecords> objConnections = EMRRecords.GetRecords(Convert.ToInt32(hidPatientID.Value));

            rptRecords.DataSource = objConnections;
            rptRecords.DataBind();
        }

        protected void rptEMRRecords_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EMRRecords di = e.Item.DataItem as EMRRecords;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal litRecordClinicName = e.Item.FindControl("litRecordClinicName") as Literal;
                Literal litRecordDate = e.Item.FindControl("litRecordDate") as Literal;
                //Literal litRecord = e.Item.FindControl("litRecord") as Literal;
                LinkButton lbRecordFile = e.Item.FindControl("popupEMR") as LinkButton;
                Xml xmlDocument = e.Item.FindControl("xmlDocument") as Xml;
                

                litRecordClinicName.Text = di.OrganizationName;
                litRecordDate.Text = di.CreatedDate.ToString();
                lbRecordFile.Text = TempPath + di.EMRDataID + ".xml";  //"View EMR";
                //lbRecordFile.PostBackUrl = TempPath + di.EMRDataID + ".xml";
                
                if (di.MessageData != null)
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(di.MessageData);
                    xmlDoc.Save(AHACommon.AHAAppSettings.TempFilesDirectory + di.EMRDataID + ".xml");

                    xmlDocument.DocumentSource = TempPath + di.EMRDataID + ".xml";

                    //litRecord.Text = di.MessageData;
                    foreach (Control rootControl in Controls)
                    {
                        if (rootControl.ID == "mpeDisplayEMR")
                        {
                            AjaxControlToolkit.ModalPopupExtender pop = (AjaxControlToolkit.ModalPopupExtender)rootControl;
                            pop.Show();
                        }
                    }
                } 
            }
        }

        protected void OnClick_ViewEMR(object sender, EventArgs e)
        {

        }



        protected void btnLinkClose_OnClick(object sender, EventArgs e)
        {
            //mpeDisplayEMR.Dispose();
            //mpeDisplayEMR.Hide();
        }

        # region VALIDATORS

        protected void ValidateConnectionKeys(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtConnectionKey.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Keys_Empty").ToString();

                return;
            }
            
            else if (String.IsNullOrEmpty(txtOrganizationID.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Keys_Empty").ToString();

                return;
            }

            else 
            { 
                EMRConnections objRequest = EMRConnections.GetConnectionRequest(txtConnectionKey.Text, txtOrganizationID.Text);

                if (objRequest != null)
                {
                    if (objRequest.ConnectionKey != txtConnectionKey.Text.Trim())
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Keys_Mismatch").ToString();

                        return;
                    }

                    if (objRequest.OrganizationID != txtOrganizationID.Text.Trim())
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Keys_Mismatch").ToString();

                        return;
                    }

                    if (objRequest.IsConnected == true)
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Keys_AlreadyConnected").ToString();

                        return;
                    }
                }
            }
        }

        protected void ValidateChallengeAnswer(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtAnswer.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Challenge_Required").ToString();

                return;
            }
            else
            {
                EMRConnections objRequest = EMRConnections.GetConnectionRequest(txtConnectionKey.Text, txtOrganizationID.Text);

                if (objRequest != null)
                {
                    if (objRequest.SecretAnswer == txtAnswer.Text)
                    {
                        v.IsValid = true;

                        return;
                    }
                    else
                    {
                        v.IsValid = false;
                        valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Challenge_Mismatch").ToString();

                        return;
                    }
                }
            }
        }

        protected void ValidateAcceptTerms(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;

            CheckBox cb = valid.NamingContainer.FindControl("chkAgree") as CheckBox;

            if (cb != null && cb.Checked)
            {
                v.IsValid = true;
            }
            else
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("EMR", "Validation_Terms_Accept").ToString();
            }
        }

        # endregion

    }
}