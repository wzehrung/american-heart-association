﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConnectionsPopup.ascx.cs" Inherits="Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsPopup" %>

<div class="full">
    <!-- ## Title ## -->
	<div class="large-12 small-12 columns spb">
		<h4 class="full go-blue text-center dspb"><asp:Literal ID="litTitle" runat="server"></asp:Literal></h4>
    </div>

    <!-- ##### TODO: Add a view for no search results #### -->
    <asp:MultiView ID="mvViews" runat="server" >
        <asp:View ID="viewConnect" runat="server" >
            <div class="large-12 small-12 columns spb">
            <asp:Repeater ID="repeaterSearchResults" runat="server" OnItemDataBound="OnProviderDetailsDataBind">
                <ItemTemplate>
                    <div class="row new-contact spb">
			            <div class="large-8 small-12 columns">
				            <ul class="first">
					            <li>
						            <span class="provider-name go-blue"><asp:Literal ID="litProviderName" runat="server" ></asp:Literal></span>
					            </li>
					            <li>
						            <span class="provider-specialty"><asp:Literal ID="litProviderOrg" runat="server"></asp:Literal></span>
					            </li>
					            <li>
						            <span class="provider-organization"><asp:Literal ID="litProviderAddr" runat="server"></asp:Literal></span>
					            </li>
					            <li>
						            <span class="provider-area"><asp:Literal ID="litProviderCityStateZip" runat="server"></asp:Literal></span>
					            </li>
				            </ul>
			            </div>
			            <div class="large-4 small-12 columns">
                            <asp:Button ID="btnConnect" CssClass="gray-button full" runat="server" Text="<%$Resources:ConnCenterPAndV,Text_Connect %>" OnClick="OnConnectClick" />
                            <asp:Label ID="lblAlreadyConnected" runat="server" CssClass="text" Text="CONNECTED" />
			            </div>
		            </div>
                </ItemTemplate>
            </asp:Repeater>

            <div id="divSearchResults" runat="server" class="row">
			    <div class="large-12 small-12 columns">
				    <p class="text-center"><%=GetGlobalResourceObject("ConnCenterPAndV", "Text_ConnectionsPopup_Footer")%></p>
			    </div>
			    <div class="large-centered small-12 columns">
				    <asp:Button ID="btnViewConnectCancel" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_Cancel%>" />
			    </div>
		    </div>
            <div id="divNoSearchResults" runat="server" class="row" >
                <div class="large-12 small-12 columns">
				    <p class="text-center">No results were found, close window and try an alternative search.</p>
			    </div>
            </div>
            </div>
        </asp:View>

        <asp:View ID="viewOk" runat="server" >
            <div class="row">
	            <div class="large-12 small-12 columns">
                    <div class="infoBox">
		                <asp:Literal ID="litViewOkDescription" runat="server"></asp:Literal>
                    </div>
	            </div>
            </div>
            <div class="row">
                <div class="large-6 large-centered small-12 columns">
				    <asp:Button ID="btnViewOkOk" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_OK %>" />
			    </div>
		    </div>
        </asp:View>

        <asp:View ID="viewConfirm" runat="server" >
            <div class="row">
	            <div class="large-4 small-12 columns">
		            <label runat="server"><asp:Literal ID="litViewConfirmDescription" runat="server"></asp:Literal></label>
	            </div>
            </div>
            <div class="row spt">
                <div class="large-3 small-3 columns fl" >
                    <asp:Button ID="btnViewConfirmOk" CssClass="gray-button full" OnClick="btnOk_Click" runat="server" Text="<%$Resources:Common,Text_OK %>" />
                </div>
                <div class="large-3 small-3 columns fl" >
                    <asp:Button ID="btnViewConfirmCancel" CssClass="gray-button full" OnClick="btnCancel_Click" runat="server" Text="<%$Resources:Common,Text_Cancel%>" />
                </div>          
            </div>
        </asp:View>
    
    </asp:MultiView>
    
</div>


