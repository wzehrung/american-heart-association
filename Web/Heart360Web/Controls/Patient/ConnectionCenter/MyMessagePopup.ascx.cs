﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Heart360Web;
using Heart360Web.Patient;  // for MODULE_ID
using AHAHelpContent;
using AHACommon;
using GRCBase;

namespace Heart360Web.Controls.Patient.ConnectionCenter
{
    public partial class MyMessagePopup : System.Web.UI.UserControl
    {
        #region declaration 
        private const string TRACKER_KEY = "Tracker";
        private const string TRACKER_TEXT_MY_MESSAGE = "Tracker_Text_MyMessages";
        private const string CONST_DELETE_MESSAGE = "Delete message:";
        private const string CONST_HTML_BREAK = "<br />";
        private const string CONST_QMARK = "?";
        private const string CONST_NONE = "None";
        private const char CHAR_COMMON = ',';
        
        private Heart360Web.PatientPortal.MODULE_ID     mModuleID;
        private string                                  m_EditKeys;         // one or more comma separated values representing the "keys" to the database item to delete.
        private string CurrentFolderType = string.Empty;
        private string CurrentMessageID = string.Empty;

        private AHAHelpContent.UserType UserType
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        private AHAHelpContent.UserTypeDetails CurrentUserDetails
        {
            get
            {
                // patient information 
                if (this.UserType == AHAHelpContent.UserType.Patient)
                {
                    return new AHAHelpContent.UserTypeDetails
                    {
                        UserID = AHAAPI.SessionInfo.GetUserIdentityContext().PatientID.Value,
                        UserType = AHAHelpContent.UserType.Patient
                    };
                }
                else
                {
                    // provider information 
                    return new AHAHelpContent.UserTypeDetails
                    {
                        UserID = AHAAPI.Provider.SessionInfo.GetProviderContext().ProviderID,
                        UserType = AHAHelpContent.UserType.Provider
                    };
                }
            }
        }
        #endregion 

        public MyMessagePopup(): base()
        {
            mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
            m_EditKeys = string.Empty;
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        public string EditKeys
        {
            set { m_EditKeys = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //get track literal
                this.litTitle.Text = GetGlobalResourceObject(TRACKER_KEY, TRACKER_TEXT_MY_MESSAGE).ToString();

                // ----------------------------------------------------------------------------------------
                // all required parameters are passing in through m_EditKeys separated by ","
                // parsing m_EditKeys : MessageID, MessageFolder, ....
                // ----------------------------------------------------------------------------------------
                string[] msgKeys = m_EditKeys.Split(CHAR_COMMON);
                this.CurrentFolderType = msgKeys[0].ToString().Trim();
                this.LitFromFolder.Text = msgKeys[0].ToString().Trim();
                this.CurrentMessageID = msgKeys[1].ToString().Trim();
                this.LitMsgID.Text = msgKeys[1].ToString().Trim(); 
                // -----------------------------------------------------------------------------------------
                // populated message id to display on the popup form 
                // ------------------------------------------------------------------------------------------
                //LitMessageID.Text = (msgKeys.Length > 0) ? CONST_DELETE_MESSAGE + CONST_HTML_BREAK + this.CurrentMessageID + CONST_QMARK : CONST_NONE;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.LitFromFolder.Text)) return;
            if (String.IsNullOrEmpty(this.LitMsgID.Text)) return;

            // ----------------------------------------------------------------
            // move message to trash can or purge message from trash can
            // ----------------------------------------------------------------
            _DeleteMessages(this.LitFromFolder.Text.Trim(), this.LitMsgID.Text.Trim());

            // ----------------------------------------------------------------------------
            // if delete is successful then close the popup form
            // ----------------------------------------------------------------------------
            // Let the popup handle how we close
            ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            // Let the popup handle how we close
            ((Heart360Web.Pages.Patient.PopUp)Page).CancelPopup();
        }
        /// <summary>
        /// 
        /// </summary>
        private void _DeleteMessages(string strCurrentFolder, string strMessageID)
        {
            // -------------------------------------------------------------------------------
            // delet message routine for message in the inbox folder and sent message folder 
            // -------------------------------------------------------------------------------
            #region delete message routine
            int iMessageID = Convert.ToInt32(strMessageID);
            // deleye message 
            switch (strCurrentFolder)
            {
                    // --------------------------------------------------------
                    //  Sent message to trash can 
                    // --------------------------------------------------------
                case AHADefs.FolderType.MESSAGE_FOLDER_SENT_MESSAGES:
                case AHADefs.FolderType.MESSAGE_FOLDER_INBOX:
                    // ------------------------------------------------------------
                    Message.DeleteMessage(this.CurrentUserDetails, iMessageID);
                    break;
                    // ----------------------------------------------------------
                case AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN:
                    // ----------------------------------------------------------
                    _PurgeMessage(iMessageID);
                    break;
                default:
                    break;
            }
            #endregion 
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iMessageID"></param>
        private void _PurgeMessage(int iMessageID)
        {
            // -----------------------------------------------------------------------
            // deleted a message from trash can folder also check if the message is 
            //  required to be be removed from the HV
            // -----------------------------------------------------------------------
            #region remove message 
            DBContext dbContext = null;
            try
            {
                dbContext = DBContext.GetDBContext();
                //Message objMessage = Message.GetMessageDetails(this.CurrentUserDetails, this.CurrentFolderType, iMessageID);
                bool bDeleteFromHV = Message.DeleteMessage(this.CurrentUserDetails, iMessageID);

                if (bDeleteFromHV)
                {
                    HVManager.MessageDataManager.MessageItem objMessageItem = MessageWrapper.GetMessageByIdForPatient(iMessageID);
                    if (objMessageItem != null)
                    {
                        MessageWrapper.DeleteMessage(objMessageItem.ThingKey, PatientGuids.CurrentPatientGuids.PersonID, PatientGuids.CurrentPatientGuids.RecordID);
                    }
                }
                dbContext.ReleaseDBContext(true);
            }
            catch { }
            #endregion 
        }
    }
}