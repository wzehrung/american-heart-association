﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Heart360Web;

namespace Heart360Web.Controls.Patient
{
    public partial class ModuleDeleteReading : System.Web.UI.UserControl
    {
        private Heart360Web.PatientPortal.MODULE_ID     mModuleID;
        private string                                  mEditKeys;         // one or more comma separated values representing the "keys" to the database item to delete.

        public ModuleDeleteReading() : base()
        {
            mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
            mEditKeys = string.Empty ;
        }

        public int ModuleIdAsInt      // need this so (html/asp) markup can specify modules
        {
            set
            {
                mModuleID = Heart360Web.PatientPortal.GetModuleIdFromInt(value);
            }
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        public string EditKeys
        {
            set { mEditKeys = value; }
        }

        public int? mHbA1c;
        public int? HbA1c
        {
            set { mHbA1c = value; }
            get { return mHbA1c; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HbA1c != 1)
            {
                switch (ModuleID)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                        this.litTitle.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_DeleteReading_BloodPressure").ToString();
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        this.litTitle.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_DeleteReading_Weight").ToString();
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                        this.litTitle.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_DeleteReading_PhysicalActivity").ToString();
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                        this.litTitle.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_DeleteReading_BloodGlucose").ToString();
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                        this.litTitle.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_DeleteReading_Cholesterol").ToString();
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                        this.litTitle.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_DeleteReading_Medication").ToString();
                        break;

                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                    default:
                        this.litTitle.Text = "INVALID MODULE ID";
                        break;
                }
            }
            else
            {
                litTitle.Text = GetGlobalResourceObject("Tracker", "Tracker_Title_DeleteReading_HbA1c").ToString();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            bool successful = true;
            try
            {
                string[] guidKeys = mEditKeys.Split(',');

                Guid guidThingID = new Guid(guidKeys[0]);
                Guid? guidVersionStamp = null;
                if (guidKeys.Count() > 0)
                    guidVersionStamp = new Guid(guidKeys[1]);
                Microsoft.Health.HealthRecordItemKey hrik = new Microsoft.Health.HealthRecordItemKey(guidThingID, guidVersionStamp.Value);

                if (HbA1c == null)
                {
                    switch (ModuleID)
                    {
                        // NOTE: BR, BG and C only delete the item from HealthVault
                        case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                            HVHelper.HVManagerPatientBase.BloodPressureDataManager.DeleteItem(hrik);
                            break;

                        case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                            HVHelper.HVManagerPatientBase.BloodGlucoseDataManager.DeleteItem(hrik);
                            break;

                        case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                            HVHelper.HVManagerPatientBase.CholesterolDataManager.DeleteItem(hrik);
                            break;

                        // NOTE: MED, PA and W delete the item from HealthVault **and** local DB. Note sure why.
                        case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                            HVHelper.HVManagerPatientBase.MedicationDataManager.DeleteItem(hrik);
                            //DB Update
                            if (AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
                            {
                                AHAHelpContent.Medication.DropMedication(hrik.Id);
                            }
                            break;

                        case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                            HVHelper.HVManagerPatientBase.ExerciseDataManager.DeleteItem(hrik);
                            //DB Update
                            if (AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
                            {
                                AHAHelpContent.Exercise.DropExercise(hrik.Id);
                            }
                            break;

                        case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                            HVHelper.HVManagerPatientBase.WeightDataManager.DeleteItem(hrik);
                            //DB Update
                            if (AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
                            {
                                AHAHelpContent.WeightDetails.DropWeight(hrik.Id);
                            }
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    HVHelper.HVManagerPatientBase.HbA1cDataManager.DeleteItem(hrik);
                }

            }
            catch (Exception ee)
            {
                successful = false;
            }

            if (successful)
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }
            else
            {
                lblDescription.InnerText = GetGlobalResourceObject("Tracker", "Tracker_DeleteReading_Failed").ToString();
                btnOk.Enabled = false;
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            // Let the popup handle how we close
            ((Heart360Web.Pages.Patient.PopUp)Page).CancelPopup();
        }
    }
}