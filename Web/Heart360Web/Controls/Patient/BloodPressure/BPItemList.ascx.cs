﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;
using HVManager;
using AHACommon;

namespace Heart360Web.Controls.Patient.BloodPressure
{
    public partial class BPItemList : System.Web.UI.UserControl, IDataList
    {
        #region Private Members

        const string _vgAdd = "Add";
        const string _vgEdit = "Edit";

        // Givens: all interaction with the list (Edit, Delete) is done through a popup with no postback.
        //         This control does not change/manage current date range
        //
        // How it works: we always render the control on Page_Load!
        //               ModuleDetails manages ViewState["DataRange"]

/**
        AHACommon.DataDateReturnType CurrentDateRange
        {
            get
            {
                if (ViewState["DataRange"] != null)
                    return (AHACommon.DataDateReturnType)ViewState["DataRange"];
                else
                    return AHACommon.DataDateReturnType.Last3Months;
            }
            set
            {
                if (ViewState["DataRange"] == null)
                {
                    ViewState.Add("DataRange", value);
                }
                else
                {
                    ViewState["DataRange"] = value;
                }
            }
        }
**/
        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "date";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return false;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }

        #endregion

        #region Public Properties

        IPeriodicDataManager m_IPeriodicDataManager = null;
        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            /**
            if (!Page.IsPostBack || CurrentDateRange != m_IPeriodicDataManager.DateRange)
            {
                CurrentDateRange = m_IPeriodicDataManager.DateRange;

                _bindDataToGridView();
            }
             **/

            //_bindDataToGridView();  // no matter if its a post-back or not.
            
            //linkRefreshSiteData.OnClientClick = string.Format("return RefreshSiteDataPatientForUrl('{0}');", AHAAPI.WebAppNavigation.H360.PAGE_TRACKER_BP);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            _bindDataToGridView();
        }


        private void _bindDataToGridView()
        {
            DataDateReturnType ddrt = m_IPeriodicDataManager.DateRange;
            LinkedList<BloodPressureDataManager.BPItem> ascendingList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataBetweenDates(ddrt);

            LinkedList<BloodPressureDataManager.BPItem> descendingList = new LinkedList<BloodPressureDataManager.BPItem>();
            foreach (BloodPressureDataManager.BPItem bpItem in ascendingList.Reverse())
            {
                descendingList.AddLast(bpItem);
            }

            
            rptHistory.DataSource = descendingList;
            rptHistory.DataBind();

            // NOTE: If there is no history, there will just be an empty table.
/**
            gvList.DataKeyNames = new string[] { "ThingID", "VersionStamp" };
            gvList.DataSource = desendingList;
            gvList.DataBind();

            if (descendingList.Count == 0)
            {
                DivNoData.Visible = true;
                DivData.Visible = false;
            }
            else
            {
                DivNoData.Visible = false;
                DivData.Visible = true;
            }
 **/
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BloodPressureDataManager.BPItem item = e.Item.DataItem as BloodPressureDataManager.BPItem;

                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litTime = e.Item.FindControl("litTime") as Literal;
                Literal litHeartRate = e.Item.FindControl("litHR") as Literal;
                Literal litSystolic = e.Item.FindControl("litSys") as Literal;
                Literal litDiastolic = e.Item.FindControl("litDia") as Literal;
                Literal litHeartbeat = e.Item.FindControl("litHeartbeat") as Literal;
                Literal litReadingSource = e.Item.FindControl("litSource") as Literal;
                Literal litComment = e.Item.FindControl("litComment") as Literal;

                
                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString( item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.SLASH, GRCBase.GRCTimeFormat.NONE);
                litTime.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString( item.When.Value, GRCBase.GRCDateFormat.NONE, GRCBase.GRCDateSeperator.NONE, GRCBase.GRCTimeFormat.HHMMAMPM);
                if (item.HeartRate.Value.HasValue)
                {
                    litHeartRate.Text = item.HeartRate.Value.Value.ToString();
                }

                if ((AHAAppSettings.ColorCoding_EffectiveDays == null) || item.EffectiveDate.CompareTo(DateTime.Now.AddDays(AHAAppSettings.ColorCoding_EffectiveDays.Value)) == 1)
                {
                    RiskLevelHelper.RiskLevelBloodPressure bpRL = new RiskLevelHelper.RiskLevelBloodPressure(item.Systolic.Value, item.Diastolic.Value);
                
                    litSystolic.Text = "<td class=\"" + bpRL.ColorReading1 + "\">" + item.Systolic.Value.ToString() + "</td>";
                    litDiastolic.Text = "<td class=\"" + bpRL.ColorReading2 + "\">" + item.Diastolic.Value.ToString() + "</td>";
                }
                else
                {
                    litSystolic.Text = "<td>" + item.Systolic.Value.ToString() + "</td>";
                    litDiastolic.Text = "<td>" + item.Diastolic.Value.ToString() + "</td>";
                }

                //if (item.IrregularHeartbeat.Value.HasValue)
                //{
                    string strIrregularHeartbeat = item.IrregularHeartbeat.Value.ToString();
                    // Change null/true/false to Don't Know/Yes/No
                    switch (strIrregularHeartbeat)
                    {
                        case "True":
                            strIrregularHeartbeat = GetGlobalResourceObject("Tracker", "BloodPressure_History_IrregularHeartbeat_Yes").ToString();
                            break;
                        case "False":
                            strIrregularHeartbeat = GetGlobalResourceObject("Tracker", "BloodPressure_History_IrregularHeartbeat_No").ToString();
                            break;
                        default:
                            strIrregularHeartbeat = GetGlobalResourceObject("Tracker", "BloodPressure_History_IrregularHeartbeat_Unknown").ToString();
                            break;
                    }

                    litHeartbeat.Text = strIrregularHeartbeat;
                //}

                litReadingSource.Text = item.Source.Value;
                if (!string.IsNullOrEmpty(item.Note.Value))
                {
                    if (item.Note.Value.Length > 18)
                    {
                        // comment is truncated and entire comment is put in a tooltip
                        litComment.Text = "<span data-tooltip=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + item.Note.Value + "\">" +
                                            GRCBase.StringHelper.GetShortString(item.Note.Value, 18, true) + "</span>";
                    }
                    else
                    {
                        // entire comment fits in cell.
                        litComment.Text = item.Note.Value;
                    }
                }

                //
                // Hook up Actions
                //
                ImageButton actEdit = e.Item.FindControl("actEditor") as ImageButton;
                ImageButton actDelete = e.Item.FindControl("actTrash") as ImageButton;
                string      itemKey = item.ThingID.ToString() ;

                if (actEdit != null)
                {
                    string editUrl = "PopUp.aspx?reading=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE).ToString() + "&elid=" + itemKey;
                    actEdit.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE);
                }
                if (actDelete != null)
                {
                        // delete requires both ThingID and VersionStamp
                    itemKey += "," + item.VersionStamp.ToString() ;
                    string trashUrl = "PopUp.aspx?delete=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE).ToString() + "&elid=" + itemKey;
                    actDelete.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE);
                }
            }
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        public void LoadItems(DataDateReturnType DateRange)
        {
            SortAscending = false;
            SortExpression = string.Empty;

            //CurrentDateRange = DateRange;

 //           _bindDataToGridView();
        }
    }
}