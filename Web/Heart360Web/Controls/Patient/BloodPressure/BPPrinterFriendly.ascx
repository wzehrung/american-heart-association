﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BPPrinterFriendly.ascx.cs" Inherits="Heart360Web.Controls.Patient.BloodPressure.BPPrinterFriendly" %>

<%@ Register Src="~/Controls/Patient/ModuleItemGraph.ascx" TagName="BPGraph" TagPrefix="printerFriendlyInnerCtrl" %>

<h3><%=GetGlobalResourceObject("Reports", "Text_Heading_MyBpRecord")%></h3>
<printerFriendlyInnerCtrl:BPGraph ID="GraphCtrl" runat="server" />
<div class="full history-table-wrap" >
    <asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="false" runat="server">
        <HeaderTemplate>
    	         <table>
		            <thead>
			            <tr>
				            <th width="20%"><%=GetGlobalResourceObject("Reports", "Text_DateTime")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_Heart_Rate")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_SystolicWithUnits")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_DiastolicWithUnits")%></th>
				            <th width="25%"><%=GetGlobalResourceObject("Reports", "Text_ReadingSource")%></th>
				            <th width="25%"><%=GetGlobalResourceObject("Reports", "Text_Comments")%></th>
			            </tr>
		            </thead>
                    <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
			    <td><asp:Literal ID="litDateTime" runat="server"/></td>
			    <td><asp:Literal ID="litHR" runat="server"/></td>
			    <td><asp:Literal ID="litSys" runat="server"/></td>
			    <td><asp:Literal ID="litDia" runat="server"/></td>
			    <td><asp:Literal ID="litSource" runat="server"/></td>
			    <td><asp:Literal ID="litComment" runat="server"/></td>
		    </tr>
        </ItemTemplate>
        <FooterTemplate>
                    </tbody>
	            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
