﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;
using HVManager;

namespace Heart360Web.Controls.Patient.BloodPressure
{
    public partial class BPPrinterFriendly : System.Web.UI.UserControl
    {
        IPeriodicDataManager m_IPeriodicDataManager = null;

        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
                GraphCtrl.IPeriodicDataManager = value;
            }
        }


        #region Private/Protected Functions

        private void _bindDataToGridView()
        {
            LinkedList<BloodPressureDataManager.BPItem> asendingList = HVHelper.HVManagerPatientBase.BloodPressureDataManager.GetDataBetweenDates(m_IPeriodicDataManager.DateRange);

            LinkedList<BloodPressureDataManager.BPItem> desendingList = new LinkedList<BloodPressureDataManager.BPItem>();
            foreach (BloodPressureDataManager.BPItem bpItem in asendingList.Reverse())
            {
                desendingList.AddLast(bpItem);
            }

            rptHistory.DataSource = desendingList;
            rptHistory.DataBind();
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            GraphCtrl.ModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE;
            GraphCtrl.Visible = true;
            GraphCtrl.UseDivTrigger = false;

            _bindDataToGridView();
        }

/**
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            _bindDataToGridView();
        }
**/


        #region GridView Event Handlers / Members

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BloodPressureDataManager.BPItem item = e.Item.DataItem as BloodPressureDataManager.BPItem;

                Literal litDateTime = e.Item.FindControl("litDateTime") as Literal;
                Literal litHeartRate = e.Item.FindControl("litHR") as Literal;
                Literal litSystolic = e.Item.FindControl("litSys") as Literal;
                Literal litDiastolic = e.Item.FindControl("litDia") as Literal;
                Literal litReadingSource = e.Item.FindControl("litSource") as Literal;
                Literal litComment = e.Item.FindControl("litComment") as Literal;


                litDateTime.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.HHMMAMPM);
                
                if (item.HeartRate.Value.HasValue)
                {
                    litHeartRate.Text = item.HeartRate.Value.Value.ToString();
                }

                litSystolic.Text = item.Systolic.Value.ToString() ;
                litDiastolic.Text = item.Diastolic.Value.ToString() ;
                litReadingSource.Text = item.Source.Value;
                litComment.Text = item.Note.Value;
            }
        }

        #endregion

        
    }
}