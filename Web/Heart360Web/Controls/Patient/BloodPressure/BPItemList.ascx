﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BPItemList.ascx.cs" Inherits="Heart360Web.Controls.Patient.BloodPressure.BPItemList" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>

<asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="true" runat="server">
    <HeaderTemplate>
    	     <table class="paginated">
		        <thead>
			        <tr>
				        <th class="tt-date">DATE</th>
				        <th class="tt-time">TIME</th>
				        <th class="bp-heartrate">HR</th>
				        <th class="bp-systolic">SYS</th>
				        <th class="bp-diastolic">DIA</th>
                        <th class="bp-heartbeat">IRREG</th>
				        <th class="bp-source">SOURCE</th>
				        <th class="tt-comments">COMMENTS</th>
				        <th class="tt-icon"></th><!-- EDIT TABLE ROW TH -->
                        <th class="tt-icon"></th>
			        </tr>
		        </thead>
                <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
			<td><asp:Literal ID="litDate" runat="server"/></td>
			<td><asp:Literal ID="litTime" runat="server"/></td>
			<td><asp:Literal ID="litHR" runat="server"/></td>
			<asp:Literal ID="litSys" runat="server"/><!-- add td for risk coloring -->
			<asp:Literal ID="litDia" runat="server"/><!-- add td for risk coloring -->
            <td><asp:Literal ID="litHeartbeat" runat="server" /></td>
			<td><asp:Literal ID="litSource" runat="server"/></td>
			<td><asp:Literal ID="litComment" runat="server"/></td>
			<td>
				<asp:ImageButton ID="actEditor" CssClass="add-note fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Edit BP reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
            <td>
                <asp:ImageButton ID="actTrash" CssClass="trash fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Delete BP reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
		</tr>
    </ItemTemplate>
    <FooterTemplate>
                </tbody>
	        </table>
    </FooterTemplate>
</asp:Repeater>


