﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;

namespace Heart360Web.Controls.Patient
{
    public partial class ModuleItemGraph : System.Web.UI.UserControl
    {
        public bool                             IsProvider = false;
        IPeriodicDataManager                    m_IPeriodicDataManager = null;
        Heart360Web.PatientPortal.MODULE_ID     mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
        bool                                    mUseDivTrigger = true;

        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        public int? GraphWidth
        {
            get;
            set;
        }

        public bool UseDivTrigger
        {
            set { mUseDivTrigger = value; }
        }

        // We render the graphs in PreRender because this stage is after any widget event handling (e.g. DateRange picker),
        // which happens after Page_Load.
        protected override void  OnPreRender(EventArgs e)
        {
 	        base.OnPreRender(e);

            Graph.AHAPlotData   plotData = null;
            PatientGuids        objGuids = null;
            DataDateReturnType  ddrt = m_IPeriodicDataManager.DateRange;

            this.divOptionalTrigger.Visible = mUseDivTrigger;
            this.HealthGraph1.Visible = false;
            if (IsProvider)
            {
                AHAAPI.Provider.ProviderBase objPage = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;

                objGuids = new PatientGuids
                {
                    PersonID = objPage.PersonID.Value,
                    RecordID = objPage.RecordID.Value
                };
            }
            else
            {
                objGuids = PatientGuids.CurrentPatientGuids;
            }


            Graph.GraphType gt1 = Graph.GraphType.AllInOne;
            Graph.GraphType gt2 = Graph.GraphType.AllInOne;
            bool do_gt1 = false;
            bool do_gt2 = false;

            switch (mModuleID)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    gt1 = Graph.GraphType.Glucose;
                    do_gt1 = true;
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    gt1 = Graph.GraphType.CombinedBP;
                    do_gt1 = true;
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    gt1 = Graph.GraphType.CombinedCholesterol;
                    gt2 = Graph.GraphType.Triglycerides;
                    do_gt1 = do_gt2 = true;
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    gt1 = Graph.GraphType.Exercise;
                    do_gt1 = true;
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    gt1 = Graph.GraphType.Weight;
                    do_gt1 = true;
                    break;

                default:
                    break;
            }

            if (do_gt1)
            {
                plotData = Graph.AHAPlotData.ConstructPlotData(objGuids, gt1, ddrt);

                if (this.GraphWidth.HasValue)
                {
                    plotData.m_graphWidth = this.GraphWidth.Value;
                }

                ((Heart360Web.Controls.Common.HealthGraph)this.HealthGraph1).PlotGraph(plotData);
            }
            this.HealthGraph1.Visible = do_gt1;

            if (do_gt2)
            {
                plotData = Graph.AHAPlotData.ConstructPlotData(objGuids, gt2, ddrt);

                if (this.GraphWidth.HasValue)
                {
                    plotData.m_graphWidth = this.GraphWidth.Value;
                }

                ((Heart360Web.Controls.Common.HealthGraph)this.HealthGraph2).PlotGraph(plotData);
            }
            this.HealthGraph2.Visible = do_gt2;
        }

    }
}