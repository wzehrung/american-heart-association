﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleAddGoal.ascx.cs" Inherits="Heart360Web.Controls.Patient.ModuleAddGoal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<div class="full">
    <!-- ## Add/Edit Goal Title ## -->
	<div class="full">
		<h3><asp:Literal ID="litTitle" runat="server"></asp:Literal></h3>
	</div>

    <!-- ## Add Goal Validation Area, one summary for each tracker except BG  ## -->
    <div class="large-12 small-12 columns spb" >
        <asp:ValidationSummary ID="vsBP" ValidationGroup="vgBP"
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            Enabled="true"
            CssClass="validation-error"
            DisplayMode="BulletList"
            ShowSummary="true"
            runat="server" /> 

        <asp:ValidationSummary ID="vsCH" ValidationGroup="vgCH"
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            Enabled="true"
            CssClass="validation-error"
            DisplayMode="BulletList"
            ShowSummary="true"
            runat="server" /> 

        <asp:ValidationSummary ID="vsCHPostCalculate" ValidationGroup="vgCHPostCalculate"
                    HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                    Enabled="true"
                    CssClass="validation-error"
                    DisplayMode="BulletList"
                    ShowSummary="true"
                    runat="server" /> 

        <asp:ValidationSummary ID="vsPA" ValidationGroup="vgPA"
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            Enabled="true"
            CssClass="validation-error"
            DisplayMode="BulletList"
            ShowSummary="true"
            runat="server" /> 

        <asp:ValidationSummary ID="vsW" ValidationGroup="vgW"
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            Enabled="true"
            CssClass="validation-error"
            DisplayMode="BulletList"
            ShowSummary="true"
            runat="server" /> 
    </div>

    <!-- ## Descriptive text for the goal ## -->
    <div id="divGoalDescription" class="full spb">
        <p><asp:Literal ID="litGoalDescription" runat="server" /></p>
    </div>

    

    <!-- ## BEGIN Blood Pressure ## -->
    <div id="divBloodPressure" visible="false" runat="server">
        <div class="row">
	        <div class="large-3 small-12 columns">
		        <label class="text-left" for="systolic"><%=GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodPressure_Systolic")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
		        <asp:TextBox ID="txtSystolic" CssClass="required" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="vBPSystolic" runat="server" EnableClientScript="false" ValidationGroup="vgBP" Display="None" OnServerValidate="ValidateBPSystolic"></asp:CustomValidator>
	        </div>
        </div>

        <div class="row">
	        <div class="large-3 small-12 columns">
		        <label class="text-left" for="diastolic"><%=GetGlobalResourceObject("Tracker", "Tracker_Title_AddReading_BloodPressure_Diastolic")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
		        <asp:TextBox ID="txtDiastolic" CssClass="required" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="vBPDiastolic" runat="server" EnableClientScript="false" ValidationGroup="vgBP" Display="None" OnServerValidate="ValidateBPDiastolic"></asp:CustomValidator>
	        </div>
        </div>      
    </div>
    <!-- ## END Blood Pressure ## -->      

    <!-- ## BEGIN Weight ## -->
    <div id="divWeight" visible="false" runat="server">
        <div class="row">
            <div class="large-3 small-12 columns">
		        <label class="text-left" for="startdate"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_StartDate")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
		        <asp:TextBox ID="txtStartDate" CssClass="required" runat="server"></asp:TextBox>
                <asp:ImageButton ID="btnCalendarStartDate" CssClass="calendar-popup-button" runat="server" BorderStyle="None" style="border:none !important;" value="" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                <asp:CalendarExtender ID="calexStartDate" TargetControlID="txtStartDate" PopupButtonID="btnCalendarStartDate" CssClass="popup_cal" runat="server">
                </asp:CalendarExtender>
                <asp:CustomValidator ID="vWStartDate" runat="server" EnableClientScript="false" ValidationGroup="vgW" Display="None" OnServerValidate="ValidateWStartDate"></asp:CustomValidator>
	        </div>
        </div>
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="startweight"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_StartWeight")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:TextBox ID="txtStartWeight" CssClass="required" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="vWStartWeight" runat="server" EnableClientScript="false" ValidationGroup="vgW" Display="None" OnServerValidate="ValidateWStartWeight"></asp:CustomValidator>
            </div>
        </div>

        <div class="row">
            <div class="large-3 small-12 columns">
		        <label class="text-left" for="targetdate"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_TargetDate")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
		        <asp:TextBox ID="txtTargetDate" CssClass="required" runat="server"></asp:TextBox>
                <asp:ImageButton ID="btnCalendarTargetDate" CssClass="calendar-popup-button" runat="server" BorderStyle="None" style="border:none !important;" value="" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                <asp:CalendarExtender ID="calexTargetDate" TargetControlID="txtTargetDate" PopupButtonID="btnCalendarTargetDate" CssClass="popup_cal" runat="server">
                </asp:CalendarExtender>
                <asp:CustomValidator ID="vWTargetDate" runat="server" EnableClientScript="false" ValidationGroup="vgW" Display="None" OnServerValidate="ValidateWTargetDate"></asp:CustomValidator>
	        </div>
        </div>
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="targetweight"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_TargetWeight")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:TextBox ID="txtTargetWeight" CssClass="required" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="vWTargetWeight" runat="server" EnableClientScript="false" ValidationGroup="vgW" Display="None" OnServerValidate="ValidateWTargetWeight"></asp:CustomValidator>
            </div>
        </div>
    </div>
    <!-- ## END Weight ## -->
            
    <!-- ## BEGIN Physical Activity ## -->
    <div id="divActivity" visible="false" runat="server">
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="duration"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Duration")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:TextBox ID="txtDuration" CssClass="required" runat="server" MaxLength="6"></asp:TextBox>
                <asp:CustomValidator ID="vPADuration" runat="server" EnableClientScript="false" ValidationGroup="vgPA" Display="None" OnServerValidate="ValidatePADuration"></asp:CustomValidator>
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="intensity"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Intensity")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:DropDownList ID="ddlIntensity" CssClass="required" runat="server"></asp:DropDownList>
            </div>
        </div>
    </div>
    <!-- ## END Physical Activity ## -->
            
    <!-- No Blood Glucose Goal -->
            
    <!-- ## BEGIN Cholesterol ## -->
    <div id="divCholesterol" visible="false" runat="server">
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="hdl"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_HDL")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:TextBox ID="txtHDL" CssClass="required" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="vCHHdl" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHHdl"></asp:CustomValidator>
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="ldl"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_LDL")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:TextBox ID="txtLDL" CssClass="required" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="vCHLdl" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHLdl"></asp:CustomValidator>
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="triglycerides"><%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_Triglycerides")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:TextBox ID="txtTriglycerides" CssClass="required" runat="server"></asp:TextBox>
                <asp:CustomValidator ID="vCHTrig" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHTrig"></asp:CustomValidator>
            </div>
        </div>
        <div class="row spb">
            <div class="large-3 small-3 columns"><label class="text-left" for="spacer" ></label></div>
            <div class="large-9 small-9 columns">
                <asp:Button ID="btnCalculate" runat="server" CssClass="gray-button full" Text="<%$Resources:Common,Text_Calculate %>" OnClick="OnRecalculateTC" CommandName="Add" />
                <asp:CustomValidator ID="vCHTc" runat="server" EnableClientScript="false" ValidationGroup="vgCH" Display="None" OnServerValidate="ValidateCHTotalC"></asp:CustomValidator>
                <asp:CustomValidator ID="vGHTc2" runat="server" EnableClientScript="false" ValidationGroup="vgCHPostCalculate" Display="None" OnServerValidate="ValidateCHPostTotalC"></asp:CustomValidator>
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-12 columns">
                <label class="text-left" for="totalc"> <%=GetGlobalResourceObject("Tracker", "Tracker_AddReading_TotalCholesterol")%>:</label>
            </div>
            <div class="large-9 small-12 columns">
                <asp:TextBox ID="txtTotalCholesterol" CssClass="required" runat="server"></asp:TextBox>
            </div>
        </div>
                              
    </div>
    <!-- ## END Cholesterol ## -->

    <!-- ## BEGIN Common Data Entry ## -->
    
    <div class="save row spt">
        <div class="large-12 small-12 columns">
            <asp:Button ID="btnSave" CssClass="gray-button full" runat="server" onclick="btnSave_Click" />
        </div>
        
        <div class="large-12 small-12 columns">
            <br />
            <asp:Button ID="btnDelete" CssClass="gray-button full" runat="server" onclick="btnDelete_Click" />
        </div>
    </div>


    <!-- ## END Common Data Entry ## -->


</div>

<%-- Filters are used to only allow numbers --%>
<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender1"
    runat="server"
    Enabled="true"
    TargetControlID="txtSystolic"
    FilterType="Numbers">
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender2"
    runat="server"
    Enabled="true"
    TargetControlID="txtDiastolic"
    FilterType="Numbers">
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender3"
    runat="server"
    Enabled="true"
    TargetControlID="txtStartWeight"
    FilterType="Numbers, Custom"
    ValidChars="." >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender4"
    runat="server"
    Enabled="true"
    TargetControlID="txtTargetWeight"
    FilterType="Numbers, Custom"
    ValidChars="." >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender5"
    runat="server"
    Enabled="true"
    TargetControlID="txtDuration"
    FilterType="Numbers, Custom" ValidChars="." >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender6"
    runat="server"
    Enabled="true"
    TargetControlID="txtHDL"
    FilterType="Numbers" >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender7"
    runat="server"
    Enabled="true"
    TargetControlID="txtLDL"
    FilterType="Numbers" >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender8"
    runat="server"
    Enabled="true"
    TargetControlID="txtTriglycerides"
    FilterType="Numbers" >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender
    ID="FilteredTextBoxExtender9"
    runat="server"
    Enabled="true"
    TargetControlID="txtTotalCholesterol"
    FilterType="Numbers" >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender 
    ID="FilteredTextBoxExtender10" 
    runat="server" 
    Enabled="true" 
    TargetControlID="txtStartDate" 
    FilterType="Numbers,Custom"
    ValidChars="/" >
</asp:FilteredTextBoxExtender>

<asp:FilteredTextBoxExtender 
    ID="FilteredTextBoxExtender11" 
    runat="server" 
    Enabled="true" 
    TargetControlID="txtTargetDate" 
    FilterType="Numbers,Custom"
    ValidChars="/" >
</asp:FilteredTextBoxExtender>



<asp:HiddenField ID="hidCalculate" runat="server" Value="0" />
