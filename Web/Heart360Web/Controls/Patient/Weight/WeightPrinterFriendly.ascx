﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WeightPrinterFriendly.ascx.cs" Inherits="Heart360Web.Controls.Patient.Weight.WeightPrinterFriendly" %>

<%@ Register Src="~/Controls/Patient/ModuleItemGraph.ascx" TagName="BPGraph" TagPrefix="printerFriendlyInnerCtrl" %>

<h3><%=GetGlobalResourceObject("Reports", "Text_Heading_MyWeightRecord")%></h3>
<printerFriendlyInnerCtrl:BPGraph ID="GraphCtrl" runat="server" />
<div class="full history-table-wrap" >
    <asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="false" runat="server">
        <HeaderTemplate>
    	         <table>
		            <thead>
			            <tr>
				            <th width="20%"><%=GetGlobalResourceObject("Reports", "Text_DateTime")%></th>
				            <th width="15%"><%=GetGlobalResourceObject("Reports", "Text_Weight")%></th>
				            <th width="15%"><%=GetGlobalResourceObject("Reports", "Text_BMI")%></th>
				            <th width="25%"><%=GetGlobalResourceObject("Reports", "Text_ReadingSource")%></th>
				            <th width="25%"><%=GetGlobalResourceObject("Reports", "Text_Comments")%></th>
			            </tr>
		            </thead>
                    <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
			    <td><asp:Literal ID="litDateTime" runat="server"/></td>
			    <td><asp:Literal ID="litWeight" runat="server"/></td>
			    <td><asp:Literal ID="litBMI" runat="server"/></td>
			    <td><asp:Literal ID="litSource" runat="server"/></td>
			    <td><asp:Literal ID="litComment" runat="server"/></td>
		    </tr>
        </ItemTemplate>
        <FooterTemplate>
                    </tbody>
	            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
