﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WeightItemList.ascx.cs" Inherits="Heart360Web.Controls.Patient.Weight.WeightItemList" %>

<asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="true" runat="server">
    <HeaderTemplate>
    	     <table  class="paginated">
		        <thead>
			        <tr>
				        <th class="tt-date">DATE</th>
				        <th class="tt-time">TIME</th>
				        <th class="wt-weight">WEIGHT</th>
				        <th class="wt-bmi">BMI</th>
				        <th class="wt-source">SOURCE</th>
				        <th class="tt-comments">COMMENTS</th>
				        <th class="tt-icon"></th>
                        <th class="tt-icon"></th>
			        </tr>
		        </thead>
                <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
			<td><asp:Literal ID="litDate" runat="server"/></td>
			<td><asp:Literal ID="litTime" runat="server"/></td>
			<td><asp:Literal ID="litWeight" runat="server"/></td>
			<asp:Literal ID="litBMI" runat="server"/><!-- has to include td tag -->
			<td><asp:Literal ID="litSource" runat="server"/></td>
            <td><asp:Literal ID="litComment" runat="server"/></td>
			<td>
				<asp:ImageButton ID="actEditor" CssClass="add-note fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Edit WT reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
            <td>
                <asp:ImageButton ID="actTrash" CssClass="trash fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Delete WT reading" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
		</tr>
    </ItemTemplate>
    <FooterTemplate>
                </tbody>
	        </table>
    </FooterTemplate>
</asp:Repeater>
