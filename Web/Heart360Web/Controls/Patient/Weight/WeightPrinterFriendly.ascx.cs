﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;
using HVManager;

namespace Heart360Web.Controls.Patient.Weight
{
    public partial class WeightPrinterFriendly : System.Web.UI.UserControl
    {
        IPeriodicDataManager m_IPeriodicDataManager = null;

        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
                GraphCtrl.IPeriodicDataManager = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GraphCtrl.ModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT;
            GraphCtrl.Visible = true;
            GraphCtrl.UseDivTrigger = false;

            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            LinkedList<HVManager.WeightDataManager.WeightItem> asendingList = HVHelper.HVManagerPatientBase.WeightDataManager.GetDataBetweenDates(m_IPeriodicDataManager.DateRange);

            LinkedList<HVManager.WeightDataManager.WeightItem> desendingList = new LinkedList<HVManager.WeightDataManager.WeightItem>();
            foreach (HVManager.WeightDataManager.WeightItem weightItem in asendingList.Reverse())
            {
                desendingList.AddLast(weightItem);
            }

            rptHistory.DataSource = desendingList;
            rptHistory.DataBind();
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HVManager.WeightDataManager.WeightItem item = e.Item.DataItem as HVManager.WeightDataManager.WeightItem;

                Literal litDateTime = e.Item.FindControl("litDateTime") as Literal;
                Literal litWeight = e.Item.FindControl("litWeight") as Literal;
                Literal litBMI = e.Item.FindControl("litBMI") as Literal;
                Literal litReadingSource = e.Item.FindControl("litSource") as Literal;
                Literal litComment = e.Item.FindControl("litComment") as Literal;


                litDateTime.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.HHMMAMPM);
                litWeight.Text = GRCBase.StringHelper.GetFormattedDoubleString(item.CommonWeight.Value.ToPounds(), 2, false);
                double? bmiForWeight = UserSummary.GetBMIForCurrentRecordForWeight(PatientGuids.CurrentPatientGuids, item.CommonWeight.Value.ToPounds());
                if (bmiForWeight.HasValue)
                {
                    litBMI.Text = GRCBase.StringHelper.GetFormattedDoubleString(bmiForWeight.Value, 2, false);
                }
                litReadingSource.Text = item.Source.Value;
                litComment.Text = item.Note.Value;
            }
        }

    }
}