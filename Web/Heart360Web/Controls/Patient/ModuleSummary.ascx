﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSummary.ascx.cs" Inherits="Heart360Web.Controls.Patient.ModuleSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!-- ### BEGIN Tracker Widget ### -->
<asp:Literal ID="litTrackerBegin" runat="server" />

	<div class="tile-inner">

		<header class="tile-title">
			<h3><a id="aTrackerLink1" href="#" runat="server"><asp:Literal ID="litTitle" runat="server" /></a></h3>
            <asp:LinkButton ID="popupReading" runat="server" CssClass="add-this add-reading" data-name="generic" Visible="false" />
            <asp:LinkButton ID="popupGoal" runat="server" CssClass="add-this add-reading" data-name="generic"  Visible="false" />
            <asp:LinkButton ID="popupA1c" runat="server" CssClass="add-this add-reading" data-name="generic" Visible="false" />
		</header>

		<section class="tile-content">
			<a id="aTrackerLink2" class="tile-button" href="#" runat="server"></a>
		    <div class="tile-reading">
                <span id="spanGoals" class="my-goals" visible="false" runat="server">
                    <span><%=GetGlobalResourceObject("Tracker", "Tracker_Title_Goal") %></span>
                    <asp:Literal ID="litGoal1" runat="server"></asp:Literal><asp:Literal ID="litGoal2" runat="server"></asp:Literal>
                </span>

                <span id="spanA1c" class="my-goals" visible="false" runat="server">
                    <span style="color:#3a94b5;"><%=GetGlobalResourceObject("Tracker", "Tracker_Title_A1c") %>:
                    <asp:Literal ID="litHbA1c" runat="server"></asp:Literal></span>
                </span>
                
                <span id="spanBlank" class="my-goals" visible="false" runat="server"><span>&nbsp;</span></span>

			    <span class="tile-data">
                    <asp:Literal ID="litTrackerData" runat="server"></asp:Literal>
                </span>
		    </div>
		</section>

		<footer class="notifications">
			<span class="last-reading">
                <asp:Literal ID="litTrackerReadingA" runat="server" />
                <asp:Literal ID="litTrackerReadingB" runat="server" />
                <asp:LinkButton ID="linkMLCReport" runat="server" OnClick="DownloadMLCReport" Visible="false"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_MLC_ViewReport")%></asp:LinkButton>
            </span>
		</footer>

	</div>

<asp:Literal ID="litTrackerEnd" runat="server" />

<asp:HiddenField id="hidModuleID" runat="server" />
<!-- ### END Tracker Widget ### -->