﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MedicationDetails.ascx.cs" Inherits="Heart360Web.Controls.Patient.Medications.MedicationDetails" %>

<section class="my-actions info-stats full">
    <div class="accordion">
        <div class="toggle-btn">
            <h4><asp:Literal ID="litTitleLearnMore" runat="server" /><span class="gray-arrow"></span></h4>
        </div>
        <div class="content" >
            <ul>
		        <li id="liLearnMore1" runat="server" >
			        <span class="blue-circle-check"></span>
                    <p><a runat="server" id="lnkLearnMore1" target="_blank"></a></p>
		        </li>
		        <li id="liLearnMore2" runat="server" >
			        <span class="blue-circle-check"></span>
                    <p><a runat="server" id="lnkLearnMore2" target="_blank"></a></p>
		        </li>
		        <li id="liLearnMore3" runat="server" >
			        <span class="blue-circle-check"></span>
                    <p><a runat="server" id="lnkLearnMore3" target="_blank"></a></p>
		        </li>
		        <li id="liLearnMore4" runat="server" >
			        <span class="blue-circle-check"></span>
                    <p><a runat="server" id="lnkLearnMore4" target="_blank"></a></p>
		        </li>
		        <li id="liLearnMore5" runat="server" >
			        <span class="blue-circle-check"></span>
                    <p><a runat="server" id="lnkLearnMore5" target="_blank"></a></p>
		        </li>
	        </ul>
        </div>
    </div>
</section>
<section class="my-history info-stats full">
	<div class="accordion">
		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Title_CurrentMeds")%><span class="gray-arrow"></span></h4>
		</div>
        <div class="content" >
             <asp:UpdatePanel ID="updatePanelCurrMeds" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:PlaceHolder ID="phCurrentMedsTable" runat="server" />
                    <div class="full my-status-controls">
			            <div class="large-1 hide-for-small columns"></div>
				        <div class="large-7 large-offset-4 small-12 columns">
				            <div class="row">
						        <!-- due to float right (fr), order needs to be inverted -->		
						        <div class="fr large-2 small-12 columns">
							        <div class="row">
								        <div class="large-6 small-6 columns">
									        <asp:LinkButton CssClass="pdf" runat="server" OnClick="OnPDFCurrent_Click"/>
								        </div>
								        <div class="large-6 small-6 columns">
									        <a href="#" class="print" runat="server" id='btnPrintCurrReport'></a>
								        </div>
							        </div>
						        </div>
                                <div class="fr large-5 small-12 columns">
							        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="updatePanelCurrMeds" runat="server" DisplayAfter="500">
                                        <ProgressTemplate>
                                            <asp:Image ID="ImageA" runat="server" ImageAlign="Middle" ImageUrl="~/images/ajax-loader.gif" />       
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>	
                                    <asp:Button ID="btnRefreshDataA" runat="server" CssClass="gray-button full" Text="<%$Resources:Tracker,Tracker_Text_RefreshData%>"  OnClick="OnRefreshDataA_Click" />
						        </div>	
                                <div class="fr large-5 small-12 columns"></div>
					        </div>
				        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>	
	</div>
</section>
<section class="my-history info-stats full">
	<div class="accordion">
		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Title_DiscontinuedMeds")%><span class="gray-arrow"></span></h4>
		</div>
        <div class="content" >
            <asp:UpdatePanel ID="updatePanelDisconMeds" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:PlaceHolder ID="phDiscontinuedMedsTable" runat="server" />	
                    <div class="full my-status-controls">
			            <div class="large-1 hide-for-small columns"></div>
				        <div class="large-7 large-offset-4 small-12 columns">
				            <div class="row">
                                <!-- due to float right (fr), order needs to be inverted -->	
						        <div class="fr large-2 small-12 columns">
							        <div class="row">
								        <div class="large-6 small-6 columns">
									        <asp:LinkButton CssClass="pdf" runat="server" OnClick="OnPDFDiscontinued_Click"/>
								        </div>
								        <div class="large-6 small-6 columns">
									        <a href="#" class="print" runat="server" id='btnPrintDisconReport'></a>
								        </div>
							        </div>
						        </div>
                                <div class="fr large-5 small-12 columns">
							        <asp:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID="updatePanelDisconMeds" runat="server" DisplayAfter="500">
                                        <ProgressTemplate>
                                            <asp:Image ID="ImageB" runat="server" ImageAlign="Middle" ImageUrl="~/images/ajax-loader.gif" />       
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>	
                                    <asp:Button ID="btnRefreshDataB" runat="server" CssClass="gray-button full" Text="<%$Resources:Tracker,Tracker_Text_RefreshData%>"  OnClick="OnRefreshDataB_Click" />
						        </div>	
                                <div class="fr large-5 small-12 columns"></div>
					        </div>
				        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>	
	</div>
</section>
<section class="learn-about info-stats full">
    <div class="accordion">
		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Title_Actions")%><span class="gray-arrow"></span></h4>
		</div>
		<div class="content">
            <!-- The actions section uses an ASP Repeater in the code behind -->
            <asp:Repeater ID="rptActions" OnItemDataBound="rptActions_ItemDataBound" runat="server">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <span class="blue-circle-check"></span>
                        <p><asp:Literal ID="litActionText" runat="server"></asp:Literal></p>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
		</div>
	</div>
</section><!-- END OF MY ACTIONS -->
<section class="compatiblity info-stats full">
	<div class="accordion">
		<div class="toggle-btn">
			<h4><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Title_Devices")%><span class="gray-arrow"></span></h4>
		</div>
		<div class="content">
			<ul>
				<li>
					<span class="circle-check"></span>
					<p><%=GetGlobalResourceObject("Tracker", "Tracker_Details_Devices_Description")%></p>
                    <a href="https://account.healthvault.com/Directory?target=Devices&SearchType=ViewAll" target="_blank"><%=GetGlobalResourceObject("Tracker", "Tracker_Text_LearnMore")%></a>
				</li>
							
			</ul>
		</div>
	</div>
</section>
