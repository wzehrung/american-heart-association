﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MedicationItemList.ascx.cs" Inherits="Heart360Web.Controls.Patient.Medications.MedicationItemList" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>

<asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="true" runat="server">
    <HeaderTemplate>
    	     <table class="paginated">
		        <thead>
			        <tr>
				        <th class="md-name">NAME</th>
				        <th class="md-strength">STRENGTH</th>
				        <th class="md-dosage">DOSAGE</th>
				        <th class="md-frequency">FREQUENCY</th>
				        <th class="md-sideeffects">SIDE EFFECTS/NOTES</th>
				        <th class="tt-icon"></th>
                        <th class="tt-icon"></th>
			        </tr>
		        </thead>
                <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
			<td><asp:Literal ID="litName" runat="server"/></td>
			<td><asp:Literal ID="litStrength" runat="server"/></td>
			<td><asp:Literal ID="litDosage" runat="server"/></td>
			<td><asp:Literal ID="litFrequency" runat="server"/></td>
			<td><asp:Literal ID="litSideEffects" runat="server"/></td>
			<td>
				<asp:ImageButton ID="actEditor" CssClass="add-note fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Edit medication" ImageUrl="~/images/1x1.png" CausesValidation="false" />
            </td>
            <td>
                <asp:ImageButton ID="actTrash" CssClass="trash fl" runat="server" BorderStyle="None" style="border:none !important;" alt="Delete medication" ImageUrl="~/images/1x1.png" CausesValidation="false" Visible="false" />
            </td>
		</tr>
    </ItemTemplate>
    <FooterTemplate>
                </tbody>
	        </table>
    </FooterTemplate>
</asp:Repeater>