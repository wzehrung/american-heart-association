﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;
using HVManager;

namespace Heart360Web.Controls.Patient.Medications
{
    public partial class MedicationPrinterFriendly : System.Web.UI.UserControl
    {
        bool                    m_IsCurrentMeds = true;
        IPeriodicDataManager    m_IPeriodicDataManager = null;

        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }

        public bool IsCurrentMeds
        {
            set
            {
                m_IsCurrentMeds = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            litTitle.Text = GetGlobalResourceObject("Reports", ((m_IsCurrentMeds) ? "Text_Heading_MyCurrMed" : "Text_Heading_MyDisMed")).ToString();

            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            LinkedList<HVManager.MedicationDataManager.MedicationItem> ascendingList = HVHelper.HVManagerPatientBase.MedicationDataManager.GetDataBetweenDates(m_IPeriodicDataManager.DateRange);
            List<HVManager.MedicationDataManager.MedicationItem> desendingList = new List<HVManager.MedicationDataManager.MedicationItem>();

            foreach (HVManager.MedicationDataManager.MedicationItem medicationItem in ascendingList)
            {
                DateTime? dtDiscontinued = null;

                if (medicationItem.HasDiscontinued
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate != null
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.HasValue)
                {
                    dtDiscontinued = new DateTime(medicationItem.DateDiscontinued.Value.HVApproximateDate.Year, medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.Value, medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.Value);
                }

                if (m_IsCurrentMeds &&
                    (!medicationItem.HasDiscontinued || (dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() > DateTime.Now.ToUniversalTime())))
                {
                    desendingList.Add(medicationItem);
                }
                else if (!m_IsCurrentMeds &&
                         ((medicationItem.HasDiscontinued && !dtDiscontinued.HasValue) ||
                          (medicationItem.HasDiscontinued && dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() <= DateTime.Now.ToUniversalTime())))
                {
                    desendingList.Add(medicationItem);
                }
            }
            desendingList = desendingList.OrderByDescending(i => i.EffectiveDate).ToList();

            rptHistory.DataSource = desendingList;
            rptHistory.DataBind();
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HVManager.MedicationDataManager.MedicationItem item = e.Item.DataItem as HVManager.MedicationDataManager.MedicationItem;

                Literal litName = e.Item.FindControl("litName") as Literal;
                Literal litStrength = e.Item.FindControl("litStrength") as Literal;
                Literal litDosage = e.Item.FindControl("litDosage") as Literal;
                Literal litFreq = e.Item.FindControl("litFreq") as Literal;
                Literal litSideEffects = e.Item.FindControl("litSideEffects") as Literal;

                litName.Text = item.Name.Value;
                litStrength.Text = item.Strength.Value;
                litDosage.Text = item.Dosage.Value;
                litFreq.Text = item.Frequency.Value;
                litSideEffects.Text = item.Note.Value; //this is side effects/notes
            }
        }


    }
}