﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;

using GRCBase.WebControls_V1;


namespace Heart360Web.Controls.Patient.Medications
{
    public partial class MedicationAddMed : System.Web.UI.UserControl
    {
        const string _vgAdd = "vgAdd";

        private string mEditKeys;         // one or more comma separated values representing the "keys" to the database item to edit.

        public MedicationAddMed()
            : base()
        {
            mEditKeys = string.Empty ;
        }

        public string EditKeys
        {
            set { mEditKeys = value; }
        }

        private bool IsEditMode
        {
            get;
            set;
        }

        private enum MedicationSteps
        {
            viewSearch = 0,
            viewMedicationFound = 1,
            viewMedicationNotFound = 2,
            viewMedicationAdd = 3
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            // We need to see if this is an edit session (logic dictated by existance of edit keys
            if (!string.IsNullOrEmpty(mEditKeys))
            {

                // TODO: try and get the data item associated with the keys.
                // if we are successful, we are in edit mode.
                IsEditMode = true;
            }

            _BindStrengthWithDefault();
            _LoadDosage();
            _LoadFrequency();

            if (!Page.IsPostBack)
            {
                _SetTitle();
                multiViewMedicationForm.ActiveViewIndex = 0;
                autoSuggestSearch.Text = string.Empty;
            }      
        }

        private void _SetTitle()
        {
           
            litTitle.Text = GetGlobalResourceObject("Tracker", ((IsEditMode) ? "Popup_Medication_EditMedication" : "Popup_Medication_AddMedication")).ToString();
        }

        private void _LoadFrequency()
        {
            ListItemCollection FrequencyList = new ListItemCollection();
            //FrequencyList.Add(new ListItem("Frequency", ""));
            FrequencyList.Add(new ListItem(GetGlobalResourceObject("Common", "Text_Frequency").ToString(), ""));
            FrequencyList.Add(new ListItem("1x", "1"));
            FrequencyList.Add(new ListItem("2x", "2"));
            FrequencyList.Add(new ListItem("3x", "3"));
            FrequencyList.Add(new ListItem("4x", "4"));
            FrequencyList.Add(new ListItem("5x", "5"));
            //FrequencyList.Add(new ListItem("As needed", "0"));
            FrequencyList.Add(new ListItem(GetGlobalResourceObject("Tracker", "Popup_Medication_AsNeeded").ToString(), "0"));

            ddlFrequency.DataSource = FrequencyList;
            ddlFrequency.DataTextField = "Text";
            ddlFrequency.DataValueField = "Value";
            ddlFrequency.DataBind();

            ddlFrequency.SelectedValue = "";
            ddlFrequency.SelectedIndex = 0;
        }

        private void _LoadDosage()
        {
            ListItemCollection DosageList = new ListItemCollection();
            DosageList.Add(new ListItem(GetGlobalResourceObject("Tracker", "Popup_Medication_SelectDosage").ToString(), ""));
            DosageList.Add(new ListItem("0.5", "0.5"));
            DosageList.Add(new ListItem("1", "1"));
            DosageList.Add(new ListItem("1.5", "1.5"));
            DosageList.Add(new ListItem("2", "2"));
            DosageList.Add(new ListItem("2.5", "2.5"));
            DosageList.Add(new ListItem("3", "3"));
            DosageList.Add(new ListItem("4", "4"));
            DosageList.Add(new ListItem("5", "5"));
            DosageList.Add(new ListItem("6", "6"));
            DosageList.Add(new ListItem("7", "7"));
            DosageList.Add(new ListItem("8", "8"));
            DosageList.Add(new ListItem("9", "9"));
            DosageList.Add(new ListItem("10", "10"));

            ddlDosage.DataSource = DosageList;
            ddlDosage.DataTextField = "Text";
            ddlDosage.DataValueField = "Value";
            ddlDosage.DataBind();

            ddlDosage.SelectedValue = "";
            ddlDosage.SelectedIndex = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsEditMode)
            {
                if (!Page.IsPostBack)
                {
                    multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationAdd;
                    _SetUIForEdit();
                    //_LoadUserSelections;
                    //_SetUIForADD(false);
                    btnMedFoundAddCancel.Visible = false;
                    btnMedSaveAndAddAnother.Visible = false;
                }
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    _ClearFields();
                    multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewSearch;
                }
                listSuggesstions.Visible = false;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            divDiscontinued.Visible = ckbIsDiscontinued.Checked;
        }

        private void _SetUIForEdit()
        {
            Guid medguid = new Guid(mEditKeys);
            HVManager.MedicationDataManager.MedicationItem item = HVHelper.HVManagerPatientBase.MedicationDataManager.GetDataWithThingId(medguid);
            litFoundAddMeddicationName.Text = item.Name.Value;

            ddlStrength.SelectedValue = item.Strength.Value;
            txtStrength.Text = item.Strength.Value;

            ddlDosage.SelectedValue = item.Dosage.Value;
            txtDosage.Text = item.Dosage.Value;

            ddlFrequency.SelectedValue = item.Frequency.Value;
            txtFrequency.Text = item.Frequency.Value;

            teSideEffects.Value = item.Note.Value;

            calexDateDiscontinued.SelectedDate = DateTime.Now;
            tbDateDiscontinued.Text = DateTime.Now.ToShortDateString();
            if (item.HasDiscontinued)
            {
                ckbIsDiscontinued.Checked = true;
                divDiscontinued.Visible = true;
                if (item.DateDiscontinued.Value != null && item.DateDiscontinued.Value.HasValue())
                {
                    DateTime dt = new DateTime(
                        item.DateDiscontinued.Value.HVApproximateDate.Year,
                        item.DateDiscontinued.Value.HVApproximateDate.Month.HasValue ? item.DateDiscontinued.Value.HVApproximateDate.Month.Value : 1,
                        item.DateDiscontinued.Value.HVApproximateDate.Day.HasValue ? item.DateDiscontinued.Value.HVApproximateDate.Day.Value : 1);
                    calexDateDiscontinued.SelectedDate = dt;
                    tbDateDiscontinued.Text = dt.ToShortDateString();
                }
            }
            else
            {
                ckbIsDiscontinued.Checked = false;
                divDiscontinued.Visible = false;
            }

            if (item.StartDate.Value != null && item.StartDate.Value.HasValue())
            {
                DateTime dt = new DateTime(item.StartDate.Value.HVApproximateDate.Year,
                                        item.StartDate.Value.HVApproximateDate.Month.HasValue ? item.StartDate.Value.HVApproximateDate.Month.Value : 1,
                                        item.StartDate.Value.HVApproximateDate.Day.HasValue ? item.StartDate.Value.HVApproximateDate.Day.Value : 1);
                calDatePicker.SelectedDate = dt;
                txtDate.Text = dt.ToShortDateString();
            }
            else
            {
                calDatePicker.SelectedDate = DateTime.Now;
                txtDate.Text = DateTime.Now.ToShortDateString();
            }
        }

        protected void btnMedFoundAddMedication_Click(object sender, EventArgs e)
        {
            _Save(false);
        }

        protected void btnMedSaveAndAddAnother_Click(object sender, EventArgs e)
        {
            _Save(true);
        }

        private void _Save(bool saveAndAddAnotherMedication)
        {
            Page.Validate(_vgAdd);
            if (!Page.IsValid)
                return;

            if (IsEditMode)
            {
                _UpdateMedication();
            }
            else
            {
                _SaveMedication();
            }
            if (saveAndAddAnotherMedication)
            {
                _ClearAllFields();
                multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewSearch;
            }
            else
            {
                // Let the popup handle how we close
                ((Heart360Web.Pages.Patient.PopUp)Page).ClosePopup();
            }
        }

        private void _SaveMedication()
        {
            HVManager.MedicationDataManager.MedicationItem objItem = new HVManager.MedicationDataManager.MedicationItem();
            objItem.Name.Value = litFoundAddMeddicationName.Text;
            objItem.DateEntered.Value = DateTime.Now;
            if (hddnMedicationFound.Value == "1")
            {
                if (ddlStrength.Visible && !string.IsNullOrEmpty(ddlStrength.SelectedValue))
                {
                    objItem.Strength.Value = ddlStrength.SelectedValue;
                }
                else if (txtStrength.Visible && !string.IsNullOrEmpty(txtStrength.Text.Trim()))
                {
                    objItem.Strength.Value = txtStrength.Text.Trim();
                }

                if( ddlDosage.Visible && !string.IsNullOrEmpty(ddlDosage.SelectedValue) )
                {
                    objItem.Dosage.Value = ddlDosage.SelectedValue;
                }
                else if (txtDosage.Visible && !string.IsNullOrEmpty(txtDosage.Text.Trim()))
                {
                    objItem.Dosage.Value = txtDosage.Text.Trim();
                }

                if (ddlFrequency.Visible && !string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                {
                    objItem.Frequency.Value = ddlFrequency.SelectedValue;
                }
                else if( txtFrequency.Visible && !string.IsNullOrEmpty(txtFrequency.Text) )
                {
                    objItem.Frequency.Value = txtFrequency.Text;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtStrength.Text.Trim()))
                {
                    objItem.Strength.Value = txtStrength.Text.Trim();
                }

                if (!string.IsNullOrEmpty(txtDosage.Text.Trim()))
                {
                    objItem.Dosage.Value = txtDosage.Text.Trim();
                }

                if (!string.IsNullOrEmpty(txtFrequency.Text.Trim()))
                {
                    objItem.Frequency.Value = txtFrequency.Text.Trim();
                }
            }
            if (!string.IsNullOrEmpty(teSideEffects.Value.Trim()))
            {
                objItem.Note.Value = teSideEffects.Value.Trim();
            }

            DateTime    temper ;
            if (ckbIsDiscontinued.Checked && DateTime.TryParse( tbDateDiscontinued.Text, out temper ) )
            {
                objItem.DateDiscontinued.Value = new HVManager.HVApproximateDateTime( temper ) ; //calexDateDiscontinued.SelectedDate.Value);
            }

            if ( DateTime.TryParse( txtDate.Text, out temper ) )
            {
                objItem.StartDate.Value = new HVManager.HVApproximateDateTime( temper );
            }

            HVManager.MedicationDataManager.MedicationItem objNewItem = HVHelper.HVManagerPatientBase.MedicationDataManager.CreateItem(objItem);
            //DB Update
            //Update only if user has consented
            if (AHAAPI.SessionInfo.GetUserIdentityContext() != null && AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
            {
                AHAHelpContent.Medication.CreateOrChangeMedication(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                    objNewItem.ThingKey.Id, objNewItem.ThingKey.VersionStamp,
                    objNewItem.Name.Value, string.Empty,
                    objNewItem.Dosage.Value, objNewItem.Strength.Value, objNewItem.Frequency.Value,
                    calDatePicker.SelectedDate, calexDateDiscontinued.SelectedDate, objNewItem.Note.Value);
            }
        }

        private void _ClearFields()
        {
            listGenerics.Items.Clear();
            listBrands.Items.Clear();
            litMedicationName.Text = string.Empty;
            divDiscontinued.Visible = ckbIsDiscontinued.Checked;
        }

        protected void ValidateSearchString(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (autoSuggestSearch.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Popup_Medication_EnterSearchString").ToString();
                return;
            }
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            Page.Validate("Search");
            if (!Page.IsValid)
                return;
            _DoSearch();
        }

        private void _DoSearch()
        {
            string strRxText = autoSuggestSearch.Text.Trim();
            bool isExactMatch = RxNorm.RxUtility.DoesExactRxMatchExist(strRxText);
            if (isExactMatch)
            {
                DBManagerService db = new DBManagerService();

                string[] suggesstions = RxNorm.RxUtility.GetSpellingSuggestions(strRxText);
                //Correct the casing of the user entered string to what RxNorm reports
                //Arun - to avoid errors where GetSpellingSuggestions returs 0 elements and
                //we were doing a where on that (null or empty) collection
                if (suggesstions != null && suggesstions.Length > 0)
                {
                    strRxText = suggesstions.Where(x => x.ToLower() == strRxText.ToLower()).FirstOrDefault();
                }

                var sRxCui = (!string.IsNullOrEmpty(strRxText)) ? db.findRxcuiByString(strRxText) : null ;
                if ( sRxCui != null && sRxCui.Count() > 0)
                {
                    ViewState["CurrentRxCui"] = sRxCui[0];
                    ViewState["CurrentDrugText"] = strRxText;
                    var yy = db.getAllRelatedInfo(sRxCui[0]);

                    var res = db.getRelatedByType(sRxCui[0], new[] { "BN", "SCDF" });

                    List<RxConcept> brandList = new List<RxConcept>();
                    List<RxConcept> genericList = new List<RxConcept>();

                    brandList.AddRange(res.Where(x => x.type == "BN").Single().rxConcept);
                    genericList.AddRange(res.Where(x => x.type == "SCDF").Single().rxConcept);

                    listBrands.DataSource = brandList.Select(y => new { Text = y.STR, RXCUI = y.RXCUI }).Distinct().OrderBy(s => s.Text);
                    listBrands.DataTextField = "Text";
                    listBrands.DataValueField = "RXCUI";
                    listBrands.DataBind();

                    listGenerics.DataSource = genericList.Select(y => new { Text = y.STR, RXCUI = y.RXCUI }).Distinct().OrderBy(s => s.Text);
                    listGenerics.DataTextField = "Text";
                    listGenerics.DataValueField = "RXCUI";
                    listGenerics.DataBind();



                    if (listBrands.Items.Count > 0 || listGenerics.Items.Count > 0)
                    {
                        litSearchString.Text = autoSuggestSearch.Text;
                        multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationFound;
                    }
                    else
                    {
                        litNotFoundMedcationName.Text = autoSuggestSearch.Text.Trim();
                        multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationNotFound;
                    }
                }
                else
                {
                    litNotFoundMedcationName.Text = autoSuggestSearch.Text.Trim();
                    multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationNotFound;
                }
            }
            else
            {
                litNotFoundMedcationName.Text = autoSuggestSearch.Text.Trim();
                string[] suggesstions = RxNorm.RxUtility.GetSpellingSuggestions(strRxText);
                listSuggesstions.DataSource = suggesstions;
                listSuggesstions.DataBind();
                listSuggesstions.Visible = true;

                if (suggesstions != null && suggesstions.Length > 0)
                {
                    DivSuggestions.Visible = true;
                }
                else
                {
                    DivSuggestions.Visible = false;
                }

                multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationNotFound;
            }
        }

        protected void OnChooseSuggesstion(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(listSuggesstions.SelectedValue))
            {
                autoSuggestSearch.Text = listSuggesstions.SelectedValue;
                _DoSearch();
            }

            if ((listBrands.Items.Count > 0) || (listGenerics.Items.Count > 0))
            {
                litSearchString.Text = autoSuggestSearch.Text;
                multiViewMedicationForm.ActiveViewIndex = 2;
            }
            else
            {
                litNotFoundMedcationName.Text = autoSuggestSearch.Text.Trim();
                multiViewMedicationForm.ActiveViewIndex = 3;
            }
        }

        protected void lnkSuggestionCancel_Click(object sender, EventArgs e)
        {
            multiViewMedicationForm.ActiveViewIndex = 0;
        }

        private void _ClearAllFields()
        {
            autoSuggestSearch.Text = string.Empty;
            listBrands.Items.Clear();
            listGenerics.Items.Clear();
            litMedicationName.Text = "";
            _BindStrengthWithDefault();
            ddlDosage.SelectedValue = string.Empty ;
            txtDosage.Text = string.Empty;
            txtStrength.Text = string.Empty;
            ddlFrequency.SelectedValue = string.Empty;
            txtFrequency.Text = string.Empty;
            ckbIsDiscontinued.Checked = false;
            litMedicationName.Text = string.Empty;
            trMedicationName.Visible = false;
            teSideEffects.Value = string.Empty;

            calDatePicker.SelectedDate = DateTime.Now;
            txtDate.Text = DateTime.Now.ToString("d");
            calexDateDiscontinued.SelectedDate = DateTime.Now;
            tbDateDiscontinued.Text = DateTime.Now.ToString("d");

        }

        protected void btnMedFoundAddCancel_Click(object sender, EventArgs e)
        {
            _ClearAllFields();
            multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewSearch;
        }

        protected void OnGenericSelected(object sender, EventArgs e)
        {
            if (listBrands.Items.Count > 0 && listBrands.SelectedItem != null)
            {
                listBrands.SelectedItem.Selected = false;
            }
            if (!String.IsNullOrEmpty(listGenerics.SelectedValue))
            {
                _LoadUserSelections(listGenerics.SelectedValue, true);
            }
        }

        protected void OnBrandSelected(object sender, EventArgs e)
        {
            if (listGenerics.Items.Count > 0 && listGenerics.SelectedItem != null)
            {
                listGenerics.SelectedItem.Selected = false;
            }
            if (!String.IsNullOrEmpty(listBrands.SelectedValue))
            {
                _LoadUserSelections(listBrands.SelectedValue, false);
            }
            lnkFoundContinue.Visible = true;
        }

        protected void OnDiscontinuedCheckChanged(object sender, EventArgs e)
        {
            // This is now handled in OnPreRender
            //divDiscontinued.Visible = ckbIsDiscontinued.Checked;
        }

        private void _LoadUserSelections(string strRxCui, bool bIsGeneric)
        {
            DBManagerService db = new DBManagerService();
            var rxConcept = db.getRxConceptProperties(strRxCui);
            litMedicationName.Text = rxConcept.STR;
            trMedicationName.Visible = true;
            List<ListItem> strStrength = new List<ListItem>();
            if (bIsGeneric)
            {
                //If the user chooses a generic (single or multi ingredient based)
                var res = db.getRelatedByType(strRxCui, new[] { "SCD" });
                var associatedClinicalDrugs = res.Where(x => x.type == "SCD").Single().rxConcept.Select(y => new { Text = y.STR, RXCUI = y.RXCUI });
                if (associatedClinicalDrugs.Count() > 0)
                {
                    associatedClinicalDrugs.Select(x => _extractStrengthText(x.Text)).OrderBy(s => s).ToList<String>().ForEach(delegate(String obj)
                    {
                        strStrength.Add(new ListItem { Text = obj, Value = obj });
                    });
                }
            }
            else
            {
                //If the user chose a brand, 
                var res = db.getRelatedByType(strRxCui, new[] { "SBDC" });
                var associatedClinicalDrugs = res.Where(x => x.type == "SBDC").Single().rxConcept.Select(y => new { Text = y.STR, RXCUI = y.RXCUI });
                associatedClinicalDrugs.Select(x => _extractStrengthText(x.Text)).OrderBy(s => s).ToList<String>().ForEach(delegate(String obj)
                {
                    strStrength.Add(new ListItem { Text = obj, Value = obj });
                });
            }

            if (strStrength.Count == 0)
            {
                _BindStrengthWithDefault();
            }
            else
            {
                strStrength.Insert(0, new ListItem(GetGlobalResourceObject("Tracker", "Popup_Medication_Select_Strength").ToString(), ""));

                ddlStrength.DataSource = strStrength;
                ddlStrength.DataTextField = "Text";
                ddlStrength.DataValueField = "Value";
                ddlStrength.DataBind();
                //ddlStrength.SelectedValue = "";
                //ddlStrength.Text = "Select Strength";
                ddlStrength.SelectedValue = string.Empty;
            }
        }

        private string _extractStrengthText(string str)
        {
            //Parse the SCD Text and get the actual strength (The current units are defined
            //at http://www.nlm.nih.gov/research/umls/rxnorm/overview.html

            StringBuilder sb = new StringBuilder();
            string regExpression = @"\d+\.?\d*\s*(MG/ACTUAT|MEQ/MG|MEQ/ML|MG/MG|MG/ML|ML/ML|PNU/ML|UNT/MG|UNT/MLMG|MG|MEQ|ML|UNT|%|ACTUAT)";
            Regex regex = new Regex(regExpression);
            bool bFirst = true;
            foreach (Match match in regex.Matches(str))
            {
                if (!bFirst)
                    sb.Append(" - ");
                sb.Append(match.Value);
                bFirst = false;
            }
            return sb.ToString();
        }

        private void _BindStrengthWithDefault()
        {
            ListItemCollection list = new ListItemCollection();
            list.Add(new ListItem(GetGlobalResourceObject("Tracker", "Popup_Medication_SelectStrength").ToString(), ""));

            ddlStrength.DataSource = list;
//            ddlStrength.ForceLoad = true;
            ddlStrength.DataTextField = "Text";
            ddlStrength.DataValueField = "Value";
            ddlStrength.DataBind();

            ddlStrength.SelectedValue = string.Empty;
        }

        protected void lnkFoundContinue_Click(object sender, EventArgs e)
        {
            Page.Validate("MedFoundContinue");
            if (!Page.IsValid)
                return;

            litFoundAddMeddicationName.Text = litMedicationName.Text;
            hddnMedicationFound.Value = "1";
            _SetUIForADD(true);
            if (ddlStrength.Items.Count <= 1)
            {
                ddlStrength.Enabled = false;
            }

            multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationAdd;
        }

        protected void lnkFoundCancel_Click(object sender, EventArgs e)
        {
            _ClearFields();
            multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewSearch;
        }

        protected void lnkNotFoundCancel_Click(object sender, EventArgs e)
        {
            _ClearFields();
            multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewSearch;
        }

        private void _SetUIForADD(bool bMedicationFound)
        {
            ddlStrength.Visible = bMedicationFound;
            ddlDosage.Visible = bMedicationFound;
            ddlFrequency.Visible = bMedicationFound;

            txtStrength.Visible = !bMedicationFound;
            txtDosage.Visible = !bMedicationFound;
            txtFrequency.Visible = !bMedicationFound;
        }

        protected void lnkNotFoundContinue_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(listSuggesstions.SelectedValue))
            {
                autoSuggestSearch.Text = listSuggesstions.SelectedValue;
                _DoSearch();
            }

            if ((listBrands.Items.Count > 0) || (listGenerics.Items.Count > 0))
            {
                litSearchString.Text = autoSuggestSearch.Text;
                multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationFound;
            }
            else
            {
                litNotFoundMedcationName.Text = autoSuggestSearch.Text.Trim();
                litFoundAddMeddicationName.Text = litNotFoundMedcationName.Text;

                hddnMedicationFound.Value = "0";
                _SetUIForADD(false);
                multiViewMedicationForm.ActiveViewIndex = (int)MedicationSteps.viewMedicationAdd;
            }

        }

        private void _UpdateMedication()
        {
            Guid medguid = new Guid(mEditKeys);
            HVManager.MedicationDataManager.MedicationItem item = HVHelper.HVManagerPatientBase.MedicationDataManager.GetDataWithThingId(medguid);
            DateTime? dtDiscontinued = null;
            if (ddlStrength.Visible)
            {
                if (AHAAPI.H360Utility.IsUseNewValue(item.Strength.Value, ddlStrength.SelectedValue.Trim()))
                    item.Strength.Value = ddlStrength.SelectedValue.Trim();
            }

            if (AHAAPI.H360Utility.IsUseNewValue(item.Strength.Value, txtStrength.Text.Trim()))
                item.Strength.Value = txtStrength.Text.Trim();

            if (AHAAPI.H360Utility.IsUseNewValue(item.Dosage.Value, txtDosage.Text.Trim()))
                item.Dosage.Value = txtDosage.Text.Trim();

            if (AHAAPI.H360Utility.IsUseNewValue(item.Frequency.Value, txtFrequency.Text.Trim()))
                item.Frequency.Value = txtFrequency.Text.Trim();

            if (AHAAPI.H360Utility.IsUseNewValue(item.Note.Value, teSideEffects.Value.Trim()))
                item.Note.Value = teSideEffects.Value.Trim();

            //if (AHAAPI.H360Utility.IsUseNewValue(item.Frequency.Value, ddlFrequency.SelectedValue.Trim()))
            //    item.Frequency.Value = ddlFrequency.SelectedValue.Trim();

            if (ckbIsDiscontinued.Checked)
            {
                dtDiscontinued = calexDateDiscontinued.SelectedDate.Value;
                item.DateDiscontinued.Value = new HVManager.HVApproximateDateTime(calexDateDiscontinued.SelectedDate.Value);
            }
            else
            {
                if (item.DateDiscontinued.Value != null && item.DateDiscontinued.Value.HasValue())
                {
                    dtDiscontinued = null;
                    item.DateDiscontinued.Value = null;
                }
            }

            if (calDatePicker.SelectedDate.HasValue)
            {
                item.StartDate.Value = new HVManager.HVApproximateDateTime(calDatePicker.SelectedDate.Value);
            }
            else
            {
                item.StartDate.Value = null;
            }

            HVManager.MedicationDataManager.MedicationItem itemUpdated = HVHelper.HVManagerPatientBase.MedicationDataManager.UpdateItem(item);
            //DB Update
            //Update only if user has consented
            if (AHAAPI.SessionInfo.GetUserIdentityContext().HasUserAgreedToNewTermsAndConditions)
            {
                AHAHelpContent.Medication.CreateOrChangeMedication(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                    itemUpdated.ThingKey.Id, itemUpdated.ThingKey.VersionStamp,
                    itemUpdated.Name.Value, itemUpdated.MedicationType.Value,
                    itemUpdated.Dosage.Value, itemUpdated.Strength.Value, itemUpdated.Frequency.Value, calDatePicker.SelectedDate,
                    dtDiscontinued, itemUpdated.Note.Value);
            }
        }

        private bool isDateStartedValid = true;

        protected void ValidateDateStarted(object sender, ServerValidateEventArgs v)
        {
            // date needs to be earlier or equal to tomorrow
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string sdate = txtDate.Text.Trim();
            if (String.IsNullOrEmpty(sdate))     // first make sure there is a date specified
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Date").ToString();
            }
            else
            {
                DateTime range = DateTime.Now.Date.AddDays(1);
                DateTimeValidation.ValidateDateBeforeDate(sdate, range, valid, v);
            }

/****
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            GRCDatePicker ControlToValidate = null;
            if (valid.ValidationGroup == _vgAdd)
            {
                ControlToValidate = calDatePicker;
            }
            ////else
            ////{
            ////    ControlToValidate = dtEditStarted;
            ////}

            DateTime? dtUserDate = UIHelper.GetUserDate();
            if (ControlToValidate.Date.HasValue)
            {
                DateTime dtCompare = (dtUserDate.HasValue) ? dtUserDate.Value.Date : DateTime.Now.Date.AddDays(1);

                if (ControlToValidate.Date.Value.Date > dtCompare )
                {
                    isDateStartedValid = false;
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Popup_Medication_ValidationFutureStartDate").ToString();
                    return;
                }
            }
 **/
        }

        protected void ValidateDateDiscontinued(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (divDiscontinued.Visible)
            {
                string sdate = tbDateDiscontinued.Text.Trim();

                // Check to see if the value is valid
                if (String.IsNullOrEmpty(sdate))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Date").ToString();
                }
                else
                {
                    string startString = txtDate.Text.Trim();
                    DateTime startDate;

                    if (!String.IsNullOrEmpty(startString) && DateTime.TryParse(startString, out startDate))
                    {
                        DateTimeValidation.ValidateDateAfterDate(sdate, startDate, valid, v);
                    }
                    else
                    {
                        DateTime range = DateTime.Now.Date.AddDays(1);
                        DateTimeValidation.ValidateDateBeforeDate(sdate, range, valid, v);
                    }
                }
            }

/** PJB
            GRCDatePicker ControlToValidate = null;
            GRCDatePicker StartDate = null;
            GRCCheckBox chkboxctrl = null;
            HtmlGenericControl ContainerDiv = null;

            //if (valid.ValidationGroup == _vgAdd)
            //{
            ControlToValidate = dtDateDiscontinued;
            chkboxctrl = chkEnableDate;
            ContainerDiv = divDiscontinued;
            StartDate = dtDateStarted;
            //}
            //else
            //{
            //    ControlToValidate = dtEditDateDiscontinued;
            //    chkboxctrl = chkEditEnableDate;
            //    ContainerDiv = divEditDiscontinued;
            //    StartDate = dtEditStarted;
            //}


            if (chkboxctrl.Checked)
            {
                if (!ControlToValidate.Date.HasValue)
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Popup_Medication_SpecifyDateDiscontinued").ToString();
                    ContainerDiv.Attributes.Add("style", "display:block");
                    return;
                }

                DateTime? dtUserDate = UIHelper.GetUserDate();
                DateTime dtCompare = (dtUserDate.HasValue) ? dtUserDate.Value.Date : DateTime.Now.Date.AddDays(1);

                if (ControlToValidate.Date.Value.Date > dtCompare )
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Popup_Medication_DiscontinuedFutureDate").ToString();
                    ContainerDiv.Attributes.Add("style", "display:block");
                    return;
                }

                

                if (StartDate.Date.HasValue && isDateStartedValid && (ControlToValidate.Date.Value < StartDate.Date.Value))
                {
                    v.IsValid = false;
                    valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Popup_Medication_DiscontinuedLessThanStartDate").ToString(); ;
                    ContainerDiv.Attributes.Add("style", "display:block");
                    return;
                }
            }
**/
        }

        protected void ValidateMedicationName(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (litMedicationName.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Popup_Medication_NotSelected").ToString();
                return;
            }
        }
    }
}