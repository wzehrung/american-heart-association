﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MedicationPrinterFriendly.ascx.cs" Inherits="Heart360Web.Controls.Patient.Medications.MedicationPrinterFriendly" %>

<h3><asp:Literal ID="litTitle" runat="server"/></h3>
<div class="full history-table-wrap" >
    <asp:Repeater ID="rptHistory" OnItemDataBound="rptHistory_ItemDataBound" EnableViewState="false" runat="server">
        <HeaderTemplate>
    	         <table>
		            <thead>
			            <tr>
				            <th width="35%"><%=GetGlobalResourceObject("Reports", "Text_Name")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_Strength")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_Dosage")%></th>
				            <th width="10%"><%=GetGlobalResourceObject("Reports", "Text_Frequency")%></th>
				            <th width="35%"><%=GetGlobalResourceObject("Reports", "Text_SideEffects")%></th>
			            </tr>
		            </thead>
                    <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
			    <td><asp:Literal ID="litName" runat="server"/></td>
			    <td><asp:Literal ID="litStrength" runat="server"/></td>
			    <td><asp:Literal ID="litDosage" runat="server"/></td>
			    <td><asp:Literal ID="litFreq" runat="server"/></td>
			    <td><asp:Literal ID="litSideEffects" runat="server"/></td>
		    </tr>
        </ItemTemplate>
        <FooterTemplate>
                    </tbody>
	            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
