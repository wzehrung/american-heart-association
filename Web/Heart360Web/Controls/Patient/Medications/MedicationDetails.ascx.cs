﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using Heart360Web.Patient;

namespace Heart360Web.Controls.Patient.Medications
{
    public partial class MedicationDetails : System.Web.UI.UserControl
    {
        public delegate void RefreshDataCompletedCallback();

        private Heart360Web.PatientPortal.MODULE_ID           mModuleID;
        private MedicationsModuleController             mModuleController;
        private RefreshDataCompletedCallback            mRefreshDataCallback = null;


        public MedicationDetails() : base()
        {
            mModuleID = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS;
            mModuleController = null;
        }

        public Heart360Web.PatientPortal.MODULE_ID ModuleID
        {
            get { return mModuleID; }
        }

        public int ModuleIdAsInt
        {
            get { return (int)mModuleID; }
        }

        public MedicationsModuleController ModuleController
        {
            get { return mModuleController; }
            set { mModuleController = value; }
        }

        public UpdatePanel MyCurrentMedsUpdatePanel
        {
            get { return this.updatePanelCurrMeds; }
        }

        public UpdatePanel MyDiscontinnuedMedsUpdatePanel
        {
            get { return this.updatePanelDisconMeds; }
        }

        public RefreshDataCompletedCallback RefreshDataCallback
        {
            set { mRefreshDataCallback = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            litTitleLearnMore.Text = GetGlobalResourceObject("Tracker", "Tracker_Details_Title_LearnMore").ToString();

            if (mModuleController == null)
                mModuleController = new MedicationsModuleController();

            if (mModuleController != null)
                mModuleController.InitializeDetail(this);

            string ocJavascript = "window.open( 'PrinterFriendly.aspx?modid=" + ModuleIdAsInt.ToString() + "&daterange=" + 
                                   AHACommon.DataDateReturnType.AllData.ToString() + "&currentmeds=1', 'scrollbars=yes,status=no,toolbar=no,menubar=no,location=no'); return false ;";
            btnPrintCurrReport.Attributes.Add("onclick", ocJavascript);

            ocJavascript = "window.open( 'PrinterFriendly.aspx?modid=" + ModuleIdAsInt.ToString() + "&daterange=" +
                                   AHACommon.DataDateReturnType.AllData.ToString() + "&currentmeds=0', 'scrollbars=yes,status=no,toolbar=no,menubar=no,location=no'); return false ;";
            btnPrintDisconReport.Attributes.Add("onclick", ocJavascript);

        }

            //
            // First section from the top: Actions I can take
            //
        public void SetActionsContents(List<AHAHelpContent.MLCActionICanTake> actionList)
        {
            if (actionList != null && actionList.Count > 0)
            {
                rptActions.DataSource = actionList;
                rptActions.DataBind();
            }
        }

        protected void rptActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AHAHelpContent.MLCActionICanTake objActionICanTake = e.Item.DataItem as AHAHelpContent.MLCActionICanTake;
                Literal litActionText = e.Item.FindControl("litActionText") as Literal;
                litActionText.Text = objActionICanTake.ActionText;
            }
        }

            //
            // Second section from the top: My History
            //

        public void AddHistoryUserControl( int _modID ) // Heart360Web.PatientPortal.MODULE_ID _modID)
        {

                // Load the current medications table
            Heart360Web.Controls.Patient.Medications.MedicationItemList mil = (Heart360Web.Controls.Patient.Medications.MedicationItemList)LoadControl("~/Controls/Patient/Medications/MedicationItemList.ascx");

            mil.LoadItems( true );      // true for current meds           
            phCurrentMedsTable.Controls.Add(mil);

                // Load the discontinued meds
            mil = (Heart360Web.Controls.Patient.Medications.MedicationItemList)LoadControl("~/Controls/Patient/Medications/MedicationItemList.ascx");

            mil.LoadItems( false );      // false for discontinued meds           
            phDiscontinuedMedsTable.Controls.Add(mil);

        }

            //
            // Fourth section from the top: Learn More
            //
        public void SetLearnMoreContents( List<AHAHelpContent.Content> contentList)
        {
            liLearnMore1.Visible = false;
            liLearnMore2.Visible = false;
            liLearnMore3.Visible = false;
            liLearnMore4.Visible = false;
            liLearnMore5.Visible = false;


            // Up to five links
            int nIndex = 0;
            foreach (AHAHelpContent.Content objContent in contentList )
            {
                switch (nIndex)
                {
                    case 0:
                        lnkLearnMore1.HRef = contentList[0].URL;
                        lnkLearnMore1.InnerText = contentList[0].DidYouKnowLinkTitle;
                        liLearnMore1.Visible = true;
                        break;
                    case 1:
                        lnkLearnMore2.HRef = contentList[1].URL;
                        lnkLearnMore2.InnerText = contentList[1].DidYouKnowLinkTitle;
                        liLearnMore2.Visible = true;
                        break;
                    case 2:
                        lnkLearnMore3.HRef = contentList[2].URL;
                        lnkLearnMore3.InnerText = contentList[2].DidYouKnowLinkTitle;
                        liLearnMore3.Visible = true;
                        break;
                    case 3:
                        lnkLearnMore4.HRef = contentList[3].URL;
                        lnkLearnMore4.InnerText = contentList[3].DidYouKnowLinkTitle;
                        liLearnMore4.Visible = true;
                        break;
                    case 4:
                        lnkLearnMore5.HRef = contentList[4].URL;
                        lnkLearnMore5.InnerText = contentList[4].DidYouKnowLinkTitle;
                        liLearnMore5.Visible = true;
                        break;

                    default:
                        break;
                }
                nIndex++;
            }
        }

        protected void OnRefreshDataA_Click(object sender, EventArgs e)
        {
            HVHelper.HVManagerPatientBase.FlushCache();

            this.updatePanelDisconMeds.Update();
            if (mRefreshDataCallback != null)
                mRefreshDataCallback();
            //            Response.Redirect(Request.RawUrl);
        }

        protected void OnRefreshDataB_Click(object sender, EventArgs e)
        {
            HVHelper.HVManagerPatientBase.FlushCache();

            this.updatePanelCurrMeds.Update();
            if (mRefreshDataCallback != null)
                mRefreshDataCallback();
            //            Response.Redirect(Request.RawUrl);
        }

        protected void OnPDFCurrent_Click(object sender, EventArgs e)
        {
            PDFHelper.CreateReport(PatientGuids.CurrentPatientGuids, PDFType.CurrentMedication, AHACommon.DataDateReturnType.AllData, DataViewType.Table);
            
        }

        protected void OnPDFDiscontinued_Click(object sender, EventArgs e)
        {
            PDFHelper.CreateReport(PatientGuids.CurrentPatientGuids, PDFType.DiscontinuedMedication, AHACommon.DataDateReturnType.AllData, DataViewType.Table);
        }
    }
}