﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;
using HVManager;
using AHACommon;

namespace Heart360Web.Controls.Patient.Medications
{
    public partial class MedicationItemList : System.Web.UI.UserControl
    {
        private bool mIsCurrentMeds = true;

        public bool IsCurrentMeds
        {
            get { return mIsCurrentMeds; }
            set { mIsCurrentMeds = value; }
        }

        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "date";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return false;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            LinkedList<HVManager.MedicationDataManager.MedicationItem>  ascendingList = HVHelper.HVManagerPatientBase.MedicationDataManager.GetDataBetweenDates( DataDateReturnType.AllData );

            List<HVManager.MedicationDataManager.MedicationItem>        descendingList = new List<HVManager.MedicationDataManager.MedicationItem>();

            foreach (HVManager.MedicationDataManager.MedicationItem medicationItem in ascendingList)
            {
                DateTime? dtDiscontinued = null;

                if (medicationItem.HasDiscontinued
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate != null
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.HasValue)
                {
                    dtDiscontinued = new DateTime(medicationItem.DateDiscontinued.Value.HVApproximateDate.Year, medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.Value, medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.Value);
                }

                if (IsCurrentMeds
                    && (!medicationItem.HasDiscontinued
                    || (dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() > DateTime.Now.ToUniversalTime())))
                    descendingList.Add(medicationItem);
                else if (!IsCurrentMeds
                    && ((medicationItem.HasDiscontinued && !dtDiscontinued.HasValue)
                    || (medicationItem.HasDiscontinued && dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() <= DateTime.Now.ToUniversalTime())))
                    descendingList.Add(medicationItem);
            }


            rptHistory.DataSource = descendingList;
            rptHistory.DataBind();

            // NOTE: If there is no history, there will just be an empty table.
            /**
                        gvList.DataKeyNames = new string[] { "ThingID", "VersionStamp" };
                        gvList.DataSource = desendingList;
                        gvList.DataBind();

                        if (descendingList.Count == 0)
                        {
                            DivNoData.Visible = true;
                            DivData.Visible = false;
                        }
                        else
                        {
                            DivNoData.Visible = false;
                            DivData.Visible = true;
                        }
             **/
        }

        protected void rptHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MedicationDataManager.MedicationItem item = e.Item.DataItem as MedicationDataManager.MedicationItem;

                Literal litName = e.Item.FindControl("litName") as Literal;
                Literal litStrength = e.Item.FindControl("litStrength") as Literal;
                Literal litDosage = e.Item.FindControl("litDosage") as Literal;
                Literal litFrequency = e.Item.FindControl("litFrequency") as Literal;
                Literal litSideEffects = e.Item.FindControl("litSideEffects") as Literal;

                litName.Text = item.Name.Value;
                litStrength.Text = item.Strength.Value;
                litDosage.Text = item.Dosage.Value;
                litFrequency.Text = item.Frequency.Value;

                if (!string.IsNullOrEmpty(item.Note.Value))
                {
                    if (item.Note.Value.Length > 18)
                    {
                        // comment is truncated and entire comment is put in a tooltip
                        litSideEffects.Text = "<span data-tooltip=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + item.Note.Value + "\">" +
                                            GRCBase.StringHelper.GetShortString(item.Note.Value, 18, true) + "</span>";
                    }
                    else
                    {
                        // entire comment fits in cell.
                        litSideEffects.Text = item.Note.Value;
                    }
                }

                //
                // Hook up Actions
                //
                ImageButton actEdit = e.Item.FindControl("actEditor") as ImageButton;
                ImageButton actTrash = e.Item.FindControl("actTrash") as ImageButton;
                string      itemKey = item.ThingID.ToString();

                if (actEdit != null)
                {
                    string editUrl = "Popup.aspx?reading=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS).ToString() + "&elid=" + itemKey;
                    actEdit.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(editUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS);
                }
                if (IsCurrentMeds)
                {
                    actTrash.Visible = false;
                }
                else
                {
                    actTrash.Visible = true;
                    // delete requires both ThingID and VersionStamp
                    itemKey += "," + item.VersionStamp.ToString();
                    string trashUrl = "PopUp.aspx?delete=1&closesummary=0&modid=" + ((int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS).ToString() + "&elid=" + itemKey;
                    actTrash.OnClientClick = ((Heart360Web.MasterPages.Patient)this.Page.Master).GetTrackerPopupOnClientClickContent(trashUrl, Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS);
                }
            }
        }


        public void LoadItems( Boolean bCurrentMeds )
        {
            SortAscending = false;
            SortExpression = string.Empty;

            IsCurrentMeds = bCurrentMeds;

            //_bindDataToGridView();
        }
    }
}