﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MedicationAddMed.ascx.cs" Inherits="Heart360Web.Controls.Patient.Medications.MedicationAddMed" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<div class="full" >
    
    <!-- ## BEGIN Add Reading Title ## -->
	<div id="popup-title" class="full">
		<h3><asp:Literal runat="server" ID="litTitle"></asp:Literal></h3>
	</div>
    <!-- ## END Add Reading Title ## -->

    <div class="large-12 small-12 columns spb" >
        <asp:ValidationSummary 
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            ValidationGroup="vgAdd" 
            Enabled="true" 
            ID="vldsummaryadd" 
            runat="server" 
            DisplayMode="BulletList"
            CssClass="validation-error"
            ShowSummary="true"  />
        <asp:ValidationSummary 
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            ValidationGroup="Search" 
            Enabled="true" 
            CssClass="validation-error"
            ID="vldsummarysearch" 
            runat="server"
            DisplayMode="BulletList" 
            ShowSummary="true" 
            ShowMessageBox="true" 
            ForeColor="#CE2930" />
        <asp:ValidationSummary 
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            ValidationGroup="MedFoundContinue" 
            Enabled="true" 
            ID="ValidationSummary1" 
            runat="server"
            DisplayMode="BulletList" 
            ShowSummary="true" 
            ShowMessageBox="true" 
            ForeColor="#CE2930" />

        <asp:CustomValidator ID="custValidMedicationname" Display="None" ValidationGroup="MedFoundContinue"
            runat="server" EnableClientScript="false" OnServerValidate="ValidateMedicationName"></asp:CustomValidator>
    </div>

    <asp:MultiView ID="multiViewMedicationForm" ActiveViewIndex="0" runat="server">

        <asp:View ID="viewSearch" runat="server" >

            <div class="row">
	            <div class="large-12 small-12 columns">
		            <label class="text-left" for="med-info"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_SearchGuidingText")%></label>
	            </div>
	            <div class="large-12 small-12 columns">
                    <GRC:GRCComboBox ID="autoSuggestSearch" runat="server" CssClass="dropdowns"
                                    CBReadOnly="false" IsAutoSuggest="true" FilterResultsOnClient="false"
                                    CBTextBoxCssClass="text-left" 
                                    ClientSuggestionsFunctionName="Heart360Web.Services.AHAJSUtility.GetRxSuggesstions" />
                                <asp:CustomValidator ID="CustomValidator1" Display="None" ValidationGroup="Search"
                                    runat="server" EnableClientScript="false" OnServerValidate="ValidateSearchString"></asp:CustomValidator>
	            </div>	
            </div>

            <div class="save row spt">
                    <div class="large-12 small-12 columns">
                        <asp:Button  runat="server" ID="lnkSearch" CssClass="gray-button full" OnClick="OnSearchClicked" Text="Find Medication"></asp:Button>
                        <asp:Button runat="server" ID="btnSearch" OnClick="OnSearchClicked" Visible="false" />
                    </div>
            </div>

        </asp:View>
        
        <asp:View ID="viewMedicationFound" runat="server">
            <div class="row">
                <div class="large-12 small-12 columns">
		            <label class="text-left" for="med-info"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_SearchGuidingText")%></label>
	            </div>
                <div class="large-12 small-12 columns">
                    <table cellpadding="0" cellspacing="0">
                        <tr id="trSearchString" runat="server">
                            <th>
                                <%=GetGlobalResourceObject("Tracker", "Popup_Medication_SearchString")%>:
                            </th>
                            <td>
                                <asp:Literal runat="server" ID="litSearchString"></asp:Literal>
                            </td>
                        </tr>
                        <tr id="trMedicationName" valign="top" visible="false" runat="server">
                            <th>
                                <%=GetGlobalResourceObject("Tracker", "Popup_Medication_MedicationName")%>:
                            </th>
                            <td>
                                <asp:Literal runat="server" ID="litMedicationName"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row" >
                <div class="large-12 small-12 columns">
		            <label class="text-left" for="med-info"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_MedFoundGuidingText")%></label>
	            </div>
            </div>
            <div class="row">
	            <div class="large-12 small-12 columns">
		            <label class="text-left"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_GenericProducts")%>:</label>
	            </div>
	            <div class="large-12 small-12 columns">
                    <asp:ListBox AutoPostBack="true" OnSelectedIndexChanged="OnGenericSelected" EnableViewState="true"
                            Rows="5" runat="server" ID="listGenerics"></asp:ListBox>
	            </div>
            </div>

            <div class="row">
	            <div class="large-12 small-12 columns">
		            <label class="text-left"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_BrandedProducts")%>:</label>
	            </div>
	            <div class="large-12 small-12 columns">
                    <asp:ListBox AutoPostBack="true" OnSelectedIndexChanged="OnBrandSelected" EnableViewState="true"
                            Width="100%" Rows="5" runat="server" ID="listBrands"></asp:ListBox>
                </div>
            </div>

            <div class="save row spt">
                <div class="large-6 small-12 columns">
                    <asp:Button ID="lnkFoundCancel" CssClass="gray-button full" OnClick="lnkFoundCancel_Click" Text="<%$Resources:Common,Text_Cancel%>" runat="server" />
                </div>
                <div class="large-6 small-12 columns">
                    <asp:Button ID="lnkFoundContinue" CssClass="gray-button full" OnClick="lnkFoundContinue_Click" Text="<%$Resources:Common,Text_Continue%>" runat="server"/>
                </div>
            </div>
        </asp:View>

        <asp:View ID="viewMedicationNotFound" runat="server">
            <div class="full">
                <div class="large-12 small-12 columns">
                    <span style="color: Red"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_MedNotFoundText")%>:</span>
                </div>
                <div class="large-12 small-12 columns">
                    <p><b><asp:Literal runat="server" ID="litNotFoundMedcationName"></asp:Literal></b></p>
                </div>
                <div class="large-12 small-12 columns">
                    <p><%=GetGlobalResourceObject("Tracker", "Popup_Medication_SupplementsNotInList")%>
                       <%=GetGlobalResourceObject("Tracker", "Popup_Medication_MedNotFoundGuidingText")%></p>
                </div>
            </div>
            <div class="whitebox" id="DivSuggestions" runat="server">
                <div class="shadowtop">
                    <div class="tl">
                    </div>
                    <div class="tm">
                    </div>
                    <div class="tr">
                    </div>
                </div>
                <table class="tablecontainer" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="left-shadow">
                        </td>
                        <td class="boxcontent center">
                            <div style="text-align: left;">
                                <%-- <%=GetGlobalResourceObject("Patient2", "Text_Medication_Suggestions")%>--%>
                                <%--If the medication you are looking for is found in the suggestion list below please
                                select it and click continue otherwise just click continue to proceed--%>
                                <%=GetGlobalResourceObject("Tracker", "Popup_Medication_SuggestionGuidingText")%>
                            </div>
                            <asp:ListBox EnableViewState="true" Width="100%" Rows="5" runat="server" ID="listSuggesstions">
                            </asp:ListBox>
                        </td>
                        <td class="right-shadow">
                        </td>
                    </tr>
                </table>
                <div class="shadowbottom">
                    <div class="bl">
                    </div>
                    <div class="bm">
                    </div>
                    <div class="br">
                    </div>
                </div>
            </div>
            <div class="save row spt">
                <div class="large-6 small-12 columns">
                    <asp:Button ID="lnkNotFoundCancel" CssClass="gray-button full" OnClick="lnkNotFoundCancel_Click" Text="<%$Resources:Common,Text_Cancel%>" runat="server" />
                </div>
                <div class="large-6 small-12 columns">
                    <asp:Button ID="lnkNotFoundContinue" CssClass="gray-button full" OnClick="lnkNotFoundContinue_Click" Text="<%$Resources:Common,Text_Continue%>" runat="server"/>
                </div>
            </div>
        </asp:View>
        
        <asp:View ID="viewMedicationAdd" runat="server">
                <div class="row spb">
	                <div class="large-3 small-12 columns">
		                <label class="text-left"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_MedicationName")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <b><asp:Literal ID="litFoundAddMeddicationName" runat="server"/></b>
                    </div>
                </div>

                <div class="row">
                    <div class="large-3 small-12 columns">
		                <label class="text-left"><%=GetGlobalResourceObject("Common", "Text_StartDate")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:TextBox ID="txtDate" class="required" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="btnCalendarDate" CssClass="calendar-popup-button" runat="server" BorderStyle="None" style="border:none !important;" value="" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                        <asp:CalendarExtender ID="calDatePicker" TargetControlID="txtDate" PopupButtonID="btnCalendarDate" CssClass="popup_cal" runat="server">
                        </asp:CalendarExtender>
                        <asp:CustomValidator ID="vAddDate" runat="server" EnableClientScript="false" ValidationGroup="vgAdd" Display="None" OnServerValidate="ValidateDateStarted"></asp:CustomValidator>
                    </div>
                </div>

                <asp:FilteredTextBoxExtender 
                        ID="FilteredTextBoxExtenderA" 
                        runat="server" 
                        Enabled="true" 
                        TargetControlID="txtDate" 
                        FilterType="Numbers,Custom"
                        ValidChars="/" >
                </asp:FilteredTextBoxExtender>

                <div class="row">
                    <div class="large-3 small-12 columns">
		                <label class="text-left"><%=GetGlobalResourceObject("Common", "Text_Strength")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlStrength" runat="server" CssClass="dropdowns" Visible="false"></asp:DropDownList>
                        <asp:TextBox ID="txtStrength" runat="server" CssClass="required" />
                    </div>
                </div>
                
                <div class="row">
                    <div class="large-3 small-12 columns">
		                <label class="text-left"><%=GetGlobalResourceObject("Common", "Text_Dosage")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlDosage" runat="server" CssClass="dropdowns" Visible="false"></asp:DropDownList>
                        <asp:TextBox ID="txtDosage" runat="server" CssClass="required" />
                    </div>
                </div>

                <div class="row">
                    <div class="large-3 small-12 columns">
		                <label class="text-left"><%=GetGlobalResourceObject("Common", "Text_Frequency")%>:</label>
                    </div>
                    <div class="large-9 small-12 columns">
                        <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="dropdowns" Visible="false"></asp:DropDownList>
                        <asp:TextBox ID="txtFrequency" runat="server" CssClass="required" />
                    </div>
                </div>
                    
                <asp:UpdatePanel UpdateMode="Conditional" runat="server" >
                <ContentTemplate>
                    <div class="row">
                        <div class="large-12 small-12 columns">
                            <p><asp:CheckBox ID="ckbIsDiscontinued" runat="server" Text="<%$Resources:Tracker,Popup_Medication_Discontinued %>" CssClass="full cbox" AutoPostBack="true" OnCheckedChanged="OnDiscontinuedCheckChanged" /></p>
                        </div>
                        <div id="divDiscontinued" runat="server" >
                            <div class="large-3 small-12 columns">
                                <label ><%=GetGlobalResourceObject("Tracker", "Popup_Medication_DateDiscontinued")%>:</label>
                            </div>
                            <div class="large-9 small-12 columns">
                                <asp:TextBox ID="tbDateDiscontinued" CssClass="required" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="btnCalendarDateDiscontinued" CssClass="calendar-popup-button" runat="server" BorderStyle="None" style="border:none !important;" value="" ImageUrl="~/images/1x1.png" CausesValidation="false" />
                                <asp:CalendarExtender ID="calexDateDiscontinued" TargetControlID="tbDateDiscontinued" PopupButtonID="btnCalendarDateDiscontinued" CssClass="popup_cal" runat="server">
                                </asp:CalendarExtender>
                                <asp:CustomValidator ID="cvAddDiscontinuedDate" runat="server" EnableClientScript="false" ValidationGroup="vgAdd" Display="None" OnServerValidate="ValidateDateDiscontinued"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>

                    <asp:FilteredTextBoxExtender 
                        ID="FilteredTextBoxExtenderB" 
                        runat="server" 
                        Enabled="true" 
                        TargetControlID="tbDateDiscontinued" 
                        FilterType="Numbers,Custom"
                        ValidChars="/" >
                    </asp:FilteredTextBoxExtender>
                </ContentTemplate>
                </asp:UpdatePanel>
                    
                <div class="row">
                    <div class="large-12 small-12 columns">
                        <p><label class="text-left"><%=GetGlobalResourceObject("Tracker", "Popup_Medication_SideEffectsNotes")%>:</label>
                        <textarea id="teSideEffects" rows="4" class="textarea" name="comments" runat="server"></textarea></p>
                    </div>
                </div>
                
                
                <div>
                    <div class="save row spt" >
                        <div class="large-6 small-12 columns">
                            <asp:Button ID="btnMedFoundAddCancel" CssClass="gray-button full" OnClick="btnMedFoundAddCancel_Click" runat="server" Text="<%$Resources:Common,Text_Cancel %>" />
                        </div>
                        <div class="large-6 small-12 columns">
                            <asp:Button ID="btnMedFoundAddMedication" CssClass="gray-button full" OnClick="btnMedFoundAddMedication_Click" runat="server" Text="<%$Resources:Common,Text_Save%>" />
                        </div>
                    </div>
                    <div class="save row">
                        <div class="large-12 small-12 columns">
                            <p><asp:Button ID="btnMedSaveAndAddAnother" CssClass="gray-button full" OnClick="btnMedSaveAndAddAnother_Click" Text="<%$Resources:Common,Text_SaveAndAddAnother%>" runat="server" /></p>
                        </div>
                    </div>
                </div>
        </asp:View>
    
    </asp:MultiView>

    <asp:HiddenField ID="hddnMedicationFound" runat="server" />

</div>

<script type="text/javascript">
    function EnableDateClick(chkbox, containerdiv) {
        if (chkbox.checked)
            containerdiv.style.display = "block";
        else
            containerdiv.style.display = "none";
    }    
</script>