﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using GRCBase;
using AHAAPI;
using System.Text.RegularExpressions;

namespace Heart360Web
{
    public partial class Error : H360PageHelper
    {
        protected override bool LogOnRequired
        {
            get
            {
                return false;
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (SessionInfo.IsUserLoggedIn)
            {
                this.MasterPageFile = "~/MasterPages/Patient.Master";
            }
            else if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
            {
                this.MasterPageFile = "~/MasterPages/Provider.Master";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string strLitLinkText = GetGlobalResourceObject("Common", "Error_Message_Link").ToString();
            strLitLinkText = Regex.Replace(strLitLinkText, "##requestsource##", Request["Source"]);
            
            litLinkText.Text = strLitLinkText;

            string strLitErrorText = GetGlobalResourceObject("Common", "Error_Message_Persists").ToString();
            strLitErrorText = Regex.Replace(strLitErrorText, "##supportmail##", AHACommon.AHAAppSettings.SupportEmail);

            litError.Text = strLitErrorText;

            System.Web.HttpContext.Current.Response.StatusCode = 500;

            string errorID = Request["ID"];
            if (Request["NOTENCRYPTED"] != null)
            {
                errorID = Request["ID"];
            }
            else
            {
                errorID = GRCBase.BlowFish.DecryptString(Request["ID"]);
            }

            litErrorID2.Text = errorID;

            LitTechnicalDetails.Text = ErrorLogHelper.GetErrorMessageFromDatabase(Convert.ToInt32(errorID));
            LitTechnicalDetails.Text = LitTechnicalDetails.Text.Replace("\n", "<br>");

            if (Request["NOTENCRYPTED"] != null)
            {
                LitCompleteDetails.Text = ErrorLogHelper.GetErrorLogFromDatabase(Convert.ToInt32(errorID));
                LitCompleteDetails.Text = LitCompleteDetails.Text.Replace("\r\n", "<br>");
            }

            if (!SessionInfo.IsUserLoggedIn && !AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
            {
                //content.Attributes.Add("class", "content");
            }
        }
    }
}