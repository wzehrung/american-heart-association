﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/PatientPopup.master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Heart360Web.Pages.Common.Feedback" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <asp:UpdatePanel ID="upFeedback" runat="server">
        <ContentTemplate>

            <div id="divPopupTrackerStyle" runat="server" visible="true" class="popup-wrap page-red">
                <div class="row" >
                    <div class="large-12 small-12 columns spt">
                        <asp:LinkButton ID="btnLinkClose" CssClass="close-modal fr spr" runat="server" Text="CLOSE" />
                    </div>
                </div>
            </div>

            <div class="full">

                <div>
                    <asp:ValidationSummary
                        ID="valSummary"
                        ValidationGroup="Send"
                        HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                        Enabled="true"
                        CssClass="validation-error"
                        DisplayMode="BulletList"
                        ShowSummary="true"
                        runat="server" /> 
                </div>

                <div class="large-12 small-12 columns spb spt">
                    <b><%=GetGlobalResourceObject("Common", "Feedback_SendUsFeedbackType")%></b><span class="red">*</span><br />
                    <asp:DropDownList ID="ddlFeedbackTypes" runat="server" Width="400"></asp:DropDownList>
                </div>

                <div class="large-12 small-12 columns spb spt">
                    <b><%=GetGlobalResourceObject("Common", "Feedback_SendUsFeedbackComments")%></b><span class="red">*</span>
                    <asp:TextBox ID="txtFeedbackText" runat="server" TextMode="MultiLine" Width="300" Rows="5" MaxLength="1024"></asp:TextBox>
                </div>

                <div class="large-12 small-12 columns spb spt">
                    <asp:CheckBox ID="chkResponse" runat="server" TextAlign="Right" /><span style="padding-left: 10px;"><%=GetGlobalResourceObject("Common", "Feedback_Response")%></span><br />
                    <b><%=GetGlobalResourceObject("Common", "Text_Email")%></b><br />
                    <asp:TextBox ID="txtEmailAddress" runat="server" Width="300"></asp:TextBox>
                </div>

                <div class="large-12 small-12 columns">
                    <asp:LinkButton runat="server" ID="lnkSend" OnClick="lnkSend_OnClick" CssClass="gray-button full"><em><strong><%=GetGlobalResourceObject("Common", "Feedback_SendUsFeedbackSubmit")%></strong></em></asp:LinkButton>
                </div>

            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:CustomValidator ValidationGroup="Send" EnableClientScript="false" ID="custValidType"
        runat="server" Display="None" OnServerValidate="OnValidateType">
    </asp:CustomValidator>
    <asp:CustomValidator ValidationGroup="Send" EnableClientScript="false" ID="custValidComments"
        runat="server" Display="None" OnServerValidate="OnValidateComments"></asp:CustomValidator>
    <asp:CustomValidator ValidationGroup="Send" EnableClientScript="false" ID="custValidEmail"
        runat="server" Display="None" OnServerValidate="OnValidateEmail">
    </asp:CustomValidator>

</asp:Content>