﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.Pages.Common
{
    public partial class Feedback : System.Web.UI.Page
    {
        private string mCloseUrl = string.Empty;       // If non-null, this page is not displayed in an iframe


        public string CloseMeButton
        {
            get { return "btnPpeSummaryProxyOk"; }
        }
        public string CancelMeButton
        {
            get { return "btnPpeSummaryCancel"; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {

            if (Request.Params["closeurl"] != null)
            {
                mCloseUrl = Request.Params["closeurl"];
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(mCloseUrl))
                {
                    divPopupTrackerStyle.Attributes.Add("class", "popup-wrap page-red");
                    divPopupTrackerStyle.Visible = true;
                    btnLinkClose.PostBackUrl = mCloseUrl;
                }
            }

            _PopulateFeedbackTypes();
        }

        private void _PopulateFeedbackTypes()
        {
            List<AHAHelpContent.ListItem> objFeedbackTypes = AHAHelpContent.ListItem.FindListItemsByListCode("FB", 1);

            ddlFeedbackTypes.DataSource = objFeedbackTypes;
            ddlFeedbackTypes.DataValueField = "ListItemID";
            ddlFeedbackTypes.DataTextField = "ListItemName";
            ddlFeedbackTypes.DataBind();
        }

        protected void lnkSend_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Send");
            if (!Page.IsValid)
                return;

            string strSubject = "Heart360" + ": Feedback-" + ddlFeedbackTypes.Text;
            StringBuilder sb = new StringBuilder();
            sb.Append(txtFeedbackText.Text.Trim());
            string strReplyTo = string.Empty;
            if (chkResponse.Checked)
            {
                if (txtEmailAddress.Text.Trim().Length > 0)
                {
                    sb.Append("<br /><br />");
                    sb.Append("I would like a response to my feedback.");
                    sb.Append("<br />");
                    sb.Append("E-mail : " + txtEmailAddress.Text.Trim());
                    strReplyTo = txtEmailAddress.Text.Trim();
                }
            }
            EmailUtils.SendFeedbackEmail(strSubject, sb.ToString(), strReplyTo);
        }

        #region Validators

        protected void OnValidateType(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (ddlFeedbackTypes.SelectedValue == "0")
            {
                e.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_FeedbackType").ToString();
                return;
            }

            return;
        }

        protected void OnValidateComments(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtFeedbackText.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_Comments").ToString();
                return;
            }

            return;
        }

        protected void OnValidateEmail(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (chkResponse.Checked)
            {
                if (txtEmailAddress.Text.Trim().Length == 0)
                {
                    e.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_Email").ToString();
                    return;
                }
                else if (!GRCBase.EmailSyntaxValidator.Valid(txtEmailAddress.Text.Trim(), true))
                {
                    e.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Common", "Feedback_Validation_ValidEmail").ToString();
                    return;
                }
            }

            return;
        }

        #endregion
    }
}