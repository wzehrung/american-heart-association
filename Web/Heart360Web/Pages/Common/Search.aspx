﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="Heart360Web.Pages.Common.Search" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html><!--HTML5 doctype-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="../../library/css/search-iframe.css" />
     <!--[if IE 8 ]> <link rel="stylesheet" href="../../library/css/search-frame-ie8.css" /> <![endif]-->
</head>
<body class="search-iframe">
    <form id="form1" runat="server" target="_top">
        <asp:ToolkitScriptManager ID="scriptmgr" runat="server" EnablePartialRendering="true" >      
        </asp:ToolkitScriptManager>
        <div class="search-field">
            <div id="getFormStuff">
                <asp:Button ID="btnSearch" runat="server" CssClass="search-btn" OnClick="OnSubmitSearch" />
                <asp:TextBox ID="txtSearch" runat="server" MaxLength="60"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="wmeSearch" runat="server" TargetControlID="txtSearch" WatermarkText="<%$Resources:Common,Header_Text_Search %>"></asp:TextBoxWatermarkExtender>	                
            </div>
	    </div>
    </form>
</body>
</html>
