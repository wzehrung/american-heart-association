﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using AHACommon;
using AHAHelpContent;
using HVManager;
using Microsoft.Health;
using Microsoft.Health.Web;

using Heart360Web.Controls.Patient;

namespace Heart360Web.Pages.Patient
{
    public partial class NewUser : H360PageHelper
    {
        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }

        private AHAHelpContent.Patient AcceptedTerms
        {
            get;
            set;
        }

        public string PhoneContact
        {
            get
            {
                string retval = txtPhoneContact_AreaCode.Text + txtPhoneContact_Prefix.Text + txtPhoneContact_Suffix.Text;
                return retval;
            }
        }

        public string PhoneMobile
        {
            get
            {
                string retval = txtPhoneMobile_AreaCode.Text + txtPhoneMobile_Prefix.Text + txtPhoneMobile_Suffix.Text;
                return retval;
            }
        }

        public void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserIdentityContext objContext = SessionInfo.GetUserIdentityContext();

            this.CurrentPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objContext.PatientID.Value);

            // If the user has already accepted the terms, send them away
            if (objContext.HasUserAgreedToNewTermsAndConditions)
            {
                Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);
            }

            if (!Page.IsPostBack)
            {
                _LoadData();
            }
        }

        private void _LoadData()
        {
            AHAHelpContent.AHAUser objUser = AHAHelpContent.AHAUser.GetAHAUserByPersonGUID(SessionInfo.GetUserIdentityContext().UserId);

            HVManager.CurrentUserHVSetting objHVSetting = HVManager.CurrentUserHVSetting.GetCurrentUserSetting((this.Page as HealthServicePage).AuthenticatedConnection);
        }

        protected void OnSubmit(object sender, EventArgs e)
        {
            // Validate the form
            Page.Validate("NewUser");
            if (!Page.IsValid)
                return;

            // If the form was valid, validate optional BP readings
            if (!AddReadingBase.PerformValidationForExternalUse())
                return;

            // Need to access the fields on the ModuleAddSummary.ascx control
            TextBox txtHeartRate = AddReadingBase.FindControl("txtHeartRate") as TextBox;
            TextBox txtSystolic = AddReadingBase.FindControl("txtSystolic") as TextBox;
            TextBox txtDiastolic = AddReadingBase.FindControl("txtDiastolic") as TextBox;
            TextBox txtDate = AddReadingBase.FindControl("txtDate") as TextBox;
            DropDownList ddlTime = AddReadingBase.FindControl("ddlTime") as DropDownList;
            DropDownList ddlReadingSource = AddReadingBase.FindControl("ddlReadingSource") as DropDownList;

            // Validate the BP reading if they entered one, otherwise don't
            if (!String.IsNullOrEmpty(txtHeartRate.Text) || !String.IsNullOrEmpty(txtSystolic.Text) || !String.IsNullOrEmpty(txtDiastolic.Text))
            {
                try
                {
                    // Need to validate and save the blood pressure reading
                    AddReadingBase._SaveBloodPressure();
                }
                catch (Exception ex)
                {
                    libError.Text = ex.ToString();
                }
            }

            try
            {
                bool bolUserAgreement = Convert.ToBoolean(chkAccept.Checked);

                HVManager.AppProfileDataManager.AppProfileItem appProfileItem = HVHelper.HVManagerPatientBase.AppProfileDataManager.Item;

                bool bUseNewValueHasUserAgreedToNewTermsAndConditions = true;

                if (appProfileItem != null && appProfileItem.HasUserAgreedToNewTermsAndConditions.Value.HasValue)
                {
                    bUseNewValueHasUserAgreedToNewTermsAndConditions = (bolUserAgreement != appProfileItem.HasUserAgreedToNewTermsAndConditions.Value.Value);
                }

                if (appProfileItem == null)
                {
                    appProfileItem = new HVManager.AppProfileDataManager.AppProfileItem();
                }

                if (bolUserAgreement)
                {
                    appProfileItem.HasUserAgreedToNewTermsAndConditions.Value = bolUserAgreement;
                    appProfileItem.HasUserAgreedToStoreDataInDB.Value = bolUserAgreement;

                    HVHelper.HVManagerPatientBase.AppProfileDataManager.CreateOrUpdateItem(appProfileItem);
                }

                /*Add these items in DB*/
                bool bFirstTimeUser = false;
                AHAHelpContent.AHAUser objUser = AHAHelpContent.AHAUser.GetAHAUserByPersonGUID(SessionInfo.GetUserIdentityContext().UserId);
                if (!objUser.NewTermsAndConditions)
                {
                    //SessionInfo.SetDBSynchronized(false);
                    HVHelper.HVManagerPatientBase.IsDBSynchronized = false;
                    HVHelper.HVManagerPatientBase.IsPolkaDataProcessed = false;
                    bFirstTimeUser = true;
                }

                AHAHelpContent.AHAUser.UpdateUserNewTermsAndConsent(SessionInfo.GetUserIdentityContext().UserId, chkAccept.Checked);

                UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
                userIdentityContext.HasUserAgreedToNewTermsAndConditions = chkAccept.Checked;

                if (bFirstTimeUser)
                {
                    userIdentityContext.IsShowGSE = true;
                }

                _SavePhoneNumbers();
            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }
        }
        
        protected void _SavePhoneNumbers()
        {
            // Setting the country codes to 1 for now until we "internationalize"
            int iContactCountryCodeID = 1;
            int iMobileCountryCodeID = 1;

            // Contact and Mobile phone numbers

            try
            {
                if (!String.IsNullOrEmpty(PhoneContact) || !String.IsNullOrEmpty(PhoneMobile))
                {
                    AHAHelpContent.PhoneNumbers.CreateOrChangePhoneNumbers(CurrentPatient.UserHealthRecordID, iContactCountryCodeID, PhoneContact, iMobileCountryCodeID, PhoneMobile);
                }

                Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);
            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }
        }

        #region Validators

        // To check if the user agreed to the Terms and Conditions.
        protected void ValidateAcceptance(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (chkAccept.Checked == false)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Common", "Text_Validation_TermsOfUse").ToString();
                chkAccept.Attributes.Add("style.color", "RED");
                return;
            }
        }

        protected void ValidatePhoneContact(object sender, ServerValidateEventArgs e)
        {
            CustomValidator validator = sender as CustomValidator;
            e.IsValid = true;

            if (!String.IsNullOrEmpty(txtPhoneContact_AreaCode.Text))
            {
                if (txtPhoneContact_AreaCode.Text.Length < 3)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Contact").ToString();
                    e.IsValid = false;
                }
            }

            if (!String.IsNullOrEmpty(txtPhoneContact_Prefix.Text))
            {
                if (txtPhoneContact_Prefix.Text.Length < 3)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Contact").ToString();
                    e.IsValid = false;
                }
            }

            if (!String.IsNullOrEmpty(txtPhoneContact_Suffix.Text))
            {
                if (txtPhoneContact_Suffix.Text.Length < 4)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Contact").ToString();
                    e.IsValid = false;
                }
            }
        }

        protected void ValidatePhoneMobile(object sender, ServerValidateEventArgs e)
        {
            CustomValidator validator = sender as CustomValidator;
            e.IsValid = true;

            if (!String.IsNullOrEmpty(txtPhoneMobile_AreaCode.Text))
            {
                if (txtPhoneMobile_AreaCode.Text.Length < 3)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Mobile").ToString();
                    e.IsValid = false;
                }
            }

            if (!String.IsNullOrEmpty(txtPhoneMobile_Prefix.Text))
            {
                if (txtPhoneMobile_Prefix.Text.Length < 3)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Mobile").ToString();
                    e.IsValid = false;
                }
            }

            if (!String.IsNullOrEmpty(txtPhoneMobile_Suffix.Text))
            {
                if (txtPhoneMobile_Suffix.Text.Length < 4)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Mobile").ToString();
                    e.IsValid = false;
                }
            }
        }

        #endregion
    }
}