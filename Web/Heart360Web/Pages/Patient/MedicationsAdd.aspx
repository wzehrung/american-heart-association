﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PatientPopup.Master" AutoEventWireup="true" CodeBehind="MedicationsAdd.aspx.cs" Inherits="Heart360Web.Pages.Patient.MedicationsAdd" %>

<%@ Register Src="~/Controls/Patient/Medications/MedicationAddMed.ascx" TagName="AddMed" TagPrefix="Module" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <Module:AddMed ID="AddMed" runat="server" />

</asp:Content>