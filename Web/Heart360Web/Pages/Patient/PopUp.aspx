﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/PatientPopup.Master" AutoEventWireup="true" CodeBehind="PopUp.aspx.cs" Inherits="Heart360Web.Pages.Patient.PopUp" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <!-- class was modal-wrap cpopuptracker page-blue -->
    <div id="divPopupTrackerStyle" runat="server" visible="false" >
        <div class="row" >
            <div class="large-12 small-12 columns spt">
                <asp:LinkButton ID="btnLinkClose" CssClass="close-modal fr spr" runat="server" Text="CLOSE" />
            </div>
        </div>
    </div>
    <asp:PlaceHolder ID="phControl" runat="server" />
</asp:Content>