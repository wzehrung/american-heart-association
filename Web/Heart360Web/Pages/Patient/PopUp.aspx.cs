﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;


namespace Heart360Web.Pages.Patient
{
    public partial class PopUp : H360PageHelper
    {
        private bool                                    mCloseUsingSummaries = true;
        private string                                  mCloseUrl = string.Empty;       // If non-null, this page is not displayed in an iframe
        private Heart360Web.PatientPortal.MODULE_ID     mModId = Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;

        public string CloseMeButton
        {
            get { return (mCloseUsingSummaries) ? "btnPpeSummaryProxyOk" : "btnPpeTrackerProxyOk"; }
        }
        public string CancelMeButton
        {
            get { return (mCloseUsingSummaries) ? "btnPpeSummaryCancel" : "btnPpeTrackerCancel"; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            int     modId = (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
            string  editKeys = string.Empty;
            bool    loadedControl = false;

            //
            // process some top level query string parameters
            //
            if (Request.Params["modid"] != null)
            {
                modId = Convert.ToInt32(Request.Params["modid"]);
            }

            mModId = Heart360Web.PatientPortal.GetModuleIdFromInt(modId);

                // Data Element Id (elid) is only used for Reading and Deleting controls.
            if (Request.Params["elid"] != null)
            {
                editKeys = Request.Params["elid"];
            }

            if (Request.Params["closesummary"] != null)
            {
                mCloseUsingSummaries = (Convert.ToInt32(Request.Params["closesummary"]) == 0) ? false : true ;
            }

            if (Request.Params["closeurl"] != null)
            {
                mCloseUrl = Request.Params["closeurl"];
            }

            
            //
            // now determine which popup control to load
            //
            if (!loadedControl && Request.Params["displayemr"] != null)
            {
                //Heart360Web.Controls.Patient.e
            }

            if (Request.QueryString["hba1c"] == "add" || Request.QueryString["hba1c"] == "edit")
            {
                Heart360Web.Controls.Patient.ModuleAddReading rc = (Heart360Web.Controls.Patient.ModuleAddReading)LoadControl("~/Controls/Patient/ModuleAddReading.ascx");

                    rc.ModuleIdAsInt = modId;
                    if (!string.IsNullOrEmpty(editKeys))
                        rc.EditKeys = editKeys;
                    phControl.Controls.Add(rc);
            }

            if (Request.Params["reading"] != null)
            {
                if (Convert.ToInt32(Request.Params["reading"]) > 0)
                {
                    if (modId != (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS)
                    {
                        Heart360Web.Controls.Patient.ModuleAddReading rc = (Heart360Web.Controls.Patient.ModuleAddReading)LoadControl("~/Controls/Patient/ModuleAddReading.ascx");
                        rc.ModuleIdAsInt = modId;
                        if (!string.IsNullOrEmpty(editKeys))
                            rc.EditKeys = editKeys;
                        phControl.Controls.Add(rc);
                    }
                    else
                    {
                        Heart360Web.Controls.Patient.Medications.MedicationAddMed mamc = (Heart360Web.Controls.Patient.Medications.MedicationAddMed)LoadControl("~/Controls/Patient/Medications/MedicationAddMed.ascx");
                        if (!string.IsNullOrEmpty(editKeys))
                            mamc.EditKeys = editKeys;
                        phControl.Controls.Add(mamc);
                    }
                    loadedControl = true;
                }
            }

                // Note: the Goal control figures out for itself whether or not it is edit mode.
            if (!loadedControl && Request.Params["goal"] != null)
            {
                if (Convert.ToInt32(Request.Params["goal"]) > 0)
                {
                    Heart360Web.Controls.Patient.ModuleAddGoal gc = (Heart360Web.Controls.Patient.ModuleAddGoal)LoadControl("~/Controls/Patient/ModuleAddGoal.ascx");
                    gc.ModuleIdAsInt = modId;
                    phControl.Controls.Add(gc);
                    loadedControl = true;
                }
            }

            if (!loadedControl && Request.Params["delete"] != null)
            {
                int delVal = Convert.ToInt32(Request.Params["delete"]) ;
                if ( delVal > 0)
                {
                    if( modId != (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER )
                    {
                        if (Request.QueryString["hba1c"] == "delete")
                        {
                            Heart360Web.Controls.Patient.ModuleDeleteReading dr = (Heart360Web.Controls.Patient.ModuleDeleteReading)LoadControl("~/Controls/Patient/ModuleDeleteReading.ascx");
                            dr.ModuleIdAsInt = modId;
                            dr.HbA1c = 1;

                            if (!string.IsNullOrEmpty(editKeys))
                                dr.EditKeys = editKeys;
                            phControl.Controls.Add(dr);
                            loadedControl = true;
                        }
                        else
                        {
                            Heart360Web.Controls.Patient.ModuleDeleteReading dr = (Heart360Web.Controls.Patient.ModuleDeleteReading)LoadControl("~/Controls/Patient/ModuleDeleteReading.ascx");
                            dr.ModuleIdAsInt = modId;
                            if (!string.IsNullOrEmpty(editKeys))
                                dr.EditKeys = editKeys;
                            phControl.Controls.Add(dr);
                            loadedControl = true;
                        }
                    }
                    else
                    {
                        if (delVal == 1)
                        {
                            Heart360Web.Controls.Patient.ConnectionCenter.MyMessagePopup mmp =
                                        (Heart360Web.Controls.Patient.ConnectionCenter.MyMessagePopup)LoadControl("~/Controls/Patient/ConnectionCenter/MyMessagePopup.ascx");
                            if (!string.IsNullOrEmpty(editKeys))
                                mmp.EditKeys = editKeys;
                            phControl.Controls.Add(mmp);
                            loadedControl = true;
                        }
                        else if (delVal == 2)
                        {
                            Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsPopup cp = (Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsPopup)LoadControl("~/Controls/Patient/ConnectionCenter/ConnectionsPopup.ascx");
                            if (!string.IsNullOrEmpty(editKeys))
                                cp.EditKeys = editKeys;
                            phControl.Controls.Add(cp);
                            loadedControl = true;
                        }
                    }
                }
            }

            if (!loadedControl && modId == (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER)
            {
                if( Request.Params["connect"] != null )
                {
                    int connval = Convert.ToInt32(Request.Params["connect"]) ;
                    if ( connval == 1)  // ConnectionsPopup
                    {
                        Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsPopup cp = (Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsPopup)LoadControl("~/Controls/Patient/ConnectionCenter/ConnectionsPopup.ascx");
                        if (!string.IsNullOrEmpty(editKeys))
                            cp.EditKeys = editKeys;
                        phControl.Controls.Add(cp);
                        loadedControl = true;
                    }
                    else if (connval == 2) // RemindMePopup
                    {
                        Heart360Web.Controls.Patient.ConnectionCenter.RemindMePopup rmp = (Heart360Web.Controls.Patient.ConnectionCenter.RemindMePopup)LoadControl("~/Controls/Patient/ConnectionCenter/RemindMePopup.ascx");
                        if (!string.IsNullOrEmpty(editKeys))
                            rmp.EditKeys = editKeys;
                        phControl.Controls.Add(rmp);
                        loadedControl = true;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(mCloseUrl))
                {
                    switch (mModId)
                    {
                        case PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                        case PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                        case PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                            divPopupTrackerStyle.Attributes.Add("class", "popup-wrap page-blue");
                            break;

                        default:
                            divPopupTrackerStyle.Attributes.Add("class", "popup-wrap page-red");
                            break;
                    }
                    divPopupTrackerStyle.Visible = true;
                    btnLinkClose.PostBackUrl = mCloseUrl;
                }
            }
        }

        public void ClosePopup()
        {
            if (string.IsNullOrEmpty(mCloseUrl))    // contents in an iframe
            {
                if (!Page.ClientScript.IsStartupScriptRegistered("CLOSE ME"))
                {
                    string script = "window.parent.document.getElementById('" + this.CloseMeButton + "').click();";      
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "CLOSE ME", script, true);
                }
            }
            else  // contents in browser
            {
                Response.Redirect(mCloseUrl);
            }
        }

        public void CancelPopup()
        {
            if (string.IsNullOrEmpty(mCloseUrl))    // contents in an iframe
            {
                if (!Page.ClientScript.IsStartupScriptRegistered("CANCEL ME"))
                {
                    string script = "window.parent.document.getElementById('" + this.CancelMeButton + "').click();";
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "CANCEL ME", script, true);
                }
            }
            else  // contents in browser
            {
                Response.Redirect(mCloseUrl);
            }
        }
    }
}