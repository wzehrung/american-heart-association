﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HVManager;
using System.Text;

namespace Heart360Web.Patient.MyLifeCheck
{
    public partial class Start : H360PageHelper
    {
        protected string strFlashFile = "main_en_US.swf";
        protected string strGender = string.Empty;
        protected string strAge = string.Empty;
        protected string strWeight = string.Empty;
        protected string strHeight = string.Empty;
        protected string strSystolic = string.Empty;
        protected string strDiastolic = string.Empty;
        protected string strTC = string.Empty;
        protected string strBG = string.Empty;

        private void _SetMLCVariables()
        {
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name == AHACommon.Locale.Spanish)
            {
                strFlashFile = "main_sp_US.swf";
            }

            Page.Title = GetGlobalResourceObject("Common", "Text_MyLifeCheck").ToString();

            StringBuilder sb = new StringBuilder();

            HVManager.BasicDataManager.BasicItem objBasic = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
            if (objBasic != null
                && objBasic.GenderOfPerson.Value.HasValue)
            {
                strGender =  objBasic.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Female ? "2" : "1";
            }

            HVManager.PersonalDataManager.PersonalItem objPersonal = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
            if (objPersonal != null && objPersonal.DateOfBirth.Value.HasValue)
            {
                strAge = UIHelper.GetAge(objPersonal.DateOfBirth.Value);
            }

            UserWeight objUserWeight = new UserWeight();
            if (objUserWeight != null 
                && objUserWeight.LatestValue != null 
                && objUserWeight.LatestValue.Length > 0
                && objUserWeight.LatestValue[0] != null)
            {
                strWeight =  objUserWeight.LatestValue[0].ToString();
            }

            UserBP objUserBP = new UserBP();
            if (objUserBP != null 
                && objUserBP.LatestValue != null 
                && objUserBP.LatestValue.Length > 1
                && objUserBP.LatestValue[0] != null
                && objUserBP.LatestValue[1] != null)
            {
                strSystolic =  objUserBP.LatestValue[0].ToString();
                strDiastolic = objUserBP.LatestValue[1].ToString();
            }

            UserCholesterol objUserCholesterol = new UserCholesterol();
            if (objUserCholesterol != null 
                && objUserCholesterol.LatestValue != null 
                && objUserCholesterol.LatestValue.Length > 0
                && objUserCholesterol.LatestValue[0] != null)
            {
                strTC = objUserCholesterol.LatestValue[0].ToString();
            }

            UserBG objUserBG = new UserBG();
            if (objUserBG != null && 
                objUserBG.LatestValue != null && 
                objUserBG.LatestValue.Length > 0
                && objUserBG.LatestValue[0] != null)
            {
                strBG = objUserBG.LatestValue[0].ToString();
            }

            HVManager.HeightDataManager.HeightItem heightItem = HeightWrapper.GetLatestHeightItem();
            if (heightItem != null)
            {
                double dInches = UnitConversion.MetersToInches(heightItem.HeightInMeters.Value);
                strHeight = dInches.ToString();
            }
        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            _SetMLCVariables();

            // Removed this because we are going to redirect everyone without flash using JS in frontend
            //string strUserAgent = Request.UserAgent.ToString().ToLower();
            //if (strUserAgent != null)
            //{
            //    if (
            //            Request.Browser.IsMobileDevice == true ||
            //            strUserAgent.Contains("iphone") ||
            //            strUserAgent.Contains("blackberry") ||
            //            strUserAgent.Contains("mobile") ||
            //            strUserAgent.Contains("windows phone") ||
            //            strUserAgent.Contains("opera mini") ||
            //            strUserAgent.Contains("palm") ||
            //            strUserAgent.Contains("ipad") ||
            //            strUserAgent.Contains("kindle")
            //        )
            //    {
            //        Response.Redirect("http://mylifecheck.heart.org/mobile/");
            //    }
            //}
        }
    }
}
