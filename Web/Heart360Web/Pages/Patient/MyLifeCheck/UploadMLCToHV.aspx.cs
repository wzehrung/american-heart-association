﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace Heart360Web.Patient.MyLifeCheck
{
    public partial class UploadMLCToHV : H360PageHelper
    {
        protected string STR_CURRENT_USER_NAME = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            STR_CURRENT_USER_NAME = CurrentUser.FirstName;
            if (!Page.IsPostBack)
            {
                AHAAPI.UserIdentityContext userIdentityContext = AHAAPI.SessionInfo.GetUserIdentityContext();
                AHAAPI.MLCInfo objMLCInfo = AHAAPI.SessionInfo.GetMLCInfoFromSession() as AHAAPI.MLCInfo;
                if (objMLCInfo != null && objMLCInfo.NameValueCollection != null)
                {
                    if (!string.IsNullOrEmpty(objMLCInfo.Name))
                    {
                        STR_CURRENT_USER_NAME = objMLCInfo.Name;
                    }
                    if (objMLCInfo.RecordID.CompareTo(userIdentityContext.RecordId) == 0 || objMLCInfo.RecordID == Guid.Empty)
                    {
                        NameValueCollection nvc = objMLCInfo.NameValueCollection;
                        bool bIsOnlineConnection = false;
                        bool bAccess = AHAAPI.H360Utility.HasPatientGivenAccessForFileType(this.AuthenticatedConnection, this.PersonGUID, this.RecordGUID, out bIsOnlineConnection);
                        if (bIsOnlineConnection)
                        {
                            HVManager.FileDataManager.FileItem objFileItem = new HVManager.FileDataManager.FileItem();
                            //save and return to home page
                            objFileItem = new HVManager.FileDataManager.FileItem();
                            objFileItem.Name.Value = string.Format("MLC_{0}.pdf", DateTime.Now.ToUniversalTime().ToString());
                            objFileItem.HealthScore.Value = Convert.ToDouble(nvc["healthScore"]);
                            objFileItem.PdfBytes.Value = Convert.FromBase64String(nvc["pdfBytes"]);
                            objFileItem.SystolicBP.Value = Convert.ToInt32(nvc["sbp"]);
                            objFileItem.DiastolicBP.Value = Convert.ToInt32(nvc["dbp"]);
                            objFileItem.TotalCholesterol.Value = Convert.ToInt32(nvc["totChol"]);
                            objFileItem.BloodGlucose.Value = Convert.ToDouble(nvc["bloodSugar"]);
                            objFileItem.BMI.Value = Convert.ToDouble(nvc["bmi"]);
                            HVHelper.HVManagerPatientBase.FileDataManager.CreateItem(objFileItem);
                            AHAHelpContent.MyLifeCheckItem.CreateMLCItem(userIdentityContext.PersonalItemGUID,
                                Convert.ToDouble(nvc["healthScore"]),
                                Convert.ToInt32(nvc["activityRating"]),
                                Convert.ToInt32(nvc["bloodSugarRating"]),
                                Convert.ToInt32(nvc["bpRating"]),
                                Convert.ToInt32(nvc["cholRating"]),
                                Convert.ToInt32(nvc["weightRating"]),
                                Convert.ToInt32(nvc["dietRating"]),
                                Convert.ToInt32(nvc["smokingRating"]),
                                Convert.ToInt32(Request.Form["sbp"]),
                                Convert.ToInt32(Request.Form["dbp"]),
                                Convert.ToInt32(Request.Form["totChol"]),
                                Convert.ToDouble(Request.Form["bloodSugar"]),
                                Convert.ToDouble(Request.Form["bmi"])
                                );
                            AHAAPI.SessionInfo.ResetMLCInfo();
                            HVHelper.HVManagerPatientBase.FileDataManager.FlushCache();
                            Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_HOME, true);
                        }
                    }
                }
                else
                {
                    Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_HOME, true);
                }
            }
            if (string.IsNullOrEmpty(STR_CURRENT_USER_NAME))
            {
                STR_CURRENT_USER_NAME = CurrentUser.FirstName;
            }
            litEnsureCorrectRecord.Text = string.Format(GetGlobalResourceObject("MyLifeCheck", "Patient_MLC_EnsureCorrectRecordSelected").ToString(), STR_CURRENT_USER_NAME);

        }

        protected void OnHome(object sender, EventArgs e)
        {
            AHAAPI.SessionInfo.ResetMLCInfo();
            Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_HOME, true);
        }

        protected void ProceedToHV(object sender, EventArgs e)
        {
            AHAAPI.H360Utility.RedirectToHVSelectRecordOfflineAccessForFilePage(GRCBase.UrlUtility.ServerName + "/Pages/Patient/MyLifeCheck/UploadMLCToHV.aspx");
        }
    }
}
