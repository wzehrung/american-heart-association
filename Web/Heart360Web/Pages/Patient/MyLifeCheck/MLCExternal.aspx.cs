﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.Patient.MyLifeCheck
{
    public partial class MLCExternal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext.Current.Server.ScriptTimeout = 300;
            if (Request.Form != null)
            {
                AHAAPI.UserIdentityContext userIdentityContext = AHAAPI.SessionInfo.GetUserIdentityContext();

                AHAAPI.MLCInfo objMLCInfo = new AHAAPI.MLCInfo();
                objMLCInfo.NameValueCollection = Request.Form;
                if (userIdentityContext != null)
                {
                    objMLCInfo.RecordID = userIdentityContext.RecordId;
                    //objMLCInfo.Name = CurrentUser.FirstName;
                }

                AHAAPI.SessionInfo.SetMLCInfoToSession(objMLCInfo);

                Response.Clear();
                Response.Write(GRCBase.UrlUtility.ServerRootUrl + "/Pages/Patient/MyLifeCheck/MLCExternalFinal.aspx");
                Response.Flush();
                Response.End();
            }
        }
    }
}
