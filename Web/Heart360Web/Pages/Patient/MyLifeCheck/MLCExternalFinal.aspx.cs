﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.Patient.MyLifeCheck
{
    public partial class MLCExternalFinal : System.Web.UI.Page
    {
        public string STR_URL = "/";
        protected void Page_Load(object sender, EventArgs e)
        {
            AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByURL(AHACommon.AHAAppSettings.MLCCampaignURL, AHAAPI.H360Utility.GetCurrentLanguageID());
            if (objCampaign != null && objCampaign.IsActive)
            {
                STR_URL = string.Format("{0}/{1}", GRCBase.UrlUtility.ServerRootUrl, AHACommon.AHAAppSettings.MLCCampaignURL);
            }
        }
    }
}
