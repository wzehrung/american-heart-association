﻿<%@ Page MasterPageFile="~/MasterPages/Patient.Master" Language="C#" AutoEventWireup="true"
    CodeBehind="UploadMLCToHV.aspx.cs" Inherits="Heart360Web.Patient.MyLifeCheck.UploadMLCToHV" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="aha-white-box">
        <div class="aha-icon-holder">
            <!--  change image  -->
            <img src="/images/report-icon.png">
        </div>
        <div class="aha-white-box-top">
        </div>
        <div class="aha-white-box-mid">
            <div class="aha-white-box-heading ">
                <h3>
                    <%=GetGlobalResourceObject("MyLifeCheck", "Text_UploadToHealthVault")%></h3>
                <div class="aha-guidingtext">
                </div>
                <div id="DivNoHVPermission" runat="server" class="uploadtohealthvault">
                    <b><%=GetGlobalResourceObject("MyLifeCheck", "Text_Heart360AdditionalAuth")%></b>
                    <br />
                    <br />
                    <%=GetGlobalResourceObject("MyLifeCheck", "Text_ProceedToHVDesc")%>
                    <br />
                    <br />
                    <asp:Literal ID="litEnsureCorrectRecord" runat="server"></asp:Literal>
                </div>
                <asp:LinkButton ID="Button1" runat="server" OnClick="ProceedToHV" CssClass="red-button-with-arrow "><em><strong><%=GetGlobalResourceObject("MyLifeCheck", "Button_ContinueHV")%></strong></em></asp:LinkButton>
                <asp:LinkButton ID="Button2" runat="server" OnClick="OnHome" CssClass="back-to-dashboard"><%=GetGlobalResourceObject("MyLifeCheck", "Button_ContinueDashboard")%></asp:LinkButton>
            </div>
            <br />
        </div>
        <div class="aha-white-box-bottom">
        </div>
    </div>
</asp:Content>
