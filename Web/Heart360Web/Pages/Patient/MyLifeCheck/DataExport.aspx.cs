﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace Heart360Web.Patient.MyLifeCheck
{
    public partial class DataExport : H360PageHelper
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext.Current.Server.ScriptTimeout = 300;
            if (Request.Form != null)
            {
                AHAAPI.UserIdentityContext userIdentityContext = AHAAPI.SessionInfo.GetUserIdentityContext();

                AHAAPI.MLCInfo objMLCInfo = new AHAAPI.MLCInfo();
                objMLCInfo.NameValueCollection = Request.Form;
                objMLCInfo.RecordID = userIdentityContext.RecordId;
                objMLCInfo.Name = CurrentUser.FirstName;

                AHAAPI.SessionInfo.SetMLCInfoToSession(objMLCInfo);
                //foreach (string strKey in Request.Form.Keys)
                //{
                //    Response.Write(string.Format("{0} = {1}<br/>", strKey, Request.Form[strKey]));
                //}
                bool bIsOnlineConnection = false;
                bool bAccess = AHAAPI.H360Utility.HasPatientGivenAccessForFileType(this.AuthenticatedConnection, this.PersonGUID, this.RecordGUID, out bIsOnlineConnection);
                if (bIsOnlineConnection)
                {
                    HVManager.FileDataManager.FileItem objFileItem = new HVManager.FileDataManager.FileItem();
                    //save and return to home page
                    objFileItem = new HVManager.FileDataManager.FileItem();
                    objFileItem.Name.Value = string.Format("MLC_{0}.pdf", DateTime.Now.ToUniversalTime().ToString());
                    objFileItem.HealthScore.Value = Convert.ToDouble(Request.Form["healthScore"]);
                    objFileItem.PdfBytes.Value = Convert.FromBase64String(Request.Form["pdfBytes"]);
                    objFileItem.SystolicBP.Value = Convert.ToInt32(Request.Form["sbp"]);
                    objFileItem.DiastolicBP.Value = Convert.ToInt32(Request.Form["dbp"]);
                    objFileItem.TotalCholesterol.Value = Convert.ToInt32(Request.Form["totChol"]);
                    objFileItem.BloodGlucose.Value = Convert.ToDouble(Request.Form["bloodSugar"]);
                    objFileItem.BMI.Value = Convert.ToDouble(Request.Form["bmi"]);
                    HVHelper.HVManagerPatientBase.FileDataManager.CreateItem(objFileItem);
                    AHAHelpContent.MyLifeCheckItem.CreateMLCItem(userIdentityContext.PersonalItemGUID,
                        Convert.ToDouble(Request.Form["healthScore"]),
                        Convert.ToInt32(Request.Form["activityRating"]),
                            Convert.ToInt32(Request.Form["bloodSugarRating"]),
                            Convert.ToInt32(Request.Form["bpRating"]),
                            Convert.ToInt32(Request.Form["cholRating"]),
                            Convert.ToInt32(Request.Form["weightRating"]),
                            Convert.ToInt32(Request.Form["dietRating"]),
                            Convert.ToInt32(Request.Form["smokingRating"]),
                            Convert.ToInt32(Request.Form["sbp"]),
                            Convert.ToInt32(Request.Form["dbp"]),
                            Convert.ToInt32(Request.Form["totChol"]),
                            Convert.ToDouble(Request.Form["bloodSugar"]),
                            Convert.ToDouble(Request.Form["bmi"])                            
                        );
                    HVHelper.HVManagerPatientBase.FileDataManager.FlushCache();
                    AHAAPI.SessionInfo.ResetMLCInfo();
                }
            }
            Response.Clear();
            Response.Write(GRCBase.UrlUtility.ServerRootUrl + "/Pages/Patient/MyLifeCheck/MLCExternalFinal.aspx");
            Response.Flush();
            Response.End();
        }
    }
}
