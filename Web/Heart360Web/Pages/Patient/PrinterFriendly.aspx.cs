﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHACommon;

namespace Heart360Web.Pages.Patient
{
    public partial class PrinterFriendly : H360PageHelper, IPeriodicDataManager
    {
        private DataDateReturnType mDateRange = DataDateReturnType.Last3Months;

        protected void Page_Init(object sender, EventArgs e)
        {
            int                 modId = (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN;
            bool                currentMeds = true ;
            

            if (Request.Params["modid"] != null)
            {
                modId = Convert.ToInt32(Request.Params["modid"]);
            }

            if (Request.Params["currentmeds"] != null)
            {
                currentMeds = (Convert.ToInt32(Request.Params["currentmeds"]) > 0) ? true : false ;
            }

            if (Request.Params["daterange"] != null)
            {
                if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.LastWeek.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.LastWeek;
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.LastMonth.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.LastMonth;
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last3Months.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.Last3Months;
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last6Months.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.Last6Months;
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last9Months.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.Last9Months;
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.LastYear.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.LastYear;
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last2Years.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.Last2Years ;
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.AllData.ToString()) == 0)
                {
                    mDateRange = AHACommon.DataDateReturnType.AllData;
                }
            }

            // Add module specific contents
            switch (modId)
            {
                case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    {
                        Heart360Web.Controls.Patient.BloodPressure.BPPrinterFriendly bppf = (Heart360Web.Controls.Patient.BloodPressure.BPPrinterFriendly)LoadControl("~/Controls/Patient/BloodPressure/BPPrinterFriendly.ascx");
                        bppf.IPeriodicDataManager = this;
                        phControl.Controls.Add(bppf);
                    }
                    break;

                case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    {
                        Heart360Web.Controls.Patient.BloodGlucose.BGPrinterFriendly bgpf = (Heart360Web.Controls.Patient.BloodGlucose.BGPrinterFriendly)LoadControl("~/Controls/Patient/BloodGlucose/BGPrinterFriendly.ascx");
                        bgpf.IPeriodicDataManager = this;
                        phControl.Controls.Add(bgpf);
                    }
                    break;

                case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    {
                        Heart360Web.Controls.Patient.Cholesterol.CholesterolPrinterFriendly cpf = (Heart360Web.Controls.Patient.Cholesterol.CholesterolPrinterFriendly)LoadControl("~/Controls/Patient/Cholesterol/CholesterolPrinterFriendly.ascx");
                        cpf.IPeriodicDataManager = this;
                        phControl.Controls.Add(cpf);
                    }
                    break;

                case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    {
                        Heart360Web.Controls.Patient.PhysicalActivity.PAPrinterFriendly papf = (Heart360Web.Controls.Patient.PhysicalActivity.PAPrinterFriendly)LoadControl("~/Controls/Patient/PhysicalActivity/PAPrinterFriendly.ascx");
                        papf.IPeriodicDataManager = this;
                        phControl.Controls.Add(papf);
                    }
                    break;

                case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    {
                        Heart360Web.Controls.Patient.Weight.WeightPrinterFriendly wpf = (Heart360Web.Controls.Patient.Weight.WeightPrinterFriendly)LoadControl("~/Controls/Patient/Weight/WeightPrinterFriendly.ascx");
                        wpf.IPeriodicDataManager = this;
                        phControl.Controls.Add(wpf);
                    }
                    break;

                case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    {
                        Heart360Web.Controls.Patient.Medications.MedicationPrinterFriendly mpf = (Heart360Web.Controls.Patient.Medications.MedicationPrinterFriendly)LoadControl("~/Controls/Patient/Medications/MedicationPrinterFriendly.ascx");
                        mpf.IPeriodicDataManager = this;
                        mpf.IsCurrentMeds = currentMeds;
                        phControl.Controls.Add(mpf);
                    }
                    break;

                case (int)Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    {
                        Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsInvitation cif = (Heart360Web.Controls.Patient.ConnectionCenter.ConnectionsInvitation)LoadControl("~/Controls/Patient/ConnectionCenter/ConnectionsInvitation.ascx");
                        phControl.Controls.Add(cif);
                    }
                    break;

                default:
                    break;
            }
          
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region IPeriodicDataManager Members

        public DataDateReturnType DateRange
        {
            get { return mDateRange; }
        }

        public void LoadItems() { }
        public void FocusTabOne() { }

        public AHAAPI.TrackerType TrackerType
        {
            get { throw new NotImplementedException(); }
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        public void OnCreatePDF()
        {
            throw new NotImplementedException();
        }

        public DataViewType DataViewType
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}