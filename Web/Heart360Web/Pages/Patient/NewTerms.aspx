﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Patient.Master" AutoEventWireup="true" CodeBehind="NewTerms.aspx.cs" Inherits="Heart360Web.Pages.Patient.NewTerms" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

	<div class="large-12 small-12 columns">

		<header class="full">   
            <h2><%=GetGlobalResourceObject("Common", "NewTerms_Title")%></h2>
            <h2><%=GetGlobalResourceObject("Common", "NewTerms_Text")%></h2>
		</header>

        <asp:ValidationSummary 
            ID="valSummary"
            ValidationGroup="NewUser"
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            Enabled="true"
            DisplayMode="BulletList"
            ShowSummary="true"
            CssClass="validation-error"
            runat="server" />

	</div>

    <!-- ## BEGIN Acceptance ## -->
    <div class="large-12 small-24 columns fl">

        <div class="row">
            <div class="large-12 small-12 columns">
                <p><span class="required" style="font-weight: bold; color: Red;">
                        <%=GetGlobalResourceObject("Common","Symbol_Asterisk")%>
                </span><strong>Required</strong></p>
            </div>                    
        </div>

        <div class="row">
            <div class="large-12 small-12 columns" style="height: 400px; overflow-y: scroll; border: 1px solid #666;">
                <%=GetGlobalResourceObject("Common","AHA_User_Agreement")%>
            </div>                    
        </div>

		<div class="row">
            <div class="large-1 small-1 columns" style="padding-top: 5px">
                <asp:CheckBox ID="chkAccept" runat="server" CssClass="fr" />
            </div>
			<div class="large-11 small-11 columns">
                <label class="check-text" for="ContentPlaceHolderMain_chkAccept" style="padding-top: 5px">
                    <span class="required" style="font-weight: bold; color: Red;"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span>
                    <%=GetGlobalResourceObject("Common","Text_User_Agreement_Checkbox") %>
                </label>
			</div>          
        </div>

        <div class="row">
            <div class="large-6 small-12 columns">
	            <div class="large-6 small-6 columns">
                    <asp:Button ID="btnAccept" CssClass="red-button full" Text="<%$Resources:Common,Text_Save%>" runat="server" onclick="OnSubmit" />
	            </div>
            </div>
		</div>

    </div>
    <!-- ## END Acceptance ## -->

    <asp:Label ID="libError" runat="server"></asp:Label>

    <asp:CustomValidator
        ID="CustomValidator1"
        ValidationGroup="NewUser"
        Display="None"
        OnServerValidate="ValidateAcceptance"
        runat="server"
        EnableClientScript="false">
    </asp:CustomValidator>

</asp:Content>