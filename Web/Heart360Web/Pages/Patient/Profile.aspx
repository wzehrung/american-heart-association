﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Patient.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Heart360Web.Pages.Patient.Profile" %>

<%@ Register Src="~/Controls/Patient/ModuleSummary.ascx" TagName="Summary" TagPrefix="Module" %>
<%@ Register Src="~/Controls/Patient/Profile/ProfileFamilyMembers.ascx" TagName="Family" TagPrefix="Profile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <!-- ## BEGIN Tracker Details ## -->
    <section id="tracker-details" class="small-12 large-8 columns">
            
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <Module:Summary ID="SummaryProfile" ModuleIdAsInt="2" TrackerType="my-profile-large" SummaryLocation="2" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSaveProfile" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnSaveHistory" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnSaveEmergency" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="upMaster" UpdateMode="Conditional" runat="server">
        <ContentTemplate>

            <!-- ## BEGIN Edit My Profile -->
            <section class="edit-profile info-stats full">
                <div class="accordion">
	                <div id="dtProfile" class="toggle-btn" runat="server">
		                <h4><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Title_EditMyProfile")%><span class="gray-arrow"></span></h4>
	                </div>

	                <div style="display: none;" class="content">

		                <div class="row">
			                <div class="large-12 small-12 columns">
				                <h4><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Text_EditMyProfile")%></h4>
			                </div>

			                <div class="large-12 small-12 columns">

                                <asp:UpdatePanel ID="upanelProfile" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>

                                        <asp:ValidationSummary 
                                            ID="valSummary"
                                            ValidationGroup="MyProfile"
                                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                            Enabled="true"
                                            CssClass="validation-error"
                                            DisplayMode="BulletList"
                                            ShowSummary="true"
                                            runat="server" />   

                                        <!-- ## BEGIN My Profile ## -->
					                    <div class="full-name row">
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker","Tracker_Profile_FirstName")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="required"></asp:TextBox>
						                    </div>
						                    <div class="large-4 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_LastName")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:TextBox ID="txtLastName" runat="server" CssClass="required"></asp:TextBox>
						                    </div>
		                                    <div class="large-4 small-12 columns">
                                                <div class="row">
								                    <div class="large-6 small-12 columns">
									                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Suffix")%></label>
									                    <asp:DropDownList ID="ddlSuffix" runat="server" CssClass="dropdowns" />
								                    </div>
							                        <div class="large-6 small-12 columns">
								                        <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Title")%></label>
								                        <asp:DropDownList ID="ddlTitle" runat="server" CssClass="dropdowns" />
							                        </div>
                                                </div>
                                            </div> 
        			                    </div>

					                    <div class="address row">
						                    <div class="large-8 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Address")%></label>
							                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
						                    </div>
						                    <div class="large-4 small-12 columns">

						                    </div>      
					                    </div>

					                    <div class="city-state-zip row">
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_City")%></label>
							                    <asp:TextBox ID="txtCity" runat="server" CssClass="required"></asp:TextBox>
						                    </div>
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_State")%></label>
							                    <asp:DropDownList ID="ddlState" runat="server" CssClass="dropdowns" />
						                    </div>
						                    <div class="large-4 small-12 columns">
							                    <div class="row">
								                    <div class="large-6 small-12 columns">
									                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_ZipCode")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
									                    <asp:TextBox ID="txtZipCode" runat="server" CssClass="required"></asp:TextBox>
								                    </div>
                                                    <div class="large-6 small-12 columns">
        
                                                    </div>
							                    </div>
						                    </div>
					                    </div>

                                        <div class="country row">
                                            <div class="large-4 small-12 columns">
 							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Country")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="dropdowns" />    
                                            </div>
                                            <div class="large-4 small-12 columns">&nbsp;</div>
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_EmailAddress")%>*</label>
							                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="32"></asp:TextBox>
						                    </div>
                                        </div>

					                    <div class="phone-email row" style="margin-top:.6em;">
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_ContactPhoneNumber")%></label>
                                                <div class="row">
							                        <div class="large-4 small-4 columns">
                                                        <asp:TextBox ID="txtPhoneContact_AreaCode" runat="server" MaxLength="3"></asp:TextBox>
                                                    </div>

                                                    <div class="large-4 small-4 columns">
                                                        <asp:TextBox ID="txtPhoneContact_Prefix" runat="server" MaxLength="3"></asp:TextBox>
                                                    </div>

                                                    <div class="large-4 small-4 columns">
                                                        <asp:TextBox ID="txtPhoneContact_Suffix" runat="server" MaxLength="4"></asp:TextBox>
                                                    </div>
                                                </div>
						                    </div>
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_MobilePhoneNumber")%></label>
                                                <div class="row">
							                        <div class="large-4 small-4 columns">
                                                        <asp:TextBox ID="txtPhoneMobile_AreaCode" runat="server" MaxLength="3"></asp:TextBox>
                                                    </div>

                                                    <div class="large-4 small-4 columns">
                                                        <asp:TextBox ID="txtPhoneMobile_Prefix" runat="server" MaxLength="3"></asp:TextBox>
                                                    </div>

                                                    <div class="large-4 small-4 columns">
                                                        <asp:TextBox ID="txtPhoneMobile_Suffix" runat="server" MaxLength="4"></asp:TextBox>
                                                    </div>
                                                </div>
						                    </div>
						                    <div class="large-4 small-12 columns">
							                    &nbsp;
						                    </div>
					                    </div>

                                        <div class="row">
						                    <div class="large-12 small-12 columns">
                                                <p><asp:Label Id="lblWarningSMS" runat="server"><span class="my-profile-warning"><%=GetGlobalResourceObject("RemindMe", "Text_Warning")%></span><%=GetGlobalResourceObject("RemindMe", "SMS_Text_Warning")%></asp:Label></p>
                                                <p><asp:Label Id="lblWarningIVR" runat="server"><span class="my-profile-warning"><%=GetGlobalResourceObject("RemindMe", "Text_Warning")%></span><%=GetGlobalResourceObject("RemindMe", "IVR_Text_Warning")%></asp:Label></p>
                                            </div>
                                        </div>

                                        <div class="birthyear row">
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Gender")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:DropDownList ID="ddlGender" runat="server" CssClass="dropdowns" />
						                    </div>
                                            <div class="large-4 small-12 columns">
                                                <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_BirthYear")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
                                                <asp:DropDownList ID="ddlYearOfBirth" runat="server" CssClass="dropdowns" />
                                            </div>
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Ethnicity")%></label>
							                    <asp:DropDownList ID="ddlEthnicity" runat="server" CssClass="dropdowns" />
						                    </div>
                                        </div>


											
					                    <div class="continue-step2 row spt">
						                    <div class="large-8 small-12 columns">

                                                <asp:UpdateProgress ID="uprogressProgressProfile" AssociatedUpdatePanelID="upanelProfile" runat="server">
                                                    <ProgressTemplate>
                                                        <div class="row">
                                                            <div class="large-4 small-12 columns">
                                                                <asp:Image ID="Image1" runat="server" ImageAlign="right" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                            </div>
                                                            <div class="large-8 small-12 columns">
                                                                <%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Message_Saving_Profile")%>
                                                            </div>
                                                        </div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>                                         
                                        
                                                <asp:Label ID="lblMessageSaveProfile" runat="server"></asp:Label>
                                            </div>

						                    <div class="large-4 small-12 columns">
                                                <asp:Button ID="btnSaveProfile" CssClass="signin-btn gray-button full" CommandName="Refresh" OnClick="OnSave_Profile" runat="server" Text="<%$Resources:Common,Text_Save %>" />
						                    </div>
					                    </div>
                                        <!-- ## END My Profile ## -->

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender1"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneContact_AreaCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender2"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneContact_Prefix"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender3"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneContact_Suffix"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender4"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneMobile_AreaCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender5"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneMobile_Prefix"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender6"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneMobile_Suffix"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

			                </div>
		                </div>						
	                </div>
                </div>
            </section>
            <!-- ## END Edit My Profile -->

            <!-- ## BEGIN Edit Health History -->
            <section class="my-graph info-stats full">
	            <div class="accordion">
		            <div class="toggle-btn">
			            <h4><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_MyHealthHistory")%><span class="gray-arrow"></span></h4>
		            </div>

		            <div class="content" style="display: block;">
			            <div class="row">
				            <div class="large-12 small-12 columns">
					            <h4><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Text_MyHealthHistory")%></h4>
				            </div>


                                <asp:UpdatePanel ID="upHealthHistory" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>

                                        <div class="large-12 small-12 columns" >
                                        <asp:ValidationSummary 
                                            ID="valHealthHistory"
                                            ValidationGroup="HealthHistory"
                                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                            Enabled="true"
                                            DisplayMode="BulletList"
                                            ShowSummary="true"
                                            CssClass="validation-error"
                                            runat="server" />
                                        </div>

                                        <!-- ## BEGIN Height/Weight ## -->

                            
                                        <div class="large-12 small-12 columns" >

                                            <div class="large-4 small-12 columns">
								                <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Height")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
                                                <label class="text-left"><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Feet")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
                                                <asp:TextBox ID="txtHeightFeet" runat="server" CssClass="required large-6 small-6 columns" MaxLength="1"></asp:TextBox>
                                            </div>
                                            <div class="large-4 small-12 columns">
                                                <label class="text-left">&nbsp;</label>
                                                <label class="text-left"><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Inches")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
                                                <asp:TextBox ID="txtHeightInches" runat="server" CssClass="required large-6 small-6 columns" MaxLength="2"></asp:TextBox>
                                            </div>
                                            <div class="large-4 small-12 columns" >
                                                <label class="text-left full">&nbsp;</label>
									            <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_BloodType")%></label>
									            <asp:DropDownList ID="ddlBloodType" runat="server" CssClass="dropdowns"></asp:DropDownList>                 
                                            </div>

                                        </div>


                                        <div class="large-12 small-12 columns">
                                        <!-- ## BEGIN Questions ## -->									
					                    <div class="large-8 small-12 columns questions">
                                            <label class="text-left">Please answer the following questions:</label>
                                                <table class="radButtons full">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_Yes")%>
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_No")%>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radBpMedicationYes" GroupName="radBpMedication" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radBpMedicationNo" GroupName="radBpMedication" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_TakingBPMedication")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radHyperTensionDietYes" GroupName="radHyperTensionDiet" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radHyperTensionDietNo" GroupName="radHyperTensionDiet" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_HypertensionDiet")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radKidneyDiseaseYes" GroupName="radKidneyDisease" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radKidneyDiseaseNo" GroupName="radKidneyDisease" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_KidneyDisease")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radDiabetesYes" GroupName="radDiabetes" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radDiabetesNo" GroupName="radDiabetes" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_Diabetes")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radFHHeartYes" GroupName="radFHHeart" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radFHHeartNo" GroupName="radFHHeart" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_FamilyHistoryHeartDisease")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radHeartDiseaseYes" GroupName="radHeartDisease" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radHeartDiseaseNo" GroupName="radHeartDisease" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_DiagnoisedHeartDisease")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radFHStrokeYes" GroupName="radFHStroke" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radFHStrokeNo" GroupName="radFHStroke" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_FamilyHistoryOfStroke")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radSmokeYes" GroupName="radSmoke" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radSmokeNo" GroupName="radSmoke" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_DoYouSmoke")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="radSmokeQuitYes" GroupName="radSmokeQuit" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton ID="radSmokeQuitNo" GroupName="radSmokeQuit" runat="server" />
                                                            </td>
                                                            <td>
                                                                <%= GetGlobalResourceObject("Tracker", "Tracker_Profile_TriedToQuitSmoking")%>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                                          
					                    </div>
					                    <!-- ## END Questions ## -->	

                                        <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Allergies")%></label>
                                                <asp:TextBox ID="txtAllergies" runat="server" TextMode="MultiLine" MaxLength="512"></asp:TextBox>
						                </div>
                                        </div>
                    					
                                        <!-- ## BEGIN Save ## -->	
                                        <div class="large-12 small-12 columns">
					                        <div class="continue-step2 row spt">
						                        <div class="large-8 small-12 columns">
                                                    <asp:UpdateProgress ID="updateProgressHistory" AssociatedUpdatePanelID="upHealthHistory" runat="server">
                                                        <ProgressTemplate>
                                                            <div class="row">
                                                                <div class="large-4 small-12 columns">
                                                                    <asp:Image ID="Image2" runat="server" ImageAlign="right" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                                </div>
                                                                <div class="large-8 small-12 columns">
                                                                    <%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Message_Saving_History")%>
                                                                </div>
                                                            </div>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>

                                                    <asp:Label ID="lblMessageSaveHistory" runat="server"></asp:Label>

                                                </div>

						                        <div class="large-4 small-12 columns">
							                        <asp:Button ID="btnSaveHistory" CssClass="signin-btn gray-button full" CommandName="Refresh" OnClick="OnSave_HealthHistory" runat="server" Text="<%$Resources:Common,Text_Save %>" />
						                        </div>
					                        </div>
                                        </div>
                                        <!-- ## END Save ## -->                   
 
                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender10"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtHeightFeet"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender11"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtHeightInches"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
             
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                        </div>
                    </div>
	            </div>
            </section>
            <!-- ## END Edit Health History -->

            <!-- ## BEGIN Edit Emergency Contact -->
            <section class="learn-about info-stats full">
	            <div class="accordion">
		            <div class="toggle-btn">
			            <h4><%=GetGlobalResourceObject("Tracker","Tracker_Profile_MyEmergencyContact") %><span class="gray-arrow"></span></h4>
		            </div>
		            <div style="display: none;" class="content">
			            <div class="row">
				            <div class="large-12 small-12 columns">
					            <p></p>
				            </div>
				            <div class="large-12 small-12 columns">

                                <asp:UpdatePanel ID="upEmergencyContact" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>

                                        <asp:ValidationSummary 
                                            ID="valEmergencyContact"
                                            ValidationGroup="EmergencyContact"
                                            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
                                            Enabled="true"
                                            DisplayMode="BulletList"
                                            ShowSummary="true"
                                            CssClass="validation-error"
                                            runat="server" />

                                        <!-- ## BEGIN Name ## -->
					                    <div class="full-name row">
						                    <div class="large-6 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker","Tracker_Profile_FirstName") %></label>
							                    <asp:TextBox ID="txtFirstName_EC" runat="server" CssClass="required"></asp:TextBox>
						                    </div>
						                    <div class="large-6 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_LastName")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:TextBox ID="txtLastName_EC" runat="server" CssClass="required"></asp:TextBox>
						                    </div>
					                    </div>
                                        <!-- ## END Name ## -->

                                        <!-- ## BEGIN Address ## -->
					                    <div class="address row">
						                    <div class="large-6 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Address")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:TextBox ID="txtAddress_EC" runat="server" CssClass="required"></asp:TextBox>
						                    </div>
					                    </div>
                                        <!-- ## END Address ## -->

                                        <!-- ## BEGIN City/State/ZipCode ## -->
					                    <div class="city-state-zip row">
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_City")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:TextBox ID="txtCity_EC" runat="server" CssClass="required"></asp:TextBox>
						                    </div>
						                    <div class="large-4 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_State")%></label>
							                    <asp:DropDownList ID="ddlState_EC" runat="server" CssClass="dropdowns"></asp:DropDownList>
						                    </div>
						                    <div class="large-4 small-12 columns">
							                    <div class="row">
								                    <div class="large-6 small-12 columns">
									                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_ZipCode")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
									                    <asp:TextBox ID="txtZipCode_EC" runat="server" CssClass="required"></asp:TextBox>
								                    </div>
							                    </div>
						                    </div>
					                    </div>

                                        <div class="country row">
                                            <div class="large-6 small-12 columns">
 							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Country")%><span class="required"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span></label>
							                    <asp:DropDownList ID="ddlCountry_EC" runat="server" CssClass="dropdowns"></asp:DropDownList>                       
                                            </div>
                                        </div>
                                        <!-- ## END City/State/ZipCode ## -->

                                        <!-- ## BEGIN Phone ## -->
					                    <div class="phone-email row">
						                    <div class="large-6 small-12 columns">
                                                <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_ContactPhoneNumber")%></label>
                                                <div class="row">
							                        <div class="large-4 small-12 columns">
                                                        <asp:TextBox ID="txtPhoneContact_AreaCode_EC" runat="server" MaxLength="3"></asp:TextBox>
                                                    </div>

                                                    <div class="large-4 small-12 columns">
                                                        <asp:TextBox ID="txtPhoneContact_Prefix_EC" runat="server" MaxLength="3"></asp:TextBox>
                                                    </div>

                                                    <div class="large-4 small-12 columns">
                                                        <asp:TextBox ID="txtPhoneContact_Suffix_EC" runat="server" MaxLength="4"></asp:TextBox>
                                                    </div>
                                                </div>
						                    </div>
						                    <div class="large-6 small-12 columns">
							                    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_EmailAddress")%>*</label>
							                    <asp:TextBox ID="txtEmail_EC" runat="server" MaxLength="32"></asp:TextBox>
						                    </div>
					                    </div>
                                        <!-- ## END Phone/Mobile/Email ## -->
											
                                        <!-- ## BEGIN Save ## -->	
					                    <div class="continue-step2 row spt">
						                    <div class="large-8 small-12 columns">
                                                <asp:UpdateProgress ID="uprEmergencyContact" AssociatedUpdatePanelID="upEmergencyContact" runat="server">
                                                    <ProgressTemplate>
                                                        <div class="row">
                                                            <div class="large-4 small-12 columns">
                                                                <asp:Image ID="Image3" runat="server" ImageAlign="right" AlternateText="Animated progress gif" ImageUrl="~/images/ajax-loader.gif" />       
                                                            </div>
                                                            <div class="large-8 small-12 columns">
                                                                <%=GetGlobalResourceObject("Tracker", "Tracker_Profile_Message_Saving_EmergencyContact")%>
                                                            </div>
                                                        </div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>

                                                <asp:Label ID="lblMessageSaveEmergency" runat="server"></asp:Label>
                                            </div>

						                    <div class="large-4 small-12 columns">
							                    <asp:Button ID="btnSaveEmergency" CssClass="signin-btn gray-button full" CommandName="Refresh" OnClick="OnSave_EmergencyContact" runat="server" Text="<%$Resources:Common,Text_Save %>" />
						                    </div>
					                    </div>
                                        <!-- ## END Save ## -->	

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender7"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneContact_AreaCode_EC"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender8"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneContact_Prefix_EC"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                        <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender9"
                                            runat="server"
                                            Enabled="true"
                                            TargetControlID="txtPhoneContact_Suffix_EC"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
											
				            </div>
			            </div>
		            </div>
	            </div>
            </section>
            <!-- ## END Edit Emergency Contact -->

            <Profile:Family ID="ProfileFamily" runat="server" /> 

            <asp:Button ID="btnSave" runat="server" Text="Save" Visible="false"  />

        </ContentTemplate>
        </asp:UpdatePanel>
    </section>
    <!-- ## END Tracker Details ## -->

    <!-- ## BEGIN Tracker Summary ## -->
	<section id="side-tiles" class="small-12 large-4 columns">

        <asp:UpdatePanel ID="UpdatePanelSummaries" UpdateMode="Conditional" runat="server" >
            <ContentTemplate>

                <Module:Summary ID="SummaryConnectionCenter" ModuleIdAsInt="1" TrackerType="my-center" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryBloodPressure" ModuleIdAsInt="3" TrackerType="my-pressure" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryWeight" ModuleIdAsInt="4" TrackerType="my-weight" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryPhysical" ModuleIdAsInt="5" TrackerType="my-activity" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryBloodGlucose" ModuleIdAsInt="6" TrackerType="my-glucose" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryMyCholesterol" ModuleIdAsInt="7" TrackerType="my-cholesterol" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryMyMedications" ModuleIdAsInt="8" TrackerType="my-medications" SummaryLocation="1" runat="server" />

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnPpeSummaryOk" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <Module:Summary ID="SummaryMyLifeCheck" ModuleIdAsInt="9" TrackerType="my-lifecheck" SummaryLocation="1" runat="server" />  
              
    </section>    
    <!-- ## END Tracker Summary ## -->


    <!-- ### BEGIN Popup Stuff ### -->
    <input id="btnPpeSummaryProxyOk" type="button" value="Done" onclick="document.getElementById('<%= btnPpeSummaryOk.ClientID %>').click(); return false; " style="display: none" />
    <input id="btnPpeTrackerProxyOk" type="button" value="Done" onclick="document.getElementById('<%= btnPpeTrackerOk.ClientID %>').click(); return false; " style="display: none" />

    <asp:UpdatePanel ID="UpdatePanelPopups" UpdateMode="Conditional" runat="server" >
    <ContentTemplate>
        <!-- ### BEGIN Summary Popup ### -->
        <div style="display: none;" >
            <div id="popupSummaryTrigger" runat="server" class="add-this" data-name="cpopupsummary" />
        </div>

        <div class="shadowbox cpopupsummary"></div>
        <div id="pnlPopupSummaryContent" style="display: none" runat="server" class="modal-bg cpopupsummary" >
            <div id="divPopupSummaryStyle" runat="server" class="modal-wrap cpopupsummary page-blue" >
                <div class="inner-modal" >
                    <a class="close-modal" onclick="return window.close_modal();">CLOSE</a>
                    <!-- src and class attributes are added procedurally -->
                    <iframe id="popupSummaryIframe" runat="server" frameborder="0" height="100%" width="100%"></iframe>
                    <asp:Button ID="btnPpeSummaryOk" CssClass="close-modal" runat="server" OnClick="btnPpeSummaryOk_Click" style="display: none" />
                    <input id="btnPpeSummaryCancel" type="button" class="close-modal" value="Cancel" style="display: none" onclick="return window.close_modal();"/>
                </div>
            </div>
        </div>
        <!-- ### END Summary Popup ### -->

        <!-- ### BEGIN Tracker Popup ### -->
        <div style="display: none;" >
            <div id="popupTrackerTrigger" runat="server" class="add-this" data-name="cpopuptracker" />
        </div>

        <div class="shadowbox cpopuptracker"></div>
        <div ID="pnlPopupTrackerContent" style="display: none" runat="server" class="modal-bg cpopuptracker" >
            <div id="divPopupTrackerStyle" runat="server" class="modal-wrap cpopuptracker page-blue" >
	            <div class="inner-modal" >
                    <a class="close-modal" onclick="return window.close_modal();">CLOSE</a>
                    <!-- src and class attributes are added procedurally -->
                    <iframe id="popupTrackerIframe" runat="server" frameborder="0" height="100%" width="100%"></iframe>
                    <!-- if the popup changes data, it will click this hidden ok button, which needs to refresh the page -->
                    <asp:Button id="btnPpeTrackerOk" CssClass="close-modal" runat="server" OnClick="btnPpeTrackerOk_Click" style="display: none" />
                    <input id="btnPpeTrackerCancel" type="button" class="close-modal" value="Cancel" style="display: none" onclick="return window.close_modal();"/>
                </div>                     
            </div>
        </div>
        <!-- ### END Tracker Popup ### -->  
    </ContentTemplate>
    </asp:UpdatePanel>
    <!-- ### END Popup Stuff ### -->



    <%--My Profile Validation--%>

    <asp:CustomValidator
        ID="validFirstName"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="MyProfile"
        Display="None"
        OnServerValidate="ValidateFirstName">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validLastName"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="MyProfile"
        Display="None"
        OnServerValidate="ValidateLastName">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validZipCode"
        runat="server"
        Display="None"
        EnableClientScript="false"
        ValidationGroup="MyProfile"
        OnServerValidate="ValidateZipCodeAndCity">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validCountry"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="MyProfile"
        Display="None"
        OnServerValidate="ValidateCountry">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validPhoneContact"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="MyProfile"
        Display="None"
        OnServerValidate="ValidatePhoneContact">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validPhoneMobile"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="MyProfile"
        Display="None"
        OnServerValidate="ValidatePhoneMobile">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validEmail"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="MyProfile"
        Display="None"
        OnServerValidate="ValidateEmail">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validGender"
        runat="server"
        EnableClientScript="false"
        OnServerValidate="ValidateGender"
        ValidationGroup="MyProfile"
        Display="None">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validYearOfBirth"
        runat="server"
        EnableClientScript="false"
        OnServerValidate="ValidateYearOfBirth"
        ValidationGroup="MyProfile"
        Display="None">
    </asp:CustomValidator>


    <%--My Emergency Contact Validation--%>

    <asp:CustomValidator
        ID="validLastName_EC"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="EmergencyContact"
        Display="None"
        OnServerValidate="ValidateLastName_EC">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validAddress_EC"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="EmergencyContact"
        Display="None"
        OnServerValidate="ValidateAddress_EC">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validCity_EC"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="EmergencyContact"
        Display="None"
        OnServerValidate="ValidateCity_EC">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validZipCode_EC"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="EmergencyContact"
        Display="None"
        OnServerValidate="ValidateZipCode_EC">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validCountry_EC"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="EmergencyContact"
        Display="None"
        OnServerValidate="ValidateCountry_EC">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validContactPhone_EC"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="EmergencyContact"
        Display="None"
        OnServerValidate="ValidateContactPhone_EC">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validEmail_EC"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="EmergencyContact"
        Display="None"
        OnServerValidate="ValidateEmail_EC">
    </asp:CustomValidator>

    <%--My Health History Validation--%>
		  
    <asp:CustomValidator
        ValidationGroup="HealthHistory"
        ID="validHeight"
        runat="server" SetFocusOnError="true"
        EnableClientScript="false"
        OnServerValidate="validateHeight"
        Display="None">
    </asp:CustomValidator>
            
    <asp:CustomValidator
        ValidationGroup="HealthHistory"
        ID="validAllergies"
        runat="server"
        Display="None"
        EnableClientScript="false"
        OnServerValidate="validateAllergies">
    </asp:CustomValidator>   

</asp:Content>