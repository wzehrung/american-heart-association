﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web.Pages.Patient
{
    public partial class PhysicalActivity : H360PageHelper
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            // Have the module controller be shared between the summary and detail components for efficientcy
            Heart360Web.PhysicalActivityModuleController pamc = new PhysicalActivityModuleController();
            SummaryPhysicalActivity.ModuleController = pamc;
            DetailsPhysicalActivity.ModuleController = pamc;

            // We need to wire up some update panel asynchronous triggers
            DetailsPhysicalActivity.RefreshDataCallback = OnRefreshDataCompleted;
        }

        public void OnRefreshDataCompleted()
        {
            this.UpdatePanelTrackerSummary.Update();
            this.UpdatePanelSummaries.Update();
        }

        protected void btnPpeSummaryOk_Click(object sender, EventArgs e)
        {
            // There is already an asynchronous postback trigger for this button to refresh UpdatePanelSummaries.
            // That is the only update panel that needs refreshing.

        }

        protected void btnPpeTrackerOk_Click(object sender, EventArgs e)
        {
            this.UpdatePanelTrackerSummary.Update();
            this.DetailsPhysicalActivity.MyHistoryUpdatePanel.Update();
            this.DetailsPhysicalActivity.MyActionsUpdatePanel.Update();
            this.DetailsPhysicalActivity.MyLearnMoreUpdatePanel.Update();
        }
    }
}