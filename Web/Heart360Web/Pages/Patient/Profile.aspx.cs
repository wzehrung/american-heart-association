﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using AHACommon;
using Microsoft.Health.ItemTypes;
using Microsoft.Health.Web;

namespace Heart360Web.Pages.Patient
{
    public partial class Profile : H360PageHelper, IPeriodicDataManager
    {
        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }

        public int LastXYears = 100;
        public int StartYear = 1960;
        public int EndYear = DateTime.Now.Year;

        public string PhoneContact
        {
            get
            {
                string retval = txtPhoneContact_AreaCode.Text + txtPhoneContact_Prefix.Text + txtPhoneContact_Suffix.Text;
                return retval;
            }
        }

        public string PhoneMobile
        {
            get
            {
                string retval = txtPhoneMobile_AreaCode.Text + txtPhoneMobile_Prefix.Text + txtPhoneMobile_Suffix.Text;
                return retval;
            }    
        }

        public string PhoneContact_EC
        {
            get
            {
                string retval = txtPhoneContact_AreaCode_EC.Text + txtPhoneContact_Prefix_EC.Text + txtPhoneContact_Suffix_EC.Text;
                return retval;
            }
        }

        private void _PopulateControls()
        {
            _LoadNameSuffixDDL();
            _LoadNameTitleDDL();
            _LoadStateDDL();
            _LoadCountryDDL();
            _LoadGenderRadList();
            _LoadEthnicityDDL();
            _LoadYear();
            _LoadBloodTypeDDL();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _PopulateControls();
            // Set the OnClientClick event on each of the save buttons in each control
            //Button btnProfile = ProfileEdit.FindControl("btnSaveProfile") as Button;
            //btnProfile.OnClientClick = "document.getElementById('" + btnSave.ClientID + "').click();";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ProfileFamily.PersonInfo = this.PersonInfo;
            ProfileFamily.ActiveRecordChanged += new Heart360Web.Controls.Patient.Profile.ProfileFamilyMembers.ActiveRecordChangedHandler(ProfileFamily_ActiveRecordChanged);

            this.CurrentPatient = AHAHelpContent.Patient.FindByUserHealthRecordGUID((Page as H360PageHelper).RecordGUID);

            if (!Page.IsPostBack)
            {
                GetData_Profile();
                GetData_HealthHistory();
                GetData_EmergencyContact();
            }
        }

        #region POPUPS
        protected void btnPpeSummaryOk_Click(object sender, EventArgs e)
        {
            // There is already an asynchronous postback trigger for this button to refresh UpdatePanelSummaries.
            // That is the only update panel that needs refreshing.

        }

        protected void btnPpeTrackerOk_Click(object sender, EventArgs e)
        {
            // Nothing to do here, everything hooked up via asynchronous postback triggers.
            //this.UpdatePanelTrackerSummary.Update();
            //this.DetailsWeight.MyHistoryUpdatePanel.Update();
        }
        #endregion

        #region MY PROFILE

        void ProfileFamily_ActiveRecordChanged(object sender, Heart360Web.Controls.Patient.Profile.ProfileFamilyMembers.ActiveRecordChangedEventArgs e)
        {
            this.SetSelectedRecord(this.PersonInfo.AuthorizedRecords[e.ActiveRecordId]);
            AHAHelpContent.AHAUser.ChangeSelectedRecord(SessionInfo.GetUserIdentityContext().UserId, e.ActiveRecordId);

            if (!string.IsNullOrEmpty(Request["invitationid"]))
            {
                Response.Redirect(string.Format("{0}?invitationid={1}&changerecord=1", AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_INVITATION_ACCEPT, Request["invitationid"]));
                Response.End();
            }

            Response.Redirect(GRCBase.UrlUtility.RelativeCurrentPageUrl);
        }

        #region GetData_Profile
        private void GetData_Profile()
        {
            HVManager.PersonalDataManager.PersonalItem objPersonalItem = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
            HVManager.BasicDataManager.BasicItem objBasicItem = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
            HVManager.PersonalContactDataManager.ContactItem objContactItem = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;
            HVManager.ExtendedProfileDataManager.ExtendedProfileItem objExtendedItem = HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item;
            HVManager.CurrentUserHVSetting objHVSetting = HVManager.CurrentUserHVSetting.GetCurrentUserSetting((this.Page as HealthServicePage).AuthenticatedConnection);

            if (objPersonalItem == null)
            {
                Guid guidPersonal = HVHelper.GetPersonalItemGUID(true);
                if (guidPersonal != Guid.Empty)
                {
                    objPersonalItem = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
                }
            }

            if (objPersonalItem != null)
            {
                txtFirstName.Text = objPersonalItem.FirstName.Value;
                txtLastName.Text = objPersonalItem.LastName.Value;

                if (!String.IsNullOrEmpty(objPersonalItem.NameSuffix.Value))
                {
                    ddlSuffix.SelectedValue = (objPersonalItem.NameSuffix.Value);
                }

                if (!String.IsNullOrEmpty(objPersonalItem.Title.Value))
                {
                    ddlTitle.SelectedItem.Text = (objPersonalItem.Title.Value).ToString();
                }

                if (!String.IsNullOrEmpty(objPersonalItem.Ethnicity.Value))
                {
                    ddlEthnicity.SelectedItem.Text = (objPersonalItem.Ethnicity.Value);
                }
            }

            // ADDRESS
            if (CurrentUser.Address != null)
            {
                if (CurrentUser.Address.StreetList.Count > 0)
                {
                    txtAddress.Text = _getDataForDisplay(CurrentUser.Address.StreetList[0].Value);
                }

                if (!String.IsNullOrEmpty(CurrentUser.Address.City.Value))
                {
                    txtCity.Text = _getDataForDisplay(CurrentUser.Address.City.Value);
                }

                if (!String.IsNullOrEmpty(CurrentUser.Address.State.Value))
                {
                    ddlState.SelectedItem.Text = (CurrentUser.Address.State.Value);
                }

                if (!String.IsNullOrEmpty(CurrentUser.Address.Country.Value))
                {
                    ddlCountry.SelectedValue = (CurrentUser.Address.Country.Value);
                }
            }
            else
            {
                ddlCountry.SelectedValue = "United States";
            }


            // GENDER and ZIPCODE
            if (objBasicItem != null)
            {
                if (objBasicItem.GenderOfPerson.Value.HasValue)
                {
                    ddlGender.SelectedValue = (objBasicItem.GenderOfPerson.Value.Value.ToString());
                }

                if (!String.IsNullOrEmpty(objBasicItem.PostalCode.Value))
                {
                    txtZipCode.Text = objBasicItem.PostalCode.Value;
                }

                ddlYearOfBirth.SelectedValue = objBasicItem.YearOfBirth.Value.ToString();
            }

            // EMAIL
            if (objContactItem != null)
            {
                if (objContactItem.EmailList.Count > 0)
                {
                    txtEmail.Text = objContactItem.EmailList[0].Email.Value;
                }

                string sHiddenPhone = objContactItem.Phone.Value;
            }

            // PHONE NUMBERS
            AHAHelpContent.PhoneNumbers objPhoneNumbers = AHAHelpContent.PhoneNumbers.GetPhoneNumbers(CurrentPatient.UserHealthRecordID);

            string sPhoneContact = string.Empty;
            string sPhoneMobile = string.Empty;

            if (objPhoneNumbers != null)
            {
                if (!String.IsNullOrEmpty(objPhoneNumbers.PhoneContact))
                {
                    sPhoneContact = objPhoneNumbers.PhoneContact;
                    txtPhoneContact_AreaCode.Text = sPhoneContact.Substring(0, 3);
                    txtPhoneContact_Prefix.Text = sPhoneContact.Substring(3, 3);
                    txtPhoneContact_Suffix.Text = sPhoneContact.Substring(6, 4);
                }

                if (!String.IsNullOrEmpty(objPhoneNumbers.PhoneMobile))
                {
                    sPhoneMobile = objPhoneNumbers.PhoneMobile;
                    txtPhoneMobile_AreaCode.Text = sPhoneMobile.Substring(0, 3);
                    txtPhoneMobile_Prefix.Text = sPhoneMobile.Substring(3, 3);
                    txtPhoneMobile_Suffix.Text = sPhoneMobile.Substring(6, 4);
                }
            }
        }

        #endregion

        public bool UpdateProfile(
            HVManager.PersonalDataManager.PersonalItem objPersonalItem,
            ref HVManager.BasicDataManager.BasicItem objBasicItem,
            ref HVManager.PersonalContactDataManager.ContactItem objContactItem)
        {
            Page.Validate("MyProfile");
            if (!Page.IsValid)
            {
                //UIHelper.ScrollToTopOfThePageAfterAjaxPostback();
                return false;
            }

            SetData_Profile(objPersonalItem, ref objBasicItem, ref objContactItem);

            AHAHelpContent.AHAUser.UpdateUserIsHealthHistoryCompleted(AHAAPI.SessionInfo.GetUserIdentityContext().UserId, true);

            return true;
        }

        private void SetData_Profile(
            HVManager.PersonalDataManager.PersonalItem objPersonalItem,
            ref HVManager.BasicDataManager.BasicItem objBasicItem,
            ref HVManager.PersonalContactDataManager.ContactItem objContactItem)
        {
            #region Personal Items
            if (objPersonalItem != null)
            {
                if (AHAAPI.H360Utility.IsUseNewValue(objPersonalItem.FirstName.Value, txtFirstName.Text.Trim()))
                {
                    objPersonalItem.FirstName.Value = txtFirstName.Text.Trim();
                }

                if (AHAAPI.H360Utility.IsUseNewValue(objPersonalItem.LastName.Value, txtLastName.Text.Trim()))
                {
                    objPersonalItem.LastName.Value = txtLastName.Text.Trim();
                }

                if (AHAAPI.H360Utility.IsUseNewValue(objPersonalItem.NameSuffix.Value, ddlSuffix.SelectedItem.Text.Trim()))
                {
                    objPersonalItem.NameSuffix.Value = ddlSuffix.SelectedValue.Trim();
                }

                if (AHAAPI.H360Utility.IsUseNewValue(objPersonalItem.Title.Value, ddlTitle.SelectedItem.Text.Trim()))
                {
                    objPersonalItem.Title.Value = ddlTitle.SelectedItem.Text.Trim();
                }

                //if (AHAAPI.H360Utility.IsUseNewValue(obj.DateOfBirth.Value, dtDoB.GetDate()))
                //{
                //    obj.DateOfBirth.Value = dtDoB.GetDate();
                //}

                if (AHAAPI.H360Utility.IsUseNewValue(objPersonalItem.Ethnicity.Value, ddlEthnicity.SelectedItem.Text.Trim()))
                {
                    objPersonalItem.Ethnicity.Value = ddlEthnicity.SelectedItem.Text.Trim();
                }
            }
            else
            {
                objPersonalItem = new HVManager.PersonalDataManager.PersonalItem();

                objPersonalItem.FirstName.Value = txtFirstName.Text.Trim();
                objPersonalItem.LastName.Value = txtLastName.Text.Trim();
                objPersonalItem.NameSuffix.Value = ddlSuffix.SelectedItem.Text.Trim();
                objPersonalItem.Title.Value = ddlTitle.SelectedValue.Trim();
                objPersonalItem.Ethnicity.Value = ddlEthnicity.SelectedItem.Text.Trim();
            }
            #endregion

            #region Basic Items

            Gender gender = Gender.Unknown;

            if (Enum.IsDefined(typeof(Gender), ddlGender.SelectedValue))
                gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.SelectedValue);

            if (objBasicItem != null)
            {
                if ((!objBasicItem.GenderOfPerson.Value.HasValue && gender != Gender.Unknown)
                ||
                (objBasicItem.GenderOfPerson.Value.HasValue && objBasicItem.GenderOfPerson.Value.Value != gender))
                {
                    objBasicItem.GenderOfPerson.Value = gender;
                }

                if (AHAAPI.H360Utility.IsUseNewValue(objBasicItem.YearOfBirth.Value.ToString(), ddlYearOfBirth.SelectedValue.Trim()))
                {
                    objBasicItem.YearOfBirth.Value = Convert.ToInt32(ddlYearOfBirth.SelectedValue.Trim());
                }

                if (AHAAPI.H360Utility.IsUseNewValue(objBasicItem.PostalCode.Value, txtZipCode.Text))
                {
                    objBasicItem.PostalCode.Value = txtZipCode.Text;
                }
            }
            else
            {
                objBasicItem = new HVManager.BasicDataManager.BasicItem();
                objBasicItem.GenderOfPerson.Value = gender;
                objBasicItem.YearOfBirth.Value = Convert.ToInt32(ddlYearOfBirth.SelectedValue);
                objBasicItem.PostalCode.Value = txtZipCode.Text;
            }

            #endregion

            #region Personal Contact Items

            if (objContactItem != null)
            {
                if (CurrentUser.Address != null)
                {
                    string strStreet = string.Empty;
                    if (CurrentUser.Address.StreetList.Count > 0)
                    {
                        strStreet = CurrentUser.Address.StreetList[0].Value;
                    }
                    if (AHAAPI.H360Utility.IsUseNewValue(strStreet, txtAddress.Text.Trim()))
                    {
                        if (String.IsNullOrEmpty(txtAddress.Text))
                        {
                            objContactItem.Street.Value = _getDataForUpdate(txtAddress.Text);
                        }
                        else
                        {
                            objContactItem.Street.Value = txtAddress.Text.Trim();
                        }
                    }

                    if (AHAAPI.H360Utility.IsUseNewValue(CurrentUser.Address.City.Value, txtCity.Text.Trim()))
                    {
                        if (String.IsNullOrEmpty(txtCity.Text))
                        {
                            objContactItem.City.Value = _getDataForUpdate(txtCity.Text);
                        }
                        else
                        {
                            objContactItem.City.Value = txtCity.Text.Trim();
                        }
                    }

                    if (AHAAPI.H360Utility.IsUseNewValue(CurrentUser.Address.State.Value, ddlState.SelectedItem.Text.Trim()))
                    {
                        objContactItem.State.Value = ddlState.SelectedItem.Text.Trim();
                    }

                    if (AHAAPI.H360Utility.IsUseNewValue(CurrentUser.Address.Zip.Value, txtZipCode.Text.Trim()))
                    {
                        objContactItem.Zip.Value = txtZipCode.Text.Trim();
                    }

                    if (AHAAPI.H360Utility.IsUseNewValue(CurrentUser.Address.Country.Value, ddlCountry.SelectedItem.Text.Trim()))
                    {
                        objContactItem.Country.Value = ddlCountry.SelectedItem.Text.Trim();
                    }

                    if (AHAAPI.H360Utility.IsUseNewValue(CurrentUser.Email, txtEmail.Text.Trim()))
                    {
                        objContactItem.Email.Value = txtEmail.Text.Trim();
                    }

                    objContactItem.Phone.Value = "-";

                    // NOTE: Wade and Phil removed phone number
                }
                else
                {
                    objContactItem.Street.Value = _getDataForUpdate(txtAddress.Text.Trim());
                    objContactItem.City.Value = _getDataForUpdate(txtCity.Text.Trim());
                    objContactItem.State.Value = ddlState.SelectedItem.Text.Trim();
                    objContactItem.Zip.Value = txtZipCode.Text.Trim();
                    objContactItem.Country.Value = ddlCountry.SelectedItem.Text.Trim();
                    objContactItem.Email.Value = txtEmail.Text.Trim();

                    objContactItem.Phone.Value = "-";
                }
            }
            else
            {
                objContactItem = new HVManager.PersonalContactDataManager.ContactItem();

                objContactItem.Street.Value = _getDataForUpdate(txtAddress.Text.Trim());
                objContactItem.City.Value = _getDataForUpdate(txtCity.Text.Trim());
                objContactItem.State.Value = ddlState.SelectedItem.Text.Trim();
                objContactItem.Zip.Value = txtZipCode.Text.Trim();
                objContactItem.Country.Value = ddlCountry.SelectedItem.Text.Trim();
                objContactItem.Email.Value = txtEmail.Text.Trim();

                objContactItem.Phone.Value = "-";
            }

            #endregion
        }
       
        #region Save Profile
        protected void OnSave_Profile(object sender, EventArgs e)
        {
            Page.Validate("MyProfile");
            if (!Page.IsValid)
            {
                return;
            }

            try
            {
                HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;

                HVManager.PersonalDataManager.PersonalItem objPersonalItem = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
                HVManager.BasicDataManager.BasicItem objBasicItem = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
                HVManager.PersonalContactDataManager.ContactItem objContactItem = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;

                if (UpdateProfile(objPersonalItem, ref objBasicItem, ref objContactItem))
                {
                    Dictionary<string, object> list = new Dictionary<string, object>();

                    list.Add(typeof(HVManager.PersonalDataManager).ToString(), objPersonalItem);
                    list.Add(typeof(HVManager.BasicDataManager).ToString(), objBasicItem);
                    list.Add(typeof(HVManager.PersonalContactDataManager).ToString(), objContactItem);

                    mgr.CreateOrUpdateSingletons(list);

                    string zipCode = "0";
                    int birthYear = 0;

                    if (objBasicItem != null)
                    {
                        if (objBasicItem.PostalCode != null)
                        {
                            zipCode = objBasicItem.PostalCode.Value;
                        }

                        if (objBasicItem.YearOfBirth != null)
                        {
                            birthYear = objBasicItem.YearOfBirth.Value;
                        }
                    }

                    // Setting the country codes to 1 for now until we "internationalize"
                    int iContactCountryCodeID = 1;
                    int iMobileCountryCodeID = 1;

                    string sCountry = ddlCountry.SelectedItem.Text;
                    string sGender = ddlGender.SelectedItem.Text;
                    string sEthnicity = ddlEthnicity.SelectedItem.Text;
                    int iBirthYear = Convert.ToInt32(ddlYearOfBirth.SelectedValue);
                    string sZipCode = txtZipCode.Text;

                        //
                        // Step 1: If the user changed mobile phone number, then we have to cleanup SMS
                        //         This step will only perform any required deletions
                        //

                        // get the current phone numbers
                    AHAHelpContent.PhoneNumbers objPhoneNumbers = AHAHelpContent.PhoneNumbers.GetPhoneNumbers(CurrentPatient.UserHealthRecordID);

                        // determine if an existing mobile phone number was deleted or changed.
                    if (!String.IsNullOrEmpty(PhoneMobile)) // User defined a mobile phone number
                    {
                        if (!string.IsNullOrEmpty(objPhoneNumbers.PhoneMobile)) // there is a previous mobile phone number
                        {
                            if (!objPhoneNumbers.PhoneMobile.Equals(PhoneMobile))   // the new number and the existing are NOT the same
                            {
                                // this will delete the phone number
                                AHAHelpContent.PhoneNumbers.CreateOrChangeSMSNumber(CurrentPatient.UserHealthRecordID, Convert.ToString(iMobileCountryCodeID), PhoneMobile, "0", string.Empty, "true");
                            }
                        }
                    }
                    else
                    {
                        // If we are here, there is NOT a mobile phone number defined by the user in the UI
                        if (!string.IsNullOrEmpty(objPhoneNumbers.PhoneSMS))   // guess what, there is a previously defined number we now have to delete
                            AHAHelpContent.PhoneNumbers.CreateOrChangeSMSNumber(CurrentPatient.UserHealthRecordID, Convert.ToString(iMobileCountryCodeID), PhoneMobile, "0", string.Empty, "true");
                    }
                        //
                        // Step 2: Now we can save the new stuff
                        //

                        // If the contact number is blank, delete an IVR schedule if there is one.
                    if (String.IsNullOrEmpty(PhoneContact))
                    {
                        AHAHelpContent.IVRReminders.DeleteIVRSchedule(CurrentPatient.UserHealthRecordID);
                    }

                    AHAHelpContent.ExtendedProfile.CreateOrUpdateMyProfile(CurrentPatient.UserHealthRecordID, sCountry, sGender, sEthnicity, iBirthYear, sZipCode, iContactCountryCodeID, PhoneContact, iMobileCountryCodeID, PhoneMobile);
                     
                    CommandEventArgs args = new CommandEventArgs("Refresh", String.Empty);
                    RaiseBubbleEvent(null, args);

                    return;
                }
            }
            catch(Exception ex)
            {
                lblMessageSaveProfile.Text = ex.ToString();
            }
            finally
            {
                
            }
            
        }
        #endregion

        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            CommandEventArgs e = (CommandEventArgs)args;
            if (e.CommandName == "Refresh")
            {
                //Update LastModificationDate on the Options Tab.             
                SummaryProfile._RefreshProfileData();
            }
            return true;
        }

        #endregion

        #region MY HEALTH HISTORY

        private void GetData_HealthHistory()
        {
            HVManager.PersonalDataManager.PersonalItem personalItem = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
            HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem = HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item;
            HVManager.CardiacDataManager.CardiacItem cardiacItem = HVHelper.HVManagerPatientBase.CardiacDataManager.Item;
            HVManager.HeightDataManager.HeightItem heightItem = HeightWrapper.GetLatestHeightItem();

            if (heightItem != null)
            {
                txtHeightFeet.Text = heightItem.HeightInFeet.ToString();
                txtHeightInches.Text = heightItem.HeightInInches.ToString();
            }

            if (epItem != null)
            {
                txtAllergies.Text = epItem.Allergies.Value;

                if (epItem.HasTriedToQuitSmoking.Value.HasValue)
                {
                    if (epItem.HasTriedToQuitSmoking.Value.Value == true)
                        radSmokeQuitYes.Checked = true;
                    else
                        radSmokeQuitNo.Checked = true;
                }

                if (epItem.IsSmokeCigarette.Value.HasValue)
                {
                    if (epItem.IsSmokeCigarette.Value.Value == true)
                        radSmokeYes.Checked = true;
                    else
                        radSmokeNo.Checked = true;
                }
            }

            if (cardiacItem != null)
            {
                if (cardiacItem.IsOnHypertensionMedication.Value.HasValue)
                {
                    if (cardiacItem.IsOnHypertensionMedication.Value.Value == true)
                        radBpMedicationYes.Checked = true;
                    else
                        radBpMedicationNo.Checked = true;
                }


                if (cardiacItem.HasRenalFailureBeenDiagnosed.Value.HasValue)
                {
                    if (cardiacItem.HasRenalFailureBeenDiagnosed.Value.Value == true)
                        radKidneyDiseaseYes.Checked = true;
                    else
                        radKidneyDiseaseNo.Checked = true;
                }

                if (cardiacItem.HasDiabetesBeenDiagnosed.Value.HasValue)
                {
                    if (cardiacItem.HasDiabetesBeenDiagnosed.Value.Value == true)
                        radDiabetesYes.Checked = true;
                    else
                        radDiabetesNo.Checked = true;
                }

                if (cardiacItem.HasPersonalHeartDiseaseHistory.Value.HasValue)
                {
                    if (cardiacItem.HasPersonalHeartDiseaseHistory.Value.Value == true)
                        radHeartDiseaseYes.Checked = true;
                    else
                        radHeartDiseaseNo.Checked = true;
                }

                if (cardiacItem.IsOnHypertensionDiet.Value.HasValue)
                {
                    if (cardiacItem.IsOnHypertensionDiet.Value.Value == true)
                        radHyperTensionDietYes.Checked = true;
                    else
                        radHyperTensionDietNo.Checked = true;
                }

                if (cardiacItem.HasFamilyHeartDiseaseHistory.Value.HasValue)
                {
                    if (cardiacItem.HasFamilyHeartDiseaseHistory.Value.Value == true)
                        radFHHeartYes.Checked = true;
                    else
                        radFHHeartNo.Checked = true;
                }

                if (cardiacItem.HasFamilyStrokeHistory.Value.HasValue)
                {
                    if (cardiacItem.HasFamilyStrokeHistory.Value.Value == true)
                        radFHStrokeYes.Checked = true;
                    else
                        radFHStrokeNo.Checked = true;
                }
            }

            if (personalItem != null)
            {
                if (!String.IsNullOrEmpty(personalItem.BloodType.Value))
                {
                    ddlBloodType.SelectedValue = (personalItem.BloodType.Value);
                }
            }
        }

        public void SetData_HealthHistory(HVManager.PersonalDataManager.PersonalItem personalItem,
                    HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem,
                    ref HVManager.CardiacDataManager.CardiacItem cardiacItem,
                    ref HVManager.HeightDataManager.HeightItem heightItem,
                    HVManager.BasicDataManager.BasicItem basicItem)
        {

            #region Personal Item

            if (personalItem != null)
            {
                if (AHAAPI.H360Utility.IsUseNewValue(personalItem.BloodType.Value, ddlBloodType.SelectedValue.Trim()))
                {
                    personalItem.BloodType.Value = ddlBloodType.SelectedValue.Trim();
                }
            }
            else
            {
                personalItem = new HVManager.PersonalDataManager.PersonalItem();

                personalItem.BloodType.Value = ddlBloodType.SelectedValue.Trim();
            }

            #endregion

            #region Height Specific

            bool bHeightChanged = false;
            double dFeetToMeter = 0;
            double dInchesToMeter = 0;

            if (txtHeightFeet.Text != string.Empty)
            {
                dFeetToMeter = HVManager.UnitConversion.FeetToMeters(Convert.ToDouble(txtHeightFeet.Text.Trim()));
            }

            if (txtHeightInches.Text != string.Empty)
            {
                dInchesToMeter = HVManager.UnitConversion.InchesToMeters(Convert.ToDouble(txtHeightInches.Text.Trim()));
            }

            double dMeters = dFeetToMeter + dInchesToMeter;

            if (heightItem != null)
            {
                if ((dMeters != 0) && AHAAPI.H360Utility.IsUseNewValue(heightItem.HeightInMeters.Value, dMeters))
                {
                    bHeightChanged = true;
                }
            }
            else
            {
                if (dMeters != 0)
                    bHeightChanged = true;
            }

            if (bHeightChanged)
            {
                heightItem = new HVManager.HeightDataManager.HeightItem();

                heightItem.When.Value = DateTime.Now;
                heightItem.HeightInMeters.Value = dMeters;
                heightItem.Source.Value = HVManager.BrandLogoHelperMachine.GetApplicationBrandInfo(Page);
            }

            #endregion

            #region Extended Properties

            bool radTriedToQuitSmokingBoolValue = false;

            if (radSmokeQuitYes.Checked || radSmokeQuitNo.Checked)
            {
                if (radSmokeQuitYes.Checked)
                    radTriedToQuitSmokingBoolValue = true;
            }

            bool radSmokeCigarettesBoolValue = false;

            if (radSmokeYes.Checked || radSmokeNo.Checked)
            {
                if (radSmokeYes.Checked)
                    radSmokeCigarettesBoolValue = true;
            }

            if (epItem != null)
            {
                if (AHAAPI.H360Utility.IsUseNewValue(epItem.Allergies.Value, txtAllergies.Text.Trim()))
                {
                    epItem.Allergies.Value = txtAllergies.Text.Trim();
                }

                if (radSmokeQuitYes.Checked || radSmokeQuitNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(epItem.HasTriedToQuitSmoking.Value, radTriedToQuitSmokingBoolValue))
                    {
                        epItem.HasTriedToQuitSmoking.Value = radTriedToQuitSmokingBoolValue;
                    }
                }

                if (radSmokeYes.Checked || radSmokeNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(epItem.IsSmokeCigarette.Value, radSmokeCigarettesBoolValue))
                    {
                        epItem.IsSmokeCigarette.Value = radSmokeCigarettesBoolValue;
                    }
                }
            }
            else
            {
                epItem = new HVManager.ExtendedProfileDataManager.ExtendedProfileItem();

                epItem.Allergies.Value = txtAllergies.Text.Trim();

                if (radSmokeQuitYes.Checked || radSmokeQuitNo.Checked)
                {
                    epItem.HasTriedToQuitSmoking.Value = radTriedToQuitSmokingBoolValue;
                }

                if (radSmokeYes.Checked || radSmokeNo.Checked)
                {
                    epItem.IsSmokeCigarette.Value = radSmokeCigarettesBoolValue;
                }
            }

            #endregion

            #region Cardiac

            bool radHighBPMedicationBoolValue = false;

            if (radBpMedicationYes.Checked || radBpMedicationNo.Checked)
            {
                if (radBpMedicationYes.Checked)
                    radHighBPMedicationBoolValue = true;
            }


            bool radHaveKidneyDiseaseBoolValue = false;

            if (radKidneyDiseaseYes.Checked || radKidneyDiseaseNo.Checked)
            {
                if (radKidneyDiseaseYes.Checked)
                    radHaveKidneyDiseaseBoolValue = true;
            }

            bool radHaveDiabetesBoolValue = false;

            if (radDiabetesYes.Checked || radDiabetesNo.Checked)
            {
                if (radDiabetesYes.Checked)
                    radHaveDiabetesBoolValue = true;
            }

            bool radDiagnosedHeartDiseaseBoolValue = false;

            if (radHeartDiseaseYes.Checked || radHeartDiseaseNo.Checked)
            {
                if (radHeartDiseaseYes.Checked)
                    radDiagnosedHeartDiseaseBoolValue = true;
            }

            bool radOnHypertensionDietBoolValue = false;

            if (radHyperTensionDietYes.Checked || radHyperTensionDietNo.Checked)
            {
                if (radHyperTensionDietYes.Checked)
                    radOnHypertensionDietBoolValue = true;
            }

            bool radHasFamilyHistoryOfHeartDiseaseBoolValue = false;

            if (radFHHeartYes.Checked || radFHHeartNo.Checked)
            {
                if (radFHHeartYes.Checked)
                    radHasFamilyHistoryOfHeartDiseaseBoolValue = true;
            }

            bool radHasFamilyHistoryOfStrokeBoolValue = false;

            if (radFHStrokeYes.Checked || radFHStrokeNo.Checked)
            {
                if (radFHStrokeYes.Checked)
                    radHasFamilyHistoryOfStrokeBoolValue = true;
            }

            if (cardiacItem != null)
            {
                if (radBpMedicationYes.Checked || radBpMedicationNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(cardiacItem.IsOnHypertensionMedication.Value, radHighBPMedicationBoolValue))
                    {
                        cardiacItem.IsOnHypertensionMedication.Value = radHighBPMedicationBoolValue;
                    }
                }

                if (radKidneyDiseaseYes.Checked || radKidneyDiseaseNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(cardiacItem.HasRenalFailureBeenDiagnosed.Value, radHaveKidneyDiseaseBoolValue))
                    {
                        cardiacItem.HasRenalFailureBeenDiagnosed.Value = radHaveKidneyDiseaseBoolValue;
                    }
                }

                if (radDiabetesYes.Checked || radDiabetesNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(cardiacItem.HasDiabetesBeenDiagnosed.Value, radHaveDiabetesBoolValue))
                    {
                        cardiacItem.HasDiabetesBeenDiagnosed.Value = radHaveDiabetesBoolValue;
                    }
                }

                if (radHeartDiseaseYes.Checked || radHeartDiseaseNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(cardiacItem.HasPersonalHeartDiseaseHistory.Value, radDiagnosedHeartDiseaseBoolValue))
                    {
                        cardiacItem.HasPersonalHeartDiseaseHistory.Value = radDiagnosedHeartDiseaseBoolValue;
                    }
                }

                if (radHyperTensionDietYes.Checked || radHyperTensionDietNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(cardiacItem.IsOnHypertensionDiet.Value, radOnHypertensionDietBoolValue))
                    {
                        cardiacItem.IsOnHypertensionDiet.Value = radOnHypertensionDietBoolValue;
                    }
                }

                if (radFHHeartYes.Checked || radFHHeartNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(cardiacItem.HasFamilyHeartDiseaseHistory.Value, radHasFamilyHistoryOfHeartDiseaseBoolValue))
                    {
                        cardiacItem.HasFamilyHeartDiseaseHistory.Value = radHasFamilyHistoryOfHeartDiseaseBoolValue;
                    }
                }

                if (radFHStrokeYes.Checked || radFHStrokeNo.Checked)
                {
                    if (AHAAPI.H360Utility.IsUseNewValue(cardiacItem.HasFamilyStrokeHistory.Value, radHasFamilyHistoryOfStrokeBoolValue))
                    {
                        cardiacItem.HasFamilyStrokeHistory.Value = radHasFamilyHistoryOfStrokeBoolValue;
                    }
                }

            }
            else
            {
                cardiacItem = new HVManager.CardiacDataManager.CardiacItem();

                if (radBpMedicationYes.Checked || radBpMedicationNo.Checked)
                {
                    cardiacItem.IsOnHypertensionMedication.Value = radHighBPMedicationBoolValue;
                }

                if (radKidneyDiseaseYes.Checked || radKidneyDiseaseNo.Checked)
                {
                    cardiacItem.HasRenalFailureBeenDiagnosed.Value = radHaveKidneyDiseaseBoolValue;
                }

                if (radDiabetesYes.Checked || radDiabetesNo.Checked)
                {
                    cardiacItem.HasDiabetesBeenDiagnosed.Value = radHaveDiabetesBoolValue;
                }

                if (radHeartDiseaseYes.Checked || radHeartDiseaseNo.Checked)
                {
                    cardiacItem.HasPersonalHeartDiseaseHistory.Value = radDiagnosedHeartDiseaseBoolValue;
                }

                if (radHyperTensionDietYes.Checked || radHyperTensionDietNo.Checked)
                {
                    cardiacItem.IsOnHypertensionDiet.Value = radOnHypertensionDietBoolValue;
                }

                if (radFHHeartYes.Checked || radFHHeartNo.Checked)
                {
                    cardiacItem.HasFamilyHeartDiseaseHistory.Value = radHasFamilyHistoryOfHeartDiseaseBoolValue;
                }

                if (radFHStrokeYes.Checked || radFHStrokeNo.Checked)
                {
                    cardiacItem.HasFamilyStrokeHistory.Value = radHasFamilyHistoryOfStrokeBoolValue;
                }
            }

            #endregion
        }

        public bool Update_HealthHistory(HVManager.PersonalDataManager.PersonalItem personalItem,
                    HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem,
                    ref HVManager.CardiacDataManager.CardiacItem cardiacItem,
                    ref HVManager.HeightDataManager.HeightItem heightItem,
                    HVManager.BasicDataManager.BasicItem basicItem)
        {
            Page.Validate("HealthHistory");
            if (!Page.IsValid)
            {
                return false;
            }

            SetData_HealthHistory(personalItem, epItem, ref cardiacItem, ref heightItem, basicItem);

            AHAHelpContent.AHAUser.UpdateUserIsHealthHistoryCompleted(AHAAPI.SessionInfo.GetUserIdentityContext().UserId, true);

            return true;
        }

        protected void OnSave_HealthHistory(object sender, EventArgs e)
        {
            try
            {
                // Update HealthVault
                #region Update HealthVault

                HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;         
                HVManager.PersonalDataManager.PersonalItem personalItem = HVHelper.HVManagerPatientBase.PersonalDataManager.Item;
                HVManager.BasicDataManager.BasicItem basicItem = HVHelper.HVManagerPatientBase.BasicDataManager.Item;
                HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem = HVHelper.HVManagerPatientBase.ExtendedProfileDataManager.Item;
                HVManager.CardiacDataManager.CardiacItem cardiacItem = HVHelper.HVManagerPatientBase.CardiacDataManager.Item;
                HVManager.HeightDataManager.HeightItem heightItem = HeightWrapper.GetLatestHeightItem();

                if (epItem == null)
                {
                    epItem = new HVManager.ExtendedProfileDataManager.ExtendedProfileItem();
                }

                if (Update_HealthHistory(personalItem, epItem, ref cardiacItem, ref heightItem, basicItem))
                {
                    Dictionary<string, object> list = new Dictionary<string, object>();

                    list.Add(typeof(HVManager.BasicDataManager).ToString(), basicItem);
                    list.Add(typeof(HVManager.ExtendedProfileDataManager).ToString(), epItem);
                    list.Add(typeof(HVManager.PersonalDataManager).ToString(), personalItem);
                    list.Add(typeof(HVManager.CardiacDataManager).ToString(), cardiacItem);

                    mgr.CreateOrUpdateSingletons(list);

                #endregion

                    bool bHeightChanged = heightItem != null ? heightItem.IsDirty : false;

                    if (bHeightChanged)
                    {
                        heightItem = HVHelper.HVManagerPatientBase.HeightDataManager.CreateItem(heightItem);
                    }

                    UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
                    userIdentityContext.HasCompletedProfile = true;

                    #region DB Inserts

                    if (bHeightChanged)
                    {
                        AHAHelpContent.HeightDetails.CreateOrChangeHeight(AHAAPI.SessionInfo.GetUserIdentityContext().PersonalItemGUID,
                            heightItem.ThingKey.Id, heightItem.ThingKey.VersionStamp,
                            heightItem.HeightInMeters.Value, null, DateTime.Now);
                    }

                    string sBloodType = ddlBloodType.SelectedValue;
                    string sAllergies = txtAllergies.Text;
                    bool bMedication = false;
                    bool bHypertension = false;
                    bool bKidney = false;
                    bool bDiabetes = false;
                    bool bHistory = false;
                    bool bDiagnosed = false;
                    bool bStroke = false;
                    bool bSmoke = false;
                    bool bQuit = false;

                    if (radBpMedicationYes.Checked == true)
                    {
                        bMedication = true;
                    }

                    if (radHyperTensionDietYes.Checked == true)
                    {
                        bHypertension = true;
                    }

                    if (radKidneyDiseaseYes.Checked == true)
                    {
                        bKidney = true;
                    }

                    if (radDiabetesYes.Checked == true)
                    {
                        bDiabetes = true;
                    }

                    if (radFHHeartYes.Checked == true)
                    {
                        bHistory = true;
                    }

                    if (radHeartDiseaseYes.Checked == true)
                    {
                        bDiagnosed = true;
                    }

                    if (radFHStrokeYes.Checked == true)
                    {
                        bStroke = true;
                    }

                    if (radSmokeYes.Checked == true)
                    {
                        bSmoke = true;
                    }

                    if (radSmokeQuitYes.Checked == true)
                    {
                        bQuit = true;
                    }

                    // string BloodType,
                    // string MedicationAllergy,
                    // bool? HighBPMedication,
                    // bool? HighBPDiet,
                    // bool? KidneyDisease,
                    // bool? DiabetesDiagnosis,
                    // bool? FamilyHistoryHD,
                    // bool? HeartDiagnosis,
                    // bool? FamilyHistoryStroke,
                    // bool? DoYouSmoke,
                    // bool? HaveYouEverTriedToQuitSmoking
                    //DB Update

                    AHAHelpContent.ExtendedProfile.CreateOrUpdateHealthHistory
                        (
                            CurrentPatient.UserHealthRecordID,
                            ddlBloodType.SelectedValue.ToString(),
                            sAllergies,
                            bMedication,
                            bHypertension,
                            bKidney,
                            bDiabetes,
                            bHistory,
                            bDiagnosed,
                            bStroke,
                            bSmoke,
                            bQuit
                            );
                    #endregion

                    CommandEventArgs args = new CommandEventArgs("Refresh", String.Empty);
                    RaiseBubbleEvent(null, args);
                }
            }
            catch
            {
                lblMessageSaveHistory.Text = GetGlobalResourceObject("Tracker", "Error_Text_Save_Profile").ToString();
            }
        }

        #endregion

        #region MY EMERGENCY CONTACT

        private void GetData_EmergencyContact()
        {
            HVManager.EmergencyContactDataManager.EmergencyContactItem obj = HVHelper.HVManagerPatientBase.EmergencyContactDataManager.Item;

            if (obj != null)
            {
                txtFirstName_EC.Text = _ReplaceDashWithEmptyString(obj.FirstName.Value);
                txtLastName_EC.Text = _getDataForDisplay(obj.LastName.Value);
                txtAddress_EC.Text = _ReplaceDashWithEmptyString(obj.StreetAddress.Value);
                txtCity_EC.Text = _getDataForDisplay(obj.City.Value);
                txtZipCode_EC.Text = _getDataForDisplay(obj.Zip.Value);
                txtEmail_EC.Text = _getDataForDisplay(obj.EmailAddress.Value);

                string sPhoneContact = Regex.Replace(obj.Phone.Value.ToString(), "[^0-9]", "");

                if (!String.IsNullOrEmpty(sPhoneContact))
                {
                    if(sPhoneContact.Length >= 10)
                    {
                        txtPhoneContact_AreaCode_EC.Text = sPhoneContact.Substring(0, 3);
                        txtPhoneContact_Prefix_EC.Text = sPhoneContact.Substring(3, 3);
                        txtPhoneContact_Suffix_EC.Text = sPhoneContact.Substring(6, 4);
                    }
                }

                if (!String.IsNullOrEmpty(obj.EmailAddress.Value))
                {
                    txtEmail_EC.Text = (_getDataForDisplay(obj.EmailAddress.Value).ToString());
                }

                if (!String.IsNullOrEmpty(obj.State.Value))
                {
                    ddlState_EC.SelectedItem.Text = obj.State.Value.ToString();
                }

                if (!String.IsNullOrEmpty(obj.Country.Value))
                {
                    ddlCountry_EC.SelectedValue = obj.Country.Value.Trim();
                }
                else
                {
                    ddlCountry_EC.SelectedValue = "United States";
                }
            }
        }

        private string _getDataForDisplay(string strVal)
        {
            if (!String.IsNullOrEmpty(strVal) && strVal != "-")
            {
                return strVal;
            }
            else
            {
                return string.Empty;
            }
        }

        private string _getDataForUpdate(string strVal)
        {
            if (!string.IsNullOrEmpty(strVal))
                return strVal;

            return "-";
        }

        // HealthVault puts a '-' for blank values, so we nuke'em
        private string _ReplaceDashWithEmptyString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            return str.Replace("-", string.Empty);
        }

        public bool Update(ref HVManager.EmergencyContactDataManager.EmergencyContactItem ecItem)
        {
            Page.Validate("EmergencyContact");
            if (!Page.IsValid)
                return false;
            SetData_EmergencyContact(ref ecItem);

            return true;
        }

        private void SetData_EmergencyContact(ref HVManager.EmergencyContactDataManager.EmergencyContactItem obj)
        {
            if (obj != null)
            {
                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.FirstName.Value), _getDataForUpdate(txtFirstName_EC.Text.Trim())))
                {
                    obj.FirstName.Value = _getDataForUpdate(txtFirstName_EC.Text.Trim());
                }

                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.LastName.Value), _getDataForUpdate(txtLastName_EC.Text.Trim())))
                {
                    obj.LastName.Value = _getDataForUpdate(txtLastName_EC.Text.Trim());
                }

                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.StreetAddress.Value), _getDataForUpdate(txtAddress_EC.Text.Trim())))
                {
                    obj.StreetAddress.Value = _getDataForUpdate(txtAddress_EC.Text.Trim());
                }

                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.City.Value), _getDataForUpdate(txtCity_EC.Text.Trim())))
                {
                    obj.City.Value = _getDataForUpdate(txtCity_EC.Text.Trim());
                }

                if (AHAAPI.H360Utility.IsUseNewValue(obj.State.Value, ddlState_EC.SelectedValue.Trim()))
                {
                    obj.State.Value = ddlState_EC.SelectedValue.Trim();
                }

                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.Zip.Value), _getDataForUpdate(txtZipCode_EC.Text.Trim())))
                {
                    obj.Zip.Value = _getDataForUpdate(txtZipCode_EC.Text.Trim());
                }

                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.Country.Value), ddlCountry_EC.SelectedValue.Trim()))
                {
                    obj.Country.Value = ddlCountry_EC.SelectedValue.Trim();
                }

                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.Phone.Value), PhoneContact_EC))
                {
                    obj.Phone.Value = _getDataForUpdate(PhoneContact_EC);
                }

                if (AHAAPI.H360Utility.IsUseNewValue(_ReplaceDashWithEmptyString(obj.EmailAddress.Value), txtEmail_EC.Text.Trim()))
                {
                    obj.EmailAddress.Value = txtEmail_EC.Text.Trim();
                }
            }
            else
            {
                obj = new HVManager.EmergencyContactDataManager.EmergencyContactItem();

                obj.FirstName.Value = _getDataForUpdate(txtFirstName_EC.Text.Trim());
                obj.LastName.Value = _getDataForUpdate(txtLastName_EC.Text.Trim());
                obj.StreetAddress.Value = _getDataForUpdate(txtAddress_EC.Text.Trim());
                obj.City.Value = _getDataForUpdate(txtCity_EC.Text.Trim());
                obj.State.Value = ddlState_EC.SelectedValue.Trim();
                obj.Zip.Value = _getDataForUpdate(txtZipCode_EC.Text.Trim());
                obj.Country.Value = ddlCountry_EC.SelectedValue.Trim();
                obj.Phone.Value = _getDataForUpdate(PhoneContact_EC);
                obj.EmailAddress.Value = txtEmail_EC.Text.Trim();
            }
        }

        #region Save EmergencyContact

        public bool Update_EmergencyContact(ref HVManager.EmergencyContactDataManager.EmergencyContactItem ecItem)
        {
            Page.Validate("EmergencyContact");
            if (!Page.IsValid)
                return false;
            SetData_EmergencyContact(ref ecItem);

            return true;
        }

        protected void OnSave_EmergencyContact(object sender, EventArgs e)
        {
            // Save items to HealthVault
            HVManager.AllItemManager mgr = HVHelper.HVManagerPatientBase;

            HVManager.EmergencyContactDataManager.EmergencyContactItem ecItem = HVHelper.HVManagerPatientBase.EmergencyContactDataManager.Item;

            try
            {
                if (Update_EmergencyContact(ref ecItem))
                {
                    Dictionary<string, object> list = new Dictionary<string, object>();

                    list.Add(typeof(HVManager.EmergencyContactDataManager).ToString(), ecItem);

                    // Commit list to HealthVault
                    mgr.CreateOrUpdateSingletons(list);
                }

                CommandEventArgs args = new CommandEventArgs("Refresh", String.Empty);
                RaiseBubbleEvent(null, args);
            }
            catch(Exception ex)
            {
                lblMessageSaveEmergency.Text = ex.ToString();
            }
        }

        #endregion

        #endregion

        #region DropDown Controls

        private void _LoadYear()
        {
            EndYear = DateTime.Now.Year;
            StartYear = EndYear - LastXYears;

            System.Collections.SortedList years = new System.Collections.SortedList();
            for (int i = StartYear; i <= EndYear; i++)
                years.Add(i, i);

            ddlYearOfBirth.DataSource = years;
            ddlYearOfBirth.DataValueField = "key";
            ddlYearOfBirth.DataTextField = "value";
            ddlYearOfBirth.DataBind();

            ddlYearOfBirth.Items.Insert(0, (new ListItem("Year", "")));
        }

        /// <summary>
        /// Loading name suffixes.
        /// </summary>
        private void _LoadNameSuffixDDL()
        {
            List<AHAHelpContent.ListItem> objSuffixList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.NameSuffixes, H360Utility.GetCurrentLanguageID());
            ddlSuffix.DataSource = objSuffixList;
            ddlSuffix.DataTextField = "ListItemName";
            ddlSuffix.DataValueField = "ListItemCode";
            ddlSuffix.DataBind();

            ddlSuffix.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlSuffix.SelectedItem.Value = (string.Empty);
        }

        /// <summary>
        /// Loading name titles.
        /// </summary>
        private void _LoadNameTitleDDL()
        {
            List<AHAHelpContent.ListItem> objTitleList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.NamePrefixes, H360Utility.GetCurrentLanguageID());
            ddlTitle.DataSource = objTitleList;
            ddlTitle.DataTextField = "ListItemName";
            ddlTitle.DataValueField = "ListItemCode";
            ddlTitle.DataBind();

            ddlTitle.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlTitle.SelectedItem.Value = (string.Empty);
        }

        /// <summary>
        /// Loading States.
        /// </summary>
        private void _LoadStateDDL()
        {
            List<AHAHelpContent.ListItem> objStateList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.States, H360Utility.GetCurrentLanguageID());
            ddlState.DataSource = objStateList;
            ddlState.DataTextField = "ListItemName";
            ddlState.DataValueField = "ListItemCode";
            ddlState.DataBind();

            ddlState.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlState.SelectedItem.Value = (string.Empty);

            // For Emergency Contact
            ddlState_EC.DataSource = objStateList;
            ddlState_EC.DataTextField = "ListItemName";
            ddlState_EC.DataValueField = "ListItemCode";
            ddlState_EC.DataBind();

            ddlState_EC.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlState_EC.SelectedItem.Value = (string.Empty);
        }

        /// <summary>
        /// Loading Country.
        /// </summary>
        private void _LoadCountryDDL()
        {
            List<AHAHelpContent.ListItem> listItems = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.Countries, H360Utility.GetCurrentLanguageID());
            ddlCountry.DataSource = listItems;
            ddlCountry.DataTextField = "ListItemName";
            ddlCountry.DataValueField = "ListItemName";
            ddlCountry.DataBind();

            ddlCountry.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlCountry.SelectedItem.Value = (string.Empty);

            // For Emergency Contact
            ddlCountry_EC.DataSource = listItems;
            ddlCountry_EC.DataTextField = "ListItemName";
            ddlCountry_EC.DataValueField = "ListItemName";
            ddlCountry_EC.DataBind();

            ddlCountry_EC.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlCountry_EC.SelectedItem.Value = (string.Empty);
        }

        /// <summary>
        /// Loading gender.
        /// </summary>
        private void _LoadGenderRadList()
        {
            List<AHAHelpContent.ListItem> objGenderList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.GenderTypes, H360Utility.GetCurrentLanguageID());

            ddlGender.DataSource = objGenderList;
            ddlGender.DataTextField = "ListItemName";
            ddlGender.DataValueField = "ListItemCode";
            ddlGender.DataBind();

            ddlGender.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlGender.SelectedValue = (string.Empty);
        }

        /// <summary>
        /// Loading ethinicity.
        /// </summary>
        private void _LoadEthnicityDDL()
        {
            List<AHAHelpContent.ListItem> objEthnicityList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.Ethnicity, H360Utility.GetCurrentLanguageID());
            ddlEthnicity.DataSource = objEthnicityList;
            ddlEthnicity.DataTextField = "ListItemName";
            ddlEthnicity.DataValueField = "ListItemName";
            ddlEthnicity.DataBind();

            ddlEthnicity.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlEthnicity.SelectedItem.Value = (string.Empty);
        }

        private void _LoadBloodTypeDDL()
        {
            List<AHAHelpContent.ListItem> objBloodTypeList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.BloodType, H360Utility.GetCurrentLanguageID());
            ddlBloodType.DataSource = objBloodTypeList;
            ddlBloodType.DataTextField = "ListItemName";
            ddlBloodType.DataValueField = "ListItemCode";
            ddlBloodType.DataBind();

            ddlBloodType.Items.Insert(0, new ListItem(GetGlobalResourceObject("Common", "Text_Select").ToString(), string.Empty));

            ddlBloodType.SelectedItem.Value = (string.Empty);
        }

        #endregion

        #region VALIDATORS

        #region Validate Profile

        /// <summary>
        /// Validating First Name.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateFirstName(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtFirstName.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_FirstName").ToString();

                return;
            }

        }

        /// <summary>
        /// Validating Last Name.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateLastName(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtLastName.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_LastName").ToString();

                return;
            }
        }

        /// <summary>
        /// Validating Address.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateAddress(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtAddress.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Address").ToString();

                return;
            }
        }

        /// <summary>
        /// Validating Zip code.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateZipCodeAndCity(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            //v.IsValid = IsValidCheckZipCodeAndCity(valid);
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtZipCode.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = (GetGlobalResourceObject("Tracker", "Validation_Text_ZipCode").ToString());

                return;
            }           
        }

        protected bool IsValidCheckZipCodeAndCity(CustomValidator valid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(GetGlobalResourceObject("Tracker", "Validation_Text").ToString() + " ");

            List<string> strList = new List<string>();
            if (String.IsNullOrEmpty(txtCity.Text.Trim()))
            {
                strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_CityName").ToString());
            }

            if (String.IsNullOrEmpty(txtZipCode.Text.Trim()))
            {
                strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_ZipCode").ToString());
            }
            else if (!string.IsNullOrEmpty(ddlCountry.SelectedValue) && ddlCountry.SelectedValue.CompareTo("US") == 0)
            {
                if (!AHAAPI.H360Utility.IsValidZipCode(txtZipCode.Text.Trim(), true))
                {
                    strList.Add(GetGlobalResourceObject("Patient1", "Validation_Text_ValidZipCode").ToString());
                }
            }

            if (strList.Count > 0)
            {
                foreach (string str in strList)
                {
                    sb.Append(str + ",");
                }
                valid.ErrorMessage = sb.ToString().TrimEnd(',');
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validating Country.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateCountry(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Country").ToString();
            }
            return;
        }

        protected void ValidatePhoneContact(object sender, ServerValidateEventArgs v)
        {
            CustomValidator validator = sender as CustomValidator;
            v.IsValid = true;

            // If areacode + prefix + suffix is not null
            if (!String.IsNullOrEmpty(PhoneContact))
            {
                // If the phone number is less than the required 10 digits
                if (PhoneContact.Length < 10)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Contact").ToString();
                    v.IsValid = false;
                }
            }
        }

        protected void ValidatePhoneMobile(object sender, ServerValidateEventArgs v)
        {
            CustomValidator validator = sender as CustomValidator;
            v.IsValid = true;

            // If areacode + prefix + suffix is not null
            if (!String.IsNullOrEmpty(PhoneMobile))
            {
                // If the phone number is less than the required 10 digits
                if (PhoneMobile.Length < 10)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Mobile").ToString();
                    v.IsValid = false;
                }
            }
        }

        protected void ValidateEmail(object sender, ServerValidateEventArgs v)
        {
            CustomValidator validator = sender as CustomValidator;
            v.IsValid = true;

            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Email").ToString();
                v.IsValid = false;
            }
            else if (!GRCBase.EmailSyntaxValidator.Valid(txtEmail.Text, true))
            {
                validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Valid_EmailAddress").ToString();
                v.IsValid = false;
            }
        }

        /// <summary>
        /// Validating Gender.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateGender(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (string.IsNullOrEmpty(ddlGender.SelectedValue))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Gender").ToString();
            }
            return;
        }

        /// <summary>
        /// Validating Year Of Birth.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateYearOfBirth(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (string.IsNullOrEmpty(ddlYearOfBirth.SelectedValue))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_YearOfBirth").ToString();
            }
            return;
        }

        #endregion

        #region Validate Health History

        /// <summary>
        /// validating feet height value
        /// </summary>
        /// <param name="source"></param>
        /// <param name="v"></param>
        protected void validateHeight(object source, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)source;
            v.IsValid = IsValidHeightEntry(valid);
            return;
        }

        protected bool IsValidHeightEntry(CustomValidator valid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(GetGlobalResourceObject("Tracker", "Validation_Text_Height").ToString() + ": ");
            List<string> strList = new List<string>();

            // Cannot be NULL in height!
            if (String.IsNullOrEmpty(txtHeightFeet.Text.Trim()))
            {
                strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_Feet").ToString());
            }
            else
            {
                try
                {
                    int iObjHeightInFeet = Convert.ToInt32(txtHeightFeet.Text.Trim());
                    // Cannot be 0 feet tall!
                    if (iObjHeightInFeet <= 0)
                    { strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_Feet").ToString()); }
                }
                catch
                {
                    strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_Feet").ToString());
                }
            }

            if (String.IsNullOrEmpty(txtHeightInches.Text.Trim()))
            {
                strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_Inches").ToString());
            }
            else
            {
                try
                {
                    double dbObjHeightInches = Convert.ToDouble(txtHeightInches.Text.Trim());
                    if ((dbObjHeightInches > 11.00) || (dbObjHeightInches < 0.0))
                    {
                        strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_Inches").ToString());
                    }
                }
                catch
                {
                    strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_Inches").ToString());
                }
            }

            if (strList.Count > 0)
            {
                foreach (string str in strList)
                {
                    sb.Append(str + ",");
                }
                valid.ErrorMessage = sb.ToString().TrimEnd(',');
                return false;
            }

            return true;
        }

        /// <summary>
        /// validating Allergies
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void validateAllergies(object source, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)source;
            v.IsValid = true;

            return;
        }


        /// <summary>
        /// validating Blood pressure value
        /// </summary>
        /// <param name="source"></param>
        /// <param name="v"></param>
        //protected void validateBP(object source, ServerValidateEventArgs v)
        //{
        //    CustomValidator valid = (CustomValidator)source;
        //    v.IsValid = IsValidBPEntry(valid);
        //    return;
        //}

        //protected bool IsValidBPEntry(CustomValidator valid)
        //{
        //    if (string.IsNullOrEmpty(txtSystolic.Text.Trim())
        //        && string.IsNullOrEmpty(txtDiastolic.Text.Trim()))
        //    {
        //        return true;
        //    }

        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(GetGlobalResourceObject("Tracker", "Validation_Text_BloodPressure"));
        //    List<string> strList = new List<string>();

        //    try
        //    {
        //        int iObjSystolic = Convert.ToInt32(txtSystolic.Text.Trim());
        //        if (iObjSystolic <= 0)
        //        { strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_SystolicField").ToString()); }
        //    }
        //    catch
        //    {
        //        strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_SystolicField").ToString());
        //    }

        //    try
        //    {
        //        int iObjDiastolic = Convert.ToInt32(txtDiastolic.Text.Trim());
        //        if (iObjDiastolic <= 0)
        //        { strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_DiastolicField").ToString()); }
        //    }
        //    catch
        //    {
        //        strList.Add(GetGlobalResourceObject("Tracker", "Validation_Text_DiastolicField").ToString());
        //    }

        //    if (strList.Count > 0)
        //    {
        //        foreach (string str in strList)
        //        {
        //            sb.Append(str + ",");
        //        }
        //        valid.ErrorMessage = sb.ToString().TrimEnd(',');
        //        return false;
        //    }

        //    return true;
        //}

        #endregion

        #region Validate Emergency Contact

        /// <summary>
        /// Validating Last Name.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateLastName_EC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtLastName_EC.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_LastName").ToString();

                return;
            }
        }

        /// <summary>
        /// Validating Address.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateAddress_EC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtAddress_EC.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = (GetGlobalResourceObject("Tracker", "Validation_Text_Address").ToString());

                return;
            }
        }

        /// <summary>
        /// Validating City.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateCity_EC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtCity_EC.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = (GetGlobalResourceObject("Tracker", "Validation_Text_City").ToString());

                return;
            }
        }

        /// <summary>
        /// Validating Zip code.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateZipCode_EC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (String.IsNullOrEmpty(txtZipCode_EC.Text.Trim()))
            {
                v.IsValid = false;
                valid.ErrorMessage = (GetGlobalResourceObject("Tracker", "Validation_Text_ZipCode").ToString());

                return;
            }
        }

        /// <summary>
        /// Validating Country.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateCountry_EC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;
            if (string.IsNullOrEmpty(ddlCountry_EC.SelectedValue))
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Country").ToString();
            }
            return;
        }

        /// <summary>
        /// Validating Phone Number.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="v"></param>
        protected void ValidateContactPhone_EC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator validator = sender as CustomValidator;
            v.IsValid = true;

            // If areacode + prefix + suffix is not null
            if (!String.IsNullOrEmpty(PhoneContact_EC))
            {
                // If the phone number is less than the required 10 digits
                if (PhoneContact_EC.Length < 10)
                {
                    validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Invalid_Phone_Contact").ToString();
                    v.IsValid = false;
                }
            }
        }

        protected void ValidateEmail_EC(object sender, ServerValidateEventArgs v)
        {
            CustomValidator validator = sender as CustomValidator;
            v.IsValid = true;

            if (string.IsNullOrEmpty(txtEmail_EC.Text))
            {
                validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Specify_Email").ToString();
                v.IsValid = false;
            }
            else if (!GRCBase.EmailSyntaxValidator.Valid(txtEmail_EC.Text, true))
            {
                validator.ErrorMessage = GetGlobalResourceObject("Tracker", "Validation_Text_Valid_EmailAddress").ToString();
                v.IsValid = false;
            }
        }

        #endregion

        #endregion

        #region IPeriodicDataManager Members

        public void LoadItems()
        {
            throw new NotImplementedException();
        }

        public void FocusTabOne()
        {
            throw new NotImplementedException();
        }

        public void OnAddRecord()
        {
            throw new NotImplementedException();
        }

        public void OnCreatePDF()
        {
            throw new NotImplementedException();
        }

        public AHACommon.DataDateReturnType DateRange
        {
            get { return AHACommon.DataDateReturnType.AllData; }
        }

        public AHAAPI.TrackerType TrackerType
        {
            get { throw new NotImplementedException(); }
        }

        public DataViewType DataViewType
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}