﻿using System;
using System.Web;
using System.Web.UI.WebControls;

using AHAAPI;
using Microsoft.Health.Web;

namespace Heart360Web.Pages.Patient
{
    public partial class NewTerms : H360PageHelper
    {
        private AHAHelpContent.Patient CurrentPatient
        {
            get;
            set;
        }

        public void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserIdentityContext objContext = SessionInfo.GetUserIdentityContext();

            this.CurrentPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objContext.PatientID.Value);

            // If the user has already accepted the terms, send them away
            if (objContext.HasUserAgreedToNewTermsAndConditions)
            {
                Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);
            }

            if (!Page.IsPostBack)
            {
                _LoadData();
            }
        }

        private void _LoadData()
        {
            AHAHelpContent.AHAUser objUser = AHAHelpContent.AHAUser.GetAHAUserByPersonGUID(SessionInfo.GetUserIdentityContext().UserId);

            HVManager.CurrentUserHVSetting objHVSetting = HVManager.CurrentUserHVSetting.GetCurrentUserSetting((this.Page as HealthServicePage).AuthenticatedConnection);
        }

        protected void OnSubmit(object sender, EventArgs e)
        {
            // Validate the form
            Page.Validate("NewUser");
            if (!Page.IsValid)
                return;

            try
            {
                bool bolUserAgreement = Convert.ToBoolean(chkAccept.Checked);
                HVManager.AppProfileDataManager.AppProfileItem appProfileItem = HVHelper.HVManagerPatientBase.AppProfileDataManager.Item;

                if (appProfileItem == null)
                {
                    appProfileItem = new HVManager.AppProfileDataManager.AppProfileItem();
                }

                if (bolUserAgreement)
                {
                    appProfileItem.HasUserAgreedToNewTermsAndConditions.Value = true;
                    appProfileItem.HasUserAgreedToStoreDataInDB.Value = true;

                    HVHelper.HVManagerPatientBase.AppProfileDataManager.CreateOrUpdateItem(appProfileItem);
                }

                /*Add these items in DB*/
                AHAHelpContent.AHAUser objUser = AHAHelpContent.AHAUser.GetAHAUserByPersonGUID(SessionInfo.GetUserIdentityContext().UserId);
                if (!objUser.NewTermsAndConditions)
                {
                    //SessionInfo.SetDBSynchronized(false);
                    HVHelper.HVManagerPatientBase.IsDBSynchronized = false;
                    HVHelper.HVManagerPatientBase.IsPolkaDataProcessed = false;
                }

                AHAHelpContent.AHAUser.UpdateUserNewTermsAndConsent(SessionInfo.GetUserIdentityContext().UserId, chkAccept.Checked);

                //update identity context
                UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
                userIdentityContext.HasUserAgreedToNewTermsAndConditions = chkAccept.Checked;

                //update session store
                HttpContext.Current.Session[AHACommon.SessionKeys.sessionKeyUserIdentityContext] = userIdentityContext;

                //Send to Home
                Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);

            }
            catch (Exception ex)
            {
                libError.Text = ex.ToString();
            }
        }
        
        #region Validators

        // To check if the user agreed to the Terms and Conditions.
        protected void ValidateAcceptance(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            if (chkAccept.Checked == false)
            {
                v.IsValid = false;
                valid.ErrorMessage = GetGlobalResourceObject("Common", "Text_Validation_TermsOfUse").ToString();
                chkAccept.Attributes.Add("style.color", "RED");
                return;
            }
        }
        
        #endregion
    }
}