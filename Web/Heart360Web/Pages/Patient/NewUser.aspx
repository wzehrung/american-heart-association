﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Patient.Master" AutoEventWireup="true" CodeBehind="NewUser.aspx.cs" Inherits="Heart360Web.Pages.Patient.NewUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Controls/Patient/ModuleAddReading.ascx" TagName="AddReading" TagPrefix="Module" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

	<div class="large-12 small-12 columns">

		<header class="full">
    
            <h2><%=GetGlobalResourceObject("Common", "NewUser_Title")%></h2>
            <h2><%=GetGlobalResourceObject("Common", "NewUser_Text")%></h2>

		</header>

        <asp:ValidationSummary 
            ID="valSummary"
            ValidationGroup="NewUser"
            HeaderText="<%$Resources:Common,Text_ValidationMessage %>"
            Enabled="true"
            DisplayMode="BulletList"
            ShowSummary="true"
            CssClass="validation-error"
            runat="server" />

	</div>

    <!-- ## BEGIN Phone and Acceptance ## -->
    <div class="large-6 small-12 columns fl">

        <div class="row">
            <div class="large-12 small-12 columns">
                <p><span class="required" style="font-weight: bold; color: Red;"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span><strong>Required</strong></p>
            </div>                    
        </div>

        <div class="row">
       
		    <div class="large-9 small-12 columns">
			    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_ContactPhoneNumber")%></label>
                <div class="row">
				    <div class="large-3 small-3 columns">
                        <asp:TextBox ID="txtPhoneContact_AreaCode" runat="server" MaxLength="3"></asp:TextBox>
                    </div>

                    <div class="large-3 small-3 columns">
                        <asp:TextBox ID="txtPhoneContact_Prefix" runat="server" MaxLength="3"></asp:TextBox>
                    </div>

                    <div class="large-3 small-3 columns fl">
                        <asp:TextBox ID="txtPhoneContact_Suffix" runat="server" MaxLength="4"></asp:TextBox>
                    </div>
                </div>
		    </div>

        </div>

        <div class="row">

		    <div class="large-9 small-12 columns">

			    <label class="text-left" ><%=GetGlobalResourceObject("Tracker", "Tracker_Profile_MobilePhoneNumber")%></label>

                <div class="row">

				    <div class="large-3 small-3 columns">
                        <asp:TextBox ID="txtPhoneMobile_AreaCode" runat="server" MaxLength="3"></asp:TextBox>
                    </div>

                    <div class="large-3 small-3 columns">
                        <asp:TextBox ID="txtPhoneMobile_Prefix" runat="server" MaxLength="3"></asp:TextBox>
                    </div>

                    <div class="large-3 small-3 columns fl">
                        <asp:TextBox ID="txtPhoneMobile_Suffix" runat="server" MaxLength="4"></asp:TextBox>
                    </div>

                </div>

		    </div>

        </div>

        <div class="row">
            <div class="large-12 small-12 columns" style="height: 200px; overflow-y: scroll; border: 1px solid #666;">
                <%=GetGlobalResourceObject("Common","AHA_User_Agreement")%>
            </div>                    
        </div>

		<div class="row">

            <div class="large-1 small-1 columns" style="padding-top: 5px">
                <asp:CheckBox ID="chkAccept" runat="server" CssClass="fr" />
            </div>

			<div class="large-11 small-11 columns">
                <label class="check-text" for="ContentPlaceHolderMain_chkAccept">
                    <span class="required" style="font-weight: bold; color: Red;"><%=GetGlobalResourceObject("Common","Symbol_Asterisk")%></span>
                    <%=GetGlobalResourceObject("Common","Text_User_Agreement_Checkbox") %>
                </label>
			</div>

		</div>

    </div>
    <!-- ## END Phone and Acceptance ## -->

    <!-- ## BEGIN First Reading ## -->
    <div class="large-6 small-12 columns">

        <div class="row">

            <div class="new-user-form page-red large-8 small-12 columns fl" style="margin-top: 0;">

                <div class="full">
                    <h5><%=GetGlobalResourceObject("Common", "NewUser_FirstBloodPressure")%></h5>        
                </div>

                <Module:AddReading ID="AddReadingBase" ModuleIdAsInt="3" EnableAddButton="false" EnableTitle="false" EnableComments="false" runat="server" /> 

                <div class="continue row spt">
	                <div class="large-8 small-12 columns">
                        <asp:Button ID="btnAccept" CssClass="red-button full" Text="<%$Resources:Common,NewUser_CreateAccount%>" runat="server" onclick="OnSubmit" />
	                </div>
                </div>

            </div>

        </div>
    
    </div>
    <!-- ## END First Reading ## -->

    <asp:Label ID="libError" runat="server"></asp:Label>

    <asp:FilteredTextBoxExtender
        ID="FilteredTextBoxExtender1"
        runat="server"
        Enabled="true"
        TargetControlID="txtPhoneContact_AreaCode"
        FilterType="Numbers">
    </asp:FilteredTextBoxExtender>

    <asp:FilteredTextBoxExtender
        ID="FilteredTextBoxExtender2"
        runat="server"
        Enabled="true"
        TargetControlID="txtPhoneContact_Prefix"
        FilterType="Numbers">
    </asp:FilteredTextBoxExtender>

    <asp:FilteredTextBoxExtender
        ID="FilteredTextBoxExtender3"
        runat="server"
        Enabled="true"
        TargetControlID="txtPhoneContact_Suffix"
        FilterType="Numbers">
    </asp:FilteredTextBoxExtender>

    <asp:FilteredTextBoxExtender
        ID="FilteredTextBoxExtender4"
        runat="server"
        Enabled="true"
        TargetControlID="txtPhoneMobile_AreaCode"
        FilterType="Numbers">
    </asp:FilteredTextBoxExtender>

    <asp:FilteredTextBoxExtender
        ID="FilteredTextBoxExtender5"
        runat="server"
        Enabled="true"
        TargetControlID="txtPhoneMobile_Prefix"
        FilterType="Numbers">
    </asp:FilteredTextBoxExtender>

    <asp:FilteredTextBoxExtender
        ID="FilteredTextBoxExtender6"
        runat="server"
        Enabled="true"
        TargetControlID="txtPhoneMobile_Suffix"
        FilterType="Numbers">
    </asp:FilteredTextBoxExtender>

    <asp:CustomValidator
        ID="CustomValidator1"
        ValidationGroup="NewUser"
        Display="None"
        OnServerValidate="ValidateAcceptance"
        runat="server"
        EnableClientScript="false">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validPhoneContact"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="NewUser"
        Display="None"
        OnServerValidate="ValidatePhoneContact">
    </asp:CustomValidator>

    <asp:CustomValidator
        ID="validPhoneMobile"
        runat="server"
        EnableClientScript="false"
        ValidationGroup="NewUser"
        Display="None"
        OnServerValidate="ValidatePhoneMobile">
    </asp:CustomValidator>

</asp:Content>