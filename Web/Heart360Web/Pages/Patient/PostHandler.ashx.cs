﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AHAHelpContent;
using AHAAPI;


namespace Heart360Web.Pages.Patient
{
    /// <summary>
    /// Summary description for PostHandler1
    /// </summary>
    public class PostHandler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            bool    succeeded = false;
            int     newNbUnread = 0;
            int     iMessageID = -1;
            int     iPatientID = -1;

            //if( !string.IsNullOrEmpty(Request.Form["ReadMail"]) )
            if (!string.IsNullOrEmpty(context.Request.Params["ReadMail"]) && !string.IsNullOrEmpty(context.Request.Params["UserId"]))
            {
                
                try
                {
                    iMessageID = int.Parse(context.Request.Params["ReadMail"]);
                    iPatientID = int.Parse(context.Request.Params["UserId"]);

                    /**
                    if (iMessageID > 0 && iPatientID > 0)
                    {
                        succeeded = true;
                    }
                    **/

                    if (iMessageID > 0 && iPatientID > 0)
                    {
                        AHAHelpContent.UserTypeDetails patDetails = new AHAHelpContent.UserTypeDetails
                                                                                {
                                                                                    UserID = iPatientID,
                                                                                    UserType = AHAHelpContent.UserType.Patient
                                                                                };

                        AHAHelpContent.Message.MarkMessageAsReadOrUnread(patDetails, iMessageID, true);

                        // assign newNbUnread
                        newNbUnread = AHAHelpContent.Message.GetNumberOfUnreadMessagesForUser( patDetails, AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX);

                        succeeded = true;
                    }
                }
                catch
                {

                }

            }
            // write response.
            string resp = "[{\"success\" : \"" + ((succeeded) ? "true" : "false") + "\",\"msgunread\" : \"" + newNbUnread.ToString() + "\"}]";
            context.Response.ContentType = "text/plain";
            context.Response.Write(resp);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}