﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Patient.Master" AutoEventWireup="true" 
         CodeBehind="ConnectionCenter.aspx.cs" Inherits="Heart360Web.Pages.Patient.ConnectionCenter"
         MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/Patient/ModuleSummary.ascx" TagName="Summary" TagPrefix="Module" %>

<%@ Register Src="~/Controls/Patient/ConnectionCenter/Announcements.ascx" TagName="Announcements" TagPrefix="Connection" %>
<%@ Register Src="~/Controls/Patient/ConnectionCenter/MyMessages.ascx" TagName="MyMessages" TagPrefix="Connection" %>
<%@ Register Src="~/Controls/Patient/ConnectionCenter/RemindMe.ascx" TagName="RemindMe" TagPrefix="Connection" %>
<%@ Register Src="~/Controls/Patient/ConnectionCenter/Connections.ascx" TagName="Connections" TagPrefix="Connection" %>
<%@ Register Src="~/Controls/Patient/ConnectionCenter/GlobalResources.ascx" TagName="GlobalResources" TagPrefix="Connection" %>
<%@ Register Src="~/Controls/Patient/ConnectionCenter/LocalResources.ascx" TagName="LocalResources" TagPrefix="Connection" %>
<%@ Register Src="~/Controls/Patient/ConnectionCenter/Sponsors.ascx" TagName="Sponsors" TagPrefix="Connection" %>
<%--<%@ Register Src="~/Controls/Patient/ConnectionCenter/EMR.ascx" TagName="MyEMR" TagPrefix="Connection" %>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script>
     ///<summary>
     ///  This will fire on initial page load, 
     ///  and all subsequent partial page updates made 
     ///  by any update panel on the page
     ///</summary>
         function pageLoad(sender, args) {
             paginatePages();
             if (args.get_isPartialLoad()) { // executes the code after asynchronous postbacks.
                     //console.log("Partial Postback occur");
                 hideShowPanels();
                 checkForAccordionSelected();
            } else { // for postback

                hideShowPanels();
                //console.log("Full page post back.");
             }
        }  

    </script>
    <!-- ## BEGIN Tracker Details ## -->
    <section id="tracker-details" class="blue-theme small-12 large-8 columns">
        <asp:UpdatePanel ID="UpdatePanelTrackerSummary" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <Module:Summary ID="SummaryConnectionCenter" ModuleIdAsInt="1" TrackerType="my-center-large" SummaryLocation="2" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- ## BEGIN Announcements ## -->
        <Connection:Announcements ID="Announcements" runat="server" />
        <!-- ## END Announcements ## -->

        <!-- ## BEGIN My Messages ## -->
        <Connection:MyMessages ID="MyMessages" runat="server" />
        <!-- ## END My Messages ## -->

        <!-- ## BEGIN RemindMe ## -->
        <Connection:RemindMe ID="RemindMe" runat="server" />
        <!-- ## END RemindMe ## -->
        
        <!-- ## BEGIN Connections ## -->
        <Connection:Connections ID="Connections" runat="server" />
        <!-- ## END Connections ## -->

        <!-- ## BEGIN My EMR ## -->
        <%--<Connection:MyEMR ID="EMR" runat="server" />--%>
        <!-- ## END My EMR ## -->

        <!-- ## BEGIN GlobalResources ## -->
        <Connection:GlobalResources ID="GlobalResources" runat="server" />
        <!-- ## END GlobalResources ## -->

        <!-- ## BEGIN LocalResources ## -->
        <Connection:LocalResources ID="LocalResources" runat="server" />
        <!-- ## END LocalResources ## -->

        <!-- ## BEGIN Sponsors ## -->
        <Connection:Sponsors ID="Sponsors" runat="server" />
        <!-- ## END Sponsors ## -->
	</section>
    <!-- ## END Tracker Details ## -->

    <!-- ## BEGIN Tracker Summary ## -->
	<section id="side-tiles" class="small-12 large-4 columns">

        <asp:UpdatePanel ID="UpdatePanelSummaries" UpdateMode="Conditional" runat="server" >
            <ContentTemplate>

                <Module:Summary ID="SummaryProfile" ModuleIdAsInt="2" TrackerType="my-profile" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryBloodPressure" ModuleIdAsInt="3" TrackerType="my-pressure" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryWeight" ModuleIdAsInt="4" TrackerType="my-weight" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryPhysical" ModuleIdAsInt="5" TrackerType="my-activity" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryBloodGlucose" ModuleIdAsInt="6" TrackerType="my-glucose" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryMyCholesterol" ModuleIdAsInt="7" TrackerType="my-cholesterol" SummaryLocation="1" runat="server" />
                <Module:Summary ID="SummaryMyMedications" ModuleIdAsInt="8" TrackerType="my-medications" SummaryLocation="1" runat="server" />

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnPpeSummaryOk" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <Module:Summary ID="SummaryMyLifeCheck" ModuleIdAsInt="9" TrackerType="my-lifecheck" SummaryLocation="1" runat="server" />  
              
    </section>    
    <!-- ## END Tracker Summary ## -->


    <!-- ### BEGIN Popup Stuff ### -->
    <input id="btnPpeSummaryProxyOk" type="button" value="Done" onclick="document.getElementById('<%= btnPpeSummaryOk.ClientID %>').click(); return false; " style="display: none" />
    <input id="btnPpeTrackerProxyOk" type="button" value="Done" onclick="document.getElementById('<%= btnPpeTrackerOk.ClientID %>').click(); return false; " style="display: none" />

    <asp:UpdatePanel ID="UpdatePanelPopups" UpdateMode="Conditional" runat="server" >
    <ContentTemplate>
        <!-- ### BEGIN Summary Popup ### -->
        <div style="display: none;" >
            <div id="popupSummaryTrigger" runat="server" class="add-this" data-name="cpopupsummary" />
        </div>

        <div class="shadowbox cpopupsummary"></div>
        <div id="pnlPopupSummaryContent" style="display: none" runat="server" class="modal-bg cpopupsummary" >
            <div id="divPopupSummaryStyle" runat="server" class="modal-wrap cpopupsummary page-blue" >
                <div class="inner-modal" >
                    <a class="close-modal" onclick="return window.close_modal();">CLOSE</a>
                    <!-- src and class attributes are added procedurally -->
                    <iframe id="popupSummaryIframe" runat="server" frameborder="0" height="100%" width="100%"></iframe>
                    <asp:Button ID="btnPpeSummaryOk" CssClass="close-modal" runat="server" OnClick="btnPpeSummaryOk_Click" style="display: none" />
                    <input id="btnPpeSummaryCancel" type="button" class="close-modal" value="Cancel" style="display: none" onclick="return window.close_modal();"/>
                </div>
            </div>
        </div>
        <!-- ### END Summary Popup ### -->

        <!-- ### BEGIN Tracker Popup ### -->
        <div style="display: none;" >
            <div id="popupTrackerTrigger" runat="server" class="add-this" data-name="cpopuptracker" />
        </div>

        <div class="shadowbox cpopuptracker"></div>
        <div ID="pnlPopupTrackerContent" style="display: none" runat="server" class="modal-bg cpopuptracker" >
            <div id="divPopupTrackerStyle" runat="server" class="modal-wrap cpopuptracker page-blue" >
	            <div class="inner-modal" >
                    <a class="close-modal" onclick="return window.close_modal();">CLOSE</a>
                    <!-- src and class attributes are added procedurally -->
                    <iframe id="popupTrackerIframe" runat="server" frameborder="0" height="100%" width="100%"></iframe>
                    <!-- if the popup changes data, it will click this hidden ok button, which needs to refresh the page -->
                    <asp:Button id="btnPpeTrackerOk" CssClass="close-modal" runat="server" OnClick="btnPpeTrackerOk_Click" style="display: none" />
                    <input id="btnPpeTrackerCancel" type="button" class="close-modal" value="Cancel" style="display: none" onclick="return window.close_modal();"/>
                </div>                     
            </div>
        </div>
        <!-- ### END Tracker Popup ### -->  
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>