﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using AHAHelpContent;

namespace Heart360Web.Patient
{
    public partial class Dashboard : H360PageHelper
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // PJB: This is what goes on in AHAWeb ~/Patient/MyHome.aspx (Page_Load)
            //      * checks for MLC (My Life Check) stuff
            //      * checks for Patient Invitation from Provider (redirect from ConnectProvider.aspx)
            UserIdentityContext objUserIdentity = SessionInfo.GetUserIdentityContext();

            //
            // First check if this is a redirect from My Life Check.
            //
            AHAAPI.MLCInfo objMLCInfo = AHAAPI.SessionInfo.GetMLCInfoFromSession() as AHAAPI.MLCInfo;
            if (objMLCInfo != null)
            {
                Response.Redirect(AHAAPI.WebAppNavigation.H360.PAGE_PATIENT_MLC_UPLOAD);
            }

            //
            // Second check if this is a redirect from ConnectProvider.aspx
            //
            AHAAPI.PatientInvitation objInvitaionContext = AHAAPI.SessionInfo.GetPatientInvitationSession();
            if (objInvitaionContext != null && objInvitaionContext.IsPatientAccept)
            {
                if (!string.IsNullOrEmpty(objInvitaionContext.ProviderCode))
                {
                    //patient entered provider code before logging in
                    bool bIsPossible = UIHelper.ProcessProviderCodeOnlyTypeInvitationForPatient(objInvitaionContext.ProviderCode, this.Patient.UserHealthRecordID, this.PersonGUID, this.RecordGUID);
                }
                else
                {
                    PatientProviderInvitation objCurrentInvitation = PatientProviderInvitation.FindByInvitationCode(objInvitaionContext.InvitationCode);

                    PatientProviderInvitationDetails objAvailableDetails = objCurrentInvitation.PendingInvitationDetails.Where(PI => PI.AcceptedPatientID == objUserIdentity.PatientID.Value).SingleOrDefault();

                    if (objAvailableDetails == null) //Patient came from the invitaition landing page
                    {
                        objAvailableDetails = objCurrentInvitation.PendingInvitationDetails.First();

                        objAvailableDetails.AcceptedPatientID = objUserIdentity.PatientID.Value;

                        bool bAlreadyConnected = ProviderDetails.IsValidPatientForProvider(objUserIdentity.PatientID.Value, objCurrentInvitation.ProviderID.Value);

                        if (!bAlreadyConnected)
                        {
                            //Marks the existing invitations, sent by this provider to the current patient, as blocked
                            PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(objUserIdentity.PatientID.Value, objCurrentInvitation.ProviderID.Value, true);

                            objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Pending;
                        }
                        else
                            objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Blocked;

                        objAvailableDetails.Update();
                    }
                }
            }

            AHAAPI.SessionInfo.ResetPatientInvitationSession();
        }

        protected void UpdatePanelSummaries_Load(object sender, EventArgs e)
        {

            int iiii = 0;

        }

        protected void btnPpeSummaryOk_Click(object sender, EventArgs e)
        {
            // this event merely has to exist, it doesn't have to do anything.
            int iiii = 0;
        }

        protected void btnPpeTrackerOk_Click(object sender, EventArgs e)
        {
            // this event merely has to exist, it doesn't have to do anything.
            int iiii = 0;
        }

    }
}