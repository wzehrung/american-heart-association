﻿#region Change Log
// ---------------------------------------------------------------------------------------
// ASPX Page: ConnectionCenter.aspx
// Code Behind: ConnectionCenter.aspx.cs
//
// Change Log: 20130731-HHC
//             Date 7/31/2013, Author: HHC, Desciption Participants working with messages
//             
// ----------------------------------------------------------------------------------------
#endregion 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

#region 20130731-HHC added using statement 
using AHAAPI;
using AHAHelpContent;
using Heart360Web.Controls.Patient;
#endregion 

namespace Heart360Web.Pages.Patient
{
    public partial class ConnectionCenter : H360PageHelper
    {
        private int iActiveAccordian;

        public int SetActiveAccordian
        {
            get { return iActiveAccordian; }
            set { iActiveAccordian = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            #region 20130731-HHC, added page init set module controler shared between the summary and detail component for efficiency

            Heart360Web.ConnectionCenterModuleController ccmc = new ConnectionCenterModuleController();
            SummaryConnectionCenter.ModuleController = ccmc;
            //DetailsConnectionCenter.ModuleController = ccmc;

            #endregion  

        }

        protected void btnPpeSummaryOk_Click(object sender, EventArgs e)
        {
            // There is already an asynchronous postback trigger for this button to refresh UpdatePanelSummaries.
            // That is the only update panel that needs refreshing.

        }

        protected void btnPpeTrackerOk_Click(object sender, EventArgs e)
        {
                // Although the popup is used by MyMessage, Connections, and RemindMe controls, 
                // and they don't always need to be updated together, there are some cases
                // where they do need to be updated together (user accepts a provider invitation.
                // So just update them all.
            this.UpdatePanelTrackerSummary.Update();
            this.MyMessages.UpdateMyMessagePanel().Update();
            this.Connections.GetUpdatePanel().Update();
            this.RemindMe.GetUpdatePanel().Update();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}