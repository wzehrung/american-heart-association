﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using AHAAPI;
using Microsoft.Health;
using Microsoft.Health.Web;
using System.Text;

namespace Heart360Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // This initializes our use of Health Vault
            HVWrapper.LifeCycle.Initialize();

            // pjb commented this out
            //RegisterRoutes();
        }

        private static void RegisterRoutes()
        {
            System.Web.Routing.RouteTable.Routes.Add(
                   "CheckBP", new System.Web.Routing.Route("CheckBP",
                                       new RouteHandler("~/CheckBP.aspx")));

            System.Web.Routing.RouteTable.Routes.Add(
                   "Facebook", new System.Web.Routing.Route("Facebook",
                                       new RouteHandler("~/Facebook.aspx")));
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}