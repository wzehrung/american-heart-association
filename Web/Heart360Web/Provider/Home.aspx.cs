﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//
// This web page exists for backwards compatibility with V5 only.
// Some Providers had book-marked the V5 Provider Portal, so we need to have a
// page that will simply redirect them to the new site.
//
namespace Heart360Web.Provider
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect(AHACommon.AHAAppSettings.ProviderPortal + "/provider/login.aspx");
        }
    }
}