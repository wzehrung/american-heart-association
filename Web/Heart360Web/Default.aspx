<%@ Page Language="C#" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" Codebehind="Default.aspx.cs" Inherits="Heart360Web._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain">

<style type="text/css">
    .popupStyleForgotPassword
    {
        border-top: 10px solid #D91C24;
        background: linear-gradient(to top, white 17%, rgb(242, 242, 242) 47%, white 80%) repeat scroll 0% 0% transparent !important;
        background-color: white;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.4);
        height: auto;  
        left: 50%;
        margin-left: -20%;
        padding: 0.5em 1em 1em 1em;
        position: absolute;
        top: 10%;
        width: 400px;
        z-index: 3002;
        overflow: visible;
    }
</style>

    <div id="main-content" class="row">

        <!-- ## BEGIN Campaign ## -->
        <div id="divCampaign" runat="server" visible="false">

            <div class="small-12 large-10 columns"> 
                <div class="small-12 large-12 columns"><h2><%=strCampaignTitle %></h2></div>
                <div class="small-12 large-12 columns">      
                </div>
            </div>   
     
            <div class="small-12 large-2 columns"> 
                <span style="float: right;"><%=strCampaignLogo %></span>
            </div>
              
        </div>
        <!-- ## END Campaign ## -->

        <!-- ## BEGIN Banner ## -->
        <section class="banner small-12 large-12 columns">
            <div id="register-slider">
                <ul data-orbit="" data-options="slide_number_class:hide; prev_class:hide; next_class:hide; resume_on_mouseout:true; timer_speed:7000;" class="orbit-slides-container">
                    <asp:Literal ID="litSlideSystem" runat="server" Visible="true"><li><img src="/images/slide-system.png" alt="slide-sys" width="1024" height="334" /></li></asp:Literal>
                    <asp:Literal ID="litSlide00" runat="server"></asp:Literal>
                    <asp:Literal ID="litSlide01" runat="server" Visible="true"><li><img src="/images/slide-1.png" alt="slide-1" width="1024" height="334" /></li></asp:Literal>
                    <asp:Literal ID="litSlide02" runat="server" Visible="true"><li><img src="/images/slide-2.png" alt="slide-2" width="1024" height="334" /></li></asp:Literal>
                    <asp:Literal ID="litSlide03" runat="server" Visible="true"><li><img src="/images/slide-3.png" alt="slide-3" width="1024" height="334" /></li></asp:Literal> 
                </ul>
            </div>  
        </section>
        <!-- ## END Banner ## -->

        <section class="info-portal-wrap small-12 large-12 column">
            <div class="info-portal full">

                <!-- ## BEGIN Title/Description ## -->
                <section class="small-12 large-7 columns h360info">
                    <h3><asp:Literal ID="litTitle" runat="server"></asp:Literal></h3>
                    <p><asp:Literal ID="litDescription" runat="server"></asp:Literal></p>
                    <p><strong><asp:Literal ID="litCampaignPromoTag" runat="server"></asp:Literal></strong></p>
                </section>
                <!-- ## END Title/Description ## -->


                <div id="login-box">

                    <div class="tabs-containerz">

                        <div class="tabs-headerz">
                            <div id="tab1action" data-tabcontent="tab1" class="tabz active"><%=GetGlobalResourceObject("Common", "Title_Login_PatientsParticipants")%></div>
                            <div id="tab2action" data-tabcontent="tab2" class="tabz"><%=GetGlobalResourceObject("Common", "Title_Login_Providers")%></div> 
                            <div id="tab3action" data-tabcontent="tab3" class="tabz"><%=GetGlobalResourceObject("Common", "Title_Login_Volunteers")%></div> 
                        </div>

                        <div class="tabs-contentz">

                            <div id="tab1" class="tab1 tab-contentz">
                                <p><%=GetGlobalResourceObject("Visitor", "Visitor_Welcome_Patient_Text")%></p>
                                
                                <div id="login-buttons">
                                    
                                    <a id="btnPatientForgot" class="forgot-password-front" runat="server" href="#"><%=GetGlobalResourceObject("Common", "Text_ForgotPassword")%></a>
                                    <a id="btnPatientRegister" class="gradient-button" runat="server" href="SignUp.aspx"><%=GetGlobalResourceObject("Common", "Text_NewAccount")%></a>
                                    <a id="btnPatientSignIn" class="gradient-button" runat="server" href="Pages/Patient/Dashboard.aspx"><%=GetGlobalResourceObject("Common", "Header_Text_SignIn")%></a>

                                </div>

                            </div>

                            <div id="tab2" class="tab2 tab-contentz">

                                <p><%=GetGlobalResourceObject("Common", "Text_Welcome_Provider")%></p>

                                <div id="login-buttons">
                                    <asp:TextBox ID="txtUserNameProvider" runat="server" CssClass="username" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtPasswordProvider" runat="server" CssClass="password" TextMode="Password" Visible="false"></asp:TextBox>
                                    <a id="btnProviderRegister" class="gradient-button" runat="server"><%=GetGlobalResourceObject("Common", "Text_NewAccount")%></a>
                                    <a id="btnProviderSignIn" class="gradient-button" runat="server"><%=GetGlobalResourceObject("Common", "Header_Text_SignIn")%></a>

                                </div>

                            </div>

                            <div id="tab3" class="tab3 tab-contentz">
                                
                                <p><%=GetGlobalResourceObject("Common", "Text_Welcome_Volunteer")%></p>

                                <div id="login-buttons">
                                    <asp:TextBox ID="txtUserNameVolunteer" runat="server" CssClass="username" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtPasswordVolunteer" runat="server" CssClass="password" TextMode="Password" Visible="false"></asp:TextBox>
                                    <a id="btnVolunteerRegister" class="gradient-button" runat="server"><%=GetGlobalResourceObject("Common", "Text_NewAccount")%></a>
                                    <a id="btnVolunteerSignIn" class="gradient-button" runat="server"><%=GetGlobalResourceObject("Common", "Header_Text_SignIn")%></a>

                                </div>

                            </div>
                        </div>
    
                    </div>

                </div>

            </div>

        </section>  

    </div>

    <asp:Panel ID="pnlForgotPassword" runat="server" Visible="true" style="display: none;">

        <div class="popupStyleForgotPassword">

            <div id="divPopupTrackerStyle" runat="server" visible="true">
                <div class="closePopup">
                    <asp:LinkButton ID="btnLinkClose" CssClass="closePopupText" runat="server" Text="CLOSE" />
                </div>
            </div>

            <div class="full">
	            <div class="large-12 small-12 columns">
		            <h3><%=GetGlobalResourceObject("Common", "Title_ForgotPassword")%></h3>
	            </div>

	            <div class="large-12 small-12 columns spb">
		            <p><label id="lblDescription" runat="server"><b><%=GetGlobalResourceObject("Common", "Text_ForgotPassword_Description")%></b></label></p>
	            </div>
            </div>

            <div class="boxes">
                <asp:LinkButton runat="server" ID="lnkContinue" OnClick="lnkContinue_Click" CssClass="gray-button full"><em><strong><%=GetGlobalResourceObject("Common", "Text_Continue")%></strong></em></asp:LinkButton>
            </div>

        </div>

    </asp:Panel>

<asp:ModalPopupExtender
    ID="mpeForgotPassword"
    runat="server"
    TargetControlID="btnPatientForgot"
    PopupControlID="pnlForgotPassword" CancelControlID="btnLinkClose" OkControlID="btnLinkClose" BackgroundCssClass="pageBackground">
</asp:ModalPopupExtender>

</asp:Content>