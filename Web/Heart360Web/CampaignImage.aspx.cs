﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web
{
    public partial class CampaignImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string header = Request.Headers["If-Modified-Since"];

            if (header != null)
            {
                Response.StatusCode = 304;
                Response.SuppressContent = true;
                return;
            }

            if (!string.IsNullOrEmpty(Request["CID"]) && !string.IsNullOrEmpty(Request["LID"]) && !string.IsNullOrEmpty(Request["Type"]))
            {
                if ((Request["Type"].ToLower() == "partner-logo"))
                {
                    AHAHelpContent.CampaignPartnerLogo campaignPartnerLogo = AHAHelpContent.Campaign.GetCampaignPartnerLogobyID(Convert.ToInt32(Request["CID"]), Convert.ToInt32(Request["LID"]));
                    if (campaignPartnerLogo != null && campaignPartnerLogo.PartnerLogo != null && campaignPartnerLogo.IsActive)
                    {
                        Response.Clear();
                        Response.Buffer = false;
                        Response.Cache.SetExpires(DateTime.Now.AddDays(30));
                        Response.Cache.SetMaxAge(new TimeSpan(30, 0, 0, 0));
                        Response.Cache.SetLastModified(DateTime.Now);
                        Response.Cache.SetCacheability(HttpCacheability.Public);
                        Response.Cache.SetValidUntilExpires(false);

                        Response.AddHeader("Content-Length", campaignPartnerLogo.PartnerLogo.Length.ToString());
                        Response.ContentType = campaignPartnerLogo.PartnerLogoContentType;
                        Response.AddHeader("content-disposition", "inline; filename=Partner_Logo_" + campaignPartnerLogo.PartnerLogoVersion);

                        Response.BinaryWrite(campaignPartnerLogo.PartnerLogo);
                        Response.End();
                    }
                }
                else if ((Request["Type"].ToLower() == "partner-image"))
                {
                    AHAHelpContent.CampaignPartnerImage campaignPartnerImage = AHAHelpContent.Campaign.GetCampaignPartnerImageByID(Convert.ToInt32(Request["CID"]), Convert.ToInt32(Request["LID"]));
                    if (campaignPartnerImage != null && campaignPartnerImage.PartnerImage != null && campaignPartnerImage.IsActive)
                    {
                        Response.Clear();
                        Response.Buffer = false;
                        Response.Cache.SetExpires(DateTime.Now.AddDays(30));
                        Response.Cache.SetMaxAge(new TimeSpan(30, 0, 0, 0));
                        Response.Cache.SetLastModified(DateTime.Now);
                        Response.Cache.SetCacheability(HttpCacheability.Public);
                        Response.Cache.SetValidUntilExpires(false);

                        Response.AddHeader("Content-Length", campaignPartnerImage.PartnerImage.Length.ToString());
                        Response.ContentType = campaignPartnerImage.PartnerImageContentType;
                        Response.AddHeader("content-disposition", "inline; filename=Partner_Image_" + campaignPartnerImage.PartnerImageVersion);

                        Response.BinaryWrite(campaignPartnerImage.PartnerImage);
                        Response.End();
                    }
                }
            }

        }
    }
}