$(document).ready(function () {

    jQuery('.search-btn').on('click', function () {

        //Add the search field data into the hidden field
        jQuery('#q').attr('value', jQuery('#q1').attr('value'));

        //console.log("Setting Search Value " + jQuery('#q1').attr('value'));

        //now that the value has been assigned submit the form
        jQuery('#Form1').submit();

    });

    //disable links on diabels asp tab
    $('.ajax__tab_header .ajax__tab_disabled .ajax__tab_outer .ajax__tab_inner .ajax__tab_tab').on('click', function () { return false; });

    //ACCORDION BUTTON ACTION	

    var cur_stus;

    //close all on default
    $('.accordion .content').hide();

    //Add custom Data attribute as closed to keep status
    $('.accordion .toggle-btn').data('open-closed-status', 'closed');

    //Set the on click event to all accordions using the .toggle-btn structure
    $('.accordion .toggle-btn').on('click', function () {

        //When the toggle-btn  is clicked we remove our dt-seleted class
        $('.accordion .toggle-btn').removeClass('dt-selected');

        //Store Current status of the clicked element
        cur_stus = $(this).data('open-closed-status');

        if (cur_stus !== "open") {

            //reset everything - content and attribute
            $('.accordion .content').slideUp();
            $('.accordion .toggle-btn').data('open-closed-status', '');

            //then open the clicked data
            $(this).next().slideDown();
            $(this).data('open-closed-status', 'open');

            $(this).addClass('dt-selected');

        }
        //Remove else part if do not want to close the current opened data
        else {

            $(this).next().slideUp();
            $(this).data('open-closed-status', '');
            $(this).toggleClass("dt-active");
            $(this).removeClass('dt-selected');


        }

        return false;

    });


    var assignTabActions = function () {

        //check for tab declaration
        if ($('.tabs-headerz').length > 0 && $('.tabs-contentz').length > 0) {

            $('.tabz').unbind();
            $('.tabz').each(function () {

                var connectedTab = $(this).attr('data-tabcontent');

                 

                if ($('.' + connectedTab).length > 0) {
                    

                    if ($(this).hasClass('active')) {
                        $('.' + connectedTab).addClass('active').show();
                    }

                   //  $('.tab-contentz.active').show();

                    $(this).on('click', function () {
                        if ($(this).hasClass('active')) {
                            //Do nothing
                        } else {
                            $('.tab-contentz').removeClass('active');
                            $('.tab-contentz').hide();
                            $('.tabz').removeClass('active');
                            $(this).addClass('active');
                            $('.' + connectedTab).addClass('active').show();
                        }
                    });

                } else {
                    // console.log("Tab content specified is missing");
                }

            });
        }
    }

    
    assignTabActions();


    // PJB - to enable an accordion to be opened on page load, we will trigger the click event on any
    // 'toggle-button' divs with the added class of 'pre-selected'. This tag will have been added
    // by ASP.NET.
    $('.accordion .pre-selected').trigger('click');


    $('.edit-content').hide();
    $('.edit-trigger').on('click',
	function () {
	    $(this).toggleClass('trigger-selected');
	    $(this).nextAll().toggle();
	});

    $('a.pop-small').on('click', function (event) {
        event.preventDefault();
        var modal = $('<div>').addClass('reveal-modal small').appendTo('body');
        $.get($(this).attr('href'), function (data) {
            modal.empty().html(data).append('<a class="close-reveal-modal">CLOSE</a>').foundation('reveal', 'open');
        });
    });


    // VIEWPORT CLASS EXCHANGE FOR DOM RESPONSIVE CHANGES

    if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
        var viewportmeta = document.querySelector('meta[name="viewport"]');
        if (viewportmeta) {
            viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
            document.body.addEventListener('gesturestart', function () {
                viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
            }, false);
        }
    }

    function do_resize() {
        var windowSize = $(window).width();

        if (windowSize <= 320) { //iPHONE AND OLDER DROID MODELS
            $('body').removeClass('new-phone mini-ipad port-ipad land-ipad').addClass('old-phone');
            $('#meta-social-wrap').removeClass('columns');
            $('#meta-social-wrap, #utility, .footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').addClass('full');

            $('#my-center-large, #my-profile-large, #my-pressure-large, #my-weight-large, #my-activity-large, #my-glucose-large, #my-cholesterol-large, #my-medications-large, #my-lifecheck-large').removeClass('tile-large').addClass('tile');
            /* $('#my-center-large, #my-profile-large, #my-pressure-large, #my-weight-large, #my-activity-large, #my-glucose-large, #my-cholesterol-large, #my-medications-large, #my-lifecheck-large').addClass('tile'); */

            $('.footer h4.master-btn, .footer .about h5:first-of-type, .footer .causes h5:first-of-type, .footer .sites h5:first-of-type, .footer .contact h5:first-of-type, .footer .legal h5:first-of-type').addClass('trigger full');

            $('.search-field').addClass('phone');

            $('.footer ul.master-colapse, .footer .about ul, .footer .causes ul, .footer .sites ul, .footer .contact > ul, .footer .legal ul').addClass('next-content full');
            $('.next-content').hide();
        }
        else if (windowSize <= 480) { //DROID S4 PLUS

            $('body').removeClass('old-phone mini-ipad port-ipad land-ipad').addClass('new-phone');
            $('#meta-social-wrap').removeClass('columns');

            $('.search-field').addClass('phone');

            $('#meta-social-wrap, #utility, .footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').addClass('full');

            $('#my-center-large, #my-profile-large, #my-pressure-large, #my-weight-large, #my-activity-large, #my-glucose-large, #my-cholesterol-large, #my-medications-large, #my-lifecheck-large').removeClass('tile-large').addClass('tile');

            $('.footer h4:first-of-type, .footer .about h5:first-of-type, .footer .causes h5:first-of-type, .footer .sites h5:first-of-type, .footer .contact h5:first-of-type, .footer .legal h5:first-of-type').addClass('trigger full');

            $('.footer ul.master-colapse, .footer .about ul, .footer .causes ul, .footer .sites ul, .footer .contact ul, .footer .legal ul').addClass('next-content full');
            $('.next-content').hide();


        }
        else if (windowSize <= 640) { //iPAD MINI AND KINDLE FIRE
            $('body').removeClass('old-phone port-ipad land-ipad large-screen').addClass('mini-ipad');
            $('#meta-social-wrap').removeClass('columns');
            $('#meta-social-wrap, #utility, .footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').addClass('full');

            $('#my-center-large, #my-profile-large, #my-pressure-large, #my-weight-large, #my-activity-large, #my-glucose-large, #my-cholesterol-large, #my-medications-large, #my-lifecheck-large').removeClass('tile').addClass('tile-large');

            $('.footer h4.master-btn, .footer .about h5:first-of-type, .footer .causes h5:first-of-type, .footer .sites h5:first-of-type, .footer .contact h5:first-of-type, .footer .legal h5:first-of-type').addClass('trigger full');

            $('.footer ul.master-colapse, .footer .about ul, .footer .causes ul, .footer .sites ul, .footer .contact ul, .footer .legal ul').addClass('next-content full');
            $('.next-content').hide();

            $('.search-field').addClass('phone');

        }
        else if (windowSize <= 768) { //IPAD AND LARGER TABLETS
            $('body').removeClass('old-phone new-phone mini-ipad land-ipad large-screen').addClass('port-ipad');
            $('#meta-social-wrap').addClass('columns');
            $('#meta-social-wrap, #utility, .footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').removeClass('full');

            $('.footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').addClass('full');
            $('.footer h4.master-btn, .footer .about h5:first-of-type, .footer .causes h5:first-of-type, .footer .sites h5:first-of-type, .footer .contact h5:first-of-type, .footer .legal h5:first-of-type').addClass('trigger full');

            $('.footer ul.master-colapse, .footer .about ul, .footer .causes ul, .footer .sites ul, .footer .contact ul, .footer .legal ul').addClass('next-content full');
            $('.next-content').hide();

        }
        else if (windowSize <= 1024) { //IPAD LANDSCAPE AND SMALL LAPTOP SCREENS
            $('.search-field').removeClass('phone');
            $('body').removeClass('old-phone new-phone mini-ipad port-ipad large-screen').addClass('land-ipad');
            $('#meta-social-wrap').addClass('columns');
            $('#meta-social-wrap, #utility, .footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').removeClass('full');

            $('.footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').addClass('full');
            $('.footer h4.master-btn, .footer .about h5:first-of-type, .footer .causes h5:first-of-type, .footer .sites h5:first-of-type, .footer .contact h5:first-of-type, .footer .legal h5:first-of-type').addClass('trigger full');

            $('.footer ul.master-colapse, .footer .about ul, .footer .causes ul, .footer .sites ul, .footer .contact ul, .footer .legal ul').addClass('next-content full');
            $('.next-content').hide();
        }
        else if (windowSize >= 1025) { //LARGE SCREENS PLUS
            $('body').removeClass('old-phone new-phone mini-ipad port-ipad land-ipad').addClass('large-screen');
            $('#meta-social-wrap').addClass('columns');
            $('#meta-social-wrap, #utility, .footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').removeClass('full');

            $('.footer .master-colapse .about, .footer .master-colapse .causes, .footer .master-colapse .sites, .footer .master-colapse .contact').removeClass('full').show();
            $('.footer h4.master-btn, .footer .about h5:first-of-type, .footer .causes h5:first-of-type, .footer .sites h5:first-of-type, .footer .contact h5:first-of-type, .footer .legal h5:first-of-type').removeClass('trigger full');
            $('.footer ul.master-colapse, .footer .about ul, .footer .causes ul, .footer .sites ul, .footer .contact ul, .footer .legal ul').removeClass('next-content full').show();

        }
        return false;
    }

    $(window).resize(function () {
        do_resize();
    });

    do_resize();

    // END OF VIEWPORT CLASSES 


    $('.next-content').hide();
    $('.trigger').click(
	function () {
	    $(this).toggleClass('trigger-selected');
	    $(this).next('.next-content').slideToggle();
	});

    //SEARCH AREA CONVERTERS
    $('.phone.search-field').hide();

    $('#search-icon.phone').click(function () {
        //Add css margin to field
        $('.hide-lt-tablet').slideToggle('fast');
    });

    //PHONE MENU TOGGLES
    $('#phone-nav-trigger').click(function () {
        $('nav#phone-nav').slideToggle();
    });


    //FEATURED LIGHTBOX OVERLAY EFFECTS 
    window.fireWindow = function (tempObject) {
        var thisModal = $('#' + tempObject).attr("data-name");
        //console.log("data-name " + thisModal);
        $('.shadowbox.' + thisModal).animate({
            'opacity': '1.00'
        }, 300, 'linear');
        $('.modal-bg.' + thisModal).animate({
            'opacity': '1.00'
        }, 300, 'linear');
        $('.modal-wrap.' + thisModal).animate({
            'opacity': '1.00'
        }, 300, 'linear');
        $('.shadowbox.' + thisModal).css('display', 'block');
        $('.modal-bg.' + thisModal).css('display', 'block');
        $('.modal-wrap.' + thisModal).css('display', 'block');
    };

    window.ShowPopup = function (btnId, iframeId, iframeUrl, styledivId, styledivClass, iCloseUrl) {
        var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        if (width < 769) {
            window.location = iframeUrl + '&closeurl=' + iCloseUrl;
        } else if (navigator.userAgent.match(/iPad/i) !== null) {
            // Should Detect Ipad
            window.location = iframeUrl + '&closeurl=' + iCloseUrl;
        } else {
            document.getElementById(styledivId).className = styledivClass;
            document.getElementById(iframeId).src = iframeUrl;
            window.fireWindow(btnId);
        }
        return false;
    };

    /*
    $('.close-modal').click(function () {
    close_modal();
    });
    */

    window.close_modal = function () {
        $('.shadowbox,.modal-bg,.modal-wrap').animate({
            'opacity': '0'
        }, 300, 'linear', function () {
            $('.shadowbox,.modal-bg,.modal-wrap').css('display', 'none');
        });

        return false;
    };

    $('.show-sub-menu').click(function () {
        $('ul.sub-menu').slideToggle('fast');
    });


    //NESTED TABS 
    $('.tab-content').hide();
    $('.tab-content.active-content').show();

    $('.tab-btn').click(function () {

        $('.tab-btn').removeClass('active-tab');

        $('.tab-content').removeClass('active-content');

        $('.tab-content').hide();

        $(this).addClass('active-tab');

        var thisTab = $(this).attr("data-name");
        //console.log(thisTab);
        $('.tab-content.' + thisTab).show().addClass('active-content');
    });

    $('.tab-content-inv').hide();
    $('.tab-content-inv.active-content').show();

    $('.tab-btn-inv').click(function () {
        $('.tab-btn-inv').removeClass('active-tab');
        $('.tab-content-inv').removeClass('active-content');
        $('.tab-content-inv').hide();

        $(this).addClass('active-tab');

        var thisTab = $(this).attr("data-name");
        //console.log(thisTab);
        $('.tab-content-inv.' + thisTab).show().addClass('active-content');
    });

});

//Global function for pagination
window.paginatePages = function () {
    // TABLE PAGINATION
    $('table.paginated').each(function () {
    	// if() {
	        var currentPage = 0;
	        var numPerPage = 6;
	        var $table = $(this);
	        $table.bind('repaginate', function () {
	            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
	        });
	        $table.trigger('repaginate');
	        var numRows = $table.find('tbody tr').length;
	        var numPages = Math.ceil(numRows / numPerPage);
	        var $pager = $('<div class="pager"></div>');
	        
	        
	        //Moved function outside of loop
			var tempEventFunction = function (t_event) {
	            currentPage = t_event.data.newPage;
	            $table.trigger('repaginate');
	            $(this).addClass('active').siblings().removeClass('active');
	        };
	        
	        
	        for (var page = 0; page < numPages; page++) {
	            $('<span class="page-number"></span>').text(page + 1).bind('click', { newPage: page }, tempEventFunction).appendTo($pager).addClass('clickable');
	        }
	        
	        
	        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
	        
	        $pager.prepend('<span class="page-start-text">Page: </span>');
        
        // }
        
    });
};


window.checkForAccordionSelected = function() {
	//Check if any elements have the following class
	if($('.trigger-selected')[0]) {
	
		//If they do make sure they are display block
		if($('.trigger-selected').parent().nextAll().is(':visible')) {
			
			// Do nothing
						
		} else {
			
			//display block
			$('.trigger-selected').parent().nextAll().slideToggle();
			
		}
		
	} else {
		
	}

};



//Refresh css when content comes back 

window.hideShowPanels = function () {
    //Unbind elemnts first
    $('.inner-trigger').off();

    $('.inner-content').hide();

    $('.inner-trigger').on('click', function () {

        $(this).toggleClass('trigger-selected');
        $(this).closest('.trigger-wrap').nextAll().slideToggle();

    });

    //console.log('Ran function panels');
};

window.addKeyDownLoader = function () {
    if ($("#onkeyloader").length === 0) { $('#popup-title').append('<img src="../../images/ajax-loader.gif" id="onkeyloader" />'); }
};

window.removeKeyDownLoader = function () {
    $('#onkeyloader').remove();
};


window.messageUpdater = function(msgid, usrid, domrefid) {
	
	var request = $.ajax({
		url: "/Pages/Patient/PostHandler.ashx",
		type: "POST",
		data: { ReadMail : msgid, UserId : usrid }
	});
	
	
	request.done(function( msg ) {
		
		var jsonStuff = $.parseJSON(msg);
		
		
        //Update number of unread messages
        $('.tile-data .messages').html(jsonStuff[0].msgunread);
		
        // Removed unread classes from dom elements
        $('#' + domrefid).children('.msg-unread').removeClass('msg-unread');

        // remove onclick event
        $('#' + domrefid).attr('onclick','');
	});

};

window.setActiveMessageClass = function(htmlid) {
	
		$('.tab-btn').removeClass('active-btn');
		$(htmlid).addClass('active-btn');
		console.log(htmlid);
}













