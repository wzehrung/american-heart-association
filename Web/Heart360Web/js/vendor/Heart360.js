﻿/*Start MLC*/
// JScript source code
analogMeter = function () {
    this.containerObj = document.getElementById(arguments[0]);
    this.analogMeter = document.createElement('div');
    this.analogMeter.className = "analogMeter";
    this.containerObj.appendChild(this.analogMeter);
    this.analogMeterPointer = document.createElement('div');
    this.analogMeterPointer.className = "analogMeterPointer";
    this.analogMeter.appendChild(this.analogMeterPointer);
    this.analogMeterShadowValue = document.createElement('div');
    this.analogMeterShadowValue.className = "analogMeterShadowValue";
    this.analogMeter.appendChild(this.analogMeterShadowValue);
    this.analogMeterShadowValue.innerHTML = "0.0";
    this.analogMeterValue = document.createElement('div');
    this.analogMeterValue.className = "analogMeterValue";
    this.analogMeter.appendChild(this.analogMeterValue);
    this.analogMeterValue.innerHTML = "0.0";
    this.analogMeterValue.style.border = "none";
    this.analogMeterCenter = document.createElement('div');
    this.analogMeterCenter.className = "analogMeterCenter";
    this.analogMeter.appendChild(this.analogMeterCenter);
    this.reading = 0;
    this.waitAnimation = false;
};
analogMeter.prototype.setValue = function (val) {

    if (val > 10) val = 10;
    if (val < 0) val = 0;
    var currentReading = parseInt(val * 80 * 10);


    if (this.waitAnimation) {
        clearInterval(this.slideIntervalHandle);

    }
    this.movePointerTo(currentReading);

};
analogMeter.prototype.movePointerTo = function (val) {
    var oInstance = this;
    var startPoint = this.reading;
    var speed = 5;
    this.slideIntervalHandle = setInterval(function () {
        var pointAdjustMent = 0;
        if (oInstance.reading > 4000) {
            pointAdjustMent++;
        }
        if (oInstance.reading > 7600) {
            pointAdjustMent++;
        }
        oInstance.waitAnimation = true;
        var bgString = (-1 * startPoint - pointAdjustMent) + "px 0px";
        oInstance.analogMeterPointer.style.backgroundPosition = bgString;
        if (startPoint == val) {
            clearInterval(oInstance.slideIntervalHandle);
            oInstance.reading = val;
            oInstance.waitAnimation = false;
        }
        else {
            if (val > oInstance.reading) {
                startPoint += 80;
            } else {
                startPoint -= 80;
            }
            oInstance.reading = startPoint;
        }
        var displayValue = (oInstance.reading / 800);
        if ((displayValue * 10 % 10) == 0 && displayValue != 10) {
            displayValue = displayValue + ".0";
        }
        oInstance.analogMeterValue.innerHTML = displayValue;
        oInstance.analogMeterShadowValue.innerHTML = displayValue;
    }, speed);
}
/*End MLC*/