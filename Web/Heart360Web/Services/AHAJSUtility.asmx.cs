﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;

using System.Collections.Generic;
using Microsoft.Health;
using AHAAPI;

namespace Heart360Web.Services
{
    /// <summary>
    /// Summary description for AHAJSUtility
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AHAJSUtility : System.Web.Services.WebService
    {
        [WebMethod]
        public bool LogJavascriptError(string ErrorName,string ErrorMessage,string ErrorLocation)
        {
            try
            {
                GRCBase.ErrorLogHelper.LogError_Javascript(ErrorName, ErrorMessage, ErrorLocation);
                return true;
            }
            catch (Exception ex)
            {
                GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
                return false;
            }
        }

        [WebMethod]
        public string[] GetRxSuggesstions(string prefixText)
        {
            return RxNorm.RxUtility.GetAutoSuggest(prefixText);
        }
    }
}
