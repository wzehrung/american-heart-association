﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Web
{
    public partial class TermsOfUse : System.Web.UI.Page
    {
        protected string strFooter = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Heart360Global.AHAPage objPage = Heart360Global.Main.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);
            if (objPage.IsProviderPage)
            {
                strFooter = AHACommon.AHAAppSettings.Disclaimer_Provider_HTML;
            }
            else
            {
                strFooter = AHACommon.AHAAppSettings.Disclaimer_Patient_HTML;
            } 
        }
    }
}