﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using HVManager;
using Microsoft.Health;
using AHAAPI;

namespace Heart360Web.MasterPages
{
    public partial class PatientPopup : System.Web.UI.MasterPage
    {
        protected string AppVirtualPath = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpRuntime.AppDomainAppVirtualPath != "/")
            {
                AppVirtualPath = HttpRuntime.AppDomainAppVirtualPath;
            }
        }
    }
}