﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAAPI;
using GRCBase;      // TODO: GET RID OF THIS!!!!!

namespace Heart360Web.MasterPages
{
    public partial class PatientPrinterFriendly : System.Web.UI.MasterPage
    {
        public string strReportHeading = string.Empty;
        protected string strFooter = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
            {
                strFooter = LanguageResourceHelper.Disclaimer_Provider_HTML;
            }
            else
            {
                strFooter = LanguageResourceHelper.Disclaimer_Patient_HTML;
            }


            string strName = string.Empty;

            if (!String.IsNullOrEmpty(Request["daterange"]))
            {
                if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.LastWeek.ToString()) == 0)
                {
                    _GetPeriodData(AHACommon.DataDateReturnType.LastWeek);
                    //_GetPeriodData(-7);
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.LastMonth.ToString()) == 0)
                {
                    _GetPeriodData(AHACommon.DataDateReturnType.LastMonth);
                    //_GetPeriodData(-30);
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last3Months.ToString()) == 0)
                {
                    _GetPeriodData(AHACommon.DataDateReturnType.Last3Months);
                    //_GetPeriodData(-90);
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last6Months.ToString()) == 0)
                {
                    _GetPeriodData(AHACommon.DataDateReturnType.Last6Months);
                    //_GetPeriodData(-180);
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last9Months.ToString()) == 0)
                {
                    _GetPeriodData(AHACommon.DataDateReturnType.Last9Months);
                    //_GetPeriodData(-180);
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.LastYear.ToString()) == 0)
                {
                    _GetPeriodData(AHACommon.DataDateReturnType.LastYear);
                    //_GetPeriodData(-180);
                }
                else if (Request["daterange"].CompareTo(AHACommon.DataDateReturnType.Last2Years.ToString()) == 0)
                {
                    _GetPeriodData(AHACommon.DataDateReturnType.Last2Years);
                    //_GetPeriodData(-180);
                }
                else
                {
                    if (!String.IsNullOrEmpty(Request["pg"]) && !String.IsNullOrEmpty(Request["rg"]))
                    {
                        HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(new Guid(System.Web.HttpContext.Current.Request["pg"]), new Guid(System.Web.HttpContext.Current.Request["rg"]));
                        HVManager.PersonalDataManager.PersonalItem obj = mgr.PersonalDataManager.Item;
                        if (obj != null)
                        {
                            strName = obj.FullName;
                        }
                    }
                    else
                    {
                        strName = CurrentUser.FullName;
                    }
                    strReportHeading = string.Format(GetGlobalResourceObject("Reports", "Text_Print_ReportForUser").ToString(), strName);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Request["pg"]) && !String.IsNullOrEmpty(Request["rg"]))
                {
                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(new Guid(System.Web.HttpContext.Current.Request["pg"]), new Guid(System.Web.HttpContext.Current.Request["rg"]));
                    HVManager.PersonalDataManager.PersonalItem obj = mgr.PersonalDataManager.Item;
                    if (obj != null)
                    {
                        strName = obj.FullName;
                        strReportHeading = string.Format(GetGlobalResourceObject("Reports", "Text_Print_ReportForUser").ToString(), "Heart360", strName);
                    }
                }

            }

            //added for Provider - Custom Practice Report, Standard Practice Report
            if (!string.IsNullOrEmpty(Request["ProvRpt"]))
            {
                string strReportType = Request["ProvRpt"];
                //
                if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
                {
                    if (strReportType.Equals("CPR"))
                    {
                        strReportHeading = GetGlobalResourceObject("Reports", "Text_Print_CustomPracticeReport").ToString();
                    }
                    else if (strReportType.Equals("SPR"))
                    {
                        strReportHeading = GetGlobalResourceObject("Reports", "Text_Print_StandardPracticeReport").ToString();
                    }
                }
            }

            Heart360Global.AHAPage objPage = Heart360Global.Main.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);
            if (objPage != null)
            {
                Page.Title = objPage.PageTitle;
            }
        }

        private void _GetPeriodData(AHACommon.DataDateReturnType daterange)
        {
            DateTime startDate = DateTime.Today;
            switch (daterange)
            {
                case AHACommon.DataDateReturnType.Last2Years:
                    startDate = DateTime.Today.AddYears(-2);
                    break;
                case AHACommon.DataDateReturnType.Last3Months:
                    startDate = DateTime.Today.AddDays(-90);
                    break;
                case AHACommon.DataDateReturnType.Last6Months:
                    startDate = DateTime.Today.AddDays(-180);
                    break;
                case AHACommon.DataDateReturnType.Last9Months:
                    startDate = DateTime.Today.AddDays(-270);
                    break;
                case AHACommon.DataDateReturnType.LastMonth:
                    startDate = DateTime.Today.AddDays(-30);
                    break;
                case AHACommon.DataDateReturnType.LastWeek:
                    startDate = DateTime.Today.AddDays(-7);
                    break;
                case AHACommon.DataDateReturnType.LastYear:
                    startDate = DateTime.Today.AddYears(-1);
                    break;
            }
            //DateTime startDate = DateTime.Today.AddDays(daterange);
            DateTime endDate = DateTime.Today;
            string strName = string.Empty;
            if (!String.IsNullOrEmpty(Request["pg"]) && !String.IsNullOrEmpty(Request["rg"]))
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(new Guid(System.Web.HttpContext.Current.Request["pg"]), new Guid(System.Web.HttpContext.Current.Request["rg"]));
                HVManager.PersonalDataManager.PersonalItem obj = mgr.PersonalDataManager.Item;
                if (obj != null)
                {
                    strName = obj.FullName;
                }
            }
            else
            {
                strName = CurrentUser.FullName;
            }
            //string strStartMonth = GRCBase.DateTimeHelper.GetMonthName(startDate.Month);
            string strStartMonth = startDate.ToString("MMMM");
            string strStartDay = startDate.Day.ToString();
            string strStartYear = startDate.Year.ToString();
            //string strEndMonth = GRCBase.DateTimeHelper.GetMonthName(endDate.Month);
            string strEndMonth = endDate.ToString("MMMM");
            string strEndDay = endDate.Day.ToString();
            string strEndYear = endDate.Year.ToString();
            strReportHeading = string.Format(GetGlobalResourceObject("Reports", "Text_Print_ReportPeriod").ToString(), "Heart360", strName, strStartMonth, strStartDay, strStartYear, strEndMonth, strEndDay, strEndYear);

            Page.Title = strReportHeading;
        }


    }
}