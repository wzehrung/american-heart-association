﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AHAAPI;
using AHACommon;
using GRCBase;

namespace Heart360Web.MasterPages
{
    public partial class Visitor : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            _PopulateLanguages();
        }

        private void _PopulateLanguages()
        {
            //ddlLanguage.DataSource = AHAHelpContent.Language.GetAllLanguages(AHAAPI.H360RequestContext.GetContext().SelectedLanguage.LanguageID);
            //ddlLanguage.DataTextField = "LanguageName";
            //ddlLanguage.DataValueField = "LanguageID";
            //ddlLanguage.DataBind();

            //if (!Page.IsPostBack)
            //{
            //    ddlLanguage.SelectedValue = AHAAPI.H360RequestContext.GetContext().SelectedLanguage.LanguageID.ToString();
            //}
        }

        protected override void OnInit(EventArgs e)
        {
            UserIdentityContext userIdentityContext = SessionInfo.GetUserIdentityContext();
            if (userIdentityContext == null
                && AHAAppSettings.IsSSLEnabled
                && Request.RequestType == "GET")
            {
                if (!System.Web.HttpContext.Current.Request.IsSecureConnection)
                {
                    Response.Redirect("https://" + GRCBase.UrlUtility.ServerName + GRCBase.UrlUtility.RelativeCurrentPageUrlWithQV);
                    Response.End();
                    return;
                }
            }

            string strUrl = GRCBase.UrlUtility.FullCurrentPageUrl.ToLower();
            if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn()
               && !strUrl.EndsWith(AHAAppSettings.ErrorPageRelativePath.ToLower())
               && !strUrl.EndsWith(AHAAppSettings.FileNotFoundPageRelativeURL.ToLower()))
            {
                Response.Redirect(AHAAPI.Provider.ProviderPageUrl.HOME, true);
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AHAAPI.H360Utility.AddImmediateExpiryHeadersToResponse();

            Heart360Global.AHAPage objPage = Heart360Global.Main.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);
            if (objPage != null)
            {
                Page.Title = objPage.PageTitle;
            }

            if (UrlUtility.FullCurrentPageUrl.ToLower().Contains("/visitor/prereg.aspx"))
            {
                //aCreateAccount.HRef = "/Patient/MyHome.aspx";
            }
        }

        protected void OnLanguageChanged(object sender, EventArgs e)
        {
            //AHAHelpContent.Language objLanguage = AHAHelpContent.Language.GetLanguageByID(Convert.ToInt32(ddlLanguage.SelectedValue));
            //AHAAPI.LanguageHelper.SetLocaleAndCulture(objLanguage.LanguageLocale);
            Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
        }
    }
}
