﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using HVManager;
using Microsoft.Health;
using AHAAPI;



namespace Heart360Web.MasterPages
{
    public partial class Patient : System.Web.UI.MasterPage, IHealthApplicationInfo
    {
        protected string WelcomeTextTitle = string.Empty;
        protected string WelcomeTextName = string.Empty;

        private string GetModulePopupClass( Heart360Web.PatientPortal.MODULE_ID _modid )
        {
            string result = string.Empty;

            switch (_modid)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_GLUCOSE:
                    result = "bg-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_BLOOD_PRESSURE:
                    result = "bp-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CHOLESTEROL:
                    result = "ch-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                    result = "cc-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                    result = "md-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PHYSICAL_ACTIVITY:
                    result = "pa-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    result = "pf-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_WEIGHT:
                    result = "wt-popup";
                    break;

                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_UNKNOWN:
                default:
                    break;
            }

            return result;
        }

        private bool IsRedTheme(Heart360Web.PatientPortal.MODULE_ID _modid)
        {
            bool retval = true;

            switch (_modid)
            {
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_CONNECTION_CENTER:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_LIFE_CHECK:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_MEDICATIONS:
                case Heart360Web.PatientPortal.MODULE_ID.MODULE_ID_PROFILE:
                    retval = false;
                    break;

                default:
                    break;
            }
            return retval;
        }

        public string GetTrackerPopupOnClientClickContent(string _url, Heart360Web.PatientPortal.MODULE_ID _modid )
        {
            string result = string.Empty;

            Control updatePanel = this.ContentPlaceHolderMain.FindControl("UpdatePanelPopups");

            if (updatePanel != null)
            {
                Control btn = updatePanel.FindControl("popupTrackerTrigger");
                Control iframe = updatePanel.FindControl("popupTrackerIframe");
                Control stylediv = updatePanel.FindControl("divPopupTrackerStyle");

                if (btn != null && iframe != null && stylediv != null)
                {
                    string cssclass = (IsRedTheme(_modid)) ? "modal-wrap cpopuptracker page-red" : "modal-wrap cpopuptracker page-blue";
                    cssclass += " " + GetModulePopupClass(_modid);
                    string closeurl = Request.Url.AbsoluteUri;
                    result = "return window.ShowPopup( '" + btn.ClientID + "','" + iframe.ClientID + "', '" + _url + "','" + stylediv.ClientID + "','" + cssclass + "', '" + closeurl + "' );";
                }
            }
            return result;
        }

        public string GetSummaryPopupOnClientClickContent(string _url, Heart360Web.PatientPortal.MODULE_ID _modid)
        {
            string result = string.Empty;

            Control updatePanel = this.ContentPlaceHolderMain.FindControl("UpdatePanelPopups");

            if (updatePanel != null)
            {
                Control btn = updatePanel.FindControl("popupSummaryTrigger");
                Control iframe = updatePanel.FindControl("popupSummaryIframe");
                Control stylediv = updatePanel.FindControl("divPopupSummaryStyle");

                if (btn != null && iframe != null && stylediv != null)
                {
                    string cssclass = (IsRedTheme(_modid)) ? "modal-wrap cpopupsummary page-red" : "modal-wrap cpopupsummary page-blue";
                    cssclass += " " + GetModulePopupClass(_modid);
                    string closeurl = Request.Url.AbsoluteUri;
                    result = "return window.ShowPopup( '" + btn.ClientID + "','" + iframe.ClientID + "', '" + _url + "','" + stylediv.ClientID + "','" + cssclass + "', '" + closeurl + "' );";
                }
            }
            return result;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            // Set the page title (titles defined in XMLFiles)
            Heart360Global.AHAPage objPage = Heart360Global.Main.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);
            if (objPage != null)
            {
                Page.Title = objPage.PageTitle;
            }

            // This was taken from AHAWeb/MasterPages/PatientNew.Master.cs
            AHAAPI.H360Utility.AddImmediateExpiryHeadersToResponse();

            //first do the polka processing
            PolkaDataProcessor.Process();

            try
            {
                DBSyncHelper.PerformSync();
            }
            catch// (Exception ex)
            {
                throw;
                //GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
            }

            AHAAPI.UserIdentityContext objUserIdentity = AHAAPI.SessionInfo.GetUserIdentityContext();
            if (objUserIdentity.IsUserFirstTimeLoggedIn)
            {
                WelcomeTextTitle = GetGlobalResourceObject("Common", "Header_Text_Welcome").ToString();
            }
            else
            {
                WelcomeTextTitle = GetGlobalResourceObject("Common", "Header_Text_WelcomeBack").ToString();
            }

            string strCurrentPageURL = GRCBase.UrlUtility.FullCurrentPageUrl.ToLower();

            if (strCurrentPageURL.Contains("dashboard"))
            {
                bodyClass.Attributes.Add("class", "large-screen dashboard");
                mainContent.Attributes.Add("class", "row dashboard");
            }
            else if (strCurrentPageURL.Contains("profile"))
            {
                bodyClass.Attributes.Add("class", "large-screen profile");
            }
            else if (strCurrentPageURL.Contains("connectioncenter"))
            {
                bodyClass.Attributes.Add("class", "large-screen connection-center");
            }
            else if (strCurrentPageURL.Contains("bloodpressure"))
            {
                bodyClass.Attributes.Add("class", "large-screen blood-pressure");
            }
            else if (strCurrentPageURL.Contains("weight"))
            {
                bodyClass.Attributes.Add("class", "large-screen weight");
            }
            else if (strCurrentPageURL.Contains("physicalactivity"))
            {
                bodyClass.Attributes.Add("class", "large-screen physical-activity");
            }
            else if (strCurrentPageURL.Contains("cholesterol"))
            {
                bodyClass.Attributes.Add("class", "large-screen cholesterol");
            }
            else if (strCurrentPageURL.Contains("bloodglucose"))
            {
                bodyClass.Attributes.Add("class", "large-screen blood-glucose");
            }
            else
            {
                bodyClass.Attributes.Add("class", "large-screen");
                mainContent.Attributes.Add("class", "row");
            }

            bool isDashboard = strCurrentPageURL.Contains("dashboard");

            if (isDashboard == true)
            {
                mainContent.Attributes.Add("class", "row dashboard");
            }
            else
            {
                mainContent.Attributes.Add("class", "row");
            }

            // Hide the navigation for new users
            bool isNewUser = strCurrentPageURL.Contains("newuser");

            if (isNewUser == true)
            {
                divNavigation1.Visible = false;
                divNavigation2.Visible = true;
                divNavigation3.Visible = true;

                linkSignOut.Visible = false;

                mainContent.Attributes.Add("class", "finish-account row");
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Page is H360PageHelper)
            {
                WelcomeTextName = CurrentUser.FirstName;
                if (WelcomeTextName != null && WelcomeTextName.Length > 11)
                {
                    WelcomeTextName = GRCBase.StringHelper.GetShortString(WelcomeTextName, 8);
                }
            }
        }

        protected void OnSignOut(object sender, EventArgs e)
        {
            String signOutURL = getSignOutUrl();

            //clear any session-length preferences we are storing in client side cookies
            //Response.Cookies[GlobalPaths_HeartHealthApp.SessionPrefsCookie].Values.Clear();
            AHAAPI.SessionInfo.ClearLoggedInUserState();

            //Calling Signoff will rediect us to our applicaiton's home page
            if (Page is H360PageHelper)
            {
                H360PageHelper objHealthPage = (H360PageHelper)this.Page;
                objHealthPage.MSHealthSignOff(signOutURL);
            }
            else
            {
                if (signOutURL != null)
                {
                    AHAAPI.H360Utility.MSHealthSignOut(signOutURL);
                }
                else
                {
                    AHAAPI.H360Utility.MSHealthSignOut(AHAAPI.WebAppNavigation.H360.PAGE_VISITOR_DEFAULT_NO_APP);
                }
            }
        }

        protected String getSignOutUrl()
        {
            String signOutURL = "/";
            //Look for a campaign home page address in two possible places:
            //1. In the user identity context object in the session (most common)
            //2. In the H360 Utility. It will be here if this is the first time they ever logged in.
            if (!string.IsNullOrEmpty(SessionInfo.GetUserIdentityContext().CampaignUrl))
            {
                signOutURL += SessionInfo.GetUserIdentityContext().CampaignUrl;
            }
            else
            {
                if (!string.IsNullOrEmpty(AHAAPI.H360Utility.GetCurrentCampaignUrl()))
                {
                    signOutURL += AHAAPI.H360Utility.GetCurrentCampaignUrl();
                }
            }
            return signOutURL;
        }

        #region IHealthApplicationInfo Members

        /// <summary>
        /// Tells the parts of this application that deal with new data creation
        /// what Source to stamp onto the data
        /// </summary>
        string IHealthApplicationInfo.HealthDataSource
        {
            get
            {
                return HVManager.Helper.DataSourceThisApplication;
            }
        }

        #endregion

    }
}