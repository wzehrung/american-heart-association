﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" MasterPageFile="~/MasterPages/Visitor.Master" Inherits="Heart360Web.Error" %>

<asp:Content runat="server" ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain">

    <div id="main-content" class="row">
        <div class="copyBox">
            <%=GetGlobalResourceObject("Common", "Error_Message_Header")%>
            <p>
                <%--You can try to <a href="<%=Request["Source"]%>">reload the page</a> or you can go
                to the <a href="~/" runat="server">home page</a>--%>
                <asp:Literal ID="litLinkText" runat="server"></asp:Literal></p>
            <p>
                <asp:Literal ID="litError" runat="server"></asp:Literal>
            </p>

            <p>
               <%=GetGlobalResourceObject("Common", "Error_Message_Reference_Number")%>:
                <asp:Literal runat="server" ID="litErrorID2"></asp:Literal></p>
            <div>
                <%=GetGlobalResourceObject("Common", "Error_Message_Technical_Details")%>
                <p>&nbsp;</p>
                <p>
                    <asp:Literal ID="LitTechnicalDetails" runat="server"></asp:Literal>
                </p>
                <p>
                    <asp:Literal runat="server" ID="LitCompleteDetails"></asp:Literal>
                </p>
            </div>
        </div>
    </div>

</asp:Content>