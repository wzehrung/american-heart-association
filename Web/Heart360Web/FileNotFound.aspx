﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" CodeBehind="FileNotFound.aspx.cs" Inherits="Heart360Web.FileNotFound" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div id="divContent" runat="server">
        <div>
            <h1><%=GetGlobalResourceObject("Common", "Error_Message_Title_FileNotFound")%></h1>
            <p><%=GetGlobalResourceObject("Common", "Error_Message_Text_FileNotFound")%></p>


            <p><asp:Literal ID="litLinkText" runat="server"></asp:Literal></p>
            <p><asp:Literal ID="litError" runat="server"></asp:Literal></p>

            <p><%=GetGlobalResourceObject("Common", "Error_Message_Code_Title")%>:<%=GetGlobalResourceObject("Common", "Error_Message_Code_404")%></p>
        </div>
    </div>

</asp:Content>