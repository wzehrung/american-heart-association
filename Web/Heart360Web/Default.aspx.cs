using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using AHAAPI;
using AHACommon;

namespace Heart360Web
{
    public partial class _Default : System.Web.UI.Page
    {
        public string strCampaignTitle = string.Empty;
        public string strCampaignDescription = string.Empty;
        public string strCampaignLogo = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if an authenticated user tries to access the root url/default.aspx, then we redirect them to their home page
            //since the root url is not accessible to a loggedin user
            if (SessionInfo.IsUserLoggedIn)
            {
                Response.Redirect(WebAppNavigation.H360.PAGE_PATIENT_HOME);
            }
            else  if (AHAAPI.Provider.SessionInfo.IsProviderLoggedIn())
            {
                Response.Redirect(AHAAPI.Provider.ProviderPageUrl.HOME);
                Response.End();
            }

            string sHost = System.Web.HttpContext.Current.Request.Url.Host.ToString();
            string sPort = HttpContext.Current.Request.ServerVariables["server_port"].ToString();
            string sPortal = AHACommon.AHAAppSettings.ProviderPortal.ToString();

            if (sPort == "80")
            {
                sPort = string.Empty;
            }
            else
            {
                sPort = ":" + sPort;
            }

            string sPath = sPortal;

            btnProviderRegister.HRef = sPath + AHACommon.AHAAppSettings.ProviderRegister;
            btnProviderSignIn.HRef = sPath + AHACommon.AHAAppSettings.ProviderLogin;
            btnVolunteerRegister.HRef = sPath + AHACommon.AHAAppSettings.VolunteerRegister;
            btnVolunteerSignIn.HRef = sPath + AHACommon.AHAAppSettings.VolunteerLogin;

            litTitle.Text = GetGlobalResourceObject("Visitor", "Visitor_Home_Title").ToString();
            litDescription.Text = GetGlobalResourceObject("Visitor", "Visitor_Home_Description").ToString();

            // Note: the following method may re-assign btnProviderRegister.HRef and btnVolunteerRegister.HRef
            _GetCampaignInfo();
        }

        protected void _GetCampaignInfo()
        {
            // Here's where we set the campaign stuff
            H360Utility.ResetCampaignSession();

            if (!string.IsNullOrEmpty(Request["cid"]))
            {
                string campurl = GRCBase.BlowFish.DecryptString(Request["cid"]);
                AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByURL(campurl, H360Utility.GetCurrentLanguageID());
                if (objCampaign == null || (objCampaign != null && !objCampaign.IsActive))
                {
                    H360Utility.ResetCampaignSession();
                    Response.Redirect("~/", true);
                }

                if (objCampaign != null && objCampaign.IsActive)
                {
                    divCampaign.Visible = true;

                    H360Utility.SetCampaignSession(objCampaign.CampaignID);
                    H360Utility.SetCampaignSessionUrl(campurl);     // PJB: need this for signout redirect back to campaign
                    if (objCampaign.PartnerLogoContentType != null) // WZ: Don't need to require an image: && objCampaign.PartnerImageContentType != null)
                    {
                        strCampaignTitle = objCampaign.Title;
                        litDescription.Text = objCampaign.Description;
                        litCampaignPromoTag.Text = objCampaign.PromotionalTagLine;

                        string strImgTag = "<img src='/CampaignImage.aspx?CID={0}&LID={1}&Type={2}' />";
                        strCampaignLogo = string.Format(strImgTag, objCampaign.CampaignID, objCampaign.LanguageID, "partner-logo"); //, objCampaign.PartnerLogoVersion);
                        //strCampaignImage = string.Format(strImgTag, objCampaign.CampaignID, objCampaign.LanguageID, "partner-image", objCampaign.PartnerImageVersion);

                        // Need to pass along the campaign ID to the v5 portal through the provider/volunteer register buttons
                        string sPath = AHACommon.AHAAppSettings.ProviderPortal.ToString();

                        btnProviderRegister.HRef = sPath + AHACommon.AHAAppSettings.ProviderRegister + "&cid=" + Request["cid"];
                        btnVolunteerRegister.HRef = sPath + AHACommon.AHAAppSettings.VolunteerRegister + "&cid=" + Request["cid"];

                        btnProviderSignIn.HRef = sPath + AHACommon.AHAAppSettings.ProviderLogin + "&cid=" + Request["cid"];
                        btnVolunteerSignIn.HRef = sPath + AHACommon.AHAAppSettings.VolunteerLogin + "&cid=" + Request["cid"];

                        // PJB: if the user is not logged in, then add the cid (as campid) to patient signin as well, in the event they register
                        // redirect.aspx will eventually handle the campid query parameter.
                        if( !AHAAPI.SessionInfo.IsUserLoggedIn )
                            btnPatientSignIn.HRef = "Pages/Patient/Dashboard.aspx" + "?campid="  + objCampaign.CampaignID.ToString();    //cid=" + Request["cid"];
                    }

                    // Check to see if there are partner images for a campaign
                    if (objCampaign.CampaignImages != null)
                    {
                        string sImages = objCampaign.CampaignImages;

                        if (objCampaign.PartnerImageContentType != null)
                        {
                            if(sImages.Contains("0"))
                            {
                                string strImage = "<li><img src='/CampaignImage.aspx?CID={0}&LID={1}&Type={2}' /></li>";
                                litSlide00.Text = string.Format(strImage, objCampaign.CampaignID, objCampaign.LanguageID, "partner-image");
                                litSlide00.Visible = true;
                            }
                        }

                        if (sImages.Contains("1")) 
                            { litSlide01.Visible = true; }
                        else 
                            { litSlide01.Visible = false; }

                        if (sImages.Contains("2"))
                            { litSlide02.Visible = true; }
                        else 
                            { litSlide02.Visible = false; }

                        if (sImages.Contains("3"))
                            { litSlide03.Visible = true; }
                        else { litSlide03.Visible = false; }
                    }
                }
            }
        }

        protected void lnkContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect(AHACommon.AHAAppSettings.PatientForgotPasswordURL);
        }
    }
}
