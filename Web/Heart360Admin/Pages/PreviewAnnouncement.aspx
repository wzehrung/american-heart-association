﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewAnnouncement.aspx.cs"
    Inherits="Heart360Admin.Pages.PreviewAnnouncement" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Css/Style.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
<link href="/Css/ie6fix.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
    <form id="form1" runat="server" class="patient-preview-bg">
    <div id="rightBox">
        <h3 runat="server" id="hZoneTitle">
        </h3>
        <asp:Literal ID="litGuidingText" runat="server" />
        <div class="clearer">
        </div>
        <div class="fade">
            <h3 runat="server" id="hTitle">
                Helpful Tips</h3>
            <div class="provider-tips">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non tortor arcu.
                Curabitur tristique nisl sed diam vestibulum aliquet. Donec sed dolor tortor. Quisque
                tincidunt, metus sit amet posuere fringilla, risus nibh volutpat metus, id aliquam
                magna eros non lorem.
            </div>
        </div>
    </div>
    </form>
</body>
</html>
