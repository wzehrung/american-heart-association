﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="ResourceList.aspx.cs" Inherits="Heart360Admin.Pages.Resources.Patient.ResourceList" %>

<%@ Register Src="~/UserCtrls/Resources/Patient/ResourceList.ascx" TagName="ResourceList"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <uc1:ResourceList ID="ResourceList1" runat="server">
        </uc1:ResourceList>
        <br />
        <br />
    </div>
</asp:Content>
