﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using AHAHelpContent;

namespace Heart360Admin.Pages.ResourceGraphics.Provider
{
    public partial class CreateResourceGraphics : System.Web.UI.Page
    {
        private void _Intialize()
        {
            List<AHAHelpContent.ProviderResourceGraphics> lstProviderResourceGraphics = ProviderResourceGraphics.GetProviderResourceGraphics(Utilities.Utility.GetCurrentLanguageID());

            int i = 0;
            foreach (ProviderResourceGraphics prg in lstProviderResourceGraphics)
            {
                switch (i)
                {
                    case 0: 
                        hddnID0.Value = prg.ProviderResourceGraphicsID.ToString();
                        break;
                    case 1:
                        hddnID1.Value = prg.ProviderResourceGraphicsID.ToString();
                        break;
                    case 2:
                        hddnID2.Value = prg.ProviderResourceGraphicsID.ToString();
                        break;
                    case 3:
                        hddnID3.Value = prg.ProviderResourceGraphicsID.ToString();
                        break;
                    case 4:
                        hddnID4.Value = prg.ProviderResourceGraphicsID.ToString();
                        break;
                }
                i = i + 1;


            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _Intialize();
            }
            litSuccess.Text = string.Empty;
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");
            if (!Page.IsValid)
            {
                return;
            }
            if (hddnID0.Value == "")
            {
                hddnID0.Value = "0";
            }
            if (hddnID1.Value == "")
            {
                hddnID1.Value = "0";
            }
            if (hddnID2.Value == "")
            {
                hddnID2.Value = "0";
            }
            if (hddnID3.Value == "")
            {
                hddnID3.Value = "0";
            }
            if (hddnID4.Value == "")
            {
                hddnID4.Value = "0";
            }


            ProviderResourceGraphics.UpdateProviderResourceGraphics(Convert.ToInt32(hddnID0.Value),
                fileUpload1.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL1.Text);
            ProviderResourceGraphics.UpdateProviderResourceGraphics(Convert.ToInt32(hddnID1.Value),
                fileUpload2.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL2.Text);
            ProviderResourceGraphics.UpdateProviderResourceGraphics(Convert.ToInt32(hddnID2.Value),
                fileUpload3.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL3.Text);
            ProviderResourceGraphics.UpdateProviderResourceGraphics(Convert.ToInt32(hddnID3.Value),
                fileUpload4.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL4.Text);
            ProviderResourceGraphics.UpdateProviderResourceGraphics(Convert.ToInt32(hddnID4.Value),
                fileUpload5.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL5.Text);

            litSuccess.Text = "<div class=\"sucess\">" + GetGlobalResourceObject("Heart360AdminGlobalResources", "graphics_has_been_successfully").ToString() + "</div>";
        }

        protected void bttnPreview_Click(object sender, EventArgs e)
        {
            string sFileName1 = string.Empty;
            string sFileName2 = string.Empty;
            string sFileName3 = string.Empty;
            string sFileName4 = string.Empty;
            string sFileName5 = string.Empty;

            sFileName1 = @"/Pages/ResourceGraphics/Provider/tempimages/" + Path.GetFileName(fileUpload1.PostedFile.FileName);
            sFileName2 = @"/Pages/ResourceGraphics/Provider/tempimages/" + Path.GetFileName(fileUpload2.PostedFile.FileName);
            sFileName3 = @"/Pages/ResourceGraphics/Provider/tempimages/" + Path.GetFileName(fileUpload3.PostedFile.FileName);
            sFileName4 = @"/Pages/ResourceGraphics/Provider/tempimages/" + Path.GetFileName(fileUpload4.PostedFile.FileName);
            sFileName5 = @"/Pages/ResourceGraphics/Provider/tempimages/" + Path.GetFileName(fileUpload5.PostedFile.FileName);


            string path1 = Server.MapPath(sFileName1);
            string path2 = Server.MapPath(sFileName2);
            string path3 = Server.MapPath(sFileName3);
            string path4 = Server.MapPath(sFileName4);
            string path5 = Server.MapPath(sFileName5);

            fileUpload1.SaveAs(path1);
            fileUpload2.SaveAs(path2);
            fileUpload3.SaveAs(path3);
            fileUpload4.SaveAs(path4);
            fileUpload5.SaveAs(path5);

            Server.Transfer("/Pages/ResourceGraphics/Provider/PreviewResource.aspx", true);
        }

        protected void OnValidateFileUpload1(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload1.PostedFile == null || fileUpload1.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 1.";
                return;
            }
            else if ((fileUpload1.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload1.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 1.";
                return;
            }
            else if (fileUpload1.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0 && (fileUpload1.PostedFile.ContentLength > 512000))
            {
                //size cannot be grater than 228KB
                v.IsValid = false;
                //custValidator.ErrorMessage = "Flash file size should not be greater than 500 KB";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Flash_file_size").ToString();
                return;
            }
        }

        protected void OnValidateFileUpload2(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload2.PostedFile == null || fileUpload2.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 2.";
                return;
            }
            else if ((fileUpload2.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload2.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 2.";
                return;
            }
        }

        protected void OnValidateFileUpload3(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload3.PostedFile == null || fileUpload3.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 3.";
                return;
            }
            else if ((fileUpload3.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload3.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 3.";
                return;
            }
        }

        protected void OnValidateFileUpload4(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload4.PostedFile == null || fileUpload4.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 4.";
                return;
            }
            else if ((fileUpload4.PostedFile != null && fileUpload4.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload4.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 4.";
                return;
            }
        }

        protected void OnValidateFileUpload5(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload5.PostedFile == null || fileUpload5.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 5.";
                return;
            }
            else if ((fileUpload5.PostedFile != null && fileUpload5.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload5.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 5.";
                return;
            }
        }

        protected void OnValidateURL1(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL1.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 1";
                //custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Url").ToString();
                return;
            }
            else if (txtURL1.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 1";
                return;
            }
            return;
        }

        protected void OnValidateURL2(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL2.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 2";
                return;
            }
            else if (txtURL2.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 2";
                return;
            }
            return;
        }

        protected void OnValidateURL3(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL3.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 3";
                return;
            }
            else if (txtURL3.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 3";
                return;
            }
            return;
        }

        protected void OnValidateURL4(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL4.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 4";
                return;
            }
            else if (txtURL4.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 4";
                return;
            }
            return;
        }

        protected void OnValidateURL5(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL5.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 5";
                return;
            }
            else if (txtURL5.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 5";
                return;
            }
            return;
        }
    }
}
