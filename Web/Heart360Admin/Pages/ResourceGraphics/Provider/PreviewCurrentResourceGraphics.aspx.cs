﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;

namespace Heart360Admin.Pages.ResourceGraphics.Provider
{
    public partial class PreviewCurrentResourceGraphics : System.Web.UI.Page
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (objUserContext != null)
            {
                List<AHAHelpContent.ProviderResourceGraphics> lstProviderResourceGraphics = ProviderResourceGraphics.GetProviderResourceGraphics(Utilities.Utility.GetCurrentLanguageID());
                if (lstProviderResourceGraphics.Count> 0)
                {
                    //Response.ContentType = "application/x-shockwave-flash";
                    //Response.BinaryWrite(objProviderResourceGraphics.Graphics);
                    //Response.Flush();
                    //Response.End();
                }
            }
        }
    }
}
