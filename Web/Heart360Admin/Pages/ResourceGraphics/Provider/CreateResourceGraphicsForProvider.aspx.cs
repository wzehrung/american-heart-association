﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using AHAHelpContent;

namespace Heart360Admin.Pages.ResourceGraphics.Provider
{
    public partial class CreateResourceGraphicsForProvider : System.Web.UI.Page
    {
        private void _Intialize()
        {
            //ProviderResourceGraphics objProviderResourceGraphics = ProviderResourceGraphics.GetProviderResourceGraphics(Utilities.Utility.GetCurrentLanguageID());
            //if (objProviderResourceGraphics != null)
            //{
            //    litFlashFileTitle.Text = objProviderResourceGraphics.Title;
            //    hddnID.Value = objProviderResourceGraphics.ProviderResourceGraphicsID.ToString();
            //    lnkAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfoForProvider.aspx?id=" + objProviderResourceGraphics.ProviderResourceGraphicsID.ToString() + "&Type=" + AHAHelpContent.AuditTrailType.ProviderResourceGraphics.ToString() + "')");
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _Intialize();
            }
            litSuccess.Text = string.Empty;
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");
            if (!Page.IsValid)
            {
                return;
            }
            //ProviderResourceGraphics.UpdateProviderResourceGraphics(Convert.ToInt32(hddnID.Value),
            //                                                     fileUpload.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
            //litSuccess.Text = "<div class=\"sucess\">Resource graphics has been successfully uploaded</div>";
            litSuccess.Text = "<div class=\"sucess\">" + GetGlobalResourceObject("Heart360AdminGlobalResources", "graphics_has_been_successfully").ToString() + "</div>";

        }


        protected void bttnPreview_Click(object sender, EventArgs e)
        {
            if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0)
            {
                //Session["providerresoucepreview"] = null;
                //Session["providerresoucepreview"] = fileUpload.FileBytes;
                Utilities.SessionInfo.ResetProviderResourcePreviewSession();
                Utilities.SessionInfo.SetProviderResourcePreviewToSession(fileUpload.FileBytes);
                Response.Redirect("/Pages/ResourceGraphics/Provider/PreviewResourceForProvider.aspx");
            }
        }

        protected void bttnCurrentPreview_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/ResourceGraphics/Provider/PreviewCurrentResourceForProvider.aspx");
        }

        protected void OnValidateFileUpload(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload.PostedFile == null || fileUpload.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Upload a flash file.";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_a_flash_file").ToString();
                return;
            }
            else if ((fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload.PostedFile.FileName).ToLower().Equals(".swf")))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "You can upload only a '.swf' file";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "You_Can_Only_Flash").ToString();
                return;
            }

            else if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0 && (fileUpload.PostedFile.ContentLength > 512000))
            {
                //size cannot be grater than 228KB
                v.IsValid = false;
                //custValidator.ErrorMessage = "Flash file size should not be greater than 500 KB";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Flash_file_size").ToString();
                return;
            }
        }
    }
}