﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="CreateResourceGraphicsForProvider.aspx.cs" Inherits="Heart360Admin.Pages.ResourceGraphics.Provider.CreateResourceGraphicsForProvider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <h1>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Resource_Graphics")%></h1>
        <p>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Use_this_page_to_update_the_current_flash_provider")%></p>
        <br />
        <div class="whitebox" style="width: 600px;">
            <div class="shadowtop">
                <div class="tl">
                </div>
                <div class="tm">
                </div>
                <div class="tr">
                </div>
            </div>
            <div>
                <table cellpadding="0" cellspacing="0" class="tablecontainer">
                    <tbody>
                        <tr>
                            <td class="left-shadow">
                            </td>
                            <td class="boxcontent">
                                <table cellspacing="0" width="100%" class="control-table">
                                    <tr>
                                        <td colspan="2" class="red align-right">
                                           <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                            <asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="validation_summary">
                                                <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                                    ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                                    DisplayMode="BulletList" ForeColor="#CE2930" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Literal ID="litSuccess" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="align-right nowrap">
                                            <%--<asp:Literal ID="litUploadLogo" runat="server"></asp:Literal>--%>
                                            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_new_flash_file")%>&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="fileUpload" onchange="javascript:showButton();" runat="server" />
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload"
                                                runat="server" Display="None" OnServerValidate="OnValidateFileUpload">
                                            </asp:CustomValidator>
                                            <%-- <asp:Button ID="bttnPreview" Text="Preview" PostBackUrl="/Pages/ResourceGraphics/Provider/PreviewResource.aspx"
                        OnClientClick="previewImage()" runat="server" />--%>
                                            <asp:Button ID="bttnPreview" Text="<%$ Resources:Heart360AdminGlobalResources, Preview %>" OnClick="bttnPreview_Click" OnClientClick="previewImage()"
                                                runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="guidingtext">
                                            (<%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_only_Swf")%>)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="align-right nowrap">
                                            <%--  <asp:Literal ID="litUploadLogo" runat="server"></asp:Literal>--%>
                                            (<%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Current_flash_file")%>)&nbsp;
                                        </td>
                                        <td>
                                            <asp:Literal ID="litFlashFileTitle" Visible="false" runat="server"></asp:Literal>
                                            <%-- <asp:Button ID="bttnCurrentPreview" OnClientClick="previewCurrentImage()" PostBackUrl="/Pages/ResourceGraphics/Provider/PreviewCurrentResource.aspx" Text="Preview current flash file" runat="server" />--%>
                                            <asp:Button ID="bttnCurrentPreview" OnClientClick="previewCurrentImage()" OnClick="bttnCurrentPreview_Click"
                                                Text="<%$ Resources:Heart360AdminGlobalResources, Preview_current_flash_file %>" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton runat="server" ID="lnkSave" OnClientClick="upLoadFlash()" OnClick="btnSave_OnClick"
                                                CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Save")%></em></strong></asp:LinkButton>&nbsp;<asp:LinkButton runat="server" ID="lnkAudit" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AuditInfo")%></em></strong></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="right-shadow">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="shadowbottom">
                <div class="bl">
                </div>
                <div class="bm">
                </div>
                <div class="br">
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var bttnPreview = document.getElementById('<%=bttnPreview.ClientID %>');
            bttnPreview.disabled = true;

            function showButton() {
                var fileUpload = document.getElementById('<%=fileUpload.ClientID %>');
                if (fileUpload.value == "") {
                    bttnPreview.disabled = true;
                    return;
                }
                else if (fileUpload.value.substring(fileUpload.value.length - 3, fileUpload.value.length).toLowerCase() != 'swf') {
                    bttnPreview.disabled = true;
                    return;
                }
                bttnPreview.disabled = false;
                return;
            }

            function previewImage() {
                window.open("", "preview_flash", "width=1038,height=380");
                document.forms[0].target = 'preview_flash';
            }

            function previewCurrentImage() {
                window.open("", "preview_current_flash", "width=1038,height=380");
                document.forms[0].target = 'preview_current_flash';
                return true;
            }

            function upLoadFlash() {
                document.forms[0].target = '_self';
                document.forms[0].action = "/Pages/ResourceGraphics/Provider/CreateResourceGraphicsForProvider.aspx";
            }                
        </script>

        <br />
        <br />
    </div>
    <asp:HiddenField ID="hddnID" runat="server" />
</asp:Content>
