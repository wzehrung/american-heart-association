﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="CreateResourceGraphics.aspx.cs" Inherits="Heart360Admin.Pages.ResourceGraphics.Provider.CreateResourceGraphics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <h1>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Resource_Graphics")%></h1>
        <p>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Use_this_page_to_update_the_current_graphics_provider")%></p>
        <br />
        <div class="whitebox" style="width: 700px;">
            <div class="shadowtop">
                <div class="tl">
                </div>
                <div class="tm">
                </div>
                <div class="tr">
                </div>
            </div>
            <div>

                <table cellpadding="0" cellspacing="0" class="tablecontainer">
                    <tbody>
                        <tr>
                            <td class="left-shadow">
                            </td>
                            <td class="boxcontent">

                                <table cellspacing="0" width="100%" class="control-table" border="0">
                                    <tr>
                                        <td colspan="4" class="red align-right">
                                            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                            <b><asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                            <asp:Literal ID="litSuccess" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="align-right nowrap" colspan="2">
                                            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_new_flash_file")%>&nbsp;
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-right nowrap">Image 1:<b style="color: Red;">*</b></td> 
                                        <td><asp:FileUpload ID="fileUpload1" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL1" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL1" OnServerValidate="OnValidateURL1" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-right nowrap">Image 2:<b style="color: Red;">*</b></td>  
                                        <td><asp:FileUpload ID="fileUpload2" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL2" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL2" OnServerValidate="OnValidateURL2" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-right nowrap">Image 3:<b style="color: Red;">*</b></td> 
                                        <td><asp:FileUpload ID="fileUpload3" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL3" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL3" OnServerValidate="OnValidateURL3" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-right nowrap">Image 4:<b style="color: Red;">*</b></td> 
                                        <td><asp:FileUpload ID="fileUpload4" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL4" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL4" OnServerValidate="OnValidateURL4" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                         
                                    <tr>
                                        <td class="align-right nowrap">Image 5:<b style="color: Red;">*</b></td> 
                                        <td><asp:FileUpload ID="fileUpload5" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL5" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL5" OnServerValidate="OnValidateURL5" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="4">
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload1" runat="server" Display="None" OnServerValidate="OnValidateFileUpload1"></asp:CustomValidator>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload2" runat="server" Display="None" OnServerValidate="OnValidateFileUpload2"></asp:CustomValidator>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload3" runat="server" Display="None" OnServerValidate="OnValidateFileUpload3"></asp:CustomValidator>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload4" runat="server" Display="None" OnServerValidate="OnValidateFileUpload4"></asp:CustomValidator>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload5" runat="server" Display="None" OnServerValidate="OnValidateFileUpload5"></asp:CustomValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                            <div class="validation_summary">
                                                <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                                    ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                                    DisplayMode="BulletList" ForeColor="#CE2930" />
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td colspan="3">
                                            <asp:LinkButton runat="server" ID="lnkSave" OnClientClick="uploadImages()" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Save")%></em></strong></asp:LinkButton>&nbsp;
                                            <asp:LinkButton runat="server" ID="lnkPreview" CssClass="aha-small-button with-arrow" OnClientClick="previewImage()" OnClick="bttnPreview_Click"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Preview")%></em></strong></asp:LinkButton>&nbsp;
                                        </td>
                                    </tr>
                                </table>
 
                            </td>
                            <td class="right-shadow">
                            </td>
                        </tr>
                    </tbody>
                </table>           
            
            </div>
            <div class="shadowbottom">
                <div class="bl">
                </div>
                <div class="bm">
                </div>
                <div class="br">
                </div>
            </div>
        </div>

        <script type="text/javascript">


            function showButton() {
                var fileUpload1 = document.getElementById('<%=fileUpload1.ClientID %>');
                var fileUpload2 = document.getElementById('<%=fileUpload2.ClientID %>');
                var fileUpload3 = document.getElementById('<%=fileUpload3.ClientID %>');
                var fileUpload4 = document.getElementById('<%=fileUpload4.ClientID %>');
                var fileUpload5 = document.getElementById('<%=fileUpload5.ClientID %>');

                if (fileUpload1.value == "") {
                    bttnPreview.disabled = true;
                    return;
                }
                else if (fileUpload1.value.substring(fileUpload1.value.length - 3, fileUpload1.value.length).toLowerCase() != 'swf') {
                    bttnPreview.disabled = true;
                    return;
                }

                if (fileUpload2.value == "") {
                    bttnPreview.disabled = true;
                    return;
                }

                bttnPreview.disabled = false;
                return;
            }

            function previewImage() {
                window.open("", "preview_resource_graphics", "width=1038,height=700");
                document.forms[0].target = 'preview_resource_graphics';
            }

            function uploadImages() {
                document.forms[0].target = '_self';
                document.forms[0].action = "/Pages/ResourceGraphics/Provider/CreateResourceGraphics.aspx";
            }                
        </script>

        <br />
        <br />
    </div>
    <asp:HiddenField ID="hddnID0" runat="server" />
    <asp:HiddenField ID="hddnID1" runat="server" />
    <asp:HiddenField ID="hddnID2" runat="server" />
    <asp:HiddenField ID="hddnID3" runat="server" />
    <asp:HiddenField ID="hddnID4" runat="server" />
</asp:Content>
