﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.ResourceGraphics.Provider
{
    public partial class PreviewResourceGraphics : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Utilities.SessionInfo.GetProviderResourcePreviewFromSession() != null)
            {
                Response.ContentType = "application/x-shockwave-flash";
                byte[] buffer = Utilities.SessionInfo.GetProviderResourcePreviewFromSession();
                Response.BinaryWrite(buffer);
                Response.Flush();
                Response.End();
            }
        }
    }
}
