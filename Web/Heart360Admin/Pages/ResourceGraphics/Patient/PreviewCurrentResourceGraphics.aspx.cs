﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;

namespace Heart360Admin.Pages.ResourceGraphics.Patient
{
    public partial class PreviewCurrentResourceGraphics : System.Web.UI.Page
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (objUserContext != null)
            {
                List<AHAHelpContent.PatientResourceGraphics> lstPatientResourceGraphics = PatientResourceGraphics.GetPatientResourceGraphics(Utilities.Utility.GetCurrentLanguageID());
                //if (objPatientResourceGraphics != null && objPatientResourceGraphics.Graphics != null)
                //{
                //    Response.ContentType = "application/x-shockwave-flash";
                //    Response.BinaryWrite(objPatientResourceGraphics.Graphics);
                //    Response.Flush();
                //    Response.End();
                //}
            }
        }
    }
}
