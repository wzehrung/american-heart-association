﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.ResourceGraphics.Patient
{
    public partial class PreviewResourceGraphics : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Utilities.SessionInfo.GetPatientResourcePreviewFromSession() != null)
            {
                Response.ContentType = "application/x-shockwave-flash";
                byte[] buffer = Utilities.SessionInfo.GetPatientResourcePreviewFromSession();
                Response.BinaryWrite(buffer);
                Response.Flush();
                Response.End();
            }
        }
    }
}
