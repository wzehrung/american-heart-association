﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="CreateResourceGraphics.aspx.cs" Inherits="Heart360Admin.Pages.ResourceGraphics.CreateResourceGraphics" %>

<%@ Register Src="~/UserCtrls/ResourceGraphics/CreateResourceGraphics.ascx" TagName="CreateResourceGraphics" TagPrefix="uc1" %>

<asp:Content ID="ContentPlaceHolder" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <%--  <uc1:CreateResourceGraphics ID="CreateResourceGraphics1" runat="server" />
        <br />
        <br />--%>
        <h1>
        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Resource_Graphics")%>
            </h1>
        <p>
        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Use_this_page_to_update_the_current_graphics_patient")%>
             </p>
        <br />
        <div class="whitebox" style="width: 720px;">

            <div class="shadowtop">
                <div class="tl">
                </div>
                <div class="tm">
                </div>
                <div class="tr">
                </div>
            </div>

            <div>

                <table cellpadding="0" cellspacing="0" class="tablecontainer">
                    <tbody>
                        <tr>
                            <td class="left-shadow">
                            </td>
                            <td class="boxcontent">

                                <table cellspacing="0" class="control-table" border="0">
                                    <tr>
                                        <td colspan="4" class="red align-right">
                                            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                            <b><asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                            <asp:Literal ID="litSuccess" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="align-right nowrap" colspan="4">
                                            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_new_flash_file")%>&nbsp;
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-right nowrap">Left Image:<b style="color: Red;">*</b></td> 
                                        <td><asp:FileUpload ID="fileUpload1" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL1" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL1" OnServerValidate="OnValidateURL1" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-right nowrap">Center Image:<b style="color: Red;">*</b></td>  
                                        <td><asp:FileUpload ID="fileUpload2" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL2" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL2" OnServerValidate="OnValidateURL2" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-right nowrap">Right Image:<b style="color: Red;">*</b></td> 
                                        <td><asp:FileUpload ID="fileUpload3" onchange="javascript:showButton();" runat="server" /></td>
                                        <td class="align-right nowrap">URL:<b style="color: Red;">*</b></td> 
                                        <td>
                                            <asp:TextBox runat="server" ID="txtURL3" Width="280">http://</asp:TextBox>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidatorURL3" OnServerValidate="OnValidateURL3" runat="server" Display="None"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                            
                                    <tr>
                                        <td colspan="4">
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload1" runat="server" Display="None" OnServerValidate="OnValidateFileUpload1"></asp:CustomValidator>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload2" runat="server" Display="None" OnServerValidate="OnValidateFileUpload2"></asp:CustomValidator>
                                            <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload3" runat="server" Display="None" OnServerValidate="OnValidateFileUpload3"></asp:CustomValidator>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                            <div class="validation_summary">
                                                <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                                    ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                                    DisplayMode="BulletList" ForeColor="#CE2930" />
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td colspan="3">
                                            <asp:LinkButton runat="server" ID="lnkSave" OnClientClick="uploadImages()" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Save")%></em></strong></asp:LinkButton>&nbsp;
                                            <asp:LinkButton runat="server" ID="lnkPreview" CssClass="aha-small-button with-arrow" OnClientClick="previewImage()" OnClick="bttnPreview_Click"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Preview")%></em></strong></asp:LinkButton>&nbsp;
                                       </td>
                                    </tr>
                                </table>
                            
                            </td>
                            <td class="right-shadow">
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="shadowbottom">
                <div class="bl">
                </div>
                <div class="bm">
                </div>
                <div class="br">
                </div>
            </div>
        </div>

        <script type="text/javascript">


            function showButton() {
                var fileUpload1 = document.getElementById('<%=fileUpload1.ClientID %>');
                if (fileUpload1.value == "") {
                    bttnPreview.disabled = true;
                    return;
                }
                else if (fileUpload1.value.substring(fileUpload1.value.length - 3, fileUpload1.value.length).toLowerCase() != 'jpg') {
                    bttnPreview.disabled = true;
                    return;
                }
                bttnPreview.disabled = false;
                return;
            }

            function previewImage() {
                window.open("", "preview_resource_graphics", "width=1038,height=700");
                document.forms[0].target = 'preview_resource_graphics';
            }

            function previewCurrentImage() {
                window.open("", "preview_current_flash", "width=1038,height=380");
                document.forms[0].target = 'preview_current_flash';
                return true;
            }

            function uploadImages() {
                document.forms[0].target = '_self';
                document.forms[0].action = "/Pages/ResourceGraphics/Patient/CreateResourceGraphics.aspx";
            }                
        </script>

        <br />
        <br />
    </div>
    <asp:HiddenField ID="hddnID0" runat="server" />
    <asp:HiddenField ID="hddnID1" runat="server" />
    <asp:HiddenField ID="hddnID2" runat="server" />
</asp:Content>
