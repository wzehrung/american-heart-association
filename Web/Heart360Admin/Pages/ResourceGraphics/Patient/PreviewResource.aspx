﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewResource.aspx.cs"
    Inherits="Heart360Admin.Pages.ResourceGraphics.PreviewResource" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="/Css/Style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">

        <div class="resourcegraphics-preview-patient-bg">
            <div id="previewBox">
                <asp:Image ID="image1" runat="server"  />
                <asp:Image ID="image2" runat="server"  />
                <asp:Image ID="image3" runat="server"  />
            </div>
        </div>

    </form>
</body> 
</html>
