﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using AHAHelpContent;

namespace Heart360Admin.Pages.ResourceGraphics
{
    public partial class CreateResourceGraphics : System.Web.UI.Page
    {
        private bool IsEditMode
        {
            get
            {
                return (Request["CID"] != null && Request["CID"].Length > 0) ? true : false;
            }
        }

        private void _Intialize()
        {
            List<AHAHelpContent.PatientResourceGraphics> lstPatientResourceGraphics  = PatientResourceGraphics.GetPatientResourceGraphics(Utilities.Utility.GetCurrentLanguageID());

            int i = 0;
            foreach (PatientResourceGraphics prg in lstPatientResourceGraphics)
            {
                switch (i)
                {
                    case 0:
                        hddnID0.Value = prg.PatientResourceGraphicsID.ToString();
                        break;
                    case 1:
                        hddnID1.Value = prg.PatientResourceGraphicsID.ToString();
                        break;
                    case 2:
                        hddnID2.Value = prg.PatientResourceGraphicsID.ToString();
                        break;

                }
                i = i + 1;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _Intialize();
            }
            litSuccess.Text = string.Empty;
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");
            if (!Page.IsValid)
            {
                return;
            }

            if (hddnID0.Value == "")
            {
                hddnID0.Value = "0";
            }
            if (hddnID1.Value == "")
            {
                hddnID1.Value = "0";
            }
            if (hddnID2.Value == "")
            {
                hddnID2.Value = "0";
            }

            PatientResourceGraphics.UpdatePatientResourceGraphics(Convert.ToInt32(hddnID0.Value),
                fileUpload1.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL1.Text);
            PatientResourceGraphics.UpdatePatientResourceGraphics(Convert.ToInt32(hddnID1.Value),
                fileUpload2.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL2.Text);
            PatientResourceGraphics.UpdatePatientResourceGraphics(Convert.ToInt32(hddnID2.Value),
                fileUpload3.FileBytes, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID(), txtURL3.Text);

            litSuccess.Text = "<div class=\"sucess\">" + GetGlobalResourceObject("Heart360AdminGlobalResources", "graphics_has_been_successfully").ToString() + "</div>";
        }

        protected void bttnCurrentPreview_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/ResourceGraphics/Patient/PreviewCurrentResource.aspx");
        }

        protected void bttnPreview_Click(object sender, EventArgs e)
        {
            string sFileName1 = string.Empty;
            string sFileName2 = string.Empty;
            string sFileName3 = string.Empty;

            sFileName1 = @"/Pages/ResourceGraphics/Patient/tempimages/" + Path.GetFileName(fileUpload1.PostedFile.FileName);
            sFileName2 = @"/Pages/ResourceGraphics/Patient/tempimages/" + Path.GetFileName(fileUpload2.PostedFile.FileName);
            sFileName3 = @"/Pages/ResourceGraphics/Patient/tempimages/" + Path.GetFileName(fileUpload3.PostedFile.FileName);

            string path1 = Server.MapPath(sFileName1);
            string path2 = Server.MapPath(sFileName2);
            string path3 = Server.MapPath(sFileName3);

            fileUpload1.SaveAs(path1);
            fileUpload2.SaveAs(path2);
            fileUpload3.SaveAs(path3);

            Server.Transfer("/Pages/ResourceGraphics/Patient/PreviewResource.aspx", true);
        }
        
        protected void OnValidateFileUpload1(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload1.PostedFile == null || fileUpload1.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 1.";
                return;
            }
            else if ((fileUpload1.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload1.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 1.";
                return;
            }
            else if (fileUpload1.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0 && (fileUpload1.PostedFile.ContentLength > 512000))
            {
                //size cannot be grater than 228KB
                v.IsValid = false;
                //custValidator.ErrorMessage = "Flash file size should not be greater than 500 KB";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Flash_file_size").ToString();
                return;
            }
        }

        protected void OnValidateFileUpload2(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload2.PostedFile == null || fileUpload2.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 2.";
                return;
            }
            else if ((fileUpload2.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload2.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 2.";
                return;
            }
        }

        protected void OnValidateFileUpload3(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload3.PostedFile == null || fileUpload3.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Upload a JPEG file for Image 3.";
                return;
            }
            else if ((fileUpload3.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0) &&
                (!Path.GetExtension(fileUpload3.PostedFile.FileName).ToLower().Equals(".jpg")))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "You can upload only a '.jpg' file for Image 3.";
                return;
            }
        }

        protected void OnValidateURL1(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL1.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 1";
                return;
            }
            else if (txtURL1.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 1";
                return;
            }
            return;
        }

        protected void OnValidateURL2(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL2.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 2";
                return;
            }
            else if (txtURL2.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 2";
                return;
            }
            return;
        }

        protected void OnValidateURL3(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL3.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 3";
                return;
            }
            else if (txtURL3.Text == "http://")
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter URL for Image 3";
                return;
            }
            return;
        }

        #region PreviousCode
        //protected void OnValidateFileUpload(object sender, ServerValidateEventArgs v)
        //{
        //    CustomValidator custValidator = sender as CustomValidator;

        //    if (string.IsNullOrEmpty(Request["ID"]) && (fileUpload1.PostedFile == null || fileUpload1.PostedFile.ContentLength == 0))
        //    {
        //        v.IsValid = false;
        //        custValidator.ErrorMessage = "Upload a JPEG file.";
        //        //custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_a_flash_file").ToString();
        //        return;
        //    }
        //    else if ((fileUpload1.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0) &&
        //        (!Path.GetExtension(fileUpload1.PostedFile.FileName).ToLower().Equals(".jpg")))
        //    {
        //        v.IsValid = false;
        //        custValidator.ErrorMessage = "You can upload only a '.jpg' file";
        //        //custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "You_Can_Only_Flash").ToString(); 
        //        return;
        //    }
        //    else if (fileUpload1.PostedFile != null && fileUpload1.PostedFile.ContentLength > 0 && (fileUpload1.PostedFile.ContentLength > 512000))
        //    {
        //        //size cannot be grater than 228KB
        //        v.IsValid = false;
        //        //custValidator.ErrorMessage = "Flash file size should not be greater than 500 KB";
        //        custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Flash_file_size").ToString();
        //        return;
        //    }
        //}

        //protected void OnValidateURL(object sender, ServerValidateEventArgs v)
        //{
        //    CustomValidator custValidator = sender as CustomValidator;
        //    if (txtURL1.Text.Trim().Length == 0)
        //    {
        //        v.IsValid = false;
        //        //custValidator.ErrorMessage = "Enter URL";
        //        custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Url").ToString();
        //        return;
        //    }
        //    //string url = litURL.Text + txtURL.Text.Trim();
        //    //if (!Regex.IsMatch(url, "^(ht|f)tp(s?)\\:\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\'\\/\\\\+&amp;%\\$#_]*)?$"))
        //    if (!Regex.IsMatch(txtURL1.Text.Trim(), "^[A-Za-z0-9_-]+$"))
        //    {
        //        v.IsValid = false;
        //        custValidator.ErrorMessage = "Enter a valid URL. (URL can contain only alpha numeric, - and _)";
        //        //custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_ValidCampUrl").ToString();
        //        return;
        //    }
        //    if (!IsEditMode && AHAHelpContent.Campaign.DoesCampaignExistByURL(txtURL1.Text.Trim()))
        //    {
        //        v.IsValid = false;
        //        //custValidator.ErrorMessage = "Campaign with URL " + txtURL.Text.Trim() + " already exists. Please try a different URL";
        //        custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Campagin_WithUrl").ToString() + " " + txtURL1.Text.Trim() + " " + GetGlobalResourceObject("Heart360AdminGlobalResources", "Please_Try_Diffrent_Url").ToString();
        //        return;
        //    }
        //    return;
        //}
        #endregion
    }
}
