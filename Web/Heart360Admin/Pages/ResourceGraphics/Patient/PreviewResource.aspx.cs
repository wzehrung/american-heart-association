﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.ResourceGraphics
{
    public partial class PreviewResource : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sFileName1 = string.Empty;
            string sFileName2 = string.Empty;
            string sFileName3 = string.Empty;

            ContentPlaceHolder cntentPlcHolder = PreviousPage.Master.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;

            if (cntentPlcHolder != null)
            {
                FileUpload fileUpload1 = cntentPlcHolder.FindControl("fileUpload1") as FileUpload;
                FileUpload fileUpload2 = cntentPlcHolder.FindControl("fileUpload2") as FileUpload;
                FileUpload fileUpload3 = cntentPlcHolder.FindControl("fileUpload3") as FileUpload;

                sFileName1 = @"/Pages/ResourceGraphics/Patient/tempimages/" + Path.GetFileName(fileUpload1.PostedFile.FileName);
                sFileName2 = @"/Pages/ResourceGraphics/Patient/tempimages/" + Path.GetFileName(fileUpload2.PostedFile.FileName);
                sFileName3 = @"/Pages/ResourceGraphics/Patient/tempimages/" + Path.GetFileName(fileUpload3.PostedFile.FileName);

                image1.ImageUrl = sFileName1;
                image2.ImageUrl = sFileName2;
                image3.ImageUrl = sFileName3;
            }
        }
    }
}
