﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewTip.aspx.cs" Inherits="Heart360Admin.Pages.TipOfTheDay.Patient.PreviewTip" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Css/Style.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
<link href="/Css/ie6fix.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
    <form id="form1" runat="server" class="patient-preview-bg">
    <div id="rightBox">
       <div class="fade">
        <h3>
            Helpful Resources</h3>
        <ul class="links list-arrow">
            <li class="fade" runat="server" id="li1"><a runat="server" id="lnk1" target="_blank">Resource 1</a></li>
            <li class="fade" runat="server" id="li2"><a runat="server" id="lnk2" target="_blank">Resource 2</a></li>
            <li class="fade" runat="server" id="li3"><a runat="server" id="lnk3" target="_blank">Resource 3</a></li>
            <li class="fade" runat="server" id="li4"><a runat="server" id="lnk4" target="_blank">Resource 4</a></li>
            <li class="fade" runat="server" id="li5"><a runat="server" id="lnk5" target="_blank">Resource 5</a></li>
        </ul>
        </div>
        <h3>
            Tip of the Day</h3>
        <asp:Literal ID="litTipOfTheDay" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
