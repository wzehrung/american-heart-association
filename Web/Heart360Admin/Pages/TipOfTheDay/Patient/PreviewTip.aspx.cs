﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GRCBase;

namespace Heart360Admin.Pages.TipOfTheDay.Patient
{
    public partial class PreviewTip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/home.aspx"))
                {
                    Response.Redirect("~/Login.aspx?ReturnURL=" + Server.UrlEncode(HttpContext.Current.Request.RawUrl));
                    return;
                }
                Response.Redirect("~/Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request["TID"]))
                {
                    AHAHelpContent.ThingTips obj = AHAHelpContent.ThingTips.FindByThingTipID(Convert.ToInt32(Request["TID"]),Utilities.Utility.GetCurrentLanguageID());
                    litTipOfTheDay.Text = Utilities.Utility.RenderLink(obj.Content);
                }
                else if (!String.IsNullOrEmpty(Utilities.SessionInfo.GetPatientTipOftheDayFromSession()))
                {
                    litTipOfTheDay.Text = Utilities.Utility.RenderLink(Utilities.SessionInfo.GetPatientTipOftheDayFromSession());
                }
                else
                {
                    throw new Exception("Unauthorized view");
                }
            }
        }
    }
}
