﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewTip.aspx.cs" Inherits="Heart360Admin.Pages.TipOfTheDay.Provider.PreviewTip" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Css/Style.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
<link href="/Css/ie6fix.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
    <form id="form1" runat="server" class="provider-preview-bg">
    <div id="rightBox">
        <div class="fade">
            <h3 runat="server" id="hZoneTitle">
                Announcement
            </h3>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non tortor arcu.
            Curabitur tristique nisl sed diam vestibulum aliquet. Donec sed dolor tortor. Quisque
            tincidunt, metus sit amet posuere fringilla, risus nibh volutpat metus, id aliquam
            magna eros non lorem.
        </div>
        <div class="clearer"></div>
        <h3 runat="server" id="hTitle">
            Helpful Tips</h3>
        <div class="provider-tips">
            <asp:Literal ID="litTipText" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
