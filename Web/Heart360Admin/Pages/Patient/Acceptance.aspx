﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="Acceptance.aspx.cs" Inherits="Heart360Admin.Pages.Patient.Acceptance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
    .hidecol { display: none; }
    .search {
        padding-bottom: 4px;
        width: 100%;
    }
    .export-button {
        margin-top: 15px;
        float: right;
    }
    .reset-all-button {
        float: right;
    }

</style>
<div class="copyBox report">
    <h1><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance_Title")%></h1>        
    <p>
        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance_Intro")%>
    </p>

    <div class="report-filter">
    
        <div class="search">
            <label><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance_Search_Label")%>:</label>
            <asp:TextBox ID="searchQuery" runat="server"></asp:TextBox>

            <asp:LinkButton ID="btnSubmit" runat="server" onclick="btnSubmit_Click" CssClass="aha-small-button with-arrow">
                <strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance_Search")%></em></strong>
            </asp:LinkButton>
            
            <asp:LinkButton ID="ResetAll" runat="server" OnClick="ResetAll_OnClick"
                OnClientClick="return confirm('This will reset User Acceptance for all users; are you sure?');"
                CssClass="reset-all-button aha-small-button with-arrow">
                <strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance_Reset_All")%></em></strong>
            </asp:LinkButton>            
        </div>

        <asp:GridView
            ID="gvSearchResults" OnRowDataBound="gv_RowDataBound" OnRowCommand="gv_RowCommand" 
            OnPageIndexChanging="gvSearchResults_OnPageIndexChanging"
            AutoGenerateColumns="false" AllowPaging="true" PageSize="10" ShowHeaderWhenEmpty="False"
            CssClass="provider-grid" GridLines="Horizontal" Width="100%" runat="server">

            <Columns>         
                <asp:BoundField DataField="PatientID" HeaderText="Patient ID" ReadOnly="True" ItemStyle-CssClass="hidecol" HeaderStyle-CssClass="hidecol"/>
                <asp:BoundField DataField="FirstName" HeaderText="First" ReadOnly="True"/>
                <asp:BoundField DataField="LastName" HeaderText="Last" ReadOnly="True"/>
                <asp:BoundField DataField="EmailAddress" HeaderText="Email" ReadOnly="True"/>
                <asp:BoundField DataField="TermsAccepted" HeaderText="Accepted" ReadOnly="True"/>
                <asp:BoundField DataField="TermsAcceptanceDate" HeaderText="Date" ReadOnly="True"/>
                
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="buttonReset" CommandName="Reset" CssClass="aha-small-button"
                            CommandArgument="<%# Container.DataItemIndex %>">
                            <strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance_Reset")%></em></strong>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
        </asp:GridView>
        
        <div style="width: 100%;">
            <asp:LinkButton ID="Export" runat="server" OnClick="Export_OnClick" Visible="False" 
                CssClass="export-button aha-small-button with-arrow">
                <strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance_Export")%></em></strong>
            </asp:LinkButton>
        </div>

    </div>

</div>

</asp:Content>
