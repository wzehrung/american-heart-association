﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.Patient
{
    public partial class Acceptance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGridView();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            BindGridView();
        }

        private void BindGridView()
        {
            gvSearchResults.DataSource = Utilities.Patients.FindPatientByLogin(searchQuery.Text);
            gvSearchResults.DataBind();

            //display bottom buttons if data present, otherwise show "no data found" message
            Export.Visible = gvSearchResults.Rows.Count > 0;
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {              
                List<Utilities.Patients> objPatients = Utilities.Patients.FindPatientByLogin(searchQuery.Text);
            }
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Reset":
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow row = gvSearchResults.Rows[index % gvSearchResults.PageSize];
                    int id = Convert.ToInt32(row.Cells[0].Text);
                    Utilities.Patients.ResetUserAcceptance(id);
                    break;
            }

            //Refresh

            BindGridView();

        }

        protected void Export_OnClick(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=UserAcceptanceExport.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            gvSearchResults.AllowPaging = false;
            gvSearchResults.DataSource = Utilities.Patients.FindPatientByLogin(searchQuery.Text);
            gvSearchResults.DataBind();

            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < gvSearchResults.Columns.Count; k++)
            {
                //add separator
                sb.Append(gvSearchResults.Columns[k].HeaderText + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < gvSearchResults.Rows.Count; i++)
            {
                for (int k = 0; k < gvSearchResults.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(gvSearchResults.Rows[i].Cells[k].Text + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            sb.Replace("&nbsp;", ""); //remove html from empty fields
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        protected void gvSearchResults_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSearchResults.PageIndex = e.NewPageIndex;
            BindGridView();
        }

        protected void ResetAll_OnClick(object sender, EventArgs e)
        {
            Utilities.Patients.ResetAllUserAcceptance();
            BindGridView();
        }
    }
}