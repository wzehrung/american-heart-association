﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GRCBase;

namespace Heart360Admin.Pages
{
    public partial class PreviewAnnouncement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/home.aspx"))
                {
                    Response.Redirect("~/Login.aspx?ReturnURL=" + Server.UrlEncode(HttpContext.Current.Request.RawUrl));
                    return;
                }
                Response.Redirect("~/Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                if (Utilities.SessionInfo.GetProviderAnnouncementPreviewFromSession() != null)
                {
                    Utilities.Announcement obj = Utilities.SessionInfo.GetProviderAnnouncementPreviewFromSession();
                    hZoneTitle.InnerHtml = obj.Title;
                    litGuidingText.Text = obj.Text;
                }
            }
        }
    }
}
