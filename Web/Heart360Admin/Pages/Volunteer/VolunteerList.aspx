﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="VolunteerList.aspx.cs" Inherits="Heart360Admin.Pages.Volunteers.VolunteerList" EnableViewState="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
<h1>
    Assign Campaign</h1>
<p>
    Below is the list of all specified user types for which you can assign their Campaigns.</p>
<table class="admin-box" cellpadding="0" cellspacing="0">
    <tr>
        <td class="align-left" style="text-align: right;">User Type:&nbsp;
            <asp:DropDownList runat="server" ID="ddlUserType" AutoPostBack="true" Width="200" OnSelectedIndexChanged="UserType_SelectedIndexChanged" >
                <asp:ListItem Value="0">All</asp:ListItem>
                <asp:ListItem Value="1">Provider</asp:ListItem>
                <asp:ListItem Value="2">Volunteer</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:CheckBox runat="server" ID="cbOnlyNullCampaigns" AutoPostBack="true" Checked="true" OnCheckedChanged="cbOnlyNullCampaigns_CheckedChanged" Text="Only Users without a Campaign" ></asp:CheckBox>
        </td> 
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="up1">
                <ContentTemplate>
                    <asp:GridView ShowHeader="true" EnableViewState="false" ID="gvVolunteerList"
                        runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="There are no Resources."
                        AllowPaging="True" PageSize="25" ShowFooter="False" OnRowDataBound="gvVolunteerList_RowDataBound"
                        OnPageIndexChanging="gvVolunteerList_PageIndexChanging" GridLines="Horizontal" CssClass="provider-grid"
                        PagerStyle-CssClass="table-page-index">
                        <Columns>
                            <asp:TemplateField HeaderText="Last Name" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litLastName"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal ID="litFirstName" runat="server"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Campaign" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal ID="litCampaignName" runat="server"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="Actions" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <a id="aChngCampaign" onserverclick="aChngCampaign_serverclick" runat="server">Change Campaign</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" />
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
    </div>
</asp:Content>
