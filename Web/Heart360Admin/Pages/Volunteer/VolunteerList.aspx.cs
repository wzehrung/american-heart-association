﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AHAHelpContent;

namespace Heart360Admin.Pages.Volunteers
{
    public partial class VolunteerList : System.Web.UI.Page
    {

        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // PJB: Save our checkbox value and current page index here.
            // Tried to do this using ViewState but couldn't get it to work.
            System.Web.HttpContext.Current.Session["VolunteerOnlyNullCampaigns"] = cbOnlyNullCampaigns.Checked;
            System.Web.HttpContext.Current.Session["VolunteerPageIndex"] = gvVolunteerList.PageIndex;
            System.Web.HttpContext.Current.Session["VolunteerUserTypeIndex"] = ddlUserType.SelectedIndex;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // PJB: apply our "state" values from current session.
                if (System.Web.HttpContext.Current.Session["VolunteerOnlyNullCampaigns"] != null)
                    cbOnlyNullCampaigns.Checked = (Boolean)System.Web.HttpContext.Current.Session["VolunteerOnlyNullCampaigns"];
                if (System.Web.HttpContext.Current.Session["VolunteerPageIndex"] != null)
                    gvVolunteerList.PageIndex = (int)System.Web.HttpContext.Current.Session["VolunteerPageIndex"];
                if (System.Web.HttpContext.Current.Session["VolunteerUserTypeIndex"] != null)
                    ddlUserType.SelectedIndex = (int)System.Web.HttpContext.Current.Session["VolunteerUserTypeIndex"];
            }

            BindGridView();

            
        }

        private void BindGridView()
        {
            switch (ddlUserType.SelectedIndex)
            {
                case 1:     // providers
                    gvVolunteerList.DataSource = AHAHelpContent.Volunteer.GetAllProviders(cbOnlyNullCampaigns.Checked);
                    gvVolunteerList.DataBind();
                    break;

                case 2:     // volunteers
                    gvVolunteerList.DataSource = AHAHelpContent.Volunteer.GetAllVolunteers(cbOnlyNullCampaigns.Checked);
                    gvVolunteerList.DataBind();
                    break;

                case 0:     // both volunteers and providers
                default:
                    gvVolunteerList.DataSource = AHAHelpContent.Volunteer.GetAllProvidersAndVolunteers(cbOnlyNullCampaigns.Checked);
                    gvVolunteerList.DataBind();
                    break;
            }
        }

        protected void gvVolunteerList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVolunteerList.PageIndex = e.NewPageIndex;
            BindGridView();
        }

        protected void gvVolunteerList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            {

                AHAHelpContent.Volunteer objVol = e.Row.DataItem as AHAHelpContent.Volunteer;
                Literal litFirstName = e.Row.FindControl("litFirstName") as Literal;
                Literal litLastName = e.Row.FindControl("litlastName") as Literal;
                Literal litEmail = e.Row.FindControl("litEmail") as Literal;
                Literal litCampaignName = e.Row.FindControl("litCampaignName") as Literal;
                HtmlAnchor aChngCampaign = e.Row.FindControl("aChngCampaign") as HtmlAnchor;
                

                litFirstName.Text = objVol.FirstName;
                litLastName.Text = objVol.LastName;
                litEmail.Text = objVol.Email;
                litCampaignName.Text = objVol.CampaignName;
                aChngCampaign.Attributes.Add("UID", objVol.UserID.ToString());

                if( !string.IsNullOrEmpty(objVol.CampaignName) )
                    aChngCampaign.Attributes.Add("CID", objVol.CampaignID.ToString());
            }
        }


        protected void aChngCampaign_serverclick(object sender, EventArgs e)
        {
            HtmlAnchor  aChngCampaign = sender as HtmlAnchor;
            string      urlRedirect = "/Pages/Volunteer/ChangeCampaign.aspx?UID=" + aChngCampaign.Attributes["UID"];

            if ( !string.IsNullOrEmpty(aChngCampaign.Attributes["CID"]) )
                urlRedirect += "&CID=" + aChngCampaign.Attributes["CID"];

            Response.Redirect( urlRedirect );

            
        }

/**
        override protected void SavePageStateToPersistenceMedium(object state)
        {
 	        base.SavePageStateToPersistenceMedium(state);
        }

        override protected Object LoadPageStateFromPersistenceMedium()
        {
            return base.LoadPageStateFromPersistenceMedium();
        }
**/
        protected void cbOnlyNullCampaigns_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;           

        }

        protected void UserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
        }
    }
}
