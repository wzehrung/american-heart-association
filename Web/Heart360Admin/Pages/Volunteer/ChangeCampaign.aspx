﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="ChangeCampaign.aspx.cs" Inherits="Heart360Admin.Pages.Volunteer.ChangeCampaign" %>

<%@ Register Src="~/UserCtrls/Volunteer/EditVolunteerCampaignCtrl.ascx" TagName="EditCampaignCtrl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <uc1:EditCampaignCtrl ID="EditCampaignCtrl1" DisplayMode="Edit" runat="server" />
        <br />
        <br />
    </div>
</asp:Content>
