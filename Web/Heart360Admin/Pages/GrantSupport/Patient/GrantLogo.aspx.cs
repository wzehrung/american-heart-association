﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;

namespace Heart360Admin.Pages.GrantSupport.Patient
{
    public partial class GrantLogo : System.Web.UI.Page
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (objUserContext != null && !string.IsNullOrEmpty(Request["GID"]))
            {
                AHAHelpContent.GrantSupport objGrantSupport = AHAHelpContent.GrantSupport.GetGrantSupportByID(Convert.ToInt32(Request["GID"]),Utilities.Utility.GetCurrentLanguageID());
                if (objGrantSupport.Logo != null)
                {
                    Response.AddHeader("Content-Length", objGrantSupport.Logo.Length.ToString());
                    Response.AddHeader("Content-Type", objGrantSupport.ContentType);
                    Response.ContentType = objGrantSupport.ContentType;
                    Response.BinaryWrite(objGrantSupport.Logo);
                    Response.Flush();
                    Response.End();
                }

            }
        }
    }
}
