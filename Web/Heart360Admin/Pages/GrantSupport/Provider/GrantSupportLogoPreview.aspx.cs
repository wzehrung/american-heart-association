﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.GrantSupport.Provider
{
    public partial class GrantSupportLogoPreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           

            if (Utilities.SessionInfo.GetProviderGrantPreviewFromSession() != null)
            {
                Utilities.Utility.GrantLogo objGrantLogo = Utilities.SessionInfo.GetProviderGrantPreviewFromSession();
                if (objGrantLogo != null && objGrantLogo.Content != null)
                {
                    Response.ContentType = objGrantLogo.ContentType;
                    Response.AddHeader("Content-Type", objGrantLogo.ContentType);
                    Response.AddHeader("Content-Length", objGrantLogo.Content.Length.ToString());
                    Response.BinaryWrite(objGrantLogo.Content);
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}
