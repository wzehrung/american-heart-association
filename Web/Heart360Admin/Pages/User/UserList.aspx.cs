﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AHAHelpContent;

namespace Heart360Admin.Pages.User
{
    public partial class UserList : System.Web.UI.Page
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGridView();
        }

        private void BindGridView()
        {
            gvUserList.DataSource = AdminUser.GetAllAdminUsers();
            gvUserList.DataBind();
        }

        protected void gvUserList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUserList.PageIndex = e.NewPageIndex;
            BindGridView();
        }

        protected void gvUserList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            {
                AdminUser objAdminUser = e.Row.DataItem as AdminUser;
                Literal litUserName = e.Row.FindControl("litUserName") as Literal;
                Literal litName = e.Row.FindControl("litName") as Literal;
                Literal litEmail = e.Row.FindControl("litEmail") as Literal;
                Literal litRoleName = e.Row.FindControl("litRoleName") as Literal;
                Literal litPipeChar = e.Row.FindControl("litPipeChar") as Literal;
                Literal litBusinessName = e.Row.FindControl("litBusinessName") as Literal;
                HtmlAnchor aEdit = e.Row.FindControl("aEdit") as HtmlAnchor;
                HtmlAnchor aDisable = e.Row.FindControl("aDisable") as HtmlAnchor;
                HtmlAnchor aChngPassword = e.Row.FindControl("aChngPassword") as HtmlAnchor;

                litUserName.Text = objAdminUser.UserName;
                litName.Text = objAdminUser.Name;
                litEmail.Text = objAdminUser.Email;
                litRoleName.Text = objAdminUser.RoleName;
                litBusinessName.Text = (string.IsNullOrEmpty(objAdminUser.BusinessName)) ? "N/A" : objAdminUser.BusinessName;
                aEdit.Attributes.Add("UID", objAdminUser.AdminUserID.ToString());

                if (objAdminUser.IsActive)
                {
                    aDisable.InnerHtml = "Disable";
                    aDisable.Attributes.Add("onclick", "return OnConfirmDelete('Do you really want to disable this user?');");
                    aDisable.ServerClick += new EventHandler(aDisable_onserverclick);
                }
                else
                {
                    aDisable.InnerHtml = "Enable";
                    aDisable.ServerClick += new EventHandler(aEnable_onserverclick);
                }
                aDisable.Attributes.Add("UID", objAdminUser.AdminUserID.ToString());
                if (objUserContext.UserID == objAdminUser.AdminUserID)
                {
                    aDisable.Visible = false;
                    litPipeChar.Visible = false;
                }
                aChngPassword.Attributes.Add("UID", objAdminUser.AdminUserID.ToString());
            }
        }

        protected void aEdit_serverclick(object sender, EventArgs e)
        {
            HtmlAnchor aEdit = sender as HtmlAnchor;
            Response.Redirect("/Pages/User/EditUser.aspx?UID=" + aEdit.Attributes["UID"]);
        }

        protected void aDisable_onserverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDisable = sender as HtmlAnchor;
            AdminUser.DisableAdminUser(Convert.ToInt32(aDisable.Attributes["UID"].ToString()));
            BindGridView();
        }

        protected void aEnable_onserverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDisable = sender as HtmlAnchor;
            AdminUser.EnableAdminUser(Convert.ToInt32(aDisable.Attributes["UID"].ToString()));
            BindGridView();
        }

        protected void aChngPassword_serverclick(object sender, EventArgs e)
        {
            HtmlAnchor aChngPassword = sender as HtmlAnchor;
            Response.Redirect("/Pages/User/ChangePassword.aspx?UID=" + aChngPassword.Attributes["UID"]);
        }
    }
}
