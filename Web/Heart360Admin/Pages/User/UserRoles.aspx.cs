﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.User
{
    public partial class UserRoles : System.Web.UI.Page
    {
        public int intRoleID
        {
            get
            {
                string strRoleID = null;

                if (Request.QueryString["RoleID"] != "" && Request.QueryString["RoleID"] != null)
                {
                    strRoleID = Request.QueryString["RoleID"];
                }
                else
                {
                    strRoleID = "1";
                }
                int ic = Convert.ToInt32(strRoleID);
                return ic;
            }  
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                litSuccess.Text = "<div class=\"sucess\">Permissions have been successfully saved</div>";
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _LoadRolesDropdown();
            _LoadPatientsCheckBoxList();
            _LoadProvidersCheckBoxList();
            _LoadMiscCheckBoxList();
            _LoadPatientReportsCheckBoxList();
            _LoadProviderReportsCheckBoxList();
        }

        public void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("UserRoles.aspx?RoleID=" + ddlRole.SelectedValue.ToString());
        }

        private void _LoadRolesDropdown()
        {
            List<AHAHelpContent.AdminRoles> list = AHAHelpContent.AdminRoles.GetAllAdminRoles();
            ddlRole.DataSource = list;
            ddlRole.DataTextField = "Name";
            ddlRole.DataValueField = "RoleID";
            ddlRole.SelectedValue = intRoleID.ToString();
            ddlRole.DataBind();
        }

        private void _LoadPatientsCheckBoxList()
        {
            List<AHAHelpContent.AdminMenu> list = AHAHelpContent.AdminMenu.GetAllAdminMenuByCategoryIDByRoleID(Convert.ToInt32("1"), intRoleID);
            cblMenuPatients.DataSource = list;
            cblMenuPatients.DataTextField = "Name";
            cblMenuPatients.DataValueField = "MenuID";
            cblMenuPatients.DataBind();

            for(int i=0; i<cblMenuPatients.Items.Count;++i)
            {
            
                if (list[i].Enabled == true)
                {
                    cblMenuPatients.Items[i].Selected = true;
                }
            }
        }

        private void _LoadProvidersCheckBoxList()
        {
            List<AHAHelpContent.AdminMenu> list = AHAHelpContent.AdminMenu.GetAllAdminMenuByCategoryIDByRoleID(Convert.ToInt32("2"), intRoleID);
            cblMenuProviders.DataSource = list;
            cblMenuProviders.DataTextField = "Name";
            cblMenuProviders.DataValueField = "MenuID";
            cblMenuProviders.DataBind();

            for (int i = 0; i < cblMenuProviders.Items.Count; ++i)
            {

                if (list[i].Enabled == true)
                {
                    cblMenuProviders.Items[i].Selected = true;
                }
            }
        }

        private void _LoadMiscCheckBoxList()
        {
            List<AHAHelpContent.AdminMenu> list = AHAHelpContent.AdminMenu.GetAllAdminMenuByCategoryIDByRoleID(Convert.ToInt32("3"), intRoleID);
            cblMenuMisc.DataSource = list;
            cblMenuMisc.DataTextField = "Name";
            cblMenuMisc.DataValueField = "MenuID";
            cblMenuMisc.DataBind();

            for (int i = 0; i < cblMenuMisc.Items.Count; ++i)
            {
                if (list[i].Enabled == true)
                {
                    cblMenuMisc.Items[i].Selected = true;

                    // Don't allow the superadmin to lock themselves out from managing
                    // users or role permissions
                    if (list[i].MenuID == 25 || list[i].MenuID == 3)
                    {
                        cblMenuMisc.Items[i].Enabled = false;
                    }
                }
            }
        }

        private void _LoadPatientReportsCheckBoxList()
        {
            List<AHAHelpContent.AdminReport> list = AHAHelpContent.AdminReport.GetAllAdminReportByCategoryIDByRoleID(Convert.ToInt32("4"), intRoleID);
            cblReportPatients.DataSource = list;
            cblReportPatients.DataTextField = "Name";
            cblReportPatients.DataValueField = "ReportID";
            cblReportPatients.DataBind();

            for (int i = 0; i < cblReportPatients.Items.Count; ++i)
            {

                if (list[i].Enabled == true)
                {
                    cblReportPatients.Items[i].Selected = true;
                }
            }
        }

        private void _LoadProviderReportsCheckBoxList()
        {
            List<AHAHelpContent.AdminReport> list = AHAHelpContent.AdminReport.GetAllAdminReportByCategoryIDByRoleID(Convert.ToInt32("5"), intRoleID);
            cblReportProviders.DataSource = list;
            cblReportProviders.DataTextField = "Name";
            cblReportProviders.DataValueField = "ReportID";
            cblReportProviders.DataBind();

            for (int i = 0; i < cblReportProviders.Items.Count; ++i)
            {

                if (list[i].Enabled == true)
                {
                    cblReportProviders.Items[i].Selected = true;
                }
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            // Patients Menu
            List<AHAHelpContent.AdminMenu> listPatients = new List<AHAHelpContent.AdminMenu>();
            for (int i = 0; i < cblMenuPatients.Items.Count; ++i)
            {
                AHAHelpContent.AdminMenu mnu = new AHAHelpContent.AdminMenu();
                mnu.MenuID = Convert.ToInt32(cblMenuPatients.Items[i].Value);
                mnu.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                mnu.Enabled = cblMenuPatients.Items[i].Selected;
                listPatients.Add(mnu);
            }
            AHAHelpContent.AdminMenu.SetAdminMenuByRoleID(listPatients);

            // Providers Menu
            List<AHAHelpContent.AdminMenu> listProviders = new List<AHAHelpContent.AdminMenu>();
            for (int i = 0; i < cblMenuPatients.Items.Count; ++i)
            {
                AHAHelpContent.AdminMenu mnu = new AHAHelpContent.AdminMenu();
                mnu.MenuID = Convert.ToInt32(cblMenuProviders.Items[i].Value);
                mnu.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                mnu.Enabled = cblMenuProviders.Items[i].Selected;
                listProviders.Add(mnu);
            }
            AHAHelpContent.AdminMenu.SetAdminMenuByRoleID(listProviders);

            // Misc Menu
            List<AHAHelpContent.AdminMenu> listMisc = new List<AHAHelpContent.AdminMenu>();
            for (int i = 0; i < cblMenuMisc.Items.Count; ++i)
            {
                AHAHelpContent.AdminMenu mnu = new AHAHelpContent.AdminMenu();
                mnu.MenuID = Convert.ToInt32(cblMenuMisc.Items[i].Value);
                mnu.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                mnu.Enabled = cblMenuMisc.Items[i].Selected;
                listMisc.Add(mnu);
            }
            AHAHelpContent.AdminMenu.SetAdminMenuByRoleID(listMisc);

            // Reports/Patients Menu
            List<AHAHelpContent.AdminReport> listReportPatients = new List<AHAHelpContent.AdminReport>();
            for (int i = 0; i < cblReportPatients.Items.Count; ++i)
            {
                AHAHelpContent.AdminReport mnu = new AHAHelpContent.AdminReport();
                mnu.ReportID = Convert.ToInt32(cblReportPatients.Items[i].Value);
                mnu.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                mnu.Enabled = cblReportPatients.Items[i].Selected;
                listReportPatients.Add(mnu);
            }
            AHAHelpContent.AdminReport.SetAdminReportByRoleID(listReportPatients);

            // Reports/Providers Menu
            List<AHAHelpContent.AdminReport> listReportProviders = new List<AHAHelpContent.AdminReport>();
            for (int i = 0; i < cblReportProviders.Items.Count; ++i)
            {
                AHAHelpContent.AdminReport mnu = new AHAHelpContent.AdminReport();
                mnu.ReportID = Convert.ToInt32(cblReportProviders.Items[i].Value);
                mnu.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                mnu.Enabled = cblReportProviders.Items[i].Selected;
                listReportProviders.Add(mnu);
            }
            AHAHelpContent.AdminReport.SetAdminReportByRoleID(listReportProviders);

            lnkSave.PostBackUrl = "UserRoles.aspx";
            //Response.Redirect("UserRoles.aspx");
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {

        }
    }
}