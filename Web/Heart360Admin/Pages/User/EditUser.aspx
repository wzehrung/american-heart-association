﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="EditUser.aspx.cs" Inherits="Heart360Admin.Pages.User.EditUser" %>

<%@ Register Src="~/UserCtrls/User/AddUserCtrl.ascx" TagName="AddUserCtrl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <uc1:AddUserCtrl ID="AddUserCtrl1" DisplayMode="Edit" runat="server" />
        <br />
        <br />
    </div>
</asp:Content>
