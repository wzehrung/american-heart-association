﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserRoles.aspx.cs" Inherits="Heart360Admin.Pages.User.UserRoles" MasterPageFile="~/MasterPages/Admin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <h1><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Roles")%></h1>
        <p> </p>

        <table width="100%" class="admin-box" cellpadding="0" cellspacing="0">
            <tr>
                <td class="align-right menubar">
                   
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel runat="server" ID="up1">
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left">Select Role: <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged"></asp:DropDownList></td>
                                </tr>
                            </table>

                            <table width="100%">
                                <tr>
                                    <td colspan="2" valign="top">
                                        <!-- BEGIN MENU PERMISSIONS -->
                                        <table id="tblMenu" class="ManageRolesTable" cellspacing="0">
                                            
                                            <th colspan="3">Menu Permissions</th>

                                            <tr>
                                                <td class="rolesHeader">Patients/Participants</td>
                                                <td class="rolesHeader">Providers/Volunteers</td>
                                                <td class="rolesHeader">Miscellaneous</td>
                                            </tr>
                                        
                                            <tr>
                                                <td class="checkboxList" valign="top" nowrap>
                                                    <asp:CheckBoxList ID="cblMenuPatients" runat="server"></asp:CheckBoxList>
                                                    <div style="padding-left: 20px;">
                                                        <asp:CheckBoxList ID="cblReportPatients" runat="server"></asp:CheckBoxList>
                                                    </div>
                                                </td>
                                                <td class="checkboxList" valign="top" nowrap>
                                                    <asp:CheckBoxList ID="cblMenuProviders" runat="server"></asp:CheckBoxList>
                                                    <div style="padding-left: 20px;">
                                                        <asp:CheckBoxList ID="cblReportProviders" runat="server"></asp:CheckBoxList>
                                                    </div>
                                                </td>
                                                <td class="checkboxList" valign="top" nowrap><asp:CheckBoxList ID="cblMenuMisc" runat="server"></asp:CheckBoxList></td>
                                            </tr>

                                        </table>
                                        <!-- END MENU PERMISSIONS -->            
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><asp:Literal ID="litSuccess" runat="server" ></asp:Literal></td>
                                    <td align="right">
                                        <asp:LinkButton runat="server" ID="lnkSave" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Save</em></strong></asp:LinkButton>
                                        &nbsp;<asp:LinkButton runat="server" ID="lnkCancel" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Cancel</em></strong></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
