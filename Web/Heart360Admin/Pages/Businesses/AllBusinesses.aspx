﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Admin.Master" CodeBehind="AllBusinesses.aspx.cs" Inherits="Heart360Admin.Pages.Businesses.AllBusinesses" %>

<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<%@ Register Src="~/UserCtrls/Business/ActiveBusinesses.ascx" TagName="ActiveBusinesses" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <div class="copyBox">
                <h1>
                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Businesses")%></h1>
                <p>
                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New_Business")%></p>
                <div class="validation_summary">
                    <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummarycreate"
                        ValidationGroup="Search" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                        DisplayMode="BulletList" ForeColor="#CE2930" />
                </div>
                <asp:LinkButton runat="server" ID="lnkNew" OnClick="lnkNew_OnClick" CssClass="aha-small-button with-arrow right"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></asp:LinkButton>
                <h1>
                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Active_Businesses")%></h1>
                <table class="admin-box" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <uc1:ActiveBusinesses ID="ActiveBusinesses1" runat="server" />
                        </td>
                    </tr>
                </table>
                <br />
                <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
