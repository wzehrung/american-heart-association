﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Heart360Admin.Pages.Businesses
{
    public partial class AllBusinesses : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnkNew_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Businesses/CreateBusiness.aspx");
        }
    }
}