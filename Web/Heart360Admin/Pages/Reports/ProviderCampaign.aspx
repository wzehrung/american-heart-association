﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="ProviderCampaign.aspx.cs" Inherits="Heart360Admin.Pages.Reports.ProviderCampaign" %>

<%@ Register Src="~/Reports/Ctrls/ProviderCampaign.ascx" TagName="Parameters" TagPrefix="uc1" %>

<%@ Register Src="../../Reports/Ctrls/CampaignProvider/Providers.ascx" TagName="Providers" TagPrefix="uc1" %>

<%@ Register Src="../../Reports/Ctrls/CampaignProvider/NewProviderVisits.ascx" TagName="NewProviderVisits" TagPrefix="uc4" %>
<%@ Register Src="../../Reports/Ctrls/CampaignProvider/AllProviderVisits.ascx" TagName="AllProviderVisits" TagPrefix="uc5" %>

<%@ Register Src="../../Reports/Ctrls/CampaignProvider/NewProviderVisitsPercentage.ascx" TagName="NewProviderVisitsPercentage" TagPrefix="uc6" %>
<%@ Register Src="../../Reports/Ctrls/CampaignProvider/AllProviderVisitsPercentage.ascx" TagName="AllProviderVisitsPercentage" TagPrefix="uc7" %>

<%@ Register Src="../../Reports/Ctrls/CampaignProvider/PatientCount.ascx" TagName="PatientCount" TagPrefix="uc8" %>
<%@ Register Src="../../Reports/Ctrls/CampaignProvider/PatientProviderConnection.ascx" TagName="PatientProviderConnection" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="copyBox report">

        <uc1:Parameters ID="ucParameters" runat="server" />

        <div runat="server" id="divReport" visible="false" enableviewstate="false">

<!--
            <div class="clearer"></div>
            <div class="right report-link">
                <a href="javascript:;" onclick="ShowHideAllReportGraphs(true);">Show all graphs</a>&nbsp;|&nbsp;<a href="javascript:;" onclick="ShowHideAllReportGraphs(false);">Hide all graphs</a>
            </div>
-->

            <h3 class="left"><%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%> <%=strUserType%></h3>
            <div class="clearer"></div>
            <uc1:Providers EnableViewState="false" ID="ucProviders" runat="server" />
            <div class="clearer"></div>

            <div id="Div1" runat="server" visible="false">
            <div class="clearer"></div>
            </div>

            <h3>Reported Visits (New <%=strUserType%> only who visited <%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%> during the time frame selected)</h3>
            <div class="clearer"></div>
            <uc4:NewProviderVisits EnableViewState="false" ID="ucNewProviderVisits" runat="server" />
            <div class="clearer"></div>

            <h3>Reported Visits (All <%=strUserType%> only who visited <%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%> during the time frame selected)</h3>
            <div class="clearer"></div>
            <uc5:AllProviderVisits EnableViewState="false" ID="ucAllProviderVisits" runat="server" />
            <div class="clearer"></div>

            <h3>Reported Visits in % (New <%=strUserType%> only )</h3>
            <div class="clearer"></div>
            <uc6:NewProviderVisitsPercentage EnableViewState="false" ID="ucNewProviderVisitsPercentage" runat="server" />
            <div class="clearer"></div>

            <h3>Reported Visits in % (All <%=strUserType%>)</h3>
            <div class="clearer"></div>
            <uc7:AllProviderVisitsPercentage EnableViewState="false" ID="ucAllProviderVisitsPercentage" runat="server" />
            <div class="clearer"></div>

            <h3>Participants - <%=strUserType%> Connection Count</h3>
            <div class="clearer"></div>
            <uc8:PatientCount ID="ucPatientCount" runat="server" />
            <div class="clearer"></div>

            <h3>Participants connected to <%=strUserType%></h3>
            <div class="clearer"></div>
            <uc9:PatientProviderConnection ID="ucPatientProviderConnection" runat="server" />

            <asp:HiddenField EnableViewState="false" ID="hidFormLoaded" runat="server" />
        </div>
    </div>

</asp:Content>

