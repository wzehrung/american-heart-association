﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.Reports
{
    public partial class ProviderCampaign : System.Web.UI.Page, IReportDataManagerReporter
    {

        public string strUserType
        {
            get
            {
                string str = ucParameters.UserTypeID.ToString();

                switch (str)
                {
                    case "1":
                        str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Provider_UserType_Provider").ToString();
                        break;
                    case "2":
                        str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Provider_UserType_Volunteer").ToString();
                        break;
                    default:
                        str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Provider_UserType_All").ToString();
                        break;
                }
                return str;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ucParameters.IReportDataManagerReporter = this;
            /**
            ucProviders.IReportDataManager = this;
            //ucReportedUploads.IReportDataManager = this;
            ucNewProviderVisits.IReportDataManager = this;
            ucAllProviderVisits.IReportDataManager = this;
            ucNewProviderVisitsPercentage.IReportDataManager = this;
            ucAllProviderVisitsPercentage.IReportDataManager = this;
            ucFilterCtrl.IReportDataManager = this;
            ucPatientCount.IReportDataManager = this;
            ucPatientProviderConnection.IReportDataManager = this;
             **/

            if (Page.IsPostBack)
            {
                if (iHidReset == 2)
                {
                    if (hidFormLoaded.Value == "1")
                    {
                        LoadItems( ucParameters );
                    }
                }
            }
        }

        #region IReportDataManagerReporter Members

        public void LoadItems( IReportDataManager iRdm )
        {
            divReport.Visible = true;
// deprecated            List<ReportFilter> listFilter = ucFilterCtrl.ReportFilterCollection;

            ucProviders.LoadItems(iRdm);
            ucNewProviderVisits.LoadItems(iRdm);
            ucAllProviderVisits.LoadItems(iRdm);
            ucNewProviderVisitsPercentage.LoadItems(iRdm);
            ucAllProviderVisitsPercentage.LoadItems(iRdm);
            ucPatientProviderConnection.LoadItems(iRdm);
            ucPatientCount.LoadItems(iRdm);


            hidFormLoaded.Value = "1";

            /** Old way to load reports
                        //ucReportedUploads.LoadItems(listFilter.Where(f => f.ReportFilterType == ReportFilterType.ReportedUploads).SingleOrDefault());
                        ucNewProviderVisits.LoadItems(listFilter.Where(f => f.ReportFilterType == ReportFilterType.NewAllUserVisits).SingleOrDefault());
                        ucAllProviderVisits.LoadItems(listFilter.Where(f => f.ReportFilterType == ReportFilterType.NewAllUserVisits).SingleOrDefault());
                        ucNewProviderVisitsPercentage.LoadItems(listFilter.Where(f => f.ReportFilterType == ReportFilterType.NewAllUserVisits).SingleOrDefault());
                        ucAllProviderVisitsPercentage.LoadItems(listFilter.Where(f => f.ReportFilterType == ReportFilterType.NewAllUserVisits).SingleOrDefault());
                        ucPatientProviderConnection.LoadItems(listFilter.Where(f => f.ReportFilterType == ReportFilterType.PatientProviderConnection).SingleOrDefault());
            **/

        }


        public int? iHidReset
        {
            get { return ucParameters.iHidReset; }
        }
        #endregion
    }
}