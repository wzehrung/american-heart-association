﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.Reports
{
    public partial class PatientReport : System.Web.UI.Page, IReportDataManagerReporter
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.Page.PreLoad += new EventHandler(OnConstructForm);
        }

        protected void OnConstructForm(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ucParameters.IReportDataManagerReporter = this;
/**
            ucUsers.IReportDataManager = this;
            ucTracker.IReportDataManager = this;
            ucUploadTracker.IReportDataManager = this;
            ucReportedUploads.IReportDataManager = this;
            ucGender.IReportDataManager = this;
            ucEthnicity.IReportDataManager = this;
            ucAge.IReportDataManager = this;
            ucAllUserVisits.IReportDataManager = this;
            ucAllUserVisitsPercentage.IReportDataManager = this;
            ucFilter.IReportDataManager = this;
**/
            if (Page.IsPostBack)
            {
                if (iHidReset == 2)
                {
                    if (hidFormLoaded.Value == "1")
                    {
                        LoadItems( ucParameters );
                    }
                }
            }
        }


        public void LoadItems( IReportDataManager iRdm )
        {
            divReport.Visible = true;
//            List<ReportFilter> listFilter = ucFilter.ReportFilterCollection;


            ucUsers.LoadItems(iRdm);
            ucTracker.LoadItems( iRdm );
            ucUploadTracker.LoadItems( iRdm );
            ucReportedUploads.LoadItems(iRdm);
            ucGender.LoadItems(iRdm);
            ucEthnicity.LoadItems(iRdm);
            ucAge.LoadItems(iRdm);
            ucAllUserVisits.LoadItems(iRdm);
            ucAllUserVisitsPercentage.LoadItems(iRdm);

            hidFormLoaded.Value = "1";
        }


        public int? iHidReset
        {
            get { return ucParameters.iHidReset; }
        }

    }
}
