﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="CampaignReport.aspx.cs" Inherits="Heart360Admin.Pages.Reports.CampaignReport" %>

<%@ Register Src="~/Reports/Ctrls/CampaignReport.ascx" TagName="Parameters" TagPrefix="uc1" %>

<%@ Register Src="../../Reports/Ctrls/Campaign/Users.ascx" TagName="Users" TagPrefix="uc2" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/Tracker.ascx" TagName="Tracker" TagPrefix="uc3" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/Gender.ascx" TagName="Gender" TagPrefix="uc4" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/Age.ascx" TagName="Age" TagPrefix="uc5" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/AllUserVisits.ascx" TagName="AllUserVisits" TagPrefix="uc7" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/AllUserVisitsPercentage.ascx" TagName="AllUserVisitsPercentage" TagPrefix="uc8" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/UploadTracker.ascx" TagName="UploadTracker" TagPrefix="uc10" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/Ethnicity.ascx" TagName="Ethnicity" TagPrefix="uc11" %>
<%@ Register Src="../../Reports/Ctrls/Campaign/ReportedUploads.ascx" TagName="ReportedUploads" TagPrefix="uc12" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="copyBox report">

        <uc1:Parameters ID="ucParameters" runat="server" />

        <div runat="server" id="divReport" visible="false" enableviewstate="false">

<!--
            <div class="clearer"></div>
            <div class="right report-link">
                <a href="javascript:;" onclick="ShowHideAllReportGraphs(true);">Show all graphs</a>&nbsp;|&nbsp;<a href="javascript:;" onclick="ShowHideAllReportGraphs(false);">Hide all graphs</a>
            </div>
-->            
            
            <h3 class="left"><%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%> Users</h3>
            <div class="clearer"></div>

            <uc2:Users EnableViewState="false" ID="ucUsers" runat="server" />
            <div class="clearer"></div>

            <div runat="server" visible="false">
            </div>

            <h3>% Tracking (Monthly Cumulative)</h3>
            <div class="clearer"></div>
            <uc3:Tracker EnableViewState="false" ID="ucTracker" runat="server" />
            <div class="clearer"></div>
            
            <h3>Reported Uploads (# of users who uploaded data 2X's per month.)</h3>
            <div class="clearer"></div>
            <uc12:ReportedUploads EnableViewState="false" ID="ucReportedUploads" runat="server" />
            <div class="clearer"></div>
            
            <h3>% Method of Upload/Reading Source (Monthly Cumulative)</h3>
            <div class="clearer"></div>
            <uc10:UploadTracker EnableViewState="false" ID="ucUploadTracker" runat="server" />
            <div class="clearer"></div>

            <h3>Reported Visits (All/New Users) – (New Patients/Participants only who visited <%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%> during the time frame selected.)</h3>
            <div class="clearer"></div>
            <uc7:AllUserVisits EnableViewState="false" ID="ucAllUserVisits" runat="server" />
            <div class="clearer"></div>

            <h3>Reported Visits in % (All/New Users) – (New Patients/Participants only who visited <%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%> during the time frame selected.)</h3>
            <div class="clearer"></div>
            <uc8:AllUserVisitsPercentage EnableViewState="false" ID="ucAllUserVisitsPercentage" runat="server" />
            <div class="clearer"></div>

            <h3>Gender (Monthly Cumulative)</h3>
            <div class="clearer"></div>
            <uc4:Gender EnableViewState="false" ID="ucGender" runat="server" />
            <div class="clearer"></div>

            <h3>Ethnicity (Monthly Cumulative)</h3>
            <div class="clearer"></div>
            <uc11:Ethnicity EnableViewState="false" ID="ucEthnicity" runat="server" />
            <div class="clearer"></div>

            <h3>Age Breakdown (Monthly Cumulative)</h3>
            <div class="clearer"></div>
            <uc5:Age EnableViewState="false" ID="ucAge" runat="server" />
            <div class="clearer"></div>            
            
            <asp:HiddenField EnableViewState="false" ID="hidFormLoaded" runat="server" />
        </div>
    </div>

</asp:Content>
