﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="PatientHealthReport.aspx.cs" Inherits="Heart360Admin.Pages.Reports.PatientHealthReport" %>

<%@ Register Src="~/Reports/Ctrls/Patient/PatientHealthReport.ascx" TagName="PatientHealthReport"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <uc1:PatientHealthReport ID="PatientHealthReport1" runat="server" />
    </div>
</asp:Content>
