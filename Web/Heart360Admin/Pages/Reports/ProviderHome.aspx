﻿<%@ Page MasterPageFile="~/MasterPages/Admin.Master" Language="C#" AutoEventWireup="true" CodeBehind="ProviderHome.aspx.cs" Inherits="Heart360Admin.Pages.Reports.ProviderHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <h1><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Provider_Page_Title")%></h1>
        <table width="100%" class="admin-box" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <strong><a id="Rpt5" href="~/Pages/Reports/ProviderCampaign.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Provider_Page_Campaign_Title")%></a></strong>
                </td>
            </tr>

            <tr>
                <td>
                    <strong><a id="Rpt4" href="~/Pages/Reports/ProviderReport.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Provider_Page_Title")%></a></strong>
                </td>
            </tr>
            <!--
            <tr>
                <td>
                    <strong><a id="A1" href="~/Pages/Volunteer/VolunteerList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Assign_Campaign_Title")%></a></strong>
                </td>
            </tr>
            -->
        </table>
    </div>
</asp:Content>
