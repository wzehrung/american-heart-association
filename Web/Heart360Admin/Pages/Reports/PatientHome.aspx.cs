﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.Reports
{
    public partial class Home : System.Web.UI.Page
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get a reference to the master page
            MasterPage ctl00 = FindControl("ctl00") as MasterPage;

            ContentPlaceHolder MainContent = ctl00.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;
 
            List<AHAHelpContent.AdminReport> list = AHAHelpContent.AdminReport.GetAdminReportByID(objUserContext.UserID);

            for (int i = 0; i < list.Count; i++)
            {
                string id = "Rpt" + Convert.ToString(i + 1);
                HtmlAnchor aMenu = MainContent.FindControl(id) as HtmlAnchor;
                if (aMenu != null)
                {
                    if (list[i].Enabled == true)
                    {
                        aMenu.Visible = true;
                    }
                    else
                    {
                        aMenu.Visible = false;
                    }
                }
            }
        }
    }
}
