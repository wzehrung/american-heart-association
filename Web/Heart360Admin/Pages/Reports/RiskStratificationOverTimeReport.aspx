﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="RiskStratificationOverTimeReport.aspx.cs" Inherits="Heart360Admin.Pages.Reports.RiskStratificationOverTimeReport" %>

<%@ Register Src="~/Reports/Ctrls/Patient/RiskStratificationOverTimeReport.ascx"
    TagName="RiskStratificationOverTimeReport" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <uc1:RiskStratificationOverTimeReport ID="RiskStratificationOverTimeReport1" runat="server" />
    </div>
</asp:Content>
