﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Heart360Admin.Pages.Reports {
    
    
    public partial class ProviderReport {
        
        /// <summary>
        /// ucParameters control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.ProviderReport ucParameters;
        
        /// <summary>
        /// divReport control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divReport;
        
        /// <summary>
        /// ucProviders control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.Provider.Providers ucProviders;
        
        /// <summary>
        /// Div1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl Div1;
        
        /// <summary>
        /// ucNewProviderVisits control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.Provider.NewProviderVisits ucNewProviderVisits;
        
        /// <summary>
        /// ucAllProviderVisits control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.Provider.AllProviderVisits ucAllProviderVisits;
        
        /// <summary>
        /// ucNewProviderVisitsPercentage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.Provider.NewProviderVisitsPercentage ucNewProviderVisitsPercentage;
        
        /// <summary>
        /// ucAllProviderVisitsPercentage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.Provider.AllProviderVisitsPercentage ucAllProviderVisitsPercentage;
        
        /// <summary>
        /// ucPatientCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.Provider.PatientCount ucPatientCount;
        
        /// <summary>
        /// ucPatientProviderConnection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Heart360Admin.Reports.Ctrls.Provider.PatientProviderConnection ucPatientProviderConnection;
        
        /// <summary>
        /// hidFormLoaded control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidFormLoaded;
    }
}
