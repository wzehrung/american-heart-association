﻿<%@ Page MasterPageFile="~/MasterPages/Admin.Master" Language="C#" AutoEventWireup="true" CodeBehind="PatientHome.aspx.cs" Inherits="Heart360Admin.Pages.Reports.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox">
        <h1><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Patient_Home_Page_Title")%></h1>
        <table width="100%" class="admin-box" cellpadding="0" cellspacing="0">

            <tr>
                <td>
                    <strong><a id="Rpt5" href="CampaignReport.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Patient_Campaign_Page_Title")%></a></strong>
                </td>
            </tr>
            
            <tr>
                <td>
                    <strong><a id="Rpt1" href="PatientReport.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Patient_Page_Title")%></a></strong>
                </td>
            </tr>

            <tr>
                <td>
                    <strong><a id="Rpt2" href="PatientHealthReport.aspx" runat="server">Risk stratification report by gender</a></strong>
                </td>
            </tr>

            <tr>
                <td>
                    <strong><a id="Rpt3" href="RiskStratificationOverTimeReport.aspx" runat="server">Population risk stratification report</a></strong>
                </td>
            </tr>

            <tr>
                <td>
                    <strong><a id="Rpt4" href="http://txda1vh3dd1/ReportServer/?%2fAHA%2fAHA+Hypertension+Stratification&rs:Command=Render">Hypertension Stratification Report for All Heart360 Users</a></strong>
                </td>
            </tr>

            <tr>
                <td>
                    <strong><a id="Rpt5" href="http://txda1vh3dd1/ReportServer/?%2fAHA%2fAHA+Affiliate+Campaign+Hypertension+Stratification&rs:Command=Render">Hypertension Stratification Report by Patient/Participant Campaigns</a></strong>
                </td>
            </tr>

        </table>
    </div>
</asp:Content>
