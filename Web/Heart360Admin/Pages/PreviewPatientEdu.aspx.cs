﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GRCBase;

namespace Heart360Admin.Pages
{
    public partial class PreviewPatientEdu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/home.aspx"))
                {
                    Response.Redirect("~/Login.aspx?ReturnURL=" + Server.UrlEncode(HttpContext.Current.Request.RawUrl));
                    return;
                }
                Response.Redirect("~/Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request["HRID"]))
                {
                    AHAHelpContent.Content obj = AHAHelpContent.Content.FindByContentID(Convert.ToInt32(Request["HRID"]),Utilities.Utility.GetCurrentLanguageID());
                    lnk1.HRef = obj.URL;
                    lnk1.InnerText = obj.DidYouKnowLinkTitle;
                }
                else if (Utilities.SessionInfo.GetPatientHelpfulResourcePreviewFromSession() != null)
                {
                    Utilities.HelpfulResource obj = Utilities.SessionInfo.GetPatientHelpfulResourcePreviewFromSession();
                    if (!string.IsNullOrEmpty(obj.URL))
                        lnk1.HRef = obj.URL;
                    else
                        lnk1.HRef = "javascript:;";
                    if (!string.IsNullOrEmpty(obj.LinkTitle))
                        lnk1.InnerText = obj.LinkTitle;
                    else
                        lnk1.InnerText = "Resource 1";
                }
                else
                {
                    throw new Exception("Unauthorized view");
                }
            }
        }
    }
}
