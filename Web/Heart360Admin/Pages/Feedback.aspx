﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Heart360Admin.Pages.Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="copyBox report">

    <h1><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Feedback")%></h1>

    <p>

    </p> 

    <asp:UpdatePanel ID="upRecipients" runat="server">
        <ContentTemplate>
            
            <table class="admin-box" cellpadding="0" cellspacing="0" width="100%" border="1">
                <tr>
                    <td>
                        <h2>Manage Feedback Form Distribution</h2><br />
                        <p>Selecting the checkbox for a recipient is automatically saved.</p>
                        <asp:GridView ID="gvFeedbackRecipients" runat="server" DataKeyNames="AdminUserID" AllowPaging="true" PageSize="10" 
                            AutoGenerateColumns="false" CssClass="provider-grid" OnPageIndexChanging="gvFeedbackRecipients_PageIndexChanging" >
                            
                            <Columns>

                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAdminUserID" runat="server" Text='<%#Eval("AdminUserID")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <%#Eval("AdminName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Email Address">
                                    <ItemTemplate>
                                        <%#Eval("AdminEmailAddress")%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Feedback Recipient?" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <center><asp:CheckBox ID="chkRecipient" runat="server" Checked='<%#bool.Parse(Eval("Recipient").ToString()) %>' OnCheckedChanged="gvFeedbackRecipients_OnCheckedChanged" AutoPostBack="true" /></center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>

                        </asp:GridView>
                    </td>
                </tr>
            </table>
        
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="up1" UpdateMode="Always">
        <ContentTemplate>

            <table class="admin-box" cellpadding="0" cellspacing="0" width="100%" border="1">
                
                <tr>
                    <td colspan="2">
                        <h2>Manage Feedback Types</h2><br />
                        <div class="validation_summary">
                            <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                                runat="server" ID="validSummarycreate" ValidationGroup="Create" Enabled="true"
                                ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
                            <asp:ValidationSummary HeaderText="Please correct the following" runat="server" ID="validsummaryupdate"
                                ValidationGroup="Update" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                DisplayMode="BulletList" ForeColor="#CE2930" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 50%">         
                        <asp:TextBox ID="txtCreateName" Width="400" MaxLength="100" runat="server"></asp:TextBox>
                        <asp:CustomValidator
                           ValidationGroup="Create"
                           EnableClientScript="false"
                           ID="CustomValidator1"
                           runat="server"
                           Display="None" OnServerValidate="OnValidateName">
                        </asp:CustomValidator>
                    </td>
                    <td align="left">
                        <asp:LinkButton runat="server" ID="lnkCreate" OnClick="lnkCreate_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></asp:LinkButton>
                    </td>        
                </tr>

                <tr>
                    <td colspan="2">                        
                        
                        <p>Selecting the checkbox to enable/disable a type is automatically saved.</p>
          
                        <asp:GridView
                            ID="gvFeedbackTypes" 
                            runat="server"
                            AutoGenerateColumns="False" 
                            OnRowEditing="gv_RowEditing"          
                            OnRowCancelingEdit="gv_RowCancelingEdit" 
                            OnRowUpdating="gv_RowUpdating"
                            OnRowDeleting="gv_RowDeleting"
                            CssClass="provider-grid" AutoGenerateEditButton="true" AutoGenerateDeleteButton="true">

                        <EmptyDataTemplate>
                            No feedback types entered
                        </EmptyDataTemplate>

                            <Columns>

                                <asp:TemplateField HeaderText="ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFeedbackTypeID" Text='<%#Eval("FeedbackTypeID") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Enabled">
                                    <ItemTemplate>
                                        <center><asp:CheckBox ID="chkEnabled" runat="server" Checked='<%#bool.Parse(Eval("Enabled").ToString()) %>' OnCheckedChanged="gvFeedbackTypes_OnCheckedChanged" AutoPostBack="true" /></center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Feedback Type Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFeedbackTypeName" runat="server" Text='<%#Eval("FeedbackTypeName")%>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtFeedbackTypeName" runat="server" Width="400" Text='<%#Eval("FeedbackTypeName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                
                        </asp:GridView>

                    </td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>

</div>

</asp:Content>
