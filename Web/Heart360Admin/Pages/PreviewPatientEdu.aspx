﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewPatientEdu.aspx.cs"
    Inherits="Heart360Admin.Pages.PreviewPatientEdu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Css/Style.css" rel="stylesheet" type="text/css" />
    <!--[if IE 6]>
<link href="/Css/ie6fix.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
    <form id="form1" runat="server" class="patient-preview-bg">
    <div id="rightBox">
        <h3>
            Helpful Resources</h3>
        <ul class="links list-arrow">
            <li runat="server" id="li1"><a runat="server" id="lnk1" target="_blank"></a></li>
            <li class="fade" runat="server" id="li2"><a runat="server" id="lnk2" target="_blank">Resource 2</a></li>
            <li class="fade" runat="server" id="li3"><a runat="server" id="lnk3" target="_blank">Resource 3</a></li>
            <li class="fade" runat="server" id="li4"><a runat="server" id="lnk4" target="_blank">Resource 4</a></li>
            <li class="fade" runat="server" id="li5"><a runat="server" id="lnk5" target="_blank">Resource 5</a></li>
        </ul>
        <div class="fade">
        <h3>
        
            Tip of the Day</h3>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non tortor arcu.
        Curabitur tristique nisl sed diam vestibulum aliquet. Donec sed dolor tortor. Quisque
        tincidunt, metus sit amet posuere fringilla, risus nibh volutpat metus, id aliquam
        magna eros non lorem.
        </div>
    </div>
    </form>
</body>
</html>
