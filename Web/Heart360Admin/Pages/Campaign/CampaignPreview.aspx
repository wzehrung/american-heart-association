﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CampaignPreview.aspx.cs"
    Inherits="Heart360Admin.Pages.Campaign.CampaignPreview" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <title></title>
        <link href="/Css/Style.css" rel="stylesheet" type="text/css" />


        <script language="javascript" type="text/javascript">
            window.opener.document.forms[0].target = "";
        </script>

        <style type="text/css">

            .previewMain
            {
                background-image: url("/images/campaign/CampaignPreviewBackground.png");
                background-attachment: scroll;
                background-repeat: no-repeat;
                padding: 0px 0px 0px 10px;
                margin: 0px 0px 10px 0px;
                width: 1024px;
                height: 860px;         
            }  
            .previewTitle
            {
                display: block;
                position: absolute;
                top: 155px;
                font-size: 1.95em;
                font-weight: 500;
                line-height: 1.5em;
            }
            .previewLogo
            {
                display: block;
                position: absolute;
                top: 155px;
                left: 800px;
            }
            .previewImage
            {
                display: block;
                position: absolute;
                top: 300px;
                left: 10px;
            }
            .previewDesc
            {
                display: block;
                position: absolute;
                top: 670px;
                width: 580px;
                font-size: 1.25em;
                line-height: 1.5em;
                padding-left: 10px;
            }
            .previewTag
            {
                display: block;
                position: absolute;
                top: 790px;
                width: 580px;
                font-size: 1.25em;
                font-weight: bold;
                line-height: 1.5em;
                padding-left: 10px;
            }

        </style>

    </head>

    <body class="previewBackground">

        <form id="form1" runat="server">

            <div class="previewMain">

                <div class="previewTitle"><asp:Literal ID="litTitle" runat="server"></asp:Literal></div>                            

                <div class="previewLogo"><img src="~/Pages/Campaign/CampaignImage.aspx?PL=true" runat="server" alt="Campaign Logo" /></div>

                <div class="previewImage"><img src="~/Pages/Campaign/CampaignImage.aspx?PI=true" runat="server" width="1003" height="327" alt="Campaign Image" /></div>

                <div class="previewDesc"><asp:Literal ID="litDescription" runat="server"></asp:Literal></div>

                <div class="previewTag"><asp:Literal ID="litPromotionalTagLine" runat="server"></asp:Literal></div>

            </div>

        </form>

    </body>

</html>