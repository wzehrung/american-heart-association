﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="CreateCampaign.aspx.cs" Inherits="Heart360Admin.Pages.Campaign.CreateCampaign" %>

<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style type="text/css">
    
    .boxMain
    {
        background-color: #F0F0F0;
        border: 1px solid #E0E0E0;
        padding: 4px;
        margin: 10px 0px 10px 0px;
        width: 906px;
        height: 610px;         
    }  
    .boxSection
    {
        clear: both;
        border-bottom: 1px solid #E0E0E0;
        display: block;
        margin-top: 10px;
    }
    .boxSectionHeader
    {
        background-color: #E0E0E0;
        padding: 6px 0px 6px 6px;

    }
    .boxLeft
    {
        float: left;
        display: block;
        padding: 10px;
        
    }
    .boxRight
    {
        float: right;
        padding: 10px 10px;
    }

</style>

<div class="copyBox report">

    <div class="report-filter">

        <h1><asp:Literal runat="server" ID="litMode"></asp:Literal></h1>
        <p><asp:Literal ID="litText" runat="server"></asp:Literal></p>
        <p>
            The customized version of the visitor home page for a campaign will be visible to
            the user only if a partner logo is uploaded and the campaign is published. 
            Until then, the user will continue to see the default visitor home page.
        </p>

        <asp:Literal runat="server" ID="litError"></asp:Literal>

        <div class="validation_summary">
            <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                runat="server" ID="validSummarycreate" ValidationGroup="Create" Enabled="true"
                ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
            <asp:ValidationSummary HeaderText="Please correct the following" runat="server" ID="validsummaryupdate"
                ValidationGroup="Update" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                DisplayMode="BulletList" ForeColor="#CE2930" />
        </div>

        <div class="clearer"></div>

        <div class="red align-right"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%></div>

        <div style="float: right;">

            <table width="100%" border="0" cellspacing="0" cellpadding="4" style="background-color: #F0F0F0; border: 1px solid #E0E0E0; padding: 4px;">

                <tr>
                    <td><img src="/images/CampaignPageLayout.png" alt="Campaign Page Layout" /></td>
                </tr>

            </table>

        </div>

        <div style="float: left;">

            <!-- ## BEGIN Campaign Details ## -->
            <table width="100%" border="0" cellspacing="0" cellpadding="4" style="background-color: #F0F0F0; border: 1px solid #E0E0E0; padding: 4px; margin-bottom: 10px;">

                <tr>
                    <td style="background-color: #E0E0E0;" colspan="2"><h3>Campaign Details</h3></td>
                </tr>

                <tr>
                    <td style="vertical-align: top" class="align-right nowrap">
                        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Title")%><b style="color: Red;">*</b>
                    </td>
                    <td>
                        <%--<asp:TextBox runat="server" ID="txtTitle" onkeyup="return CreateURL();" Width="300"></asp:TextBox>--%>
                        <asp:TextBox runat="server" ID="txtTitle" Width="300"></asp:TextBox>
                        <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator1"
                            runat="server" Display="None" OnServerValidate="OnValidateTitle">
                        </asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator2"
                            runat="server" Display="None" OnServerValidate="OnValidateTitle">
                        </asp:CustomValidator>
                    </td>
                </tr>

                <tr>
                    <td class="align-right nowrap">
                        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "url")%><b style="color: Red;">*</b>
                    </td>
                    <td>
                        <b style="vertical-align: middle"><asp:Literal runat="server" ID="litURL"></asp:Literal></b>
                        <asp:TextBox runat="server" ID="txtURL" Width="280"></asp:TextBox>
                        <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator3" runat="server" Display="None" OnServerValidate="OnValidateURL"></asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator4" runat="server" Display="None" OnServerValidate="OnValidateURL"></asp:CustomValidator>
                    </td>
                </tr>

                <tr>
                    <td style="vertical-align: top" class="align-right nowrap">
                        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "startDate")%><b style="color: Red;">*</b>
                    </td>
                    <td>
                        <GRC:GRCDatePicker ID="dtStartDate" runat="server" CBReadOnly="true" />
                        <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator5"
                            runat="server" Display="None" OnServerValidate="OnValidateStartDate">
                        </asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator6"
                            runat="server" Display="None" OnServerValidate="OnValidateStartDate">
                        </asp:CustomValidator>
                    </td>
                </tr>

                <tr>
                    <td style="vertical-align: top" class="align-right nowrap">
                        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "enddate")%><b style="color: Red;">*</b>
                    </td>
                    <td>
                        <GRC:GRCDatePicker ID="dtEndDate" runat="server" CBReadOnly="true" />
                        <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator7"
                            runat="server" Display="None" OnServerValidate="OnValidateEndDate">
                        </asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator8"
                            runat="server" Display="None" OnServerValidate="OnValidateEndDate">
                        </asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator10"
                            runat="server" Display="None" OnServerValidate="OnValidateDate">
                        </asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator9"
                            runat="server" Display="None" OnServerValidate="OnValidateDate">
                        </asp:CustomValidator>
                    </td>
                </tr>

                <tr>
                    <td style="vertical-align: top" class="align-right nowrap">
                        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Description")%><b style="color: Red;">*</b>
                    </td>
                    <td>

                        <FCKeditorV2:FCKeditor ID="fckEditorDescription" ToolbarCanCollapse="false" Width="330"
                            Height="200" runat="server">
                        </FCKeditorV2:FCKeditor>
                        <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator11"
                            runat="server" Display="None" OnServerValidate="OnValidateDescription">
                        </asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator12"
                            runat="server" Display="None" OnServerValidate="OnValidateDescription">
                        </asp:CustomValidator>
                    </td>
                </tr>

                <tr>
                    <td style="vertical-align: top" class="align-right nowrap">
                        Promotional tagline
                    </td>
                    <td>
                        <asp:TextBox ID="txtPromotionalTagline" Width="300" runat="server"></asp:TextBox>
                        <br />
                        Example: Brought to you by...
                    </td>
                </tr>

            </table>
            <!-- ## END Campaign Details ## -->

        </div>

            <!-- ## BEGIN Affiliate and Business Assignment ## -->
            <table width="100%" border="0" cellspacing="0" cellpadding="4" style="background-color: #F0F0F0; border: 1px solid #E0E0E0; padding: 4px; margin-bottom: 10px;">

                <tr>
                    <td colspan="4" align="left" style="background-color: #E0E0E0;">
                        <strong>Affiliate and Business Assignment</strong>
                    </td>
                </tr>

                <tr>
                    <td style="vertical-align: top; text-align: right;" class="nowrap">
                        Business Campaign:
                    </td>
                    <td style="vertical-align: top; text-align: left;">
                        <asp:DropDownList ID="ddlBusiness" runat="server" AutoPostBack="false" />
                    </td>

                    <td style="vertical-align: top; text-align: right;" class="nowrap">
                        Affiliate:
                    </td>
                    <td align="left">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="padding-left: 3px;"><asp:CheckBox ID="cbAll" runat="server" Text="All" AutoPostBack="true" OnCheckedChanged="cbAll_OnCheckedChanged" /></div>
                                <asp:CheckBoxList
                                    ID="cblAffiliates" RepeatColumns="2" runat="server">
                                </asp:CheckBoxList>                                
                            </ContentTemplate>
                        
                        </asp:UpdatePanel>

                    </td>
                </tr>

            </table>
            <!-- ## END Affiliate and Business Assignment ## -->

            <!-- ## BEGIN Partner Images ## -->
            <div class="boxMain">

                <div class="boxSectionHeader"><strong>Images</strong></div>

                <!-- # BEGIN Partner Logo # -->
                <div class="boxSection">

                    <div class="boxLeft">
                        Partner Logo:<b style="color: Red;">*</b><br />
                        <asp:FileUpload ID="fileUploadPartnerLogo" runat="server" /><asp:Label ID="lblPartnerLogoName" runat="server"></asp:Label><br />
                        
                        <br />
                        You can upload 200w X 130h JPG, GIF or PNG files under 300kb.<br />
                        <a href="../../images/campaign/SamplePartnerLogo_200x130.png" target="_blank">Download sample partner logo</a>
                    </div>

                    <asp:UpdatePanel ID="upPartnerLogo" runat="server" UpdateMode="Conditional">
<%--
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="lnkPreviewLogo" />
                        </Triggers>
--%>
                        <ContentTemplate>

                            <div class="boxRight">
                                <asp:Literal ID="litPartnerLogo" runat="server"></asp:Literal>
                                <asp:Image id="imgNewLogo" runat="server" alt="Campaign Logo" visible="false" />
                                <%--<asp:LinkButton runat="server" ID="lnkPreviewLogo" OnClick="lnkPreviewLogo_OnClick"  CssClass="aha-small-button with-arrow"><strong><em>Preview Logo</em></strong></asp:LinkButton>--%>
                            </div>
                        
                            <div>
                                <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="cvCreateFileUploadPartnerLogo"
                                    Display="None" OnServerValidate="OnValidatePartnerLogo" runat="server"></asp:CustomValidator>
                                <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator14"
                                    Display="None" OnServerValidate="OnValidatePartnerLogo" runat="server"></asp:CustomValidator>
                            </div>                        
                        
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <!-- # END Partner Logo # -->

                <!-- # BEGIN Partner Image # -->
                <div class="boxSection">

                    <asp:UpdatePanel ID="upPartnerImage" runat="server">
                        <ContentTemplate>
                    
                            <div class="boxLeft">
                                Partner Image:<br />
                                <p>This image will display first in the slideshow, along with the default images selected below.</p>
                                <asp:FileUpload ID="fileUploadPartnerImage" runat="server" /><br />
                                <br />
                                You can upload 1,024w X 334h JPG, GIF or PNG files under 300kb.<br />
                                <a href="../../images/campaign/SamplePartnerImage_1024x334.png" target="_blank">Download sample partner image</a>
                            </div>

                            <div class="boxRight"><asp:CheckBox ID="cbPartnerImage" runat="server" Checked="false" Enabled="true" /><asp:Image ID="imgPartnerImage" ImageUrl="/images/campaign/NoImage.png" runat="server" /></div>

                            <div>
                                <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator13"
                                    Display="None" OnServerValidate="OnValidatePartnerImage" runat="server"></asp:CustomValidator>
                                <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator15"
                                    Display="None" OnServerValidate="OnValidatePartnerImage" runat="server"></asp:CustomValidator>                        
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <!-- # END Partner Image # -->

                <!-- # BEGIN Default Images # -->
                <div class="boxSection">
                    <div class="boxLeft">
                        Default Images:
                    </div>

                    <div class="boxRight">
                        <asp:CheckBox ID="cbSlide01" runat="server" Checked="true" /><asp:Image ID="imgSlide01" runat="server" ImageUrl="/images/campaign/slide-1.png" /><br />
                        <br />
                        <asp:CheckBox ID="cbSlide02" runat="server" Checked="true" /><asp:Image ID="imgSlide02" runat="server" ImageUrl="/images/campaign/slide-2.png" /><br />
                        <br />
                        <asp:CheckBox ID="cbSlide03" runat="server" Checked="true" /><asp:Image ID="imgSlide03" runat="server" ImageUrl="/images/campaign/slide-3.png" />
                    </div>

                </div>
                <!-- # END Default Images # -->

            </div>
            <!-- ## END Images ## -->

            <!-- ## BEGIN Campaign Resources ## -->
            <table width="100%" border="0" cellspacing="0" cellpadding="4" style="background-color: #F0F0F0; border: 1px solid #E0E0E0; padding: 4px;">

                <tr>
                    <td colspan="2" align="left" style="background-color: #E0E0E0;">
                        <strong>Campaign Resources</strong>
                    </td>
                </tr>

                <tr>
                    <td style="vertical-align: top" class="align-right nowrap">Title</td>
                    <td><asp:TextBox ID="txtRscTitle" Width="300" runat="server"></asp:TextBox></td>
                </tr>

                <tr>
                    <td style="vertical-align: top" class="align-right nowrap">URL</td>
                    <td>
                        <asp:TextBox ID="txtResourceURL" Width="300" runat="server" Text="http://"></asp:TextBox>
                        <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator16" runat="server" Display="None"></asp:CustomValidator>
                        <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator17" runat="server" Display="None"></asp:CustomValidator>
                        <asp:RegularExpressionValidator ID="regexResourceURLCreate" runat="server" ControlToValidate="txtResourceURL" ValidationGroup="Create" Visible="false" Enabled="false" ErrorMessage='<%$ Resources:Heart360AdminGlobalResources, Validator_Resource_URL %>' />
                        <asp:RegularExpressionValidator ID="regexResourceURLUpdate" runat="server" ControlToValidate="txtResourceURL" ValidationGroup="Update" Visible="false" Enabled="false" ErrorMessage='<%$ Resources:Heart360AdminGlobalResources, Validator_Resource_URL %>' />
                    </td>
                </tr>

            </table>
            <!-- ## END Campaign Resources ## -->

        <!-- ## BEGIN Buttons ## -->
        <table width="100%" border="0" cellspacing="0" cellpadding="4">

            <tr>
                <td style="text-align: right; vertical-align: bottom;">
                    <asp:LinkButton runat="server" ID="lnkCancel" OnClick="lnkCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Cancel</em></strong></asp:LinkButton>&nbsp;&nbsp;
                    <asp:LinkButton runat="server" ID="lnkPreview" OnClick="lnkPreview_OnClick" OnClientClick="javascript:previewImage();"
                        CssClass="aha-small-button with-arrow"><strong><em>Preview</em></strong></asp:LinkButton>&nbsp;&nbsp;
                    <asp:LinkButton runat="server" ID="lnkCreate" OnClick="lnkCreate_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Save</em></strong></asp:LinkButton>
                </td>
            </tr>

        </table>
        <!-- ## END Buttons ## -->

    </div>

</div>

<asp:HiddenField runat="server" ID="hidSiteURL" />

<script type="text/javascript">
    var iChars = "~`!@#$%^&*()+=|\\}{][\"\':;<,>.?/";
    function CreateURL() {
        var title = removeSpaces(document.getElementById('<%=txtTitle.ClientID %>').value);
        title = removeSpChars(title);
        document.getElementById('<%=txtURL.ClientID %>').value = title;
    }

    function removeSpaces(url) {
        return url.split(' ').join('');
    }
    function removeSpChars(url) {
        var str = url;
        for (var i = 0; i < str.length; i++) {
            if (iChars.indexOf(str.charAt(i)) != -1) {
                url = url.split(str.charAt(i)).join('');
            }
        }
        return url;
    }


    function previewImage() {
        window.open("", "preview_campaign", "width=1100,height=800,resizable=yes, scrollbars=yes");
        document.forms[0].target = 'preview_campaign';
        return true;
    }

    //    function save() {
    //        document.forms[0].target = '_self';
    //        document.forms[0].action = "/Pages/Campaign/CreateCampaign.aspx";
    //    } 
</script>

     <br />

</asp:Content>