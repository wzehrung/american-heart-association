﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Admin.Master" AutoEventWireup="true"
    CodeBehind="AllCampaigns.aspx.cs" Inherits="Heart360Admin.Pages.Campaign.AllCampaigns" %>

<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<%@ Register Src="~/UserCtrls/Campaign/ActiveCampaigns.ascx" TagName="ActiveCampaigns"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserCtrls/Campaign/ExpiredCampaigns.ascx" TagName="ExpiredCampaigns"
    TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="copyBox">

        <asp:UpdatePanel runat="server" ID="up1">
            <ContentTemplate>
            
                <h1><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Campaigns")%></h1>
                <p><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New_Campaign")%></p>

                <div class="validation_summary">
                    <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummarycreate"
                        ValidationGroup="Search" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                        DisplayMode="BulletList" ForeColor="#CE2930" />
                </div>

                <asp:LinkButton runat="server" ID="lnkNew" OnClick="lnkNew_OnClick" CssClass="aha-small-button with-arrow right"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></asp:LinkButton>

                <h1> <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Active_Campaigns")%></h1>
                <table class="admin-box" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <uc1:ActiveCampaigns ID="ActiveCampaigns1" runat="server" />
                        </td>
                    </tr>
                </table>
        
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

                <!-- Looks like some early attempts at a search box -->
                <div class="whitebox" id="DivSearchResult" style="display: none;">
                    <div class="shadowtop">
                        <div class="tl">
                        </div>
                        <div class="tm">
                        </div>
                        <div class="tr">
                        </div>
                    </div>

                    <table class="tablecontainer" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="left-shadow">
                            </td>
                            <td class="boxcontent center">
                                <div>
                                    <asp:GridView BorderWidth="0" ShowHeader="true" EnableViewState="true" ID="gvCampaigns"
                                        runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, EmtyCampaignText %>"
                                        ShowFooter="False" OnRowDataBound="gvCampaigns_RowDataBound" GridLines="Horizontal"
                                        CssClass="provider-grid borderfill" PagerStyle-CssClass="table-page-index">
                                        <Columns>
                                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Title %>" ItemStyle-Width="250" HeaderStyle-CssClass="alignleft">
                                                <ItemTemplate>
                                                    <asp:Literal runat="server" ID="litTitle"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Description %>" ItemStyle-Width="100"
                                                HeaderStyle-CssClass="alignleft">
                                                <ItemTemplate>
                                                    <asp:Literal runat="server" ID="litDescription"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, url %>" ItemStyle-Width="100" HeaderStyle-CssClass="alignleft">
                                                <ItemTemplate>
                                                    <asp:Literal runat="server" ID="litURL"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, startDate %>" ItemStyle-Width="100" HeaderStyle-CssClass="alignleft">
                                                <ItemTemplate>
                                                    <asp:Literal runat="server" ID="litStartDate"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, enddate %>"  ItemStyle-Width="100" HeaderStyle-CssClass="alignleft">
                                                <ItemTemplate>
                                                    <asp:Literal runat="server" ID="litEndDate"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                    </asp:GridView>
                                    <asp:LinkButton runat="server" ID="lnkSearchClose" CssClass="aha-small-button with-arrow"><strong><em>Close</em></strong></asp:LinkButton>
                                </div>
                            </td>
                            <td class="right-shadow">
                            </td>
                        </tr>
                    </table>
                
                    <div class="shadowbottom">
                        <div class="bl">
                        </div>
                        <div class="bm">
                        </div>
                        <div class="br">
                        </div>
                    </div>
                </div>

</asp:Content>