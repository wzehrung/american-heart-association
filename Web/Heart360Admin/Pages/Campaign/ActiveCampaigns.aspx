﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Admin.Master" CodeBehind="ActiveCampaigns.aspx.cs" Inherits="Heart360Admin.Pages.Campaign.ActiveCampaigns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="copyBox" style="margin-top: 0px; padding-top: 0px;">

        <asp:UpdatePanel ID="upActiveCampaigns" runat="server">
            <ContentTemplate>

                <table>
                    <tr>
                        <td>
                            <table class="campaign-list">
                                <tr>
                                    <td colspan="6" style="border-bottom: 1px solid grey;">
                                        <h1 style="margin-top: 0px;"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Campaigns_Active")%></h1>
                                        <p><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New_Campaign_Active")%></p>
                                        <asp:LinkButton ID="lnkNew" runat="server" CssClass="aha-small-button with-arrow right" PostBackUrl="CreateCampaign.aspx"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr style="background-color: #efefef;">
                                    <td align="left">Filters:</td>
                                    <td style="white-space: nowrap">Affiliate:
                                        <asp:DropDownList ID="ddlAffiliate" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAffiliate_OnSelectedIndexChanged" Width="200">
                                            <asp:ListItem Selected="True" Value="All">All</asp:ListItem>
                                            <asp:ListItem Value="Founders">Founders</asp:ListItem>
                                            <asp:ListItem Value="Great Rivers">Great Rivers</asp:ListItem>
                                            <asp:ListItem Value="Greater Southeast">Greater Southeast</asp:ListItem>
                                            <asp:ListItem Value="Mid-Atlantic">Mid-Atlantic</asp:ListItem>
                                            <asp:ListItem Value="Midwest">Midwest</asp:ListItem>
                                            <asp:ListItem Value="SouthWest">SouthWest</asp:ListItem>
                                            <asp:ListItem Value="Western States">Western States</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="white-space: nowrap;">Campaign URL:
                                        <asp:TextBox ID="txtCampaignURL" runat="server"></asp:TextBox>
                                        &nbsp; </td>
                                    <td style="text-align: left; white-space: nowrap;">
                                        <asp:LinkButton ID="lnkFilter" runat="server" CssClass="aha-small-button with-arrow right" OnClick="lnkFilter_Click"><strong><em>Find by Campaign URL</em></strong></asp:LinkButton>
                                    </td>
                                    <td style="text-align: left; white-space: nowrap;">
                                        <asp:LinkButton ID="lnkClear" runat="server" CssClass="aha-small-button with-arrow right" OnClick="lnkClear_Click"><strong><em>Clear</em></strong></asp:LinkButton>
                                    </td>
                                    <td style="width: 100%; background-color: white; height: 32px;">
                                        <asp:UpdateProgress ID="uprgCampaigns" runat="server">
                                            <ProgressTemplate>
                                                <img src="../../images/ajax-loader.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td>

                            <asp:Label ID="lblValidationMessage" Visible="false" ForeColor="Red" runat="server"></asp:Label>
                            <asp:GridView 
                                ShowHeader="true" 
                                EnableViewState="true" 
                                ID="gvActiveCampaigns" 
                                runat="server"
                                AutoGenerateColumns="False" 
                                CellPadding="0" 
                                CellSpacing="0" 
                                EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, There_are_no_active_campaigns %>"
                                AllowPaging="True" 
                                PageSize="20" 
                                ShowFooter="False" 
                                AllowSorting="true" 
                                OnRowDataBound="gvActiveCampaigns_RowDataBound" 
                                OnRowCommand="gvActiveCampaigns_RowCommand"
                                OnSorting="gridView_Sorting"
                                OnPageIndexChanging="gvActiveCampaigns_PageIndexChanging" 
                                GridLines="Horizontal"
                                CssClass="provider-grid" 
                                PagerStyle-CssClass="table-page-index"
                                DataKeyNames="CampaignID">

                                <Columns>

                                    <asp:BoundField DataField="CampaignID" HeaderText="ID" SortExpression="CampaignID" />

                                    <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />

                                    <asp:TemplateField SortExpression="Description" HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Literal ID="litDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField SortExpression="CampaignUrl" HeaderText="Url">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidCampaignUrl" runat="server" Value='<%# Eval("CampaignUrl") %>' />
                                            <asp:HyperLink ID="lnkCampaignUrl" runat="server"></asp:HyperLink>                                       
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField SortExpression="StartDate" HeaderText="Start Date">
                                        <ItemTemplate>
                                            <asp:Literal ID="litStartDate" runat="server" Text='<%# Eval("StartDate") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField SortExpression="EndDate" HeaderText="End Date">
                                        <ItemTemplate>
                                            <asp:Literal ID="litEndDate" runat="server" Text='<%# Eval("EndDate") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="BusinessName" HeaderText="Business" SortExpression="BusinessName" />
        
                                    <asp:BoundField DataField="AffiliateName" HeaderText="Affiliate" SortExpression="AffiliateName" />

<%--                                    <asp:BoundField DataField="Participants" HeaderText="Participants" SortExpression="Participants" />--%>

                                    <asp:TemplateField>
                                        <HeaderTemplate>Actions</HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="padding-bottom: 10px;"><asp:LinkButton ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%#Eval("CampaignID") %>' /></div>
                                            <asp:LinkButton ID="btnUnPublish" runat="server" Text="UnPublish" CommandName="UnPublish" CommandArgument='<%#Eval("CampaignID") %>' />
                                            <asp:LinkButton ID="btnPublish" runat="server" Text="Publish" CommandName="Publish" CommandArgument='<%#Eval("CampaignID") %>' />
                                            <asp:HiddenField ID="hidIsPublished" runat="server" Value='<%#Eval("IsPublished") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <PagerStyle HorizontalAlign="Right" />
                            </asp:GridView>

                        </td>
                    </tr>
                </table>   

            </ContentTemplate>


        </asp:UpdatePanel>
                
    </div>

</asp:Content>
