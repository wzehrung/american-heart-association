﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Heart360Admin.Pages.Campaign
{
    public partial class AllCampaigns : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //dtStartDate.Text = DateTime.Now.ToShortDateString();
                //dtEndDate.Text = DateTime.Now.ToShortDateString();
                //lnkSearchClose.Attributes.Add("onclick", "return HideGenericConfirmationDiv('DivSearchResult');");
            }
        }

        protected void lnkNew_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Campaign/CreateCampaign.aspx");
        }

        protected void lnkSearch_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Search");
            if (!Page.IsValid)
                return;

            _DoSearch();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "JS_SearchResult", "ShowGenericConfirmationDiv('DivSearchResult',700,500);", true);
        }

        protected void gvCampaigns_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            AHAHelpContent.Campaign objCampaign = e.Row.DataItem as AHAHelpContent.Campaign;

            Literal litTitle = e.Row.FindControl("litTitle") as Literal;
            Literal litDescription = e.Row.FindControl("litDescription") as Literal;
            Literal litURL = e.Row.FindControl("litURL") as Literal;
            Literal litStartDate = e.Row.FindControl("litStartDate") as Literal;
            Literal litEndDate = e.Row.FindControl("litEndDate") as Literal;

            litTitle.Text = objCampaign.Title;
            litDescription.Text = objCampaign.Description;
            litURL.Text = objCampaign.CampaignURL;
            litStartDate.Text = objCampaign.StartDate.ToShortDateString();
            litEndDate.Text = objCampaign.EndDate.ToShortDateString();
        }

        private void _DoSearch()
        {
            //gvCampaigns.DataSource = AHAHelpContent.Campaign.SearchCampaigns(txtSearch.Text.Trim(), dtStartDate.Date.Value, dtEndDate.Date.Value);
            //gvCampaigns.DataBind();
        }

        protected void OnValidateSearch(object sender, ServerValidateEventArgs v)
        {
            //CustomValidator custValidator = sender as CustomValidator;
            //if (txtSearch.Text.Length == 0)
            //{
            //    if (dtStartDate.Date.Value >= dtEndDate.Date.Value)
            //    {
            //        v.IsValid = false;
            //        custValidator.ErrorMessage = "Enter a search string";
            //        return;
            //    }
            //}
            return;
        }
    }
}
