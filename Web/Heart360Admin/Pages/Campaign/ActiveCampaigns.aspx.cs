﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;

using System.Reflection;
using System.ComponentModel;

namespace Heart360Admin.Pages.Campaign
{
    public partial class ActiveCampaigns : System.Web.UI.Page
    {
        public string sAffiliate
        {          
            get
            {
                return ddlAffiliate.SelectedValue.ToString();
            }
        }

        public string sCampaignURL
        {
            get
            {
                string strURL = txtCampaignURL.Text.ToString();
                if (strURL == null || strURL == "")
                {
                    strURL = "%";
                }
                return strURL;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {        
            if (!IsPostBack)
            {
                ViewState["sortOrder"] = "";
                bindDataToGridView("", "", sAffiliate);
            }

            lblValidationMessage.Visible = false;
        }

        protected void gvActiveCampaigns_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            foreach (GridViewRow row in gvActiveCampaigns.Rows)
            {
                // Get the campaign ID from the first cell in the row
                int iCampaignID = Convert.ToInt32(row.Cells[0].Text);

                Literal litDescription = (Literal)(row.FindControl("litDescription"));
                HiddenField hidCampaignUrl = (HiddenField)(row.FindControl("hidCampaignUrl"));
                HyperLink lnkCampaignUrl = (HyperLink)(row.FindControl("lnkCampaignUrl"));

                Literal litStartDate = (Literal)(row.FindControl("litStartDate"));
                Literal litEndDate = (Literal)(row.FindControl("litEndDate"));

                LinkButton btnUnPublish = (LinkButton)(row.FindControl("btnUnPublish"));
                LinkButton btnPublish = (LinkButton)(row.FindControl("btnPublish"));

                HiddenField hidIsPublished = (HiddenField)(row.FindControl("hidIsPublished"));

                bool bIsPublished = Convert.ToBoolean(hidIsPublished.Value); // Convert.ToInt32(row.Cells[9].Text);

                // Display the publish/unpublish button
                if (bIsPublished == true)
                {
                    btnUnPublish.Visible = true;
                    btnPublish.Visible = false;
                }
                else
                {
                    btnPublish.Visible = true;
                    btnUnPublish.Visible = false;
                }

                string sDescription = litDescription.Text;
                // Remove any HTML tags
                sDescription = GRCBase.HtmlScrubber.Clean(sDescription, false, true);

                // Limit the length of the description
                int iMaxLength = 64;
                if (sDescription.Length > 64)
                {
                    sDescription = sDescription.Substring(0, iMaxLength) + "...";
                }

                litDescription.Text = sDescription;

                lnkCampaignUrl.Text = ".../" + hidCampaignUrl.Value;
                lnkCampaignUrl.NavigateUrl = AHACommon.AHAAppSettings.SiteUrl + "/" + hidCampaignUrl.Value;

                // Format the Start and End dates
                DateTime dtStartDate = Convert.ToDateTime(litStartDate.Text);
                litStartDate.Text = dtStartDate.ToShortDateString();

                DateTime dtEndDate = Convert.ToDateTime(litEndDate.Text);
                litEndDate.Text = dtEndDate.ToShortDateString();
            }
        }

        protected void gvActiveCampaigns_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvActiveCampaigns.PageIndex = e.NewPageIndex;
            bindDataToGridView("", "", sAffiliate);
        }

        private void bindDataToGridView(string sortExp, string sortDir, string sAffiliate)
        {

            List<AHAHelpContent.Campaign> listCampaign = AHAHelpContent.Campaign.FindAllActiveCampaigns(Utilities.Utility.GetCurrentLanguageID(), sAffiliate, sCampaignURL);

            DataTable table = ConvertToDataTable(listCampaign);

            DataView myDataView = new DataView();
            myDataView = table.DefaultView;

            if (sortExp != string.Empty)
            {
                myDataView.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvActiveCampaigns.DataSource = myDataView;
            gvActiveCampaigns.DataBind();
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            bindDataToGridView(e.SortExpression, sortOrder, sAffiliate);
        }

        protected void gvActiveCampaigns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string sCampaignID = e.CommandArgument.ToString();

            if (e.CommandName == "Edit")
            {
                Response.Redirect("/pages/campaign/CreateCampaign.aspx?CID=" + sCampaignID);
            }
            else if (e.CommandName == "UnPublish")
            {
                AHAHelpContent.Campaign.UnPublishCampaign(Convert.ToInt32(sCampaignID), 1);
            }
            else if (e.CommandName == "Publish")
            {
                AHAHelpContent.Campaign.PublishCampaign(Convert.ToInt32(sCampaignID), 1);
            }

            bindDataToGridView("", "", sAffiliate);
        }

        public void ddlAffiliate_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            bindDataToGridView("", "", sAffiliate);
        }

        protected void lnkFilter_Click(object sender, EventArgs e)
        {
            bindDataToGridView("", "", sAffiliate);
        }

        protected void lnkClear_Click(object sender, EventArgs e)
        {
            txtCampaignURL.Text = string.Empty;
            bindDataToGridView("", "", sAffiliate);
        }
    }
}