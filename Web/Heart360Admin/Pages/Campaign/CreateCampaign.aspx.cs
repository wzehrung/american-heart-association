﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;

namespace Heart360Admin.Pages.Campaign
{
    
    public partial class CreateCampaign : System.Web.UI.Page
    {
        #region Properties

        public static int PartnerLogoHeight
        {
            get
            {
                return Convert.ToInt32(GRCBase.ConfigReader.GetValue("PartnerLogoHeight"));
            }
        }

        public static int PartnerLogoWidth
        {
            get
            {
                return Convert.ToInt32(GRCBase.ConfigReader.GetValue("PartnerLogoWidth"));
            }
        }

        public static int PartnerImageHeight
        {
            get
            {
                return Convert.ToInt32(GRCBase.ConfigReader.GetValue("PartnerImageHeight"));
            }
        }

        public static int PartnerImageWidth
        {
            get
            {
                return Convert.ToInt32(GRCBase.ConfigReader.GetValue("PartnerImageWidth"));
            }
        }

        public static int CampaignImagesThumbHeight
        {
            get
            {
                return Convert.ToInt32(GRCBase.ConfigReader.GetValue("CampaignImagesThumbHeight"));
            }
        }

        private bool IsEditMode
        {
            get
            {
                return (Request["CID"] != null && Request["CID"].Length > 0) ? true : false;
            }
        }

        private string CampaignID
        {
            get
            {
                return Request["CID"];
            }
        }

        private void _SetDateFormat()
        {
            if (System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower() == "es-mx")
            {
                dtStartDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.DDMMYYYY;
                dtEndDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.DDMMYYYY;
            }
            else
            {
                dtStartDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.MMDDYYYY;
                dtEndDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.MMDDYYYY;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _SetDateFormat();

            if (!Page.IsPostBack)
            {
                hidSiteURL.Value = AHACommon.AHAAppSettings.SiteUrl;
                litURL.Text = AHACommon.AHAAppSettings.SiteUrl + "/";

                LoadBusinesses();
                
                if (IsEditMode)
                {
                    _LoadCampaign();
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_Campaign").ToString();
                    lnkCreate.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateCampGuid_Text").ToString();
                }
                else
                {
                    dtStartDate.Text = DateTime.Now.ToShortDateString();
                    dtEndDate.Text = DateTime.Now.ToShortDateString();
                    dtStartDate.Date = DateTime.Now;
                    dtEndDate.Date = DateTime.Now;
                    litMode.Text = "Create New Campaign";
                    lnkCreate.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkSaveBtn_Text").ToString();
                    litText.Text = "Use the form below to create a campaign";
                    txtTitle.Attributes.Add("onkeyup", "return CreateURL();");
                    LoadAffiliates();

                    imgPartnerImage.ImageUrl = "/images/campaign/NoImage.png";
                }

                // Get the Heart360 landing page slideshow images
                // 
                imgSlide01.Height = CampaignImagesThumbHeight;
                imgSlide02.Height = CampaignImagesThumbHeight;
                imgSlide03.Height = CampaignImagesThumbHeight;
            }
        }

        private void LoadAffiliates()
        {
            DataSet ds = AHAHelpContent.PatientReport.GetAffiliatesForCampaignsReport(null);

            cblAffiliates.DataSource = ds;
            cblAffiliates.DataTextField = "AffiliateName";
            cblAffiliates.DataValueField = "AffiliateID";
            cblAffiliates.DataBind();
            //if (cblAffiliates.Items.Count > 1)
            //    cblAffiliates.Items.Insert(0, new ListItem("All", "0"));
        }

        private void LoadBusinesses()
        {
            List<AHAHelpContent.Business> list = AHAHelpContent.Business.FindAllBusinesses();

            AHAHelpContent.Business nobiz = new AHAHelpContent.Business() ;
            nobiz.BusinessID = 0 ;
            nobiz.Name = "N/A" ;

            list.Insert(0, nobiz);

            ddlBusiness.DataSource = list;
            ddlBusiness.DataTextField = "Name";
            ddlBusiness.DataValueField = "BusinessID";
            ddlBusiness.DataBind();           
        }

        protected void lnkCreate_OnClick(object sender, EventArgs e)
        {
            if (IsEditMode)
            {
                Page.Validate("Update");
            }
            else
            {
                Page.Validate("Create");
            }

            if (!Page.IsValid)
                return;

            if (IsEditMode)
            {
                UpdateCampaign();
            }
            else
            {
                CreateNewCampaign();
            }
            Response.Redirect("~/Pages/Campaign/ActiveCampaigns.aspx", true);
        }

        private void _LoadCampaign()
        {
            AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.GetCampaignByCampaignID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());

            if (objCampaign != null)
            {
                string sImages = objCampaign.CampaignImages;

                txtTitle.Text = objCampaign.Title;
                fckEditorDescription.Value = objCampaign.Description;
                txtURL.Text = objCampaign.CampaignURL;
                txtURL.Enabled = false;
                dtStartDate.Text = objCampaign.StartDate.ToShortDateString();
                dtEndDate.Text = objCampaign.EndDate.ToShortDateString();
                dtStartDate.Date = objCampaign.StartDate;
                dtEndDate.Date = objCampaign.EndDate;
                txtPromotionalTagline.Text = objCampaign.PromotionalTagLine;
                txtRscTitle.Text = objCampaign.ResourceTitle;
                txtResourceURL.Text = objCampaign.ResourceURL;

                if (objCampaign.BusinessID != null)
                {
                    ddlBusiness.SelectedValue = objCampaign.BusinessID.ToString();
                }

                // Get the campaign/partner logo/image
                AHAHelpContent.CampaignPartnerLogo objPartnerLogo = AHAHelpContent.Campaign.GetCampaignPartnerLogobyID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());
                AHAHelpContent.CampaignPartnerImage objPartnerImage = AHAHelpContent.Campaign.GetCampaignPartnerImageByID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());

                if (objPartnerLogo.PartnerLogo != null)
                {
                    string strImgTag = "<img src='/Pages/Campaign/CampaignImage.aspx?CID={0}&LID={1}&Type={2}' />";
                    litPartnerLogo.Text = string.Format(strImgTag, objCampaign.CampaignID, objCampaign.LanguageID, "partner-logo"); 
                }

                if (objPartnerImage.PartnerImage != null)
                {
                    string strImgTag = "/Pages/Campaign/CampaignImage.aspx?CID={0}&LID={1}&Type={2}";
                    imgPartnerImage.ImageUrl = string.Format(strImgTag, objCampaign.CampaignID, objCampaign.LanguageID, "partner-image");
                    imgPartnerImage.Height = CampaignImagesThumbHeight;

                    if (sImages.Contains("0"))
                    {
                        cbPartnerImage.Enabled = true;
                        cbPartnerImage.Checked = true;
                    }
                }

                // Get a list of affiliates and any that are assigned to given campaign
                List<AHAHelpContent.Campaign> list = AHAHelpContent.Campaign.GetAffiliatesByCampaignID(Convert.ToInt32(CampaignID));

                DataSet ds = AHAHelpContent.PatientReport.GetAffiliatesForCampaignsReport(null);

                cblAffiliates.DataSource = ds;
                cblAffiliates.DataTextField = "AffiliateName";
                cblAffiliates.DataValueField = "AffiliateID";
                cblAffiliates.DataBind();

                // loop over each affiliate of selected campaign
                for (int li = 0; li < list.Count; li++)
                {
                    // Find which checkbox that is and flag it
                    for (int cbi = 0; cbi < cblAffiliates.Items.Count; cbi++)
                    {
                        if (list[li].AffiliateID == Convert.ToInt32(cblAffiliates.Items[cbi].Value))
                        {
                            cblAffiliates.Items[cbi].Selected = true;
                            break;
                        }
                    }
                }

                // Get what default images are selected

                if (sImages.Contains("1"))
                {
                    cbSlide01.Checked = true;
                }
                else
                {
                    cbSlide01.Checked = false;
                }

                if (sImages.Contains("2"))
                {
                    cbSlide02.Checked = true;
                }
                else
                {
                    cbSlide02.Checked = false;
                }

                if (sImages.Contains("3"))
                {
                    cbSlide03.Checked = true;
                }
                else
                {
                    cbSlide03.Checked = false;
                }

                //if (cbPartnerImage.Checked)
                //{
                //    sImages += 0 + "|";
                //}

                //if (cbSlide01.Checked)
                //{
                //    sImages += 1 + "|";
                //}

                //if (cbSlide02.Checked)
                //{
                //    sImages += 2 + "|";
                //}

                //if (cbSlide03.Checked)
                //{
                //    sImages += 3;
                //}
            }
        }

        private void CreateNewCampaign()
        {
            //AHAHelpContent.Campaign.CreateCampaign(txtTitle.Text.Trim(), txtDescription.Text.Trim(), null, txtURL.Text.Trim(), dtStartDate.Date.Value, dtEndDate.Date.Value, Utilities.Utility.GetCurrentLanguageID(), txtPromotionalTagline.Text.Trim()
            //    , _GetScaledImage(fileUploadPartnerLogo.PostedFile.InputStream, 130, 200)
            //    , fileUploadPartnerLogo.PostedFile.ContentType
            //    , _GetScaledImage(fileUploadPartnerImage.PostedFile.InputStream, 320, 570)
            //    , fileUploadPartnerImage.PostedFile.ContentType);

            string strPartnerLogoContentType = null;
            string strPartnerImageContentType = null;
            byte[] partnerLogo = null;
            byte[] partnerImage = null;

            if (fileUploadPartnerLogo.PostedFile != null && fileUploadPartnerLogo.PostedFile.ContentLength > 0)
            {
                strPartnerLogoContentType = fileUploadPartnerLogo.PostedFile.ContentType;
                partnerLogo = _GetScaledImage(fileUploadPartnerLogo.PostedFile.InputStream, 130, 200);
            }
            if (fileUploadPartnerImage.PostedFile != null && fileUploadPartnerImage.PostedFile.ContentLength > 0)
            {
                strPartnerImageContentType = fileUploadPartnerImage.PostedFile.ContentType;
                partnerImage = _GetScaledImage(fileUploadPartnerImage.PostedFile.InputStream, 320, 570);
            }
            //AHAHelpContent.Campaign.CreateCampaign(txtTitle.Text.Trim(), fckEditorDescription.Value.Trim(), null, txtURL.Text.Trim(), dtStartDate.Date.Value, dtEndDate.Date.Value, Utilities.Utility.GetCurrentLanguageID(), txtPromotionalTagline.Text.Trim()
            //    , _GetScaledImage(fileUploadPartnerLogo.PostedFile.InputStream, 130, 200)
            //    , fileUploadPartnerLogo.PostedFile.ContentType
            //    , _GetScaledImage(fileUploadPartnerImage.PostedFile.InputStream, 320, 570)
            //    , fileUploadPartnerImage.PostedFile.ContentType);

            string sAffiliateIDs = string.Empty;

            foreach (ListItem li in cblAffiliates.Items)
            {
                if (li.Selected)
                    sAffiliateIDs += li.Value + "|";
            }

            string sImages = string.Empty;

            if (cbPartnerImage.Checked)
            {
                sImages += 0 + "|";
            }

            if (cbSlide01.Checked)
            {
                sImages += 1 + "|";
            }

            if (cbSlide02.Checked)
            {
                sImages += 2 + "|";
            }

            if (cbSlide03.Checked)
            {
                sImages += 3;
            }

            AHAHelpContent.Campaign.CreateCampaign(
                txtTitle.Text.Trim(),
                fckEditorDescription.Value.Trim(),
                null,
                txtURL.Text.Trim(),
                dtStartDate.Date.Value,
                dtEndDate.Date.Value,
                Utilities.Utility.GetCurrentLanguageID(),
                txtPromotionalTagline.Text.Trim(),
                Convert.ToInt32(ddlBusiness.SelectedValue),
                partnerLogo,
                strPartnerLogoContentType,
                partnerImage,
                strPartnerImageContentType,
                txtRscTitle.Text,
                txtResourceURL.Text,
                sAffiliateIDs,
                sImages);
        }

        private void UpdateCampaign()
        {
            // Update logo
            byte[] partnerLogoBytes = null;
            string partnerLogoContentType = null;
            if (fileUploadPartnerLogo.PostedFile != null && fileUploadPartnerLogo.PostedFile.ContentLength > 0)
            {
                partnerLogoBytes = _GetScaledImage(fileUploadPartnerLogo.PostedFile.InputStream, PartnerLogoHeight, PartnerLogoWidth);
                partnerLogoContentType = fileUploadPartnerLogo.PostedFile.ContentType;
            }
            else
            {
                AHAHelpContent.CampaignPartnerLogo objCampaignPartnerLogo = AHAHelpContent.Campaign.GetCampaignPartnerLogobyID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());
                if ((objCampaignPartnerLogo != null) && (objCampaignPartnerLogo.PartnerLogo != null))
                {
                    partnerLogoBytes = objCampaignPartnerLogo.PartnerLogo;
                    partnerLogoContentType = objCampaignPartnerLogo.PartnerLogoContentType;
                }
            }

            // Update image
            byte[] partnerImageBytes = null;
            string partnerImageContentType = null;
            
            if (fileUploadPartnerImage.PostedFile != null && fileUploadPartnerImage.PostedFile.ContentLength > 0)
            {
                partnerImageBytes = _GetScaledImage(fileUploadPartnerImage.PostedFile.InputStream, PartnerImageHeight, PartnerImageWidth);
                partnerImageContentType = fileUploadPartnerImage.PostedFile.ContentType;
            }
            else
            {
                AHAHelpContent.CampaignPartnerImage objCampaignPartnerImage = AHAHelpContent.Campaign.GetCampaignPartnerImageByID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());
                if ((objCampaignPartnerImage != null) && (objCampaignPartnerImage.PartnerImage != null))
                {
                    partnerImageBytes = objCampaignPartnerImage.PartnerImage;
                    partnerImageContentType = objCampaignPartnerImage.PartnerImageContentType;
                }
            }

            // Update affiliates
            string sAffiliateIDs = string.Empty;

            foreach (ListItem li in cblAffiliates.Items)
            {
                if (li.Selected)
                    sAffiliateIDs += li.Value + "|";
            }

            string sImages = string.Empty;

            if (cbPartnerImage.Checked)
            {
                sImages += 0 + "|";
            }

            if (cbSlide01.Checked)
            {
                sImages += 1 + "|";
            }

            if (cbSlide02.Checked)
            {
                sImages += 2 + "|";
            }

            if (cbSlide03.Checked)
            {
                sImages += 3;
            }

            AHAHelpContent.Campaign.UpdateCampaign(
                Convert.ToInt32(CampaignID),
                txtTitle.Text.Trim(),
                fckEditorDescription.Value.Trim(),
                dtStartDate.Date.Value,
                dtEndDate.Date.Value,
                Utilities.Utility.GetCurrentLanguageID(),
                txtPromotionalTagline.Text.Trim(),
                Convert.ToInt32(ddlBusiness.SelectedValue),
                partnerLogoBytes,
                partnerLogoContentType,
                partnerImageBytes,
                partnerImageContentType,
                txtRscTitle.Text,
                txtResourceURL.Text,
                sAffiliateIDs,
                sImages);
        }

        protected void lnkPreview_OnClick(object sender, EventArgs e)
        {
            AHAHelpContent.Campaign objCampaign = new AHAHelpContent.Campaign();
            string[] strImageExtArray = { ".jpeg", ".jpg", ".gif", ".png" };

            if (IsEditMode)
            {
                AHAHelpContent.Campaign objCampaignFromDB = AHAHelpContent.Campaign.FindCampaignByCampaignID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());
                AHAHelpContent.CampaignPartnerLogo objCampaignPartnerLogo = AHAHelpContent.Campaign.GetCampaignPartnerLogobyID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());
                //objCampaign.Title = string.IsNullOrEmpty(txtTitle.Text.Trim())?objCampaignFromDB.Title:txtTitle.Text.Trim();
                //objCampaign.Description = string.IsNullOrEmpty(txtDescription.Text.Trim()) ? objCampaignFromDB.Description : txtDescription.Text.Trim();
                //objCampaign.PromotionalTagLine = string.IsNullOrEmpty(txtPromotionalTagline.Text.Trim()) ? objCampaignFromDB.PromotionalTagLine : txtPromotionalTagline.Text.Trim();
                objCampaign.Title = txtTitle.Text.Trim();
                //objCampaign.Description = txtDescription.Text.Trim();
                objCampaign.Description = fckEditorDescription.Value.Trim();
                objCampaign.PromotionalTagLine = txtPromotionalTagline.Text.Trim();
                if ((fileUploadPartnerLogo.PostedFile == null || fileUploadPartnerLogo.PostedFile.ContentLength == 0) && (objCampaignPartnerLogo != null))
                {
                    objCampaign.PartnerLogo = objCampaignPartnerLogo.PartnerLogo;
                    objCampaign.PartnerLogoContentType = objCampaignPartnerLogo.PartnerLogoContentType;
                }
                else
                {
                    if (strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUploadPartnerLogo.PostedFile.FileName).ToLower())))
                    {
                        objCampaign.PartnerLogo = _GetScaledImage(fileUploadPartnerLogo.PostedFile.InputStream, 130, 200);
                        objCampaign.PartnerLogoContentType = fileUploadPartnerLogo.PostedFile.ContentType;
                    }
                }
                AHAHelpContent.CampaignPartnerImage objCampaignPartnerImage = AHAHelpContent.Campaign.GetCampaignPartnerImageByID(Convert.ToInt32(CampaignID), Utilities.Utility.GetCurrentLanguageID());
                if ((fileUploadPartnerImage.PostedFile == null || fileUploadPartnerImage.PostedFile.ContentLength == 0) && (objCampaignPartnerImage != null))
                {
                    objCampaign.PartnerImage = objCampaignPartnerImage.PartnerImage;
                    objCampaign.PartnerImageContentType = objCampaignPartnerImage.PartnerImageContentType;
                }
                else
                {
                    if (strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUploadPartnerImage.PostedFile.FileName).ToLower())))
                    {
                        objCampaign.PartnerImage = _GetScaledImage(fileUploadPartnerImage.PostedFile.InputStream, 320, 570);
                        objCampaign.PartnerImageContentType = fileUploadPartnerImage.PostedFile.ContentType;
                    }
                }
            }
            else
            {
                objCampaign.Title = txtTitle.Text.Trim();
                //objCampaign.Description = txtDescription.Text.Trim();
                objCampaign.Description = fckEditorDescription.Value.Trim();
                objCampaign.PromotionalTagLine = txtPromotionalTagline.Text.Trim();
                if (fileUploadPartnerLogo.PostedFile != null && fileUploadPartnerLogo.PostedFile.ContentLength > 0
                    && strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUploadPartnerLogo.PostedFile.FileName).ToLower())))
                {
                    objCampaign.PartnerLogo = _GetScaledImage(fileUploadPartnerLogo.PostedFile.InputStream, 130, 200);
                    objCampaign.PartnerLogoContentType = fileUploadPartnerLogo.PostedFile.ContentType;
                }
                if (fileUploadPartnerImage.PostedFile != null && fileUploadPartnerImage.PostedFile.ContentLength > 0
                    && strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUploadPartnerLogo.PostedFile.FileName).ToLower())))
                {
                    objCampaign.PartnerImage = _GetScaledImage(fileUploadPartnerImage.PostedFile.InputStream, 320, 570);
                    objCampaign.PartnerImageContentType = fileUploadPartnerImage.PostedFile.ContentType;
                }
            }
            Utilities.SessionInfo.ResetCampaignPreviewSession();
            Utilities.SessionInfo.SetCampaignPreviewToSession(objCampaign);
            Response.Redirect("/Pages/Campaign/CampaignPreview.aspx");
        }

        private byte[] _GetScaledImage(Stream imageStream, int height, int width)
        {
            System.Drawing.Image image = System.Drawing.Image.FromStream(imageStream);
            System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
            //Should scale the image if it's height is greater than 70 or width is greater than 160
            byte[] buffer = (byte[])converter.ConvertTo(image, typeof(byte[]));
            if (image.PhysicalDimension.Height > height || image.PhysicalDimension.Width > width)
            {
                buffer = (byte[])converter.ConvertTo(ImageLib.ImageHelper.Resample(image, "", width, height), typeof(byte[]));
            }
            return buffer;
        }

        protected void lnkCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Campaign/ActiveCampaigns.aspx", true);
        }

        protected void lnkPreviewLogo_OnClick(object sender, EventArgs e)
        {

            Session["PartnerLogo"] = fileUploadPartnerLogo.FileBytes;
            imgNewLogo.ImageUrl = "ImageHandler.ashx";

            imgNewLogo.Visible = true;
        }

        // Days Check All
        protected void cbAll_OnCheckedChanged(object sender, EventArgs e)
        {
            if (cbAll.Checked == true)
            {
                foreach (System.Web.UI.WebControls.ListItem li in cblAffiliates.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (System.Web.UI.WebControls.ListItem li in cblAffiliates.Items)
                {
                    li.Selected = false;
                }
            }
        }

        #region Validators

        protected void OnValidateTitle(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtTitle.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Enter title";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Title").ToString();
                return;
            }
            return;
        }

        protected void OnValidateURL(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtURL.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Url").ToString();
                return;
            }
            //string url = litURL.Text + txtURL.Text.Trim();
            //if (!Regex.IsMatch(url, "^(ht|f)tp(s?)\\:\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\'\\/\\\\+&amp;%\\$#_]*)?$"))
            if (!Regex.IsMatch(txtURL.Text.Trim(), "^[A-Za-z0-9_-]+$"))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_ValidCampUrl").ToString();
                return;
            }
            if (!IsEditMode && AHAHelpContent.Campaign.DoesCampaignExistByURL(txtURL.Text.Trim()))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Campagin_WithUrl").ToString() + " " + txtURL.Text.Trim() + " " + GetGlobalResourceObject("Heart360AdminGlobalResources", "Please_Try_Diffrent_Url").ToString();
                return;
            }
            return;
        }

        protected void OnValidateResourceURL(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (Regex.IsMatch(txtResourceURL.Text.Trim(), "^[A-Za-z0-9_-]+$"))
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_Resource_URL").ToString();
                return;
            }

            return;
        }

        protected void OnValidateStartDate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (dtStartDate.Text.Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Select_Start_Date").ToString();
                return;
            }
            return;
        }

        protected void OnValidateEndDate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (dtEndDate.Text.Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Select_End_Date").ToString();
                return;
            }
            return;

        }

        protected void OnValidateDate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (dtStartDate.Text.Length != 0 && dtEndDate.Text.Length != 0)
            {
                if (dtStartDate.Date.Value > dtEndDate.Date.Value)
                {
                    v.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Select_End_Date_Select_End_Date").ToString();
                    return;
                }
            }
            return;
        }

        protected void OnValidateDescription(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (fckEditorDescription.Value.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "Enter Description";
                return;
            }
        }

        protected void OnValidatePartnerLogo(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
           
            if (fileUploadPartnerLogo.PostedFile != null && fileUploadPartnerLogo.PostedFile.ContentLength > 0)
            {
                string[] strImageExtArray = { ".jpeg", ".jpg", ".gif", ".png" };
                if (!strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUploadPartnerLogo.PostedFile.FileName).ToLower())))
                {
                    v.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_PartnerLogo_FileType").ToString();
                    return;
                }
                else if (fileUploadPartnerLogo.PostedFile.ContentLength > 307200)
                {
                    v.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_PartnerLogo_FileSize").ToString();
                    return;
                }
                else 
                {
                    System.Drawing.Image PartnerLogo = System.Drawing.Image.FromStream(fileUploadPartnerLogo.PostedFile.InputStream);
                    if ((PartnerLogo.PhysicalDimension.Height != PartnerLogoHeight) || (PartnerLogo.PhysicalDimension.Width != PartnerLogoWidth))
                    {
                        v.IsValid = false;
                        custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_PartnerLogo_Dimensions").ToString();
                        return;
                    }
                }
            }
        }

        protected void OnValidatePartnerImage(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (fileUploadPartnerImage.PostedFile != null && fileUploadPartnerImage.PostedFile.ContentLength > 0)
            {
                string[] strImageExtArray = { ".jpeg", ".jpg", ".gif", ".png" };
                
                if (!strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUploadPartnerImage.PostedFile.FileName).ToLower())))
                {
                    v.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_PartnerImage_FileType").ToString();
                    return;
                }
                else if (fileUploadPartnerImage.PostedFile.ContentLength > 307200)
                {
                    v.IsValid = false;
                    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_PartnerImage_FileSize").ToString();
                    return;
                }
                else 
                {
                    System.Drawing.Image PartnerImage = System.Drawing.Image.FromStream(fileUploadPartnerImage.PostedFile.InputStream);
                    if ((PartnerImage.Height != PartnerImageHeight) || (PartnerImage.Width != PartnerImageWidth))
                    {
                        v.IsValid = false;
                        custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_PartnerImage_Dimensions").ToString();
                        return;
                    }
                }
            }
        }
        
        #endregion
        }
    }