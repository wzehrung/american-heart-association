﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;

using System.Reflection;
using System.ComponentModel;

namespace Heart360Admin.Pages.Campaign
{
    public partial class InactiveCampaigns : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["sortOrder"] = "";
                _bindDataToGridView("", "");
            }
        }

        protected void gvExpiredCampaigns_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            foreach (GridViewRow row in gvExpiredCampaigns.Rows)
            {
                // Get the campaign ID from the first cell in the row
                int iCampaignID = Convert.ToInt32(row.Cells[0].Text);

                AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByCampaignID(iCampaignID, 1);

                Literal litTest = (Literal)(row.FindControl("litTest"));

                string sDescription = string.Empty;
                // Remove any HTML tags
                sDescription = GRCBase.HtmlScrubber.Clean(objCampaign.Description, false, true);

                // Limit the length of the description
                int iMaxLength = 64;
                if (sDescription.Length > 64)
                {
                    sDescription = sDescription.Substring(0, iMaxLength) + "...";
                }

                row.Cells[2].Text = sDescription;

                // Format the URL to be clickable
                string sCampaignURL = objCampaign.CampaignURL;
                row.Cells[3].Text = "<a href=" + AHACommon.AHAAppSettings.SiteUrl + "/" + sCampaignURL + ">.../" + sCampaignURL;

                // Format the Start and End dates
                row.Cells[4].Text = objCampaign.StartDate.ToShortDateString();
                row.Cells[5].Text = objCampaign.EndDate.ToShortDateString();
            }
        }

        protected void gvExpiredCampaigns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string sCampaignID = e.CommandArgument.ToString();

            if (e.CommandName == "Edit")
            {
                Response.Redirect("/pages/campaign/CreateCampaign.aspx?CID=" + sCampaignID);
            }
            else if (e.CommandName == "Delete")
            {
                AHAHelpContent.Campaign.DeleteCampaign(Convert.ToInt32(sCampaignID));
                Response.Redirect("/Pages/Campaign/InactiveCampaigns.aspx");
            }

            _bindDataToGridView("", "");
        }

        protected void gvExpiredCampaigns_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvExpiredCampaigns.PageIndex = e.NewPageIndex;
            _bindDataToGridView("", "");


        }

        private void _bindDataToGridView(string sortExp, string sortDir)
        {
            List<AHAHelpContent.Campaign> listCampaign = AHAHelpContent.Campaign.FindAllExpiredCampaigns(Utilities.Utility.GetCurrentLanguageID());

            DataTable table = ConvertToDataTable(listCampaign);

            DataView myDataView = new DataView();
            myDataView = table.DefaultView;

            if (sortExp != string.Empty)
            {
                myDataView.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvExpiredCampaigns.DataSource = listCampaign;
            gvExpiredCampaigns.DataBind();
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            _bindDataToGridView(e.SortExpression, sortOrder);
        }


        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        private string FormatUrlText(string strURL, int length)
        {
            if (string.IsNullOrEmpty(strURL) || strURL.Length <= length)
            {
                return strURL;
            }
            else
            {
                int intLength = strURL.Length;
                for (int i = 1; i <= intLength / length; i++)
                {
                    if (i == 1)
                    {
                        //length is the number of charcters after which to insert '<wbr>&shy;'
                        strURL = strURL.Insert((i * length), "<wbr>&shy;");
                    }
                    else
                    {
                        //length is the number of charcters after which to insert '<wbr>&shy;' and 10 is the number of charcters in '<wbr>&shy;'
                        strURL = strURL.Insert((i * length) + (5 * (i - 1)), "<wbr>&shy;");
                    }
                }
            }
            return strURL;
        }
    }
}