﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages.Campaign
{
    public partial class CampaignPreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AHAHelpContent.Campaign objCampaign = Utilities.SessionInfo.GetCampaignPreviewFromSession();
            if (objCampaign != null)
            {
                litTitle.Text = objCampaign.Title;
                litDescription.Text = objCampaign.Description;
                litPromotionalTagLine.Text = objCampaign.PromotionalTagLine;
            }
        }
    }
}
