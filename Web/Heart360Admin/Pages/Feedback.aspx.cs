﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Pages
{
    public partial class Feedback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRecipientsGridView();
                BindGridView();
            }
        }

        private void BindGridView()
        {
            List<AHAHelpContent.Feedback> objFeedbackTypes = AHAHelpContent.Feedback.GetFeedbackTypes(false);

            gvFeedbackTypes.DataSource = objFeedbackTypes;
            gvFeedbackTypes.DataBind();
        }

        private void BindRecipientsGridView()
        {
            List<AHAHelpContent.Feedback> objRecipients = AHAHelpContent.Feedback.GetAdminUsers();

            gvFeedbackRecipients.DataSource = objRecipients;
            gvFeedbackRecipients.DataBind();
        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Set the edit index.
            gvFeedbackTypes.EditIndex = e.NewEditIndex;
            BindGridView();
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //Reset the edit index.
            gvFeedbackTypes.EditIndex = -1;
            //Bind data to the GridView control.
            BindGridView();
        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            // Need to get the row we are editing
            GridViewRow row = (GridViewRow)gvFeedbackTypes.Rows[e.RowIndex];

            // Now find the controls inside the gridview
            TextBox tb = (TextBox)row.FindControl("txtFeedbackTypeName");
            Label lbl = (Label)row.FindControl("lblFeedbackTypeID");

            //Reset the edit index.
            gvFeedbackTypes.EditIndex = -1;

            AHAHelpContent.Feedback.UpdateFeedbackType(tb.Text, Convert.ToInt32(lbl.Text));

            //Bind data to the GridView control.
            BindGridView();
        }

        protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Need to get the row we are editing
            GridViewRow row = (GridViewRow)gvFeedbackTypes.Rows[e.RowIndex];

            // Now find the controls inside the gridview
            Label lbl = (Label)row.FindControl("lblFeedbackTypeID");

            //Reset the edit index.
            gvFeedbackTypes.EditIndex = -1;

            AHAHelpContent.Feedback.DeleteFeedbackType(Convert.ToInt32(lbl.Text));

            //Bind data to the GridView control.
            BindGridView();
        }

        protected void gvFeedbackTypes_OnCheckedChanged(object sender, EventArgs e)
        {

            foreach (GridViewRow row in gvFeedbackTypes.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkEnabled");
                Label lblType = (Label)row.FindControl("lblFeedbackTypeID");

                if (chk != null)
                {
                    AHAHelpContent.Feedback.EnableFeedbackType(Convert.ToInt32(lblType.Text), Convert.ToBoolean(chk.Checked));
                }
            }

            BindGridView();
        }

        protected void CreateFeedbackType()
        {
            AHAHelpContent.Feedback.CreateFeedbackType(txtCreateName.Text, true);
            BindGridView();
        }

        protected void gvFeedbackRecipients_OnCheckedChanged(object sender, EventArgs e)
        {
            //gvFeedbackRecipients.EditIndex = -1;

            foreach (GridViewRow row in gvFeedbackRecipients.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkRecipient");
                Label lbl = (Label)row.FindControl("lblAdminUserID");
                if (chk != null)
                {
                    AHAHelpContent.Feedback.AddFeedbackRecipients(Convert.ToInt32(lbl.Text), Convert.ToBoolean(chk.Checked));
                }
            }

            BindRecipientsGridView();
        }

        protected void gvFeedbackRecipients_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvFeedbackRecipients.PageIndex = e.NewPageIndex;
            BindRecipientsGridView();
        }

        protected void lnkCreate_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");

            if (!Page.IsValid)
                return;
            CreateFeedbackType();
        }

        #region Validators

        protected void OnValidateName(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtCreateName.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Validator_Feedback_BlankName").ToString();
            }
        }

        #endregion

    }
}