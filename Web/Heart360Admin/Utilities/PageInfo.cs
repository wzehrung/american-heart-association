﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using GRCBase;
using System.Xml;

public class AdminPage
{
    public string Url
    {
        get;
        set;
    }

    public string PageTitle
    {
        get;
        set;
    }

    public string MainTabID
    {
        get;
        set;
    }
}
public class PageInfo
{
    static Hashtable PageTitles = new Hashtable();
    static bool bIsLoaded = false;
    static object objLock = new object();

    private static void _parseAndLoadData()
    {
        PageTitles.Clear();

        System.Xml.XmlDocument document = new System.Xml.XmlDocument();
        string xmlFilePath = System.Web.HttpContext.Current.Server.MapPath("~/XmlFiles/PageTitle.xml");

        System.IO.FileStream fileStream = System.IO.File.OpenRead(xmlFilePath);
        document.Load(fileStream);

        XmlNodeList nodeList = document.SelectNodes("Pages/Page");
        foreach (XmlNode pageNode in nodeList)
        {
            AdminPage objPage = new AdminPage
            {
                Url = XmlUtility.GetAttributeStringValue(pageNode, "Url"),
                PageTitle = XmlUtility.GetAttributeStringValue(pageNode, "Title"),
                MainTabID = XmlUtility.GetAttributeStringValue(pageNode, "MainTabID")
            };

            PageTitles.Add(objPage.Url.ToLower(), objPage);
        }

        fileStream.Close();
        fileStream.Dispose();
    }

    private static void _loadData()
    {
        if (!bIsLoaded)
        {
            lock (objLock)
            {
                if (!bIsLoaded)
                {
                    _parseAndLoadData();
                    bIsLoaded = true;
                }
            }
        }
    }

    public static AdminPage GetPageDetailsFromUrl(string strUrl)
    {
        _loadData();

        AdminPage objPage = PageTitles[strUrl.ToLower()] as AdminPage;

        return objPage;
    }
}

