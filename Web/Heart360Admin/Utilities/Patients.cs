﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GRCBase;
using System.Data.SqlClient;
using System.Data;

namespace Heart360Admin.Utilities
{
    public class Patients
    {
        #region Public Properties

        public int PatientID
        {
            get;
            set;
        }

        public string CreatedDate
        {
            get;
            set;
        }

        public string UpdatedDate
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string EmailAddress
        {
            get;
            set;
        }

        public bool TermsAccepted { get; set; }

        public string TermsAcceptanceDate { get; set; }
        #endregion

        #region Private Methods

        private void OnDataBind(SqlDataReader reader)
        {
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
            this.CreatedDate = BasicConverter.DbToStringValue(reader["CreatedDate"]);
            this.FirstName = BasicConverter.DbToStringValue(reader["FirstName"]);
            this.LastName = BasicConverter.DbToStringValue(reader["LastName"]);
            this.EmailAddress = BasicConverter.DbToStringValue(reader["EmailAddress"]);
            this.TermsAccepted = BasicConverter.DbToBoolValue(reader["TermsAccepted"]);
            this.TermsAcceptanceDate = BasicConverter.DbToStringValue((reader["TermsAcceptanceDate"]));
        }

        #endregion

        public static void ResetUserAcceptance(int PatientID)
        {

            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Admin_Patient_ResetUserAcceptance]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@PatientID", SqlDbType.Int);
                param.Value = BasicConverter.IntToDbValue(PatientID);

                reader = cmd.ExecuteReader();

                return;
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static void ResetAllUserAcceptance()
        {

            DBContext dbContext = null;
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                string sql = "[Proc_Admin_Patient_ResetAllUserAcceptance]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;
                reader = cmd.ExecuteReader();
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

        public static List<Patients> FindPatientByLogin(string login)
        {
            DBContext dbContext = null;
            List<Patients> objPatientList = new List<Patients>();
            SqlDataReader reader = null;
            SqlCommand cmd = null;
            bool bException = false;

            try
            {
                dbContext = DBContext.GetDBContext();

                SqlParameter param = null;
                string sql = "[Proc_Admin_Patient_FindByLogin]";

                cmd = dbContext.ContextSqlCommand;
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;

                param = dbContext.ContextSqlCommand.Parameters.Add("@Login", SqlDbType.NVarChar);
                param.Value = BasicConverter.StringToDbValue(login);

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Patients objPatient = new Patients();
                    objPatient.OnDataBind(reader);
                    objPatientList.Add(objPatient);
                }

                return objPatientList;
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                DBContext.CloseReader(reader);
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
        }

    }
}