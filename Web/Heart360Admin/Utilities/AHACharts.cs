﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;

namespace Heart360Admin.Utilities
{
    public class AHACharts
    {
        public string Title
        {
            get;
            set;
        }
        public string XAxisTitle
        {
            get;
            set;
        }
        public string YAxisTitle
        {
            get;
            set;
        }

        public List<GraphData> GraphDataList
        {
            get;
            set;
        }

        public SeriesChartType ChartType
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public bool YAxisMajorGridEnabled
        {
            get;
            set;
        }

        public bool XAxisMajorGridEnabled
        {
            get;
            set;
        }

        public bool Enable3D
        {
            get;
            set;
        }

        public Chart GenerateChart(Chart chart)
        {
            //Title t_web = new Title(this.Title, Docking.Top, new System.Drawing.Font("Trebuchet MS", 14, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(26, 59, 105));
            //chart.Titles.Add(t_web);
            chart.Width = this.Width;
            chart.Height = this.Height;
            chart.BorderWidth = 2;
            chart.RenderType = RenderType.ImageTag;

            ChartArea chartArea = chart.ChartAreas.Add("PatientHealth");

            chartArea.AxisX.Title = this.XAxisTitle;
            chartArea.AxisX.MajorGrid.Enabled = XAxisMajorGridEnabled;
            chartArea.AxisY.Title = this.YAxisTitle;
            chartArea.AxisY.MajorGrid.Enabled = YAxisMajorGridEnabled;
            chartArea.Area3DStyle.Enable3D = this.Enable3D;
            chartArea.AxisX.IsLabelAutoFit = true;
            chartArea.AxisX.Interval = 1;

            foreach (GraphData graphData in GraphDataList)
            {
                Series series = new Series(graphData.SeriesName);
                //series.ChartType = SeriesChartType.StackedColumn;
                series.YValueType = ChartValueType.Int32;
                series.ChartType = this.ChartType;
                series.BorderWidth = 3;
                if (!string.IsNullOrEmpty(graphData.Color))
                {
                    series.Color = System.Drawing.ColorTranslator.FromHtml(graphData.Color);
                }
                foreach (GraphInputReading graphInputReading in graphData.GraphInputReadingList)
                {
                    int index = series.Points.AddXY(graphInputReading.XValue, graphInputReading.YValue);
                    series.Points[index].ToolTip = graphInputReading.YValue.ToString();
                }
                chart.Series.Add(series);
            }
            var legend = chart.Legends.Add(string.Empty);
            legend.Docking = Docking.Bottom;
            return chart;
        }
    }


    public class GraphData
    {
        public string SeriesName
        {
            get;
            set;
        }

        public string Color
        {
            get;
            set;
        }


        public List<GraphInputReading> GraphInputReadingList
        {
            get;
            set;
        }

    }

    public class GraphInputReading
    {
        public object YValue
        {
            get;
            set;
        }
        public object XValue
        {
            get;
            set;
        }
    }



}
