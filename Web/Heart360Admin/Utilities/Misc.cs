﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Heart360Admin
{
    public enum ReportFilterType
    {
        Tracker,
        ReportedUploads,
        ReadingSource,
        Gender,
        Age,
        //AllUserVisits,
        NewAllUserVisits,
        Campaign,
        Ethnicity,
        PatientProviderConnection
    }

    public class ReportFilter
    {
        public ReportFilterType ReportFilterType
        {
            get;
            set;
        }

        public int FilterIndex
        {
            get;
            set;
        }
    }

    public class ReportHelper
    {
        public static ListItemCollection TrackerList
        {
            get
            {
                ListItemCollection list = new ListItemCollection();
                list.Add(new ListItem("Blood Glucose", "0"));
                list.Add(new ListItem("Blood Pressure", "1"));
                list.Add(new ListItem("Cholesterol", "2"));
                list.Add(new ListItem("Medication", "3"));
                list.Add(new ListItem("Physical Activity", "4"));
                list.Add(new ListItem("Weight", "5"));
                return list;
            }
        }

        public static ListItemCollection GenderList
        {
            get
            {
                ListItemCollection list = new ListItemCollection();
                list.Add(new ListItem("Male", "0"));
                list.Add(new ListItem("Female", "1"));
                list.Add(new ListItem("Unreported", "2"));
                return list;
            }
        }


        public static ListItemCollection EthnicityList
        {
            get
            {
                ListItemCollection list = new ListItemCollection();
                list.Add(new ListItem("American Indian or Alaska Native", "0"));
                list.Add(new ListItem("Asian", "1"));
                list.Add(new ListItem("Black or African American", "2"));
                list.Add(new ListItem("Hispanic or Latino", "3"));
                list.Add(new ListItem("Mixed Race", "4"));
                list.Add(new ListItem("Native Hawaiian or Other Pacific Islander", "5"));
                list.Add(new ListItem("Other Race", "6"));
                list.Add(new ListItem("White", "7"));

                return list;
            }
        }



        public static ListItemCollection UploadMethodSourceList
        {
            get
            {
                ListItemCollection list = new ListItemCollection();
                list.Add(new ListItem("Doctor's Office", "0"));
                list.Add(new ListItem("Mobile", "1"));
                list.Add(new ListItem("Pharmacy", "2"));
                list.Add(new ListItem("Home", "3"));
                list.Add(new ListItem("Hospital", "4"));
                list.Add(new ListItem("Other", "5"));
                list.Add(new ListItem("IVR", "6"));
                list.Add(new ListItem("Health Fair", "7"));
                list.Add(new ListItem("Device", "8"));
                return list;
            }
        }

        public static ListItemCollection AgeList
        {
            get
            {
                ListItemCollection list = new ListItemCollection();
                list.Add(new ListItem("Under 25", "0"));
                list.Add(new ListItem("25-34", "1"));
                list.Add(new ListItem("35-44", "2"));
                list.Add(new ListItem("45-54", "3"));
                list.Add(new ListItem("55-64", "4"));
                list.Add(new ListItem("65-74", "5"));
                list.Add(new ListItem("75 or older", "6"));
                list.Add(new ListItem("Unreported", "7"));
                return list;
            }
        }

        public static ListItemCollection UserVisitsList
        {
            get
            {
                ListItemCollection list = new ListItemCollection();
                list.Add(new ListItem("1 Visit (All)", "0"));
                list.Add(new ListItem("2-4 Visits (All)", "1"));
                list.Add(new ListItem("5-10 Visits (All)", "2"));
                list.Add(new ListItem(">10 Visits (All)", "3"));
                list.Add(new ListItem("1 Visit (New)", "4"));
                list.Add(new ListItem("2-4 Visits (New)", "5"));
                list.Add(new ListItem("5-10 Visits (New)", "6"));
                list.Add(new ListItem(">10 Visits (New)", "7"));
                return list;
            }
        }

        public static ListItemCollection NumberOfConnectedPatients
        {
            get
            {
                ListItemCollection list = new ListItemCollection();
                list.Add(new ListItem("1 - 10 Patients", "0"));
                list.Add(new ListItem("11 - 25 Patients", "1"));
                list.Add(new ListItem("26 - 75 Patients", "2"));
                list.Add(new ListItem("76 - 100 Patients", "3"));
                list.Add(new ListItem(">100 Patients", "4"));
                return list;
            }
        }
    }
}
