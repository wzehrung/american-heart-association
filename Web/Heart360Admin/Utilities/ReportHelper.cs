﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Admin
{
    public interface IReportDataManager
    {
        // These are deprecated from previous versions. Use the IReportDataManagerReporter interface.
        //void LoadItems();
        //void ClearFilters();

        DateTime StartDate
        {
            get;
        }

        DateTime EndDate
        {
            get;
        }

        // If all Campaigns are selected, then this value should be null
        // Otherwise this is the ID of the selected Campaign
        // If this value is -1, it means that the option "None" was selected for Campaigns (not applicable to all reports)
        int? CampaignID
        {
            get;
        }

        // string.Empty when all campaigns are selected, otherwise the string value of an ID

        string CampaignIDs
        {
            get;
        }

        // string.Empty when all affiliate regions are selected.
        // Otherwise a CSV list of affiliate names
        string AffiliateNames
        {
            get;
        }

        // string.Empty when all markets are selected.
        // Otherwise a CSV list of Markets (names)
        string Markets
        {
            get;
        }

        int? PartnerID
        {
            get;
        }

/**
        string PartnerIDs
        {
            get;
        }
         **/

        int? UserTypeID
        {
            get;
        }

        List<string> MonthNames
        {
            get;
        }

        
    }

    // This is an interface for classes/controls that produce a report from a reference to an IReportDataManager.
    // 
    public interface IReportDataManagerReporter
    {
        void LoadItems(IReportDataManager iRdm);
    }

}
