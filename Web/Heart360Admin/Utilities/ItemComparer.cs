﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Admin.Utilities
{
    public class HelpFulResourcesComparer : IComparer<AHAHelpContent.Content>
    {
        private string strFieldName;
        private bool bAscending;

        public HelpFulResourcesComparer(string strFieldName, bool bAscending)
        {
            this.strFieldName = strFieldName;
            this.bAscending = bAscending;
        }

        #region IComparer<Content> Members

        public int Compare(AHAHelpContent.Content x, AHAHelpContent.Content y)
        {
            if (this.strFieldName.ToLower().Equals("title"))
            {
                return (bAscending ? 1 : -1) * string.Compare(x.Title, y.Title,true);
            }
            else if (this.strFieldName.ToLower().Equals("didyouknowtext"))
            {
                return (bAscending ? 1 : -1) * string.Compare(x.DidYouKnowText, y.DidYouKnowText,true);
            }
            else if (this.strFieldName.ToLower().Equals("didyouknowlinktitle"))
            {
                return (bAscending ? 1 : -1) * string.Compare(x.DidYouKnowLinkTitle, y.DidYouKnowLinkTitle,true);
            }
            else if (this.strFieldName.ToLower().Equals("tag"))
            {
                return (bAscending ? 1 : -1) * string.Compare(x.ContentRuleNames, y.ContentRuleNames,true);
            }
            else if (this.strFieldName.ToLower().Equals("url"))
            {
                return (bAscending ? 1 : -1) * string.Compare(x.URL, y.URL, true);
            }
            else
            {
                return (bAscending ? 1 : -1) * Nullable.Compare<DateTime>(x.ExpirationDate, y.ExpirationDate);
            }
            
        }

        #endregion
    }
    public class ItemComparer
    {
    }
}
