﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using GRCBase;

namespace Heart360Admin.Utilities
{
    public class Utility
    {
        public static int GetCurrentLanguageID()
        {
            Heart360Admin.HttpModule.AdminRequestContext objContext = Heart360Admin.HttpModule.AdminRequestContext.GetContext();
            if (objContext != null)
            {
                return objContext.SelectedLanguage.LanguageID;
            }

            return AHAHelpContent.Language.GetDefaultLanguage().LanguageID;
        }

        public static string TrademarkedHeart360
        {
            get
            {
                return "Heart360<sup>&#174;</sup>";
            }
        }

        [Serializable]
        public class GrantLogo
        {
            public byte[] Content
            {
                get;
                set;
            }

            public string ContentType
            {
                get;
                set;
            }
        }


       



        /// <summary>
        /// Returns hexadecimal string of MD5 hashed string
        /// http://msdn.microsoft.com/en-us/library/system.security.cryptography.md5cryptoserviceprovider.aspx
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetHashString(string input)
        {
            try
            {
                // Create a new instance of the MD5CryptoServiceProvider object.
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();

            }
            catch (Exception e)
            {
                throw new Exception("An Error occurred:-" + e.Message);
            }
        }


        /// <summary>
        /// Render the Link expressions as html anchor
        /// </summary>
        /// <param name="strContent">Search Content</param>
        /// <returns>Text with html anchor</returns>
        public static string RenderLink(string strContent)
        {
            //Construct Links
            if (!string.IsNullOrEmpty(strContent))
            {
                strContent = Regex.Replace(strContent,
                    "\\[(L|l)(I|i)(N|n)(K|k)(\\s)*(T|t)(E|e)(X|x)(T|t)(\\s)*=(\\s)*'.*'(\\s)*(U|u)(R|r)(L|l)(\\s)*=(\\s)*'.*'(\\s)*\\]",
                    delegate(Match match)
                    {
                        string strMatchText = match.Value;
                        strMatchText = Regex.Replace(strMatchText, "\\[(L|l)(I|i)(N|n)(K|k)(\\s)*", string.Empty);
                        strMatchText = Regex.Replace(strMatchText, "(\\s)*\\]", string.Empty);

                        //Remove unwanted space
                        strMatchText = Regex.Replace(strMatchText, "(\\s)*=(\\s)*", "=");
                        strMatchText = Regex.Replace(strMatchText, "'(\\s)+", "',");

                        string strText = string.Empty;
                        string strHref = string.Empty;

                        strMatchText.Split((",".ToCharArray())).ToList().ForEach(delegate(string strAttributes)
                        {
                            if (strAttributes.ToLower().Contains("text"))
                                strText = Regex.Replace(strAttributes, "(T|t)(E|e)(X|x)(T|t)(\\s)*=(\\s)*", string.Empty).Replace("'", string.Empty);

                            if (strAttributes.ToLower().Contains("url"))
                                strHref = Regex.Replace(strAttributes, "(U|u)(R|r)(L|l)(\\s)*=(\\s)*", string.Empty).Replace("'", string.Empty);
                        });

                        if (strHref.ToLower().StartsWith("#"))
                        {
                            return string.Format("<a href=\"{0}\">{1}</a>", strHref, strText);
                        }
                        else
                        {
                            if (!strHref.ToLower().StartsWith("http://"))
                            {
                                strHref = strHref.Insert(0, "http://");
                            }
                        }

                        return string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", strHref, strText);
                    });
            }

            return strContent;
        }

        internal static string GetMonthName(int month)
        {
            string retValue = string.Empty;
            switch (month)
            {
                case 1: retValue = "Jan"; break;
                case 2: retValue = "Feb"; break;
                case 3: retValue = "Mar"; break;
                case 4: retValue = "Apr"; break;
                case 5: retValue = "May"; break;
                case 6: retValue = "Jun"; break;
                case 7: retValue = "Jul"; break;
                case 8: retValue = "Aug"; break;
                case 9: retValue = "Sep"; break;
                case 10: retValue = "Oct"; break;
                case 11: retValue = "Nov"; break;
                case 12: retValue = "Dec"; break;
                default: throw new Exception("Month is not supplied to get the Month name");
            }
            return retValue;
        }

        //public static void SetLocaleInfo(string Locale)
        //{
        //    CookieHelper.SetCookie("AHALocaleSetting", Locale, DateTime.Now.AddDays(3));
        //}

        //public static int GetSelectedLanguage()
        //{
        //    HttpCookie objCookie = CookieHelper.GetCookie("AHALocaleSetting");
        //    if (objCookie != null)
        //    {
        //        return Convert.ToInt32(objCookie.Value);
        //    }
        //    if (Utilities.SessionInfo.GetUserContext() != null)
        //    {
        //        AHAHelpContent.AdminUser objAdminUser = AHAHelpContent.AdminUser.GetDefaultLanguageIDForAdminUser(Utilities.SessionInfo.GetUserContext().UserID);
        //        return objAdminUser.DefaultLanguageID;
        //    }
        //    else
        //    {
        //        return AHAHelpContent.Language.GetDefaultLanguage().LanguageID;
        //    }
        //}        


    }
}
