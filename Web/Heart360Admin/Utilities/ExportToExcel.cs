﻿using System.Collections.Generic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Heart360Admin.Utilities
{
    public class ExportToExcel
    {
        private const string cnstDateFrom = "B3";

        #region Patient Specific Cell Constants

        private const string cnstUsers = "A4";
        private const string cnstReturningUsersFrom = "K9";

        private const string cnstTrackingReportCell = "A11";
        private const string cnstRprtdUploadsCell = "A19";
        private const string cnstUploadMethodCell = "A27";

        private const string cnstReportedVisitsAllUsers = "A38";
        private const string cnstReportedVisitsAllUsersPercentage = "A48";

        private const string cnstGenderCumulativeCell = "A58";
        private const string cnstEthnBrkdwnCell = "A63";
        private const string cnstAgeBrkdwnCell = "A73";

        #endregion

        #region Provider Specific Cell Constants

        private const string cnstProviders = "A4";
        private const string cnstNewProviderVisits = "A26"; // "A10";
        private const string cnstAllProviderVisits = "A48"; // "A16";
        private const string cnstNewProviderVisitsPercentage = "A70";   // "A22";
        private const string cnstAllProviderVisitsPercentage = "A76";   // "A28";
        private const string cnstPatientCount = "A82";  // "A34";
        private const string cnstPatientConnectedToProviders = "A87";   // "A39";

        #endregion

        #region To Fill Patient Specific Cells

        private static void _fillUsers(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetHeart360UserReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillTracking(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetTrackerReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillReportedUploads(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetReportedUploadsReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillUploadMethodTracker(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetUploadMethodTrackerReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillEthnicity(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetEthnicityBreakdownReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillReportedVisitsAllUsers(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetAllUserVisitsReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillReportedVisitsAllUsersPercentage(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetAllUserVisitsPercentageReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillGenderCumulativeReport(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetGenderBreakdownReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        private static void _fillAgeBreakdownReport(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iPartnerID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReport.GetAgeBreakdownReport(dtFrom, dtTo, iPartnerID).Tables[0]);
        }

        #endregion

        #region To Fill Patient/Campaign Specific Cells

        private static void _fillUsers_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetHeart360UserReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillTracking_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetTrackerReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillReportedUploads_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetReportedUploadsReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillUploadMethodTracker_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetUploadMethodTrackerReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillReportedVisitsAllUsers_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetAllUserVisitsReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillReportedVisitsAllUsersPercentage_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetAllUserVisitsPercentageReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillGenderCumulativeReport_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetGenderBreakdownReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillEthnicity_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetEthnicityBreakdownReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        private static void _fillAgeBreakdownReport_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.PatientReportCampaigns.GetAgeBreakdownReportCampaign(dtFrom, dtTo, sCampaignIDs, sMarkets).Tables[0]);
        }

        #endregion

        #region To Fill PROVIDER/CAMPAIGN Specific Cells

        private static void _fillProviders_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            /** non-rollup version
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReportCampaign.GetAllProvidersReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
            **/
            objEH.SetMatrixValue_RollUp(strCellName, AHAHelpContent.ProviderReportCampaign.GetAllProvidersReport_RollUp(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillReportedVisitsNewProviders_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            /** non-rollup version
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReportCampaign.GetNewProviderVisitsReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
            **/
            objEH.SetMatrixValue_RollUp(strCellName, AHAHelpContent.ProviderReportCampaign.GetNewProviderVisitsReport_RollUp(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillReportedVisitsAllProviders_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            /** non-rollup version
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReportCampaign.GetAllProviderVisitsReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
            **/
            objEH.SetMatrixValue_RollUp(strCellName, AHAHelpContent.ProviderReportCampaign.GetAllProviderVisitsReport_RollUp(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillReportedVisitsNewProvidersPercentage_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReportCampaign.GetNewProviderVisitsPercentageReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillReportedVisitsAllProvidersPercentage_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReportCampaign.GetAllProviderVisitsPercentageReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillPatientCounts_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReportCampaign.GetPatientCountPerProviderReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillPatientProviderConnection_Campaign(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReportCampaign.GetProviderConnectedToPatientsReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        #endregion

        #region To Fill Provider Specific Cells

        private static void _fillProviders(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID )
        {
            /** non-rollup version
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReport.GetAllProvidersReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
            **/

            objEH.SetMatrixValue_RollUp(strCellName, AHAHelpContent.ProviderReport.GetAllProvidersReport_RollUp(dtFrom, dtTo).Tables[0]);
        }

        private static void _fillReportedVisitsNewProviders(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            /** non-rollup version
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReport.GetNewProviderVisitsReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
            **/
            objEH.SetMatrixValue_RollUp(strCellName, AHAHelpContent.ProviderReport.GetNewProviderVisitsReport_RollUp(dtFrom, dtTo).Tables[0]);
        }

        private static void _fillReportedVisitsAllProviders(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            /** non-rollup version
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReport.GetAllProviderVisitsReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
             **/
            objEH.SetMatrixValue_RollUp(strCellName, AHAHelpContent.ProviderReport.GetAllProviderVisitsReport_RollUp(dtFrom, dtTo).Tables[0]);
        }

        private static void _fillReportedVisitsNewProvidersPercentage(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReport.GetNewProviderVisitsPercentageReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillReportedVisitsAllProvidersPercentage(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReport.GetAllProviderVisitsPercentageReport(dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID).Tables[0]);
        }

        private static void _fillPatientCounts(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets )
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReport.GetPatientCountPerProviderReport(dtFrom, dtTo, iCampaignID, sMarkets).Tables[0]);
        }

        private static void _fillPatientProviderConnection(string strCellName, ExcelHelper objEH, DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets)
        {
            objEH.SetMatrixValue(strCellName, AHAHelpContent.ProviderReport.GetProviderConnectedToPatientsReport(dtFrom, dtTo, iCampaignID, sMarkets).Tables[0]);
        }

        #endregion

        public static void ExportPatientReport(DateTime dtFrom, DateTime dtTo, int? iCampaignID, int? iPartnerID)
        {
            System.Diagnostics.Debug.WriteLine("Entered into ExportPatientReport");

            string strTemplateFilePath = string.Format("{0}{1}",
                    Utilities.AppSettings.ExcelTemplatePath,
                    Utilities.AppSettings.BackEndReporting_Patient_FileName);

            string strSaveFilePath = string.Empty;

            System.Diagnostics.Debug.WriteLine("start creating excel helper");

            using (ExcelHelper objEH = new ExcelHelper(strTemplateFilePath))
            {
                System.Diagnostics.Debug.WriteLine("Created excel helper");
                //Fill the date header
                //Fill only the from date cell; rest will be managed by worksheet using formula
                System.Diagnostics.Debug.WriteLine("fill dates");
                objEH.SetCellValue(cnstDateFrom, dtFrom);

                System.Diagnostics.Debug.WriteLine("fill users");
                _fillUsers(cnstUsers, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill tracking");
                _fillTracking(cnstTrackingReportCell, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill Reported Uploads Report");
                _fillReportedUploads(cnstRprtdUploadsCell, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill Upload method Report");
                _fillUploadMethodTracker(cnstUploadMethodCell, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllUsers");
                _fillReportedVisitsAllUsers(cnstReportedVisitsAllUsers, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllUsersPercentage");
                _fillReportedVisitsAllUsersPercentage(cnstReportedVisitsAllUsersPercentage, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill Gender cumulative report");
                _fillGenderCumulativeReport(cnstGenderCumulativeCell, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill Ethnicity Breakdown Report");
                _fillEthnicity(cnstEthnBrkdwnCell, objEH, dtFrom, dtTo, iPartnerID);

                System.Diagnostics.Debug.WriteLine("fill AgeBreakdownReport");
                _fillAgeBreakdownReport(cnstAgeBrkdwnCell, objEH, dtFrom, dtTo, iPartnerID);

                strSaveFilePath = string.Format("{0}AHA_BackEndReport_Patient_{1}.xls", Utilities.AppSettings.ReportTempPath, Guid.NewGuid());
                System.Diagnostics.Debug.WriteLine("start saving file");
                objEH.SaveAs(strSaveFilePath);
                System.Diagnostics.Debug.WriteLine("end saving file");
            }

            #region Sending Response

            System.Diagnostics.Debug.WriteLine("start sending response");
            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + strSaveFilePath + "\"");

            FileStream sourceFile = new FileStream(strSaveFilePath, FileMode.Open);

            long FileSize = sourceFile.Length;

            byte[] getContent = new byte[(int)FileSize];

            sourceFile.Read(getContent, 0, (int)sourceFile.Length);
            sourceFile.Close();

            System.Web.HttpContext.Current.Response.BinaryWrite(getContent);
            System.Diagnostics.Debug.WriteLine("end sending response");

            System.Web.HttpContext.Current.Response.End();

            #endregion
        }

        public static void ExportPatientReportCampaign(DateTime dtFrom, DateTime dtTo, string sCampaignIDs, string sMarkets)
        {
            System.Diagnostics.Debug.WriteLine("Entered into ExportPatientReport");

            string strTemplateFilePath = string.Format("{0}{1}",
                    Utilities.AppSettings.ExcelTemplatePath,
                    Utilities.AppSettings.BackEndReporting_Patient_FileName);

            string strSaveFilePath = string.Empty;

            System.Diagnostics.Debug.WriteLine("start creating excel helper");

            using (ExcelHelper objEH = new ExcelHelper(strTemplateFilePath))
            {
                System.Diagnostics.Debug.WriteLine("Created excel helper");

                System.Diagnostics.Debug.WriteLine("fill dates");
                objEH.SetCellValue(cnstDateFrom, dtFrom);

                System.Diagnostics.Debug.WriteLine("fill users");
                _fillUsers_Campaign(cnstUsers, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill tracking");
                _fillTracking_Campaign(cnstTrackingReportCell, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllUsers");
                _fillReportedVisitsAllUsers_Campaign(cnstReportedVisitsAllUsers, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllUsersPercentage");
                _fillReportedVisitsAllUsersPercentage_Campaign(cnstReportedVisitsAllUsersPercentage, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill Upload method Report");
                _fillUploadMethodTracker_Campaign(cnstUploadMethodCell, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill Reported Uploads Report");
                _fillReportedUploads_Campaign(cnstRprtdUploadsCell, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill Gender cumulative report");
                _fillGenderCumulativeReport_Campaign(cnstGenderCumulativeCell, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill AgeBreakdownReport");
                _fillAgeBreakdownReport_Campaign(cnstAgeBrkdwnCell, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill Ethnicity Breakdown Report");
                _fillEthnicity_Campaign(cnstEthnBrkdwnCell, objEH, dtFrom, dtTo, sCampaignIDs, sMarkets);

                strSaveFilePath = string.Format("{0}AHA_BackEndReport_Patient_{1}.xls", Utilities.AppSettings.ReportTempPath, Guid.NewGuid());
                System.Diagnostics.Debug.WriteLine("start saving file");
                objEH.SaveAs(strSaveFilePath);
                System.Diagnostics.Debug.WriteLine("end saving file");
            }

            #region Sending Response
            System.Diagnostics.Debug.WriteLine("start sending response");
            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + strSaveFilePath + "\"");


            FileStream sourceFile = new FileStream(strSaveFilePath, FileMode.Open);

            long FileSize = sourceFile.Length;

            byte[] getContent = new byte[(int)FileSize];

            sourceFile.Read(getContent, 0, (int)sourceFile.Length);
            sourceFile.Close();

            System.Web.HttpContext.Current.Response.BinaryWrite(getContent);
            System.Diagnostics.Debug.WriteLine("end sending response");
            System.Web.HttpContext.Current.Response.End();

            #endregion
        }

        public static void ExportProviderReport(DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            System.Diagnostics.Debug.WriteLine("Entered into ExportProviderReport");

            string strTemplateFilePath = string.Format("{0}{1}",
                    Utilities.AppSettings.ExcelTemplatePath,
                    Utilities.AppSettings.BackEndReporting_Provider_FileName);

            string strSaveFilePath = string.Empty;
            System.Diagnostics.Debug.WriteLine("creating ExcelHelper");
            using (ExcelHelper objEH = new ExcelHelper(strTemplateFilePath))
            {
                System.Diagnostics.Debug.WriteLine("created ExcelHelper");
                //Fill the date header
                //Fill only the from date cell; rest will be managed by worksheet using formula
                System.Diagnostics.Debug.WriteLine("fill dates");
                objEH.SetCellValue(cnstDateFrom, dtFrom);

                System.Diagnostics.Debug.WriteLine("fill providers");
                _fillProviders(cnstProviders, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID );

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsNewProviders");
                _fillReportedVisitsNewProviders(cnstNewProviderVisits, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID );

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllProviders");
                // PJB - TODO
                _fillReportedVisitsAllProviders(cnstAllProviderVisits, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID );

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsNewProviders");
                _fillReportedVisitsNewProvidersPercentage(cnstNewProviderVisitsPercentage, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllProviders");
                _fillReportedVisitsAllProvidersPercentage(cnstAllProviderVisitsPercentage, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill PatientCounts");
                _fillPatientCounts(cnstPatientCount, objEH, dtFrom, dtTo, iCampaignID, sMarkets);

                System.Diagnostics.Debug.WriteLine("fill PatientProviderConnection");
                _fillPatientProviderConnection(cnstPatientConnectedToProviders, objEH, dtFrom, dtTo, iCampaignID, sMarkets);

                strSaveFilePath = string.Format("{0}AHA_BackEndReport_Provider_{1}.xls", Utilities.AppSettings.ReportTempPath, Guid.NewGuid());
                System.Diagnostics.Debug.WriteLine("start saving file");
                objEH.SaveAs(strSaveFilePath);
                System.Diagnostics.Debug.WriteLine("end saving file");
            }

            #region Sending Response
            System.Diagnostics.Debug.WriteLine("start sending response");
            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + strSaveFilePath + "\"");


            FileStream sourceFile = new FileStream(strSaveFilePath, FileMode.Open);

            long FileSize = sourceFile.Length;

            byte[] getContent = new byte[(int)FileSize];

            sourceFile.Read(getContent, 0, (int)sourceFile.Length);
            sourceFile.Close();

            System.Web.HttpContext.Current.Response.BinaryWrite(getContent);
            System.Diagnostics.Debug.WriteLine("end sending response");
            System.Web.HttpContext.Current.Response.End();

            #endregion
        }

        public static void ExportProviderReportCampaign(DateTime dtFrom, DateTime dtTo, int? iCampaignID, string sMarkets, int? iUserTypeID)
        {
            System.Diagnostics.Debug.WriteLine("Entered into ExportProviderReportCampaign");

            string strTemplateFilePath = string.Format("{0}{1}",
                    Utilities.AppSettings.ExcelTemplatePath,
                    Utilities.AppSettings.BackEndReporting_Provider_FileName);

            string strSaveFilePath = string.Empty;
            System.Diagnostics.Debug.WriteLine("creating ExcelHelper");
            using (ExcelHelper objEH = new ExcelHelper(strTemplateFilePath))
            {
                System.Diagnostics.Debug.WriteLine("created ExcelHelper");
                //Fill the date header
                //Fill only the from date cell; rest will be managed by worksheet using formula
                System.Diagnostics.Debug.WriteLine("fill dates");
                objEH.SetCellValue(cnstDateFrom, dtFrom);

                System.Diagnostics.Debug.WriteLine("fill providers");
                _fillProviders_Campaign(cnstProviders, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsNewProviders");
                _fillReportedVisitsNewProviders_Campaign(cnstNewProviderVisits, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllProviders");
                _fillReportedVisitsAllProviders_Campaign(cnstAllProviderVisits, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsNewProvidersPercentage");
                _fillReportedVisitsNewProvidersPercentage_Campaign(cnstNewProviderVisitsPercentage, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill ReportedVisitsAllProvidersPercentage");
                _fillReportedVisitsAllProvidersPercentage_Campaign(cnstAllProviderVisitsPercentage, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill PatientCounts");
                _fillPatientCounts_Campaign(cnstPatientCount, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                System.Diagnostics.Debug.WriteLine("fill PatientProviderConnection");
                _fillPatientProviderConnection_Campaign(cnstPatientConnectedToProviders, objEH, dtFrom, dtTo, iCampaignID, sMarkets, iUserTypeID);

                strSaveFilePath = string.Format("{0}AHA_BackEndReport_Provider_{1}.xls", Utilities.AppSettings.ReportTempPath, Guid.NewGuid());
                System.Diagnostics.Debug.WriteLine("start saving file");
                objEH.SaveAs(strSaveFilePath);
                System.Diagnostics.Debug.WriteLine("end saving file");
            }

            #region Sending Response
            System.Diagnostics.Debug.WriteLine("start sending response");
            System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + strSaveFilePath + "\"");

            FileStream sourceFile = new FileStream(strSaveFilePath, FileMode.Open);

            long FileSize = sourceFile.Length;

            byte[] getContent = new byte[(int)FileSize];

            sourceFile.Read(getContent, 0, (int)sourceFile.Length);
            sourceFile.Close();

            System.Web.HttpContext.Current.Response.BinaryWrite(getContent);
            System.Diagnostics.Debug.WriteLine("end sending response");
            System.Web.HttpContext.Current.Response.End();

            #endregion
        }

        public static void HideBar()
        {
            Page thePage = (Page)HttpContext.Current.Handler;
            // Get a reference to the master page
            MasterPage ctl00 = thePage.FindControl("ctl00") as MasterPage;

            //ContentPlaceHolder MainContent = ctl00.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;

            Control div = ctl00.FindControl("divProgressBarHider") as Control;
            div.Visible = false;
        }
    }
}