﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Admin.Utilities
{
    [Serializable]
    public class UserContext
    {
        public int UserID
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }


        public int RoleID
        {
            get;
            set;
        }

        public string RoleName
        {
            get;
            set;
        }

        public string LanguageLocale
        {
            get;
            set;
        }

        public int? LanguageID
        {
            get;
            set;
        }

        public int BusinessID   // only when user role is BusinessAdmin
        {
            get;
            set;
        }
    }
}
