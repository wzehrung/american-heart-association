﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XL = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Collections;

namespace Heart360Admin.Utilities
{
    public class ExcelHelper : IDisposable
    {
        #region Private Members

        private XL.Application m_xlApp = null;
        private XL.Workbook m_WorkBook = null;
        private object oMissing = Type.Missing;

        #endregion

        #region Properties

        public XL._Worksheet WorkSheet
        {
            get;
            internal set;
        }

        public XL.Range Cells
        {
            get;
            internal set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates new helper object
        /// </summary>
        /// <param name="strTemplatePath">Path of Template File; Null/Empty - No Template</param>
        public ExcelHelper(string strTemplatePath)
        {
            this.m_xlApp = new XL.Application();

            if (string.IsNullOrEmpty(strTemplatePath))
            {
                this.m_WorkBook = this.m_xlApp.Workbooks.Add(oMissing);
            }
            else
            {
                this.m_WorkBook = this.m_xlApp.Workbooks.Open(strTemplatePath,
                            oMissing, oMissing, oMissing, oMissing, oMissing,
                            oMissing, oMissing, oMissing, oMissing, oMissing,
                            oMissing, oMissing, oMissing, oMissing);
            }

            this.WorkSheet = this.m_WorkBook.Sheets[1] as XL._Worksheet;

            this.Cells = this.WorkSheet.Cells;
        }

        /// <summary>
        /// Saves the Workbook to the given file
        /// </summary>
        /// <param name="strFilePath">File name</param>
        public void SaveAs(string strFilePath)
        {
            this.m_WorkBook.SaveAs(strFilePath,
                XL.XlFileFormat.xlWorkbookNormal,
                oMissing, oMissing, oMissing, oMissing,
                XL.XlSaveAsAccessMode.xlNoChange, oMissing,
                oMissing, oMissing, oMissing, oMissing);
        }

        /// <summary>
        /// Closes the current handle of Workbook.
        /// </summary>
        public void Close()
        {
            if (this.m_WorkBook != null)
                this.m_WorkBook.Close(oMissing, oMissing, oMissing);
        }

        /// <summary>
        /// Set value of a cell
        /// </summary>
        /// <param name="strCell">Cell Name (Eg: "A1", "BC12" etc)</param>
        /// <param name="oValue">Value to be set</param>
        public void SetCellValue(string strCell, object oValue)
        {
            this.WorkSheet.get_Range(strCell, oMissing).set_Value(XL.XlRangeValueDataType.xlRangeValueDefault, oValue);
        }

        /// <summary>
        /// Set value of a row
        /// </summary>
        /// <typeparam name="T">Type of data to be filled</typeparam>
        /// <param name="strCell"></param>
        /// <param name="IEValues"></param>
        public void SetRowValue<T>(string strCell, IEnumerable<T> IEValues)
        {
            int iCnt = IEValues.Count();

            XL.Range range = this.WorkSheet.get_Range(strCell, oMissing);
            range = range.get_Resize(1, iCnt);

            range.set_Value(oMissing, IEValues.ToArray());
        }

        /// <summary>
        /// Sets value of a column
        /// </summary>
        /// <typeparam name="T">Type of the value</typeparam>
        /// <param name="strCell"></param>
        /// <param name="IEValues"></param>
        public void SetColumnValue<T>(string strCell, IEnumerable<T> IEValues)
        {
            int iCnt = IEValues.Count();

            XL.Range range = this.WorkSheet.get_Range(strCell, oMissing);
            range = range.get_Resize(iCnt, 1);

            T[,] arrValues = new T[iCnt, 1];

            int index = 0;

            foreach (T objValue in IEValues)
            {
                arrValues[index, 0] = objValue;
                index++;
            }

            range.set_Value(oMissing, arrValues);
        }

        /// <summary>
        /// Sets a n X n matrix data
        /// </summary>
        /// <param name="strCell">From Cell</param>
        /// <param name="dataValues">Metrix Data</param>
        public void SetMatrixValue(string strCell, DataTable dataValues)
        {
            int iRowCnt = dataValues.Rows.Count;
            int iColCnt = dataValues.Columns.Count;

            XL.Range range = this.WorkSheet.get_Range(strCell, oMissing);
            range = range.get_Resize(iRowCnt, iColCnt);

            object[,] objValues = new object[iRowCnt,iColCnt];
            

            int iRowIndex = 0;
            foreach (DataRow row in dataValues.Rows)
            {
                for (int iColIndex = 0; iColIndex < iColCnt; iColIndex++)
                {
                    objValues[iRowIndex, iColIndex] = row[iColIndex];
                }
                iRowIndex++;
            }

            range.set_Value(oMissing, objValues);
        }

        /// <summary>
        /// Sets a n X n matrix data
        /// PJB: added logic to handle extra sub-item column that shouldn't be displayed.
        /// </summary>
        /// <param name="strCell">From Cell</param>
        /// <param name="dataValues">Metrix Data</param>
        public void SetMatrixValue_RollUp(string strCell, DataTable dataValues)
        {
            int iRowCnt = dataValues.Rows.Count;
            int iColCnt = dataValues.Columns.Count;

            XL.Range range = this.WorkSheet.get_Range(strCell, oMissing);
            range = range.get_Resize(iRowCnt, iColCnt-1);   // PJB: ignore one column

            object[,] objValues = new object[iRowCnt, iColCnt-1];   // PJB: ignore one column


            int iRowIndex = 0;
            foreach (DataRow row in dataValues.Rows)
            {
                for (int iColIndex = 0; iColIndex < iColCnt; iColIndex++)
                {
                    if (iColIndex == 0)
                    {
                        objValues[iRowIndex, 0] = (row[1].ToString().Equals("All")) ? row[0] : row[1];
                    }
                    else if (iColIndex >1)   // ignore sub-item description
                    {
                        objValues[iRowIndex, iColIndex-1] = row[iColIndex];
                    }
                }
                iRowIndex++;
            }

            range.set_Value(oMissing, objValues);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            this.Close();

            System.Runtime.InteropServices.Marshal.ReleaseComObject(this.m_xlApp);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(this.m_WorkBook);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(this.WorkSheet);

            GC.Collect();
        }

        #endregion
    }
}
