﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Admin.Utilities
{
    public class SessionInfo
    {
        private const string sessionKeyUserContext = "UserContext";
        private const string sessionPatientResourcePreview = "patientresourcepreview";
        private const string sessionProviderResourcePreview = "providerresoucepreview";
        private const string sessionPatientGrantPreview = "patientgrantpreview";
        private const string sessionProviderGrantPreview = "providergrantpreview";
        private const string sessionCampaignPreview = "campaignpreview";

        private const string sessionPatientHelpfulResourcePreview = "patienthelpfulresourcepreview";
        private const string sessionPatientTipPreview = "patienttippreview";
        private const string sessionProviderTipPreview = "providertippreview";
        private const string sessionProviderAnnouncementPreview = "providerannouncementpreview";

        internal static bool IsAdminLoggedin()
        {

            if (System.Web.HttpContext.Current.Session[sessionKeyUserContext] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        internal static void SetCurrentLoggedinUser(int intUserID, string strUserName, string strName, int intRoleID, string strRoleName, int? iLanguageID, string strLanguageLocale, int intBusinessID )
        {
            System.Web.HttpContext.Current.Session[sessionKeyUserContext] = new UserContext {   UserID = intUserID, 
                                                                                                UserName = strUserName, 
                                                                                                Name = strName, 
                                                                                                RoleID = intRoleID, 
                                                                                                RoleName = strRoleName, 
                                                                                                LanguageID = iLanguageID, 
                                                                                                LanguageLocale = strLanguageLocale,
                                                                                                BusinessID = intBusinessID };
        }

        internal static UserContext GetUserContext()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionKeyUserContext] != null
                )
            {
                return (UserContext)System.Web.HttpContext.Current.Session[sessionKeyUserContext];
            }
            return null;
        }

        internal static void ResetCurrentLoggedinUser()
        {
            System.Web.HttpContext.Current.Session.Abandon();
        }

        internal static void SetPatientResourcePreviewToSession(byte[] fileBytes)
        {
            System.Web.HttpContext.Current.Session[sessionPatientResourcePreview] = fileBytes;
        }

        internal static byte[] GetPatientResourcePreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionPatientResourcePreview] != null)
            {
                return (byte[])System.Web.HttpContext.Current.Session[sessionPatientResourcePreview];
            }
            return null;
        }

        internal static void ResetPatientResourcePreviewSession()
        {
            System.Web.HttpContext.Current.Session[sessionPatientResourcePreview] = null;
        }

        internal static void SetPatientHelpfulResourcePreviewToSession(HelpfulResource obj)
        {
            System.Web.HttpContext.Current.Session[sessionPatientHelpfulResourcePreview] = obj;
        }

        internal static void ResetPatientHelpfulResourcePreviewToSession()
        {
            System.Web.HttpContext.Current.Session[sessionPatientHelpfulResourcePreview] = null;
        }

        internal static HelpfulResource GetPatientHelpfulResourcePreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionPatientHelpfulResourcePreview] != null)
            {
                return System.Web.HttpContext.Current.Session[sessionPatientHelpfulResourcePreview] as HelpfulResource;
            }
            return null;
        }

        internal static void SetPatientTipofthedayToSession(string tip)
        {
            System.Web.HttpContext.Current.Session[sessionPatientTipPreview] = tip;
        }

        internal static void ResetPatientTipofthedayToSession()
        {
            System.Web.HttpContext.Current.Session[sessionPatientTipPreview] = null;
        }

        internal static string GetPatientTipOftheDayFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionPatientTipPreview] != null)
            {
                return System.Web.HttpContext.Current.Session[sessionPatientTipPreview] as string;
            }
            return null;
        }

        internal static void SetProviderTipofthedayToSession(string tip)
        {
            System.Web.HttpContext.Current.Session[sessionProviderTipPreview] = tip;
        }

        internal static void ResetProviderTipofthedayToSession()
        {
            System.Web.HttpContext.Current.Session[sessionProviderTipPreview] = null;
        }

        internal static string GetProviderTipOftheDayFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionProviderTipPreview] != null)
            {
                return System.Web.HttpContext.Current.Session[sessionProviderTipPreview] as string;
            }
            return null;
        }

        internal static void SetProviderAnnouncementPreviewToSession(Announcement obj)
        {
            System.Web.HttpContext.Current.Session[sessionProviderAnnouncementPreview] = obj;
        }

        internal static void ResetProviderAnnouncementPreviewToSession()
        {
            System.Web.HttpContext.Current.Session[sessionProviderAnnouncementPreview] = null;
        }

        internal static Announcement GetProviderAnnouncementPreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionProviderAnnouncementPreview] != null)
            {
                return System.Web.HttpContext.Current.Session[sessionProviderAnnouncementPreview] as Announcement;
            }
            return null;
        }

        internal static void SetProviderResourcePreviewToSession(byte[] fileBytes)
        {
            System.Web.HttpContext.Current.Session[sessionProviderResourcePreview] = fileBytes;
        }

        internal static byte[] GetProviderResourcePreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionProviderResourcePreview] != null)
            {
                return (byte[])System.Web.HttpContext.Current.Session[sessionProviderResourcePreview];
            }
            return null;
        }

        internal static void ResetProviderResourcePreviewSession()
        {
            System.Web.HttpContext.Current.Session[sessionProviderResourcePreview] = null;
        }

        internal static void SetPatientGrantPreviewToSession(Utilities.Utility.GrantLogo objGrantLogo)
        {
            System.Web.HttpContext.Current.Session[sessionPatientGrantPreview] = objGrantLogo;
        }

        internal static Utilities.Utility.GrantLogo GetPatientGrantPreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionPatientGrantPreview] != null)
            {
                return (Utilities.Utility.GrantLogo)System.Web.HttpContext.Current.Session[sessionPatientGrantPreview];
            }
            return null;
        }

        internal static void ResetPatientGrantPreviewSession()
        {
            System.Web.HttpContext.Current.Session[sessionPatientGrantPreview] = null;
        }

        internal static void SetProviderGrantPreviewToSession(Utilities.Utility.GrantLogo objGrantLogo)
        {
            System.Web.HttpContext.Current.Session[sessionProviderGrantPreview] = objGrantLogo;
        }

        internal static Utilities.Utility.GrantLogo GetProviderGrantPreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionProviderGrantPreview] != null)
            {
                return (Utilities.Utility.GrantLogo)System.Web.HttpContext.Current.Session[sessionProviderGrantPreview];
            }
            return null;
        }

        internal static void ResetProviderGrantPreviewSession()
        {
            System.Web.HttpContext.Current.Session[sessionProviderGrantPreview] = null;
        }

        internal static void SetCampaignPreviewToSession(AHAHelpContent.Campaign objCampaign)
        {
            System.Web.HttpContext.Current.Session[sessionCampaignPreview] = objCampaign;
        }

        internal static AHAHelpContent.Campaign GetCampaignPreviewFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null &&
                System.Web.HttpContext.Current.Session[sessionCampaignPreview] != null)
            {
                return (AHAHelpContent.Campaign)System.Web.HttpContext.Current.Session[sessionCampaignPreview];
            }
            return null;
        }

        internal static void ResetCampaignPreviewSession()
        {
            System.Web.HttpContext.Current.Session[sessionCampaignPreview] = null;
        }
    }
}
