﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GRCBase;

namespace Heart360Admin.Utilities
{
    public class AppSettings
    {
        /// <summary>
        /// This function is used to know if IsSSLEnabled is enabled or not.
        /// </summary>
        public static bool IsSSLEnabled
        {
            get
            {
                return ConfigReader.GetValue("IsSSLEnabled") == "1" ? true : false;
            }
        }

        /// <summary>
        /// SiteUrl
        /// </summary>
        public static bool SiteUrl
        {
            get
            {
                return ConfigReader.GetValue("SiteUrl") == "1" ? true : false;
            }
        }

        public static string ExcelTemplatePath
        {
            get
            {
                return ConfigReader.GetValue("ExcelTemplatePath");
            }
        }

        public static string BackEndReporting_Patient_FileName
        {
            get
            {
                return ConfigReader.GetValue("BackEndReporting_Patient_FileName");
            }
        }

        public static string BackEndReporting_Patient_Campaign_FileName
        {
            get
            {
                return ConfigReader.GetValue("BackEndReporting_Patient_Campaign_FileName");
            }
        }

        public static string BackEndReporting_Provider_FileName
        {
            get
            {
                return ConfigReader.GetValue("BackEndReporting_Provider_FileName");
            }
        }

        public static string BackEndReporting_Provider_Campaign_FileName
        {
            get
            {
                return ConfigReader.GetValue("BackEndReporting_Provider_Campaign_FileName");
            }
        }

        public static string ReportTempPath
        {
            get
            {
                return ConfigReader.GetValue("ReportTempPath");
            }
        }

        public static string ReportPeriod
        {
            get
            {
                return ConfigReader.GetValue("ReportPeriod");
            }
        }
    }
}
