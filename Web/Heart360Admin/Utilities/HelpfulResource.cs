﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Admin.Utilities
{
    [Serializable]
    public class HelpfulResource
    {
        public string URL
        {
            get;
            set;
        }

        public string LinkTitle
        {
            get;
            set;
        }

        public HelpfulResource(string url, string linktitle)
        {
            this.URL = url;
            this.LinkTitle = linktitle;
        }
    }

    [Serializable]
    public class Announcement
    {
        public string Title
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public Announcement(string title, string text)
        {
            this.Title = title;
            this.Text = text;
        }
    }
}
