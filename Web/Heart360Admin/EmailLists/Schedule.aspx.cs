﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;
using System.Security.AccessControl;
using System.IO;
using GRCBase;

namespace Heart360Admin.EmailLists
{
    public class UserTypeItem
    {
        public string UserTypeCode
        {
            get;
            set;
        }

        public string UserType
        {
            get;
            set;
        }
    }

    public partial class Schedule : System.Web.UI.Page
    {
        public const string STR_ALL_USERS = "None";
        public const string STR_ALL_CAMPAIGNS = "All Campaigns";
        public const string STR_ALL_AFFILIATE_REGIONS = "All Affiliate Regions";
        public const string STR_ALL_MARKETS = "All Markets";
        public const string STR_NONE = "None";

        public const int ID_NONE = -1;
        public const int ID_ALL = 0;
        public const int DIRECTOR_ROLE_ID = 3;


        public int? CampaignID
        {
            get
            {
                if (!string.IsNullOrEmpty(ddlCampaign.SelectedValue))
                {
                    int ic = Convert.ToInt32(ddlCampaign.SelectedValue);
                    if (ic == 0)
                        return null;
                    return ic;
                }

                return null;
            }
        }

        public string AffiliateNames
        {
            get
            {
                // Do not use the id of affiliate, use its name (because of ZipRegionTable) 
                if (ddlAffiliate.SelectedItem == null || string.Compare(ddlAffiliate.SelectedItem.Text, STR_ALL_AFFILIATE_REGIONS) == 0)
                    return string.Empty;
                if (string.Compare(ddlAffiliate.SelectedItem.Text, STR_NONE) == 0)
                    return string.Empty;
                
                return ddlAffiliate.SelectedItem.Text;
                //                return (string.Compare(ddlAffiliate.SelectedItem.Text, strAffiliatesAll) == 0) ? strAffiliatesAll : ddlAffiliate.SelectedValue;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _LoadDropdowns();
        }

        private void _LoadEmailSchedulerList()
        {
            Utilities.UserContext   uc = Utilities.SessionInfo.GetUserContext();
            bool                    providersOnly = (uc.RoleID == DIRECTOR_ROLE_ID) ? true : false ;

            RepScheduler.DataSource = EmailScheduler.GetAllEmailListSchedulersForDate(null, Heart360Admin.Utilities.Utility.GetCurrentLanguageID(), providersOnly);
            RepScheduler.DataBind();
        }

        private void _LoadDropdowns()
        {
            Utilities.UserContext uc = Utilities.SessionInfo.GetUserContext();

            List<AHAHelpContent.Campaign> list = AHAHelpContent.Campaign.FindAllCampaignsInLast2Years(Utilities.Utility.GetCurrentLanguageID());
//            list.Insert(0, new AHAHelpContent.Campaign { Title = STR_ALL_CAMPAIGNS, CampaignID = ID_ALL });
//            list.Insert(0, new AHAHelpContent.Campaign { Title = STR_NONE, CampaignID = ID_NONE });
            ddlCampaign.DataSource = list;
            ddlCampaign.DataTextField = "Title";
            ddlCampaign.DataValueField = "CampaignID";
            ddlCampaign.DataBind();

            ddlCampaign.Items.Insert(0, new System.Web.UI.WebControls.ListItem(STR_ALL_CAMPAIGNS, ID_ALL.ToString()));
            ddlCampaign.Items.Insert(0, new System.Web.UI.WebControls.ListItem(STR_NONE, ID_NONE.ToString()));

            ddlCampaign.SelectedIndex = 1;  // All Campaigns

            _LoadAffiliateDropdown();
            _LoadMarketDropdown();


            // We need to list 
            List<UserTypeItem> listUserType = new List<UserTypeItem>();
            
            listUserType.Add(new UserTypeItem { UserTypeCode = "1", UserType = "Provider" });
            listUserType.Add(new UserTypeItem { UserTypeCode = "2", UserType = "Volunteer" });
            if (uc.RoleID != DIRECTOR_ROLE_ID)
            {
                listUserType.Add(new UserTypeItem { UserTypeCode = "3", UserType = "Patient/Participant" });
            }

            ddlUserType.DataSource = listUserType;
            ddlUserType.DataTextField = "UserType";
            ddlUserType.DataValueField = "UserTypeCode";
            ddlUserType.DataBind();

            // Make the first element in the drop down the current one
            ddlUserType.SelectedIndex = 0;
        }

        private void _LoadAffiliateDropdown()
        {
            int? cid = this.CampaignID;
            DataSet ds = AHAHelpContent.PatientReport.GetAffiliatesForCampaignsReport( (cid == ID_NONE) ? null : cid );

            ddlAffiliate.DataSource = ds;
            ddlAffiliate.DataTextField = "AffiliateName";
            ddlAffiliate.DataValueField = "AffiliateID";
            ddlAffiliate.DataBind();
            if (ddlAffiliate.Items.Count > 1)
                ddlAffiliate.Items.Insert(0, new System.Web.UI.WebControls.ListItem(STR_ALL_AFFILIATE_REGIONS, ID_ALL.ToString()));

            ddlAffiliate.Items.Insert(0, new System.Web.UI.WebControls.ListItem(STR_NONE, ID_NONE.ToString()));
        }

        private void _LoadMarketDropdown()
        {
            DataSet ds = AHAHelpContent.PatientReport.GetMarketsForAffiliateReport(this.AffiliateNames);

            ddlMarket.DataSource = ds;
            ddlMarket.DataTextField = "MarketName";
            ddlMarket.DataValueField = "MarketId";
            ddlMarket.DataBind();
            if (ddlMarket.Items.Count > 1)
                ddlMarket.Items.Insert(0, new System.Web.UI.WebControls.ListItem(STR_ALL_MARKETS, ID_ALL.ToString()));

            ddlMarket.Items.Insert(0, new System.Web.UI.WebControls.ListItem(STR_NONE, ID_NONE.ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dtScheduledDate.Date = DateTime.Now.AddDays(1);
                performWidgetVisibility();
            }
            _LoadEmailSchedulerList();
        }

        protected void OnSchedule(object sender, EventArgs e)
        {
            Page.Validate("EmailScheduler");

            if (!Page.IsValid)
                return;
            // TODO: ------------------------------------- ddlUserType.SelectedValue
            AHAHelpContent.EmailScheduler.CreateEmailListScheduler(CampaignID, dtScheduledDate.Date.Value, ddlUserType.SelectedValue == "L" ? true : false);

            Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
        }

        protected void OnExcelExport(object sender, EventArgs e)
        {
            Page.Validate("EmailScheduler");

            if (!Page.IsValid)
                return;

            // TODO: implement a method to export this type of report.
            //Utilities.ExportToExcel.ExportProviderReportCampaign(StartDate, EndDate, CampaignIDs, UserTypeID, PartnerID);

            //Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
        }

        protected void OnDelete(object sender, EventArgs e)
        {
            DBContext dbContext = null;
            bool bException = false;
            try
            {
                dbContext = DBContext.GetDBContext();
                LinkButton linkDelete = sender as LinkButton;
                EmailScheduler objEmailScheduler = EmailScheduler.FindEmailSchedulerByID(Convert.ToInt32(linkDelete.CommandArgument));

                EmailScheduler.DeleteEmailListScheduler(Convert.ToInt32(linkDelete.CommandArgument));

                string strFolderName = string.Format("{0}-{1}-{2}", objEmailScheduler.ScheduledDate.Year, objEmailScheduler.ScheduledDate.Month, objEmailScheduler.ScheduledDate.Day);
                string strFileName = AHACommon.Helper.GetFileNameForEmailList(objEmailScheduler.IsProviderList, objEmailScheduler.SchedulerID, objEmailScheduler.Title);

                string strDirPath = string.Format("{0}\\{1}", AHACommon.AHAAppSettings.EmailListDownloadPath, strFolderName);
                string strFilePath = string.Format("{0}\\{1}.csv", strDirPath, strFileName);

                if (AHACommon.Helper.DoesCurrentUserHaveFullAccess(true, strFilePath))
                {
                    if (File.Exists(strFilePath))
                    {
                        File.Delete(strFilePath);
                    }
                }
            }
            catch
            {
                bException = true;
                throw;
            }
            finally
            {
                try
                {
                    if (bException)
                    {
                        if (dbContext != null)
                        {
                            dbContext.ReleaseDBContext(false);
                        }
                    }
                    else
                    {
                        dbContext.ReleaseDBContext(true);
                    }
                }
                catch
                {
                    if (dbContext != null)
                    {
                        dbContext.ReleaseDBContext(false);
                    }
                    throw;
                }
            }
            Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
        }

        protected void OnDownload(object sender, EventArgs e)
        {
            LinkButton linkDownload = sender as LinkButton;

            EmailScheduler objEmailScheduler = EmailScheduler.FindEmailSchedulerByID(Convert.ToInt32(linkDownload.CommandArgument));

            string strFolderName = string.Format("{0}-{1}-{2}", objEmailScheduler.ScheduledDate.Year, objEmailScheduler.ScheduledDate.Month, objEmailScheduler.ScheduledDate.Day);
            string strFileName = AHACommon.Helper.GetFileNameForEmailList(objEmailScheduler.IsProviderList, objEmailScheduler.SchedulerID, objEmailScheduler.Title);

            string strDirPath = string.Format("{0}\\{1}", AHACommon.AHAAppSettings.EmailListDownloadPath, strFolderName);
            string strFilePath = string.Format("{0}\\{1}.csv", strDirPath, strFileName);

            if (AHACommon.Helper.DoesCurrentUserHaveFullAccess(true, strFilePath))
            {
                if (File.Exists(strFilePath))
                {
                    Response.Clear();
                    Response.ContentType = "application/csv";
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.csv", strFileName));

                    FileStream sourceFile = new FileStream(strFilePath, FileMode.Open);
                    long FileSize;
                    FileSize = sourceFile.Length;
                    byte[] getContent = new byte[(int)FileSize];
                    sourceFile.Read(getContent, 0, (int)sourceFile.Length);
                    sourceFile.Close();

                    Response.BinaryWrite(getContent);
                    Response.End();
                }
            }

            Response.Write(string.Format("{0}\\{1} - {2}", Environment.UserDomainName, Environment.UserName, strFilePath));
        }


        protected void OnSchedulerDataBind(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                EmailScheduler objEmailScheduler = e.Item.DataItem as EmailScheduler;

                Literal litDate = e.Item.FindControl("litDate") as Literal;
                Literal litTitle = e.Item.FindControl("litTitle") as Literal;
                Literal litUserType = e.Item.FindControl("litUserType") as Literal;
                LinkButton linkDownload = e.Item.FindControl("linkDownload") as LinkButton;
                LinkButton linkDelete = e.Item.FindControl("linkDelete") as LinkButton;

                litTitle.Text = string.IsNullOrEmpty(objEmailScheduler.Title) ? STR_NONE : objEmailScheduler.Title;
                litDate.Text = GRCBase.DateTimeHelper.GetFormattedDateTimeString(objEmailScheduler.ScheduledDate, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.SLASH, GRCBase.GRCTimeFormat.NONE);
                linkDownload.CommandArgument = objEmailScheduler.SchedulerID.ToString();
                linkDelete.CommandArgument = objEmailScheduler.SchedulerID.ToString();
                if (!objEmailScheduler.IsDownloaded)
                {
                    linkDownload.Enabled = false;
                }
                else
                {
                    linkDownload.Enabled = true;
                }

                litUserType.Text = objEmailScheduler.IsProviderList ? "Provider" : "Patient";
            }
        }

        protected void OnValidateScheduler(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
                    // TODO: -------------------------------------------------------------------------------------------------------- TODO
            bool bRetVal = AHAHelpContent.EmailScheduler.DoesEmailSchedulerExistForCampaignIDAndDate(CampaignID, dtScheduledDate.Date.Value, ddlUserType.SelectedValue == "L" ? true : false);
            if (bRetVal)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = "A download process of this type is already scheduled for this date.";
                return;
            }
            return;
        }


        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            performWidgetVisibility();
        }

        private void performWidgetVisibility()
        {
            bool isPatient = (string.Compare(ddlUserType.SelectedItem.Value, "3") == 0) ? true : false;
/**

            ddlCampaign.Enabled = (!isPatient) ? true : false;

            if (isPatient)
            {
                linkExportToExcel.Visible = false;
            }
            else
            {
                linkExportToExcel.Visible = true;
            }
 */
        }

        public void ddlCampaign_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlAffiliate.Items.Clear();
            ddlMarket.Items.Clear();


            _LoadAffiliateDropdown();
            _LoadMarketDropdown();
        }

        public void ddlAffiliate_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            // If an affiliate is selected, this will change what goes in the Markets dropdown
            ddlMarket.Items.Clear();
            _LoadMarketDropdown();
        }
        
    }
}
