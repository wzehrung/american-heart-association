﻿<%@ Page MasterPageFile="~/MasterPages/Admin.Master" Language="C#" AutoEventWireup="true"
    CodeBehind="Schedule.aspx.cs" Inherits="Heart360Admin.EmailLists.Schedule" %>

<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="copyBox report">
        <h1>
            <%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%>
            Email Lists</h1>
        <em>To download the email list, click on the download link</em>
        <br />
        <br />
        <div class="report-filter">
            <table>
                <tr>
                    <td>
                        <h2>
                            Schedule an Email list download.
                        </h2>
                        <br />
                        (<em>To schedule the download of emails for all users, select 'None' from the campaign dropdown
                            list below.</em>)
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="validation_summary">
                            <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                                runat="server" ID="validSummarycreate" ValidationGroup="EmailScheduler" Enabled="true"
                                ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- Treat the region of the report controls as a separate update panel -->
                        <!-- Note that there is already an asp:ScriptManager in the MasterPage -->
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="4" style="background-color: #F0F0F0; border: 1px solid #E0E0E0; padding: 4px;">
                                <tr>
                                    <td style="text-align: right;" width="10%">Date</td>
                                    <td style="text-align: left;" >
                                        <GRC:GRCDatePicker ID="dtScheduledDate" runat="server" CBReadOnly="true" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td width="10%"></td>
                                    <td style="text-align: right;">Campaign</td>
                                    <td>
                                        <asp:DropDownList ID="ddlCampaign" Width="200" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCampaign_OnSelectedIndexChanged"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="5"></td>
                                    <!-- ### BEGIN AFFILIATE ### -->
                                    <td style="text-align: right;">Affiliate Region</td>

                                    <td>
                                        <asp:DropDownList ID="ddlAffiliate" runat="server" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ddlAffiliate_OnSelectedIndexChanged" />
                                    </td>
                                    <!-- ### END MARKET ### -->
                                </tr>

                                <tr>
                                    <td colspan="5"></td>
                                    <!-- ### BEGIN MARKET ### -->
                                    <td style="text-align: right;">Market</td>

                                    <td>
                                        <asp:DropDownList ID="ddlMarket" runat="server" Width="200" />
                                    </td>
                                    <!-- ### END MARKET ### -->
                                </tr>

                                <tr>
                                    <td colspan="5"></td>
                                    <td style="text-align: right;">User Type</td>

                                    <td>
                                        <asp:DropDownList
                                            ID="ddlUserType"
                                            runat="server"
                                            Width="200">
                                            <asp:ListItem Value="0">Patient/Participant</asp:ListItem>
                                            <asp:ListItem Value="1">Provider</asp:ListItem>
                                            <asp:ListItem Value="2">Volunteer</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%" border="0" cellspacing="0" cellpadding="4">

                                <tr>
                                    <td style="text-align: right;">
                                        <asp:LinkButton ID="linkSchedule" CssClass="aha-small-button with-arrow" runat="server" OnClick="OnSchedule">
                                            <strong><em>Schedule</em></strong>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="linkExportToExcel" CssClass="aha-small-button with-arrow" runat="server" OnClick="OnExcelExport">
                                            <strong><em>Export to Excel</em></strong>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>


                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:CustomValidator ValidationGroup="EmailScheduler" EnableClientScript="false"
                            ID="CustomValidator2" runat="server" Display="None" OnServerValidate="OnValidateScheduler">
                        </asp:CustomValidator>
                    </td>
                </tr>
            </table>
            <br /><br />
            <asp:Repeater ID="RepScheduler" runat="server" OnItemDataBound="OnSchedulerDataBind">
                <HeaderTemplate>
                    <table cellpadding="0" cellspacing="0" width="100%" class="provider-grid">
                        <tr>
                            <th class="alignleft">
                                SCHEDULED DATE
                            </th>
                            <th class="alignleft">
                                CAMPAIGN NAME
                            </th>
                             <th class="alignleft">
                                USER TYPE
                            </th>
                            <th class="alignleft">
                                ACTIONS
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Literal ID="litDate" runat="server" />
                        </td>
                        <td>
                            <asp:Literal ID="litTitle" runat="server" />
                        </td>
                          <td>
                            <asp:Literal ID="litUserType" runat="server" />
                        </td>
                        <td>
                            <asp:LinkButton ID="linkDownload" runat="server" OnClick="OnDownload">Download</asp:LinkButton>
                        |
                            <asp:LinkButton ID="linkDelete" runat="server" OnClick="OnDelete">Delete</asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
