﻿<%@ Page MasterPageFile="~/MasterPages/Admin.Master" Language="C#" AutoEventWireup="true"
    CodeBehind="Error.aspx.cs" Inherits="Heart360Admin.Error" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="content" runat="server">
        <div class="copyBox">
            <h1>
                ERROR!</h1>
            <p>
                Oops! We are sorry. There is a problem with the page that you are trying to use.</p>
            <p>
                You can try to <strong><a href="<%=Request["Source"]%>">reload the page</a></strong> or you can go
                to the <strong><a id="A1" href="~/" runat="server">home page</a></strong>.</p>
            <p>
                If the problem persists, please <a href="mailto:<%=AHACommon.AHAAppSettings.SupportEmail%>">
                    contact support</a> and describe the problem<br />
                in detail, including any technical error information that may appear below and<br />
                what you were doing when the problem occurred.</p>
            <p>
                Reference Number:
                <asp:Literal runat="server" ID="litErrorID2"></asp:Literal></p>
            <div>
                <strong>TECHNICAL DETAILS</strong> (please copy/paste into your Support Request)
                <p>
                    &nbsp;</p>
                <p>
                    <asp:Literal ID="LitTechnicalDetails" runat="server"></asp:Literal>
                </p>
                <p>
                    <asp:Literal runat="server" ID="LitCompleteDetails"></asp:Literal>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
