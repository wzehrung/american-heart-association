﻿<%@ Page MasterPageFile="~/MasterPages/Admin.Master" Language="C#" AutoEventWireup="true"
    CodeBehind="FileNotFound.aspx.cs" Inherits="Heart360Admin.FileNotFound" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    <div id="content" runat="server">
        <div class="copyBox">
            <h1>
                FILE NOT FOUND!</h1>
            <p>
                Oops! We are sorry. The page you are looking for cannot be found.</p>
            <p>
                You can try to <a href="<%=Request["Source"]%>">reload the page</a> or you can go
                back to the <a href="~/" runat="server">home page</a>.</p>
            <p>
                If the problem persists, please <a href="mailto:<%=AHACommon.AHAAppSettings.SupportEmail%>">
                    contact support</a> and describe the problem<br />
                in detail, including any technical error information that may appear below and<br />
                what you were doing when the problem occurred.</p>
            <p>
                ERROR CODE: HTTP 404
                <br />
            </p>
        </div>
    </div>
</asp:Content>
