﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GRCBase;

namespace Heart360Admin.MasterPages
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Utilities.Utility.IsAdminLoggedin())
            //{
            //    if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/home.aspx"))
            //    {
            //        Response.Redirect("~/Login.aspx?ReturnURL="+Server.UrlEncode(HttpContext.Current.Request.RawUrl));
            //        return;
            //    }
            //    Response.Redirect("~/Login.aspx");
            //}

            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/home.aspx"))
                {
                    Response.Redirect("~/Login.aspx?ReturnURL=" + Server.UrlEncode(HttpContext.Current.Request.RawUrl));
                    return;
                }
                Response.Redirect("~/Login.aspx");
            }

            AdminPage objPage = PageInfo.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);
            if (objPage != null)
            {
                Page.Title = objPage.PageTitle;
            }
        }
    }
}
