﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignReport.ascx.cs" Inherits="Heart360Admin.Reports.Ctrls.CampaignReport" %>

<h1><%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%> <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Patient_Campaign_Page_Title")%></h1>

<div id="reportSpecification" runat="server" class="report-filter">

    <ul style="margin-left: -20px;">
        <li>Please select a date range of at least 1 month.</li>
        <li>Report displays data horizontally by month. Scroll right and left to view all data, if applicable.</li>
        <li><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Report_Duration")%></li>
    </ul>

    <div class="clearer"></div>
    
    <asp:UpdatePanel ID="upReportHeader" runat="server">
        <ContentTemplate>

            <table width="100%" border="0" cellspacing="0" cellpadding="4" style="background-color: #F0F0F0; border: 1px solid #E0E0E0; padding: 4px;">

                <tr>
                    <!-- ### BEGIN START DATE ### -->
                    <td style="text-align: right;" nowrap>Start Date<span class="red">*</span></td>
                    <td style="text-align: right; width: 90px;">Month</td>
                    <td style="text-align: left;">
                        <asp:DropDownList EnableViewState="false" ID="ddlMonthStart" runat="server" Width="100" AutoPostBack="true">
                            <asp:ListItem Text="Jan" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                            <asp:ListItem Text="May" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                            <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                            <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                            <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                            <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                            <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                            <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                        </asp:DropDownList>   
                    </td>
                    <td style="text-align: right; width: 90px">Year</td>
                    <td style="text-align: left;"><asp:DropDownList EnableViewState="false" ID="ddlYearStart" runat="server" Width="100" AutoPostBack="true"></asp:DropDownList></td>
                    <!-- ### END START DATE ### -->

                    <!-- ### BEGIN CAMPAIGN ### -->
                    <td style="text-align: right; width: 450px;">Campaign</td>
                    <td>
                        <asp:DropDownList
                            ID="ddlCampaign"
                            runat="server"
                            OnSelectedIndexChanged="ddlCampaign_OnSelectedIndexChanged"
                            AutoPostBack="true" Width="200">
                        </asp:DropDownList>
                    </td>
                    <!-- ### END CAMPAIGN ### -->
                </tr>

                <tr>
                    <!-- ### BEGIN END DATE ### -->
                    <td style="text-align: right;">End Date<span class="red">*</span></td>
                    <td style="text-align: right;">Month</td>
                    <td style="text-align: left;">
                        <asp:DropDownList EnableViewState="false" ID="ddlMonthEnd" runat="server" Width="100" AutoPostBack="true">
                            <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Feb" Value="2" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                            <asp:ListItem Text="May" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                            <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                            <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                            <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                            <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                            <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                            <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                        </asp:DropDownList>   
                    </td>
                    <td style="text-align: right;">Year</td>
                    <td style="text-align: left;"><asp:DropDownList EnableViewState="false" ID="ddlYearEnd" runat="server" Width="100" AutoPostBack="true"></asp:DropDownList></td>
                    <!-- ### END END DATE ### -->

                    <!-- ### BEGIN AFFILIATE REGION ### -->
                    <td style="text-align: right; width: 450px;">Affiliate Region</td>
                    <td>
                        <asp:DropDownList
                            ID="ddlAffiliate"
                            runat="server"
                            AutoPostBack="true"
                            Width="200"
                            OnSelectedIndexChanged="ddlAffiliate_OnSelectedIndexChanged">
                        </asp:DropDownList>

                    </td>
                    <!-- ### END AFFILIATE REGION ### -->
        </tr>

                <tr>
                    <td colspan="5"></td>
                    <!-- ### BEGIN Market ### -->
                    <td style="text-align: right;">Market</td>

                    <td>
                        <asp:DropDownList
                            ID="ddlMarket"
                            runat="server"
                            AutoPostBack="true"
                            Width="200">
                        </asp:DropDownList>
                    </td>
                    <!-- ### END MARKET ### -->
                </tr>

                <tr>
                    <td>
                        <asp:LinkButton ID="btnReset" CssClass="aha-small-button with-arrow" runat="server" onclick="btnReset_Click"><strong><em>Reset</em></strong></asp:LinkButton>
                    </td>
                    <td colspan="5" style="text-align: right;">
                        <asp:UpdateProgress ID="upProgress" AssociatedUpdatePanelID="upReportHeader" runat="server">
                            <ProgressTemplate>
                                Updating Filter...
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td align="right">
                        <asp:CheckBox ID="cbOnlyZipcodes" runat="server" Text="Only those with zipcodes" OnCheckedChanged="cbOnlyZipcodes_Changed" AutoPostBack="true" Checked="false" />
                    </td>
                </tr>

            </table>

        </ContentTemplate>

    </asp:UpdatePanel>

    <table width="100%" border="0" cellspacing="0" cellpadding="4">

        <tr>
            <td style="text-align: right; vertical-align: bottom;">
                <asp:LinkButton ID="btnSubmit" runat="server" Enabled="false" OnClick="OnApply"><strong><em>Submit</em></strong></asp:LinkButton>
                <asp:LinkButton ID="btnExcel" CssClass="aha-small-button with-arrow" runat="server" OnClick="OnExcelExport"><strong><em>Export to Excel</em></strong></asp:LinkButton><br />
                <asp:Label ID="lblDateValidationMsg" Visible="false" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>

    </table>

    <asp:HiddenField ID="hidReset" EnableViewState="false" runat="server" />



</div>

<div id="reportNone" runat="server" class="report-filter" visible="false">
    <ul style="margin-left: -20px;">
        <li>There are no Campaigns associated with your Business.</li>
        <li>Therefore, this report would be empty.</li>
    </ul>
</div>
