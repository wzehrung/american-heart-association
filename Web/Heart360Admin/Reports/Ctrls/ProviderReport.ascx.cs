﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Reports.Ctrls
{
    public partial class ProviderReport : System.Web.UI.UserControl, IReportDataManager
    {
        IReportDataManagerReporter m_IReportDataManagerReporter = null;
        public IReportDataManagerReporter IReportDataManagerReporter
        {
            set
            {
                m_IReportDataManagerReporter = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                // If the selected items in the dropdowns don't = Select...
                if (ddlYearStart.SelectedItem.Text != "Select..." && ddlMonthStart.SelectedItem.Text != "Select...")
                {
                    DateTime dt = new DateTime(Convert.ToInt32(ddlYearStart.SelectedValue), Convert.ToInt32(ddlMonthStart.SelectedValue), 1);
                    return dt;
                }
                else
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    return dt;
                }
            }
        }

        public DateTime EndDate
        {
            get
            {
                if (ddlYearEnd.SelectedItem.Text != "Select..." && ddlMonthEnd.SelectedItem.Text != "Select...")
                {
                    DateTime dt = new DateTime(Convert.ToInt32(ddlYearEnd.SelectedValue), Convert.ToInt32(ddlMonthEnd.SelectedValue), 1);
                    return dt;
                }
                else
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    return dt;
                }
            }
        }

        public List<string> MonthNames
        {
            get
            {
                List<string> list = new List<string>();
                DateTime dtEnd = EndDate;
                DateTime dtStart = StartDate;

                while (dtStart <= dtEnd)
                {
                    list.Add(dtStart.ToString("MMM") + ", " + dtStart.ToString("yy"));
                    dtStart = dtStart.AddMonths(1);
                }

                return list;
            }
        }

        public int? CampaignID
        {
            get { return null; }
        }

        public string CampaignIDs
        {
            get { return null; }
        }


        public string AffiliateNames
        {
            get { return string.Empty; }
        }

        public string Markets
        {
            get { return string.Empty; }
        }

        public int? UserTypeID
        {
            get { return null; }
        }

        public int? PartnerID
        {
            get { return null; }
        }

        public string strSelect
        {
            get
            {
                string str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Text_Select").ToString();
                return str;
            }
        }

        public int? iHidReset
        {
            get;
            set;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            iHidReset = 1;

            _LoadYearStartDropdown();
            _LoadYearEndDropdown();

            btnSubmit.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Report_Processing_Message_Generating_Report").ToString() + "'+ '...');ShowHideTrackerMask(true);");
            btnReset.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Resetting_Report").ToString() + "'+ '...');ShowHideTrackerMask(true);");
            //btnExcel.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Generating_Excel").ToString() + "'+ '...');ShowHideTrackerMask(true);");

            _ResetItems();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                lblDateValidationMsg.Visible = false;
            }
        }

        private void _ResetItems()
        {
            iHidReset = 1;

            ddlYearStart.Items.Clear();
            ddlYearEnd.Items.Clear();
            ddlMonthStart.ClearSelection();
            ddlMonthEnd.ClearSelection();
            ddlMonthEnd.SelectedValue = "2";
            _LoadYearStartDropdown();
            _LoadYearEndDropdown();
        }

        private void _LoadYearStartDropdown()
        {
            int ReportPeriod = Convert.ToInt32(Utilities.AppSettings.ReportPeriod);
            int StartYear = DateTime.Now.Year;

            for (int year = StartYear; year > StartYear - ReportPeriod; year--)
            {
                ddlYearStart.Items.Add(new ListItem(year.ToString(), year.ToString()));
            }

            ddlYearStart.SelectedValue = DateTime.Now.Year.ToString();
            ddlYearStart.Text = DateTime.Now.Year.ToString();
        }

        private void _LoadYearEndDropdown()
        {
            int ReportPeriod = Convert.ToInt32(Utilities.AppSettings.ReportPeriod);
            int StartYear = DateTime.Now.Year;

            for (int year = StartYear; year > StartYear - ReportPeriod; year--)
            {
                ddlYearEnd.Items.Add(new ListItem(year.ToString(), year.ToString()));
            }

            ddlYearEnd.SelectedValue = DateTime.Now.Year.ToString();
            ddlYearEnd.Text = DateTime.Now.Year.ToString();
        }

        protected void OnApply(object sender, EventArgs e)
        {
            if (validateDateRange())
            {
                if( m_IReportDataManagerReporter != null )
                    m_IReportDataManagerReporter.LoadItems( this );
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            _ResetItems();
            Response.Redirect("ProviderReport.aspx");
        }

        protected void OnExcelExport(object sender, EventArgs e)
        {
            if (validateDateRange())
            {
                System.Diagnostics.Debug.WriteLine("Entered into OnExcelExport");
                Utilities.ExportToExcel.ExportProviderReport(StartDate, EndDate, CampaignID, Markets, UserTypeID );
                System.Diagnostics.Debug.WriteLine("Exit from ExportPatientReport");
            }
        }

        private bool validateDateRange()
        {
            if (DateTime.Parse(EndDate.ToString()) < DateTime.Parse(StartDate.ToString()))
            {
                lblDateValidationMsg.Text = "Please select end date past start date.";
                lblDateValidationMsg.Visible = true;
                return false;
            }
            else
            {
                lblDateValidationMsg.Visible = false;
                return true;
            }
        }
    }
}