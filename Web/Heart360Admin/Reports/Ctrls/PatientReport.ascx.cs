﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace Heart360Admin.Reports.Ctrls
{
    public partial class PatientReport : System.Web.UI.UserControl, IReportDataManager
    {
        IReportDataManagerReporter m_IReportDataManagerReporter = null;
        public IReportDataManagerReporter IReportDataManagerReporter
        {
            set
            {
                m_IReportDataManagerReporter = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                // If the selected items in the dropdowns don't = Select...
                if (ddlYearStart.SelectedItem.Text != "Select..." && ddlMonthStart.SelectedItem.Text != "Select...")
                {
                    DateTime dt = new DateTime(Convert.ToInt32(ddlYearStart.SelectedValue), Convert.ToInt32(ddlMonthStart.SelectedValue), 1);
                    return dt;
                }
                else
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    return dt;
                }
            }
        }

        public DateTime EndDate
        {
            get
            {
                if (ddlYearEnd.SelectedItem.Text != "Select..." && ddlMonthEnd.SelectedItem.Text != "Select...")
                {
                    DateTime dt = new DateTime(Convert.ToInt32(ddlYearEnd.SelectedValue), Convert.ToInt32(ddlMonthEnd.SelectedValue), 1);
                    return dt;
                }
                else
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    return dt;
                }
            }
        }

        public List<string> MonthNames
        {
            get
            {
                List<string> list = new List<string>();
                DateTime dtEnd = EndDate;
                DateTime dtStart = StartDate;

                while (dtStart <= dtEnd)
                {
                    list.Add(dtStart.ToString("MMM") + ", " + dtStart.ToString("yy"));
                    dtStart = dtStart.AddMonths(1);
                }

                return list;
            }
        }

        public string strSelect
        {
            get
            {
                string str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Text_Select").ToString();
                return str;
            }
        }

        public int? CampaignID
        {
            get { return null; }
        }

        public string CampaignIDs
        {
            get { return null; }
        }

        public string AffiliateNames
        {
            get { return string.Empty; }
        }

        public string Markets
        {
            get { return string.Empty; }
        }

        public int? PartnerID
        {
            get { return null; }
        }

        public int? UserTypeID
        {
            get { return null; }
        }



        public bool isSuccessful
        {
            get;
            set;
        }

        public int? iHidReset
        {
            get;
            set;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            iHidReset = 1;

            _LoadYearStartDropdown();
            _LoadYearEndDropdown();

            btnSubmit.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Report_Processing_Message_Generating_Report").ToString() + "'+ '...');ShowHideTrackerMask(true);");
            btnSubmit.CssClass = "aha-small-button with-arrow";
            btnSubmit.Enabled = true;

            btnReset.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Resetting_Report").ToString() + "'+ '...');ShowHideTrackerMask(true);");

            btnExcel.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Generating_Excel").ToString() + "'+ '...');ShowHideTrackerMask(true);");
            //btnHide.Attributes.Add("onclick", "javascript:ShowHideTrackerMask(false);");

            _ResetItems();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                lblDateValidationMsg.Visible = false;
            }
        }

        private void _ResetItems()
        {
            iHidReset = 1;

            ddlYearStart.ClearSelection();
            ddlYearEnd.ClearSelection();
            ddlMonthStart.ClearSelection();
            ddlMonthEnd.ClearSelection();

            ddlMonthEnd.SelectedValue = "2";
        }

        private void _LoadYearStartDropdown()
        {
            int ReportPeriod = Convert.ToInt32(Utilities.AppSettings.ReportPeriod);
            int StartYear = DateTime.Now.Year;

            for (int year = StartYear; year > StartYear - ReportPeriod; year--)
            {
                ddlYearStart.Items.Add(new ListItem(year.ToString(), year.ToString()));
            }

            ddlYearStart.SelectedValue = DateTime.Now.Year.ToString();
            ddlYearStart.Text = DateTime.Now.Year.ToString();
        }

        private void _LoadYearEndDropdown()
        {
            int ReportPeriod = Convert.ToInt32(Utilities.AppSettings.ReportPeriod);
            int StartYear = DateTime.Now.Year;

            for (int year = StartYear; year > StartYear - ReportPeriod; year--)
            {
                ddlYearEnd.Items.Add(new ListItem(year.ToString(), year.ToString()));
            }

            ddlYearEnd.SelectedValue = DateTime.Now.Year.ToString();
            ddlYearEnd.Text = DateTime.Now.Year.ToString();
        }

        protected void OnApply(object sender, EventArgs e)
        {
            if (validateDateRange())
            {
                if( m_IReportDataManagerReporter != null )
                    m_IReportDataManagerReporter.LoadItems( this );
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            _ResetItems();
        }

        protected void OnExcelExport(object sender, EventArgs e)
        {
            if (validateDateRange())
            {
                System.Diagnostics.Debug.WriteLine("Entered into OnExcelExport");
                Utilities.ExportToExcel.ExportPatientReport(StartDate, EndDate, CampaignID, PartnerID);   // TODO
                System.Diagnostics.Debug.WriteLine("Exit from ExportPatientReport");           
            }
        }

        protected void HideBar()
        {
            // Get a reference to the master page
            MasterPage ctl00 = FindControl("ctl00") as MasterPage;

            //ContentPlaceHolder MainContent = ctl00.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;

            Control div = ctl00.FindControl("divProgressBarHider") as Control;
            div.Visible = false;
        }

        private bool validateDateRange()
        {

            if (DateTime.Parse(EndDate.ToString()) < DateTime.Parse(StartDate.ToString()))
            {
                lblDateValidationMsg.Text = "Please select end date past start date.";
                lblDateValidationMsg.Visible = true;
                return false;
            }
            else
            {
                lblDateValidationMsg.Visible = false;
                return true;
            }
        }
    }
}