﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Heart360Admin.Utilities;
using System.Web.UI.DataVisualization.Charting;
using AHAHelpContent;
using GRCBase.WebControls_V1;



namespace Heart360Admin.Reports.Ctrls.Campaign
{
    public partial class PatientHealthReport : System.Web.UI.UserControl
    {
        private System.Drawing.Color percentageColor = System.Drawing.ColorTranslator.FromHtml("#4f89c4");
        private const string strHealthyColorHtmlCode = "#a9e101";
        private const string strAcceptableColorHtmlCode = "#ffe400";
        private const string strAtRiskColorHtmlCode = "#ff7800";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dtStartDate.DateFormat = AHACommon.DateTimeHelper.GetDateFormatForCalendar();
                dtEndDate.DateFormat = AHACommon.DateTimeHelper.GetDateFormatForCalendar();
                dtStartDate.Date = DateTime.Today.AddMonths(-1);
                dtEndDate.Date = DateTime.Today;
                lnkClearProvider.Visible = false;
                _LoadItemTypeDDL(ddlItemType);
                _LoadCampaignDropdown();
                rdBoth.Checked = true;
                dtEndDate.OnChangeFunction = "onEndDateChange();";
            }
            if (dtEndDate.Date.HasValue)
            {
                lnkClearEndDate.Style.Add("display", "");
                trActiveFilter.Style.Add("display", "");
            }
            else
            {
                lnkClearEndDate.Style.Add("display", "none");
                trActiveFilter.Style.Add("display", "none");
            }


        }

        private int? _GetCampaignID()
        {
            if (!string.IsNullOrEmpty(ddlCampaign.SelectedValue))
            {
                int ic = Convert.ToInt32(ddlCampaign.SelectedValue);
                if (ic == 0)
                    return null;
                return ic;
            }

            return null;
        }

        private void _LoadCampaignDropdown()
        {
            List<AHAHelpContent.Campaign> list = AHAHelpContent.Campaign.FindAllCampaignsInLast2Years(Utilities.Utility.GetCurrentLanguageID());
            list.Insert(0, new AHAHelpContent.Campaign { Title = "All", CampaignID = 0 });
            ddlCampaign.DataSource = list;
            ddlCampaign.DataTextField = "Title";
            ddlCampaign.DataValueField = "CampaignID";
            ddlCampaign.DataBind();
            ddlCampaign.SelectedValue = string.Empty;
            ddlCampaign.Text = "All";
        }

        private void _LoadItemTypeDDL(GRCComboBox ddlItemType)
        {
            ListItemCollection objList = new ListItemCollection();
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.BloodPressure), TrackerDataType.BloodPressure.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.Cholesterol), TrackerDataType.Cholesterol.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.BloodGlucose), TrackerDataType.BloodGlucose.ToString()));           
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.Weight), TrackerDataType.Weight.ToString()));
            ddlItemType.DataSource = objList;
            ddlItemType.DataTextField = "Text";
            ddlItemType.DataValueField = "Value";
            ddlItemType.DataBind();


            ddlItemType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", string.Empty));
            ddlItemType.SetSelectedItem(string.Empty);
        }

        private AHACharts _PopulateChartData(AHAHelpContent.Reports.PatientHealthReport objPatientHealthReport)
        {
            AHACharts objAHACharts = new AHACharts();
            objAHACharts.Height = 350;
            objAHACharts.Width = 430;
            objAHACharts.Enable3D = true;
            objAHACharts.XAxisTitle = "Gender";
            objAHACharts.YAxisTitle = "Totals";
            objAHACharts.ChartType = SeriesChartType.StackedColumn;
            List<GraphData> graphDataList = new List<GraphData>();

            GraphData graphDataHealthy = new GraphData();
            graphDataHealthy.SeriesName = "Healthy";
            graphDataHealthy.Color = strHealthyColorHtmlCode;
            List<GraphInputReading> GraphInputReadingListHealthy = new List<GraphInputReading>();

            GraphData graphDataAcceptable = new GraphData();
            graphDataAcceptable.SeriesName = "Acceptable";
            graphDataAcceptable.Color = strAcceptableColorHtmlCode;
            List<GraphInputReading> GraphInputReadingListAcceptable = new List<GraphInputReading>();

            GraphData graphDataAtRisk = new GraphData();
            graphDataAtRisk.SeriesName = "At Risk";
            graphDataAtRisk.Color = strAtRiskColorHtmlCode;
            List<GraphInputReading> GraphInputReadingListAtRisk = new List<GraphInputReading>();
            foreach (AHAHelpContent.Reports.HealthStatus healthStatus in objPatientHealthReport.HealthStatusList)
            {

                GraphInputReading graphInputReadingHealthy = new GraphInputReading();
                graphInputReadingHealthy.XValue = healthStatus.Gender;
                graphInputReadingHealthy.YValue = healthStatus.Healthy;
                GraphInputReadingListHealthy.Add(graphInputReadingHealthy);


                GraphInputReading graphInputReadingAcceptable = new GraphInputReading();
                graphInputReadingAcceptable.XValue = healthStatus.Gender;
                graphInputReadingAcceptable.YValue = healthStatus.Acceptable;
                GraphInputReadingListAcceptable.Add(graphInputReadingAcceptable);


                GraphInputReading graphInputReadingAtRisk = new GraphInputReading();
                graphInputReadingAtRisk.XValue = healthStatus.Gender;
                graphInputReadingAtRisk.YValue = healthStatus.AtRisk;
                GraphInputReadingListAtRisk.Add(graphInputReadingAtRisk);

            }
            graphDataHealthy.GraphInputReadingList = GraphInputReadingListHealthy;
            graphDataAcceptable.GraphInputReadingList = GraphInputReadingListAcceptable;
            graphDataAtRisk.GraphInputReadingList = GraphInputReadingListAtRisk;

            graphDataList.Add(graphDataAtRisk);
            graphDataList.Add(graphDataAcceptable);
            graphDataList.Add(graphDataHealthy);

            objAHACharts.GraphDataList = graphDataList;
            return objAHACharts;
        }

        protected void HealthStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AHAHelpContent.Reports.HealthStatus objHealthStatus = e.Item.DataItem as AHAHelpContent.Reports.HealthStatus;

                Literal litGender = e.Item.FindControl("litGender") as Literal;
                Literal litHealthy = e.Item.FindControl("litHealthy") as Literal;
                Label lblHealthyPerc = e.Item.FindControl("lblHealthyPerc") as Label;
                Literal litAcceptable = e.Item.FindControl("litAcceptable") as Literal;
                Label lblAcceptablePerc = e.Item.FindControl("lblAcceptablePerc") as Label;
                Literal litAtRisk = e.Item.FindControl("litAtRisk") as Literal;
                Label lblAtRiskPerc = e.Item.FindControl("lblAtRiskPerc") as Label;

                litGender.Text = objHealthStatus.Gender;
                litHealthy.Text = objHealthStatus.Healthy.ToString();
                lblHealthyPerc.Text = string.Format("({0}%)", objHealthStatus.HealthyPerc.ToString());
                lblHealthyPerc.ForeColor = percentageColor;
                litAcceptable.Text = objHealthStatus.Acceptable.ToString();
                lblAcceptablePerc.Text = string.Format("({0}%)", objHealthStatus.AcceptablePerc.ToString());
                lblAcceptablePerc.ForeColor = percentageColor;
                litAtRisk.Text = objHealthStatus.AtRisk.ToString();
                lblAtRiskPerc.Text = string.Format("({0}%)", objHealthStatus.AtRiskPerc.ToString());
                lblAtRiskPerc.ForeColor = percentageColor;
            }
        }


        protected void lnkGenerateReport_Click(object sender, EventArgs e)
        {
            Page.Validate("CreateReport");
            if (!Page.IsValid)
            {
                plcReportWithEndDate.Visible = false;
                plcReportWithOnlyStartDate.Visible = false;
                return;
            }


            string strGender = null;
            if (rdMale.Checked)
            {
                strGender = AHACommon.AHAGender.Male;
            }
            else if (rdFemale.Checked)
            {
                strGender = AHACommon.AHAGender.Female;
            }
            TrackerDataType trackerDataType = (TrackerDataType)Enum.Parse(typeof(TrackerDataType), ddlItemType.SelectedValue, true);
            string strHeading = ddlItemType.Text + " risk stratification by gender";
            litReportWithEndDateHeading.Text = strHeading;
            litReportWithOnlyStartDateHeading.Text = strHeading;
            int? providerID = null;
            if (!string.IsNullOrEmpty(hddnProviderID.Value))
            {
                providerID = Convert.ToInt32(hddnProviderID.Value);
            }

            bool hasToTakeActiveUsers = false;
            if (dtEndDate.Date.HasValue)
            {
                hasToTakeActiveUsers = chkActiveFilter.Checked;
            }
            List<AHAHelpContent.Reports.PatientHealthReport> listPatientHealthReport = AHAHelpContent.Reports.PatientHealthReport.GetPatientHealthReport(strGender, _GetCampaignID(), providerID, hasToTakeActiveUsers, dtStartDate.Date.Value, dtEndDate.Date, trackerDataType);
            litStartingPeriod.Text = dtStartDate.Date.Value.ToShortDateString();
            litOnlyStartingPeriod.Text = dtStartDate.Date.Value.ToShortDateString();
            if (dtEndDate.Date.HasValue)
            {
                litEndingPeriod.Text = dtEndDate.Date.Value.ToShortDateString();
                bool showReports = false;
                plcReportWithOnlyStartDate.Visible = false;
                if (listPatientHealthReport.Count > 0)
                {
                    AHAHelpContent.Reports.PatientHealthReport objStartingPeriodPatientHealthReport = listPatientHealthReport.Find(ph => ph.PatientHealthReportType == AHAHelpContent.Reports.PatientHealth_ReportType.StartingPeriodReport);
                    if (objStartingPeriodPatientHealthReport != null && objStartingPeriodPatientHealthReport.HealthStatusList.Count > 0)
                    {
                        showReports = true;
                        AHACharts objAHACharts = _PopulateChartData(objStartingPeriodPatientHealthReport);
                        objAHACharts.GenerateChart(chtStartingPeriod);

                        rptStartingPeriod.DataSource = objStartingPeriodPatientHealthReport.HealthStatusList;
                        rptStartingPeriod.DataBind();

                    }

                    AHAHelpContent.Reports.PatientHealthReport objEndingPeriodPatientHealthReport = listPatientHealthReport.Find(ph => ph.PatientHealthReportType == AHAHelpContent.Reports.PatientHealth_ReportType.EndingPeriodReport);
                    if (objEndingPeriodPatientHealthReport != null && objEndingPeriodPatientHealthReport.HealthStatusList.Count > 0)
                    {
                        showReports = true;
                        AHACharts objAHACharts = _PopulateChartData(objEndingPeriodPatientHealthReport);
                        objAHACharts.GenerateChart(chtEndingPeriod);

                        rptEndingPeriod.DataSource = objEndingPeriodPatientHealthReport.HealthStatusList;
                        rptEndingPeriod.DataBind();

                    }

                    AHAHelpContent.Reports.PatientHealthReport objPeriodDiffPatientHealthReport = listPatientHealthReport.Find(ph => ph.PatientHealthReportType == AHAHelpContent.Reports.PatientHealth_ReportType.PeriodDifferenceReport);
                    if (objPeriodDiffPatientHealthReport != null && objPeriodDiffPatientHealthReport.HealthStatusList.Count > 0)
                    {
                        showReports = true;
                        rptPeriodDifference.DataSource = objPeriodDiffPatientHealthReport.HealthStatusList;
                        rptPeriodDifference.DataBind();
                    }
                    plcReportWithEndDate.Visible = showReports;
                    tblNoRecords.Visible = !showReports;
                }
                else
                {
                    plcReportWithEndDate.Visible = false;
                    tblNoRecords.Visible = true;
                }
            }
            else
            {
                plcReportWithEndDate.Visible = false;
                AHAHelpContent.Reports.PatientHealthReport objStartingPeriodPatientHealthReport = listPatientHealthReport.Find(ph => ph.PatientHealthReportType == AHAHelpContent.Reports.PatientHealth_ReportType.StartingPeriodReport);
                if (objStartingPeriodPatientHealthReport != null && objStartingPeriodPatientHealthReport.HealthStatusList.Count > 0)
                {

                    AHACharts objAHACharts = _PopulateChartData(objStartingPeriodPatientHealthReport);
                    objAHACharts.GenerateChart(chtOnlyStarting);

                    rptOnlyStartingPeriod.DataSource = objStartingPeriodPatientHealthReport.HealthStatusList;
                    rptOnlyStartingPeriod.DataBind();
                    plcReportWithOnlyStartDate.Visible = true;
                    tblNoRecords.Visible = false;
                }
                else
                {
                    plcReportWithOnlyStartDate.Visible = false;
                    tblNoRecords.Visible = true;
                }
            }

            //updatePanelResult.Update();
        }


        protected void cvSearchProvider_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator cvValidate = sender as CustomValidator;
            if (string.IsNullOrEmpty(txtFirstName.Text.Trim()) &&
                string.IsNullOrEmpty(txtLastName.Text.Trim()))
            {
                cvValidate.ErrorMessage = "Please enter a part of first name or last name";
                v.IsValid = false;
                return;
            }
        }

        protected void cvItemType_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator cvValidate = sender as CustomValidator;
            if (string.IsNullOrEmpty(ddlItemType.SelectedValue))
            {
                cvValidate.ErrorMessage = "Please select a Item Type";
                v.IsValid = false;
                return;
            }
        }

        protected void cvStartDate_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator cvValidate = sender as CustomValidator;
            if (!dtStartDate.Date.HasValue)
            {
                cvValidate.ErrorMessage = "Please select a StartDate";
                v.IsValid = false;
                return;
            }
            else if (dtStartDate.Date.HasValue && dtStartDate.Date.Value > DateTime.Today.AddDays(1))
            {
                cvValidate.ErrorMessage = "Start date cannot be a future date";
                v.IsValid = false;
                return;
            }
        }

        protected void cvEndDate_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator cvValidate = sender as CustomValidator;
            if (dtEndDate.Date != null)
            {
                if (dtEndDate.Date.Value > DateTime.Today.AddDays(1))
                {
                    cvValidate.ErrorMessage = "End date cannot be a future date";
                    v.IsValid = false;
                    return;
                }
                else if (dtStartDate.Date.HasValue && (dtEndDate.Date.Value < dtStartDate.Date.Value))
                {
                    cvValidate.ErrorMessage = "End date can not be less than Start date";
                    v.IsValid = false;
                    return;
                }
            }
        }

        protected void lnkSearchProvider_Click(object sender, EventArgs e)
        {
            //txtSearchProvider.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            gvProviders.Visible = false;
            plcReportWithEndDate.Visible = false;
            plcReportWithOnlyStartDate.Visible = false;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "showProviderSearchPopup_" + ClientID, "showProviderSearchPopup();", true);
        }

        protected void lnkSearch_Click(object sender, EventArgs e)
        {
            Page.Validate("SearchProvider");
            if (!Page.IsValid)
            {
                return;
            }

            //need to get the english language ID from database
           
            ////rptProviders.Visible = true;
            ////rptProviders.DataSource = listProviderDetails;
            ////rptProviders.DataBind();
            gvProviders.PageIndex = 0;
            _BindProvidersToGridView();
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "showProviderSearchPopup_" + ClientID, "showProviderSearchPopup();", true);
        }

        private void _BindProvidersToGridView()
        {
            //need to get the english language ID from database
            List<AHAHelpContent.ProviderDetails> listProviderDetails = AHAHelpContent.ProviderDetails.SearchProvider(txtFirstName.Text.Trim(),txtLastName.Text.Trim(),null,null, null, null, 1);
            gvProviders.Visible = true;
            gvProviders.DataSource = listProviderDetails;
            gvProviders.DataBind();
        }

        protected void rptProviders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AHAHelpContent.ProviderDetails objProviderDetails = e.Item.DataItem as AHAHelpContent.ProviderDetails;

                Literal litProviderName = e.Item.FindControl("litProviderName") as Literal;
                LinkButton lnkSelect = e.Item.FindControl("lnkSelect") as LinkButton;
                litProviderName.Text = objProviderDetails.FirstName + " " + objProviderDetails.LastName;
                lnkSelect.Attributes.Add("ProviderID", objProviderDetails.ProviderID.ToString());
                lnkSelect.Attributes.Add("ProviderName", objProviderDetails.FirstName + " " + objProviderDetails.LastName);
            }
        }


        protected void gvProviders_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            {
                AHAHelpContent.ProviderDetails objProviderDetails = e.Row.DataItem as AHAHelpContent.ProviderDetails;
                Literal litProviderName = e.Row.FindControl("litProviderName") as Literal;
                LinkButton lnkSelect = e.Row.FindControl("lnkSelect") as LinkButton;
                litProviderName.Text = objProviderDetails.FirstName + " " + objProviderDetails.LastName;
                lnkSelect.Attributes.Add("ProviderID", objProviderDetails.ProviderID.ToString());
                lnkSelect.Attributes.Add("ProviderName", objProviderDetails.FirstName + " " + objProviderDetails.LastName);
            }
        }


        protected void gvProviders_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProviders.PageIndex = e.NewPageIndex;
            _BindProvidersToGridView();
        }

        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            LinkButton lnkSelect = sender as LinkButton;
            string strProviderID = lnkSelect.Attributes["ProviderID"];
            if (!string.IsNullOrEmpty(strProviderID))
            {
                hddnProviderID.Value = strProviderID;
                litProviderName.Text = lnkSelect.Attributes["ProviderName"];
                lnkClearProvider.Visible = true;
            }
            else
            {
                lnkClearProvider.Visible = false;
            }
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            gvProviders.Visible = false;
            updatePanelOutter.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideProviderSearchPopup_" + ClientID, "hideProviderSearchPopup();", true);
        }

        protected void lnkClearProvider_Click(object sender, EventArgs e)
        {
            hddnProviderID.Value = string.Empty;
            litProviderName.Text = string.Empty;
            lnkClearProvider.Visible = false;
            plcReportWithEndDate.Visible = false;
            plcReportWithOnlyStartDate.Visible = false;
        }

        protected void lnkClearEndDate_Click(object sender, EventArgs e)
        {
            dtEndDate.Date = null;
            plcReportWithEndDate.Visible = false;
            plcReportWithOnlyStartDate.Visible = false;
            lnkClearEndDate.Style.Add("display", "none");
            trActiveFilter.Style.Add("display", "none");
        }
    }
}