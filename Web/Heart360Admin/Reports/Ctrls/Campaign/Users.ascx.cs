﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Heart360Admin.Reports.Ctrls.Campaign
{
    public partial class Users : System.Web.UI.UserControl, IReportDataManagerReporter
    {

        public void LoadItems( IReportDataManager iRdm )
        {
            using (DataSet ds = AHAHelpContent.PatientReportCampaigns.GetHeart360UserReportCampaign(iRdm.StartDate, iRdm.EndDate, iRdm.CampaignIDs, iRdm.Markets))
            {
                ucCompositeReportCtrl.LoadItems(ds.Tables[0], iRdm.MonthNames.ToArray());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}