﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GRCBase.WebControls_V1;
using AHAHelpContent;
using AHACommon;
using Heart360Admin.Utilities;
using System.Web.UI.HtmlControls;

namespace Heart360Admin.Reports.Ctrls.Campaign
{
    public partial class RiskStratificationOverTimeReport : System.Web.UI.UserControl
    {
        public int intColoumnCount = 1;
        public string strLowRisk = string.Empty;
        public string strMedium = string.Empty;
        public string strHigh = string.Empty;
        public string strVeryHigh = string.Empty;
        public string strInActiveUsers = string.Empty;
        public bool showVeryHighRiskRow = false;




        private void _SetUIRiskNames()
        {
            if (ddlItemType.SelectedValue.Equals(TrackerDataType.BloodPressure.ToString()))
            {
                strLowRisk = "Normal BP";
                strMedium = "Pre-hypertension";
                strHigh = "Hypertension";
                strVeryHigh = "Very high";

            }
            else
            {
                strLowRisk = "Low risk";
                strMedium = "Medium risk";
                strHigh = "High risk";
            }
            strInActiveUsers = "Inactive Users";
        }


        private void _LoadItemTypeDDL(GRCComboBox ddlItemType)
        {
            ListItemCollection objList = new ListItemCollection();
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.BloodPressure), TrackerDataType.BloodPressure.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.Cholesterol), TrackerDataType.Cholesterol.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.BloodGlucose), TrackerDataType.BloodGlucose.ToString()));
            objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(TrackerDataType.Weight), TrackerDataType.Weight.ToString()));
            ddlItemType.DataSource = objList;
            ddlItemType.DataTextField = "Text";
            ddlItemType.DataValueField = "Value";
            ddlItemType.DataBind();


            ddlItemType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", string.Empty));
            ddlItemType.SetSelectedItem(string.Empty);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _LoadItemTypeDDL(ddlItemType);
                _LoadMonthsDDL(ddlMonths);
                _LoadCampaignDropdown();
                lnkClearProvider.Visible = false;
            }
            _SetUIRiskNames();
        }

        private void _LoadMonthsDDL(GRCComboBox ddlMonths)
        {
            ListItemCollection objList = new ListItemCollection();
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(DataDateReturnType.Last3Months), DataDateReturnType.Last3Months.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(DataDateReturnType.Last6Months), DataDateReturnType.Last6Months.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(DataDateReturnType.Last9Months), DataDateReturnType.Last9Months.ToString()));
            //objList.Add(new System.Web.UI.WebControls.ListItem(GRCBase.MiscUtility.GetComponentModelPropertyDescription(DataDateReturnType.LastYear), DataDateReturnType.LastYear.ToString()));
            for (int i = -23; i <= 0; i++)
            {
                DateTime dtDate = DateTime.Now.AddMonths(i);
                string strMonthWithYearText = string.Format("{0} {1}", GRCBase.DateTimeHelper.GetMonthName(dtDate.Month), dtDate.Year);
                string strDate = (new DateTime(dtDate.Year, dtDate.Month, 1)).ToShortDateString();
                objList.Add(new System.Web.UI.WebControls.ListItem(strMonthWithYearText, strDate));
            }
            ddlMonths.DataSource = objList;
            ddlMonths.DataTextField = "Text";
            ddlMonths.DataValueField = "Value";
            ddlMonths.DataBind();


            ddlMonths.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select", string.Empty));
            ddlMonths.SetSelectedItem(string.Empty);
        }

        protected void lnkGenerateReport_Click(object sender, EventArgs e)
        {
            Page.Validate("Create");
            if (!Page.IsValid)
            {
                plcReport.Visible = false;
                divNoRecords.Visible = false;
                return;
            }

            _SetUIRiskNames();
            TrackerDataType trackerDataType = (TrackerDataType)Enum.Parse(typeof(TrackerDataType), ddlItemType.SelectedValue, true);
            DateTime dtStartDate = _GetStartDate(ddlMonths);
            DateTime dtEndDate = dtStartDate.AddYears(1).AddMilliseconds(-1);

            int? providerID = null;
            if (!string.IsNullOrEmpty(hddnProviderID.Value))
            {
                providerID = Convert.ToInt32(hddnProviderID.Value);
            }
            List<AHAHelpContent.Reports.RiskStratificationOvertimeReport> listRiskStratificationOvertimeReport = AHAHelpContent.Reports.RiskStratificationOvertimeReport.GetRiskStratificationOverTimeReport(dtStartDate, dtEndDate, _GetCampaignID(), providerID, trackerDataType);

            List<string> listMonth = new List<string>();
            List<double> listLowRisk = new List<double>();
            List<double> listMedium = new List<double>();
            List<double> listHigh = new List<double>();
            List<double> listTotal = new List<double>();
            List<double> listLowRiskPerc = new List<double>();
            List<double> listMediumPerc = new List<double>();
            List<double> listHighPerc = new List<double>();
            //List<double> listVeryHigh = new List<double>();
            List<double> listVeryHighPerc = new List<double>();
            List<double> listInActiveUsers = new List<double>();
            List<double> listInActiveUsersPerc = new List<double>();
            foreach (AHAHelpContent.Reports.RiskStratificationOvertimeReport objRiskStratificationOvertimeReport in listRiskStratificationOvertimeReport)
            {
                listMonth.Add(objRiskStratificationOvertimeReport.Date.ToString("MMM yy"));
                //listMonth.Add(String.Format("{0:MMM yy}", objRiskStratificationOvertimeReport.Date));
                listLowRisk.Add(objRiskStratificationOvertimeReport.Healthy);
                listMedium.Add(objRiskStratificationOvertimeReport.Acceptable);
                listHigh.Add(objRiskStratificationOvertimeReport.AtRisk);
                listTotal.Add(objRiskStratificationOvertimeReport.Total);
                listLowRiskPerc.Add(objRiskStratificationOvertimeReport.HealthyPerc);
                listMediumPerc.Add(objRiskStratificationOvertimeReport.AcceptablePerc);
                listHighPerc.Add(objRiskStratificationOvertimeReport.AtRiskPerc);
                listInActiveUsers.Add(objRiskStratificationOvertimeReport.InActiveUsers);
                listInActiveUsersPerc.Add(objRiskStratificationOvertimeReport.InActiveUsersPerc);
                //if (ddlItemType.SelectedValue.Equals(TrackerDataType.BloodPressure.ToString()))
                //{
                //    listVeryHigh.Add(objRiskStratificationOvertimeReport.VeryHighRisk);
                //    listVeryHighPerc.Add(objRiskStratificationOvertimeReport.VeryHighRiskPerc);
                //}
            }

            if (listRiskStratificationOvertimeReport.Count > 0)
            {
                plcReport.Visible = true;
                divNoRecords.Visible = false;
                intColoumnCount = intColoumnCount + listRiskStratificationOvertimeReport.Count;

                rptMonth.DataSource = listMonth;
                rptMonth.DataBind();

                rptLowRisk.DataSource = listLowRisk;
                rptLowRisk.DataBind();

                rptMedium.DataSource = listMedium;
                rptMedium.DataBind();

                rptHigh.DataSource = listHigh;
                rptHigh.DataBind();

                rptTotal.DataSource = listTotal;
                rptTotal.DataBind();

                rptLowRiskPerc.DataSource = listLowRiskPerc;
                rptLowRiskPerc.DataBind();

                rptMediumPerc.DataSource = listMediumPerc;
                rptMediumPerc.DataBind();

                rptHighPerc.DataSource = listHighPerc;
                rptHighPerc.DataBind();

                rptInActiveUsers.DataSource = listInActiveUsers;
                rptInActiveUsers.DataBind();

                rptInActiveUsersPerc.DataSource = listInActiveUsersPerc;
                rptInActiveUsersPerc.DataBind();

                //if (ddlItemType.SelectedValue.Equals(TrackerDataType.BloodPressure.ToString()))
                //{
                //    rptVeryHigh.DataSource = listVeryHigh;
                //    rptVeryHigh.DataBind();
                //    rptVeryHighPerc.DataSource = listVeryHighPerc;
                //    rptVeryHighPerc.DataBind();
                //    rptVeryHigh.Visible = true;
                //    rptVeryHighPerc.Visible = true;
                //    showVeryHighRiskRow = true;
                //}
                //else
                //{
                //    rptVeryHigh.Visible = false;
                //    rptVeryHighPerc.Visible = false;
                //    showVeryHighRiskRow = false;
                //}

                AHACharts objAHACharts = _PopulateChartData(listRiskStratificationOvertimeReport);
                objAHACharts.GenerateChart(chtRiskStratification);
            }
            else
            {
                plcReport.Visible = false;
                divNoRecords.Visible = true;
            }
            updatePanelReport.Update();
        }

        protected void lnkSearchProvider_Click(object sender, EventArgs e)
        {
            //txtSearchProvider.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            gvProviders.Visible = false;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "showProviderSearchPopup_" + ClientID, "showProviderSearchPopup();", true);
        }

        protected void lnkSearch_Click(object sender, EventArgs e)
        {
            Page.Validate("SearchProvider");
            if (!Page.IsValid)
            {
                return;
            }

            //need to get the english language ID from database

            ////rptProviders.Visible = true;
            ////rptProviders.DataSource = listProviderDetails;
            ////rptProviders.DataBind();
            gvProviders.PageIndex = 0;
            _BindProvidersToGridView();
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "showProviderSearchPopup_" + ClientID, "showProviderSearchPopup();", true);
        }

        private void _BindProvidersToGridView()
        {
            //need to get the english language ID from database
            List<AHAHelpContent.ProviderDetails> listProviderDetails = AHAHelpContent.ProviderDetails.SearchProviderV2(txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtPracticeName.Text.Trim(), txtProviderCode.Text.Trim(), null, null, null, 1);
            gvProviders.Visible = true;
            gvProviders.DataSource = listProviderDetails;
            gvProviders.DataBind();
        }

        protected void rptProviders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AHAHelpContent.ProviderDetails objProviderDetails = e.Item.DataItem as AHAHelpContent.ProviderDetails;

                Literal litProviderName = e.Item.FindControl("litProviderName") as Literal;
                LinkButton lnkSelect = e.Item.FindControl("lnkSelect") as LinkButton;
                litProviderName.Text = objProviderDetails.FirstName + " " + objProviderDetails.LastName;
                lnkSelect.Attributes.Add("ProviderID", objProviderDetails.ProviderID.ToString());
                lnkSelect.Attributes.Add("ProviderName", objProviderDetails.FirstName + " " + objProviderDetails.LastName);
            }
        }


        protected void gvProviders_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            {
                AHAHelpContent.ProviderDetails objProviderDetails = e.Row.DataItem as AHAHelpContent.ProviderDetails;
                Literal litProviderName = e.Row.FindControl("litProviderName") as Literal;
                LinkButton lnkSelect = e.Row.FindControl("lnkSelect") as LinkButton;
                litProviderName.Text = objProviderDetails.FirstName + " " + objProviderDetails.LastName;
                lnkSelect.Attributes.Add("ProviderID", objProviderDetails.ProviderID.ToString());
                lnkSelect.Attributes.Add("ProviderName", objProviderDetails.FirstName + " " + objProviderDetails.LastName);
            }
        }

        protected void gvProviders_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProviders.PageIndex = e.NewPageIndex;
            _BindProvidersToGridView();
        }

        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            LinkButton lnkSelect = sender as LinkButton;
            string strProviderID = lnkSelect.Attributes["ProviderID"];
            if (!string.IsNullOrEmpty(strProviderID))
            {
                hddnProviderID.Value = strProviderID;
                litProviderName.Text = lnkSelect.Attributes["ProviderName"];
                lnkClearProvider.Visible = true;
            }
            else
            {
                lnkClearProvider.Visible = false;
            }
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            gvProviders.Visible = false;
            updatePanelOutter.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "hideProviderSearchPopup_" + ClientID, "hideProviderSearchPopup();", true);
        }

        protected void lnkClearProvider_Click(object sender, EventArgs e)
        {
            hddnProviderID.Value = string.Empty;
            litProviderName.Text = string.Empty;
            lnkClearProvider.Visible = false;
        }

        protected void rptMonth_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string strMonthName = e.Item.DataItem as string;
                Literal litMonth = e.Item.FindControl("litMonth") as Literal;
                litMonth.Text = strMonthName;
            }
        }

        protected void Value_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                double dblValue = (double)e.Item.DataItem;
                Literal litValue = e.Item.FindControl("litValue") as Literal;
                litValue.Text = dblValue.ToString();
            }
        }

        protected void ValuePerc_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                double dblValue = (double)e.Item.DataItem;
                Literal litValue = e.Item.FindControl("litValue") as Literal;
                litValue.Text = dblValue.ToString("N2") + "%";
            }
        }

        protected void cvSearchProvider_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator cvValidate = sender as CustomValidator;
            if (string.IsNullOrEmpty(txtFirstName.Text.Trim()) &&
                string.IsNullOrEmpty(txtLastName.Text.Trim()) &&
                string.IsNullOrEmpty(txtPracticeName.Text.Trim()) &&
                string.IsNullOrEmpty(txtProviderCode.Text.Trim()))
            {
                cvValidate.ErrorMessage = "Please enter a provider code or part of first name or last name or practice name";
                v.IsValid = false;
                return;
            }
        }

        private AHACharts _PopulateChartData(List<AHAHelpContent.Reports.RiskStratificationOvertimeReport> objRiskStratificationReportList)
        {
            AHACharts objAHACharts = new AHACharts();
            objAHACharts.Height = 400;
            objAHACharts.Width = 800;
            objAHACharts.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
            objAHACharts.XAxisTitle = "";
            objAHACharts.YAxisTitle = "";
            objAHACharts.YAxisMajorGridEnabled = true;
            List<GraphData> graphDataList = new List<GraphData>();

            GraphData graphDataHealthy = new GraphData();
            graphDataHealthy.SeriesName = strLowRisk;
            graphDataHealthy.Color = "#63c122";//"#be4b48";
            List<GraphInputReading> GraphInputReadingListHealthy = new List<GraphInputReading>();

            GraphData graphDataAcceptable = new GraphData();
            graphDataAcceptable.SeriesName = strMedium;
            graphDataAcceptable.Color = "#DDDD09";//"#98b954";
            List<GraphInputReading> GraphInputReadingListAcceptable = new List<GraphInputReading>();

            GraphData graphDataAtRisk = new GraphData();
            graphDataAtRisk.SeriesName = strHigh;
            graphDataAtRisk.Color = "#d72f22";//"#7d60a0";
            List<GraphInputReading> GraphInputReadingListAtRisk = new List<GraphInputReading>();

            GraphData graphDataInactiveUsers = new GraphData();
            graphDataInactiveUsers.SeriesName = strInActiveUsers;
            graphDataInactiveUsers.Color = "#666666";
            List<GraphInputReading> GraphInputReadingListInactiveUsers = new List<GraphInputReading>();


            foreach (AHAHelpContent.Reports.RiskStratificationOvertimeReport riskStratification in objRiskStratificationReportList)
            {

                GraphInputReading graphInputReadingHealthy = new GraphInputReading();
                graphInputReadingHealthy.XValue = riskStratification.Date.ToString("MMM yy");
                //graphInputReadingHealthy.XValue = String.Format("{0:MMM yy}", riskStratification.Date);
                graphInputReadingHealthy.YValue = riskStratification.Healthy;
                GraphInputReadingListHealthy.Add(graphInputReadingHealthy);


                GraphInputReading graphInputReadingAcceptable = new GraphInputReading();
                graphInputReadingAcceptable.XValue = riskStratification.Date.ToString("MMM yy");
                //graphInputReadingAcceptable.XValue = String.Format("{0:MMM yy}", riskStratification.Date);
                graphInputReadingAcceptable.YValue = riskStratification.Acceptable;
                GraphInputReadingListAcceptable.Add(graphInputReadingAcceptable);


                GraphInputReading graphInputReadingAtRisk = new GraphInputReading();
                graphInputReadingAtRisk.XValue = riskStratification.Date.ToString("MMM yy");
                //graphInputReadingAtRisk.XValue = String.Format("{0:MMM yy}", riskStratification.Date);
                graphInputReadingAtRisk.YValue = riskStratification.AtRisk;
                GraphInputReadingListAtRisk.Add(graphInputReadingAtRisk);

                GraphInputReading graphInputReadingInactiveUsers = new GraphInputReading();
                graphInputReadingInactiveUsers.XValue = riskStratification.Date.ToString("MMM yy");
                //graphInputReadingAtRisk.XValue = String.Format("{0:MMM yy}", riskStratification.Date);
                graphInputReadingInactiveUsers.YValue = riskStratification.InActiveUsers;
                GraphInputReadingListInactiveUsers.Add(graphInputReadingInactiveUsers);


            }
            graphDataHealthy.GraphInputReadingList = GraphInputReadingListHealthy;
            graphDataAcceptable.GraphInputReadingList = GraphInputReadingListAcceptable;
            graphDataAtRisk.GraphInputReadingList = GraphInputReadingListAtRisk;
            graphDataInactiveUsers.GraphInputReadingList = GraphInputReadingListInactiveUsers;

            graphDataList.Add(graphDataHealthy);
            graphDataList.Add(graphDataAcceptable);
            graphDataList.Add(graphDataAtRisk);
            graphDataList.Add(graphDataInactiveUsers);

            objAHACharts.GraphDataList = graphDataList;
            return objAHACharts;
        }


        protected void cvItemType_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator cvValidate = sender as CustomValidator;
            if (string.IsNullOrEmpty(ddlItemType.SelectedValue))
            {
                cvValidate.ErrorMessage = "Please select a Item Type";
                v.IsValid = false;
                return;
            }
        }

        protected void cvPeriod_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator cvValidate = sender as CustomValidator;
            if (string.IsNullOrEmpty(ddlMonths.SelectedValue))
            {
                cvValidate.ErrorMessage = "Please select a period";
                v.IsValid = false;
                return;
            }
        }


        //private DateTime _GetStartDate(GRCComboBox ddlReportPeriod)
        //{
        //    DateTime dtDate;
        //    if (ddlReportPeriod.SelectedValue.Equals(DataDateReturnType.Last3Months.ToString()))
        //    {
        //        dtDate = DateTime.Today.AddMonths(-2);
        //    }
        //    else if (ddlReportPeriod.SelectedValue.Equals(DataDateReturnType.Last6Months.ToString()))
        //    {
        //        dtDate = DateTime.Today.AddMonths(-5);
        //    }
        //    else if (ddlReportPeriod.SelectedValue.Equals(DataDateReturnType.Last9Months.ToString()))
        //    {
        //        dtDate = DateTime.Today.AddMonths(-8);
        //    }
        //    else
        //    {
        //        dtDate = DateTime.Today.AddYears(-1).AddMonths(1);
        //    }

        //    return new DateTime(dtDate.Year, dtDate.Month, 1);
        //}

        private DateTime _GetStartDate(GRCComboBox ddlMonths)
        {
            DateTime dtStartDate;
            if (DateTime.TryParse(ddlMonths.SelectedValue, out dtStartDate))
            {
                return dtStartDate;
            }
            else
            {
                throw new Exception("Start date is not valid");
            }
        }

        private int? _GetCampaignID()
        {
            if (!string.IsNullOrEmpty(ddlCampaign.SelectedValue))
            {
                int ic = Convert.ToInt32(ddlCampaign.SelectedValue);
                if (ic == 0)
                    return null;
                return ic;
            }

            return null;
        }

        private void _LoadCampaignDropdown()
        {
            List<AHAHelpContent.Campaign> list = AHAHelpContent.Campaign.FindAllCampaignsInLast2Years(Utilities.Utility.GetCurrentLanguageID());
            list.Insert(0, new AHAHelpContent.Campaign { Title = "All", CampaignID = 0 });
            ddlCampaign.DataSource = list;
            ddlCampaign.DataTextField = "Title";
            ddlCampaign.DataValueField = "CampaignID";
            ddlCampaign.DataBind();
            ddlCampaign.SelectedValue = string.Empty;
            ddlCampaign.Text = "All";
        }
    }
}