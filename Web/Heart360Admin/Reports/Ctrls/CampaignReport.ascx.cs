﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.Reports.Ctrls
{
    public partial class CampaignReport : System.Web.UI.UserControl, IReportDataManager
    {
        IReportDataManagerReporter m_IReportDataManagerReporter = null;
        public IReportDataManagerReporter IReportDataManagerReporter
        {
            set
            {
                m_IReportDataManagerReporter = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                // If the selected items in the dropdowns don't = Select...
                if (ddlYearStart.SelectedItem.Text != "Select..." && ddlMonthStart.SelectedItem.Text != "Select...")
                {
                    DateTime dt = new DateTime(Convert.ToInt32(ddlYearStart.SelectedValue), Convert.ToInt32(ddlMonthStart.SelectedValue), 1);
                    return dt;
                }
                else
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    return dt;
                }
            }
        }

        public DateTime EndDate
        {
            get
            {
                if (ddlYearEnd.SelectedItem.Text != "Select..." && ddlMonthEnd.SelectedItem.Text != "Select...")
                {
                    DateTime dt = new DateTime(Convert.ToInt32(ddlYearEnd.SelectedValue), Convert.ToInt32(ddlMonthEnd.SelectedValue), 1);
                    return dt;
                }
                else
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    return dt;
                }
            }
        }

        public string StartDateMonth
        {
            get { return ddlMonthStart.SelectedValue.ToString(); }
        }

        public string StartDateYear
        {
            get { return ddlYearStart.SelectedValue.ToString(); }
        }

        public string EndDateMonth
        {
            get { return ddlMonthEnd.SelectedValue.ToString(); }
        }

        public string EndDateYear
        {
            get { return ddlYearEnd.SelectedValue.ToString(); }
        }

        public List<string> MonthNames
        {
            get
            {
                List<string> list = new List<string>();
                DateTime dtEnd = EndDate;
                DateTime dtStart = StartDate;

                while (dtStart <= dtEnd)
                {
                    list.Add(dtStart.ToString("MMM") + ", " + dtStart.ToString("yy"));
                    dtStart = dtStart.AddMonths(1);
                }

                return list;
            }
        }

        public string AffiliateNames
        {
            get
            {

                // Do not use the id of affiliate, use its name (because of ZipRegionTable) 
                return (ddlAffiliate.SelectedItem == null || string.Compare(ddlAffiliate.SelectedItem.Text, strAffiliatesAll) == 0) ? string.Empty : ddlAffiliate.SelectedItem.Text;
                //                return (string.Compare(ddlAffiliate.SelectedItem.Text, strAffiliatesAll) == 0) ? strAffiliatesAll : ddlAffiliate.SelectedValue;
            }
        }

        // Todo: remove
        public string CampaignIDs
        {
            get
            {
                string outStr = string.Empty;

                if (string.Compare(this.strCampaignsAll, ddlCampaign.SelectedItem.Text) == 0)
                {
                    // could use StringBuilder
                    foreach (ListItem item in ddlCampaign.Items)
                    {
                        outStr += (string.IsNullOrEmpty(outStr)) ? item.Value : ("," + item.Value);
                    }
                }
                else
                {
                    outStr = ddlCampaign.SelectedValue;
                }
                return outStr;
            }
        }

        public int? CampaignID
        {
            get
            {
                if (!string.IsNullOrEmpty(ddlCampaign.SelectedValue))
                {
                    int ic = Convert.ToInt32(ddlCampaign.SelectedValue);
                    if (ic == 0)
                        return null;
                    return ic;
                }

                return null;
            }
        }

        public string Markets
        {
            get
            {
                string outStr = string.Empty;

                if (cbOnlyZipcodes.Checked)
                {
                    if (string.Compare(this.strMarketsAll, ddlMarket.SelectedItem.Text) == 0)
                    {
                        // could use StringBuilder
                        // DELIMIT THE LIST BY '|' (pipe)
                        foreach (ListItem item in ddlMarket.Items)
                        {
                            outStr += (string.IsNullOrEmpty(outStr)) ? item.Value : ("|" + item.Value);
                        }
                    }
                    else
                    {
                        outStr = ddlMarket.SelectedValue;
                    }
                }
                return outStr;

            }
        }

        public int? PartnerID
        {
            get { return null; }
        }

        public int? UserTypeID
        {
            get
            {
                return 3;   // Patient/Participant
            }
        }

        public string strAffiliatesAll
        {
            get
            {
                string str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Text_Affiliates_All").ToString();
                return str;
            }
        }

        public string strCampaignsAll
        {
            get
            {
                string str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Text_Campaigns_All").ToString();
                return str;
            }
        }

        public string strMarketsAll
        {
            get
            {
                return GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Text_Markets_All").ToString();
            }
        }

        public string strSelect
        {
            get
            {
                string str = GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Text_Select").ToString();
                return str;
            }
        }

        public int? iHidReset
        {
            get;
            set;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            iHidReset = 1;

            // There is a possibility that there are no Campaigns associated with the current Biz Admin.
            // Check the logic and display the appropriate div.
            Utilities.UserContext uc = Heart360Admin.Utilities.SessionInfo.GetUserContext();
            bool bizAdmin = (uc.RoleID != (int)AHAHelpContent.AdminUserRole.BusinessAdmin) ? false : true;

            bool showReports = true;
            if (bizAdmin)
            {
                DataSet ds = AHAHelpContent.PatientReport.GetBusinessAdminCampaignsForAffiliatesReport(uc.BusinessID);
                if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0 )
                    showReports = false;

            }

            if (!showReports)
            {
                reportSpecification.Visible = false;
                reportNone.Visible = true;
            }
            else
            {
                _LoadYearStartDropdown();
                _LoadYearEndDropdown();
                _LoadCampaignDropdown();

                //ddlAffiliate.Attributes.Add("onchange", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Report_Processing_Message_Updating_Lists").ToString() + "'+ '...');ShowHideTrackerMask(true);");
                //ddlCampaign.Attributes.Add("onchange", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Report_Processing_Message_Updating_Lists").ToString() + "'+ '...');ShowHideTrackerMask(true);");

                btnSubmit.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Report_Processing_Message_Generating_Report").ToString() + "'+ '...');ShowHideTrackerMask(true);");
                btnSubmit.CssClass = "aha-small-button with-arrow";
                btnSubmit.Enabled = true;

                btnReset.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Resetting_Report").ToString() + "'+ '...');ShowHideTrackerMask(true);");
                //btnExcel.Attributes.Add("onclick", "javascript:ShowRequestProcessing('" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Generating_Excel").ToString() + "'+ '...');ShowHideTrackerMask(true);");

                _ResetItems();
            }

            if (!Page.IsPostBack)
            {
                //cbOnlyZipcodes.Checked = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                lblDateValidationMsg.Visible = false;
            }
        }

        private void _ResetItems()
        {
            iHidReset = 1;

            ddlCampaign.ClearSelection();
            ddlAffiliate.Items.Clear();
            ddlMarket.Items.Clear();

            _LoadAffiliateDropdown();
            _LoadMarketDropdown();

        }

        private void _LoadYearStartDropdown()
        {
            int ReportPeriod = Convert.ToInt32(Utilities.AppSettings.ReportPeriod);
            int StartYear = DateTime.Now.Year;

            for (int year = StartYear; year > StartYear - ReportPeriod; year--)
            {
                ddlYearStart.Items.Add(new ListItem(year.ToString(), year.ToString()));
            }

            ddlYearStart.SelectedValue = DateTime.Now.Year.ToString();
            ddlYearStart.Text = DateTime.Now.Year.ToString();
        }

        private void _LoadYearEndDropdown()
        {
            int ReportPeriod = Convert.ToInt32(Utilities.AppSettings.ReportPeriod);
            int StartYear = DateTime.Now.Year;

            for (int year = StartYear; year > StartYear - ReportPeriod; year--)
            {
                ddlYearEnd.Items.Add(new ListItem(year.ToString(), year.ToString()));
            }

            ddlYearEnd.SelectedValue = DateTime.Now.Year.ToString();
            ddlYearEnd.Text = DateTime.Now.Year.ToString();

            // PJB - make the end month be current month
            int endMonth = DateTime.Now.Month;
            ddlMonthEnd.SelectedIndex = endMonth - 1;
        }

        private void _LoadAffiliateDropdown()
        {

            DataSet ds = AHAHelpContent.PatientReport.GetAffiliatesForCampaignsReport(this.CampaignID);

            ddlAffiliate.DataSource = ds;
            ddlAffiliate.DataTextField = "AffiliateName";
            ddlAffiliate.DataValueField = "AffiliateID";
            ddlAffiliate.DataBind();
            if (ddlAffiliate.Items.Count > 1)
                ddlAffiliate.Items.Insert(0, new ListItem(strAffiliatesAll, "0"));
        }

        private void _LoadCampaignDropdown()
        {
            // 1.28.2014 - need to base logic off of current user type
            Utilities.UserContext uc = Heart360Admin.Utilities.SessionInfo.GetUserContext();
            bool bizAdmin = (uc.RoleID != (int)AHAHelpContent.AdminUserRole.BusinessAdmin) ? false : true;
            DataSet ds = (!bizAdmin) ?
                            AHAHelpContent.PatientReport.GetCampaignsForAffiliatesReport(this.AffiliateNames):
                            AHAHelpContent.PatientReport.GetBusinessAdminCampaignsForAffiliatesReport(uc.BusinessID);

            ddlCampaign.DataSource = ds;
            ddlCampaign.DataTextField = "Title";
            ddlCampaign.DataValueField = "CampaignID";
            ddlCampaign.DataBind();

            if( !bizAdmin )
                ddlCampaign.Items.Insert(0, new ListItem(strCampaignsAll, "0"));
        }

        private void _LoadMarketDropdown()
        {
            DataSet ds = AHAHelpContent.PatientReport.GetMarketsForAffiliateReport(this.AffiliateNames);

            ddlMarket.DataSource = ds;
            ddlMarket.DataTextField = "MarketName";
            ddlMarket.DataValueField = "MarketId";
            ddlMarket.DataBind();
            if (ddlMarket.Items.Count > 1)
                ddlMarket.Items.Insert(0, new ListItem(strMarketsAll, "0"));
        }

        public void ddlAffiliate_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            // If an affiliate is selected, this will change what goes in the Markets dropdown
            ddlMarket.Items.Clear();
            _LoadCampaignDropdown();
            _LoadMarketDropdown();
        }

        public void ddlCampaign_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ddlAffiliate.Items.Clear();
            ddlMarket.Items.Clear();


            _LoadAffiliateDropdown();
            _LoadMarketDropdown();
        }

        protected void OnApply(object sender, EventArgs e)
        {
            iHidReset = 2;

            if (validateDateRange())
            {
                if (m_IReportDataManagerReporter != null)
                    m_IReportDataManagerReporter.LoadItems(this);
            }
        }

        protected void OnExcelExport(object sender, EventArgs e)
        {
            if (validateDateRange())
            {
                System.Diagnostics.Debug.WriteLine("Entered into OnExcelExport");
                Utilities.ExportToExcel.ExportPatientReportCampaign(StartDate, EndDate, CampaignIDs, Markets );
                System.Diagnostics.Debug.WriteLine("Exit from ExportPatientReport");
            }
        }

        private bool validateDateRange()
        {
            if (DateTime.Parse(EndDate.ToString()) < DateTime.Parse(StartDate.ToString()))
            {
                lblDateValidationMsg.Text = "Please select end date past start date.";
                lblDateValidationMsg.Visible = true;
                return false;
            }
            else
            {
                lblDateValidationMsg.Visible = false;
                return true;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            _ResetItems();
            Response.Redirect("CampaignReport.aspx");
        }

        protected void cbOnlyZipcodes_Changed(object sender, EventArgs e)
        {
            ddlAffiliate.Enabled = cbOnlyZipcodes.Checked;
            ddlMarket.Enabled = cbOnlyZipcodes.Checked;

        }
    }
}