﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiskStratificationOverTimeReport.ascx.cs"
    Inherits="Heart360Admin.Reports.Ctrls.Patient.RiskStratificationOverTimeReport" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<asp:UpdatePanel ID="updatePanelOutter" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <h1>Population risk stratification over time</h1>
        <div class="validation_summary">
            <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                runat="server" ID="validSummary" ValidationGroup="Create" Enabled="true" ShowSummary="true"
                ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
        </div>
        <asp:CustomValidator ID="cvItemType" Display="None" EnableClientScript="false" ValidationGroup="Create"
            OnServerValidate="cvItemType_ServerValidate" runat="server"></asp:CustomValidator>
        <asp:CustomValidator ID="cvPeriod" Display="None" EnableClientScript="false" ValidationGroup="Create"
            OnServerValidate="cvPeriod_ServerValidate" runat="server"></asp:CustomValidator>
        <div id="reportSpecification" runat="server" class="report-filter">
            <table>
                <tr>
                    <th>
                        Item Type:
                    </th>
                    <td>
                        <GRC:GRCComboBox ID="ddlItemType" Width="100" runat="server">
                        </GRC:GRCComboBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <th>
                        Starting Month
                    </th>
                    <td>
                        <GRC:GRCComboBox ID="ddlMonths" Width="100" runat="server">
                        </GRC:GRCComboBox>
                    </td>
                </tr>
                <tr>
                    <th>
                        Campaign:
                    </th>
                    <td>
                        <GRC:GRCComboBox ID="ddlCampaign" runat="server" Width="100">
                        </GRC:GRCComboBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <th style="vertical-align: top">
                        Provider Name:
                    </th>
                    <td>
                        <div>
                            <asp:Literal ID="litProviderName" runat="server"></asp:Literal></div>
                        <asp:LinkButton ID="lnkSearchProvider" OnClick="lnkSearchProvider_Click" runat="server">Select provider</asp:LinkButton>
                        <asp:LinkButton ID="lnkClearProvider" OnClick="lnkClearProvider_Click" runat="server">Clear provider</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <th>
                        &nbsp;
                    </th>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <td>
                            <asp:LinkButton ID="lnkGenerateReport" CssClass="aha-small-button with-arrow" OnClick="lnkGenerateReport_Click"
                                runat="server"><strong><em>Generate Report</em></strong></asp:LinkButton>
                        </td>
                    </td>
                    <th>
                        &nbsp;
                    </th>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div id="reportNone" runat="server" class="report-filter" visible="false">
            <ul style="margin-left: -20px;">
                <li>There are no Campaigns associated with your Business.</li>
                <li>Therefore, this report would be empty.</li>
            </ul>
        </div>
        <div id="DimmerPlaceholder" class="popup">
            <div>
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table class="admin-box" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkFeedbackClose" OnClientClick="javascript:hideProviderSearchPopup();"
                                        CssClass="right closelink"></asp:LinkButton>
                                    <h2 class="red">
                                        Search a provider</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="report-filter">
                                        <div class="validation_summary">
                                            <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                                                runat="server" ID="ValidationSummary1" ValidationGroup="SearchProvider" Enabled="true"
                                                ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
                                        </div>
                                        <asp:CustomValidator ID="cvSearchProvider" Display="None" EnableClientScript="false"
                                            ValidationGroup="SearchProvider" OnServerValidate="cvSearchProvider_ServerValidate"
                                            runat="server"></asp:CustomValidator>
                                        <table>
                                            <tr>
                                                <th>
                                                    <strong>First Name:</strong>
                                                    <br />
                                                    <%--<asp:TextBox ID="txtSearchProvider" runat="server"></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                                </th>
                                                <th>
                                                    <strong>Last Name:</strong>
                                                    <br />
                                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                                </th>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <strong>Practice Name:</strong>
                                                    <br />
                                                    <asp:TextBox ID="txtPracticeName" runat="server"></asp:TextBox>
                                                </th>
                                                <th>
                                                    <strong>Provider Code:</strong>
                                                    <br />
                                                    <asp:TextBox ID="txtProviderCode" MaxLength="6" runat="server"></asp:TextBox>
                                                </th>
                                                <td>
                                                    <br />
                                                    <asp:LinkButton ID="lnkSearch" OnClick="lnkSearch_Click" CssClass="aha-small-button with-arrow"
                                                        runat="server"><strong><em>Search</em></strong></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvProviders" ShowHeader="true" Width="100%" EnableViewState="true"
                                        AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="No providers was found"
                                        AllowPaging="True" PageSize="6" ShowFooter="false" OnRowDataBound="gvProviders_RowDataBound"
                                        OnPageIndexChanging="gvProviders_PageIndexChanging" GridLines="Horizontal" CssClass="provider-grid"
                                        PagerStyle-CssClass="table-page-index" runat="server">
                                        <Columns>
                                            <asp:TemplateField Visible="True" HeaderText="Provider name" HeaderStyle-CssClass="alignleft">
                                                <ItemTemplate>
                                                    <asp:Literal ID="litProviderName" runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="True" HeaderText="" HeaderStyle-CssClass="alignleft">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSelect" OnClick="lnkSelect_Click" runat="server">Select</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="hddnProviderID" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="updatePanelReport" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <asp:PlaceHolder ID="plcReport" Visible="false" runat="server">
            <table width="100%" cellpadding="3" cellspacing="0">
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Chart ID="chtRiskStratification" runat="server">
                                    </asp:Chart>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="3" cellspacing="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <asp:Repeater ID="rptMonth" OnItemDataBound="rptMonth_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <th>
                                            <asp:Literal ID="litMonth" runat="server"></asp:Literal>
                                        </th>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <tr>
                                <th align="left" colspan="<%=intColoumnCount %>">
                                    <hr />
                                    Number of people
                                    <hr />
                                </th>
                            </tr>
                            <tr>
                                <th align="left">
                                    <%=strLowRisk%>
                                </th>
                                <asp:Repeater ID="rptLowRisk" OnItemDataBound="Value_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%--<asp:Literal ID="litLowRisk" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <tr>
                                <th align="left">
                                    <%=strMedium%>
                                </th>
                                <asp:Repeater ID="rptMedium" OnItemDataBound="Value_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%--<asp:Literal ID="litMedium" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <tr>
                                <th align="left">
                                    <%=strHigh%>
                                </th>
                                <asp:Repeater ID="rptHigh" OnItemDataBound="Value_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%--<asp:Literal ID="litHigh" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <!--
                    <asp:Repeater ID="rptVeryHigh" OnItemDataBound="Value_ItemDataBound" runat="server">
                        <HeaderTemplate>
                            <tr>
                                <th align="left">
                                    <%=strVeryHigh%>
                                </th>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <td>
                                <%--<asp:Literal ID="litVeryHigh" runat="server"></asp:Literal>--%>
                                 <asp:Literal ID="litValue" runat="server"></asp:Literal>
                            </td>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tr></FooterTemplate>
                    </asp:Repeater>-->
                            <tr>
                                <th align="left">
                                    <%=strInActiveUsers%>
                                </th>
                                <asp:Repeater ID="rptInActiveUsers" OnItemDataBound="Value_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%--<asp:Literal ID="litHigh" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <tr>
                                <th align="left">
                                    Total
                                </th>
                                <asp:Repeater ID="rptTotal" OnItemDataBound="Value_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%--<asp:Literal ID="litTotal" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <tr>
                                <th align="left" colspan="<%=intColoumnCount %>">
                                    <hr />
                                    Percent
                                    <hr />
                                </th>
                            </tr>
                            <tr>
                                <th align="left">
                                    <%=strLowRisk%>
                                </th>
                                <asp:Repeater ID="rptLowRiskPerc" OnItemDataBound="ValuePerc_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%--<asp:Literal ID="litLowRiskPerc" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <tr>
                                <th align="left">
                                    <%=strMedium%>
                                </th>
                                <asp:Repeater ID="rptMediumPerc" OnItemDataBound="ValuePerc_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%--<asp:Literal ID="litMediumPerc" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <tr>
                                <th align="left">
                                    <%=strHigh%>
                                </th>
                                <asp:Repeater ID="rptHighPerc" OnItemDataBound="ValuePerc_ItemDataBound" runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%-- <asp:Literal ID="litHighPerc" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                            <!--
                    <asp:Repeater ID="rptVeryHighPerc" OnItemDataBound="ValuePerc_ItemDataBound"
                        runat="server">
                        <HeaderTemplate>
                            <tr>
                                <th align="left">
                                    <%=strVeryHigh%>
                                </th>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <td>
                                <%--<asp:Literal ID="litVeryHighPerc" runat="server"></asp:Literal>--%>
                                 <asp:Literal ID="litValue" runat="server"></asp:Literal>
                            </td>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tr></FooterTemplate>
                    </asp:Repeater>-->
                            <tr>
                                <th align="left">
                                    <%=strInActiveUsers%>
                                </th>
                                <asp:Repeater ID="rptInActiveUsersPerc" OnItemDataBound="ValuePerc_ItemDataBound"
                                    runat="server">
                                    <ItemTemplate>
                                        <td>
                                            <%-- <asp:Literal ID="litHighPerc" runat="server"></asp:Literal>--%>
                                            <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:PlaceHolder>
        <div id="divNoRecords" visible="false" runat="server">
            <strong>No Data to display</strong>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript" language="javascript">
    function showProviderSearchPopup() {
        //setTimeout("showpopup('DimmerPlaceholder');", 200);
        setTimeout("ShowGenericConfirmationDiv('DimmerPlaceholder', 500, 500);", 200);
        return false;
    }

    function hideProviderSearchPopup() {
        HideGenericConfirmationDiv('DimmerPlaceholder');
        return false;
    }
</script>

