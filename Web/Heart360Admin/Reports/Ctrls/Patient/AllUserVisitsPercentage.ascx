﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AllUserVisitsPercentage.ascx.cs" Inherits="Heart360Admin.Reports.Ctrls.Patient.AllUserVisitsPercentage" %>
<%@ Register src="../Common/CompositeReportCtrl.ascx" tagname="CompositeReportCtrl" tagprefix="uc1" %>
<uc1:CompositeReportCtrl Cell00Name="Range" GraphYText="% user(s) visited" ID="ucCompositeReportCtrl" runat="server" />