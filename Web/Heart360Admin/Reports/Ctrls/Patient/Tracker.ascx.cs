﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Heart360Admin.Reports.Ctrls.Patient
{
    public partial class Tracker : System.Web.UI.UserControl, IReportDataManagerReporter
    {
        
        public void LoadItems( IReportDataManager iRdm )
        {
            using (DataSet ds = AHAHelpContent.PatientReport.GetTrackerReport(iRdm.StartDate, iRdm.EndDate, null))
            {
                ucCompositeReportCtrl.LoadItems(ds.Tables[0], iRdm.MonthNames.ToArray());
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}