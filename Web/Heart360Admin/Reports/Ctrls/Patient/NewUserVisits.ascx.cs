﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Heart360Admin.Reports.Ctrls.Patient
{
    public partial class NewUserVisits : System.Web.UI.UserControl
    {
        IReportDataManager m_IReportDataManager = null;
        public IReportDataManager IReportDataManager
        {
            set
            {
                m_IReportDataManager = value;
            }
        }

        public void LoadItems(ReportFilter filter)
        {
            using (DataSet ds = AHAHelpContent.PatientReport.GetNewUserVisitsReport(m_IReportDataManager.StartDate, m_IReportDataManager.EndDate, m_IReportDataManager.PartnerID))
            {
                if (filter != null)
                {
                    using (DataTable dtTemp = ds.Tables[0].Clone())
                    {
                        DataRow newRow = dtTemp.NewRow();
                        newRow.ItemArray = ds.Tables[0].Rows[filter.FilterIndex].ItemArray;
                        dtTemp.Rows.Add(newRow);
                        ucCompositeReportCtrl.LoadItems(dtTemp, m_IReportDataManager.MonthNames.ToArray());
                    }
                }
                else
                {
                    ucCompositeReportCtrl.LoadItems(ds.Tables[0], m_IReportDataManager.MonthNames.ToArray());
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}