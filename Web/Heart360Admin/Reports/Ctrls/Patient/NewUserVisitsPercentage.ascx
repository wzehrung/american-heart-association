﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewUserVisitsPercentage.ascx.cs" Inherits="Heart360Admin.Reports.Ctrls.Patient.NewUserVisitsPercentage" %>
<%@ Register src="../Common/CompositeReportCtrl.ascx" tagname="CompositeReportCtrl" tagprefix="uc1" %>
<uc1:CompositeReportCtrl Cell00Name="Range" GraphYText="% user(s) visited" ID="ucCompositeReportCtrl" runat="server" />