﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientHealthReport.ascx.cs"
    Inherits="Heart360Admin.Reports.Ctrls.Patient.PatientHealthReport" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<asp:UpdatePanel ID="updatePanelOutter" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <h1>Risk stratification report</h1>
        <div id="reportSpecification" runat="server" class="report-filter">
            <div class="validation_summary">
                <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                    runat="server" ID="validSummary" ValidationGroup="CreateReport" Enabled="true"
                    ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
            </div>
            <asp:CustomValidator ID="cvItemType" Display="None" EnableClientScript="false" ValidationGroup="CreateReport"
                OnServerValidate="cvItemType_ServerValidate" runat="server"></asp:CustomValidator>
            <asp:CustomValidator ID="cvStartDate" Display="None" EnableClientScript="false" ValidationGroup="CreateReport"
                OnServerValidate="cvStartDate_ServerValidate" runat="server"></asp:CustomValidator>
            <asp:CustomValidator ID="cvEndDate" Display="None" EnableClientScript="false" ValidationGroup="CreateReport"
                OnServerValidate="cvEndDate_ServerValidate" runat="server"></asp:CustomValidator>
            <table>
                <tr>
                    <th>
                        Starting Period:
                    </th>
                    <td colspan="3" align="left">
                        <GRC:GRCDatePicker Width="70" ID="dtStartDate" runat="server" CBReadOnly="true">
                        </GRC:GRCDatePicker>
                    </td>
                    <th>
                        Ending Period:
                    </th>
                    <td>
                        <GRC:GRCDatePicker Width="70" ID="dtEndDate" runat="server" CBReadOnly="true">
                        </GRC:GRCDatePicker>&nbsp;&nbsp;<asp:LinkButton ID="lnkClearEndDate" OnClick="lnkClearEndDate_Click"
                            runat="server">Clear end date</asp:LinkButton>&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <th>
                        Item Type:
                    </th>
                    <td>
                        <GRC:GRCComboBox ID="ddlItemType" Width="100" runat="server">
                        </GRC:GRCComboBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <th>
                        Campaign:
                    </th>
                    <td>
                        <GRC:GRCComboBox ID="ddlCampaign" runat="server" Width="100">
                        </GRC:GRCComboBox>
                    </td>
                </tr>
                <tr>
                    <th>
                        Gender:
                    </th>
                    <td>
                        <asp:RadioButton ID="rdMale" Text="Male" GroupName="gender" runat="server" />
                        <asp:RadioButton ID="rdFemale" Text="Female" GroupName="gender" runat="server" />
                        <asp:RadioButton ID="rdBoth" Text="Both" GroupName="gender" runat="server" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <th style="vertical-align: top">
                        Provider Name:
                    </th>
                    <td>
                        <div>
                            <asp:Literal ID="litProviderName" runat="server"></asp:Literal></div>
                        <asp:LinkButton ID="lnkSearchProvider" OnClick="lnkSearchProvider_Click" runat="server">Select provider</asp:LinkButton>
                        <asp:LinkButton ID="lnkClearProvider" OnClick="lnkClearProvider_Click" runat="server">Clear provider</asp:LinkButton>
                    </td>
                </tr>
                <tr id="trActiveFilter" runat="server">
                    <td colspan="6" align="left">
                        <asp:CheckBox ID="chkActiveFilter" Text="filter by those participants that were active during the starting and ending periods."
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <asp:LinkButton ID="lnkGenerateReport" CssClass="aha-small-button with-arrow" OnClick="lnkGenerateReport_Click"
                            runat="server"><strong><em>Generate Report</em></strong></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <div id="reportNone" runat="server" class="report-filter" visible="false">
            <ul style="margin-left: -20px;">
                <li>There are no Campaigns associated with your Business.</li>
                <li>Therefore, this report would be empty.</li>
            </ul>
        </div>
        <div id="DimmerPlaceholder" class="popup">
            <div>
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table class="admin-box" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkFeedbackClose" OnClientClick="javascript:hideProviderSearchPopup();"
                                        CssClass="right closelink"></asp:LinkButton>
                                    <h2 class="red">
                                        Search a provider</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="report-filter">
                                        <div class="validation_summary">
                                            <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                                                runat="server" ID="ValidationSummary1" ValidationGroup="SearchProvider" Enabled="true"
                                                ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
                                        </div>
                                        <asp:CustomValidator ID="cvSearchProvider" Display="None" EnableClientScript="false"
                                            ValidationGroup="SearchProvider" OnServerValidate="cvSearchProvider_ServerValidate"
                                            runat="server"></asp:CustomValidator>
                                        <table>
                                            <tr>
                                                <th>
                                                    <strong>First Name:</strong>
                                                    <br />
                                                    <%--<asp:TextBox ID="txtSearchProvider" runat="server"></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                </td>
                                <th>
                                    <strong>Last Name:</strong>
                                    <br />
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <br />
                                        <asp:LinkButton ID="lnkSearch" OnClick="lnkSearch_Click" CssClass="aha-small-button with-arrow"
                                            runat="server"><strong><em>Search</em></strong></asp:LinkButton>
                                    </td>
                            </tr>
                        </table>
                        </div> </td> </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvProviders" ShowHeader="true" Width="100%" EnableViewState="true"
                                    AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="No providers was found"
                                    AllowPaging="True" PageSize="6" ShowFooter="false" OnRowDataBound="gvProviders_RowDataBound"
                                    OnPageIndexChanging="gvProviders_PageIndexChanging" GridLines="Horizontal" CssClass="provider-grid"
                                    PagerStyle-CssClass="table-page-index" runat="server">
                                    <Columns>
                                        <asp:TemplateField Visible="True" HeaderText="Provider name" HeaderStyle-CssClass="alignleft">
                                            <ItemTemplate>
                                                <asp:Literal ID="litProviderName" runat="server"></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="True" HeaderText="" HeaderStyle-CssClass="alignleft">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSelect" OnClick="lnkSelect_Click" runat="server">Select</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        </table>
                        <asp:HiddenField ID="hddnProviderID" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <asp:PlaceHolder ID="plcReportWithEndDate" Visible="false" runat="server">
            <br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3" align="center">
                        <h3>
                            <asp:Literal ID="litReportWithEndDateHeading" runat="server"></asp:Literal></h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <strong>Starting Period:<asp:Literal ID="litStartingPeriod" runat="server"></asp:Literal></strong>
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Chart ID="chtStartingPeriod" runat="server">
                                    </asp:Chart>
                                </td>
                            </tr>
                        </table>
                        <asp:Repeater ID="rptStartingPeriod" OnItemDataBound="HealthStatus_ItemDataBound"
                            runat="server">
                            <HeaderTemplate>
                                <table width="80%" border="0" cellpadding="0" class="admin-box provider-grid" cellspacing="0">
                                    <tr>
                                        <th>
                                            Gender
                                        </th>
                                        <th>
                                            Healthy
                                        </th>
                                        <th>
                                            Acceptable
                                        </th>
                                        <th>
                                            At Risk
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td align="center">
                                        <asp:Literal ID="litGender" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litHealthy" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblHealthyPerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAcceptable" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAcceptablePerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAtRisk" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAtRiskPerc" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td align="center">
                        <strong>Ending Period:<asp:Literal ID="litEndingPeriod" runat="server"></asp:Literal></strong>
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Chart ID="chtEndingPeriod" runat="server">
                                    </asp:Chart>
                                </td>
                            </tr>
                        </table>
                        <asp:Repeater ID="rptEndingPeriod" OnItemDataBound="HealthStatus_ItemDataBound" runat="server">
                            <HeaderTemplate>
                                <table width="80%" class="admin-box provider-grid" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>
                                            Gender
                                        </th>
                                        <th>
                                            Healthy
                                        </th>
                                        <th>
                                            Acceptable
                                        </th>
                                        <th>
                                            At Risk
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td align="center">
                                        <asp:Literal ID="litGender" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litHealthy" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblHealthyPerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAcceptable" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAcceptablePerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAtRisk" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAtRiskPerc" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <strong>Change between starting and ending periods</strong>
                        <asp:Repeater ID="rptPeriodDifference" OnItemDataBound="HealthStatus_ItemDataBound"
                            runat="server">
                            <HeaderTemplate>
                                <table class="admin-box provider-grid" width="50%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>
                                            Gender
                                        </th>
                                        <th>
                                            Healthy
                                        </th>
                                        <th>
                                            Acceptable
                                        </th>
                                        <th>
                                            At Risk
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td align="center">
                                        <asp:Literal ID="litGender" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litHealthy" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblHealthyPerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAcceptable" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAcceptablePerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAtRisk" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAtRiskPerc" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcReportWithOnlyStartDate" Visible="false" runat="server">
            <br />
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Literal ID="litReportWithOnlyStartDateHeading" runat="server"></asp:Literal></h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <strong>Starting Period:<asp:Literal ID="litOnlyStartingPeriod" runat="server"></asp:Literal></strong>
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Chart ID="chtOnlyStarting" runat="server">
                                    </asp:Chart>
                                </td>
                            </tr>
                        </table>
                        <asp:Repeater ID="rptOnlyStartingPeriod" OnItemDataBound="HealthStatus_ItemDataBound"
                            runat="server">
                            <HeaderTemplate>
                                <table width="50%" class="admin-box provider-grid" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>
                                            Gender
                                        </th>
                                        <th>
                                            Healthy
                                        </th>
                                        <th>
                                            Acceptable
                                        </th>
                                        <th>
                                            At Risk
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td align="center">
                                        <asp:Literal ID="litGender" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litHealthy" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblHealthyPerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAcceptable" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAcceptablePerc" runat="server"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAtRisk" runat="server"></asp:Literal><br />
                                        <asp:Label ID="lblAtRiskPerc" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </asp:PlaceHolder>
        <table id="tblNoRecords" visible="false" runat="server" width="100%">
            <tr>
                <td align="center">
                    <strong>No Data to display</strong>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript" language="javascript">
    function showProviderSearchPopup() {
        //setTimeout("showpopup('DimmerPlaceholder');", 200);
        setTimeout("ShowGenericConfirmationDiv('DimmerPlaceholder', 500, 500);", 200);
        return false;
    }

    function hideProviderSearchPopup() {
        HideGenericConfirmationDiv('DimmerPlaceholder');
        return false;
    }

    function onEndDateChange() {
        //var txtEndDate = document.getElementById("<%=ClientID %>_dtEndDate_CBTextBoxID");
        var lnkClearEndDate = document.getElementById("<%=lnkClearEndDate.ClientID %>");
        var trActiveFilter = document.getElementById("<%=trActiveFilter.ClientID %>");
        lnkClearEndDate.style.display = "";
        trActiveFilter.style.display = "";
    }
</script>

