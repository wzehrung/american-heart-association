﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;

namespace Heart360Admin.Reports.Ctrls.Common
{
    public partial class CompositeReportCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        string __Cell00Name = "&nbsp;";
        public string Cell00Name
        {
            get
            {
                return __Cell00Name;
            }
            set
            {
                __Cell00Name = value;
            }
        }

        string __GraphYText = "entries";
        public string GraphYText
        {
            get
            {
                return __GraphYText;
            }
            set
            {
                __GraphYText = value;
            }
        }

        string __strOuterTableClassName = "report-table";
        
        string __strMonthFirstCellClassName = "first-cell";
        string __strMonthCellClassName = "";
        string __strMonthLastCellClassName = "last-cell";

        string __strDataRowClassName = "";
        string __strDataAlternatingRowClassName = "alter";
        string __strDataFirstCellClassName = "";
        string __strDataCellClassName = "";
        string __strCellGraphIconClassName = "last-cell";
        
        string __strChartCellOneClassName = "";
        string __strChartCellClassName = "";
        string __strChartLastCellClassName = "last-cell";

 //       string __strGraphIcon = "/images/graph-icon.gif";
       

        public void LoadItems(DataTable dtSource, string[] xVal)
        {
            ph.Controls.Clear();

            //this is the outer table which houses the following:
            //table row of heading, 
            //table row of data, a cell containing the graph icon
            //table row containing the actual graph
            Table tOuter = new Table();
            tOuter.CssClass = __strOuterTableClassName;
            tOuter.Style.Add(HtmlTextWriterStyle.Width, "100%");
            tOuter.CellPadding = 0;
            tOuter.CellSpacing = 0;
            //tOuter.GridLines = GridLines.Both;            
            ph.Controls.Add(tOuter);
            int iRow = 0;

            TableRow trMonth = new TableRow();
            TableHeaderCell tdMonthCellOne = new TableHeaderCell();
            tdMonthCellOne.CssClass = __strMonthFirstCellClassName;
            Literal litMonthCellOne = new Literal();
            litMonthCellOne.Text = __Cell00Name;
            tdMonthCellOne.Controls.Add(litMonthCellOne);
            trMonth.Cells.Add(tdMonthCellOne);

            foreach (string str in xVal)
            {
                TableHeaderCell td = new TableHeaderCell();
                td.CssClass = __strMonthCellClassName;
                Literal litMonth = new Literal();
                litMonth.Text = str;
                td.Controls.Add(litMonth);
                trMonth.Cells.Add(td);
            }

            TableCell tdMonthLastCell = new TableCell();
            tdMonthLastCell.CssClass = __strMonthLastCellClassName;
            trMonth.Cells.Add(tdMonthLastCell);

            tOuter.Rows.Add(trMonth);


            foreach (DataRow dr in dtSource.Rows)
            {
                TableRow trData = new TableRow();
                if (iRow % 2 == 0)
                {
                    trData.CssClass = __strDataRowClassName;
                }
                else
                {
                    trData.CssClass = __strDataAlternatingRowClassName;
                }

                double[] yVal = new double[dtSource.Columns.Count - 1];
                int iCount = 0;
                int index = 0;
                string strChartRowID = string.Format("{0}_{1}_ChartRow", this.ID, iRow.ToString());
                foreach (DataColumn dc in dtSource.Columns)
                {
                    TableCell tdData = new TableCell();
                    Literal litData = new Literal();
                    litData.Text = dr[dc].ToString();
                    tdData.Controls.Add(litData);


                    if (index > 0)
                    {
                        if (dr[dc] != null && dr[dc].ToString() != string.Empty)
                        {
                            yVal[iCount] = Convert.ToDouble(dr[dc].ToString().Replace("%", string.Empty));
                        }
                        else
                        {
                            yVal[iCount] = 0;
                        }
                        iCount++;
                        tdData.CssClass = __strDataCellClassName;
                        tdData.HorizontalAlign = HorizontalAlign.Center;
                    }
                    else
                    {
                        //first cell of the datarow
                        tdData.CssClass = __strDataFirstCellClassName;
                    }
                    

                    trData.Cells.Add(tdData);

                    index++;
                }

                TableCell tdCellGraphIcon = new TableCell();
                tdCellGraphIcon.CssClass = __strCellGraphIconClassName;
                tdCellGraphIcon.Attributes.Add("class", __strCellGraphIconClassName);
                //first column
                HtmlAnchor linkGraph = new HtmlAnchor();
                linkGraph.HRef = "javascript:;";
                //linkGraph.InnerHtml = string.Format("<img src=\"{0}\" border=\"0\">",__strGraphIcon);
                linkGraph.Attributes.Add("onclick", string.Format("ShowHideReportGraph('{0}','{1}');", strChartRowID, this.ClientID));
                tdCellGraphIcon.Controls.Add(linkGraph);
                trData.Cells.Add(tdCellGraphIcon);

                tOuter.Rows.Add(trData);

                TableRow trChart = new TableRow();
                trChart.Style.Add(HtmlTextWriterStyle.Display, "none");
                trChart.ID = strChartRowID;
                TableCell tdChartCellOne = new TableCell();
                tdCellGraphIcon.CssClass = __strChartCellOneClassName;
                trChart.Cells.Add(tdChartCellOne);

                TableCell tdChart = new TableCell();
                tdChart.CssClass = __strChartCellClassName;
                tdChart.ColumnSpan = dtSource.Columns.Count - 1;
                LineGraphCtrl lineGraph = LoadControl("~/Reports/Ctrls/Common/LineGraphCtrl.ascx") as LineGraphCtrl;
                lineGraph.ID = string.Format("{0}_{1}_Line", this.ID, iRow.ToString());
                lineGraph.GraphYText = __GraphYText;
                lineGraph.XVal = xVal;
                lineGraph.YVal = yVal;

                tdChart.Controls.Add(lineGraph);
                trChart.Cells.Add(tdChart);

                TableCell tdChartLastCell = new TableCell();
                tdChartLastCell.CssClass = __strChartLastCellClassName;
                trChart.Cells.Add(tdChartLastCell);

                tOuter.Rows.Add(trChart);

                lineGraph.LoadChart();

                iRow++;
            }

            dtSource.Dispose();
        }
    }
}