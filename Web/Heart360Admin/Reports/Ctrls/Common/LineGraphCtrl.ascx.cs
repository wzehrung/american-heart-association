﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

namespace Heart360Admin.Reports.Ctrls.Common
{
    public partial class LineGraphCtrl : System.Web.UI.UserControl
    {
        string __GraphYText = "entries";
        public string GraphYText
        {
            get
            {
                return __GraphYText;
            }
            set
            {
                __GraphYText = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Chart1.Series["Series1"].Points.DataBindXY(xVal, yVal);
            //Chart1.Series["Series1"].ToolTip = string.Format("#VALY {0} in the month of #VALX.", __GraphYText);
            //Chart1.Series["Series1"]["DrawingStyle"] = "Default";
        }

        string[] xVal;
        public string[] XVal
        {
            get
            {
                return xVal;
            }
            set
            {
                xVal = value;
            }
        }

        double[] yVal;
        public double[] YVal
        {
            get
            {
                return yVal;
            }
            set
            {
                yVal = value;
            }
        }

        public void LoadChart()
        {

        }
    }
}