﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin
{
    public partial class FileNotFound : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, System.EventArgs e)
        {
            //if (!Utilities.Utility.IsAdminLoggedin())
            //{
            //    this.MasterPageFile = "~/MasterPages/Home.Master";
            //}
            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                this.MasterPageFile = "~/MasterPages/Home.Master";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.Response.StatusCode = 404;
            //if (!Utilities.Utility.IsAdminLoggedin())
            //{
            //    content.Attributes.Add("class", "content");
            //}
            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                content.Attributes.Add("class", "content");
            }
        }
    }
}
