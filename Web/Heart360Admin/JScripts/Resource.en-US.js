﻿var strAppError = "Oops!. There seems to be an issue, please take the reading again!";
var strBpProgress = "Blood Pressure reading {0}% complete";
var strStarting = "Starting...";
var strCheckingServiceAttempt = "Checking Service...attempt ({0})";
var strStartConnectingToDevice = "Starting to connect to device...";
var strServiceNotInstalled = "Heart360 BP monitor service is not installed, please install the service and take the reading again.";
var strConnectingToDeviceAttempt = "Connecting to device...attempt ({0})";
var strWaitingForDevice = "Waiting for the device to get ready...attempt ({0})";
var strGettingMeasurement = "Getting measurement";
var strDeviceNotDetected = "Heart360 BP monitor service was unable to detect the device, please make sure the device is connected properly and take the reading again.";
var strMonitorServiceTimeout = "Heart360 BP monitor service did not respond in a timely fashion. Please take the reading again.";

var weekday = new Array("S", "M", "T", "W", "T", "F", "S");

var monthArray = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

var weekday_names = new Array(7);
weekday_names[0] = "Sunday";
weekday_names[1] = "Monday";
weekday_names[2] = "Tuesday";
weekday_names[3] = "Wednesday";
weekday_names[4] = "Thursday";
weekday_names[5] = "Friday";
weekday_names[6] = "Saturday";

var monthname = new Array(12);
monthname[1] = "January";
monthname[2] = "February";
monthname[3] = "March";
monthname[4] = "April";
monthname[5] = "May";
monthname[6] = "June";
monthname[7] = "July";
monthname[8] = "August";
monthname[9] = "September";
monthname[10] = "October";
monthname[11] = "November";
monthname[12] = "December";

var STR_SMALL_AM = "am";
var STR_SMALL_PM = "pm";
var strProcessing = "Processing...";
var strMLCDownload = "Please wait while we download your My Life Check Report...";
var strDays = "days";
var strDay = "day";
var strClose = "Close";