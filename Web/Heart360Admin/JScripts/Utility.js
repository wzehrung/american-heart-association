﻿
/*Start Update Panel - Progress Helper*/
function _CreateFloatingContainerForMask(id) {
    //alert("hai");
    var tOuter = document.createElement("table");
    tOuter.id = 'table_' + id;
    tOuter.className = "tableAjaxLoader";

    tbodyOuter = document.createElement("tbody");

    var tRow = document.createElement("tr");
    var tCell = document.createElement("td");

    var img = document.createElement("img");
    img.src = "/images/ajax-loader.gif";

    tCell.appendChild(img);
    tRow.appendChild(tCell);

    tbodyOuter.appendChild(tRow);
    tOuter.appendChild(tbodyOuter);

    return tOuter;
}

function ShowHideDivTrackerMask(idDivToMark, bShow) {
    //alert(idDivToMark);

    var floatingContainer = null;
    var divToMask = document.getElementById(idDivToMark);
    if (divToMask == null) {
        throw "no element with id = " + idDivToMark + " found!";
        return false;
    }

    if (!bShow) {
        /*    floatingContainer = document.getElementById('table_' + idDivToMark);
        if (floatingContainer != null) {
        divToMask.removeChild(floatingContainer);
        }*/
    }
    else {
        floatingContainer = _CreateFloatingContainerForMask(idDivToMark);

        floatingContainer.style.left = findPosX(divToMask) + "px";
        floatingContainer.style.top = findPosY(divToMask) + "px";
        floatingContainer.style.width = divToMask.offsetWidth + "px";
        floatingContainer.style.height = divToMask.offsetHeight + "px";
        floatingContainer.style.display = "";
        divToMask.appendChild(floatingContainer);
    }
}
/*End Update Panel - Progress*/

/*For popup*/
function setItToCenter(obj) {
    try {
        var oW = obj.clientWidth;
        var bW = getPageSize()[2];

        var oH = obj.clientHeight;
        var bH = getPageSize()[3];


        var vscroll = document.documentElement.scrollTop;

        var screensize = (document.body.clientHeight) ? document.body.clientHeight : document.body.scrollHeight;


        if ((oH + 50) > screensize || !IsBrowserFixedPositionCompatible()) {

            obj.style.position = "absolute";
            obj.style.top = vscroll + 50 + "px";
        } else {
            obj.style.position = "fixed";
            obj.style.top = ((bH - oH) / 2) + "px";
        }



        obj.style.left = ((bW - oW) / 2) + "px";

        dimmer.resize();


        //obj.style.display = "block";
    }
    catch (e) {
    }
}
function showpopup(DivId) {
    //alert(DivId);
    var obj = document.getElementById(DivId);
    obj.style.display = "block";
    dimmer.show();
}


function hidepopup(DivId) {
    document.getElementById(DivId).style.display = "none";
    dimmer.hide();
}
var isDimmer = false;
var isPopup = false;
var currentPopupObj;
dimmer = {
    style: ["top:0px", "left:0px", "position:absolute", "zIndex:9998", "backgroundColor:#bbb", "opacity:.5", "filter:alpha(opacity=50)", "width:100%"],
    show: function() {
        var tDimmer = document.getElementById("dynamicdimmer");
        if (!tDimmer) {
            oDimmer = document.createElement('div');
            for (var i = 0; i < this.style.length; i++) {
                var selector = this.style[i].split(":");
                oDimmer.style[selector[0]] = selector[1];
            }
            oDimmer.id = "dynamicdimmer";
            (document.body.firstChild) ? document.body.insertBefore(oDimmer, document.body.firstChild) : document.body.appendChild(oDimmer); isDimmer = true;
            isDimmer = true;
            this.resize();

        }

    },
    hide: function() {
        var tDimmer = document.getElementById("dynamicdimmer");
        if (tDimmer) document.body.removeChild(tDimmer);
        isDimmer = false;
    },
    resize: function() {
        var tDimmer = document.getElementById("dynamicdimmer");
        if (tDimmer) {
            var viewHeight = (document.documentElement.scrollHeight) ? document.documentElement.scrollHeight : document.documentElement.clientHeight;
            tDimmer.style.height = viewHeight + "px";
        }
    }
};
/*End For popup*/


/*Start Z-Index Manager*/
var AHAMaxZIndex = 7000;
var PageScrollY = 0;
/*End Z-Index Manager*/

/*
This function is used for auto tab functionality in date control.
Parameters - textbox control, length of the textbox,event.
*/
var isNN = (navigator.appName.indexOf("Netscape") != -1);
function autoTab(input, len, e) {
    var keyCode = (isNN) ? e.which : e.keyCode;
    var filter = (isNN) ? [0, 8, 9] : [0, 8, 9, 16, 17, 18, 37, 38, 39, 40, 46];
    if (input.value.length >= len && !containsElement(filter, keyCode)) {
        input.value = input.value.slice(0, len);
        input.form[(getIndex(input) + 1) % input.form.length].focus();
    }
    function containsElement(arr, ele) {
        var found = false, index = 0;
        while (!found && index < arr.length)
            if (arr[index] == ele)
            found = true;
        else
            index++;
        return found;
    }
    function getIndex(input) {
        var index = -1, i = 0, found = false;
        while (i < input.form.length && index == -1)
            if (input.form[i] == input) index = i;
        else i++;
        return index;
    }
    return true;
}

function GetUserDate() {
    var dt = new Date();
    //debugger;
    return dt.getDate() + ":" + (dt.getMonth() + 1) + ":" + dt.getFullYear();
}

function OpenPrinterFriendlyPage(url) {
    window.open(url, '_print', 'height=750,width=770,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no,top=100,left=100');
    return false;
}

function HideEditDiv() {
    var element = document.getElementById("divPopupContainer");
    element.innerHTML = "";
    element.style.display = "none";
    document.getElementById('divMask').style.display = "none";
    return false;
}

function HideHeightDiv() {
    var element = document.getElementById("divPopupContainer");
    element.innerHTML = "";
    element.style.display = "none";
    document.getElementById('divMask').style.display = "none";
    return false;
}

function IsBrowserFixedPositionCompatible() {
    if (navigator.appName != "Microsoft Internet Explorer" || navigator.appVersion.indexOf("MSIE 7.") != -1) {
        return true;
    }

    return false;
}

function IsIE8() {
    if (navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE 8.") != -1) {
        return true;
    }

    return false;
}

function ShowHeightDiv(width) {
    var element = document.getElementById("divPopupContainer");
    element.innerHTML = "";
    var height = 120;
    if (IsBrowserFixedPositionCompatible()) {
        element.style.position = "fixed";
        document.getElementById('divMask').style.position = "fixed";
    }
    element.style.display = "block";
    element.style.left = (getPageSize()[0] / 2) - (width / 2) + "px";
    element.style.top = (getPageSize()[3] / 2) - (height / 2) + "px";
    element.style.width = width + 'px';
    element.style.height = height + 'px';
    document.getElementById("DivHeight").style.display = "block";
    element.appendChild(document.getElementById("DivHeight"));


    document.getElementById('divMask').style.display = "block";
    //document.getElementById('divMask').style.height = getPageSize()[1] + 'px';
}

function HideSaveButton(bHide, btnSave, btnCancel) {
    var Save = document.getElementById(btnSave);
    var Cancel = document.getElementById(btnCancel);
    var waitdiv = document.getElementById('divWait');
    if (bHide) {
        Save.style.visibility = "hidden";
        Cancel.style.visibility = "hidden";
        waitdiv.style.display = "block";
    }
    else {
        Save.style.visibility = "visible";
        Cancel.style.visibility = "visible";
        waitdiv.style.display = "none";
    }
}

function ShowEditDiv(divEdit, width) {
    document.getElementById('divMask').style.display = "block";
    var element = document.getElementById("divPopupContainer");
    if (IsBrowserFixedPositionCompatible()) {
        element.style.position = "fixed";
        document.getElementById('divMask').style.position = "fixed";
    }
    element.innerHTML = "";
    element.style.display = "block";
    element.style.left = (getPageSize()[0] / 2) - (width / 2) + "px";
    element.style.width = width + 'px';
    document.getElementById(divEdit).style.display = "block";
    try {
        element.appendChild(document.getElementById(divEdit));
    }
    catch (e) {
    }

    //document.getElementById('divMask').style.height = getPageSize()[1] + 'px';
}

function OnConfirmDelete(val) {
    return confirm(val);
}

function OnConfirmDeleteWithProcessing(val, btnId, divId) {
    if (confirm(val)) {
        ShowHideButton(btnId, divId, false);
        return true;
    }
    else {
        return false;
    }
}

function ShowHideTrackerMask(bShow) {
    ShowHideDivMask('DivTacker', bShow);
}

function ShowHideDivMask(divIdToMask, bShow) {
    var DivMaskTracker = document.getElementById('DivMaskTracker');
    var divToMask = document.getElementById(divIdToMask);

    if (divToMask == null)
        return;


    if (bShow) {
        DivMaskTracker.style.left = (divToMask.offsetLeft + 193) + "px";
        DivMaskTracker.style.top = (divToMask.offsetTop + 133) + "px";
        DivMaskTracker.style.width = divToMask.offsetWidth + "px";
        DivMaskTracker.style.height = divToMask.offsetHeight + "px";
        DivMaskTracker.className = "popupmask";
        DivMaskTracker.style.display = "block";
    }
    else {
        DivMaskTracker.style.display = "none";
        DivMaskTracker.className = "";
    }
}

function ShowHideDivTranslucentMask(divIdToMask, divIdForMasking, bShow) {
    var divToMask = document.getElementById(divIdToMask);
    var divForMasking = document.getElementById(divIdForMasking);
    //debugger;
    //alert(divToMask.offsetLeft + " : " + divToMask.offsetTop);
    if (bShow) {
        //divTranslucent.style.left = (d.offsetLeft) + "px";
        //divTranslucent.style.top = (d.offsetTop) + "px";
        divForMasking.style.width = divToMask.offsetWidth + "px";
        divForMasking.style.height = divToMask.offsetHeight + "px";
        divForMasking.className = "translucentmask";
        divForMasking.style.display = "block";
    }
    else {
        divForMasking.style.display = "none";
        divForMasking.className = "";
    }
}

function ShowHideButton(IdToHide, IdToShow, bShow) {
    if (bShow) {
        document.getElementById(IdToHide).style.display = "block";
        document.getElementById(IdToHide).disabled = false;
        document.getElementById(IdToShow).style.display = "none";
    }
    else {
        document.getElementById(IdToHide).style.display = "none";
        document.getElementById(IdToShow).style.display = "block";
    }
}

function getPageSize() {

    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) {	// all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if (yScroll < windowHeight) {
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    if (navigator.appName == "Netscape" && getPageYScroll() > 0) {
        pageWidth -= 18;
    }

    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;
}

function getPageYScroll() {

    var yScroll;

    if (self.pageYOffset) {
        yScroll = self.pageYOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
        yScroll = document.documentElement.scrollTop;
    } else if (document.body) {// all other Explorers
        yScroll = document.body.scrollTop;
    }

    return yScroll;
}


/****************Start Progress Div****************************************************/
var nRequestProcessingCount = 0;
function ShowRequestProcessing(strProgressText) {
    nRequestProcessingCount++;
    var rpDiv = document.getElementById('divProgressBar');
    rpDiv.style.display = "block";
    rpDiv.style.width = getPageSize()[0] + "px"; //window.screen.width - 20 + "px";
    if (IsBrowserFixedPositionCompatible()) {
        rpDiv.style.position = "fixed";
    }
    rpDiv.innerHTML = strProgressText;
}

function HideRequestProcessing() {
    nRequestProcessingCount--;
    if (nRequestProcessingCount == 0) {
        var rpDiv = document.getElementById('divProgressBar');
        rpDiv.style.display = "none";
        rpDiv.innerHTML = "";
    }
}

/****************End Progress Div****************************************************/



/*************** Graph Plot Functions ***********************************************/


function getXOffset(e) {
    if (typeof e.offsetX != 'undefined')
        return e.offsetX;
    else if (typeof e.pageX != 'undefined')
        return e.pageX - e.target.offsetLeft;
}

function getYOffset(e) {
    if (typeof e.offsetY != 'undefined')
        return e.offsetY;
    else if (typeof e.pageY != 'undefined')
        return e.pageY - e.target.offsetTop;
}

function on_image_over(e) {
    e = (e) ? e : window.event;
    //document.getElementById('mytext').value = getXOffset(e) + ' : ' + getYOffset(e);
}

function findPos(obj) {
    var curleft = 0;
    var curtop = 0;
    var border = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {

            curleft += obj.offsetLeft - obj.scrollLeft;
            curtop += obj.offsetTop - obj.scrollTop;
            var position = '';
            if (obj.style && obj.style.position) position = obj.style.position.toLowerCase();
            if ((position == 'absolute') || (position == 'relative')) break;
            while (obj.parentNode != obj.offsetParent) {
                obj = obj.parentNode;
                curleft -= obj.scrollLeft;
                curtop -= obj.scrollTop;
            }
            obj = obj.offsetParent;
        }
    }
    else {
        if (obj.x)
            curleft += obj.x;
        if (obj.y)
            curtop += obj.y;
    }
    return { left: curleft, top: curtop };
}

function showGraphDiv(toggle, bubblediv, imageid, x1, y1, h, w, str) {
    var obj = document.getElementById(bubblediv);


    if (toggle) {
        var imagediv = document.getElementById(imageid);
        var pos = findPos(imagediv);

        obj.style.display = "block";
        obj.style.left = (x1 + 50) + "px";
        obj.style.top = (pos.top + y1 - 200) + "px";
        obj.style.width = 80 + "px";
        //obj.style.height = 20 + "px";

        obj.innerHTML = str;

        //obj.style.left = 100;
        //obj.style.top= 100;
        //obj.innerHTML = str;
    }
    else {
        obj.style.display = "none";
    }


}



/*************** Graph Plot Functions ***********************************************/



function TextLimit(field, maxlen) {
    var txt = document.getElementById(field);
    if (txt.value.length > maxlen + 1) {
        txt.value = txt.value.substring(0, maxlen);
        return false;
    }
    return;
}

/********************START HIT BOX**************************************************/
function signInClicked() {
    _hbLink('GetStartedSignIn');
    return true;
}

function registerClicked() {
    _hbLink('GetStartedRegister');
    return true;
}
/********************END HIT BOX**************************************************/

/********************START JS ERROR LOG**************************************************/
window.onerror = AHATrapError;

function AHATrapError(msg, url, ln) {
    if (!TrapJSErrors)
        return;

    var error = new Error(msg);

    error.location = url + ', line: ' + ln; // add custom property

    AHAWeb.Patient.AHAJSUtility.LogJavascriptError(error.name, error.message, error.location, OnErrorLogSuccess, OnErrorLogFailure);

    return true;
}

function OnErrorLogSuccess(result, userContext, methodName) {
}

function OnErrorLogFailure(exception, userContext, methodName) {
    /*  
    We can also perform different actions like the  
    succeededCallback handler based upon the methodName and userContext  
    */
}
/********************END JS ERROR LOG**************************************************/

function ChangeContent(bNext) {
    if (bNext)
        myLytebox.changeContent(myLytebox.activeFrame + 1);
    else
        myLytebox.changeContent(myLytebox.activeFrame - 1);
}

function EndTour() {
    myLytebox.end();
}

function Signup(bIsScientificSession) {
    if (bIsScientificSession) {
        window.location = "../../ScientificSession/prereg.aspx";
    }
    else {
        window.location = "../../visitor/prereg.aspx";
    }
}

/*Provider Additions*/

function ShowGenericConfirmationDiv(divid, width, height) {
    var element = document.getElementById("divPopupContainer");

    if (IsBrowserFixedPositionCompatible()) {
        element.style.position = "fixed";
        document.getElementById('divMask').style.position = "fixed";
    }
    element.style.display = "block";
    element.style.left = (getPageSize()[0] / 2) - (width / 2) + "px";
    element.style.top = (getPageSize()[3] / 2) - (height / 2) + "px";
    element.style.width = width + 'px';
    element.style.height = height + 'px';

    element.appendChild(document.getElementById(divid));

    document.getElementById(divid).style.display = "block";
    document.getElementById('divMask').style.display = "block";

    return false;
}

function HideGenericConfirmationDiv(divid) {
    var element = document.getElementById("divPopupContainer");
    var childDiv = document.getElementById(divid);
    childDiv.style.display = "none";
    element.removeChild(childDiv);
    document.body.appendChild(childDiv);
    element.style.display = "none";
    document.getElementById('divMask').style.display = "none";

    return false;
}
/*End Provider Additions*/

/* Mail Specific */

function OnMailSelectAll(item) {
    var bchecked = false;
    if (item.checked)
        bchecked = true;

    for (var i = 0; i < document.forms[0].elements.length; i++) {
        var ele = document.forms[0].elements[i];
        if (ele.type == "checkbox" && ele.id.indexOf("chkSelect") != -1) {
            ele.checked = bchecked;
        }
    }
}

function OnMailSelectAllUnchecked(item, chkSelectAll) {
    if (!item.checked) {
        document.getElementById(chkSelectAll).checked = false;
        return;
    }

    var bIfAllChecked = true;
    for (var i = 0; i < document.forms[0].elements.length; i++) {
        var ele = document.forms[0].elements[i];
        if (ele.type == "checkbox" && ele.id.indexOf("chkSelect") != -1 && !ele.checked) {
            bIfAllChecked = false;
        }
    }

    if (bIfAllChecked)
        document.getElementById(chkSelectAll).checked = true;
}

/* End Mail Specific */

var currentDropDownObj = null;
function showdropdownmenu(obj) {


    obj.style.clip = "rect(auto auto auto auto)";
    obj.removeAttribute("style");
    currentDropDownObj = obj;


}
function hidedropdownmenu(obj) {
    setTimeout(function() { if (obj !== currentDropDownObj) obj.style.clip = "rect(auto auto 27px auto)"; }, 50);
    currentDropDownObj = null;
}
function TrimWhiteSpaces(str) {
    var start;
    var end;
    str = str.toString();
    var len = str.length;
    for (start = 0; start < len; start++) {
        ch = str.charAt(start);
        if (ch != ' ' && ch != '\t' && ch != '\n' && ch != '\r' && ch != '\f')
            break;
    }
    if (start == len)
        return "";
    for (end = len - 1; end > start; end--) {
        ch = str.charAt(end);
        if (ch != ' ' && ch != '\t' && ch != '\n' && ch != '\r' && ch != '\f')
            break;
    }
    end++;
    return str.substring(start, end);

}

function ShowHideReportGraph(rowid, id) {
    var strActualID = id + "_" + rowid;
    
    if (document.getElementById(strActualID).style.display == "none") {
        document.getElementById(strActualID).style.display = "";
    }
    else {
        document.getElementById(strActualID).style.display = "none";
    }
}

function ShowHideAllReportGraphs(bShow) {
    var trReport = document.getElementsByTagName("tr");

    if (bShow) {
        for (var i = 0; i < trReport.length; i++) {
            if (trReport[i].id.indexOf("_ChartRow") != -1) {
                trReport[i].style.display = "";
            }
        }
    }
    else {
        for (var i = 0; i < trReport.length; i++) {
            if (trReport[i].id.indexOf("_ChartRow") != -1) {
                trReport[i].style.display = "none";
            }
        }
    }
}

function OpenAuditInfoPage(url) {
    window.open(url, '_auditinfo', 'height=665,width=1025,scrollbars=yes,status=no,toolbar=no,menubar=no,location=no,top=100,left=100');
    return false;
}