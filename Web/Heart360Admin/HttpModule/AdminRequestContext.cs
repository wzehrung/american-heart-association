﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GRCBase;
using AHAHelpContent;

namespace Heart360Admin.HttpModule
{
    public class AdminRequestContext
    {
        public HttpContext CurrentContext
        {
            get;
            set;
        }

        public string ContextUrl
        {
            get;
            set;
        }

        public AHAHelpContent.Language SelectedLanguage
        {
            get;
            set;
        }

        public AdminRequestContext(System.Web.HttpContext currentContext)
        {
            CurrentContext = currentContext;
        }

        public static AdminRequestContext GetContext()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new System.Exception("This call is only valid during an HTTP request");
            }

            System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

            if (currentContext.Items["AdminRequestContext"] == null)
            {
                return null;
            }

            return currentContext.Items["AdminRequestContext"] as AdminRequestContext;
        }

        public static void PopulateContextData()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new System.Exception("This call is only valid during an HTTP request");
            }

            System.Web.HttpContext currentContext = System.Web.HttpContext.Current;

            if (currentContext.Items["AdminRequestContext"] != null)
            {
                throw new System.Exception("H360RequestContext already populated");
            }

            currentContext.Items["AdminRequestContext"] = new AdminRequestContext(System.Web.HttpContext.Current);


            AdminRequestContext objAdminRequestContext = currentContext.Items["AdminRequestContext"] as AdminRequestContext;
            objAdminRequestContext.DoPopulate();
        }

        private void DoPopulate()
        {
            ContextUrl = UrlUtility.FullCurrentPageUrl;

            Uri uriContext = new Uri(ContextUrl);

            Heart360Admin.Utilities.UserContext objContext = Heart360Admin.Utilities.SessionInfo.GetUserContext();

            if (Heart360Admin.Utilities.SessionInfo.IsAdminLoggedin() && objContext != null && !string.IsNullOrEmpty(objContext.LanguageLocale))
            {
                LanguageHelper.SetLocaleAndCulture(objContext.LanguageLocale);
                SelectedLanguage = new Language { LanguageID = objContext.LanguageID.Value, LanguageLocale = objContext.LanguageLocale };
            }
            else
            {
                LanguageHelper.SetLocaleAndCulture(LanguageHelper.GetSelectedLanguageLocaleForVisitor());
                SelectedLanguage = AHAHelpContent.Language.GetLanguageByLocale(LanguageHelper.GetSelectedLanguageLocaleForVisitor());
            }
        }
    }
}
