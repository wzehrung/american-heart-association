﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Heart360Admin.HttpModule
{
    public class AHAAdminModule : IHttpModule, System.Web.SessionState.IRequiresSessionState
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication application)
        {
            application.BeginRequest += new EventHandler(BeginRequest);
            application.PreRequestHandlerExecute += new EventHandler(PreRequestHandlerExecute);
        }

        protected virtual void BeginRequest(object sender, EventArgs args)
        {
            //int nLanguageID = Utilities.Utility.GetSelectedLanguage();
            //AHAHelpContent.Language objSelectLanguage = AHAHelpContent.Language.GetLanguageByID(nLanguageID);

            //CultureInfo cInfo = new CultureInfo(objSelectLanguage.LanguageLocale);
            //System.Threading.Thread.CurrentThread.CurrentUICulture = cInfo;
            //System.Threading.Thread.CurrentThread.CurrentCulture = cInfo;


            HttpApplication application = (HttpApplication)sender;
            string strFullUrl = GRCBase.UrlUtility.FullCurrentPageUrl.ToLower();

            if (Heart360Admin.Utilities.AppSettings.IsSSLEnabled
                && !application.Request.IsSecureConnection
                && application.Request.RequestType == "GET")
            {
                application.Response.Redirect(string.Format("https://{0}{1}", GRCBase.UrlUtility.ServerName, GRCBase.UrlUtility.RelativeCurrentPageUrlWithQV));
                application.Response.End();
                return;
            }
        }

        protected virtual void PreRequestHandlerExecute(object sender, EventArgs args)
        {
            string current_RelativeUrl_lower = System.Web.HttpContext.Current.Request["URL"].ToLower();

            if (!current_RelativeUrl_lower.EndsWith(".aspx"))
            {
                return;
            }

            if (!current_RelativeUrl_lower.EndsWith("pages/resourcegraphics/patient/previewcurrentresourcegraphics.aspx") &&
                !current_RelativeUrl_lower.EndsWith("pages/resourcegraphics/patient/previewresourcegraphics.aspx") &&
                !current_RelativeUrl_lower.EndsWith("pages/resourcegraphics/provider/previewcurrentresourcegraphics.aspx") &&
                !current_RelativeUrl_lower.EndsWith("pages/resourcegraphics/provider/previewresourcegraphics.aspx"))
            {
                AddImmediateExpiryHeadersToResponse();
            }

            AdminRequestContext.PopulateContextData();
        }

        public void AddImmediateExpiryHeadersToResponse()
        {
            if (System.Web.HttpContext.Current.Request.HttpMethod.ToLower() == "get")
            {
                System.Web.HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
                System.Web.HttpContext.Current.Response.Expires = 0;
                System.Web.HttpContext.Current.Response.Cache.SetNoStore();
                System.Web.HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
            }
            return;
        }
    }
}
