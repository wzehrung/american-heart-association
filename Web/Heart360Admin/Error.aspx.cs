﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GRCBase;

namespace Heart360Admin
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, System.EventArgs e)
        {
            //if (!Utilities.Utility.IsAdminLoggedin())
            //{
            //    this.MasterPageFile = "~/MasterPages/Home.Master";
            //}
            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                this.MasterPageFile = "~/MasterPages/Home.Master";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.Response.StatusCode = 500;

            string errorID = Request["ID"];
            if (Request["NOTENCRYPTED"] != null)
            {
                errorID = Request["ID"];
            }
            else
            {
                errorID = GRCBase.BlowFish.DecryptString(Request["ID"]);
            }

            litErrorID2.Text = errorID;

            LitTechnicalDetails.Text = ErrorLogHelper.GetErrorMessageFromDatabase(Convert.ToInt32(errorID));
            LitTechnicalDetails.Text = LitTechnicalDetails.Text.Replace("\n", "<br>");

            if (Request["NOTENCRYPTED"] != null)
            {
                LitCompleteDetails.Text = ErrorLogHelper.GetErrorLogFromDatabase(Convert.ToInt32(errorID));
                LitCompleteDetails.Text = LitCompleteDetails.Text.Replace("\r\n", "<br>");
            }

            //if (!Utilities.Utility.IsAdminLoggedin())
            //{
            //    content.Attributes.Add("class", "content");
            //}
            if (!Utilities.SessionInfo.IsAdminLoggedin())
            {
                content.Attributes.Add("class", "content");
            }
        }
    }
}
