﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls.Guidingtext.Provider
{
    public partial class CreateGuidingText : System.Web.UI.UserControl
    {
        AHACommon.AHADefs.ContentType contentType;

        protected void Page_Load(object sender, EventArgs e)
        {
            _CheckForValidContenttype();
            if (!Page.IsPostBack)
            {
                AHAHelpContent.ProviderContent objContent = AHAHelpContent.ProviderContent.FindContentByZoneTitle(contentType.ToString(), Utilities.Utility.GetCurrentLanguageID());
                if (objContent != null)
                {
                    txtText.Text = objContent.ContentText;
                    hddnContentID.Value = objContent.ContentID.ToString();
                }
                lnkAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfo.aspx?id=" + objContent.ContentID + "&Type=" + AHAHelpContent.AuditTrailType.ProviderContent.ToString() + "')");
            }
        }

        private void _CheckForValidContenttype()
        {
            if (Enum.GetNames(typeof(AHACommon.AHADefs.ContentType)).Contains(Request["Title"]))
            {
                contentType = (AHACommon.AHADefs.ContentType)Enum.Parse(typeof(AHACommon.AHADefs.ContentType), Request["Title"]);
                return;
            }

            throw new ArgumentException("Given content type is not valid!");

        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
                return;

            AHAHelpContent.ProviderContent.UpdateContent(Convert.ToInt32(hddnContentID.Value),contentType.ToString(), null, null, txtText.Text.Trim(), Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID,Utilities.Utility.GetCurrentLanguageID());
            Response.Redirect("/Pages/GuidingText/Provider/GuidingTextList.aspx");
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/GuidingText/Provider/GuidingTextList.aspx");
        }

        #region Validators

        protected void OnValidateText(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtText.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a Text";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Text").ToString();
                return;
            }

            return;
        }
        #endregion
    }
}