﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateGuidingText.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.Guidingtext.Provider.CreateGuidingText" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_Provider_Guiding_Text")%></h1>
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Use_the_form_below_to_update_the_guiding_text")%>
</p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellpadding="0" cellspacing="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2" class="red align-right">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                    <asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                            ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Text")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtText" Width="400" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidText"
                                        runat="server" Display="None" OnServerValidate="OnValidateText">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="guidingtext">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "To_add_a_link_in_the_text_above_provider")%> <%=Heart360Admin.Utilities.Utility.TrademarkedHeart360%>.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Update")%></em></strong></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkCancel" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Cancel")%></em></strong></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkAudit" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AuditInfo")%></em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>
<asp:HiddenField ID="hddnContentID" runat="server" />
