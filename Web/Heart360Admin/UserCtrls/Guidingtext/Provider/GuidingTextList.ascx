﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GuidingTextList.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.Guidingtext.Provider.GuidingTextList" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Guiding_Text")%></h1>
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Below_is_the_list_of_all_provider")%></p>
<table cellpadding="0" cellspacing="0" class="admintable">
    <tr>
        <th>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_pages")%>
        </th>
        <th>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Specific_pages")%>
        </th>
        <th>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Others")%>
        </th>
    </tr>
    <tr>
        <td>
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Patients">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patients")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Groups">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Groups")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=CreateGroup">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Groups_Create_Group")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=UpdateGroup">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Groups_Update_Group")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=AddPatientsToGroup">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Groups_Add_Patients")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=AddGroupsToPatient">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Groups_Add_Patient_to_multiple_groups")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=GroupsRightRail">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Groups_Right_Box")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Alerts">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Alerts")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=ViewAlertRules">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Alerts_View_Rules")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=CreateAlert">C<%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Alert")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=AlertRightRail">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Alert_Right_Box")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Messages">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Messages")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Reports">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports")%></a>
        </td>
        <td>
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=PatientSummary">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Summary")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=PatientAlerts">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Alerts")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=ViewPatientAlertRules">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Alerts_View_Rules")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=PatientGroupMembership">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Group_membership")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=BloodPressure">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Blood_Pressure")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=BloodGlucose">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Blood_Glucose")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Weight">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Weight")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Cholesterol">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Cholesterol")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=PhysicalActivity">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Physical_Activity")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Medication">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Medication")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=HealthHistory">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Health_History")%></a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=DisconnectPatient">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Disconnect_Patient_Message")%></a>
        </td>
        <td>
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Registration">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Registration")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=RegistrationConfirmation">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Registration_Confirmation")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=NewUserConfirmation">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "New_User_Confirmation")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=ForgotPassword">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Forgot_Password")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=ForgotPasswordConfirmation">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Forgot_Password_Confirmation")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=EditProfile">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Edit_Profile")%>
            </a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Login">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Login")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=ResetPassword">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reset_Password")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=Invitation">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Invitation")%>
            </a>
            <br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=GrantProviderAccess">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Grant_Provider_Access")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=ConnectWithPatient">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Connect_with_Patient")%></a><br />
            <a href="/Pages/GuidingText/Provider/CreateGuidingText.aspx?Title=GrantProviderAccessEmail">
                Grant Provider Access Email</a><br />
        </td>
    </tr>
</table>
