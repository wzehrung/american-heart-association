﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GuidingTextList.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.Guidingtext.Patient.GuidingTextList" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Guiding_Text")%></h1>
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Below_is_the_list_of_all_patient")%></p>
<table cellpadding="0" cellspacing="0" class="admintable" width="550">
    <tr>
        <th>
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_pages")%>
        </th>
    </tr>
    <tr>
        <td>
            <a id="A1" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=PatientHome"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_DashBoard")%></a>
            <br />
            <a id="A2" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=BloodPressureTop"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Blood_Pressure_Top")%></a>
            <br />
            <a id="A3" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=BloodPressureBottom"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Blood_Pressure_Bottom")%></a>
            <br />
            <a id="A4" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=CholestrolTop"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Cholestrol_Top")%></a>
            <br />
            <a id="A5" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=CholestrolBottom"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Cholestrol_Bottom")%></a>
            <br />
            <a id="A6" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=BloodGlucoseTop"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Blood_Glucose_Top")%></a>
            <br />
            <a id="A7" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=BloodGlucoseBottom"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Blood_Glucose_Bottom")%></a>
            <br />
            <a id="A8" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=MedicationTop"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Medication_Top")%></a>
            <br />
            <a id="A9" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=MedicationBottom"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Medication_Bottom")%></a>
            <br />
            <a id="A10" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=WeightTop"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Weight_Top")%></a>
            <br />
            <a id="A11" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=WeightBottom"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Weight_Bottom")%></a>
            <br />
            <a id="A12" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=PhysicalActivityTop"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Physical_Activity_Top")%></a>
            <br />
            <a id="A13" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=PhysicalActivityBottom"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Physical_Activity_Bottom")%></a>
            <br />
            <a id="A14" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=CreateReportTop"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Report_Top")%></a>
            <br />
            <a id="A15" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=CreatReportBottom"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Report_Bottom")%></a>
            <br />
            <a id="A16" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=Resources"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Resources_")%></a>
            <br />
            <a id="A17" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=ChangeUser"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Change_User")%></a>
            <br />
            <a id="A18" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=ProviderInvitation"
                runat="server">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Invitation")%></a>
            <br />
            <a id="A19" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=MailHome"
                runat="server">Mail Home</a>
            <br />
            <a id="A20" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=MailHome_No_Provider_With_Messaging"
                runat="server">Mail Home No Provider With Messaging</a><br />
            <a id="A21" href="/Pages/GuidingText/Patient/CreateGuidingText.aspx?Title=MailView_No_Provider_With_Messaging"
                runat="server">Mail View No Provider With Messaging</a>
        </td>
    </tr>
</table>
