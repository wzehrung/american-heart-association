﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls.Guidingtext.Patient
{
    public partial class CreateGuidingText : System.Web.UI.UserControl
    {
        AHACommon.AHADefs.PatientContentType patientContentType;

        protected void Page_Load(object sender, EventArgs e)
        {
            _CheckForValidContenttype();
            if (!Page.IsPostBack)
            {
                AHAHelpContent.PatientContent objContent = AHAHelpContent.PatientContent.FindPatientContentByZoneTitle(patientContentType.ToString(), Utilities.Utility.GetCurrentLanguageID());
                if (objContent != null)
                {
                    txtText.Text = objContent.ContentText;
                    hddnContentID.Value = objContent.PatientContentID.ToString();
                    lnkAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfo.aspx?id=" + objContent.PatientContentID + "&Type=" + AHAHelpContent.AuditTrailType.PatientContent.ToString() + "')");
                }
                else
                {
                    hddnContentID.Value = "0";
                }
            }
        }

        private void _CheckForValidContenttype()
        {
            if (Enum.GetNames(typeof(AHACommon.AHADefs.PatientContentType)).Contains(Request["Title"]))
            {
                patientContentType = (AHACommon.AHADefs.PatientContentType)Enum.Parse(typeof(AHACommon.AHADefs.PatientContentType), Request["Title"]);
                return;
            }

            throw new ArgumentException("Given content type is not valid!");

        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
                return;

            AHAHelpContent.PatientContent.UpdateContent(Convert.ToInt32(hddnContentID.Value), patientContentType.ToString(), null, null, txtText.Text.Trim(), Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
            Response.Redirect("/Pages/GuidingText/Patient/GuidingTextList.aspx");
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/GuidingText/Patient/GuidingTextList.aspx");
        }

        protected void OnValidateText(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtText.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a Text";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Text").ToString();
                return;
            }

            return;
        }
    }
}