﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientEduCtrl.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.PatientEduCtrl" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Helpful_Resources")%></h1>
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "links_visible_on_the_right_rail_of_the_patient_portal")%></p>
<table class="admin-box wrap" cellpadding="0" cellspacing="0">
    <tr>
        <td class="align-right menubar">
            <a href="../Pages/CreatePatientEdu.aspx" class="aha-small-button with-arrow"><strong>
                <em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></a>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="up1">
                <ContentTemplate>
                    <asp:GridView ShowHeader="true" EnableViewState="true" ID="gvContents" runat="server"
                        AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, EmtyContentText %>"
                        AllowPaging="True" PageSize="10" ShowFooter="False" OnRowDataBound="gvContents_RowDataBound"
                        AllowSorting="True" OnPageIndexChanging="gvContents_PageIndexChanging" OnSorting="gvContents_Sorting"
                        GridLines="None" CssClass="provider-grid" PagerStyle-CssClass="table-page-index"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField Visible="True" SortExpression="title" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Title %>" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litTitle"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" SortExpression="url" HeaderText="<%$ Resources:Heart360AdminGlobalResources, url %>" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <a runat="server" id="aURL" target="_blank"></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" SortExpression="DidYouKnowText" HeaderText="<%$ Resources:Heart360AdminGlobalResources, didyouknowtext %>"
                                HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litText"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" SortExpression="DidYouKnowLinkTitle" HeaderText="<%$ Resources:Heart360AdminGlobalResources, didyouknowtitle %>"
                                HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litLinkTitle"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" SortExpression="tag" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Tags %>" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litTag"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" SortExpression="ExpirationDate" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Expiration_Date %>"
                                HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litExpirationDate"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Actions %>" HeaderStyle-CssClass="alignleft " ItemStyle-CssClass="admin-action">
                                <ItemTemplate>
                                    <a runat="server" id="aEdit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Edit")%></a>&nbsp;|&nbsp;<a runat="server" id="aDelete"
                                        onserverclick="aDelete_onserverclick"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Delete")%></a>&nbsp;|&nbsp;<a runat="server" id="aPreview"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Preview")%></a><br /><a runat="server" id="aAudit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AuditInfo")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" />
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function PreviewResource(id) {
        window.open("/Pages/PreviewPatientEdu.aspx?HRID=" + id, "preview_resource", "width=1038,height=700");
        document.forms[0].target = 'preview_resource';
    }
</script>

