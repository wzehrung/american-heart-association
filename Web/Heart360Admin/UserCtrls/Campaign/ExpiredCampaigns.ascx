﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExpiredCampaigns.ascx.cs" Inherits="Heart360Admin.UserCtrls.Campaign.ExpiredCampaigns" %>

        <asp:UpdatePanel runat="server" ID="upInactiveCampaigns" UpdateMode="Always">
            <ContentTemplate>

<asp:GridView
    ShowHeader="true"
    EnableViewState="true"
    ID="gvExpiredCampaigns"
    DataKeyNames="CampaignID"
    runat="server"
    AutoGenerateColumns="False"
    CellPadding="0"
    CellSpacing="0"
    EmptyDataText="There are no expired campaigns."
    AllowPaging="True"
    PageSize="10"
    ShowFooter="False"
    OnRowDataBound="gvExpiredCampaigns_RowDataBound"
    OnPageIndexChanging="gvExpiredCampaigns_PageIndexChanging"
    OnRowCommand="gvExpiredCampaigns_RowCommand"
    GridLines="Horizontal"
    CssClass="provider-grid"
    PagerStyle-CssClass="table-page-index">

    <Columns>

        <asp:BoundField DataField="CampaignID" HeaderText="ID" SortExpression="CampaignID" />

        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />

        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />

        <asp:BoundField DataField="CampaignUrl" HeaderText="URL" SortExpression="CampaignUrl" />

        <asp:BoundField DataField="StartDate" HeaderText="Start Date" SortExpression="StartDate" />

        <asp:BoundField DataField="EndDate" HeaderText="End Date" SortExpression="EndDate" />

        <asp:BoundField DataField="BusinessName" HeaderText="Business" SortExpression="BusinessName" />

        <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Actions %>"
            ItemStyle-Width="80" HeaderStyle-CssClass="alignleft">
            <ItemTemplate>
                <asp:LinkButton ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%#Eval("CampaignID") %>' /><br /><br />
                <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%#Eval("CampaignID") %>' OnClientClick="javascript:return confirm('This campaign will be deleted. Do you want to delete this campaign?');" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerStyle HorizontalAlign="Right" />
</asp:GridView>

                            </ContentTemplate>
        </asp:UpdatePanel>