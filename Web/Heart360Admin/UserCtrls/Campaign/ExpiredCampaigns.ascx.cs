﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Heart360Admin.UserCtrls.Campaign
{
    public partial class ExpiredCampaigns : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _bindDataToGridView();
            }
        }

        protected void gvExpiredCampaigns_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            foreach (GridViewRow row in gvExpiredCampaigns.Rows)
            {
                // Get the campaign ID from the first cell in the row
                int iCampaignID = Convert.ToInt32(row.Cells[0].Text);

                AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByCampaignID(iCampaignID, 1);

                Literal litTest = (Literal)(row.FindControl("litTest"));

                string sDescription = string.Empty;
                // Remove any HTML tags
                sDescription = GRCBase.HtmlScrubber.Clean(objCampaign.Description, false, true);

                // Limit the length of the description
                int iMaxLength = 64;
                if (sDescription.Length > 64)
                {
                    sDescription = sDescription.Substring(0, iMaxLength) + "...";
                }

                row.Cells[2].Text = sDescription;

                // Format the URL to be clickable
                string sCampaignURL = objCampaign.CampaignURL;
                row.Cells[3].Text = "<a href=" + AHACommon.AHAAppSettings.SiteUrl + "/" + sCampaignURL + ">.../" + sCampaignURL;

                // Format the Start and End dates
                row.Cells[4].Text = objCampaign.StartDate.ToShortDateString();
                row.Cells[5].Text = objCampaign.EndDate.ToShortDateString();
            }
        }

        //protected void gvExpiredCampaigns_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType != DataControlRowType.DataRow)
        //        return;
        //    AHAHelpContent.Campaign objCampaign = e.Row.DataItem as AHAHelpContent.Campaign;

        //    Literal litTitle = e.Row.FindControl("litTitle") as Literal;
        //    Literal litDescription = e.Row.FindControl("litDescription") as Literal;
        //    //Literal litURL = e.Row.FindControl("litURL") as Literal;
        //    Literal litStartDate = e.Row.FindControl("litStartDate") as Literal;
        //    Literal litEndDate = e.Row.FindControl("litEndDate") as Literal;

        //    HtmlAnchor aEdit = e.Row.FindControl("aEdit") as HtmlAnchor;
        //    HtmlAnchor aURL = e.Row.FindControl("aURL") as HtmlAnchor;
        //    //HtmlAnchor aDisable = e.Row.FindControl("aDisable") as HtmlAnchor;

        //    aURL.InnerHtml = GRCBase.StringHelper.GetShortString(".../" + objCampaign.CampaignURL, 14, false);
        //    aURL.Title = string.Format("{0}/{1}", AHACommon.AHAAppSettings.SiteUrl, objCampaign.CampaignURL);
        //    aURL.HRef = string.Format("{0}/{1}", AHACommon.AHAAppSettings.SiteUrl, objCampaign.CampaignURL);
        //    litTitle.Text = FormatUrlText(objCampaign.Title, 6);

        //    // Limit the length of the description
        //    string sDescription = objCampaign.Description;
        //    int iMaxLength = 64;
        //    if (sDescription.Length > 64)
        //    {
        //        sDescription = sDescription.Substring(0, iMaxLength) + "...";
        //    }

        //    litDescription.Text = sDescription;

        //    //litURL.Text = FormatUrlText(string.Format("{0}/{1}", AHACommon.AHAAppSettings.SiteUrl, objCampaign.CampaignURL), 19);
        //    litStartDate.Text = objCampaign.StartDate.ToShortDateString();
        //    litEndDate.Text = objCampaign.EndDate.ToShortDateString();

        //    //aDisable.Attributes.Add("ID", objCampaign.CampaignID.ToString());
        //    aEdit.HRef = String.Format("{0}?CID={1}", "/Pages/Campaign/CreateCampaign.aspx", objCampaign.CampaignID.ToString());
        //}

        protected void gvExpiredCampaigns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string sCampaignID = e.CommandArgument.ToString();

            if (e.CommandName == "Edit")
            {
                Response.Redirect("/pages/campaign/CreateCampaign.aspx?CID=" + sCampaignID);
            }
            else if (e.CommandName == "Delete")
            {
                AHAHelpContent.Campaign.DeleteCampaign(Convert.ToInt32(sCampaignID));
            }

            _bindDataToGridView();
        }

        protected void gvExpiredCampaigns_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvExpiredCampaigns.PageIndex = e.NewPageIndex;
            _bindDataToGridView();

        }

        protected void aDelete_serverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDelete = sender as HtmlAnchor;
            if (aDelete.Attributes["ID"] != null)
            {

                AHAHelpContent.Campaign.DeleteCampaign(Convert.ToInt32(aDelete.Attributes["ID"]));
                _bindDataToGridView();
            }
        }

        private void _bindDataToGridView()
        {
            List<AHAHelpContent.Campaign> listCampaign = AHAHelpContent.Campaign.FindAllExpiredCampaigns(Utilities.Utility.GetCurrentLanguageID());

            gvExpiredCampaigns.DataSource = listCampaign;
            gvExpiredCampaigns.DataBind();
        }

        private string FormatUrlText(string strURL, int length)
        {
            if (string.IsNullOrEmpty(strURL) || strURL.Length <= length)
            {
                return strURL;
            }
            else
            {
                int intLength = strURL.Length;
                for (int i = 1; i <= intLength / length; i++)
                {
                    if (i == 1)
                    {
                        //length is the number of charcters after which to insert '<wbr>&shy;'
                        strURL = strURL.Insert((i * length), "<wbr>&shy;");
                    }
                    else
                    {
                        //length is the number of charcters after which to insert '<wbr>&shy;' and 10 is the number of charcters in '<wbr>&shy;'
                        strURL = strURL.Insert((i * length) + (5 * (i - 1)), "<wbr>&shy;");
                    }
                }
            }
            return strURL;
        }
    }
}