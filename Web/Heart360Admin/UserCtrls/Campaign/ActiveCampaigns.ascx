﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActiveCampaigns.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.Campaign.ActiveCampaigns" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>

<asp:Label ID="lblValidationMessage" Visible="false" ForeColor="Red" runat="server"></asp:Label>

<asp:GridView 
    ShowHeader="true" 
    EnableViewState="true" 
    ID="gvActiveCampaigns" 
    runat="server"
    AutoGenerateColumns="False" 
    CellPadding="0" 
    CellSpacing="0" 
    EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, There_are_no_active_campaigns %>"
    AllowPaging="True" 
    PageSize="10" 
    ShowFooter="False" 
    OnRowDataBound="gvActiveCampaigns_RowDataBound" 
    AllowSorting="true" 
    OnSorting="gridView_Sorting"
    OnPageIndexChanging="gvActiveCampaigns_PageIndexChanging" 
    GridLines="Horizontal"
    CssClass="provider-grid" 
    PagerStyle-CssClass="table-page-index"
    DataKeyNames="CampaignID" OnRowCommand="gvActiveCampaigns_RowCommand">
    <Columns>

        <asp:BoundField DataField="CampaignID" HeaderText="ID" SortExpression="CampaignID" />

        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />

        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />

        <asp:BoundField DataField="CampaignUrl" HeaderText="URL" SortExpression="CampaignUrl" />

        <asp:BoundField DataField="StartDate" HeaderText="Start Date" SortExpression="StartDate" />

        <asp:BoundField DataField="EndDate" HeaderText="End Date" SortExpression="EndDate" />

        <asp:BoundField DataField="BusinessName" HeaderText="Business" SortExpression="BusinessName" />
        
        <asp:TemplateField>
            <HeaderTemplate>Actions</HeaderTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%#Eval("CampaignID") %>' /><br /><br />
                <asp:LinkButton ID="btnUnPublish" runat="server" Text="UnPublish" CommandName="UnPublish" CommandArgument='<%#Eval("CampaignID") %>' Visible="false" />
                <asp:LinkButton ID="btnPublish" runat="server" Text="Publish" CommandName="Publish" CommandArgument='<%#Eval("CampaignID") %>' Visible="false" />
            </ItemTemplate>
        </asp:TemplateField>


    </Columns>
    <PagerStyle HorizontalAlign="Right" />
</asp:GridView>
