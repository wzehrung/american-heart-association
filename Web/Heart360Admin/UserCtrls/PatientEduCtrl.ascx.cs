﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;

namespace Heart360Admin.UserCtrls
{
    public partial class PatientEduCtrl : System.Web.UI.UserControl
    {
        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "title";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return true;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _bindDataToGridView();
            }
        }

        protected void gvContents_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            AHAHelpContent.Content objContent = e.Row.DataItem as AHAHelpContent.Content;

            Literal litTitle = e.Row.FindControl("litTitle") as Literal;
            HtmlAnchor aURL = e.Row.FindControl("aURL") as HtmlAnchor;
            Literal litText = e.Row.FindControl("litText") as Literal;
            Literal litLinkTitle = e.Row.FindControl("litLinkTitle") as Literal;
            Literal litTag = e.Row.FindControl("litTag") as Literal;
            Literal litExpirationDate = e.Row.FindControl("litExpirationDate") as Literal;
            HtmlAnchor aDelete = e.Row.FindControl("aDelete") as HtmlAnchor;
            HtmlAnchor aEdit = e.Row.FindControl("aEdit") as HtmlAnchor;
            HtmlAnchor aPreview = e.Row.FindControl("aPreview") as HtmlAnchor;
            HtmlAnchor aAudit = e.Row.FindControl("aAudit") as HtmlAnchor;

            litTitle.Text = objContent.Title;
            aURL.InnerHtml = FormatUrlText(objContent.URL);
            aURL.HRef = objContent.URL;
            litText.Text = GRCBase.StringHelper.GetShortString(Utilities.Utility.RenderLink(objContent.DidYouKnowText), 20, true);
            litLinkTitle.Text = objContent.DidYouKnowLinkTitle;
            litTag.Text = objContent.ContentRuleNames.Replace(",", "<br />");
            litExpirationDate.Text = objContent.ExpirationDate.HasValue ? objContent.ExpirationDate.Value.ToShortDateString() : string.Empty;

            aDelete.Attributes.Add("CID", objContent.ContentID.ToString());
            //aDelete.Attributes.Add("onclick", "return OnConfirmDelete('Do you really want to delete this item?');");
            string confirmMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "delete_confirm").ToString();
            aDelete.Attributes.Add("onclick", "return OnConfirmDelete('" + confirmMessage + "');");
            aEdit.HRef = String.Format("/Pages/CreatePatientEdu.aspx?CID={0}", objContent.ContentID.ToString());
            aPreview.HRef = "javascript:;";//String.Format("/Pages/PreviewPatientEdu.aspx?HRID={0}", objContent.ContentID.ToString());
            aPreview.Attributes.Add("onclick", "return PreviewResource(" + objContent.ContentID.ToString() + ")");
            aAudit.HRef = "javascript:;";
            aAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfo.aspx?Type=" + AHAHelpContent.AuditTrailType.Content.ToString() + "&id=" + objContent.ContentID.ToString() + "');");
        }

        //private string FormatUrlText(string strURL)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    if (string.IsNullOrEmpty(strURL) || strURL.Length <= 20)
        //    {
        //        return strURL;
        //    }
        //    else
        //    {
        //        char[] strArray = strURL.ToCharArray();
        //        int i = 0;
        //        foreach (char c in strArray)
        //        {

        //            sb.Append(c.ToString());
        //            i++;
        //            if (i == 20)
        //            {
        //                sb.Append("<wbr>&shy;");
        //                i = 0;
        //            }

        //        }
        //    }


        //    return sb.ToString();
        //}

        private string FormatUrlText(string strURL)
        {
            if (string.IsNullOrEmpty(strURL) || strURL.Length <= 20)
            {
                return strURL;
            }
            else
            {
                int intLength = strURL.Length;
                for (int i = 1; i <= intLength / 20; i++)
                {
                    if (i == 1)
                    {
                        //20 is the number of charcters after which to insert '<wbr>&shy;'
                        strURL = strURL.Insert((i * 20), "<wbr>&shy;");
                    }
                    else
                    {
                        //20 is the number of charcters after which to insert '<wbr>&shy;' and 10 is the number of charcters in '<wbr>&shy;'
                        strURL = strURL.Insert((i * 20) + (10 * (i - 1)), "<wbr>&shy;");
                    }
                }
            }
            return strURL;
        }

        protected void gvContents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvContents.PageIndex = e.NewPageIndex;
            _bindDataToGridView();
        }

        protected void gvContents_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (SortExpression != e.SortExpression)
            {
                SortExpression = e.SortExpression;
                SortAscending = false;
            }
            else
            {
                if (SortAscending)
                {
                    SortAscending = false;
                }
                else
                {
                    SortAscending = true;
                }
            }

            gvContents.PageIndex = 0;
            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            List<AHAHelpContent.Content> listContents = AHAHelpContent.Content.GetAllContents(Utilities.Utility.GetCurrentLanguageID());
            listContents.Sort(new Utilities.HelpFulResourcesComparer(SortExpression, SortAscending));
            gvContents.DataSource = listContents;
            gvContents.DataBind();
        }

        protected void aDelete_onserverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDelete = sender as HtmlAnchor;
            AHAHelpContent.Content.DeleteContent(Convert.ToInt32(aDelete.Attributes["CID"]), Utilities.Utility.GetCurrentLanguageID());
            _bindDataToGridView();
        }


    }
}