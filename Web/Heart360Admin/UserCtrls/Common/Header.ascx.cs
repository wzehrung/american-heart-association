﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GRCBase;

namespace Heart360Admin.UserCtrls.Common
{
    public partial class Header : System.Web.UI.UserControl
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        private void _PopulateLanguages(int iLanguageID)
        {
            ddlLanguage.DataSource = AHAHelpContent.Language.GetAllLanguages(iLanguageID);
            ddlLanguage.DataTextField = "LanguageName";
            ddlLanguage.DataValueField = "LanguageID";
            ddlLanguage.DataBind();
        }

        protected void OnLanguageChanged(object sender, EventArgs e)
        {
            AHAHelpContent.Language objLanguage = AHAHelpContent.Language.GetLanguageByID(Convert.ToInt32(ddlLanguage.SelectedValue));
            LanguageHelper.SetLocaleAndCulture(objLanguage.LanguageLocale);
            Heart360Admin.Utilities.UserContext objContext = Heart360Admin.Utilities.SessionInfo.GetUserContext();
            objContext.LanguageID = objLanguage.LanguageID;
            objContext.LanguageLocale = objLanguage.LanguageLocale;
            AHAHelpContent.AdminUser.UpdateDefaultLanguage(objContext.UserID, objLanguage.LanguageID);
            Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _SetLinkCss();
                if (objUserContext != null)
                {
                    litName.Text = objUserContext.Name;

                    // Need to get the menu permissions here
                    List<AHAHelpContent.AdminMenu> list = AHAHelpContent.AdminMenu.GetAdminMenuByID(objUserContext.UserID);

                    for (int i = 0; i < list.Count; i++)
                    {
                        string id = "menu" + list[i].MenuID ;                       // Convert.ToString(i+1);
                        HtmlAnchor aMenu = this.FindControl(id) as HtmlAnchor;
                        if (aMenu != null)
                        {
                            if (list[i].Enabled == true)
                            {
                                aMenu.Visible = true;
                            }
                            else
                            {
                                aMenu.Visible = false;
                            }
                        }
                    }

                    if (objUserContext.RoleID.ToString() == "3")
                    {
                        if (!string.IsNullOrEmpty(UrlUtility.FullCurrentPageUrl))
                        {
                            string strCurrentUrl = UrlUtility.FullCurrentPageUrl.Trim().ToLower();
                            if (strCurrentUrl.EndsWith("/pages/user/adduser.aspx") ||
                                strCurrentUrl.EndsWith("/pages/user/edituser.aspx") ||
                                strCurrentUrl.EndsWith("/pages/user/userlist.aspx") ||
                                strCurrentUrl.EndsWith("/pages/user/userroles.aspx"))
                            {
                                Response.Redirect("/Pages/Home.aspx");
                            }
                        }
                    }
                }

                int iLanguageID = Heart360Admin.Utilities.Utility.GetCurrentLanguageID();
                _PopulateLanguages(iLanguageID);
                ddlLanguage.SetSelectedItem(iLanguageID.ToString());
            }

            if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/resourcegraphics/patient/createresourcegraphics.aspx"))
            {

                Utilities.SessionInfo.ResetPatientResourcePreviewSession();
            }

            if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/resourcegraphics/provider/createresourcegraphics.aspx"))
            {

                Utilities.SessionInfo.ResetProviderResourcePreviewSession();
            }


            if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/grantsupport/patient/creategrantsupport.aspx"))
            {

                Utilities.SessionInfo.ResetPatientGrantPreviewSession();
            }

            if (!UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/grantsupport/provider/creategrantsupport.aspx"))
            {

                Utilities.SessionInfo.ResetProviderGrantPreviewSession();
            }

            if (UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/resourcegraphics/patient/createresourcegraphics.aspx"))
            {
                aLogout.Attributes.Add("onclick", "upLoadPatientFlash();");
            }
            else if (UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("pages/resourcegraphics/provider/createresourcegraphics.aspx"))
            {
                aLogout.Attributes.Add("onclick", "upLoadProviderFlash();");
            }
            else if (UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("/pages/grantsupport/patient/creategrantsupport.aspx"))
            {
                aLogout.Attributes.Add("onclick", "upLoadPatientLogo();");
            }
            else if (UrlUtility.FullCurrentPageUrl.ToLower().EndsWith("/pages/grantsupport/provider/creategrantsupport.aspx"))
            {
                aLogout.Attributes.Add("onclick", "upLoadProviderLogo();");
            }
        }




        protected void aLogout_onserverclick(object sender, EventArgs e)
        {
            //Utilities.Utility.ResetCurrentLoggedinUser();
            Utilities.SessionInfo.ResetCurrentLoggedinUser();
            Response.Redirect("~/Login.aspx");
        }

        private void _SetLinkCss()
        {
            AdminPage objPage = PageInfo.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);

            if (objPage != null && !string.IsNullOrEmpty(objPage.MainTabID))
            {
                HtmlAnchor link = FindControl(objPage.MainTabID) as HtmlAnchor;

                if (link != null)
                {
                    if (!string.IsNullOrEmpty(link.Attributes["class"]))
                    {
                        link.Attributes.Add("class", string.Format("{0} current", link.Attributes["class"]));
                    }
                    else
                    {
                        link.Attributes.Add("class", "current");
                    }
                }
            }
        }
    }
}
