﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls.Common
{
    public partial class AHAHeader : System.Web.UI.UserControl
    {
        bool m_HideDonate = true;
        public bool HideDonate
        {
            get
            {
                return m_HideDonate;
            }
            set
            {
                m_HideDonate = value;
            }
        }

        bool m_HideSearch = true;
        public bool HideSearch
        {
            get
            {
                return m_HideSearch;
            }
            set
            {
                m_HideSearch = value;
            }
        }

        bool m_HidePersonas = true;
        public bool HidePersonas
        {
            get
            {
                return m_HidePersonas;
            }
            set
            {
                m_HidePersonas = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}