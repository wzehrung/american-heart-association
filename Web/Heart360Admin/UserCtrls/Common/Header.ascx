﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Heart360Admin.UserCtrls.Common.Header" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<div id="header">
    <div class="redbar">
        <div class="redbarmenuContainer">
            <div class="redbarmenu" onmouseover="showdropdownmenu(this);" onmouseout="hidedropdownmenu(this);"
                style="clip: rect(auto auto 27px auto);">
                <div class="redbar-tl-curve">
                </div>
                <div class="redbar-tr-curve">
                </div>
                <a class="redbarmenuHead" href="javascript:;" id="linkPatient" runat="server">
                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Patient_Section")%></a>
                <div class="redbarsubmenu">

                    <a id="menu1" href="/Pages/Resources/Patient/ResourceList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Resources_")%></a>
                    <a id="menu4" href="/Pages/Guidingtext/Patient/GuidingTextList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Guiding_Text")%></a>
                    <a id="menu5" href="/Pages/TipOfTheDay/Patient/TipList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Tip_Of_The_Day")%></a>
                    <a id="menu6" href="/Pages/GrantSupport/Patient/GrantSupportList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Grant_Support")%></a>
                    <a id="menu7" href="/Pages/ResourceGraphics/Patient/CreateResourceGraphics.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Resource_Graphics")%></a>
                    <a id="menu8" href="../../Pages/PatientEdu.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Helpful_Resources")%></a>
                    <a id="menu9" href="../../Pages/Reports/PatientHome.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Patient")%></a>
                    <a id="menu31" href="/Pages/Patient/Acceptance.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "User_Acceptance")%></a>
                    
                    <div class="redMenuBottomLeft"></div>
                    <div class="redMenuBottom"></div>
                    <div class="redMenuBottomRight"></div>
                </div>
            </div>
        </div>

        <div class="redbarmenuContainer">
            <div class="redbarmenu" onmouseover="showdropdownmenu(this);" onmouseout="hidedropdownmenu(this);"
                style="clip: rect(auto auto 27px auto);">
                <div class="redbar-tl-curve">
                </div>
                <div class="redbar-tr-curve">
                </div>
                <a class="redbarmenuHead" href="javascript:;" id="linkProvider" runat="server">
                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Provider_Section")%></a>
                <div class="redbarsubmenu">

                    <a id="menu2" href="/Pages/Resources/Provider/ResourceList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Resources_")%></a>
                    <a id="menu10" href="/Pages/Guidingtext/Provider/GuidingTextList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Guiding_Text")%></a>
                    <a id="menu11" href="/Pages/TipOfTheDay/Provider/TipList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Tip_Of_The_Day")%></a>
                    <a id="menu12" href="/Pages/GrantSupport/Provider/GrantSupportList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Grant_Support")%></a>
                    <a id="menu13" href="/Pages/ResourceGraphics/Provider/CreateResourceGraphics.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Resource_Graphics")%></a>
                    <a id="menu14" href="../../Pages/Announcement.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Announcement_")%></a>
                    <a id="menu28" href="../../Pages/Volunteer/VolunteerList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AssignCampaigns_Provider")%></a>
                    <a id="menu15" href="../../Pages/Reports/ProviderHome.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Reports_Provider")%></a>

                    <div class="redMenuBottomLeft"></div>
                    <div class="redMenuBottom"></div>
                    <div class="redMenuBottomRight"></div>
                </div>
            </div>
        </div>

        <div class="redbarmenuContainer">
            <div class="redbarmenu" onmouseover="showdropdownmenu(this);" onmouseout="hidedropdownmenu(this);"
                style="clip: rect(auto auto 27px auto);">
                <div class="redbar-tl-curve">
                </div>
                <div class="redbar-tr-curve">
                </div>
                <a class="redbarmenuHead" href="javascript:;" id="linkMisc" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Miscellaneous_")%></a>
                <div class="redbarsubmenu">

                    <a id="menu3" href="/Pages/User/UserList.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Users")%></a>
                    <a id="menu25" href="/Pages/User/UserRoles.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Roles")%></a>
                    <a id="menu22" href="/Pages/User/ChangePassword.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Change_Your_Password")%></a>
                    <a id="menu23" href="/Pages/Campaign/ActiveCampaigns.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Active_Campaigns")%></a>
                    <a id="menu30" href="/Pages/Campaign/InactiveCampaigns.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Inactive_Campaigns")%></a>
                    <a id="menu24" href="~/EmailLists/Schedule.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_EmailLists")%></a>
                    <a id="menu27" href="/Pages/Businesses/AllBusinesses.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Businesses")%></a>
                    <a id="menu29" href="/Pages/Feedback.aspx" runat="server"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Manage_Feedback")%></a>

                    <div class="redMenuBottomLeft"></div>
                    <div class="redMenuBottom"></div>
                    <div class="redMenuBottomRight"></div>
                </div>
            </div>
        </div>

        <div id="signin">
            <GRC:GRCComboBox ID="ddlLanguage" OnChangeFunctionName="OnChangeLanguage" Width="50"
                runat="server" />
            <asp:Button ID="btnLanguage" runat="server" Style="display: none;" OnClick="OnLanguageChanged" />
        </div>
        <%--<div class="change-language" runat="server" id="chLanguage">
            <div id="language" class="select-language" runat="server">
                English</div>
            <div class="change-language-option" id="change-language">
                <div class="change-language-option-in">
                    <ul runat="server" id="ulLanguages">
                    </ul>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="greybar" id="DivDashBoardTitle">
        <div id="subnav">
            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Welcome_")%>
            <asp:Literal ID="litName" runat="server"></asp:Literal>
            | <a runat="server" id="aLogout" onclick="OnLogout()" onserverclick="aLogout_onserverclick">
                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Sign_Out")%></a>
        </div>
    </div>
</div>
<script type="text/javascript">    function upLoadPatientFlash() {
        document.forms[0].target
= '_self'; document.forms[0].action = "/Pages/ResourceGraphics/Patient/CreateResourceGraphics.aspx";
    } function upLoadProviderFlash() {
        document.forms[0].target = '_self'; document.forms[0].action
= "/Pages/ResourceGraphics/Provider/CreateResourceGraphics.aspx";
    } function upLoadPatientLogo() {
        document.forms[0].target = '_self'; document.forms[0].action = "/Pages/GrantSupport/Patient/CreateGrantSupport.aspx";
    } function upLoadProviderLogo() {
        document.forms[0].target = '_self'; document.forms[0].action
= "/Pages/GrantSupport/Provider/CreateGrantSupport.aspx";
    } function OnLogout()
    { document.forms[0].target = '_self'; } function OnChangeLanguage(item) {
        document.getElementById('<%=btnLanguage.ClientID%>').click();
    } </script>
