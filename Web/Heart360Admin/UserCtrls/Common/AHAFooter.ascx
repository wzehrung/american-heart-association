﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AHAFooter.ascx.cs" Inherits="Heart360Admin.UserCtrls.Common.AHAFooter" %>

<script type="text/javascript">
    var footerContactUs = new Object();
    footerContactUs['Address'] = new Array("7272 Greenville Ave.", "Dallas, TX 75231");
    footerContactUs['Customer Service'] = new Array("1-800-AHA-USA1");
</script>

<script type="text/javascript" src="<%=Heart360Admin.Utilities.AppSettings.IsSSLEnabled ? "https://static.heart.org/aha_footer.js" : "http://static.heart.org/aha_footer.js"%>"></script>

<script type="text/javascript">
    if (typeof window.addEventListener !== 'undefined') {

        window.addEventListener('load', removeReset, false);

    } else if (typeof window.attachEvent !== 'undefined') {
        window.attachEvent('onload', removeReset);

    }
    function removeReset() { window.setTimeout(r2, 5); }

    function r2() {

        for (var i = document.styleSheets.length - 1; i >= 0; i--) {
            if (document.styleSheets[i].href) {
                if (document.styleSheets[i].href.indexOf("static.heart.org/css/reset.css") >= 0) {
                    document.styleSheets[i].disabled = true;
                    break;
                }
            }
        }
    } 
</script>

