﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeCtrl.ascx.cs" Inherits="Heart360Admin.UserCtrls.HomeCtrl" %>
<h2>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Welcome_")%>
    <asp:Literal ID="litName" runat="server"></asp:Literal></h2>
<br />
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "UseTheMenu1")%><br /> <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "UseTheMenu2")%>
</p>
<br />
