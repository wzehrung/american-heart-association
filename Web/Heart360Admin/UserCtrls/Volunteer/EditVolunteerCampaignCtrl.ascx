﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditVolunteerCampaignCtrl.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.Volunteer.EditVolunteerCampaignCtrl" %>
<h1>
    <asp:Literal ID="litMode" runat="server" ></asp:Literal></h1>
<p>
    <asp:Literal ID="litText" runat="server"></asp:Literal></p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellspacing="0" cellpadding="0" border="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td  class="align-right nowrap">Last Name</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLastName" Width="200" Enabled="false" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td  class="align-right nowrap">First Name</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtFirstName" Width="200" Enabled="false" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top"  class="align-right nowrap">Email</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtEmail" Width="200" Enabled="false" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="align-right nowrap">
                                    Campaign<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCampaigns" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCampaigns_SelectedIndexChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Save</em></strong></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" ID="lnkCancel" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Cancel</em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>

    <asp:Literal runat="server" ID="litRoleName" Visible="false"></asp:Literal>
</div>
