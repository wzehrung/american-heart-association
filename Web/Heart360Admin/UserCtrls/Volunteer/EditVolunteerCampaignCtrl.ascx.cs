﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GRCBase;
using System.Text.RegularExpressions;
using AHAHelpContent;

namespace Heart360Admin.UserCtrls.Volunteer
{
    public partial class EditVolunteerCampaignCtrl : System.Web.UI.UserControl
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        public enum Mode
        {
            Add,
            Edit
        }

        public Mode DisplayMode
        {
            get;
            set;
        }

        private void _Initialize()
        {

            AHAHelpContent.Volunteer objVol = AHAHelpContent.Volunteer.GetUserByID(Convert.ToInt32(Request["UID"]));

            if (objVol != null)
            {
                txtFirstName.Text = objVol.FirstName;
                txtLastName.Text = objVol.LastName;
                txtEmail.Text = objVol.Email;

                if (!String.IsNullOrEmpty(Request["CID"]))
                {
                    ddlCampaigns.SelectedValue = Request["CID"];
                }
                else
                {
                    ddlCampaigns.Items.Insert(0, new System.Web.UI.WebControls.ListItem("<undefined>", "0"));
                    lnkSave.Enabled = false;
                }
                
            }
            else
            {
                ddlCampaigns.Enabled = false;
                lnkSave.Visible = false;
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _LoadCampaignsDropdown();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                litMode.Text = "Update User Campaign";
                litText.Text = "Use the form below to modify a user.";

                if (!String.IsNullOrEmpty(Request["UID"]))
                {
                    _Initialize();
                    lnkSave.Text = "<strong><em>Update</em></strong>";
                }
            }
        }

 
        private void _LoadCampaignsDropdown()
        {
            DataSet ds = AHAHelpContent.PatientReport.GetCampaignsForAffiliatesReport(null);

            ddlCampaigns.DataSource = ds;
            ddlCampaigns.DataTextField = "Title";
            ddlCampaigns.DataValueField = "CampaignID";
            ddlCampaigns.DataBind();

            // setting current item is done in _Initialize().
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request["UID"]))
            {
                int volunteerId = Convert.ToInt32(Request["UID"]);
                int campaignId = Convert.ToInt32(ddlCampaigns.SelectedValue);
                AHAHelpContent.Volunteer.UpdateUserCampaign(volunteerId, campaignId);
            }

            Response.Redirect("/Pages/Volunteer/VolunteerList.aspx");
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Volunteer/VolunteerList.aspx");
        }

        protected void ddlCampaigns_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCampaigns.SelectedIndex == 0 && string.Compare("0", ddlCampaigns.SelectedValue) == 0)
                lnkSave.Enabled = false;
            else
                lnkSave.Enabled = true;

        }

        
    }
}
