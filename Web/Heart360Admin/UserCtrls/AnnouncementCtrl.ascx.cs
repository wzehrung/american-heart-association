﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls
{
    public partial class AnnouncementCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AHAHelpContent.ProviderContent objContent = AHAHelpContent.ProviderContent.FindContentByZoneTitle(AHACommon.AHADefs.ContentType.ProviderAnnouncement.ToString(),Utilities.Utility.GetCurrentLanguageID());
                if (objContent != null)
                {
                    txtTitle.Text = objContent.ContentTitle;
                    txtText.Text = objContent.ContentText;
                    hddnContentID.Value = objContent.ContentID.ToString();
                }
                lnkAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfo.aspx?id=" + objContent.ContentID.ToString() + "&Type=" + AHAHelpContent.AuditTrailType.ProviderContent.ToString() + "')");
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            AHAHelpContent.ProviderContent.UpdateContent(Convert.ToInt32(hddnContentID.Value),AHACommon.AHADefs.ContentType.ProviderAnnouncement.ToString(),
                                                        txtTitle.Text.Trim(), null, txtText.Text.Trim(),Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID,Utilities.Utility.GetCurrentLanguageID());
            //litSuccess.Text = "<div class=\"sucess\">Provider Announcement has been successfully updated</div>";
            litSuccess.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Announcement_Succesfully").ToString(); ;
        }

        protected void lnkPreview_OnClick(object sender, EventArgs e)
        {
            Utilities.Announcement objAnnouncement = new Heart360Admin.Utilities.Announcement(txtTitle.Text.Trim(), txtText.Text.Trim());
            Utilities.SessionInfo.ResetProviderAnnouncementPreviewToSession();
            Utilities.SessionInfo.SetProviderAnnouncementPreviewToSession(objAnnouncement);
            Response.Redirect("~/Pages/PreviewAnnouncement.aspx");
        }

        #region Validators
        //protected void OnValidateTitle(object sender, ServerValidateEventArgs e)
        //{
        //    CustomValidator custValidator = sender as CustomValidator;

        //    if (txtTitle.Text.Trim().Length == 0)
        //    {
        //        e.IsValid = false;
        //        custValidator.ErrorMessage = "Please enter a Title";
        //        return;
        //    }

        //    return;
        //}

        //protected void OnValidateText(object sender, ServerValidateEventArgs e)
        //{
        //    CustomValidator custValidator = sender as CustomValidator;

        //    if (txtText.Text.Trim().Length == 0)
        //    {
        //        e.IsValid = false;
        //        custValidator.ErrorMessage = "Please enter a Text";
        //        return;
        //    }

        //    return;
        //}
        #endregion
    }
}