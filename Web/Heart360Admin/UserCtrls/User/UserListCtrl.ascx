﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserListCtrl.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.User.UserListCtrl" %>
<h1>
    Users</h1>
<p>
    Below is the list of all admin users. To add a new user click on the 'Add New' link below. Super admin users have access to the admin user list and can add/modify/disable an admin user.</p>
<table class="admin-box" cellpadding="0" cellspacing="0">
    <tr>
        <td class="align-right menubar">
            <a href="/Pages/User/AddUser.aspx" class="aha-small-button with-arrow"><strong><em>Add
                New</em></strong></a>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="up1">
                <ContentTemplate>
                    <asp:GridView ShowHeader="true" EnableViewState="false" ID="gvUserList"
                        runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="There are no Resources."
                        AllowPaging="True" PageSize="25" ShowFooter="False" OnRowDataBound="gvUserList_RowDataBound"
                        OnPageIndexChanging="gvUserList_PageIndexChanging" GridLines="Horizontal" CssClass="provider-grid"
                        PagerStyle-CssClass="table-page-index">
                        <Columns>
                            <asp:TemplateField HeaderText="User Name" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litUserName"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal ID="litName" runat="server"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Role" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal ID="litSuperAdmin" runat="server"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="Actions" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <a id="aChngPassword" onserverclick="aChngPassword_serverclick" runat="server">Change
                                        Password</a>&nbsp;|&nbsp;<a runat="server" id="aEdit" onserverclick="aEdit_serverclick">Edit</a>&nbsp;<asp:Literal ID="litPipeChar" runat="server">|</asp:Literal>&nbsp;<a
                                            runat="server" id="aDisable"></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" />
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
