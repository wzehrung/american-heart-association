﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePasswordCtrl.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.User.ChangePasswordCtrl" %>
<h1>
    Change Password</h1>
<p>
    Use the form below to change the admin user password.</p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellspacing="0" cellpadding="0" border="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2" class="red align-right">
                                  * Mandatory
                                        <asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="Please correct the following" runat="server" ID="validSummary"
                                            ValidationGroup="Change" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Literal ID="litSuccess" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            <tr id="trUserName" runat="server">
                                <td  class="align-right nowrap">
                                    Username&nbsp;
                                </td>
                                <td>
                                   <b><asp:Label ID="lblUserName" runat="server"></asp:Label></b> 
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    Password <b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtPassword" Width="200" TextMode="Password"></asp:TextBox>
                                    <%-- <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidPassword"
                runat="server" Display="None" OnServerValidate="OnValidatePassword">
            </asp:CustomValidator>--%>
                                    <asp:RequiredFieldValidator ID="rqPassword" ControlToValidate="txtPassword" EnableClientScript="false"
                                        ValidationGroup="Change" Display="None" ErrorMessage="Enter a password"
                                        runat="server"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    Confirm Password<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtConfirmPassword" Width="200" TextMode="Password" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rqConfirmPassword" ControlToValidate="txtConfirmPassword"
                                        EnableClientScript="false" ValidationGroup="Change" Display="None" ErrorMessage="Confirm the password"
                                        runat="server"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cmpPassword" ValidationGroup="Change" Display="None" EnableClientScript="false"
                                        ControlToValidate="txtPassword" ControlToCompare="txtConfirmPassword" ErrorMessage="Password and ConfirmPassword must be the same"
                                        runat="server"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ValidationGroup="Change" runat="server" ID="lnkSave" OnClick="btnSave_OnClick"
                                        CssClass="aha-small-button with-arrow"><strong><em>Save</em></strong></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" ID="lnkCancel" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Cancel</em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>
