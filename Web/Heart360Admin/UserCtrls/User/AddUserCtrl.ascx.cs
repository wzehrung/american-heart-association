﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;
using GRCBase;
using System.Text.RegularExpressions;

namespace Heart360Admin.UserCtrls.User
{
    public partial class AddUserCtrl : System.Web.UI.UserControl
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        public enum Mode
        {
            Add,
            Edit
        }

        public Mode DisplayMode
        {
            get;
            set;
        }

        private void _Initialize()
        {
            trPassword.Visible = false;
            trConfirmPassword.Visible = false;
            txtUserName.Visible = false;
            AdminUser objAdminUser = AdminUser.GetAdminUserByID(Convert.ToInt32(Request["UID"]));
            lblUserName.Text = objAdminUser.UserName;
            txtName.Text = objAdminUser.Name;
            txtEmail.Text = objAdminUser.Email;
            ddlRole.SelectedValue = objAdminUser.RoleID.ToString();
            litRoleName.Text = objAdminUser.RoleName;

            ddlBusiness.SelectedValue = objAdminUser.BusinessID.ToString();

            PerformBusinessUxVisibility();


            if (objUserContext.UserID == Convert.ToInt32(Request["UID"]))
            {
                trIsSuperAdmin.Visible = false;
                trBusiness.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (DisplayMode == Mode.Add)
                {
                    litMode.Text = "Create New User";
                    litText.Text = "Use the form below to add a new admin user.";
                }
                else
                {
                    litMode.Text = "Update User";
                    litText.Text = "Use the form below to modify a  admin user.";
                }

                if (!String.IsNullOrEmpty(Request["UID"]))
                {
                    _Initialize();
                    lnkSave.Text = "<strong><em>Update</em></strong>";
                }
                else
                {
                    lblUserName.Visible = false;
                    lnkSave.Text = "<strong><em>Create</em></strong>";
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _LoadRolesDropdown();
            _LoadBusinessDropdown();
            PerformBusinessUxVisibility();
        }

        private void _LoadRolesDropdown()
        {
            List<AHAHelpContent.AdminRoles> list = AHAHelpContent.AdminRoles.GetAllAdminRoles();
            ddlRole.DataSource = list;
            ddlRole.DataTextField = "Name";
            ddlRole.DataValueField = "RoleID";
            ddlRole.DataBind();
        }

        private void _LoadBusinessDropdown()
        {
            List<AHAHelpContent.Business> list = AHAHelpContent.Business.FindAllBusinesses();
            ddlBusiness.DataSource = list;
            ddlBusiness.DataTextField = "Name";
            ddlBusiness.DataValueField = "BusinessID";
            ddlBusiness.DataBind();
        }

        protected void ddlRole_SelectedIndexChanged( object sender, EventArgs e )
        {
            PerformBusinessUxVisibility();
        }

        private void PerformBusinessUxVisibility()
        {
            if (ddlRole.SelectedIndex == (ddlRole.Items.Count - 1))
            {
                trBusiness.Visible = true;
            }
            else
            {
                trBusiness.Visible = false;
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");
            if (!Page.IsValid)
                return;

            int bizIdToUse = (trBusiness.Visible) ? Convert.ToInt32(ddlBusiness.SelectedValue) : 0 ;    // 0 = N/A

            if (String.IsNullOrEmpty(Request["UID"]))
            {
                AdminUser.CreateAdminUser(txtName.Text.Trim(),
                                          txtUserName.Text.Trim(),
                                          Utilities.Utility.GetHashString(txtPassword.Text),
                                          txtEmail.Text.Trim(),
                                          Convert.ToInt32(ddlRole.SelectedValue),
                                          bizIdToUse,
                                          Heart360Admin.HttpModule.AdminRequestContext.GetContext().SelectedLanguage.LanguageID);

            }
            else
            {
                AdminUser.UpdateAdminUser(Convert.ToInt32(Request["UID"]),
                                          txtName.Text.Trim(),
                                          txtEmail.Text.Trim(),
                                          Convert.ToInt32(ddlRole.SelectedValue),
                                          bizIdToUse);
                if (objUserContext.UserID == Convert.ToInt32(Request["UID"]))
                {
                    //Utilities.Utility.SetCurrentLoggedinUser(objUserContext.UserID, objUserContext.UserName, txtName.Text.Trim(), objUserContext.IsSuperAdmin);
                    Utilities.SessionInfo.SetCurrentLoggedinUser(objUserContext.UserID, objUserContext.UserName, txtName.Text.Trim(), objUserContext.RoleID, objUserContext.RoleName, objUserContext.LanguageID, objUserContext.LanguageLocale, objUserContext.BusinessID);
                }

            }
            Response.Redirect("/Pages/User/UserList.aspx");
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/User/UserList.aspx");
        }

        protected void OnValidateUserName(object sender, ServerValidateEventArgs v)
        {
            //CustomValidator custValidUserName = sender as CustomValidator;
            //if (string.IsNullOrEmpty(Request["UID"]) && 
            //    string.IsNullOrEmpty(txtUserName.Text.Trim()))
            //{
            //    v.IsValid = false;
            //    custValidUserName.ErrorMessage = "Enter a UserName";
            //    return;
            //}
            //else
            //{
            //    AdminUser objAdminUser = AdminUser.GetAdminUserByUserName(txtUserName.Text.Trim());
            //    if (objAdminUser != null)
            //    {
            //        v.IsValid = false;
            //        custValidUserName.ErrorMessage = "Enter a different UserName";
            //        return;
            //    }
            //}

            CustomValidator custValidUserName = sender as CustomValidator;
            if (string.IsNullOrEmpty(Request["UID"]))
            {
                if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
                {
                    v.IsValid = false;
                    custValidUserName.ErrorMessage = "Enter a UserName";
                    return;
                }
                else if (!Regex.IsMatch(txtUserName.Text.Trim(), @"^[a-zA-Z0-9.]{1,128}$"))
                {
                    v.IsValid = false;
                    custValidUserName.ErrorMessage = "UserName can contain only letters (a-z,A-Z), numbers (0-9), and periods (.)";
                    return;
                }
                else
                {
                    AdminUser objAdminUser = AdminUser.GetAdminUserByUserName(txtUserName.Text.Trim());
                    if (objAdminUser != null)
                    {
                        v.IsValid = false;
                        custValidUserName.ErrorMessage = "Enter a different UserName";
                        return;
                    }
                }
            }

            return;
        }

        protected void OnValidatePassword(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidPassword = sender as CustomValidator;
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                v.IsValid = false;
                custValidPassword.ErrorMessage = "Enter a password";
                return;
            }
            return;
        }

        protected void OnValidateName(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidName = sender as CustomValidator;
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                v.IsValid = false;
                custValidName.ErrorMessage = "Enter a Name";
                return;
            }
            return;
        }

        protected void OnValidateEmail(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidEmail = sender as CustomValidator;
            if (string.IsNullOrEmpty(txtEmail.Text.Trim()))
            {
                v.IsValid = false;
                custValidEmail.ErrorMessage = "Enter a Email";
                return;
            }
            else if (!EmailSyntaxValidator.Valid(txtEmail.Text.Trim(), true))
            {
                v.IsValid = false;
                custValidEmail.ErrorMessage = "Enter a valid Email";
            }
            return;
        }
    }
}
