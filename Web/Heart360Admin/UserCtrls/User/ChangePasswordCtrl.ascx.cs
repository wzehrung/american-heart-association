﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;

namespace Heart360Admin.UserCtrls.User
{
    public partial class ChangePasswordCtrl : System.Web.UI.UserControl
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                //return Utilities.Utility.GetUserContext();
                return Utilities.SessionInfo.GetUserContext();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["UID"]))
            {
                AdminUser objAdminUser = AdminUser.GetAdminUserByID(Convert.ToInt32(Request["UID"]));
                lblUserName.Text = objAdminUser.UserName;
            }
            else
            {
                //trUserName.Visible = false;
                lblUserName.Text = objUserContext.UserName;
            }

        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Change");
            if (!Page.IsValid)
                return;

            if (!string.IsNullOrEmpty(Request["UID"]))
            {
                AdminUser.ChangePassword(Convert.ToInt32(Request["UID"]), Utilities.Utility.GetHashString(txtPassword.Text));
                Response.Redirect("/Pages/User/UserList.aspx");
            }
            else
            {
                if (objUserContext != null)
                {
                    AdminUser.ChangePassword(objUserContext.UserID, Utilities.Utility.GetHashString(txtPassword.Text));
                    litSuccess.Text = "<div class=\"sucess\">Your password has been successfully changed</div>";
                }
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["UID"]))
            {
                Response.Redirect("/Pages/User/UserList.aspx");
            }
            else
            {
                if (objUserContext != null && objUserContext.RoleID.ToString() == "1")
                {
                    Response.Redirect("/Pages/Home.aspx");
                }
            }
        }

       
    }
}