﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddUserCtrl.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.User.AddUserCtrl" %>
<h1>
    <asp:Literal runat="server" ID="litMode"></asp:Literal></h1>
<p>
    <asp:Literal ID="litText" runat="server"></asp:Literal></p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellspacing="0" cellpadding="0" border="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2" class="red align-right">
                                    <b>* Mandatory</b>
                                    <asp:Literal runat="server" ID="litError"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="Please correct the following" runat="server" ID="validSummary"
                                            ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true" 
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    UserName<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserName" runat="server" Width="200"></asp:TextBox>
                                    <asp:Label ID="lblUserName" runat="server"></asp:Label>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidUserName"
                                        runat="server" Display="None" OnServerValidate="OnValidateUserName">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr id="trPassword" runat="server">
                                <td  class="align-right nowrap">
                                    Password <b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" Width="200"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidPassword"
                                        runat="server" Display="None" OnServerValidate="OnValidatePassword">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr id="trConfirmPassword" runat="server">
                                <td  class="align-right nowrap">
                                    Confirm Password<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" Width="200"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rqConfirmPassword" ControlToValidate="txtConfirmPassword"
                                        EnableClientScript="false" ValidationGroup="Create" Display="None" ErrorMessage="Please confirm the password"
                                        runat="server"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cmpPassword" ValidationGroup="Create" Display="None" EnableClientScript="false"
                                        ControlToValidate="txtPassword" ControlToCompare="txtConfirmPassword" ErrorMessage="Password and ConfirmPassword must be the same"
                                        runat="server"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td  class="align-right nowrap">
                                    Name<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtName" Width="200"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidName"
                                        runat="server" Display="None" OnServerValidate="OnValidateName">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top"  class="align-right nowrap">
                                    Email<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtEmail" Width="200"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidEmail"
                                        runat="server" Display="None" OnServerValidate="OnValidateEmail">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr id="trIsSuperAdmin" runat="server">
                                <td valign="top" class="align-right nowrap">
                                    Select Role<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" />
                                </td>
                            </tr>
                            <tr id="trBusiness" runat="server">
                                <td valign="top" class="align-right nowrap">
                                    Business<b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlBusiness" runat="server" AutoPostBack="false" />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Save</em></strong></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" ID="lnkCancel" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Cancel</em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>

    <asp:Literal runat="server" ID="litRoleName" Visible="false"></asp:Literal>
</div>
