﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls
{
    public partial class AuditTrailCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _bindDataToGridView();
            }
        }

        protected void gvAuditTrail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            
            Literal litName = e.Row.FindControl("litName") as Literal;
            Literal litAuditDate = e.Row.FindControl("litAuditDate") as Literal;
            Literal litTitle = e.Row.FindControl("litTitle") as Literal;
            
            AHAHelpContent.AuditTrail objAuditInfo = e.Row.DataItem as AHAHelpContent.AuditTrail;
            litName.Text = objAuditInfo.Name;
            litAuditDate.Text = objAuditInfo.AuditDate.ToString();
        }

        protected void gvAuditTrail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAuditTrail.PageIndex = e.NewPageIndex;
            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            AHAHelpContent.AuditTrailType eAuditTrailType = (AHAHelpContent.AuditTrailType)Enum.Parse(typeof(AHAHelpContent.AuditTrailType), Request["Type"], true);
            gvAuditTrail.DataSource = AHAHelpContent.AuditTrail.GetAuditDetails(eAuditTrailType, Convert.ToInt32(Request["id"]),Utilities.Utility.GetCurrentLanguageID());
            gvAuditTrail.DataBind();
        }
    }
}