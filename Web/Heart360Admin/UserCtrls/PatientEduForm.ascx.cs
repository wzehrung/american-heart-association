﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Heart360Admin.UserCtrls
{
    public partial class PatientEduForm : System.Web.UI.UserControl
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                //return Utilities.Utility.GetUserContext();
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            setDateFormat();
            if (!Page.IsPostBack)
            {
                _LoadContentRules();
                if (!String.IsNullOrEmpty(Request["CID"]) && String.IsNullOrEmpty(Request["IsCreated"]))
                {
                    //litMode.Text = "Update Patient Helpful Resources Item";
                    //lnkSave.Text = "<strong><em>Update</em></strong>";
                    //litText.Text = "Use the form below to update a new helpful link on the right rail of the patient portal.";

                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_PatientHelpfulResource").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "update_a_new_helpful").ToString();
                    _Initialize();
                }
                else if (!String.IsNullOrEmpty(Request["CID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
                {
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_PatientHelpfulResource").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "update_a_new_helpful_ForAnother").ToString();
                    _Initialize();
                }
                else
                {
                    //lnkSave.Text = "<strong><em>Create</em></strong>";
                    //litMode.Text = "Create Patient Helpful Resources Item";
                    //litText.Text = "Use the form below to create a new helpful link on the right rail of the patient portal.";

                    lnkSave.Text = "<strong><em>" + GetGlobalResourceObject("Heart360AdminGlobalResources", "Create").ToString() + "</em></strong>";
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Patient_HelpFul").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Use_the_form_below_to_create_a_new_helpful_link").ToString();
                }
            }
        }
        protected void setDateFormat()
        {
            if (System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower() == "es-mx")
            {
                dtDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.DDMMYYYY;
            }
            else
            {
                dtDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.MMDDYYYY;
            }
        }

        private void _Initialize()
        {
            AHAHelpContent.Content objContent = AHAHelpContent.Content.FindByContentID(Convert.ToInt32(Request["CID"]),Utilities.Utility.GetCurrentLanguageID());
            if (objContent != null)
            {
                txtTitle.Text = objContent.Title;
                txtURL.Text = objContent.URL;
                txtURLText.Text = objContent.DidYouKnowText;
                txtURLTitle.Text = objContent.DidYouKnowLinkTitle;
                dtDate.Date = objContent.ExpirationDate;
                foreach (ListItem item in chkRuleList.Items)
                {
                    foreach (string ruleid in objContent.ContentRuleNames.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (item.Value == ruleid)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "update_a_new_helpful").ToString();
            }
        }

        private void _LoadContentRules()
        {
            List<AHAHelpContent.ContentRules> lstContentRules = AHAHelpContent.ContentRules.GetAllContentRules(Utilities.Utility.GetCurrentLanguageID());
            if (lstContentRules.Count > 0)
            {
                chkRuleList.DataSource = lstContentRules;
                chkRuleList.DataTextField = "DisplayName";
                chkRuleList.DataValueField = "ContentRuleID";
                chkRuleList.DataBind();
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/PatientEdu.aspx");
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");
            if (!Page.IsValid)
                return;
            List<string> listRuleIds = _GetRuleIds();

            if (!String.IsNullOrEmpty(Request["CID"]) && String.IsNullOrEmpty(Request["IsCreated"]))
            {
                AHAHelpContent.Content.UpdateContent(txtTitle.Text.Trim(), txtURL.Text.Trim(), txtURLTitle.Text.Trim(), txtURLText.Text.Trim(), Convert.ToInt32(Request["CID"]), listRuleIds, dtDate.Date, objUserContext.UserID, objUserContext.UserID, Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("~/Pages/PatientEdu.aspx");
            }
            else if (!String.IsNullOrEmpty(Request["CID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
            {
                AHAHelpContent.Content.UpdateContent(txtTitle.Text.Trim(), txtURL.Text.Trim(), txtURLTitle.Text.Trim(), txtURLText.Text.Trim(), Convert.ToInt32(Request["CID"]), listRuleIds, dtDate.Date, objUserContext.UserID, objUserContext.UserID, Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("~/Pages/PatientEdu.aspx");
            }
            else
            {
               AHAHelpContent.Content objContent= AHAHelpContent.Content.CreateContent(txtTitle.Text.Trim(), txtURL.Text.Trim(), txtURLTitle.Text.Trim(), txtURLText.Text.Trim(), listRuleIds, dtDate.Date, objUserContext.UserID, objUserContext.UserID, Utilities.Utility.GetCurrentLanguageID());
               Response.Redirect("~/Pages/CreatePatientEdu.aspx?CID=" + objContent.ContentID.ToString() + "&IsCreated=1");
            }
           
        }

        protected void lnkPreview_OnClick(object sender, EventArgs e)
        {
            Utilities.HelpfulResource objResource = new Heart360Admin.Utilities.HelpfulResource(txtURL.Text.Trim(), txtURLTitle.Text.Trim());
            Utilities.SessionInfo.ResetPatientHelpfulResourcePreviewToSession();
            Utilities.SessionInfo.SetPatientHelpfulResourcePreviewToSession(objResource);
            Response.Redirect("~/Pages/PreviewPatientEdu.aspx");
        }

        private List<string> _GetRuleIds()
        {
            List<string> listRuleIds = new List<string>();
            foreach (ListItem item in chkRuleList.Items)
            {
                if (item.Selected)
                    listRuleIds.Add(item.Value);
            }

            return listRuleIds;
        }

        #region Validators
        protected void OnValidateTitle(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtTitle.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a Title";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Title").ToString(); ;
                return;
            }

            return;
        }

        protected void OnValidateURL(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtURL.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a URL";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Url").ToString();
                return;
            }
            else
            {
                Match url = Regex.Match(txtURL.Text.Trim(), "(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?");
                if (!url.Success)
                {
                    e.IsValid = false;
                    //custValidator.ErrorMessage = "Enter a valid URL";
                    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_ValidUrl").ToString();
                    return;
                }
            }

            return;
        }

        protected void OnValidateURLTitle(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtURLTitle.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a URL Title";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_UrlTitle").ToString();
                return;
            }

            return;
        }

        protected void OnValidateURLText(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtURLText.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a URL Text";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_UrlText").ToString();
                return;
            }

            return;
        }

        protected void OnValidateTag(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;
            int iTagCount = _GetRuleIds().Count;
            if (iTagCount == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Select at least any one of the tags.";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Select_Atleast_OneTag").ToString();
                return;
            }

            return;
        }

        protected void custExpirationDate_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (dtDate.Date == null)
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Select a expiration date";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Select_Expiration_Date").ToString();
                return;
            }
            //if (!string.IsNullOrEmpty(dtDate.Text) && !AHACommon.Helper.IsValidDate(dtDate.Date))
            //{
            //    v.IsValid = false;
            //    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Expiry_Date").ToString();
            //    return;
            //}
            else if (dtDate.Date != null && dtDate.Date.Value <= DateTime.Today)
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Select a future date";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Select_Future_Date").ToString(); ;
                return;
            }
            return;
        }

        #endregion
    }
}