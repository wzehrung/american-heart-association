﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls
{
    public partial class HomeCtrl : System.Web.UI.UserControl
    {
        Utilities.UserContext objUserContext
        {
            get
            {
                //return Utilities.Utility.GetUserContext();
                return Utilities.SessionInfo.GetUserContext();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (objUserContext != null)
            {
                litName.Text = objUserContext.Name;
            }
        }
    }
}