﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls.TipOfTheDay.Patient
{
    public partial class AddPatientTips : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _LoadContentRules();
                //if (!String.IsNullOrEmpty(Request["TID"]))
                if (!String.IsNullOrEmpty(Request["TID"]) && String.IsNullOrEmpty(Request["IsCreated"]))
                {
                    //litMode.Text = "Update Patient Tip Of The Day Item";
                    //lnkSave.Text = "<strong><em>Update</em></strong>";
                    //litText.Text = "Use the form below to update a tip.";

                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_PatientTip").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateGuidTip_Text").ToString();
                    _Initialize();
                }
                else if (!String.IsNullOrEmpty(Request["TID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
                {

                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_PatientTip").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Patient_ForAnother_Tip").ToString();
                    _Initialize();

                }
                else
                {
                    //litMode.Text = "Create Patient Tip Of The Day Item";
                    //lnkSave.Text = "<strong><em>Create</em></strong>";
                    //litText.Text = "Use the form below to create a tip.";

                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_PatientTip").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkSaveBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litCreateGuidTip_Text").ToString();
                }
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/TipOfTheDay/Patient/TipList.aspx");
        }
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
                return;

            List<string> listRuleIds = _GetRuleIds();

            if (!String.IsNullOrEmpty(Request["TID"]) && String.IsNullOrEmpty(Request["IsCreated"]))
            {
                AHAHelpContent.ThingTips.UpdateThingTip(txtContent.Text.Trim(), listRuleIds, Convert.ToInt32(Request["TID"]), Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID,Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("/Pages/TipOfTheDay/Patient/TipList.aspx");
            }
            else if (!String.IsNullOrEmpty(Request["TID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
            {
                AHAHelpContent.ThingTips.UpdateThingTip(txtContent.Text.Trim(), listRuleIds, Convert.ToInt32(Request["TID"]), Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("/Pages/TipOfTheDay/Patient/TipList.aspx");

            }
            else
            {
              AHAHelpContent.ThingTips objThingTips=  AHAHelpContent.ThingTips.CreateThingTip(txtContent.Text.Trim(), listRuleIds, Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
              Response.Redirect("/Pages/TipOfTheDay/Patient/CreateTips.aspx?TID=" + objThingTips.ThingTipsID.ToString() + "&IsCreated=1");

            }
            //Response.Redirect("/Pages/TipOfTheDay/Patient/TipList.aspx");
        }

        protected void lnkPreview_OnClick(object sender, EventArgs e)
        {
            Utilities.SessionInfo.ResetPatientTipofthedayToSession();
            Utilities.SessionInfo.SetPatientTipofthedayToSession(txtContent.Text.Trim());
            Response.Redirect("~/Pages/TipOfTheDay/Patient/PreviewTip.aspx");
        }
        private void _Initialize()
        {
            AHAHelpContent.ThingTips objThingTip = AHAHelpContent.ThingTips.FindByThingTipID(Convert.ToInt32(Request["TID"]),Utilities.Utility.GetCurrentLanguageID());
            if (objThingTip != null)
            {
                txtContent.Text = objThingTip.Content;

                foreach (ListItem item in chkRuleList.Items)
                {
                    foreach (string ruleid in objThingTip.ThingTipTypes.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (item.Value == ruleid)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateGuidTip_Text").ToString();

            }
        }

        private void _LoadContentRules()
        {
            List<AHAHelpContent.ThingTipsType> lstThingTipsType = AHAHelpContent.ThingTipsType.GetAllThingTipsTypes(Utilities.Utility.GetCurrentLanguageID());
            if (lstThingTipsType.Count > 0)
            {
                chkRuleList.DataSource = lstThingTipsType;
                chkRuleList.DataTextField = "DisplayName";
                chkRuleList.DataValueField = "ThingTipsTypeID";
                chkRuleList.DataBind();
            }
        }

        private List<string> _GetRuleIds()
        {
            List<string> listRuleIds = new List<string>();
            foreach (ListItem item in chkRuleList.Items)
            {
                if (item.Selected)
                    listRuleIds.Add(item.Value);
            }

            return listRuleIds;
        }

        #region Validators
        protected void OnValidateContent(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtContent.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter Content";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Content").ToString(); ;
                return;
            }

            return;
        }

        protected void OnValidateTag(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;
            int iTagCount = _GetRuleIds().Count;
            if (iTagCount == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Select at least any one of the tags.";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Select_at_least_any_one_of_the_tags").ToString(); ;
                return;
            }

            return;
        }

        #endregion
    }
}