﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Heart360Admin.Utilities;

namespace Heart360Admin.UserCtrls.TipOfTheDay.Patient
{
    public partial class TipList : System.Web.UI.UserControl
    {
        private string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] != null)
                    return (string)ViewState["SortExpression"];
                else
                    return "groupname";
            }
            set
            {
                if (ViewState["SortExpression"] == null)
                {
                    ViewState.Add("SortExpression", value);
                }
                else
                {
                    ViewState["SortExpression"] = value;
                }
            }
        }

        private bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] != null)
                    return (bool)ViewState["SortAscending"];
                else
                    return true;
            }
            set
            {
                if (ViewState["SortAscending"] == null)
                {
                    ViewState.Add("SortAscending", value);
                }
                else
                {
                    ViewState["SortAscending"] = value;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _bindDataToGridView();
            }
        }

        protected void gvTips_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            AHAHelpContent.ThingTips objThingTips = e.Row.DataItem as AHAHelpContent.ThingTips;

            Literal litTip = e.Row.FindControl("litTip") as Literal;
            Literal litTags = e.Row.FindControl("litTags") as Literal;

            HtmlAnchor aDelete = e.Row.FindControl("aDelete") as HtmlAnchor;
            HtmlAnchor aEdit = e.Row.FindControl("aEdit") as HtmlAnchor;
            HtmlAnchor aPreview = e.Row.FindControl("aPreview") as HtmlAnchor;
            HtmlAnchor aAudit = e.Row.FindControl("aAudit") as HtmlAnchor;

            litTip.Text = Utilities.Utility.RenderLink(objThingTips.Content);
            litTags.Text = objThingTips.ThingTipTypes.Replace(",", "<br />");//Todo

            aDelete.Attributes.Add("TID", objThingTips.ThingTipsID.ToString());
            //aDelete.Attributes.Add("onclick", "return OnConfirmDelete('Do you really want to delete this item?');");
            string confirmMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "delete_confirm").ToString();
            aDelete.Attributes.Add("onclick", "return OnConfirmDelete('" + confirmMessage + "');");
            aEdit.HRef = String.Format("/Pages/TipOfTheDay/Patient/CreateTips.aspx?TID={0}", objThingTips.ThingTipsID.ToString());
            aPreview.HRef = "javascript:;";
            aPreview.Attributes.Add("onclick", "return PreviewTip(" + objThingTips.ThingTipsID.ToString() + ")");
            aAudit.HRef = "javascript:;";
            aAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfo.aspx?Type=" + AHAHelpContent.AuditTrailType.ThingTips.ToString() + "&id=" + objThingTips.ThingTipsID.ToString() + "');");
        }

        protected void gvTips_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTips.PageIndex = e.NewPageIndex;
            _bindDataToGridView();
        }

        protected void gvTips_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (SortExpression != e.SortExpression)
            {
                SortExpression = e.SortExpression;
                SortAscending = false;
            }
            else
            {
                if (SortAscending)
                {
                    SortAscending = false;
                }
                else
                {
                    SortAscending = true;
                }
            }

            gvTips.PageIndex = 0;
            _bindDataToGridView();
        }

        private void _bindDataToGridView()
        {
            List<AHAHelpContent.ThingTips> listThingTips = AHAHelpContent.ThingTips.GetAllThingTips(Utility.GetCurrentLanguageID());

            gvTips.DataSource = listThingTips;
            gvTips.DataBind();
        }

        protected void aDelete_onserverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDelete = sender as HtmlAnchor;
            AHAHelpContent.ThingTips.DeleteThingTip(Convert.ToInt32(aDelete.Attributes["TID"]),Utilities.Utility.GetCurrentLanguageID());
            _bindDataToGridView();
        }      
    }
}