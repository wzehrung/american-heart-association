﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TipList.ascx.cs" Inherits="Heart360Admin.UserCtrls.TipOfTheDay.Patient.TipList" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Patient_Tip_Of_The_Day")%></h1>
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "list_of_all_tips")%></p>
<table class="admin-box" cellpadding="0" cellspacing="0">
    <tr>
        <td class="align-right menubar">
            <a href="/Pages/TipOfTheDay/Patient/CreateTips.aspx" class="aha-small-button with-arrow">
                <strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></a></div>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="up1">
                <ContentTemplate>
                    <asp:GridView  ShowHeader="true" EnableViewState="true" ID="gvTips"
                        runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, EmtyTipText %>"
                        AllowPaging="True" PageSize="10" ShowFooter="False" OnRowDataBound="gvTips_RowDataBound"
                        AllowSorting="True" OnPageIndexChanging="gvTips_PageIndexChanging" OnSorting="gvTips_Sorting"
                        GridLines="None" CssClass="provider-grid" PagerStyle-CssClass="table-page-index">
                        <Columns>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Tip_Content %>" ItemStyle-Width="560"
                                HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litTip"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Tags %>" ItemStyle-Width="100" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litTags"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Actions %>" ItemStyle-Width="220" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <a runat="server" id="aEdit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Edit")%></a>&nbsp;|&nbsp;<a runat="server" id="aDelete" onserverclick="aDelete_onserverclick"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Delete")%></a>&nbsp;|&nbsp;<a runat="server" id="aPreview"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Preview")%></a>&nbsp;|&nbsp;<a runat="server" id="aAudit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AuditInfo")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" />
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function PreviewTip(id) {
        window.open("/Pages/TipOfTheDay/Patient/PreviewTip.aspx?TID=" + id, "preview_tip", "width=1038,height=700");
        document.forms[0].target = 'preview_tip';
    }
</script>
