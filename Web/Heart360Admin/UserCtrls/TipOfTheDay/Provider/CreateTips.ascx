﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateTips.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.TipOfTheDay.Provider.AddProviderTips" %>
<h1>
    <asp:Literal runat="server" ID="litMode"></asp:Literal></h1>
<p>
    <asp:Literal ID="litText" runat="server"></asp:Literal></p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellpadding="0" cellspacing="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2" class="red align-right">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                    <asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                            ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Tip_Content")%> <b style="color: Red;">*</b>
                                </td>
                                <td valign="top">
                                    <asp:TextBox runat="server" ID="txtContent" Width="400" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidContent"
                                        runat="server" Display="None" OnServerValidate="OnValidateContent">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="guidingtext">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "GuidingText")%>.
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Tags")%><b style="color: Red;">*</b>
                                </td>
                                <td valign="top">
                                    <div class="guidingtext">
                                        <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "TagDetermineTextProvider")%>
                                    </div>
                                    <asp:CheckBoxList runat="server" ID="chkRuleList" RepeatColumns="1" RepeatDirection="Horizontal">
                                    </asp:CheckBoxList>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidTagname"
                                        runat="server" Display="None" OnServerValidate="OnValidateTag">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClientClick="SaveTip()" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkCancel" OnClientClick="CancelTip()" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Cancel</em></strong></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkPreview" OnClientClick="return PreviewTip();" OnClick="lnkPreview_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Preview</em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>

<script type="text/javascript">
    function PreviewTip() {

        var txtContent = TrimWhiteSpaces(document.getElementById('<%=txtContent.ClientID %>').value);

        if (txtContent.length == 0) {
            alert("Please fill the content");
            return false;
        }
        window.open("", "preview_tip", "width=1038,height=700");
        document.forms[0].target = 'preview_tip';
    }

    function SaveTip() {
        document.forms[0].target = '_self';
    }
    function CancelTip() {
        document.forms[0].target = '_self';
    }
</script>

