﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientEduForm.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.PatientEduForm" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<h1>
    <asp:Literal runat="server" ID="litMode"></asp:Literal></h1>
<p>
    <asp:Literal ID="litText" runat="server"></asp:Literal></p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellpadding="0" cellspacing="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2" class="red align-right">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                    <asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                            ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Title")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTitle" Width="200"></asp:TextBox><i><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Maximum250Character")%></i>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidTitle"
                                        runat="server" Display="None" OnServerValidate="OnValidateTitle">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Did_you_link_url")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtURL" Width="200"></asp:TextBox><i><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Maximum500Character")%></i>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidURL"
                                        runat="server" Display="None" OnServerValidate="OnValidateURL">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "didyouknowtitle")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtURLTitle" Width="200"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidURLTitle"
                                        runat="server" Display="None" OnServerValidate="OnValidateURLTitle">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "didyouknowtext")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtURLText" Width="400" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidURLText"
                                        runat="server" Display="None" OnServerValidate="OnValidateURLText">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                               <td class="guidingtext">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "GuidingText")%>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Tags")%><b style="color: Red;">*</b>
                                </td>
                                <td valign="top"><div class="guidingtext"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "tagdeterminelink")%></div>
                                    <asp:CheckBoxList runat="server" ID="chkRuleList" RepeatColumns="1" RepeatDirection="Horizontal">
                                    </asp:CheckBoxList>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidTagname"
                                        runat="server" Display="None" OnServerValidate="OnValidateTag">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Expiration_Date")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <GRC:GRCDatePicker ID="dtDate" runat="server" Width="100" 
                                        CBReadOnly="true"></GRC:GRCDatePicker>
                                    <asp:CustomValidator ID="custExpirationDate" EnableClientScript="false" ValidationGroup="Create"
                                        Display="None" OnServerValidate="custExpirationDate_ServerValidate" runat="server"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClientClick="SaveResource()" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkCancel" OnClientClick="CancelResource()" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Cancel")%></em></strong></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkPreview" OnClientClick="return PreviewResource();" OnClick="lnkPreview_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Preview")%></em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>
<script type="text/javascript">
    function PreviewResource() {

        var txtURL = TrimWhiteSpaces(document.getElementById('<%=txtURL.ClientID %>').value);
        var txtURLTitle = TrimWhiteSpaces(document.getElementById('<%=txtURLTitle.ClientID %>').value);
        if (txtURL.length == 0 || txtURLTitle.length == 0) {
            alert('<%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Please_fill_the_linkurldidyouknowtext")%>');
            
            return false;
        }
        window.open("", "preview_resource", "width=1038,height=700");
        document.forms[0].target = 'preview_resource';
    }

    function SaveResource() {
        document.forms[0].target = '_self';
    }
    function CancelResource() {
        document.forms[0].target = '_self';
    }
</script>