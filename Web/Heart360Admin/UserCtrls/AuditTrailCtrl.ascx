﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuditTrailCtrl.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.AuditTrailCtrl" %>
    <h1 class="red"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Audit_Trail")%></h1>
<asp:GridView ShowHeader="true" EnableViewState="true" ID="gvAuditTrail" runat="server"
    AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, EmtyAuditInfoText %>"
    AllowPaging="True" PageSize="25" ShowFooter="False" OnRowDataBound="gvAuditTrail_RowDataBound"
    OnPageIndexChanging="gvAuditTrail_PageIndexChanging" GridLines="Horizontal" CssClass="provider-grid"
    PagerStyle-CssClass="table-page-index">
    <Columns>
        <asp:TemplateField Visible="True" HeaderText="Modified By" HeaderStyle-CssClass="alignleft">
            <ItemTemplate>
                <asp:Literal ID="litName" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="True" HeaderText="Date Modified" HeaderStyle-CssClass="alignleft">
            <ItemTemplate>
                <asp:Literal ID="litAuditDate" runat="server"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
       
    </Columns>
    <PagerStyle HorizontalAlign="Right" />
</asp:GridView>
