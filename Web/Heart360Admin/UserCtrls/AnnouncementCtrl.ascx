﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnnouncementCtrl.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.AnnouncementCtrl" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Announcement")%></h1>
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "UseThisForm_Announcement")%></p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellpadding="0" cellspacing="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                            <%--<tr>
                                <td colspan="2" class="red align-right">
                                    * Mandatory
                                        <asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                </td>
                            </tr>--%>
                            <%-- <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="Please correct the following" runat="server" ID="validSummary"
                                            ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>--%>
                            <tr>
                                <td colspan="2">
                                    <asp:Literal ID="litSuccess" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Title")%>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTitle" Width="200"></asp:TextBox><i><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Maximum80Character")%></i>
                                    <%-- <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidTitle"
                                        runat="server" Display="None" OnServerValidate="OnValidateTitle">
                                    </asp:CustomValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Text")%>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtText" Width="400" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                    <%--<asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidText"
                                        runat="server" Display="None" OnServerValidate="OnValidateText">
                                    </asp:CustomValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="guidingtext">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "To_add_a_link_in_the_text_above")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" OnClientClick="SaveContent()" ID="lnkSave" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Save")%></em></strong></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkPreview" OnClientClick="return PreviewContent();" OnClick="lnkPreview_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Preview")%></em></strong></asp:LinkButton>&nbsp;<asp:LinkButton
                                        runat="server" ID="lnkAudit" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AuditInfo")%></em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>
<asp:HiddenField ID="hddnContentID" runat="server" />

<script type="text/javascript">
    function PreviewContent() {
        window.open("", "preview_announcement", "width=1038,height=700");
        document.forms[0].target = 'preview_announcement';
    }

    function SaveContent() {
        document.forms[0].target = '_self';
    }
</script>

