﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AHAHelpContent;
using System.Text.RegularExpressions;

namespace Heart360Admin.UserCtrls.GrantSupport.Provider
{
    public partial class GrantSupportList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGridView();     
        }

        private void BindGridView()
        {
            gvUserList.DataSource = AHAHelpContent.GrantSupport.GetAllProviderGrantSupport(Utilities.Utility.GetCurrentLanguageID());
            gvUserList.DataBind();
        }

        protected void gvUserList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUserList.PageIndex = e.NewPageIndex;
            BindGridView();
        }

        protected void gvUserList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            if (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
            {
                AHAHelpContent.GrantSupport objGrantSupport = e.Row.DataItem as AHAHelpContent.GrantSupport;
                Literal litTitle = e.Row.FindControl("litTitle") as Literal;
                Literal litDescription = e.Row.FindControl("litDescription") as Literal;
                HtmlAnchor aEdit = e.Row.FindControl("aEdit") as HtmlAnchor;
                HtmlAnchor aDelete = e.Row.FindControl("aDelete") as HtmlAnchor;
                HtmlAnchor aAudit = e.Row.FindControl("aAudit") as HtmlAnchor;
                Image imgLogo = e.Row.FindControl("imgLogo") as Image;

                litTitle.Text = objGrantSupport.Title;
                litDescription.Text = Utilities.Utility.RenderLink(objGrantSupport.Description);
                imgLogo.ImageUrl = "/Pages/GrantSupport/Provider/GrantLogo.aspx?GID=" + objGrantSupport.GrantSupportID;
                aEdit.HRef = String.Format("/Pages/GrantSupport/Provider/CreateGrantSupport.aspx?GID={0}", objGrantSupport.GrantSupportID.ToString());
                //aDelete.Attributes.Add("onclick", "return OnConfirmDelete('Do you really want to delete this item?');");
                string confirmMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "delete_confirm").ToString();
                aDelete.Attributes.Add("onclick", "return OnConfirmDelete('" + confirmMessage + "');");
                aDelete.Attributes.Add("GID", objGrantSupport.GrantSupportID.ToString());
                aAudit.HRef = "javascript:;";
                aAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfo.aspx?Type=" + AHAHelpContent.AuditTrailType.ProviderGrantSupport.ToString() + "&id=" + objGrantSupport.GrantSupportID.ToString() + "');");
            }
        }
        
        protected void aDelete_serverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDelete = sender as HtmlAnchor;
            if (aDelete.Attributes["GID"] != null)
            {
                AHAHelpContent.GrantSupport.DeleteGrantSupport(Convert.ToInt32(aDelete.Attributes["GID"]),Utilities.Utility.GetCurrentLanguageID());
            }
            BindGridView();
        }

      
    }
}