﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GrantSupportList.ascx.cs" Inherits="Heart360Admin.UserCtrls.GrantSupport.Provider.GrantSupportList" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Grant_Support_List")%></h1>
    <p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "list_of_all_grant_sponsors_provider")%></p>
<table class="admin-box" cellpadding="0" cellspacing="0">
    <tr>
        <td class="align-right menubar">    <a href="/Pages/GrantSupport/Provider/CreateGrantSupport.aspx" class="aha-small-button with-arrow">
        <strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></a>
 </td>
    </tr>
    <tr>
        <td>
<asp:UpdatePanel runat="server" ID="up1">
    <ContentTemplate>
        <asp:GridView  ShowHeader="true" EnableViewState="false" ID="gvUserList"
            runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, EmtyGrantSupportText %>"
            AllowPaging="True" PageSize="25" ShowFooter="False" OnRowDataBound="gvUserList_RowDataBound"
            OnPageIndexChanging="gvUserList_PageIndexChanging" GridLines="Horizontal" CssClass="provider-grid"
            PagerStyle-CssClass="table-page-index">
            <Columns>
                <asp:TemplateField HeaderText="<%$ Resources:Heart360AdminGlobalResources, Title %>" HeaderStyle-CssClass="alignleft">
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="litTitle"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Heart360AdminGlobalResources, Logo %>" ItemStyle-Width="200" ItemStyle-Height="80"
                    HeaderStyle-CssClass="alignleft">
                    <ItemTemplate>
                        <asp:Image ID="imgLogo" runat="server" />
                        <%--<a id="imgLogo" runat="server"></a>--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Heart360AdminGlobalResources, Description %>" ItemStyle-Width="760" ItemStyle-Height="80"
                    HeaderStyle-CssClass="alignleft">
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="litDescription"></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Actions %>" ItemStyle-Width="200" HeaderStyle-CssClass="alignleft">
                    <ItemTemplate>
                        <%--<a runat="server" id="aEdit" onserverclick="aEdit_serverclick">Edit</a>--%>
                        <a runat="server" id="aEdit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Edit")%></a>&nbsp;|&nbsp;<a runat="server" onserverclick="aDelete_serverclick"
                            id="aDelete"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Delete")%></a>&nbsp;|&nbsp;<a runat="server" id="aAudit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AuditInfo")%></a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
 </td>
    </tr>
</table>