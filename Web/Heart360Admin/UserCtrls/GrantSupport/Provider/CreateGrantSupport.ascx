﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateGrantSupport.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.GrantSupport.Provider.CreateGrantSupport" %>
<h1>
    <asp:Literal runat="server" ID="litMode"></asp:Literal></h1>
<p>
    <asp:Literal ID="litText" runat="server"></asp:Literal></p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellpadding="0" cellspacing="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2" class="red align-right">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                        <asp:Literal runat="server" ID="litError"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                            ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>
                            <%-- <tr>
        <td colspan="2">
            <asp:Literal runat="server" ID="litMode1"></asp:Literal>
            for provider
        </td>
    </tr>--%>
                            <tr>
                                <td  class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Title")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTitle" runat="server" Width="200"></asp:TextBox><i><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Maximum128Character")%></i>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidTitle"
                                        runat="server" Display="None" OnServerValidate="OnValidateTitle">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"  class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Description")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDescription" Width="400" TextMode="MultiLine"
                                        Rows="5"></asp:TextBox>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidDescription"
                                        runat="server" Display="None" OnServerValidate="OnValidateDescription">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="guidingtext">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "GuidingText")%>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"  class="align-right nowrap">
                                    <asp:Literal ID="litUploadLogo" runat="server"></asp:Literal>
                                </td>
                                <td>
                                    <asp:FileUpload ID="fileUpload" onchange="javascript:showButton();" runat="server" />
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custFileUpload"
                                        runat="server" Display="None" OnServerValidate="OnValidateFileUpload">
                                    </asp:CustomValidator>
                                    <asp:Button ID="bttnPreview" Text="<%$ Resources:Heart360AdminGlobalResources, Preview %>" OnClientClick="previewImage()" OnClick="bttnPreview_Click"
                                        runat="server" />
                                    <br />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="guidingtext">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "UploadOnly")%>
                                </td>
                            </tr>
                            <tr id="trCurrentLogo" runat="server">
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Current_Logo")%>&nbsp;
                                </td>
                                <td>
                                    <asp:Image ID="imgLogo" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Save</em></strong></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" ID="lnkCancel" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Cancel")%></em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>
<asp:HiddenField ID="hddnContentType" runat="server" />

<script type="text/javascript" language="javascript">
    var bttnPreview = document.getElementById('<%=bttnPreview.ClientID %>');
    bttnPreview.disabled = true;
    function showButton() {
        var fileUpload = document.getElementById('<%=fileUpload.ClientID %>');
        if (fileUpload.value == "") {
            bttnPreview.disabled = true;
            return;
        }
        else {
            var ext1 = fileUpload.value.substring(fileUpload.value.length - 4, fileUpload.value.length).toLowerCase();
            var ext = fileUpload.value.substring(fileUpload.value.length - 3, fileUpload.value.length).toLowerCase();
            if ((ext1 != 'jpeg') && (ext != 'jpg') && (ext != 'gif') && (ext != 'png') && (ext != 'bmp')) {
                bttnPreview.disabled = true;
                return;
            }
        }
        bttnPreview.disabled = false;
        return;
    }

    function previewImage() {
        window.open("", "preview_providergrantlogo", "width=500,height=500");
        document.forms[0].target = 'preview_providergrantlogo';
        return true;
    }

    function upLoadLogo(id) {
        document.forms[0].target = '_self';
        document.forms[0].action = "/Pages/GrantSupport/Provider/CreateGrantSupport.aspx" + id;
    }   
</script>

