﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;
using System.IO;
using System.Drawing;
using System.Text;
using ImageLib;

namespace Heart360Admin.UserCtrls.GrantSupport.Provider
{
    public partial class CreateGrantSupport : System.Web.UI.UserControl
    {
        private void _Initialize()
        {
            AHAHelpContent.GrantSupport objGrantSupport = AHAHelpContent.GrantSupport.GetGrantSupportByID(Convert.ToInt32(Request["GID"]),Utilities.Utility.GetCurrentLanguageID());
            if (objGrantSupport != null)
            {
                txtDescription.Text = objGrantSupport.Description;
                txtTitle.Text = objGrantSupport.Title;
                imgLogo.ImageUrl = "/Pages/GrantSupport/Provider/GrantLogo.aspx?GID=" + Request["GID"];
                hddnContentType.Value = objGrantSupport.ContentType;
                trCurrentLogo.Visible = true;
            }
            else
            {
                litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateGrantSupport_Text").ToString();
                trCurrentLogo.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request["GID"]) && String.IsNullOrEmpty(Request["IsCreated"]))
                {
                    _Initialize();
                    //litMode.Text = litMode1.Text = "Update provider grant support";

                    //litMode.Text = "Update Provider Grant Support Item";
                    //lnkSave.Text = "<strong><em>Update</em></strong>";

                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_ProviderGrantSupport").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();

                    //trCurrentLogo.Visible = true;

                    //litUploadLogo.Text = "Upload new logo";
                    //litText.Text = "Use the form below to update a grant support item.";

                    litUploadLogo.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_NewLogo").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateGrantSupport_Text").ToString();

                    lnkSave.Attributes.Add("onclick", "upLoadLogo('?GID=" + Request["GID"] + "');");
                    lnkCancel.Attributes.Add("onclick", "upLoadLogo('?GID=" + Request["GID"] + "');");
                }
                else if (!String.IsNullOrEmpty(Request["GID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
                {
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_ProviderGrantSupport").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litUploadLogo.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_NewLogo").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateGrantSupport_Text_ForAnother").ToString();

                    lnkSave.Attributes.Add("onclick", "upLoadLogo('?GID=" + Request["GID"] + "');");
                    lnkCancel.Attributes.Add("onclick", "upLoadLogo('?GID=" + Request["GID"] + "');");
                    _Initialize();

                }
                else
                {
                    //litMode.Text = litMode1.Text = "Create provider grant support";
                    //litMode.Text = "Create Provider Grant Support Item";
                    //lnkSave.Text = "<strong><em>Create</em></strong>";
                    //litText.Text = "Use the form below to create a grant support item.";

                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_ProviderGrantSupport").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkSaveBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litCreateGrantSupport_Text").ToString();

                    trCurrentLogo.Visible = false;
                    litUploadLogo.Text = "Upload logo <b style=\"color: Red;\">*</b>";
                    lnkSave.Attributes.Add("onclick", "upLoadLogo('');");
                    lnkCancel.Attributes.Add("onclick", "upLoadLogo('');");
                }
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");
            if (!Page.IsValid)
                return;

            if (!String.IsNullOrEmpty(Request["GID"]) && String.IsNullOrEmpty(Request["IsCreated"]))
            {
                byte[] byteLogoArray = null;
                string strContentType = hddnContentType.Value;
                if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0)
                {
                    //byteLogoArray = fileUpload.FileBytes;
                    byteLogoArray = _GetScaledImage(fileUpload.PostedFile.InputStream);
                    strContentType = fileUpload.PostedFile.ContentType;
                }

                AHAHelpContent.GrantSupport.UpdateGrantSupport(Convert.ToInt32(Request["GID"]),
                                                               txtTitle.Text.Trim(),
                                                               txtDescription.Text.Trim(),
                                                               byteLogoArray,
                                                                strContentType,
                                                               true, Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("/Pages/GrantSupport/Provider/GrantSupportList.aspx");

                
            }
            else if (!String.IsNullOrEmpty(Request["GID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
            {
                byte[] byteLogoArray = null;
                string strContentType = hddnContentType.Value;
                if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0)
                {
                    //byteLogoArray = fileUpload.FileBytes;
                    byteLogoArray = _GetScaledImage(fileUpload.PostedFile.InputStream);
                    strContentType = fileUpload.PostedFile.ContentType;
                }

                AHAHelpContent.GrantSupport.UpdateGrantSupport(Convert.ToInt32(Request["GID"]),
                                                               txtTitle.Text.Trim(),
                                                               txtDescription.Text.Trim(),
                                                               byteLogoArray,
                                                                strContentType,
                                                               true, Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("/Pages/GrantSupport/Provider/GrantSupportList.aspx");


            }
            else
            {
               AHAHelpContent.GrantSupport objGrantSupport= AHAHelpContent.GrantSupport.CreateGrantSupport(txtTitle.Text.Trim(),
                                                               txtDescription.Text.Trim(),
                                                               fileUpload.PostedFile.InputStream != null ? _GetScaledImage(fileUpload.PostedFile.InputStream) : null,
                                                               fileUpload.PostedFile.ContentType,
                                                               true, Utilities.SessionInfo.GetUserContext().UserID, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
               Response.Redirect("/Pages/GrantSupport/Provider/CreateGrantSupport.aspx?GID=" + objGrantSupport.GrantSupportID.ToString() + "&IsCreated=1");


            }
            
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/GrantSupport/Provider/GrantSupportList.aspx");
        }

        protected void bttnPreview_Click(object sender, EventArgs e)
        {
            if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0)
            {
                Utilities.Utility.GrantLogo objGrantLogo = new Utilities.Utility.GrantLogo();
                //objGrantLogo.Content = fileUpload.FileBytes;
                objGrantLogo.ContentType = fileUpload.PostedFile.ContentType;
                objGrantLogo.Content = _GetScaledImage(fileUpload.PostedFile.InputStream);
                //Session["providergrantpreview"] = null;
                //Session["providergrantpreview"] = objGrantLogo;
                Utilities.SessionInfo.ResetProviderGrantPreviewSession();
                Utilities.SessionInfo.SetProviderGrantPreviewToSession(objGrantLogo);
                Response.Redirect("/Pages/GrantSupport/Provider/GrantSupportLogoPreview.aspx");
            }
        }
        private byte[] _GetScaledImage(Stream imageStream)
        {
            System.Drawing.Image image = System.Drawing.Image.FromStream(imageStream);
            System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
            //Should scale the image if it's height is greater than 70 or width is greater than 160
            byte[] buffer = (byte[])converter.ConvertTo(image, typeof(byte[]));
            if (image.PhysicalDimension.Height > 70 || image.PhysicalDimension.Width > 160)
            {
                buffer = (byte[])converter.ConvertTo(ImageLib.ImageHelper.Resample(image, "", 160, 70), typeof(byte[]));
            }
            return buffer;
        }


        protected void OnValidateTitle(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(txtTitle.Text.Trim()))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Enter a title";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Title").ToString(); ;
                return;
            }
            return;
        }

        protected void OnValidateDescription(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (string.IsNullOrEmpty(txtDescription.Text.Trim()))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Enter a description";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Description").ToString(); ;
                return;
            }
            return;
        }


        protected void OnValidateFileUpload(object sender, ServerValidateEventArgs v)
        {
            //CustomValidator custValidator = sender as CustomValidator;
            //string[] strImageExtArray = { ".jpeg", ".jpg", ".gif", ".png", ".bmp" };
            //if (string.IsNullOrEmpty(Request["GID"]) && (fileUpload.PostedFile == null || fileUpload.PostedFile.ContentLength == 0))
            //{
            //    v.IsValid = false;
            //    //custValidator.ErrorMessage = "Upload a logo";
            //    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_a_Logo").ToString(); ;
            //    return;
            //}
            //else if ((fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0) &&
            //    (!strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUpload.PostedFile.FileName).ToLower()))))
            //{
            //    v.IsValid = false;
            //    //custValidator.ErrorMessage = "You can only upload only '.jpeg','.jpg','.gif','.png','.bmp' files as logo";
            //    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "You_CanOnly_Upload").ToString(); 
            //    return;
            //}
            //else if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0 && (fileUpload.PostedFile.ContentLength > 102400))
            //{
            //    v.IsValid = false;
            //    //custValidator.ErrorMessage = "Logo size should not be greater than 100 KB";
            //    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Logo_Size").ToString(); 
            //    return;
            //}
            CustomValidator custValidator = sender as CustomValidator;
            string[] strImageExtArray = { ".jpeg", ".jpg", ".gif", ".png", ".bmp" };
            //--------------added by ragesh 
            if (!string.IsNullOrEmpty(Request["GID"]) && trCurrentLogo.Visible == false && (fileUpload.PostedFile == null || fileUpload.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Upload a logo";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_a_Logo").ToString(); ;
                return;

            }
            //    ---------------
            else if (string.IsNullOrEmpty(Request["GID"]) && (fileUpload.PostedFile == null || fileUpload.PostedFile.ContentLength == 0))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Upload a logo";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Upload_a_Logo").ToString(); ;
                return;
            }
            else if ((fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0) &&
                (!strImageExtArray.Any(x => x.ToLower().Equals(Path.GetExtension(fileUpload.PostedFile.FileName).ToLower()))))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "You can only upload only '.jpeg','.jpg','.gif','.png','.bmp' files as logo";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "You_CanOnly_Upload").ToString();
                return;
            }
            else if (fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0 && (fileUpload.PostedFile.ContentLength > 102400))
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Logo size should not be greater than 100 KB";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Logo_Size").ToString();
                return;
            }
            
            return;
        }
    }
}