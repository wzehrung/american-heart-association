﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHAHelpContent;

namespace Heart360Admin.UserCtrls
{
    public partial class LoginCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bttnHidSubmit.Style.Add(HtmlTextWriterStyle.Display, "none");
            if (!IsPostBack)
            {
                //if (Utilities.Utility.IsAdminLoggedin())
                //{
                //    Response.Redirect("~/Pages/Home.aspx", true);
                //}
                if (Utilities.SessionInfo.IsAdminLoggedin())
                {
                    Response.Redirect("~/Pages/Home.aspx", true);
                }
                txtUserName.Focus();
            }
        }
        protected void lnkLogin_OnClick(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid)
            {
                AdminUser objAdminUser = AdminUser.GetAdminUserByUserName(txtUserName.Text.Trim());
                if (objAdminUser != null)
                {
                    //Utilities.Utility.SetCurrentLoggedinUser(objAdminUser.AdminUserID, objAdminUser.UserName,  objAdminUser.Name, objAdminUser.IsSuperAdmin);
                    Utilities.SessionInfo.SetCurrentLoggedinUser(objAdminUser.AdminUserID, objAdminUser.UserName, objAdminUser.Name, objAdminUser.RoleID, objAdminUser.RoleName, objAdminUser.DefaultLanguageID, objAdminUser.LanguageLocale, objAdminUser.BusinessID );
                    AdminUserLoginDetails.CreateAdminUserLoginDetails(objAdminUser.AdminUserID);
                }
                if (!string.IsNullOrEmpty(Request["ReturnURL"]))
                {
                    Response.Redirect(Server.UrlDecode(Request["ReturnURL"]));
                }
                else
                {
                    Response.Redirect("~/Pages/Home.aspx");
                }
            }
        }

        private bool AuthenticateUser()
        {
            bool bIsSuccess = false;
            string username = GRCBase.ConfigReader.GetValue("UserName");
            string password = GRCBase.ConfigReader.GetValue("Password");
            if (txtUserName.Text.Trim() == username && txtPassword.Text.Trim() == password)
                bIsSuccess = true;
            return bIsSuccess;
        }

        protected void OnValidateUserName(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtUserName.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                custValidator.ErrorMessage = "Enter username";
                return;
            }

            return;
        }

        protected void OnValidatePassword(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtPassword.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                custValidator.ErrorMessage = "Enter password";
                return;
            }

            return;
        }

        protected void OnValidateCredentails(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtUserName.Text.Trim().Length > 0 && txtPassword.Text.Length > 0)
            {
                if (!AdminUser.IsValidAdminUser(txtUserName.Text.Trim(), Utilities.Utility.GetHashString(txtPassword.Text)))
                {
                    e.IsValid = false;
                    custValidator.ErrorMessage = "Enter a valid Username and password";
                    return;
                }
            }

            return;
        }
    }
}