﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls.Business
{
    public partial class BusinessForm : System.Web.UI.UserControl
    {
        private bool IsEditMode
        {
            get
            {
                return (Request["BID"] != null && Request["BID"].Length > 0) ? true : false;
            }
        }

        private string BusinessID
        {
            get
            {
                return Request["BID"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (IsEditMode)
                {
                    _LoadBusiness();
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_Business").ToString();
                    lnkCreate.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateBusiness_Text").ToString();
                }
                else
                {
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Business").ToString();
                    lnkCreate.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkSaveBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litCreateBusiness_Text").ToString();
                }
            }
        }


        protected void lnkCreate_OnClick(object sender, EventArgs e)
        {
            if (IsEditMode)
            {
                Page.Validate("Update");
            }
            else
            {
                Page.Validate("Create");
            }
            if (!Page.IsValid)
                return;

            if (IsEditMode)
            {
                UpdateBusiness();
            }
            else
            {
                CreateBusiness();
            }
            Response.Redirect("~/Pages/Businesses/AllBusinesses.aspx", true);
        }

        private void _LoadBusiness()
        {
            AHAHelpContent.Business objBusiness = AHAHelpContent.Business.FindBusinessByBusinessID(Convert.ToInt32(BusinessID) );
            if (objBusiness != null)
            {
                txtName.Text = objBusiness.Name;
                fckEditorDescription.Value = objBusiness.Comments;
            }
        }

        private void CreateBusiness()
        {

            AHAHelpContent.Business.CreateBusiness(txtName.Text.Trim(), fckEditorDescription.Value.Trim() );
        }

        private void UpdateBusiness()
        {
            
            AHAHelpContent.Business.UpdateBusiness(Convert.ToInt32(BusinessID), txtName.Text.Trim(), fckEditorDescription.Value.Trim() );
        }

        protected void lnkCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Businesses/AllBusinesses.aspx", true);
        }



        #region Validators
        protected void OnValidateName(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (txtName.Text.Trim().Length == 0)
            {
                v.IsValid = false;
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Name").ToString();
            }
        }

        #endregion

    }
}