﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActiveBusinesses.ascx.cs" Inherits="Heart360Admin.UserCtrls.Business.ActiveBusinesses" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<asp:Label ID="lblValidationMessage" Visible="false" ForeColor="Red" runat="server"></asp:Label>
<asp:GridView ShowHeader="true" EnableViewState="true" ID="gvBusinesses" runat="server"
    AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, ThereAreNoBusinesses %>"
    AllowPaging="True" PageSize="10" ShowFooter="False" OnRowDataBound="gvBusinesses_RowDataBound"
    OnPageIndexChanging="gvBusinesses_PageIndexChanging" GridLines="Horizontal"
    CssClass="provider-grid " PagerStyle-CssClass="table-page-index">
    <Columns>
        <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Name %>"
            ItemStyle-Width="250" HeaderStyle-CssClass="alignleft">
            <ItemTemplate>
                <asp:Literal runat="server" ID="litName"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Comments %>"
            ItemStyle-Width="400" HeaderStyle-CssClass="alignleft">
            <ItemTemplate>
                <asp:Literal runat="server" ID="litComments"></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Actions %>"
            ItemStyle-Width="80" HeaderStyle-CssClass="alignleft">
            <ItemTemplate>
                <a runat="server" id="aEdit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Edit")%></a><br />
                <br />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <PagerStyle HorizontalAlign="Right" />
</asp:GridView>

