﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusinessForm.ascx.cs" Inherits="Heart360Admin.UserCtrls.Business.BusinessForm" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<h1><asp:Literal runat="server" ID="litMode"></asp:Literal></h1>
<p><asp:Literal ID="litText" runat="server"></asp:Literal></p>

<br />
<div class="whitebox">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellspacing="0" cellpadding="0" border="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table>
                            <tr>
                                <td style="width: 650px">
                                    <table cellspacing="0" width="100%" class="control-table">
                                        <tr>
                                            <td colspan="2" class="red align-right">
                                                <asp:Literal runat="server" ID="litError"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div class="validation_summary">
                                                    <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>"
                                                        runat="server" ID="validSummarycreate" ValidationGroup="Create" Enabled="true"
                                                        ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" ForeColor="#CE2930" />
                                                    <asp:ValidationSummary HeaderText="Please correct the following" runat="server" ID="validsummaryupdate"
                                                        ValidationGroup="Update" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                                        DisplayMode="BulletList" ForeColor="#CE2930" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2">
                                                <strong>Business Details</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top" class="align-right nowrap">
                                                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Name")%><b style="color: Red;">*</b>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtName" Width="300"></asp:TextBox>
                                                <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="CustomValidator1"
                                                    runat="server" Display="None" OnServerValidate="OnValidateName">
                                                </asp:CustomValidator>
                                                <asp:CustomValidator ValidationGroup="Update" EnableClientScript="false" ID="CustomValidator2"
                                                    runat="server" Display="None" OnServerValidate="OnValidateName">
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top" class="align-right nowrap">
                                                <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Comments")%>
                                            </td>
                                            <td>
                                                <FCKeditorV2:FCKeditor ID="fckEditorDescription" ToolbarCanCollapse="false" Width="330"
                                                    Height="200" runat="server">
                                                </FCKeditorV2:FCKeditor>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" ID="lnkCancel" OnClick="lnkCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Cancel</em></strong></asp:LinkButton>&nbsp;&nbsp;
                                                <asp:LinkButton runat="server" ID="lnkCreate" OnClick="lnkCreate_OnClick" CssClass="aha-small-button with-arrow"><strong><em>Save</em></strong></asp:LinkButton>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                <td valign="top">
                                    <div class="red align-right"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>



