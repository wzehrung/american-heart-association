﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using GRCBase.WebControls_V1;

namespace Heart360Admin.UserCtrls.Business
{
    public partial class ActiveBusinesses : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _bindDataToGridView();
            }
            lblValidationMessage.Visible = false;
        }

        protected void gvBusinesses_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            AHAHelpContent.Business objBusiness = e.Row.DataItem as AHAHelpContent.Business;

            Literal litName = e.Row.FindControl("litName") as Literal;
            Literal litComments = e.Row.FindControl("litComments") as Literal;

            HtmlAnchor aEdit = e.Row.FindControl("aEdit") as HtmlAnchor;

            litName.Text = objBusiness.Name;
            litComments.Text = GRCBase.HtmlScrubber.Clean( objBusiness.Comments, false, true);
            
            aEdit.HRef = String.Format("{0}?BID={1}", "/Pages/Businesses/CreateBusiness.aspx", objBusiness.BusinessID.ToString());
        }

        protected void gvBusinesses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBusinesses.PageIndex = e.NewPageIndex;
            _bindDataToGridView();

        }

        private void _bindDataToGridView()
        {
            List<AHAHelpContent.Business> listBusiness = AHAHelpContent.Business.FindAllBusinesses();
            gvBusinesses.DataSource = listBusiness;
            gvBusinesses.DataBind();
        }

        private string FormatUrlText(string strURL, int length)
        {
            if (string.IsNullOrEmpty(strURL) || strURL.Length <= length)
            {
                return strURL;
            }
            else
            {
                int intLength = strURL.Length;
                for (int i = 1; i <= intLength / length; i++)
                {
                    if (i == 1)
                    {
                        //length is the number of charcters after which to insert '<wbr>&shy;'
                        strURL = strURL.Insert((i * length), "<wbr>");
                    }
                    else
                    {
                        //length is the number of charcters after which to insert '<wbr>&shy;' and 10 is the number of charcters in '<wbr>&shy;'
                        strURL = strURL.Insert((i * length) + (5 * (i - 1)), "<wbr>");
                    }
                }
            }
            return strURL;
        }

    }
}