﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginCtrl.ascx.cs" Inherits="Heart360Admin.UserCtrls.LoginCtrl" %>
<div class="whitebox">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <asp:Panel ID="panel" DefaultButton="bttnHidSubmit" runat="server">
        <table class="tablecontainer" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="left-shadow">
                </td>
                <td class="boxcontent">
                    <h1>
                        Login</h1>
                    <p>
                        Use your username & password to login.</p>
                    <div class="validation_summary">
                        <asp:ValidationSummary HeaderText="Please correct the following errors" ValidationGroup="Login"
                            Enabled="true" ID="vldSummaryAdd" runat="server" DisplayMode="BulletList" ShowSummary="true"
                            ShowMessageBox="true" ForeColor="#CE2930" />
                    </div>
                    <div class="login-ctrl">
                        <div class="ctrl-row">
                            <label class="inline-block">
                                Username</label>&nbsp;<asp:TextBox runat="server" ID="txtUserName" Width="200"></asp:TextBox></div>
                        <div class="ctrl-row">
                            <label class="inline-block">
                                Password</label>&nbsp;<asp:TextBox runat="server" ID="txtPassword" Width="200" TextMode="Password"></asp:TextBox></div>
                        <div class="ctrl-row">
                            <label class="inline-block">
                                &nbsp;</label>
                            <asp:LinkButton runat="server" ID="lnkLogin" OnClick="lnkLogin_OnClick" CssClass="loginbutton inline-block"></asp:LinkButton></div>
                       <asp:Button ID="bttnHidSubmit" OnClick="lnkLogin_OnClick" runat="server" />
                        &nbsp;
                    </div>
                    <asp:CustomValidator ValidationGroup="Login" EnableClientScript="false" ID="custValid"
                        runat="server" Display="None" OnServerValidate="OnValidateUserName"></asp:CustomValidator>
                    <asp:CustomValidator ValidationGroup="Login" EnableClientScript="false" ID="CustomValidator1"
                        runat="server" Display="None" OnServerValidate="OnValidatePassword"></asp:CustomValidator>
                    <asp:CustomValidator ValidationGroup="Login" EnableClientScript="false" ID="CustomValidator2"
                        runat="server" Display="None" OnServerValidate="OnValidateCredentails"></asp:CustomValidator>
                </td>
                <td class="right-shadow">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>
 <script type="text/javascript" language="javascript">
        function foucsLogin() {
            $get('<%=txtUserName.ClientID%>').focus();
        }

        window.onload = foucsLogin;
    </script>
