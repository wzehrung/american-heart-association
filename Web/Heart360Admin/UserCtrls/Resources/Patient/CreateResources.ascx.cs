﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Admin.UserCtrls.Resources.Patient
{
    public partial class CreateResources : System.Web.UI.UserControl
    {
        
        Utilities.UserContext objUserContext
        {
            get
            {
                //return Utilities.Utility.GetUserContext();
                return Utilities.SessionInfo.GetUserContext();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            setDateFormat();
            if (!Page.IsPostBack)
            {
                //if (!String.IsNullOrEmpty(Request["RID"]))
                if (!String.IsNullOrEmpty(Request["RID"]) && String.IsNullOrEmpty(Request["IsCreated"]))
                {
                    _Initialize();
                    //litMode.Text = litMode1.Text = "Update patient resource";
                    //litMode.Text = "Update Patient Resource Item";
                    //lnkSave.Text = "<strong><em>Update</em></strong>";
                    //litText.Text = "Use the below form to update a resource";

                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_Patient").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateGuid_Text").ToString();
                }
                else if (!String.IsNullOrEmpty(Request["RID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
                {
                    
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Update_Patient").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkUpdtBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Patient_ForAnother").ToString();
                    _Initialize();

                }
                else
                {
                    //litMode.Text = litMode1.Text = "Create patient resource";
                    //litMode.Text = "Create Patient Resource Item";
                    //lnkSave.Text = "<strong><em>Create</em></strong>";
                    //litText.Text = "Use the below form to create a resource";
                    litMode.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "Create_Patient").ToString();
                    lnkSave.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "lnkSaveBtn_Text").ToString();
                    litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litCreateGuid_Text").ToString();
                }
            }
        }
        protected void  setDateFormat()
        {
            if (System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower() == "es-mx")
            {
                dtDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.DDMMYYYY;
            }
            else
            {
                dtDate.DateFormat = GRCBase.WebControls_V1.GRCDatePicker.CalendarDateFormat.MMDDYYYY;
            }
        }

        private void _Initialize()
        {
            AHAHelpContent.Resource objResource = AHAHelpContent.Resource.FindByResourceID(Convert.ToInt32(Request["RID"]), Utilities.Utility.GetCurrentLanguageID());
            if (objResource != null)
            {
                txtTitle.Text = objResource.ContentTitle;
                txtLink.Text = objResource.ContentTitleUrl;
                txtText.Text = objResource.ContentText;
                dtDate.Date = objResource.ExpirationDate;
            }
            else
            {
                litText.Text = GetGlobalResourceObject("Heart360AdminGlobalResources", "litUpdateGuid_Text").ToString();

            }
        }
       
        
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            Page.Validate("Create");

            if (!Page.IsValid)
                return;       

            if (!String.IsNullOrEmpty(Request["RID"]) &&  String.IsNullOrEmpty(Request["IsCreated"]))
            {
                AHAHelpContent.Resource.UpdateResource(Convert.ToInt32(Request["RID"]), txtTitle.Text.Trim(), txtLink.Text.Trim(), txtText.Text.Trim(),false, dtDate.Date,objUserContext.UserID, objUserContext.UserID, Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("/Pages/Resources/Patient/ResourceList.aspx");
            }
            else if (!String.IsNullOrEmpty(Request["RID"]) && !String.IsNullOrEmpty(Request["IsCreated"]))
            {
                AHAHelpContent.Resource.UpdateResource(Convert.ToInt32(Request["RID"]), txtTitle.Text.Trim(), txtLink.Text.Trim(), txtText.Text.Trim(), false, dtDate.Date, objUserContext.UserID, objUserContext.UserID, Utilities.Utility.GetCurrentLanguageID());
                Response.Redirect("/Pages/Resources/Patient/ResourceList.aspx");
            }
            else
            {
               AHAHelpContent.Resource objResource= AHAHelpContent.Resource.CreateResource(txtTitle.Text.Trim(), txtLink.Text.Trim(), txtText.Text.Trim(), false, dtDate.Date, objUserContext.UserID, objUserContext.UserID, Utilities.Utility.GetCurrentLanguageID());
               Response.Redirect("/Pages/Resources/Patient/CreateResources.aspx?RID=" + objResource.ResourceID.ToString()+"&IsCreated=1");

            }
            //Response.Redirect("/Pages/Resources/Patient/ResourceList.aspx");
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("/Pages/Resources/Patient/ResourceList.aspx");
        }

        #region Validators
        protected void OnValidateTitle(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtTitle.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a Title";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Title").ToString();
                return;
            }

            return;
        }

        protected void OnValidateLink(object sender, ServerValidateEventArgs e)
        {
            CustomValidator custValidator = sender as CustomValidator;

            if (txtLink.Text.Trim().Length == 0)
            {
                e.IsValid = false;
                //custValidator.ErrorMessage = "Enter a URL";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Url").ToString();
                return;
            }

            return;
        }

        //protected void OnValidateText(object sender, ServerValidateEventArgs e)
        //{
        //    CustomValidator custValidator = sender as CustomValidator;

        //    if (txtText.Text.Trim().Length == 0)
        //    {
        //        e.IsValid = false;
        //        //custValidator.ErrorMessage = "Enter a Text";
        //        custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Enter_Text").ToString();
        //        return;
        //    }

        //    return;
        //}

      

        protected void custExpirationDate_ServerValidate(object sender, ServerValidateEventArgs v)
        {
            CustomValidator custValidator = sender as CustomValidator;
            if (dtDate.Date == null)
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Select a expiration date";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Expiry_Date").ToString();
                return;
            }
            //if (!string.IsNullOrEmpty(dtDate.Text) && !AHACommon.Helper.IsValidDate(dtDate.Date))
            //{
            //    v.IsValid = false;
            //    custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Expiry_Date").ToString();
            //    return;
            //}
            else if (dtDate.Date != null && dtDate.Date.Value <= DateTime.Today)
            {
                v.IsValid = false;
                //custValidator.ErrorMessage = "Select a future date";
                custValidator.ErrorMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "Future_Date").ToString();
                return;
            }
            return;
        }

        

        #endregion
    }
}