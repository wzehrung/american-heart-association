﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Resources;

namespace Heart360Admin.UserCtrls.Resources.Patient
{
    public partial class ResourceList : System.Web.UI.UserControl
    {
        public int Count
        {
            get
            {
                if (Session["Count"] != null)
                {
                    return Convert.ToInt32(Session["Count"].ToString());
                }
                else
                {
                    return 0;
                }

            }
            set
            {
                if (Session["Count"] == null)
                {
                    Session.Add("Count", value);
                }
                else
                {
                    Session["Count"] = value;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Count = 0;
                _bindDataToGridView();
            }
        }

        protected void gvResources_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;
            AHAHelpContent.Resource objResource = e.Row.DataItem as AHAHelpContent.Resource;

            HtmlAnchor aResource = e.Row.FindControl("aResource") as HtmlAnchor;
            Literal litResource = e.Row.FindControl("litResource") as Literal;
            Literal litExpirationDate = e.Row.FindControl("litExpirationDate") as Literal;

            HtmlAnchor aUp = e.Row.FindControl("aUp") as HtmlAnchor;
            HtmlAnchor aDown = e.Row.FindControl("aDown") as HtmlAnchor;
            HtmlAnchor aEdit = e.Row.FindControl("aEdit") as HtmlAnchor;
            HtmlAnchor aDelete = e.Row.FindControl("aDelete") as HtmlAnchor;
            HtmlAnchor aAudit = e.Row.FindControl("aAudit") as HtmlAnchor;

            if (this.Count == 0)
                aUp.Visible = false;

            if (this.Count + 1 == Convert.ToInt32(ViewState["TC"].ToString()))
                aDown.Visible = false;

            litResource.Text = Utilities.Utility.RenderLink(objResource.ContentText);
            litExpirationDate.Text = objResource.ExpirationDate.HasValue ? objResource.ExpirationDate.Value.ToShortDateString() : string.Empty;
            aResource.HRef = objResource.ContentTitleUrl;
            aResource.InnerHtml = objResource.ContentTitle;
            aDelete.Attributes.Add("RID", objResource.ResourceID.ToString());
            //aDelete.Attributes.Add("onclick", "return OnConfirmDelete('Do you really want to delete this item?');");
            string confirmMessage = GetGlobalResourceObject("Heart360AdminGlobalResources", "delete_confirm").ToString();
            aDelete.Attributes.Add("onclick", "return OnConfirmDelete('" + confirmMessage + "');");
            aEdit.HRef = String.Format("/Pages/Resources/Patient/CreateResources.aspx?RID={0}", objResource.ResourceID.ToString());
            aAudit.HRef = "javascript:;";
            aAudit.Attributes.Add("onclick", "return OpenAuditInfoPage('/Pages/Reports/AuditInfo.aspx?Type=" + AHAHelpContent.AuditTrailType.PatientResources.ToString() + "&id=" + objResource.ResourceID.ToString() + "');");

            aUp.Attributes.Add("CurrentID", objResource.ResourceID.ToString());
            aUp.Attributes.Add("CurrentPos", Count.ToString());
            aDown.Attributes.Add("CurrentID", objResource.ResourceID.ToString());
            aDown.Attributes.Add("CurrentPos", Count.ToString());
            Count++;
        }

        protected void gvResources_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvResources.PageIndex = e.NewPageIndex;
            this.Count = e.NewPageIndex * gvResources.PageSize;
            _bindDataToGridView();

        }

        private void _bindDataToGridView()
        {
            _DoBind(false);
        }

        private void _DoBind(bool bIsProvider)
        {
            List<AHAHelpContent.Resource> listResource = AHAHelpContent.Resource.FindAllResources(bIsProvider, Utilities.Utility.GetCurrentLanguageID());

            ViewState["TC"] = listResource.Count.ToString();
            ViewState["RL"] = listResource;
            gvResources.DataSource = listResource;
            gvResources.DataBind();

        }

        protected void aDelete_onserverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDelete = sender as HtmlAnchor;
            AHAHelpContent.Resource.DeleteResource(Convert.ToInt32(aDelete.Attributes["RID"]),Utilities.Utility.GetCurrentLanguageID());
            this.Count = gvResources.PageIndex * gvResources.PageSize;
            _bindDataToGridView();
        }

        protected void aUp_onserverclick(object sender, EventArgs e)
        {
            HtmlAnchor aUp = sender as HtmlAnchor;
            AHAHelpContent.Resource objResource = ((List<AHAHelpContent.Resource>)ViewState["RL"])[Convert.ToInt32(aUp.Attributes["CurrentPos"]) - 1];
            AHAHelpContent.Resource.SwapResources(Convert.ToInt32(aUp.Attributes["CurrentID"]), objResource.ResourceID, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
            this.Count = gvResources.PageIndex * gvResources.PageSize;
            _bindDataToGridView();
        }

        protected void aDown_onserverclick(object sender, EventArgs e)
        {
            HtmlAnchor aDown = sender as HtmlAnchor;
            AHAHelpContent.Resource objResource = ((List<AHAHelpContent.Resource>)ViewState["RL"])[Convert.ToInt32(aDown.Attributes["CurrentPos"]) + 1];
            AHAHelpContent.Resource.SwapResources(Convert.ToInt32(aDown.Attributes["CurrentID"]), objResource.ResourceID, Utilities.SessionInfo.GetUserContext().UserID, Utilities.Utility.GetCurrentLanguageID());
            this.Count = gvResources.PageIndex * gvResources.PageSize;
            _bindDataToGridView();
        }

        protected void ddlResources_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            _bindDataToGridView();
        }



    }
}