﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateResources.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.Resources.Patient.CreateResources" %>
<%@ Register Assembly="GRCBase" Namespace="GRCBase.WebControls_V1" TagPrefix="GRC" %>
<h1>
    <asp:Literal runat="server" ID="litMode"></asp:Literal></h1>
<p>
    <asp:Literal ID="litText" runat="server"></asp:Literal>
</p>
<br />
<div class="whitebox" style="width: 600px;">
    <div class="shadowtop">
        <div class="tl">
        </div>
        <div class="tm">
        </div>
        <div class="tr">
        </div>
    </div>
    <div>
        <table cellpadding="0" cellspacing="0" class="tablecontainer">
            <tbody>
                <tr>
                    <td class="left-shadow">
                    </td>
                    <td class="boxcontent">
                        <table cellspacing="0" width="100%" class="control-table">
                            <tr>
                                <td colspan="2" class="red align-right">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Mandatory")%>
                                        <asp:Literal runat="server" ID="litError"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="validation_summary">
                                        <asp:ValidationSummary HeaderText="<%$ Resources:Heart360AdminGlobalResources, please_correcrt_following %>" runat="server" ID="validSummary"
                                            ValidationGroup="Create" Enabled="true" ShowSummary="true" ShowMessageBox="true"
                                            DisplayMode="BulletList" ForeColor="#CE2930" />
                                    </div>
                                </td>
                            </tr>
                            <%-- <tr>
        <td>
            <asp:Literal runat="server" ID="litMode1"></asp:Literal>
            for patient
        </td>
    </tr>--%>
                            <tr>
                                <td class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Title")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTitle" Width="200"></asp:TextBox><i><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Maximum80Character")%></i>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidTitle"
                                        runat="server" Display="None" OnServerValidate="OnValidateTitle">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Link")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLink" Width="200"></asp:TextBox><i><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Maximum25Character")%></i>
                                    <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidLink"
                                        runat="server" Display="None" OnServerValidate="OnValidateLink">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Text")%><%--<b style="color: Red;">*</b>--%>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtText" Width="400" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                   <%-- <asp:CustomValidator ValidationGroup="Create" EnableClientScript="false" ID="custValidText"
                                        runat="server" Display="None" OnServerValidate="OnValidateText">
                                    </asp:CustomValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="guidingtext">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "GuidingText")%>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top" class="align-right nowrap">
                                    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Expiration_Date")%><b style="color: Red;">*</b>
                                </td>
                                <td>
                                    <GRC:GRCDatePicker  ID="dtDate" runat="server" Width="100" 
                                        CBReadOnly="true">
                                    </GRC:GRCDatePicker>
                                    <asp:CustomValidator ID="custExpirationDate" EnableClientScript="false" ValidationGroup="Create"
                                        Display="None" OnServerValidate="custExpirationDate_ServerValidate" runat="server"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="btnSave_OnClick" CssClass="aha-small-button with-arrow"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" ID="lnkCancel" OnClick="btnCancel_OnClick" CssClass="aha-small-button with-arrow"><strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Cancel")%></em></strong></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right-shadow">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="shadowbottom">
        <div class="bl">
        </div>
        <div class="bm">
        </div>
        <div class="br">
        </div>
    </div>
</div>
