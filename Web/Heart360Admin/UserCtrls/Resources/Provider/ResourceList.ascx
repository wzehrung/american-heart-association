﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceList.ascx.cs"
    Inherits="Heart360Admin.UserCtrls.Resources.Provider.ResourceList" %>
<h1>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Provider_Resources")%></h1>
<p>
    <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "This_is_the_list_of_all_provider")%></p>
<table class="admin-box" cellpadding="0" cellspacing="0">
    <tr>
        <td class="align-right menubar">
            <a href="/Pages/Resources/Provider/CreateResources.aspx" class="aha-small-button with-arrow">
                <strong><em><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Add_New")%></em></strong></a>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="up1">
                <ContentTemplate>
                    <asp:GridView ShowHeader="true" EnableViewState="true" ID="gvResources" runat="server"
                        AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" EmptyDataText="<%$ Resources:Heart360AdminGlobalResources, EmtyResourceText %>"
                        AllowPaging="True" PageSize="25" ShowFooter="False" OnRowDataBound="gvResources_RowDataBound"
                        OnPageIndexChanging="gvResources_PageIndexChanging" GridLines="Horizontal" CssClass="provider-grid"
                        PagerStyle-CssClass="table-page-index">
                        <Columns>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Resource_item %>" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <a runat="server" id="aResource" target="_blank"></a>
                                    <br />
                                    <asp:Literal runat="server" ID="litResource"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Expiration_Date %>" ItemStyle-Width="100"
                                HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="litExpirationDate"></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Move_ %>"  ItemStyle-CssClass="order" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <a style="text-decoration: none;" href="#" runat="server" id="aUp" onserverclick="aUp_onserverclick">
                                        <img src="/images/Move-Up.jpg" alt="Move Up" title="Move Up" />
                                    </a><a style="text-decoration: none;" href="#" runat="server" id="aDown" onserverclick="aDown_onserverclick">
                                        <img src="/images/Move-Down.jpg" alt="Move Down" title="Move Down" /></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="True" HeaderText="<%$ Resources:Heart360AdminGlobalResources, Actions %>" ItemStyle-Width="160" HeaderStyle-CssClass="alignleft">
                                <ItemTemplate>
                                    <a runat="server" id="aEdit"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Edit")%></a>&nbsp;|&nbsp;<a runat="server" id="aDelete"
                                        onserverclick="aDelete_onserverclick"><%=GetGlobalResourceObject("Heart360AdminGlobalResources", "Delete")%></a>&nbsp;|&nbsp;<a runat="server" id="aAudit">
                                            <%=GetGlobalResourceObject("Heart360AdminGlobalResources", "AuditInfo")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" />
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
