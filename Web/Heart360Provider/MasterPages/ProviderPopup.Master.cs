﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAProvider;

namespace Heart360Provider.MasterPages
{
    public partial class ProviderPopup : System.Web.UI.MasterPage
    {
        protected string AppVirtualPath = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpRuntime.AppDomainAppVirtualPath != "/")
            {
                AppVirtualPath = HttpRuntime.AppDomainAppVirtualPath;
            }
        }
    }
}