﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//using HVManager;
//using Microsoft.Health;

using AHAProvider;



namespace Heart360Provider.MasterPages
{
                                                            // PJB: not sure this has to be a Health Vault application
    public partial class Provider : System.Web.UI.MasterPage // , IHealthApplicationInfo
    {
            // This is a hash table int = ProviderId
            // Instead of keeping this in session state, we will keep module headers by provider.
        private static Dictionary<int,Heart360Provider.Controls.ModuleHeader[]>     mModuleHeaders ;
            // We will also keep global popups by provider
        private static Dictionary<int, AHACommon.UXControl[]>                       mModulePopups;

            // these are displayed on the page
        protected string WelcomeTextTitle = string.Empty;
        protected string WelcomeTextName = string.Empty;
        protected string ProviderCodeTitle = string.Empty;
        protected string ProviderCodeTooltip = string.Empty;
        protected string ProviderCode = string.Empty;

            // we need to know what module is using this master page.
            // this needs to be assigned before Page_Load, on preinit

        private Heart360Provider.ProviderModules.MODULE_ID mModuleId = ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN;

        public Heart360Provider.ProviderModules.MODULE_ID ModuleId
        {
            get { return mModuleId; }
            set { mModuleId = value; }
        }

        public AHAHelpContent.ProviderDetails CurrentProvider
        {
            get;
            set;
        }

        public static Heart360Provider.Controls.ModuleHeader[] GetModuleHeaders( int providerId )
        {
            return mModuleHeaders.ContainsKey(providerId) ? mModuleHeaders[providerId] : null;
        }

        public static AHACommon.UXControl[] GetModulePopups(int providerId)
        {
            return mModulePopups.ContainsKey(providerId) ? mModulePopups[providerId] : null;
        }

            //
            // Page_Init needs to validate that there is a current provider logged in.
            // If not, need to redirect.
            //
        protected void Page_Init(object sender, EventArgs e)
        {
            // AHAProvider.SessionInfo.GetProviderContext().ProviderID;
            this.CurrentProvider = AHAProvider.H360RequestContext.GetContext().ProviderDetails;

            if (this.CurrentProvider == null)
            {
                // REDIRECT TO LANDING PAGE
                string landingPage = AppSettings.Url.ProviderPortal;
                HttpContext.Current.Response.Redirect(landingPage, true);
            }

            //
            // If we are here, we are good to go.
            // Allocate all the module headers
            //
            if( mModuleHeaders == null )
            {
                mModuleHeaders = new  Dictionary<int,Heart360Provider.Controls.ModuleHeader[]>() ;
            }

            if ( !mModuleHeaders.ContainsKey( CurrentProvider.ProviderID ) || mModuleHeaders[CurrentProvider.ProviderID] == null)
            {
                try
                {
                    Heart360Provider.Controls.ModuleHeader[]    headers = new Heart360Provider.Controls.ModuleHeader[(int)ProviderModules.MODULE_ID.MODULE_ID_COUNT];

                    foreach (ProviderModules.MODULE_ID modenum in Enum.GetValues(typeof(ProviderModules.MODULE_ID)))
                    {
                        if (modenum != ProviderModules.MODULE_ID.MODULE_ID_COUNT)
                        {
                            Heart360Provider.Controls.ModuleHeader mh = new Heart360Provider.Controls.ModuleHeader();
                            headers[(int)modenum] = mh;
                            mh.Name = "ModuleHeader";
                            mh.ModuleID = modenum;
                            if (!mh.Initialize())
                            {
                                mh = null;
                                headers = null;
                                break;
                            }
                        }
                    }

                    mModuleHeaders[CurrentProvider.ProviderID] = headers;

                }
                catch (Exception)
                {
                    
                }
            }

            //
            // Allocate all the module popups
            //
            if (mModulePopups == null)
            {
                mModulePopups = new Dictionary<int, AHACommon.UXControl[]>();
            }
            if (!mModulePopups.ContainsKey(CurrentProvider.ProviderID) || mModulePopups[CurrentProvider.ProviderID] == null)
            {
                try
                {
                    AHACommon.UXControl[] popups = new AHACommon.UXControl[(int)ProviderModules.POPUP_ID.POPUP_ID_COUNT];

                    // Note, the controls actually get new'd on an "as needed" basis

                    mModulePopups[CurrentProvider.ProviderID] = popups;

                }
                catch (Exception)
                {

                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            // This was taken from AHAWeb/MasterPages/PatientNew.Master.cs
            AHAProvider.H360Utility.AddImmediateExpiryHeadersToResponse();

            // The Provider Portal does not have to do a database sync with health vault 
            /**
            try
            {
                DBSyncHelper.PerformSync();
            }
            catch// (Exception ex)
            {
                throw;
                //GRCBase.ErrorLogHelper.LogAndIgnoreException(ex);
            }
             **/

            // Assign welcome text title
            if (!IsPostBack)
            {
                int iCount = AHAHelpContent.ProviderLoginDetails.GetProviderLoginCount(H360RequestContext.GetContext().ProviderDetails.ProviderID);
                if (iCount > 1)
                {
                    WelcomeTextTitle = GetGlobalResourceObject("Common", "Header_Text_WelcomeBack").ToString();

                }
                else
                {
                    WelcomeTextTitle = GetGlobalResourceObject("Common", "Header_Text_Welcome").ToString();
                }
            }

            //Assign provider/volunteer code title & tooltip
            if (CurrentProvider.UserTypeID == (int)AHAHelpContent.DbUserType.DbUserTypeProvider)
            {
                ProviderCodeTitle = GetGlobalResourceObject("Common", "Header_Provider_Code_Title").ToString();
                ProviderCodeTooltip = GetGlobalResourceObject("Common", "Header_Provider_Code_Tooltip").ToString();
            }
            else
            {
                ProviderCodeTitle = GetGlobalResourceObject("Common", "Header_Volunteer_Code_Title").ToString();
                ProviderCodeTooltip = GetGlobalResourceObject("Common", "Header_Volunteer_Code_Tooltip").ToString();
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (CurrentProvider != null)
            {
                WelcomeTextName = CurrentProvider.FirstName;
                if (WelcomeTextName != null && WelcomeTextName.Length > 11)
                {
                    WelcomeTextName = GRCBase.StringHelper.GetShortString(WelcomeTextName, 8);
                }
                ProviderCode = CurrentProvider.ProviderCode;
            }

        }

        protected void OnSignOut(object sender, EventArgs e)
        {
            String signOutURL = getSignOutUrl();    // have to take into consideration an optional campaign

            //clear any session-length preferences we are storing in client side cookies
            //Response.Cookies[GlobalPaths_HeartHealthApp.SessionPrefsCookie].Values.Clear();

            if (mModuleHeaders.ContainsKey(CurrentProvider.ProviderID))
                mModuleHeaders[CurrentProvider.ProviderID] = null;
            AHAProvider.SessionInfo.ResetProviderSession(); // ClearLoggedInUserState();

            // Redirect to our application's landing page
            HttpContext.Current.Response.Redirect(signOutURL, true);
        }

        protected String getSignOutUrl()
        {
            String signOutURL = AppSettings.Url.ProviderPortal;

            //Look for a campaign home page address in two possible places:
            //1. In the user identity context object in the session (most common)
            //2. In the H360 Utility. It will be here if this is the first time they ever logged in.
            if (!string.IsNullOrEmpty(SessionInfo.GetProviderContext().CampaignUrl))
            {
                signOutURL += SessionInfo.GetProviderContext().CampaignUrl;
            }
            else
            {
                if (!string.IsNullOrEmpty(AHAProvider.H360Utility.GetCurrentCampaignUrl()))
                {
                    signOutURL += AHAProvider.H360Utility.GetCurrentCampaignUrl();
                }
            }
            return signOutURL;
        }

    }
}