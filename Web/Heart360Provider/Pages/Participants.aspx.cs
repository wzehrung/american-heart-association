﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using AHAProvider;

namespace Heart360Provider.Pages
{
    public partial class Participants : Heart360Provider.Utility.ProviderWebPage
    {
        protected bool AutoOpenReceviedInvitations = false;
        protected bool AutoOpenPatientList = false;

        // local controls will go here. One for each accordion and then any local popups
        private static Heart360Provider.Controls.ReceivedInvitations        mReceivedInvitationsListControl;
        private static Heart360Provider.Controls.AcceptInvitationPopup      mAcceptInvitationPopup;
        private static Heart360Provider.Controls.DeclineInvitationPopup     mDeclineInvitationPopup;
        private static Heart360Provider.Controls.DisconnectParticipantPopup mDisconnectPatientPopup;

        private static Heart360Provider.Controls.SentInvitations            mSentInvitationsListControl;
        private static Heart360Provider.Controls.ReinviteViaEmail           mReinvitePopup;
        private static Heart360Provider.Controls.CancelInvitationPopup      mCancelInvitationPopup;

        private static Heart360Provider.Controls.PatientList                mPatientListControl;



        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS;

            //
            // Initialize module headers
            //
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS, "Pages/Participants" );


            // Allocate and initialize any local controls
            try
            {
                mReceivedInvitationsListControl = new Heart360Provider.Controls.ReceivedInvitations();
                mReceivedInvitationsListControl.Name = "ReceivedInvitations";
                if (!mReceivedInvitationsListControl.Initialize())
                {
                    mReceivedInvitationsListControl = null;
                }
            }
            catch (Exception ex)
            {
                mReceivedInvitationsListControl = null;
            }

            try
            {
                mAcceptInvitationPopup = new Heart360Provider.Controls.AcceptInvitationPopup();
                mAcceptInvitationPopup.Name = "AcceptInvitationPopup";
                if (!mAcceptInvitationPopup.Initialize())
                {
                    mAcceptInvitationPopup = null;
                }
            }
            catch (Exception ex)
            {
                mAcceptInvitationPopup = null;
            }

            try
            {
                mDeclineInvitationPopup = new Heart360Provider.Controls.DeclineInvitationPopup();
                mDeclineInvitationPopup.Name = "DeclineInvitationPopup";
                if (!mDeclineInvitationPopup.Initialize())
                {
                    mDeclineInvitationPopup = null;
                }
            }
            catch (Exception ex)
            {
                mDeclineInvitationPopup = null;
            }

            try
            {
                mSentInvitationsListControl = new Heart360Provider.Controls.SentInvitations();
                mSentInvitationsListControl.Name = "SentInvitations";
                if (!mSentInvitationsListControl.Initialize())
                {
                    mSentInvitationsListControl = null;
                }
            }
            catch (Exception ex)
            {
                mSentInvitationsListControl = null;
            }

            try
            {
                mReinvitePopup = new Heart360Provider.Controls.ReinviteViaEmail();
                mReinvitePopup.Name = "ReinvitePopup";
                if (!mReinvitePopup.Initialize())
                {
                    mReinvitePopup = null;
                }
            }
            catch (Exception ex)
            {
                mReinvitePopup = null;
            }

            try
            {
                mCancelInvitationPopup = new Heart360Provider.Controls.CancelInvitationPopup();
                mCancelInvitationPopup.Name = "CancelInvitationPopup";
                if (!mCancelInvitationPopup.Initialize())
                {
                    mCancelInvitationPopup = null;
                }
            }
            catch (Exception ex)
            {
                mCancelInvitationPopup = null;
            }

            try
            {
                mDisconnectPatientPopup = new Heart360Provider.Controls.DisconnectParticipantPopup();
                mDisconnectPatientPopup.Name = "DisconnectParticipantPopup";
                if (!mDisconnectPatientPopup.Initialize())
                {
                    mDisconnectPatientPopup = null;
                }
            }
            catch (Exception ex)
            {
                mDisconnectPatientPopup = null;
            }

            try
            {
                mPatientListControl = new Heart360Provider.Controls.PatientList();
                mPatientListControl.Name = "PatientList";
                if (!mPatientListControl.Initialize())
                {
                    mPatientListControl = null;
                }
            }
            catch (Exception ex)
            {
                mPatientListControl = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if invitations are outstanding, open the accordion, otherwise open the patients accordion
            ProviderContext provider = SessionInfo.GetProviderContext();
            List<AHAHelpContent.PatientProviderInvitation> ilist = 
                AHAHelpContent.PatientProviderInvitation.FindPendingInvitationsByAcceptedProvider(
                    provider.ProviderID, 
                    AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Pending);
            if (ilist == null || ilist.Count == 0)
            {
                AutoOpenReceviedInvitations = false;
                AutoOpenPatientList = true;
            }
            else
            {
                AutoOpenReceviedInvitations = true;
                AutoOpenPatientList = false;
            }

            /** Uncomment this when we have the Table View Template
            if (!IsPostBack)
            {
                AHAProvider.H360RequestContext objRequestContext = AHAProvider.H360RequestContext.GetContext();
                List<AHAHelpContent.Patient> lstPatients = AHAProvider.H360RequestContext.GetContext().PatientList;

                // Now load patient information from HealthVault
                List<Utility.PatientInfo> lstPatientInfo = Utility.PatientInfo.GetPopulatedPatientInfo(lstPatients);

            }
             **/

        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs )
        {
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string ReceivedInvitationsListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mReceivedInvitationsListControl, INIT, inputs);
        }

        [WebMethod]
        public static string AcceptPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mAcceptInvitationPopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeclinePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeclineInvitationPopup, INIT, inputs);
        }

        [WebMethod]
        public static string PatientListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mPatientListControl, INIT, inputs);
        }

        [WebMethod]
        public static string DisconnectPatientPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDisconnectPatientPopup, INIT, inputs);
        }

        [WebMethod]
        public static string SentInvitationsListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mSentInvitationsListControl, INIT, inputs);
        }

        [WebMethod]
        public static string ReinvitePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mReinvitePopup, INIT, inputs);
        }

        [WebMethod]
        public static string CancelSentPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mCancelInvitationPopup, INIT, inputs);
        }
    }
}