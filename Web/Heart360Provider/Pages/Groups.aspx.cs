﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using AHAProvider;
using AHAHelpContent;

namespace Heart360Provider.Pages
{
    public partial class Groups : Heart360Provider.Utility.ProviderWebPage
    {
        private static Heart360Provider.Controls.EditGroupPopup mEditGroupPopup;
        private static Heart360Provider.Controls.EditDynamicGroupPopup mEditDynamicGroupPopup;
        private static Heart360Provider.Controls.DeleteGroupPopup mDeleteGroupPopup;
        private static Heart360Provider.Controls.GroupList mGroupList;
        private static Heart360Provider.Controls.AddAlertRulePopup  mAddAlertRulePopup;
        private static Heart360Provider.Controls.AddAlertRulePopup  mEditAlertRulePopup;
        private static Heart360Provider.Controls.DeleteAlertRule    mDeleteAlertRulePopup;

        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_GROUPS;

            //
            // Initialize module headers
            //
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_GROUPS, "Pages/Groups" );

            // Allocate and initialize Popups
            try
            {
                mEditGroupPopup = new Controls.EditGroupPopup();
                mEditGroupPopup.Name = "EditGroupPopup";
                if (!mEditGroupPopup.Initialize())
                {
                    mEditGroupPopup = null;
                }
            }
            catch (Exception)
            {
                mEditGroupPopup = null;
            }
            try
            {
                mEditDynamicGroupPopup = new Controls.EditDynamicGroupPopup();
                mEditDynamicGroupPopup.Name = "EditDynamicGroupPopup";
                if (!mEditDynamicGroupPopup.Initialize())
                {
                    mEditDynamicGroupPopup = null;
                }
            }
            catch (Exception)
            {
                mEditDynamicGroupPopup = null;
            }
            try
            {
                mDeleteGroupPopup = new Controls.DeleteGroupPopup();
                mDeleteGroupPopup.Name = "DeleteGroupPopup";
                if (!mDeleteGroupPopup.Initialize())
                {
                    mDeleteGroupPopup = null;
                }
            }
            catch (Exception)
            {
                mDeleteGroupPopup = null;
            }

            // Allocate group list control
            try
            {
                mGroupList = new Controls.GroupList();
                mGroupList.Name = "GroupList";
                if (!mGroupList.Initialize())
                {
                    mGroupList = null;
                }
            }
            catch (Exception)
            {
                mGroupList = null;
            }

            // Allocate alert rule popups
            try
            {
                mAddAlertRulePopup = new Controls.AddAlertRulePopup();
                mAddAlertRulePopup.Name = "AddAlertRulePopup";
                if (!mAddAlertRulePopup.Initialize())
                {
                    mAddAlertRulePopup = null;
                }
            }
            catch (Exception)
            {
                mAddAlertRulePopup = null;
            }
            try
            {
                mEditAlertRulePopup = new Controls.AddAlertRulePopup();
                mEditAlertRulePopup.Name = "AddAlertRulePopup";
                if (!mEditAlertRulePopup.Initialize())
                {
                    mEditAlertRulePopup = null;
                }
            }
            catch (Exception)
            {
                mEditAlertRulePopup = null;
            }
            try
            {
                mDeleteAlertRulePopup = new Controls.DeleteAlertRule();
                mDeleteAlertRulePopup.Name = "DeleteAlertRulePopup";
                if (!mDeleteAlertRulePopup.Initialize())
                {
                    mDeleteAlertRulePopup = null;
                }
            }
            catch (Exception)
            {
                mDeleteAlertRulePopup = null;
            }
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string EditGroupPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditGroupPopup, INIT, inputs);
        }

        [WebMethod]
        public static string EditDynamicGroupPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditDynamicGroupPopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeleteGroupPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteGroupPopup, INIT, inputs);
        }

        [WebMethod]
        public static string GroupListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mGroupList, INIT, inputs);
        }

        [WebMethod]
        public static string AddGroupAlertRulePopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            if (inputs != null && inputs.Length > 0)
            {
                int size = inputs.Length;
                Array.Resize(ref inputs, size + 1);
                inputs[size] = new AHACommon.NameValue("handling", "preselect-group");
            }
            return AHACommon.UXControl.ControlHandler(mAddAlertRulePopup, INIT, inputs);
        }

        [WebMethod]
        public static string EditAlertRulePopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditAlertRulePopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeleteAlertRulePopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteAlertRulePopup, INIT, inputs);
        }
    }
}