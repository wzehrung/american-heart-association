﻿<%@ Page Title="Heart360 - Provider Alerts" Language="C#" MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="Alerts.aspx.cs" Inherits="Heart360Provider.Pages.Alerts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <!-- Big Tile Column -->
    <div class="eightcol tablet-twelvecol mobile-twelvecol first">
        <!-- Alerts Module Header (current) -->
        <div id="idModuleHeader1" class="dynamic-content-container big-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

        <!-- TODO: update explanitory text  -->
        <div class="blue-text-area">
		    <p>In Heart360® you can create alerts to notify you when a patient/participant has entered a reading that you would like to be aware of. You can create new rules below, review existing rules, and view all of the alerts that have been triggered by patients/participants.</p>
	    </div>

        <!-- Alerts accordion -->
        <div class="accordion-container alerts">

			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
				    <!-- Start Accordion Text ( No need for a div or span)-->
						Alerts 
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->
			<div class="general-content">
                <!-- Alerts List control -->
                <div id="idControlAlertList" class="dynamic-content-container" data-page="Pages/Alerts" data-method="AlertListControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                </div>
			</div><!-- End .general-content -->
		</div><!-- End .accordion-container -->



        <!-- Alert Rules accordion -->
		<div class="accordion-container alert-triggers">
			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
				    <!-- Start Accordion Text ( No need for a div or span)-->
					Alert Rules
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->
			<div class="general-content">
			    <!-- Alert Rules List control -->
                <div id="idControlAlertRulesList" class="dynamic-content-container" data-page="Pages/Alerts" data-method="AlertRuleListControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    
                </div>
			</div>
		</div><!-- End .accordion-container -->




        <!-- Settings accordion -->
		<div class="accordion-container alert-settings">
			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
				    <!-- Start Accordion Text ( No need for a div or span)-->
					Settings
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->
			<div class="general-content">
			    <!-- Alert Settings control -->
                <div id="idControlAlertSettings" class="dynamic-content-container" data-page="Pages/Alerts" data-method="SettingsControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    
                </div>
			</div>
		</div><!-- End .accordion-container -->




    </div><!-- End Big Tile Column -->

    <!-- Small tiles -->
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- My Participants Module Header -->
        <div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Messages Module Header -->
        <div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Reports Module Header  -->
        <div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Groups Module Header  -->
        <div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Resources Module Header  -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Profile Module Header  -->
        <div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header  -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Alerts" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>
