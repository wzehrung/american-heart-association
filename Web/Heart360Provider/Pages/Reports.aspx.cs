﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using AHACommon;
using AHAProvider;
using AHABusinessLogic;
using Heart360Provider.Reports;


namespace Heart360Provider.Pages
{
    public partial class Reports : Heart360Provider.Utility.ProviderWebPage
    {
        // local controls and popups
        private static Heart360Provider.Controls.IndividualReport mIndividualReport;
        private static Heart360Provider.Controls.StandardReport mStandardReport;
        private static Heart360Provider.Controls.CustomReport mCustomReport;
        private static Heart360Provider.Controls.DownloadReport mDownloadReport;


        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_REPORTS;

            // Initialize module headers
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_REPORTS, "Pages/Reports" );

            try
            {
                mIndividualReport = new Controls.IndividualReport();
                mIndividualReport.Name = "IndividualReport";
                if (!mIndividualReport.Initialize())
                {
                    mIndividualReport = null;
                }
            }
            catch (Exception)
            {
                mIndividualReport = null;
            }
            try
            {
                mStandardReport = new Controls.StandardReport();
                mStandardReport.Name = "StandardReport";
                if (!mStandardReport.Initialize())
                {
                    mStandardReport = null;
                }
            }
            catch (Exception)
            {
                mStandardReport = null;
            }
            try
            {
                mCustomReport = new Controls.CustomReport();
                mCustomReport.Name = "CustomReport";
                if (!mCustomReport.Initialize())
                {
                    mCustomReport = null;
                }
            }
            catch (Exception)
            {
                mCustomReport = null;
            }
            try
            {
                mDownloadReport = new Controls.DownloadReport();
                mDownloadReport.Name = "DownloadReport";
                if (!mDownloadReport.Initialize())
                {
                    mDownloadReport = null;
                }
            }
            catch (Exception)
            {
                mDownloadReport = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //every report accordion submits through here in order to push the PDF back to the user
            string runReport = HttpContext.Current.Request.Form["runReport"];
            if ( runReport != null)
            {
                switch(runReport) {
                    case "standard":
                        mStandardReport.RunReport(HttpContext.Current.Request.Form);
                        break;
                    case "custom": 
                        mCustomReport.RunReport(HttpContext.Current.Request.Form);
                        break;
                    case "individual":
                        mIndividualReport.RunReport(HttpContext.Current.Request.Form);
                        break;
                    case "download": 
                        mDownloadReport.RunReport(HttpContext.Current.Request.Form);
                        break;
                    default:
                        //do nothing
                        break;
                }
            }
            //load base page
        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string IndividualReport(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mIndividualReport, INIT, inputs);
        }

        [WebMethod]
        public static string StandardReport(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mStandardReport, INIT, inputs);
        }

        [WebMethod]
        public static string CustomReport(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler( mCustomReport, INIT, inputs);
        }

        [WebMethod]
        public static string DownloadReport(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDownloadReport, INIT, inputs);
        }
    }
}