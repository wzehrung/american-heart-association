﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Heart360Provider.Pages
{
    public partial class Profile : Heart360Provider.Utility.ProviderWebPage
    {
        private static Heart360Provider.Controls.ProfileInfo mProfileControl ;


        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_PROFILE;

            //
            // Initialize module headers
            //
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_PROFILE, "Pages/Profile" );

            // Allocate and initialize our profile information control
            try
            {
                mProfileControl = new Controls.ProfileInfo();
                mProfileControl.Name = "ProfileInfo";
                if (!mProfileControl.Initialize())
                {
                    mProfileControl = null;
                }
                else
                {
                    mProfileControl.ProviderInfo = ((Heart360Provider.MasterPages.Provider)this.Master).CurrentProvider;
                    mProfileControl.ProviderType = (mProfileControl.ProviderInfo.UserTypeID == (int)AHAHelpContent.DbUserType.DbUserTypeVolunteer) ?
                                                        AHAHelpContent.DbUserType.DbUserTypeVolunteer : AHAHelpContent.DbUserType.DbUserTypeProvider;
                }
            }
            catch (Exception)
            {
                mProfileControl = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string ProfileControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mProfileControl, INIT, inputs);
        }

    }
}