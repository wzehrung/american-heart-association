﻿<%@ Page Title="Heart360 - Provider Patients/Participants" Language="C#" MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="Participants.aspx.cs" Inherits="Heart360Provider.Pages.Participants" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <!-- Big Tile Column -->
    <div class="eightcol tablet-twelvecol mobile-twelvecol first">
        <!-- My Participants Module Header (current) -->
        <div id="idModuleHeader4" class="dynamic-content-container big-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

        <!-- TODO: update explanitory text  -->
        <div class="blue-text-area">
            <p>If you are new to Heart360®, the best place to start is by inviting patient/participants to connect with you by using the invite options located on the top right hand corner of the "My Participants" tile. Once connected, you will see summary information on your patient's/participant's recent activity within Heart360®. Also, you can click the tiles to the right for more specific information on their data entries, alerts and groups, and health history.</p>
        </div>

        <!-- Received Invitations accordion -->
        <div class="accordion-container received-invitations">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    <!-- Start Accordion Text ( No need for a div or span)-->
                        Received Invitations 
                    <!-- End Accordion TExt -->
            </div><!-- End .general-trigger -->

            <div class="general-content">
                <!-- Add received invitations list control -->
                <div id="idControlReceivedInvitationsList" class="dynamic-content-container" data-page="Pages/Participants" data-method="ReceivedInvitationsListControlHandler" data-refresh-ids="idControlReceivedInvitationsList" <%= AutoOpenReceviedInvitations ? "data-customclass='open-on-invitations'" : ""  %>>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                </div>
            </div><!-- End .general-content -->
        </div><!-- End .accordion-container -->

        <!-- Sent Invitations accordion -->
        <div class="accordion-container sent-invitations">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    <!-- Start Accoriodn Text ( No need for a div or span)-->
                    Sent Invitations
                    <!-- End Accordion TExt -->
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <!-- sent invitations list control -->
                <div id="idControlSentInvitationsList" class="dynamic-content-container" data-page="Pages/Participants" data-method="SentInvitationsListControlHandler" data-refresh-ids="idControlSentInvitationsList" >
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                </div>
            </div>
        </div><!-- End .accordion-container -->

        <!-- My Patients/Participants accordion -->
        <div class="accordion-container my-patients">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    <!-- Start Accoriodn Text ( No need for a div or span)-->
                    My Patients/Participants
                    <!-- End Accordion TExt -->
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <!-- Add patients list control -->

                <div id="idControlPatientList" class="dynamic-content-container" data-page="Pages/Participants" data-method="PatientListControlHandler" data-refresh-ids="idControlPatientList" <%= AutoOpenPatientList ? "data-customclass='open-on-no-invitations'" : ""  %>>

                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                </div>
            </div>
        </div><!-- End .accordion-container -->
    </div><!-- End Big Tile Column -->


    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- Messages Module Header -->
        <div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Alerts Module Header -->
        <div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Reports Module Header  -->
        <div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Groups Module Header  -->
        <div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Resources Module Header  -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Profile Module Header  -->
        <div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header  -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Participants" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>
