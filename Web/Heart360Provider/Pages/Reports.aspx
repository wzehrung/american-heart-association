﻿<%@ Page Title="Heart360 - Provider Reports" Language="C#" MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Heart360Provider.Pages.Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <input name="runReport" id="runReport" value="none" type="hidden" />
    <script type="text/javascript">
        function runReport(reportType) {
            $('#runReport').val(reportType);
            $('#formHomePage').prop('target', '_self');
            $('#formHomePage').submit();
        }
        function showError(errorText, messageArea) {
            messageArea.addClass('error-message');
            messageArea.html(errorText);
            messageArea.fadeIn();
        }
        function removeError(messageArea) {
            messageArea.hide();
            messageArea.html('');
            messageArea.removeClass('error-message');
        }
    </script>

    <!-- Big Tile Column -->
    <div class="eightcol tablet-twelvecol mobile-twelvecol first">
        <!-- Reports Module Header (current) -->
        <div id="idModuleHeader8" class="dynamic-content-container big-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

        <div class="blue-text-area">
            <p>In Heart360® you can create reports for any and all data types for a specific list of patients/participants or by group.</p>
        </div>

        <!-- Individual Patient Report accordion -->
        <div class="accordion-container no-accordion-icon">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    <!-- Start Accordion Text ( No need for a div or span)-->
                        Individual Patient Report 
                    <!-- End Accordion TExt -->
            </div><!-- End .general-trigger -->

            <div class="general-content">
                <div id="idIndividualReport" class="dynamic-content-container" data-page="Pages/Reports" data-method="IndividualReport" >
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                </div>
            </div><!-- End .general-content -->
        </div><!-- End .accordion-container -->

        <!-- Standard Organization Report accordion -->
        <div class="accordion-container no-accordion-icon">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    <!-- Start Accordion Text ( No need for a div or span)-->
                    Standard Organization Report
                    <!-- End Accordion TExt -->
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <div id="idStandardReport" class="dynamic-content-container" data-page="Pages/Reports" data-method="StandardReport" >
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                </div>
            </div><!-- End .general-content -->
        </div><!-- End .accordion-container -->

        <!-- Custom Organization Report accordion -->
        <div class="accordion-container no-accordion-icon">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    <!-- Start Accordion Text ( No need for a div or span)-->
                    Custom Organization Report
                    <!-- End Accordion TExt -->
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <div id="idCustomReport" class="dynamic-content-container" data-page="Pages/Reports" data-method="CustomReport" >
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                </div>            
            </div>
        </div><!-- End .accordion-container -->

        <!-- Download CSV accordion -->
        <div class="accordion-container no-accordion-icon">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    <!-- Start Accordion Text ( No need for a div or span)-->
                    Download CSV
                    <!-- End Accordion TExt -->
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <div id="idDownloadReport" class="dynamic-content-container" data-page="Pages/Reports" data-method="DownloadReport" >
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                </div>            
            </div>
        </div><!-- End .accordion-container -->

    </div><!-- End Big Tile Column -->

    <!-- Small tiles -->
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- Participants Module Header  -->
        <div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Messages Module Header -->
        <div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Alerts Module Header -->
        <div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Groups Module Header  -->
        <div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Resources Module Header  -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Profile Module Header  -->
        <div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header  -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Reports" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>
