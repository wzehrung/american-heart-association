﻿<%@ Page Title="Heart360 - Provider Profile" Language="C#" MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Heart360Provider.Pages.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <!-- Big Tile Column -->
    <div class="eightcol tablet-twelvecol mobile-twelvecol first">
        <!-- My Participants Module Header (current) -->
        <div id="idModuleHeader5" class="dynamic-content-container big-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

        <!-- TODO: update explanitory text  -->
        <div class="blue-text-area">
		    <p>The Heart360® Profile page allows you to edit your personal information. Patients and Participants can view your profile information when they connect with you.</p>
	    </div>

        <!-- Add profile information control -->
        <div id="idControlProfile" class="dynamic-content-container" data-page="Pages/Profile" data-method="ProfileControlHandler" data-refresh-ids="idControlProfile" >
            <div class="dynamic-content-message">
                <!-- Messages will be added here -->
            </div>
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
        </div>

    </div> <!-- End Big Tile Column -->

    <!-- Small tiles -->
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- My Participants Module Header  -->
        <div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Messages Module Header -->
        <div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Alerts Module Header -->
        <div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Reports Module Header  -->
        <div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Groups Module Header  -->
        <div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Resources Module Header  -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header  -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Profile" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>
