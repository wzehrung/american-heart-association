﻿<%@ Page Title="Heart360 - Provider Messages" Language="C#" MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="Heart360Provider.Pages.Messages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <!-- Big Tile Column -->
    <div class="eightcol tablet-twelvecol mobile-twelvecol first">
        <!-- My Participants Module Header (current) -->
        <div id="idModuleHeader3" class="dynamic-content-container big-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

        <!-- TODO: update explanitory text  -->
        <div class="blue-text-area">
		    <p>The Heart360® Message center allows you to send and receive secure messages with your connected patients/participants. Messages can be managed through your Heart360 Inbox once you have logged in to your account.</p>
	    </div>


        <!-- Messages accordion -->
        <div class="accordion-container messages">
			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
				    <!-- Start Accordion Text ( No need for a div or span)-->
						Messages 
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->

			<div class="general-content">
                <!-- need a tab container with three tabs: inbox, sent, and trash -->
                <div class="tabs-container">
                    <div class="tabs-header">
                        <div class="tab tab-inbox active" data-tabcontent="content-inbox">Inbox</div>
                        <div class="tab tab-sent" data-tabcontent="content-sent">Sent</div>
                        <div class="tab tab-trash" data-tabcontent="content-trash">Trash</div>
                    </div>
                    <div class="tabs-content">
                        <div class="tab-content content-inbox">
                            <div class="twelvecol" >
                                <!-- Inbox message list control -->
                                <div id="idControlInboxMessageList" class="dynamic-content-container" data-page="Pages/Messages" data-method="InboxMessageListControlHandler" >
                                    <div class="dynamic-content-html">
                                        <!-- Dynamic content will be loaded here -->
                                    </div>
                                    <div class="dynamic-content-message">
                                        <!-- Alert Messages will be added here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content content-sent">
                            <div class="twelvecol" >
                                <!-- Sent message list control -->
                                <div id="idControlSentMessageList" class="dynamic-content-container" data-page="Pages/Messages" data-method="SentMessageListControlHandler" >
                                    <div class="dynamic-content-html">
                                        <!-- Dynamic content will be loaded here -->
                                    </div>
                                    <div class="dynamic-content-message">
                                        <!-- Alert Messages will be added here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content content-trash">
                            <div class="twelvecol" >
                                <!-- Trash message list control -->
                                <div id="idControlTrashMessageList" class="dynamic-content-container" data-page="Pages/Messages" data-method="TrashMessageListControlHandler" >
                                    <div class="dynamic-content-html">
                                        <!-- Dynamic content will be loaded here -->
                                    </div>
                                    <div class="dynamic-content-message">
                                        <!-- Alert Messages will be added here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

			</div><!-- End .general-content -->

		</div><!-- End .accordion-container -->



        <!-- Settings accordion -->
		<div class="accordion-container message-settings">
			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
				    <!-- Start Accordion Text ( No need for a div or span)-->
					Settings
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->
			<div class="general-content">
			    <!-- Message Settings control -->
                <div id="idControlMessageSettings" class="dynamic-content-container" data-page="Pages/Messages" data-method="SettingsControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Alert Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                    
                </div>
			</div>
		</div><!-- End .accordion-container -->




    </div><!-- End Big Tile Column -->

    <!-- Small tiles -->
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- Participants Module Header -->
        <div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Alerts Module Header -->
        <div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Reports Module Header  -->
        <div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Groups Module Header  -->
        <div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Resources Module Header  -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Profile Module Header  -->
        <div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header  -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Messages" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>
