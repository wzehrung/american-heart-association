﻿<%@ Page Title="Heart360 - Provider Dashboard" Language="C#"  MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Heart360Provider.Pages.Dashboard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div class="fourcol tablet-twelvecol mobile-twelvecol first">
        <!-- My Participants Module Header -->
        <div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Messages Module Header -->
        <div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Alerts Module Header -->
        <div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        
        <!-- Reports Module Header -->
        <div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        
        <!-- Groups Module Header -->
        <div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        
        <!-- Resources Module Header -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

    </div>
   
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- Profile Module Header -->
        <div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Dashboard" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>
