﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using AHAProvider;
using AHAHelpContent;

namespace Heart360Provider.Pages
{
    public partial class Dashboard : Heart360Provider.Utility.ProviderWebPage
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_DASHBOARD;

            //
            // Initialize module headers
            //

            Heart360Provider.Controls.ModuleHeader[] headers = Heart360Provider.MasterPages.Provider.GetModuleHeaders(this.ProviderID);

            if (headers != null)
            {
                foreach (ProviderModules.MODULE_ID modenum in Enum.GetValues(typeof(ProviderModules.MODULE_ID)))
                {
                    if (modenum != ProviderModules.MODULE_ID.MODULE_ID_COUNT)
                    {
                        headers[(int)modenum].HeaderLocation = Heart360Provider.Controls.ModuleHeader.HEADER_LOCATION.HEADER_LOCATION_DASHBOARD;
                        headers[(int)modenum].PopupPage = "Pages/Dashboard";
                        headers[(int)modenum].PopupLink1Method = "HeaderPopupHandler";
                        headers[(int)modenum].PopupLink2Method = "HeaderPopupHandler";
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //
            // First check if this is a redirect from ConnectPatient.aspx
            //
            /**
            AHAAPI.PatientInvitation objInvitaionContext = AHAAPI.SessionInfo.GetPatientInvitationSession();
            if (objInvitaionContext != null && objInvitaionContext.IsPatientAccept)
            {
            }
            **/
        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }
    }
}