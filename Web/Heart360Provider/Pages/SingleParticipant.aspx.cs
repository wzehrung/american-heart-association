﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using AHABusinessLogic;

namespace Heart360Provider.Pages
{
    public partial class SingleParticipant : Heart360Provider.Utility.ProviderWebPage
    {
        // Local Controls
        private static Heart360Provider.Controls.HealthHistory          mHealthHistoryControl;
        private static Heart360Provider.Controls.ProviderNoteList       mNoteListControl;
        private static Heart360Provider.Controls.AlertList              mAlertListControl;
        private static Heart360Provider.Controls.PatientGroupList       mGroupListControl;
        private static Heart360Provider.Controls.MessageList            mInboxMessageList;
        private static Heart360Provider.Controls.MessageList            mSentMessageList;
        private static Heart360Provider.Controls.MessageList            mTrashMessageList;

        // Popups
        private static Heart360Provider.Controls.AddProviderNotePopup       mEditProviderNotePopup;
        private static Heart360Provider.Controls.DeleteProviderNotePopup    mDeleteNotePopup;
        private static Heart360Provider.Controls.DismissAlertPopup          mDismissAlertPopup;
        private static Heart360Provider.Controls.EditGroupPopup             mEditGroupPopup;
        private static Heart360Provider.Controls.EditDynamicGroupPopup      mEditDynamicGroupPopup;
        private static Heart360Provider.Controls.ComposeMessagePopup        mReplyMessagePopup;
        private static Heart360Provider.Controls.DeleteMessagePopup         mDeleteMessagePopup;

        // Assorted values
        AHABusinessLogic.PatientGuids   mPatientOfflineGuids;
        AHACommon.DataDateReturnType    mDataRange = AHACommon.DataDateReturnType.Last3Months; // default value


        protected void Page_Init(object sender, EventArgs e)
        {
                // Assign our module id to the master page
                this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT;

                mPatientOfflineGuids = null;

                // Make sure a patient is specified
                bool validPatientGuid = false;
                if (!string.IsNullOrEmpty(this.Request.QueryString["pg"]))
                {
                    string sguid = this.Request.QueryString["pg"];
                    Guid tryGuid;
                    if (Guid.TryParse(sguid, out tryGuid))
                    {
                        AHAHelpContent.Patient trypat = AHAHelpContent.Patient.FindByUserHealthRecordGUID(tryGuid);
                        if (trypat != null)
                        {
                            AHAProvider.SessionInfo.SetProviderCurrentPatient(sguid);
                            mPatientOfflineGuids = new AHABusinessLogic.PatientGuids { PersonID = trypat.OfflinePersonGUID.Value, RecordID = trypat.OfflineHealthRecordGUID.Value };
                            validPatientGuid = true;
                        }
                    }
                }

                if (!validPatientGuid)
                {
                    // REDIRECT TO LANDING PAGE
                    string landingPage = AppSettings.Url.ProviderPortal;
                    HttpContext.Current.Response.Redirect(landingPage, true);
                }

                //
                // Initialize module headers
                //
                Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT, "Pages/SingleParticipant");

                //
                // Initialize controls and popups
                //

                try
                {
                    mHealthHistoryControl = new Controls.HealthHistory();
                    mHealthHistoryControl.Name = "HealthHistory";
                    if (!mHealthHistoryControl.Initialize())
                    {
                        mHealthHistoryControl = null;
                    }
                }
                catch (Exception)
                {
                    mHealthHistoryControl = null;
                }
                try
                {
                    mNoteListControl = new Controls.ProviderNoteList();
                    mNoteListControl.Name = "ProviderNoteList";
                    if (!mNoteListControl.Initialize())
                    {
                        mNoteListControl = null;
                    }
                }
                catch (Exception)
                {
                    mNoteListControl = null;
                }
                try
                {
                    mAlertListControl = new Controls.AlertList();
                    mAlertListControl.Name = "AlertList";
                    mAlertListControl.WebMethodPage = "Pages/SingleParticipant";
                    if (!mAlertListControl.Initialize())
                    {
                        mAlertListControl = null;
                    }
                }
                catch (Exception)
                {
                    mAlertListControl = null;
                }
                try
                {
                    mGroupListControl = new Controls.PatientGroupList();
                    mGroupListControl.Name = "PatientGroupList";
                    if (!mGroupListControl.Initialize())
                    {
                        mGroupListControl = null;
                    }
                }
                catch (Exception)
                {
                    mGroupListControl = null;
                }
                try
                {
                    mInboxMessageList = new Controls.MessageList();
                    mInboxMessageList.Name = "MessageList";
                    mInboxMessageList.WebMethodPage = "Pages/SingleParticipant";
                    mInboxMessageList.Folder = Heart360Provider.Controls.MessageList.FOLDER.FOLDER_INBOX;
                    if (!mInboxMessageList.Initialize())
                    {
                        mInboxMessageList = null;
                    }
                }
                catch (Exception)
                {
                    mSentMessageList = null;
                }

                try
                {
                    mSentMessageList = new Controls.MessageList();
                    mSentMessageList.Name = "MessageList";
                    mSentMessageList.WebMethodPage = "Pages/SingleParticipant";
                    mSentMessageList.Folder = Heart360Provider.Controls.MessageList.FOLDER.FOLDER_SENT;
                    if (!mSentMessageList.Initialize())
                    {
                        mSentMessageList = null;
                    }
                }
                catch (Exception)
                {
                    mTrashMessageList = null;
                }

                try
                {
                    mTrashMessageList = new Controls.MessageList();
                    mTrashMessageList.Name = "MessageList";
                    mTrashMessageList.WebMethodPage = "Pages/SingleParticipant";
                    mTrashMessageList.Folder = Heart360Provider.Controls.MessageList.FOLDER.FOLDER_TRASH;
                    if (!mTrashMessageList.Initialize())
                    {
                        mTrashMessageList = null;
                    }
                }
                catch (Exception)
                {
                    mTrashMessageList = null;
                }

                try
                {
                    mEditProviderNotePopup = new Controls.AddProviderNotePopup();
                    mEditProviderNotePopup.Name = "AddProviderNotePopup";
                    if (!mEditProviderNotePopup.Initialize())
                    {
                        mEditProviderNotePopup = null;
                    }
                }
                catch (Exception)
                {
                    mEditProviderNotePopup = null;
                }

                try
                {
                    mDeleteNotePopup = new Controls.DeleteProviderNotePopup();
                    mDeleteNotePopup.Name = "DeleteProviderNotePopup";
                    if (!mDeleteNotePopup.Initialize())
                    {
                        mDeleteNotePopup = null;
                    }
                }
                catch (Exception)
                {
                    mDeleteNotePopup = null;
                }

                try
                {
                    mDismissAlertPopup = new Controls.DismissAlertPopup();
                    mDismissAlertPopup.Name = "DismissAlertPopup";
                    if (!mDismissAlertPopup.Initialize())
                    {
                        mDismissAlertPopup = null;
                    }
                }
                catch (Exception)
                {
                    mDismissAlertPopup = null;
                }

                try
                {
                    mEditGroupPopup = new Controls.EditGroupPopup();
                    mEditGroupPopup.Name = "EditGroupPopup";
                    if (!mEditGroupPopup.Initialize())
                    {
                        mEditGroupPopup = null;
                    }
                }
                catch (Exception)
                {
                    mEditGroupPopup = null;
                }
                try
                {
                    mEditDynamicGroupPopup = new Controls.EditDynamicGroupPopup();
                    mEditDynamicGroupPopup.Name = "EditDynamicGroupPopup";
                    if (!mEditDynamicGroupPopup.Initialize())
                    {
                        mEditDynamicGroupPopup = null;
                    }
                }
                catch (Exception)
                {
                    mEditDynamicGroupPopup = null;
                }

                try
                {
                    mReplyMessagePopup = new Controls.ComposeMessagePopup();
                    mReplyMessagePopup.Name = "ComposeMessagePopup";
                    if (!mReplyMessagePopup.Initialize())
                    {
                        mReplyMessagePopup = null;
                    }
                }
                catch (Exception)
                {
                    mReplyMessagePopup = null;
                }

                try
                {
                    mDeleteMessagePopup = new Controls.DeleteMessagePopup();
                    mDeleteMessagePopup.Name = "DeleteMessagePopup";
                    if (!mDeleteMessagePopup.Initialize())
                    {
                        mDeleteMessagePopup = null;
                    }
                }
                catch (Exception)
                {
                    mDeleteMessagePopup = null;
                }

                //
                // Initialize trackers
                //

                this.trackerBloodPressure.TrackerType = AHACommon.PatientTrackers.TrackerType.BloodPressure;
                this.trackerBloodPressure.PatientOfflineGuids = mPatientOfflineGuids;
                
                this.trackerCholesterol.TrackerType = AHACommon.PatientTrackers.TrackerType.Cholesterol;
                this.trackerCholesterol.PatientOfflineGuids = mPatientOfflineGuids;
                
                this.trackerBloodGlucose.TrackerType = AHACommon.PatientTrackers.TrackerType.BloodGlucose;
                this.trackerBloodGlucose.PatientOfflineGuids = mPatientOfflineGuids;
                
                this.trackerWeight.TrackerType = AHACommon.PatientTrackers.TrackerType.Weight;
                this.trackerWeight.PatientOfflineGuids = mPatientOfflineGuids;
                
                this.trackerPhysicalActivity.TrackerType = AHACommon.PatientTrackers.TrackerType.PhysicalActivity;
                this.trackerPhysicalActivity.PatientOfflineGuids = mPatientOfflineGuids;
                
                this.trackerMedications.PatientOfflineGuids = mPatientOfflineGuids;

                /** these get set later in Page_Load
                this.trackerBloodPressure.DataRange = mDataRange;
                this.trackerCholesterol.DataRange = mDataRange;
                this.trackerBloodGlucose.DataRange = mDataRange;
                this.trackerWeight.DataRange = mDataRange;
                this.trackerPhysicalActivity.DataRange = mDataRange;
                 **/
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            // We need to get our module header and assign it the patient guid
            Heart360Provider.Controls.ModuleHeader modhead = Heart360Provider.Utility.ProviderWebPage.GetModuleHeader(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT);
           
            string  patguid = AHAProvider.SessionInfo.GetProviderCurrentPatient() ;
            modhead.PatientOfflineHealthRecordGuid = patguid;

            AHAHelpContent.Patient pat = AHAHelpContent.Patient.FindByUserHealthRecordGUID(new Guid(patguid));
            if (pat != null)
            {
                modhead.PatientID = pat.UserHealthRecordID;

                if (mHealthHistoryControl != null)
                    mHealthHistoryControl.InitializePatient( ((pat.OfflinePersonGUID.HasValue) ? pat.OfflinePersonGUID.Value : Guid.Empty), ((pat.OfflineHealthRecordGUID.HasValue) ? pat.OfflineHealthRecordGUID.Value : Guid.Empty) );
                if (mNoteListControl != null)
                    mNoteListControl.PatientID = pat.UserHealthRecordID;
                if (mAlertListControl != null)
                    mAlertListControl.PatientID = pat.UserHealthRecordID;
                if (mDismissAlertPopup != null)
                {
                    mDismissAlertPopup.PatientID = pat.UserHealthRecordID;
                    mDismissAlertPopup.PatientHealthRecordGuid = pat.UserHealthRecordGUID;
                }
                if (mGroupListControl != null)
                    mGroupListControl.PatientID = pat.UserHealthRecordID;
                if (mInboxMessageList != null)
                    mInboxMessageList.PatientID = pat.UserHealthRecordID;
                if (mSentMessageList != null)
                    mSentMessageList.PatientID = pat.UserHealthRecordID;
                if (mTrashMessageList != null)
                    mSentMessageList.PatientID = pat.UserHealthRecordID;

//                AHABusinessLogic.PatientInfo pati = AHABusinessLogic.PatientInfo.GetPopulatedPatientInfo(pat) ;
//                litFullname.Text = (pati != null) ? pati.FullName : "UNKNOWN";

            }
            else
            {
//                litFullname.Text = "UNKNOWN";
                AHAProvider.SessionInfo.ResetProviderCurrentPatient();

                modhead.PatientOfflineHealthRecordGuid = string.Empty;
            }


            // Update data ranges of all sub-controls. The ddlDataRange has view state enabled.
            int newIdx = this.ddlDataRange.SelectedIndex;
            int months = int.Parse(this.ddlDataRange.SelectedValue);

            switch (months)
            {
                case 3:
                    this.mDataRange = AHACommon.DataDateReturnType.Last3Months;
                    break;

                case 6:
                    this.mDataRange = AHACommon.DataDateReturnType.Last6Months;
                    break;

                case 9:
                    this.mDataRange = AHACommon.DataDateReturnType.Last9Months;
                    break;

                case 12:
                    this.mDataRange = AHACommon.DataDateReturnType.LastYear;
                    break;

                case 24:
                    this.mDataRange = AHACommon.DataDateReturnType.Last2Years;
                    break;

                default:
                    break;
            }
            this.trackerBloodPressure.DataRange = this.trackerCholesterol.DataRange = this.trackerBloodGlucose.DataRange = mDataRange;
            this.trackerWeight.DataRange = this.trackerPhysicalActivity.DataRange = mDataRange;

        }

        protected void RefreshData_OnClick(object sender, EventArgs e)
        {
            HVManager.AllItemManager.GetItemManagersForRecordId(mPatientOfflineGuids.PersonID, mPatientOfflineGuids.RecordID).FlushCache();

            /** Not sure what this mechanism does and why.....
            if (mRefreshDataCallback != null)
                mRefreshDataCallback();
            **/
        }

        protected void DataRange_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            /** This is now handled in Page_Load
            int newIdx = this.ddlDataRange.SelectedIndex;
            int months = int.Parse(this.ddlDataRange.SelectedValue);

            switch (months)
            {
                case 3:
                    this.mDataRange = AHACommon.DataDateReturnType.Last3Months;
                    break;

                case 6:
                    this.mDataRange = AHACommon.DataDateReturnType.Last6Months;
                    break;

                case 9:
                    this.mDataRange = AHACommon.DataDateReturnType.Last9Months;
                    break;

                case 12:
                    this.mDataRange =  AHACommon.DataDateReturnType.LastYear;
                    break;

                case 24:
                    this.mDataRange = AHACommon.DataDateReturnType.Last2Years;
                    break;

                default:
                    break;
            }
            this.trackerBloodPressure.DataRange = this.trackerCholesterol.DataRange = this.trackerBloodGlucose.DataRange = mDataRange;
            this.trackerWeight.DataRange = this.trackerPhysicalActivity.DataRange = mDataRange;
             **/
        }


        //
        // ////// Web Methods //////////////////////////////////////////////////////
        //

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HealthHistoryControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mHealthHistoryControl, INIT, inputs);
        }

        [WebMethod]
        public static string NoteListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mNoteListControl, INIT, inputs);
        }

        [WebMethod]
        public static string EditNotePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditProviderNotePopup, INIT, inputs);
        }
        [WebMethod]
        public static string DeleteNotePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteNotePopup, INIT, inputs);
        }

        //
        // NOTE: the alert list control requires the dismiss alert popup and its requisite handler
        //
        [WebMethod]
        public static string AlertListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mAlertListControl, INIT, inputs);
        }

        [WebMethod]
        public static string DismissAlertPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDismissAlertPopup, INIT, inputs);
        }

        [WebMethod]
        public static string GroupListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mGroupListControl, INIT, inputs);
        }
        [WebMethod]
        public static string EditGroupPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditGroupPopup, INIT, inputs);
        }

        [WebMethod]
        public static string EditDynamicGroupPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditDynamicGroupPopup, INIT, inputs);
        }

        [WebMethod]
        public static string InboxMessageListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mInboxMessageList, INIT, inputs);
        }

        [WebMethod]
        public static string SentMessageListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mSentMessageList, INIT, inputs);
        }

        [WebMethod]
        public static string TrashMessageListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mTrashMessageList, INIT, inputs);
        }

        [WebMethod]
        public static string ReplyMessagePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mReplyMessagePopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeleteMessagePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteMessagePopup, INIT, inputs);
        }
    }
}