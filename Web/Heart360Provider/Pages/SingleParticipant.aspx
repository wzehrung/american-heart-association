﻿<%@ Page Title="Heart360 - Patient/Participant" Language="C#" MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="SingleParticipant.aspx.cs" Inherits="Heart360Provider.Pages.SingleParticipant" %>
<%@ Register Src="~/Controls/Common/HealthDataTracker.ascx" TagName="HealthTracker" TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/Common/MedicationDataTracker.ascx" TagName="MedicationTracker" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <script type="text/javascript">
        function pageLoad(sender, args) {
            window.aspAccordionSetup();
            //alert('coucou');
        }
    </script>
    <!-- asp script manager for update panel -->
    <asp:ToolkitScriptManager ID="scriptmgr" runat="server" EnablePartialRendering="true" >
    </asp:ToolkitScriptManager>
    <!-- Big Tile Column -->
    <div class="eightcol tablet-twelvecol mobile-twelvecol first">
        <!-- Single Participant Module Header (current) -->
        <div id="idModuleHeader7" class="dynamic-content-container big-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="7" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

        <div class="blue-text-area">
            <p>If you are new to Heart360®, the best place to start is by inviting patient/participants to connect with you by using the invite options located on the top right hand corner of the "My Participants" tile. Once connected, you will see summary information on your patient's/participant's recent activity within Heart360®. Also, you can click the tiles to the right for more specific information on their data entries, alerts and groups, and health history.</p>
        </div>

        <!-- Health Data accordion -->
        <div class="accordion-container no-accordion-icon">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    Health Data
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <asp:UpdatePanel ID="updatePanelTrackers" UpdateMode="Conditional" runat="server" OnUpdateComplete class="updatePanelContainer" >
                <ContentTemplate>
                    <!-- tracker controls: data range and refresh -->
                    <div class="twelvecol first">                
                        <div class="threecol mobile-twelvecol first">
                            <p>Select time period of readings displayed</p>
                            <asp:DropDownList CssClass="gradient-button" ID="ddlDataRange" runat="server" OnSelectedIndexChanged="DataRange_OnSelectedIndexChanged" AutoPostBack="true" EnableViewState="true" >
                                <asp:ListItem Text="3 Months" Value="3" />
                                <asp:ListItem Text="6 Months" Value="6" />
                                <asp:ListItem Text="9 Months" Value="9" />
                                <asp:ListItem Text="1 Year" Value="12" />
                                <asp:ListItem Text="2 Years" Value="24" />
                            </asp:DropDownList>
                        </div>
                        <div class="threecol mobile-twelvecol">
                            <p>View new readings after you logged in</p>
                            
                            <asp:Button CssClass="gradient-button" Text="Refresh Data" ID="btnRefreshData" runat="server" OnClick="RefreshData_OnClick" OnClientClick="$('#refresh-spinner').removeClass('force-hide');" />
                        </div>
                        <div id="refresh-spinner" class="sixcol mobile-twelvecol force-hide"><img src="/library/images/ajax-loader.gif" /></div>
                    </div>
                    <!-- blood pressure tracker -->
                    <uc:HealthTracker ID="trackerBloodPressure" runat="server" />
                    <!-- cholesterol tracker -->
                    <uc:HealthTracker ID="trackerCholesterol" runat="server" />
                    <!-- blood glucose tracker -->
                    <uc:HealthTracker ID="trackerBloodGlucose" runat="server" />
                    <!-- weight tracker -->
                    <uc:HealthTracker ID="trackerWeight" runat="server" />
                    <!-- exercise tracker -->
                    <uc:HealthTracker ID="trackerPhysicalActivity" runat="server" />
                    <!-- medications tracker -->                    
                    <uc:MedicationTracker ID="trackerMedications" runat="server" />

                </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>

        <!-- Health History accordion -->
        <div class="accordion-container no-accordion-icon">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    Health History
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <!-- Health History control -->
                <div id="idControlHealthHistory" class="dynamic-content-container" data-page="Pages/SingleParticipant" data-method="HealthHistoryControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Notes accordion -->
        <div class="accordion-container notes">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    Notes
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <!-- Notes List control -->
                <div id="idControlNoteList" class="dynamic-content-container" data-page="Pages/SingleParticipant" data-method="NoteListControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Alerts accordion -->
        <div class="accordion-container alerts">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    Alerts
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <!-- Alerts List control -->
                <div id="idControlAlertList" class="dynamic-content-container" data-page="Pages/SingleParticipant" data-method="AlertListControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Group Membership accordion -->
        <div class="accordion-container no-accordion-icon">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    Group Membership
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <!-- Group List control -->
                <div id="idControlGroupList" class="dynamic-content-container" data-page="Pages/SingleParticipant" data-method="GroupListControlHandler" >
                    <div class="dynamic-content-message">
                        <!-- Messages will be added here -->
                    </div>
                    <div class="dynamic-content-html">
                        <!-- Dynamic content will be loaded here -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Messages accordion -->
        <div class="accordion-container messages">
            <div class="general-trigger gradient-title ">
                <div class="action-icon"></div>
                <div class="accordion-arrow"></div>
                    Messages
            </div><!-- End .general-trigger -->
            <div class="general-content">
                <!-- need a tab container with three tabs: inbox, sent, and trash -->
                <div class="tabs-container">
                    <div class="tabs-header">
                        <div class="tab tab-inbox active" data-tabcontent="content-inbox">Inbox</div>
                        <div class="tab tab-sent" data-tabcontent="content-sent">Sent</div>
                        <div class="tab tab-trash" data-tabcontent="content-trash">Trash</div>
                    </div>
                    <div class="tabs-content">
                        <div class="tab-content content-inbox">
                            <div class="twelvecol" >
                                <!-- Inbox message list control -->
                                <div id="idControlInboxMessageList" class="dynamic-content-container" data-page="Pages/SingleParticipant" data-method="InboxMessageListControlHandler" >
                                    <div class="dynamic-content-html">
                                        <!-- Dynamic content will be loaded here -->
                                    </div>
                                    <div class="dynamic-content-message">
                                        <!-- Alert Messages will be added here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content content-sent">
                            <div class="twelvecol" >
                                <!-- Sent message list control -->
                                <div id="idControlSentMessageList" class="dynamic-content-container" data-page="Pages/SingleParticipant" data-method="SentMessageListControlHandler" >
                                    <div class="dynamic-content-html">
                                        <!-- Dynamic content will be loaded here -->
                                    </div>
                                    <div class="dynamic-content-message">
                                        <!-- Alert Messages will be added here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content content-trash">
                            <div class="twelvecol" >
                                <!-- Trash message list control -->
                                <div id="idControlTrashMessageList" class="dynamic-content-container" data-page="Pages/SingleParticipant" data-method="TrashMessageListControlHandler" >
                                    <div class="dynamic-content-html">
                                        <!-- Dynamic content will be loaded here -->
                                    </div>
                                    <div class="dynamic-content-message">
                                        <!-- Alert Messages will be added here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end messages tab -->
            </div>
        </div>

    </div><!-- End Big Tile Column -->

    <!-- Small tiles -->
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- My Participants Module Header -->
        <div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Messages Module Header -->
        <div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Alerts Module Header -->
        <div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Reports Module Header  -->
        <div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Groups Module Header  -->
        <div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Resources Module Header  -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Profile Module Header  -->
        <div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header  -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/SingleParticipant" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>