﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Heart360Provider.Pages
{
    public partial class Alerts : Heart360Provider.Utility.ProviderWebPage
    {
        // LOCAL CONTROLS
        private static Heart360Provider.Controls.AlertList      mAlertListControl;
        private static Heart360Provider.Controls.AlertRuleList  mAlertRuleListControl;
        private static Heart360Provider.Controls.AlertSettings  mSettingsControl;

        // Popup controls
        private static Heart360Provider.Controls.AddAlertRulePopup  mEditAlertRulePopup;
        private static Heart360Provider.Controls.DeleteAlertRule    mDeleteAlertRulePopup;
        private static Heart360Provider.Controls.DismissAlertPopup  mDismissAlertPopup;     // used for Dismiss All too
        

        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_ALERTS;


            //
            // Initialize module headers
            //
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_ALERTS, "Pages/Alerts" );

            try
            {
                mAlertListControl = new Controls.AlertList();
                mAlertListControl.Name = "AlertList";
                if (!mAlertListControl.Initialize())
                {
                    mAlertListControl = null;
                }
            }
            catch (Exception)
            {
                mAlertListControl = null;
            }
            try
            {
                mSettingsControl = new Controls.AlertSettings();
                mSettingsControl.Name = "AlertSettings";
                if (!mSettingsControl.Initialize())
                {
                    mSettingsControl = null;
                }
            }
            catch (Exception)
            {
                mSettingsControl = null;
            }

            try
            {
                mAlertRuleListControl = new Controls.AlertRuleList();
                mAlertRuleListControl.Name = "AlertRuleList";
                if (!mAlertRuleListControl.Initialize())
                {
                    mAlertRuleListControl = null;
                }
            }
            catch (Exception)
            {
                mAlertRuleListControl = null;
            }

            try
            {
                mEditAlertRulePopup = new Controls.AddAlertRulePopup();
                mEditAlertRulePopup.Name = "AddAlertRulePopup";
                if (!mEditAlertRulePopup.Initialize())
                {
                    mEditAlertRulePopup = null;
                }
            }
            catch (Exception)
            {
                mEditAlertRulePopup = null;
            }

            try
            {
                mDeleteAlertRulePopup = new Controls.DeleteAlertRule();
                mDeleteAlertRulePopup.Name = "DeleteAlertRulePopup";
                if (!mDeleteAlertRulePopup.Initialize())
                {
                    mDeleteAlertRulePopup = null;
                }
            }
            catch (Exception)
            {
                mDeleteAlertRulePopup = null;
            }

            try
            {
                mDismissAlertPopup = new Controls.DismissAlertPopup();
                mDismissAlertPopup.Name = "DismissAlertPopup";
                if (!mDismissAlertPopup.Initialize())
                {
                    mDismissAlertPopup = null;
                }
            }
            catch (Exception)
            {
                mDismissAlertPopup = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string AlertListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mAlertListControl, INIT, inputs);
        }

        [WebMethod]
        public static string AlertRuleListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // return AHACommon.UXControl.ControlHandler(mAlertRuleListControl, INIT, inputs);

            if (mAlertRuleListControl == null)
            {
                return "<p>Control not initialized.</p>";
            }
            string thestuff = "<p>Don't know nothin</p>" ;

            try
            {
                thestuff = AHACommon.UXControl.ControlHandler(mAlertRuleListControl, INIT, inputs);
            }
            catch (Exception ex)
            {
                thestuff = "<p>ERROR: " + ex.StackTrace + "</p>";
            }
            return thestuff;
        }

        [WebMethod]
        public static string SettingsControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mSettingsControl, INIT, inputs);
        }

        [WebMethod]
        public static string EditAlertRulePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditAlertRulePopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeleteAlertRulePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteAlertRulePopup, INIT, inputs);
        }

        [WebMethod]
        public static string DismissAlertPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDismissAlertPopup, INIT, inputs);
        }
    }
}