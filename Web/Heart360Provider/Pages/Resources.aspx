﻿<%@ Page Title="Heart360 - Provider Resources" Language="C#" MasterPageFile="~/MasterPages/Provider.Master"AutoEventWireup="true" CodeBehind="Resources.aspx.cs" Inherits="Heart360Provider.Pages.Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

	<!-- Big Tile Column -->
	<div class="eightcol tablet-twelvecol mobile-twelvecol first">
		<!-- My Participants Module Header (current) -->
		<div id="idModuleHeader6" class="dynamic-content-container big-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="6" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>

		<div class="blue-text-area">
			<p>In Heart360® you can create custom announcements and share helpful resources for easier patient/participant engagement. AHA resources on a variety of health-healthy topics are also provided for your convenience.</p>
		</div>

		<!-- My Announcements accordion -->
		<div class="accordion-container my-announcements">
			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
					<!-- Start Accordion Text ( No need for a div or span)-->
						My Announcements 
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->

			<div class="general-content">
				<div id="idAnnouncementList" class="dynamic-content-container" data-page="Pages/Resources" data-method="AnnouncementList" >
					<div class="dynamic-content-html">
						<!-- Dynamic content will be loaded here -->
					</div>
					<div class="dynamic-content-message">
						<!-- Alert Messages will be added here -->
					</div>
				</div>
			</div><!-- End .general-content -->
		</div><!-- End .accordion-container -->

		<!-- My Resources accordion -->
		<div class="accordion-container my-resources">
			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
					<!-- Start Accordion Text ( No need for a div or span)-->
					My Resources
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->
			<div class="general-content">
				<div id="idResourceList" class="dynamic-content-container" data-page="Pages/Resources" data-method="ResourceList" >
					<div class="dynamic-content-html">
						<!-- Dynamic content will be loaded here -->
					</div>
					<div class="dynamic-content-message">
						<!-- Alert Messages will be added here -->
					</div>
				</div>			
			</div>
		</div><!-- End .accordion-container -->

		<!-- AHA Resources accordion -->
		<div class="accordion-container aha-resources">
			<div class="general-trigger gradient-title ">
				<div class="action-icon"></div>
				<div class="accordion-arrow"></div>
					<!-- Start Accordion Text ( No need for a div or span)-->
					AHA Resources
					<!-- End Accordion TExt -->
			</div><!-- End .general-trigger -->
			<div class="general-content">
				<div id="idAhaResourceList" class="dynamic-content-container" data-page="Pages/Resources" data-method="AhaResourceList" >
					<div class="dynamic-content-html">
						<!-- Dynamic content will be loaded here -->
					</div>
					<div class="dynamic-content-message">
						<!-- Alert Messages will be added here -->
					</div>
				</div>			
			</div>
		</div><!-- End .accordion-container -->

	</div><!-- End Big Tile Column -->

	<!-- Small tiles -->
	<div class="fourcol tablet-twelvecol mobile-twelvecol">
		<!-- My Participants Module Header  -->
		<div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="4" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>
		<!-- Messages Module Header -->
		<div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="3" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>
		<!-- Alerts Module Header -->
		<div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="1" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>
		<!-- Reports Module Header  -->
		<div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="8" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>
		<!-- Groups Module Header  -->
		<div id="idModuleHeader2" class="dynamic-content-container small-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="2" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>
		<!-- Profile Module Header  -->
		<div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="5" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>
		<!-- Sponsors Module Header  -->
		<div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Resources" data-method="HeaderHandler" data-id="9" >
			<div class="dynamic-content-html">
				<!-- Dynamic content will be loaded here -->
			</div>
			<div class="dynamic-content-message">
				<!-- Alert Messages will be added here -->
			</div>
		</div>
	</div>

</asp:Content>
