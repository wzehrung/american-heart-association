﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Heart360Provider.Pages
{
    public partial class Sponsors : Heart360Provider.Utility.ProviderWebPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_SPONSORS;

            //
            // Initialize module headers
            //
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_SPONSORS, "Pages/Sponsors" );

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }
    }
}