﻿<%@ Page Title="Heart360 - Provider Groups" Language="C#" MasterPageFile="~/MasterPages/Provider.Master" AutoEventWireup="true" CodeBehind="Groups.aspx.cs" Inherits="Heart360Provider.Pages.Groups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <!-- Big Tile Column -->
    <div class="eightcol tablet-twelvecol mobile-twelvecol first">
        <!-- Groups Module Header (current) -->
        <div id="idModuleHeader2" class="dynamic-content-container big-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="2" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

        <!-- TODO: update explanitory text  -->
        <div class="blue-text-area">
		    <p>In Heart360® you can create custom groups for easier patient/participant management. This allows you to manage your patients/participants by their common factors - you can set alerts, run reports, and even filter your patient/participant dashboard view by group. We have included some sample groups for you; please feel free to edit or delete groups based on your own needs.</p>
        </div>

        <h3 class="blue-page-title">Groups</h3>

         <!-- Add group list control -->
        <div id="idControlGroupList" class="dynamic-content-container" data-page="Pages/Groups" data-method="GroupListControlHandler" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>

    </div>

    <!-- Small tiles -->
    <div class="fourcol tablet-twelvecol mobile-twelvecol">
        <!-- Participants Module Header  -->
        <div id="idModuleHeader4" class="dynamic-content-container small-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="4" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Messages Module Header -->
        <div id="idModuleHeader3" class="dynamic-content-container small-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="3" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Alerts Module Header -->
        <div id="idModuleHeader1" class="dynamic-content-container small-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="1" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Reports Module Header  -->
        <div id="idModuleHeader8" class="dynamic-content-container small-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="8" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Resources Module Header  -->
        <div id="idModuleHeader6" class="dynamic-content-container small-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="6" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Profile Module Header  -->
        <div id="idModuleHeader5" class="dynamic-content-container small-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="5" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
        <!-- Sponsors Module Header  -->
        <div id="idModuleHeader9" class="dynamic-content-container small-panel-container" data-page="Pages/Groups" data-method="HeaderHandler" data-id="9" >
            <div class="dynamic-content-html">
                <!-- Dynamic content will be loaded here -->
            </div>
            <div class="dynamic-content-message">
                <!-- Alert Messages will be added here -->
            </div>
        </div>
    </div>

</asp:Content>
