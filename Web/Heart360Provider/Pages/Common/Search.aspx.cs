﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Provider.Pages.Common
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnSubmitSearch(object sender, EventArgs e)
        {
            string strSearch = txtSearch.Text;
            Response.Redirect("https://www.heart.org/HEARTORG/search/searchResults.jsp?_dyncharset=ISO-8859-1&q=" + strSearch);
        }
    }
}