﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="Heart360Provider.Pages.Common.Search" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html><!--HTML5 doctype-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../library/css/style.css" />
</head>
<body>
    <form id="form1" runat="server" target="_top">
        <asp:ToolkitScriptManager ID="scriptmgr" runat="server" EnablePartialRendering="true" ></asp:ToolkitScriptManager>

        <!-- FIXME need to adapt to iframe -->
        <div class="container">
	        <div class="wrap">
		        <div id="header">
                    <div class="twelvecol first tablet-twelvecol mobile-twelvecol">
                        <div id="top-small-navigation">
                            <div class="elevencol first">
                                <asp:TextBox ID="txtSearch" runat="server" MaxLength="60" CssClass="search-input"></asp:TextBox>
                            </div>
	                        <div class="onecol first">
                                <asp:Button ID="btnSearch" runat="server" CssClass="search-button" OnClick="OnSubmitSearch" />
                            </div>
                            <asp:TextBoxWatermarkExtender ID="wmeSearch" runat="server" TargetControlID="txtSearch" WatermarkText="<%$Resources:Common,Header_Text_Search %>"></asp:TextBoxWatermarkExtender>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>        
</body>
</html>
