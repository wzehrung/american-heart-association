﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Heart360Provider.Pages
{
    public partial class Messages : Heart360Provider.Utility.ProviderWebPage
    {
        // Controls - one for each tab list
        private static Heart360Provider.Controls.MessageList mInboxMessageList;
        private static Heart360Provider.Controls.MessageList mSentMessageList;
        private static Heart360Provider.Controls.MessageList mTrashMessageList;
        private static Heart360Provider.Controls.MessageSettings mSettingsControl;

        // Popups
        private static Heart360Provider.Controls.ComposeMessagePopup   mReplyMessagePopup;
        private static Heart360Provider.Controls.DeleteMessagePopup    mDeleteMessagePopup;


        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_MESSAGES;

            //
            // Initialize module headers
            //
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_MESSAGES, "Pages/Messages" );

            //
            // Allocate and initialize Controls
            //
            try
            {
                mInboxMessageList = new Controls.MessageList();
                mInboxMessageList.Name = "MessageList";
                mInboxMessageList.Folder = Heart360Provider.Controls.MessageList.FOLDER.FOLDER_INBOX;
                if (!mInboxMessageList.Initialize())
                {
                    mInboxMessageList = null;
                }
            }
            catch (Exception)
            {
                mSentMessageList = null;
            }

            try
            {
                mSentMessageList = new Controls.MessageList();
                mSentMessageList.Name = "MessageList";
                mSentMessageList.Folder = Heart360Provider.Controls.MessageList.FOLDER.FOLDER_SENT;
                if (!mSentMessageList.Initialize())
                {
                    mSentMessageList = null;
                }
            }
            catch (Exception)
            {
                mTrashMessageList = null;
            }

            try
            {
                mTrashMessageList = new Controls.MessageList();
                mTrashMessageList.Name = "MessageList";
                mTrashMessageList.Folder = Heart360Provider.Controls.MessageList.FOLDER.FOLDER_TRASH;
                if (!mTrashMessageList.Initialize())
                {
                    mTrashMessageList = null;
                }
            }
            catch (Exception)
            {
                mTrashMessageList = null;
            }

            try
            {
                mSettingsControl = new Controls.MessageSettings();
                mSettingsControl.Name = "MessageSettings";
                if (!mSettingsControl.Initialize())
                {
                    mSettingsControl = null;
                }
            }
            catch (Exception)
            {
                mSettingsControl = null;
            }

            //
            // Allocate and initialize Popups
            //
            try
            {
                mReplyMessagePopup = new Controls.ComposeMessagePopup();
                mReplyMessagePopup.Name = "ComposeMessagePopup";
                if (!mReplyMessagePopup.Initialize())
                {
                    mReplyMessagePopup = null;
                }
            }
            catch (Exception)
            {
                mReplyMessagePopup = null;
            }

            try
            {
                mDeleteMessagePopup = new Controls.DeleteMessagePopup();
                mDeleteMessagePopup.Name = "DeleteMessagePopup";
                if (!mDeleteMessagePopup.Initialize())
                {
                    mDeleteMessagePopup = null;
                }
            }
            catch (Exception)
            {
                mDeleteMessagePopup = null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // The logic for retrieving messages in V5 is found in: provider/mail/mailhome.aspx
            // Also UserCtrls/Mail/*.ascx

        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string InboxMessageListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mInboxMessageList, INIT, inputs);
        }

        [WebMethod]
        public static string SentMessageListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mSentMessageList, INIT, inputs);
        }

        [WebMethod]
        public static string TrashMessageListControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mTrashMessageList, INIT, inputs);
        }

        [WebMethod]
        public static string ReplyMessagePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mReplyMessagePopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeleteMessagePopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteMessagePopup, INIT, inputs);
        }

        [WebMethod]
        public static string SettingsControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mSettingsControl, INIT, inputs);
        }

        // the following method is used to mark an unread message as being read
        [WebMethod]
        public static string ReadMessageHandler( string ReadMail, string UserId )
        {

            string  rez = "[{\"success\" : \"false\",\"msgunread\" : \"000\"}]";
            try
            {
                int     messageID = int.Parse( ReadMail );
                int     providerID = int.Parse(UserId);

                if (messageID > 0 && providerID > 0)
                {
                    AHAHelpContent.UserTypeDetails provDetails = new AHAHelpContent.UserTypeDetails
                    {
                        UserID = providerID,
                        UserType = AHAHelpContent.UserType.Provider
                    };

                    AHAHelpContent.Message.MarkMessageAsReadOrUnread(provDetails, messageID, true);

                    // assign newNbUnread
                    int newNbUnread = AHAHelpContent.Message.GetNumberOfUnreadMessagesForUser(provDetails, AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX);

                    rez = "[{\"success\" : \"true\",\"msgunread\" : \"" + newNbUnread.ToString() + "\"}]";
                }
            }
            catch( Exception) {}

            return rez;

            //return "[{\"success\" : \"true\",\"msgunread\" : \"999\"}]";

        }
    }
}