﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Heart360Provider.Pages
{
    public partial class Resources : Heart360Provider.Utility.ProviderWebPage
    {
        // local controls and popups
        private static Heart360Provider.Controls.AnnouncementList mAnnouncementList;
        private static Heart360Provider.Controls.ResourceList mResourceList;
        private static Heart360Provider.Controls.AhaResourceList mAhaResourceList;
        private static Heart360Provider.Controls.EditAnnouncementPopup mEditAnnouncementPopup;
        private static Heart360Provider.Controls.EditResourcePopup mEditResourcePopup;
        private static Heart360Provider.Controls.DeleteAnnouncementPopup mDeleteAnnouncementPopup;
        private static Heart360Provider.Controls.DeleteResourcePopup mDeleteResourcePopup;

        protected void Page_Init(object sender, EventArgs e)
        {
            // Assign our module id to the master page
            this.ProviderMaster.ModuleId = ProviderModules.MODULE_ID.MODULE_ID_RESOURCES;

            //
            // Initialize module headers
            //
            Heart360Provider.Utility.ProviderWebPage.InitializeModuleHeaders(this.ProviderID, ProviderModules.MODULE_ID.MODULE_ID_RESOURCES, "Pages/Resources" );


            // Allocate and initialize local controls and popups
            try
            {
                mAnnouncementList = new Controls.AnnouncementList();
                mAnnouncementList.Name = "AnnouncementList";
                if (!mAnnouncementList.Initialize())
                {
                    mAnnouncementList = null;
                }
            }
            catch (Exception)
            {
                mAnnouncementList = null;
            }
            try
            {
                mResourceList = new Controls.ResourceList();
                mResourceList.Name = "ResourceList";
                if (!mResourceList.Initialize())
                {
                    mResourceList = null;
                }
            }
            catch (Exception)
            {
                mResourceList = null;
            }
            try
            {
                mAhaResourceList = new Controls.AhaResourceList();
                mAhaResourceList.Name = "AhaResourceList";
                if (!mAhaResourceList.Initialize())
                {
                    mAhaResourceList = null;
                }
            }
            catch (Exception)
            {
                mAhaResourceList = null;
            }
            try
            {
                mEditAnnouncementPopup = new Controls.EditAnnouncementPopup();
                mEditAnnouncementPopup.Name = "EditAnnouncementPopup";
                if (!mEditAnnouncementPopup.Initialize())
                {
                    mEditAnnouncementPopup = null;
                }
            }
            catch (Exception)
            {
                mEditAnnouncementPopup = null;
            }
            try
            {
                mEditResourcePopup = new Controls.EditResourcePopup();
                mEditResourcePopup.Name = "EditResourcePopup";
                if (!mEditResourcePopup.Initialize())
                {
                    mEditResourcePopup = null;
                }
            }
            catch (Exception)
            {
                mEditResourcePopup = null;
            }
            try
            {
                mDeleteAnnouncementPopup = new Controls.DeleteAnnouncementPopup();
                mDeleteAnnouncementPopup.Name = "DeleteAnnouncementPopup";
                if (!mDeleteAnnouncementPopup.Initialize())
                {
                    mDeleteAnnouncementPopup = null;
                }
            }
            catch (Exception)
            {
                mDeleteAnnouncementPopup = null;
            }
            try
            {
                mDeleteResourcePopup = new Controls.DeleteResourcePopup();
                mDeleteResourcePopup.Name = "DeleteResourcePopup";
                if (!mDeleteResourcePopup.Initialize())
                {
                    mDeleteResourcePopup = null;
                }
            }
            catch (Exception)
            {
                mDeleteResourcePopup = null;
            }     
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //empty
        }

        [WebMethod]
        public static string HeaderHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            // Let our base class do the boilerplate work :-)
            return HeaderHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string HeaderPopupHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            return PopupHandlerImplementation(INIT, inputs);
        }

        [WebMethod]
        public static string AnnouncementList(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mAnnouncementList, INIT, inputs);
        }

        [WebMethod]
        public static string ResourceList(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mResourceList, INIT, inputs);
        }

        [WebMethod]
        public static string AhaResourceList(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mAhaResourceList, INIT, inputs);
        }

        [WebMethod]
        public static string EditAnnouncementPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditAnnouncementPopup, INIT, inputs);
        }

        [WebMethod]
        public static string EditResourcePopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mEditResourcePopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeleteAnnouncementPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteAnnouncementPopup, INIT, inputs);
        }

        [WebMethod]
        public static string DeleteResourcePopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mDeleteResourcePopup, INIT, inputs);
        }       
    }
}