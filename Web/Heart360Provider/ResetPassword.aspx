﻿<%@ Page Title="Heart360 - Reset Password" Language="C#" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Heart360Provider.ResetPassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain">

    <div class="twelvecol tablet-twelvecol mobile-twelvecol first">
        <div class="tile big red signin">
                <asp:UpdatePanel ID="updatePanel1" UpdateMode="Conditional" runat="server" >
                    <ContentTemplate>
                        <div class="top-tile">
                            <%=GetGlobalResourceObject("Common", "Text_ResetPasswordTitle")%>
                          
                            <a id="aTrackerLink1" href="" runat="server"></a>
                            <br />
                        </div>
                        <div class="middle-tile">
            	            <a id="a2" class="tile-button" href="" runat="server"></a>
                            <div class="tile-icon"></div>
                            <div class="tile-icon-color red"></div>
                            <div class="tile-value">
                                <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" Font-Size="Medium" Placeholder="New Password" CssClass="password" ></asp:TextBox>
                                <asp:TextBox ID="tbPasswordConfirm" runat="server" Font-Size="Medium" TextMode="Password" Placeholder="Confirm New Password"  CssClass="password"></asp:TextBox>
                                <asp:Button ID="btnReset" runat="server" Text="Reset" Font-Size="Medium" CssClass="gradient-button gobutton" OnClick="btnReset_OnClick" />       
                                <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="false" ValidationGroup="vgReset" Display="None" OnServerValidate="ValidatePassword"></asp:CustomValidator>
            	            </div>
                        </div>
                        <div class="bottom-tile">
                        </div>
                        <asp:Literal ID="litErrorMsg" runat="server"/>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    

    <!-- Need some spacing after the tile -->
    <br /><br /><br />

</asp:Content>
