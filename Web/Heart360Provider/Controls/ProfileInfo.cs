﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;

namespace Heart360Provider.Controls
{
    public class ProfileInfo : AHACommon.UXControl
    {

        //
        // Methods for configuring the control:
        //      * existing or new Provider/Volunteer
        //      * Provider or Volunteer
        //
        public AHAHelpContent.ProviderDetails ProviderInfo
        {
            get;
            set;
        }

        public AHAHelpContent.DbUserType ProviderType
        {
            get;
            set;
        }

            // Username and Password are for use case of creating a new user
        public string Username
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }



        override public string GetMarkup(NameValue[] inputs)
        {
            // we need to build a list of NameValue pairs for assigning current values, dropdown contents, etc..
            //List<NameValue>    nvarr = new List<NameValue>() ;

            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.String:
                    case ControlWidgetType.Email:
                    case ControlWidgetType.PhoneNumber:
                    case ControlWidgetType.Url:
                    case ControlWidgetType.Zipcode:
                        switch (cw.Name)
                        {
                            case "FirstName":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.FirstName : string.Empty;
                                break;

                            case "LastName":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.LastName : string.Empty;
                                break;

                            case "Organization":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.PracticeName : string.Empty;
                                break;

                            case "Address":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.PracticeAddress : string.Empty;
                                break;

                            case "City":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.City : string.Empty;
                                break;

                            case "Zipcode":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.Zip : string.Empty;
                                break;

                            case "PhoneNumber":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.PracticePhone : string.Empty;
                                break;

                            case "FaxNumber":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.PracticeFax : string.Empty;
                                break;

                            case "Email":
                            case "ConfirmEmail":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.Email : string.Empty ;
                                break ;

                            case "Website":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.PracticeUrl : string.Empty;
                                break;

                            case "Message":
                                cw.Value = (ProviderInfo != null) ? ProviderInfo.ProfileMessage : string.Empty;
                                break;

                            default:
                                break;
                        }
                        break;

                    case ControlWidgetType.DropDown:
                        if (cw.Name.Equals("Speciality"))
                        {
                            cw.Value = GetSpecialityDropdownContent(); 
                        }
                        else if (cw.Name.Equals("Salutation"))
                        {
                            cw.Value = GetSalutationDropdownContent();
                        }
                        else if (cw.Name.Equals("State"))
                        {
                            cw.Value = GetStateDropdownContent();
                        }
                        break;

                    default:
                        break;

                }
            }

            // call the base class to do all the string comparison
            string markup = this.PrepareMarkup();

            // Hide or show buttons based on control use
            markup = markup.Replace("$HideButtons$", (this.ProviderInfo == null) ? "force-hide" : "" );
            
            // Hide or show Speciality based on type
            markup = markup.Replace("$HideSpeciality$", (this.ProviderType == AHAHelpContent.DbUserType.DbUserTypeVolunteer) ? "soft-hide" : "");
            
            return markup;

        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Save button. Clicking Cancel does an INIT call

            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                // We have a special case when its a volunteer and the Speciality widget is hidden.
                if (ProviderType != AHAHelpContent.DbUserType.DbUserTypeVolunteer || !cw.Name.Equals("Speciality"))
                {
                    if (!cw.Validate(hashInputs[cw.Id], ref errMsg))
                    {

                        sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                        goodToGo = false;
                    }
                }
            }


            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }

            // Save or Update Provider Details.
            string successMsg = string.Empty ;
            bool isProfilePage = true;

            if (ProviderInfo != null)  // Update existing Provider information
            {
                // The DAL throws an error if it fails
                try
                {
                    /**
                    string email = hashInputs[this.MetadataHash["Email"].Id];
                    string firstname = hashInputs[this.MetadataHash["FirstName"].Id];
                     **/
                    string email = hashInputs[this.MetadataHash["Email"].Id] != null ? hashInputs[this.MetadataHash["Email"].Id] : string.Empty;
                    AHAHelpContent.ProviderDetails.UpdateProvider(
                        ProviderInfo.ProviderID,
                        email,
                        hashInputs[this.MetadataHash["FirstName"].Id] != null ? hashInputs[this.MetadataHash["FirstName"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["LastName"].Id] != null ? hashInputs[this.MetadataHash["LastName"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["Salutation"].Id] != null ? hashInputs[this.MetadataHash["Salutation"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["Organization"].Id] != null ? hashInputs[this.MetadataHash["Organization"].Id] : string.Empty,
                        string.Empty,    // Practice Email (unused)
                        hashInputs[this.MetadataHash["Address"].Id] != null ? hashInputs[this.MetadataHash["Address"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["PhoneNumber"].Id] != null ? hashInputs[this.MetadataHash["PhoneNumber"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["FaxNumber"].Id] != null ? hashInputs[this.MetadataHash["FaxNumber"].Id] : string.Empty,
                        ((ProviderInfo.UserTypeID == (int)AHAHelpContent.DbUserType.DbUserTypeProvider) ? hashInputs[this.MetadataHash["Speciality"].Id] : string.Empty),
                        string.Empty,   // Pharmacy partner (unused)
                        hashInputs[this.MetadataHash["Message"].Id] != null ? hashInputs[this.MetadataHash["Message"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["Website"].Id] != null ? hashInputs[this.MetadataHash["Website"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["City"].Id] != null ? hashInputs[this.MetadataHash["City"].Id] : string.Empty,
                        ((!string.IsNullOrEmpty(hashInputs[this.MetadataHash["State"].Id])) ? Convert.ToInt32(hashInputs[this.MetadataHash["State"].Id]) : 0),
                        hashInputs[this.MetadataHash["Zipcode"].Id] != null ? hashInputs[this.MetadataHash["Zipcode"].Id] : string.Empty,
                        ProviderInfo.AllowPatientsToSendMessages,
                        ProviderInfo.IncludeAlertNotificationInDailySummary,
                        ProviderInfo.IncludeMessageNotificationInDailySummary,
                        ProviderInfo.NotifyNewAlertImmediately,
                        ProviderInfo.NotifyNewMessageImmediately,
                        ProviderInfo.DismissAlertAfterWeeks,
                        ProviderInfo.EmailForDailySummary,
                        ProviderInfo.EmailForIndividualAlert,
                        ProviderInfo.EmailForIndividualMessage,
                        ProviderInfo.AllowAHAEmail);

                    // If we successfully updated the Provider, we need to update the values stored in ProviderInfo.
                    this.ProviderInfo = AHAHelpContent.ProviderDetails.FindByProviderID(ProviderInfo.ProviderID);

                    successMsg = "Successfully updated your profile." ;
                }
                catch
                {
                    return JsonFailure("Internal Error.");
                }
            }
            else // Create a new Provider, requires several steps
            {
                isProfilePage = false;
                try
                {
                    // All the steps are taken from V5 Provider Portal logic
                    // AHAWeb\UserCtrls\Provider\RegistrationForm.ascx.cs
                    //
                    // Step 1. Create the Provider
                    string email = hashInputs[this.MetadataHash["Email"].Id] != null ? hashInputs[this.MetadataHash["Email"].Id] : string.Empty;
                    AHAHelpContent.ProviderDetails pd = AHAHelpContent.ProviderDetails.CreateProvider( 
                        Username, 
                        email,
                        Heart360Provider.Utility.UIHelper.GetHashString(Password),
                        hashInputs[this.MetadataHash["FirstName"].Id] != null ? hashInputs[this.MetadataHash["FirstName"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["LastName"].Id] != null ? hashInputs[this.MetadataHash["LastName"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["Salutation"].Id] != null ? hashInputs[this.MetadataHash["Salutation"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["Organization"].Id] != null ? hashInputs[this.MetadataHash["Organization"].Id] : string.Empty,
                        string.Empty,   // Organization email unused
                        hashInputs[this.MetadataHash["Address"].Id] != null ? hashInputs[this.MetadataHash["Address"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["PhoneNumber"].Id] != null ? hashInputs[this.MetadataHash["PhoneNumber"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["FaxNumber"].Id] != null ? hashInputs[this.MetadataHash["FaxNumber"].Id] : string.Empty,
                        ((ProviderType == AHAHelpContent.DbUserType.DbUserTypeProvider) ? hashInputs[this.MetadataHash["Speciality"].Id] : string.Empty),
                        string.Empty,   // pharmacy partner unused
                        hashInputs[this.MetadataHash["Message"].Id] != null ? hashInputs[this.MetadataHash["Message"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["Website"].Id] != null ? hashInputs[this.MetadataHash["Website"].Id] : string.Empty,
                        hashInputs[this.MetadataHash["City"].Id] != null ? hashInputs[this.MetadataHash["City"].Id] : string.Empty,
                        ((!string.IsNullOrEmpty(hashInputs[this.MetadataHash["State"].Id])) ? Convert.ToInt32( hashInputs[this.MetadataHash["State"].Id] ) : 0),
                        hashInputs[this.MetadataHash["Zipcode"].Id] != null ? hashInputs[this.MetadataHash["Zipcode"].Id] : string.Empty,
                        ((ProviderType == AHAHelpContent.DbUserType.DbUserTypeProvider) ? true : false), //default message settings
                        true,
                        true,
                        false,
                        false,
                        2,
                        email,
                        email,
                        email,
                        true,
                        EnglishLanguage,
                        (int)ProviderType );

                    if (pd == null)
                    {
                        return JsonFailure("Internal Error.");
                    }

                    // PJB: We have to go get the ProviderDetails again because it fills out more information than CreateProvider().
                    // This is what was done in V5.
                    pd = AHAHelpContent.ProviderDetails.FindByUsername(Username);

                    // Step 2. If need be, assign a campaign to the provider
                    int? iCampaignID = AHAProvider.SessionInfo.GetCurrentCampaignID();
                    if (iCampaignID.HasValue)
                    {
                        AHAHelpContent.Campaign.UpdateCampaignForProvider( pd.ProviderID, iCampaignID.Value);
                        AHAProvider.SessionInfo.ResetCampaignSession();
                    }

                    // Step 3. Do some stuff with default groups
                    List<GroupManager.GroupInfo> lstGroups = Heart360Global.Main.GetSystemCreatedGroups();//GroupManager.GroupInfo.GetSystemCreatedGroups();

                    lstGroups.ForEach(delegate(GroupManager.GroupInfo obj)
                    {
                        if (obj.AddRemovePatientsAutomatically)
                        {
                            AHAHelpContent.PatientGroup.CreateGroup( pd.ProviderID, obj.Name, obj.Description, obj.GroupRule, AHAHelpContent.PatientGroup.GroupPatientRemoveOption.A, true);
                        }
                        else
                        {
                            AHAHelpContent.PatientGroup.CreateGroup( pd.ProviderID, obj.Name, obj.Description, obj.GroupRule, AHAHelpContent.PatientGroup.GroupPatientRemoveOption.D, true);

                        }
                    });

                    // Step 4. Send Welcome Email
                    Heart360Provider.Utility.EmailUtils.SendWelcomeEmailToProvider( pd.FirstName, pd.Email, pd.LanguageLocale, pd.ProviderCode );

                    // Step 5. If an invitation ID was provided, update the invitation to use this provider's ID, if not already set
                    string invitationId = AHAProvider.SessionInfo.GetPatientInvitationId();
                    if (!string.IsNullOrEmpty(invitationId))
                    {
                        Heart360Provider.Utility.Invitations.SetInvitationProvider(invitationId, pd.ProviderID);
                        AHAProvider.SessionInfo.SetPatientInvitationId(null);
                    }

                    // Step 6. Put new user into session state
                    //get from DB
                    int iProviderID = pd.ProviderID;
                    //set session
                    AHAProvider.SessionInfo.SetProviderSession(pd);

                    AHAHelpContent.ProviderLoginDetails.CreateLoginDetails(iProviderID);

                    successMsg = "Successfully created your account." ;

                }
                catch
                {
                    return JsonFailure("Internal Error.");
                }
            }

            return (isProfilePage) ? JsonRefresh( successMsg ) : JsonSuccess( successMsg ) ;
        }


        private string GetSalutationDropdownContent()
        {
            bool checkCurrValue = true ;
            StringBuilder sb = new StringBuilder();

            List<AHAHelpContent.ListItem> objSalutationList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.Salutation, EnglishLanguage);

            if (ProviderInfo == null || string.IsNullOrEmpty(ProviderInfo.Salutation))
            {
                sb.Append("<option selected=\"selected\" value=\"\">Select:</option>");
                checkCurrValue = false;
            }

            

            foreach (AHAHelpContent.ListItem li in objSalutationList)
            {
                if (checkCurrValue && li.ListItemCode.Equals(ProviderInfo.Salutation))
                {
                    sb.Append("<option selected=\"selected\" value=\"" + li.ListItemCode + "\">" + li.ListItemName + "</option>");
                    checkCurrValue = false;
                }
                else
                {
                    sb.Append("<option value=\"" + li.ListItemCode + "\">" + li.ListItemName + "</option>");
                }
            }

            return sb.ToString();
        }

        private string GetSpecialityDropdownContent()
        {
            bool checkCurrValue = true;
            StringBuilder sb = new StringBuilder();

            if (ProviderInfo == null || string.IsNullOrEmpty(ProviderInfo.Speciality))
            {
                sb.Append("<option selected=\"selected\" value=\"\">Select:</option>");
                checkCurrValue = false;
            }

            //not used in production (see HEART-292)
            //List<AHAHelpContent.ListItem> objSpecialityList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.PhysicianOrProviderSpecialty, EnglishLanguage);

            //short list of specialties (HEART-292)
            List<string> filteredListItems = new List<string>(new string[] { 
                "Cardiologist", "General Medicine", "Internist", "Nurse", "Nurse Practitioner", "Physicial Assistant", "Other"
            });

            foreach (string item in filteredListItems)
            {
                sb.Append("<option value='");
                sb.Append(item);
                if (checkCurrValue && item.Equals(ProviderInfo.Speciality))
                {
                    sb.Append("' selected='selected'>");
                    checkCurrValue = false;
                }
                else 
                {
                    sb.Append("'>");                
                }
                sb.Append(item);
                sb.Append("</option>");
            }

            return sb.ToString();
        }

        private string GetStateDropdownContent()
        {
            bool checkCurrValue = true;
            StringBuilder sb = new StringBuilder();

            // PJB: StateName is null oftentimes....
            if (ProviderInfo == null || ProviderInfo.StateID < 0 )
            {
                sb.Append("<option selected=\"selected\" value=\"\">Select:</option>");
                checkCurrValue = false;
            }

            List<AHAHelpContent.ListItem> objStateList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.States, EnglishLanguage);

            foreach (AHAHelpContent.ListItem li in objStateList)
            {
                if (checkCurrValue && li.ListItemID == ProviderInfo.StateID)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + li.ListItemID.ToString() + "\">" + li.ListItemCode + "</option>");
                    checkCurrValue = false;
                }
                else
                {
                    sb.Append("<option value=\"" + li.ListItemID.ToString() + "\">" + li.ListItemCode + "</option>");
                }
            }

            return sb.ToString();
        }
    }
}