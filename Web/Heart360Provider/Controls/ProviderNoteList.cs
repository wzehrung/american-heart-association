﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;


namespace Heart360Provider.Controls
{
    public class ProviderNoteList : AHACommon.UXControl
    {
        private const int NO_PATIENT = -1;
        private int mPatientID = NO_PATIENT;           // default value

        public int PatientID
        {
            set
            {
                mPatientID = value;
            }
        }

        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext provider = SessionInfo.GetProviderContext();

            List<AHAHelpContent.ProviderNote> pnList = null;

            if (provider != null && mPatientID != NO_PATIENT )
            {
                pnList = AHAHelpContent.ProviderNote.GetAllNotesForProviderAndPatient(provider.ProviderID, mPatientID);
            }

            if (pnList == null || pnList.Count < 1)
            {
                return "<p>There are no notes.</p>";
            }

            StringBuilder sb = new StringBuilder();

            // sb.Append( string.Format( "<p>There are {0} messages in this list.</p>", msgList.Count ) );

            string markup = this.PrepareMarkup();

            foreach (AHAHelpContent.ProviderNote pn in pnList)
            {
                // Do all literal replacement for this provider note and add it to the final markup
                string pnHtml = markup.Replace("$NoteId$", pn.ProviderNoteID.ToString() );
                pnHtml = pnHtml.Replace("$EditNoteDataId$", Heart360Provider.Controls.AddProviderNotePopup.FormatDataIds(string.Empty, pn.ProviderNoteID.ToString()) );
                pnHtml = pnHtml.Replace("$Title$", pn.Title);
                pnHtml = pnHtml.Replace("$Date$", pn.DateCreated.ToShortDateString());
                pnHtml = pnHtml.Replace("$Note$", pn.Note);

                sb.Append(pnHtml);
            }
            return sb.ToString();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }


    }
}