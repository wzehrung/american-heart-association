﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;
using Heart360Provider.Reports;

namespace Heart360Provider.Controls
{
    public class IndividualReport : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            StringBuilder markup = new StringBuilder(this.PrepareMarkup());
            ProviderContext provider = SessionInfo.GetProviderContext();

            //create participant scrolling checklist
            StringBuilder checklist = new StringBuilder();

            //Create a list of participants for provider
            List<Patient> allPatientsForProvider = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);
            List<PatientInfo> allPatientInfo = PatientInfo.GetPopulatedPatientInfo(allPatientsForProvider);

            if (allPatientsForProvider == null || allPatientsForProvider.Count == 0)
            {
                markup.Replace("$PatientChecklist$", "<p>There are no patients/participants.</p>");
            }
            else
            {
                foreach (Patient patient in allPatientsForProvider)
                {
                    checklist.Append("<div class='twelvecol first'><input name='individual-reportParticipant' type='checkbox' value='");
                    checklist.Append(patient.UserHealthRecordID);
                    checklist.Append("' /><label> ");
                    checklist.Append(getPatientNameById(patient));
                    checklist.Append("</label></div>");
                }

                markup.Replace("$PatientChecklist$", checklist.ToString());
            }

            //add options for groups drop-down
            StringBuilder groups = new StringBuilder();            
            List<PatientGroup> groupList = (provider != null) ? PatientGroup.FindAllByProviderID(provider.ProviderID) : null;
            if (groupList == null || groupList.Count == 0)
            {
                markup.Replace("$GroupOptions$", "<p>There are no groups.</p>");
            }
            else
            {
                groups.Append("<select name='individual-reportGroup' id='individual-reportGroup'>");
                foreach (PatientGroup group in groupList) {
                    if (!group.IsDefault && group.TotalMembers > 0)
                    {
                        groups.Append("<option value='");
                        groups.Append(group.GroupID);
                        groups.Append("'>");
                        groups.Append(group.Name);
                        groups.Append("</option>");
                    }
                }
                groups.Append("</select>");
            }

            markup.Replace("$GroupOptions$", groups.ToString());

            return markup.ToString();
        }

        private static string getPatientNameById(Patient patient)
        {
            List<Patient> patientList = new List<Patient>();
            patientList.Add(patient);
            List<PatientInfo> patientInfo = PatientInfo.GetPopulatedPatientInfo(patientList);
            if (patientInfo != null && patientInfo.Count > 0)
            {
                return patientInfo[0].FullName;
            }
            return "";
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            //this isn't used as the report download requires direct page interaction, no HOTDOG for you!
            return JsonFailure("Not implemented");
        }

        public void RunReport(NameValueCollection inputs)
        {
            //parse form inputs
            List<int> reportParticipants = new List<int>();
            bool groupReport = false, individualReport = false, multiGraph = false, cGroup = false, bgGroup = false, wGroup = false, bpGroup = false;
            HashSet<string> reportFlags = new HashSet<string>();
            string reportHeading = "", reportPeriod = "", yAxis = "", reportTitle = "", reportTimefame = "";
            int reportGroup = -1;
            AHACommon.DataDateReturnType dateRange = DataDateReturnType.AllData;

            if(!string.IsNullOrEmpty(inputs["individual-tabSelected"])) //string
            {
                string value = inputs["individual-tabSelected"];
                if(value.Equals("1")) {
                    individualReport = true;                
                } else if (value.Equals("2")) {
                    groupReport = true;                
                } else {
                    PDFHelper.CreateErrorPDF();
                }
            }
            if(!string.IsNullOrEmpty(inputs["individual-reportParticipant"])) //checkbox group
            {
                var values = inputs["individual-reportParticipant"].Split(',');
                foreach (string value in values)
                {
                    int iReportParticipant;
                    if (Int32.TryParse(value, out iReportParticipant))
                    {
                        reportParticipants.Add(iReportParticipant);
                    }
                }
            }
            if(!string.IsNullOrEmpty(inputs["individual-reportGroup"])) //drop-down
            {
                string value = inputs["individual-reportGroup"];
                int iReportGroup;
                if (Int32.TryParse(value, out iReportGroup))
                {
                    reportGroup = iReportGroup;
                }
            }
            if(!string.IsNullOrEmpty(inputs["individual-reportPeriod"])) //string
            {
                reportPeriod = inputs["individual-reportPeriod"];
            }
            if(!string.IsNullOrEmpty(inputs["individual-addMultiGraph"])) //checkbox
            {
                multiGraph = true;
            }
            if(!string.IsNullOrEmpty(inputs["individual-bpTable"])) //checkbox
            {
                reportFlags.Add("bpTable");
            }
            if(!string.IsNullOrEmpty(inputs["individual-bpChart"])) //checkbox
            {
                reportFlags.Add("bpChart");
            }
            if(!string.IsNullOrEmpty(inputs["individual-bgTable"])) //checkbox
            {
                reportFlags.Add("bgTable");
            }
            if(!string.IsNullOrEmpty(inputs["individual-bgChart"])) //checkbox
            {
                reportFlags.Add("bgChart");
            }
            if(!string.IsNullOrEmpty(inputs["individual-wTable"])) //checkbox
            {
                reportFlags.Add("wTable");
            }
            if(!string.IsNullOrEmpty(inputs["individual-wChart"])) //checkbox
            {
                reportFlags.Add("wChart");
            }
            if(!string.IsNullOrEmpty(inputs["individual-paTable"])) //checkbox
            {
                reportFlags.Add("paTable");
            }
            if(!string.IsNullOrEmpty(inputs["individual-paChart"])) //checkbox
            {
                reportFlags.Add("paChart");
            }
            if(!string.IsNullOrEmpty(inputs["individual-cTable"])) //checkbox
            {
                reportFlags.Add("cTable");
            }
            if(!string.IsNullOrEmpty(inputs["individual-cChart"])) //checkbox
            {
                reportFlags.Add("cChart");
            }
            if(!string.IsNullOrEmpty(inputs["individual-yAxis"])) //drop-down
            {
                yAxis = inputs["individual-yAxis"];
            }
            if(!string.IsNullOrEmpty(inputs["individual-bpGroup"])) //checkbox
            {
                bpGroup = true;
            }
            if(!string.IsNullOrEmpty(inputs["individual-cGroup"])) //checkbox
            {
                cGroup = true;
            }
            if(!string.IsNullOrEmpty(inputs["individual-wGroup"])) //checkbox
            {
                wGroup = true;
            }
            if(!string.IsNullOrEmpty(inputs["individual-bgGroup"])) //checkbox
            {
                bgGroup = true;
            }


            // ***** setup report *****

            //patient list
            List<AHAHelpContent.Patient> patientList = null;
            if (individualReport)
            {
                patientList = new List<AHAHelpContent.Patient>();
                foreach (int id in reportParticipants)
                {
                    patientList.Add(AHAHelpContent.Patient.FindByUserHealthRecordID(id));
                }
            }
            else if (groupReport)
            {
                patientList = AHAHelpContent.Patient.FindAllByGroupID(reportGroup);
            }

            //y-axis graph type
            Graph.GraphGroupType yAxisGroups = 0;
            switch (yAxis)
            {
                case "bp":
                    yAxisGroups = Graph.GraphGroupType.PressureGroup;
                    break;
                case "c":
                    yAxisGroups = Graph.GraphGroupType.CholesterolGroup;
                    break;
                case "w":
                    yAxisGroups = Graph.GraphGroupType.WeightGroup;
                    break;
                case "bg":
                    yAxisGroups = Graph.GraphGroupType.GlucoseGroup;
                    break;
            }

            //multi graphs to plot
            Graph.GraphGroupType multiGraphsToPlot = 0;
            if (cGroup)
            {
                multiGraphsToPlot = multiGraphsToPlot | Graph.GraphGroupType.CholesterolGroup;
            }
            if (bgGroup)
            {
                multiGraphsToPlot = multiGraphsToPlot | Graph.GraphGroupType.GlucoseGroup;
            }
            if (bpGroup)
            {
                multiGraphsToPlot = multiGraphsToPlot | Graph.GraphGroupType.PressureGroup;
            }
            if (wGroup)
            {
                multiGraphsToPlot = multiGraphsToPlot | Graph.GraphGroupType.WeightGroup;
            }

            switch (reportPeriod)
            {
                case "3m":
                    dateRange = DataDateReturnType.Last3Months;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_MultipleDataTypeGraphForLast3Months").ToString();
                    reportTimefame = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Provider_Report_Duration_3Months").ToString();
                    break;
                case "6m":
                    dateRange = DataDateReturnType.Last6Months;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_MultipleDataTypeGraphForLast6Months").ToString();
                    reportTimefame = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Provider_Report_Duration_6Months").ToString();
                    break;
                case "9m":
                    dateRange = DataDateReturnType.Last9Months;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_MultipleDataTypeGraphForLast9Months").ToString();
                    reportTimefame = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Provider_Report_Duration_9Months").ToString();
                    break;
                case "1y":
                    dateRange = DataDateReturnType.LastYear;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_MultipleDataTypeGraphForLastYear").ToString();
                    reportTimefame = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Provider_Report_Duration_1Year").ToString();
                    break;
                case "2y":
                    dateRange = DataDateReturnType.Last2Years;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_MultipleDataTypeGraphForLast2Years").ToString();
                    reportTimefame = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Provider_Report_Duration_2Years").ToString();
                    break;
                default:
                    PDFHelper.CreateErrorPDF();
                    return;
            }

            if (multiGraph)
            {
                reportTitle = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_MultipleDataTypeGraph").ToString();
                PDFHelper.CreateMultipleDataTypeGraphReportPDF(patientList, multiGraphsToPlot, yAxisGroups, dateRange, reportTitle, reportHeading);
            }
            else
            {
                reportTitle = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Reports_IndividualPatientReport").ToString();
                PDFHelper.CreateIndividualPatientReportPDF(dateRange, patientList, reportTitle, reportTimefame, reportFlags);
            }
        }
    }
}