﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class DeleteMessagePopup : AHACommon.UXControl
    {
        private int     mMessageID ;
        private string  mFolderType;

        public static string FormatDataIds(string msgFolder, string strMessageID)
        {
            return msgFolder + "," + strMessageID;
        }

        override public string GetMarkup(NameValue[] inputs)
        {
            // no widgets, all read-only data

            ProviderContext provider = SessionInfo.GetProviderContext();
            string          markup = UXControl.GenericHtmlErrorMessage ;

            // Process input parameters, find message, etc..
            string strMessageId = string.Empty;
            string strFolderType = string.Empty;

            if (inputs.Length > 0 && inputs[0].name.Equals("id") && !string.IsNullOrEmpty(inputs[0].value))
            {
                char[] delimiter = { ',' };
                string[] inIds = inputs[0].value.Split(delimiter);

                if (inIds.Length > 1)
                {
                    strFolderType = inIds[0];
                    strMessageId = inIds[1];
                }
            }

            int msgID = Convert.ToInt32(strMessageId);
            AHAHelpContent.UserTypeDetails utd = new UserTypeDetails();

            utd.UserID = provider.ProviderID;
            utd.UserType = UserType.Provider;

            AHAHelpContent.Message msg = AHAHelpContent.Message.GetMessageDetails(utd, strFolderType, msgID);
            Heart360Provider.Utility.MessageInfo msgInfo = (msg != null) ? Heart360Provider.Utility.MessageInfo.GetMessageInfoObject(msg, utd, strFolderType) : null;

            if (msgInfo != null)
            {
                mMessageID = msgID;
                mFolderType = strFolderType;

                // Do markup
                markup = this.PrepareMarkup();
                markup = markup.Replace("$Subject$", msgInfo.Subject);
                markup = markup.Replace("$Delete$", (AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN.Equals( strFolderType)) ? "permanently delete" : "delete" );
            }
            
            return markup;

        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Delete button.

            ProviderContext provider = SessionInfo.GetProviderContext();
            UserTypeDetails utd = new UserTypeDetails() ;

            utd.UserType = UserType.Provider ;
            utd.UserID = SessionInfo.GetProviderContext().ProviderID ;

            try
            {
                Message objMessage = Message.GetMessageDetails(utd, mFolderType, mMessageID);
                if (AHAHelpContent.Message.DeleteMessage(utd, mMessageID))
                {
                    // remove from health vault.
                    HVManager.MessageDataManager.MessageItem objMessageItem = Heart360Provider.Utility.MessageWrapper.GetMessageByIdForProvider( provider.ProviderID, mMessageID);
                    if (objMessageItem != null)
                    {
                        AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objMessage.PatientID);
                        Heart360Provider.Utility.MessageWrapper.DeleteMessage(objMessageItem.ThingKey, objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                    }
                }
                return JsonRefresh("DELETE MESSAGE", "Successfully deleted message.");
            }
            catch (Exception) { }

            return JsonFailure("Error deleting message.");
        }
    }
}