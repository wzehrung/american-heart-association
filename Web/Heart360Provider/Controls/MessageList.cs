﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class MessageList : AHACommon.UXControl
    {
        // Note: use AHACommon.AHADefs.FolderType for mail folder types

        public enum FOLDER
        {
            FOLDER_INBOX,
            FOLDER_SENT,
            FOLDER_TRASH
        }

        private FOLDER mFolder = FOLDER.FOLDER_INBOX;

        public FOLDER Folder
        {
            set { mFolder = value; }
        }

            // Note: Message List can display all messages for a provider, or only those pertaining to a single participant of a provider
        private const int NO_PATIENT = -1;
        private int mPatientID = NO_PATIENT;           // default value

        public int PatientID
        {
            set
            {
                mPatientID = value;
            }
        }

        private string mWebMethodPage = "Pages/Messages";   // default
        public string WebMethodPage
        {
            set { mWebMethodPage = value; }
        }

        //---------------------

        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext                 provider = SessionInfo.GetProviderContext();
            string                          folder = (mFolder == FOLDER.FOLDER_INBOX) ? AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX : ((mFolder == FOLDER.FOLDER_SENT) ? AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_SENT_MESSAGES :AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_TRASH_CAN);
            AHAHelpContent.UserTypeDetails  utd = new UserTypeDetails();

            utd.UserType = UserType.Provider;
            utd.UserID = provider.ProviderID;

            List<AHAHelpContent.Message>    msgList = null ;

            if (provider != null)
            {
                if (mPatientID == NO_PATIENT)
                {
                    msgList = AHAHelpContent.Message.GetAllMessagesByFolderForUser(utd, folder);
                }
                else
                {
                    List<AHAHelpContent.Message> msgListForPatient = AHAHelpContent.Message.GetAllMessagesByFolderForUser(utd, folder);
                    msgList = msgListForPatient.Where(m => m.PatientID == mPatientID).ToList<AHAHelpContent.Message>() ;
                }
            }

            if (msgList == null || msgList.Count < 1)
            {
                return "<p>There are no messages.</p>";
            }

            StringBuilder sb = new StringBuilder();

            // sb.Append( string.Format( "<p>There are {0} messages in this list.</p>", msgList.Count ) );

            string markup = this.PrepareMarkup();

            msgList.Reverse();  // make list from most recent to oldest.

            foreach (AHAHelpContent.Message msg in msgList)
            {
                Heart360Provider.Utility.MessageInfo    minfo = Heart360Provider.Utility.MessageInfo.GetMessageInfoObject( msg, utd, folder ) ;

                // Do all literal replacement for this patient group and add it to the final markup
                string  msgid = msg.MessageID.ToString() ;
                string pgHtml = markup.Replace("$DeleteDataIds$", Heart360Provider.Controls.DeleteMessagePopup.FormatDataIds(folder, msgid));
                pgHtml = pgHtml.Replace( "$ReplyDataIds$", Heart360Provider.Controls.ComposeMessagePopup.FormatDataIds( folder, msgid, string.Empty ) ) ;

                pgHtml = pgHtml.Replace("$Subject$", minfo.Subject );

                if( minfo.SentByProvider )
                {
                    pgHtml = pgHtml.Replace("$From$", minfo.To + " | " + minfo.SentDate.ToShortDateString() ) ;
                }
                else
                {
                    pgHtml = pgHtml.Replace("$From$", minfo.From + " | " + minfo.SentDate.ToShortDateString() );
                }

                /**
                if( mFolder == FOLDER.FOLDER_INBOX )
                {
                    pgHtml = pgHtml.Replace("$From$", minfo.From );
                }
                else if( mFolder == FOLDER.FOLDER_SENT )
                {
                    pgHtml = pgHtml.Replace("$From$", minfo.To ) ;
                }
                else
                {
                   // TODO: Need to determine if the message was deleted from the Inbox or Sent folder.

                }
                 **/

                pgHtml = pgHtml.Replace("$Content$", minfo.MessageBody.Replace( "\n", "<br />"));

                if (mFolder == FOLDER.FOLDER_INBOX && !minfo.IsRead)
                {
                    string onclick = "onclick=\"messageUpdater('" + minfo.MessageID.ToString() + "','" + provider.ProviderID.ToString() + "','idTriggerUnreadMessage" + minfo.MessageID.ToString() + "');\"" ;

                    pgHtml = pgHtml.Replace("$UnreadMessageHandler$", onclick );
                    pgHtml = pgHtml.Replace("$UnreadMessageId$", ("id=\"idTriggerUnreadMessage" + minfo.MessageID.ToString() + "\"") );
                }
                else
                {
                    pgHtml = pgHtml.Replace("$UnreadMessageHandler$", string.Empty);
                    pgHtml = pgHtml.Replace("$UnreadMessageId$", string.Empty);
                }

                sb.Append(pgHtml);
            }
            return sb.ToString().Replace( "$WebMethodPage$", mWebMethodPage ) ;
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }





    }
}