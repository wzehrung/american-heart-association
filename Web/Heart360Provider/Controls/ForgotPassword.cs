﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class ForgotPassword : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            StringBuilder markup = new StringBuilder(this.PrepareMarkup());
            markup.Replace("$Username$", "");
            markup.Replace("$EmailAddress$", "");
            return markup.ToString();
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                //check that we have an entry, and determine which to use
                string username = hashInputs["txtUsername"];
                string emailAddress = hashInputs["txtEmailAddress"];

                if (!string.IsNullOrEmpty(username))
                {
                    if (ResetPasswordFromUsername(username))
                    {
                        return JsonSuccess("FORGOT PASSWORD", "Message sent.");
                    }
                }
                else if (!string.IsNullOrEmpty(emailAddress))
                {
                    if (ResetPasswordFromEmail(emailAddress))
                    {
                        return JsonSuccess("FORGOT PASSWORD", "Message sent.");
                    }
                }
                else
                {
                    return JsonFailure("Username or Email Address must be specified.");
                }

            }
            catch (Exception)
            {
                //the dictionary can throw exceptions
            }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }

        protected bool ResetPasswordFromUsername(string username)
        {
            try
            {
                Guid guid = Guid.NewGuid();
                ProviderDetails objPD = ProviderDetails.FindByUsername(username);

                // Note: validation has already checked that objPD should exist. This is just extra logic
                if (objPD != null)
                {
                    string strAppVirtualPath = (HttpRuntime.AppDomainAppVirtualPath != "/") ? HttpRuntime.AppDomainAppVirtualPath : string.Empty;
                    /**
                    string strLink = string.Format("{0}{1}?r={2}", 
                        GRCBase.UrlUtility.ServerRootUrl, 
                        strAppVirtualPath + AppSettings.Url.ResetPassword, 
                        GRCBase.BlowFish.EncryptString(guid.ToString()));
                     **/
                    string strLink = string.Format("{0}{1}?r={2}",
                        AppSettings.Url.SiteUrl,
                        AppSettings.Url.ResetPassword,
                        GRCBase.BlowFish.EncryptString(guid.ToString()));

                    ResetPasswordLog.CreateLog(guid, objPD.ProviderID);

                    Heart360Provider.Utility.EmailUtils.SendForgotPasswordEmail(string.Empty, objPD.Email, strLink, objPD.LanguageLocale);
                }
                //don't let them know if the username does not exist
                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }

        protected bool ResetPasswordFromEmail(string email)
        {
            try
            {
                Guid guid = Guid.NewGuid();
                ProviderDetails objPD = ProviderDetails.FindByEmail(email);

                // Note: validation has already checked that objPD should exist. This is just extra logic
                if (objPD != null)
                {
                    string strUsername = objPD.UserName;
                    ResetPasswordLog.CreateLog(guid, objPD.ProviderID);
                    Heart360Provider.Utility.EmailUtils.SendForgotUsernameEmail(string.Empty, objPD.Email, strUsername, objPD.LanguageLocale);
                }
                //don't let them know if the email does not exist
                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }
    }
}