﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Provider.Controls
{
    public partial class ModuleSummary : System.Web.UI.UserControl
    {
        // This is an enum to specify the "state" of the ModuleSummary.
        // The state will assign the proper color scheme and sometimes the secondary text.
        public enum SUMMARY_STATE
        {
            SUMMARY_STATE_NONE,                 // grey scheme (default)
            SUMMARY_STATE_NEUTRAL,              // blue scheme, 
            SUMMARY_STATE_EXCELLENT,            // green scheme, secondary text assigned
            SUMMARY_STATE_NEEDS_IMPROVEMENT,    // orange scheme, secondary text assigned
            SUMMARY_STATE_WARNING               // red scheme, secondary text assigned
        } ;

        // This enumeration is used to assign the proper css classes
        public enum SUMMARY_LOCATION
        {
            SUMMARY_LOCATION_DASHBOARD = 0,
            SUMMARY_LOCATION_MODULE,               // it is in the side-bar of (non-current) modules
            SUMMARY_LOCATION_MODULE_CURRENT      // it is the current module
        };

        private Heart360Provider.ProviderModules.MODULE_ID mModuleID;
        private bool mEnableAddButton;
        private bool mRefresh;
        private string mNavigationUrl;
        private string mTrackerType;
        private SUMMARY_STATE mSummaryState;
        private SUMMARY_LOCATION mSummaryLocation;

        // the module controller wants to potentially be referenced by both the Summary and Detail views.
        // so this can be new'd either internally or externally
/** TODO
        private Heart360Provider.ModuleControllerInterface mModuleController;
**/

        public ModuleSummary()
            : base()
        {
            mSummaryState = ModuleSummary.SUMMARY_STATE.SUMMARY_STATE_NONE;
            mSummaryLocation = ModuleSummary.SUMMARY_LOCATION.SUMMARY_LOCATION_DASHBOARD;
            mNavigationUrl = string.Empty;
            mEnableAddButton = true;
            mModuleID = Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN;
            mTrackerType = string.Empty;
            /* mModuleController = null; */
        }

        //
        // Attributes
        //

        public int ModuleIdAsInt      // need this so (html/asp) markup can specify modules
        {
            set
            {
                mModuleID = Heart360Provider.ProviderModules.GetModuleIdFromInt(value);
            }
            get
            {
                return (int)mModuleID;
            }
        }

        public Heart360Provider.ProviderModules.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_ALERTS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_GROUPS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_MESSAGES:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_RESOURCES:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_DASHBOARD:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_REPORTS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_SPONSORS:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        // Note that TrackerType will determine the icon of the ModuleSummary, via CSS
        public string TrackerType
        {
            get { return mTrackerType; } // string sTracker = litTrackerBegin.Text; return sTracker; }
            set { mTrackerType = value; }   //  litTracker.Text = value; }
        }

        public string Title
        {
            set { litTitle.Text = value; }
        }

        public string PrimaryText
        {
            set { litModuleData.Text = value; }
        }

        public string PopupLink1
        {
            set { litPopupLink1.Text = value; }
        }

        public string PopupLink2
        {
            set { litPopupLink2.Text = value; }
        }

        public bool RefreshControl
        {
            get { return mRefresh; }
            set { mRefresh = value; }
        }

        public string NavigationUrl
        {
            get { return mNavigationUrl; }
            set { mNavigationUrl = value; }
        }

        public SUMMARY_STATE SummaryState
        {
            get { return mSummaryState; }
            set
            {
                switch (value)
                {
                    case SUMMARY_STATE.SUMMARY_STATE_NONE:
                        mSummaryState = value;
// Prov                        SecondaryTextNoColor = ".";      // we need at least one space character for css styling
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_NEUTRAL:
                        mSummaryState = value;
                        // SecondaryText is set by instance
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_EXCELLENT:
                        mSummaryState = value;
/** Prov
                        SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_LastReading").ToString() + ": ";
                        SecondaryTextColored = "<span class=\"status has-tip tip-left\" data-tooltip=\"\" title=\"" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Color_Indication_Green").ToString() + "\">" +
                                               GetGlobalResourceObject("Tracker", "Tracker_Text_Excellent").ToString() + "[?]</span>";
 **/
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT:
                        mSummaryState = value;
/** Prov
                        SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_LastReading").ToString() + ": ";
                        SecondaryTextColored = "<span class=\"status has-tip tip-left\" data-tooltip=\"\" title=\"" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Color_Indication_Orange").ToString() + "\">" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Text_NeedsImprovement").ToString() + "[?]</span>";
 **/
                        break;

                    case SUMMARY_STATE.SUMMARY_STATE_WARNING:
                        mSummaryState = value;
/** Prov
                        SecondaryTextNoColor = GetGlobalResourceObject("Tracker", "Tracker_Text_LastReading").ToString() + ": ";
                        SecondaryTextColored = "<span class=\"status has-tip tip-left\" data-tooltip=\"\" title=\"" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Color_Indication_Red").ToString() + "\">" +
                                                GetGlobalResourceObject("Tracker", "Tracker_Text_Warning").ToString() + "[?]</span>";
 **/
                        break;

                    default:
                        break;

                }
            }
        }

        public SUMMARY_LOCATION SummaryLocation
        {
            get { return mSummaryLocation; }
            set { mSummaryLocation = value; }
        }
/** Prov
        public Heart360Web.ModuleControllerInterface ModuleController
        {
            set { mModuleController = value; }
            get { return mModuleController; }
        }
**/
        // This is the name of the css color code class that Push Design and Tectura agreed to use.
        // It is used, for example, to color the border around the icon.
        private string CssColorCode
        {
            get
            {
                string retval = string.Empty;

                switch (mSummaryState)
                {
                    case SUMMARY_STATE.SUMMARY_STATE_NEUTRAL:
                        retval = "blue";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_EXCELLENT:
                        retval = "green";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_NEEDS_IMPROVEMENT:
                        retval = "orange";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_WARNING:
                        retval = "red";
                        break;
                    case SUMMARY_STATE.SUMMARY_STATE_NONE:
                    default:
                        retval = "blue"; // JM EDIT added default blue to get colors to show up
                        break; // use default return value
                }
                return retval;
            }
        }

        private string CssClass
        {
            get
            {
                string retval = string.Empty;
                switch (mSummaryLocation)
                {
                    case SUMMARY_LOCATION.SUMMARY_LOCATION_DASHBOARD:

                    case SUMMARY_LOCATION.SUMMARY_LOCATION_MODULE:
                        retval = "tile " + CssColorCode;
                        break;

                    case SUMMARY_LOCATION.SUMMARY_LOCATION_MODULE_CURRENT:
                        retval = "tile " + CssColorCode + " big";
                        break;

                    default:
                        break;
                }
                return retval;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // PJB 3.10.2014 - not sure why there is not some logic around whether or not it is a post-back
            // PJB 4.21.2014 - going to put some post-back logic and see what happens
            if (!this.Page.IsPostBack)
            {
                AHAProvider.ProviderContext provider = AHAProvider.SessionInfo.GetProviderContext();    // this will be used below on per-module basis
                
                this.NavigationUrl = AppSettings.Url.ProviderModule(mModuleID);
                switch (mModuleID)
                {
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_ALERTS:
                        {
                            this.Title = GetGlobalResourceObject("Provider", "Title_Alerts").ToString();

                            //set module text
                            if (provider != null)
                            {
                                List<AHAHelpContent.Alert> alist = AHAHelpContent.Alert.FindByProviderID(provider.ProviderID);
                                PrimaryText = alist.Count.ToString();
                            }
                            else
                            {
                                PrimaryText = "0";
                            }

                            //add alert rule  popup link
                            PopupLink1 = CreateActionLink(GetGlobalResourceObject("Provider", "Add_Alert_Rule_Popup_Title").ToString(), false, "Pages/Alerts", "AddAlertRulePopup");

                            // TODO: construct a controller if we have not been assigned one
                        }
                        break;

                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_GROUPS:
                        {
                            this.Title = GetGlobalResourceObject("Provider", "Title_Groups").ToString();

                            //set module text
                            if (provider != null)
                            {
                                List<AHAHelpContent.PatientGroup> groupList = AHAHelpContent.PatientGroup.FindAllByProviderID(provider.ProviderID);
                                PrimaryText = groupList.Count.ToString();
                            }
                            else
                            {
                                PrimaryText = "0";
                            }

                            //create group  popup link
                            PopupLink1 = CreateActionLink(GetGlobalResourceObject("Provider", "Create_Group_Popup_Title").ToString(), false, "Pages/Groups", "CreateGroupPopup");

                            // TODO: construct a controller if we have not been assigned one (if we end up using a controller).
                        }
                        break;

                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_MESSAGES:
                        {
                            this.Title = GetGlobalResourceObject("Provider", "Title_Messages").ToString();

                            // set module text
                            PrimaryText = (provider != null) ? AHAHelpContent.Message.GetNewMessageCountForProvider(provider.ProviderID).ToString() : "0" ;

                            //compose message popup link
                            PopupLink1 = CreateActionLink(GetGlobalResourceObject("Provider", "Compose_Message_Popup_Title").ToString(), false, "Pages/Messages", "ComposeMessagePopup");
                        }
                        break;

                    case ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS:
                        {
                            this.Title = GetGlobalResourceObject("Provider", "Title_Participants").ToString();

                            // set module text
                            if (provider != null)
                            {
                                List<AHAHelpContent.PatientProviderInvitation> ilist = AHAHelpContent.PatientProviderInvitation.FindPendingInvitationsByAcceptedProvider(provider.ProviderID, AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Pending);
                                List<AHAHelpContent.Patient> plist = AHAHelpContent.Patient.FindAllByProviderID( provider.ProviderID ) ;
                                PrimaryText = ilist.Count.ToString() + " | " + plist.Count.ToString();
                            }
                            else
                            {
                                PrimaryText = "0 | 0";
                            }

                            //TODO - invite by email popup link
                            PopupLink1 = CreateActionLink(GetGlobalResourceObject("Provider", "Invite_By_Email_Popup_Title").ToString(), false, "Pages/Participants", "InviteByEmailPopup");
                            //TODO - invite by print popup link
                            PopupLink2 = CreateActionLink(GetGlobalResourceObject("Provider", "Invite_By_Print_Popup_Title").ToString(), true, "Pages/Participants", "InviteByPrintPopup");

                            // TODO: construct a controller if we have not been assigned one
                        }
                        break;

                    case ProviderModules.MODULE_ID.MODULE_ID_PROFILE:
                        this.Title = GetGlobalResourceObject("Provider", "Title_Profile").ToString();
                        // TODO: construct a controller if we have not been assigned one
                        break;

                    case ProviderModules.MODULE_ID.MODULE_ID_RESOURCES:
                        {
                            this.Title = GetGlobalResourceObject("Provider", "Title_Resources").ToString();

                            // Set module text
                            if (provider != null)
                            {
                                List<AHAHelpContent.Announcement> alist = AHAHelpContent.Resource.FindAllAnnouncements(true, AHACommon.UXControl.EnglishLanguage, provider.ProviderID);
                                List<AHAHelpContent.Resource> rlist = AHAHelpContent.Resource.FindAllResources(true, AHACommon.UXControl.EnglishLanguage, provider.ProviderID);

                                PrimaryText = alist.Count.ToString() + " | " + rlist.Count.ToString();
                            }
                            else
                            {
                                PrimaryText = "0 | 0";
                            }

                            //TODO - create announcement popup link
                            PopupLink1 = CreateActionLink(GetGlobalResourceObject("Provider", "Create_Announcement_Popup_Title").ToString(), false, "Pages/Resources", "CreateAnnouncementPopup");

                            //TODO - create resource popup link
                            PopupLink2 = CreateActionLink(GetGlobalResourceObject("Provider", "Create_Resource_Popup_Title").ToString(), true, "Pages/Resources", "CreateResourcePopup");

                            // NOTE: this may require adding icons and tool-tips as was done for patient portal connection center
                            // TODO: construct a controller if we have not been assigned one
                        }
                        break;

                    case ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT:
                        this.Title = "TBD";
                        // NOTE: the title for this is the patient name. Has to be set programmatically.
                        // TODO: construct a controller if we have not been assigned one
                        break;

                    case ProviderModules.MODULE_ID.MODULE_ID_REPORTS:
                        this.Title = GetGlobalResourceObject("Provider", "Title_Reports").ToString();
                        break;

                    case ProviderModules.MODULE_ID.MODULE_ID_SPONSORS:
                        this.Title = GetGlobalResourceObject("Provider", "Title_Sponsors").ToString();
                        break;


                    case ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN:
                    default:
                        this.Title = "Unknown";
                        this.PrimaryText = "NO DATA";
                        break;
                }

                // If there is a module controller, initialize it
                /** Prov
                    if (mModuleController != null)
                    {
                        mModuleController.InitializeSummary(this);
                    }
                **/

                // Need to dynamically assign the appropriate class to the tile
                litTrackerBegin.Text = "<div id='" + TrackerType + "'  class='" + CssClass + " " + TrackerType + "' >";
                litTrackerEnd.Text = "</div>";

                // Set the URLs for each tracker based on the value given to the control on the dashboard page
                aTrackerLink1.HRef = mNavigationUrl;
                aTrackerLink2.HRef = mNavigationUrl;
            }
        }

        private string CreateActionLink(string popupText, bool secondAction, string contentPage, 
            string contentMethod, string customClass = null)
        {
            StringBuilder sb = new StringBuilder("<div class='action");
            if (secondAction)
            {
                sb.Append("-two");
            }
            sb.Append(" popuplink' data-page='");
            sb.Append(contentPage);
            sb.Append("' data-method='");
            sb.Append(contentMethod);
            if (customClass != null)
            {
                sb.Append("' data-customclass='");
                sb.Append(customClass);
            }
            sb.Append("'>");
            sb.Append(popupText);
            sb.Append("</div>");
            return sb.ToString();
        }
    }
}