﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class InviteByPrintPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            this.Initialize();  // for development only

            ProviderContext provider = SessionInfo.GetProviderContext();

            AHAHelpContent.ProviderDetails provDetails = (provider != null) ? AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID) : null;

            string logopath = GRCBase.UrlUtility.ServerRootUrl + "/library/images/logo.png";

            StringBuilder markup = new StringBuilder(this.PrepareMarkup());

            markup.Replace("$LogoPath$", logopath);
            markup.Replace("$ProviderName$", provDetails.FormattedName);
            markup.Replace("$EntryUrl$", Heart360Provider.AppSettings.Url.ProviderInvitationEntrypoint);  // Heart360Provider.Utility.GlobalPaths.GetInvitationLink(string.Empty, false));
            markup.Replace("$ProviderCode$", provDetails.ProviderCode);

            return markup.ToString();
        }


        public override string ProcessFormValues(NameValue[] inputs)
        {
            // All the data is read-only. If we are here, the print button should have been pushed.

            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            if (hashInputs.ContainsKey(UXControl.InputsKeyword_Print))
            {
                if (hashInputs[UXControl.InputsKeyword_Print].ToLower().Equals(UXControl.InputsValue_True))
                {
                    //return JsonPrint("<div class=\"stuff\">Hello World</div>");
                    return JsonPrint(GetMarkup(inputs));
                }
            }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }

    }
}