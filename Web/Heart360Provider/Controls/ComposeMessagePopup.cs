﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;

namespace Heart360Provider.Controls
{
    
    /// <summary>
    /// The ComposeMessagePopup is the swiss army knife of composing a new message and for replying to or forwarding an existing message.
    /// The "id" parameter can contain comma delimited values in the following order:
    ///     folderType - a string, the DAL requires a folderType with a messageID
    ///     messageID - a string representing an ID of a message
    ///     patientOfflineHealthRecordGuid - if this is a compose message for a specific patient/participant
    /// 
    /// Use cases:
    ///     1. no folderType, messageID, or patientGuid => Compose a new message, no default destination.
    ///     2. no folderType nor messageID, but a patientGuid => Compose a new message, default patient/provider destination.
    ///     3. folderType and messageID, no patientGuid => Reply to or forward existing message, destination is defaulted to same as message
    ///     * any other combination of id parameter values will default to Use Case 1
    /// </summary>
    public class ComposeMessagePopup : AHACommon.UXControl
    {
        private enum RadioButton
        {
            RadioButton_None,
            RadioButton_Global,
            RadioButton_Group,
            RadioButton_Patient
        }

        /// <summary>
        /// This function enforces the use cases and returns a properly formatted string that can be used for html data-id attribute
        /// </summary>
        /// <param name="msgFolder">One of the AHACommon.AHADefs.FolderType. enumerations OR string.Empty </param>
        /// <param name="strMessageID">String version of MessageID OR string.Empty</param>
        /// <param name="strPatientOfflineHealthRecordGuid">string version of guid OR string.Empty</param>
        /// <returns></returns>
        public static string FormatDataIds(string msgFolder, string strMessageID, string strPatientOfflineHealthRecordGuid)
        {
            string result = string.Empty;   // default to use case 1.

            if (!string.IsNullOrEmpty(strPatientOfflineHealthRecordGuid))
            {
                // use case 2
                result = ",," + strPatientOfflineHealthRecordGuid;
            }
            else if (!string.IsNullOrEmpty(msgFolder) && !string.IsNullOrEmpty(strMessageID))
            {
                result = msgFolder + "," + strMessageID + ",";
            }
            return result;


        }

        override public string GetMarkup(NameValue[] inputs)
        {
            ProviderContext                 provider = SessionInfo.GetProviderContext();
            List<AHAHelpContent.Patient>    plist = (provider != null) ? AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID) : null;

            // First make sure the provider has patients. If not, return empty popup
            if( plist == null || plist.Count == 0 )
                return "<div id=\"data-form\"><div class=\"popup-title\">Compose Message</div><div class=\"popup-body\"><p>You have no patients/participants to send a message to.</p></div></div>";

            // For testing
            this.Initialize();


            // Now figure out the Use Case
            string strMessageId = string.Empty;
            string strPatientGuid = string.Empty;
            string strFolderType = string.Empty;
            RadioButton radioCurrent = RadioButton.RadioButton_None;

            if( inputs.Length > 0 && inputs[0].name.Equals("id") && !string.IsNullOrEmpty(inputs[0].value) )
            {
                char[]  delimiter = { ',' } ;
                string[]    inIds = inputs[0].value.Split( delimiter ) ;

                if( inIds.Length > 0 )
                    strFolderType = inIds[0] ;
                if( inIds.Length > 1 )
                    strMessageId = inIds[1] ;
                if( inIds.Length > 2 )
                    strPatientGuid = inIds[2] ;
            }

            AHAHelpContent.Patient  defaultPatient = null ;
            int?                    defaultGroupID = null ;

            string title = "COMPOSE MESSAGE";
            string subject = string.Empty;
            string contents = string.Empty;

                // check Use Case 2
            if( !string.IsNullOrEmpty( strPatientGuid ) )
            {
                Guid    findGuid = new Guid( strPatientGuid ) ;
                foreach( AHAHelpContent.Patient pat in plist )
                {
                    if( pat.OfflineHealthRecordGUID.HasValue && pat.OfflineHealthRecordGUID.Value.Equals( findGuid ) )
                    {
                        defaultPatient = pat ;
                        radioCurrent = RadioButton.RadioButton_Patient;
                        break ;
                    }
                }
            }  
                // check Use Case 3
            else if( !string.IsNullOrEmpty( strMessageId ) && !string.IsNullOrEmpty( strFolderType ) )
            {
                title = "REPLY TO MESSAGE" ;

                int                                         msgID = Convert.ToInt32( strMessageId) ;
                AHAHelpContent.UserTypeDetails              utd = new UserTypeDetails();

                utd.UserID = provider.ProviderID ;
                utd.UserType = UserType.Provider ;

                AHAHelpContent.Message                      msg = AHAHelpContent.Message.GetMessageDetails( utd, strFolderType, msgID ) ;
                Heart360Provider.Utility.MessageInfo        msgInfo = (msg != null) ? Heart360Provider.Utility.MessageInfo.GetMessageInfoObject( msg, utd, strFolderType ) : null ;

                //HVManager.MessageDataManager.MessageItem    msgContents = (provider != null) ? Heart360Provider.Utility.MessageWrapper.GetMessageByIdForProvider(provider.ProviderID, msgID) : null;

                if (msgInfo != null)
                {
                    subject = "Re: " + msgInfo.Subject ;    // msgContents.Subject.Value ;
                    contents = string.Format( "\nTO: {0}\nSENT:{1} {2}\nFROM: {3}\nSUBJECT: {4}\n\n{5}", msgInfo.To, msgInfo.SentDate.ToShortDateString(), msgInfo.SentDate.ToShortTimeString(), msgInfo.From, msgInfo.Subject, msgInfo.MessageBody ) ;         // msgContents.MessageBody.Value ;

                    int     patientIDtoSearchFor = -1 ;

                    if( msgInfo.SentByProvider )
                    {
                        if( msgInfo.GroupID.HasValue )
                        {
                            defaultGroupID = msgInfo.GroupID ;
                        }
                        else
                        {
                            // Need to default recipient to be person message was sent to.
                            patientIDtoSearchFor = msgInfo.PatientID ;
                        }
                    }
                    else
                    {
                        patientIDtoSearchFor = msgInfo.PatientID ;
                    }

                    if( patientIDtoSearchFor != -1 )
                    {
                        AHAHelpContent.Patient psearch = AHAHelpContent.Patient.FindByUserHealthRecordID(msgInfo.PatientID);

                        foreach( AHAHelpContent.Patient pat in plist )
                        {
                            if( pat.PersonalItemGUID.Equals( psearch.PersonalItemGUID ) )
                            //if( pat.PatientID.HasValue && pat.PatientID.Value == patientIDtoSearchFor )
                            {
                                defaultPatient = pat ;
                                radioCurrent = RadioButton.RadioButton_Patient;
                                break ;
                            }
                        }
                    }
                }
            }
                // else we will default to Use Case 1


            // Assign default values so they will be injected in markup.
            // we need to call GetGroupDropdownContent() before we loop because it can change radioCurrent
            string groupDropdown = GetGroupDropdownContent(defaultGroupID, ref radioCurrent);
            if (radioCurrent == RadioButton.RadioButton_None)
                radioCurrent = RadioButton.RadioButton_Global;

            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.DropDown:
                        if (cw.Name.Equals("Groups"))
                        {
                            cw.Value = groupDropdown;
                        }
                        else if (cw.Name.Equals("Patients"))
                        {
                            cw.Value = GetPatientDropdownContent( plist, defaultPatient );
                        }
                        break;

                    case ControlWidgetType.String:
                        if (cw.Name.Equals("Subject"))
                        {
                            cw.Value = subject;
                        }
                        else if (cw.Name.Equals("Message"))
                        {
                            cw.Value = contents;
                        }
                        break;

                    case ControlWidgetType.Radio:
                        if (cw.Name.Equals("RadioGlobal"))
                        {
                            cw.Value = (radioCurrent == RadioButton.RadioButton_Global) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioGroup"))
                        {
                            cw.Value = (radioCurrent == RadioButton.RadioButton_Group) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioPatient"))
                        {
                            cw.Value = (radioCurrent == RadioButton.RadioButton_Patient) ? "true" : string.Empty;
                        }
                        break;

                    default:
                        break;
                }
            }


            string  markup = this.PrepareMarkup();  // this will do string replacement of values in metadata
            return markup.Replace("$Title$", title);
            
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Send button.

            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                    // if a radio button is not checked, then that widget is not passed in inputs. Have to test for existance.
                string valToCheck = (hashInputs.ContainsKey(cw.Id)) ? hashInputs[cw.Id] : string.Empty;
                if (!cw.Validate(valToCheck, ref errMsg))
                {

                    sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                    goodToGo = false;
                }
            }


            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }

            // Now we need to determine to whom we are sending the message and if we need to check the dropdown values
            string radioVal = hashInputs[this.MetadataHash["To"].Id] ;
                // we build a list of ids (UserHealthRecordID) to send the message to.
            List<int>   listToIds = new List<int>();
            int?        iGroupID = null;
            ProviderContext provider = SessionInfo.GetProviderContext();

            if (radioVal.Equals("group"))
            {
                // need to check dropdown
                string strGroupId = hashInputs[this.MetadataHash["Groups"].Id] ;
                if (string.IsNullOrEmpty(strGroupId))
                {
                    goodToGo = false;
                    sb.Append("A group needs to be selected.");
                }
                else
                {
                    iGroupID = Convert.ToInt32(strGroupId);
                    //Finds all patients in this group
                    listToIds.AddRange(AHAHelpContent.Patient.FindAllByGroupID(iGroupID.Value).Select(P => P.UserHealthRecordID));
                }
            }
            else if (radioVal.Equals("patient"))
            {
                // need to check dropdown
                string patientId = hashInputs[this.MetadataHash["Patients"].Id];
                if (string.IsNullOrEmpty(patientId))
                {
                    goodToGo = false;
                    sb.Append("A patient/provider needs to be selected.");
                }
                else
                {
                    listToIds.Add( Convert.ToInt32( patientId ) ) ;
                }
            }
            else     // global
            {
                //Finds the default group id
                iGroupID = (AHAHelpContent.PatientGroup.FindAllByProviderID(provider.ProviderID).Where(PG => PG.IsDefault).FirstOrDefault()).GroupID;

                //Finds all patient for this provider
                listToIds.AddRange(AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID).Select(P => P.UserHealthRecordID));
            }

            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }

            // 
            // If we are here we can do the work....
            //

            string strToUsers = string.Join(",", Array.ConvertAll<int, string>(listToIds.ToArray(), new Converter<int, string>(Convert.ToString)));

            //Create message entry in db
            int iMessageID = Message.CreateMessage( provider.ProviderID, strToUsers, AHACommon.AHADefs.MessageType.MESSAGE_TYPE_NEW, true, iGroupID);

            listToIds.ForEach(delegate(int iToUserID)
            {
                //Create message in HV
                AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(iToUserID);

                HVManager.MessageDataManager.MessageItem objMessage = new HVManager.MessageDataManager.MessageItem();

                objMessage.MessageBody.Value = hashInputs[MetadataHash["Message"].Id] ;
                objMessage.Subject.Value = hashInputs[MetadataHash["Subject"].Id];
                objMessage.IsSentByProvider.Value = true;
                objMessage.DateSent.Value = DateTime.Now;
                objMessage.MessageID.Value = iMessageID;
                objMessage.ProviderID.Value = provider.ProviderID;

                Heart360Provider.Utility.MessageWrapper.CreateMessage(objMessage, objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
            });

            string title = (hashInputs.ContainsKey("hidTitle")) ? hashInputs["hidTitle"] : "Compose Message";
            return JsonRefresh( title, string.Format("Successfully sent your message to {0} patient(s)/participant(s).", listToIds.Count.ToString()) );
        }

        private string GetGroupDropdownContent( int? defaultGroupID, ref RadioButton currentRadio )
        {
            ProviderContext     provider = SessionInfo.GetProviderContext();
            List<PatientGroup>  groupList = (provider != null) ? PatientGroup.FindAllByProviderID(provider.ProviderID) : null;
            int                 addedGroupCount = 0;

            StringBuilder       sb = new StringBuilder();

            if (groupList != null && groupList.Count > 0 )
            {
                foreach (AHAHelpContent.PatientGroup pg in groupList)
                {
                    // do not include the default group
                    if (!pg.IsDefault && pg.TotalMembers > 0)
                    {
                        if (!defaultGroupID.HasValue || defaultGroupID.Value != pg.GroupID)
                        {
                            sb.Append("<option value=\"" + pg.GroupID.ToString() + "\">" + pg.Name + "</option>");
                        }
                        else
                        {
                            sb.Append("<option selected=\"selected\" value=\"" + pg.GroupID.ToString() + "\">" + pg.Name + "</option>");
                            currentRadio = RadioButton.RadioButton_Group;
                        }
                        addedGroupCount++;
                    }
                    else if (pg.IsDefault && defaultGroupID.HasValue && defaultGroupID.Value == pg.GroupID)
                    {
                        currentRadio = RadioButton.RadioButton_Global;
                    }
                }
            }

            if (addedGroupCount == 0)
            {
                sb.Append("<option selected=\"selected\" value=\"\">No groups available</option>");
            }

            return sb.ToString();
        }

        private string GetPatientDropdownContent( List<AHAHelpContent.Patient> plist, AHAHelpContent.Patient defaultPatient )
        {
            // Now get extended patient class with personal information
            List<AHABusinessLogic.PatientInfo>   lstPatientInfo = (plist != null && plist.Count > 0) ? AHABusinessLogic.PatientInfo.GetPopulatedPatientInfo(plist) : null ;

            StringBuilder sb = new StringBuilder();

            if (lstPatientInfo != null && lstPatientInfo.Count > 0)
            {
                foreach (AHABusinessLogic.PatientInfo pat in lstPatientInfo)
                {
                    if( defaultPatient == null || defaultPatient.UserHealthRecordID != pat.UserHealthRecordID )
                        sb.Append("<option value=\"" + pat.UserHealthRecordID.ToString() + "\">" + pat.FullName + "</option>");
                    else
                        sb.Append("<option selected=\"selected\" value=\"" + pat.UserHealthRecordID.ToString() + "\">" + pat.FullName + "</option>");
                }
            }
            else
            {
                sb.Append("<option selected=\"selected\" value=\"\">No patients/participants available</option>");
            }

            return sb.ToString() ;
        }

    }
}