﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSummary.ascx.cs" Inherits="Heart360Provider.Controls.ModuleSummary" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!-- ### BEGIN Module Summary Widget ### -->
<asp:Literal ID="litTrackerBegin" runat="server" />

		<div class="top-tile">

			<a id="aTrackerLink1" href="" runat="server"><asp:Literal ID="litTitle" runat="server" /></a>

            <asp:Literal ID="litPopupLink1" runat="server"></asp:Literal>
            <asp:Literal ID="litPopupLink2" runat="server"></asp:Literal>

		</div>

		<div class="middle-tile">

   			<a id="aTrackerLink2" class="tile-button" href="" runat="server">
    			<div class="tile-icon"></div>
            </a>

			<div class="tile-icon-color"></div>
			
			<div class="tile-value">
                <asp:Literal ID="litModuleData" runat="server"></asp:Literal>
            </div>

		</div>

		<div class="bottom-tile">
		</div>


<asp:Literal ID="litTrackerEnd" runat="server" />

<asp:HiddenField id="hidModuleID" runat="server" />
<!-- ### END Module Summary Widget ### -->
