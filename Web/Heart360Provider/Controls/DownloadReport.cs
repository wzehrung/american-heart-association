﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;
using Heart360Provider.Reports;

namespace Heart360Provider.Controls
{
    public class DownloadReport : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            StringBuilder markup = new StringBuilder(this.PrepareMarkup());
            ProviderContext provider = SessionInfo.GetProviderContext();

            //create participant scrolling checklist
            StringBuilder checklist = new StringBuilder();

            //Create a list of participants for provider
            List<Patient> allPatientsForProvider = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);
            List<PatientInfo> allPatientInfo = PatientInfo.GetPopulatedPatientInfo(allPatientsForProvider);

            if (allPatientsForProvider == null || allPatientsForProvider.Count == 0)
            {
                markup.Replace("$PatientChecklist$", "<p>There are no patients/participants.</p>");
            }
            else
            {
                foreach (Patient patient in allPatientsForProvider)
                {
                    checklist.Append("<div class='twelvecol first'><input name='download-reportParticipant' type='checkbox' value='");
                    checklist.Append(patient.UserHealthRecordID);
                    checklist.Append("' /><label> ");
                    checklist.Append(getPatientNameById(patient));
                    checklist.Append("</label></div>");
                }

                markup.Replace("$PatientChecklist$", checklist.ToString());
            }

            //add options for groups drop-down
            StringBuilder groups = new StringBuilder();
            List<PatientGroup> groupList = (provider != null) ? PatientGroup.FindAllByProviderID(provider.ProviderID) : null;
            if (groupList == null || groupList.Count == 0)
            {
                markup.Replace("$GroupOptions$", "<p>There are no groups.</p>");
            }
            else
            {
                groups.Append("<select name='download-reportGroup' id='download-reportGroup'>");
                foreach (PatientGroup group in groupList)
                {
                    if (!group.IsDefault && group.TotalMembers > 0)
                    {
                        groups.Append("<option value='");
                        groups.Append(group.GroupID);
                        groups.Append("'>");
                        groups.Append(group.Name);
                        groups.Append("</option>");
                    }
                }
                groups.Append("</select>");
            }

            markup.Replace("$GroupOptions$", groups.ToString());

            return markup.ToString();
        }

        private static string getPatientNameById(Patient patient)
        {
            List<Patient> patientList = new List<Patient>();
            patientList.Add(patient);
            List<PatientInfo> patientInfo = PatientInfo.GetPopulatedPatientInfo(patientList);
            if (patientInfo != null && patientInfo.Count > 0)
            {
                return patientInfo[0].FullName;
            }
            return "";
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            //this isn't used as the report download requires direct page interaction, no HOTDOG for you!
            return JsonFailure("Not implemented");
        }

        public void RunReport(NameValueCollection inputs)
        {
            ProviderContext provider = SessionInfo.GetProviderContext();

            //parse form inputs
            bool individualReport = false, groupReport = false, contactlistReport = false;
            List<int> reportParticipants = new List<int>();
            int reportGroup = -1;

            if (!string.IsNullOrEmpty(inputs["download-scope"])) //drop-down
            {
                //"individual", "group", "all"
                string value = inputs["download-scope"];
                if (value.Equals("individual"))
                    individualReport = true;
                else if (value.Equals("group"))
                    groupReport = true;
                else if (value.Equals("contact"))
                    contactlistReport = true;
            }
            if (!string.IsNullOrEmpty(inputs["download-reportParticipant"])) //checkbox group
            {
                var values = inputs["download-reportParticipant"].Split(',');
                foreach (string value in values)
                {
                    int iReportParticipant;
                    if (Int32.TryParse(value, out iReportParticipant))
                    {
                        reportParticipants.Add(iReportParticipant);
                    }
                }
            }
            if (!string.IsNullOrEmpty(inputs["download-reportGroup"])) //drop-down
            {
                string value = inputs["download-reportGroup"];
                int iReportGroup;
                if (Int32.TryParse(value, out iReportGroup))
                {
                    reportGroup = iReportGroup;
                }
            }

            //patient list
            List<AHAHelpContent.Patient> patientList = null;
            List<PatientInfo> patientInfoList = null;
            if (individualReport)
            {
                patientList = new List<AHAHelpContent.Patient>();
                foreach (int id in reportParticipants)
                {
                    patientList.Add(AHAHelpContent.Patient.FindByUserHealthRecordID(id));
                }
            }
            else if (groupReport)
            {
                patientList = AHAHelpContent.Patient.FindAllByGroupID(reportGroup);
            }
            else //all
            {
                patientList = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);
            }

            //create the report
            patientInfoList = PatientInfo.GetPopulatedPatientInfo(patientList);
            string fileName = System.Web.HttpContext.GetGlobalResourceObject(
                "Reports", "Provider_Patientlist").ToString() + " " +
                DateTime.Now.ToString().Replace("/", "_").Replace(":", "_") + ".csv";

            if (contactlistReport)
            {
                CSVHelper.CreateCsvReportContactList(patientInfoList, fileName, getCsvHeadingsContactList());
            }
            else
            {
                CSVHelper.CreateCsvReport(patientInfoList, fileName, getCsvHeadings());
            }
            
        }

        private static string getCsvHeadings()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_Name"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_DOB"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_DateJoined"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Last_Active"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BloodPressure"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BloodGlucose"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_Cholesterol"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Alerts"));
            sb.Append('\n');

            return sb.ToString();
        }

        private static string getCsvHeadingsContactList()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_Name"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_EmailAddress"));
            sb.Append(",");
            sb.Append(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_PhoneNumber"));
            sb.Append(",");
            sb.Append('\n');

            return sb.ToString();
        }
   }
}