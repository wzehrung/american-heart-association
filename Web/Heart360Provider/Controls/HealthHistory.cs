﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class HealthHistory : AHACommon.UXControl
    {
        private const string MISSING_VALUE = "-";    // string.Empty
        private const string YES_VALUE = "yes";
        private const string NO_VALUE = "no";
        private Guid mPatientPersonGUID;
        private Guid mPatientRecordGUID;

        public void InitializePatient(Guid personGuid, Guid recordGuid)
        {
            mPatientPersonGUID = personGuid;
            mPatientRecordGUID = recordGuid;
        }

        private string GetHeight(HVManager.AllItemManager mgr)
        {
            LinkedList<HVManager.HeightDataManager.HeightItem> objList = mgr.HeightDataManager.GetLatestNItems(1);
            HVManager.HeightDataManager.HeightItem heightItem = (objList != null && objList.Count > 0) ? objList.First.Value : null;

            return (heightItem != null) ? (heightItem.HeightInFeet.ToString() + " feet " + heightItem.HeightInInches.ToString() + " inches") : MISSING_VALUE;        
        }

        private string GetWeight(HVManager.AllItemManager mgr)
        {
            LinkedList<HVManager.WeightDataManager.WeightItem> objList = mgr.WeightDataManager.GetLatestNItems(1);
            HVManager.WeightDataManager.WeightItem weightItem = (objList != null && objList.Count > 0) ? objList.First.Value : null;

            return (weightItem != null) ? weightItem.CommonWeight.Value.ToPounds().ToString() : MISSING_VALUE;
        }

        private string GetBloodPressure(HVManager.AllItemManager mgr)
        {
            LinkedList<HVManager.BloodPressureDataManager.BPItem> objList = mgr.BloodPressureDataManager.GetLatestNItems(1);
            HVManager.BloodPressureDataManager.BPItem bp = (objList != null && objList.Count > 0) ? objList.First.Value : null;

            return (bp != null) ? (bp.Systolic.Value.ToString() + "/" + bp.Diastolic.Value.ToString()) : MISSING_VALUE;
        }

        override public string GetMarkup(NameValue[] inputs)
        {
            bool            processedMarkup = false ;
            StringBuilder   sbMarkup = new StringBuilder();

            if (mPatientRecordGUID != Guid.Empty && mPatientPersonGUID != Guid.Empty)
            {
                try
                {
                    
                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(mPatientPersonGUID, mPatientRecordGUID);

                    HVManager.PersonalDataManager.PersonalItem objPersonalItem = mgr.PersonalDataManager.Item;
                    HVManager.BasicDataManager.BasicItem objBasicItem = mgr.BasicDataManager.Item;
                    //HVManager.PersonalContactDataManager.ContactItem objContactItem = HVHelper.HVManagerPatientBase.PersonalContactDataManager.Item;
                    HVManager.ExtendedProfileDataManager.ExtendedProfileItem objExtendedItem = mgr.ExtendedProfileDataManager.Item;
                    HVManager.CardiacDataManager.CardiacItem cardiacItem = mgr.CardiacDataManager.Item;


                    sbMarkup.Append(this.PrepareMarkup());

                    sbMarkup.Replace("$Ethnicity$", "Ethnicity: " + ((objPersonalItem != null) ? objPersonalItem.Ethnicity.Value : MISSING_VALUE));
                    sbMarkup.Replace("$Gender$", "Gender: " + ((objBasicItem != null && objBasicItem.GenderOfPerson.Value.HasValue) ? objBasicItem.GenderOfPerson.Value.Value.ToString() : MISSING_VALUE) );

                    sbMarkup.Replace("$Height$", GetHeight( mgr ));
                    sbMarkup.Replace("$Weight$", GetWeight( mgr ) ) ;
                    sbMarkup.Replace("$BloodPressure$", GetBloodPressure(mgr));
                    sbMarkup.Replace("$BloodType$", (objPersonalItem != null) ? objPersonalItem.BloodType.Value : MISSING_VALUE);
                    sbMarkup.Replace("$Allergies$", (objExtendedItem != null) ? objExtendedItem.Allergies.Value : MISSING_VALUE);
                    sbMarkup.Replace("$BPMeds$", (cardiacItem != null && cardiacItem.IsOnHypertensionMedication.Value.HasValue) ? ((cardiacItem.IsOnHypertensionMedication.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE) ;
                    sbMarkup.Replace("$HDiet$", (cardiacItem != null && cardiacItem.IsOnHypertensionDiet.Value.HasValue) ? ((cardiacItem.IsOnHypertensionDiet.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE );
                    sbMarkup.Replace("$Kidney$", (cardiacItem != null && cardiacItem.HasRenalFailureBeenDiagnosed.Value.HasValue) ? ((cardiacItem.HasRenalFailureBeenDiagnosed.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE);
                    sbMarkup.Replace("$Diabetes$", (cardiacItem != null && cardiacItem.HasDiabetesBeenDiagnosed.Value.HasValue) ? ((cardiacItem.HasDiabetesBeenDiagnosed.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE) ;
                    sbMarkup.Replace("$HDFamily$", (cardiacItem != null && cardiacItem.HasFamilyHeartDiseaseHistory.Value.HasValue) ? ((cardiacItem.HasFamilyHeartDiseaseHistory.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE );
                    sbMarkup.Replace("$HDDiagnosed$", (cardiacItem != null && cardiacItem.HasPersonalHeartDiseaseHistory.Value.HasValue) ? ((cardiacItem.HasPersonalHeartDiseaseHistory.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE) ;
                    sbMarkup.Replace("$Stroke$", (cardiacItem != null && cardiacItem.HasFamilyStrokeHistory.Value.HasValue) ? ((cardiacItem.HasFamilyStrokeHistory.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE) ;
                    sbMarkup.Replace("$Smoke$", (objExtendedItem != null && objExtendedItem.IsSmokeCigarette.Value.HasValue) ? ((objExtendedItem.IsSmokeCigarette.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE);
                    sbMarkup.Replace("$QuitSmoking$", (objExtendedItem != null && objExtendedItem.HasTriedToQuitSmoking.Value.HasValue) ? ((objExtendedItem.HasTriedToQuitSmoking.Value.Value == true) ? YES_VALUE : NO_VALUE) : MISSING_VALUE);

                    processedMarkup = true;
                }
                catch (Exception)
                {

                }
            }

            if (!processedMarkup)
            {
                sbMarkup.Append( "<p>This user has not allowed their personal information to be shared.</p>" ) ;
            }

            return sbMarkup.ToString();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            //
            // Determine if we are printing, and if so, do that and return
            //
            if (hashInputs.ContainsKey(UXControl.InputsKeyword_Print) && hashInputs[UXControl.InputsKeyword_Print].ToLower().Equals(UXControl.InputsValue_True))
            {
                return JsonPrint(GetMarkup(inputs));
            }

            return JsonFailure("Functionality not applicable");
        }
    }
}