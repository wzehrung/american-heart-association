﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class MessageSettings : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {
            // we need to build a list of NameValue pairs for assigning current values, dropdown contents, etc..
            //List<NameValue>    nvarr = new List<NameValue>() ;

            ProviderContext                 provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderDetails  providerDetails = (provider != null) ? AHAHelpContent.ProviderDetails.FindByProviderID( provider.ProviderID ) : null ;

            if( providerDetails == null )
            {
                return UXControl.GenericHtmlErrorMessage ;
            }

            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.Email:
                        switch (cw.Name)
                        {
                            case "NotificationEmail":
                                cw.Value = providerDetails.EmailForIndividualMessage ;
                                break;

                            case "DailySummaryEmail":
                                cw.Value = providerDetails.EmailForDailySummary ;
                                break;

                            default:
                                break;
                        }
                        break;

                    case ControlWidgetType.CheckBox:
                        switch( cw.Name)
                        {
                            case "NewMessageNotification":
                                cw.Value = (providerDetails.NotifyNewMessageImmediately.HasValue && providerDetails.NotifyNewMessageImmediately.Value == true) ? "true" : string.Empty ;
                                break ;

                            case "DailySummaryAlert":
                                cw.Value = (providerDetails.IncludeAlertNotificationInDailySummary.HasValue && providerDetails.IncludeAlertNotificationInDailySummary.Value == true) ? "true" : string.Empty ;
                                break ;

                            case "DailySummaryMessage":
                                cw.Value = (providerDetails.IncludeMessageNotificationInDailySummary.HasValue && providerDetails.IncludeMessageNotificationInDailySummary.Value == true) ? "true" : string.Empty ;
                                break ;

                            default:
                                break;
                        }
                        break ;

                    case ControlWidgetType.Radio:
                        if (cw.Name.Equals("CanSendMessage"))
                        {
                            cw.Value = (providerDetails.AllowPatientsToSendMessages.HasValue && providerDetails.AllowPatientsToSendMessages.Value) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("CannotSendMessage"))
                        {
                            cw.Value = (providerDetails.AllowPatientsToSendMessages.HasValue && !providerDetails.AllowPatientsToSendMessages.Value) ? "true" : string.Empty;
                        }
                        break ;

                    default:
                        break;

                }
            }


            // call the base class to do all the string comparison
            return this.PrepareMarkup();

        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Update button.

            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                // if a radio button is not checked, then that widget is not passed in inputs. Have to test for existance.
                string valToCheck = (hashInputs.ContainsKey(cw.Id)) ? hashInputs[cw.Id] : string.Empty;
                if (!cw.Validate(valToCheck, ref errMsg))
                {

                    sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                    goodToGo = false;
                }
            }


            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }


            ProviderContext                 provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderDetails  providerDetails = (provider != null) ? AHAHelpContent.ProviderDetails.FindByProviderID( provider.ProviderID ) : null ;

            if (providerDetails != null)
            {
                // update values in providerDetails
                providerDetails.AllowPatientsToSendMessages = (hashInputs.ContainsKey("radSend") && hashInputs["radSend"].Equals("can")) ? true : false;
                providerDetails.NotifyNewMessageImmediately = (hashInputs.ContainsKey("cbNewMessageNotification") && hashInputs["cbNewMessageNotification"].Equals(UXControl.HtmlCheckBoxChecked)) ? true : false;
                providerDetails.IncludeAlertNotificationInDailySummary = (hashInputs.ContainsKey("cbDailySummaryAlert") && hashInputs["cbDailySummaryAlert"].Equals(UXControl.HtmlCheckBoxChecked)) ? true : false;
                providerDetails.IncludeMessageNotificationInDailySummary = (hashInputs.ContainsKey("cbDailySummaryMessage") && hashInputs["cbDailySummaryMessage"].Equals(UXControl.HtmlCheckBoxChecked)) ? true : false;
                if (providerDetails.NotifyNewMessageImmediately.Value)
                {
                    // this handles a corner case in that we cannot make the email field required
                    if (string.IsNullOrEmpty(hashInputs["txtNotificationEmail"]))
                        return JsonFailure("Notification Email is required");
                    providerDetails.EmailForIndividualMessage = hashInputs["txtNotificationEmail"];
                }
                if (providerDetails.IncludeMessageNotificationInDailySummary.Value || providerDetails.IncludeAlertNotificationInDailySummary.Value)
                {
                    // this handles a corner case in that we cannot make the email field required
                    if (string.IsNullOrEmpty(hashInputs["txtDailySummaryEmail"]))
                        return JsonFailure("Daily Summary Email is required.");
                    providerDetails.EmailForDailySummary = hashInputs["txtDailySummaryEmail"];
                }

                try
                {
                    AHAHelpContent.ProviderDetails.UpdateProvider(provider.ProviderID,
                                                                    providerDetails.Email,
                                                                    providerDetails.FirstName,
                                                                    providerDetails.LastName,
                                                                    providerDetails.Salutation,
                                                                    providerDetails.PracticeName,
                                                                    providerDetails.PracticeEmail,
                                                                    providerDetails.PracticeAddress,
                                                                    providerDetails.PracticePhone,
                                                                    providerDetails.PracticeFax,
                                                                    providerDetails.Speciality,
                                                                    providerDetails.PharmacyPartner,
                                                                    providerDetails.ProfileMessage,
                                                                    providerDetails.PracticeUrl,
                                                                    providerDetails.City,
                                                                    providerDetails.StateID,
                                                                    providerDetails.Zip,
                                                                    providerDetails.AllowPatientsToSendMessages,
                                                                    providerDetails.IncludeAlertNotificationInDailySummary,
                                                                    providerDetails.IncludeMessageNotificationInDailySummary,
                                                                    providerDetails.NotifyNewAlertImmediately,
                                                                    providerDetails.NotifyNewMessageImmediately,
                                                                    providerDetails.DismissAlertAfterWeeks,
                                                                    providerDetails.EmailForDailySummary,
                                                                    providerDetails.EmailForIndividualAlert,
                                                                    providerDetails.EmailForIndividualMessage,
                                                                    providerDetails.AllowAHAEmail);

                    return JsonSuccess("Successfully updated Message Settings");
                }
                catch (Exception)
                { }
            }

            return JsonFailure("Error saving your Message Settings.");

        }

    }
}