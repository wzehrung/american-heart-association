﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class GroupList : AHACommon.UXControl
    {


        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext provider = SessionInfo.GetProviderContext();
            List<PatientGroup> groupList = (provider != null) ? PatientGroup.FindAllByProviderID(provider.ProviderID) : null ;

            if( groupList == null || groupList.Count < 1 )
            {
                return "There are no groups.";
            }

            StringBuilder sb = new StringBuilder();
            string markup = this.PrepareMarkup();

            // put the most recent groups first
            groupList.Reverse();

            foreach (PatientGroup pg in groupList)
            {
                if (pg.IsDefault)   // Do not show the default group!
                    continue;

                // Do all literal replacement for this patient group and add it to the final markup
                string pgHtml = markup.Replace("$GroupID$", pg.GroupID.ToString());

                pgHtml = pgHtml.Replace("$GroupName$", pg.Name);
                pgHtml = pgHtml.Replace("$TotalMembers$", pg.TotalMembers.ToString());
                pgHtml = pgHtml.Replace("$Description$", pg.Description);

                // Determine type of group for edit popup
                // NOTE: IF THIS LOGIC CHANGES, ALSO NEED TO UPDATE THE LOGIC IN PatientGroupList.cs!!!!
                if(pg.GroupRuleXml == null) 
                {
                    //Manual group
                    pgHtml = pgHtml.Replace("$GroupEditMethod$", "EditGroupPopup");
                }
                else 
                {
                    //Dynamic group
                    pgHtml = pgHtml.Replace("$GroupEditMethod$", "EditDynamicGroupPopup");                
                }

                // Note: use AHAWeb (V5) Provider/Groups/ViewIndividualGroup.aspx as reference for DAL 
                //
                // Add in list of participants
                //
                string          listMarkup = string.Empty;
                List<Patient>   grpPatList = Patient.FindAllByGroupID(pg.GroupID);
                if (grpPatList.Count > 0)
                {
                    StringBuilder partListSb = new StringBuilder();
                    List<PatientInfo> lstPatientInfo = PatientInfo.GetPopulatedPatientInfo(grpPatList);
                    foreach (PatientInfo pi in lstPatientInfo)
                    {
                        string yob = (pi.YearOfBirth.HasValue) ? pi.YearOfBirth.Value.ToString() : "";
                        string lastActive = (pi.LastActive.HasValue) ? pi.LastActive.Value.ToString("d") : "";
                        string dateJoined = (pi.DateJoined.HasValue) ? pi.DateJoined.Value.ToString("d") : "";
                        // ProviderConnectionDate is actually the date the patient was added to the group!
                        string addedToGroup = (pi.ProviderConnectionDate.HasValue) ? pi.ProviderConnectionDate.Value.ToString("d") : "";
                        partListSb.Append("<tr><td>" + Utility.UIHelper.GetHtmlAtagForPatientInfo(pi) + "</td><td>" + yob + "</td><td>" + lastActive + "</td><td>" + addedToGroup + "</td><td>" + dateJoined + "</td></tr>");
                    }
                    listMarkup = partListSb.ToString();
                }
                else
                {
                    listMarkup = "<tr><td colspan='5'>There are no group participants.</td></tr>";
                }
                pgHtml = pgHtml.Replace("$ParticipantList$", listMarkup );
                

                // Add in alert rules list
                listMarkup = string.Empty;
                List<AHAHelpContent.AlertRule> lstAlertRules = AHAHelpContent.AlertRule.FindByGroupID( pg.GroupID );
                if (lstAlertRules.Count > 0)
                {
                    StringBuilder alsb = new StringBuilder();
                    foreach (AHAHelpContent.AlertRule ar in lstAlertRules)
                    {
                        string litAlert = "-";
                        if (ar.Type == TrackerDataType.BloodGlucose)
                        {
                            litAlert = AlertManager.BloodGlucoseAlertRule.GetFormattedAlertRule(ar.AlertRuleData);
                        }
                        else if (ar.Type == AHAHelpContent.TrackerDataType.BloodPressure)
                        {
                            litAlert = AlertManager.BloodPressureAlertRule.GetFormattedAlertRule(ar.AlertRuleData);
                        }
                        else if (ar.Type == TrackerDataType.Weight)
                        {
                            litAlert = AlertManager.WeightAlertRule.GetFormattedAlertRule(ar.AlertRuleData);
                        }
                        string editaction = "<div class=\"edit-icon popuplink\" data-page=\"Pages/Groups\" data-method=\"EditAlertRulePopup\" data-refresh-ids=\"idControlGroupList\" data-id=\"" + ar.AlertRuleID.ToString() + "\"></div> ";
                        string delaction = "<div class=\"trash-icon popuplink\" data-page=\"Pages/Groups\" data-method=\"DeleteAlertRulePopup\" data-refresh-ids=\"idControlGroupList\" data-id=\"" + ar.AlertRuleID.ToString() + "\"></div>";
                        alsb.Append("<tr><td>" + litAlert + "</td><td colspan='2'>" + delaction + editaction + "</td></tr>");
                    }
                    listMarkup = alsb.ToString();
                }
                else
                {
                    //no alerts
                    listMarkup = "<tr><td colspan='5'>There are no alert rules.</td></tr>";
                }

                pgHtml = pgHtml.Replace("$AlertRuleList$", listMarkup);

                sb.Append(pgHtml);
            }

            return sb.ToString();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }

    }
}