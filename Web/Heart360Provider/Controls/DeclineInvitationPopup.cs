﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class DeclineInvitationPopup : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {

            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                // the id is the invitation id
                try
                {
                    char[] delimiters = { '|' };
                    string[] svals = inputs[0].value.Split(delimiters, 2);


                    //populate the popup data
                    StringBuilder markup = new StringBuilder(this.PrepareMarkup());
                    markup.Replace("$InvitationId$", svals[0]);
                    markup.Replace("$Fullname$", svals[1]);

                    return markup.ToString();
                }
                catch (Exception)
                {
                    //int number format exception
                }
            }

            return "Sorry, an internal error occurred (no ID). Please close popup and try again.";
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                string sInviteId = hashInputs["txtInvitationId"];
                int     iInviteId = (!string.IsNullOrEmpty(sInviteId)) ? Convert.ToInt32(sInviteId) : -1 ;

                if (iInviteId > 0)
                {
                    //
                    // Cancel and Block Invitation
                    //
                    AHAProvider.ProviderContext provider = AHAProvider.SessionInfo.GetProviderContext();
                    PatientProviderInvitation objInvitation = PatientProviderInvitation.FindByInvitationID(iInviteId);
                    PatientProviderInvitationDetails objAvailableDetails = objInvitation.PendingInvitationDetails.First();

                    //Marks the existing invitations, sent by this patient to the current provider, as blocked
                    //cannot user objInvitation.PatientID.Value as patient id is null because whilr creating an invitation patient id is passed as null
                    PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(objInvitation.PatientID.Value, provider.ProviderID, false);

                    objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Blocked;
                    objAvailableDetails.Update();

                    return JsonRefresh("DECLINE INVITATION", "Patient/Participant invitation declined.");
                }
            }
            catch (Exception)
            {
                //the dictionary can throw exceptions, or a bunch of other stuff
            }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }

    }
}