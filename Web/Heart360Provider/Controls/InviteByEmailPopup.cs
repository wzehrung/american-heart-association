﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class InviteByEmailPopup : AHACommon.UXControl
    {
        private class ParsedInvitation
        {
            public string mFirstName;
            public string mLastName;
            public string mEmail;
            public string mConfirmEmail;
            public int mIndex;

            public ParsedInvitation(string fname, string lname, string email, string confirmEmail, int index )
            {
                mFirstName = fname;
                mLastName = lname;
                mEmail = email;
                mConfirmEmail = confirmEmail;
                mIndex = index;
            }

                // This method will validate the parsed invitation values using the provided control widgets
            public bool Validate( ref ControlWidget[] validators, ref StringBuilder sbError)
            {
                string errMsg = string.Empty ;

                if( !validators[0].Validate( mFirstName, ref errMsg ) ||
                    !validators[1].Validate( mLastName, ref errMsg ) ||
                    !validators[2].Validate(mEmail, ref errMsg ) ||
                    !validators[3].Validate(mConfirmEmail, ref errMsg ) )
                {
                    sbError.Append(string.Format("Invitation #{0} {1} ", mIndex, errMsg));
                    return false;
                }

                if( !mEmail.Equals( mConfirmEmail ) )
                {
                    sbError.Append(string.Format("Invitation #{0} {1} and {2} values are not the same. ", mIndex, validators[2].Name, validators[3].Name));
                    return false ;
                }

                return true;
            }

                // this method will send the invitation for the parsed invitation.
                // this method assumes that the parsed invitation has been validated.
            public bool Send( AHAHelpContent.ProviderDetails objProvider, ref StringBuilder sbError)
            {
                try
                {
                    //Create invitation entry
                    AHAHelpContent.PatientProviderInvitation objInvitation = PatientProviderInvitation.CreatePatientInvitation(
                            mFirstName, mLastName, mEmail, objProvider.ProviderID, true);

                    EmailUtils.SendInvitationToPatient(objInvitation, objProvider);

                    return true;
                }
                catch (Exception ex)
                {
                    sbError.Append(string.Format("Internal error {0}", ex.Message));
                }
                return false;
            }
        };

        override public string GetMarkup(NameValue[] inputs)
        {
            this.Initialize();  // for development only

            return this.PrepareMarkup();
        }


        public override string ProcessFormValues(NameValue[] inputs )
        {
            int nbInvitations = 0;
            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);
            List<ParsedInvitation> invitations = new List<ParsedInvitation>();

            // Note: the value for 'repeater-number' will be 0 if only one invitation is specified.
            // the 'repeater-number' value specifies how many invitations there are over and above
            // the first one.
            if (hashInputs.ContainsKey("repeater-number"))
            {
                nbInvitations = int.Parse(hashInputs["repeater-number"]);
            }

            StringBuilder       sbErr = new StringBuilder() ;
            ControlWidget[]     validators = new ControlWidget[4] ;

            for (int i = 0; i < 4; i++)
                validators[i] = new ControlWidget();

            validators[0].WidgetType = validators[1].WidgetType = ControlWidgetType.String;
            validators[2].WidgetType = validators[3].WidgetType = ControlWidgetType.Email;
            validators[0].Required = validators[1].Required = validators[2].Required = validators[3].Required = true;
            validators[0].Name = "Name";
            validators[1].Name = "Last Name";
            validators[2].Name = "Email";
            validators[3].Name = "Confirm Email";

            // validate specified invitations
            for (int i = 0; i <= nbInvitations; i++)
            {
                string fnkey = (i == 0) ? "txtFirstName" : string.Format("txtFirstName-{0}", i);
                string lnkey = (i == 0) ? "txtLastName" : string.Format("txtLastName-{0}", i);
                string ekey = (i == 0) ? "txtEmail" : string.Format("txtEmail-{0}", i);
                string cekey = (i == 0) ? "txtConfirmEmail" : string.Format("txtConfirmEmail-{0}", i);

                ParsedInvitation invite = new ParsedInvitation(hashInputs[fnkey], hashInputs[lnkey], hashInputs[ekey], hashInputs[cekey], i + 1);
                if (!invite.Validate(ref validators, ref sbErr))
                {
                    return JsonFailure(sbErr.ToString());
                }
                invitations.Add(invite);
            }
            
            // if we are here, we have a list of validated invitations.
            // process the invitations.
            ProviderContext                 provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderDetails  providerDetails = ProviderDetails.FindByProviderID( provider.ProviderID ) ;

            int processedCount = 0 ;
            foreach( ParsedInvitation p in invitations )
            {
                if( !p.Send( providerDetails, ref sbErr ) )
                    return JsonFailure( string.Format( "Sent {0} invitations, then error: {1}", processedCount, sbErr.ToString() ) );
                processedCount++ ;
            }

            return JsonRefresh( "INVITE PATIENT/PARTICIPANT BY EMAIL", string.Format("Sent {0} invitations.", processedCount ));

            //return JsonFailure("Unimplemented functionality");
        }

    }
}