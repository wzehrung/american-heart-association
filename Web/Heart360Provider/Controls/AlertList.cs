﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class AlertList : AHACommon.UXControl
    {
        private const int NO_PATIENT = -1;
        private int     mPatientID = NO_PATIENT ;           // default value
        private string  mWebMethodPage = "Pages/Alerts";    // default value

        /// <summary>
        /// The Alert List can display all alerts for a Provider (default), or alerts for a single patient of a Provider.
        /// For the latter, the PatientID must be specified.
        /// </summary>
        public int PatientID
        {
            set
            {
                mPatientID = value;
            }
        }

        public string WebMethodPage
        {
            set { mWebMethodPage = value; }
        }

        private string getWhoContent(AHAHelpContent.Alert objAlert)
        {
            AHAHelpContent.Patient      objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(objAlert.PatientID);
            AHABusinessLogic.PatientInfo     objPatientInfo = (objPatient != null) ? AHABusinessLogic.PatientInfo.GetPopulatedPatientInfo( objPatient ) : null  ;

            return (objPatientInfo != null) ? Utility.UIHelper.GetHtmlAtagForPatientInfo( objPatientInfo ) : "- unknown -" ;
        }

        private string getReading(AHAHelpContent.Alert objAlert)
        {
            string rez = "-";

            if (objAlert.Type == AHAHelpContent.TrackerDataType.BloodGlucose)
            {
                rez = AlertManager.BloodGlucoseAlert.GetFormattedAlert(objAlert.AlertXml);
            }
            else if (objAlert.Type == AHAHelpContent.TrackerDataType.BloodPressure)
            {
                rez = AlertManager.BloodPressureAlert.GetFormattedAlert(objAlert.AlertXml) ;    // .Replace(",", "<br />");
            }
            else if (objAlert.Type == AHAHelpContent.TrackerDataType.Weight)
            {
                rez = AlertManager.WeightAlert.GetFormattedAlert(objAlert.AlertXml);    // .Replace(",", "<br />");
            }

            return rez;
        }

        private string getAlertMeIfText(AHAHelpContent.Alert objAlert)
        {
            string rez = "-";

            if (objAlert.Type == AHAHelpContent.TrackerDataType.BloodGlucose)
            {
                rez = AlertManager.BloodGlucoseAlertRule.GetFormattedAlertRule(objAlert.AlertRuleXml);
            }
            else if (objAlert.Type == AHAHelpContent.TrackerDataType.BloodPressure)
            {
                rez = AlertManager.BloodPressureAlertRule.GetFormattedAlertRule(objAlert.AlertRuleXml);
            }
            else if (objAlert.Type == AHAHelpContent.TrackerDataType.Weight)
            {
                rez = AlertManager.WeightAlertRule.GetFormattedAlertRule(objAlert.AlertRuleXml);
            }
            return rez;
        }

        

        override public string GetMarkup(NameValue[] inputs)
        {
            // Need to get the list of alerts for the list.

            ProviderContext provider = SessionInfo.GetProviderContext();
            List<AHAHelpContent.Alert> alist = (mPatientID == NO_PATIENT) ? AHAHelpContent.Alert.FindByProviderID(provider.ProviderID) : 
                                                                    AHAHelpContent.Alert.FindByPatientID( mPatientID, provider.ProviderID ) ;

            if (alist == null || alist.Count < 1)
            {
                return "<p>There are no alerts.</p>";
            }

            //
            // Now do Alerts
            //
            string          markup = this.PrepareMarkup();
            StringBuilder   sb = new StringBuilder();
            int             dismissCount = 0;   // the number of alerts that can be dismissed

            foreach (AHAHelpContent.Alert al in alist)
            {
                string dismissaction = (!al.IsDismissed) ? "<div class=\"trash-icon popuplink\" data-page=\"" + mWebMethodPage + "\" data-method=\"DismissAlertPopupHandler\" data-id=\"" + al.AlertID.ToString() + "\" data-refresh-ids=\"idControlAlertList,idModuleHeader1\"></div>" : string.Empty ;
                if (!al.IsDismissed)
                    dismissCount += 1;

                if( mPatientID == NO_PATIENT)
                    sb.Append("<tr><td>" + al.ItemEffectiveDate.ToShortDateString() + "</td><td>" + getWhoContent(al) + "</td><td>" + getReading(al) + "</td><td>" + getAlertMeIfText(al) + "</td><td>" + dismissaction + "</td></tr>");
                else
                    sb.Append("<tr><td>" + al.ItemEffectiveDate.ToShortDateString() + "</td><td>" + getReading(al) + "</td><td>" + getAlertMeIfText(al) + "</td><td>" + dismissaction + "</td></tr>");
            }

            markup = markup.Replace("$WhoHeader$", (mPatientID == NO_PATIENT) ? "<th data-sort=\"string\"><div class=\"column-name-box\">Who</div></th>" : string.Empty );
            markup = markup.Replace("$AlertList$", sb.ToString());
            return markup.Replace("$DismissAll$", (dismissCount == 0) ? string.Empty :
                string.Format("<div class=\"popuplink gradient-button float-right\" data-page=\"{0}\" data-method=\"DismissAlertPopupHandler\" data-id=\"{1}\" data-refresh-ids=\"idControlAlertList,idModuleHeader1\">Dismiss All Alerts</div>", mWebMethodPage, Heart360Provider.Controls.DismissAlertPopup.ID_ALL));
 //           return markup.Replace("$DismissAllId$", Heart360Provider.Controls.DismissAlertPopup.ID_ALL.ToString());

        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            //
            // Unless, of course, we are printing....
            //
            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            //
            // Determine if we are printing, and if so, do that and return
            //
            if (hashInputs.ContainsKey(UXControl.InputsKeyword_Print) && hashInputs[UXControl.InputsKeyword_Print].ToLower().Equals(UXControl.InputsValue_True))
            {
                return JsonPrint(GetMarkup(inputs));
            }

            //
            // If we are here, we are not printing.
            //

            return JsonFailure("Feature not supported.");
        }
    }
}