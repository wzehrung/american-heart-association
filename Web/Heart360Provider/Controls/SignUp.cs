﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;

namespace Heart360Provider.Controls
{
        //
        // The SignUp control cannot extend the ProfileInfo class, it needs to include it.
        //
    public class SignUp : AHACommon.UXControl
    {
        private ProfileInfo mProfileInfoControl;


        public AHAHelpContent.DbUserType ProviderType
        {
            get { return (mProfileInfoControl != null) ? mProfileInfoControl.ProviderType : AHAHelpContent.DbUserType.DbUserTypeUnknown; }
            set { if( mProfileInfoControl != null ) mProfileInfoControl.ProviderType = value ; }
        }

        public string Username
        {
            get { return (mProfileInfoControl != null) ? mProfileInfoControl.Username : null ; }
            set { if( mProfileInfoControl != null) mProfileInfoControl.Username = value ; }
        }

        public string Password
        {
            get { return (mProfileInfoControl != null) ? mProfileInfoControl.Password : null ; }
            set { if( mProfileInfoControl != null) mProfileInfoControl.Password = value ; }
        }

        public SignUp()
        {

            try
            {
                mProfileInfoControl = new ProfileInfo();
                mProfileInfoControl.Name = "ProfileInfo";
                if (!mProfileInfoControl.Initialize())
                {
                    mProfileInfoControl = null;
                }
                else
                {
                    // tell the control that we are in create provider mode
                    mProfileInfoControl.ProviderInfo = null;
                }
            }
            catch
            {
                mProfileInfoControl = null;
            }

        }



        //
        // Methods from base class
        //

        override public string GetMarkup(NameValue[] inputs)
        {
            // we need to build a list of NameValue pairs for assigning current values, dropdown contents, etc..
            //List<NameValue>    nvarr = new List<NameValue>() ;

            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.String:
                    case ControlWidgetType.Email:
                    case ControlWidgetType.PhoneNumber:
                    case ControlWidgetType.Url:
                    case ControlWidgetType.Zipcode:
                        switch (cw.Name)
                        {
                            case "Agreement":
                                cw.Value = (ProviderType == AHAHelpContent.DbUserType.DbUserTypeProvider) ? "Provider Agreement" : "Volunteer Agreement" ;
                                break;

                            case "Username":
                                cw.Value = string.Empty;
                                break;

                            case "Password":
                                cw.Value = string.Empty;
                                break;

                            case "ConfirmPassword":
                                cw.Value = string.Empty;
                                break;

                            default:
                                break;
                        }
                        break;

                    case ControlWidgetType.CheckBox:
                        switch (cw.Name)
                        {
                            case "AgreeToTerms":
                                cw.Value = false.ToString();
                                break;

                            default:
                                break;
                        }
                        break;


                    case ControlWidgetType.DropDown:
                        break;

                    default:
                        break;

                }
            }

            // call the base class to do all the string comparison
            string markup = this.PrepareMarkup();

            // get the markup for our child control
            if (mProfileInfoControl != null)
            {
                string childMarkup = mProfileInfoControl.GetMarkup(inputs);

                // Insert the child markup into our markup
                markup = markup.Replace("$ProfileInfoContent$", childMarkup);

                if( ProviderType == AHAHelpContent.DbUserType.DbUserTypeProvider )
                    markup = markup.Replace("$Agreement$", Resources.Provider.TermsOfUse_Provider ) ;
                else
                    markup = markup.Replace( "$Agreement$", Resources.Provider.TermsOfUse_Volunteer ) ;
            }

            return markup;

        }


        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Save button. Cancel fires an INIT

            if (mProfileInfoControl == null)
                return JsonFailure("Internal error (child initialization)");

            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                // html5 checkboxes are not submitted as a form field if they are not checked.
                // so we have to do a bit more error handling here.
                string cwVal = hashInputs.ContainsKey(cw.Id) ? hashInputs[cw.Id] : string.Empty ;
                if (!cw.Validate(cwVal, ref errMsg))
                {
                    sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                    goodToGo = false;
                }
            }


            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }

            //
            // Now make sure that the username and password follow the rules
            errMsg = string.Empty;
            if (!AHAHelpContent.ProviderDetails.ValidateUsernameFormat(hashInputs[this.MetadataHash["Username"].Id], ref errMsg) ||
                !AHAHelpContent.ProviderDetails.ValidatePasswordFormat(hashInputs[this.MetadataHash["Password"].Id], ref errMsg))
            {
                return JsonFailure(errMsg);
            }
            if (!hashInputs[this.MetadataHash["Password"].Id].Equals(hashInputs[this.MetadataHash["ConfirmPassword"].Id]))
            {
                return JsonFailure("Password and Confirm Password need to be the same.");
            }

            // Assign username and password to ProfileInfo class
            Username = hashInputs[this.MetadataHash["Username"].Id];
            Password = hashInputs[this.MetadataHash["Password"].Id];

            return mProfileInfoControl.ProcessFormValues(inputs);

        }

    }
}