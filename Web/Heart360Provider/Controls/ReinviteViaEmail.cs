﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider ;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class ReinviteViaEmail : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {

            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                try
                {
                    int invitationDetailsId = int.Parse(inputs[0].value);

                    // ProviderContext provider = SessionInfo.GetProviderContext();

                    AHAHelpContent.PatientProviderInvitationDetails invite = AHAHelpContent.PatientProviderInvitationDetails.FindByInvitationDetailsID(invitationDetailsId);


                    if (invite != null)
                    {
                        //populate the popup data
                        StringBuilder markup = new StringBuilder(this.PrepareMarkup());
                        markup.Replace("$InvitationCode$", inputs[0].value);
                        markup.Replace("$Fullname$", invite.FirstName + " " + invite.LastName);

                        return markup.ToString();
                    }
                }
                catch (Exception)
                {
                    //int number format exception
                }
            }

            return "Sorry, an internal error occurred. Please close popup and try again.";
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {

            bool result = false;
            string fullname = string.Empty;
            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                string inviteCode = hashInputs["txtInvitationCode"];
                ProviderContext provider = SessionInfo.GetProviderContext();
                AHAHelpContent.ProviderDetails  providerDetails = ProviderDetails.FindByProviderID( provider.ProviderID ) ;

                if (!string.IsNullOrEmpty(inviteCode))
                {
                    int invitationDetailsId = int.Parse(inviteCode); 

                    // Reinvite them
                    AHAHelpContent.PatientProviderInvitationDetails.CreateResentInvitation(invitationDetailsId);

                    AHAHelpContent.PatientProviderInvitationDetails objDetails =
                        AHAHelpContent.PatientProviderInvitationDetails.FindByInvitationDetailsID(invitationDetailsId);

                    if (objDetails != null)
                    {
                        AHAHelpContent.PatientProviderInvitation objInvitation = AHAHelpContent.PatientProviderInvitation.FindByInvitationID(objDetails.InvitationID);
                        fullname = objDetails.FirstName + " " + objDetails.LastName;
                        EmailUtils.SendInvitationToPatient(objDetails, providerDetails, objInvitation.InvitationCode);
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                //the dictionary can throw exceptions, or a bunch of other stuff
            }
             
            return (result == true) ? JsonRefresh( "REINVITE VIA EMAIL", fullname + " successfully reinvited.") : JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");

        }

    }
}