﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class AcceptInvitationPopup : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {

            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                // the id input has a pipe-delimited value set. The first element is the invitation code,
                // and the second value is the name of the person who sent the invitation.
                try
                {
                    char[]    delimiters = { '|' } ;
                    string[]  svals = inputs[0].value.Split( delimiters, 2 ) ;
                    

                    //populate the popup data
                    StringBuilder markup = new StringBuilder(this.PrepareMarkup());
                    markup.Replace("$InvitationCode$", svals[0] );
                    markup.Replace("$Fullname$", svals[1] );

                    return markup.ToString();
                }
                catch (Exception)
                {
                    //int number format exception
                }
            }

            return "Sorry, an internal error occurred (no ID). Please close popup and try again.";
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            bool result = false;
            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                string inviteCode = hashInputs["txtInvitationCode"];

                if ( !string.IsNullOrEmpty( inviteCode ) )
                {
                    // Accept the invitation. Taken from V5 Provider/Invitation/AcceptInvitation.aspx
                    AHAProvider.ProviderContext                         provider = AHAProvider.SessionInfo.GetProviderContext();

                    result = Heart360Provider.Utility.Invitations.AcceptInvitation(provider, inviteCode);
                }
            }
            catch (Exception)
            {
                //the dictionary can throw exceptions, or a bunch of other stuff
            }

            return (result == true) ? JsonRefresh( "ACCEPT INVITATION", "Patient/Participant invitation accepted.") : JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }

        
    }
}