﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class ModuleHeader : AHACommon.UXControl
    {
        // This enumeration is used to assign the proper css classes
        public enum HEADER_LOCATION
        {
            HEADER_LOCATION_DASHBOARD = 0,
            HEADER_LOCATION_MODULE,             // it is in the side-bar of (non-current) modules
            HEADER_LOCATION_MODULE_CURRENT      // it is the current module
        };

        private const int NO_ID = -1;

        private Heart360Provider.ProviderModules.MODULE_ID  mModuleID;
        private HEADER_LOCATION                             mHeaderLocation;
        private string                                      mPopupPage;
        private string                                      mPopupLink1Method;
        private string                                      mPopupLink2Method;
        private string                                      mPatientOfflineHealthRecordGuid;
        private int                                         mPatientID;

        //
        // Attributes
        //

        public Heart360Provider.ProviderModules.MODULE_ID ModuleID
        {
            get { return mModuleID; }
            set
            {
                switch (value)
                {
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_ALERTS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_GROUPS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_MESSAGES:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_PROFILE:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_RESOURCES:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_DASHBOARD:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_REPORTS:
                    case Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_SPONSORS:
                        mModuleID = value;
                        break;

                    default:
                        mModuleID = Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN;
                        break;
                }
            }
        }

        public HEADER_LOCATION HeaderLocation
        {
            get { return mHeaderLocation; }
            set { mHeaderLocation = value; }
        }

        /// <summary>
        /// The name of the page encapsulating the WebMethods used for the two popup links
        /// </summary>
        public string PopupPage
        {
            set { mPopupPage = value; }
        }

        /// <summary>
        /// The name of the WebMethod used for PopupLink1
        /// </summary>
        public string PopupLink1Method
        {
            set { mPopupLink1Method = value; }
        }

        /// <summary>
        /// The name of the WebMethod used for PopupLink2
        /// </summary>
        public string PopupLink2Method
        {
            set { mPopupLink2Method = value; }
        }

        /// <summary>
        /// For modules (really only Single Participant) that will need information about the "current patient/participant".
        /// </summary>
        public string PatientOfflineHealthRecordGuid
        {
            set { mPatientOfflineHealthRecordGuid = value; }
        }

        public int PatientID
        {
            set { mPatientID = value; }
        }

        //
        // Methods
        //

        public ModuleHeader()
            : base()
        {
            mHeaderLocation = ModuleHeader.HEADER_LOCATION.HEADER_LOCATION_MODULE;
            mModuleID = Heart360Provider.ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN;
            mPatientOfflineHealthRecordGuid = string.Empty;
            mPatientID = NO_ID;
        }

        private string PopupLinkContent( ProviderModules.POPUP_ID popid, string sDataPage, string sDataMethod, bool bIsFirst = true )
        {
            string title = "Unknown";
            string dataId = ((int)popid).ToString();
            string refreshIds = string.Empty;

            switch (popid)
            {
                case ProviderModules.POPUP_ID.POPUP_ID_COMPOSE_MESSAGE:
                    title = "Compose Message <span>+</span>";
                    // refreshIds only if current module is Messages (use mHeaderLocation)
                    // TODO: may have to add the message list for Single Participant to the refreshIds in the unlikely event it is the current module
                    if (mHeaderLocation == HEADER_LOCATION.HEADER_LOCATION_MODULE_CURRENT)
                        refreshIds = "idControlSentMessageList";
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_CREATE_ALERT:
                    title = "Create Alert Rule <span>+</span>";
                    // refreshIds = string.Format("idModuleHeader{0}", (int)ProviderModules.MODULE_ID.MODULE_ID_ALERTS);
                    if (mHeaderLocation == HEADER_LOCATION.HEADER_LOCATION_MODULE_CURRENT)
                        refreshIds = "idControlAlertRulesList";
                    else if (mHeaderLocation == HEADER_LOCATION.HEADER_LOCATION_MODULE)
                        refreshIds = "idControlGroupList";
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_CREATE_ANNOUNCEMENT:
                    title = "Create Announcement <span>+</span>";
                    refreshIds = string.Format("idModuleHeader{0}", (int)ProviderModules.MODULE_ID.MODULE_ID_RESOURCES) 
                        + ",idAnnouncementList";
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_CREATE_GROUP:
                    title = "Create Group <span>+</span>";
                        // the group module header will need to be refreshed
                    refreshIds = string.Format("idModuleHeader{0}", (int)ProviderModules.MODULE_ID.MODULE_ID_GROUPS)
                        + ",idControlGroupList" ;
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_CREATE_NOTE:
                    title = "Create Note <span>+</span>";
                    refreshIds = "idControlNoteList";
                    if( mPatientID != NO_ID )
                        dataId += "," + Heart360Provider.Controls.AddProviderNotePopup.FormatDataIds(mPatientID.ToString(), string.Empty );
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_CREATE_RESOURCE:
                    title = "Create Resource <span>+</span>";
                    refreshIds = string.Format("idModuleHeader{0}", (int)ProviderModules.MODULE_ID.MODULE_ID_RESOURCES)
                        + ",idResourceList";
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_INVITE_BY_EMAIL:
                    title = "Invite By Email <span>+</span>";
                    if( mHeaderLocation == HEADER_LOCATION.HEADER_LOCATION_MODULE_CURRENT )
                        refreshIds = "idControlSentInvitationsList";
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_INVITE_BY_PRINT:
                    title = "Invite By Print <span>+</span>";
                    // no refreshIds needed
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_CHANGE_PASSWORD:
                    title = "Change Password <span>+</span>";
                    // no refreshIds needed
                    break;

                case ProviderModules.POPUP_ID.POPUP_ID_PATIENT_MESSAGE:
                    title = "Message Patient <span>+</span>";
                    // NOTE: We have a special case where we need to append some data to the dataId for this global popup.
                    if (!string.IsNullOrEmpty(mPatientOfflineHealthRecordGuid))
                        dataId += "," + Heart360Provider.Controls.ComposeMessagePopup.FormatDataIds(string.Empty, string.Empty, mPatientOfflineHealthRecordGuid);
                    // refreshIds only if current module is Single Participant
                    if (mHeaderLocation == HEADER_LOCATION.HEADER_LOCATION_MODULE_CURRENT)
                    {
                        refreshIds = "idControlSentMessageList";
                    }
                    break;

                default:
                    break;
            }

            string classaction = (bIsFirst) ? "action" : "action-two";
            return "<div id=\"id" + sDataMethod + dataId + "\" class=\"" + classaction + " popuplink\" data-page=\"" + sDataPage + "\" data-method=\"" + sDataMethod + "\" data-id=\"" + dataId + "\" data-refresh-ids=\"" + refreshIds + "\">" + title + "</div>";
        }

        private void GetSingleParticipantInfo( ref string fullname, ref string values )
        {
            fullname = "Unknown";   // in the event of an error

            try
            {
                AHAHelpContent.Patient pat = AHAHelpContent.Patient.FindByUserHealthRecordGUID(new Guid(mPatientOfflineHealthRecordGuid));
                AHABusinessLogic.PatientInfo pati = (pat != null) ? AHABusinessLogic.PatientInfo.GetPopulatedPatientInfo(pat) : null;
                AHABusinessLogic.PatientContact pc = (pat != null) ? AHABusinessLogic.PatientContact.GetPatientContact(pat) : null;

                PhoneNumbers objPhoneNumbers = (pat != null) ? PhoneNumbers.GetPhoneNumbers(pat.UserHealthRecordID) : null ;

                if (pati != null)
                {
                    fullname = pati.FullName;

                    string cityName = (pc != null && !string.IsNullOrEmpty(pc.Address.City.Value) && !pc.Address.City.Value.Equals("-")) ? pc.Address.City.Value : string.Empty;
                    string stateName = (pc != null && !string.IsNullOrEmpty(pc.Address.State.Value) && !pc.Address.State.Value.StartsWith("Select:")) ? pc.Address.State.Value : string.Empty;


                    string yob = (pati.YearOfBirth.HasValue) ? pati.YearOfBirth.Value.ToString() : string.Empty;
                    string city = (!string.IsNullOrEmpty(cityName) && !string.IsNullOrEmpty(stateName)) ? (cityName + ", " +  stateName + " " + pc.Address.Zip.Value) : (cityName + stateName + " " + pc.Address.Zip.Value) ; 
                    string phone = (objPhoneNumbers != null) ? GRCBase.PhoneHelper.GetFormattedUSPhoneNumber(objPhoneNumbers.PhoneContact) : string.Empty ;
                    string email = (pc != null) ? pc.Email : string.Empty;
                    string lastReading = (pati.LastReadingDate.HasValue) ? pati.LastReadingDate.Value.Date.ToShortDateString() : string.Empty;

                    values = "<fieldset class='info'><p>YOB: " + yob + "</p><p>Address: " + city + "</p><p>Phone: " + phone + "</p><p>Email: " + email + "</p><p>Last Reading: " + lastReading + "</p></fieldset>";
                }
            }
            catch (Exception)
            { }
        }

        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext provider = SessionInfo.GetProviderContext();

            string markup = this.PrepareMarkup();

            //
            // First get values for string replacements
            //
            string moduleLink = AppSettings.Url.ProviderModule( mModuleID ) ;
            string title = string.Empty ;
            string mainClasses = (mHeaderLocation == HEADER_LOCATION.HEADER_LOCATION_MODULE_CURRENT) ? "tile blue big " : "tile blue ";
            string id = string.Empty;
            string popupLink1 = string.Empty;
            string popupLink2 = string.Empty;
            string leftValue = string.Empty;
            string rightValue = string.Empty;
            string verticalBar = string.Empty;


            switch (mModuleID)
            {
                case ProviderModules.MODULE_ID.MODULE_ID_ALERTS:
                    title = "Alerts" ;
                    id = "alerts";
                    if( !string.IsNullOrEmpty( mPopupPage ) && !string.IsNullOrEmpty( mPopupLink1Method ) )
                        popupLink1 = PopupLinkContent( ProviderModules.POPUP_ID.POPUP_ID_CREATE_ALERT, this.mPopupPage, this.mPopupLink1Method );
                    if (provider != null)
                    {
                        //List<AHAHelpContent.Alert> alist = AHAHelpContent.Alert.FindByProviderID(provider.ProviderID);
                        //rightValue = alist.Count.ToString();
                        rightValue = AHAHelpContent.Alert.GetNewAlertCountForProvider(provider.ProviderID).ToString();
                    }
                    else
                    {
                        rightValue = "0";
                    }
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_GROUPS:
                    title = "Groups";
                    id = "my-groups";
                    if( !string.IsNullOrEmpty( mPopupPage ) && !string.IsNullOrEmpty( mPopupLink1Method ) )
                        popupLink1 = PopupLinkContent( ProviderModules.POPUP_ID.POPUP_ID_CREATE_GROUP,  this.mPopupPage, this.mPopupLink1Method);
                    if (provider != null)
                    {
                        List<AHAHelpContent.PatientGroup> groupList = AHAHelpContent.PatientGroup.FindAllByProviderID(provider.ProviderID);
                        int count = groupList.Count;
                        count = count > 0 ? count - 1 : count; //remove the default group from the count
                        rightValue = count.ToString();
                    }
                    else
                    {
                        rightValue = "0";
                    }
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_MESSAGES:
                    title = "Messages";
                    id = "messages";
                    if( !string.IsNullOrEmpty( mPopupPage ) && !string.IsNullOrEmpty( mPopupLink1Method ) )
                        popupLink1 = PopupLinkContent( ProviderModules.POPUP_ID.POPUP_ID_COMPOSE_MESSAGE, this.mPopupPage, this.mPopupLink1Method );
                    rightValue = (provider != null) ? AHAHelpContent.Message.GetNewMessageCountForProvider(provider.ProviderID).ToString() : "0";
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS:
                    ProviderDetails providerDetails = AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID);
                    if(providerDetails.UserTypeID == (int)AHAHelpContent.DbUserType.DbUserTypeProvider)
                        title = "Patients";
                    else
                        title = "Participants";
                    id = "participants";
                    if (!string.IsNullOrEmpty(mPopupPage))
                    {
                        if (!string.IsNullOrEmpty(mPopupLink1Method))
                            popupLink1 = PopupLinkContent( ProviderModules.POPUP_ID.POPUP_ID_INVITE_BY_EMAIL, this.mPopupPage, this.mPopupLink1Method );
                        if (!string.IsNullOrEmpty(mPopupLink2Method))
                            popupLink2 = PopupLinkContent( ProviderModules.POPUP_ID.POPUP_ID_INVITE_BY_PRINT, this.mPopupPage, this.mPopupLink2Method, false );
                    }
                    verticalBar = "left-sidebar";
                    if (provider != null)
                    {
                        List<AHAHelpContent.PatientProviderInvitation> ilist = AHAHelpContent.PatientProviderInvitation.FindPendingInvitationsByAcceptedProvider(provider.ProviderID, AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Pending);
                        List<AHAHelpContent.Patient> plist = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);
                        leftValue = ilist.Count.ToString();
                        rightValue = plist.Count.ToString();
                    }
                    else
                    {
                        leftValue = rightValue = "0";
                    }
                    leftValue = "<span  class=\"tip\" title=\"New invitations to connect\" data-tip=\"New invitations to connect\">" + leftValue + "</span>";
                    rightValue = "<span class=\"tip\" title=\"Total connected patients or participants\" data-tip=\"Total connected patients or participants\">" + rightValue + "</span>";

                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_PROFILE:
                    title = "Profile";
                    id = "my-profile";
                    if (!string.IsNullOrEmpty(mPopupPage) && !string.IsNullOrEmpty(mPopupLink1Method))
                        popupLink1 = PopupLinkContent(ProviderModules.POPUP_ID.POPUP_ID_CHANGE_PASSWORD, this.mPopupPage, this.mPopupLink1Method);
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_REPORTS:
                    title = "Reports";
                    id = "reports";
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_RESOURCES:
                    title = "Resources";
                    id = "resources";
                    if (!string.IsNullOrEmpty(mPopupPage))
                    {
                        if (!string.IsNullOrEmpty(mPopupLink1Method))
                            popupLink1 = PopupLinkContent( ProviderModules.POPUP_ID.POPUP_ID_CREATE_ANNOUNCEMENT, this.mPopupPage, this.mPopupLink1Method );
                        if (!string.IsNullOrEmpty(mPopupLink2Method))
                            popupLink2 = PopupLinkContent( ProviderModules.POPUP_ID.POPUP_ID_CREATE_RESOURCE, this.mPopupPage, this.mPopupLink2Method, false );
                    }
                    verticalBar = "left-sidebar";
                    if (provider != null)
                    {
                        List<AHAHelpContent.Announcement> alist = AHAHelpContent.Resource.FindAllAnnouncements(true, AHACommon.UXControl.EnglishLanguage, provider.ProviderID);
                        List<AHAHelpContent.Resource> rlist = AHAHelpContent.Resource.FindAllResources(true, AHACommon.UXControl.EnglishLanguage, provider.ProviderID);
                        leftValue = alist.Count.ToString();
                        rightValue = rlist.Count.ToString();
                    }
                    else
                    {
                        leftValue = rightValue = "0";
                    }
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT:
                    GetSingleParticipantInfo( ref title, ref leftValue ) ;
                    id = "single-participant";
                    if (!string.IsNullOrEmpty(mPopupPage) && !string.IsNullOrEmpty(mPopupLink1Method))
                        popupLink1 = PopupLinkContent(ProviderModules.POPUP_ID.POPUP_ID_CREATE_NOTE, this.mPopupPage, this.mPopupLink1Method);
                    if (!string.IsNullOrEmpty(mPopupPage) && !string.IsNullOrEmpty(mPopupLink2Method))
                        popupLink2 = PopupLinkContent(ProviderModules.POPUP_ID.POPUP_ID_PATIENT_MESSAGE, this.mPopupPage, this.mPopupLink2Method, false);
                    //leftValue = "<p>YOB: 1960</p><p>City: Seattle, WA 98136</p><p>Phone: 206.456.3219</p><p>Email: fever@gmail.com</p><p>Last Reading: 05/17/13</p>";
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_SPONSORS:
                    title = "Sponsors";
                    id = "sponsors";
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN:
                default:
                    title = "Unknown";
                    id = "unknown";
                    break;
            }

            mainClasses += id;

            //
            // Now do the string replacements
            //

            markup = markup.Replace("$Module$", id);
            markup = markup.Replace("$MainClasses$", mainClasses);
            markup = markup.Replace("$ModuleLink$", moduleLink);
            markup = markup.Replace("$ModuleTitle$", title);
            markup = markup.Replace("$PopupLink1$", popupLink1);
            markup = markup.Replace("$PopupLink2$", popupLink2);
            markup = markup.Replace("$ModuleLeftValue$", leftValue);
            markup = markup.Replace("$ModuleRightValue$", rightValue);
            markup = markup.Replace("$VerticalBar$", verticalBar);

            return markup;
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }
    }
}