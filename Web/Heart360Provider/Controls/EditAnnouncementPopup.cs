﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class EditAnnouncementPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            PatientContent announcement = null;

            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                try
                {
                    int announcementId = Convert.ToInt32(inputs[0].value);
                    announcement = AHAHelpContent.PatientContent.FindPatientContentByID(announcementId, EnglishLanguage);
                }
                catch (Exception)
                {
                }
            }

            if (announcement == null)
            {
                return "Sorry, an internal error occurred. Please close popup and try again.";
            }

            ProviderContext provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderDetails provDetails = (provider != null) ? AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID) : null;

            StringBuilder markup = new StringBuilder(this.PrepareMarkup());

            //popuplate the default data
            markup.Replace("$Id$", announcement.PatientContentID.ToString());
            markup.Replace("$Title$", announcement.ContentTitle);
            markup.Replace("$Content$", announcement.ContentText);
            markup.Replace("$ProviderName$", provDetails.FormattedName);

            return markup.ToString();
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                ProviderContext provider = SessionInfo.GetProviderContext();
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                // validate required fields
                bool goodToGo = true;
                StringBuilder sb = new StringBuilder();
                string errMsg = string.Empty;

                // Loop over meta-data and do processing.
                foreach (ControlWidget cw in this.Metadata.Widgets)
                {
                    if (!cw.Validate(hashInputs[cw.Id], ref errMsg))
                    {
                        sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                        goodToGo = false;
                    }
                }

                if (!goodToGo)
                {
                    return JsonFailure(sb.ToString());
                }

                int announcementId;
                if (!Int32.TryParse(hashInputs[this.MetadataHash["Id"].Id], out announcementId))
                {
                    return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
                }

                //create announcement
                AHAHelpContent.PatientContent.UpdateContent(announcementId, AHACommon.AHADefs.ContentType.PatientAnnouncement.ToString(),
                   hashInputs[this.MetadataHash["Title"].Id], null, hashInputs[this.MetadataHash["Content"].Id],
                   provider.ProviderID, EnglishLanguage, provider.ProviderID);

                return JsonRefresh("EDIT ANNOUNCEMENT", "Announcement updated.");
            }
            catch (Exception) { }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }
    }
}