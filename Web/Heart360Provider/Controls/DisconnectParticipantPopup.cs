﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class DisconnectParticipantPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {

            ProviderContext provider = SessionInfo.GetProviderContext();

            // We need to get the patient/participant identifier from input
            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                try
                {
                    Guid patientOfflineRecordGuid = Guid.Parse(inputs[0].value);

                    AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserOfflineHealthRecordGUID(patientOfflineRecordGuid);

                    HVManager.PersonalDataManager.PersonalItem objPatientDetails = Heart360Provider.Utility.PatientDetails.GetPatientDetails(objPatient);

                    StringBuilder markup = new StringBuilder(this.PrepareMarkup());

                    markup.Replace("$ParticipantName$", objPatientDetails.FullName);
                    markup.Replace("$ParticipantId$", objPatient.OfflineHealthRecordGUID.ToString() ) ;

                    return markup.ToString();
                }
                catch (Exception)
                { }
            }
            return UXControl.GenericHtmlErrorMessage;
        }


        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                ProviderContext provider = SessionInfo.GetProviderContext();
                AHAHelpContent.ProviderDetails providerDetails = AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID);

                Guid participantHealthRecordGuid = new Guid(hashInputs["txtParticipantId"]);
                AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordGUID(participantHealthRecordGuid);
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                HVManager.PersonalDataManager.PersonalItem objPersonal = mgr.PersonalDataManager.Item;

                string strPatientEmail = string.Empty;

                if (mgr.PersonalContactDataManager.Item != null && mgr.PersonalContactDataManager.Item.EmailList.Count > 0)
                {
                    strPatientEmail = mgr.PersonalContactDataManager.Item.EmailList[0].Email.Value;
                }

                AHAHelpContent.ProviderDetails.DisconnectPatientOrProvider(provider.ProviderID, objPatient.UserHealthRecordID);

                string strPatientName = string.Format("{0} {1}", objPersonal.FirstName.Value, objPersonal.LastName.Value);

                if (!string.IsNullOrEmpty(strPatientEmail))
                {
                    string strProviderSalutation = providerDetails.Salutation;
                    string strProviderName = providerDetails.FormattedName;

                    EmailUtils.SendDisconnectPatientEmail(strPatientName, strPatientEmail, strProviderName, strProviderSalutation);
                }

                return JsonRefresh("DISCONNECT PATIENT/PARTICIPANT", "Successfully disconnected from " + strPatientName );
            }
            catch (Exception)
            { }


            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }

    }
}