﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class PatientList : AHACommon.UXControl
    {
        // Remember that the search is applied to the current group!
        private static int  ALL_PATIENTS_GROUP_ID = -1 ;

        // Need to store the current group from the group drop-down.
        protected bool      mDoSearch = false ;     // if true, the list displays current group. If false, the list displays search results
        protected string    mSearchString = string.Empty;
        protected int       mCurrentGroupId = ALL_PATIENTS_GROUP_ID ;

        override public string GetMarkup(NameValue[] inputs)
        {
            // Need to get the list of groups for the <select> widget
            // Need to get the list of patients for the list.

            ProviderContext     provider = SessionInfo.GetProviderContext();
            List<PatientGroup>  groupList = (provider != null) ? PatientGroup.FindAllByProviderID(provider.ProviderID) : null;

            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.DropDown:
                        if( cw.Name.Equals( "GroupFilter" ) )
                        {
                            cw.Value = GetGroupsDropdownContent(groupList) ;
                        }
                        break ;

                    case ControlWidgetType.String:
                        if (cw.Name.Equals("SearchFilter"))
                        {
                            cw.Value = mSearchString;
                        }
                        break;

                    default:
                        break;

                }
            }

            string markup = this.PrepareMarkup();

         
            //
            // Now do Patients
            //
            StringBuilder                   sb = new StringBuilder() ;

            List<AHAHelpContent.Patient>    plist = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);

            if (plist != null && plist.Count > 0)
            {
                // We need to have 'plist' be the patient list for the current group
                AHAHelpContent.PatientGroup currgrp = GetCurrentGroup( groupList ) ;
                if( currgrp != null )
                {
                    plist = plist.Where(P => P.GroupIDList.Split(",".ToCharArray()).Contains(currgrp.GroupID.ToString())).ToList();
                }

                // Now get extended patient class with personal information
                List<AHABusinessLogic.PatientInfo> lstPatientInfo = AHABusinessLogic.PatientInfo.GetPopulatedPatientInfo(plist);

                // If there is a search, we need to potentially limit plist even more.
                if (mDoSearch)
                {
                    // This is taken from V5 AHAWeb/UserCtrls/Provider/Patient/AllPatients.ascx.cs
                    // Looks like you can only search on date of birth or name
                    // PJB: In V6, you can search on Year of Birth, not date of birth.
                    int yob = 0;
                    bool bIsValidYear = int.TryParse(mSearchString, out yob);
                    if( bIsValidYear && yob >= 1800 && yob <= 3000 )
                    {
                        lstPatientInfo = lstPatientInfo.Where(PU => PU.YearOfBirth.Value == yob).ToList();
                    }
                    else
                    {
                        lstPatientInfo = lstPatientInfo.Where(PU => PU.FullName.ToLower().Contains(mSearchString)).ToList();
                    }
                }

                List<GroupManager.GroupInfo> riskGroups = Heart360Global.Main.GetSystemCreatedGroups();
                int? daysOffset = AHACommon.AHAAppSettings.ColorCoding_EffectiveDays;



                foreach (AHABusinessLogic.PatientInfo pi in lstPatientInfo)
                {
                    AHAHelpContent.Patient pat = AHAHelpContent.Patient.FindByUserHealthRecordGUID(pi.RecordGUID);
                    AHABusinessLogic.PatientContact pc = (pat != null) ? AHABusinessLogic.PatientContact.GetPatientContact(pat) : null;

                    string noValue = "-";

                    // name as hyperlink
                    sb.Append("<tr><td>" 
                        + Heart360Provider.Utility.UIHelper.GetHtmlAtagForPatientInfoWithAlertCount(pi) 
                        + "<br/><a href='mailto:" + pc.Email + "'>" + pc.Email + "</a>"
                        + "<br/>" + GRCBase.PhoneHelper.GetFormattedUSPhoneNumber(pc.ContactPhone)
                        + "</td>");
                    // last reading date
                    sb.Append("<td>" + ((pi.LastReadingDate.HasValue) ? pi.LastReadingDate.Value.ToShortDateString() : noValue) + "</td>");
                    // blood pressure
                    if (pi.Systolic.HasValue && pi.Diastolic.HasValue)
                    {
                        if ((daysOffset == null) || pi.BloodPressureEffectiveDate.CompareTo(DateTime.Now.AddDays(daysOffset.Value)) == 1)
                        {
                            AHABusinessLogic.RiskLevelHelper.RiskLevelBloodPressure bp = new AHABusinessLogic.RiskLevelHelper.RiskLevelBloodPressure( riskGroups, pi.Systolic.Value, pi.Diastolic.Value );
                            sb.Append( "<td><span class=\"" + bp.ColorReading1 + "\">" + pi.Systolic.Value.ToString() + "</span> | <span class=\"" + bp.ColorReading2 + "\">" + pi.Diastolic.Value.ToString() + "</span></td>" ) ;
                        }
                        else
                        {
                            sb.Append("<td>" + pi.Systolic.Value.ToString() + " | " + pi.Diastolic.Value.ToString() + "</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td>" +noValue + "</td>");
                    }

                    // blood glucose
                    if (pi.BloodGlucose.HasValue)
                    {
                        if ((daysOffset == null) || pi.BloodGlucoseEffectiveDate.CompareTo(DateTime.Now.AddDays(daysOffset.Value)) == 1)
                        {
                            AHABusinessLogic.RiskLevelHelper.RiskLevelBloodGlucose bg = new AHABusinessLogic.RiskLevelHelper.RiskLevelBloodGlucose(riskGroups, pi.BloodGlucose.Value);
                            sb.Append("<td><span class=\"" + bg.ColorReading1 + "\">" + pi.BloodGlucose.Value.ToString() + "</span></td>");
                        }
                        else
                        {
                            //GRCBase.StringHelper.GetFormattedDoubleString(pi.BloodGlucose.Value, 2, false)
                            sb.Append("<td>" + pi.BloodGlucose.Value.ToString() + "</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td>" + noValue + "</td>");
                    }

                    // cholesterol
                    if (pi.Cholesterol.HasValue)
                    {
                        if ((daysOffset == null) || pi.CholesterolEffectiveDate.CompareTo(DateTime.Now.AddDays(daysOffset.Value)) == 1)
                        {
                            AHABusinessLogic.RiskLevelHelper.RiskLevelCholesterol rlc = new AHABusinessLogic.RiskLevelHelper.RiskLevelCholesterol(riskGroups, pi.Cholesterol.Value, 0, 0, 0, true);
                            sb.Append("<td><span class=\"" + rlc.ColorReading1 + "\">" + pi.Cholesterol.Value.ToString() + "</span></td>");
                        }
                        else
                        {
                            sb.Append("<td>" + pi.Cholesterol.Value.ToString() + "</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td>" + noValue + "</td>");
                    }

                    // weight
                    if (pi.Weight.HasValue)
                    {
                        string sWeight = pi.Weight.Value.ToString();
                        sWeight = sWeight.Substring(0, 3);

                        sb.Append("<td>" + sWeight + "</td>");
                    }
                    else
                    {
                        sb.Append("<td>" + noValue + "</td>");
                    }
                   

                    // disconnect action - TODO: FIGURE OUT WHAT ID VALUES TO PASS
                    sb.Append("<td class=\"action-column\"><div class=\"trash-icon popuplink\" data-page=\"Pages/Participants\" data-method=\"DisconnectPatientPopupHandler\" data-id=\"" + pi.RecordGUID.ToString() + "\" data-refresh-ids=\"idModuleHeader4,idControlPatientList\"></div></td></tr>");

                }

            }

            markup = markup.Replace("$ParticipantList$", sb.ToString());
            markup = markup.Replace("$ContactListReportNote$", HttpContext.GetGlobalResourceObject("Provider", "Text_Report_PatientContactList_Note").ToString());
            return markup;
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            //
            // All we do is scrape off current group selection and any search parameters.
            // Unless, of course, we are printing....
            //
            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            //
            // Determine if we are printing, and if so, do that and return
            //
            if (hashInputs.ContainsKey(UXControl.InputsKeyword_Print) && hashInputs[UXControl.InputsKeyword_Print].ToLower().Equals(UXControl.InputsValue_True))
            {
                return JsonPrint(GetMarkup(inputs));
            }

            //
            // If we are here, we are not printing.
            //
            if (string.IsNullOrEmpty(hashInputs[this.MetadataHash["GroupFilter"].Id]))
            {
                mCurrentGroupId = ALL_PATIENTS_GROUP_ID;
            }
            else
            {
                mCurrentGroupId = Convert.ToInt32(hashInputs[this.MetadataHash["GroupFilter"].Id]);
            }

            string ttt = hashInputs[this.MetadataHash["SearchFilter"].Id];

            if (!string.IsNullOrEmpty(ttt))
            {
                mSearchString = ttt;
                mDoSearch = true;
            }
            else
            {
                mSearchString = string.Empty;
                mDoSearch = false;
            }

            return JsonRefresh("");
        }

        private string GetGroupsDropdownContent(List<PatientGroup> groupList)
        {
            if (groupList == null)
                return string.Empty;


            bool checkCurrValue = true;
            StringBuilder sb = new StringBuilder();

            if (mCurrentGroupId < 0)
            {
                sb.Append("<option selected=\"selected\" value=\"\">All Patients/Participants</option>");
                checkCurrValue = false;
            }
            else
            {
                sb.Append("<option value=\"\">All Patients/Participants</option>");
            }

            foreach( PatientGroup pg in groupList )
            {
                if (!pg.IsDefault)
                {
                    if (checkCurrValue && pg.GroupID == mCurrentGroupId)
                    {
                        sb.Append("<option selected=\"selected\" value=\"" + pg.GroupID.ToString() + "\">" + pg.Name + " (" + pg.TotalMembers.ToString() + ")</option>");
                        checkCurrValue = false;
                    }
                    else
                    {
                        sb.Append("<option value=\"" + pg.GroupID.ToString() + "\">" + pg.Name + " (" + pg.TotalMembers.ToString() + ")</option>");
                    }
                }
            }

            return sb.ToString();
        }

        private AHAHelpContent.PatientGroup GetCurrentGroup(List<PatientGroup> groupList)
        {
            AHAHelpContent.PatientGroup result = null;

            if (groupList != null)
            {
                foreach (PatientGroup pg in groupList)
                {
                    if (pg.GroupID == mCurrentGroupId)
                    {
                        result = pg;
                        break;
                    }
                }
            }
            return result;
        }
    
    }
}