﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class AddProviderNotePopup : AHACommon.UXControl
    {
        private const int NO_ID = -1;
        private int mProviderNoteID = NO_ID;
        private int mPatientID = NO_ID;


        public static string FormatDataIds(string sPatientID, string sProviderNoteID)
        {
            return sPatientID + "," + sProviderNoteID;
        }

        override public string GetMarkup(NameValue[] inputs)
        {
            ProviderContext provider = SessionInfo.GetProviderContext();
            bool            goodToGo = true;
            ProviderNote    objProviderNote = null;    // this will be non-null if editing

            // For testing
            this.Initialize();

            // Now figure out the use case by what ids were given
            if (inputs.Length > 0 && inputs[0].name.Equals("id") && !string.IsNullOrEmpty(inputs[0].value))
            {
                char[] delimiter = { ',' };
                string[] inIds = inputs[0].value.Split(delimiter);

                if (inIds.Length > 0 && !string.IsNullOrEmpty(inIds[0]) )
                    goodToGo = int.TryParse(inIds[0], out mPatientID);
                if (inIds.Length > 1 && !string.IsNullOrEmpty(inIds[1]))
                {
                    goodToGo = (!int.TryParse(inIds[1], out mProviderNoteID)) ? false : goodToGo;
                    if (goodToGo)
                    {
                        objProviderNote = AHAHelpContent.ProviderNote.FindByProviderNoteID(mProviderNoteID);
                        if (objProviderNote == null)
                            goodToGo = false;
                    }
                }
            }

            if (!goodToGo)
                return UXControl.FormatErrorPopupContent("Add/Edit Note");



            // Assign default values so they will be injected in markup.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.Date:
                        if (cw.Name.Equals("Date"))
                        {
                            cw.Value = (objProviderNote != null) ? objProviderNote.DateCreated.ToShortDateString() : string.Empty ;
                        }
                        break;

                    case ControlWidgetType.String:
                        if (cw.Name.Equals("Title"))
                        {
                            cw.Value = (objProviderNote != null) ? objProviderNote.Title : string.Empty;
                        }
                        else if (cw.Name.Equals("Note"))
                        {
                            cw.Value = (objProviderNote != null) ? objProviderNote.Note : string.Empty;
                        }
                        break;


                    default:
                        break;
                }
            }

            string markup = this.PrepareMarkup().Replace( "$Title$", (objProviderNote != null) ? "EDIT NOTE" : "ADD NOTE" ) ;
            return markup.Replace("$DisableCreationDate$", ((objProviderNote != null) ? "disabled=\"disabled\"" : string.Empty));
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Save button.
            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                // The Creation Date widget is readonly when editing a Provider Note
                bool validateIt = (mProviderNoteID == NO_ID || !cw.Name.Equals("Date")) ? true : false;
                if (validateIt && !cw.Validate(hashInputs[cw.Id], ref errMsg))
                {
                    sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                    goodToGo = false;
                }
            }

            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }

            //return JsonFailure("Unimplemented Functionality...");

            // get together all the values needed for the DAL call
           

            if (mProviderNoteID != NO_ID)
            {
                AHAHelpContent.ProviderNote.UpdateProviderNote(mProviderNoteID, 
                                                                (hashInputs.ContainsKey("tTitle") ? hashInputs["tTitle"] : string.Empty),
                                                                (hashInputs.ContainsKey("taNote") ? hashInputs["taNote"] : string.Empty) ) ;
                return JsonRefresh( "EDIT NOTE", "Successfully updated the Note");
            }
            else
            {
                ProviderContext provider = SessionInfo.GetProviderContext();
                DateTime dtCreated = DateTime.Parse(hashInputs["tDate"]);

                // Verify that the date is not in the future!
                if (dtCreated.CompareTo(DateTime.Now) > 0)
                {
                    return JsonFailure("The date specified cannot be in the future.");
                }

                AHAHelpContent.ProviderNote.CreateProviderNote(provider.ProviderID, mPatientID, dtCreated, hashInputs["tTitle"], (hashInputs.ContainsKey("taNote") ? hashInputs["taNote"] : string.Empty) );
                return JsonRefresh( "ADD NOTE", "Successfully created the Note");
            }
        }
    }
}