﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAProvider;


namespace Heart360Provider.Controls
{
    public class AlertSettings : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {

            ProviderContext provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderDetails providerDetails = (provider != null) ? AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID) : null;

            if (providerDetails == null)
            {
                return UXControl.GenericHtmlErrorMessage;
            }

            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.Email:
                        switch (cw.Name)
                        {
                            case "NotificationEmail":
                                cw.Value = providerDetails.EmailForIndividualMessage;
                                break;

                            case "DailySummaryEmail":
                                cw.Value = providerDetails.EmailForDailySummary;
                                break;

                            default:
                                break;
                        }
                        break;

                    case ControlWidgetType.CheckBox:
                        switch (cw.Name)
                        {
                            case "NewAlertNotification":
                                cw.Value = (providerDetails.NotifyNewAlertImmediately.HasValue && providerDetails.NotifyNewAlertImmediately.Value == true) ? "true" : string.Empty;
                                break;

                            case "DailySummaryAlert":
                                cw.Value = (providerDetails.IncludeAlertNotificationInDailySummary.HasValue && providerDetails.IncludeAlertNotificationInDailySummary.Value == true) ? "true" : string.Empty;
                                break;

                            case "DailySummaryMessage":
                                cw.Value = (providerDetails.IncludeMessageNotificationInDailySummary.HasValue && providerDetails.IncludeMessageNotificationInDailySummary.Value == true) ? "true" : string.Empty;
                                break;

                            default:
                                break;
                        }
                        break;

                    case ControlWidgetType.DropDown:
                        if (cw.Name.Equals("DismissAlerts"))
                        {
                            cw.Value = GetDismissDropdownContent( providerDetails.DismissAlertAfterWeeks );
                        }
                        break;

                    default:
                        break;

                }
            }


            // call the base class to do all the string comparison
            return this.PrepareMarkup();

        }

        private string GetDismissDropdownContent( int afterWeeks )
        {
            List<AHAHelpContent.ListItem> objDismissAlertList = AHAHelpContent.ListItem.FindListItemsByListCode(AHADefs.VocabDefs.DissmissAlert, H360Utility.GetCurrentLanguageID());

            StringBuilder sb = new StringBuilder();

            foreach (AHAHelpContent.ListItem li in objDismissAlertList)
            {
                if( Convert.ToInt32( li.ListItemCode ) == afterWeeks )
                    sb.Append("<option selected=\"selected\" value=\"" + li.ListItemCode + "\">" + li.ListItemName + "</option>");
                else
                    sb.Append("<option value=\"" + li.ListItemCode + "\">" + li.ListItemName + "</option>");
            }

            return sb.ToString();
        }


        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Update button.

            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                // if a radio button is not checked, then that widget is not passed in inputs. Have to test for existance.
                string valToCheck = (hashInputs.ContainsKey(cw.Id)) ? hashInputs[cw.Id] : string.Empty;
                if (!cw.Validate(valToCheck, ref errMsg))
                {

                    sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                    goodToGo = false;
                }
            }


            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }


            ProviderContext provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderDetails providerDetails = (provider != null) ? AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID) : null;

            if (providerDetails != null)
            {
                // update values in providerDetails
                providerDetails.DismissAlertAfterWeeks = Convert.ToInt32(hashInputs["ddlDismiss"]);
                providerDetails.NotifyNewAlertImmediately = (hashInputs.ContainsKey("cbNewAlertNotification") && hashInputs["cbNewAlertNotification"].Equals(UXControl.HtmlCheckBoxChecked)) ? true : false;
                providerDetails.IncludeAlertNotificationInDailySummary = (hashInputs.ContainsKey("cbDailySummaryAlert") && hashInputs["cbDailySummaryAlert"].Equals(UXControl.HtmlCheckBoxChecked)) ? true : false;
                providerDetails.IncludeMessageNotificationInDailySummary = (hashInputs.ContainsKey("cbDailySummaryMessage") && hashInputs["cbDailySummaryMessage"].Equals(UXControl.HtmlCheckBoxChecked)) ? true : false;
                if (providerDetails.NotifyNewAlertImmediately.Value)
                {
                    // this handles a corner case in that we cannot make the email field required
                    if (string.IsNullOrEmpty(hashInputs["txtNotificationEmail"]))
                        return JsonFailure("Notification Email is required");
                    providerDetails.EmailForIndividualMessage = hashInputs["txtNotificationEmail"];
                }
                if (providerDetails.IncludeMessageNotificationInDailySummary.Value || providerDetails.IncludeAlertNotificationInDailySummary.Value)
                {
                    // this handles a corner case in that we cannot make the email field required
                    if (string.IsNullOrEmpty(hashInputs["txtDailySummaryEmail"]))
                        return JsonFailure("Daily Summary Email is required.");
                    providerDetails.EmailForDailySummary = hashInputs["txtDailySummaryEmail"];
                }

                try
                {
                    AHAHelpContent.ProviderDetails.UpdateProvider(provider.ProviderID,
                                                                    providerDetails.Email,
                                                                    providerDetails.FirstName,
                                                                    providerDetails.LastName,
                                                                    providerDetails.Salutation,
                                                                    providerDetails.PracticeName,
                                                                    providerDetails.PracticeEmail,
                                                                    providerDetails.PracticeAddress,
                                                                    providerDetails.PracticePhone,
                                                                    providerDetails.PracticeFax,
                                                                    providerDetails.Speciality,
                                                                    providerDetails.PharmacyPartner,
                                                                    providerDetails.ProfileMessage,
                                                                    providerDetails.PracticeUrl,
                                                                    providerDetails.City,
                                                                    providerDetails.StateID,
                                                                    providerDetails.Zip,
                                                                    providerDetails.AllowPatientsToSendMessages,
                                                                    providerDetails.IncludeAlertNotificationInDailySummary,
                                                                    providerDetails.IncludeMessageNotificationInDailySummary,
                                                                    providerDetails.NotifyNewAlertImmediately,
                                                                    providerDetails.NotifyNewMessageImmediately,
                                                                    providerDetails.DismissAlertAfterWeeks,
                                                                    providerDetails.EmailForDailySummary,
                                                                    providerDetails.EmailForIndividualAlert,
                                                                    providerDetails.EmailForIndividualMessage,
                                                                    providerDetails.AllowAHAEmail);

                    return JsonSuccess("Successfully updated Alert Settings");
                }
                catch (Exception)
                { }
            }

            return JsonFailure("Error saving your Alert Settings.");

        }

    }
}