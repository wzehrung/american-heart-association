﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    /// <summary>
    /// The AddAlertRulePopup is used to create a new Alert Rule and to edit an existing one.
    /// When present, the "id" parameter represents the ID of an Alert Rule to edit.
    /// 
    /// Use cases:
    ///     1. no AlertRuleID => Create a new alert rule
    ///     2. AlertRuleID => Edit an existing alert rule
    /// </summary>
    public class AddAlertRulePopup : AHACommon.UXControl
    {
        private enum RadioButton
        {
            RadioButton_None,
            RadioButton_Global,
            RadioButton_Group,
            RadioButton_Patient
        }

        private int mAlertRuleID;

        override public string GetMarkup(NameValue[] inputs)
        {
            ProviderContext                 provider = SessionInfo.GetProviderContext();
            List<AHAHelpContent.Patient>    plist = (provider != null) ? AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID) : null;

            // Note: even if the provider/volunteer doesn't have any patient/participants or any groups,
            // they still need to be able to configure alert rules.
            bool disablePatient = (plist == null || plist.Count == 0) ? true : false;
            bool disableGroups = false;
                
            // For testing
            //this.Initialize();


            // Now figure out the Use Case
            string strElementID = string.Empty;
            string handling = string.Empty;
            RadioButton radioCurrent = RadioButton.RadioButton_Global;

            if (inputs.Length > 0)
            {
                if (inputs[0].name.Equals("id") && !string.IsNullOrEmpty(inputs[0].value))
                {
                    strElementID = inputs[0].value;
                }
                //look for special handling
                foreach (NameValue nv in inputs)
                {
                    if (nv.name.Equals("handling"))
                    {
                        handling = nv.value;
                    }
                }
            }

            AHAHelpContent.Patient      defaultPatient = null ;
            int?                        defaultGroupID = null ;
            AHAHelpContent.AlertRule    currAlertRule = null ;
            

                // these are the classes that hold the specific alert rule values
            AlertManager.WeightAlertRule        arWeight = null ;
            AlertManager.BloodGlucoseAlertRule  arBloodGlucose = null ;
            AlertManager.BloodPressureAlertRule arBloodPressure = null ;

            string title = "ADD ALERT RULE";
            mAlertRuleID = -1;
            bool editMode = false;

            if (handling.Equals("preselect-group")) //from GroupList, preselect the group
            {
                radioCurrent = RadioButton.RadioButton_Group;
                defaultGroupID = Convert.ToInt32(strElementID); // ID is of group to select
            }
            else if (!string.IsNullOrEmpty(strElementID)) // check Use Case 2, edit alert rule
            {
                editMode = true;

                mAlertRuleID = Convert.ToInt32(strElementID); // ID of alert rule

                currAlertRule = AHAHelpContent.AlertRule.FindByAlertRuleID(mAlertRuleID);

                if (currAlertRule != null)
                {
                    title = "EDIT ALERT RULE";
                    if (currAlertRule.AlertRuleSubscriberType == AlertRuleSubscriberType.A)
                    {
                        radioCurrent = RadioButton.RadioButton_Global;
                    }
                    else if (currAlertRule.AlertRuleSubscriberType == AlertRuleSubscriberType.G)
                    {
                        radioCurrent = RadioButton.RadioButton_Group;
                        defaultGroupID = currAlertRule.GroupID;
                    }
                    else if (currAlertRule.AlertRuleSubscriberType == AlertRuleSubscriberType.P)
                    {
                        AHAHelpContent.Patient psearch = AHAHelpContent.Patient.FindByUserHealthRecordID(currAlertRule.PatientID.Value);

                        foreach (AHAHelpContent.Patient pat in plist)
                        {
                            if (pat.PersonalItemGUID.Equals(psearch.PersonalItemGUID))
                            {
                                defaultPatient = pat;
                                radioCurrent = RadioButton.RadioButton_Patient;
                                break;
                            }
                        }
                    }

                    switch (currAlertRule.Type)
                    {
                        case TrackerDataType.BloodGlucose:
                            arBloodGlucose = AlertManager.BloodGlucoseAlertRule.ParseAlertRuleXml(currAlertRule.AlertRuleData);
                            break;

                        case TrackerDataType.BloodPressure:
                            arBloodPressure = AlertManager.BloodPressureAlertRule.ParseAlertRuleXml(currAlertRule.AlertRuleData);
                            break;

                        case TrackerDataType.Weight:
                            arWeight = AlertManager.WeightAlertRule.ParseAlertRuleXml(currAlertRule.AlertRuleData);
                            break;

                        default:
                            break;
                    }

                }
                else
                {
                    mAlertRuleID = -1;
                }
            }

            // Assign default values so they will be injected in markup.
            // we need to call GetGroupDropdownContent() before we loop because it can change radioCurrent
            string groupDropdown = GetGroupDropdownContent(defaultGroupID, ref radioCurrent, ref disableGroups );
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.DropDown:
                        if (cw.Name.Equals("Groups"))
                        {
                            cw.Value = groupDropdown;
                        }
                        else if (cw.Name.Equals("Patients"))
                        {
                            cw.Value = GetPatientDropdownContent( plist, defaultPatient );
                        }
                        else if (cw.Name.Equals("Conditions"))
                        {
                            string currCondition = string.Empty;
                            cw.Value = GetConditionDropdownContent( (currAlertRule != null) ? currAlertRule.Type.ToString() : string.Empty );
                        }
                        else if (cw.Name.Equals("Period"))
                        {
                            cw.Value = GetPeriodDropdownContent( (arWeight != null && arWeight.PeriodType == PeriodType.D) ? arWeight.PeriodCount : 0 );
                        }
                        break;

                    case ControlWidgetType.String:
                        if (cw.Name.Equals("Message"))
                        {
                            cw.Value = (currAlertRule != null) ? currAlertRule.MessageToPatient : 
                                "You have entered a reading that has triggered an alert set by your Provider/Volunteer.";
                        }
                        break;

                    case ControlWidgetType.Radio:
                        if (cw.Name.Equals("RadioGlobal"))
                        {
                            cw.Value = (radioCurrent == RadioButton.RadioButton_Global) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioGroup"))
                        {
                            cw.Value = (radioCurrent == RadioButton.RadioButton_Group) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioPatient"))
                        {
                            cw.Value = (radioCurrent == RadioButton.RadioButton_Patient) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioBgAbove"))
                        {
                            if (!editMode || arBloodGlucose == null)
                                cw.Value = "true"; //default value
                            else
                                cw.Value = (arBloodGlucose != null && arBloodGlucose.IsAbove) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioBgBelow"))
                        {
                            cw.Value = (arBloodGlucose != null && !arBloodGlucose.IsAbove) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioSystolicAbove"))
                        {
                            if (!editMode || arBloodPressure == null)
                                cw.Value = "true"; //default value
                            else
                                cw.Value = (arBloodPressure != null && arBloodPressure.IsSystolicAbove.HasValue && arBloodPressure.IsSystolicAbove.Value) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioSystolicBelow"))
                        {
                            cw.Value = (arBloodPressure != null && arBloodPressure.IsSystolicAbove.HasValue && !arBloodPressure.IsSystolicAbove.Value) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioDiastolicAbove"))
                        {
                            if (!editMode || arBloodPressure == null)
                                cw.Value = "true"; //default value
                            else
                                cw.Value = (arBloodPressure != null && arBloodPressure.IsDiastolicAbove.HasValue && arBloodPressure.IsDiastolicAbove.Value) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioDiastolicBelow"))
                        {
                            cw.Value = (arBloodPressure != null && arBloodPressure.IsDiastolicAbove.HasValue && !arBloodPressure.IsDiastolicAbove.Value) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioHeartRateAbove"))
                        {
                            if (!editMode || arBloodPressure == null)
                                cw.Value = "true"; //default value
                            else
                                cw.Value = (arBloodPressure != null && arBloodPressure.IsHeartRateAbove.HasValue && arBloodPressure.IsHeartRateAbove.Value) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioHeartRateBelow"))
                        {
                            cw.Value = (arBloodPressure != null && arBloodPressure.IsHeartRateAbove.HasValue && !arBloodPressure.IsHeartRateAbove.Value) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioWeightGain"))
                        {
                            if (!editMode || arWeight == null)
                                cw.Value = "true"; //default value
                            else
                                cw.Value = (arWeight != null && arWeight.IsGain) ? "true" : string.Empty;
                        }
                        else if (cw.Name.Equals("RadioWeightLoss"))
                        {
                            cw.Value = (arWeight != null && !arWeight.IsGain) ? "true" : string.Empty;
                        }
                        break;

                    case ControlWidgetType.Integer:
                        if (cw.Name.Equals("SystolicValue"))
                        {
                            //cw.Value = (arBloodPressure != null && arBloodPressure.IsSystolicAbove.HasValue && arBloodPressure.IsSystolicAbove.Value && arBloodPressure.Systolic.HasValue) ? arBloodPressure.Systolic.Value.ToString() : string.Empty;
                            cw.Value = (arBloodPressure != null && arBloodPressure.Systolic.HasValue) ? arBloodPressure.Systolic.Value.ToString() : string.Empty;
                        }
                        else if (cw.Name.Equals("DiastolicValue"))
                        {
                            cw.Value = (arBloodPressure != null && arBloodPressure.Diastolic.HasValue) ? arBloodPressure.Diastolic.Value.ToString() : string.Empty;
                        }
                        else if (cw.Name.Equals("HeartRateValue"))
                        {
                            cw.Value = (arBloodPressure != null && arBloodPressure.HeartRate.HasValue) ? arBloodPressure.HeartRate.Value.ToString() : string.Empty;
                        }
                        break;

                    case ControlWidgetType.Double:
                        if (cw.Name.Equals("BgValue"))
                        {
                            cw.Value = (arBloodGlucose != null) ? arBloodGlucose.Value.ToString() : string.Empty;
                        }
                        else if (cw.Name.Equals("WeightValue"))
                        {
                            cw.Value = (arWeight != null) ? arWeight.Value.ToString() : string.Empty;
                        }
                        break ;

                    case ControlWidgetType.CheckBox:
                        if (cw.Name.Equals("SendPatientMessage"))
                        {
                            cw.Value = (currAlertRule != null && currAlertRule.SendMessageToPatient.HasValue && currAlertRule.SendMessageToPatient.Value) ? "true" : string.Empty;
                        }
                        break;

                    default:
                        break;
                }
            }


            string  markup = this.PrepareMarkup();  // this will do string replacement of values in metadata
            markup = markup.Replace("$disablePatient$", ((disablePatient) ? "disabled=\"disabled\"" : string.Empty));
            markup = markup.Replace("$disableGroups$", ((disableGroups) ? "disabled=\"disabled\"" : string.Empty));
            return markup.Replace("$Title$", title);
            
        }

        private string GetGroupDropdownContent( int? defaultGroupID, ref RadioButton currentRadio, ref bool disableGroups )
        {
            ProviderContext     provider = SessionInfo.GetProviderContext();
            List<PatientGroup>  groupList = (provider != null) ? PatientGroup.FindAllByProviderID(provider.ProviderID) : null;
            int                 addedGroupCount = 0;

            StringBuilder       sb = new StringBuilder();

            if (groupList != null && groupList.Count > 0 )
            {
                foreach (AHAHelpContent.PatientGroup pg in groupList)
                {
                    // do not include the default group
                    if (!pg.IsDefault )
                    {
                        if (!defaultGroupID.HasValue || defaultGroupID.Value != pg.GroupID)
                        {
                            sb.Append("<option value=\"" + pg.GroupID.ToString() + "\">" + pg.Name + "</option>");
                        }
                        else
                        {
                            sb.Append("<option selected=\"selected\" value=\"" + pg.GroupID.ToString() + "\">" + pg.Name + "</option>");
                            currentRadio = RadioButton.RadioButton_Group;
                        }
                        addedGroupCount++;
                    }
                    else if (pg.IsDefault && defaultGroupID.HasValue && defaultGroupID.Value == pg.GroupID)
                    {
                        currentRadio = RadioButton.RadioButton_Global;
                    }
                }
            }

            if (addedGroupCount == 0)
            {
                sb.Append("<option selected=\"selected\" value=\"\">No groups available</option>");
                disableGroups = true;
            }

            return sb.ToString();
        }

        private string GetPatientDropdownContent( List<AHAHelpContent.Patient> plist, AHAHelpContent.Patient defaultPatient )
        {
            // Now get extended patient class with personal information
            List<AHABusinessLogic.PatientInfo>   lstPatientInfo = (plist != null && plist.Count > 0) ? AHABusinessLogic.PatientInfo.GetPopulatedPatientInfo(plist) : null ;

            StringBuilder sb = new StringBuilder();

            if (lstPatientInfo != null && lstPatientInfo.Count > 0)
            {
                foreach (AHABusinessLogic.PatientInfo pat in lstPatientInfo)
                {
                    if( defaultPatient == null || defaultPatient.UserHealthRecordID != pat.UserHealthRecordID )
                        sb.Append("<option value=\"" + pat.UserHealthRecordID.ToString() + "\">" + pat.FullName + "</option>");
                    else
                        sb.Append("<option selected=\"selected\" value=\"" + pat.UserHealthRecordID.ToString() + "\">" + pat.FullName + "</option>");
                }
            }
            else
            {
                sb.Append("<option selected=\"selected\" value=\"\">No patients/participants available</option>");
            }

            return sb.ToString() ;
        }

            // note: currentCondition will be string.Empty if there isn't one
        private string GetConditionDropdownContent( string currentCondition )
        {
            System.Web.UI.WebControls.ListItemCollection    licoll = AlertManager.Helper.GetTrackerDataTypes();
            StringBuilder                                   sb = new StringBuilder();
            bool                                            doDefault = string.IsNullOrEmpty(currentCondition);

            foreach (System.Web.UI.WebControls.ListItem li in licoll)
            {
                if (li.Value.Equals(currentCondition) || doDefault)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + li.Value + "\">" + li.Text + "</option>");
                    doDefault = false;
                }
                else
                {
                    sb.Append("<option value=\"" + li.Value + "\">" + li.Text + "</option>");
                }
            }

            return sb.ToString();
        }

        private string GetPeriodDropdownContent( int currValue )
        {
            StringBuilder sb = new StringBuilder();

            // V5 /UserCntrls/Provider/Alert/AlertRuleForm.ascx.cs 
            for (int i = 1; i < 8; i++)
            {
                string  days = (i == 1) ? "1 day" : (i.ToString() + " days") ;
                if( currValue != i )
                    sb.Append("<option value=\"" + i.ToString() + "\">" + days + "</option>");
                else
                    sb.Append("<option selected=\"selected\" value=\"" + i.ToString() + "\">" + days + "</option>");
            }
            return sb.ToString();
        }


        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Save button.
            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                // if a radio button is not checked, then that widget is not passed in inputs. Have to test for existance.
                string valToCheck = (hashInputs.ContainsKey(cw.Id)) ? hashInputs[cw.Id] : string.Empty;
                if (!cw.Validate(valToCheck, ref errMsg))
                {
                    sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                    goodToGo = false;
                }
            }

            TrackerDataType tdt = TrackerDataType.BloodGlucose; // use this as default
            string alertRuleXml = string.Empty;

            if (goodToGo)
            {
                // Now do custom (non-boilerplate) validation of condition
                if (hashInputs["ddlConditions"].Equals("BloodGlucose"))
                {
                    goodToGo = ValidateBloodGlucose(hashInputs, ref sb, ref alertRuleXml );
                }
                else if (hashInputs["ddlConditions"].Equals("BloodPressure"))
                {
                    tdt = TrackerDataType.BloodPressure;
                    goodToGo = ValidateBloodPressure(hashInputs, ref sb, ref alertRuleXml );
                }
                else if (hashInputs["ddlConditions"].Equals("Weight"))
                {
                    tdt = TrackerDataType.Weight;
                    goodToGo = ValidateWeight(hashInputs, ref sb, ref alertRuleXml );

                }
                else
                {
                    sb.Append("Unknown Condition");
                    goodToGo = false;
                }
            }

            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }

            //return JsonFailure("Unimplemented Functionality...");

            // get together all the values needed for the DAL call
            int? patientID = null;
            int? groupID = null;
            bool sendPatientMsg = (hashInputs.ContainsKey("cbSendPatientMessage") && hashInputs["cbSendPatientMessage"].Equals(UXControl.HtmlCheckBoxChecked)) ? true : false;

            if (hashInputs["radToWhom"].Equals("group"))
            {
                groupID = int.Parse(hashInputs["ddlGroups"]);
            }
            else if (hashInputs["radToWhom"].Equals("patient"))
            {
                patientID = int.Parse(hashInputs["ddlPatients"]);
            }
            else
            {
                groupID = 0;    // from V5, to signal it's global
            }


            if ( mAlertRuleID != -1 )
            {
                AHAHelpContent.AlertRule.UpdateAlertRule(mAlertRuleID, patientID, groupID, tdt, alertRuleXml, sendPatientMsg, (hashInputs.ContainsKey("taMessage") ? hashInputs["taMessage"] : string.Empty));
                return JsonRefresh("EDIT ALERT RULE", "Successfully updated the Alert Rule");
            }
            else
            {
                ProviderContext provider = SessionInfo.GetProviderContext();

                AHAHelpContent.AlertRule.CreateAlertRule(provider.ProviderID, patientID, groupID, tdt, alertRuleXml, sendPatientMsg, (hashInputs.ContainsKey("taMessage") ? hashInputs["taMessage"] : string.Empty));
                return JsonRefresh( "ADD ALERT RULE", "Successfully created the Alert Rule");
            }

        }

        private bool ValidateBloodGlucose(Dictionary<string, string> hashInputs, ref StringBuilder errMsg, ref string alertRuleXML )
        {
            bool goodToGo = true;
            bool isAbove = true;
            double  alertRuleVal = 0.0 ;

            // Check radio
            if (!hashInputs.ContainsKey("bgc"))
            {
                errMsg.Append("Please select an Above/Below radio button.");
                goodToGo = false;
            }
            else
            {
                if (hashInputs["bgc"].Equals("below"))
                    isAbove = false;

                if (!hashInputs.ContainsKey("bgValue") || string.IsNullOrEmpty(hashInputs["bgValue"])) 
                {                
                    goodToGo = false;                    
                    if(isAbove) 
                        errMsg.Append("Please specify an Above value.");
                    else 
                        errMsg.Append("Please specify a Below value.");                    
                } 
                else
                    alertRuleVal = double.Parse(hashInputs["bgValue"]);
            }

            if (goodToGo)
                alertRuleXML = AlertManager.BloodGlucoseAlertRule.CreateAlertRuleXml(isAbove, alertRuleVal);

            return goodToGo;
        }

        private bool ValidateBloodPressure(Dictionary<string, string> hashInputs, ref StringBuilder errMsg, ref string alertRuleXML)
        {
            bool goodToGo = true;
            bool isSystolicAbove = true;
            bool isDiastolicAbove = true;
            bool isHeartRateAbove = true;
            int? iSystolic = null;
            int? iDiastolic = null;
            int? iHeartRate = null;

            //at least one input value must be set
            bool systolicValueSet = (hashInputs.ContainsKey("systolicValue") && !string.IsNullOrEmpty(hashInputs["systolicValue"]));
            bool diastolicValueSet = (hashInputs.ContainsKey("diastolicValue") && !string.IsNullOrEmpty(hashInputs["diastolicValue"]));
            bool heartrateValueSet = (hashInputs.ContainsKey("heartrateValue") && !string.IsNullOrEmpty(hashInputs["heartrateValue"]));

            if(!systolicValueSet && !diastolicValueSet && !heartrateValueSet)
            {
                goodToGo = false;
                errMsg.Append("Please enter at least one value for Systolic, Diastolic, or Heart Rate. ");
            }
            else
            {
                // UXControl has already validated that a number can be parsed if there is data.
                // FUTURE: V5 doesn't check for negative numbers. we may want to do so in the future.

                // Systolic, if present
                if (systolicValueSet)
                {
                    if (!hashInputs.ContainsKey("systolic"))
                    {
                        errMsg.Append("Please select a Systolic Above/Below radio button. ");
                        goodToGo = false;
                    }
                    else
                    {
                        iSystolic = int.Parse(hashInputs["systolicValue"]);
                        if (hashInputs["systolic"].Equals("below"))
                            isSystolicAbove = false;
                    }
                }

                // Diastolic, if present
                if (diastolicValueSet)
                {
                    if (!hashInputs.ContainsKey("diastolic"))
                    {
                        errMsg.Append("Please select a Diastolic Above/Below radio button. ");
                        goodToGo = false;
                    }
                    else
                    {
                        iDiastolic = int.Parse(hashInputs["diastolicValue"]);
                        if (hashInputs["diastolic"].Equals("below"))
                            isDiastolicAbove = false;
                    }
                }

                // Heart Rate, if present
                if (heartrateValueSet)
                {
                    if (!hashInputs.ContainsKey("heartrate"))
                    {
                        errMsg.Append("Please select a HeartRate Above/Below radio button");
                        goodToGo = false;
                    }
                    else
                    {
                        iHeartRate = int.Parse(hashInputs["heartrateValue"]);
                        if (hashInputs["heartrate"].Equals("below"))
                            isHeartRateAbove= false;
                    }
                }
            }

            if (goodToGo)
                alertRuleXML = AlertManager.BloodPressureAlertRule.CreateAlertRuleXml(
                    isSystolicAbove, iSystolic, isDiastolicAbove, iDiastolic, isHeartRateAbove, iHeartRate);

            return goodToGo;
        }

        private bool ValidateWeight(Dictionary<string, string> hashInputs, ref StringBuilder errMsg, ref string alertRuleXML)
        {
            bool goodToGo = true;
            bool isGain = true;
            double  alertRuleVal = 1.0 ;
            int     iPeriod = 1 ;

            // Check radio
            if (!hashInputs.ContainsKey("wc"))
            {
                errMsg.Append("Please select a Gain/Loss radio button. ");
                goodToGo = false;
            }
            else
            {
                if (hashInputs["wc"].Equals("loss"))
                    isGain = false;

                if (!hashInputs.ContainsKey("weightValue") || string.IsNullOrEmpty(hashInputs["weightValue"]))
                {
                    goodToGo = false;
                    if (isGain)
                        errMsg.Append("Please specify a Gain value.");
                    else
                        errMsg.Append("Please specify a Loss value.");
                }
                else
                {
                    // Because V5 had a dropdown of values from 1 - 10, we need to make sure the range is enforced here.
                    if (double.TryParse(hashInputs["weightValue"], out alertRuleVal))
                    {
                        if (alertRuleVal < 1.0 || alertRuleVal > 10.0)
                        {
                            goodToGo = false;
                            if (isGain)
                                errMsg.Append("The Gain value must be in the range from 1 to 10. ");
                            else
                                errMsg.Append("The Loss value must be in the range from 1 to 10. ");
                        }
                    }
                }

                if( !hashInputs.ContainsKey("ddlPeriod") )
                {
                    errMsg.Append("Please specify a time Period. ") ;
                    goodToGo = false ;
                }
                else
                    iPeriod = int.Parse(hashInputs["ddlPeriod"]) ;
            }

            if (goodToGo)
                alertRuleXML = AlertManager.WeightAlertRule.CreateAlertRuleXml(isGain, alertRuleVal, iPeriod, PeriodType.D);

            return goodToGo;
        }

    }
}