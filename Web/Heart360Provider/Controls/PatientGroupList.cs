﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class PatientGroupList : AHACommon.UXControl
    {
        private const int NO_PATIENT = -1;
        private int mPatientID = NO_PATIENT;           // default value

        public int PatientID
        {
            set
            {
                mPatientID = value;
            }
        }


         //List<AHAHelpContent.PatientGroup> lstGroups = AHAHelpContent.PatientGroup.FindAllByPatientIDAndProviderID(objPatient.UserHealthRecordID,AHAAPI.H360RequestContext.GetContext().ProviderDetails.ProviderID);
          //  lstGroups.RemoveAll(x => x.IsDefault);//No need to show default group of A Provider.

        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext provider = SessionInfo.GetProviderContext();
            List<PatientGroup> groupList = (provider != null && mPatientID != NO_PATIENT) ? PatientGroup.FindAllByPatientIDAndProviderID( mPatientID, provider.ProviderID) : null;

            if (groupList != null)
                groupList.RemoveAll(x => x.IsDefault);  // do not show default group of the Provider
            if (groupList == null || groupList.Count < 1)
            {
                return "There are no groups.";
            }

            StringBuilder sb = new StringBuilder();
            string markup = this.PrepareMarkup();

            foreach (PatientGroup pg in groupList)
            {
                // group name is first column
                sb.Append("<tr><td>" + Heart360Provider.Utility.UIHelper.GetHtmlAtagForPatientGroup(pg) + "</td>");
                // edit group is second column
                //determine type of group for edit popup    
                if (pg.GroupRuleXml == null)
                {
                    //Manual group
                    sb.Append(string.Format("<td><div class=\"edit-icon popuplink\" id=\"idEditGroupPopup\" data-page=\"Pages/SingleParticipant\" data-method=\"EditGroupPopup\" data-id=\"{0}\" data-refresh-ids=\"idControlGroupList\"></div></td></tr>", pg.GroupID));
                }
                else
                {
                    //Dynamic group
                    sb.Append(string.Format("<td><div class=\"edit-icon popuplink\" id=\"idEditGroupPopup\" data-page=\"Pages/SingleParticipant\" data-method=\"EditDynamicGroupPopup\" data-id=\"{0}\" data-refresh-ids=\"idControlGroupList\"></div></td></tr>", pg.GroupID));
                }
            }
            return markup.Replace("$GroupList$", sb.ToString());
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }
    }
}