﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class SentInvitations : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext provider = SessionInfo.GetProviderContext();

            List<AHAHelpContent.PatientProviderInvitationDetails> idlist = (provider != null) ?
                AHAHelpContent.PatientProviderInvitationDetails.FindPendingInvitationByProviderID(provider.ProviderID) : null;

            if (idlist == null || idlist.Count < 1)
            {
                return "There are no pending sent invitations.";
            }

            StringBuilder sb = new StringBuilder();
            string markup = this.PrepareMarkup();


            foreach (PatientProviderInvitationDetails ppid in idlist)
            {
                // PJB: I don't think printed invitations are added to the database.
                // but this is just some insurance.
                if (!ppid.IsPrintInvitation)
                {
                    sb.Append("<tr class=\"received-name\"><td>" + ppid.LastName + ", " + ppid.FirstName + "</td>");
                    // sb.Append( ((ppid.IsPrintInvitation) ? "<td>Printed</td>" : "<td>Email</td>") ) ;
                    sb.Append("<td>" + ppid.DateInserted.ToShortDateString() + "</td>");


                    string resendaction = "<div class=\"popuplink\" data-page=\"Pages/Participants\" data-method=\"ReinvitePopupHandler\" data-id=\"" + ppid.InvitationDetailsID.ToString() + "\" data-refresh-ids=\"idControlSentInvitationsList,idModuleHeader4\">Reinvite</div> ";
                    string removeaction = "<div class=\"popuplink trash-icon\" data-page=\"Pages/Participants\" data-method=\"CancelSentPopupHandler\" data-id=\"" + ppid.InvitationDetailsID.ToString() + "\" data-refresh-ids=\"idControlSentInvitationsList,idModuleHeader4\"></div> ";

                    sb.Append("<td class=\"actions-column\">" + removeaction + resendaction + "</td></tr>");
                }
            }     

            return markup.Replace("$InvitationList$", sb.ToString());
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Internal Error (code pathway execution)");
        }


    }
}