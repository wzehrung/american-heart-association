﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AHAFooter.ascx.cs" Inherits="Heart360Provider.Controls.Common.AHAFooter" %>



    <div class="mobile-title mobile-trigger">
        American Heart Association
        <div class="white-arrow"></div>
    </div>

    <div class="mobile-content main-footer-content">

        <div class="fourcol first tablet-twelvecol mobile-twelvecol red-side-line mobile-black-line">
            <div class="mobile-trigger footer-item-mobile"> About Us <div class="grey-arrow"></div> </div>
            <div class="mobile-content">
                <p>Our mission is to build healthier lives, free of cardiovascular diseases and stroke. That single purpose drives all we do. The need for our work is beyond question. <a title="About American Heart Association" href="http://www.heart.org/HEARTORG/General/About-Us---American-Heart-Association_UCM_305422_SubHomePage.jsp">More</a></p>
            </div>
        </div>

        <div class="threecol tablet-twelvecol mobile-twelvecol red-side-line mobile-black-line">
                <div class="mobile-trigger">Our Causes <div class="grey-arrow"></div></div>
                <div class="mobile-content">       
                    <h5><a title="Go Red for Women" href="http://www.goredforwomen.org/">Go Red For Women</a></h5>
                    <h5><a title="Go Red Corazon" href="http://www.goredcorazon.org">Go Red Por Tu Corazón</a></h5>
                    <h5><a title="My Heart My Life" href="http://www.heart.org/myheartmylife">My Heart My Life</a></h5>
                    <h5><a title="Power to End Stroke" href="http://www.powertoendstroke.org">Power To End Stroke</a></h5>							
                    <h5 class="spb">
                        <a title="Heart Attack, Stroke and Cardiac Arrest Warning Signs" href="http://www.heart.org/HEARTORG/Conditions/Conditions_UCM_305346_SubHomePage.jsp">The Warning Signs</a>
                    </h5>						
                    <h5>
                        <a title="Browse the Heart and Stroke Encyclopedia" href="http://www.heart.org/HEARTORG/Conditions/The-Heart-and-Stroke-Encyclopedia_UCM_445688_SubHomePage.jsp">Heart and Stroke Encyclopedia</a>
                    </h5>	
                </div>
        </div>

        <div class="threecol tablet-twelvecol mobile-twelvecol red-side-line mobile-black-line">
                <div class="mobile-trigger">Our Sites <div class="grey-arrow"></div></div>
                <div class="mobile-content">								
                    <h5><a title="American Heart Association" href="http://www.heart.org/HEARTORG">American Heart Association</a></h5>
                    <h5><a title="American Stroke Association" href="http://www.strokeassociation.org/STROKEORG">American Stroke Association</a></h5>
                    <h5><a title="link to My Life Check" href="http://www.heart.org/mylifecheck/">My Life Check</a></h5>
                    <h5><a title="link to Heart360" href="http://www.heart360.org">Heart360</a></h5>
                    <h5><a title="Everyday Choices" href="http://www.everydaychoices.org/">Everyday Choices</a></h5>
                    <h5><a title="My.AmericanHeart for Professionals" href="http://my.americanheart.org/">My.AmericanHeart for Professionals</a></h5>
                    <h5><a title="Scientific Sessions" href="http://www.scientificsessions.org/">Scientific Sessions</a></h5>
                    <h5><a title="Stroke Conference" href="http://www.strokeconference.org/">Stroke Conference</a></h5>
                    <h5><a title="You're The Cure" href="http://www.yourethecure.org">You're The Cure</a></h5>
                    <h5><a title="Global Programs" href="http://www.heart.org/HEARTORG/General/Global-Strategies-Pages_UCM_312090_SubHomePage.jsp">Global Programs</a></h5>
                    <h5><a title="Shop Heart" href="http://www.shopheart.org">Shop Heart</a></h5>
                    <h5><a title="CEO Nancy Brown" href="https://volunteer.heart.org/Pages/SiteHomePage.aspx">CEO Nancy Brown</a></h5>								
                </div>
        </div>

        <div class="twocol tablet-twelvecol mobile-twelvecol red-side-line mobile-black-line contact-us">
            <div class="mobile-trigger">Contact Us <div class="grey-arrow"></div> </div>
            <div class="mobile-content">
                <h6>Address</h6>
                <p>
                    7272 Greenville Ave.<br/>
                    Dallas, TX 75231
                </p>											
                <h6>Customer Service</h6>
                <p>
                    1-800-AHA-USA-1<br />
                    1-800-242-8721<br />
                    1-888-474-VIVE
                </p>						
                <h5>
                    <a class="heading" href="/HEARTORG/localization/chooseState.jsp">Local Info</a>
                </h5>
                <h6 class="rss">
                    <a href="/HEARTORG/General/RSSFeed-page-datafile_UCM_003860_RSSFeed.jsp">RSS</a>
                </h6>						

            </div>
        </div>

                
    </div> <!-- End .main-footer-content -->

    <div class="footer-links">
        <div class="mobile-trigger footer-links-title">
            Additional Links <div class="white-arrow"></div>
        </div>
    </div>
    
    <div class="mobile-content footer-bottom">
        <div class="twelvecol first">				      
            <a href="http://www.heart.org/HEARTORG/GettingHealthy/GettingHealthy_UCM_001078_SubHomePage.jsp" title="Getting Healthy" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Getting Healthy']);">Getting Healthy</a>
            |
            <a href="http://www.heart.org/HEARTORG/Conditions/Conditions_UCM_001087_SubHomePage.jsp" title="Conditions" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Conditions']);">Conditions</a>
            |
            <a href="http://www.heart.org/HEARTORG/HealthcareResearch/Healthcare-Research_UCM_001093_SubHomePage.jsp" title="Healthcare / Research" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Healthcare / Research']);">Healthcare / Research</a>
            |
            <a href="http://www.heart.org/HEARTORG/Caregiver/Caregiver_UCM_001103_SubHomePage.jsp" title="Caregiver" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Caregiver']);">Caregiver</a>
            |
            <a href="http://www.heart.org/HEARTORG/Educator/Educator_UCM_001113_SubHomePage.jsp" title="Educator" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Educator']);">Educator</a>
            |
            <a href="http://www.heart.org/HEARTORG/CPRAndECC/CPR_UCM_001118_SubHomePage.jsp" title="CPR &amp; ECC" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'CPR &amp; ECC']);">CPR &amp; ECC</a>
            |
            <a href="http://shopheart.org" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Shop']);" title="Shop">Shop</a>
            |
            <a href="http://www.heart.org/HEARTORG/Causes/Causes_UCM_001128_SubHomePage.jsp" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Causes']);" title="Causes">Causes</a>
            |
            <a href="http://www.heart.org/HEARTORG/Advocate/Advocate_UCM_001133_SubHomePage.jsp" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Advocate']);" title="Advocate">Advocate</a>
            |
            <a href="http://www.heart.org/HEARTORG/Giving/Giving_UCM_001137_SubHomePage.jsp" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Giving']);" title="Giving">Giving</a>
            |
            <a href="http://newsroom.heart.org" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'News']);" title="News">News</a>
            |
            <a href="http://www.heart.org/HEARTORG/volunteer/volunteerForm.jsp" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Volunteer']);" title="Volunteer">Volunteer</a>
            |
            <a href="https://donate.americanheart.org/ecommerce/donation/donation_info.jsp?campaignId=101&amp;site=Heart&amp;itemId=prod20008" onclick="_gaq.push(['_trackEvent', 'FooterNav', 'Click', 'Donate']);" title="Donate">Donate</a>							    
        </div>

        <div class="twelvecol first">
            <a href="http://www.heart.org/HEARTORG/General/Privacy-Policy_UCM_300371_Article.jsp" title="Privacy Policy">Privacy Policy</a>
            |
            <a href="http://www.heart.org/HEARTORG/General/Copyright-Notice_UCM_300378_Article.jsp" title="Copyright &amp; DMCA Info">Copyright &amp; DMCA Info</a>
            |
            <a href="http://www.heart.org/HEARTORG/General/Ethics-Policy_UCM_300430_Article.jsp" title="Ethics Policy">Ethics Policy</a>
            |
            <a href="http://www.heart.org/HEARTORG/General/Conflict-of-Interest-Policy_UCM_300435_Article.jsp" title="Conflict of Interest Policy">Conflict of Interest Policy</a>
            |
            <a href="http://www.heart.org/HEARTORG/General/American-Heart-Association-and-American-Stroke-Association-Linking-Policy_UCM_303551_Article.jsp" title="Linking Policy">Linking Policy</a>
            |
            <a href="http://www.heart.org/HEARTORG/General/Life-at-the-American-Heart-Association_UCM_303457_SubHomePage.jsp" title="Diversity">Diversity</a>
            |
            <a href="http://www.heart.org/HEARTORG/General/Careers_UCM_303455_SubHomePage.jsp" title="Careers">Careers</a>
        </div>					      
    </div>
    <div class="footer-information">
        <p class="copyright small-12 large-4 columns fl">&copy;<asp:Literal ID="litCopyrightDate" runat="server" /> American Heart Association, Inc. All rights reserved. Unauthorized use prohibited.</p>
        <p class="disclaimer small-12 large-8 columns fl">The American Heart Association is a qualified 501(c)(3) tax-exempt organization.</p>
    </div>


