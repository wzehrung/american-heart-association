﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrackerGraph.ascx.cs" Inherits="Heart360Provider.Controls.Common.TrackerGraph" %>
<%@ Register Src="~/Controls/Common/HealthGraph.ascx" TagName="HealthGraph" TagPrefix="uc1" %>

<div class="accordion-container white-accordion ">
    <div id="divOptionalTrigger" class="general-trigger asp-trigger" runat="server">
	    <h4>GRAPH</h4>
	    <div class="accordion-arrow"></div>
	</div>
	<div class="general-content next-content">
        <!-- Cholesterol requires two graphs -->
        <uc1:HealthGraph ID="HealthGraph1" runat="server" />
        <uc1:HealthGraph ID="HealthGraph2" runat="server" />
    </div>
</div>