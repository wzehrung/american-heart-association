﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using AHABusinessLogic;

namespace Heart360Provider.Controls.Common
{
    public partial class MedicationDataTracker : System.Web.UI.UserControl
    {
        private PatientGuids mPatientGuids;     // the offline health record guids of current patient

        public PatientGuids PatientOfflineGuids
        {
            set { mPatientGuids = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (mPatientGuids != null)
                {
                    HVManager.AllItemManager hvMan = HVManager.AllItemManager.GetItemManagersForRecordId(mPatientGuids.PersonID, mPatientGuids.RecordID);

                    LinkedList<HVManager.MedicationDataManager.MedicationItem> ascendingList = hvMan.MedicationDataManager.GetDataBetweenDates(AHACommon.DataDateReturnType.AllData);
                    int countOfExistingMeds = 0;

                    // Loop over result set and count existing meds...
                    foreach (HVManager.MedicationDataManager.MedicationItem medicationItem in ascendingList)
                    {
                        DateTime? dtDiscontinued = null;

                        if (medicationItem.HasDiscontinued
                            && medicationItem.DateDiscontinued.Value.HVApproximateDate != null
                            && medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                            && medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.HasValue)
                        {
                            dtDiscontinued = new DateTime(medicationItem.DateDiscontinued.Value.HVApproximateDate.Year, medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.Value, medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.Value);
                        }

                        if ((!medicationItem.HasDiscontinued
                            || (dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() > DateTime.Now.ToUniversalTime())))
                        {
                            countOfExistingMeds++;
                        }
                    }

                    this.litValue.Text = string.Format("<div class=\"right-value\">{0}</div>", countOfExistingMeds);

                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            StringBuilder sb = new StringBuilder();

            FormatMedicationTable(ref sb, true );
            litTableCurrent.Text = sb.ToString();

            sb.Clear();

            FormatMedicationTable(ref sb, false);
            litTableDiscontinued.Text = sb.ToString();
        }

        private string FormatNote(string note)
        {
            if (string.IsNullOrEmpty(note))
                return string.Empty;

            if (note.Length <= 18)
                return note;

            return "<span data-tooltip=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + note + "\">" + GRCBase.StringHelper.GetShortString(note, 18, true) + "</span>";
        }

        private void FormatMedicationTable(ref StringBuilder sb, bool isCurrentMeds)
        {
            sb.Append("<thead><tr><th>Name</th><th>Strength</th><th>Dosage</th><th>Frequency</th><th>Side Effects/Notes</th></tr></thead><tbody>");

            HVManager.AllItemManager hvMan = HVManager.AllItemManager.GetItemManagersForRecordId(mPatientGuids.PersonID, mPatientGuids.RecordID);

            LinkedList<HVManager.MedicationDataManager.MedicationItem> ascendingList = hvMan.MedicationDataManager.GetDataBetweenDates(AHACommon.DataDateReturnType.AllData);

            // Loop over result set and count existing meds...
            foreach (HVManager.MedicationDataManager.MedicationItem medicationItem in ascendingList.Reverse())
            {
                DateTime? dtDiscontinued = null;

                if (medicationItem.HasDiscontinued
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate != null
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                    && medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.HasValue)
                {
                    dtDiscontinued = new DateTime(medicationItem.DateDiscontinued.Value.HVApproximateDate.Year, medicationItem.DateDiscontinued.Value.HVApproximateDate.Month.Value, medicationItem.DateDiscontinued.Value.HVApproximateDate.Day.Value);
                }

                if( (isCurrentMeds && ((!medicationItem.HasDiscontinued
                        || (dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() > DateTime.Now.ToUniversalTime())))) ||
                    (!isCurrentMeds && ((medicationItem.HasDiscontinued && !dtDiscontinued.HasValue) ||
                                        (medicationItem.HasDiscontinued && dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() <= DateTime.Now.ToUniversalTime()))) )
                    {
                        sb.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                                                    medicationItem.Name.Value,
                                                    medicationItem.Strength.Value,
                                                    medicationItem.Dosage.Value,
                                                    medicationItem.Frequency.Value,
                                                    FormatNote(medicationItem.Note.Value)));
                    }

            }

            sb.Append("</tbody>");
        }

        protected void OnPDFCurrent_Click(object sender, EventArgs e)
        {
            Heart360Provider.Reports.PDFHelper.CreateReport(mPatientGuids, Heart360Provider.Reports.PDFType.CurrentMedication, AHACommon.DataDateReturnType.AllData, Heart360Provider.Reports.DataViewType.Table, null);

        }

        protected void OnPDFDiscontinued_Click(object sender, EventArgs e)
        {
            Heart360Provider.Reports.PDFHelper.CreateReport(mPatientGuids, Heart360Provider.Reports.PDFType.DiscontinuedMedication, AHACommon.DataDateReturnType.AllData, Heart360Provider.Reports.DataViewType.Table, null);
        }
    }
}