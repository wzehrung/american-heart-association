﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AHACommon;

namespace Heart360Provider.Controls.Common
{
    public partial class AHAFooter : System.Web.UI.UserControl
    {
        protected string strFooter = String.Empty;
        protected string AppVirtualPath = string.Empty;
        public string AHA_SCRIPT_FOOTER = string.Empty;

        protected string URL_PRIVACY_POLICY { get { return AppSettings.Url.Heart360PrivacyPolicy; } }
        protected string URL_COPYRIGHT { get { return AppSettings.Url.Heart360Copyright; } }
        protected string URL_ETHICS_POLICY { get { return AppSettings.Url.Heart360EthicsPolicy; } }
        protected string URL_CONFLICT_OF_INTEREST { get { return AppSettings.Url.Heart360ConflictOfInterest; } }
        protected string URL_LINKING_POLICY { get { return AppSettings.Url.Heart360LinkingPolicy; } }
        protected string URL_DIVERSITY { get { return AppSettings.Url.Heart360Diversity; } }
        protected string URL_CAREERS { get { return AppSettings.Url.Heart360Careers; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            /** PJB - we do not want to do this. Was setting page title from an .xml file
            Heart360Global.AHAPage objPage = Heart360Global.Main.GetPageDetailsFromUrl(GRCBase.UrlUtility.RelativeCurrentPageUrl);
            if (objPage != null)
            {
            //    if (objPage.IsProviderPage)
            //    {
            //        strFooter = LanguageResourceHelper.Disclaimer_Provider_HTML;
            //    }
            //    else
            //    {
            //        strFooter = LanguageResourceHelper.Disclaimer_Patient_HTML;
            //    }
            }
             **/

            litCopyrightDate.Text = DateTime.Now.Year.ToString();

            if (HttpRuntime.AppDomainAppVirtualPath != "/")
            {
                AppVirtualPath = HttpRuntime.AppDomainAppVirtualPath;
            }

            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name == Locale.Spanish)
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_FOOTER = "https://static.heart.org/es_aha_footer.js";
                }
                else
                {
                    AHA_SCRIPT_FOOTER = "http://static.heart.org/es_aha_footer.js";
                }
            }
            else
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_FOOTER = "https://static.heart.org/aha_footer.js";
                }
                else
                {
                    AHA_SCRIPT_FOOTER = "http://static.heart.org/aha_footer.js";
                }
            }
        }
    }
}