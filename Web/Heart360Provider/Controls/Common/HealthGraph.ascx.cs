﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Text;

using AHABusinessLogic;
using Heart360Provider.Graph;
using NPlot;

namespace Heart360Provider.Controls.Common
{
    public class AHAPlotSurface2D : NPlot.Web.PlotSurface2D
    {

        /// <summary>
        /// This override essentially adds a div surrounding the image and a mouse move javascript handler
        /// It also adds absolute positioned bubble div to show values on mouse over
        /// </summary>
        /// <param name="output"></param>
        protected override void Render(HtmlTextWriter output)
        {
            if (this.Width.Value == 0)
                return;

            // PJB
            // ScriptManager.RegisterStartupScript(this, Page.GetType(), string.Format("JS_Graph_{0}", ClientID), "CreateGraphDiv('" + BubbleDiv + "');", true);


            output.AddAttribute("id", ImageContainerDiv);
            output.AddAttribute("name", ImageContainerDiv);
            output.AddAttribute("class", "borderless_imagetable");
            // PJB (with JM) this isn't doing anything
            //       output.AddAttribute("onmousemove", "javascript:on_image_over(event);return false;");
            output.RenderBeginTag("div");

            //output.AddAttribute("style", "position:absolute; display:none");
            //output.AddAttribute("class", "bubble");
            //output.AddAttribute("id", BubbleDiv);
            //output.RenderBeginTag("div");
            //output.RenderEndTag();

            base.Render(output);
            output.RenderEndTag();
            output.Flush();
        }


        /// <summary>
        /// Constructs the id of the surrounding div
        /// </summary>
        public string ImageContainerDiv
        {
            get
            {
                return this.ClientID + "_image_container_div";
            }
        }


        /// <summary>
        /// Constructs the id of the bubble div
        /// </summary>
        public string BubbleDiv
        {
            get
            {
                return this.ClientID + "_bubble_div";
            }
        }

    }

    public partial class HealthGraph : System.Web.UI.UserControl
    {
            // Will hold the plot values
        Graph.AHAPlotData m_plotData = null;
            //This is the list where we will store the physical points of the graph image that is getting constructed
        List<Graph.GraphPoint> graphPointList = new List<Graph.GraphPoint>();               

        PointF m_yPos_zero;

        private PatientGuids mPatientGuids;     // the offline health record guids of current patient

        public PatientGuids PatientOfflineGuids
        {
            set { mPatientGuids = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //Attach an image map to the graph
            this.PlotSurface2D1.ImageMapName = this.ClientID + "_image_map";
        }


        public void PlotGraph(Graph.AHAPlotData plotData)
        {
            m_plotData = plotData;
        }


        protected override void OnPreRender(EventArgs e)
        {
            if (m_plotData != null)
            {

                _InitializeSurface();

                bool bhasGraphToDraw = _DrawGraph();
                if (!bhasGraphToDraw)
                {
                    this.PlotSurface2D1.Visible = false;
                    divGraph.Visible = false;
                }
                else
                {
                    this.PlotSurface2D1.Visible = true;
                    divGraph.Visible = true;
                    divGraph.Attributes.Add("style", "height:" + this.PlotSurface2D1.Height + "px");
                }
            }

            base.OnPreRender(e);

        }

        /// <summary>
        /// This function sets the basis styles for the graph and adds the legend object
        /// </summary>
        private void _InitializeSurface()
        {
            //Prepare the plot object
            PlotSurface2D1.BorderColor = Color.White;
            PlotSurface2D1.BackColor = Color.White;
            PlotSurface2D1.Width = m_plotData.m_graphWidth;
            PlotSurface2D1.Height = m_plotData.m_graphHeight;
            PlotSurface2D1.BorderWidth = 0;
        }


        /// <summary>
        /// This binds the image map for the graph constructed
        /// </summary>
        private void _BindImageMap(Graph.AHAGraphGenerator ahaGraphGenerator)
        {


            //Find the physical axis so that we can construct the image map points
            Axis xAxis1 = null;
            Axis xAxis2 = null;
            Axis yAxis1 = null;
            Axis yAxis2 = null;

            PlotSurface2D1.DetermineAxesToDraw(out xAxis1, out xAxis2, out yAxis1, out yAxis2);

            System.Drawing.Rectangle cb = new Rectangle(0, 0, (int)PlotSurface2D1.Width.Value, (int)PlotSurface2D1.Height.Value);

            // determine the default physical positioning of those axes.
            PhysicalAxis pXAxis1 = null;
            PhysicalAxis pYAxis1 = null;
            PhysicalAxis pXAxis2 = null;
            PhysicalAxis pYAxis2 = null;
            PlotSurface2D1.DeterminePhysicalAxesToDraw(
                cb, xAxis1, xAxis2, yAxis1, yAxis2,
                out pXAxis1, out pXAxis2, out pYAxis1, out pYAxis2);

            float scale = PlotSurface2D1.DetermineScaleFactor((int)PlotSurface2D1.Width.Value, (int)PlotSurface2D1.Height.Value);

            Point legendPosition;
            PlotSurface2D1.Legend.UpdateAxesPositions(
                    pXAxis1, pYAxis1, pXAxis2, pYAxis2,
                    PlotSurface2D1.Drawables, scale, PlotSurface2D1.Padding, cb,
                    out legendPosition);


            m_yPos_zero = PlotSurface2D1.YAxis1.WorldToPhysical(0, pYAxis1.PhysicalMin, pYAxis1.PhysicalMax, false);



            int index = 0;
            foreach (Graph.AHAGraphData graphData in m_plotData.m_graphsToPlot)
            {
                Graph.AHAGraphGenerator.GraphContextData contextData = ahaGraphGenerator.m_ContextDataList[index++];


                for (int i = 0; i < contextData.DL_X1X2.Length; i++)
                {
                    Graph.GraphPoint gPoint = new Graph.GraphPoint();

                    PointF xPos = PlotSurface2D1.XAxis1.WorldToPhysical(contextData.DL_X1X2[i], pXAxis1.PhysicalMin, pXAxis1.PhysicalMax, false);
                    PointF yPos = PlotSurface2D1.YAxis1.WorldToPhysical(contextData.DL_Y1Y2[i], pYAxis1.PhysicalMin, pYAxis1.PhysicalMax, false);
                    //System.Diagnostics.Trace.WriteLine("G" + (int)xPos.X + " : " + (int)yPos.Y);

                    gPoint.x = (int)xPos.X;
                    gPoint.y = (int)yPos.Y;

                    string dtstring = "";

                    if (ahaGraphGenerator.m_plotData.bShowTimeOfReadingOnMouseOverDiv)
                    {
                        dtstring = GRCBase.DateTimeHelper.GetFormattedDateTimeString(graphData.m_readingList[i].DateOfReading, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.HHMMAMPM);
                    }
                    else
                    {
                        dtstring = GRCBase.DateTimeHelper.GetFormattedDateTimeString(graphData.m_readingList[i].DateOfReading, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                    }

                    StringBuilder sbData = new StringBuilder();
                    //sbData.Append(contextData.DL_Y1Y2[i]);

                    sbData.Append(graphData.m_readingList[i].DisplayValue);

                    sbData.Append(" ");
                    //sbData.Append(m_plotData.m_readingUnits);
                    if (m_plotData.m_GraphType != Graph.GraphType.Exercise)
                    {
                        sbData.Append(graphData.m_readingUnits);
                        sbData.Append("<br>");
                    }

                    if (m_plotData.m_GraphType == Graph.GraphType.Weight)
                    {
                        double? dBMI = AHABusinessLogic.VirtualCoach.UserSummary.GetBMIForCurrentRecordForWeight( mPatientGuids, contextData.DL_Y1Y2[i]);

                        if (dBMI.HasValue)
                        {
                            sbData.AppendFormat("{0}:", GetGlobalResourceObject("Common", "Text_BodyMassIndex"));
                            sbData.Append(Math.Round(dBMI.Value, 2).ToString());
                            sbData.Append("<br>");
                        }
                    }
                    if (m_plotData.m_GraphType != Graph.GraphType.Exercise)
                    {
                        sbData.Append(dtstring);
                    }
                    //gPoint.data = contextData.DL_Y1Y2[i] + " " + m_plotData.m_readingUnits + "<br/>" + dtstring; 

                    gPoint.data = sbData.ToString();


                    graphPointList.Add(gPoint);
                }


                //If there is a goal endpoint (start or end) we show it image map
                if (contextData.GP_X1X2 != null)
                {
                    for (int i = 0; i < contextData.GP_X1X2.Length; i++)
                    {
                        Graph.GraphPoint gPoint = new Graph.GraphPoint();
                        PointF xPos = PlotSurface2D1.XAxis1.WorldToPhysical(contextData.GP_X1X2[i], pXAxis1.PhysicalMin, pXAxis1.PhysicalMax, false);
                        PointF yPos = PlotSurface2D1.YAxis1.WorldToPhysical(contextData.GP_Y1Y2[i], pYAxis1.PhysicalMin, pYAxis1.PhysicalMax, false);
                        //System.Diagnostics.Trace.WriteLine("G" + (int)xPos.X + " : " + (int)yPos.Y);

                        gPoint.x = (int)xPos.X;
                        gPoint.y = (int)yPos.Y;

                        string prefix = "";
                        string dtstring = "";
                        string valStr = "";
                        if (contextData.GL_X1X2[0] == contextData.GP_X1X2[i])
                        {
                            prefix = GetGlobalResourceObject("Common", "Text_GoalStart").ToString();
                            dtstring = GRCBase.DateTimeHelper.GetFormattedDateTimeString(graphData.m_goalData.GoalStartDate, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                            //valStr = graphData.m_goalData.m_goalStartValue + " " + m_plotData.m_readingUnits;

                            valStr = graphData.m_goalData.GoalStartDisplayValue + " " + graphData.m_readingUnits;


                        }
                        else if (contextData.GL_X1X2[1] == contextData.GP_X1X2[i])
                        {
                            prefix = GetGlobalResourceObject("Common", "Text_GoalEnd").ToString();
                            dtstring = GRCBase.DateTimeHelper.GetFormattedDateTimeString(graphData.m_goalData.GoalEndDate, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                            //valStr = graphData.m_goalData.m_goalEndValue + " " + m_plotData.m_readingUnits;

                            valStr = graphData.m_goalData.GoalEndDisplayValue + " " + graphData.m_readingUnits;

                        }

                        gPoint.data = prefix + "<br/>" + dtstring + "<br/>" + valStr;
                        graphPointList.Add(gPoint);
                    }
                }
            }
/**
            repImageMap.DataSource = graphPointList;
            repImageMap.DataBind();
 **/
        }


        /// <summary>
        /// This does the actual drawing of the graph
        /// </summary>
        /// <returns></returns>
        bool _DrawGraph()
        {

            Graph.AHAGraphGenerator ahaGraphGenerator = new Graph.AHAGraphGenerator(this.m_plotData, this.PlotSurface2D1);

            bool retVal = ahaGraphGenerator.DrawGraph();
            if (!retVal)
                return false;

            _BindImageMap(ahaGraphGenerator);

            return true;
        }

        protected void repImageMap_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Graph.GraphPoint gpoint = e.Item.DataItem as Graph.GraphPoint;

                Literal litAreaText = (Literal)e.Item.FindControl("litAreaText");

                /**
                                if (m_plotData.m_graphRenderingMode == GraphRenderingMode.LineAndPoint)
                                {
                                    string templateMap_point = @"<area shape='circle' coords='{0}' href='javascript:;' onclick='{1}' target='{2}' alt='{3}' onmouseover='{4}' onmouseout='{5}' />";
                                    if (string.IsNullOrEmpty(Request["PrinterFriendly"]))
                                    {
                                        litAreaText.Text = String.Format(templateMap_point,
                                                                        gpoint.x.ToString() + "," + gpoint.y.ToString() + ",10",
                                                                        "javascript:return false;",
                                                                        "_blank",
                                                                        "",
                                                                        "javascript:showGraphDiv(" + "true,\"" + PlotSurface2D1.BubbleDiv + "\",\"" + PlotSurface2D1.ImageContainerDiv + "\"," + gpoint.x.ToString() + "," + gpoint.y.ToString() + ",60,50,\"" + gpoint.data + "\"" + ");return false;",
                                                                        "javascript:showGraphDiv(" + "false,\"" + PlotSurface2D1.BubbleDiv + "\");return false;");
                                    }
                                }

                                else
                                {
                                    int barwidth = m_plotData.GetBarWidth();

                                    int x1 = (int)(gpoint.x - barwidth / 2);
                                    int y1 = (int)gpoint.y;

                                    int x2 = (int)(gpoint.x + barwidth / 2);
                                    int y2 = (int)m_yPos_zero.Y;


                                    string templateMap_bar = @"<area shape='rect' coords='{0}' href='#' onclick='{1}' target='{2}' alt='{3}' onmouseover='{4}' onmouseout='{5}' />";
                                    if (string.IsNullOrEmpty(Request["PrinterFriendly"]))
                                    {
                                        litAreaText.Text = String.Format(templateMap_bar,
                                                                        x1 + "," + y1 + "," + x2 + "," + y2,
                                                                        "javascript:return false;",
                                                                        "_blank",
                                                                        "",
                                                                        "javascript:showGraphDiv(" + "true,\"" + PlotSurface2D1.BubbleDiv + "\",\"" + PlotSurface2D1.ImageContainerDiv + "\"," + gpoint.x.ToString() + "," + gpoint.y.ToString() + ",60,50,\"" + gpoint.data + "\"" + ");return false;",
                                                                        "javascript:showGraphDiv(" + "false,\"" + PlotSurface2D1.BubbleDiv + "\");return false;");
                                    }

                                }
                 **/
            }
        }

    }
}