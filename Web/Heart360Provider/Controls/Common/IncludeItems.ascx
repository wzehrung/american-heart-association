﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncludeItems.ascx.cs"Inherits="Heart360Provider.Controls.Common.IncludeItems" %>

<meta name="keywords" content="Ace Inhibitor,AED, American Heart Walk, Aneurysm, Angina, Angiogram, Angioplasty, Aortic Stenosis, Arrhythmia, Arteriosclerosis, Atherosclerosis, Atrial Fibrillation, Blood Clot, Brain Attack, Bypass, Bypass Surgery, Cardiac Arrest, Cardiac Diseases, Cardiac Guidelines, Cardiology, Cardiology Conferences, Cardiomyopathy, Cardiopulmonary Resuscitation, Cardiovascular, Cardiovascular Disease, Caregiver, Carotid Endarterectomy, Cerebrovascular Accident, Cerebrovascular Disease, Chest Pain, CHF, Cholesterol, Circulation, Circulatory Disorders, Congenital Heart Disease, Congestive Heart Failure, Cookbooks, Coronary Arteriosclerosis, Coronary Artery Bypass Graft, Coronary Disease, Coronary Heart Disease, CPR, Deep Vein Thrombosis, Defibrillator, Diabetes, Diastolic, Diuretics, Endocarditis, Fibrillation, HDL, Heart Attack, Heart Beat, Heartbeat, Heart Bypass Surgery, Heart Disease, Heart Failure, Heart Murmur, Heart Operation, Heart rate, Heart Surgery, Heart Transplant, Heart Valve Diseases, High Blood Pressure, Hypertension, Irregular Heartbeat, LDL, Low Density Lipoprotein, Medical Research, Mini-Stroke, Mitral Valve,  Myocardial Infarction, Pacemaker, Risk Factor, Scientific Sessions, Stress Test, Stroke, Stroke Association, Stroke Conference, Stroke Guidelines, Stroke Rehabilitation, Stroke Symptoms, Syncope, Systolic, Support Group, Tachycardia, Tetralogy, TIA, Transient Ischemic Attack, Triglycerides, Valve Replacement, Valvular Heart Disease, Vascular Diseases, Vasculitis, Warning Signs, Women's Health,  American Heart Association, neck pain, navigation, tier 1, Health Tracking, Health tracker, Health log, Track health, track heart health, Heart health tracker, Heart health log, Cardiovascular Health, Cardiovascular tracker, Cardiovascular log, Track High Blood Pressure, High Blood Pressure Tracker, High blood pressure log, HBP tracker, HBP log, Blood Pressure Tracker, Blood Pressure log, BP tracker, BP log, Track Blood Pressure, Weight Tracker, Weight log, Track weight, Diabetes Tracker, Diabetes log, Track Diabetes, Glucose tracker, Glucose log, Track glucose, Blood glucose tracker, Blood glucose log, Track blood glucose, Blood sugar tracker, Blood sugar log, Track blood sugar, Diabetic tracker, Diabetic log, Tracker for diabetics, Log for diabetics, Heart Disease Management, Heart Health, Cardiovascular wellness, Wellness log, Wellness tracker, Diet log, Diet tracker, Nutrition log, Nutrition tracker, Calorie log, Calorie tracker, Fitness log, Fitness tracker, Exercise log, Exercise tracker, Physical fitness log, Physical fitness tracker, Physical activity log, Physical activity tracker, cholesterol log, Cholesterol tracker, Smoking cessation, quit smoking, smoking, tobacco, tobacco cessation, quit tobacco, Recipe, CABG, Coronary Artery Disease, CAD, Diabetic, High Density Lipoprotein, Blood pressure, Cardiac rehab, Cardiac rehabilitation, PAD, Peripheral Artery Disease, Peripheral Vascular Disease" />

<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

<link rel="stylesheet" media="all" href="<%=AppVirtualPath%>/library/css/style.css" />
<link rel="stylesheet" media="all" href="<%=AppVirtualPath%>/library/css/BeatPicker.min.css" />

<!--[if IE 6]>
	<link rel="stylesheet" type="text/css" media="all" href="<%=AppVirtualPath%>/library/css/ie.css" />
	<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/ie6.css" />
<![endif]-->

<!--[if IE 7]>
	<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/ie.css" />
	<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/ie7.css" />
	<script type="text/javascript">alert('I am IE 7');</script>
<![endif]-->

<!--[if IE 8]>
	<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/ie.css" />
	<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/ie8.css" />
<![endif]-->

<!--[if IE 9]>
	<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/ie.css" />
	<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/ie9.css" />
<![endif]-->

<!-- <script type="text/javascript" src="<%=AppVirtualPath%>/library/js/respond.js"></script> -->

<link rel="shortcut icon" href="<%=AppVirtualPath%>/library/images/favicon.ico?Version=<%=VersionNumber%>" type="image/x-icon" />

<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/jquery.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/jquery.heart360.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/json-compatibility.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/respond.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/table.min.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/jquery.bear-slider.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/nicEdit-latest.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/date.js"></script>
<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/BeatPicker.min.js"></script>

<script type="text/javascript" src="<%=AppVirtualPath%>/library/js/tipr.js"></script>
<link rel="stylesheet" type="text/css"  media="all" href="<%=AppVirtualPath%>/library/css/tipr.css" />

<!-- IE Conditional tags to add in Stylesheet just for IE Fixes -->

