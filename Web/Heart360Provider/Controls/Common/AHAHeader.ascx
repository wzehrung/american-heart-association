﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AHAHeader.ascx.cs" Inherits="Heart360Provider.Controls.Common.AHAHeader" %>

<!-- AHA Logo -->
<div class="twelvecol first tablet-twelvecol mobile-twelvecol">
    <div id="logo">
        <a href='<%=UrlHeart360%>'>
            <img src="/library/images/logo.png" alt="<%=GetGlobalResourceObject("Common", "Header_Text_AHA")%>" />
        </a>
    </div>

    <div id="top-small-navigation">

        <div id="mobile-search-button"></div>

        <div id="search-box-wrapper">
            <form onsubmit="return document.getElementById('txtSearch') != '';"
                    action="https://www.heart.org/HEARTORG/search/searchResults.jsp?_dyncharset=ISO-8859-1">
                <div class="elevencol first">
                    <input type="text" id="txtSearch" maxlength="60" name="q" class="search-input" autocomplete="off" />
                </div>
                <div class="onecol first">
                    <input type="submit" class="search-button" id="btnSearch" value="" name="btnSearch" />
                </div>
                <input type="text" name="hidden" style="display:none;" />
            </form>
        </div>

        <div id="donate-button">
            <span class="donate-button"><a href='<%=UrlDonate%>'><%=GetGlobalResourceObject("Common", "Header_Text_Donate")%></a></span>        
        </div>

        <div id="social-icons">
            <a href="<%=UrlTwitter%>'>" class="twitter"></a>
            <a href="<%=UrlFacebook%>'>" class="facebook"></a>
            <div class="feedback popuplink" data-page="SignIn" data-method="SendFeedbackPopup"></div>
        </div>

    </div><!-- End #top-small-navigation -->
</div>