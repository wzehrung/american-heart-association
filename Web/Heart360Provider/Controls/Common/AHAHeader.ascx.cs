﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// For search
using System.IO;
using System.Net;
using System.Text;

using AHACommon;

namespace Heart360Provider.Controls.Common
{
    public partial class AHAHeader : System.Web.UI.UserControl
    {
        bool m_HideDonate = true;
        public bool HideDonate
        {
            get
            {
                return m_HideDonate;
            }
            set
            {
                m_HideDonate = value;
            }
        }

        bool m_HideSearch = true;
        public bool HideSearch
        {
            get
            {
                return m_HideSearch;
            }
            set
            {
                m_HideSearch = value;
            }
        }

        bool m_HidePersonas = true;
        public bool HidePersonas
        {
            get
            {
                return m_HidePersonas;
            }
            set
            {
                m_HidePersonas = value;
            }
        }

        public string HOME = string.Empty;

        public string AHA_SCRIPT_HEADER = string.Empty;
        public string AHA_HEADER_TYPE = string.Empty;

        protected string UrlHeart360
        {
            get
            {
                return AppSettings.Url.Heart360;
            }
        }
        protected string UrlDonate
        {
            get
            {
                return AppSettings.Url.Donate;
            }
        }
        protected string UrlFacebook
        {
            get
            {
                return AppSettings.Url.Facebook;
            }
        }
        protected string UrlTwitter
        {
            get
            {
                return AppSettings.Url.Twitter;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //if (UIHelper.IsScientificSessionsPage())
            //{
            //    HOME = WebAppNavigation.H360.PAGE_SCIENTIFIC_HOME;
            //}
            //else
            //{
            //    HOME = "~/";
            //}

            // if IE9
            // else
    //            <form id="Form2" method="get" name="suggestion_form" action="https://www.heart.org/HEARTORG/search/searchResults.jsp">
    //    <input type="submit" name="search" class="search-btn"/>  
    //    <input id="Text1" type="text" autocomplete="off" name="q" value="How Can We Help Your Heart?" onblur="ss_hide('');if (this.value == '') {this.value = 'How Can We Help Your Heart?';}" onfocus="if (this.value == 'How Can We Help Your Heart?') {this.value = '';}" /> 
    //</form>

            if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name == Locale.Spanish)
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_HEADER = "https://static.heart.org/es_aha_header.js";
                }
                else
                {
                    AHA_SCRIPT_HEADER = "http://static.heart.org/es_aha_header.js";
                }
                AHA_HEADER_TYPE = "headerType=\"aha-es\";";
            }
            else
            {
                if (AHACommon.AHAAppSettings.IsSSLEnabled)
                {
                    AHA_SCRIPT_HEADER = "https://static.heart.org/aha_header.js";
                }
                else
                {
                    AHA_SCRIPT_HEADER = "http://static.heart.org/aha_header.js";
                }
            }
            //<%=AHACommon.AHAAppSettings.IsSSLEnabled ? "https://static.heart.org/aha_header.js" : "http://static.heart.org/aha_header.js"%>

        }

    }
}