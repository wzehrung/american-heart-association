﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MedicationDataTracker.ascx.cs" Inherits="Heart360Provider.Controls.Common.MedicationDataTracker" %>
<div class="accordion-container thin-accorion"> 
    <div class="general-trigger asp-trigger">
        <div class="thin-tile medications">
            <div class="icon"></div>
            <div class="icon-color"></div>
            <div class="white-color"></div>

            <div class="title">Medications</div>
            <div class="value">
                <asp:Literal ID="litValue" runat="server" />                
            </div>
            <div class="arrow"></div>
        </div> <!-- End .thin-tile -->
    </div>
    <div class="general-content">
        <!-- table -->
        <div>
            <h4>Current</h4>
        </div>
        <div class="twelvecol first">
            <table class="styled-table">
                <asp:Literal ID="litTableCurrent" runat="server" />
            </table>
        </div>

        <div class="twelvecol first">
            <asp:LinkButton CssClass="gradient-button float-right" Text="PDF Report" runat="server" OnClick="OnPDFCurrent_Click"/>
        </div>

        <hr />

        <div>
            <h4>Discontinued</h4>
        </div>
        <div class="twelvecol first">
            <table class="styled-table">
                <asp:Literal ID="litTableDiscontinued" runat="server" />
            </table>
        </div>

        <div class="twelvecol first">
            <asp:LinkButton CssClass="gradient-button float-right" Text="PDF Report" runat="server" OnClick="OnPDFDiscontinued_Click"/>
        </div>

        <!-- controls -->

    </div>

</div>
