﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthDataTracker.ascx.cs" Inherits="Heart360Provider.Controls.Common.HealthDataTracker" %>
<%@ Register Src="~/Controls/Common/TrackerGraph.ascx" TagName="TrackerGraph" TagPrefix="uc" %>

<div class="accordion-container thin-accorion"> 

    <div class="general-trigger asp-trigger">
        <asp:Literal ID="litThinTile" runat="server" />
            <div class="icon"></div>
            <div class="icon-color"></div>
            <div class="white-color"></div>

            <div class="title"><asp:Literal ID="litTitle" runat="server" /></div>
            <div class="value">
                <asp:Literal ID="litValue" runat="server" />                
            </div>
            <div class="arrow"></div>
    </div> <!-- End .thin-tile -->
    </div>
    <div class="general-content">
        <!-- table -->
        <div class="twelvecol first">
            <table class="styled-table">
                <asp:Literal ID="litTableData" runat="server" />
            </table>
        </div>
            
        <!-- graph -->
        <uc:TrackerGraph ID="GraphControl" runat="server" />

        <!-- controls -->
        <div class="twelvecol first">
            <asp:LinkButton CssClass="gradient-button float-right" Text="PDF Report" runat="server" OnClick="OnPDFReport_Click"/>
        </div>

        <!-- HbA1c table -->
        <div class="twelvecol first">
            <table class="styled-table">
                <asp:Literal ID="litHbA1cTableData" runat="server" />
            </table>
        </div>

    </div>

</div>