﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthGraph.ascx.cs" Inherits="Heart360Provider.Controls.Common.HealthGraph" %>
<%@ Register Assembly="NPlot" Namespace="NPlot.Web" TagPrefix="GraphCtrl" %>
<%@ Register Assembly="Heart360Provider" Namespace="Heart360Provider.Controls.Common" TagPrefix="GraphCtrl" %>

<div id="divGraph"  runat="server" >
    <GraphCtrl:AHAPlotSurface2D ID="PlotSurface2D1" runat="server" />
</div>
