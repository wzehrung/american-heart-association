﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using Heart360Provider.Graph;
using AHABusinessLogic;

namespace Heart360Provider.Controls.Common
{
    public partial class HealthDataTracker : System.Web.UI.UserControl
    {
        private PatientGuids                            mPatientGuids;     // the offline health record guids of current patient
        private AHACommon.PatientTrackers.TrackerType   mTrackerType;
        private AHACommon.DataDateReturnType            mDataRange;
        private AHABusinessLogic.UserHealthDataDetails  mTrackerData = null ;

        public PatientGuids PatientOfflineGuids
        {
            set { mPatientGuids = value; this.GraphControl.PatientOfflineGuids = mPatientGuids; }
        }
        public AHACommon.PatientTrackers.TrackerType TrackerType
        {
            get { return mTrackerType; }
            set { mTrackerType = value; this.GraphControl.TrackerType = mTrackerType; }
        }
        public AHACommon.DataDateReturnType DataRange
        {
            set { mDataRange = value; GraphControl.DataRange = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected string GetRiskLevelColor(RiskLevelHelper.RiskLevelResultTypeEnum risk)
        {
            string result = string.Empty;

            if (risk == RiskLevelHelper.RiskLevelResultTypeEnum.Excellent)
                result = "green";
            else if (risk == RiskLevelHelper.RiskLevelResultTypeEnum.NeedsImprovement)
                result = "orange";
            else if (risk == RiskLevelHelper.RiskLevelResultTypeEnum.Warning)
                result = "red";

            return result;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (mTrackerData == null && mPatientGuids != null )
                {
                    switch (mTrackerType)
                    {
                        case AHACommon.PatientTrackers.TrackerType.BloodPressure:
                            mTrackerData = new AHABusinessLogic.UserBP(HVManager.AllItemManager.GetItemManagersForRecordId(mPatientGuids.PersonID, mPatientGuids.RecordID), Heart360Global.Main.GetSystemCreatedGroups());
                            break;

                        case AHACommon.PatientTrackers.TrackerType.Cholesterol:
                            mTrackerData = new AHABusinessLogic.UserCholesterol( HVManager.AllItemManager.GetItemManagersForRecordId(mPatientGuids.PersonID, mPatientGuids.RecordID), Heart360Global.Main.GetSystemCreatedGroups());
                            break;

                        case AHACommon.PatientTrackers.TrackerType.BloodGlucose:
                            mTrackerData = new AHABusinessLogic.UserBG(HVManager.AllItemManager.GetItemManagersForRecordId(mPatientGuids.PersonID, mPatientGuids.RecordID), Heart360Global.Main.GetSystemCreatedGroups());
                            break;

                        case AHACommon.PatientTrackers.TrackerType.Weight:
                            mTrackerData = new AHABusinessLogic.UserWeight(HVManager.AllItemManager.GetItemManagersForRecordId(mPatientGuids.PersonID, mPatientGuids.RecordID), Heart360Global.Main.GetSystemCreatedGroups());
                            break;

                        case AHACommon.PatientTrackers.TrackerType.PhysicalActivity:
                            mTrackerData = new AHABusinessLogic.UserExercise(HVManager.AllItemManager.GetItemManagersForRecordId(mPatientGuids.PersonID, mPatientGuids.RecordID), Heart360Global.Main.GetSystemCreatedGroups());
                            break;

                        default:
                            break;
                    }
                }

                switch (mTrackerType)
                {
                    case AHACommon.PatientTrackers.TrackerType.BloodPressure:
                        this.litTitle.Text = "Blood Pressure";
                        if (mTrackerData != null && mTrackerData.LatestValue != null)
                        {
                            this.litThinTile.Text = string.Format("<div class=\"thin-tile {0} blood-pressure\">", GetRiskLevelColor( mTrackerData.GetRiskLevelLatestReading().RiskLevelResultType ) ) ;
                            this.litValue.Text = string.Format("<div class=\"right-value left-sidebar\">{0}</div><div class=\"left-value\">{1}</div>", mTrackerData.LatestValue[1], mTrackerData.LatestValue[0]);
                        }
                        else
                        {
                            this.litThinTile.Text = "<div class=\"thin-tile blood-pressure\">";
                            this.litValue.Text = string.Empty;
                        }
                        break;

                    case AHACommon.PatientTrackers.TrackerType.Cholesterol:
                        this.litTitle.Text = "Cholesterol";
                        if (mTrackerData != null && mTrackerData.LatestValue != null)
                        {
                            this.litThinTile.Text = string.Format("<div class=\"thin-tile {0} cholesterol\">", GetRiskLevelColor( mTrackerData.GetRiskLevelLatestReading().RiskLevelResultType ) ) ;
                            this.litValue.Text = string.Format("<div class=\"right-value\">{0}</div>", mTrackerData.LatestValue[0] );
                        }
                        else
                        {
                            this.litThinTile.Text = "<div class=\"thin-tile cholesterol\">";
                            this.litValue.Text = string.Empty;
                        }
                        break;

                    case AHACommon.PatientTrackers.TrackerType.BloodGlucose:
                        this.litTitle.Text = "Blood Glucose";
                        if (mTrackerData != null && mTrackerData.LatestValue != null)
                        {
                            this.litThinTile.Text = string.Format("<div class=\"thin-tile {0} blood-glucose\">", GetRiskLevelColor(mTrackerData.GetRiskLevelLatestReading().RiskLevelResultType));
                            this.litValue.Text = string.Format("<div class=\"right-value\">{0}</div>", mTrackerData.LatestValue[0] );
                        }
                        else
                        {
                            this.litThinTile.Text = "<div class=\"thin-tile blood-glucose\">";
                            this.litValue.Text = string.Empty;
                        }
                        break;

                    case AHACommon.PatientTrackers.TrackerType.Weight:
                        this.litTitle.Text = "Weight";
                        if (mTrackerData != null && mTrackerData.LatestValue != null)
                        {
                            this.litThinTile.Text = string.Format("<div class=\"thin-tile {0} weight\">", GetRiskLevelColor(mTrackerData.GetRiskLevelLatestReading().RiskLevelResultType));
                            this.litValue.Text = string.Format("<div class=\"right-value\">{0}</div>", mTrackerData.LatestValue[0]);
                        }
                        else
                        {
                            this.litThinTile.Text = "<div class=\"thin-tile weight\">";
                            this.litValue.Text = string.Empty;
                        }
                        break;

                    case AHACommon.PatientTrackers.TrackerType.PhysicalActivity:
                        this.litTitle.Text = "Physical Activity";
                        if (mTrackerData != null && mTrackerData.LatestValue != null)
                        {
                            this.litThinTile.Text = string.Format("<div class=\"thin-tile {0} physical-activity\">", GetRiskLevelColor(mTrackerData.GetRiskLevelLatestReading().RiskLevelResultType));
                            this.litValue.Text = string.Format("<div class=\"right-value\">{0}</div>", GRCBase.StringHelper.GetFormattedDoubleString(((UserExercise)mTrackerData).WeeklyTotal, 2, false));
                        }
                        else
                        {
                            this.litThinTile.Text = "<div class=\"thin-tile physical-activity\">";
                            this.litValue.Text = string.Empty;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // no matter if it is a postback or not, we do this here because the DateRange event handling happens after Page_Load
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();

            switch (mTrackerType)
            {
                case AHACommon.PatientTrackers.TrackerType.BloodPressure:
                    FormatBloodPressureTable(ref sb);
                    break;

                case AHACommon.PatientTrackers.TrackerType.Cholesterol:
                    FormatCholesterolTable(ref sb);
                    break;

                case AHACommon.PatientTrackers.TrackerType.BloodGlucose:
                    FormatBloodGlucoseTable(ref sb);
                    FormatHbA1cTable(ref sb2);
                    break;

                case AHACommon.PatientTrackers.TrackerType.Weight:
                    FormatWeightTable(ref sb);
                    break;

                case AHACommon.PatientTrackers.TrackerType.PhysicalActivity:
                    FormatPhysicalActivityTable(ref sb);
                    break;

                default:
                    break;
            }

            litTableData.Text = sb.ToString();
            litHbA1cTableData.Text = sb2.ToString();
        }

        private string FormatNote(string note)
        {
            if (string.IsNullOrEmpty(note))
                return string.Empty;

            if (note.Length <= 18)
                return note;

            return "<span data-tooltip=\"\" data-width=\"300\" class=\"has-tip\" title=\"" + note + "\">" + GRCBase.StringHelper.GetShortString(note, 18, true) + "</span>";
        }

        private void FormatBloodPressureTable(ref StringBuilder sb)
        {
            sb.Append("<thead><tr><th>Date</th><th>Time</th><th>HR</th><th>SYS</th><th>DIA</th><th>IRREG</th><th>Source</th><th>Comments</th></tr></thead><tbody>");
            
            LinkedList<HVManager.BloodPressureDataManager.BPItem> ascendingList = HVManager.AllItemManager.GetItemManagersForRecordId(this.mPatientGuids.PersonID, this.mPatientGuids.RecordID).BloodPressureDataManager.GetDataBetweenDates( mDataRange );
            if (ascendingList != null && ascendingList.Count > 0)
            {
                List<GroupManager.GroupInfo>    riskGroups = Heart360Global.Main.GetSystemCreatedGroups();
                int?                            daysOffset = AHACommon.AHAAppSettings.ColorCoding_EffectiveDays;

                foreach (HVManager.BloodPressureDataManager.BPItem bpItem in ascendingList.Reverse())
                {
                    string contentSystolic;
                    string contentDiastolic;

                    if ((daysOffset == null) || bpItem.EffectiveDate.CompareTo(DateTime.Now.AddDays(daysOffset.Value)) == 1)
                    {
                        RiskLevelHelper.RiskLevelBloodPressure bpRL = new RiskLevelHelper.RiskLevelBloodPressure(riskGroups, bpItem.Systolic.Value, bpItem.Diastolic.Value);

                        contentSystolic = "<span class=\"" + bpRL.ColorReading1 + "\">" + bpItem.Systolic.Value.ToString() + "</span>";
                        contentDiastolic = "<span class=\"" + bpRL.ColorReading2 + "\">" + bpItem.Diastolic.Value.ToString() + "</span>";
                    }
                    else
                    {
                        contentSystolic = bpItem.Systolic.Value.ToString();
                        contentDiastolic = bpItem.Diastolic.Value.ToString();
                    }

                    string strIrregularHeartbeat = bpItem.IrregularHeartbeat.Value.ToString();

                    switch (strIrregularHeartbeat)
                    {
                        case "True":
                            strIrregularHeartbeat = GetGlobalResourceObject("Common", "Text_Yes").ToString();
                            break;
                        case "False":
                            strIrregularHeartbeat = GetGlobalResourceObject("Common", "Text_No").ToString();
                            break;
                        default:
                            strIrregularHeartbeat = GetGlobalResourceObject("Common", "Text_Unknown").ToString();
                            break;

                    }

                    // date, time, HR, Systolic, Diastolic, Irregular Heartbeat, Source, Comments
                    sb.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td></tr>",
                                                bpItem.When.Value.ToShortDateString(), 
                                                bpItem.When.Value.ToShortTimeString(),
                                                (bpItem.HeartRate.Value.HasValue) ? bpItem.HeartRate.Value.Value.ToString() : string.Empty,
                                                contentSystolic, contentDiastolic,
                                                strIrregularHeartbeat,
                                                bpItem.Source.Value,
                                                FormatNote(bpItem.Note.Value)));
                }
            }
            sb.Append("</tbody>");
        }
        
        private void FormatCholesterolTable(ref StringBuilder sb)
        {
            HVManager.AllItemManager    mgr = HVManager.AllItemManager.GetItemManagersForRecordId(this.mPatientGuids.PersonID, this.mPatientGuids.RecordID);

            sb.Append("<thead><tr><th>Date</th><th>TOTAL CHOLESTEROL (mg/dL)</th><th>HDL (mg/dL)</th><th>LDL (mg/dL)</th><th>TRIGLYCERIDES (mg/dL)</th><th>Source</th><th>Comments</tr></thead><tbody>");

            LinkedList<HVManager.CholesterolDataManager.CholesterolItem> ascendingList = mgr.CholesterolDataManager.GetDataBetweenDates(mDataRange);
            if (ascendingList != null && ascendingList.Count > 0)
            {
                List<GroupManager.GroupInfo> riskGroups = Heart360Global.Main.GetSystemCreatedGroups();
                int? daysOffset = AHACommon.AHAAppSettings.ColorCoding_EffectiveDays;

                bool isGenderMale = (mgr.BasicDataManager.Item != null && mgr.BasicDataManager.Item.GenderOfPerson.Value.HasValue &&
                mgr.BasicDataManager.Item.GenderOfPerson.Value.Value == Microsoft.Health.ItemTypes.Gender.Male) ? true : false;

                foreach (HVManager.CholesterolDataManager.CholesterolItem cItem in ascendingList.Reverse())
                {

                    RiskLevelHelper.RiskLevelCholesterol cRiskLevel = new RiskLevelHelper.RiskLevelCholesterol(riskGroups, (cItem.TotalCholesterol.Value.HasValue ? cItem.TotalCholesterol.Value.Value : 0)
                        , (cItem.HDL.Value.HasValue ? cItem.HDL.Value.Value : 0)
                        , (cItem.LDL.Value.HasValue ? cItem.LDL.Value.Value : 0)
                        , (cItem.Triglycerides.Value.HasValue ? cItem.Triglycerides.Value.Value : 0), isGenderMale );

                    string  sTC = (cItem.TotalCholesterol.Value.HasValue) ? cItem.TotalCholesterol.Value.Value.ToString() : string.Empty ;
                    string  sHDL = (cItem.HDL.Value.HasValue) ? cItem.HDL.Value.Value.ToString() : string.Empty;
                    string  sLDL = (cItem.LDL.Value.HasValue) ? cItem.LDL.Value.Value.ToString() : string.Empty;
                    string  sTrig = (cItem.Triglycerides.Value.HasValue) ? cItem.Triglycerides.Value.Value.ToString() : string.Empty;

                    if (cRiskLevel != null)
                    {
                        if (!string.IsNullOrEmpty(sTC))
                        {
                            sTC = "<span class=\"" + cRiskLevel.ColorReading1 + "\">" + sTC + "</span>";
                        }
                        if (!string.IsNullOrEmpty(sHDL) && isGenderMale )
                        {
                            sHDL = "<span class=\"" + cRiskLevel.ColorReading2 + "\">" + sHDL + "</span>";
                        }
                        if (!string.IsNullOrEmpty(sLDL))
                        {
                            sLDL = "<span class=\"" + cRiskLevel.ColorReading3 + "\">" + sLDL + "</span>";
                        }
                        if (!string.IsNullOrEmpty(sTrig))
                        {
                            sTrig = "<span class=\"" + cRiskLevel.ColorReading4 + "\">" + sTrig + "</span>";
                        }
                    }


                    // date, total cholesterol, HDL, LDL, Triglycerides, Source, Comments
                    sb.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>",
                                                cItem.When.Value.ToShortDateString(), 
                                                sTC,
                                                sHDL,
                                                sLDL,
                                                sTrig,
                                                cItem.Source.Value,
                                                FormatNote(cItem.Note.Value)));
                }
            }
            sb.Append("</tbody>");
        }

        private string GetBloodGlucoseReadingType(string rt)
        {
            string retval = string.Empty;

            if (!string.IsNullOrEmpty(rt))
            {
                switch (rt.ToLower())
                {
                    case "postprandial":    // Internationalization issue: current language id
                        retval = AHAHelpContent.ListItem.FindListItemNameByListItemCode("postprandial", AHACommon.AHADefs.VocabDefs.ReadingType, AHACommon.UXControl.EnglishLanguage );
                        break;
                    case "fasting":         // Internationalization issue: current language id
                        retval = AHAHelpContent.ListItem.FindListItemNameByListItemCode("fasting", AHACommon.AHADefs.VocabDefs.ReadingType, AHACommon.UXControl.EnglishLanguage);
                        break;
                    default:
                        retval = rt;
                        break;
                }

            }
            return retval;
        }

        private void FormatBloodGlucoseTable(ref StringBuilder sb)
        {
            sb.Append("<thead><tr><th>Date</th><th>Time</th><th>BG <em>(mg/dL)</em></th><th>READING TYPE</th><th>ACTION TAKEN</th><th>Comments</th></tr></thead><tbody>");

            LinkedList<HVManager.BloodGlucoseDataManager.BGItem> ascendingList = HVManager.AllItemManager.GetItemManagersForRecordId(this.mPatientGuids.PersonID, this.mPatientGuids.RecordID).BloodGlucoseDataManager.GetDataBetweenDates(mDataRange);
            if (ascendingList != null && ascendingList.Count > 0)
            {
                List<GroupManager.GroupInfo> riskGroups = Heart360Global.Main.GetSystemCreatedGroups();
                int? daysOffset = AHACommon.AHAAppSettings.ColorCoding_EffectiveDays;

                foreach (HVManager.BloodGlucoseDataManager.BGItem bgItem in ascendingList.Reverse())
                {
                    string sBG = GRCBase.StringHelper.GetFormattedDoubleString(bgItem.Value.Value, 2, false);

                    if ((daysOffset == null) || bgItem.EffectiveDate.CompareTo(DateTime.Now.AddDays(daysOffset.Value)) == 1)
                    {
                        RiskLevelHelper.RiskLevelBloodGlucose rlBG = new RiskLevelHelper.RiskLevelBloodGlucose(riskGroups, bgItem.Value.Value);

                        sBG = "<span class=\"" + rlBG.ColorReading1 + "\">" + sBG + "</span>";                       
                    }

                    // date, time, BG, Reading Type, Action Taken, Comments
                    sb.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>",
                                                bgItem.When.Value.ToShortDateString(), bgItem.When.Value.ToShortTimeString(),
                                                sBG,                      
                                                GetBloodGlucoseReadingType( bgItem.ReadingType.Value),
                                                (!string.IsNullOrEmpty(bgItem.ActionTaken.Value)) ? bgItem.ActionTaken.Value : string.Empty,
                                                FormatNote(bgItem.Note.Value)));
                }
            }
            sb.Append("</tbody>");
        }

        private void FormatHbA1cTable(ref StringBuilder sb2)
        {
            sb2.Append("<div>HbA1c</div>" + "<thead><tr><th>Date</th><th>Time</th><th>HBA1c <em>(%)</em></th><th>SOURCE</th><th>Comments</th></tr></thead><tbody>");

            LinkedList<HVManager.HbA1cDataManager.HbA1cItem> ascendingList = HVManager.AllItemManager.GetItemManagersForRecordId(this.mPatientGuids.PersonID, this.mPatientGuids.RecordID).HbA1cDataManager.GetDataBetweenDates(mDataRange);

            if (ascendingList != null && ascendingList.Count > 0)
            {
                foreach (HVManager.HbA1cDataManager.HbA1cItem item in ascendingList.Reverse())
                {
                    string sHbA1c = GRCBase.StringHelper.HbA1cToPercentage(item.Value.Value).ToString();

                    sb2.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                        item.When.Value.ToShortDateString(),
                        item.When.Value.ToShortTimeString(),
                        sHbA1c,
                        item.Source.Value,
                        FormatNote(item.Note.Value)
                        ));
                }

            }

            sb2.Append("</tbody>");
        }

        private void FormatWeightTable(ref StringBuilder sb)
        {
            HVManager.AllItemManager hvMgr = HVManager.AllItemManager.GetItemManagersForRecordId(this.mPatientGuids.PersonID, this.mPatientGuids.RecordID);

            sb.Append("<thead><tr><th>Date</th><th>Time</th><th>Weight <em>(lbs)</em></th><th>BMI</th><th>Source</th><th>Comments</th></tr></thead><tbody>");

            LinkedList<HVManager.WeightDataManager.WeightItem> ascendingList = hvMgr.WeightDataManager.GetDataBetweenDates(mDataRange);
            if (ascendingList != null && ascendingList.Count > 0)
            {
                List<GroupManager.GroupInfo> riskGroups = Heart360Global.Main.GetSystemCreatedGroups();
                int? daysOffset = AHACommon.AHAAppSettings.ColorCoding_EffectiveDays;

                foreach (HVManager.WeightDataManager.WeightItem wItem in ascendingList.Reverse())
                {
                    // date, time, Weight, BMI, Source, Comments
                    double? bmiForWeight = AHABusinessLogic.VirtualCoach.UserSummary.GetBMIForCurrentRecordForWeight(hvMgr, wItem.CommonWeight.Value.ToPounds());

                    string sBMI = string.Empty;

                    if (bmiForWeight.HasValue)
                    {
                        RiskLevelHelper.RiskLevelBMI bmiRL = null;

                        if ((daysOffset == null) || wItem.EffectiveDate.CompareTo(DateTime.Now.AddDays(daysOffset.Value)) == 1)
                        {
                            bmiRL = new RiskLevelHelper.RiskLevelBMI( riskGroups, bmiForWeight.Value);
                        }

                        if (bmiRL != null)
                            sBMI = "<span class=\"" + bmiRL.ColorReading1 + "\">" + GRCBase.StringHelper.GetFormattedDoubleString(bmiForWeight.Value, 2, false) + "</span>";
                        else
                            sBMI = GRCBase.StringHelper.GetFormattedDoubleString(bmiForWeight.Value, 2, false);
                    }

                    sb.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>",
                                                wItem.When.Value.ToShortDateString(), wItem.When.Value.ToShortTimeString(),
                                                GRCBase.StringHelper.GetFormattedDoubleString(wItem.CommonWeight.Value.ToPounds(), 2, false),
                                                sBMI,
                                                wItem.Source.Value,                       
                                                FormatNote(wItem.Note.Value)));
                }
            }
            sb.Append("</tbody>");
        }

        private void FormatPhysicalActivityTable(ref StringBuilder sb)
        {
            HVManager.AllItemManager hvMgr = HVManager.AllItemManager.GetItemManagersForRecordId(this.mPatientGuids.PersonID, this.mPatientGuids.RecordID);

            sb.Append("<thead><tr><th>Date</th><th>Activity</th><th>Duration</th><th>Intensity</th><th>Steps</th><th>Comments</th></tr></thead><tbody>");

            LinkedList<HVManager.ExerciseDataManager.ExerciseItem> ascendingList = hvMgr.ExerciseDataManager.GetDataBetweenDates(mDataRange);
            if (ascendingList != null && ascendingList.Count > 0)
            {
                foreach (HVManager.ExerciseDataManager.ExerciseItem eItem in ascendingList.Reverse())
                {
                    // date, activity, duration, intensity, steps, comments
                    string sIntensity = string.Empty;

                    switch (eItem.Intensity.Value)
                    {
                        case Microsoft.Health.ItemTypes.RelativeRating.VeryHigh:
                        case Microsoft.Health.ItemTypes.RelativeRating.High: 
                            sIntensity = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Vigorous", AHACommon.AHADefs.VocabDefs.ExerciseIntensity, AHACommon.UXControl.EnglishLanguage);
                            break;

                        case Microsoft.Health.ItemTypes.RelativeRating.Low:
                        case Microsoft.Health.ItemTypes.RelativeRating.VeryLow:
                            sIntensity = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Light", AHACommon.AHADefs.VocabDefs.ExerciseIntensity, AHACommon.UXControl.EnglishLanguage);
                            break;

                        case Microsoft.Health.ItemTypes.RelativeRating.Moderate: 
                            sIntensity = AHAHelpContent.ListItem.FindListItemNameByListItemCode("Moderate", AHACommon.AHADefs.VocabDefs.ExerciseIntensity, AHACommon.UXControl.EnglishLanguage);
                            break;

                        case Microsoft.Health.ItemTypes.RelativeRating.None:
                        default:
                            break;
                    }

                    sb.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>",
                                                eItem.When.Value.ToShortDateString(),
                                                eItem.Activity.Value,
                                                (eItem.Duration.Value.HasValue) ? GRCBase.StringHelper.GetFormattedDoubleString(eItem.Duration.Value.Value, 2, false) : string.Empty,
                                                sIntensity,
                                                (eItem.NumberOfSteps.Value.HasValue) ? eItem.NumberOfSteps.Value.Value.ToString() : string.Empty,
                                                FormatNote(eItem.Note.Value)));
                }
            }
            sb.Append("</tbody>");
        }

        protected void OnPDFReport_Click(object sender, EventArgs e)
        {
            switch (mTrackerType)
            {
                case AHACommon.PatientTrackers.TrackerType.BloodPressure:
                    Heart360Provider.Reports.PDFHelper.CreateReport(mPatientGuids, Heart360Provider.Reports.PDFType.BloodPressure, mDataRange, Heart360Provider.Reports.DataViewType.TableAndCombinedChart, null);
                    break;

                case AHACommon.PatientTrackers.TrackerType.Cholesterol:
                    Heart360Provider.Reports.PDFHelper.CreateReport(mPatientGuids, Heart360Provider.Reports.PDFType.Cholesterol, mDataRange, Heart360Provider.Reports.DataViewType.TableAndCombinedChart, null);
                    break;

                case AHACommon.PatientTrackers.TrackerType.BloodGlucose:
                    Heart360Provider.Reports.PDFHelper.CreateReport(mPatientGuids, Heart360Provider.Reports.PDFType.BloodGlucose, mDataRange, Heart360Provider.Reports.DataViewType.TableAndChart, null);
                    break;

                case AHACommon.PatientTrackers.TrackerType.PhysicalActivity:
                    Heart360Provider.Reports.PDFHelper.CreateReport(mPatientGuids, Heart360Provider.Reports.PDFType.Exercise, mDataRange, Heart360Provider.Reports.DataViewType.TableAndChart, null);
                    break;

                case AHACommon.PatientTrackers.TrackerType.Weight:
                    Heart360Provider.Reports.PDFHelper.CreateReport(mPatientGuids, Heart360Provider.Reports.PDFType.Weight, mDataRange, Heart360Provider.Reports.DataViewType.TableAndChart, null);
                    break;

                default:
                    break;
            }
        }

    }
}