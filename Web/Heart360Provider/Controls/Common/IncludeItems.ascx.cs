﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using AHACommon;

namespace Heart360Provider.Controls.Common
{
    public partial class IncludeItems : System.Web.UI.UserControl
    {
        public string Locale = "en-US";
        public string VersionNumber = string.Empty;
        protected string TrapJSErrors = "false";
        protected string AppVirtualPath = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            Locale = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
            VersionNumber = AHAAppSettings.VersionNumber;
            TrapJSErrors = AHAAppSettings.EnableJSErrorLog.ToString().ToLower();

            if (HttpRuntime.AppDomainAppVirtualPath != "/")
            {
                AppVirtualPath = HttpRuntime.AppDomainAppVirtualPath;
            }
        }
    }
}