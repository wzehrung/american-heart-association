﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHABusinessLogic;
using Heart360Provider.Graph;

namespace Heart360Provider.Controls.Common
{
    public partial class TrackerGraph : System.Web.UI.UserControl
    {
        //IPeriodicDataManager m_IPeriodicDataManager = null;
        AHACommon.PatientTrackers.TrackerType   mTrackerType;
        PatientGuids                            mPatientGuids;
        AHACommon.DataDateReturnType            mDataRange = AHACommon.DataDateReturnType.Last3Months ; // default
        bool                                    mUseDivTrigger = true;

        /**
        public IPeriodicDataManager IPeriodicDataManager
        {
            set
            {
                m_IPeriodicDataManager = value;
            }
        }
        **/

        public AHACommon.PatientTrackers.TrackerType TrackerType
        {
            get { return mTrackerType; }
            set { mTrackerType = value; }
        }

        public PatientGuids PatientOfflineGuids
        {
            set { mPatientGuids = value; }
        }

        public AHACommon.DataDateReturnType DataRange
        {
            set { mDataRange = value; }
        }

        public int? GraphWidth
        {
            get;
            set;
        }

        public bool UseDivTrigger
        {
            set { mUseDivTrigger = value; }
        }



        // We render the graphs in PreRender because this stage is after any widget event handling (e.g. DateRange picker),
        // which happens after Page_Load.
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //this.divOptionalTrigger.Visible = mUseDivTrigger;
            this.HealthGraph1.Visible = false;

            Graph.GraphType gt1 = Graph.GraphType.AllInOne;   // not a valid default
            Graph.GraphType gt2 = Graph.GraphType.AllInOne;   // not a valid default
            bool do_gt1 = false;
            bool do_gt2 = false;

            switch (mTrackerType)
            {
                case AHACommon.PatientTrackers.TrackerType.BloodGlucose:
                    gt1 = Graph.GraphType.Glucose;
                    do_gt1 = true;
                    break;

                case AHACommon.PatientTrackers.TrackerType.BloodPressure:
                    gt1 = Graph.GraphType.CombinedBP;
                    do_gt1 = true;
                    break;

                case AHACommon.PatientTrackers.TrackerType.Cholesterol:
                    gt1 = Graph.GraphType.CombinedCholesterol;
                    gt2 = Graph.GraphType.Triglycerides;
                    do_gt1 = do_gt2 = true;
                    break;

                case AHACommon.PatientTrackers.TrackerType.PhysicalActivity:
                    gt1 = Graph.GraphType.Exercise;
                    do_gt1 = true;
                    break;

                case AHACommon.PatientTrackers.TrackerType.Weight:
                    gt1 = Graph.GraphType.Weight;
                    do_gt1 = true;
                    break;

                default:
                    break;
            }

            if (do_gt1)
            {
                Graph.AHAPlotData plotData1 = Graph.AHAPlotData.ConstructPlotData(mPatientGuids, gt1, mDataRange, null);

                if (this.GraphWidth.HasValue)
                {
                    plotData1.m_graphWidth = this.GraphWidth.Value;
                }

                ((Heart360Provider.Controls.Common.HealthGraph)this.HealthGraph1).PlotGraph(plotData1);
            }
            this.HealthGraph1.Visible = do_gt1;

            if (do_gt2)
            {
                Graph.AHAPlotData plotData2 = Graph.AHAPlotData.ConstructPlotData(mPatientGuids, gt2, mDataRange, null);

                if (this.GraphWidth.HasValue)
                {
                    plotData2.m_graphWidth = this.GraphWidth.Value;
                }

                ((Heart360Provider.Controls.Common.HealthGraph)this.HealthGraph2).PlotGraph(plotData2);
            }
            this.HealthGraph2.Visible = do_gt2;
        }
    }
}