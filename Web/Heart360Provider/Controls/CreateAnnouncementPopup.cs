﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class CreateAnnouncementPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            StringBuilder markup = new StringBuilder(this.PrepareMarkup());

            ProviderContext provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderDetails provDetails = (provider != null) ? AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID) : null;

            //popuplate the default data
            markup.Replace("$Title$", "");
            markup.Replace("$Content$", "");
            markup.Replace("$ProviderName$", provDetails.FormattedName);

            return markup.ToString();
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                ProviderContext provider = SessionInfo.GetProviderContext();
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                // validate required fields
                bool goodToGo = true;
                StringBuilder sb = new StringBuilder();
                string errMsg = string.Empty;

                // Loop over meta-data and do processing.
                foreach (ControlWidget cw in this.Metadata.Widgets)
                {
                    if (!cw.Validate(hashInputs[cw.Id], ref errMsg))
                    {
                        sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                        goodToGo = false;
                    }
                }

                if (!goodToGo)
                {
                    return JsonFailure(sb.ToString());
                }

                //create announcement
                AHAHelpContent.PatientContent.UpdateContent(0, AHACommon.AHADefs.ContentType.PatientAnnouncement.ToString(), 
                    hashInputs[this.MetadataHash["Title"].Id], null, hashInputs[this.MetadataHash["Content"].Id],
                    provider.ProviderID, EnglishLanguage, provider.ProviderID);

                return JsonRefresh("ADD ANNOUNCEMENT", "Announcement created.");
            }
            catch (Exception) { }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }
    }
}