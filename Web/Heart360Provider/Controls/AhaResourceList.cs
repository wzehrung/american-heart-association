﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class AhaResourceList : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            //get list
            List<Resource> resourceList = Resource.FindAllResources(true, EnglishLanguage);

            //if empty
            if (resourceList == null || resourceList.Count == 0)
            {
                return "<p>There are no AHA resources.</p>";
            }

            StringBuilder sb = new StringBuilder(this.PrepareMarkup());

            //build drop-down & description divs
            StringBuilder options = new StringBuilder();
            StringBuilder descriptions = new StringBuilder();
            foreach (Resource resource in resourceList)
            {
                options.Append("<option value='");
                options.Append(resource.ContentTitleUrl);
                options.Append("' data-id='");
                options.Append(resource.ResourceID);
                options.Append("'>");
                options.Append(resource.ContentTitle);
                options.Append("</option>\n");

                if (!string.IsNullOrEmpty(resource.ContentText))
                {
                    descriptions.Append("<div class='description-item force-hide' id='resource-");
                    descriptions.Append(resource.ResourceID);
                    descriptions.Append("'>");
                    descriptions.Append(resource.ContentText);
                    descriptions.Append("</div>");
                }
            }
            sb.Replace("$Options$", options.ToString());
            sb.Replace("$Descriptions$", descriptions.ToString());

            return sb.ToString();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }
    }
}