﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class EditGroupPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            AHAHelpContent.PatientGroup group = null;

            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                try
                {
                    int groupId = Convert.ToInt32(inputs[0].value);
                    group = AHAHelpContent.PatientGroup.FindByGroupID(groupId);
                }
                catch (Exception)
                {
                }
            }

            if(group == null) 
            {
                return "Sorry, an internal error occurred. Please close popup and try again.";
            }

            // Assign values to widgets so boilerplate can make markup replacements.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {
                switch (cw.WidgetType)
                {
                    case ControlWidgetType.String:
                        if (cw.Name.Equals("Name"))
                        {
                            cw.Value = group.Name;
                        }
                        else if (cw.Name.Equals("Description"))
                        {
                            cw.Value = group.Description;
                        }
                        break;

                    case ControlWidgetType.Hidden:
                        if (cw.Name.Equals("Id"))
                        {
                            cw.Value = group.GroupID.ToString();
                        }
                        break;

                    default:
                        break;
                }
            }



            //populate the group data
            StringBuilder markup = new StringBuilder(this.PrepareMarkup());

            //create group member scrolling checklist
            StringBuilder checklist = new StringBuilder();

            //Loop through list of participants for provider, checking to see which are already selected
            List<Patient> allPatientsForProvider = AHAHelpContent.Patient.FindAllByProviderID(group.ProviderID);
            List<Patient> groupPatients = AHAHelpContent.Patient.FindAllByGroupID(group.GroupID);
            List<PatientInfo> allPatientInfo = PatientInfo.GetPopulatedPatientInfo(allPatientsForProvider);

            if (allPatientsForProvider == null || allPatientsForProvider.Count == 0)
            {
                markup.Replace("$GroupMembers$", "<p>There are no patients/participants.</p>");
            }
            else
            {
                Hashtable groupHash = new Hashtable();
                foreach (Patient patient in groupPatients)
                {
                    groupHash.Add(patient.UserHealthRecordID, "");
                }

                foreach (Patient patient in allPatientsForProvider)
                {
                    checklist.Append("<div class='twelvecol first'><div class='inline-input'><input name='groups' id='groups' type='checkbox' value='");
                    checklist.Append(patient.UserHealthRecordID);
                    if (groupHash.ContainsKey(patient.UserHealthRecordID))
                    {
                        checklist.Append("' checked='true");
                    }
                    checklist.Append("' /></div><div class='inline-text'><label>");
                    checklist.Append(getPatientNameById(patient));
                    checklist.Append("</label></div></div>");
                }

                markup.Replace("$GroupMembers$", checklist.ToString());
            }

            return markup.ToString();
        }

        public static string getPatientNameById(Patient patient)
        {
            List<Patient> patientList = new List<Patient>();
            patientList.Add(patient);
            List<PatientInfo> patientInfo = PatientInfo.GetPopulatedPatientInfo(patientList);
            if (patientInfo != null && patientInfo.Count > 0)
            {
                return patientInfo[0].FullName;
            }
            return "";
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                // pull the input values off the form
                // can't use a Dictionary here, because the checkbox names repeat in the inputs array,
                // so we have to do some non-standard calling of boilerplate validation.

                int groupId = -1;
                string groupName = null;
                string groupDescription = null;
                List<int> patientsInGroup = new List<int>();

                foreach(NameValue nv in inputs)
                {
                    switch(nv.name) 
                    {
                        case "hidGroupId":
                            try
                            {
                                groupId = Convert.ToInt32(nv.value);
                            }
                            catch (Exception) { }
                            break;
                        case "txtGroupName":
                            groupName = nv.value;
                            break;

                        case "txtDescription":
                            groupDescription = nv.value;
                            break;
                        case "groups":
                            try
                            {
                                patientsInGroup.Add(Convert.ToInt32(nv.value));
                            }
                            catch (Exception) { }
                            break;
                    }   
                }

                // Now validate using boilerplate
                // Need to check that there are valid entries for each required field
                // Need to validate inputs
                bool goodToGo = true;
                StringBuilder sb = new StringBuilder();
                string errMsg = string.Empty;


                // Loop over meta-data and do processing.
                foreach (ControlWidget cw in this.Metadata.Widgets)
                {
                    if( (cw.Name.Equals("Name") && !cw.Validate( groupName, ref errMsg )) ||
                        (cw.Name.Equals("Description") && !cw.Validate(groupDescription, ref errMsg)) )
                    {
                        sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                        goodToGo = false;
                    }
                }

                if (!goodToGo)
                {
                    return JsonFailure(sb.ToString());
                }

                //if we have an ID, let's do this thing...
                if (groupId != -1)
                {
                    //retrieve the group, then overlay any text changes
                    AHAHelpContent.PatientGroup group = AHAHelpContent.PatientGroup.FindByGroupID(groupId);

                    group.Name = groupName;
                    if (groupDescription != null)
                    {
                        group.Description = groupDescription;
                    }
                    AHAHelpContent.PatientGroup.UpdateGroup(group.GroupID, group.Name, group.Description, group.GroupRuleXml, group.RemoveOption);

                    //update group memebership
                    AHAHelpContent.PatientGroup.AddPatientToGroup(patientsInGroup, group.GroupID, group.ProviderID);

                    return JsonRefresh("UPDATE GROUP", "Group updated.");
                }
            }
            catch (Exception) { }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }
    }
}