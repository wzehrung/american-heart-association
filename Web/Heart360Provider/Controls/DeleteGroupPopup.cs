﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class DeleteGroupPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            AHAHelpContent.PatientGroup group = null;

            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                try
                {
                    int groupId = Convert.ToInt32(inputs[0].value);
                    group = AHAHelpContent.PatientGroup.FindByGroupID(groupId);

                    //populate the group data
                    StringBuilder markup = new StringBuilder(this.PrepareMarkup());
                    markup.Replace("$GroupId$", group.GroupID.ToString());
                    markup.Replace("$GroupName$", group.Name);

                    return markup.ToString();
                }
                catch (Exception)
                {
                    //int number format exception
                }
            }

            return "Sorry, an internal error occurred (no ID). Please close popup and try again.";
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                string groupId = hashInputs["txtGroupId"];

                if (groupId != null && groupId.Length > 0)
                {
                    //delete the group
                    AHAHelpContent.PatientGroup.DeleteGroup(Convert.ToInt32(groupId));
                    //return JsonSuccess("Group deleted.");
                    return JsonRefresh( "DELETE GROUP", "Group deleted.");
                }
            }
            catch (Exception)
            {
                //the dictionary can throw exceptions, int number format exceptions, or the DeleteGroup process
            }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }


    }
}