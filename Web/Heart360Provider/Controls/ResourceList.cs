﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class ResourceList : UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            //get list
            ProviderContext provider = SessionInfo.GetProviderContext();
            List<Resource> resourceList = Resource.FindAllResources(true, EnglishLanguage, provider.ProviderID);

            //if empty
            if (resourceList == null || resourceList.Count == 0)
            {
                return "<p>There are no resources.</p>";
            }

            StringBuilder sb = new StringBuilder();

            //iterate
            foreach (Resource resource in resourceList)
            {
                //create a copy of the markup for each entry in the list
                StringBuilder item = new StringBuilder(this.PrepareMarkup());

                item.Replace("$Id$", resource.ResourceID.ToString());
                item.Replace("$Title$", resource.ContentTitle);
                item.Replace("$Content$", resource.ContentText);

                sb.Append(item.ToString());
            }

            return sb.ToString();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }
    }
}