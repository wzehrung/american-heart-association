﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;
using Heart360Provider.Reports;

namespace Heart360Provider.Controls
{
    public class StandardReport : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            //no $$ replacements needed
            return this.PrepareMarkup();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            //this isn't used as the report download requires direct page interaction, no HOTDOG for you!
            return JsonFailure("Not implemented");
        }

        public void RunReport(NameValueCollection inputs)
        {
            //parse form inputs
            string reportPeriod = inputs["standard-reportPeriod"];

            //setup report
            string reportHeading = "";
            string reportTitle = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_StandardPracticeReport").ToString();
            ProviderContext provider = SessionInfo.GetProviderContext();
            AHACommon.DataDateReturnType dateRange = DataDateReturnType.AllData;

            switch (reportPeriod)
            {
                case "3m":
                    dateRange = DataDateReturnType.Last3Months;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_StandardPracticeReportForlast3Months").ToString();
                    break;
                case "6m":
                    dateRange = DataDateReturnType.Last6Months;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_StandardPracticeReportForlast6Months").ToString();
                    break;
                case "9m":
                    dateRange = DataDateReturnType.Last9Months;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_StandardPracticeReportForlast9Months").ToString();
                    break;
                case "1y":
                    dateRange = DataDateReturnType.LastYear;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_StandardPracticeReportForlastYear").ToString();
                    break;
                case "2y":
                    dateRange = DataDateReturnType.Last2Years;
                    reportHeading = System.Web.HttpContext.GetGlobalResourceObject(
                        "Reports", "Patient_Report_StandardPracticeReportForlast2Years").ToString();
                    break;
                default:
                    PDFHelper.CreateErrorPDF();
                    return;
            }

            //create the report
            List<AHAHelpContent.Patient> allPatientsForProvider = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);
            PDFHelper.CreateSPReportPDF(dateRange, reportTitle, reportHeading, GetPatientGuidList(allPatientsForProvider));
        }

        private static List<PatientGuids> GetPatientGuidList(List<AHAHelpContent.Patient> listPatient)
        {
            if (listPatient == null)
                return new List<PatientGuids>();

            return listPatient.Select(i => new PatientGuids { PersonID = i.OfflinePersonGUID.Value, RecordID = i.OfflineHealthRecordGUID.Value }).ToList();
        }
    }
}