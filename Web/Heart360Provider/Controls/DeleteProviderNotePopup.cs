﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class DeleteProviderNotePopup : AHACommon.UXControl
    {
        private int mProviderNoteID;

        override public string GetMarkup(NameValue[] inputs)
        {
            // no widgets, all read-only data

            bool validInput = false;

            if (inputs.Length > 0 && inputs[0].name.Equals("id") && !string.IsNullOrEmpty(inputs[0].value))
            {
                validInput = int.TryParse(inputs[0].value, out mProviderNoteID);
            }

            ProviderContext provider = SessionInfo.GetProviderContext();
            AHAHelpContent.ProviderNote currentNote = (validInput) ? AHAHelpContent.ProviderNote.FindByProviderNoteID(mProviderNoteID) : null;

            if (currentNote == null)
            {
                return UXControl.FormatErrorPopupContent("Delete Note");
            }

            // if we are here, we are good to go

            string markup = this.PrepareMarkup();

            return markup.Replace("$Title$", currentNote.Title );
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Delete button.
            AHAHelpContent.ProviderNote currentNote = AHAHelpContent.ProviderNote.FindByProviderNoteID(mProviderNoteID);

            if (currentNote != null)
            {
                AHAHelpContent.ProviderNote.DeleteProviderNote(mProviderNoteID);
                mProviderNoteID = -1;
                return JsonRefresh( "DELETE NOTE", "Successfully deleted Note");
            }

            return JsonFailure("Internal error deleting Note.");
        }
    }
}