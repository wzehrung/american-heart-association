﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class ReceivedInvitations : AHACommon.UXControl
    {
        // this method is from V5 AHAWeb/Provider/Home.aspx.cs

        private void CancelAndBlockInvitation(int iInvitationID, int iProviderID )
        {
            PatientProviderInvitation objInvitation = PatientProviderInvitation.FindByInvitationID(iInvitationID);

            PatientProviderInvitationDetails objAvailableDetails = objInvitation.PendingInvitationDetails.First();

            //Marks the existing invitations, sent by this patient to the current provider, as blocked
            //cannot user objInvitation.PatientID.Value as patient id is null because whilr creating an invitation patient id is passed as null
            PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(objInvitation.PatientID.Value, iProviderID, false);

            objAvailableDetails.Status = PatientProviderInvitationDetails.InvitationStatus.Blocked;

            objAvailableDetails.Update();
        }


        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext provider = SessionInfo.GetProviderContext();

            List<AHAHelpContent.PatientProviderInvitation> ilist = (provider != null) ?
                AHAHelpContent.PatientProviderInvitation.FindPendingInvitationsByAcceptedProvider(
                                                                        provider.ProviderID,
                                                                        PatientProviderInvitationDetails.InvitationStatus.Pending) : null;


            if (ilist == null || ilist.Count < 1)
            {
                return "There are no pending invitations.";
            }

            StringBuilder sb = new StringBuilder();
            string markup = this.PrepareMarkup();


            foreach (PatientProviderInvitation ppi in ilist)
            {
                HVManager.PersonalDataManager.PersonalItem objPersonalInfo = Heart360Provider.Utility.PatientDetails.GetPatientDetails(ppi.PatientID.Value);

                if (objPersonalInfo == null)
                {
                    CancelAndBlockInvitation(ppi.InvitationID, provider.ProviderID);
                }
                else
                {
                    // ACCEPT | DECLINE are popups. Because getting the patient/participant name is such a chore, we will send that with the Invitation Id
                    // in a pipe-delimited value set.
                    string acceptaction = "<div class=\"popuplink\" data-page=\"Pages/Participants\" data-method=\"AcceptPopupHandler\" data-id=\"" + ppi.InvitationCode + "|" + objPersonalInfo.FullName + "\" data-refresh-ids=\"idControlReceivedInvitationsList,idModuleHeader4,idControlPatientList\">ACCEPT</div> ";
                    string declineaction = "<div class=\"popuplink leftspacing\" data-page=\"Pages/Participants\" data-method=\"DeclinePopupHandler\" data-id=\"" + ppi.InvitationID.ToString() + "|" + objPersonalInfo.FullName + "\" data-refresh-ids=\"idControlReceivedInvitationsList,idModuleHeader4\">DECLINE</div> ";

                    sb.Append("<tr class=\"received-name\"><td>" + objPersonalInfo.FullName + "</td><td class=\"actions-column\">" + declineaction + acceptaction  + "</td></tr>");
                }
            }
            

            return markup.Replace("$InvitationList$", sb.ToString());
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Internal Error (code pathway execution)");
            /**
             ** This functionality was nixed by AHA
             **
            // This gets called when the "Accept All" button is clicked
            int             nbAccepted = 0;
            int             nbWithIssues = 0;
            ProviderContext provider = SessionInfo.GetProviderContext();

            List<AHAHelpContent.PatientProviderInvitation> ilist = (provider != null) ?
                AHAHelpContent.PatientProviderInvitation.FindPendingInvitationsByAcceptedProvider(
                                                                        provider.ProviderID,
                                                                        PatientProviderInvitationDetails.InvitationStatus.Pending) : null;

            foreach (AHAHelpContent.PatientProviderInvitation ppi in ilist)
            {
                try
                {
                    if (Heart360Provider.Utility.Invitations.AcceptInvitation(provider, ppi.InvitationCode))
                        nbAccepted += 1;
                    else
                        nbWithIssues += 1;
                }
                catch (Exception)
                {
                    nbWithIssues += 1;
                }
            }

            return (nbWithIssues == 0) ? JsonRefresh( string.Format("Accepted {0} invitations.", nbAccepted.ToString() ) ) :
                                         JsonRefresh( string.Format("Accepted {0} invitations and {1} invitations had internal errors and were closed.", nbAccepted.ToString(), nbWithIssues.ToString() ) ) ;
            **/
        }

    }
}