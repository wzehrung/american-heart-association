﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class CreateGroupPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            ProviderContext provider = SessionInfo.GetProviderContext();

            StringBuilder markup = new StringBuilder(this.PrepareMarkup());
            markup.Replace("$GroupName$", "");
            markup.Replace("$Description$", "");

            //create group member scrolling checklist
            StringBuilder checklist = new StringBuilder();

            //Create a list of participants for provider
            List<Patient> allPatientsForProvider = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);
            List<PatientInfo> allPatientInfo = PatientInfo.GetPopulatedPatientInfo(allPatientsForProvider);

            if (allPatientsForProvider == null || allPatientsForProvider.Count == 0)
            {
                markup.Replace("$GroupMembers$", "<p>There are no patients/participants.</p>");
            }
            else
            {
                foreach (Patient patient in allPatientsForProvider)
                {
                    checklist.Append("<div class='twelvecol first'><div class='inline-input'><input name='groups' id='groups' type='checkbox' value='");
                    checklist.Append(patient.UserHealthRecordID);
                    checklist.Append("' /></div><div class='inline-text'><label>");
                    checklist.Append(getPatientNameById(patient));
                    checklist.Append("</label></div></div>");
                }

                markup.Replace("$GroupMembers$", checklist.ToString());
            }

            return markup.ToString();
        }

        public static string getPatientNameById(Patient patient)
        {
            List<Patient> patientList = new List<Patient>();
            patientList.Add(patient);
            List<PatientInfo> patientInfo = PatientInfo.GetPopulatedPatientInfo(patientList);
            if (patientInfo != null && patientInfo.Count > 0)
            {
                return patientInfo[0].FullName;
            }
            return "";
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                ProviderContext provider = SessionInfo.GetProviderContext();

                //pull the input values off the form
                //can't use a Dictionary here, because the checkbox names repeat in the inputs array
                string groupType = null;
                string groupName = null;
                string groupDescription = null;
                List<int> patientsInGroup = new List<int>();
                string addRemove = null;
                string age = null;
                int ageAbove = -1;
                int ageBelow = -1;
                string gender = null;
                int repeaterNumberMax = 0;

                //repeating items defining rules for the group
                Dictionary<string, string> ruleEntries = new Dictionary<string, string>();

                foreach (NameValue nv in inputs)
                {
                    switch (nv.name)
                    {
                        //generic items
                        case "groupType":
                            //M: manual, D: dynamic
                            groupType = nv.value;
                            break;
                        case "txtGroupName":
                            groupName = nv.value;
                            break;
                        case "txtDescription":
                            groupDescription = nv.value;
                            break;

                        //Manual group item
                        case "groups":
                            int iGroups;
                            if (Int32.TryParse(nv.value, out iGroups))
                            {
                                patientsInGroup.Add(iGroups);
                            }
                            break;

                        //Dyanmic group items
                        case "addRemove": //radio, both, add
                            addRemove = nv.value;
                            break;
                        case "age": //radio: above, below, any
                            age = nv.value;
                            break;
                        case "ageAbove": // text
                            int iAgeAbove;
                            if (Int32.TryParse(nv.value, out iAgeAbove))
                            {
                                ageAbove = iAgeAbove;
                            }
                            break;
                        case "ageBelow": //text
                            int iAgeBelow;
                            if (Int32.TryParse(nv.value, out iAgeBelow))                            
                            {
                                ageBelow = iAgeBelow;
                            }
                            break;
                        case "gender": //radio: male, female, both
                            gender = nv.value;
                            break;
                        case "repeater-number": //hidden, max number for rule entry repeaters
                            int iRepeaterNumber;
                            if (Int32.TryParse(nv.value, out iRepeaterNumber))
                            {
                                repeaterNumberMax = iRepeaterNumber;
                            }
                            break;
                        //collect the repeating rule entry items
                        default:
                            ruleEntries.Add(nv.name, nv.value);
                            break;
                    }
                }

                //validate common fields
                if (string.IsNullOrEmpty(groupName))
                {
                    return JsonFailure("Group Name is required.");
                }

                //create manual group
                if (groupType.Equals("M"))
                {
                    //create group
                    PatientGroup newGroup = AHAHelpContent.PatientGroup.CreateGroup(
                        provider.ProviderID, groupName, groupDescription, "", PatientGroup.GroupPatientRemoveOption.M, false);

                    //update group memebership
                    AHAHelpContent.PatientGroup.AddPatientToGroup(patientsInGroup, newGroup.GroupID, newGroup.ProviderID);

                    return JsonRefresh( "CREATE GROUP", "Manual group created.");
                }
                //create dynamic / rule-based group
                else if (groupType.Equals("D"))
                {
                    //addRemove: both, add
                    if (string.IsNullOrEmpty(addRemove))
                    {
                        return JsonFailure("An Add/Remove selection is required.");
                    }
                    PatientGroup.GroupPatientRemoveOption removeOption = PatientGroup.GroupPatientRemoveOption.D;
                    if (addRemove.Equals("add"))
                    {
                        removeOption = PatientGroup.GroupPatientRemoveOption.A;
                    }

                    //age: above, below, any
                    if (string.IsNullOrEmpty(age))
                    {
                        return JsonFailure("An Age selection is required.");
                    }
                    GroupManager.Age groupAge = new GroupManager.Age();
                    if (age.Equals("above"))
                    {
                        if (ageAbove == -1)
                        {
                            return JsonFailure("A value for Age Above is required.");
                        }
                        groupAge.Value = ageAbove;
                        groupAge.IsAbove = true;
                    }
                    else if (age.Equals("below"))
                    {
                        if (ageBelow == -1)
                        {
                            return JsonFailure("A value for Age Below is required.");
                        }
                        groupAge.Value = ageBelow;
                        groupAge.IsAbove = false;
                    }

                    //gender: male, female, both
                    if (string.IsNullOrEmpty(gender))
                    {
                        return JsonFailure("A Gender selection is required.");
                    }
                    GroupManager.Gender groupGender = new GroupManager.Gender();
                    if (gender.Equals("male"))
                    {
                        groupGender.IsMale = true;
                    }
                    else if (gender.Equals("female"))
                    {
                        groupGender.IsMale = false;
                    }
                    else if (gender.Equals("both"))
                    {
                        groupGender.IsMale = null;
                    }

                    //parse dynamic group rule entries
                    string error = null;
                    List<GroupManager.Rule> ruleList = GroupPopupUtils.ParseRuleEntries(ruleEntries, repeaterNumberMax, out error);
                    if (error != null)
                    {
                        return JsonFailure(error);
                    }

                    //create group rule
                    GroupManager.GroupRule grule = new GroupManager.GroupRule(groupAge, groupGender, ruleList);

                    //create group
                    PatientGroup newGroup = AHAHelpContent.PatientGroup.CreateGroup(
                        provider.ProviderID, groupName, groupDescription, grule.GetGroupRuleXml(), removeOption, false);

                    return JsonRefresh( "CREATE GROUP", "Dynamic group created.");
                }
            }
            catch (Exception) { }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }

    }
}