﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHABusinessLogic;
using AHAProvider;
using Heart360Provider.Reports;

namespace Heart360Provider.Controls
{
    public class CustomReport : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            //no $$ replacements needed
            return this.PrepareMarkup();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            //this isn't used as the report download requires direct page interaction, no HOTDOG for you!
            return JsonFailure("Not implemented");
        }

        public void RunReport(NameValueCollection inputs)
        {
            //parse form inputs
            string reportPeriod = "";
            double amount = 0.0;
            int? occurences = null;
            bool? isAbove = null;
            CustomPracticeReportType reportType = CustomPracticeReportType.BloodGlucose;
            CustomPracticeReportHelper.ReportFunction reportFunction = CustomPracticeReportHelper.ReportFunction.AboveOrBelowThreshold;
            AHACommon.DataDateReturnType dateRange = DataDateReturnType.AllData;

            if(!string.IsNullOrEmpty(inputs["custom-readingType"])) //drop-down
            {
                string value = inputs["custom-readingType"];
                switch (value)
                {
                    //TODO - what form is needed?
                    case "bs": //BP Systolic
                        reportType = CustomPracticeReportType.Systolic;
                        break;   
                    case "bd": //BP Diastolic
                        reportType = CustomPracticeReportType.Diastolic;
                        break;
                    case "ct": //Cholesterol Total
                        reportType = CustomPracticeReportType.TotalCholesterol;
                        break;   
                    case "ch": //Cholesterol HDL
                        reportType = CustomPracticeReportType.HDL;
                        break;
                    case "cl": //Cholesterol LDL
                        reportType = CustomPracticeReportType.LDL;
                        break;
                    case "bg": //Blood Glucose
                        reportType = CustomPracticeReportType.BloodGlucose;
                        break;
                    case "w": //Weight
                        reportType = CustomPracticeReportType.Weight;
                        break;
                }
            }

            if(!string.IsNullOrEmpty(inputs["custom-condition"])) //radio
            {
                string value = inputs["custom-condition"];

                if (value.Equals("1")) //radio one
                {
                    reportFunction = CustomPracticeReportHelper.ReportFunction.AboveOrBelowThreshold;
                    if (!string.IsNullOrEmpty(inputs["custom-condition1"])) //drop-down
                    {
                        string valueC1 = inputs["custom-condition1"];
                        if (valueC1.Equals("above"))
                        {
                            isAbove = true;
                        }
                        else if (valueC1.Equals("below"))
                        {
                            isAbove = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(inputs["custom-condition1value"])) //string
                    {
                        double dAmount1;
                        if (Double.TryParse(inputs["custom-condition1value"], out dAmount1))
                        {
                            amount = dAmount1;
                        }
                    }
                }
                else if (value.Equals("2")) //radio two
                {
                    reportFunction = CustomPracticeReportHelper.ReportFunction.AboveOrBelowThresholdXTimes;
                    if (!string.IsNullOrEmpty(inputs["custom-condition2"])) //drop-down
                    {
                        string valueC2 = inputs["custom-condition2"];
                        if (valueC2.Equals("above"))
                        {
                            isAbove = true;
                        }
                        else if (valueC2.Equals("below"))
                        {
                            isAbove = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(inputs["custom-condition2value"])) //string
                    {
                        double dAmount2;
                        if (Double.TryParse(inputs["custom-condition2value"], out dAmount2))
                        {
                            amount = dAmount2;
                        }
                    }

                    if (!string.IsNullOrEmpty(inputs["custom-occurences"])) //drop-down
                    {
                        int iOccurences;
                        if (Int32.TryParse(inputs["custom-occurences"], out iOccurences))
                        {
                            occurences = iOccurences;
                        }
                    }
                }
                else if (value.Equals("3")) //radio three
                {
                    reportFunction = CustomPracticeReportHelper.ReportFunction.ThresholdChanged;
                    if (!string.IsNullOrEmpty(inputs["custom-changedBy"]))
                    {
                        double dChangedBy;
                        if (Double.TryParse(inputs["custom-changedBy"], out dChangedBy))
                        {
                            amount = dChangedBy;
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(inputs["custom-reportPeriod"])) //string
            {
                reportPeriod = inputs["custom-reportPeriod"];
            }

            switch (reportPeriod)
            {
                case "3m":
                    dateRange = DataDateReturnType.Last3Months;
                    break;
                case "6m":
                    dateRange = DataDateReturnType.Last6Months;
                    break;
                case "9m":
                    dateRange = DataDateReturnType.Last9Months;
                    break;
                case "1y":
                    dateRange = DataDateReturnType.LastYear;
                    break;
                case "2y":
                    dateRange = DataDateReturnType.Last2Years;
                    break;
                default:
                    PDFHelper.CreateErrorPDF();
                    return;
            }

            //run report
            string reportTitle = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_CustomPracticeReport").ToString();
            string reportHeading = HttpContext.GetGlobalResourceObject("Reports", "Provider_Reports_CustomPracticeReport").ToString();

            ProviderContext provider = SessionInfo.GetProviderContext();
            List<AHAHelpContent.Patient> patientList = AHAHelpContent.Patient.FindAllByProviderID(provider.ProviderID);

            PDFHelper.CreateCPReportPDF(patientList, reportFunction, reportType, dateRange, amount, isAbove, occurences, reportTitle, reportHeading);
        }
    }
}