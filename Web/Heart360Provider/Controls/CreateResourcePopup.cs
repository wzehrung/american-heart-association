﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class CreateResourcePopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            StringBuilder markup = new StringBuilder(this.PrepareMarkup());

            //populate the default data
            markup.Replace("$Title$", "");
            markup.Replace("$Link$", "");
            markup.Replace("$Content$", "");
            markup.Replace("$Expiration Date$", "");

            return markup.ToString();
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                ProviderContext provider = SessionInfo.GetProviderContext();
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                // validate required fields
                bool goodToGo = true;
                StringBuilder sb = new StringBuilder();
                string errMsg = string.Empty;

                // Loop over meta-data and do processing.
                foreach (ControlWidget cw in this.Metadata.Widgets)
                {
                    if (!cw.Validate(hashInputs[cw.Id], ref errMsg))
                    {
                        sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                        goodToGo = false;
                    }
                }

                if (!goodToGo)
                {
                    return JsonFailure(sb.ToString());
                }

                //Date validation
                DateTime expirationDate;
                if (!DateTime.TryParse(hashInputs[this.MetadataHash["Expiration Date"].Id], out expirationDate))
                {
                    return JsonFailure("Expiration Date not understood, please try again.");
                }

                //create resource
                Resource.CreateResource(
                    hashInputs[this.MetadataHash["Title"].Id], hashInputs[this.MetadataHash["Link"].Id], hashInputs[this.MetadataHash["Content"].Id],
                    true, expirationDate, provider.ProviderID, provider.ProviderID, EnglishLanguage, provider.ProviderID);

                return JsonRefresh("ADD RESOURCE", "Resource created.");
            }
            catch (Exception) { }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }
    }
}