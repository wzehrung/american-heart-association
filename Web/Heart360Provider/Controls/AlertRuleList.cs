﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class AlertRuleList: AHACommon.UXControl
    {

        private string getWhoContent( AlertRule objAlertRule )
        {
            string  rez = "-" ;

            if (objAlertRule.AlertRuleSubscriberType == AHAHelpContent.AlertRuleSubscriberType.A)
            {
                rez = Utility.UIHelper.GetHtmlAtagForAllPatients( "All Patients/Participants" ) ;
            }
            else if (objAlertRule.AlertRuleSubscriberType == AHAHelpContent.AlertRuleSubscriberType.G)
            {
                AHAHelpContent.PatientGroup pg = PatientGroup.FindByGroupID( objAlertRule.GroupID.Value ) ;

                rez = Utility.UIHelper.GetHtmlAtagForPatientGroup( pg ) ;
            }
            else if (objAlertRule.AlertRuleSubscriberType == AHAHelpContent.AlertRuleSubscriberType.P)
            {
                AHAHelpContent.Patient          objPatient = (objAlertRule.PatientID.HasValue) ? AHAHelpContent.Patient.FindByUserHealthRecordID(objAlertRule.PatientID.Value) : null ;
                AHABusinessLogic.PatientInfo    objPatientInfo = (objPatient != null) ? AHABusinessLogic.PatientInfo.GetPopulatedPatientInfo( objPatient ) : null ;

                if( objPatientInfo != null )
                    rez = Utility.UIHelper.GetHtmlAtagForPatientInfo( objPatientInfo ) ;
            }
            return rez ;
        }

        private string getAlertMeIfText( TrackerDataType tdt, string alertRuleData )
        {
            string  rez = "-" ;

            switch( tdt )
            {
                case AHAHelpContent.TrackerDataType.BloodGlucose:
                    rez = AlertManager.BloodGlucoseAlertRule.GetFormattedAlertRule(alertRuleData) ;
                    break ;

                case AHAHelpContent.TrackerDataType.BloodPressure:
                    rez = AlertManager.BloodPressureAlertRule.GetFormattedAlertRule(alertRuleData);
                    break ;

                case AHAHelpContent.TrackerDataType.Weight:
                    rez = AlertManager.WeightAlertRule.GetFormattedAlertRule(alertRuleData) ;
                    break ;

                default:
                    break;
            }

            return rez ;
        }


        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no widgets because the data is all literals

            ProviderContext provider = SessionInfo.GetProviderContext();
            List<AlertRule> arList = (provider != null) ? AlertRule.FindByProviderID(provider.ProviderID) : null ;

            if( arList == null || arList.Count < 1 )
            {
                return "<p>There are no alert rules.</p>";
            }

            StringBuilder sb = new StringBuilder();
            string markup = this.PrepareMarkup();

            foreach (AlertRule ar in arList)
            {
                string editaction = "<div class=\"edit-icon popuplink\" data-page=\"Pages/Alerts\" data-method=\"EditAlertRulePopupHandler\" data-id=\"" + ar.AlertRuleID.ToString() + "\" data-refresh-ids=\"idControlAlertRulesList\"></div> ";
                string delaction = "<div class=\"trash-icon popuplink\" data-page=\"Pages/Alerts\" data-method=\"DeleteAlertRulePopupHandler\" data-id=\"" + ar.AlertRuleID.ToString() + "\" data-refresh-ids=\"idControlAlertRulesList\"></div>";
                        
                sb.Append("<tr><td>" + getWhoContent( ar ) + "</td><td>" + getAlertMeIfText( ar.Type, ar.AlertRuleData) + "</td><td>" + editaction + delaction + "</td></tr>");
            }

            return markup.Replace( "$AlertRuleList$", sb.ToString() ) ;
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }

    }
}
