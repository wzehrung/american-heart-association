﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class AnnouncementList : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            //get list
            ProviderContext provider = SessionInfo.GetProviderContext();
            List<Announcement> announcementList = Resource.FindAllAnnouncements(true, EnglishLanguage, provider.ProviderID);

            //if empty
            if (announcementList == null || announcementList.Count == 0)
            {
                return "<p>There are no announcements.</p>";
            }

            StringBuilder sb = new StringBuilder();

            //iterate
            foreach (Announcement announcement in announcementList)
            {
                //create a copy of the markup for each entry in the list
                StringBuilder item = new StringBuilder(this.PrepareMarkup());

                item.Replace("$Id$", announcement.AnnouncementID.ToString());
                item.Replace("$Title$", (announcement.Title == null) ? "(No Title)" : announcement.Title);
                item.Replace("$Content$", announcement.Text);
                
                sb.Append(item.ToString());
            }

            return sb.ToString();
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            return JsonFailure("Feature not supported.");
        }
    }
}