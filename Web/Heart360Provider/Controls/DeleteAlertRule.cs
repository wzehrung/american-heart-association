﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class DeleteAlertRule : AHACommon.UXControl
    {
        private int mAlertRuleID;

        private string getAlertMeIfText(TrackerDataType tdt, string alertRuleData)
        {
            string rez = "-";

            switch (tdt)
            {
                case AHAHelpContent.TrackerDataType.BloodGlucose:
                    rez = AlertManager.BloodGlucoseAlertRule.GetFormattedAlertRule(alertRuleData);
                    break;

                case AHAHelpContent.TrackerDataType.BloodPressure:
                    rez = AlertManager.BloodPressureAlertRule.GetFormattedAlertRule(alertRuleData);
                    break;

                case AHAHelpContent.TrackerDataType.Weight:
                    rez = AlertManager.WeightAlertRule.GetFormattedAlertRule(alertRuleData);
                    break;

                default:
                    break;
            }

            return rez;
        }

        override public string GetMarkup(NameValue[] inputs)
        {
            // no widgets, all read-only data

            bool    validInput = false ;

            if( inputs.Length > 0 && inputs[0].name.Equals("id") && !string.IsNullOrEmpty(inputs[0].value) )
            {
                validInput = int.TryParse( inputs[0].value, out mAlertRuleID ) ;
            }

            ProviderContext             provider = SessionInfo.GetProviderContext();
            AHAHelpContent.AlertRule    currentAlertRule = (validInput) ? AHAHelpContent.AlertRule.FindByAlertRuleID( mAlertRuleID ) : null ;

            if( currentAlertRule == null )
            {
                return UXControl.FormatErrorPopupContent( "Delete Alert Rule" ) ;
            }

            // if we are here, we are good to go

            string markup = this.PrepareMarkup() ;

            return markup.Replace( "$Subject$", getAlertMeIfText( currentAlertRule.Type, currentAlertRule.AlertRuleData ) ) ;
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Delete button.
            AHAHelpContent.AlertRule currentAlertRule = AHAHelpContent.AlertRule.FindByAlertRuleID(mAlertRuleID);

            if (currentAlertRule != null)
            {
                AHAHelpContent.AlertRule.DeleteAlertRule(mAlertRuleID);
                mAlertRuleID = -1;
                return JsonRefresh( "DELETE ALERT RULE", "Successfully deleted Alert Rule");
            }

            return JsonFailure("Internal error deleting Alert Rule.");
        }
    }
}