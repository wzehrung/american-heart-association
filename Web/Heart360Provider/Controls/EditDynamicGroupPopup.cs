﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using AHAProvider;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class EditDynamicGroupPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            //FOR TESTING ONLY - forces reload of the html
            //this.Initialize();

            AHAHelpContent.PatientGroup group = null;

            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                try
                {
                    int groupId = Convert.ToInt32(inputs[0].value);
                    group = AHAHelpContent.PatientGroup.FindByGroupID(groupId);
                }
                catch (Exception)
                {
                }
            }

            if (group == null)
            {
                return "Sorry, an internal error occurred. Please close popup and try again.";
            }

            //populate the group data
            StringBuilder markup = new StringBuilder(this.PrepareMarkup());
            markup.Replace("$GroupId$", group.GroupID.ToString());
            markup.Replace("$GroupName$", group.Name);
            markup.Replace("$Description$", group.Description);

            //fill in data for dynamic group
            if (group.RemoveOption == PatientGroup.GroupPatientRemoveOption.A)
            {
                markup.Replace("$addRemoveBothChecked$", "");
                markup.Replace("$addRemoveAddChecked$", "checked='checked'");
            }
            else
            {
                markup.Replace("$addRemoveBothChecked$", "checked='checked'");
                markup.Replace("$addRemoveAddChecked$", "");
            }

            GroupManager.GroupRule grule = GroupManager.GroupRule.ParseGroupRuleXml(group.GroupRuleXml);
            if (grule != null)
            {
                //age
                if(grule.Age == null || grule.Age.IsAbove == null) //any
                {
                    markup.Replace("$ageAbove$", "");
                    markup.Replace("$ageBelow$", "");
                    markup.Replace("$ageAboveChecked$", "");
                    markup.Replace("$ageBelowChecked$", "");
                    markup.Replace("$ageAnyChecked$", "checked='checked'");
                
                }
                else if (grule.Age.IsAbove.Value) //above
                {
                    markup.Replace("$ageAbove$", grule.Age.Value.ToString());
                    markup.Replace("$ageBelow$", "");
                    markup.Replace("$ageAboveChecked$", "checked='checked'");
                    markup.Replace("$ageBelowChecked$", "");
                    markup.Replace("$ageAnyChecked$", "");
                }
                else //below
                {
                    markup.Replace("$ageAbove$", "");
                    markup.Replace("$ageBelow$", grule.Age.Value.ToString());
                    markup.Replace("$ageAboveChecked$", "");
                    markup.Replace("$ageBelowChecked$", "checked='checked'");
                    markup.Replace("$ageAnyChecked$", "");
                }

                if (grule.Gender == null || grule.Gender.IsMale == null) //both
                {
                    markup.Replace("$genderMaleChecked$", "");
                    markup.Replace("$genderFemaleChecked$", "");
                    markup.Replace("$genderBothChecked$", "checked='checked'");
                }
                else if (grule.Gender.IsMale.Value) //male
                {
                    markup.Replace("$genderMaleChecked$", "checked='checked'");
                    markup.Replace("$genderFemaleChecked$", "");
                    markup.Replace("$genderBothChecked$", "");
                }
                else //female
                {
                    markup.Replace("$genderMaleChecked$", "");
                    markup.Replace("$genderFemaleChecked$", "checked='checked'");
                    markup.Replace("$genderBothChecked$", "");
                }

                //handle repeating rules
                List<GroupManager.Rule> ruleList = grule.RuleList;
                if (ruleList != null)
                {
                    StringBuilder ruleEntries = new StringBuilder();
                    for(int i = 0; i < ruleList.Count; i++) 
                    {
                        ruleEntries.Append(createGroupRuleEntry(ruleList[i], i));
                    }
                    markup.Replace("$GroupRuleEntries$", ruleEntries.ToString());

                    //count of repeats
                    markup.Replace("$repeaterNumber$", (ruleList.Count > 1) ? (ruleList.Count - 1).ToString() : "0");
                }
                else
                {
                    return "Sorry, an internal error occurred. Please close popup and try again.";
                }
            }

            return markup.ToString();
        }

        //WARNING: this is long & painful - this method manually crafts the HTML for each repeating rule entry
        //including all the wacky show/hide rules (see https://confluence.mytectura.com/display/HEART/Groups+Page).
        private string createGroupRuleEntry(GroupManager.Rule rule, int index)
        {
            StringBuilder sb = new StringBuilder();

            //string-ify the period setting
            string period = null;
            if (rule.PeriodType == GroupManager.PeriodType.D)
            {
                period = rule.PeriodCount + "-d";
            }
            else if (rule.PeriodType == GroupManager.PeriodType.W)
            {
                period = rule.PeriodCount + "-w";
            }
            else if (rule.PeriodType == GroupManager.PeriodType.M)
            {
                period = rule.PeriodCount + "-m";
            }

            bool activeInactiveConnection = (
                rule.RuleType == GroupManager.RuleType.ActivePatient ||
                rule.RuleType == GroupManager.RuleType.InactivePatient ||
                rule.RuleType == GroupManager.RuleType.PatientConnection);

            //set the appropriate repeater-area-# class
            sb.Append("<div class='repeat-area repeater-area-");
            sb.Append(index);
            sb.Append("'>");

            sb.Append("<div class='group-rule-entry'>");
            sb.Append("<div class='remove-button remove-fields force-hide'></div>");
            sb.Append("<div class='twelvecol first'>");
            sb.Append("<div class='inline-text'>");
            sb.Append("<label class='text-left'>If </label>");
            sb.Append("</div>");
            sb.Append("<div class='inline-input'>");

            //READING TYPE
            if (index == 0)
            {
                sb.Append("<select name='readingType' id='readingType'");
                if(rule.RuleType == GroupManager.RuleType.BMI)
                {
                    sb.Append(" disabled='disabled' class='force-hide'>");                
                }
                else if (activeInactiveConnection)
                {
                    sb.Append(" class='force-hide'>");
                }
                else
                {
                    sb.Append(">");
                }
            }
            else
            {
                sb.Append("<select name='readingType-");
                sb.Append(index);
                sb.Append("' id='readingType-");
                sb.Append(index);
                if (rule.RuleType == GroupManager.RuleType.BMI)
                {
                    sb.Append("' disabled='disabled' class='force-hide'>");
                }
                else if (activeInactiveConnection)
                {
                    sb.Append(" class='force-hide'>");
                }
                else
                {
                    sb.Append("'>");
                }
            }

            sb.Append("<option value=''>Select reading type</option>");
            sb.Append(rule.LastNReadings == null ?
                "<option value='any' selected='selected'>Any reading</option>" : "<option value='any'>Any reading</option>");
            sb.Append(rule.LastNReadings == 1 ?
                "<option value='latest' selected='selected'>Latest reading</option>" : "<option value='latest'>Latest reading</option>");
            sb.Append(rule.LastNReadings == 2 ? //assumes IsAverage is set to true
                "<option value='average' selected='selected'>Average of last two</option>" : "<option value='average'>Average of last two</option>");
            sb.Append("</select>");

            if (index == 0)
            {
                sb.Append("<select name='readingType' id='readingTypeBmi'");
                if(rule.RuleType != GroupManager.RuleType.BMI)
                {
                    sb.Append(" disabled='disabled' class='force-hide'>");                
                }
                else
                {
                    sb.Append(">");
                }
            }
            else
            {
                sb.Append("<select name='readingType-");
                sb.Append(index);
                sb.Append("' id='readingTypeBmi-");
                sb.Append(index);
                if (rule.RuleType != GroupManager.RuleType.BMI)
                {
                    sb.Append("' disabled='disabled' class='force-hide'>");
                }
                else
                {
                    sb.Append("'>");
                }
            }

            sb.Append("<option value='latest' selected='selected'>Latest reading</option>");
            sb.Append("</select>");
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("<div class='twelvecol first'>");
            sb.Append("<div class='inline-text'>");
            sb.Append("<label class='text-left'>of Type </label>");
            sb.Append("</div>");
            sb.Append("<div class='inline-text'>");

            //DATA TYPE
            if (index == 0)
            {
                sb.Append("<select name='dataType' id='dataType' onchange='updateFields(this);'>");
            }
            else
            {
                sb.Append("<select name='dataType-");
                sb.Append(index);
                sb.Append("' id='dataType-");
                sb.Append(index);
                sb.Append(" onchange='updateFields(this);'>");
            }

            sb.Append("<option value=''>Select data type</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.Systolic ?
                "<option selected='selected' value='bps'>BP Systolic</option>" : "<option value='bps'>BP Systolic</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.Diastolic ?
                "<option selected='selected' value='bps'>BP Systolic</option>" : "<option value='bps'>BP Systolic</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.TotalCholesterol ?
                "<option selected='selected' value='ctotal'>Cholesterol Total</option>" : "<option value='ctotal'>Cholesterol Total</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.HDL ?
                "<option selected='selected' value='chdl'>Cholesterol HDL</option>" : "<option value='chdl'>Cholesterol HDL</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.LDL ?
                "<option selected='selected' value='cldl'>Cholesterol LDL</option>" : "<option value='cldl'>Cholesterol LDL</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.Triglyceride ?
                "<option selected='selected' value='tri'>Triglyceride</option>" : "<option value='tri'>Triglyceride</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.BloodGlucose ?
                "<option selected='selected' value='bg'>Blood Glucose</option>" : "<option value='bg'>Blood Glucose</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.Weight ?
                "<option selected='selected' value='w'>Weight</option>" : "<option value='w'>Weight</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.BMI ?
                "<option selected='selected' value='bmi'>BMI</option>" : "<option value='bmi'>BMI</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.ActivePatient ?
                "<option selected='selected' value='active'>Active</option>" : "<option value='active'>Active</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.InactivePatient ?
                "<option selected='selected' value='inactive'>Inactive</option>" : "<option value='inactive'>Inactive</option>");
            sb.Append(rule.RuleType == GroupManager.RuleType.PatientConnection ?
                "<option selected='selected' value='connected'>Connected</option>" : "<option value='connected'>Connected</option>");
            sb.Append("</select>");
            sb.Append("</div>");
            sb.Append("</div>");


            //hide when data type is active/inactive/connection
            if (activeInactiveConnection)
            {
                sb.Append("<div class='twelvecol first force-hide'>");
            }
            else
            {
                sb.Append("<div class='twelvecol first'>");
            }

            //ABOVE
            if (index == 0)
            {
                sb.Append("<div class='inline-input'><input type='radio' default='default' name='readingIs' value='above' id='readingIs-above' ");
                if (rule.MinValue != null && rule.MaxValue == null)
                {
                    sb.Append("checked='checked' /></div>");
                }
                else
                {
                    sb.Append("/></div>");
                }
            }
            else
            {
                sb.Append("<div class='inline-input'><input type='radio' default='default' name='readingIs-");
                sb.Append(index);
                sb.Append("' value='above' id='readingIs-above-");
                sb.Append(index);
                if (rule.MinValue != null && rule.MaxValue == null)
                {
                    sb.Append("' checked='checked' /></div>");
                }
                else
                {
                    sb.Append("' /></div>");
                }
            }

            sb.Append("<div class='inline-text'> <label for='readingIs-above' class='text-left'>Above</label></div>");

            if (index == 0)
            {
                sb.Append("<div class='inline-input'><input name='above' id='above' ");
                if (rule.MinValue != null && rule.MaxValue == null)
                {
                    sb.Append("value='");
                    sb.Append(rule.MinValue);
                    sb.Append("' type='text' maxlength='5' /></div>");
                }
                else
                {
                    sb.Append("value='' type='text' maxlength='5' /></div>");
                }
            }
            else
            {
                sb.Append("<div class='inline-input'><input name='above-");
                sb.Append(index);
                sb.Append("' id='above-");
                sb.Append(index);
                if (rule.MinValue != null && rule.MaxValue == null)
                {
                    sb.Append("' value='");
                    sb.Append(rule.MinValue);
                }
                else
                {
                    sb.Append("' value='");
                }
                sb.Append("' type='text' maxlength='5' /></div>");
            }

            sb.Append("</div>");

            //hide when data type is active/inactive/connection
            if (activeInactiveConnection)
            {
                sb.Append("<div class='twelvecol first inline force-hide'>");
            }
            else
            {
                sb.Append("<div class='twelvecol first inline'>");
            }

            //BELOW
            if (index == 0)
            {
                sb.Append("<div class='inline-input'><input type='radio' name='readingIs' id='readingIs-below' value='below' ");
                if (rule.MinValue == null && rule.MaxValue != null)
                {
                    sb.Append("checked='checked' /></div>");
                }
                else
                {
                    sb.Append("/></div>");
                }
            }
            else
            {
                sb.Append("<div class='inline-input'><input type='radio' name='readingIs-");
                sb.Append(index);
                sb.Append("' id='readingIs-below-");
                sb.Append(index);
                sb.Append("' value='below' ");
                if (rule.MinValue == null && rule.MaxValue != null)
                {
                    sb.Append("checked='checked' /></div>");
                }
                else
                {
                    sb.Append("/></div>");
                }
            }

            sb.Append("<div class='inline-text'><label for='readingIs-below'>Below</label></div>");

            if (index == 0)
            {
                sb.Append("<div class='inline-input'><input name='below' id='below' value='");
                if (rule.MinValue == null && rule.MaxValue != null)
                {
                    sb.Append(rule.MaxValue);
                }
                sb.Append("' type='text' maxlength='5' /></div>");
            }
            else
            {
                sb.Append("<div class='inline-input'><input name='below-");
                sb.Append(index);
                sb.Append("' id='below-");
                sb.Append(index);
                sb.Append("' value='");
                if (rule.MinValue == null && rule.MaxValue != null)
                {
                    sb.Append(rule.MaxValue);
                }
                sb.Append("' type='text' maxlength='5' /></div>");
            }

            sb.Append("</div>");

            //hide when data type is active/inactive/connection
            if (activeInactiveConnection)
            {
                sb.Append("<div class='twelvecol first inline force-hide'>");
            }
            else
            {
                sb.Append("<div class='twelvecol first inline'>");
            }

            //BETWEEN
            if (index == 0)
            {
                sb.Append("<div class='inline-input'><input type='radio' name='readingIs' id='readingIs-between' value='between'");
                if (rule.MaxValue != null && rule.MinValue != null)
                {
                    sb.Append(" checked='checked' /></div>");
                }
                else
                {
                    sb.Append(" /></div>");
                }
            }
            else
            {
                sb.Append("<div class='inline-input'><input type='radio' name='readingIs-");
                sb.Append(index);
                sb.Append("' id='readingIs-between-");
                sb.Append(index);
                if (rule.MaxValue != null && rule.MinValue != null)
                {
                    sb.Append("' checked='checked' value='between' /></div>");
                }
                else
                {
                    sb.Append("' value='between' /></div>");
                }
            }

            sb.Append("<div class='inline-text'><label for='readingIs-between'>Between</label></div>");

            if (index == 0)
            {
                sb.Append("<div class='inline-input'><input name='betweenStart' id='betweenStart' value='");
                if (rule.MaxValue != null && rule.MinValue != null)
                {
                    sb.Append(rule.MinValue);
                }
                sb.Append("' type='text' maxlength='5' /> </div>");
            }
            else
            {
                sb.Append("<div class='inline-input'><input name='betweenStart-");
                sb.Append(index);
                sb.Append("' id='betweenStart-");
                sb.Append(index);
                sb.Append("' value='");
                if (rule.MaxValue != null && rule.MinValue != null)
                {
                    sb.Append(rule.MinValue);
                }
                sb.Append("' type='text' maxlength='5' /> </div>");
            }

            sb.Append("<div class='inline-text'><label>&amp;</label></div>");

            if (index == 0)
            {
                sb.Append("<div class='inline-input'><input name='betweenEnd' value='");
                if (rule.MaxValue != null && rule.MinValue != null)
                {
                    sb.Append(rule.MaxValue);
                }
                sb.Append("' type='text' maxlength='5' /></div>");
            }
            else
            {
                sb.Append("<div class='inline-input'><input name='betweenEnd-");
                sb.Append(index);
                sb.Append("' id='betweenEnd-");
                sb.Append(index);
                sb.Append("' value='");
                if (rule.MaxValue != null && rule.MinValue != null)
                {
                    sb.Append(rule.MinValue);
                }
                sb.Append("' type='text' maxlength='5' /> </div>");
            }

            sb.Append("</div>");
            sb.Append("<div class='twelvecol first inline'>");
            sb.Append("<div class='inline-text'>");
            sb.Append("<label class='text-left'>for </label>");
            sb.Append("</div>");
            sb.Append("<div class='inline-text'>");

            //PERIOD
            if (index == 0)
            {
                sb.Append("<select name='period' id='period'");
                //if Connection, hide/disable this and show periodConnected instead
                if (rule.RuleType == GroupManager.RuleType.PatientConnection)
                {
                    sb.Append(" disabled='disabled' class='force-hide'>");
                }
                else
                {
                    sb.Append(">");
                }
            }
            else
            {
                sb.Append("<select name='period-");
                sb.Append(index);
                sb.Append("' id='period-");
                sb.Append(index);
                //if Connection, hide/disable this and show periodConnected instead
                if (rule.RuleType == GroupManager.RuleType.PatientConnection)
                {
                    sb.Append("' disabled='disabled' class='force-hide'>");
                }
                else
                {
                    sb.Append("'>");
                }
            }
            sb.Append("<option value=''>Select period</option>");
            sb.Append(period.Equals("1-d") ? "<option selected='selected' value='1-d'>1 day</option>" : "<option value='1-d'>1 day</option>");
            sb.Append(period.Equals("2-d") ? "<option selected='selected' value='2-d'>2 days</option>" : "<option value='2-d'>2 days</option>");
            sb.Append(period.Equals("5-d") ? "<option selected='selected' value='5-d'>5 days</option>" : "<option value='5-d'>5 days</option>");
            sb.Append(period.Equals("1-w") ? "<option selected='selected' value='1-w'>1 week</option>" : "<option value='1-w'>1 week</option>");
            sb.Append(period.Equals("2-w") ? "<option selected='selected' value='2-w'>2 weeks</option>" : "<option value='2-w'>2 weeks</option>");
            sb.Append(period.Equals("3-w") ? "<option selected='selected' value='3-w'>3 weeks</option>" : "<option value='3-w'>3 weeks</option>");
            sb.Append(period.Equals("1-m") ? "<option selected='selected' value='1-m'>1 month</option>" : "<option value='1-m'>1 month</option>");
            sb.Append(period.Equals("2-m") ? "<option selected='selected' value='2-m'>2 months</option>" : "<option value='2-m'>2 months</option>");
            sb.Append(period.Equals("6-m") ? "<option selected='selected' value='6-m'>6 months</option>" : "<option value='6-m'>6 months</option>");
            sb.Append("</select>");

            if (index == 0)
            {
                sb.Append("<select name='period' id='periodConnected'");
                //if Connection, show this and hide/disable periodConnected
                if (rule.RuleType == GroupManager.RuleType.PatientConnection)
                {
                    sb.Append("'>");                    
                }
                else
                {
                    sb.Append("' disabled='disabled' class='force-hide'>");
                }
            }
            else
            {
                sb.Append("<select name='period-");
                sb.Append(index);
                sb.Append("' id='periodConnected-");
                sb.Append(index);
                //if Connection, show this and hide/disable periodConnected
                if (rule.RuleType == GroupManager.RuleType.PatientConnection)
                {
                    sb.Append("'>");                    
                }
                else
                {
                    sb.Append("' disabled='disabled' class='force-hide'>");
                }
            }
            sb.Append("<option value=''>Select period</option>");
            sb.Append(period.Equals("2-w") ? "<option selected='selected' value='2-w'>2 weeks</option>" : "<option value='2-w'>2 weeks</option>");
            sb.Append(period.Equals("4-w") ? "<option selected='selected' value='4-w'>4 weeks</option>" : "<option value='4-w'>4 weeks</option>");
            sb.Append(period.Equals("6-w") ? "<option selected='selected' value='6-w'>6 weeks</option>" : "<option value='6-w'>6 weeks</option>");
            sb.Append(period.Equals("8-w") ? "<option selected='selected' value='8-w'>8 weeks</option>" : "<option value='8-w'>8 weeks</option>");
            sb.Append(period.Equals("10-w") ? "<option selected='selected' value='10-w'>10 weeks</option>" : "<option value='10-w'>10 weeks</option>");
            sb.Append(period.Equals("12-w") ? "<option selected='selected' value='12-w'>12 weeks</option>" : "<option value='12-w'>12 weeks</option>");
            sb.Append("</select>");

            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div> <!-- group-rule-entry -->");
            sb.Append("</div> <!-- repeat-area -->");

            return sb.ToString();
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                ProviderContext provider = SessionInfo.GetProviderContext();

                //pull the input values off the form
                //can't use a Dictionary here, because the checkbox names repeat in the inputs array
                int groupId = -1;
                string groupName = null;
                string groupDescription = null;
                List<int> patientsInGroup = new List<int>();
                string addRemove = null;
                string age = null;
                int ageAbove = -1;
                int ageBelow = -1;
                string gender = null;
                int repeaterNumberMax = 0;

                //repeating items defining rules for the group
                Dictionary<string, string> ruleEntries = new Dictionary<string, string>();

                foreach (NameValue nv in inputs)
                {
                    switch (nv.name)
                    {
                        case "txtGroupId":
                            int iGroupId;
                            if (Int32.TryParse(nv.value, out iGroupId))
                            {
                                groupId = iGroupId;
                            }
                            break;
                        case "txtGroupName":
                            groupName = nv.value;
                            break;
                        case "txtDescription":
                            groupDescription = nv.value;
                            break;
                        case "addRemove": //radio, both, add
                            addRemove = nv.value;
                            break;
                        case "age": //radio: above, below, any
                            age = nv.value;
                            break;
                        case "ageAbove": // text
                            int iAgeAbove;
                            if (Int32.TryParse(nv.value, out iAgeAbove))
                            {
                                ageAbove = iAgeAbove;
                            }
                            break;
                        case "ageBelow": //text
                            int iAgeBelow;
                            if (Int32.TryParse(nv.value, out iAgeBelow))
                            {
                                ageBelow = iAgeBelow;
                            }
                            break;
                        case "gender": //radio: male, female, both
                            gender = nv.value;
                            break;
                        case "repeater-number": //hidden, max number for rule entry repeaters
                            int iRepeaterNumber;
                            if (Int32.TryParse(nv.value, out iRepeaterNumber))
                            {
                                repeaterNumberMax = iRepeaterNumber;
                            }
                            break;
                        //collect the repeating rule entry items
                        default:
                            ruleEntries.Add(nv.name, nv.value);
                            break;
                    }
                }

                //validate common fields
                if (string.IsNullOrEmpty(groupName))
                {
                    return JsonFailure("Group Name is required.");
                }

                //addRemove: both, add
                if (string.IsNullOrEmpty(addRemove))
                {
                    return JsonFailure("An Add/Remove selection is required.");
                }
                PatientGroup.GroupPatientRemoveOption removeOption = PatientGroup.GroupPatientRemoveOption.D;
                if (addRemove.Equals("add"))
                {
                    removeOption = PatientGroup.GroupPatientRemoveOption.A;
                }

                //age: above, below, any
                if (string.IsNullOrEmpty(age))
                {
                    return JsonFailure("An Age selection is required.");
                }
                GroupManager.Age groupAge = new GroupManager.Age();
                if (age.Equals("above"))
                {
                    if (ageAbove == -1)
                    {
                        return JsonFailure("A value for Age Above is required.");
                    }
                    groupAge.Value = ageAbove;
                    groupAge.IsAbove = true;
                }
                else if (age.Equals("below"))
                {
                    if (ageBelow == -1)
                    {
                        return JsonFailure("A value for Age Below is required.");
                    }
                    groupAge.Value = ageBelow;
                    groupAge.IsAbove = false;
                }

                //gender: male, female, both
                if (string.IsNullOrEmpty(gender))
                {
                    return JsonFailure("A Gender selection is required.");
                }
                GroupManager.Gender groupGender = new GroupManager.Gender();
                if (gender.Equals("male"))
                {
                    groupGender.IsMale = true;
                }
                else if (gender.Equals("female"))
                {
                    groupGender.IsMale = false;
                }
                else if (gender.Equals("both"))
                {
                    groupGender.IsMale = null;
                }

                //parse dynamic group rule entries
                string error = null;
                List<GroupManager.Rule> ruleList = GroupPopupUtils.ParseRuleEntries(ruleEntries, repeaterNumberMax, out error);
                if (error != null)
                {
                    return JsonFailure(error);
                }

                //create group rule
                GroupManager.GroupRule grule = new GroupManager.GroupRule(groupAge, groupGender, ruleList);

                //update group
                AHAHelpContent.PatientGroup.UpdateGroup(groupId, groupName, groupDescription, grule.GetGroupRuleXml(), removeOption);

                return JsonRefresh("UPDATE GROUP", "Dynamic group updated.");
            }
            catch (Exception) { }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }
    }
}