﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class ChangePasswordPopup : AHACommon.UXControl
    {

        override public string GetMarkup(NameValue[] inputs)
        {
            // There are no current values to assign :-)

            return this.PrepareMarkup();
        }



        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Save button. Clicking Cancel does an INIT call

            Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

            // Need to check that there are valid entries for each required field
            // Need to validate inputs
            bool goodToGo = true;
            StringBuilder sb = new StringBuilder();
            string errMsg = string.Empty;


            // Loop over meta-data and do processing.
            foreach (ControlWidget cw in this.Metadata.Widgets)
            {  
                if (!cw.Validate(hashInputs[cw.Id], ref errMsg))
                {

                    sb.Append(((goodToGo) ? errMsg : ", " + errMsg));
                    goodToGo = false;
                }
            }


            if (!goodToGo)
            {
                return JsonFailure(sb.ToString());
            }

            // Now make sure that the new password follows the rules
            errMsg = string.Empty;
            if (!AHAHelpContent.ProviderDetails.ValidatePasswordFormat(hashInputs[this.MetadataHash["NewPassword"].Id], ref errMsg))
            {
                return JsonFailure(errMsg);
            }
            if (!hashInputs[this.MetadataHash["NewPassword"].Id].Equals(hashInputs[this.MetadataHash["ConfirmNewPassword"].Id]))
            {
                return JsonFailure("New Password and Confirm New Password need to be the same.");
            }

            AHAProvider.ProviderContext provider = AHAProvider.SessionInfo.GetProviderContext();
            if (provider == null)
                return JsonFailure("Internal error.");

            // Change the password
            AHAHelpContent.ProviderDetails.UpdatePassword(provider.ProviderID, Heart360Provider.Utility.UIHelper.GetHashString(hashInputs[this.MetadataHash["NewPassword"].Id]));


            return JsonSuccess( "CHANGE PASSWORD", "Successfully changed password.");
        }
    }
}