﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;

using AHACommon;
using AHAHelpContent;
using AHAProvider;

namespace Heart360Provider.Controls
{
    public class DismissAlertPopup : AHACommon.UXControl
    {
        private const int ID_NONE = -1;
        public const int ID_ALL = -2;
        private const int NO_PATIENT = -1;

            // The dismiss alert popup is used in both Alerts page and Single Participant.
            // When it performs a Dismiss All, it needs to know if it is for a single participant or not.
        private int mPatientID = NO_PATIENT;           // default value
        private Guid mPatientHealthRecordGuid;          // Arg, the DAL function 
        private int mAlertID;

        public int PatientID
        {
            set
            {
                mPatientID = value;
            }
        }
        public Guid PatientHealthRecordGuid
        {
            set { mPatientHealthRecordGuid = value; }
        }

        private string getAlertMeIfText(AHAHelpContent.Alert objAlert)
        {
            string rez = "-";

            if (objAlert.Type == AHAHelpContent.TrackerDataType.BloodGlucose)
            {
                rez = AlertManager.BloodGlucoseAlertRule.GetFormattedAlertRule(objAlert.AlertRuleXml);
            }
            else if (objAlert.Type == AHAHelpContent.TrackerDataType.BloodPressure)
            {
                rez = AlertManager.BloodPressureAlertRule.GetFormattedAlertRule(objAlert.AlertRuleXml);
            }
            else if (objAlert.Type == AHAHelpContent.TrackerDataType.Weight)
            {
                rez = AlertManager.WeightAlertRule.GetFormattedAlertRule(objAlert.AlertRuleXml);
            }
            return rez;
        }

        private int GetNewAlertCountForPatient(int providerID)
        {
            int result = 0;

            List<AHAHelpContent.Alert> alist = AHAHelpContent.Alert.FindByPatientID(mPatientID, providerID);

            if (alist != null && alist.Count > 0)
            {
                foreach (AHAHelpContent.Alert al in alist)
                {
                    if (!al.IsDismissed)
                        result += 1;
                }
            }

            return result;
        }

        override public string GetMarkup(NameValue[] inputs)
        {
            // no widgets, all read-only data
            mAlertID = ID_NONE;

            bool        validInput = false;
            string      title = "DISMISS ALERT";
            string      content = string.Empty ;

            if (inputs.Length > 0 && inputs[0].name.Equals("id") && !string.IsNullOrEmpty(inputs[0].value))
            {
                validInput = int.TryParse(inputs[0].value, out mAlertID);
            }

            if (validInput)
            {
                ProviderContext provider = SessionInfo.GetProviderContext();
                if (mAlertID == ID_ALL)
                {
                    int alertCount = (mPatientID == NO_PATIENT) ? AHAHelpContent.Alert.GetNewAlertCountForProvider(provider.ProviderID) : GetNewAlertCountForPatient( provider.ProviderID ) ;
                    if (alertCount > 0)
                    {
                        title = "DISMISS ALL ALERTS";
                        content = string.Format("Dismiss all {0} alerts?", alertCount);
                    }
                    else
                    {
                        validInput = false;
                    }
                }
                else
                {
                    AHAHelpContent.Alert        alertToDismiss = AHAHelpContent.Alert.FindByAlertID(mAlertID);
                    if (alertToDismiss != null)
                    {
                        content = getAlertMeIfText(alertToDismiss);
                    }
                    else
                    {
                        validInput = false;
                    }
                }
            }

            if (!validInput)
            {
                return UXControl.FormatErrorPopupContent("Dismiss Alert");
            }

            // if we are here, we are good to go

            string markup = this.PrepareMarkup().Replace( "$Title$", title);
            return markup.Replace("$Content$", content);
        }

        override public string ProcessFormValues(NameValue[] inputs)
        {
            // This is always the Dismiss button.
            if (mAlertID == ID_ALL)
            {
                ProviderContext provider = SessionInfo.GetProviderContext();
                if (mPatientID == NO_PATIENT)
                    AHAHelpContent.Alert.DismissAllAlertsForProvider(provider.ProviderID);
                else
                    AHAHelpContent.Alert.DismissAllPatientAlerts(mPatientHealthRecordGuid, provider.ProviderID);

                return JsonRefresh( "DISMISS ALL ALERTS", "Successfully dismissed all alerts.");
            }
            
            AHAHelpContent.Alert.DismissAlert( mAlertID, string.Empty ) ;
            return JsonRefresh( "DISMISS ALERT", "Successfully dismissed the alert.");
        }


    }
}