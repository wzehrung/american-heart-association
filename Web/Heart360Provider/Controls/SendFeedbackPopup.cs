﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class SendFeedbackPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            StringBuilder markup = new StringBuilder(this.PrepareMarkup());
            markup.Replace("$FeedbackTypeOptions$", GetFeedbackTypeOptions());
            return markup.ToString();
        }

        protected StringBuilder addError(StringBuilder errorBuilder, string errorText)
        {
            if (errorBuilder.Length > 0)
            {
                errorBuilder.Append(", ");
            }
            errorBuilder.Append(errorText);
            return errorBuilder;
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            int feedbackType = -1;
            string comments = string.Empty, email = string.Empty;
            bool responseDesired = false, errors = false;

            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);

                StringBuilder errorText = new StringBuilder();

                //parse inputs and validate
                foreach (NameValue nv in inputs)
                {
                    switch (nv.name)
                    {
                        case "txtType":
                                int iType = -1;
                                if (Int32.TryParse(nv.value, out iType))
                                {
                                    feedbackType = iType;
                                }
                            break;
                        case "txtComments":
                            comments = nv.value;
                            break;
                        case "cbResponse":
                            responseDesired = true;
                            break;
                        case "txtEmail":
                            try
                            {
                                // .Net way of validating an email address without having to write some Regex
                                System.Net.Mail.MailAddress ma = new System.Net.Mail.MailAddress(nv.value);
                                email = nv.value;
                            }
                            catch
                            {
                                errorText = addError(errorText, "Email is not a valid email address");
                            }
                            break;
                    }
                }
                if (feedbackType == -1)
                {
                    errorText = addError(errorText, "Feedback Type is required and needs a value");
                }
                if (string.IsNullOrEmpty(comments))
                {
                    errorText = addError(errorText, "Comments are required and need a value");                
                }
                if (errorText.Length > 0)
                {
                    return JsonFailure(errorText.ToString());
                }

                //send email if response requested
                if (responseDesired)
                {
                    string subject = "Heart360" + ": Feedback-" + GetFeedbackTypeNameForID(feedbackType);
                    string body = comments + "<br /><br />I would like a response to my feedback.<br />E-mail : " + email;

                    string from = AHAAppSettings.SystemEmail;
                    body = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, body, false, Locale.English);
                    List<AHAHelpContent.Feedback> recipients = AHAHelpContent.Feedback.GetFeedbackRecipients();
                    if (recipients != null)
                    {
                        for (int i = 0; i < recipients.Count; i++)
                        {
                            // PJB: fix DMARC - 
                            //GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, from, recipients[i].AdminEmailAddress.ToString(), 
                            //    subject, body, true, null, AHAAppSettings.SenderEmail, email);
                            GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, from, recipients[i].AdminEmailAddress.ToString(),
                                subject, body, true, null, from, email);
                        }
                    }  
                }

                //insert feedback in database
                AHAHelpContent.Feedback.FeedbackInsert(comments, feedbackType);

                return JsonSuccess("SEND US YOUR FEEDBACK", "Your feedback has been sent!");
            }
            catch (Exception)
            {
                return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
            }
        }

        private string GetFeedbackTypeNameForID(int feedbackTypeID)
        {
            List<Feedback> feedbackList = AHAHelpContent.Feedback.GetFeedbackTypes(true);
            foreach (Feedback feedback in feedbackList)
            {
                if (feedback.FeedbackTypeID == feedbackTypeID)
                {
                    return feedback.FeedbackTypeName;
                }
            }
            return null;
        }

        private string GetFeedbackTypeOptions()
        {
            StringBuilder options = new StringBuilder();
            List<Feedback> feedbackList = AHAHelpContent.Feedback.GetFeedbackTypes(true);
            foreach (Feedback feedback in feedbackList)
            {
                options.Append("<option value='");
                options.Append(feedback.FeedbackTypeID);
                options.Append("'>");
                options.Append(feedback.FeedbackTypeName);
                options.Append("</option>");
            }
            return options.ToString();
        }

    }
}