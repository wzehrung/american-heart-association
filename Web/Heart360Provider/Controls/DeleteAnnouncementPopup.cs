﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using AHACommon;
using AHAHelpContent;
using Heart360Provider.Utility;

namespace Heart360Provider.Controls
{
    public class DeleteAnnouncementPopup : AHACommon.UXControl
    {
        override public string GetMarkup(NameValue[] inputs)
        {
            if (inputs != null && inputs.Length != 0 && inputs[0].name == "id")
            {
                try
                {
                    int announcementId = Convert.ToInt32(inputs[0].value);

                    //populate the data
                    StringBuilder markup = new StringBuilder(this.PrepareMarkup());
                    markup.Replace("$Id$", announcementId.ToString());

                    return markup.ToString();
                }
                catch (Exception)
                {
                    //int number format exception
                }
            }

            return "Sorry, an internal error occurred (no ID). Please close popup and try again.";
        }

        public override string ProcessFormValues(NameValue[] inputs)
        {
            try
            {
                Dictionary<string, string> hashInputs = inputs.ToDictionary(k => k.name, v => v.value);
                int announcementId;
                if (!Int32.TryParse(hashInputs[this.MetadataHash["Id"].Id], out announcementId))
                {
                    return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
                }

                //delete the announcement
                Resource.DeleteAnnouncement(announcementId, EnglishLanguage);
                return JsonRefresh("DELETE ANNOUNCEMENT", "Announcement deleted.");
            }
            catch (Exception)
            {
                //the dictionary can throw exceptions, int number format exceptions, or the DeleteGroup process
            }

            //fail
            return JsonFailure("Sorry, an internal error occurred. Please close popup and try again.");
        }


    }
}