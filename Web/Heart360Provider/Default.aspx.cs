﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AHAProvider;

namespace Heart360Provider
{
    public partial class _Default : System.Web.UI.Page
    {
        public string strCampaignTitle = string.Empty;
        public string strCampaignDescription = string.Empty;
        public string strCampaignLogo = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if an authenticated user tries to access the root url/default.aspx, then we redirect them to their home page
            //since the root url is not accessible to a loggedin user

            if (AHAProvider.SessionInfo.IsProviderLoggedIn())
            {
                Response.Redirect( "~" + AppSettings.Url.ProviderHome);   // gets this from AHA.config
                Response.End();
            }

            string sHost = System.Web.HttpContext.Current.Request.Url.Host ;
            string sPort = HttpContext.Current.Request.ServerVariables["server_port"].ToString();
            string sForgot = AppSettings.Url.ForgotPassword;

            if (sPort == "80")
            {
                sPort = string.Empty;
            }
            else
            {
                sPort = ":" + sPort;
            }

            string sPath = "http://" + sHost + sPort;

            btnProviderSignIn.HRef = sPath + AppSettings.Url.ProviderLogin;
            btnVolunteerSignIn.HRef = sPath + AppSettings.Url.VolunteerLogin;
        }


        protected void btnPpeSummaryOk_Click(object sender, EventArgs e)
        {

        }
    }
}
