﻿<%@ Page Title="Heart360 - SignUp" Language="C#" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Heart360Provider.SignUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <!-- page title and description -->
    <div class="twelvecol tablet-twelvecol mobile-twelvecol first">
        <h2>Sign Up for Heart360</h2>
        <p>Enter your organization information below. Be sure to fill in the required fields. You can change your information or add to it later on your profile page.</p>
    </div>
    
    <!-- Role information -->
    <div class="twelvecol tablet-twelvecol mobile-twelvecol first">
        <h4>YOUR ROLE</h4>

        <div class="fourcol tablet-twelvecol mobile-twelvecol first">
            <label class="text-left">Role<span class="required">*</span></label>
            <asp:DropDownList ID="ddlRole" Width="100%" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" />
        </div>
        <div class="eightcol tablet-twelvecol mobile-twelvecol">
            <br />
        </div>
    </div>

    <!-- Need some spacing -->
    <div class="twelvecol tablet-twelvecol mobile-twelvecol first">
        <br /><br />
    </div>

    <!-- Add signup information control -->
    <div class="dynamic-content-container" data-page="SignUp" data-method="SignUpControlHandler" >
        <div class="dynamic-content-html">
            <!-- Dynamic content will be loaded here -->
        </div>

        <div class="dynamic-content-message">
            <!-- ALert Messages will be added herer -->
        </div>
    </div>

</asp:Content>
