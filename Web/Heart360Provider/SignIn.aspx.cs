﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using AHAHelpContent;   // for validating provider user credentials

namespace Heart360Provider
{
    public partial class SignIn : System.Web.UI.Page
    {

        private static Heart360Provider.Controls.ForgotPassword mForgotPasswordControl;
        private static Heart360Provider.Controls.SendFeedbackPopup mSendFeedbackPopup;

        private AHAHelpContent.DbUserType mUserType = DbUserType.DbUserTypeProvider;    // default

        protected void Page_Init(object sender, EventArgs e)
        {
            // Allocate and initialize ForgotPassword control
            try
            {
                mForgotPasswordControl = new Controls.ForgotPassword();
                mForgotPasswordControl.Name = "ForgotPassword";
                if (!mForgotPasswordControl.Initialize())
                {
                    mForgotPasswordControl = null;
                }


            }
            catch (Exception)
            {                
                mForgotPasswordControl = null;
            }
            try
            {
                mSendFeedbackPopup = new Heart360Provider.Controls.SendFeedbackPopup();
                mSendFeedbackPopup.Name = "SendFeedbackPopup";
                if (!mSendFeedbackPopup.Initialize())
                {
                    mSendFeedbackPopup = null;
                }
            }
            catch (Exception ex)
            {
                mSendFeedbackPopup = null;
            }

            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["pvtype"]))
                {
                    int ival = int.Parse(this.Request.QueryString["pvtype"]);
                    if (ival == (int)AHAHelpContent.DbUserType.DbUserTypeVolunteer)
                        this.mUserType = AHAHelpContent.DbUserType.DbUserTypeVolunteer;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["invitationid"]))
                {
                    String inviteCode = this.Request.QueryString["invitationid"];
                    if (!string.IsNullOrEmpty(inviteCode))
                        AHAProvider.SessionInfo.SetPatientInvitationId(inviteCode);
                }

                // Next check to see if a campaign was specified
                AHAProvider.H360Utility.ResetCampaignSession();
                if (!string.IsNullOrEmpty(this.Request.QueryString["cid"]))
                {
                    string campurl = GRCBase.BlowFish.DecryptString(Request["cid"]);
                    AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByURL(campurl, AHAProvider.H360Utility.GetCurrentLanguageID());
                    if (objCampaign != null && objCampaign != null && objCampaign.IsActive)
                    {
                        AHAProvider.SessionInfo.SetCampaignSession(objCampaign.CampaignID);
                        AHAProvider.SessionInfo.SetCampaignSessionUrl(campurl);
                        /**
                        AHAProvider.H360Utility.SetCampaignSession(objCampaign.CampaignID);
                        AHAProvider.H360Utility.SetCampaignSessionUrl(campurl);     // PJB: need this for signout redirect back to campaign
                         **/
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //if an authenticated user tries to access the root url/default.aspx, then we redirect them to their home page
                //since the root url is not accessible to a loggedin user

                if (AHAProvider.SessionInfo.IsProviderLoggedIn())
                {
                    // If an invitation ID was provided, update the invitation to use this provider's ID, if not already set
                    string invitationId = AHAProvider.SessionInfo.GetPatientInvitationId();
                    if (!string.IsNullOrEmpty(invitationId))
                    {
                        Heart360Provider.Utility.Invitations.SetInvitationProvider(invitationId, AHAProvider.SessionInfo.GetProviderContext().ProviderID);
                        AHAProvider.SessionInfo.SetPatientInvitationId(null);
                    }

                    Response.Redirect("~" + AppSettings.Url.ProviderHome);
                    Response.End();
                }

                // Logic for Provider vs. Volunteer
                aSignUp.HRef = "~" + ((this.mUserType == DbUserType.DbUserTypeProvider) ? AppSettings.Url.ProviderRegister : AppSettings.Url.VolunteerRegister);
            }
        }

        [WebMethod]
        public static string ForgotPasswordPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mForgotPasswordControl, INIT, inputs, false);
        }

        [WebMethod]
        public static string SendFeedbackPopup(bool INIT, AHACommon.NameValue[] inputs)
        {
            return AHACommon.UXControl.ControlHandler(mSendFeedbackPopup, INIT, inputs, false);
        }

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            // Validate content
            Page.Validate("vgSignIn");
            if (Page.IsValid)
            {

                // PJB: may need a try/catch here....

                // Try logging in
                ProviderDetails obj = ProviderDetails.FindByUserNameAndPassword(tbUsername.Text.Trim(), Heart360Provider.Utility.UIHelper.GetHashString(tbPassword.Text.Trim()) );

                if (obj != null)
                {

                    //get from DB
                    int iProviderID = obj.ProviderID;
                    //set session
                    AHAProvider.SessionInfo.SetProviderSession(obj);

                    AHAHelpContent.ProviderLoginDetails.CreateLoginDetails(iProviderID);

                    // If an invitation ID was provided, update the invitation to use this provider's ID, if not already set
                    string invitationId = AHAProvider.SessionInfo.GetPatientInvitationId();
                    if (!string.IsNullOrEmpty(invitationId))
                    {
                        Heart360Provider.Utility.Invitations.SetInvitationProvider(invitationId, iProviderID);
                        AHAProvider.SessionInfo.SetPatientInvitationId(null);
                    }

                    Response.Redirect("~" + AppSettings.Url.ProviderHome, true);
                }
                else
                {
                    errorText.Text = "<div class='inline-error'>Enter a valid username and password.</div>";
                }
            }
        }

        protected void ValidateUsername(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string trimstr = tbUsername.Text.Trim();

            if (String.IsNullOrEmpty(trimstr))
            {
                v.IsValid = false;
                errorText.Text = "<div class='inline-error'>Please specify a Username.</div>";
            }

        }

        protected void ValidatePassword(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string trimstr = tbPassword.Text.Trim();
            if (String.IsNullOrEmpty(trimstr))
            {
                v.IsValid = false;
                // valid.ErrorMessage = "Please specify a Username.";   // GetGlobalResourceObject("Tracker", "Validation_Text_Specify_BloodGlucose").ToString();
                errorText.Text = "<div class='inline-error'>Please specify a Password.</div>";
            }

        }

    }
}