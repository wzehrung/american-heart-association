﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;
using System.Security.Cryptography;


namespace Heart360Provider.Utility
{
    public class UIHelper
    {
        public static string GetHtmlAtagForPatientInfo(AHABusinessLogic.PatientInfo p)
        {
            return "<a href=\"" + AppSettings.Url.ProviderModule(ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT) + "?pg=" + p.RecordGUID.ToString() + "\">" + p.FullName + "</a>";
        }

        public static string GetHtmlAtagForPatientInfoWithAlertCount(AHABusinessLogic.PatientInfo p)
        {
            // p.RecordGUID is the OfflineHealthRecordGUID
            return "<a href=\"" + AppSettings.Url.ProviderModule(ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT) + "?pg=" + p.RecordGUID.ToString() + "\">" + p.FullName + " (" + p.TotalAlerts.ToString() + ")</a>";
        }

        public static string GetHtmlAtagForPatientInfoWithGroupCount(AHABusinessLogic.PatientInfo p, string groupCount)
        {
            return "<a href=\"" + AppSettings.Url.ProviderModule(ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT) + "?pg=" + p.RecordGUID.ToString() + "\">" + p.FullName + " (" + groupCount + ")</a>";
        }

        public static string GetHtmlAtagForPatientGroup(AHAHelpContent.PatientGroup pg)
        {
            return "<a href=\"" + AppSettings.Url.ProviderModule(ProviderModules.MODULE_ID.MODULE_ID_GROUPS) + "?pg=" + pg.GroupID.ToString() + "\">" + pg.Name + "</a>";
        }

        public static string GetHtmlAtagForAllPatients(string name)
        {
            return "<a href=\"" + AppSettings.Url.ProviderModule(ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS) + "\">" + name + "</a>";
        }

        /// <summary>
        /// Returns hexadecimal string of MD5 hashed string
        /// http://msdn.microsoft.com/en-us/library/system.security.cryptography.md5cryptoserviceprovider.aspx
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetHashString(string input)
        {
            try
            {
                // Create a new instance of the MD5CryptoServiceProvider object.
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();

            }
            catch (Exception e)
            {
                throw new Exception("An Error occurred:-" + e.Message);
            }
        }
    }
}