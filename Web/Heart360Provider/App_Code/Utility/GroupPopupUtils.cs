﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Provider.Utility
{
    /*  
     * Utility functions used by the group popup controls
     */
    public class GroupPopupUtils
    {
        public static string getEntry(string key, Dictionary<string, string> ruleEntries)
        {
            string val;
            if (ruleEntries.TryGetValue(key, out val))
            {
                return val;
            }
            else
            {
                return null;
            }
        }
 
        
        public static List<GroupManager.Rule> ParseRuleEntries(Dictionary<string, string> ruleEntries, int repeaterNumberMax, out string error)
        {
            error = null;
            List<GroupManager.Rule> ruleList = new List<GroupManager.Rule>();

            //parse the initial rule entry - no trailing repeat number
            if (ruleEntries.ContainsKey("dataType"))
            {
                ruleList.Add(parseGroupRule(
                    getEntry("readingType", ruleEntries),
                    getEntry("dataType", ruleEntries),
                    getEntry("readingIs", ruleEntries),
                    getEntry("above", ruleEntries),
                    getEntry("below", ruleEntries),
                    getEntry("betweenStart", ruleEntries),
                    getEntry("betweenEnd", ruleEntries),
                    getEntry("period", ruleEntries),
                    out error));
                if (error != null)
                {
                    return null;
                }
            }

            //parse any/all subsequent rule entries
            int repeatIndex = 1;
            while (repeatIndex <= repeaterNumberMax)
            {
                //may have missing numbers due to on-page add/removes
                if (ruleEntries.ContainsKey("dataType-" + repeatIndex))
                {
                    //for each individual rule, parse it and add to the list
                    ruleList.Add(parseGroupRule(
                        getEntry("readingType-" + repeatIndex, ruleEntries),
                        getEntry("dataType-" + repeatIndex, ruleEntries),
                        getEntry("readingIs-" + repeatIndex, ruleEntries),
                        getEntry("above-" + repeatIndex, ruleEntries),
                        getEntry("below-" + repeatIndex, ruleEntries),
                        getEntry("betweenStart-" + repeatIndex, ruleEntries),
                        getEntry("betweenEnd-" + repeatIndex, ruleEntries),
                        getEntry("period-" + repeatIndex, ruleEntries),
                        out error));
                    if (error != null)
                    {
                        return null;
                    }
                }
                repeatIndex++;
            }

            return ruleList;
        }

        //parse group rule
        private static GroupManager.Rule parseGroupRule(
            string readingType, string dataType, string readingIs,
            string above, string below, string betweenStart, 
            string betweenEnd, string period, out string error)
        {
            Nullable<int> readingCount = null;
            Nullable<double> minValue = null, maxValue = null;
            bool isAverage = false;
            bool activeInactiveConnection = false;
            error = null;

            //data type
            if (string.IsNullOrEmpty(dataType))
            {
                error = "A Data Type selection is required";
                return null;
            }
            GroupManager.RuleType type = GroupManager.RuleType.ActivePatient;
            if (dataType.Equals("bps"))
            {
                type = GroupManager.RuleType.Systolic;
            }
            else if (dataType.Equals("bpd"))
            {
                type = GroupManager.RuleType.Diastolic;
            }
            else if (dataType.Equals("ctotal"))
            {
                type = GroupManager.RuleType.TotalCholesterol;
            }
            else if (dataType.Equals("chdl"))
            {
                type = GroupManager.RuleType.HDL;
            }
            else if (dataType.Equals("cldl"))
            {
                type = GroupManager.RuleType.LDL;
            }
            else if (dataType.Equals("tri"))
            {
                type = GroupManager.RuleType.Triglyceride;
            }
            else if (dataType.Equals("bg"))
            {
                type = GroupManager.RuleType.BloodGlucose;
            }
            else if (dataType.Equals("w"))
            {
                type = GroupManager.RuleType.Weight;
            }
            else if (dataType.Equals("bmi"))
            {
                type = GroupManager.RuleType.BMI;
            }
            else if (dataType.Equals("active"))
            {
                type = GroupManager.RuleType.ActivePatient;
                activeInactiveConnection = true;
            }
            else if (dataType.Equals("inactive"))
            {
                type = GroupManager.RuleType.InactivePatient;
                activeInactiveConnection = true;
            }
            else if (dataType.Equals("connected"))
            {
                type = GroupManager.RuleType.PatientConnection;
                activeInactiveConnection = true;
            }

            //reading type
            if (string.IsNullOrEmpty(readingType))
            {
                error = "A Reading Type selection is required";
                return null;
            }
            if (readingType.Equals("any"))
            {
                //leave it null
            }
            else if (readingType.Equals("latest"))
            {
                //last reading
                readingCount = 1;
            }
            else if (readingType.Equals("average"))
            {
                //average of last 2 readings
                readingCount = 2;
                isAverage = true;
            }


            //reading is:
            if (string.IsNullOrEmpty(readingIs) && !activeInactiveConnection)
            {
                error = "An Above/Below/Between selection is required";
                return null;
            }
            if (readingIs != null)
            {
                if (readingIs.Equals("above"))
                {
                    double dAbove;
                    if (Double.TryParse(above, out dAbove))
                    {
                        minValue = dAbove;
                    }
                    else
                    {
                        error = "A value for Above is required";
                        return null;
                    }
                }
                else if (readingIs.Equals("below"))
                {
                    double dBelow;
                    if (Double.TryParse(below, out dBelow))
                    {
                        maxValue = dBelow;
                    }
                    else
                    {
                        error = "A value for Below is required";
                        return null;
                    }
                }
                else if (readingIs.Equals("between"))
                {
                    double dBetweenStart, dBetweenEnd;
                    if (Double.TryParse(betweenStart, out dBetweenStart))
                    {
                        minValue = dBetweenStart;
                    }
                    else
                    {
                        error = "A starting value for Between is required";
                        return null;
                    }
                    if (Double.TryParse(betweenEnd, out dBetweenEnd))
                    {
                        maxValue = dBetweenEnd;
                    }
                    else
                    {
                        error = "An ending value for Between is required";
                        return null;
                    }
                }
            }

            //period count
            if (string.IsNullOrEmpty(period))
            {
                error = "A Period selection is required";
                return null;
            }
            int periodCount = 0;
            GroupManager.PeriodType periodType = GroupManager.PeriodType.N;

            //split number from type
            string[] values = period.Split('-');
            Int32.TryParse(values[0], out periodCount);
            switch (values[1])
            {
                case "d":
                    periodType = GroupManager.PeriodType.D;
                    break;
                case "w":
                    periodType = GroupManager.PeriodType.W;
                    break;
                case "m":
                    periodType = GroupManager.PeriodType.M;
                    break;
            }

            //create rule object
            GroupManager.Rule rule = new GroupManager.Rule(
                type, isAverage, periodCount, periodType, readingCount, minValue, maxValue);

            return rule;
        }
    }
}