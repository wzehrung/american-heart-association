﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using GRCBase;
//using AHACommon;
using AHAHelpContent;
using System.Resources;

namespace Heart360Provider.Utility
{
    public class EmailUtils
    {
        public static string GetEmailTemplateDirectory(string strLocale)
        {
            if (!string.IsNullOrEmpty(strLocale) && strLocale != AHACommon.Locale.English )
            {
                return System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates.{0}/", strLocale));
            }

            return System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/");
        }

        /** PJB (6.27.2014) - THIS IS NOT USED IN THE PROVIDER PORTAL
        public static void SendNewMessageEmail(bool bIsProvider, string strToName, string strToEmail, string strLocale)
        {
            string filepath = string.Format("{0}\\{1}", GetEmailTemplateDirectory(strLocale), AppSettings.Email.TemplateNewMessagePatient);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_New_Message_Patient", cultureInfo);

            if (bIsProvider)
            {
                filepath = string.Format("{0}\\{1}", GetEmailTemplateDirectory(strLocale),AppSettings.Email.TemplateNewMessageProvider);
                subject = Resources.Common.ResourceManager.GetString("Subject_New_Message_Provider", cultureInfo);
            }

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", GRCBase.UrlUtility.ServerRootUrl);

            strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, bIsProvider, strLocale);

            //Send Email
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AppSettings.Email.System, strToEmail, subject, strContent, true, null, AppSettings.Email.Sender, null);
        }
         **/

        public static void SendWelcomeEmailToProvider( string strToName, string strToEmail, string strLocale, string strProviderCode)
        {
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), AppSettings.Email.TemplateRegisterProvider );
            //string subject = emailSetting.Subject_Register_Provider;
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Register_Provider", cultureInfo);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_CODE##", strProviderCode);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", AppSettings.Url.SiteUrl );    // GRCBase.UrlUtility.ServerRootUrl);

            strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            // PJB: Fix for DMARC
            GRCBase.EmailHelper.SendMail(AppSettings.Email.System, strToName, AppSettings.Email.System, strToEmail, subject, strContent, true, null, null, null);
        }

        public static void SendForgotPasswordEmail(string strToName, string strToEmail, string strLink, string strLocale)
        {
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), AppSettings.Email.TemplateForgotPassword );
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Forgot_Password", cultureInfo);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##LINK##", strLink);

            strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            // PJB: Fix for DMARC
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AppSettings.Email.System, strToEmail, subject, strContent, true, null, null, null);
        }

        public static void SendForgotUsernameEmail(string strToName, string strToEmail, string strUsername, string strLocale)
        {
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), AppSettings.Email.TemplateForgotUsername );
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Forgot_Username", cultureInfo);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##USERNAME##", strUsername);

            strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            // PJB: Fix for DMARC
            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AppSettings.Email.System, strToEmail, subject, strContent, true, null, null, null);
        }

        public static void SendInvitationToPatient(AHAHelpContent.PatientProviderInvitation objInvitation, ProviderDetails objProvider)
        {
            AHAProvider.H360RequestContext objContext = AHAProvider.H360RequestContext.GetContext();
            // PJB: refactored to use variable 'lang' because provider portal was not assigning H360RequestContext
            string lang = (objContext != null) ? objContext.SelectedLanguage.LanguageLocale : AHACommon.Locale.English;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(lang), AppSettings.Email.TemplateInvitePatient);


            foreach (PatientProviderInvitationDetails objDetails in objInvitation.InvitationDetails)
            {
                System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(lang);
                string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Patient", cultureInfo);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", objDetails.Name);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);

                string strContent = IOUtility.ReadHtmlFile(filepath);

                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", objDetails.Name);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_CODE##", objProvider.ProviderCode);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##INVITATION_URL##", GlobalPaths.GetInvitationLink(objInvitation.InvitationCode, false));

                strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, lang);

                //Send Email
                // PJB: Fix for DMARC (see http://stackoverflow.com/questions/22968931/dmarc-anti-spoofing-error-when-sending-email-with-sender-yahoo-domain-other-than)
                // GRCBase.EmailHelper.SendMail(objProvider.FirstName, objDetails.Name, objProvider.Email, objDetails.Email, subject, strContent, true, null, objProvider.Email, null);
                GRCBase.EmailHelper.SendMail(objProvider.Email + " via ", objDetails.Name, AppSettings.Email.System, objDetails.Email, subject, strContent, true, null, AppSettings.Email.System, null);
            }
        }

        public static void SendInvitationToPatient(AHAHelpContent.PatientProviderInvitationDetails objInvitationDetails, ProviderDetails objProvider, string strInvitationCode)
        {
            AHAProvider.H360RequestContext objContext = AHAProvider.H360RequestContext.GetContext();
            // PJB: refactored to use variable 'lang' because provider portal was not assigning H360RequestContext
            string lang = (objContext != null) ? objContext.SelectedLanguage.LanguageLocale : AHACommon.Locale.English;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(lang), AppSettings.Email.TemplateInvitePatient );

            //string subject = System.Text.RegularExpressions.Regex.Replace(emailSetting.Subject_Invite_Patient, "##PATIENT_NAME##", objInvitationDetails.Name);
            //subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(lang);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Patient", cultureInfo);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", objInvitationDetails.Name);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", objInvitationDetails.Name);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_CODE##", objProvider.ProviderCode);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##INVITATION_URL##", GlobalPaths.GetInvitationLink(strInvitationCode, false));

            strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, lang);

            //Send Email
            // PJB: Fix for DMARC (see http://stackoverflow.com/questions/22968931/dmarc-anti-spoofing-error-when-sending-email-with-sender-yahoo-domain-other-than)
            // GRCBase.EmailHelper.SendMail(objProvider.FirstName, objInvitationDetails.Name, objProvider.Email, objInvitationDetails.Email, subject, strContent, true, null, objProvider.Email, null);
            GRCBase.EmailHelper.SendMail(objProvider.Email + " via ", objInvitationDetails.Name, AppSettings.Email.System, objInvitationDetails.Email, subject, strContent, true, null, AppSettings.Email.System, null);
        }

        /** NOT REQUIRED FOR PROVIDER PORTAL
        public static void SendInvitationToProvider(AHAHelpContent.PatientProviderInvitation objInvitation, string strPatientName, string strEmail)
        {
            AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(objContext.SelectedLanguage.LanguageLocale), emailSetting.File_Invite_Provider);

            foreach (PatientProviderInvitationDetails objDetails in objInvitation.InvitationDetails)
            {

                System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(objContext.SelectedLanguage.LanguageLocale);
                string subject = Resources.ConnCenterPAndV.ResourceManager.GetString("Email_Subject_Invite_Provider", cultureInfo);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objDetails.Name);
                subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strPatientName);

                string strContent = IOUtility.ReadHtmlFile(filepath);

                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strPatientName);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objDetails.Name);
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##INVITATION_URL##", AHAAPI.GlobalPaths.GetInvitationLink(objInvitation.InvitationCode, false));


                string strFromMail = string.IsNullOrEmpty(strEmail) ? AHAAppSettings.SystemEmail : strEmail;

                strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, objContext.SelectedLanguage.LanguageLocale);

                GRCBase.EmailHelper.SendMail(strPatientName,
                    objDetails.Name,
                    strFromMail,
                    objDetails.Email,
                    subject,
                    strContent,
                    true, null,
                    strFromMail,
                    null);
            }
        }
        **/

        public static void SendInvitationConfirmationToPatient(ProviderDetails objProvider, string strFName, string strEmail)
        {
            AHAProvider.H360RequestContext objContext = AHAProvider.H360RequestContext.GetContext();
            // PJB: refactored to use variable 'lang' because provider portal was not assigning H360RequestContext
            string lang = (objContext != null) ? objContext.SelectedLanguage.LanguageLocale : AHACommon.Locale.English;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(lang), AppSettings.Email.TemplateInvitationConfirmationPatient );

            //string subject = System.Text.RegularExpressions.Regex.Replace(emailSetting.Subject_Invitation_Confirmation_Patient, "##PROVIDER_NAME##", objProvider.FormattedName);
            //subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strFName);

            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(lang);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Confirmation_Patient", cultureInfo);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strFName);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strFName);

            if (objProvider.Salutation == null)
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", "");
            }
            else
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", objProvider.Salutation);
            }
           
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);

            strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, lang);

            //Send Email
            // PJB: Fix for DMARC (see http://stackoverflow.com/questions/22968931/dmarc-anti-spoofing-error-when-sending-email-with-sender-yahoo-domain-other-than)
            //GRCBase.EmailHelper.SendMail(objProvider.FirstName, strFName, objProvider.Email, strEmail, subject, strContent, true, null, objProvider.Email, null);
            GRCBase.EmailHelper.SendMail(objProvider.Email + " via ", strFName, AppSettings.Email.System, strEmail, subject, strContent, true, null, AppSettings.Email.System, null);
        }

        /** NOT REQUIRED FOR PROVIDER PORTAL
        public static void SendInvitationConfirmationToProvider(ProviderDetails objProvider, string strPatientName, string strPatientEmail, string strLocale)
        {
            //AHAAPI.H360RequestContext objContext = AHAAPI.H360RequestContext.GetContext();
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_Invitation_Confirmation_Provider);

            ////string subject = System.Text.RegularExpressions.Regex.Replace(emailSetting.Subject_Invitation_Confirmation_Provider, "##PROVIDER_NAME##", objProvider.FormattedName);
            ////subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strPatientName);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Invite_Confirmation_Provider", cultureInfo);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PROVIDER_NAME##", objProvider.FormattedName);
            subject = System.Text.RegularExpressions.Regex.Replace(subject, "##PATIENT_NAME##", strPatientName);

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strPatientName);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", objProvider.FormattedName);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            //Send Email

            string fromMail = string.Empty;

            if (string.IsNullOrEmpty(strPatientEmail))
            {
                fromMail = AHAAppSettings.SystemEmail;
            }
            else
            {
                fromMail = strPatientEmail;
            }

            GRCBase.EmailHelper.SendMail(strPatientName, objProvider.FirstName, fromMail, objProvider.Email, subject, strContent, true, null, fromMail, null);
        }
        **/
        
        public static void SendDisconnectPatientEmail(string strToName, string strToEmail, string strProviderName, string strProviderSalutation)
        {
            AHAProvider.H360RequestContext objContext = AHAProvider.H360RequestContext.GetContext();
            // PJB: refactored to use variable 'lang' because provider portal was not assigning H360RequestContext
            string lang = (objContext != null) ? objContext.SelectedLanguage.LanguageLocale : AHACommon.Locale.English;

            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(lang), AppSettings.Email.TemplateDisconnectPatient );
            //ResourceManager rm = new System.Resources.ResourceManager("Resources.Common", System.Reflection.Assembly.Load("App_GlobalResources"));
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(lang);
            string subject = Resources.Common.ResourceManager.GetString("Subject_Disconnect_Patient", cultureInfo);
            //string subject = emailSetting.Subject_Disconnect_Patient;

            string strContent = IOUtility.ReadHtmlFile(filepath);

            if (strProviderSalutation == null)
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", "");
            }
            else
            {
                strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_SALUTATION##", strProviderSalutation);
            }

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", strProviderName);

            strContent = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, false, lang);

            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AppSettings.Email.System, strToEmail, subject, strContent, true, null, AppSettings.Email.System, null);
        }

        /** PJB - NOT REQUIRED FOR PROVIDER PORTAL
        public static void SendDisconnectProviderEmail(string strToName, string strToEmail, string strPatientName, string strLocale)
        {
            PatientProviderEmailSettings emailSetting = AHAAppSettings.PatientProviderEmailSettingsConfig;
            string filepath = string.Format("{0}{1}", GetEmailTemplateDirectory(strLocale), emailSetting.File_Disconnect_Provider);
            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(strLocale);
            string subject = Resources.Common.ResourceManager.GetString("Email_Subject_Disconnect_Provider", cultureInfo);
            //string subject = emailSetting.Subject_Disconnect_Provider;

            string strContent = IOUtility.ReadHtmlFile(filepath);

            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PATIENT_NAME##", strPatientName);
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##PROVIDER_NAME##", strToName);

            strContent = AHACommon.EmailTemplateHelper.GetCompleteHtmlForEmail(false, strContent, true, strLocale);

            GRCBase.EmailHelper.SendMail(string.Empty, strToName, AppSettings.Email.System, strToEmail, subject, strContent, true, null, AppSettings.Email.Sender, null);
        }
        **/

        /** Controls/SendFeedbackPopup.cs does this internally
        public static void SendFeedbackEmail(string strSubject, string strBody, string strResponseEmail)
        {
            string strFromEmail = AppSettings.Email.System;
            string strToEmail = AppSettings.Email.Feedback; // AHAAppSettings.FeedbackToEmail;
            string strReplyTo = null;
            if (!String.IsNullOrEmpty(strResponseEmail))
                strReplyTo = strResponseEmail;

            strBody = EmailTemplateHelper.GetCompleteHtmlForEmail(false, strBody, false, AHACommon.Locale.English);

            GRCBase.EmailHelper.SendMail(string.Empty, string.Empty, strFromEmail, strToEmail, strSubject, strBody, true, null, strFromEmail, strReplyTo);
        }
         **/
    }
}
