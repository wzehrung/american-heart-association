﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Provider.Utility
{
    public class Invitations
    {
        // returns true if invitation updated to specified provider, false if already set or failure
        public static bool SetInvitationProvider(string invitationCode, int providerId)
        {
            try
            {
                //retrieve both records based on invitation code
                AHAHelpContent.PatientProviderInvitation ppi = AHAHelpContent.PatientProviderInvitation.FindByInvitationCode(invitationCode);
                AHAHelpContent.PatientProviderInvitationDetails ppid = (ppi != null) ? ppi.PendingInvitationDetails.Where(PID => PID.InvitationID == ppi.InvitationID).SingleOrDefault() : null;

                if (ppi != null && ppid != null)
                {
                    //if provider ID is already set, don't update (only YOU can prevent Invitation re-use!)
                    if (ppi.ProviderID == null)
                    {
                        ppi.ProviderID = providerId;
                        ppi.Update();

                        ppid.AcceptedProviderID = providerId;
                        ppid.Status = AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Pending;
                        ppid.Update();

                        return true;
                    }
                }
            }
            catch (Exception) { }

            return false;
        }

            // returns true if the invitation is successfully accepted, false if there was any issue
        public static bool AcceptInvitation(AHAProvider.ProviderContext provider, string invitationCode)
        {
            bool result = true;

            // get all the information we need....
            AHAHelpContent.PatientProviderInvitation ppi = AHAHelpContent.PatientProviderInvitation.FindByInvitationCode(invitationCode);
            AHAHelpContent.PatientProviderInvitationDetails ppid = (ppi != null) ? ppi.PendingInvitationDetails.Where(PID => PID.AcceptedProviderID == provider.ProviderID).SingleOrDefault() : null;
            HVManager.PersonalDataManager.PersonalItem patDetails = (ppi != null) ? Heart360Provider.Utility.PatientDetails.GetPatientDetails(ppi.PatientID.Value) : null;
            HVManager.PersonalContactDataManager.ContactItem patContact = (ppi != null) ? Heart360Provider.Utility.PatientDetails.GetPatientContactDetails(ppi.PatientID.Value) : null;


            // This is the real work for getting connected with accepting the invitation
            if (ppid != null && patDetails != null)
            {
                bool bIsConnected = AHAHelpContent.ProviderDetails.IsValidPatientForProvider(ppi.PatientID.Value, provider.ProviderID);

                if (!bIsConnected)
                {
                    ConnectPatientAndProvider(ppi, provider.ProviderID);

                    MarkInvitationAccepted(ppi.InvitationDetails.Where(PID => PID.AcceptedProviderID == provider.ProviderID).Single());

                    if (ppi.SentConfirmation && patContact != null && !string.IsNullOrEmpty(patContact.Email.Value))
                    {
                        EmailUtils.SendInvitationConfirmationToPatient(AHAHelpContent.ProviderDetails.FindByProviderID(provider.ProviderID), patDetails.FullName, patContact.Email.Value);
                    }
                }
                else
                {
                    MarkInvitationAccepted(ppi.InvitationDetails.Where(PID => PID.AcceptedProviderID == provider.ProviderID).Single());
                }

                /** PJB: not sure we want to do this....
                if (AHAHelpContent.PatientGroup.FindAllByProviderID(this.CurrentProvider.ProviderID).Count(PG => string.IsNullOrEmpty(PG.GroupRuleXml)) > 1)
                    Response.Redirect(string.Format("{0}?pid={1}&invitationid=", AHAAPI.Provider.ProviderPageUrl.GROUP_ADD_GROUPS_FOR_PATIENT, GRCBase.BlowFish.EncryptString(this.CurrentPatient.UserHealthRecordID.ToString())));
                else
                    Response.Redirect(AHAAPI.Provider.ProviderPageUrl.HOME);
                **/
                
            }
            else if (ppi != null)
            {
                // Cancel And Block Invitation
                // PJB: not sure why the below uses ppi.PendgingInvitationDetails.First() while code above uses a lamba expression
                // to get what should be the same thing??
                AHAHelpContent.PatientProviderInvitationDetails objAvailableDetails = ppi.PendingInvitationDetails.First();

                //Marks the existing invitations, sent by this patient to the current provider, as blocked
                //cannot user objInvitation.PatientID.Value as patient id is null because whilr creating an invitation patient id is passed as null
                AHAHelpContent.PatientProviderInvitationDetails.MarkExistingInvitationsAsBlocked(ppi.PatientID.Value, provider.ProviderID, false);

                objAvailableDetails.Status = AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Blocked;
                objAvailableDetails.Update();

                result = false;
            }
            return result;
        }

        private static void ConnectPatientAndProvider(AHAHelpContent.PatientProviderInvitation currentInvitation, int providerID)
        {
            List<int> patientlist = new List<int>();
            patientlist.Add(currentInvitation.PatientID.Value);

            AHAHelpContent.PatientGroup.AddPatientToGroup(patientlist, null, providerID);
        }

        private static void MarkInvitationAccepted(AHAHelpContent.PatientProviderInvitationDetails currentInvitationDetails)
        {
            currentInvitationDetails.DateAccepted = DateTime.Now;
            currentInvitationDetails.Status = AHAHelpContent.PatientProviderInvitationDetails.InvitationStatus.Accepted;
            currentInvitationDetails.Update();
        }
    }
}