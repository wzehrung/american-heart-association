﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

using AHACommon;
using AHAProvider;
using AHAHelpContent ;
using Heart360Provider;

namespace Heart360Provider.Utility
{
    /// <summary>
    /// This class should be the base class for all Provider Portal pages where the provider is logged in!
    /// It mainly serves as a mapper/helper to the Provider.Master methods.
    /// </summary>
    public class ProviderWebPage : System.Web.UI.Page
    {
        public static int NO_PROVIDER_ID = -1;

        // This is the standardized name for the WebMethod to handle global popups as defined in ProviderModules.POPUP_ID
        public static string POPUP_WEBMETHOD = "HeaderPopupHandler";


        // We need a way to store the POPUP_ID of the current popup so when the popup form is submitted,
        // we know which popup it is!
        // The INIT call contains the POPUP_ID in the "id" field, and that will specify the current popup.
        // What we will do is create a private UXControl that can hold a POPUP_ID.
        // We will store that UXControl in the POPUP_ID_UNKNOWN index of the popup list, which was going to be unused :-)
        private class CurrentPopup : AHACommon.UXControl
        {
            public int mPopId;      // ProviderModules.POPUP_ID

            public CurrentPopup()
                : base()
            {
                mPopId = (int)ProviderModules.POPUP_ID.POPUP_ID_UNKNOWN;
            }

            override public string GetMarkup(NameValue[] inputs)
            {
                return string.Empty;
            }
            override public string ProcessFormValues(NameValue[] inputs)
            {
                return JsonFailure("Feature not supported");
            }

        }

        public Heart360Provider.MasterPages.Provider ProviderMaster
        {
            get { return (Heart360Provider.MasterPages.Provider)this.Page.Master; }
        }

        public int ProviderID
        {
            get
            {
                AHAHelpContent.ProviderDetails pd = ((MasterPages.Provider)this.Master).CurrentProvider;
                return (pd != null) ? pd.ProviderID : NO_PROVIDER_ID;
            }
        }

        /// <summary>
        /// This is used by static WebMethods where we need the ProviderID.
        /// We have to do some digging in current context....
        /// </summary>
        public static int GetProviderID()
        {
                // Note: assuming the provider is logged in.
            try
            {
                ProviderContext pc = AHAProvider.SessionInfo.GetProviderContext();
                return (pc != null) ? pc.ProviderID : NO_PROVIDER_ID;
            }
            catch (Exception)
            {
            }
            return NO_PROVIDER_ID;
        }

        public static void InitializeModuleHeaders( int providerId, Heart360Provider.ProviderModules.MODULE_ID modid, string pagename )
        {
            Heart360Provider.Controls.ModuleHeader[] headers = Heart360Provider.MasterPages.Provider.GetModuleHeaders( providerId );

            if (headers != null)
            {
                foreach (ProviderModules.MODULE_ID modenum in Enum.GetValues(typeof(ProviderModules.MODULE_ID)))
                {
                    if (modenum != ProviderModules.MODULE_ID.MODULE_ID_COUNT)
                    {
                        headers[(int)modenum].HeaderLocation = (modenum !=modid) ?
                                                                Heart360Provider.Controls.ModuleHeader.HEADER_LOCATION.HEADER_LOCATION_MODULE : Heart360Provider.Controls.ModuleHeader.HEADER_LOCATION.HEADER_LOCATION_MODULE_CURRENT;
                        headers[(int)modenum].PopupPage = pagename ;
                        headers[(int)modenum].PopupLink1Method = POPUP_WEBMETHOD;
                        headers[(int)modenum].PopupLink2Method = POPUP_WEBMETHOD;
                    }
                }
            }

        }

        public static Heart360Provider.Controls.ModuleHeader GetModuleHeader(int providerId, Heart360Provider.ProviderModules.MODULE_ID modid)
        {
            Heart360Provider.Controls.ModuleHeader[] headers = Heart360Provider.MasterPages.Provider.GetModuleHeaders( providerId );

            return (headers != null) ? headers[(int)modid] : null ;
        }

        public static string HeaderHandlerImplementation(bool INIT, AHACommon.NameValue[] inputs)
        {
            //check for session expiration
            if (!UXControl.CheckProviderSessionActive())
            {
                return UXControl.RedirectToLoginJavascript;
            }

            string result = UXControl.GenericHtmlErrorMessage;

            //
            // This Header Handler only needs to support INIT since it does not contain or represent a form.
            //
            if (INIT)
            {
                int modId = (inputs[0].name.Equals("id")) ? int.Parse(inputs[0].value) : -1;

                if (modId >= (int)ProviderModules.MODULE_ID.MODULE_ID_UNKNOWN && modId < (int)ProviderModules.MODULE_ID.MODULE_ID_COUNT)
                {
                    Heart360Provider.Controls.ModuleHeader[] headers = Heart360Provider.MasterPages.Provider.GetModuleHeaders(GetProviderID());

                    if (headers != null)
                    {
                        result = headers[modId].GetMarkup(inputs);
                    }
                }
            }

            return result;
        }

        public static string PopupHandlerImplementation(bool INIT, AHACommon.NameValue[] inputs )
        {
            //check for session expiration
            if (!UXControl.CheckProviderSessionActive())
            {
                //javascript to redirect to login
                return UXControl.RedirectToLoginJavascript;
            }

            string result = string.Empty;

            if (INIT)
            {
                result = "<div>Unimplemented functionality...</div>";

                int popId = -1 ;
                int inputsIndexPopId = 0;
                for (; inputsIndexPopId < inputs.Length; inputsIndexPopId++)
                {
                    if (inputs[inputsIndexPopId].name.Equals("id"))
                    {
                        char[]  delimiter = { ',' };
                        // PJB: the id parameter may contain a comma delimited multi-value. The first value is always the popup id.
                        string[]    ids = inputs[inputsIndexPopId].value.Split(delimiter);

                        popId = int.Parse(ids[0]);

                        // PJB: strip out the first id as it is the module id and popups don't need to know this.
                        if (ids.Length > 0)
                            inputs[inputsIndexPopId].value = (ids.Length == 1) ? string.Empty : inputs[inputsIndexPopId].value.Substring( ids[0].Length+1 ) ;
                        break;
                    }
                }

                // So inputs has had the popupid stripped out.

                if (popId >= (int)ProviderModules.POPUP_ID.POPUP_ID_UNKNOWN && popId < (int)ProviderModules.POPUP_ID.POPUP_ID_COUNT)
                {
                    AHACommon.UXControl[] popups = Heart360Provider.MasterPages.Provider.GetModulePopups(GetProviderID());

                    if (popups != null)
                    {
                        if (popups[popId] == null)
                        {
                            switch (popId)
                            {
                                case (int)ProviderModules.POPUP_ID.POPUP_ID_CREATE_GROUP:
                                    {
                                        Heart360Provider.Controls.CreateGroupPopup cgp = new Controls.CreateGroupPopup();
                                        cgp.Name = "CreateGroupPopup";
                                        popups[popId] = cgp;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_INVITE_BY_PRINT:
                                    {
                                        Heart360Provider.Controls.InviteByPrintPopup ibep = new Controls.InviteByPrintPopup();
                                        ibep.Name = "InviteByPrintPopup";
                                        popups[popId] = ibep;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_INVITE_BY_EMAIL:
                                    {
                                        Heart360Provider.Controls.InviteByEmailPopup ibep = new Controls.InviteByEmailPopup();
                                        ibep.Name = "InviteByEmailPopup";
                                        popups[popId] = ibep;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_CHANGE_PASSWORD:
                                    {
                                        Heart360Provider.Controls.ChangePasswordPopup cpp = new Controls.ChangePasswordPopup();
                                        cpp.Name = "ChangePasswordPopup";
                                        popups[popId] = cpp;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_COMPOSE_MESSAGE:
                                    {
                                        Heart360Provider.Controls.ComposeMessagePopup cmp = new Controls.ComposeMessagePopup();
                                        cmp.Name = "ComposeMessagePopup";
                                        popups[popId] = cmp;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_PATIENT_MESSAGE:
                                    {
                                        Heart360Provider.Controls.ComposeMessagePopup pmp = new Controls.ComposeMessagePopup();
                                        pmp.Name = "ComposeMessagePopup";
                                        popups[popId] = pmp;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_CREATE_ALERT:
                                    {
                                        Heart360Provider.Controls.AddAlertRulePopup arp = new Controls.AddAlertRulePopup();
                                        arp.Name = "AddAlertRulePopup";
                                        popups[popId] = arp;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_CREATE_ANNOUNCEMENT:
                                    {
                                        Heart360Provider.Controls.CreateAnnouncementPopup cap = new Controls.CreateAnnouncementPopup();
                                        cap.Name = "CreateAnnouncementPopup";
                                        popups[popId] = cap;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_CREATE_RESOURCE:
                                    {
                                        Heart360Provider.Controls.CreateResourcePopup crp = new Controls.CreateResourcePopup();
                                        crp.Name = "CreateResourcePopup";
                                        popups[popId] = crp;
                                    }
                                    break;

                                case (int)ProviderModules.POPUP_ID.POPUP_ID_CREATE_NOTE:
                                    {
                                        Heart360Provider.Controls.AddProviderNotePopup pnp = new Controls.AddProviderNotePopup();
                                        pnp.Name = "AddProviderNotePopup";
                                        popups[popId] = pnp;
                                    }
                                    break;

                                default:
                                    break;
                            }

                            if (popups[popId] != null && !popups[popId].Initialize())
                            {
                                popups[popId] = null;
                            }
                        }

                        // set who the current popup is now
                        ProviderWebPage.CurrentPopup currpop = (popups[(int)ProviderModules.POPUP_ID.POPUP_ID_UNKNOWN] != null) ?
                                                                    (ProviderWebPage.CurrentPopup)popups[(int)ProviderModules.POPUP_ID.POPUP_ID_UNKNOWN] : new ProviderWebPage.CurrentPopup();
                        currpop.mPopId = popId;
                        popups[(int)ProviderModules.POPUP_ID.POPUP_ID_UNKNOWN] = currpop;

                        // get the markup
                        if (popups[popId] != null)
                        {
                            result = popups[popId].GetMarkup(inputs);
                        }
                    }
                }
            }
            else
            {
                result = AHACommon.UXControl.JsonFailure("Unimplemented functionality");

                // Use the current popup!
                AHACommon.UXControl[] popups = Heart360Provider.MasterPages.Provider.GetModulePopups(GetProviderID());
                if (popups != null && popups[(int)ProviderModules.POPUP_ID.POPUP_ID_UNKNOWN] != null )
                {
                    int popId = ((ProviderWebPage.CurrentPopup)popups[(int)ProviderModules.POPUP_ID.POPUP_ID_UNKNOWN]).mPopId;

                    result = (popups[popId] != null) ? popups[popId].ProcessFormValues(inputs) : AHACommon.UXControl.JsonFailure("Internal error.");
                }
            }

            return result;

        }

    }
}