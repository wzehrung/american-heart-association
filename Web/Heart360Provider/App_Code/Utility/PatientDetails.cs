﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Provider.Utility
{
    public class PatientGuids
    {
        public Guid PersonID
        {
            get;
            set;
        }

        public Guid RecordID
        {
            get;
            set;
        }
    }

    public class PatientDetails
    {
            //
            // the PersonalItem class has name and some other things about a patient.
            // but not email
            //
        public static HVManager.PersonalDataManager.PersonalItem GetPatientDetails(int patientID)
        {
            HVManager.PersonalDataManager.PersonalItem result = null;

            AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(patientID );

            if (objPatient != null && objPatient.OfflinePersonGUID.HasValue && objPatient.OfflineHealthRecordGUID.HasValue)
            {

                // Now try and get the Patients name from HV
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                try
                {
                    result = mgr.PersonalDataManager.Item;
                }
                catch (Microsoft.Health.HealthServiceException hse)
                {
                    if (hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.InvalidRecordState ||
                        hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.AccessDenied)
                    {
                        //
                    }
                    result = null;
                }
            }

            return result;
        }

        public static HVManager.PersonalDataManager.PersonalItem GetPatientDetails(AHAHelpContent.Patient objPatient)
        {
            HVManager.PersonalDataManager.PersonalItem result = null;

            if (objPatient != null && objPatient.OfflinePersonGUID.HasValue && objPatient.OfflineHealthRecordGUID.HasValue)
            {

                // Now try and get the Patients name from HV
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                try
                {
                    result = mgr.PersonalDataManager.Item;
                }
                catch (Microsoft.Health.HealthServiceException hse)
                {
                    if (hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.InvalidRecordState ||
                        hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.AccessDenied)
                    {
                        //
                    }
                    result = null;
                }
            }

            return result;
        }

        public static HVManager.PersonalContactDataManager.ContactItem GetPatientContactDetails(int patientID)
        {
            HVManager.PersonalContactDataManager.ContactItem result = null;

            AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(patientID );

            if (objPatient != null && objPatient.OfflinePersonGUID.HasValue && objPatient.OfflineHealthRecordGUID.HasValue)
            {

                // Now try and get the Patients name from HV
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                try
                {
                    result = mgr.PersonalContactDataManager.Item;
                }
                catch (Microsoft.Health.HealthServiceException hse)
                {
                    if (hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.InvalidRecordState ||
                        hse.ErrorCode == Microsoft.Health.HealthServiceStatusCode.AccessDenied)
                    {
                        //
                    }
                    result = null;
                }
            }

            return result;
        }

    }
}