﻿using System;
using System.Linq;
using System.Collections.Generic;
using AHAHelpContent;
using HVManager;

namespace Heart360Provider.Utility
{
    public class MessageInfo : AHAHelpContent.Message
    {
        private string m_Subject;
        private string m_Body;
        private bool m_IsSentByProvider;
        private bool _SubjectAndBodyLoaded = false;

        private UserTypeDetails CurrentUserDetails
        {
            get;
            set;
        }

        private string CurrentFolderType
        {
            get;
            set;
        }

        private void _LoadSubjectAndBody()
        {
            if (!_SubjectAndBodyLoaded)
            {
                HVManager.MessageDataManager.MessageItem objMessageItem = MessageWrapper.GetMessageByIdForProvider(this.CurrentUserDetails.UserID, this.MessageID);
                if (objMessageItem == null)
                {
                    //had an issue where a provider was able to send messages to all patients/grop when the ptovider 
                    //had no patiens, in such cases, the message is null and the patient id is 0
                    //the fix is to NOT CHEck for that message agaian becasue the patient id is 0 and we do not have a OfflinePersonGUID
                    if (this.PatientID > 0)
                    {
                        AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(this.PatientID);
                        objMessageItem = MessageWrapper.FetchMessageAgainByIdForProvider(objPatient.OfflinePersonGUID.Value,
                                                objPatient.OfflineHealthRecordGUID.Value,
                                                this.CurrentUserDetails.UserID,
                                                this.MessageID);

                    }
                }

                m_Subject = objMessageItem != null ? objMessageItem.Subject.Value : "-No Subject-";
                m_Body = objMessageItem != null ? objMessageItem.MessageBody.Value : string.Empty;
                m_IsSentByProvider = (objMessageItem != null && objMessageItem.IsSentByProvider.Value.HasValue) ? objMessageItem.IsSentByProvider.Value.Value : false;
                
            }
        }

        public bool SentByProvider
        {
            get
            {
                _LoadSubjectAndBody();
                return m_IsSentByProvider;
            }
        }

        public string From
        {
            get
            {
                if (this.CurrentFolderType == AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX)
                {   
                    AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(this.PatientID);

                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                    HVManager.PersonalDataManager.PersonalItem objPersonalInfo = mgr.PersonalDataManager.Item;

                    return string.Format("{0} {1}", objPersonalInfo.FirstName.Value, objPersonalInfo.LastName.Value);
                }
                else
                {       
                    return AHAHelpContent.ProviderDetails.FindByProviderID(this.ProviderID).FormattedName;
                }
            }
        }

        public string To
        {
            get
            {
                if (this.CurrentFolderType == AHACommon.AHADefs.FolderType.MESSAGE_FOLDER_INBOX)
                {       
                    return AHAHelpContent.ProviderDetails.FindByProviderID(this.ProviderID).FormattedName;
                }
                else
                {   
                    if (this.GroupID.HasValue)
                    {
                        AHAHelpContent.PatientGroup objGroup = AHAHelpContent.PatientGroup.FindByGroupID(this.GroupID.Value);

                        if (objGroup.IsDefault)
                            return "All Patients";

                        return objGroup.Name;
                    }

                    AHAHelpContent.Patient objPatient = AHAHelpContent.Patient.FindByUserHealthRecordID(this.PatientID);

                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);

                    HVManager.PersonalDataManager.PersonalItem objPersonalInfo = mgr.PersonalDataManager.Item;

                    return string.Format("{0} {1}", objPersonalInfo.FirstName.Value, objPersonalInfo.LastName.Value);          
                }
            }
        }

        public string Subject
        {
            get
            {
                _LoadSubjectAndBody();

                return m_Subject;
            }
        }

        public string MessageBody
        {
            get
            {
                _LoadSubjectAndBody();

                return m_Body;
            }
        }

        public static List<MessageInfo> PopulateMessages(List<Message> objMessageList, UserTypeDetails utdCurrentUserDetails, string strCurrentFolderType)
        {
            List<MessageInfo> objMessageInfoList = new List<MessageInfo>();
            objMessageList.ForEach(delegate(Message objMessage)
            {
                objMessageInfoList.Add(new MessageInfo()
                {
                    CurrentUserDetails = utdCurrentUserDetails,
                    CurrentFolderType = strCurrentFolderType,
                    IsFlaged = objMessage.IsFlaged,
                    IsRead = objMessage.IsRead,
                    MessageID = objMessage.MessageID,
                    MessageType = objMessage.MessageType,
                    PatientID = objMessage.PatientID,
                    ProviderID = objMessage.ProviderID,
                    SentDate = objMessage.SentDate,
                    GroupID = objMessage.GroupID
                });
            });

            return objMessageInfoList;
        }

        public static MessageInfo GetMessageInfoObject(Message objMessage, UserTypeDetails utdCurrentUserDetails, string strCurrentFolderType)
        {
            MessageInfo objMessageInfo = new MessageInfo()
            {
                CurrentUserDetails = utdCurrentUserDetails,
                CurrentFolderType = strCurrentFolderType,
                IsFlaged = objMessage.IsFlaged,
                IsRead = objMessage.IsRead,
                MessageID = objMessage.MessageID,
                MessageType = objMessage.MessageType,
                PatientID = objMessage.PatientID,
                ProviderID = objMessage.ProviderID,
                SentDate = objMessage.SentDate,
                GroupID = objMessage.GroupID
            };

            return objMessageInfo;
        }
    }
}