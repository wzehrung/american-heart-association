﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.Health;

//
// NOTE: There was a MessageWrapper in V5 AHAAPI, then the V6 Patient Portal defined its own in AppCode/DataWrappers
//       Can't put the one in Patient Portal into HVWrapper because it still uses session state from AHAAPI.
//

namespace Heart360Provider.Utility
{
    public class MessageWrapper
    {
        public static void CreateMessage(HVManager.MessageDataManager.MessageItem itemEdits, Guid gPersonGUID, Guid gRecordGUID)
        {
            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);
            mgr.MessageDataManager.CreateItem(itemEdits);
        }

        public static void UpdateMessage(HVManager.MessageDataManager.MessageItem itemEdits, Guid gPersonGUID, Guid gRecordGUID)
        {
            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);
            mgr.MessageDataManager.UpdateItem(itemEdits);
        }

        public static HVManager.MessageDataManager.MessageItem GetMessageByIdForProvider(int iProviderID, int iMessageID)
        {
            List<HVManager.MessageDataManager.MessageItem> allProviderMessages = new List<HVManager.MessageDataManager.MessageItem>();

            // PJB: because we are using AJAX and WebMethods, we can't use this request context. OPTIMIZE OPTIMIZE OPTIMIZE
            //List<AHAHelpContent.Patient> listPatient = AHAProvider.H360RequestContext.GetContext().PatientList;
            List<AHAHelpContent.Patient> listPatient = AHAHelpContent.Patient.FindAllByProviderID(iProviderID); // Really??? All patients when we know the message id?

            foreach (AHAHelpContent.Patient objPatient in listPatient)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                List<HVManager.MessageDataManager.MessageItem> listMessage = mgr.MessageDataManager.GetAllMessagesForProvider(iProviderID);
                allProviderMessages.AddRange(listMessage);
            }

            return allProviderMessages.Where(i => i.MessageID != null && i.MessageID.Value == iMessageID).FirstOrDefault();
        }
        /**
        public static HVManager.MessageDataManager.MessageItem GetMessageByIdForPatient(int iMessageID)
        {
            LinkedList<HVManager.MessageDataManager.MessageItem> listMessage = HVHelper.HVManagerPatientBase.MessageDataManager.GetDataBetweenDates(AHACommon.DataDateReturnType.AllData);

            if (listMessage.Count == 0)
                return null;

            return listMessage.Where(i => i.MessageID != null && i.MessageID.Value == iMessageID).FirstOrDefault();
        }
        **/

        public static HVManager.MessageDataManager.MessageItem FetchMessageAgainByIdForProvider(Guid gPersonGUID, Guid gRecordGUID, int iProviderID, int iMessageID)
        {
            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);

            mgr.MessageDataManager.FlushCache();

            List<HVManager.MessageDataManager.MessageItem> listMessage = mgr.MessageDataManager.GetAllMessagesForProvider(iProviderID);

            return listMessage.Where(i => i.MessageID != null && i.MessageID.Value == iMessageID).FirstOrDefault();
        }

        /** HVHelper not in Heart360Provider
        public static HVManager.MessageDataManager.MessageItem FetchMessageAgainByIdForPatient(int iMessageID)
        {
            HVHelper.HVManagerPatientBase.MessageDataManager.FlushCache();

            LinkedList<HVManager.MessageDataManager.MessageItem> listMessage = HVHelper.HVManagerPatientBase.MessageDataManager.GetDataBetweenDates(AHACommon.DataDateReturnType.AllData);

            if (listMessage.Count == 0)
                return null;

            return listMessage.Where(i => i.MessageID != null && i.MessageID.Value == iMessageID).FirstOrDefault();
        }
         **/

        public static void DeleteMessage(HealthRecordItemKey itemKey, Guid gPersonGUID, Guid gRecordGUID)
        {
            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(gPersonGUID, gRecordGUID);
            HVManager.WCQueries.DeleteThing(HVManager.Helper.GetAccessor(gPersonGUID, gRecordGUID), itemKey);
            mgr.MessageDataManager.RemoveItem(itemKey.Id);
        }
    }
}