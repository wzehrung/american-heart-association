﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GRCBase;

namespace Heart360Provider.Utility
{
    public class EmailTemplateHelper
    {
        public static string SiteUrlForImage(bool bProcess)
        {
            if (bProcess)
            {
                return AppSettings.Url.SiteUrlNoHttps;
            }
            else
            {
                if (string.IsNullOrEmpty(System.Web.HttpContext.Current.Request["PrinterFriendly"]))
                {
                    return GRCBase.UrlUtility.ServerRootUrl.Replace("https://", "http://");
                }
                else
                {
                    return GRCBase.UrlUtility.ServerRootUrl;
                }
            }
        }

        public static string GetEmailTemplateDirectory(bool bProcess, string strLocale)
        {
            if (bProcess)
            {
                if (strLocale != AHACommon.Locale.English)
                {
                    return string.Format( AppSettings.Directory.EmailTemplate + ".{0}", strLocale);
                }
                return AppSettings.Directory.EmailTemplate;
            }
            else
            {
                if (strLocale != AHACommon.Locale.English)
                {
                    return System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates.{0}/", strLocale));
                }

                return System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/");
            }
        }

        public static string GetEmailHeader(bool bProcess, string strLocale)
        {
            string strFilePath = string.Format("{0}\\Header.htm", GetEmailTemplateDirectory(bProcess, strLocale));

            string strHeader = IOUtility.ReadHtmlFile(strFilePath);

            strHeader = System.Text.RegularExpressions.Regex.Replace(strHeader, "##SiteUrl##", SiteUrlForImage(bProcess));

            return strHeader;
        }

        public static string GetEmailFooter(bool bProcess, bool bIsForProvider, string strLocale)
        {
            string strFilePath = string.Format("{0}\\Footer.htm", GetEmailTemplateDirectory(bProcess, strLocale));
            string strFooter = IOUtility.ReadHtmlFile(strFilePath);

            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##SiteUrl##", SiteUrlForImage(bProcess));

            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##PrivacyPolicy##", AppSettings.Url.Heart360PrivacyPolicy);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##Copyright##", AppSettings.Url.Heart360Copyright );
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##EthicsPolicy##", AppSettings.Url.Heart360EthicsPolicy);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##ConflictOfInterest##", AppSettings.Url.Heart360ConflictOfInterest);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##LinkingPolicy##", AppSettings.Url.Heart360LinkingPolicy);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##Diversity##", AppSettings.Url.Heart360Diversity);
            strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##Careers##", AppSettings.Url.Heart360Careers);

            if (!bProcess)
            {
                if (bIsForProvider)
                {
                    strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##DISCLAIMER##", LanguageResourceHelper.Disclaimer_Provider_HTML);
                }
                else
                {
                    strFooter = System.Text.RegularExpressions.Regex.Replace(strFooter, "##DISCLAIMER##", LanguageResourceHelper.Disclaimer_Patient_HTML);
                }
            }

            return strFooter;
        }

        public static string GetCompleteHtmlForEmail(bool bProcess, string strContent, bool bIsForProvider, string strLocale)
        {
            strContent = System.Text.RegularExpressions.Regex.Replace(strContent, "##SiteUrl##", SiteUrlForImage(bProcess));

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{0}{1}{2}", GetEmailHeader(bProcess, strLocale), strContent, GetEmailFooter(bProcess, bIsForProvider, strLocale));

            return sb.ToString();
        }
    }
}
