﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AHACommon;

namespace Heart360Provider.Utility
{

    public class LanguageResourceHelper
    {
        /** Please use AHACommon.CommonUtility.GetDateTimeForDB()
        public static string GetDateTimeForDB(DateTime dt)
        {
            return String.Format("{0:yyyy/M/d HH:mm:ss}", dt);
        }
         **/

        /** Please use AHACommon.DataDateFormatter.GetDateLabelForDateRange()
        public static string GetDateLabelForDateRange(AHACommon.DataDateReturnType dtType)
        {
            if (dtType == AHACommon.DataDateReturnType.LastYear || dtType == AHACommon.DataDateReturnType.Last2Years)
                return "MM/yy";
            else
                return AHACommon.DateTimeHelper.GetShortDateFormatForWeb();
        }
         **/

        /// <summary>
        /// Disclaimer_Provider
        /// </summary>
        public static string Disclaimer_Provider_PDF
        {
            get
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Footer_Disclaimer").ToString();
            }
        }

        /// <summary>
        /// Disclaimer_Patient
        /// </summary>
        public static string Disclaimer_Patient_PDF
        {
            get
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Footer_Disclaimer").ToString();
            }
        }

        /// <summary>
        /// Disclaimer_Provider
        /// </summary>
        public static string Disclaimer_Provider_HTML
        {
            get
            {
                string strVal = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Footer_Disclaimer").ToString().Replace("\\r\\n", "<br />");
                strVal = System.Text.RegularExpressions.Regex.Replace(strVal, "##REGISTERED##", "<sup>&#174;</sup>");
                return strVal;
            }
        }

        /// <summary>
        /// Disclaimer_Patient
        /// </summary>
        public static string Disclaimer_Patient_HTML
        {
            get
            {
                string strVal = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Footer_Disclaimer").ToString().Replace("\\r\\n", "<br />");
                strVal = System.Text.RegularExpressions.Regex.Replace(strVal, "##REGISTERED##", "<sup>&#174;</sup>");
                return strVal;
            }
        }
    }
}
