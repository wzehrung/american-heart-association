
using System;
using System.Web.UI;
using System.Configuration;
using System.Text;

namespace Heart360Provider.Utility
{
    /// <summary>
    /// A list of file paths that are used throughout the application
    /// </summary>
    public class GlobalPaths
    {
        // public const string WebConfigSetting_DataCacheDiagnostics = "DataCacheDiagnostics";

        public static string GetInvitationLink( string strInvitationGUID, bool isPatient )
        {
            StringBuilder sbInvitation = new StringBuilder();
            // PJB: Commented this out on 6/23/2014 since invitation entrypoint will most likely be elsewhere than where provider portal is hosted
            //sbInvitation.Append(GRCBase.UrlUtility.ServerRootUrl);
            if(!isPatient)
                sbInvitation.Append(GetURLWithoutApp(Heart360Provider.AppSettings.Url.ProviderInvitationEntrypoint));
            else
                sbInvitation.Append(GetURLWithoutApp(Heart360Provider.AppSettings.Url.PatientInvitationEntrypoint));

            sbInvitation.Append("?invitationid=");
            //sbInvitation.Append(GRCBase.BlowFish.EncryptString(strInvitationGUID));
            sbInvitation.Append(strInvitationGUID);
            return sbInvitation.ToString();
        }

        private static string GetURLWithoutApp(string strUrl)
        {
            if (strUrl.StartsWith("~"))
                return strUrl.Substring(1).ToLower();

            return strUrl.ToLower();
        }
    }
}