﻿using System;
using System.Drawing;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using Microsoft.Health.Web;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;

using AHACommon;
using AHABusinessLogic;
using Heart360Provider.Graph;


namespace Heart360Provider.Reports
{
    public enum DataViewType
    {
        Table,
        Chart,
        TableAndChart,
        CombinedChart,
        TableAndCombinedChart
    }

    public enum PDFType
    {
        PersonalInfo,
        HealthHistory,
        EmergencyContact,
        Weight,
        Cholesterol,
        CurrentMedication,
        DiscontinuedMedication,
        BloodPressure,
        BloodGlucose,
        Exercise,
    }

    /// <summary>
    /// http://itextsharp.sourceforge.net/tutorial/index.html
    /// http://itextsharp.sourceforge.net/tutorial/ch12.html#openclose - Adding Page Eevents
    /// open source, library can be downloaded from http://itextsharp.sourceforge.net/
    /// </summary>
    public class PDFHelper
    {
        public static string EmptyImage = "empty_image";
        public static iTextSharp.text.Font PageHeaderFont = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 12, iTextSharp.text.Font.BOLD);
        public static iTextSharp.text.Font PatientNameFont = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 14, iTextSharp.text.Font.BOLD + iTextSharp.text.Font.UNDERLINE);
        public static iTextSharp.text.Font UrlFont = new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 12, iTextSharp.text.Font.UNDERLINE, new iTextSharp.text.Color(0, 153, 255));
        public static string noDataText = System.Web.HttpContext.GetGlobalResourceObject("Reports", "No_Data_To_Report").ToString();

        #region Individual Report

        // Create Individual Patient Report
        public static void CreateIndividualPatientReportPDF(
            DataDateReturnType dateRange, List<AHAHelpContent.Patient> patientList, String reportTitle, 
            String reportTimeframe, HashSet<string> reportFlags)
        {
            MemoryStream memStream = null;

            try
            {
                string strFileName = "Individual_Report_for_" + reportTimeframe.Replace(" ", "_") + ".pdf";
                string strReportTitle = string.Format(
                    reportTitle,
                    GRCBase.DateTimeHelper.GetFormattedDateTimeString(
                        DateTime.Now,
                        AHACommon.DateTimeHelper.GetDateFormatForWeb(),
                        GRCBase.GRCDateSeperator.HYPHEN,
                        GRCBase.GRCTimeFormat.NONE));
                string strAuthor = GRCBase.ConfigReader.GetValue("Author");

                Document document = new Document(PageSize.A4, 22f, 20f, 99f, 60f);
                memStream = new MemoryStream();

                PdfWriter writer = PdfWriter.GetInstance(document, memStream);
                HeaderFooter footer = null;
                footer = new HeaderFooter(_GetDisclaimerPhrase(
                    AHACommon.AHAAppSettings.Disclaimer_Provider_PDF, 
                    new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 8, iTextSharp.text.Font.NORMAL)),false);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                document.Footer = footer;

                writer.PageEvent = new PDFPageEvent();

                //Adding document headers
                document.AddTitle(reportTitle);
                document.AddCreationDate();
                document.AddAuthor(strAuthor);

                //End adding document headers
                bool bShowIndexPage = false;
                int iChapterIndex = 1;
                Phrase objPhrase = null;
                PdfPTable tbl = new PdfPTable(1);
                List<Chapter> ChapterCollections = new List<Chapter>();

                tbl.DefaultCell.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                tbl.DefaultCell.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                tbl.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                tbl.DefaultCell.PaddingTop = 5f;

                document.Open();

                if (patientList.Count > 1)
                {
                    //There are multiple patient; so we need to show the index page.
                    iChapterIndex = 2;

                    bShowIndexPage = true;

                    objPhrase = new Phrase(20);
                    objPhrase.Add(new Chunk(reportTitle));

                    tbl = new PdfPTable(1);
                    tbl.DefaultCell.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                    tbl.DefaultCell.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                    tbl.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    tbl.DefaultCell.PaddingTop = 5f;

                    tbl.AddCell(objPhrase);
                }

                #region Populate Inner Chapters

                foreach (AHAHelpContent.Patient objPatient in patientList)
                {
                    PatientGuids objGuids = new PatientGuids()
                    {
                        PersonID = objPatient.OfflinePersonGUID.Value,
                        RecordID = objPatient.OfflineHealthRecordGUID.Value
                    };

                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                    HVManager.PersonalDataManager.PersonalItem objPersonItem = mgr.PersonalDataManager.Item;

                    string strName = objPersonItem.FullName;
                    string strGuid = Guid.NewGuid().ToString();

                    objPhrase = new Phrase(20);
                    objPhrase.Add(new Chunk(string.Format(reportTitle + " - {0}", strName), PatientNameFont).SetLocalDestination(strGuid));

                    Chapter objChapter = new Chapter(new Paragraph(objPhrase), iChapterIndex);
                    objChapter.NumberDepth = 0;

                    if (CreateChapters(objChapter, objGuids, mgr, dateRange, reportFlags))
                    {
                        ChapterCollections.Add(objChapter);
                        if (bShowIndexPage)
                        {
                            tbl.AddCell(new Phrase(new Chunk(string.Format("{0}", strName), UrlFont).SetLocalGoto(strGuid)));
                        }
                        iChapterIndex++;
                    }
                }

                #endregion

                //objIndexChapter.Add(tbl);

                if (ChapterCollections.Count > 0)
                {
                    if (ChapterCollections.Count > 1)
                        document.Add(tbl);

                    //Adding each chapter in the chapter collection
                    foreach (Chapter objChapter in ChapterCollections)
                    {
                        document.Add(objChapter);
                    }
                }


                else
                {
                    //no data
                    document.Add(new Paragraph(noDataText));
                }

                document.Close();

                System.Web.HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + strFileName);
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.OutputStream.Write(memStream.GetBuffer(), 0, memStream.GetBuffer().Length);
                System.Web.HttpContext.Current.Response.OutputStream.Flush();
                System.Web.HttpContext.Current.Response.OutputStream.Close();
                System.Web.HttpContext.Current.Response.End();
            }
            finally
            {
                if (memStream != null)
                {
                    memStream.Close();
                    memStream.Dispose();
                }
            }
        }

        private static bool CreateChapters(
            Chapter objChapter, PatientGuids objGuids, HVManager.AllItemManager mgr, AHACommon.DataDateReturnType eType, HashSet<string> reportFlags)
        {

            bool bPatientHeadAdded = false; //has a prior section been created

            #region BP Specific
            {
                if (reportFlags.Contains("bpChart") || reportFlags.Contains("bpTable"))
                {
                    LinkedList<HVManager.BloodPressureDataManager.BPItem> objItems = mgr.BloodPressureDataManager.GetDataBetweenDates(eType);
                    if (objItems != null && objItems.Count > 0)
                    {
                        objChapter.Add(PDFHelper.GetPageHeadingTable(new Phrase(
                            System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Report_DataType_BP").ToString(), PageHeaderFont)));

                        if (reportFlags.Contains("bpTable"))
                            objChapter.Add(PDFReports.GetBPTable(objGuids, eType));

                        if (reportFlags.Contains("bpChart"))
                        {
                            IElement image = PDFReports.GetImage(objGuids, Graph.GraphType.CombinedBP, eType, null);
                            if (image != null)
                            {
                                if (reportFlags.Contains("bpTable"))
                                    objChapter.Add(Chunk.NEXTPAGE);
                                objChapter.Add(image);
                            }
                        }
                        bPatientHeadAdded = true;
                    }
                }
            }

            #endregion

            #region Blood Glucose
            {
                if (reportFlags.Contains("bgChart") || reportFlags.Contains("bgTable"))
                {
                    LinkedList<HVManager.BloodGlucoseDataManager.BGItem> objBG = mgr.BloodGlucoseDataManager.GetDataBetweenDates(eType);
                    if (objBG != null && objBG.Count > 0)
                    {
                        if (bPatientHeadAdded)
                            objChapter.Add(Chunk.NEXTPAGE);

                        objChapter.Add(PDFHelper.GetPageHeadingTable(new Phrase(
                            System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Report_DataType_BG").ToString(), PageHeaderFont)));

                        PdfPTable tblBGAv = BloodGlucoseWrapper.GetAverageBloodGlucoseForPeriodForPDF(objGuids, eType);
                        if (tblBGAv != null)
                        {
                            objChapter.Add(tblBGAv);
                        }

                        if (reportFlags.Contains("bgTable"))
                        {
                            objChapter.Add(PDFReports.GetBloodGlucoseTable(objGuids, eType, UXControl.EnglishLanguage));
                        }

                        if (reportFlags.Contains("bgChart"))
                        {
                            IElement image = PDFReports.GetImage(objGuids, Graph.GraphType.Glucose, eType, null);
                            if (image != null)
                            {
                                if (reportFlags.Contains("bgTable"))
                                    objChapter.Add(Chunk.NEXTPAGE);
                                objChapter.Add(image);
                            }
                        }
                        bPatientHeadAdded = true;
                    }
                }
            }
            #endregion

            #region Weight
            {

                if (reportFlags.Contains("wChart") || reportFlags.Contains("wTable"))
                {
                    LinkedList<HVManager.WeightDataManager.WeightItem> weightColl = mgr.WeightDataManager.GetDataBetweenDates(eType);
                    if (weightColl != null && weightColl.Count > 0)
                    {
                        if (bPatientHeadAdded)
                            objChapter.Add(Chunk.NEXTPAGE);

                        objChapter.Add(PDFHelper.GetPageHeadingTable(new Phrase(
                            System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Report_DataType_Weight").ToString(), PageHeaderFont)));

                        if (reportFlags.Contains("wTable"))
                        {                            
                            objChapter.Add(PDFReports.GetWeightTable(objGuids, eType));
                        }

                        if (reportFlags.Contains("wChart"))
                        {
                            IElement image = PDFReports.GetImage(objGuids, Graph.GraphType.Weight, eType, null);
                            if (image != null)
                            {
                                if (reportFlags.Contains("wTable"))
                                    objChapter.Add(Chunk.NEXTPAGE);
                                objChapter.Add(image);
                            }
                        }
                        bPatientHeadAdded = true;
                    }
                }
            }
            #endregion

            #region Physical Activity
            {
                if (reportFlags.Contains("paChart") || reportFlags.Contains("paTable"))
                {
                    LinkedList<HVManager.ExerciseDataManager.ExerciseItem> exColl = mgr.ExerciseDataManager.GetDataBetweenDates(eType);
                    if (exColl != null && exColl.Count > 0)
                    {
                        if (bPatientHeadAdded)
                            objChapter.Add(Chunk.NEXTPAGE);

                        objChapter.Add(PDFHelper.GetPageHeadingTable(new Phrase(
                            System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Report_DataType_PA").ToString(), PageHeaderFont)));

                        if (reportFlags.Contains("paTable"))
                        {
                            objChapter.Add(PDFReports.GetExerciseTable(objGuids, eType));
                        }

                        if (reportFlags.Contains("paChart"))
                        {
                            IElement image = PDFReports.GetImage(objGuids, Graph.GraphType.Exercise, eType, null);
                            if (image != null)
                            {
                                if (reportFlags.Contains("paTable"))
                                    objChapter.Add(Chunk.NEXTPAGE);
                                objChapter.Add(image);
                            }
                        }
                        bPatientHeadAdded = true;
                    }
                }
            }
            #endregion

            #region Cholesterol
            {

                if (reportFlags.Contains("cChart") || reportFlags.Contains("cTable"))
                {
                    LinkedList<HVManager.CholesterolDataManager.CholesterolItem> cholesterolColl = mgr.CholesterolDataManager.GetDataBetweenDates(eType);
                    if (cholesterolColl != null && cholesterolColl.Count > 0)
                    {
                        if (bPatientHeadAdded)
                            objChapter.Add(Chunk.NEXTPAGE);

                        objChapter.Add(PDFHelper.GetPageHeadingTable(new Phrase(
                            System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_Report_DataType_Cholesterol").ToString(), PageHeaderFont)));

                        if (reportFlags.Contains("cTable"))
                        {
                            objChapter.Add(PDFReports.GetCholesterolTable(objGuids, eType));
                        }

                        if (reportFlags.Contains("cChart"))
                        {
                            IElement imgTG = PDFReports.GetImage(objGuids, Graph.GraphType.Triglycerides, eType, null);
                            IElement imgComb = PDFReports.GetImage(objGuids, Graph.GraphType.CombinedCholesterol, eType, null);

                            if (reportFlags.Contains("cTable") && (imgTG != null || imgComb != null))
                                objChapter.Add(Chunk.NEXTPAGE);

                            if(imgTG != null)
                                objChapter.Add(imgTG);
                            if(imgComb != null)
                                objChapter.Add(imgComb);
                        }
                        bPatientHeadAdded = true;
                    }
                }
            }
            #endregion

            return bPatientHeadAdded;
        }

        #endregion

        #region Custom Organization Report

        //Custom Organization Report
        public static void CreateCPReportPDF(List<AHAHelpContent.Patient> patientList,
            CustomPracticeReportHelper.ReportFunction rptFunction, CustomPracticeReportType rptType,
            DataDateReturnType dtRange, double dAmount, bool? isAbove, int? times, string reportTitle, string reportHeading)
        {
            string strPDFFileName = "Custom_Practice_Report.pdf";
            string strReportTitle = string.Format(
                reportTitle,
                GRCBase.DateTimeHelper.GetFormattedDateTimeString(
                    DateTime.Now,
                    AHACommon.DateTimeHelper.GetDateFormatForWeb(),
                    GRCBase.GRCDateSeperator.HYPHEN,
                    GRCBase.GRCTimeFormat.NONE));

            string strAuthor = GRCBase.ConfigReader.GetValue("Author");
            string strTitle = reportHeading;

            List<CustomPracticeReportHelper.PatientReport> lstPI = null;
            string strCriteria = string.Empty;
            string strIsAbove = string.Empty;
            if (isAbove.HasValue)
            {
                strIsAbove = isAbove.Value ? System.Web.HttpContext.GetGlobalResourceObject("Reports", "Provider_CustomPracticeReport_Above").ToString().ToLower() : System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Below").ToString().ToLower();
            }

            if (rptFunction == CustomPracticeReportHelper.ReportFunction.AboveOrBelowThreshold)
            {
                lstPI = CustomPracticeReportHelper.GetPatientsAboveOrBelowThreshold(patientList, rptType, dAmount, isAbove.Value, dtRange);
                strCriteria = string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_AboveOrBelowThreshold").ToString(), rptType.ToString(), strIsAbove, dAmount.ToString(), GetPeriodData(dtRange));
            }
            else if (rptFunction == CustomPracticeReportHelper.ReportFunction.ThresholdChanged)
            {
                lstPI = CustomPracticeReportHelper.GetPatientsForThresholdChanged(patientList, rptType, dtRange, dAmount);
                //strCriteria = string.Format("Selection Criteria: {0} that has changed {1} for the period {2}", rptType.ToString(), dAmount.ToString(), GetPeriodData(dtRange));
                strCriteria = string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_ThresholdChanged").ToString(), rptType.ToString(), dAmount.ToString(), GetPeriodData(dtRange));
            }
            else if (rptFunction == CustomPracticeReportHelper.ReportFunction.AboveOrBelowThresholdXTimes)
            {
                lstPI = CustomPracticeReportHelper.GetPatientsAboveOrBelowThresholdXTimes(patientList, rptType, dAmount, isAbove.Value, dtRange, times.Value);
                if (times.Value == 1)
                {
                    strCriteria = string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_AboveOrBelowXTime").ToString(), rptType.ToString(), strIsAbove, dAmount.ToString(), GetPeriodData(dtRange), times.Value.ToString());
                }
                else
                {
                    strCriteria = string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_AboveOrBelowXTimes").ToString(), rptType.ToString(), strIsAbove, dAmount.ToString(), GetPeriodData(dtRange), times.Value.ToString());
                }
            }

            MemoryStream memStream = null;

            try
            {
                Document document = new Document(PageSize.A4, 22f, 20f, 99f, 60f);
                memStream = new MemoryStream();

                List<PdfPTable> pdfTableList = null;

                PdfWriter writer = PdfWriter.GetInstance(document, memStream);
                writer.PageEvent = new PDFPageEvent();

                HeaderFooter footer = null;
                footer = new HeaderFooter(_GetDisclaimerPhrase(AHAAppSettings.Disclaimer_Provider_PDF, new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL)), false);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                document.Footer = footer;

                iTextSharp.text.Font PageHeaderFont = new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD);

                //custom practice report specific start
                pdfTableList = new List<PdfPTable>();

                //Selection criteria for report
                PdfPTable tblCriteria = new PdfPTable(1);
                tblCriteria.DefaultCell.Border = 0;
                tblCriteria.AddCell(new Phrase(strCriteria, new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                pdfTableList.Add(tblCriteria);

                //make a pdftable and add to pdfTableList
                PdfPTable tblCPRpt = new PdfPTable(2);

                tblCPRpt.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Name").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                tblCPRpt.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Provider_Report_MaxValueQualified").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));

                bool hasData = false;
                foreach (CustomPracticeReportHelper.PatientReport item in lstPI)
                {
                    HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(item.PersonGUID, item.RecordGUID);
                    string strName = string.Empty;
                    HVManager.PersonalDataManager.PersonalItem obj = mgr.PersonalDataManager.Item;
                    if (obj != null)
                    {
                        strName = string.Format("{0}, {1}", obj.LastName.Value, obj.FirstName.Value);
                        hasData = true;
                    }

                    tblCPRpt.AddCell(strName);
                    tblCPRpt.AddCell(item.MaxValueThatQualified);
                }

                pdfTableList.Add(tblCPRpt);

                //custom practice report specific end

                //Adding document headers
                document.AddAuthor(strAuthor);
                document.AddTitle(strReportTitle);
                document.AddCreationDate();
                //End adding document headers
                document.Open();

                PdfPTable tbl = new PdfPTable(1);

                //Adding report heading to the first page of the report.
                Paragraph para = new Paragraph(new Phrase(strTitle, PageHeaderFont));
                PdfPCell cellContent = new PdfPCell();
                cellContent.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.Border = iTextSharp.text.Rectangle.NO_BORDER;
                cellContent.AddElement(para);
                tbl.AddCell(cellContent);
                document.Add(tbl);

                if (pdfTableList != null)
                {
                    foreach (PdfPTable pdfTable in pdfTableList)
                    {
                        if (!writer.FitsPage(pdfTable))
                            document.NewPage();

                        document.Add(GetEmptyTable());
                        document.Add(pdfTable);
                    }
                }

                if (!hasData)
                {
                    //no data
                    document.Add(new Paragraph(noDataText));
                }

                document.Close();

                System.Web.HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + strPDFFileName);
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.OutputStream.Write(memStream.GetBuffer(), 0, memStream.GetBuffer().Length);
                System.Web.HttpContext.Current.Response.OutputStream.Flush();
                System.Web.HttpContext.Current.Response.OutputStream.Close();
                System.Web.HttpContext.Current.Response.End();
            }
            finally
            {
                if (memStream != null)
                {
                    memStream.Close();
                    memStream.Dispose();
                }
            }
        }

        private static string GetPeriodData(DataDateReturnType dtRange)
        {
            DateTime startDate = DateTime.Today.AddDays(-90);
            if (dtRange == DataDateReturnType.Last6Months)
            {
                startDate = DateTime.Today.AddDays(-180);
            }
            else if (dtRange == DataDateReturnType.Last9Months)
            {
                startDate = DateTime.Today.AddDays(-270);
            }
            else if (dtRange == DataDateReturnType.LastYear)
            {
                startDate = DateTime.Today.AddYears(-1);
            }
            else if (dtRange == DataDateReturnType.Last2Years)
            {
                startDate = DateTime.Today.AddYears(-2);
            }


            //DateTime startDate = DateTime.Today.AddDays(daterange);
            DateTime endDate = DateTime.Today;

            //string strStartMonth = GRCBase.DateTimeHelper.GetMonthName(startDate.Month);
            string strStartMonth = startDate.ToString("MMMM");
            string strStartDay = startDate.Day.ToString();
            string strStartYear = startDate.Year.ToString();
            //string strEndMonth = GRCBase.DateTimeHelper.GetMonthName(endDate.Month);
            string strEndMonth = endDate.ToString("MMMM");
            string strEndDay = endDate.Day.ToString();
            string strEndYear = endDate.Year.ToString();
            return string.Format("{0} {1}, {2} to {3} {4}, {5}", strStartMonth, strStartDay, strStartYear, strEndMonth, strEndDay, strEndYear);
        }

        #endregion

        #region Error Report

        public static void CreateErrorPDF()
        {
            MemoryStream memStream = null;

            try
            {
                string strPDFFileName = "ERROR_Report.pdf";
                string strTitle = "ERROR CREATING REPORT";
                string strReportTitle = string.Format(
                    "ERROR",
                    GRCBase.DateTimeHelper.GetFormattedDateTimeString(
                        DateTime.Now,
                        AHACommon.DateTimeHelper.GetDateFormatForWeb(),
                        GRCBase.GRCDateSeperator.HYPHEN,
                        GRCBase.GRCTimeFormat.NONE));
                string strAuthor = GRCBase.ConfigReader.GetValue("Author");

                Document document = new Document(PageSize.A4, 22f, 20f, 99f, 60f);
                memStream = new MemoryStream();

                PdfWriter writer = PdfWriter.GetInstance(document, memStream);
                writer.PageEvent = new PDFPageEvent();

                HeaderFooter footer = null;
                footer = new HeaderFooter(_GetDisclaimerPhrase(AHAAppSettings.Disclaimer_Provider_PDF, new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL)), false);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                document.Footer = footer;

                iTextSharp.text.Font PageHeaderFont = new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD);

                //Adding document headers
                document.AddAuthor(strAuthor);
                document.AddTitle(strReportTitle);
                document.AddCreationDate();
                //End adding document headers
                document.Open();

                PdfPTable tbl = new PdfPTable(1);

                //Adding report heading to the first page of the report.
                Paragraph para = new Paragraph(new Phrase(strTitle, PageHeaderFont));
                PdfPCell cellContent = new PdfPCell();
                cellContent.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.Border = iTextSharp.text.Rectangle.NO_BORDER;
                cellContent.AddElement(para);
                tbl.AddCell(cellContent);
                document.Add(tbl);

                document.Close();

                //Push the PDF back at the browser
                System.Web.HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + strPDFFileName);
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.OutputStream.Write(memStream.GetBuffer(), 0, memStream.GetBuffer().Length);
                System.Web.HttpContext.Current.Response.OutputStream.Flush();
                System.Web.HttpContext.Current.Response.OutputStream.Close();
                System.Web.HttpContext.Current.Response.End();
            }
            finally
            {
                if (memStream != null)
                {
                    memStream.Close();
                    memStream.Dispose();
                }
            }
        }

        #endregion

        #region Multiple Data Type Report

        // Create multiple data type PDF Report
        public static void CreateMultipleDataTypeGraphReportPDF(
            List<AHAHelpContent.Patient> objPatients, Graph.GraphGroupType graphsToPlot,
            Graph.GraphGroupType groupForYaxis, AHACommon.DataDateReturnType dateRange, String reportTitle, String reportHeading)
        {
            MemoryStream memStream = null;

            try
            {
                string strPDFFileName = "Multiple_Data_Type_Graph.pdf";
                string strReportTitle = string.Format(
                    reportTitle,
                    GRCBase.DateTimeHelper.GetFormattedDateTimeString(
                        DateTime.Now,
                        AHACommon.DateTimeHelper.GetDateFormatForWeb(),
                        GRCBase.GRCDateSeperator.HYPHEN,
                        GRCBase.GRCTimeFormat.NONE));
                string strAuthor = GRCBase.ConfigReader.GetValue("Author");
                
                Document document = new Document(PageSize.A4, 22f, 20f, 99f, 60f);
                memStream = new MemoryStream();

                PdfWriter writer = PdfWriter.GetInstance(document, memStream);
                writer.PageEvent = new PDFPageEvent();

                HeaderFooter footer = null;
                footer = new HeaderFooter(_GetDisclaimerPhrase(AHAAppSettings.Disclaimer_Provider_PDF, new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL)), false);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                document.Footer = footer;

                iTextSharp.text.Font PageHeaderFont = new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD);

                //Adding document headers
                document.AddAuthor(strAuthor);
                document.AddTitle(strReportTitle);
                document.AddCreationDate();
                //End adding document headers
                document.Open();

                PdfPTable tbl = new PdfPTable(1);

                //Adding report heading to the first page of the report.
                Paragraph para = new Paragraph(new Phrase(reportHeading, PageHeaderFont));
                PdfPCell cellContent = new PdfPCell();
                cellContent.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.Border = iTextSharp.text.Rectangle.NO_BORDER;
                cellContent.AddElement(para);
                tbl.AddCell(cellContent);
                document.Add(tbl);

                //List<iTextSharp.text.Image> listImage = new List<iTextSharp.text.Image>();
                IElement objImage = _GetImage(objPatients, graphsToPlot, groupForYaxis, dateRange);
                if (objImage != null)
                {
                    document.Add(objImage);
                }
                else
                {
                    //no data
                    document.Add(new Paragraph(noDataText));
                }

                document.Close();

                System.Web.HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + strPDFFileName);
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.OutputStream.Write(memStream.GetBuffer(), 0, memStream.GetBuffer().Length);
                System.Web.HttpContext.Current.Response.OutputStream.Flush();
                System.Web.HttpContext.Current.Response.OutputStream.Close();
                System.Web.HttpContext.Current.Response.End();
            }
            finally
            {
                if (memStream != null)
                {
                    memStream.Close();
                    memStream.Dispose();
                }
            }
        }

        #endregion

        #region Standard Organization Report

        /// Creates standard practice report pdf
        public static void CreateSPReportPDF(DataDateReturnType dateRange, string reportTitle, string reportDescription, List<PatientGuids> patientGuidsList)
        {
            MemoryStream memStream = null;

            try
            {
                string strPDFFileName = "Standard_Practice_Report.pdf";
                string strReportTitle = string.Format(
                    reportTitle,
                    GRCBase.DateTimeHelper.GetFormattedDateTimeString(
                        DateTime.Now,
                        AHACommon.DateTimeHelper.GetDateFormatForWeb(),
                        GRCBase.GRCDateSeperator.HYPHEN,
                        GRCBase.GRCTimeFormat.NONE));
                string strAuthor = GRCBase.ConfigReader.GetValue("Author");

                Document document = new Document(PageSize.A4, 22f, 20f, 99f, 60f);
                memStream = new MemoryStream();

                PdfWriter writer = PdfWriter.GetInstance(document, memStream);
                writer.PageEvent = new PDFPageEvent();

                HeaderFooter footer = null;
                footer = new HeaderFooter(_GetDisclaimerPhrase(
                    AHAAppSettings.Disclaimer_Provider_PDF, new iTextSharp.text.Font(
                        iTextSharp.text.Font.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL)), false);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                document.Footer = footer;

                iTextSharp.text.Font PageHeaderFont = new iTextSharp.text.Font(
                    iTextSharp.text.Font.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD);

                //Adding document headers
                document.AddAuthor(strAuthor);
                document.AddTitle(strReportTitle);
                document.AddCreationDate();
                //End adding document headers
                document.Open();

                PdfPTable tbl = new PdfPTable(1);

                //Adding report heading to the first page of the report.
                Paragraph para = new Paragraph(new Phrase(reportDescription, PageHeaderFont));
                PdfPCell cellContent = new PdfPCell();
                cellContent.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.Border = iTextSharp.text.Rectangle.NO_BORDER;
                cellContent.AddElement(para);
                tbl.AddCell(cellContent);
                document.Add(tbl);

                //List<iTextSharp.text.Image> listImage = new List<iTextSharp.text.Image>();

                //Insert the graphs
                bool hasData = false;
                IElement image = PDFReports.GetImage(null, Graph.GraphType.SPRGlucose, dateRange, patientGuidsList);
                if (image == null)
                    image = new Chunk(string.Empty);
                else
                    hasData = true;
                document.Add(image);
                IElement image2 = PDFReports.GetImage(null, Graph.GraphType.SPRCombinedBP, dateRange, patientGuidsList);
                if (image2 == null)
                    image2 = new Chunk(string.Empty);
                else
                    hasData = true;
                document.Add(image2);
                IElement image3 = PDFReports.GetImage(null, Graph.GraphType.SPRCombinedCholesterol, dateRange, patientGuidsList);
                if (image3 == null)
                    image3 = new Chunk(string.Empty);
                else
                    hasData = true;
                document.Add(image3);
                IElement image4 = PDFReports.GetImage(null, Graph.GraphType.SPRWeight, dateRange, patientGuidsList);
                if (image4 == null)
                    image4 = new Chunk(string.Empty);
                else
                    hasData = true;
                document.Add(image4);
                IElement image5 = PDFReports.GetImage(null, Graph.GraphType.SPRExercise, dateRange, patientGuidsList);
                if (image5 == null)
                    image5 = new Chunk(string.Empty);
                else
                    hasData = true;
                document.Add(image5);

                //no data                
                if (!hasData)
                    document.Add(new Paragraph(noDataText));

                document.Close();

                //Push the PDF back at the browser
                System.Web.HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + strPDFFileName);
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.OutputStream.Write(memStream.GetBuffer(), 0, memStream.GetBuffer().Length);
                System.Web.HttpContext.Current.Response.OutputStream.Flush();
                System.Web.HttpContext.Current.Response.OutputStream.Close();
                System.Web.HttpContext.Current.Response.End();
            }
            finally
            {
                if (memStream != null)
                {
                    memStream.Close();
                    memStream.Dispose();
                }
            }
        }

        #endregion

        #region Tracker Report Creation

        public static void CreateReport(PatientGuids objGuids, PDFType type, DataDateReturnType dateReturnType, DataViewType viewType, List<PatientGuids> patientGuidsList)
        {
            MemoryStream memStream = null;

            string strDateRange = GetPeriod(dateReturnType);
            string strReportTitle = GetReportTitle(objGuids, dateReturnType, type);

            try
            {
                Document document = new Document(PageSize.A4, 22f, 20f, 99f, 60f);
                memStream = new MemoryStream();

                List<PdfPTable> pdfTableList = null;
                List<iTextSharp.text.Image> imgList = null;

                PdfWriter writer = PdfWriter.GetInstance(document, memStream);
                writer.PageEvent = new PDFPageEvent();

                HeaderFooter footer = null;
                footer = new HeaderFooter(_GetDisclaimerPhrase(AHAAppSettings.Disclaimer_Provider_PDF, new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL)), false);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                document.Footer = footer;

                iTextSharp.text.Font PageHeaderFont = new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD);

                PDFReportSetting setting = AHAAppSettings.WeightPDFCreationSettings;
                string strAuthor = string.Empty;
                string strTitle = string.Empty;
                string strPDFFileName = string.Empty;

                switch (type)
                {
                    case PDFType.Weight:
                        if (viewType == DataViewType.TableAndChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetWeightGraph(objGuids, dateReturnType, patientGuidsList));

                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetWeightTable(objGuids, dateReturnType));
                        }
                        else if (viewType == DataViewType.Chart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetWeightGraph(objGuids, dateReturnType, patientGuidsList));
                        }
                        else if (viewType == DataViewType.Table)
                        {
                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetWeightTable(objGuids, dateReturnType));
                        }
                        break;

                    case PDFType.BloodPressure:
                        setting = AHAAppSettings.BloodPressurePDFCreationSettings;
                        if (viewType == DataViewType.TableAndChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetSystolicGraph(objGuids, dateReturnType, patientGuidsList));
                            imgList.Add(PDFReports.GetDiastolicGraph(objGuids, dateReturnType, patientGuidsList));

                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetBPTable(objGuids, dateReturnType));
                        }
                        else if (viewType == DataViewType.Chart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetSystolicGraph(objGuids, dateReturnType, patientGuidsList));
                            imgList.Add(PDFReports.GetDiastolicGraph(objGuids, dateReturnType, patientGuidsList));
                        }
                        else if (viewType == DataViewType.Table)
                        {
                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetBPTable(objGuids, dateReturnType));
                        }
                        else if (viewType == DataViewType.CombinedChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetCombinedBPGraph(objGuids, dateReturnType, patientGuidsList));
                        }
                        else if (viewType == DataViewType.TableAndCombinedChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetCombinedBPGraph(objGuids, dateReturnType, patientGuidsList));

                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetBPTable(objGuids, dateReturnType));
                        }
                        break;

                    case PDFType.BloodGlucose:
                        setting = AHAAppSettings.BloodGlucosePDFCreationSettings;

                        PdfPTable tblBGAv = BloodGlucoseWrapper.GetAverageBloodGlucoseForPeriodForPDF(objGuids, dateReturnType);

                        if (tblBGAv != null)
                        {
                            pdfTableList = new List<PdfPTable>();

                            pdfTableList.Add(tblBGAv);
                        }
                        else
                        {
                            pdfTableList = new List<PdfPTable>();
                        }

                        if (viewType == DataViewType.TableAndChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetBloodGlucoseGraph(objGuids, dateReturnType, patientGuidsList));

                            if (pdfTableList.Count == 0)
                            {
                                pdfTableList = new List<PdfPTable>();
                            }
                            pdfTableList.Add(PDFReports.GetBloodGlucoseTable(objGuids, dateReturnType, UXControl.EnglishLanguage));
                        }
                        else if (viewType == DataViewType.Chart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetBloodGlucoseGraph(objGuids, dateReturnType, patientGuidsList));
                        }
                        else if (viewType == DataViewType.Table)
                        {
                            if (pdfTableList.Count == 0)
                            {
                                pdfTableList = new List<PdfPTable>();
                            }
                            pdfTableList.Add(PDFReports.GetBloodGlucoseTable(objGuids, dateReturnType, UXControl.EnglishLanguage));
                        }
                        break;

                    case PDFType.Cholesterol:
                        setting = AHAAppSettings.CholesterolPDFCreationSettings;
                        if (viewType == DataViewType.TableAndChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            iTextSharp.text.Image imgGraph = PDFReports.GetTotalCholesterolGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetHDLGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetLDLGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetTriglyceridesGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetCholesterolTable(objGuids, dateReturnType));
                        }
                        else if (viewType == DataViewType.Chart)
                        {
                            imgList = new List<iTextSharp.text.Image>();

                            iTextSharp.text.Image imgGraph = PDFReports.GetTotalCholesterolGraph(objGuids, dateReturnType, patientGuidsList);

                            imgGraph = PDFReports.GetTotalCholesterolGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetHDLGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetLDLGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetTriglyceridesGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);
                        }
                        else if (viewType == DataViewType.Table)
                        {
                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetCholesterolTable(objGuids, dateReturnType));
                        }
                        else if (viewType == DataViewType.CombinedChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();

                            iTextSharp.text.Image imgGraph = PDFReports.GetCombinedCholesterolGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetTriglyceridesGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);
                        }
                        else if (viewType == DataViewType.TableAndCombinedChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            iTextSharp.text.Image imgGraph = PDFReports.GetCombinedCholesterolGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            imgGraph = PDFReports.GetTriglyceridesGraph(objGuids, dateReturnType, patientGuidsList);
                            if (imgGraph.Alt != PDFHelper.EmptyImage)
                                imgList.Add(imgGraph);

                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetCholesterolTable(objGuids, dateReturnType));
                        }
                        break;

                    case PDFType.CurrentMedication:
                        setting = AHAAppSettings.CurrentMedicationPDFCreationSettings;
                        if (viewType == DataViewType.Table)
                        {
                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetMedicationTable(objGuids, dateReturnType, true));
                        }
                        break;

                    case PDFType.DiscontinuedMedication:
                        setting = AHAAppSettings.DiscontinuedMedicationPDFCreationSettings;
                        if (viewType == DataViewType.Table)
                        {
                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetMedicationTable(objGuids, dateReturnType, false));
                        }
                        break;

                    case PDFType.Exercise:
                        setting = AHAAppSettings.ExercisePDFCreationSettings;
                        if (viewType == DataViewType.TableAndChart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetExerciseGraph(objGuids, dateReturnType, patientGuidsList));

                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetExerciseTable(objGuids, dateReturnType));
                        }
                        else if (viewType == DataViewType.Chart)
                        {
                            imgList = new List<iTextSharp.text.Image>();
                            imgList.Add(PDFReports.GetExerciseGraph(objGuids, dateReturnType, patientGuidsList));
                        }
                        else if (viewType == DataViewType.Table)
                        {
                            pdfTableList = new List<PdfPTable>();
                            pdfTableList.Add(PDFReports.GetExerciseTable(objGuids, dateReturnType));
                        }
                        break;
                }

                strAuthor = GetActualValue(objGuids, setting.Author, dateReturnType, type);
                strTitle = GetActualValue(objGuids, setting.Title, dateReturnType, type);
                strPDFFileName = GetActualValue(objGuids, setting.PDFReportFileName, dateReturnType, type);

                //Adding document headers
                document.AddAuthor(strAuthor);
                document.AddTitle(strReportTitle);
                document.AddCreationDate();
                //End adding document headers
                document.Open();

                bool bCheckContentFits = false;

                PdfPTable tbl = new PdfPTable(1);

                //Adding report heading to the first page of the report.
                Paragraph para = new Paragraph(new Phrase(strTitle, PageHeaderFont));
                PdfPCell cellContent = new PdfPCell();
                cellContent.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
                cellContent.Border = iTextSharp.text.Rectangle.NO_BORDER;
                cellContent.AddElement(para);
                tbl.AddCell(cellContent);
                document.Add(tbl);


                if (imgList != null)
                {
                    bCheckContentFits = true;
                    foreach (iTextSharp.text.Image img in imgList)
                    {
                        document.Add(GetEmptyTable());

                        PdfPTable tblImg = new PdfPTable(1);
                        PdfPCell cellImg = new PdfPCell();
                        cellImg.AddElement(img);
                        tblImg.AddCell(cellImg);

                        if (!writer.FitsPage(tblImg))
                        {
                            document.NewPage();
                        }

                        document.Add(tblImg);
                    }
                }


                if (pdfTableList != null)
                {
                    foreach (PdfPTable pdfTable in pdfTableList)
                    {
                        if (!writer.FitsPage(pdfTable) && bCheckContentFits)
                        {
                            document.NewPage();
                        }

                        document.Add(GetEmptyTable());

                        document.Add(pdfTable);

                        bCheckContentFits = true;
                    }
                }


                document.Close();

                System.Web.HttpContext.Current.Response.AddHeader("Content-disposition", "attachment; filename=" + strPDFFileName);
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.Clear();
                System.Web.HttpContext.Current.Response.OutputStream.Write(memStream.GetBuffer(), 0, memStream.GetBuffer().Length);
                System.Web.HttpContext.Current.Response.OutputStream.Flush();
                System.Web.HttpContext.Current.Response.OutputStream.Close();
                System.Web.HttpContext.Current.Response.End();
            }
            finally
            {
                if (memStream != null)
                {
                    memStream.Close();
                    memStream.Dispose();
                }
            }
        }

        #endregion

        #region Supporting Methods

        /* ***** SUPPORTING METHODS ***** */

        private static Phrase _GetDisclaimerPhrase(string strDisclaimer, iTextSharp.text.Font font)
        {
            List<string> DisclaimerTextParts = strDisclaimer.Split(new string[] { "##REGISTERED##" }, StringSplitOptions.None).ToList();

            Phrase phrase = new Phrase();
            Chunk chunk = null;
            int iTextIndex = 1;

            iTextSharp.text.Font registeredFont = new iTextSharp.text.Font(font);
            registeredFont.Size = 6;

            foreach (string strDisclaimerTextPart in DisclaimerTextParts)
            {
                chunk = new Chunk(strDisclaimerTextPart, font);
                phrase.Add(chunk);

                if (iTextIndex < DisclaimerTextParts.Count)
                {
                    //chunk = new Chunk("Heart360", font);
                    //phrase.Add(chunk);

                    chunk = new Chunk("®", registeredFont);
                    chunk.SetTextRise(4.5f);
                    phrase.Add(chunk);
                }

                iTextIndex++;
            }

            return phrase;
        }

        public static PdfPTable GetPageHeadingTable(Phrase objPhrase)
        {
            PdfPTable tbl = new PdfPTable(1);
            Paragraph para = new Paragraph(objPhrase);
            PdfPCell cellContent = new PdfPCell();
            cellContent.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
            cellContent.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
            cellContent.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellContent.AddElement(para);
            cellContent.AddElement(Chunk.NEWLINE);
            tbl.AddCell(cellContent);

            return tbl;
        }

        public static PdfPTable GetEmptyTable()
        {
            PdfPTable tbl = new PdfPTable(1);
            PdfPCell cellContent = new PdfPCell();
            cellContent.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
            cellContent.VerticalAlignment = iTextSharp.text.Rectangle.ALIGN_MIDDLE;
            cellContent.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellContent.AddElement(Chunk.NEWLINE);
            tbl.AddCell(cellContent);

            return tbl;
        }

        public static PdfPTable GetPageHeaderTable()
        {
            //Adding header image to the first page of the report.
            PdfPTable tbl = new PdfPTable(1);

            PdfPCell cellImage = new PdfPCell();
            cellImage.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cellImage.AddElement(PDFHelper.GetPageHeaderImage());
            tbl.AddCell(cellImage);
            //End adding header image to the first page of the report.    

            //Adding blank spaces after the header image to the first page of the report.
            PdfPCell cellBlank = new PdfPCell();
            cellBlank.Border = iTextSharp.text.Rectangle.NO_BORDER;
            tbl.AddCell(cellBlank);
            //End adding blank spaces after the header image to the first page of the report.

            return tbl;
        }

        /// <summary>
        /// Returns the heading image to be displayed on the first page of report.
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetPageHeaderImage()
        {
            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(GRCBase.ConfigReader.GetValue("PDFReportHeaderImagePath"));
            img.SetAbsolutePosition(0, 0);
            img.ScalePercent(75);
            return img;
        }
        
        // PJB - Added objGuids so routine doesn't look at url request parameters.
        private static string GetActualValue( PatientGuids objGuids, string strConfigValue, DataDateReturnType dateReturnType, PDFType pdfType)
        {
            string strReturnValue = string.Empty;
            if (strConfigValue.Contains("#~"))
            {
                char[] strDelimiter = { '#' };
                string[] strArray = strConfigValue.Split(strDelimiter, StringSplitOptions.RemoveEmptyEntries);

                foreach (string str in strArray)
                {
                    switch (str)
                    {
                        case "~FirstName~":                       
                            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId( objGuids.PersonID, objGuids.RecordID );
                            HVManager.PersonalDataManager.PersonalItem obj = mgr.PersonalDataManager.Item;
                            if (obj != null)
                            {
                                strReturnValue += obj.FullName;
                            }                  
                            break;
                        case "~Period~":
                            strReturnValue += GetPeriod(dateReturnType);
                            break;
                        case "~ReportTitle~":
                            strReturnValue += GetReportTitle( objGuids, dateReturnType, pdfType);
                            break;
                        default:
                            strReturnValue += str;
                            break;
                    }
                }
                return strReturnValue;
            }
            return strReturnValue = strConfigValue;
        }
  
        private static string GetPeriod(DataDateReturnType dateReturnType)
        {
            if (dateReturnType == DataDateReturnType.Last3Months)
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_Duration_3Months").ToString();
            }
            else if (dateReturnType == DataDateReturnType.Last6Months)
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_Duration_6Months").ToString();
            }
            else if (dateReturnType == DataDateReturnType.Last9Months)
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_Duration_9Months").ToString();
            }
            else if (dateReturnType == DataDateReturnType.LastYear)
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_Duration_1Year").ToString();
            }
            else if (dateReturnType == DataDateReturnType.Last2Years)
            {
                return System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_Duration_2Years").ToString();
            }
            return string.Empty;
        }

 
        private static string GetReportTitle( PatientGuids patGuids, DataDateReturnType dateReturnType, PDFType pdfType)
        {
            string strType = string.Empty;
            switch (pdfType)
            {
                case PDFType.Weight:
                    strType = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_Weight").ToString().ToLower();
                    break;
                case PDFType.BloodGlucose:
                    strType = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BloodGlucose").ToString().ToLower();
                    break;
                case PDFType.BloodPressure:
                    strType = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_BloodPressure").ToString().ToLower();
                    break;
                case PDFType.Cholesterol:
                    strType = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_Cholesterol").ToString().ToLower();
                    break;
                case PDFType.CurrentMedication:
                    strType = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_CurrMed").ToString();
                    break;
                case PDFType.DiscontinuedMedication:
                    strType = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_DisConMed").ToString();
                    break;
                case PDFType.Exercise:
                    strType = System.Web.HttpContext.GetGlobalResourceObject("Reports", "Text_PhysicalActivity").ToString().ToLower();
                    break;
            }

            string strName = string.Empty;           
            HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId( patGuids.PersonID, patGuids.RecordID );
            HVManager.PersonalDataManager.PersonalItem obj = mgr.PersonalDataManager.Item;
            if (obj != null)
            {
                strName = obj.FullName;
            }
            
            if (dateReturnType == DataDateReturnType.Last3Months)
            {
                //return "Last three months " + strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_ThreeMonthReport").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            else if (dateReturnType == DataDateReturnType.Last6Months)
            {
                //return "Last six months " + strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_SixMonthReport").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            else if (dateReturnType == DataDateReturnType.LastMonth)
            {
                //return "Last month " + strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_LastMonthReport").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            else if (dateReturnType == DataDateReturnType.LastWeek)
            {
                //return "Last week " + strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_LastWeekReport").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            else if (dateReturnType == DataDateReturnType.Last9Months)
            {
                //return "Last nine months " + strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_NineMonthReport").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            else if (dateReturnType == DataDateReturnType.LastYear)
            {
                //return "Last year " + strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_LastYearReport").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            else if (dateReturnType == DataDateReturnType.Last2Years)
            {
                //return "Last two years " + strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_LastTwoYearReport").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            else if (dateReturnType == DataDateReturnType.AllData)
            {
                //return strType + " report for " + strName + " generated on " + GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE);
                return string.Format(System.Web.HttpContext.GetGlobalResourceObject("Reports", "Patient_Report_AllData").ToString(), strType, strName, GRCBase.DateTimeHelper.GetFormattedDateTimeString(DateTime.Now, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
            }
            return string.Empty;
        }

        private static iTextSharp.text.Image _GetImage(List<AHAHelpContent.Patient> objPatients, Graph.GraphGroupType graphsToPlot, Graph.GraphGroupType groupForYaxis, AHACommon.DataDateReturnType dateRange)
        {
            System.IO.MemoryStream memstream = null;
            try
            {

                Graph.AHAPlotData ahaPlotData = Graph.AHAPlotData.ConstructPlotData(
                objPatients,
                graphsToPlot,
                groupForYaxis,
                dateRange);

                bool bHasValue = false;
                ahaPlotData.m_graphsToPlot.ForEach(delegate(Graph.AHAGraphData objGraphData)
                {
                    if (objGraphData.m_readingList.Count != 0)
                    {
                        bHasValue = true;
                    }
                });

                if (!bHasValue)
                    return null;

                NPlot.Bitmap.PlotSurface2D bm = null;

                bm = new NPlot.Bitmap.PlotSurface2D(Graph.AHAGraphGenerator.reportGraphWidth, Graph.AHAGraphGenerator.reportGraphHeight);

                Graph.AHAGraphGenerator graphGenerator = new Graph.AHAGraphGenerator(ahaPlotData, bm);
                graphGenerator.DrawGraph();
                memstream = bm.ToStream(System.Drawing.Imaging.ImageFormat.Png);
                iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(memstream.ToArray());
                img1.ScalePercent(75);
                img1.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                return img1;
            }
            finally
            {
                if (memstream != null)
                {
                    memstream.Close();
                    memstream.Dispose();
                }
            }
        }
    }


    #endregion

    #region PDFReports (Supporting)

    public class PDFReports
    {
        private static string _getDataForDisplay(string strVal)
        {
            if (!String.IsNullOrEmpty(strVal) && strVal != "-")
            {
                return strVal;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Creating PDF data for personal information
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetEmergencyContact()
        {
            HVManager.EmergencyContactDataManager.EmergencyContactItem ecItem = CurrentUser.HVManagerPatientBase().EmergencyContactDataManager.Item;
            PdfPTable tblEmergencyInfo = new PdfPTable(2);
            int iRowNumber = 0;

            if (ecItem != null)
            {
                string strName = string.Empty;
                if (!String.IsNullOrEmpty(ecItem.FirstName.Value))
                    strName = ecItem.FirstName.Value + " ";
                if (!String.IsNullOrEmpty(ecItem.LastName.Value))
                    strName += ecItem.LastName.Value;

                if (!String.IsNullOrEmpty(strName))
                {
                    tblEmergencyInfo.AddCell(HttpContext.GetGlobalResourceObject("Common", "Text_Name").ToString());
                    tblEmergencyInfo.AddCell(strName);
                    iRowNumber++;
                }

                StringBuilder sbAddress = new StringBuilder();
                sbAddress.Append(_getDataForDisplay(ecItem.StreetAddress.Value));
                if (ecItem.StreetAddress.Value != null && _getDataForDisplay(ecItem.StreetAddress.Value).Length > 0)
                {
                    sbAddress.Append("\r\n");
                }

                sbAddress.Append(_getDataForDisplay(ecItem.City.Value));

                if (_getDataForDisplay(ecItem.City.Value) != null && _getDataForDisplay(ecItem.City.Value).Length > 0)
                {
                    sbAddress.Append("\r\n");
                }

                if (!String.IsNullOrEmpty(_getDataForDisplay(ecItem.State.Value)))
                {
                    sbAddress.Append(HVManager.VocabHelper.GetDisplayTextFromCode(_getDataForDisplay(ecItem.State.Value), HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_STATES_KEY, HVManager.VocabHelper.STR_FAMILY_WC)));
                }

                if (_getDataForDisplay(ecItem.State.Value) != null && _getDataForDisplay(ecItem.State.Value).Length > 0)
                {
                    sbAddress.Append("\r\n");
                }

                sbAddress.Append(_getDataForDisplay(ecItem.Zip.Value));

                if (_getDataForDisplay(ecItem.Zip.Value) != null && _getDataForDisplay(ecItem.Zip.Value).Length > 0)
                {
                    sbAddress.Append("\r\n");
                }

                if (!String.IsNullOrEmpty(_getDataForDisplay(ecItem.Country.Value)))
                {
                    sbAddress.Append(HVManager.VocabHelper.GetDisplayTextFromCode(_getDataForDisplay(ecItem.Country.Value), HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_COUNTRY_KEY, HVManager.VocabHelper.STR_FAMILY_ISO, HVManager.VocabHelper.STR_VERSION_1_0)));
                }

                if (!String.IsNullOrEmpty(sbAddress.ToString()))
                {
                    tblEmergencyInfo.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Address").ToString());
                    tblEmergencyInfo.AddCell(sbAddress.ToString());
                    iRowNumber++;
                }

                if (!String.IsNullOrEmpty(ecItem.Phone.Value))
                {
                    tblEmergencyInfo.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_ContactPhoneNumber").ToString());
                    tblEmergencyInfo.AddCell(GRCBase.StringHelper.GetFormattedUSPhoneNumber(ecItem.Phone.Value));
                    iRowNumber++;
                }
            }
            return tblEmergencyInfo;
        }

        /// <summary>
        /// Creating PDF data for personal information
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetPersonalInformation(int intLanguageId)
        {
            HVManager.PersonalDataManager.PersonalItem personalItem = CurrentUser.HVManagerPatientBase().PersonalDataManager.Item;
            HVManager.PersonalContactDataManager.ContactItem contactItem = CurrentUser.HVManagerPatientBase().PersonalContactDataManager.Item;
            HVManager.BasicDataManager.BasicItem basicItem = CurrentUser.HVManagerPatientBase().BasicDataManager.Item;
            HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem = CurrentUser.HVManagerPatientBase().ExtendedProfileDataManager.Item;

            PdfPTable tblPersonalInfo = new PdfPTable(2);
            int iRowNumber = 0;

            if (personalItem != null)
            {
                string strName = string.Empty;
                if (!String.IsNullOrEmpty(personalItem.Title.Value))
                {
                    string strTitle = AHAHelpContent.ListItem.FindListItemNameByListItemCode(personalItem.Title.Value, AHADefs.VocabDefs.NamePrefixes, UXControl.EnglishLanguage);
                    strName = !string.IsNullOrEmpty(strTitle) ? strTitle + " " : HVManager.VocabHelper.GetDisplayTextFromCode(personalItem.Title.Value, HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_NAME_PREFIXES_KEY, HVManager.VocabHelper.STR_FAMILY_WC)) + " ";
                }
                if (!String.IsNullOrEmpty(personalItem.FirstName.Value))
                    strName += personalItem.FirstName.Value + " ";
                if (!String.IsNullOrEmpty(personalItem.LastName.Value))
                    strName += personalItem.LastName.Value + " ";
                if (!String.IsNullOrEmpty(personalItem.NameSuffix.Value))
                {
                    string strNameSuffix = AHAHelpContent.ListItem.FindListItemNameByListItemCode(personalItem.NameSuffix.Value, AHADefs.VocabDefs.NameSuffixes, UXControl.EnglishLanguage);
                    strName += !string.IsNullOrEmpty(strNameSuffix) ? strNameSuffix : HVManager.VocabHelper.GetDisplayTextFromCode(personalItem.NameSuffix.Value, HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_NAME_SUFFIXES_KEY, HVManager.VocabHelper.STR_FAMILY_WC));
                }
                if (!String.IsNullOrEmpty(strName))
                {
                    tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Common", "Text_Name").ToString());
                    tblPersonalInfo.AddCell(strName);
                    iRowNumber++;
                }
            }

            if (CurrentUser.Address != null)
            {
                StringBuilder sbAddress = new StringBuilder();

                if (CurrentUser.Address.StreetList.Count > 0 && !string.IsNullOrEmpty(CurrentUser.Address.StreetList[0].Value))
                {
                    sbAddress.Append(CurrentUser.Address.StreetList[0].Value);
                    sbAddress.Append("\r\n");
                }

                sbAddress.Append(CurrentUser.Address.City.Value);

                if (CurrentUser.Address.City.Value != null && CurrentUser.Address.City.Value.Length > 0)
                {
                    sbAddress.Append("\r\n");
                }

                if (!String.IsNullOrEmpty(CurrentUser.Address.State.Value))
                {
                    string strState = AHAHelpContent.ListItem.FindListItemNameByListItemCode(CurrentUser.Address.State.Value, AHADefs.VocabDefs.States, UXControl.EnglishLanguage);
                    sbAddress.Append(!string.IsNullOrEmpty(strState) ? strState : HVManager.VocabHelper.GetDisplayTextFromCode(CurrentUser.Address.State.Value, HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_STATES_KEY, HVManager.VocabHelper.STR_FAMILY_WC)));
                }

                if (CurrentUser.Address.State.Value != null && CurrentUser.Address.State.Value.Length > 0)
                {
                    sbAddress.Append("\r\n");
                }

                sbAddress.Append(CurrentUser.Address.Zip.Value);

                if (CurrentUser.Address.Zip.Value != null && CurrentUser.Address.Zip.Value.Length > 0)
                {
                    sbAddress.Append("\r\n");
                }

                if (!String.IsNullOrEmpty(CurrentUser.Address.Country.Value))
                {
                    string strCountry = AHAHelpContent.ListItem.FindListItemNameByListItemCode(CurrentUser.Address.Country.Value, AHADefs.VocabDefs.Countries, UXControl.EnglishLanguage);
                    sbAddress.Append(!string.IsNullOrEmpty(strCountry) ? strCountry : HVManager.VocabHelper.GetDisplayTextFromCode(CurrentUser.Address.Country.Value, HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_COUNTRY_KEY, HVManager.VocabHelper.STR_FAMILY_ISO, HVManager.VocabHelper.STR_VERSION_1_0)));
                }

                if (!String.IsNullOrEmpty(sbAddress.ToString()))
                {
                    tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Address").ToString());
                    tblPersonalInfo.AddCell(sbAddress.ToString());
                    iRowNumber++;
                }
            }

            if (personalItem != null && personalItem.DateOfBirth.Value.HasValue)
            {
                tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Common", "Text_DateOfBirth").ToString());
                tblPersonalInfo.AddCell(GRCBase.DateTimeHelper.GetFormattedDateTimeString(personalItem.DateOfBirth.Value.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
                iRowNumber++;
            }

            if (basicItem != null)
            {
                string strGender = string.Empty;
                if (basicItem.GenderOfPerson.Value.HasValue && basicItem.GenderOfPerson.Value.Value == Gender.Female)
                    strGender = HttpContext.GetGlobalResourceObject("Common", "Text_Female").ToString();
                else if (basicItem.GenderOfPerson.Value.HasValue && basicItem.GenderOfPerson.Value.Value == Gender.Male)
                    strGender = HttpContext.GetGlobalResourceObject("Common", "Text_Male").ToString();

                if (!String.IsNullOrEmpty(strGender))
                {
                    tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Common", "Text_Gender").ToString());
                    tblPersonalInfo.AddCell(strGender);
                    iRowNumber++;
                }
            }

            if (personalItem != null && !String.IsNullOrEmpty(personalItem.Ethnicity.Value))
            {
                tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Ethnicity").ToString());
                string strEthnicity = AHAHelpContent.ListItem.FindListItemNameByListItemCode(personalItem.Ethnicity.Value, AHADefs.VocabDefs.Ethnicity, intLanguageId);
                tblPersonalInfo.AddCell(!string.IsNullOrEmpty(strEthnicity) ? strEthnicity : HVManager.VocabHelper.GetDisplayTextFromCode(personalItem.Ethnicity.Value, HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_ETHNICITY_KEY, HVManager.VocabHelper.STR_FAMILY_WC)));
                iRowNumber++;
            }

            if (personalItem != null && !String.IsNullOrEmpty(personalItem.MaritalStatus.Value))
            {
                tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Relationship").ToString());
                string strMaritalStatus = AHAHelpContent.ListItem.FindListItemNameByListItemCode(personalItem.MaritalStatus.Value, AHADefs.VocabDefs.RelationshipStatus, intLanguageId);
                tblPersonalInfo.AddCell(!string.IsNullOrEmpty(strMaritalStatus) ? strMaritalStatus : HVManager.VocabHelper.GetDisplayTextFromCode(personalItem.MaritalStatus.Value, HVManager.VocabHelper.GetVocabularyList(HVManager.VocabHelper.STR_MARITAL_STATUS_KEY, HVManager.VocabHelper.STR_FAMILY_WC)));
                iRowNumber++;
            }

            if (epItem != null && !String.IsNullOrEmpty(epItem.IncomeLevel.Value))
            {
                tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_IncomeLevel").ToString());
                tblPersonalInfo.AddCell(epItem.IncomeLevel.Value);
                iRowNumber++;
            }

            if (epItem != null && epItem.HasHealthInsurance.Value.HasValue)
            {
                tblPersonalInfo.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_MedicalInsurance").ToString());
                tblPersonalInfo.AddCell(epItem.HasHealthInsurance.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                iRowNumber++;
            }

            return tblPersonalInfo;
        }

        /// <summary>
        /// Creating PDF data for health history
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetHealthHistory(int intLanguageId)
        {
            HVManager.AllItemManager hvMgr = CurrentUser.HVManagerPatientBase();

            HVManager.PersonalDataManager.PersonalItem personalItem = hvMgr.PersonalDataManager.Item;
            HVManager.ExtendedProfileDataManager.ExtendedProfileItem epItem = hvMgr.ExtendedProfileDataManager.Item;
            HVManager.CardiacDataManager.CardiacItem cardiacItem = hvMgr.CardiacDataManager.Item;
            HVManager.HeightDataManager.HeightItem heightItem = HeightWrapper.GetLatestHeightItem(hvMgr);
            HVManager.WeightDataManager.WeightItem weightItem = MyWeightWrapper.GetLatestWeight(hvMgr);
            HVManager.BloodPressureDataManager.BPItem bpItem = BloodPressureWrapper.GetLatestBP(hvMgr);

            PdfPTable tblHealthHistory = new PdfPTable(2);
            int iRowNumber = 0;
            if (heightItem != null)
            {
                string strHeightInFeet = string.Empty;
                string strHeightInInches = string.Empty;

                if (heightItem.HeightInFeet > 0)
                {
                    strHeightInFeet = heightItem.HeightInFeet.ToString() + " " + HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Feet").ToString() + " ";
                }
                if (heightItem.HeightInInches > 0)
                {
                    strHeightInInches = heightItem.HeightInInches.ToString() + " " + HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Inches").ToString() + " ";
                }

                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Height").ToString());
                tblHealthHistory.AddCell(strHeightInFeet + strHeightInInches);
                iRowNumber++;
            }

            if (weightItem != null)
            {
                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "Text_WeightInPounds").ToString());
                tblHealthHistory.AddCell(Math.Round(weightItem.CommonWeight.Value.ToPounds(), 2).ToString());
                iRowNumber++;
            }

            if (bpItem != null)
            {
                string strSystolic = string.Empty;
                string strDiastolic = string.Empty;

                if (bpItem.Systolic.Value > 0)
                {
                    strSystolic = bpItem.Systolic.Value.ToString() + " " + HttpContext.GetGlobalResourceObject("Common", "Text_Systolic").ToString() + " ";
                }
                if (bpItem.Diastolic.Value > 0)
                {
                    strDiastolic = bpItem.Diastolic.Value.ToString() + " " + HttpContext.GetGlobalResourceObject("Common", "Text_Diastolic").ToString() + " ";
                }

                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "Text_BloodPressure").ToString());
                tblHealthHistory.AddCell(strSystolic + strDiastolic);
                iRowNumber++;
            }

            if (personalItem != null && !String.IsNullOrEmpty(personalItem.BloodType.Value))
            {
                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_BloodType").ToString());
                string strBloodType = AHAHelpContent.ListItem.FindListItemNameByListItemCode(personalItem.BloodType.Value, AHADefs.VocabDefs.BloodType, intLanguageId);
                tblHealthHistory.AddCell(!string.IsNullOrEmpty(strBloodType) ? strBloodType : personalItem.BloodType.Value);
                iRowNumber++;
            }

            if (epItem != null && !String.IsNullOrEmpty(epItem.Allergies.Value))
            {
                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Allergies").ToString());
                tblHealthHistory.AddCell(epItem.Allergies.Value);
                iRowNumber++;
            }

            if (cardiacItem != null)
            {
                if (cardiacItem.IsOnHypertensionMedication.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_TakingBPMedication").ToString());
                    tblHealthHistory.AddCell(cardiacItem.IsOnHypertensionMedication.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }

                if (cardiacItem.IsOnHypertensionDiet.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "PDF_HypertensionDiet").ToString());
                    tblHealthHistory.AddCell(cardiacItem.IsOnHypertensionDiet.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }

                if (cardiacItem.HasRenalFailureBeenDiagnosed.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_KidneyDisease").ToString());
                    tblHealthHistory.AddCell(cardiacItem.HasRenalFailureBeenDiagnosed.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }

                if (cardiacItem.HasDiabetesBeenDiagnosed.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Diabetes").ToString());
                    tblHealthHistory.AddCell(cardiacItem.HasDiabetesBeenDiagnosed.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }

                if (cardiacItem.HasFamilyHeartDiseaseHistory.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "PDF_FamilyHistoryOfHeartDisease").ToString());
                    tblHealthHistory.AddCell(cardiacItem.HasFamilyHeartDiseaseHistory.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }

                if (cardiacItem.HasPersonalHeartDiseaseHistory.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_DiagnoisedHeartDisease").ToString());
                    tblHealthHistory.AddCell(cardiacItem.HasPersonalHeartDiseaseHistory.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }

                if (cardiacItem.HasFamilyStrokeHistory.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "PDF_FamilyHistoryOfStroke").ToString());
                    tblHealthHistory.AddCell(cardiacItem.HasFamilyStrokeHistory.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }

                if (cardiacItem.HasPersonalStrokeHistory.Value.HasValue)
                {
                    tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "PDF_HadStroke").ToString());
                    tblHealthHistory.AddCell(cardiacItem.HasPersonalStrokeHistory.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                    iRowNumber++;
                }
            }

            if (epItem != null && epItem.IsSmokeCigarette.Value.HasValue)
            {
                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_DoYouSmoke").ToString());
                tblHealthHistory.AddCell(epItem.IsSmokeCigarette.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                iRowNumber++;
            }

            if (epItem != null && epItem.HasTriedToQuitSmoking.Value.HasValue)
            {
                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_TriedToQuitSmoking").ToString());
                tblHealthHistory.AddCell(epItem.HasTriedToQuitSmoking.Value.Value ? HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_Yes").ToString() : HttpContext.GetGlobalResourceObject("Patient1", "Patient_Profile_No").ToString());
                iRowNumber++;
            }

            if (epItem != null && !String.IsNullOrEmpty(epItem.TravelTimeToWork.Value))
            {
                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "PDF_TravelTimeToWork").ToString());
                tblHealthHistory.AddCell(epItem.TravelTimeToWork.Value);
                iRowNumber++;
            }

            if (epItem != null && !String.IsNullOrEmpty(epItem.AttitudeTowardsHealthCare.Value))
            {
                tblHealthHistory.AddCell(HttpContext.GetGlobalResourceObject("Common", "PDF_AttitudeToHealthCare").ToString());
                tblHealthHistory.AddCell(epItem.AttitudeTowardsHealthCare.Value);
                iRowNumber++;
            }
            return tblHealthHistory;
        }

        /// <summary>
        /// Creating PDF data for BP table
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetBPTable(PatientGuids objGuids, DataDateReturnType DateRange)
        {
            PdfPTable tblBP = new PdfPTable(6);

            if (objGuids != null)
            {
                //AHAAPI.Provider.ProviderBase objPage = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                List<HVManager.BloodPressureDataManager.BPItem> objBP = mgr.BloodPressureDataManager.GetDataBetweenDates(DateRange).OrderByDescending(i => i.EffectiveDate).ToList();

                if (objBP != null && objBP.Count > 0)
                {
                    tblBP.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Date").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBP.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Heart_Rate").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBP.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_SystolicWithUnits").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBP.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_DiastolicWithUnits").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBP.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_ReadingSource").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBP.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Comments").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));

                    foreach (HVManager.BloodPressureDataManager.BPItem item in objBP)
                    {
                        tblBP.AddCell(GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.HHMMAMPM));

                        if (item.HeartRate != null)
                            tblBP.AddCell(item.HeartRate.Value.ToString());
                        else
                            tblBP.AddCell(string.Empty);

                        tblBP.AddCell(item.Systolic.Value.ToString());
                        tblBP.AddCell(item.Diastolic.Value.ToString());
                        tblBP.AddCell(item.Source.Value);
                        tblBP.AddCell(item.Note.Value);
                    }
                }
            }
            return tblBP;
        }


        private static iTextSharp.text.Image _GetImage(PatientGuids objGuids, Graph.GraphType graphType, AHACommon.DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            System.IO.MemoryStream memstream = null;
            try
            {

                Graph.AHAPlotData ahaPlotData = Graph.AHAPlotData.ConstructPlotData(objGuids, graphType, DateRange, patientGuidsList);

                bool bHasValue = false;
                ahaPlotData.m_graphsToPlot.ForEach(delegate(Graph.AHAGraphData objGraphData)
                {
                    if (objGraphData.m_readingList.Count != 0)
                    {
                        bHasValue = true;
                    }
                });

                if (!bHasValue)
                {
                    iTextSharp.text.Image imgBlank = iTextSharp.text.Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("/library/images/clear.gif"));
                    imgBlank.Alt = PDFHelper.EmptyImage;
                    return imgBlank;

                    //return null;
                }

                NPlot.Bitmap.PlotSurface2D bm = null;

                if (graphType != Graph.GraphType.CombinedCholesterol)
                {
                    bm = new NPlot.Bitmap.PlotSurface2D(Graph.AHAGraphGenerator.reportGraphWidth, Graph.AHAGraphGenerator.reportGraphHeight);
                }
                else
                {
                    bm = new NPlot.Bitmap.PlotSurface2D(Graph.AHAGraphGenerator.reportGraphWidth, (int)(Graph.AHAGraphGenerator.reportGraphHeight * 1.5));
                }
                Graph.AHAGraphGenerator graphGenerator = new Graph.AHAGraphGenerator(ahaPlotData, bm);
                graphGenerator.DrawGraph();
                memstream = bm.ToStream(System.Drawing.Imaging.ImageFormat.Png);
                iTextSharp.text.Image img1 = iTextSharp.text.Image.GetInstance(memstream.ToArray());
                img1.ScalePercent(75);
                img1.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                return img1;
            }
            finally
            {
                if (memstream != null)
                {
                    memstream.Close();
                    memstream.Dispose();
                }
            }
        }

        /// <summary>
        /// Creating PDF image for BP Systolic
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetSystolicGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.PressureSystolic, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF image for BP Systolic
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetDiastolicGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.PressureDiastolic, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF image for combined graph of BP
        /// </summary>
        /// <param name="DateRange"></param>
        /// <returns></returns>
        public static iTextSharp.text.Image GetCombinedBPGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.CombinedBP, DateRange, patientGuidsList);
        }

        public static double? GetBMIForCurrentRecordForWeight(PatientGuids objGuids, double weight)
        {
            if (objGuids != null)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.HeightDataManager.HeightItem> objH = mgr.HeightDataManager.GetLatestNItems(1);
                if (objH != null && objH.Count > 0)
                {
                    HVManager.HeightDataManager.HeightItem objHeight = objH.First.Value as HVManager.HeightDataManager.HeightItem;
                    double dHeightInInches = HVManager.UnitConversion.MetersToInches(objHeight.HeightInMeters.Value);
                    double bmi = (weight * 703) / ((dHeightInInches) * (dHeightInInches));
                    return bmi;

                }
            }
            return null;
        }

        /// <summary>
        /// Creating PDF data for weight table
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetWeightTable(PatientGuids objGuids, DataDateReturnType DateRange)
        {
            PdfPTable tblWeight = new PdfPTable(5);

            if (objGuids != null)
            {
                //AHAAPI.Provider.ProviderBase objPage = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                List<HVManager.WeightDataManager.WeightItem> objW = mgr.WeightDataManager.GetDataBetweenDates(DateRange).OrderByDescending(i => i.EffectiveDate).ToList();

                if (objW != null && objW.Count > 0)
                {
                    tblWeight.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Date").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblWeight.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Weight").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblWeight.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_BMI").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblWeight.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_ReadingSource").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblWeight.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Comments").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));

                    double? bmiForWeight = null;
                    foreach (HVManager.WeightDataManager.WeightItem item in objW)
                    {
                        tblWeight.AddCell(GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.HHMMAMPM));
                        tblWeight.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(item.CommonWeight.Value.ToPounds(), 2, false));
                        bmiForWeight = GetBMIForCurrentRecordForWeight(objGuids, item.CommonWeight.Value.ToPounds());
                        if (bmiForWeight.HasValue)
                        {
                            tblWeight.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(bmiForWeight.Value, 2, false));
                        }
                        else
                        {
                            tblWeight.AddCell(string.Empty);
                        }
                        tblWeight.AddCell(item.Source.Value);
                        tblWeight.AddCell(item.Note.Value);
                    }
                }
            }
            return tblWeight;
        }

        /// <summary>
        /// Creating PDF image for Weight
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetWeightGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.Weight, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF data for cholesterol table
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetCholesterolTable(PatientGuids objGuids, DataDateReturnType DateRange)
        {
            PdfPTable tblCholesterol = new PdfPTable(6);

            if (objGuids != null)
            {
                //AHAAPI.Provider.ProviderBase objPage = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                List<HVManager.CholesterolDataManager.CholesterolItem> objC = mgr.CholesterolDataManager.GetDataBetweenDates(DateRange).OrderByDescending(i => i.EffectiveDate).ToList();

                if (objC != null && objC.Count > 0)
                {
                    tblCholesterol.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Date").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblCholesterol.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_TCWithUnits").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblCholesterol.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_HDLWithUnits").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblCholesterol.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_LDLWithUnits").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblCholesterol.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_TriglycerideWithUnits").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblCholesterol.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_TestLocation").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));

                    foreach (HVManager.CholesterolDataManager.CholesterolItem item in objC)
                    {
                        tblCholesterol.AddCell(GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));

                        if (item.TotalCholesterol.Value.HasValue)
                            tblCholesterol.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(item.TotalCholesterol.Value.Value, 2, false));
                        else
                            tblCholesterol.AddCell(string.Empty);

                        if (item.HDL.Value.HasValue)
                            tblCholesterol.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(item.HDL.Value.Value, 2, false));
                        else
                            tblCholesterol.AddCell(string.Empty);

                        if (item.LDL.Value.HasValue)
                            tblCholesterol.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(item.LDL.Value.Value, 2, false));
                        else
                            tblCholesterol.AddCell(string.Empty);

                        if (item.Triglycerides.Value.HasValue)
                            tblCholesterol.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(item.Triglycerides.Value.Value, 2, false));
                        else
                            tblCholesterol.AddCell(string.Empty);

                        tblCholesterol.AddCell(item.TestLocation.Value);
                    }
                }
            }
            return tblCholesterol;
        }

        /// <summary>
        /// Creating PDF image for total cholesterol
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetTotalCholesterolGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.TotalCholesterol, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF image for combined graph of cholesterol
        /// </summary>
        /// <param name="DateRange"></param>
        /// <returns></returns>
        public static iTextSharp.text.Image GetCombinedCholesterolGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.CombinedCholesterol, DateRange, patientGuidsList);
        }


        /// <summary>
        /// Creating PDF image for HDL
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetHDLGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.HDL, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF image for LDL
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetLDLGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.LDL, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF image for Triglycerides
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetTriglyceridesGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.Triglycerides, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF data for blood glucose table
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetBloodGlucoseTable(PatientGuids objGuids, DataDateReturnType DateRange, int intLanguageId)
        {
            PdfPTable tblBloodGlucose = new PdfPTable(4);
            if (objGuids != null)
            {
                //AHAAPI.Provider.ProviderBase objPage = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                List<HVManager.BloodGlucoseDataManager.BGItem> objBG = mgr.BloodGlucoseDataManager.GetDataBetweenDates(DateRange).OrderByDescending(i => i.EffectiveDate).ToList();

                if (objBG != null && objBG.Count > 0)
                {
                    tblBloodGlucose.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_DateTime").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBloodGlucose.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_BGWithUnits").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBloodGlucose.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_ReadingType").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblBloodGlucose.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_ActionITook").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));

                    foreach (HVManager.BloodGlucoseDataManager.BGItem item in objBG)
                    {
                        tblBloodGlucose.AddCell(GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.HHMMAMPM));
                        tblBloodGlucose.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(item.Value.Value, 2, false));
                        string strReadingValue = string.Empty;
                        if (!string.IsNullOrEmpty(item.ReadingType.Value))
                        {
                            switch (item.ReadingType.Value.ToLower())
                            {
                                case "postprandial":
                                    strReadingValue = AHAHelpContent.ListItem.FindListItemNameByListItemCode("postprandial", AHADefs.VocabDefs.ReadingType, intLanguageId);
                                    break;
                                case "fasting":
                                    strReadingValue = AHAHelpContent.ListItem.FindListItemNameByListItemCode("fasting", AHADefs.VocabDefs.ReadingType, intLanguageId);
                                    break;
                                default:
                                    strReadingValue = item.ReadingType.Value;
                                    break;
                            }
                        }
                        tblBloodGlucose.AddCell(strReadingValue);
                        tblBloodGlucose.AddCell(item.ActionTaken.Value);
                    }
                }
            }
            return tblBloodGlucose;
        }

        /// <summary>
        /// Creating PDF image for blood glucose
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetBloodGlucoseGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {
            return _GetImage(objGuids, Graph.GraphType.Glucose, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF table for medication
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetMedicationTable(PatientGuids objGuids, DataDateReturnType DateRange, bool bCurrent)
        {
            PdfPTable tblMedication = new PdfPTable(5);
            if (objGuids != null)
            {
                //AHAAPI.Provider.ProviderBase objPage = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                List<HVManager.MedicationDataManager.MedicationItem> objMed = mgr.MedicationDataManager.GetDataBetweenDates(DateRange).OrderByDescending(i => i.EffectiveDate).ToList();

                if (objMed != null && objMed.Count > 0)
                {
                    tblMedication.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Name").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblMedication.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Strength").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblMedication.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Dosage").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblMedication.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Frequency").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblMedication.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_SideEffects").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));

                    List<HVManager.MedicationDataManager.MedicationItem> desendingList = new List<HVManager.MedicationDataManager.MedicationItem>();

                    foreach (HVManager.MedicationDataManager.MedicationItem item in objMed)
                    {
                        DateTime? dtDiscontinued = null;

                        if (item.HasDiscontinued
                            && item.DateDiscontinued.Value.HVApproximateDate != null
                            && item.DateDiscontinued.Value.HVApproximateDate.Month.HasValue
                            && item.DateDiscontinued.Value.HVApproximateDate.Day.HasValue)
                        {
                            dtDiscontinued = new DateTime(item.DateDiscontinued.Value.HVApproximateDate.Year, item.DateDiscontinued.Value.HVApproximateDate.Month.Value, item.DateDiscontinued.Value.HVApproximateDate.Day.Value);
                        }

                        if (bCurrent
                            && (!item.HasDiscontinued
                            || (dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() > DateTime.Now.ToUniversalTime())))
                        {
                            desendingList.Add(item);
                        }
                        else if (!bCurrent
                            && ((item.HasDiscontinued && !dtDiscontinued.HasValue)
                            || (item.HasDiscontinued && dtDiscontinued.HasValue && dtDiscontinued.Value.ToUniversalTime() <= DateTime.Now.ToUniversalTime())))
                        {
                            desendingList.Add(item);
                        }
                    }
                    List<HVManager.MedicationDataManager.MedicationItem> orderedMedList = desendingList.OrderByDescending(i => i.EffectiveDate).ToList();

                    foreach (HVManager.MedicationDataManager.MedicationItem meditem in orderedMedList)
                    {
                        tblMedication.AddCell(meditem.Name.Value);
                        tblMedication.AddCell(meditem.Strength.Value);
                        tblMedication.AddCell(meditem.Dosage.Value);
                        tblMedication.AddCell(meditem.Frequency.Value);
                        tblMedication.AddCell(meditem.Note.Value);
                    }
                }
            }
            return tblMedication;
        }

        /// <summary>
        /// Creating PDF image for exercise
        /// </summary>
        /// <returns></returns>
        public static iTextSharp.text.Image GetExerciseGraph(PatientGuids objGuids, DataDateReturnType DateRange, List<PatientGuids> patientGuidsList)
        {

            return _GetImage(objGuids, Graph.GraphType.Exercise, DateRange, patientGuidsList);
        }

        /// <summary>
        /// Creating PDF data for exercise table
        /// </summary>
        /// <returns></returns>
        public static PdfPTable GetExerciseTable(PatientGuids objGuids, DataDateReturnType DateRange)
        {
            PdfPTable tblExercise = new PdfPTable(6);

            if (objGuids != null)
            {
                //AHAAPI.Provider.ProviderBase objPage = System.Web.HttpContext.Current.Handler as AHAAPI.Provider.ProviderBase;
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);

                List<HVManager.ExerciseDataManager.ExerciseItem> objEx = mgr.ExerciseDataManager.GetDataBetweenDates(DateRange).OrderByDescending(i => i.EffectiveDate).ToList();

                if (objEx != null && objEx.Count > 0)
                {
                    tblExercise.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Date").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblExercise.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Activity").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblExercise.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_DurationInMinutes").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblExercise.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Intensity").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblExercise.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Steps").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));
                    tblExercise.AddCell(new Phrase(HttpContext.GetGlobalResourceObject("Reports", "Text_Comments").ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)));

                    foreach (HVManager.ExerciseDataManager.ExerciseItem item in objEx)
                    {
                        tblExercise.AddCell(GRCBase.DateTimeHelper.GetFormattedDateTimeString(item.When.Value, AHACommon.DateTimeHelper.GetDateFormatForWeb(), GRCBase.GRCDateSeperator.HYPHEN, GRCBase.GRCTimeFormat.NONE));
                        tblExercise.AddCell(item.Activity.Value);

                        if (item.Duration.Value.HasValue)
                            tblExercise.AddCell(GRCBase.StringHelper.GetFormattedDoubleString(item.Duration.Value.Value, 2, false));
                        else
                            tblExercise.AddCell(string.Empty);

                        string strIntensity = string.Empty;


                        if (item.Intensity.Value == RelativeRating.High)
                            strIntensity = HttpContext.GetGlobalResourceObject("Reports", "Text_Vigorous").ToString();
                        else if (item.Intensity.Value == RelativeRating.Low)
                            strIntensity = HttpContext.GetGlobalResourceObject("Reports", "Text_Light").ToString();
                        else if (item.Intensity.Value == RelativeRating.Moderate)
                            strIntensity = HttpContext.GetGlobalResourceObject("Reports", "Text_Moderate").ToString();
                        else if (item.Intensity.Value == RelativeRating.None)
                            strIntensity = string.Empty;
                        else if (item.Intensity.Value == RelativeRating.VeryHigh)
                            strIntensity = HttpContext.GetGlobalResourceObject("Reports", "Text_Vigorous").ToString();
                        else if (item.Intensity.Value == RelativeRating.VeryLow)
                            strIntensity = HttpContext.GetGlobalResourceObject("Reports", "Text_Light").ToString();


                        tblExercise.AddCell(strIntensity);
                        tblExercise.AddCell(item.NumberOfSteps.Value.HasValue ? item.NumberOfSteps.Value.Value.ToString() : string.Empty);
                        tblExercise.AddCell(item.Note.Value);
                    }
                }
            }
            return tblExercise;
        }

        public static IElement GetImage(PatientGuids objGuids, Graph.GraphType gType, DataDateReturnType dateRange, List<PatientGuids> patientGuidsList)
        {
            IElement objImage = _GetImage(objGuids, gType, dateRange, patientGuidsList);
            if (objImage != null &&
                (objImage as iTextSharp.text.Image).Alt != PDFHelper.EmptyImage)
                return objImage;
            else
                return null;
        }
    }

    public class PDFPageEvent : PdfPageEventHelper
    {
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            //document.Add(PDFHelper.GetPageHeaderTable());
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfPTable oTable = PDFHelper.GetPageHeaderTable();
            oTable.TotalWidth = document.PageSize.Width - 40;
            oTable.WriteSelectedRows(0, -1, 20, document.PageSize.Height - 15, writer.DirectContent);

        }
    }

#endregion
}
