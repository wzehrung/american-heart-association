﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Web;

using AHABusinessLogic;
using AHACommon;
using AHAHelpContent;
using GRCBase;

namespace Heart360Provider.Reports
{
    public static class CSVHelper
    {
        public static void CreateCsvReport(List<PatientInfo> patientInfoList, string fileName, string header)
        {
            string reportContent = CSVHelper.getCSVStringContent(patientInfoList, header);
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(reportContent);

            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            System.Web.HttpContext.Current.Response.ContentType = "application/csv";
            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
            System.Web.HttpContext.Current.Response.OutputStream.Flush();
            System.Web.HttpContext.Current.Response.OutputStream.Close();
            System.Web.HttpContext.Current.Response.End();
        }

        public static void CreateCsvReportContactList(List<PatientInfo> patientInfoList, string fileName, string header)
        {
            string reportContent = CSVHelper.getCSVStringContentContactList(patientInfoList, header);
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(reportContent);

            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            System.Web.HttpContext.Current.Response.ContentType = "application/csv";
            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
            System.Web.HttpContext.Current.Response.OutputStream.Flush();
            System.Web.HttpContext.Current.Response.OutputStream.Close();
            System.Web.HttpContext.Current.Response.End();
        }

        public static string getCSVStringContent(List<PatientInfo> lstPatientInfo, string header)
        {
            StringBuilder sb = new StringBuilder(header);

            foreach (PatientInfo objPatientInfo in lstPatientInfo)
            {
                sb.Append(objPatientInfo.FullName.Replace(",",""));
                sb.Append(",");
                if (objPatientInfo.YearOfBirth.HasValue)
                {
                    sb.Append(objPatientInfo.YearOfBirth.Value.ToString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }
                if (objPatientInfo.ProviderConnectionDate.HasValue)
                {

                    sb.Append(objPatientInfo.ProviderConnectionDate.Value.ToShortDateString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }

                if (objPatientInfo.LastActive.HasValue)
                {
                    sb.Append(objPatientInfo.LastActive.Value.ToShortDateString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }

                if ((!string.IsNullOrEmpty(objPatientInfo.Systolic.ToString()) && (!string.IsNullOrEmpty(objPatientInfo.Diastolic.ToString()))))
                {

                    sb.Append(objPatientInfo.Systolic.ToString() + "/" + objPatientInfo.Diastolic.ToString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }

                if (!string.IsNullOrEmpty(objPatientInfo.BloodGlucose.ToString()))
                {
                    sb.Append(objPatientInfo.BloodGlucose.ToString() + " mg/dL");
                    sb.Append(",");
                }
                else
                {
                    sb.Append(objPatientInfo.BloodGlucose.ToString());
                    sb.Append(",");
                }
                if (objPatientInfo.Cholesterol != null && objPatientInfo.Cholesterol > 0)
                {
                    sb.Append(objPatientInfo.Cholesterol.ToString() + " mg/dL");
                    sb.Append(",");
                }
                else
                {
                    sb.Append(objPatientInfo.Cholesterol.ToString());
                    sb.Append(",");
                }

                if (objPatientInfo.TotalAlerts > 0)
                {
                    sb.Append(objPatientInfo.TotalAlerts.ToString());
                    sb.Append('\n');
                }
                else
                {
                    sb.Append("");
                    sb.Append('\n');
                }
            }
            return sb.ToString();
        }

        public static string getCSVStringContentContactList(List<PatientInfo> lstPatientInfo, string header)
        {
            StringBuilder sb = new StringBuilder(header);

            foreach (PatientInfo objPatientInfo in lstPatientInfo)
            {
                AHAHelpContent.Patient pat = AHAHelpContent.Patient.FindByUserHealthRecordGUID(objPatientInfo.RecordGUID);
                AHABusinessLogic.PatientContact pc = (pat != null) ? AHABusinessLogic.PatientContact.GetPatientContact(pat) : null;

                sb.Append(objPatientInfo.FullName.Replace(",", ""));
                sb.Append(",");
                if (!string.IsNullOrEmpty(pc.Email))
                {
                    sb.Append(pc.Email.ToString());
                    sb.Append(",");
                }
                else
                {
                    sb.Append("");
                    sb.Append(",");
                }

                if (!string.IsNullOrEmpty(pc.ContactPhone))
                {
                    sb.Append(pc.ContactPhone.ToString());
                    sb.Append("\n");
                }
                else
                {
                    sb.Append("");
                    sb.Append("\n");
                }


            }
            return sb.ToString();
        }
    }
}
