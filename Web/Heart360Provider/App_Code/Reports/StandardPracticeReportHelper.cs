﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

using AHABusinessLogic;

namespace Heart360Provider.Reports
{
    public class StandardPracticeReportHelper
    {
        public class GraphItem
        {
            public int Value
            {
                get;
                set;
            }

            public DateTime Date
            {
                get;
                set;
            }
        }

        public class GraphItemTemp
        {
            public List<int> ValueList
            {
                get;
                set;
            }

            public DateTime Date
            {
                get;
                set;
            }
        }


        #region Private Methods

        private static int _GetAverageValueForDate(DateTime dt, Hashtable objPatientList)
        {
            List<int> iList = new List<int>();

            foreach (Guid key in objPatientList.Keys)
            {
                List<GraphItemTemp> gItemTempList = objPatientList[key] as List<GraphItemTemp>;

                GraphItemTemp gItem = gItemTempList.Where(t => t.Date.CompareTo(dt.Date) == 0).SingleOrDefault();
                if (gItem != null && gItem.ValueList.Count > 0)
                {
                    iList.Add(gItem.ValueList[0]);
                }
            }

            return (int)iList.Average();
        }

        #endregion

        #region Blood Glucose Specific

        public static List<GraphItem> GetBloodGlucoseList(AHACommon.DataDateReturnType dateRange, List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 50;
            double dThreshold_Upper = 600;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.BloodGlucoseDataManager.BGItem> listBG = mgr.BloodGlucoseDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.BloodGlucoseDataManager.BGItem item in listBG)
                {
                    if (item.Value.Value < dThreshold_Lower || item.Value.Value > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)item.Value.Value);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)item.Value.Value }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });

            //foreach (int key in objPatientList.Keys)
            //{
            //    List<GraphItemTemp> gItemTempList = objPatientList[key] as List<GraphItemTemp>;

            //    int iVal = 0;

            //    foreach (GraphItemTemp gItemTemp in gItemTempList)
            //    {
            //        iVal = (int)gItemTempList.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
            //        (objPatientList[key] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int>{ iVal};
            //    }
            //}

            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        #endregion

        #region Blood Pressure Specific

        public static List<GraphItem> GetSystolicPressureList(AHACommon.DataDateReturnType dateRange, List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 20;
            double dThreshold_Upper = 400;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.BloodPressureDataManager.BPItem> listBP = mgr.BloodPressureDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.BloodPressureDataManager.BPItem item in listBP)
                {
                    if (item.Systolic.Value < dThreshold_Lower || item.Systolic.Value > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)item.Systolic.Value);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)item.Systolic.Value }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });


            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        public static List<GraphItem> GetDiastolicPressureList(AHACommon.DataDateReturnType dateRange, List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 20;
            double dThreshold_Upper = 400;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.BloodPressureDataManager.BPItem> listBP = mgr.BloodPressureDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.BloodPressureDataManager.BPItem item in listBP)
                {
                    if (item.Diastolic.Value < dThreshold_Lower || item.Diastolic.Value > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)item.Diastolic.Value);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)item.Diastolic.Value }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });


            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        #endregion

        #region Cholesterol Specific

        public static List<GraphItem> GetTotalCholesterolList(AHACommon.DataDateReturnType dateRange, List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 50;
            double dThreshold_Upper = 600;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listBP = mgr.CholesterolDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.CholesterolDataManager.CholesterolItem item in listBP)
                {
                    if (!item.TotalCholesterol.Value.HasValue)
                        continue;

                    if (item.TotalCholesterol.Value.Value < dThreshold_Lower || item.TotalCholesterol.Value.Value > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)item.TotalCholesterol.Value.Value);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)item.TotalCholesterol.Value.Value }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });


            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        public static List<GraphItem> GetHDLCholesterolList(AHACommon.DataDateReturnType dateRange,List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 0;
            double dThreshold_Upper = 300;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listBP = mgr.CholesterolDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.CholesterolDataManager.CholesterolItem item in listBP)
                {
                    if (!item.HDL.Value.HasValue)
                        continue;

                    if (item.HDL.Value.Value < dThreshold_Lower || item.HDL.Value.Value > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)item.HDL.Value.Value);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)item.HDL.Value.Value }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });


            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        public static List<GraphItem> GetLDLCholesterolList(AHACommon.DataDateReturnType dateRange, List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 50;
            double dThreshold_Upper = 500;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID, objGuids.RecordID);
                LinkedList<HVManager.CholesterolDataManager.CholesterolItem> listBP = mgr.CholesterolDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.CholesterolDataManager.CholesterolItem item in listBP)
                {
                    if (!item.LDL.Value.HasValue)
                        continue;

                    if (item.LDL.Value.Value < dThreshold_Lower || item.LDL.Value.Value > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)item.LDL.Value.Value);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)item.LDL.Value.Value }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });


            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        #endregion

        #region Weight Specific

        public static List<GraphItem> GetWeightList(AHACommon.DataDateReturnType dateRange, List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 50;
            double dThreshold_Upper = 800;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID,objGuids.RecordID);
                LinkedList<HVManager.WeightDataManager.WeightItem> listWeight = mgr.WeightDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.WeightDataManager.WeightItem item in listWeight)
                {
                    double weight = item.CommonWeight.Value.ToPounds();

                    if (weight < dThreshold_Lower || weight > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)weight);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)weight }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });

            //foreach (int key in objPatientList.Keys)
            //{
            //    List<GraphItemTemp> gItemTempList = objPatientList[key] as List<GraphItemTemp>;

            //    int iVal = 0;

            //    foreach (GraphItemTemp gItemTemp in gItemTempList)
            //    {
            //        iVal = (int)gItemTempList.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
            //        (objPatientList[key] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int>{ iVal};
            //    }
            //}

            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        #endregion

        #region Physical Activity Specific

        public static List<GraphItem> GetPhysicalActivityList(AHACommon.DataDateReturnType dateRange, List<PatientGuids> listGuids)
        {
            double dThreshold_Lower = 0;
            double dThreshold_Upper = 500;

            List<GraphItem> objList = new List<GraphItem>();

            Hashtable objPatientList = new Hashtable();

            //List<AHAHelpContent.Patient> listPatient = AHAAPI.H360RequestContext.GetContext().PatientList;
            listGuids.ForEach(delegate(PatientGuids objGuids)
            {
                HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(objGuids.PersonID , objGuids.RecordID);
                LinkedList<HVManager.ExerciseDataManager.ExerciseItem> listExercise = mgr.ExerciseDataManager.GetDataBetweenDates(dateRange);

                List<GraphItemTemp> objListTemp = new List<GraphItemTemp>();

                foreach (HVManager.ExerciseDataManager.ExerciseItem item in listExercise)
                {
                    if(!item.Duration.Value.HasValue) return;

                    double duration = item.Duration.Value.Value;

                    if (duration < dThreshold_Lower || duration > dThreshold_Upper)
                        continue;

                    if (objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() > 0)
                    {
                        objListTemp.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).SingleOrDefault().ValueList.Add((int)duration);
                    }
                    else
                    {
                        objListTemp.Add(new GraphItemTemp { ValueList = new List<int> { (int)duration }, Date = item.When.Value.Date });
                    }

                    if (objList.Where(i => i.Date.CompareTo(item.When.Value.Date) == 0).Count() == 0)
                    {
                        objList.Add(new GraphItem { Date = item.When.Value.Date, Value = 0 });
                    }
                }

                objPatientList.Add(objGuids.RecordID, objListTemp);

                int iVal = 0;

                foreach (GraphItemTemp gItemTemp in objListTemp)
                {
                    iVal = (int)objListTemp.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
                    (objPatientList[objGuids.RecordID] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int> { iVal };
                }
            });

            //foreach (int key in objPatientList.Keys)
            //{
            //    List<GraphItemTemp> gItemTempList = objPatientList[key] as List<GraphItemTemp>;

            //    int iVal = 0;

            //    foreach (GraphItemTemp gItemTemp in gItemTempList)
            //    {
            //        iVal = (int)gItemTempList.Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList.Average();
            //        (objPatientList[key] as List<GraphItemTemp>).Where(t => t.Date.CompareTo(gItemTemp.Date) == 0).SingleOrDefault().ValueList = new List<int>{ iVal};
            //    }
            //}

            foreach (GraphItem gitem in objList)
            {
                objList.Where(g => g.Date == gitem.Date).SingleOrDefault().Value = _GetAverageValueForDate(gitem.Date, objPatientList);
            }

            if (objList.Count > 0)
            {
                return objList.OrderBy(o => o.Date).ToList();
            }

            return objList;
        }

        #endregion

    }
}
