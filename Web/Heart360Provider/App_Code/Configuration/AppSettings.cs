﻿using System;
using System.Collections.Generic;
using System.Text;
using GRCBase;

//
// PJB (3.5.2014) - this was refactored from AHACommon/AHAAppSettings.cs.
//
namespace Heart360Provider.AppSettings
{
    /****************************************************************
     *
     * URLS
     * 
     ****************************************************************/

    public class Url
    {

        /// <summary>
        /// Provider Portal
        /// </summary>
        public static string ProviderPortal
        {
            get
            {
                return ConfigReader.GetValue("ProviderPortal");
            }
        }

        public static string ProviderLogin
        {
            get
            {
                return ConfigReader.GetValue("ProviderLogin");
            }
        }

        public static string ProviderRegister
        {
            get
            {
                return ConfigReader.GetValue("ProviderRegister");
            }
        }

        public static string ProviderHome
        {
            get
            {
                return ConfigReader.GetValue("ProviderHome");
            }
        }

        public static string VolunteerLogin
        {
            get
            {
                return ConfigReader.GetValue("VolunteerLogin");
            }
        }

        public static string VolunteerRegister
        {
            get
            {
                return ConfigReader.GetValue("VolunteerRegister");
            }
        }

        public static string ForgotPassword { get { return ConfigReader.GetValue("ForgotPassword"); } }

        public static string ResetPassword { get { return ConfigReader.GetValue("ResetPassword"); } }
        
        public static string ProviderInvitationEntrypoint
        {
            get
            {
                return ConfigReader.GetValue("ProviderInvitationLink");
            }
        }

        public static string PatientInvitationEntrypoint
        {
            get
            {
                return ConfigReader.GetValue("PatientInvitationLink");
            }
        }

        public static string SearchAHA
        {
            get
            {
                return ConfigReader.GetValue("SearchAHA");
            }
        }

        public static string ProviderModule(Heart360Provider.ProviderModules.MODULE_ID modid)
        {
            string retval = ErrorPageRelativePath;
            switch (modid)
            {
                case ProviderModules.MODULE_ID.MODULE_ID_ALERTS:
                    retval = ConfigReader.GetValue("ProviderModuleAlerts");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_DASHBOARD:
                    retval = ConfigReader.GetValue("ProviderModuleDashboard");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_GROUPS:
                    retval = ConfigReader.GetValue("ProviderModuleGroups");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_MESSAGES:
                    retval = ConfigReader.GetValue("ProviderModuleMessages");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_PARTICIPANTS:
                    retval = ConfigReader.GetValue("ProviderModuleParticipants");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_PROFILE:
                    retval = ConfigReader.GetValue("ProviderModuleProfile");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_RESOURCES:
                    retval = ConfigReader.GetValue("ProviderModuleResources");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT:
                    retval = ConfigReader.GetValue("ProviderModuleSingleParticipant");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_REPORTS:
                    retval = ConfigReader.GetValue("ProviderModuleReports");
                    break;

                case ProviderModules.MODULE_ID.MODULE_ID_SPONSORS:
                    retval = ConfigReader.GetValue("ProviderModuleSponsors");
                    break;

                default:
                    break;

            }

            return retval;

        }

        /// <summary>
        /// SiteUrl - used by alot, like email templates
        /// </summary>
        public static string SiteUrl
        {
            get
            {
                return ConfigReader.GetValue("SiteUrl");
            }
        }

        /// <summary>
        /// SiteUrlNoHttps
        /// </summary>
        public static string SiteUrlNoHttps
        {
            get
            {
                return ConfigReader.GetValue("SiteUrl").Replace("https://", "http://");
            }
        }

        

        /// <summary>
        /// Error page url
        /// </summary>
        public static string ErrorPageRelativePath
        {
            get
            {
                return ConfigReader.GetValue("ErrorPageRelativePath");
            }
        }

        /// <summary>
        /// file not found page url
        /// </summary>
        public static string FileNotFoundPageRelativeURL
        {
            get
            {
                return ConfigReader.GetValue("FileNotFoundPageRelativeURL");
            }
        }


        public static string AHA
        {
            get
            {
                return ConfigReader.GetValue("url.AHA");
            }
        }

        public static string Heart360
        {
            get
            {
                return ConfigReader.GetValue("url.Heart360");
            }
        }

        public static string Donate
        {
            get
            {
                return ConfigReader.GetValue("url.Donate");
            }
        }

        public static string Facebook
        {
            get
            {
                return ConfigReader.GetValue("url.Facebook");
            }
        }

        public static string Twitter { get { return ConfigReader.GetValue("url.Twitter"); } }

        public static string Heart360PrivacyPolicy { get { return ConfigReader.GetValue("url.H360PrivacyPolicy"); } }
        public static string Heart360Copyright { get { return ConfigReader.GetValue("url.H360Copyright"); } }
        public static string Heart360EthicsPolicy { get { return ConfigReader.GetValue("url.H360EthicsPolicy"); } }
        public static string Heart360ConflictOfInterest { get { return ConfigReader.GetValue("url.H360ConflictOfInterest"); } }
        public static string Heart360LinkingPolicy { get { return ConfigReader.GetValue("url.H360LinkingPolicy"); } }
        public static string Heart360Diversity { get { return ConfigReader.GetValue("url.H360Diversity"); } }
        public static string Heart360Careers { get { return ConfigReader.GetValue("url.H360Careers"); } }

    }

    /****************************************************************
     *
     * DIRECTORIES
     * 
     ****************************************************************/

    public class Directory
    {
        /// <summary>
        /// This function is used to get the information about EmailTemplateDirectory.
        /// </summary>
        public static string EmailTemplate
        {
            get
            {
                return ConfigReader.GetValue("EmailTemplateDirectory");
            }
        }

        /// <summary>
        /// This function is used to get the information about XmlFilesDirectory. It is here
        /// where all the Xml files used by the application are stored.
        /// </summary>
        public static string XmlFilesDirectory
        {
            get
            {
                return ConfigReader.GetValue("XmlFilesDirectory");
            }
        }

        /// <summary>
        /// EmailListDownloadPath
        /// </summary>
        public static string EmailListDownloadPath
        {
            get
            {
                return ConfigReader.GetValue("EmailListDownloadPath");
            }
        }
    }

    /****************************************************************
     *
     * SETTINGS
     * 
     ****************************************************************/

    public class Setting
    {
        /// <summary>
        /// This function is used to get the information about VersionNumber.
        /// </summary>
        public static string VersionNumber
        {
            get
            {
                return ConfigReader.GetValue("VersionNumber");
            }
        }

        /// <summary>
        /// This function is used to know if EnableDataCacheDiagnostics is enabled or not.
        /// </summary>
        public static bool EnableDataCacheDiagnostics
        {
            get
            {
                return ConfigReader.GetValue("EnableDataCacheDiagnostics") == "1" ? true : false;
            }
        }

        /// <summary>
        /// This function is used to know if IsSSLEnabled is enabled or not.
        /// </summary>
        public static bool IsSSLEnabled
        {
            get
            {
                return ConfigReader.GetValue("IsSSLEnabled") == "1" ? true : false;
            }
        }

        /// <summary>
        /// This function is used to know if IsGoogleAnalyticsEnabled is enabled or not.
        /// </summary>
        public static bool IsGoogleAnalyticsEnabled
        {
            get
            {
                return ConfigReader.GetValue("IsGoogleAnalyticsEnabled") == "0" ? false : true;
            }
        }

        /// <summary>
        /// EnableJSErrorLog
        /// </summary>
        public static bool EnableJSErrorLog
        {
            get
            {
                return ConfigReader.GetValue("EnableJSErrorLog") == "1" ? true : false;
            }
        }

        /// <summary>
        /// Disclaimer_Provider
        /// </summary>
        public static string Disclaimer_Provider_PDF
        {
            get
            {
                //return System.Web.HttpContext.GetGlobalResourceObject("Common1", "Provider_Footer_Disclaimer").ToString();
                return System.Web.HttpContext.GetGlobalResourceObject("Common", "Text_Footer_Disclaimer_Provider").ToString().Replace("<br/>", "\r\n");
            }
        }

        /// <summary>
        /// Disclaimer_Provider
        /// </summary>
        public static string Disclaimer_Provider_HTML
        {
            get
            {
                string strVal = System.Web.HttpContext.GetGlobalResourceObject("Email", "Provider_Footer_Disclaimer").ToString().Replace("\\r\\n", "<br />");
                strVal = System.Text.RegularExpressions.Regex.Replace(strVal, "##REGISTERED##", "<sup>&#174;</sup>");
                return strVal;
            }
        }

    }

    /****************************************************************
     *
     * SETTINGS
     * 
     ****************************************************************/
    public class Email
    {
        /// <summary>
        /// This function is used to get the information about which is the SMTPServer to be used.
        /// </summary>
        public static string SMTPServer
        {
            get
            {
                return ConfigReader.GetValue("SMTPServer");
            }
        }

        /// <summary>
        /// This function is used to know if Messaging is enabled or not.
        /// </summary>
        public static bool IsMessagingEnabled
        {
            get
            {
                return ConfigReader.GetValue("Is_Messaging_Enabled") == "1" ? true : false;
            }
        }

        /// <summary>
        /// SystemEmail
        /// </summary>
        public static string System
        {
            get
            {
                return ConfigReader.GetValue("SystemEmail");
            }
        }

        /// <summary>
        /// FeedbackToEmail
        /// </summary>
        public static string Feedback
        {
            get
            {
                return ConfigReader.GetValue("FeedbackToEmail");
            }
        }

        /// <summary>
        /// SenderEmail
        /// </summary>
        public static string Sender
        {
            get
            {
                return ConfigReader.GetValue("SenderEmail");
            }
        }

        public static string TemplateForgotPassword
        {
            get
            {
                return ConfigReader.GetValue("File_Forgot_Password");
            }
        }

        public static string TemplateForgotUsername
        {
            get
            {
                return ConfigReader.GetValue("File_Forgot_Username");
            }
        }

        public static string TemplateRegisterProvider
        {
            get
            {
                return ConfigReader.GetValue("File_Register_Provider");
            }
        }

        public static string TemplateNewMessagePatient
        {
            get
            {
                return ConfigReader.GetValue("File_New_Message_Patient");
            }
        }

        public static string TemplateNewMessageProvider
        {
            get
            {
                return ConfigReader.GetValue("File_New_Message_Provider");
            }
        }

        public static string TemplateInvitePatient
        {
            get
            {
                return ConfigReader.GetValue("File_Invite_Patient");
            }
        }

        public static string TemplateInvitationConfirmationPatient
        {
            get
            {
                return ConfigReader.GetValue("File_Invite_Confirmation_Patient");
            }
        }

        public static string TemplateDisconnectPatient
        {
            get
            {
                return ConfigReader.GetValue("File_Disconnect_Patient");
            }
        }
    }
}
