﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heart360Provider
{
    public class ProviderModules
    {

        public enum MODULE_ID : int
        {
            MODULE_ID_UNKNOWN = 0,
            MODULE_ID_ALERTS = 1,
            MODULE_ID_GROUPS = 2,
            MODULE_ID_MESSAGES = 3,
            MODULE_ID_PARTICIPANTS = 4,
            MODULE_ID_PROFILE = 5,
            MODULE_ID_RESOURCES = 6,
            MODULE_ID_SINGLE_PARTICIPANT = 7,
            MODULE_ID_REPORTS = 8,
            MODULE_ID_SPONSORS = 9,
            MODULE_ID_DASHBOARD = 10,           // does not have its own module header, used for page logic
            MODULE_ID_COUNT = 11                // for range checking
        } ;

        // These are an enumeration of all "global" popups accessible by any module
        public enum POPUP_ID : int
        {
            POPUP_ID_UNKNOWN = 0,
            POPUP_ID_CREATE_GROUP = 1,          // Groups module
            POPUP_ID_INVITE_BY_EMAIL = 2,       // My Participants module
            POPUP_ID_INVITE_BY_PRINT = 3,       // My Participants module
            POPUP_ID_COMPOSE_MESSAGE = 4,       // Messages
            POPUP_ID_CREATE_ALERT = 5,          // Alerts module
            POPUP_ID_CREATE_ANNOUNCEMENT = 6,   // Resources module
            POPUP_ID_CREATE_RESOURCE = 7,       // Resources module
            POPUP_ID_CREATE_NOTE = 8,           // Single Participant
            POPUP_ID_CHANGE_PASSWORD = 9,       // Profile module
            POPUP_ID_PATIENT_MESSAGE = 10,      // Single Participant
            POPUP_ID_COUNT = 11                 // for range checking
        } ;


        public static MODULE_ID GetModuleIdFromInt(int _modId)
        {
            MODULE_ID result = MODULE_ID.MODULE_ID_UNKNOWN;

            switch (_modId)
            {
                case (int)MODULE_ID.MODULE_ID_ALERTS:
                    result = MODULE_ID.MODULE_ID_ALERTS;
                    break;

                case (int)MODULE_ID.MODULE_ID_GROUPS:
                    result = MODULE_ID.MODULE_ID_GROUPS;
                    break;

                case (int)MODULE_ID.MODULE_ID_MESSAGES:
                    result = MODULE_ID.MODULE_ID_MESSAGES;
                    break;

                case (int)MODULE_ID.MODULE_ID_PARTICIPANTS:
                    result = MODULE_ID.MODULE_ID_PARTICIPANTS;
                    break;

                case (int)MODULE_ID.MODULE_ID_PROFILE:
                    result = MODULE_ID.MODULE_ID_PROFILE;
                    break;

                case (int)MODULE_ID.MODULE_ID_RESOURCES:
                    result = MODULE_ID.MODULE_ID_RESOURCES;
                    break;

                case (int)MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT:
                    result = MODULE_ID.MODULE_ID_SINGLE_PARTICIPANT;
                    break;

                case (int)MODULE_ID.MODULE_ID_REPORTS:
                    result = MODULE_ID.MODULE_ID_REPORTS;
                    break;

                case (int)MODULE_ID.MODULE_ID_SPONSORS:
                    result = MODULE_ID.MODULE_ID_SPONSORS;
                    break;

                default:
                    break;
            }
            return result;
        }
    }
}