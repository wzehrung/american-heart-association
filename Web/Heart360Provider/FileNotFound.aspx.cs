﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Text.RegularExpressions;

using AHAProvider;

namespace Heart360Provider
{
    public partial class FileNotFound : System.Web.UI.Page
    {

        protected void Page_PreInit(object sender, System.EventArgs e)
        {
            if (SessionInfo.IsProviderLoggedIn())
            {
                this.MasterPageFile = "~/MasterPages/Provider.Master";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string strLitErrorText = GetGlobalResourceObject("Common", "Error_Message_Persists").ToString();
            strLitErrorText = Regex.Replace(strLitErrorText, "##supportmail##", AHACommon.AHAAppSettings.SupportEmail);

            litError.Text = strLitErrorText;
            string strLitLinkText = GetGlobalResourceObject("Common", "Error_Message_Details").ToString();
            strLitLinkText = Regex.Replace(strLitLinkText, "##requestsource##", Request["Source"]);

            litLinkText.Text = strLitLinkText;
            System.Web.HttpContext.Current.Response.StatusCode = 404;

            if (!SessionInfo.IsProviderLoggedIn() )
            {
                divContent.Attributes.Add("class", "content");
            }
        }
    }
}