﻿<%@ Page Title="Heart360 - SignIn" Language="C#" MasterPageFile="~/MasterPages/Visitor.Master" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="Heart360Provider.SignIn" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain">

    <div class="tile big blue signin">
        <asp:UpdatePanel ID="updatePanelActions" UpdateMode="Conditional" runat="server">
            <ContentTemplate>

                    <div class="top-tile">
                        <%=GetGlobalResourceObject("Common", "Text_SignInTitle")%>                        
                    </div>
                    <div class="middle-tile">
        		        <a id="aTrackerLink2" class="tile-button" href="" runat="server"></a>
                        <div class="tile-icon"></div>
                        <div class="tile-icon-color"></div>
                        <div class="tile-value">
                            <asp:TextBox ID="tbUsername" runat="server" TextMode="SingleLine" Font-Size="Medium" Placeholder="Username" CssClass="username"></asp:TextBox>
                            <asp:TextBox ID="tbPassword" runat="server" Font-Size="Medium" TextMode="Password" Placeholder="Password"  CssClass="password"></asp:TextBox>
                            <asp:Button ID="btnLogin" runat="server" Text="Go" Font-Size="Medium" CssClass="gradient-button" OnClick="btnLogin_OnClick" />
                            <asp:CustomValidator ID="vSignInUsername" runat="server" EnableClientScript="false" ValidationGroup="vgSignIn" Display="None" OnServerValidate="ValidateUsername"></asp:CustomValidator>
                            <asp:CustomValidator ID="vSignInPassword" runat="server" EnableClientScript="false" ValidationGroup="vgSignIn" Display="None" OnServerValidate="ValidatePassword"></asp:CustomValidator>
        		        </div>
                    </div>                  
                    <asp:Literal ID="errorText" runat="server"/>

                </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottom-tile">
            <a id="aTrackerLink1" href="" runat="server"></a>
            <a id="aSignUp" class="new-account" href="#" runat="server"><%=GetGlobalResourceObject("Common", "Text_NewAccount")%>?</a> <br />
            <div class="forgot-password popuplink" data-page="SignIn" data-method="ForgotPasswordPopup" data-customclass="red-popup">
                <%=GetGlobalResourceObject("Common", "Text_ForgotPassword")%>
            </div>
        </div>
    </div>

    <!-- Need some spacing after the tile -->
    <br /><br /><br />

    <!-- ### BEGIN Popup Container ### -->
        <div class="popup-container">
            <!-- Used for the transparent bg as well as the click to close event -->
            <div class="dark-bg"></div>

            <div class="main-popup">

                <div class="popup-close-button">CLOSE</div><!-- End .popup-close-button -->
                <div class="popup-message"></div><!-- End .popup-message -->
                <div class="popup-html"></div> <!-- End .popup-html -->
                <!-- Where all html data will be injected -->
            </div><!-- End .main-popup -->
        </div><!-- End .popup-container -->
    <!-- ### END Popup Container ### -->

</asp:Content>
