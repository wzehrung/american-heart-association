﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Provider
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AHAProvider.SessionInfo.ResetProviderSession();

                string strRequestGUID = Request["r"];

                if (!string.IsNullOrEmpty(strRequestGUID))
                {
                    try
                    {
                        strRequestGUID = GRCBase.BlowFish.DecryptString(strRequestGUID);
                    }
                    catch
                    {
                        strRequestGUID = string.Empty;
                    }
                }
                else if( !string.IsNullOrEmpty( Request["rdev"] ) )
                {
                    strRequestGUID = Request["rdev"];
                }

                if (string.IsNullOrEmpty(strRequestGUID) || !AHAProvider.H360Utility.IsGUID(strRequestGUID))
                {
                    Response.Redirect( AppSettings.Url.ProviderLogin, true);
                }

                AHAHelpContent.ResetPasswordLog objRPL = AHAHelpContent.ResetPasswordLog.GetLogByResetGUID(new Guid(strRequestGUID));
                if (objRPL == null)
                {
                    Response.Redirect(AppSettings.Url.ProviderLogin, true);
                }
                else
                {
                    if (objRPL.ResetedDate.HasValue)
                    {
                        Response.Redirect(AppSettings.Url.ProviderLogin, true);
                    }
                }
            }
        }

        protected void btnReset_OnClick(object sender, EventArgs e)
        {
            // Validate content
            Page.Validate("vgReset");
            if (Page.IsValid)
            {
                try
                {
                    string strRequestGUID = (!string.IsNullOrEmpty(Request["r"]) ) ? GRCBase.BlowFish.DecryptString(Request["r"]) : Request["rdev"] ;
                    Guid theguid = new Guid(strRequestGUID);

                    AHAHelpContent.ResetPasswordLog objRPL = AHAHelpContent.ResetPasswordLog.GetLogByResetGUID(theguid);

                    AHAHelpContent.ProviderDetails objPD = AHAHelpContent.ProviderDetails.FindByProviderID(objRPL.ProviderID);

                    AHAHelpContent.ResetPasswordLog.SetPasswordResetForResetGUID(theguid);

                    AHAHelpContent.ProviderDetails.UpdatePassword(objPD.ProviderID, Heart360Provider.Utility.UIHelper.GetHashString(tbPassword.Text.Trim()));

                    Response.Redirect(AppSettings.Url.ProviderLogin, true);
                }
                catch
                {
                    litErrorMsg.Text = "<div class='inline-error'>An internal error occurred while resetting your password.</div>";
                }

            }
        }

        protected void ValidatePassword(object sender, ServerValidateEventArgs v)
        {
            CustomValidator valid = (CustomValidator)sender;
            v.IsValid = true;

            string trimstrA = tbPassword.Text.Trim();
            string trimstrB = tbPasswordConfirm.Text.Trim();
            string errstr = string.Empty;

            if (!AHAHelpContent.ProviderDetails.ValidatePasswordFormat(trimstrA, ref errstr))
            {
                v.IsValid = false;
                litErrorMsg.Text = "<div class='inline-error'>" + errstr + "</div>";
            }
            else if( !trimstrA.Equals( trimstrB ) )
            {
                v.IsValid = false;
                litErrorMsg.Text = "<div class='inline-error'>The passwords are not identical.</div>";
            }

        }

        protected void btnPpeSummaryOk_Click(object sender, EventArgs e)
        {
            // there are no popups on this page, so this shouldn't be called.
            

        }
    }
}