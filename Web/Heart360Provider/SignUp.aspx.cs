﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Heart360Provider
{
    public partial class SignUp : System.Web.UI.Page
    {
        private static Heart360Provider.Controls.SignUp mSignUpControl;


        [System.Web.Services.WebMethod]
        public static string SignUpControlHandler(bool INIT, AHACommon.NameValue[] inputs)
        {
            string result = AHACommon.UXControl.ControlHandler(mSignUpControl, INIT, inputs, false);

            if (!INIT && AHACommon.UXControl.IsJsonSuccess(result))
            {
                // Note: newly created provider information is in session state via mSignUpControl
                result = AHACommon.UXControl.JsonRedirect( Heart360Provider.AppSettings.Url.ProviderHome ) ;
            }

            return result;
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                AHAHelpContent.DbUserType pvtype = AHAHelpContent.DbUserType.DbUserTypeProvider;

                // First check to see if a user role was specified
                if (!string.IsNullOrEmpty(this.Request.QueryString["pvtype"]))
                {
                    int ival = int.Parse(this.Request.QueryString["pvtype"]);
                    if (ival == (int)AHAHelpContent.DbUserType.DbUserTypeVolunteer)
                        pvtype = AHAHelpContent.DbUserType.DbUserTypeVolunteer;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["invitationid"]))
                {
                    //invitation from a patient sent via email to a provider who is now logging in
                    String inviteCode = this.Request.QueryString["invitationid"];
                    if (!string.IsNullOrEmpty(inviteCode))
                        AHAProvider.SessionInfo.SetPatientInvitationId(inviteCode);
                }

                // Next check to see if a campaign was specified
                AHAProvider.H360Utility.ResetCampaignSession();
                if (!string.IsNullOrEmpty(this.Request.QueryString["cid"]))
                {
                    string campurl = GRCBase.BlowFish.DecryptString(Request["cid"]);
                    AHAHelpContent.Campaign objCampaign = AHAHelpContent.Campaign.FindCampaignByURL(campurl, AHAProvider.H360Utility.GetCurrentLanguageID());
                    if (objCampaign != null && objCampaign != null && objCampaign.IsActive)
                    {
                        AHAProvider.SessionInfo.SetCampaignSession(objCampaign.CampaignID);
                        AHAProvider.SessionInfo.SetCampaignSessionUrl(campurl);
                        /**
                        AHAProvider.H360Utility.SetCampaignSession(objCampaign.CampaignID);
                        AHAProvider.H360Utility.SetCampaignSessionUrl(campurl);     // PJB: need this for signout redirect back to campaign
                         **/
                    }
                }


                // Allocate and initialize our profile information control
                try
                {
                    mSignUpControl = new Controls.SignUp();
                    mSignUpControl.Name = "SignUpInfo";
                    if (!mSignUpControl.Initialize())
                    {
                        mSignUpControl = null;
                    }
                    else
                    {
                        // tell the control what user type we are.
                        mSignUpControl.ProviderType = pvtype;
                    }
                }
                catch (Exception)
                {
                    mSignUpControl = null;
                }
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // load contents of Role dropdown and set selected index accordingly
                ddlRole.Items.Add(new ListItem("Provider", AHAHelpContent.DbUserType.DbUserTypeProvider.ToString()));
                ddlRole.Items.Add(new ListItem("Volunteer", AHAHelpContent.DbUserType.DbUserTypeVolunteer.ToString()));
                ddlRole.SelectedIndex = (mSignUpControl.ProviderType == AHAHelpContent.DbUserType.DbUserTypeProvider) ? 0 : 1;


                // load appropriate consent agreement
                //LoadConsentAgreement();
            }
        }

/**
        private void LoadConsentAgreement()
        {

            if(ddlRole.SelectedValue.Equals( AHAHelpContent.DbUserType.DbUserTypeProvider.ToString() ) )
            {
                tbAgreement.Text = "TODO: Insert Provider Terms of Use.";  // GetGlobalResourceObject("Provider", "TermsOfUse_Provider").ToString();

            }
            else
            {
                tbAgreement.Text = "TODO: Insert Volunteer Terms of Use."; // GetGlobalResourceObject("Provider", "TermsOfUse_Volunteer").ToString();
            }

        }
 **/

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRole.SelectedValue.Equals(AHAHelpContent.DbUserType.DbUserTypeProvider.ToString()))
                mSignUpControl.ProviderType = AHAHelpContent.DbUserType.DbUserTypeProvider;
            else if (ddlRole.SelectedValue.Equals(AHAHelpContent.DbUserType.DbUserTypeVolunteer.ToString()))
                mSignUpControl.ProviderType = AHAHelpContent.DbUserType.DbUserTypeVolunteer;

            //LoadConsentAgreement();
        }

        /**
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            // Validate everything

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            // Go back to landing page
            Response.Redirect("~" + AppSettings.Url.ProviderPortal);
            Response.End();
        }
         **/
    }
}