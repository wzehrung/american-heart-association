﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Microsoft.Health;
using Microsoft.Health.Web;
using Microsoft.Health.ItemTypes;
using System.IO;
using System.Collections;

namespace Heart360Provider
{
    /// <summary>
    /// Summary description for PhotoHandler
    /// </summary>
    public class PhotoHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string header = context.Request.Headers["If-Modified-Since"];

            if (header != null)
            {
                context.Response.StatusCode = 304;
                context.Response.SuppressContent = true;
                return;
            }

            HealthRecordAccessor objHRI = HVManager.Helper.GetAccessor(Guid hvpersonid, Guid hvrecordid);

            AHAProvider.ImageItem objImageItem = AHACommon.PersonalImageHelper.GetPersonalImageBytes(objHRI);

            context.Response.Clear();
            context.Response.Buffer = false;
            context.Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            context.Response.Cache.SetMaxAge(new TimeSpan(1, 0, 0, 0));
            context.Response.Cache.SetLastModified(DateTime.Now);
            context.Response.Cache.SetCacheability(HttpCacheability.Public);
            context.Response.Cache.SetValidUntilExpires(false);

            context.Response.AddHeader("Content-Length", objImageItem.ImageByteArray.Length.ToString());
            context.Response.ContentType = "image/JPEG";
            context.Response.AddHeader("content-disposition", "inline; filename=img_" + RecordID.Value);

            context.Response.BinaryWrite(objImageItem.ImageByteArray);
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}