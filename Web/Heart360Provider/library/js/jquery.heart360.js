//Heart 360 Javascript File


$(document).ready(function () {

    //Define screen sizes
    var mobilesize = 767;
    var tabletsize = 760;
    var desktopsize = 1024;

    var viewport_width = $(window).width();

    //jQuery Custom jQuery slideshow plugin by Push Design
    jQuery('#slideshow-container').bearSlider();
    jQuery('.tip').tipr();

    //Check for mobile first
    if (viewport_width <= tabletsize) {
        //Mobile stuff

        //Set the search box size
        $('#search-box-wrapper').css({ width: viewport_width });

        //Unbind Event
        $('#mobile-search-button').unbind();

        //Add click functionality to search
        $('#mobile-search-button').on('click', function () {
            $('#search-box-wrapper').slideToggle();

        });

        //Hide all accordions on page load
        jQuery('.mobile-content').hide();

        //Clear the unbind before we keep re-binding
        jQuery(".mobile-trigger").unbind();

        //Assign click event to all accordion-trigger tags
        jQuery('.mobile-trigger').on('click', function () {

            if (jQuery(this).hasClass('mobile-trigger-selected')) {
                //add selected class to newly opened accordion

                jQuery(this).removeClass('mobile-trigger-selected');
                jQuery(this).next('.mobile-content').slideUp();

            } else {

                //Animate up the div and on return remove its selected class
                // jQuery('.mobile-content').slideUp('fast', function() {});

                //add selected class to newly opened accordion
                jQuery(this).addClass('mobile-trigger-selected');
                jQuery(this).next('.mobile-content').slideDown();

            }

        });

    } else {

        //Hide all accordions on page load
        jQuery('.mobile-content').show();

        //Clear the unbind before we keep re-binding
        jQuery(".mobile-trigger").unbind();

    }

    //All Resizing Events

    $(window).resize(function () {

        //Reset the viewport width on resize
        var viewport_width = $(window).width();

        //Check for mobile first
        if ($(window).width() <= tabletsize) {

            //Mobile stuff

            //Unbind first
            $('#mobile-search-button').unbind();

            //Add click functionality to search
            $('#mobile-search-button').on('click', function () {
                $('#search-box-wrapper').slideToggle();
            });

            //Hide all accordions on page load
            jQuery('.mobile-content').hide();

            jQuery(".mobile-trigger").unbind();

            //Assign click event to all accordion-trigger tags
            jQuery('.mobile-trigger').on('click', function () {

                if (jQuery(this).hasClass('mobile-trigger-selected')) {

                    //add selected class to newly opened accordion
                    jQuery(this).removeClass('mobile-trigger-selected');
                    jQuery(this).next('.mobile-content').slideUp();


                } else {

                    //Animate up the div and on return remove its selected class
                    // jQuery('.mobile-content').slideUp('fast', function() {});

                    //add selected class to newly opened accordion
                    jQuery(this).addClass('mobile-trigger-selected');
                    jQuery(this).next('.mobile-content').slideDown();

                }


            });


        } else {
            //Desktop size

            //Hide all accordions on page load
            jQuery('.mobile-content').show();

            //Clear the unbind before we keep re-binding
            jQuery(".mobile-trigger").unbind();

        }

    });

    /*
    Tab Structure
    */
    var assignTabActions = function () {

        //check for tab declaration
        if ($('.tabs-header').length > 0 && $('.tabs-content').length > 0) {

            $('.tab').unbind();
            $('.tab').each(function () {

                var connectedTab = $(this).attr('data-tabcontent');

                if ($('.' + connectedTab).length > 0) {

                    if ($(this).hasClass('active')) {
                        $('.' + connectedTab).addClass('active');
                    }

                    $(this).on('click', function () {
                        if ($(this).hasClass('active')) {
                            //Do nothing
                        } else {
                            $('.tab-content').removeClass('active');
                            $('.tab').removeClass('active');
                            $(this).addClass('active');
                            $('.' + connectedTab).addClass('active');
                        }
                    });

                } else {
                    // console.log("Tab content specified is missing");
                }

            });
        }
    }

    /*************************
    GENERAL ACCORDIONS 
    Used on most pages
    **************************/

    /***
    
    Used for vanilla asp update panels to be called and reassign accordion actions

    ***/

    window.aspAccordionSetup = function () {



        $(".general-trigger.asp-trigger").unbind();
        $(".general-trigger.asp-trigger").each(function (index) {

            if ($(this).hasClass('general-trigger-selected')) {

            } else {
                $(this).parent('.accordion-container').children('.general-content').hide();
            }



            //Unbind this
            $(this).unbind();

            //Check if inner-bind exists
            if ($(this).children().hasClass('inner-bind')) {

                //Go through each element with binding class
                $(this).children('.inner-bind').each(function (index2) {

                    //Unbind this just to be sure
                    $(this).unbind();

                    $(this).on('click', function () {

                        if ($(this).parent('.accordion-container').children('.general-content').attr('style') == "display: block;") {
                            (this).parent('.accordion-container').hide('.general-content')
                        } else {

                            if ($(this).parent('.general-trigger').hasClass('general-trigger-selected')) {

                                $(this).parent('.general-trigger').removeClass('general-trigger-selected');
                                $(this).parent('.general-trigger').parent('.accordion-container').children('.general-content').slideUp();

                            } else {

                                //Animate up the div and on return remove its selected class
                                $(this).parent('.general-trigger').parent('.accordion-container').children('.general-content').slideUp('fast', function () {
                                    $(this).parent('.general-trigger').parent('.accordion-container').children('.general-trigger').removeClass('general-trigger-selected');
                                });

                                //add selected class to newly opened accordion
                                $(this).parent('.general-trigger').parent('.accordion-container').children('.general-trigger').addClass('general-trigger-selected');
                                $(this).parent('.general-trigger').parent('.accordion-container').children('.general-content').slideDown();

                            }

                        }

                    });
                })


            } else {
                //Unbind this just to be sure
                $(this).unbind();

                //Assign click event to all accordion-trigger tags
                $(this).on('click', function () {

                    if ($(this).hasClass('general-trigger-selected')) {
                        //add selected class to newly opened accordion
                        $(this).parent('.accordion-container').children('.general-trigger').removeClass('general-trigger-selected');
                        $(this).parent('.accordion-container').children('.general-content').slideUp();

                    } else {

                        //Animate up the div and on return remove its selected class
                        $(this).parent('.accordion-container').children('.general-content').slideUp('fast', function () {
                            $(this).parent('.accordion-container').children('.general-trigger').removeClass('general-trigger-selected');

                        });

                        //add selected class to newly opened accordion
                        $(this).parent('.accordion-container').children('.general-trigger').addClass('general-trigger-selected');
                        $(this).parent('.accordion-container').children('.general-content').slideDown();

                    }

                });

            } //End else if statement

        }); //End .each for .general-trigger
    }

    /**

    Style Tables
    - Adds a class to every other row to be used with css
    
    **/

    window.addTableRowClasses = function () {

        if ($('.styled-table').length > 0) {
            $('.styled-table').each(function (i1) {

                //Run through each row
                $(this).find('tr').each(function (i2) {
                    if (i2 % 2 != 0) {
                        $(this).addClass('color-row');
                    }
                });

            });
        }
    }

    var assignAccordionActions = function (initLoad, parent_html_container) {

        parent_html_container.find(".general-trigger").unbind();
        parent_html_container.find(".general-trigger").each(function (index) {

            var current_parent_trigger = $(this);

            //Hide all accordions on page load
            if (initLoad == true) {
                $(this).parent('.accordion-container').children('.general-content').hide();
            }

            //Check if inner-bind exists
            if ($(this).children().hasClass('inner-bind')) {

                //Go through each element with binding class
                $(this).children('.inner-bind').each(function (index2) {
                    //Unbind this just to be sure
                    $(this).unbind();

                    $(this).on('click', function () {

                        if ($(this).parent('.general-trigger').hasClass('general-trigger-selected')) {

                            $(this).parent('.general-trigger').removeClass('general-trigger-selected');
                            $(this).parent('.general-trigger').parent('.accordion-container').children('.general-content').slideUp();

                        } else {

                            //Animate up the div and on return remove its selected class
                            $(this).parent('.general-trigger').parent('.accordion-container').children('.general-content').slideUp('fast', function () {
                                $(this).parent('.general-trigger').parent('.accordion-container').children('.general-trigger').removeClass('general-trigger-selected');
                            });

                            //add selected class to newly opened accordion
                            $(this).parent('.general-trigger').parent('.accordion-container').children('.general-trigger').addClass('general-trigger-selected');
                            $(this).parent('.general-trigger').parent('.accordion-container').children('.general-content').slideDown();

                        }

                    });
                })


            } else {
                //Unbind this just to be sure
                $(this).unbind();

                //Assign click event to all accordion-trigger tags
                $(this).on('click', function () {

                    if ($(this).hasClass('general-trigger-selected')) {
                        //add selected class to newly opened accordion

                        $(this).parent('.accordion-container').children('.general-trigger').removeClass('general-trigger-selected');
                        $(this).parent('.accordion-container').children('.general-content').slideUp();

                    } else {

                        //Animate up the div and on return remove its selected class
                        $(this).parent('.accordion-container').children('.general-content').slideUp('fast', function () {
                            $(this).parent('.accordion-container').children('.general-trigger').removeClass('general-trigger-selected');
                        });

                        //add selected class to newly opened accordion
                        $(this).parent('.accordion-container').children('.general-trigger').addClass('general-trigger-selected');
                        $(this).parent('.accordion-container').children('.general-content').slideDown();

                    }

                });

            } //End else if statement

        }); //End .each for .general-trigger
    }

    var createPopupActions = function () {
        removedWrap = false;

        $('.popuplink').unbind();

        // Popup display event
        $('.popuplink').on('click', function () {
            $('.popup-message').html('');
            $('.popup-message').hide();
            loadDynamicData($(this));
            $('.popup-container').fadeIn();

            if ($(window).width() < 768) {
                $('.container').fadeOut();
                removedWrap = true;
            }

        });
    }

    /* ----- Repeater -----
    * Setup a template of the fields to be repeated (div class repeat-area)
    * Place a div of class repeater-fields where they should show up in the page
    * Use create-fields to add more repeats, and remove-fields to erase a repeat
    * Add a hidden input with an ID of 'repeater-number' to get the max repeater value (optional)
    */
    var createRepeaterActions = function (parentHTMLContainer) {

        var repeaterFieldsClass = '.repeater-fields';
        var repeaterAreaClass = '.repeat-area';
        var repeaterActionClass = '.create-fields';
        var repeaterRemoveActionClass = '.remove-fields';
        var repeaterNumberField = '#repeater-number';
        var repeaterNumber = 0;

        //Check how many repeater areas there are and assign the first one a unique class numner

        //First check to see if repeater target exists
        if ($(parentHTMLContainer).find(repeaterFieldsClass).length > 0) {

            //  console.log("initializing repeater...");

            //Then check if there is a template area for the repeater content
            if ($(parentHTMLContainer).find(repeaterAreaClass).length > 0) {

                //If it's not already there, add our first inital number to our repeater
                if (!$('.repeater-area-0').length > 0) {
                    $(parentHTMLContainer).find(repeaterAreaClass).addClass('repeater-area-' + repeaterNumber);
                } else {
                    //repeaters are already present, may need to wire them up
                    //start at 1, as 0 doesn't get a remove button
                    var existingRepeaterNumber = 1;
                    var existingRepeaterArea = '.repeater-area-1';
                    while ($(existingRepeaterArea).length > 0) {
                        var removeAction = $(existingRepeaterArea).find(repeaterRemoveActionClass);

                        //unhide the delete button - the first one isn't deleteable
                        removeAction.removeClass("force-hide");
                        removeAction.on('click', function () {
                            $(this).parent().parent().remove();
                        });

                        existingRepeaterArea = '.repeater-area-' + ++existingRepeaterNumber;
                    }
                }

                repeaterNumber++;

                //Then check if there is an action for the repeater
                if ($(parentHTMLContainer).find(repeaterActionClass).length > 0) {

                    $(parentHTMLContainer).find(repeaterActionClass).unbind();

                    $('<div class="' + repeaterAreaClass.replace(/\./g, "") + '"></div>').append($(parentHTMLContainer).find(repeaterAreaClass).html()).html();

                    var originalHTML = $(parentHTMLContainer).find('.repeater-area-0').html();

                    //Create a new repeat button
                    $(parentHTMLContainer).find(repeaterActionClass).on('click', function () {

                        //Create and store new repeater class
                        var newRepeaterAreaClass = 'repeater-area-' + repeaterNumber;

                        //If repeater area is greater than 0 we need to make make max height 500px
                        var curPopupHeight = $('.popup-body').height();

                        $('.popup-body').css({ 'max-height': curPopupHeight });


                        while ($('.' + newRepeaterAreaClass).length > 0) {
                            // console.log("Found exisitng repeater at: " + newRepeaterAreaClass);
                            newRepeaterAreaClass = 'repeater-area-' + ++repeaterNumber;
                        }

                        //Re-create the class after we know now that this class is unique
                        var newRepeaterAreaClass = 'repeater-area-' + repeaterNumber;


                        //Store new html container
                        var storedHTML = $('<div class="' + repeaterAreaClass.replace(/\./g, "") + ' ' + newRepeaterAreaClass + '"></div>');

                        //Add new html to repeater area container to repeater area
                        $(repeaterFieldsClass).append(storedHTML);

                        //Add copied template fields to our new container div, Return Vanilla HTML object
                        var newerHTML = $(originalHTML)[0];

                        //Using Vanilla JS clone HTML, no Jquery here
                        var newerHTML = newerHTML.cloneNode(true);

                        //grab all the input, select, textarea, and button tags in the repeat area
                        var repeaterHTMLFields = $(newerHTML).find(':input');

                        repeaterHTMLFields.each(function (index) {

                            var type = $(repeaterHTMLFields[index]).attr('type') || repeaterHTMLFields[index].tagName.toLowerCase();

                            if (type == 'text' || type == 'textarea') {
                                $(repeaterHTMLFields[index]).val('');
                            } else if (type == 'radio' || type == 'checkbox') {
                                if ($(repeaterHTMLFields[index]).attr('default') !== undefined) {
                                    $(repeaterHTMLFields[index]).prop("checked", true);
                                } else {
                                    $(repeaterHTMLFields[index]).prop("checked", false);
                                }
                            } else if (type == 'select') {
                                $(repeaterHTMLFields[index]).val('');
                            }

                            //Get name and add dynamic number to it and check if a field exists with it itfirst
                            var curName = $(repeaterHTMLFields[index]).attr('name');
                            var curID = $(repeaterHTMLFields[index]).attr('id');

                            var newFieldName = curName + '-' + repeaterNumber;
                            var newFieldID = curID + '-' + repeaterNumber;

                            //Change Name
                            $(repeaterHTMLFields[index]).attr('name', newFieldName);

                            //Change ID
                            $(repeaterHTMLFields[index]).attr('id', newFieldID);

                        });


                        $('.' + newRepeaterAreaClass).append(newerHTML);

                        //wire up removal action for the new repeat
                        var removeAction = $("." + newRepeaterAreaClass).find(repeaterRemoveActionClass);

                        //unhide the delete button - the first one isn't deleteable
                        removeAction.removeClass("force-hide");
                        removeAction.on('click', function () {
                            $(this).parent().parent().remove();
                        });

                        //set max repeater number for form, if field present
                        if ($(repeaterNumberField).length > 0) {
                            $(repeaterNumberField).val(repeaterNumber);
                        }

                        //call optional on-page function
                        if ($(parentHTMLContainer).find(repeaterActionClass).attr('data-function') != undefined) {
                            var fnName = $(parentHTMLContainer).find(repeaterActionClass).attr('data-function');
                            var fn = window[fnName];
                            fn();
                        }

                        repeaterNumber++;

                    });

                    //Now we have a fully functional repeater
                } else {
                    // console.log("Repeater action class missing (create-fields)");
                }
            } else {
                // console.log("Repeater content not found (repeat-area)");
            }
        } else {
            // console.log("Repeater target does not exist");
        }
    }

    var postActions = function (parentHTMLContainer, parentMessageContainer, completePageMethod, refreshids, isPrint, isPopup) {

        //show spinner after button press
        var loaderHtml = '<div class="dynamic-loader"><div class="dynamic-loader-gif"><img src="/library/images/ajax-loader.gif" /></div></div>';
        $(parentHTMLContainer).prepend(loaderHtml);
        $('.dynamic-loader').fadeIn('slow');

        //Data form fiels being passed to the PageMethod object
        var fields = $(parentHTMLContainer).find('#data-form').find(':input').serializeArray();

        if (isPrint === true) { fields.push({ name: 'print', value: isPrint }); }

        //inital JSON String with Empty Inputs
        var jsonstr = JSON.stringify({ INIT: 'false', inputs: fields });

        var building_json;
        if (isPrint === true) {

            building_json = {
                url: completePageMethod,
                type: "POST",
                async: false,
                data: jsonstr,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }

        } else {

            building_json = {
                url: completePageMethod,
                type: "POST",
                data: jsonstr,
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }

        }

        //Send in generated JSON object
        var sendreceivedata = $.ajax(building_json);

        sendreceivedata.done(function (msg) {

            $('.dynamic-loader').fadeOut('fast', function () {
                $(parentHTMLContainer).remove('.dynamic-loader');
            });

            //Remove error class by default, it will be added if needed later
            $(parentMessageContainer).removeClass('error-message');

            var jsonStuff = $.parseJSON(msg.d);

            //When submitted check for any errors that come bacack
            if (jsonStuff[0].success == 'true') {

                // Clear html and add success message if its popup
                if (isPopup) {
                    $(parentHTMLContainer).html('');
                    $(parentMessageContainer).show();
                    //Display success message in HTML area
                    $(parentHTMLContainer).html(jsonStuff[0].message);
                } else {
                    //Display success message in message area
                    $(parentMessageContainer).html(jsonStuff[0].message);
                }

                //Fade in message if the message is not blank
                if (jsonStuff[0].message.length > 0) {
                    if (isPopup) {
                        $(parentMessageContainer).hide();
                    } else {
                        $(parentMessageContainer).fadeIn();
                    }
                }

                //On Click events for OK button
                $('.cancel-popup').unbind();
                $('.cancel-popup').on('click', function () {
                    $('.popup-container').fadeOut();
                });

                if (jsonStuff[0].refresh == 'true' && refreshids.length > 0) {
                    //Do refresh of all panels here
                    loadAllModules(refreshids);
                }

            } else if (jsonStuff[0].success == 'redirect') {

                //Redirect to the URI specified in the message
                window.location.replace(jsonStuff[0].message);

            } else if (jsonStuff[0].success == 'print') {

                //Create window
                var myWindow = window.open('', '', 'width=1000,height=800,resizable,scrollbars,status');

                myWindow.document.write(jsonStuff[0].message);

                myWindow.document.close();

                myWindow.focus();
                myWindow.print();
                myWindow.close();

                //Close the original popup following print dialog completion
                $('.print-popup').closest('.popup-container').fadeOut();

            } else {

                //Add Failure message
                $(parentMessageContainer).html(jsonStuff[0].message);

                //Fade in message if the message is not blank
                if (jsonStuff[0].message.length > 0) { $(parentMessageContainer).fadeIn(); }

                $(parentMessageContainer).addClass('error-message');

                //iPad fix scrolling fix
                if (isPopup && $(window).width() > 768) {
                    $(parentHTMLContainer).css('height', '400px');
                }
            }

        });

        //Deafult Return with no print return
        return false;
    }

    var createPopupSubmitActions = function (parentHTMLContainer, parentMessageContainer, completePageMethod, refreshids, isPopup) {

        $(parentHTMLContainer).find('.submit-popup').unbind();

        $(parentHTMLContainer).find('.submit-popup').on('click', function () {

            var isPrint = false;
            if ($(this).hasClass('print-popup')) {
                isPrint = true;
            }

            postActions(parentHTMLContainer, parentMessageContainer, completePageMethod, refreshids, isPrint, isPopup);

        });


        //Unbind events to make sure we dont double bind
        $('.popup-close-button, .cancel-popup').unbind();

        //On Click events for greybg and close button
        $('.popup-close-button, .cancel-popup').on('click', function () {

            //Bring up the html popup window to start
            $('.popup-container').fadeOut();
            $('.container').fadeIn();


        });
    }


    /******************************
    GENERAL CONTENT & POPUP LOADING
    *******************************/

    var dynamic_class = '.dynamic-content-container';

    var loadDynamicData = function (container) {

        //check if this gets popup or normal page handling
        var isPopup = container.hasClass('popuplink');

        //Store Page
        var aspx_page = container.attr('data-page');
        var aspx_method = container.attr('data-method');
        var aspx_data_id = container.attr('data-id');

        var parent_html_container = container.children('.dynamic-content-html');
        var parent_message_container = container.children('.dynamic-content-message');

        //Did a custom class get passsed?
        var customclass = '';
        if (container.attr('data-customclass')) {
            //Custom class exists lets add it to our main dynamic container
            customclass = container.attr('data-customclass');
        }

        //Did any refresh ids get passed?
        var refreshids = '';
        if (container.attr('data-refresh-ids')) {
            //Custom class exists lets add it to our main dynamic container
            var refreshidsPreSplit = container.attr('data-refresh-ids');
            refreshids = refreshidsPreSplit.split(',');
        }

        //If is popup overwrite dynamic content classes
        if (isPopup) {
            parent_html_container = '.popup-html';
            parent_message_container = '.popup-message';
        }

        //Add custom class variable to main container
        $(parent_message_container).addClass(customclass);

        //Pagemethod to be submitted to 
        var completePageMethod = "/" + aspx_page + ".aspx/" + aspx_method;

        var jsonstr;
        if (aspx_data_id != null) {
            //if data-id is set, include that with the INIT
            jsonstr = JSON.stringify({ INIT: 'true', inputs: [{ 'name': 'id', 'value': aspx_data_id}] });
        } else {
            //inital JSON String with Empty Inputs
            jsonstr = JSON.stringify({ INIT: 'true', inputs: [] });
        }

        //Do first initial loading of ajax
        var dynamic_content = $.ajax({

            type: "POST",
            url: completePageMethod,
            data: jsonstr,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                //Append 
                var loaderHtml = '<div class="dynamic-loader"><div class="dynamic-loader-gif"><img src="/library/images/ajax-loader.gif" /></div></div>';
                $(container).prepend(loaderHtml);
                $('.dynamic-loader').fadeIn('slow');

            }

        });

        //Handle errors
        dynamic_content.fail(function () {

            $(parent_html_container).html('<p>Error communicating with server.</p>');

            $('.dynamic-loader').fadeOut('fast', function () {
                $(container).remove('.dynamic-loader');
            });

        });

        //When returned put the message in the html
        dynamic_content.done(function (msg) {

            //Load in loader for loading loads
            $(container).find('.dynamic-loader').fadeOut('fast', function () {
                $(this).remove('.dynamic-loader');
            });

            //Clear the html first, needed for reloads
            $(parent_html_container).html('');

            //Clear the Message container
            $(parent_message_container).html('');

            //Now hide it
            $(parent_message_container).hide();

            //Load in html that was returned
            $(parent_html_container).html(msg.d);

            //Fade in the main container
            $(parent_html_container).fadeIn('slow');

            if (isPopup) {

                //Clear them first
                $('.dark-bg').unbind();
                $('.popup-close-button').unbind();
                $('.cancel-popup').unbind();

                $('.dark-bg, .popup-close-button, .cancel-popup').on('click', function () {
                    //Potentially we should also clear form fields here?
                    $('.popup-container').fadeOut();
                    $('.container').fadeIn();

                });

                //Clear out min height from repeater popups
                //  $('.popup-body').attr('css', '');

            }

            jQuery('.tip').tipr();

            //add listenter for submit button
            createPopupSubmitActions(parent_html_container, parent_message_container, completePageMethod, refreshids, isPopup);

            //If there needs to be a repeater, let's do it now
            createRepeaterActions(parent_html_container);

            addTableRowClasses();

            //Assign click binding to table headers
            if ($(container).find('.patients-list').length > 0) { $(".patients-list").tablesort(); }

            //For when there is select with submit on
            if ($(container).find('.submit-on-change').length > 0) {

                submitBindUpdateListChange(parent_html_container, parent_message_container, completePageMethod, refreshids, isPopup);

            } else if (isPopup) {
                //Reload and Bind accordions
                // jQuery('.date-pick').datePicker();
                var options = { dateInputNode: $(".date-picker") }
                var instance = new BeatPicker(options);
                assignTabActions();

            } else {
                //Reload and Bind accordions
                assignAccordionActions(true, parent_html_container);
                assignTabActions();
                addTableRowClasses
            }

            //If a reset button is present, a click will reset form
            if ($(container).find('.reset-button').length > 0) {
                $(container).find('.reset-button').unbind();
                $(container).find('.reset-button').on('click', function () {
                    loadDynamicData(container);
                    console.log("should reset");
                });
            }

            //If a cancel button is present, a click will redirect to home
            if ($('.cancel-button').length > 0) {
                $('.cancel-button').unbind();
                $('.cancel-button').on('click', function () {
                    window.location.replace('/');
                });
            }

            if (!isPopup) {
                //On popup we dont want to do this
                createPopupActions();
            }

        });
    }

    var loadAllModules = function (idArray) {

        if (idArray instanceof Array) {
            if (idArray.length > 0) {
                for (var j = 0; idArray.length > j; j++) {
                    loadDynamicData($('#' + idArray[j]));
                }
            } else {
                $(dynamic_class).each(function () {
                    //Make sure to clear data(html) first in the inside function
                    loadDynamicData($(this));
                });
            }
        } else {
            $(dynamic_class).each(function () {
                //Make sure to clear data(html) first in the inside function
                loadDynamicData($(this));
            });
        }
    }

    //Run load all modules on page start
    loadAllModules();

    //Load on start accordion actions
    $(".general-trigger").unbind();
    $(".general-trigger").each(function (index) {
        var parent_html_container = $(".general-trigger").parent('.accordion-container');
        assignAccordionActions(true, parent_html_container);
    });

    assignTabActions();
    createPopupActions();
    addTableRowClasses();


    //On Click events for greybg and close button
    $('.dark-bg, .popup-close-button').on('click', function () {
        //Bring up the html popup window to start
        $('.popup-container').fadeOut();
        $('.container').fadeIn();
    });


    var submitBindUpdateListChange = function (parentHTMLContainer, parentMessageContainer, completePageMethod, refreshids, isPopup) {
        // ddlGroup
        $(parentHTMLContainer).find('.submit-on-change').unbind();
        $(parentHTMLContainer).find('.submit-on-change').change(function () {
            postActions(parentHTMLContainer, parentMessageContainer, completePageMethod, refreshids, isPopup);
            //Reload and Bind accordions
            assignAccordionActions(false, parent_html_container);
        });

    }

    window.messageUpdater = function (msgid, usrid, domrefid) {
        //inital JSON String with Empty Inputs
        var jsonstr = JSON.stringify({ ReadMail: msgid, UserId: usrid });

        var request = $.ajax({
            url: "/Pages/Messages.aspx/ReadMessageHandler",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: jsonstr
        });

        request.done(function (msg) {

            var jsonStuff = $.parseJSON(msg.d);

            //Update number of unread messages
            $('#messages .middle-tile .tile-value .right-value').html(jsonStuff[0].msgunread);

            // Removed unread classes from dom elements
            $('#' + domrefid).children('.msg-unread').removeClass('msg-unread');

            // remove onclick event
            $('#' + domrefid).attr('onclick', '');


        });

    }


    /**************************************
    --- END GENERAL CONTENT & POPUP LOADING
    ***************************************/

});



