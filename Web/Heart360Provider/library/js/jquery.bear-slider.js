(function ( $ ) {

    $.fn.bearSlider = function(options) {

		// Push slider options
		var settings = $.extend({

			// These are the defaults.
			autoplay : "yes",
			slideDuration : '3000',
			fadeDuration : '3000',
			captionAnimationDuration : '200',
			captionFadInDur : '3000',
			captionFadeOutDur : '2000',
			blendCaption : 'no',
			removeCaption : 'no',
			removePagination : 'no',
			removeImage : 'no',
			slideOrFade : 'slide',
			slideshowContainer : '#slideshow-container',
			windowSelector : '#bear-slide-window',
			innerWindowSelector : '#bear-inner-window',
			paginationSelector : '#bear-slide-pagination',
			paginationButtonSelector : '.bear-pagination-button',
			captionWindowSelector : '#bear-caption-window',
			slideSelector : '.bear-slide', //Selector for each slide used
			slideImageSelector : '.bear-image',
			slideCaptionSelector : '.bear-caption',
			slideActiveSelector : '.bear-active',
			makeImageBackgroundImage: false, //True will take the image and make it acss bg image.
			debug : false

		}, options );


		var fading = null,
			bear_errors = Array();
			slideTimer = Array(), //Containers the slider timer
			totalSlides = 0 , //Total number of slides
			activeSlide = 0, //Index of active slide
			newSlide = 0, //Index of upcoming slide
			allSlides =  $(settings.slideshowContainer).find(settings.slideSelector), //Store all slidesin a jquery object
			paginationList = $(settings.slideshowContainer).find(settings.paginationSelector),
			paginationButtons = null;

		/*-----
		
			Helper Functions
			Set Errors
			Erros Exist
			Debuging
			Logging

		------*/
			function setError(bear_error) {
				
				if(settings.debug === true) {
					bear_errors.push(bear_error);
					console.log(bear_error);
				} else {
					//Dont log anything
				}
			} //End set error function

			function errorsExist() {
				if(bear_errors.length > 0) {
					return true;
				} else {
					return false;
				}
			} // End errors exist

			function debugApplication() {

				if(settings.debug === true) {
					
					//Check for slideshow container
					if($(settings.slideshowContainer).length > 0) {

						//Now check for outer window div
						if($(settings.windowSelector).length > 0) {

							//Check Inner window div
							if($(settings.innerWindowSelector).length > 0) {


								if($(settings.paginationSelector).length > 0 && settings.removePagination == 'no' ) {

									
									if($(settings.slideSelector).length > 0) {


										if($(settings.slideImageSelector).length > 0 && settings.removeImage == 'no') {


												if($(settings.slideCaptionSelector).length > 0 && settings.removeCaption == 'no') {


													//Okay We are successful, the entire slide structure is in place
											
										
												} else {

													setError("Could not find  (" + settings.slideCaptionSelector + '), and you specified it should bear there. Its also possible there is a broken/mismatched div on the page causing this error to occur.');
												}

											
										
										} else {

											setError("Could not find (" + settings.slideImageSelector + '), and you specified it in settings it should be there. Its also possible there is a broken/mismatched div on the page causing this error to occur.');
										}


									} else {

										setError("Could not find (" + settings.slideSelector + '), you need atleast one slide div for Bear slides to work. Its also possible there is a broken/mismatched div on the page causing this error to occur.');
									}

								
								} else {

									setError("Could not find (" + settings.paginationSelector + '), in your html document the settings say you need it, did you forget to add it in? Maybe another error, or broken div on the page is causing an issue.');
								}
								

							} else {
								setError("Could not find  " + settings.innerWindowSelector + 'in your html document , did you forget to add it in? Maybe another error, or broken div on the page is causing an issue.');
							}


						} else {
							setError("Could not find " + settings.windowSelector + 'in your html document , did you forget to add it in? Maybe another error, or broken div on the page is causing an issue.');
						}

					} else {
						setError("Could not find " + settings.slideshowContainer + 'in your html document , did you forget to add it in? Maybe another error, or broken div on the page is causing an issue.');
					}


				} else {

					//Dont debug
				}
			} //End debug application function

			function __logAction(action) {
				if(settings.debug === true) {
					//We may want to see if it exists or not on page
					window.console.log(action);
				} else {
					//Cant log anything debug is off
				}
			} // End log action function

			//Custom color parsing function
			function returnColorString(passedclass) {
				// if(passedclass.length > 0) {

				// 	var classesArray = passedclass.split(" ");
					
				// 	for(t=0; t < classesArray.length; t++) {

				// 		if(classesArray[t].indexOf('bgcolor') > -1) {
							
				// 			//Found it now split up the two parts
				// 			var singleClassSplit = classesArray[t].split("-");
				// 			var currentBgColor =  singleClassSplit[0];
							
				// 			return currentBgColor;

				// 		} else {
				// 			//Didnt find it
				// 			return false
				// 		}

				// 	}
				// } else {
				// 	return false;
				// }
			}
			
			//Find BG color
			function findBGColor() {

				//Find this slides bg color
				var classesToSplit = allSlides.eq(newSlide).attr('class');
				
				var currentBgColor = returnColorString(classesToSplit);

				//Incase they didnt add a bg color
				if(currentBgColor === false) { currentBgColor = 'ffffff'; }

				return currentBgColor;
			}

		/*-----------

			Slide Functions

		------------*/

			//Settup Pagination
			function setupPagination() {

				//Go through each page and add a button to our pagination list
				for(var i = 0; i < totalSlides; i++){
					paginationList.prepend('<li class="'+ settings.paginationButtonSelector.replace(/\./g, "") + '" data-target="'+i+'"></li>');
				}

				paginationButtons = $(settings.windowSelector).find(settings.paginationButtonSelector);

				//Log out all buttons found
				__logAction("Pagination Buttons Found and Added: " + i);

				//Add the the active class to our first pagination button
				paginationButtons.eq(0).addClass(settings.slideActiveSelector.replace(/\./g, ""));
				
				//Bind click
				$(settings.paginationButtonSelector).on('click', function() {

					var target = $(this).attr('data-target');
					clearTimeout(slideTimer);
					changeSlides(target);

					//Log when pagination button gets clicked
					__logAction("Pagination Button Clicked - Target " + target);

				});
			}
			
			function changeSlides(target) {

				//If the user already clicked a pagination button we dont want them to click again.
				if(fading){
					__logAction("Returning False " + activeSlide);
					return false;
				}

				fading = true;

				//Remove the active classes and then add the new slides active pagination class
				paginationButtons.removeClass(settings.slideActiveSelector)
				.eq(newSlide)
				.addClass(settings.slideActiveSelector.replace(/\./g, "") );


				// Settup framework to change slides
				// Depending on the passed in option

				__logAction("Target = " + target + " Current Slide = " + activeSlide);

				if(activeSlide == target) {

					//Do nothing, we are already on this slide
					__logAction("Target the same");

					fading = false;
					return false;


				} else if(target === 'next') {

					// Go to next slide

					newSlide = activeSlide + 1;

					// Check if we're at the end of the stack
					// If so then loop back to the start

					if(newSlide > totalSlides - 1) {
						newSlide = 0;
					}

					// Normally i would pass variables in but they are in this case,
					// global so i can access them outside this function

					animateSlides();

					__logAction("Changing Slides");


				}  else if(target === 'prev') {

					//Target the previous slide
					newSlide = activeSlide - 1;
		
					// Check if we're at the start of the stac
					// and if so, loop back to the end
		
					if(newSlide < 0) {
						newSlide = totalSlides - 1;
					}

					// Normally i would pass variables in but they are in this case,
					// global so i can access them outside this function

					animateSlides();

					__logAction("Changing Slides");


				}  else {
					//If not next or prev, target must be an integer,
					// so target is newSlide

					newSlide = target;

					// Normally i would pass variables in but they are in this case,
					// global so i can access them outside this function

					animateSlides();

					__logAction("Changing Slides");

				}			

			} //End change slides

			function settupSlideCSS(status) {

				var currentBgColor = findBGColor();

				var currentWindowWidth = $(settings.windowSelector).width();

				if(status == 'start') { tempCurrentSlide = 0; } else { tempCurrentSlide = newSlide; }

				var startSlideImage = allSlides.eq(tempCurrentSlide).find(settings.slideImageSelector +  ' img');
				var slideCSSOptions = {
					'display' : 'block',
					'opacity': 1,
					'background-color' : '#' + currentBgColor,
					'position' : 'absolute',
					'top' : '0'
				};

				if(settings.makeImageBackgroundImage == true) {
					slideCSSOptions['background-image'] = "url(" + startSlideImage.attr('src') + ")";
				}

				if(status == 'start') {
					slideCSSOptions['right'] = '0';
				} else {
					if(settings.slideOrFade) {
						slideCSSOptions['right'] = '-' + currentWindowWidth + 'px';
					} else {
						slideCSSOptions['right'] = '0';
					}
				}

				var currentWindowWidth = $(settings.windowSelector).width();

				allSlides.eq(tempCurrentSlide).css(slideCSSOptions);


			}

			function slideAnimation(slide) {

				if(slide == 'start') { 
					
					//Settup the CSS for the slide
					settupSlideCSS(slide);

					allSlides.eq(0).find(settings.slideCaptionSelector).fadeIn();

					var oldMousePos = { x: 0, y: 0 };
					var currentMousePos = { x: 0, y: 0 };
					var draggingSlide = false;

					//Bind Mouse down so users can drag to the next slide
					var amountToAnimate = 0;

					/*
						$(settings.slideSelector).mousemove(function(event) {

							//Is the slide in dragging state
							if(draggingSlide === true) {

								 currentMousePos.x = event.pageX;
					       		 currentMousePos.y = event.pageY;

									if(currentMousePos.x > oldMousePos.x) {
						       		 	
						       		 	//Moving Right
						       		 	//Animate both slides towards how far the mouse is left or right

					       		 		amountToAnimate = amountToAnimate + (currentMousePos.x - oldMousePos.x);

					       		 		//Animate the slide on the right the same pace as the current slide
										allSlides.eq(activeSlide).css({ 'left' : amountToAnimate });

										//Animate the slide on the right the same pace as the current slide
										allSlides.eq(newSlide).css({ 'left' : amountToAnimate });


						       		 } else if(currentMousePos.x < oldMousePos.x) {

						       		 	//Moveing Left

						       		 	var tempCurrentLeft = allSlides.eq(activeSlide).position();

						       		 	amountToAnimate = tempCurrentLeft.left + (currentMousePos.x - oldMousePos.x);

					       		 		//Animate the slide on the right the same pace as the current slide
										allSlides.eq(activeSlide).css({ 'left' : amountToAnimate });

										//Animate the slide on the right the same pace as the current slide
										allSlides.eq(newSlide).css({ 'left' : amountToAnimate });


						       		}
						       	
						       	 oldMousePos.x = currentMousePos.x;
					       		 oldMousePos.y = currentMousePos.y;
					       	}

						});

						$(settings.slideSelector).mousedown(function(event) {

						 	 currentMousePos.x = event.pageX;
				       		 currentMousePos.y = event.pageY;

				       		 draggingSlide = true;

				       		 oldMousePos.x = currentMousePos.x;
				       		 oldMousePos.y = currentMousePos.y;

				       		 //Animate the current and newSlide

				       		 //Stop Slide animation 
				       		 clearTimeout(slideTimer);

						});

						$(settings.slideSelector).mouseup(function(event) {
							draggingSlide = false;
						});

					*/

				} else {
					
					//Store Current Window Width;
					var currentWindowWidth = $(settings.windowSelector).width();
					
					settupSlideCSS(slide);

					//Fade in caption
					allSlides.eq(newSlide).find(settings.slideCaptionSelector).fadeIn();
			
					//Change the pagination to the active slide
					paginationButtons.eq(newSlide).addClass(settings.slideActiveSelector.replace(/\./g, ""));

					//Remove the old class
					paginationButtons.eq(activeSlide).removeClass(settings.slideActiveSelector.replace(/\./g, ""));

					//Animate the slide on the right the same pace as the current slide
					allSlides.eq(newSlide).animate({ 'right' : 0 }, settings.fadeDuration, function(){});

					//Animate current slide out of view
					allSlides.eq(activeSlide).animate({ 'right' : currentWindowWidth + 'px' }, settings.fadeDuration, function(){
						//CLEAN Up
						cleanUp();
					});
				}
			}

			function fadeAnimation(slide) {

				if(slide == 'start') { 
					
					//Settup the CSS for the slide
					settupSlideCSS(slide);

					allSlides.eq(0).find(settings.slideCaptionSelector).fadeIn();

				
				} else {

					//Move the active slide to have a z-index of 3
					allSlides.eq(activeSlide).css('z-index', 3);
					
					//Settup the CSS for the slide
					settupSlideCSS(slide);


					allSlides.eq(newSlide).find(settings.slideCaptionSelector).fadeIn();

					// Using jquery Animate we need to fade
					// out current slide to reveal slide underneath it

					//Change the pagination to the active slide
					paginationButtons.eq(newSlide).addClass(settings.slideActiveSelector.replace(/\./g, ""));

					//Remove the old class
					paginationButtons.eq(activeSlide).removeClass(settings.slideActiveSelector.replace(/\./g, ""));

					//Fade out current slide
					allSlides.eq(activeSlide).animate({ 'opacity' : 0}, settings.fadeDuration, function(){
						//CLEAN Up
						cleanUp();
					});

				}
			}

			function animateSlides(slide) {

				if(settings.slideOrFade == 'slide') {
					slideAnimation(slide);
				} else {
					fadeAnimation(slide);
				}

				__logAction("Animated Slide " + activeSlide); 

			} //End Animate slides

			function cleanUp() {

				//Remove all styles
				allSlides.eq(activeSlide).removeAttr('style');

				//Set new Active Slide
				activeSlide = newSlide;
				
				//Start the timer again
				fading = false;

				waitForNext();

			} // End clean up function

			function waitForNext() {
				clearTimeout(slideTimer);
				slideTimer = setTimeout(function() {
					changeSlides('next');
				}, settings.slideDuration);

				__logAction("Set timer");
			}

			function start() {
			
				//Figure out tottal number of slides
				totalSlides = allSlides.length;

				//Run through each slide and settup stuff
				allSlides.each(function(index) {

					var tempObjects = allSlides.find(settings.slideImageSelector +  ' img'),
						tempCaptions = allSlides.eq(index).find(settings.slideCaptionSelector);

					//Hide Children
					if(settings.makeImageBackgroundImage == true) { 
						tempObjects.hide(); 
					}

					//Hide caption HTML
					tempCaptions.hide();

				});

				//Start setting up first slide for show
				animateSlides('start');

				// Now that the first slide is active
				// Set the global active to 0
				activeSlide = 0;

				//Start the timer
				if(settings.autoplay === 'yes') {
					waitForNext();
					__logAction("Auto Play Started - Active Slide: " + activeSlide);
				}
				
				//Settup pagination
				setupPagination();


				$('#right-arrow').on('click', function() {
					changeSlides('next');
				});

				__logAction("Started");

			} //End start function


		//Start debugging
		debugApplication();

		//Start Plugin
		if(errorsExist()) {

			//Dont start Errors have been found
			console.log('Failed To Start application, turn on debugging to see why Bear slider failed to start');

		} else {

			start(); //Start the application
		
		}

    };
}( jQuery ));