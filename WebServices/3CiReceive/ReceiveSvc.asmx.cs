﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Data;
using System.Data.Common;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Text;

namespace _3CiReceive
{
    /// <summary>
    /// Summary description for ReceiveSvc
    /// </summary>
    [WebService(Namespace = "http://aha.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ReceiveSvc : System.Web.Services.WebService
    {
        public string strXml = "";
        string POLKA_DATA_SOURCE = "";
        private String RemindersSQL = "SELECT ISNULL(MAX(PolkaDataUniqueID), 0)+1 AS ToolID FROM dbo.[PolkaData] a  ";
        private readonly String InsertSQL = "INSERT INTO [dbo].PolkaData (UserHealthRecordID, PolkaDataUniqueID, ItemType, PolkaData, DateCreated, IsUploadedToHV) VALUES ('{0}', '{1}','{2}','{3}','{4}','{5}')";
        public String PatientRemindersConnectionName = "DBConnStr";
        private readonly String LogInsertSQL = "INSERT INTO [dbo].[3CiPatientReminderLog] (ReminderScheduleID, PatientID, Message, Messagedate, Direction) VALUES ('{0}', '{1}','{2}','{3}','{4}')";

        private readonly String accountLookupSQL = "SELECT patientid, PIN FROM [IVRPatientReminder] WHERE PhoneNumber = '{0}' ";
        private readonly String ivrInsertLogSQL = "INSERT INTO [Heart360].[dbo].[IVRPatientReminderLog] ([PatientID],[Responded],[RetryAttempts],[FinalAction],[ActionDate],[ActionSource]) VALUES ('{0}', '{1}','{2}','{3}', getdate(), 'IVRTrackerService')";
        
        [WebMethod]
        public XmlDocument sendMessage(XmlDocument req)
        {
            Request request = null;
            Response res = new Response();
            XmlDocument retXml = new XmlDocument();
            try
            {
                request = Desearialize(req, new Request());
            }
            catch (Exception ex)
            {
                res.ResponseCode = "100";
                res.ResponseDescription = "Invalid XML format";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }

            bool isValid = false;
            try
            {
                isValid = checkValidResponse(request);
            }
            catch (Exception ex)
            {
                res.ResponseCode = "101";
                res.ResponseDescription = "Invalid Response";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            if (!(isValid))
            {
                res.ResponseCode = "101";
                res.ResponseDescription = "Invalid Response";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            bool storeDBase = false;
            try
            {
                POLKA_DATA_SOURCE = "Mobile";
                storeDBase = storeDB(request);
            }
            catch (Exception ex)
            {
                res.ResponseCode = "102";
                res.ResponseDescription = "Internal Error: Error while storing in the Database";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            if (!(storeDBase))
            {
                res.ResponseCode = "102";
                res.ResponseDescription = "Internal Error: Error while storing in the Database";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }

            res.ResponseCode = "1";
            res.ResponseDescription = "Success";
            string strXml = Serialize(res);
            retXml.LoadXml(strXml);
            return retXml;
        }

        [WebMethod]
        public string testMessage(string req)
        {
            return "Hello";

        }

        public String getAppSetting(String settingName)
        {
            String setting = ConfigurationManager.AppSettings[settingName];
            return setting;
        }

        private bool checkValidResponse(Request req)
        {
            string category = req.Category;
            string Message = req.Value;
            bool isValid = false;
            string[] response = Message.Split(' ');
            string dateInUTC = req.DateOfEvent;

            //System.Diagnostics.EventLog.WriteEntry("3Ci", response[0]);
            //System.Diagnostics.EventLog.WriteEntry("3Ci", response[1]);

            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Weight"))
            {
                if ((response.Length == 2) && ((StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "wt")) || (StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "testwt"))))
                {
                    strXml = "<WeightLogItem WTRecordID='' Weight='" + response[1] + "' ReadingSource='' Comments='' ";
                    isValid = true;
                }
            }

            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Blood Pressure"))
            {
                if ((response.Length == 3) && ((StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "bp")) || (StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "testbp"))))
                {
                    strXml = "<BloodPressureLogItem BPRecordID='' HeartRate='0' Systolic='" + response[1] + "' Diastolic='" + response[2] + "' ReadingSource='' Comments='' ";
                    isValid = true;
                }
            }

            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Cholesterol"))
            {
                if ((response.Length >= 4) && ((StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "ch")) || (StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "testch"))))
                {
                    int TC = Convert.ToInt32(response[1]) + Convert.ToInt32(response[2]) + (Convert.ToInt32(response[3]) / 5);
                    strXml = "<CholesterolLogItem CHRecordID='' Hdl='" + response[1] + "' Ldl='" + response[2] + "' Triglyceride='" + response[3] + "' TotalCholesterol='" + TC.ToString() + "' Location='' ";
                    isValid = true;
                }
            }

            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Blood Glucose"))
            {

                if ((response.Length == 2) && ((StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "bg")) || (StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "testbg")) || (StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "bgtest"))))
                {

                    strXml = "<BloodGlucoseLogItem BGRecordID='' BloodGlucoseValue='" + response[1] + "' ReadingType='' ActionITook='' Notes='' ";
                    isValid = true;
                }
            }

            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Medication"))
            {
                isValid = true;
            }

            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Physical Activity"))
            {
                if ((response.Length == 2) && ((StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "pa")) || (StringComparer.CurrentCultureIgnoreCase.Equals(response[0], "testpa"))))
                {
                    strXml = "<PhysicalActivityLogItem PARecordID='' ExerciseType='' Duration='" + response[1] + "' Intensity='' NumberOfSteps='0' Comments='' ";
                    isValid = true;
                }
            }
            if (POLKA_DATA_SOURCE.ToLower().Equals("ivr"))
            {
                strXml = strXml + " DateMeasuredUTC='" + dateInUTC + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString() + "' />";
            }
            else
            {
                strXml = strXml + " DateMeasuredUTC='" + dateInUTC + "' />";
            }
            //isValid = false;
            return isValid;
        }

        private bool storeDB(Request req)
        {
            string category = req.Category;
            string Message = req.Value;
            bool isValid = false;
            string[] response = Message.Split(' ');
            string dateInUTC = req.DateOfEvent;
            string uniqueID = "";
            string itemType = "";

            try
            {
                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //set the sql command
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(RemindersSQL);

                using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        uniqueID = dataReader["ToolID"].ToString().Trim();

                    }
                }
                //System.Diagnostics.EventLog.WriteEntry("3Ci-UniqueID", uniqueID);
            }
            catch (Exception ex)
            {
                //System.Diagnostics.EventLog.WriteEntry("3Ci-UniqueID", ex.Message);
                throw ex;
            }

            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Weight"))
            {
                itemType = "WeightLogItem";
                strXml = "<WeightLogItem WTRecordID=''" + uniqueID + "'' Weight=''" + response[1] + "'' ReadingSource=''" + POLKA_DATA_SOURCE + "'' Comments='''' ";
                isValid = true;
            }
            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Blood Pressure"))
            {
                itemType = "BloodPressureLogItem";
                strXml = "<BloodPressureLogItem BPRecordID=''" + uniqueID + "'' HeartRate=''0'' Systolic=''" + response[1] + "'' Diastolic=''" + response[2] + "'' ReadingSource=''" + POLKA_DATA_SOURCE + "'' Comments='''' ";
                isValid = true;
            }
            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Cholesterol"))
            {
                itemType = "CholesterolLogItem";
                int TC = 0;
                if ((response[1] == "0") && (response[2] == "0") && (response[3] == "0"))
                {
                    TC = Convert.ToInt32(response[4]);
                    strXml = "<CholesterolLogItem CHRecordID=''" + uniqueID + "'' Hdl='''' Ldl='''' Triglyceride='''' TotalCholesterol=''" + TC.ToString() + "'' Location=''" + POLKA_DATA_SOURCE + "'' ";
                }
                else
                {
                    TC = Convert.ToInt32(response[1]) + Convert.ToInt32(response[2]) + (Convert.ToInt32(response[3]) / 5);
                    strXml = "<CholesterolLogItem CHRecordID=''" + uniqueID + "'' Hdl=''" + response[1] + "'' Ldl=''" + response[2] + "'' Triglyceride=''" + response[3] + "'' TotalCholesterol=''" + TC.ToString() + "'' Location=''" + POLKA_DATA_SOURCE + "'' ";
                }
                isValid = true;
            }
            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Blood Glucose"))
            {
                itemType = "BloodGlucoseLogItem";
                strXml = "<BloodGlucoseLogItem BGRecordID=''" + uniqueID + "'' BloodGlucoseValue=''" + response[1] + "'' ReadingType=''" + POLKA_DATA_SOURCE + "'' ActionITook='''' Notes='''' ";
                isValid = true;
            }
            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Medication"))
            {
                isValid = true;
            }
            if (StringComparer.CurrentCultureIgnoreCase.Equals(category, "Physical Activity"))
            {
                itemType = "PhysicalActivityLogItem";
                strXml = "<PhysicalActivityLogItem PARecordID=''" + uniqueID + "'' ExerciseType=''" + POLKA_DATA_SOURCE + "'' Duration=''" + response[1] + "'' Intensity='''' NumberOfSteps=''0'' Comments='''' ";
                isValid = true;
            }
            if (POLKA_DATA_SOURCE.ToLower().Equals("ivr"))
            {
                strXml = strXml + " DateMeasuredUTC=''" + dateInUTC + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString() + "'' />";
            }
            else
            {
                strXml = strXml + " DateMeasuredUTC=''" + dateInUTC + "'' />";
            }
            //isValid = false;

            try
            {
                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //System.Diagnostics.EventLog.WriteEntry("3Ci-InsertSQL", String.Format(InsertSQL, req.PatientID, uniqueID, itemType, strXml, req.DateOfEvent, "0"));
                //set the sql command
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(String.Format(InsertSQL, req.PatientID, uniqueID, itemType, strXml, req.DateOfEvent, "0"));

                //Execute the command
                dbSQL.ExecuteNonQuery(dbCommand);

                //set the sql command
                DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(LogInsertSQL, req.ReminderScheduleID, req.PatientID, req.Value, req.DateOfEvent, 1));

                //Execute the command
                dbSQL.ExecuteNonQuery(dbCommand1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isValid;
        }

        public String Serialize(Object obj)
        {
            MemoryStream outputStream = new MemoryStream();
            XmlSerializer ser = new XmlSerializer(obj.GetType());//, nameSpace);
            ser.Serialize(outputStream, obj);
            outputStream.Position = 0;

            // return the response
            StreamReader reader = new StreamReader(outputStream);
            return reader.ReadToEnd();
        }

        //public T Desearialize<T>(Stream stream, T obj)
        public T Desearialize<T>(XmlDocument doc, T obj)
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));

            // create the stream
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            String str = doc.InnerXml;

            writer.Write(str);
            writer.Flush();
            stream.Position = 0;

            try
            {
                return (T)xs.Deserialize(stream);
            }

            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("3Ci-DeSerialize", ex.Message);
                throw ex;
            }
        }

        [WebMethod]
        public XmlDocument AccountLookup(XmlDocument accountLookupReq)
        {

            AccountLookupRequest request = null;
            AccountLookupResponse res = new AccountLookupResponse();
            XmlDocument retXml = new XmlDocument();
            try
            {
                request = Desearialize(accountLookupReq, new AccountLookupRequest());
            }
            catch (Exception ex)
            {
                res.IsValid = "N";
                res.PIN = "OOOO";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            //System.Diagnostics.EventLog.WriteEntry("3Ci", request.Password);
            checkValidAccount(res, request.PhoneNumber);
            retXml.LoadXml(Serialize(res));
            return retXml;
        }

        [WebMethod]
        public XmlDocument recordTracker(XmlDocument tracker)
        {
            AHATracker req = null;
            Response res = new Response();
            XmlDocument retXml = new XmlDocument();
            try
            {
                req = Desearialize(tracker, new AHATracker());
            }
            catch (Exception ex)
            {
                res.ResponseCode = "100";
                res.ResponseDescription = "Invalid XML format";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }


            if (String.IsNullOrEmpty(req.TrackerDetails.Type))
            {
                //Handle the Retry attempts. Return success for now.
                res.ResponseCode = "1";
                res.ResponseDescription = "Success";
                retXml.LoadXml(Serialize(res));
                return retXml;
            }


            Request request = new Request();
            request = convertTrackerToRequest(req);

            bool isValid = false;
            try
            {
                isValid = checkValidResponse(request);
            }
            catch (Exception ex)
            {
                res.ResponseCode = "101";
                res.ResponseDescription = "Invalid Response";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            if (!(isValid))
            {
                res.ResponseCode = "101";
                res.ResponseDescription = "Invalid Response";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            bool storeDBase = false;
            try
            {
                POLKA_DATA_SOURCE = "IVR";
                storeDBase = storeDB(request);
            }
            catch (Exception ex)
            {
                res.ResponseCode = "102";
                res.ResponseDescription = "Internal Error: Error while storing in the Database";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            if (!(storeDBase))
            {
                res.ResponseCode = "102";
                res.ResponseDescription = "Internal Error: Error while storing in the Database";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }

            bool storeDBaseLog = false;
            try
            {
                storeDBaseLog = storeDBIVRLog(req);
            }
            catch (Exception ex)
            {
                res.ResponseCode = "102";
                res.ResponseDescription = "Internal Error: Error while storing in the Database";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }
            if (!(storeDBaseLog))
            {
                res.ResponseCode = "102";
                res.ResponseDescription = "Internal Error: Error while storing in the Database";
                string xml = Serialize(res);
                retXml.LoadXml(xml);
                return retXml;
            }

            res.ResponseCode = "1";
            res.ResponseDescription = "Success";
            string strXml = Serialize(res);
            retXml.LoadXml(strXml);
            return retXml;
        }

        private void checkValidAccount(AccountLookupResponse res, string mobileNumber)
        {
            try
            {
                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //set the sql command
                DbCommand dbCommand = dbSQL.GetSqlStringCommand(String.Format(accountLookupSQL, mobileNumber));

                if (res != null)
                {
                    res.IsValid = "N";
                    using (IDataReader dataReader = dbSQL.ExecuteReader(dbCommand))
                    {
                        while (dataReader.Read())
                        {
                            res.PatientID = dataReader["PatientID"].ToString().Trim();
                            res.PIN = dataReader["PIN"].ToString().Trim();
                            res.IsValid = "Y";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Request convertTrackerToRequest(AHATracker req)
        {
            if (req != null)
            {
                Request request = new Request();
                request.Category = req.TrackerDetails.Type;
                request.DateOfEvent = req.TrackerDetails.Date;
                request.MobileNumber = req.MobileNumber;
                request.Password = req.Password;
                request.PatientID = req.PatientID;
                request.UserName = req.Username;

                if (StringComparer.CurrentCultureIgnoreCase.Equals(request.Category, "Weight"))
                {
                    request.Value = "wt " + req.TrackerDetails.Value;
                }
                if (StringComparer.CurrentCultureIgnoreCase.Equals(request.Category, "Blood Pressure"))
                {
                    request.Value = "bp " + req.TrackerDetails.Value;
                }
                if (StringComparer.CurrentCultureIgnoreCase.Equals(request.Category, "Cholesterol"))
                {
                    request.Value = "ch 0 0 0 " + req.TrackerDetails.Value;
                }
                if (StringComparer.CurrentCultureIgnoreCase.Equals(request.Category, "Blood Glucose"))
                {
                    request.Value = "bg " + req.TrackerDetails.Value;
                }
                if (StringComparer.CurrentCultureIgnoreCase.Equals(request.Category, "Medication"))
                {
                    request.Value = "med " + req.TrackerDetails.Value;
                }
                if (StringComparer.CurrentCultureIgnoreCase.Equals(request.Category, "Physical Activity"))
                {
                    request.Value = "pa " + req.TrackerDetails.Value;
                }
                return request;
            }
            return null;
        }

        private bool storeDBIVRLog(AHATracker req)
        {
            bool isValid = false;

            try
            {
                SqlDatabase dbSQL = new SqlDatabase(getAppSetting(PatientRemindersConnectionName));

                //set the sql command
                DbCommand dbCommand1 = dbSQL.GetSqlStringCommand(String.Format(ivrInsertLogSQL, req.PatientID, req.Responded, req.RetryAttempts, req.FinalAction));

                //Execute the command
                dbSQL.ExecuteNonQuery(dbCommand1);

                isValid = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isValid;
        }
    }
}