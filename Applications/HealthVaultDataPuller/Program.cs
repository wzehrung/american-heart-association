﻿using System;
using log4net;

namespace HealthVaultDataPuller
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            log4net.Config.XmlConfigurator.Configure();
            Log.Info("Starting HealthVaultDataPuller...");

            DataInterface api = new DataInterface();
            api.PullHealthVaultData();

            Log.Info("HealthVaultDataPuller Complete.");
            Log.Info("Time elapsed: " + DateTime.Now.Subtract(start).TotalSeconds + " seconds.");
        }
    }
}
