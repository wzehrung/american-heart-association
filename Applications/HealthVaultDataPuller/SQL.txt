﻿USE [Heart360]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HVData](
	[PatientID] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[EmailAddress] [nvarchar](200) NULL,
	[ZipCode] [nchar](10) NULL,
	[DataTimestamp] [datetime] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[HVDataError](
	[PatientID] [int] NOT NULL,
	[ErrorText] [nvarchar](500) NULL,
	[ErrorTimestamp] [datetime] NULL
) ON [PRIMARY]

GO
