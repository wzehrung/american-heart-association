﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using HVManager;
using log4net;

namespace HealthVaultDataPuller
{
    public class DataInterface
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataInterface));
        
        //configuration parameters
        private static readonly string DB_CONNECTION = "DBConnStr";
        private static readonly string SYNC_LEVEL = "SyncLevel";
        private static readonly string REMOVE_OLDER = "RemoveOlder";
        private static readonly string TEST_RECORD_LIMIT = "TestRecordLimit";

        //SQL Queries
        private static readonly string PATIENT_LIST_ALL = 
            "SELECT a.*, '0000000000' as phonenumber, '0' as PatientID, '0' as PIN from HealthRecord a " +
            "WHERE a.OfflineHealthRecordGUID is not null AND a.offlinepersonguid is not null ";
        private static readonly string PATIENT_LIST_REFRESH =
            "SELECT a.*, '0000000000' as phonenumber, '0' as PatientID, '0' as PIN from HealthRecord a " +
            "LEFT JOIN HVDataError b ON a.UserHealthRecordID = b.PatientID " + 
            "WHERE b.PatientID is null AND a.OfflineHealthRecordGUID is not null AND a.offlinepersonguid is not null ";
        private static readonly string PATIENT_LIST_DELTA =
            "SELECT a.*, '0000000000' as phonenumber, '0' as PatientID, '0' as PIN from HealthRecord a " +
            "LEFT JOIN HVDataError b ON a.UserHealthRecordID = b.PatientID " +
            "LEFT JOIN HVData c ON a.UserHealthRecordID = c.PatientID " +
            "WHERE b.PatientID is null AND c.PatientID is null AND a.OfflineHealthRecordGUID is not null AND a.offlinepersonguid is not null ";
        private static readonly string PATIENT_LIST_TEST =
            "SELECT TOP {0} a.*, '0000000000' as phonenumber, '0' as PatientID, '0' as PIN from HealthRecord a " +
            "WHERE a.OfflineHealthRecordGUID is not null AND a.offlinepersonguid is not null ";
        private static readonly string INSERT_DATA =
            "INSERT INTO [Heart360].[dbo].[HVData] ([PatientID], [FirstName], [LastName], [EmailAddress], [Zipcode], [DataTimestamp]) " + 
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')  ";
        private static readonly string INSERT_ERROR =
            "INSERT INTO [Heart360].[dbo].[HVDataError] ([PatientID], [ErrorText], [ErrorTimestamp]) " +
            "VALUES ('{0}', '{1}', '{2}')  ";
        private static readonly string REMOVE_OLDER_DATA =
            ";WITH cte1 AS (SELECT Row_number() OVER (partition BY PatientID ORDER BY DataTimestamp DESC) RowNum FROM [Heart360].[dbo].[HVData]) " +
            "DELETE FROM cte1 WHERE RowNum > 1 " +
            ";WITH cte2 AS (SELECT Row_number() OVER (partition BY PatientID ORDER BY ErrorTimestamp DESC) RowNum FROM [Heart360].[dbo].[HVDataError]) " +
            "DELETE FROM cte2 WHERE RowNum > 1 ";

        public string GetAppSetting(string settingName)
        {
            string setting = ConfigurationManager.AppSettings[settingName];
            return setting;
        }

        public void PullHealthVaultData()
        {
            try
            {
                int i = 0;
                DateTime start = DateTime.Today.AddDays(-4);
                DateTime end = DateTime.Today.AddDays(1);
                List<string> hvTypes = new List<string>
                    {
                        typeof(HVManager.PersonalDataManager).ToString(),
                        typeof(HVManager.PersonalContactDataManager).ToString()
                    };

                var db = new SqlDatabase(GetAppSetting(DB_CONNECTION));
                var syncLevel = GetAppSetting((SYNC_LEVEL));

                var query = PATIENT_LIST_DELTA;
                switch (syncLevel)
                {
                    case "ALL":
                        query = PATIENT_LIST_ALL;
                        break;
                    case "REFRESH":
                        query = PATIENT_LIST_REFRESH;
                        break;
                    case "DELTA":
                        query = PATIENT_LIST_DELTA;
                        break;
                    case "TEST":
                        query = string.Format(PATIENT_LIST_TEST, GetAppSetting(TEST_RECORD_LIMIT));
                        break;
                    default:
                        query = PATIENT_LIST_DELTA;
                        syncLevel = "DELTA";
                        break;
                }

                Console.Out.WriteLine("Starting HealthVault sync at level " + syncLevel);
                Log.Debug("Constructing dataset to sync with HealthVault at level " + syncLevel);

                var attachQuery = db.GetSqlStringCommand(query);
                Log.Debug("ATTACH SQL: " + attachQuery.CommandText);
                using (var dataReader = db.ExecuteReader(attachQuery))
                {
                    while (dataReader.Read())
                    {
                        //update index
                        i = i + 1;

                        //bind data
                        var objPatient = new IVRPatient();
                        objPatient.OnDataBindIVR(dataReader);

                        //doanload all data relevant to the patient
                        try
                        {
                            AllItemManager aim = HVHelper.HVManagerPatientBase(objPatient.OfflinePersonGUID.Value, objPatient.OfflineHealthRecordGUID.Value);
                            string result = aim.DownloadDataBetweenDatesforCampaign(objPatient.UserHealthRecordID, hvTypes, start, end, false);
                            Log.Debug("HealthVault Result: " + result);
                            
                            //handle ticks in data
                            result = result.Replace("'", "''");
                            string[] data = result.Split('!');

                            //parse output
                            //firstName + "!" + lastName + "!" + eMailAddress + "!" + zipcode + "!" + DOB + "!" + yearOfBirth.ToString() + "!"
                            string firstName = "", lastName = "", emailAddress = "", zipCode = "";
                            if (data.Length > 0)
                            {
                                firstName = data[0];
                                lastName = data[1];
                                emailAddress = data[2];
                                zipCode = data[3];
                            }

                            //Insert the data into DB
                            var insertQuery = db.GetSqlStringCommand(string.Format(
                                INSERT_DATA, objPatient.UserHealthRecordID, firstName, lastName, emailAddress, zipCode, DateTime.Now));
                            Log.Debug("INSERT SQL: " + insertQuery.CommandText);
                            db.ExecuteNonQuery(insertQuery);

                            if (i%100 == 0)
                                Log.Info("Processed " + i);
                        }
                        catch (Exception e)
                        {
                            string errorText = e.Message.Length > 500 ? e.Message.Substring(0, 499) : e.Message;
                            var errorQuery = db.GetSqlStringCommand(string.Format(
                                INSERT_ERROR, objPatient.UserHealthRecordID, errorText, DateTime.Now));
                            Log.Debug("ERROR SQL: " + errorQuery.CommandText);
                            db.ExecuteNonQuery(errorQuery);
                        }
                    }

                    //remove older records for same patient ID
                    if (GetAppSetting(REMOVE_OLDER).Equals("true"))
                    {
                        Log.Info("Removing older data for the same patient...");
                        var deleteQuery = db.GetSqlStringCommand(REMOVE_OLDER_DATA);
                        Log.Debug("DELETE SQL: " + deleteQuery.CommandText);
                        db.ExecuteNonQuery(deleteQuery);
                    }
                }
                Console.Out.WriteLine("Complete, " + i + " records processed.");
                Log.Info("Complete, " + i + " records processed.");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Error while processing: " + ex.Message);
                Log.Fatal("Program halted: " + ex.Message);
            }
        }

    }
}
