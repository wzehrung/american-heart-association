﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Health;
using Microsoft.Health.ItemTypes;
using System.Collections.ObjectModel;

namespace HealthVaultDataPuller
{
    public class IVRPatient
    {
        #region Public Properties

        public string PolkaID
        {
            get;
            set;
        }

        public int UserHealthRecordID
        {
            get;
            set;
        }

        public Guid? OfflinePersonGUID
        {
            get;
            set;
        }

        public Guid UserHealthRecordGUID
        {
            get;
            set;
        }

        public Guid? OfflineHealthRecordGUID
        {
            get;
            set;
        }

        public bool HasAgreedProviderTOU
        {
            get;
            set;
        }

        public bool HasProvidedOfflineAccess
        {
            get;
            set;
        }

        public Guid PersonalItemGUID
        {
            get;
            set;
        }

        public DateTime CreatedDate
        {
            get;
            set;
        }

        public DateTime UpdatedDate
        {
            get;
            set;
        }

        public DateTime? DateJoined
        {
            get;
            set;
        }

        public DateTime? ProviderConnectionDate
        {
            get;
            set;
        }

        public bool IsMigrated
        {
            get;
            set;
        }

        public int TotalAlerts
        {
            get;
            set;
        }

        public DateTime? LastActivityDate
        {
            get;
            set;
        }

        public string GroupIDList
        {
            get;
            set;
        }

        public bool HasAlertRule
        {
            get;
            set;
        }

        #region IVR Stuff
        public int? PatientID
        {
            get;
            set;
        }

        public bool IVREnabled
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public string PIN
        {
            get;
            set;
        }

        public string Time
        {
            get;
            set;
        }


        public string TimeZone
        {
            get;
            set;
        }

        public DateTime CreatedDateTime
        {
            get;
            set;
        }

        public DateTime UpdatedDateTime
        {
            get;
            set;
        }
        #endregion

        #endregion

        #region Private Methods
        private void OnDataBind(SqlDataReader reader, bool bIncludeAlert, bool bIncludeGroupIDList, bool bHasAlertRule)
        {
            this.UserHealthRecordID = BasicConverter.DbToIntValue(reader["UserHealthRecordID"]);
            this.OfflinePersonGUID = BasicConverter.DbToNullableGuidValue(reader["OfflinePersonGUID"]);
            this.OfflineHealthRecordGUID = BasicConverter.DbToNullableGuidValue(reader["OfflineHealthRecordGUID"]);
            this.UserHealthRecordGUID = BasicConverter.DbToGuidValue(reader["UserHealthRecordGUID"]);
            this.HasAgreedProviderTOU = BasicConverter.DbToBoolValue(reader["HasAgreedProviderTOU"]);
            this.HasProvidedOfflineAccess = BasicConverter.DbToBoolValue(reader["HasProvidedOfflineAccess"]);
            this.PersonalItemGUID = BasicConverter.DbToGuidValue(reader["PersonalItemGUID"]);
            this.CreatedDate = BasicConverter.DbToDateValue(reader["CreatedDate"]);
            this.UpdatedDate = BasicConverter.DbToDateValue(reader["UpdatedDate"]);
            this.IsMigrated = BasicConverter.DbToBoolValue(reader["IsMigrated"]);

            this.LastActivityDate = BasicConverter.DbToNullableDateValue(reader["LastActivityDate"]);

            if (bIncludeAlert)
            {
                this.TotalAlerts = BasicConverter.DbToIntValue(reader["TotalAlerts"]);
            }

            if (bIncludeGroupIDList)
            {
                this.GroupIDList = BasicConverter.DbToStringValue(reader["GroupIDs"]);
            }

            if (bHasAlertRule)
            {
                this.HasAlertRule = BasicConverter.DbToBoolValue(reader["HasAlertRule"]);
            }

            using (DataTable dt = reader.GetSchemaTable())
            {
                if (Misc.DoesColumnExist(dt, "Date"))
                {
                    this.DateJoined = BasicConverter.DbToDateValue(reader["Date"]);
                }

                if (Misc.DoesColumnExist(dt, "ProviderConnectionDate"))
                {
                    this.ProviderConnectionDate = BasicConverter.DbToDateValue(reader["ProviderConnectionDate"]);
                }
            }

            this.PolkaID = BasicConverter.DbToStringValue(reader["PolkaID"]);
        }

        public void OnDataBindIVR(IDataReader reader)
        {
            this.UserHealthRecordID = BasicConverter.DbToIntValue(reader["UserHealthRecordID"]);
            this.OfflinePersonGUID = BasicConverter.DbToNullableGuidValue(reader["OfflinePersonGUID"]);
            this.OfflineHealthRecordGUID = BasicConverter.DbToNullableGuidValue(reader["OfflineHealthRecordGUID"]);
            this.UserHealthRecordGUID = BasicConverter.DbToGuidValue(reader["UserHealthRecordGUID"]);
            this.HasAgreedProviderTOU = BasicConverter.DbToBoolValue(reader["HasAgreedProviderTOU"]);
            this.HasProvidedOfflineAccess = BasicConverter.DbToBoolValue(reader["HasProvidedOfflineAccess"]);
            this.PersonalItemGUID = BasicConverter.DbToGuidValue(reader["PersonalItemGUID"]);
            this.PatientID = BasicConverter.DbToIntValue(reader["PatientID"]);
            //this.IVREnabled = BasicConverter.DbToBoolValue(reader["Enabled"]);
            this.PhoneNumber = BasicConverter.DbToStringValue(reader["PhoneNumber"]);
            this.PIN = BasicConverter.DbToStringValue(reader["PIN"]);
            //this.Time = BasicConverter.DbToStringValue(reader["Time"]);
            //this.TimeZone = BasicConverter.DbToStringValue(reader["TimeZone"]);
            //this.CreatedDateTime = BasicConverter.DbToDateValue(reader["CreatedDateTime"]);
            //this.UpdatedDateTime = BasicConverter.DbToDateValue(reader["UpdatedDateTime"]);
        }
        #endregion

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(IVRPatient))
            {
                IVRPatient objPatient = obj as IVRPatient;

                if (objPatient.UserHealthRecordID == this.UserHealthRecordID)
                {
                    return true;
                }
            }
            return false;
        }


    }

    public class BasicConverter
    {
        public static char DbToCharValue(object obj)
        {

            if (obj == DBNull.Value)
            {
                throw new System.Exception("Cannot convert DBNull to char value");
            }

            if (obj.ToString().Length != 1)
            {
                throw new System.Exception("Invalid length - Unable to convert to char value");
            }

            return obj.ToString()[0];
        }

        public static object CharToDbValue(char c)
        {
            return c.ToString();
        }

        public static char[] DbToNCharValue(object obj, int numChars)
        {

            if (obj == DBNull.Value)
            {
                return null;
            }

            if (obj.ToString().Length != numChars)
            {
                throw new System.Exception("Invalid length - Unable to convert to char value");
            }

            return obj.ToString().ToCharArray();
        }



        public static string DbToStringValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }
            return obj.ToString();
        }

        public static Guid DbToGuidValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                throw new System.Exception("Cannot convert DBNull to guid value");
            }
            return new Guid(obj.ToString());
        }

        public static bool DbToBoolValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return false;
            }

            string str = obj.ToString();
            try
            {
                return (Convert.ToInt32(obj.ToString()) == 0) ? false : true;
            }
            catch (FormatException)
            {
                return Convert.ToBoolean(str);
            }
        }

        public static bool? DbToNullableBoolValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }

            string str = obj.ToString();
            try
            {
                return (Convert.ToInt32(obj.ToString()) == 0) ? false : true;
            }
            catch (FormatException)
            {
                return Convert.ToBoolean(str);
            }
        }

        public static Guid? DbToNullableGuidValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }
            return new Guid(obj.ToString());
        }

        public static int DbToIntValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return 0;
            }

            if (obj.ToString().Length == 0)
            {
                return 0;
            }

            return Convert.ToInt32(obj.ToString());
        }

        public static int? DbToNullableIntValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }

            if (obj.ToString().Length == 0)
            {
                return null;
            }

            return Convert.ToInt32(obj.ToString());
        }

        public static ulong DbToULongValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return 0;
            }

            if (obj.ToString().Length == 0)
            {
                return 0;
            }

            return Convert.ToUInt64(obj.ToString());
        }

        public static double DbToDoubleValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return 0;
            }

            if (obj.ToString().Length == 0)
            {
                return 0;
            }

            return Convert.ToDouble(obj.ToString());
        }

        public static double? DbToNullableDoubleValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }

            if (obj.ToString().Length == 0)
            {
                return 0;
            }

            return Convert.ToDouble(obj.ToString());
        }

        public static DateTime DbToDateValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return new DateTime();
            }

            if (obj.ToString().Length == 0)
            {
                return new DateTime();
            }

            return Convert.ToDateTime(obj.ToString());
        }

        public static DateTime? DbToNullableDateValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }

            if (obj.ToString().Length == 0)
            {
                return null;
            }

            return Convert.ToDateTime(obj.ToString());
        }


        public static object NCharToDbValue(char[] c)
        {
            if (c == null)
            {
                return DBNull.Value;
            }

            //Our db is designed for the below criteria
            if (c.Length == 0)
            {
                return DBNull.Value;
            }

            return c;

        }

        public static object StringToDbValue(string str)
        {
            if (str == null)
            {
                return DBNull.Value;
            }

            str = str.Trim();

            //Our db is designed for the below criteria
            if (str.Length == 0)
            {
                return DBNull.Value;
            }

            return str;
        }

        public static object IntToDbValue(int val)
        {
            if (val == 0)
                return DBNull.Value;
            return val;
        }

        public static object NullableIntToDbValue(int? val)
        {
            if (!val.HasValue)
                return DBNull.Value;
            return val.Value;
        }

        public static object DoubleToDbValue(double val)
        {
            if (val == 0)
                return DBNull.Value;
            return val;
        }

        public static object NullableDoubleToDbValue(double? val)
        {
            if (!val.HasValue)
                return DBNull.Value;
            return val;
        }

        public static object ULongToDbValue(ulong val)
        {
            if (val == 0)
                return DBNull.Value;
            return val;
        }

        public static int BoolToDbValue(bool bVal)
        {
            return bVal ? 1 : 0;
        }

        public static object NullableBoolToDbValue(bool? bVal)
        {
            if (!bVal.HasValue)
                return DBNull.Value;

            return bVal.Value ? 1 : 0;
        }

        public static char? DbToNullableCharValue(object obj)
        {
            if (obj == DBNull.Value)
            {
                return null;
            }

            if (obj.ToString().Length == 0)
            {
                return null;
            }

            return Convert.ToChar(obj);
        }

        public static object NullableCharToDBValue(char? val)
        {
            if (val.HasValue)
            {
                return val.Value;
            }
            else
            {
                return DBNull.Value;
            }
        }

        public static object DateToDbValue(DateTime dt)
        {
            if (dt == DateTime.MinValue)
                return DBNull.Value;
            return dt;
        }

        public static object NullableDateToDbValue(DateTime? dt)
        {
            if (!dt.HasValue)
                return DBNull.Value;
            return dt.Value;
        }

        public static object GuidToDbValue(Guid val)
        {
            return val;
        }

        public static string NStr(object obj)
        {
            return NStr(obj, "");
        }


        public static string NStr(object obj, string replacementStringIfNull)
        {
            if (obj == null)
                return replacementStringIfNull;

            return obj.ToString();
        }
    }

    public class Misc
    {
        public static bool DoesColumnExist(DataTable dt, string columnName)
        {
            dt.DefaultView.RowFilter = "ColumnName= '" + columnName + "'";
            return (dt.DefaultView.Count > 0);
        }
    }

    public class HVHelper
    {
        public static HVManager.AllItemManager HVManagerPatientBase(Guid PersonGUID, Guid RecordGUID)
        {
            //HVManager.AllItemManager mgr = HVManager.AllItemManager.GetItemManagersForRecordId(PersonGUID, RecordGUID);
            HVManager.AllItemManager mgrNew = new HVManager.AllItemManager(PersonGUID, RecordGUID);
            return mgrNew;
        }

        public static bool HasPatientGivenOfflineAccessToApp(Guid hvpersonid, Guid hvrecordid)
        {
            HealthRecordAccessor accessor = HVManager.Helper.GetAccessor(hvpersonid, hvrecordid);

            IList<Guid> idlist = new List<Guid>();
            idlist.Add(BloodPressure.TypeId);
            idlist.Add(Personal.TypeId);

            Collection<HealthRecordItemTypePermission> list = accessor.QueryPermissions(idlist);

            bool bHasOfflineAccess = false;

            foreach (HealthRecordItemTypePermission item in list)
            {
                if (item.OfflineAccessPermissions == HealthRecordItemPermissions.All)
                {
                    bHasOfflineAccess = true;
                    break;
                }
            }

            return bHasOfflineAccess;
        }

        /// <summary>
        /// This functions returns the personal object item guid of the selected record
        /// </summary>
        /// <returns>Guid</returns>
        //public static Guid GetPersonalItemGUID(bool bRedirect)
        //{
        //    HVManager.PersonalDataManager.PersonalItem objPI = HVManagerPatientBase.PersonalDataManager.Item;
        //    if (objPI == null)
        //    {
        //        HVManagerPatientBase.PersonalDataManager.FlushCache();
        //        objPI = HVManagerPatientBase.PersonalDataManager.Item;
        //        if (objPI == null)
        //        {
        //            HVManager.PersonalDataManager.PersonalItem item = new HVManager.PersonalDataManager.PersonalItem();
        //            item.FirstName.Value = HVManagerPatientBase.Name;
        //            HVManager.PersonalDataManager.PersonalItem objPersonalItem = HVManagerPatientBase.PersonalDataManager.CreateOrUpdateItem(item);
        //            HVManagerPatientBase.PersonalDataManager.FlushCache();
        //            objPI = HVManagerPatientBase.PersonalDataManager.Item;
        //            ////if (objPI == null)
        //            ////{
        //            ////    if (bRedirect)
        //            ////    {
        //            ////        System.Web.HttpContext.Current.Response.Redirect(string.Format("~/NoPersonalInfo.aspx?Guid={0}", Guid.NewGuid().ToString()), true);
        //            ////    }
        //            ////    else
        //            ////    {
        //            ////        return Guid.Empty;
        //            ////    }
        //            ////}
        //            ////else
        //            ////{
        //            ////    SessionInfo.ClearUserIdentityContext();
        //            ////    System.Web.HttpContext.Current.Response.Redirect(GRCBase.UrlUtility.FullCurrentPageUrlWithQV);
        //            ////}
        //        }
        //        //throw new Exception(string.Format("Personal info not found! Please add your personal info by going to {0}.", Microsoft.Health.Web.WebApplicationConfiguration.ShellUrl));
        //    }

        //    return objPI.ThingID; ;
        //}
    }

}
