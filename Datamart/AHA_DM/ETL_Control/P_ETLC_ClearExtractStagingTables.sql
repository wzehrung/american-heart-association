--P_ETLC_ClearExtractStagingTables.sql
use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'ClearExtractStagingTables' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - ClearExtractStagingTables'
	drop procedure ETLC.ClearExtractStagingTables
end
else
	print 'New Procedure created - ClearExtractStagingTables'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure ETLC.ClearExtractStagingTables
@MasterControlID int

as

begin

truncate table [H360].[Extract_Affiliate]
truncate table [H360].[Extract_AffiliateCampaign]


end