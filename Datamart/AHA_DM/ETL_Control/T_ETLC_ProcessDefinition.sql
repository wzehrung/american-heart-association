use [HealthDataMart]
go
drop table ETLC.ProcessDefinition
go

create table ETLC.ProcessDefinition(
ProcessDefinitionID int identity(1,1) constraint PK_ETLC_ProcessDefinition PRIMARY KEY NONCLUSTERED,
RootProcessID int null, --this is the root for multi level point to self indicates master
ParentProcessID int null, --null indicates master/parent job
--
ProcessName varchar(100),
ProcessType varchar(50),
ExecutionName varchar(200),
--
SortOrder int,
ParallelStreamID int constraint DF_ETLC_ProcessDefinition_ParallelStreamID default(0), --zero on children denote serial stream


--updated in terminal stage of step
LastRunStatus int, --   1 = success, 0 = fail, -1 = child failed
LastRunDate datetime,
LastRunID int,
--
LSET datetime, --last successful execution time - this is a short cut
LSEID int, 
--last successful execution Process LogID (updated in terminal stage of step - job cleanup stats)
--ProcessStarted datetime, --for log
--ExecutionTime decimal(18,6),  --in seconds --for log

--
CreatedDate datetime constraint DF_ETLC_ProcessDefinition_CreatedDate default(getdate()),
IsEnabled int constraint DF_ETLC_ProcessDefinition_IsEnabled default(1)
) on [PRIMARY]


truncate table ETLC.ProcessDefinition

insert into ETLC.ProcessDefinition(RootProcessID, ParentProcessID, ProcessName, ProcessType, ExecutionName, SortOrder)
values
	--main job
	(1,null,'BP DataMart Daily','Job','BP DataMart Daily', 0)
	--dimensions
	--
	,(1,1,'Load Date Dimension', 'SP', 'dbo.Load_DimDate',100)
	,(1,1,'Initialise Dimensions', 'SP', 'dbo.InitialiseDimensions',120)
	,(1,1,'Load Reference Dimensions', 'SP', 'BP.Load_Small_Dimensions',140)
	--
	,(1,1,'Extract Affiliate','SP','H360.Extract_AffiliateInfo', 1010)
	,(1,1,'Transform Affiliate','SP','H360.Transform_AffiliateInfo', 2010)
	,(1,1,'Load Affiliate','SP','DIM.Load_AffiliateInfo',3010)
	--
	,(1,1,'Extract Campaign','SP','H360.Extract_CampaignInfo', 1020)
	,(1,1,'Transform Campaign','SP','H360.Transform_CampaignInfo', 2020)
	,(1,1,'Load Campaign','SP','DIM.Load_CampaignInfo',3020)
	--
	,(1,1,'Extract Ethnicity','SP','H360.Extract_EthnicityInfo', 1030)
	,(1,1,'Transform Ethnicity','SP','H360.Transform_EthnicityInfo', 2030)
	,(1,1,'Load Ethnicity','SP','DIM.Load_EthnicityInfo',3030)
	--
	,(1,1,'Extract HealthRecord','SP','H360.Extract_HealthRecordExtendedInfo', 1040)
	,(1,1,'Transform HealthRecord','SP','H360.Transform_HealthRecordExtendedInfo', 2040)
	,(1,1,'Transform HR_InferredGeog','SP','H360.Transform_HR_InferredGeog', 2050)
	,(1,1,'Load HealthRecord','SP','DIM.Load_HealthRecordExtendedInfo',3040)
	--
	,(1,1,'Extract Geography','SP','H360.Extract_ZipRegion', 1060)
	,(1,1,'Transform Geography','SP','H360.Transform_ZipRegion', 2060)
	,(1,1,'Load Dim AHAGeography','SP','DIM.Load_AHAGeography',3060)
	--
	,(1,1,'Extract AffiliateCampaign','SP','H360.Extract_AffiliateCampaignInfo', 1070)
	--has to happen after affilate and campaing load
	,(1,1,'Transform AffiliateCampaign','SP','H360.Transform_AffiliateCampaignInfo', 4010) --includes inferred dimension insertion
	,(1,1,'Load AffiliateCampaign','SP','dbo.Load_AffiliateCampaignInfo', 4030) --is a pseudo fact and should bridge to campaign statistics

	

	--facts
	,(1,1,'Extract BloodPressure','SP','H360.Extract_BloodPressureInfo', 5010)
	,(1,1,'Transform BPCommentInfo','SP','H360.Transform_BPCommentInfo', 6010) 
	,(1,1,'Load BPComment','SP','DIM.Load_BPCommentInfo',6020)
	,(1,1,'Transform BloodPressureInfo','SP','H360.Transform_BloodPressureInfo', 6030)
	,(1,1,'Load Fact BloodPressure','SP','BP.Load_BloodPressureInfo',7010)



update ETLC.ProcessDefinition set isenabled = 0 where processname in ('Load Date Dimension','Initialise Dimensions')


select * from ETLC.ProcessDefinition order by rootprocessid, sortorder


