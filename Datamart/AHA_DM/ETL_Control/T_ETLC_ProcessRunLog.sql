use [HealthDataMart]
go
drop table ETLC.ProcessRunLog
go

create table ETLC.ProcessRunLog(
ProcessRunLogID bigint identity(1,1) constraint PK_ETLC_ProcessRunLog PRIMARY KEY NONCLUSTERED,
ProcessID int, --the actual process id
RootProcessExecutionID int, --the main controling job instance
--
ProcessStarted datetime,
ExecutionTime decimal(18,6), --seconds
--
LastRunStatus int, --   1 = success, 0 = fail, -1 = child failed --null = in progress
--
KPIType varchar(20),
KPIMetric varchar(400),
Notes varchar(100)
) on [PRIMARY]

--select * from ETLC.ProcessRunLog