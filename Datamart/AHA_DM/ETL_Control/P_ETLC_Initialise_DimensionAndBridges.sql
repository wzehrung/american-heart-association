--P_ETLC_ClearExtractStagingTables.sql
use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Initialise_DimensionAndBridges' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Initialise_DimensionAndBridges'
	drop procedure ETLC.Initialise_DimensionAndBridges
end
else
	print 'New Procedure created - Initialise_DimensionAndBridges'
GO


create procedure ETLC.Initialise_DimensionAndBridges
@MasterControlID int

as

begin
/*

Note initialisation of dimensions includes reinsertion of null, unknown, not reported, NA etc
 
*/
--CLEAR TABLES SECTION
--dimensions
truncate table [dbo].[DimAffiliate]
truncate table [dbo].[DimCampaign]
--truncate table [dbo].[DimDate] --handeled in process below
truncate table [dbo].[DimEthnicity]
--truncate table [dbo].[DimGender] --handeled in process below
truncate table [dbo].[DimGeography]
--truncate table [BP].[DimBPCategory] --handeled in process below
truncate table [BP].[DimBPReadingComments]
truncate table [BP].[DimHealthRecord]
--truncate table [BP].[DimImprovementCategory] --handeled in process below
--truncate table [BP].[DimRetention] --handeled in process below
--truncate table [BP].[DimValidUser] --handeled in process below

--dimension to dimension bridges
truncate table [BP].[Bridge_AffiliateCampaign] --there is no default bridge map for unknown to unknown - it is invalid.


--POPULATE TABLES SECTION
--processes
exec dbo.Load_DimDate 2005,2030
exec [BP].[Load_Small_Dimensions] @MasterControlID 

--values
insert into dbo.DimAffiliate(AffiliateCode, AffiliateName, AffiliateAbbreviation, ExtractControlID )
values('-1','Unknown','Unknown',-1),('0','NA','NA',-1)

insert into dbo.DimCampaign(CampaignID, CampaignCode, CampaignTitle, FromDate, ToDate, ExtractControlID, LEID )
values(-1,'Unknown','Unknown','1900-01-01','9999-12-31',-1,-1),(0,'NA','NA','1900-01-01','9999-12-31',-1,-1)

insert into dbo.DimEthnicity(EthnicityCode, EthnicityDescription, ExtractControlID )
values('Unknown','Unknown',-1),('Not Reported','Not Reported',-1)

insert into dbo.dimgeography(Country,RegionState, CountyName, ZipCode, ZipName, Affiliate, ExtractControlID)
values('NA','NA','NA','NA','NA', 'NA', -1)

insert into BP.DimBPReadingComments(comment, inserteddate, insertedprocessid)
values('[NULL]','1900-01-01',-1),('<Blank>','1900-01-01',-1)

insert into BP.DimHealthRecord( UserHealthRecordID, 
	CurrentCampaignID, [CurrentCampaign_SFKey], 
	Ethnicity, [Ethnicity_SFKey],
	Gender, [Gender_SFKey],
	ZipCode, [Geography_SFKey],
	ExtractControlID, LEID) 
-- -1 Extract Control ID is the default initial insert of unknown and test data; 0 = initial population data; > 1 live populations
values  (-1,		-1,-1,		'Unknown',-1,		'Unknown',-1,		'NA',-1,		 -1, -1),
		(-1,		0, 0,		'Not Reported',0,		'Unknown',-1,		'NA',-1,		 -1, -1)


end