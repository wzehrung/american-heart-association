use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_Campaign' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_Campaign'
	drop procedure H360.Transform_Campaign
end
else
	print 'New Procedure created - Transform_Campaign'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_Campaign
@MasterControlID int

as
/*

exec H360.Transform_Campaign -2

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform Campaign'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.Extract_CampaignKey) from H360.extract_campaign z),0)

--if the dimension doesnt have the unknown and No Campaign Rows then truncate the target table and insert them - the key mapping is all wrong.
--set a rekey fact flag
if not exists(select 1  from dbo.DimCampaign d where campaigntitle in ('Unknown','NA') )
begin
	truncate table dbo.dimcampaign

	insert into dbo.DimCampaign(CampaignID, CampaignCode, CampaignTitle, FromDate, ToDate, ExtractControlID, LEID )
	values(-1,'Unknown','Unknown','1900-01-01','9999-12-31',-1,-1),(0,'NA','NA','1900-01-01','9999-12-31',-1,-1)
	
end 



--update not found + table aliases : e = extract, d = dimension
update e
set e.foundmatchflag = isnull((select max(d.CampaignKey) from dbo.DimCampaign d where d.campaignID = e.CampaignID ),0) 
from [H360].[Extract_campaign] e


--update data differences
update e
set e.datachangeflag = 
	case 
		when isnull(d.campaigncode,'') <>  isnull(e.campaigncode,'') then 1
		when isnull(d.[CampaignTitle],'') <>  isnull(e.[CampaignTitle],'') then 1
		when isnull(d.FromDate,'1900-01-01') <>  isnull(e.FromDate,'1900-01-01') then 1
		when isnull(d.ToDate,'9999-12-31') <>  isnull(e.ToDate,'9999-12-31') then 1
		else 0 
	end
from [H360].[Extract_Campaign] e
	inner join dbo.DimCampaign d on e.foundmatchflag = d.campaignkey
where isnull(e.FoundMatchFlag,0) <> 0

set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'Evaluated all rows - Found and Change Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end
end

