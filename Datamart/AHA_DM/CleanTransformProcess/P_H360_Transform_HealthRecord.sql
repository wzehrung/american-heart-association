use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_HealthRecord' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_HealthRecord'
	drop procedure H360.Transform_HealthRecord
end
else
	print 'New Procedure created - Transform_HealthRecord'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_HealthRecord
@MasterControlID int

as
/*

exec H360.Transform_HealthRecord -2

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform HealthRecord'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.Extract_HealthRecordExtendedKey) from H360.Extract_HealthRecordExtended z),0)

--update not found + table aliases : e = extract, d = dimension
update e
set e.foundmatchflag = isnull((select top 1 d.HealthRecordKey from BP.DimHealthRecord d where d.[UserHealthRecordID] = e.[UserHealthRecordID] order by healthrecordkey desc),0) 
from [H360].[Extract_HealthRecordExtended] e


--update data differences
--target keys excepth
update e
set e.datachangeflag = 
	case 
		when isnull(d.PolkaID,'') <>  isnull(e.PolkaID,'') then 1 --value
		when isnull(d.BloodType,'') <>  isnull(e.BloodType,'') then 1 --value
		when isnull(d.CountryRegion,'') <>  isnull(e.CountryRegion,'') then 1 --key geo
		when isnull(d.ZipCode,'') <>  isnull(e.ZipCode,'') then 1 --key geo
		when isnull(d.Gender,'') <>  isnull(e.Gender,'') then 1 --key gender
		when isnull(d.Ethnicity,'') <>  isnull(e.Ethnicity,'') then 1 --key ethnicity
		when isnull(d.MaritalStatus,'') <>  isnull(e.MaritalStatus,'') then 1 --value
		when isnull(d.YearOfBIrth,-1) <>  isnull(e.YearOfBIrth,-1) then 1 --value
		when isnull(d.IsMigrated,-1) <>  isnull(e.IsMigrated,-1) then 1 --value
		when isnull(d.CurrentCampaignID,-1) <>  isnull(e.CurrentCampaignID,-1) then 1 --key campaign
		when isnull(d.HRCreatedDate,'1900-01-01') <>  isnull(e.HRCreatedDate,'1900-01-01') then 1 --add key
		when isnull(d.HRUpdatedDate,'9999-12-31') <>  isnull(e.HRUpdatedDate,'9999-12-31') then 1 --add key
		when isnull(d.HRLastActivityDate,'9999-12-31') <>  isnull(e.HRLastActivityDate,'9999-12-31') then 1 --add key
		else 0 
	end --select *
from [H360].Extract_HealthRecordExtended e
	inner join BP.DimHealthRecord d on e.foundmatchflag = d.HealthRecordKey
where isnull(e.FoundMatchFlag,0) <> 0
--derived keys will be abc_SFKey  = SnowFlakeKey


set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'Evaluated all rows - Found and Change Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end
end

