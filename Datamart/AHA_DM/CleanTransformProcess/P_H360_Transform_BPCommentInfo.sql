use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_BPCommentInfo' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_BPCommentInfo'
	drop procedure H360.Transform_BPCommentInfo
end
else
	print 'New Procedure created - Transform_BPCommentInfo'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_BPCommentInfo
@MasterControlID int

as

/*

exec H360.Transform_BPCommentInfo -2

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform BPCommentInfo'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int, @StepStart datetime, @StepEnd datetime

truncate table H360.Staging_BloodPressureComments

insert into H360.Staging_BloodPressureComments(comments,extractControlID, BPCommentKey)
select distinct 
	case
		when comments is null then '[NULL]'
		when comments = '' then '<blank>'
		else comments
	end, 
	isnull(d.insertedprocessid, @MasterControlID),
	bpcommentkey
from [H360].[Extract_BloodPressure] e
	left outer join  [BP].[DimBPReadingComments] d on 	case
		when e.comments is null then '[NULL]'
		when e.comments = '' then '<blank>'
		else e.comments
	end = 	case
		when d.comment is null then '[NULL]'
		when d.comment = '' then '<blank>'
		else d.comment
	end

set @TotalRowCount = isnull((select count(z.[BPCommentExtractRowID]) from H360.Staging_BloodPressureComments z),0)

set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'Unique Comments inserted to staging table.')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end
end

--select * from [H360].[Staging_BloodPressureComments] where bpcommentkey is null