use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_DimAffiliate' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_DimAffiliate'
	drop procedure H360.Transform_DimAffiliate
end
else
	print 'New Procedure created - Transform_DimAffiliate'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_DimAffiliate
@MasterControlID int

as
/*

exec H360.Transform_DimAffiliate -2

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform Affiliate'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin

if not exists(select 1 from dimaffiliate where affiliatename in ('Unknown','NA'))
begin
truncate table dimaffiliate

insert into dbo.DimAffiliate(AffiliateCode, AffiliateName, AffiliateAbbreviation, ExtractControlID )
values('-1','Unknown','Unknown',-1),('0','NA','NA',-1)

end

/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.Extract_AffiliateKey) from H360.extract_Affiliate z),0)

--update not found + table aliases : e = extract, d = dimension
update e
set e.foundmatchflag = isnull((select max(d.Affiliatekey) from dbo.DimAffiliate d where d.AffiliateCode = e.AffiliateID),0) 
from [H360].[Extract_Affiliate] e


--update data differences
update e
set e.datachangeflag = 
	case 
		when isnull(d.AffiliateName,'') <>  isnull(e.AffiliateName,'') then 1
		when isnull(d.AffiliateAbbreviation,'') <>  isnull(e.AffiliateAbbr,'') then 1
		else 0 
	end
from [H360].[Extract_Affiliate] e
	inner join dbo.DimAffiliate d on e.foundmatchflag = d.Affiliatekey
where isnull(e.FoundMatchFlag,0) <> 0

set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'Evaluated all rows - Found and Change Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end
end

