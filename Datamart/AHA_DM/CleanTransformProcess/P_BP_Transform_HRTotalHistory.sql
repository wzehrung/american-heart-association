create procedure bp.transform_hrtotalhistory
@MasterControlID int 
as /* exec bp.transform_hrtotalhistory -999 */

begin


--declare @MasterControlID int = -9999

truncate table BP.Staging_HRTotalHistory
/*
for each user - get a first and last reading.
*/

insert into BP.Staging_HRTotalHistory(HealthRecordKey, userhealthrecordid, 
affiliatekey, campaignkey, geographykey, ethnicitykey, genderkey, FirstReadingYMKey,
totalhistoricreadingcount, firstreadingdate, latestreadingdate,
RetainedUserCandidateFlag,validuserflag,ExtractControlID )
select 
	HealthRecordKey, userhealthrecordid, 
	--descriptor dimensions
	affiliatekey, campaignkey, geographykey, ethnicitykey, genderkey,
	FirstReadingYMKey = min(bpreportingmonthkey),
	--measures
	totalhistoricreadingcount = count(*), 
	firstreadingdate = min(BPMeasuredDate), 
	--cast(null as int) as FirstSystolic, cast(null as int) as FirstDiastolic, cast(null as int) as FirstBPCategoryKey
	
	latestreadingdate = case when count(*) = 1 then null else max(BPMeasuredDate) end,
	--cast(null as int) as LatestSystolic, cast(null as int) as LatestDiastolic,

	--cast(null as int) as ChangeSystolic, cast(null as int) as ChangeDiastolic,

	RetainedUserCandidateFlag = case when count(*) > 7 then 1 else 0 end,  ---rule from first part of if statement on retention calculation sheet column B
	validuserflag = case when count(*) = 1 then 0
		when datediff(d,min(BPMeasuredDate),max(BPMeasuredDate)) > 7 then 1
		else 0
		end
	,@mastercontrolid
--into bp.FactUserTotalHistory
from [BP].[FactBloodPressure]
where bpcategorykey <> -1 --only use valid blood pressure readings
group by HealthRecordKey, userhealthrecordid --26325
	,affiliatekey, campaignkey, geographykey, ethnicitykey, genderkey --26325
order by userhealthrecordid, healthrecordkey
--order by firstreadingdate, count(*) desc

/*
Table aliases - use a one or two letter alias to make things clear especially when there are joins.
standard aliases are -
 s = staging, f= fact, d = dimension (if only one, otherwise whatever makes sense g = gender, z = zip etc)
*/

/*
now update actual first reading data
-----------------
there has to be a first reading so should be an inner join
*/
update s
set s.FirstSystolic = f.Systolic, s.FirstDiastolic = f.Diastolic, s.FirstBPCategoryKey = f.BPCategoryKey
from BP.Staging_HRTotalHistory S
	inner join bp.factbloodpressure f on s.HealthRecordKey = f.HealthRecordKey and s.firstreadingdate = f.BPMeasuredDate


/*
now update actual latest reading data
*/
update s
set s.LatestSystolic = f.Systolic, s.LatestDiastolic = f.Diastolic, s.LatestBPCategoryKey = f.BPCategoryKey
from BP.Staging_HRTotalHistory S
	inner join bp.factbloodpressure f on s.HealthRecordKey = f.HealthRecordKey and s.LatestReadingDate = f.BPMeasuredDate
where s.latestreadingdate is not null

/*
now calculate change
*/
update s
set s.changesystolic = s.firstsystolic - s.latestsystolic, 
	s.changeDiastolic = s.firstdiastolic - s.latestdiastolic
from BP.Staging_HRTotalHistory S
where s.latestreadingdate is not null

/*
now categorise change
*/
--select * from ref.categorychangelookup
update s
set s.SystolicImprovementCategoryKey = null,
	s.DiastolicImprovementCategoryKey = null
from BP.Staging_HRTotalHistory S
where s.latestreadingdate is not null


end
--select * from BP.Staging_HRTotalHistory S