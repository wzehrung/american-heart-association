use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Staging_BloodPressure' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Staging_BloodPressure'
	drop table H360.Staging_BloodPressure
end
else
	print 'New Table created - Staging_BloodPressure'
GO

create table H360.Staging_BloodPressure
(
	--surrogate key
	Staging_BloodPressureKey int identity(1,1)
		constraint PK_H360_Staging_BloodPressure Primary Key NONCLUSTERED, 
	--main extract table field list
	--BP
	BloodPressureID int not null, --bk
	UserHealthRecordID int not null, --rif
	HealthRecordKey int null,
	--derived --key value lookups must come from dimension not extract tables
	AffiliateKey int, 
	CampaignKey int,
	GeographyKey int,
	EthnicityKey int,
	GenderKey int,
	--

	BPMeasuredDate datetime,
	RowCreatedDate datetime,
	RowUpdatedDate datetime,
	BPMeasuredDateKey int,
	BPReportingMonthKey int,

	Systolic int,
	Diastolic int,
	BPCategoryKey int, --high,per,normal, unknown/invalid - this is calculated off of reference table
	
	Pulse int, --informational


	Comments varchar(max), --exclude the lorm ipsum text on reading 367182 during staging
	BPCommentKey int, --expecting this to happen and drop comments from the fact
	--control columns

	PopulatedDateTime datetime
		constraint DF_H360_Staging_BloodPressure_PopulateDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	ProcessingFlag int
		constraint DF_H360_Staging_BloodPressure_ProcessingFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	DataChangeFlag int
		constraint DF_H360_Staging_BloodPressure_DataChanged default(0)
		--only valid if matchflag = 1

)
--select len(comments), * from [dbo].[BloodPressure] order by len(comments) desc --6831 max length