use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_BloodPressureInfo' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_BloodPressureInfo'
	drop procedure H360.Transform_BloodPressureInfo
end
else
	print 'New Procedure created - Transform_BloodPressureInfo'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_BloodPressureInfo
@MasterControlID int

as
/*
--truncate table [BP].[FactBloodPressure]

exec H360.Transform_BloodPressureInfo -201

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform BloodPressureInfo'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int, @StepStart datetime, @StepEnd datetime
/*
Find if fact is already loaded - AND - if the data in it has changed
*/
--update not found + table aliases : e = extract, d = dimension
update e
set e.foundmatchflag = isnull((select top 1 d.BloodPressurekey from BP.FactBloodPressure d where d.BloodpressureID = e.BloodpressureID order by d.bloodpressurekey desc),0) 
from [H360].[Extract_BloodPressure] e


--update data differences
--target keys excepth
update e
set e.datachangeflag = 
	case 
		when isnull(d.[UserHealthRecordID],-999) <>  isnull(e.[UserHealthRecordID],-999) then 1 --value --this is a bk change so what the heck!?!
		when isnull(d.[BPMeasuredDate],'') <>  isnull(e.[BPMeasuredDate],'') then 1 --value
		when isnull(d.[RowCreatedDate],'') <>  isnull(e.[RowCreatedDate],'') then 1 --key geo
		when isnull(d.[RowUpdatedDate],'') <>  isnull(e.[RowUpdatedDate],'') then 1 --key geo
		when isnull(d.[Systolic],'') <>  isnull(e.[Systolic],'') then 1 --key gender
		when isnull(d.[Diastolic],'') <>  isnull(e.[Diastolic],'') then 1 --key ethnicity
		when isnull(d.[Pulse],-1) <>  isnull(e.[Pulse],-1) then 1 --value
		else 0 
	end --select *
from [H360].[Extract_BloodPressure] e
	inner join BP.FactBloodPressure d on e.foundmatchflag = d.BloodPressureKey
where isnull(e.FoundMatchFlag,0) <> 0
--derived keys will be abc_SFKey  = SnowFlakeKey

--this is the reduced set to be processed
truncate table h360.staging_bloodpressure

insert into h360.staging_bloodpressure(BloodPressureID, UserHealthRecordID, healthrecordkey, BPMeasuredDate, RowCreatedDate, RowUpdatedDate, 
	Systolic, Diastolic, Pulse, Comments, PopulatedDateTime, ExtractControlID, ProcessingFlag, DataChangeFlag)
select
	BloodPressureID, UserHealthRecordID, 
	HealthRecordKey = isnull((select top 1 h.healthrecordkey from bp.Dimhealthrecord h where e.userhealthrecordid = h.userhealthrecordid order by healthrecordkey desc),-1),
	BPMeasuredDate, RowCreatedDate, RowUpdatedDate, 
	Systolic, Diastolic, Pulse, Comments, PopulatedDateTime, ExtractControlID, FoundMatchFlag, DataChangeFlag
from [H360].[Extract_BloodPressure] e
where e.FoundMatchFlag = 0 OR e.DataChangeFlag <> 0

/*
Match the dimension keys to the data -- data logging at each step
*/


--base info
update s
set 
	s.bpmeasureddatekey = convert(varchar(50), s.bpmeasureddate, 112),
	s.bpreportingmonthkey = left(convert(varchar(50), s.bpmeasureddate, 112),6),
	s.BPCategorykey = case --bpcategory set
		when nullif(s.systolic,0) is null or nullif(s.diastolic,0) is null then -1 --(unknown for invalid reading)
		else isnull((select top 1 r.BPCategoryKey 
						from Ref.BloodPressureRange r
						where (s.[Systolic] between r.SystolicFrom and r.SystolicTo ) OR (s.[Diastolic] between r.diastolicFrom and r.diastolicTo )
							--(s.[Systolic] >= r.SystolicFrom and s.[Systolic] < r.SystolicTo ) 
							--OR 
							--(s.[Diastolic] >= r.diastolicFrom and s.[Diastolic] < r.diastolicTo )
						order by r.BPCategoryKey desc)
				,-1)
		end
		--select top 10000 * 
from h360.staging_bloodpressure s


--person related
--[GeographyKey][EthnicityKey][GenderKey]
update s
set 
	s.geographyKey = d.geography_SFKey,
	s.ethnicityKey = d.ethnicity_SFKey,
	s.genderkey = d.gender_SFKey
--select count(*)
from [H360].[Staging_BloodPressure] s
	inner join [BP].[DimHealthRecord] d on s.healthrecordkey = d.healthrecordkey


--[BPCommentKey]
update s
set s.BPCommentKey = isnull(d.bpcommentkey,-1) --select count(*)
from [H360].[Staging_BloodPressure] s
	left outer join  [BP].[DimBPReadingComments] d on 	case
		when s.comments is null then '[NULL]'
		when s.comments = '' then '<blank>'
		else s.comments
	end = 	case
		when d.comment is null then '[NULL]'
		when d.comment = '' then '<blank>'
		else d.comment
	end

--campaign/affiliate related
--[AffiliateKey][CampaignKey]
update s
set s.affiliatekey = case 
			when ac.affiliatekey is null and isnull(cd.campaignid,-1) in (0,-1) then 0
			when ac.affiliatekey is null and hr.currentcampaign_SFKey in (0,-1) then 0
			else isnull(af.affiliatekey, -1)
	end, 
	s.CampaignKey = cd.campaignkey --select s.userhealthrecordid, hr.currentcampaign_SFKey, *
from H360.Staging_BloodPressure s
	inner join BP.DimHealthRecord hr on s.userhealthrecordid = hr.UserHealthRecordID
	LEFT OUTER JOIN dbo.DimCampaign AS cd ON cd.campaignkey = hr.currentcampaign_SFKey 
	LEFT OUTER JOIN	BP.Bridge_AffiliateCampaign AS ac ON ac.Campaignkey = hr.currentcampaign_SFKey 
	LEFT OUTER JOIN dbo.DimAffiliate AS af ON af.AffiliateKey = ac.AffiliateKey



set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'Evaluated all rows - Found and Change Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end


end

--select bpcategorykey, count(*) from [H360].[Staging_BloodPressure] group by bpcategorykey

--select * from [H360].[Staging_BloodPressure] where healthrecordkey = 3  order by bpreportingmonthkey
