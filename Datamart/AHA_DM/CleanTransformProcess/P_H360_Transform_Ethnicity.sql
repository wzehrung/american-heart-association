use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_Ethnicity' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_Ethnicity'
	drop procedure H360.Transform_Ethnicity
end
else
	print 'New Procedure created - Transform_Ethnicity'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_Ethnicity
@MasterControlID int

as
/*

exec H360.Transform_Ethnicity -2

*/

begin
declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform Ethnicity'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.Extract_EthnicityKey) from H360.extract_ethnicity z),0)

--step 1 - find matches
update e
set e.foundmatchflag = isnull((select top 1 d.ethnicitykey from dbo.DimEthnicity d where d.ethnicityCode = e.[EthnicityCode] order by ethnicitykey desc),0) 
from [H360].[Extract_ethnicity] e

--select * from dimethnicity where ethnicitykey = 0
--select * from [BP].[DimHealthRecord] where ethnicity = 'Asian'
--select * from [BP].[FactBloodPressure] where ethnicitykey in (0,7)
-- h360.Extract_Ethnicity([Extract_EthnicityKey],	EthnicityCode, EthnicityDescription, LookupListRowID, ExtractControlID)


--insert into tasks

--step 2 - for matches - find if a data update is required
update e
set e.datachangeflag = 
	case
		when isnull(d.ethnicitydescription,'') = isnull(e.ethnicitydescription,'') then 1
		else 0 
	end
from [H360].[Extract_Ethnicity] e
	inner join dbo.DimEthnicity d on e.foundmatchflag = d.Ethnicitykey
where isnull(e.FoundMatchFlag,0) <> 0


--insert into tasks

set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'All rows evaluated - Match and Change Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end
