use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_AffiliateCampaign' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_AffiliateCampaign'
	drop procedure H360.Transform_AffiliateCampaign
end
else
	print 'New Procedure created - Transform_AffiliateCampaign'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_AffiliateCampaign
@MasterControlID int

as
/*

exec H360.Transform_AffiliateCampaign -20

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform AffiliateCampaign'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(*) from H360.extract_AffiliateCampaign z),0)

--update not found + table aliases : e = extract, d = dimension
--inferred affiliate if required

update e
set e.[AffiliateMatchKey] = isnull((select affiliatekey from dimaffiliate d where isnull(e.affiliateid,0) = d.affiliatecode),-1)
from [H360].[Extract_AffiliateCampaign] e




--inferred dimension insert - dimension keys always have to exists or there is a DM error condition.
insert into [dbo].[DimCampaign] ( CampaignID, CampaignCode, CampaignTitle, FromDate, ToDate,  ExtractControlID, LEID)
select distinct campaignid, 'Inferred','Inferred','1900-01-01','9999-12-31', @MasterControlID, @mastercontrolID
from  [H360].[Extract_AffiliateCampaign] e
where e.campaignid not in (select campaignid from dimcampaign) --campaignmatchkey = -99

update e
set e.[CampaignMatchKey] = isnull((select max(campaignkey) from dimcampaign d where isnull(e.campaignid,0) = d.campaignid), -1)
--isnull means is unknown, remap blank extract to zero means NA or no campaign
from [H360].[Extract_AffiliateCampaign] e


--
update e
set e.[AffiliateCampaignMatch] = isnull(d.Bridge_AffiliateCampaignKey, -99)
from [H360].[Extract_AffiliateCampaign] e
	left outer join BP.Bridge_AffiliateCampaign d on d.AffiliateKey = e.AffiliateMatchKey and e.campaignmatchkey = d.campaignkey
--if -99 then new record 



set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'Evaluated all rows - Link Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end
end

