use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_HRMonthlyRetention' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_HRMonthlyRetention'
	drop procedure bp.Transform_HRMonthlyRetention
end
else
	print 'New Procedure created - Transform_HRMonthlyRetention'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure BP.Transform_HRMonthlyRetention
@MasterControlID int, @recalculatemonthkey int = 999912

as
/*

exec bp.Transform_HRMonthlyRetention -2

*/

begin

--declare @mastercontrolid int = -9999, @recalculatemonthkey int = 999912

truncate table bp.staging_hrmonthlyretention

declare @FromMonth int, @ToMonth int

set @ToMonth = case when isnull(@recalculatemonthkey,0) = 0 then 999912 else @recalculatemonthkey end

set @FromMonth = case when isnull(@recalculatemonthkey,0) = 0 then 190001 
	else isnull((select top 1 d1.yearmonthkey from dimdate d1 
			where d1.datevalue = (select min(dateadd(month,-3,datevalue)) from dimdate d2 where d2.yearmonthkey =  @recalculatemonthkey)), 190001)
	end


print @ToMonth; Print @FromMonth; 

insert into bp.staging_hrmonthlyretention([HealthRecordKey], [UserHealthRecordID], [ReportingMonthKey], 
	Month1Count, LatestReadingInPeriod, ExtractControlID)
select p.healthrecordkey, p.UserHealthRecordID, 
	p.BPReportingMonthKey, count(p.[BloodPressureKey]), Max([BPMeasuredDate]), 
	@mastercontrolid
from bp.factbloodpressure p --on d.yearmonthkey = p.BPReportingMonthKey
where (@ToMonth = 999912 OR p.BPReportingMonthKey = @ToMonth)
group by p.healthrecordkey, p.UserHealthRecordID, p.BPReportingMonthKey
order by HealthRecordKey


update S
set FirstReadingInPeriod = (select min(p.bpmeasureddate) from bp.factbloodpressure p
							where p.[HealthRecordKey] = s.HealthRecordKey
								and p.BPReportingMonthKey between 
										(select min(d1.yearmonthkey) 
											from dimdate d1 where d1.datevalue = 
												(select min(dateadd(month,-3,d2.datevalue)) from dimdate d2 where d2.yearmonthkey = s.ReportingMonthKey))
								and s.reportingmonthkey)
	--should never be null on month1 because that is what was populated from
from bp.staging_hrmonthlyretention s

update S
set Month4Count = isnull((select count(p.BloodPressureKey) from bp.factbloodpressure p
							where p.[HealthRecordKey] = s.HealthRecordKey
								and p.BPReportingMonthKey = 
										(select min(d1.yearmonthkey) 
											from dimdate d1 where d1.datevalue = 
												(select min(dateadd(month,-3,d2.datevalue)) from dimdate d2 where d2.yearmonthkey = s.ReportingMonthKey))
								),0) --and s.reportingmonthkey)
	--should never be null on month1 because that is what was populated from
from bp.staging_hrmonthlyretention s

update S
set Month3Count = isnull((select count(p.BloodPressureKey) from bp.factbloodpressure p
							where p.[HealthRecordKey] = s.HealthRecordKey
								and p.BPReportingMonthKey = 
										(select min(d1.yearmonthkey) 
											from dimdate d1 where d1.datevalue = 
												(select min(dateadd(month,-2,d2.datevalue)) from dimdate d2 where d2.yearmonthkey = s.ReportingMonthKey))
								),0) --and s.reportingmonthkey)
	--should never be null on month1 because that is what was populated from
from bp.staging_hrmonthlyretention s

update S
set Month2Count = isnull((select count(p.BloodPressureKey) from bp.factbloodpressure p
							where p.[HealthRecordKey] = s.HealthRecordKey
								and p.BPReportingMonthKey = 
										(select min(d1.yearmonthkey) 
											from dimdate d1 where d1.datevalue = 
												(select min(dateadd(month,-1,d2.datevalue)) from dimdate d2 where d2.yearmonthkey = s.ReportingMonthKey))
								),0) --and s.reportingmonthkey)
	--should never be null on month1 because that is what was populated from
from bp.staging_hrmonthlyretention s


update s
set validuserflag = case when datediff(d,FirstReadingInPeriod,LatestReadingInPeriod) > 7 then 1 else 0 end
from bp.staging_hrmonthlyretention s


update s
set retaineduserflag = case 
	when validuserflag = 0 then 0
	when month1count > 0 and month2count > 0 and Month3Count > 0 and month4count > 0 and (month1count + month2count + month3count + month4count ) > 7then 1
	else 0 
	end
from bp.staging_hrmonthlyretention s


end
--select * from bp.staging_hrmonthlyretention order by healthrecordkey, reportingmonthkey


/*
select 
from bp.factbloodpressure p --on d.yearmonthkey = p.BPReportingMonthKey
where d.yearmonthkey = between @FromMonth and @ToMonth
order by HealthRecordKey
*/




	