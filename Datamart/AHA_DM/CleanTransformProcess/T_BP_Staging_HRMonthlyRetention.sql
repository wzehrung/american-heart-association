use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Staging_HRMonthlyRetention' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Staging_HRMonthlyRetention'
	drop table BP.Staging_HRMonthlyRetention
end
else
	print 'New Table created - Staging_HRMonthlyRetention'
GO



create table BP.Staging_HRMonthlyRetention(
Staging_HRMonthlyRetentionKey bigint identity(1,1) 
	constraint PK_BP_Staging_HRMonthlyRetention PRIMARY KEY NONCLUSTERED,
HealthRecordKey int, 
UserHealthRecordID int, --bk just incase rekeying is required
--descriptor dimensions -these are derived off the primary fact so doesnt need the bk
ReportingMonthKey int,

--all dimensions if null then -1 (unknown)

--measures
FirstReadingInPeriod date,
LatestReadingInPeriod date,

Month1Count int,
Month2Count int,
Month3Count int,
Month4Count int,


--this is the "within the 4 month" perspective
RetainedUserFlag int,
 
--= case when count(*) > 7 then 1 else 0 end,  ---rule from first part of if statement on retention calculation sheet column B
validuserflag int,
--= case when count(*) = 1 then 0		when datediff(d,min(BPMeasuredDate),max(BPMeasuredDate)) > 7 then 1		else 0		end

--control
PopulationDateTime datetime
	constraint DF_BP_Staging_HRMonthlyRetention_PopulationDateTime default(getdate()),

ExtractControlID int


) on [PRIMARY]
Go

create index IDX_BP_Staging_HRMonthlyRetention_Population_Perf001
ON BP.Staging_HRMonthlyRetention(ReportingMonthKey,HealthRecordKey) include(RetainedUserFlag, validuserflag)
Go

