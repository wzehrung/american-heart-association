use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_HR_InferredGeog' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_HR_InferredGeog'
	drop procedure H360.Transform_HR_InferredGeog
end
else
	print 'New Procedure created - Transform_HR_InferredGeog'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_HR_InferredGeog
@MasterControlID int

as
/*

exec H360.Transform_HR_InferredGeog -2

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform HR_InferredGeog'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.Extract_HealthRecordExtendedKey) from H360.Extract_HealthRecordExtended z
	where z.zipcode not in (select zipcode from dbo.DimGeography) ),0)

--insert into extract as inferred 
insert into [H360].[Extract_ZipRegionMapping]( ZIP_Code, ZIP_Name, State, Affiliate, 
	TotalPopulation, CBSA_Code, CBSA_Name, CountyFIPS, CountyName, 
	ExtractPopulatedDateTime, ExtractControlID)
select distinct
	z.zipcode, 'Inferred','Inferred','Inferred',
	null,'Inferred','Inferred',  'Inferred','Inferred',
	getdate(), @MasterControlID 
from H360.Extract_HealthRecordExtended z
	where z.zipcode not in (select zipcode from dbo.DimGeography)

set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'InferredInsert', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'Insert Inferred Zipcodes from health record into geography extract for processing')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end


end

