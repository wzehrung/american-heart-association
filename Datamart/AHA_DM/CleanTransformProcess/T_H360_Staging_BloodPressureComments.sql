use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Staging_BloodPressureComments' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Staging_BloodPressureComments'
	drop table H360.Staging_BloodPressureComments
end
else
	print 'New Table created - Staging_BloodPressureComments'
GO

create table H360.Staging_BloodPressureComments(
BPCommentExtractRowID int identity(1,1) constraint PK_Staging_BloodPressureComments PRIMARY KEY NONCLUSTERED,
	Comments varchar(max), --exclude the lorm ipsum text on reading 367182 during staging
	BPCommentKey int, --expecting this to happen and drop comments from the fact
	--control columns

	PopulatedDateTime datetime
		constraint DF_H360_Staging_BloodPressureComments_PopulateDT default(getdate()),
	ExtractControlID int

)