use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_ZIPRegion' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_ZIPRegion'
	drop procedure H360.Transform_ZIPRegion
end
else
	print 'New Procedure created - Transform_ZIPRegion'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_ZipRegion
@MasterControlID int

as
/*

exec H360.Transform_ZipRegion -2

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform Geography'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.extractrowid) from H360.extract_zipregionmapping z),0)

--step 1 - find matches
update e
set e.foundmatchflag = isnull((select max(g.geographykey) from dbo.DimGeography g where g.zipcode = e.zip_code and g.geographykey > 0),0) 
from [H360].[Extract_ZipRegionMapping] e

--insert into tasks

--step 2 - for matches - find if a data update is required
update e
set e.datachangeflag = 
	case		when isnull(g.ZipName,'') <>  isnull(e.zip_name,'') then 1
		when isnull(g.RegionState,'') <>  isnull(e.State,'') then 1
		when isnull(g.Affiliate,'') <>  isnull(e.affiliate,'') then 1
		when isnull(g.countyname,'') <>  isnull(e.countyname,'') then 1

		else 0 
	end
from [H360].[Extract_ZipRegionMapping] e
	inner join dbo.DIMGeography g on e.foundmatchflag = g.geographykey
where isnull(e.FoundMatchFlag,0) <> 0



--insert into tasks

set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'All rows Found and Match Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end
end

/*
ProcessRunLogID bigint identity(1,1) constraint PK_ETLC_ProcessRunLog PRIMARY KEY NONCLUSTERED,
ProcessID int, --the actual process id
RootProcessExecutionID int, --the main controling job instance
--
ProcessStarted datetime,
ExecutionTime decimal(18,6), --seconds
--
LastRunStatus int, --   1 = success, 0 = fail, -1 = child failed --null = in progress
--
KPIType varchar(20),
KPIMetric varchar(100),
Notes varchar(100)
*/