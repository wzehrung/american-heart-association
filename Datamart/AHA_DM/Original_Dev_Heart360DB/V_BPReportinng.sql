
CREATE VIEW [dbo].[v_Reporting_BloodPressure]
AS
SELECT     TOP (100) PERCENT 
	hr.UserHealthRecordID,
	hr.CreatedDate AS JoinedDate,
	bp.Systolic,
	bp.Diastolic,
	YEAR(bp.DateMeasured) AS Year, 
    MONTH(bp.DateMeasured) AS Month,
	DAY(bp.DateMeasured) AS Day,
	bp.DateMeasured,
	hr.CampaignID,
	cd.Title AS Campaign,
	ac.AffiliateID,
	af.AffiliateName, 
	zrm.City,
	zrm.State,
	ep.ZipCode,
	ep.Gender, 

	CASE
		WHEN ep.Ethnicity IS NULL
			OR ep.Ethnicity = ''
			OR ep.Ethnicity = 'Select:'
		THEN 'Not Reported'
		
		WHEN ep.Ethnicity = '1'
		THEN 'American Indian or Alaska Native'
		
		WHEN ep.Ethnicity = '2'
		THEN 'Asian'
		
		WHEN ep.Ethnicity = '3'
		THEN 'Black or African American'
		
		WHEN ep.Ethnicity = '4'
		THEN 'Hispanic or Latino'
		
		WHEN ep.Ethnicity = '5'
		THEN 'Native Hawaiian or Other Pacific Islander'		

		WHEN ep.Ethnicity = '6'
		THEN 'White'	

		WHEN ep.Ethnicity = '7'
		THEN 'Mixed Race'	
		
		WHEN ep.Ethnicity = '8'
		THEN 'Other Race'			
		
		ELSE ep.Ethnicity
	END Ethnicity,
	ep.YearOfBirth, bp.CreatedDate


FROM
	dbo.HealthRecord AS hr 
	INNER JOIN dbo.BloodPressure AS bp 
		ON hr.UserHealthRecordID = bp.UserHealthRecordID 
	LEFT OUTER JOIN dbo.CampaignDetails AS cd 
		ON cd.CampaignID = hr.CampaignID 
	LEFT OUTER JOIN	dbo.AffiliatesCampaigns AS ac 
		ON ac.CampaignID = hr.CampaignID 
	LEFT OUTER JOIN dbo.Affiliates AS af 
		ON af.AffiliateID = ac.AffiliateID
	LEFT OUTER JOIN ExtendedProfile ep
		ON ep.UserHealthRecordID = hr.UserHealthRecordID
	LEFT OUTER JOIN v_ZipRegionMapping zrm 
		ON zrm.ZipCode = ep.ZipCode

--WHERE     (hr.CampaignID IS NOT NULL)
ORDER BY hr.UserHealthRecordID



GO