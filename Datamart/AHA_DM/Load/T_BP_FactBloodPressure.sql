use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'FactBloodPressure' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - FactBloodPressure'
	drop table BP.FactBloodPressure
end
else
	print 'New Table created - FactBloodPressure'
GO

create table BP.FactBloodPressure
(
	--surrogate key
	BloodPressureKey int identity(1,1)
		constraint PK_BP_FactBloodPressure Primary Key NONCLUSTERED, 
	--main extract table field list
	--BP
	BloodPressureID int not null, --bk
	UserHealthRecordID int not null, --rif
	--
	HealthRecordKey int not null,
	--derived --key value lookups must come from dimension not extract tables
	AffiliateKey int, 
	CampaignKey int,
	GeographyKey int,
	EthnicityKey int,
	GenderKey int,
	--

	BPMeasuredDate datetime,
	RowCreatedDate datetime,
	RowUpdatedDate datetime,
	BPMeasuredDateKey int,
	BPReportingMonthKey int,

	Systolic int,
	Diastolic int,
	BPCategoryKey int, --high,per,normal, unknown/invalid - this is calculated off of reference table
	
	Pulse int, --informational


	--Comments varchar(max), --exclude the lorm ipsum text on reading 367182 during staging
	BPCommentKey int, --expecting this to happen and drop comments from the fact
	--control columns

	PopulatedDateTime datetime
		constraint DF_Fact_BloodPressure_PopulateDT default(getdate()),
	ExtractControlID int
)
--select len(comments), * from [dbo].[BloodPressure] order by len(comments) desc --6831 max length

create index IDX_FactBloodPressure_BK
on BP.FactBloodPressure(BloodPressureID, UserHealthRecordID, HealthRecordKey, BPMeasuredDate) Include (BloodPressureKey,BPCategoryKey)

--create index IDX_FactBloodPressure_ on BP.FactBloodPressure(BloodPressureID, UserHealthRecordID, HealthRecordKey, BPMeasuredDate) Include (BloodPressureKey,BPCategoryKey)
