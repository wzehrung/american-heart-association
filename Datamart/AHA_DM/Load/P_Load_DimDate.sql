if exists (select * from sysobjects where name = 'Load_DimDate' and xtype = 'P')
	begin
		print 'Procedure exists - dropped to recreate - Load_DimDate'
		drop procedure dbo.Load_DimDate
	end
else
	print 'Procedure did not exist - creating new procedure - Load_DimDate'
go

create procedure dbo.Load_DimDate
@StartYear int = 2007,
@EndYear int = 2030

as

begin
--declare @StartYear int = 2007, @EndYear int = 2030

truncate table dbo.dimdate

;WITH DimDateCTE AS
(
SELECT datefromparts(@startyear,1,1) DATE_
UNION ALL
SELECT DATEADD(DAY,1,DimDateCTE.DATE_) DATE_ FROM DimDateCTE WHERE DATE_ < datefromparts(@EndYear,12,31) 
),
FixedDate AS
(
SELECT datefromparts(1900,1,1) DATE_
UNION
SELECT Date_ from DimDateCTE
UNION
SELECT datefromparts(9999,12,31)
)
INSERT INTO dbo.dimDate --Use this part if there is an existing DimDate
SELECT distinct
--primary and alternate key
	CAST(CONVERT(VARCHAR(8),DATE_, 112) as int) DateKey --primary key
	, DATE_ DateValue
--value keys
    , YEAR(DATE_) YearValue
	, datepart(qq,date_) QuarterValue
    , MONTH(DATE_) MonthValue
	, DATEPART(dd,DATE_) DayOfMonthValue
	, DATEpart(dw,DATE_) DayOfWeekValue
	, DATEpart(wk,DATE_) WeekOFYearValue
    , DATEpart(dy,DATE_) DayOfYearValue
	, YEAR(DATE_) * 100 + datepart(qq,date_) YearQuarterKey
	, YEAR(DATE_) * 100 + month(date_) YearMonthKey
	, YEAR(DATE_) * 100 + DATEpart(wk,DATE_) YearWeekKey 

--descriptive attributes
	, CONVERT(VARCHAR(11), DATE_, 106) FullDate
	, DATEname(dw,DATE_) + ', ' + CONVERT(VARCHAR(11), DATE_, 106) FullDateDOW
    , 'Qtr ' + DATENAME(qq, DATE_) QuarterDescription
	, DATENAME(M,DATE_) MonthDescription
	, DATEname(dw,DATE_) DayOfWeekDescription
	, datename(YYYY, date_) + ' Q' + DATENAME(qq, DATE_) YearQuarterDescription
	, 'Q' + DATENAME(qq, DATE_) + ' ' + datename(YYYY, date_) QuarterYearDescription
	, datename(YYYY, date_) + ' ' + DATENAME(M, DATE_) YearMonthDescription
	, DATENAME(M, DATE_) + ' ' + datename(YYYY, date_) MonthYearDescription
	, datename(YYYY, date_) + ' w' + DATENAME(wk, DATE_) YearWeekDescription
	, 'Week ' + DATENAME(wk, DATE_) + ' ' + datename(YYYY, date_) WeekYearDescription
	, datename(YYYY, date_) + ' d' + DATENAME(dy, DATE_) YearDOYDescription
	, 'Day ' + DATENAME(dy, DATE_) + ' ' + datename(YYYY, date_) DOYYearDescription
FROM FixedDate OPTION (MAXRECURSION 0)



/*
;WITH DimDateCTE AS
(
SELECT datefromparts(@startyear,1,1) DATE_
UNION ALL
SELECT DATEADD(DAY,1,DimDateCTE.DATE_) DATE_ FROM DimDateCTE WHERE DATE_ < datefromparts(@EndYear,12,31) 
)
INSERT INTO dbo.dimDate --Use this part if there is an existing DimDate
SELECT distinct
--primary and alternate key
	CAST(CONVERT(VARCHAR(8),DATE_, 112) as int) DateKey --primary key
	, DATE_ DateValue
--value keys
    , YEAR(DATE_) YearValue
	, datepart(qq,date_) QuarterValue
    , MONTH(DATE_) MonthValue
	, DATEPART(dd,DATE_) DayOfMonthValue
	, DATEpart(dw,DATE_) DayOfWeekValue
	, DATEpart(wk,DATE_) WeekOFYearValue
    , DATEpart(dy,DATE_) DayOfYearValue
	, YEAR(DATE_) * 100 + datepart(qq,date_) YearQuarterKey
	, YEAR(DATE_) * 100 + month(date_) YearMonthKey
	, YEAR(DATE_) * 100 + DATEpart(wk,DATE_) YearWeekKey 

--descriptive attributes
	, CONVERT(VARCHAR(11), DATE_, 106) FullDate
	, DATEname(dw,DATE_) + ', ' + CONVERT(VARCHAR(11), DATE_, 106) FullDateDOW
    , 'Qtr ' + DATENAME(qq, DATE_) QuarterDescription
	, DATENAME(M,DATE_) MonthDescription
	, DATEname(dw,DATE_) DayOfWeekDescription
	, datename(YYYY, date_) + ' Q' + DATENAME(qq, DATE_) YearQuarterDescription
	, 'Q' + DATENAME(qq, DATE_) + ' ' + datename(YYYY, date_) QuarterYearDescription
	, datename(YYYY, date_) + ' ' + DATENAME(M, DATE_) YearMonthDescription
	, DATENAME(M, DATE_) + ' ' + datename(YYYY, date_) MonthYearDescription
	, datename(YYYY, date_) + ' w' + DATENAME(wk, DATE_) YearWeekDescription
	, 'Week ' + DATENAME(wk, DATE_) + ' ' + datename(YYYY, date_) WeekYearDescription
	, datename(YYYY, date_) + ' d' + DATENAME(dy, DATE_) YearDOYDescription
	, 'Day ' + DATENAME(dy, DATE_) + ' ' + datename(YYYY, date_) DOYYearDescription
FROM DimDateCTE OPTION (MAXRECURSION 0)
*/
--drop table dbo.dimdate

--select * from dbo.dimDate

end