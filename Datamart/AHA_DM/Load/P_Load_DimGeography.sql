use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_DimGeography' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_DimGeography'
	drop procedure dbo.Load_DimGeography
end
else
	print 'New Procedure created - Load_DimGeography'
GO

Create procedure dbo.Load_DimGeography
@MasterControlID int

as
/*
exec DIM.Load_DimGeography -9
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load Dim AHAGeography'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


--scd type 1 == current values plus historic items left in table.
--truncate table DIM.AHAGeography

insert into dbo.DIMGeography (Country, RegionState, CountyName, ZipCode, ZipName, Affiliate, PopulatedDateTime, ExtractControlID)
select 'USA', [State], [CountyName],[ZIP_Code],[ZIP_Name],[Affiliate],[ExtractPopulatedDateTime],[ExtractControlID]
from [H360].[Extract_ZipRegionMapping]
where foundmatchflag = 0


--logging here - log extract row count
set @TotalRowCount = isnull((select count(z.ExtractRowid) from H360.[Extract_ZipRegionMapping] z WHERE FOUNDMATCHFLAG = 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into dimension')

set @totalrowcount = 0
set @st2 = getdate()

update g
set g.zipname =  case when isnull(g.ZipName,'') <>  isnull(e.zip_name,'') then e.zip_name else g.zipname end,
	g.RegionState = case when isnull(g.RegionState,'') <>  isnull(e.State,'') then e.State else g.RegionState end,
	g.Affiliate = case when isnull(g.Affiliate,'') <>  isnull(e.affiliate,'') then e.Affiliate else g.Affiliate end,
	g.countyname = case when isnull(g.countyname,'') <>  isnull(e.countyname,'') then e.countyname else g.countyname end
from 
	dbo.DIMGeography g 
	inner join [H360].[Extract_ZipRegionMapping] e on e.foundmatchflag = g.geographykey
where isnull(e.FoundMatchFlag,0) <> 0 and e.datachangeflag <> 0

set @TotalRowCount = isnull((select count(z.ExtractRowid) from H360.[Extract_ZipRegionMapping] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag <> 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@st2, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'UpdatedRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Updated in dimension')

set @TotalRowCount = isnull((select count(z.ExtractRowid) from H360.[Extract_ZipRegionMapping] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag = 0),0)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ExistingRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Existing Rows Ignored in dimension')

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessCompleted', '<RowCount>0</RowCount><Message>Success</Message>', 'Dimension Load Completed')


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end