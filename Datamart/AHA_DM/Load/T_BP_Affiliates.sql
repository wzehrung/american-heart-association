use [HealthDataMart]
go

if exists (select * from sysobjects where name = 'DimAffiliate' and xtype = 'U')
	begin
		print 'Table DimAffiliate exists - dropped to recreate'
		drop table dbo.DimAffiliate
	end
else
	print 'Table DimAffiliate did not exist - creating new table'
go

create table dbo.DimAffiliate(
	--SCD1 type dimension
	AffiliateKey [int] identity(-1,1) not null constraint PK_DimAffiliate primary key nonclustered,
	--arbitrary key
	AffiliateCode varchar(20), --affiliateID --bk
	AffiliateName varchar(50),
	AffiliateAbbreviation varchar(20),
	--control columns
	PopulatedDateTime datetime
		constraint DF_dbo_DimAffiliate_ExtractDT default(getdate()),
	ExtractControlID int -- master wrapper controlID for the insert update --
	--scd2 control not included
		--IsCurrent int,
		--ValidFromDate date,
		--ValidToDate date,
	--optional sc1 controls
		--UpdatedDate datetime --when this is used it is the last execution time that updated the value
		--LEID int --when this is used it is the last execution ID that updated the value
) on [PRIMARY]

insert into dbo.DimAffiliate(AffiliateCode, AffiliateName, AffiliateAbbreviation, ExtractControlID )
values('-1','Unknown','Unknown',-1),('0','NA','NA',-1)