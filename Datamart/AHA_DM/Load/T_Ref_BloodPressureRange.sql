use [HealthDataMart]
go

if exists (select * from sysobjects where name = 'BloodPressureRange' and xtype = 'U')
	begin
		print 'Table Ref.BloodPressureRange exists - dropped to recreate'
		drop table Ref.BloodPressureRange
	end
else
	print 'Table Ref.BloodPressureRange did not exist - creating new table'
go

create table Ref.BloodPressureRange(
BPReferenceID int identity(1,1) 
	constraint PK_Ref_BloodPressureRange Primary Key nonclustered,
SystolicFrom decimal(16,6),
SystolicTo decimal(16,6),
DiastolicFrom decimal(16,6),
DiastolicTo decimal(16,6),
BPCategoryName varchar(50),
BPCategoryKey int,
--
DateCreated datetime
	constraint DF_Ref_BloodPressureRange_DateCreated default(Getdate()),
IsActive int
	constraint DF_Ref_BloodPressureRange_IsActive default(1),
Notes varchar(500)
) on [Primary]

/*
BPCategoryKey	BPCategoryCode	BPCategoryDescription
-1	Unknown	Unknown
0	Normal	In normal range <120/80
1	PreHyperTensive	Between 120-140 or 80-90
2	High	Over 140/90
*/

truncate table ref.bloodpressurerange

insert into ref.bloodpressurerange(SystolicFrom, SystolicTo, DiastolicFrom, DiastolicTo, BPCategoryName, BPCategoryKey, Notes)
values(-999,0,-999,0, 'Unknown', -1, 'Unknown or values dont map correctly'),
		(0,120,0,80,'Normal',0,'In the normal range (including low BP) - Both figures below max'),
		(120,140,80,90,'PreHyperTensive',1,'Either figure over minimum but not over maximum'),
		(140,999,90,999,'High',2,'Either figure is above the minimum level')


/*
expected usage
==============
functional prototype of subselect -- b = blood pressure reading; r = reference table


BPCategorykey = case 
when nullif(b.systolic,'') is null or nullif(b.diastolic,'') then -1 --(unknown for invalid reading)
else isnull((select top 1 r.BPCategoryKey 
			from Ref.BloodPressureRange r
			where b.systolic between r.SystolicFrom and r.SystolicTo OR b.diasolic between r.diastolicFrom and r.diastolicTo 
			order by r.BPCategoryKey desc)
			,-1)
end

*/