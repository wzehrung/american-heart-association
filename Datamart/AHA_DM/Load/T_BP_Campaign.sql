use [HealthDataMart]
go

if exists (select * from sysobjects where name = 'DimCampaign' and xtype = 'U')
	begin
		print 'Table DimCampaign exists - dropped to recreate'
		drop table dbo.DimCampaign
	end
else
	print 'Table DimCampaign did not exist - creating new table'
go

create table dbo.DimCampaign(
	--SCD1 type dimension
	CampaignKey [int] identity(-1,1) not null constraint PK_DimCampaign primary key nonclustered,
	--arbitrary key
	CampaignID int, --bk
	CampaignCode varchar(50), --affiliateID --bk
	CampaignTitle varchar(200),
	FromDate date,
	ToDate date,	
	--control columns
	PopulatedDateTime datetime
		constraint DF_dbo_DimCampaign_ExtractDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID for the insert update --
	--scd2 control not included
		--IsCurrent int,
		--ValidFromDate date,
		--ValidToDate date,
	--optional sc1 controls
	UpdatedDate datetime
		constraint DF_dbo_DimCampaign_UpdatedDT default(getdate()), --when this is used it is the last execution time that updated the value
	LEID int --when this is used it is the last execution ID that updated the value
) on [PRIMARY]

insert into dbo.DimCampaign(CampaignID, CampaignCode, CampaignTitle, FromDate, ToDate, ExtractControlID, LEID )
values(-1,'Unknown','Unknown','1900-01-01','9999-12-31',-1,-1),(0,'NA','NA','1900-01-01','9999-12-31',-1,-1)

select * from dimcampaign

