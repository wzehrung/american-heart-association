use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_DimEthnicity' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_DimEthnicity'
	drop procedure dbo.Load_DimEthnicity
end
else
	print 'New Procedure created - Load_DimEthnicity'
GO

Create procedure dbo.Load_DimEthnicity
@MasterControlID int

as
/*
exec dbo.Load_DimEthnicity -9
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load Ethnicity'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


--scd type 1 == current values plus historic items left in table.
--truncate table DIM.AHAGeography

insert into dbo.DIMEthnicity ([EthnicityCode], [EthnicityDescription], ExtractControlID)
select e.[EthnicityCode] ,e.[EthnicityDescription],e.[ExtractControlID]
from [H360].[Extract_Ethnicity] e
where e.foundmatchflag = 0


--logging here - log extract row count
set @TotalRowCount = isnull((select count(z.[Extract_EthnicityKey]) from H360.[Extract_Ethnicity] z WHERE z.FOUNDMATCHFLAG = 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into dimension')

set @totalrowcount = 0
set @st2 = getdate()

update d
set d.ethnicitydescription = isnull(e.ethnicitydescription,''), 
	populateddatetime = getdate(), 
	extractcontrolid = @mastercontrolID
from 
	dbo.DIMethnicity d
	inner join [H360].[Extract_Ethnicity] e on e.foundmatchflag = d.ethnicitykey
where isnull(e.FoundMatchFlag,0) <> 0 and e.datachangeflag <> 0

set @TotalRowCount = isnull((select count(z.[Extract_EthnicityKey]) from H360.[Extract_Ethnicity] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag <> 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@st2, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'UpdatedRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Updated in dimension')

set @TotalRowCount = isnull((select count(z.[Extract_EthnicityKey]) from H360.[Extract_Ethnicity] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag = 0),0)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ExistingRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Existing Rows Ignored in dimension')

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessCompleted', '<RowCount>0</RowCount><Message>Success</Message>', 'Dimension Load Completed')


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end