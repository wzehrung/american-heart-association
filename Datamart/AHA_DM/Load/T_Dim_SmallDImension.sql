/*
Trivial Dimensions
--gender (male, female)
--retention (retained,not retained)
--validuser (yes,no)
--bpcategory (high,pre,normal)
--improvementcategroy (better,same,worse)
*/
use [HealthDataMart]
go

if exists (select * from sysobjects where name = 'DimGender' and xtype = 'U')
	begin
		print 'Table DimGender exists - dropped to recreate'
		drop table dbo.DimGender
	end
else
	print 'Table DimGender did not exist - creating new table'
go

create table dbo.DimGender(
--
	GenderKey [int] not null constraint PK_Dim_Gender primary key nonclustered,
	GenderCode varchar(20),
	GenderDescription varchar(50)
) on [PRIMARY]

insert into dbo.DimGender(genderkey,gendercode, genderdescription)
values(-1,'Unknown','Unknown'),(0,'NA','NA'),(1,'Male','Male'),(2,'Female','Female')



if exists (select * from sysobjects where name = 'DimRetention' and xtype = 'U')
	begin
		print 'Table DimRetention exists - dropped to recreate'
		drop table bp.DimRetention
	end
else
	print 'Table DimRetention did not exist - creating new table'
go

create table bp.DimRetention(
--
	RetentionKey [int] not null constraint PK_Dim_DimRetention primary key nonclustered,
	RetentionCode varchar(20),
	RetentionDescription varchar(50)
) on [PRIMARY]

insert into bp.DimRetention(RetentionKey,RetentionCode, RetentionDescription)
values(-1,'Unknown','Unknown'),(0,'Not Retained','Does not meet rules for retention'),(1,'Retained','Meets rules for retention')


if exists (select * from sysobjects where name = 'DimValidUser' and xtype = 'U')
	begin
		print 'Table DimValidUser exists - dropped to recreate'
		drop table bp.DimValidUser
	end
else
	print 'Table DimValidUser did not exist - creating new table'
go

create table bp.DimValidUser(
--
	ValidUserKey [int] not null constraint PK_Dim_ValidUser primary key nonclustered,
	ValidUserCode varchar(20),
	ValidUserDescription varchar(50)
) on [PRIMARY]

insert into bp.DimValidUser(ValidUserKey,ValidUserCode, ValidUserDescription)
values(-1,'Unknown','Unknown'),(0,'No','Does not meet valid user requirment'),(1,'Yes','Meets requirement for valid user')


if exists (select * from sysobjects where name = 'DimBPCategory' and xtype = 'U')
	begin
		print 'Table DimBPCategory exists - dropped to recreate'
		drop table bp.DimBPCategory
	end
else
	print 'Table DimBPCategory did not exist - creating new table'
go

create table bp.DimBPCategory(
--
	BPCategoryKey [int] not null constraint PK_Dim_BPCategory primary key nonclustered,
	BPCategoryCode varchar(20),
	BPCategoryDescription varchar(50)
) on [PRIMARY]

insert into bp.DimBPCategory(BPCategoryKey,BPCategoryCode, BPCategoryDescription)
values(-1,'Unknown','Unknown'),(0,'Normal','In normal range <120/80'),(1,'PreHyperTensive','Between 120-140 or 80-90'),(2,'High','Over 140/90')




if exists (select * from sysobjects where name = 'DimImprovementCategory' and xtype = 'U')
	begin
		print 'Table DimImprovementCategory exists - dropped to recreate'
		drop table bp.DimImprovementCategory
	end
else
	print 'Table DimImprovementCategory did not exist - creating new table'
go

create table bp.DimImprovementCategory(
	ImprovementCategoryKey [int] not null constraint PK_Dim_ImprovementCategory primary key nonclustered,
	ImprovementCategoryCode varchar(20),
	ImprovementCategoryDescription varchar(50)
) on [PRIMARY]

insert into bp.DimImprovementCategory(ImprovementCategoryKey,ImprovementCategoryCode, ImprovementCategoryDescription)
values(-1,'Unknown','Unknown'),(0,'Better','Measured value has improved'),(1,'Same','Measured value is same or similar'),(2,'Worse','Measured value has deteriorated')

