use [HealthDataMart]
GO

drop table BP.DimBPReadingComments
go

create table BP.DimBPReadingComments(
BPCommentKey int identity(-1,1),
Comment varchar(7500),
InsertedDate datetime,
InsertedProcessID int
) on [PRIMARY]
GO 

insert into BP.DimBPReadingComments(comment, inserteddate, insertedprocessid)
values('[NULL]','1900-01-01',-1),('<Blank>','1900-01-01',-1)
GO