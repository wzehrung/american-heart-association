--P_Load_FactUserMonthlyRetention

use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_FactUserMonthlyRetention' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_FactUserMonthlyRetention'
	drop procedure BP.Load_FactUserMonthlyRetention
end
else
	print 'New Procedure created - Load_FactUserMonthlyRetention'
GO

create procedure BP.Load_FactUserMonthlyRetention
@MasterControlID int

as

/*

BP.Load_FactUserMonthlyRetention -9909
*/
begin

/*
check if there is one month, multiple - up to 5 or all.
=======================================================
delete, delete, truncate
*/

--if count(reportingmonths) > 5 OR @incrementalLoad = 0 --process control set to not allow incremental load or there are more than 5 months to load.
truncate table [BP].[Fact_HRMonthlyRetention]
--else
--	DELETE FROM BP.Fact_HRMonthlyRetention WHERE reportingmonthkey in (select distinct reportingmonthkey from [BP].[Staging_HRMonthlyRetention])

insert into BP.Fact_HRMonthlyRetention
(HealthRecordKey, UserHealthRecordID, ReportingMonthKey, FirstReadingInPeriod, LatestReadingInPeriod, 
	Month1Count, Month2Count, Month3Count, Month4Count, RetainedUserFlag, validuserflag, PopulationDateTime, ExtractControlID)
select HealthRecordKey, UserHealthRecordID, ReportingMonthKey, FirstReadingInPeriod, LatestReadingInPeriod, 
	Month1Count, Month2Count, Month3Count, Month4Count, RetainedUserFlag, validuserflag, PopulationDateTime, ExtractControlID
from [BP].[Staging_HRMonthlyRetention]

end