use [HealthDataMart]
go

if exists (select * from sysobjects where name = 'DimEthnicity' and xtype = 'U')
	begin
		print 'Table DimEthnicity exists - dropped to recreate'
		drop table dbo.DimEthnicity
	end
else
	print 'Table DimEthnicity did not exist - creating new table'
go

create table dbo.DimEthnicity(
--SCD1 type dimension
	EthnicityKey [int] identity(-1,1) not null constraint PK_DimEthnicity primary key nonclustered,
	EthnicityCode varchar(20),
	EthnicityDescription varchar(50),
	--control columns
	PopulatedDateTime datetime
		constraint DF_dbo_DimEthnicity_ExtractDT default(getdate()),
	ExtractControlID int -- master wrapper controlID for the insert update
	--scd2 control not included
) on [PRIMARY]

insert into dbo.DimEthnicity(EthnicityCode, EthnicityDescription, ExtractControlID )
values('Unknown','Unknown',-1),('Not Reported','Not Reported',-1)