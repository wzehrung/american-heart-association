use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'DimGeography' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - DimGeography'
	drop table dbo.DimGeography
end
else
	print 'New Table created - DimGeography'
GO

create table dbo.DimGeography
(
	--surrogate key
	GeographyKey int identity(0,1)
		constraint PK_dbo_DimGeography Primary Key NONCLUSTERED, 
	--
	Country varchar(50), --ak1 
	RegionState varchar(50), --ak2
	CountyName varchar(50),
	ZipCode varchar(10), --validation should be 5 char long for USA --main business key -ak3
	ZipName varchar(50),
	Affiliate varchar(100),
	--control columns
	PopulatedDateTime datetime
		constraint DF_dbo_Dimgeography_ExtractDT default(getdate()),
	ExtractControlID int -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
)
--zero is unknown
insert into dbo.dimgeography(Country,RegionState, CountyName, ZipCode, ZipName, Affiliate,ExtractControlID)
values('NA','NA','NA','NA','NA', 'NA', -1)
go

create index IDX_dbo_DimGeography_MatchPerf01
on dbo.DimGeography(zipcode, zipname, regionstate, affiliate, countyname) include(geographykey)

--select * from dim.ahageography

--clustered index - country,regionstate,zipcode
--create index zipcode, affiliate, extract

/*

CREATE VIEW [dbo].[v_ZipRegionMapping]
AS

SELECT
	CASE
		WHEN LEN(ZIP_Code) < 5
		THEN '0' + [ZIP_Code]
		ELSE ZIP_Code
	END ZipCode,
	Zip_Name AS City,
	State,
	Affiliate,
	CountyName AS County

FROM ZipRegionMapping

GO


*/