/*
SIMPLE SCD1 Dimension

copy.
search and replace H360 with schema actual name
search and replace Extract_HealthRecordExtended with dimension actual name
save

add field list to d

change the Null/unknown/NA insert row to suit columns and business rules
save

execute
*/
use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'DimHealthRecord' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - DimHealthRecord'
	drop table BP.HealthRecord
end
else
	print 'New Table created - DimHealthRecord'
GO

create table BP.DimHealthRecord
(
	--surrogate key
	HealthRecordKey int identity(-1,1)
		constraint PK_BP_DimHealthRecord Primary Key NONCLUSTERED, 
	--main extract table field list
	--HR
	UserHealthRecordID int not null, --bk
	HRCreatedDate datetime,
	HRUpdatedDate datetime,
	HRLastActivityDate datetime,
	CurrentCampaignID int,
	IsMigrated int,
	PolkaID varchar(10),
	--extended
	BloodType varchar(100),
	CountryRegion varchar(50),
	ZipCode varchar(50),
	
	Gender varchar(10),
	Ethnicity varchar(50),
	MaritalStatus varchar(50),
	YearOfBirth int,
	--Key maps
	CurrentCampaign_SFKey int,
	Geography_SFKey int,
	Gender_SFKey int,
	Ethnicity_SFKey int,
	--control columns
	PopulatedDateTime datetime
		constraint DF_BP_HealthRecord_PopulatedDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	UpdatedDate datetime
		constraint DF_BP_HealthRecord_Updated default(getdate()),
	LEID int --last execution updated id

)
/*
--index - create index on main business key; include extract control - if scd2 include from and to date in main index
create index IDX_H360_Extract_HealthRecordExtended_MatchPerf01
ON H360.Extract_HealthRecordExtended(UserHealthRecordID,ExtendedProfileID) INCLUDE(ExtractControlID) --scd1

create index IDX_H360_Extract_HealthRecordExtended_ChangePerf01
ON H360.Extract_HealthRecordExtended(UserHealthRecordID,ExtendedProfileID) INCLUDE(CountryRegion, ZipCode, MaritalStatus, BloodType) --scd1

--check business logic of dimension for NA and Unknown otherwise Unknown = -1, NA = 0

insert into H360.Extract_HealthRecordExtended( UserHealthRecordID, ExtendedProfileID, CurrentCampaignID, Ethnicity, ZipCode ,ExtractControlID) 
-- -1 Extract Control ID is the default initial insert of unknown and test data; 0 = initial population data; > 1 live populations
values (-1,-1,-1,'Unknown','Unknown', -1)
*/
go


/*

*/