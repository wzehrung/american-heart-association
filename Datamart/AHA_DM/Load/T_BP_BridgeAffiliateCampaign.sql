use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Bridge_AffiliateCampaign' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Bridge_AffiliateCampaign'
	drop table BP.Extract_AffiliateCampaign
end
else
	print 'New Table created - Bridge_AffiliateCampaign'
GO

create table BP.Bridge_AffiliateCampaign
(
	--surrogate key
	Bridge_AffiliateCampaignKey int identity(-1,1)
		constraint PK_BP_Bridge_AffiliateCampaign Primary Key NONCLUSTERED, 
	--main extract table field list
	AffiliateID int not null, --bk
	CampaignID int not null, --bk
	AffiliateKey int,
	CampaignKey int,
	--control columns 
	PopulatedDateTime datetime
		constraint DF_H360_Extract_AC_ExtractDT default(getdate()),
	ControlID int -- master wrapper controlID
)
