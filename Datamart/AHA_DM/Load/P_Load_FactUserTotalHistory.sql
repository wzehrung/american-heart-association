use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_FactUserTotalHistory' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_FactUserTotalHistory'
	drop procedure BP.Load_FactUserTotalHistory
end
else
	print 'New Procedure created - Load_FactUserTotalHistory'
GO

create procedure BP.Load_FactUserTotalHistory
@MasterControlID int

as

/*

BP.Load_FactUserTotalHistory -9909
*/
begin

--declare @MasterControlID int = -9999

truncate table BP.Fact_HRTotalHistory


insert into BP.Fact_HRTotalHistory(HealthRecordKey, userhealthrecordid, 
affiliatekey, campaignkey, geographykey, ethnicitykey, genderkey, FirstReadingYMKey,
totalhistoricreadingcount, firstreadingdate, FirstSystolic, FirstDiastolic, FirstBPCategoryKey, 
LatestReadingDate, LatestSystolic, LatestDiastolic, LatestBPCategoryKey, 
ChangeSystolic, ChangeDiastolic, SystolicImprovementCategoryKey, DiastolicImprovementCategoryKey, OverallImprovementCategoryKey, 
RetainedUserCandidateFlag, validuserflag, PopulationDateTime, ExtractControlID)
select 
HealthRecordKey, UserHealthRecordID, AffiliateKey, CampaignKey, GeographyKey, EthnicityKey, GenderKey, FirstReadingYMKey, 
totalhistoricreadingcount, firstreadingdate, FirstSystolic, FirstDiastolic, FirstBPCategoryKey, 
LatestReadingDate, LatestSystolic, LatestDiastolic, LatestBPCategoryKey, 
ChangeSystolic, ChangeDiastolic, SystolicImprovementCategoryKey, DiastolicImprovementCategoryKey, OverallImprovementCategoryKey, 
RetainedUserCandidateFlag, validuserflag, PopulationDateTime, ExtractControlID
from [BP].[Staging_HRTotalHistory]


/* log user total history reload*/

end