use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Fact_HRTotalHistory' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Fact_HRTotalHistory'
	drop table BP.Fact_HRTotalHistory
end
else
	print 'New Table created - Fact_HRTotalHistory'
GO



create table BP.Fact_HRTotalHistory(
HRTotalHistoryKey bigint identity(1,1) 
	constraint PK_BP_Fact_HRTotalHistory PRIMARY KEY NONCLUSTERED,
HealthRecordKey int, 
UserHealthRecordID int, --bk just incase rekeying is required
--descriptor dimensions -these are derived off the primary fact so doesnt need the bk
AffiliateKey int, 
CampaignKey int, 
GeographyKey int, 
EthnicityKey int, 
GenderKey int,
FirstReadingYMKey int,
--all dimensions if null then -1 (unknown)

--measures
totalhistoricreadingcount int, 
firstreadingdate datetime, 
FirstSystolic int, 
FirstDiastolic int,
FirstBPCategoryKey int,

LatestReadingDate datetime,
LatestSystolic int, 
LatestDiastolic int,
LatestBPCategoryKey int,

ChangeSystolic int, 
ChangeDiastolic int,

SystolicImprovementCategoryKey int,
DiastolicImprovementCategoryKey int,

OverallImprovementCategoryKey int,

--this is the "over all time" perspective, retained and valid have to be calculated on the fly depending on the date being applied.

RetainedUserCandidateFlag int, 
--= case when count(*) > 7 then 1 else 0 end,  ---rule from first part of if statement on retention calculation sheet column B
validuserflag int,
--= case when count(*) = 1 then 0		when datediff(d,min(BPMeasuredDate),max(BPMeasuredDate)) > 7 then 1		else 0		end

--control
PopulationDateTime datetime
	constraint DF_BP_Fact_HRTotalHistory_PopulationDateTime default(getdate()),

ExtractControlID int
) on [PRIMARY]
Go

create index IDX_BP_Fact_HRTotalHistory_Population_Perf001
ON BP.Fact_HRTotalHistory(HealthrecordKey,FirstReadingDate,LatestReadingDate) include(validuserflag)
Go

create index IDX_BP_Fact_HRTotalHistory_AffiliateValid_Perf001
ON BP.Fact_HRTotalHistory(AffiliateKey, ValidUserFlag, RetainedUserCandidateFlag) include(HealthrecordKey, CampaignKey)
Go
