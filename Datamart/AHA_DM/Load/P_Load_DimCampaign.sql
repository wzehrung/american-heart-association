use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_DimCampaign' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_DimCampaign'
	drop procedure dbo.Load_DimCampaign
end
else
	print 'New Procedure created - Load_DimCampaign'
GO

Create procedure dbo.Load_DimCampaign
@MasterControlID int

as
/*
exec dbo.Load_DimCampaign -9
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load Campaign'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


--scd type 1 == current values plus historic items left in table.
--truncate table DIM.AHAGeography

insert into [dbo].[DimCampaign] (CampaignID, CampaignCode, CampaignTitle, FromDate, ToDate, ExtractControlID, LEID)
select e.[CampaignID], e.[CampaignCode], e.[CampaignTitle], e.[FromDate], e.[ToDate], e.[ExtractControlID], e.[ExtractControlID] --initial LEID is same as Extract
from [H360].[Extract_Campaign] e
where e.foundmatchflag = 0


--logging here - log extract row count
set @TotalRowCount = isnull((select count(z.[Extract_CampaignKey]) from H360.[Extract_Campaign] z WHERE z.FOUNDMATCHFLAG = 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into dimension')

set @totalrowcount = 0
set @st2 = getdate()

update d
set d.campaigncode = case when isnull(d.campaigncode ,'') <> isnull(e.campaigncode ,'') then isnull(e.campaigncode,'') else d.campaigncode end, 
	d.campaignTitle = case when isnull(d.campaignTitle ,'') <> isnull(e.campaignTitle ,'') then isnull(e.campaignTitle,'') else d.campaignTitle end,  
	d.FromDate = case when isnull(d.FromDate ,'1900-01-01') <> isnull(e.FromDate ,'1900-01-01') then isnull(e.FromDate,'1900-01-01') else d.FromDate end,  
	d.todate = case when isnull(d.todate ,'9999-12-31') <> isnull(e.todate ,'9999-12-31') then isnull(e.todate,'9999-12-31') else d.todate end,  
	--CONTROL UPDATE
	--original extract controlID and Populated date are from initial population.
	d.updateddate = getdate(), 
	d.LEID = @mastercontrolID
from 
	dbo.DimCampaign d
	inner join [H360].[Extract_Campaign] e on e.foundmatchflag = d.CampaignKey
where isnull(e.FoundMatchFlag,0) <> 0 and e.datachangeflag <> 0

set @TotalRowCount = isnull((select count(z.[Extract_CampaignKey]) from H360.[Extract_Campaign] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag <> 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@st2, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'UpdatedRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Updated in dimension')

set @TotalRowCount = isnull((select count(z.[Extract_CampaignKey]) from H360.[Extract_Campaign] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag = 0),0)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ExistingRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Existing Rows Ignored in dimension')

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessCompleted', '<RowCount>0</RowCount><Message>Success</Message>', 'Dimension Load Completed')


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end