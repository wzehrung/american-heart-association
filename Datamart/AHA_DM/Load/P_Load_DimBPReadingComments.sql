use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_DimBPReadingCommentInfo' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_DimBPReadingCommentInfo'
	drop procedure dbo.Load_DimBPReadingCommentInfo
end
else
	print 'New Procedure created - Load_DimBPReadingCommentInfo'
GO

Create procedure dbo.Load_DimBPReadingCommentInfo
@MasterControlID int

as
/*
exec dbo.Load_DimBPReadingCommentInfo -9
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load BPComment'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


--scd type 1 == current values plus historic items left in table.
--truncate table DIM.AHAGeography

insert into BP.DimBPReadingComments (Comment, InsertedDate, InsertedProcessID)
select [Comments],[PopulatedDateTime],[ExtractControlID]
from [H360].[Staging_BloodPressureComments] e
where e.[BPCommentKey] is null


--logging here - log extract row count
set @TotalRowCount = isnull((select count(z.[BPCommentExtractRowID]) from [H360].[Staging_BloodPressureComments] z WHERE z.bpcommentkey is null),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into dimension')

set @TotalRowCount = isnull((select count(z.[Extract_CampaignKey]) from H360.[Extract_Campaign] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag = 0),0)


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end