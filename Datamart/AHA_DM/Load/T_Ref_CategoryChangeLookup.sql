create table ref.CategoryChangeLookup(
ChangeCategoryID int identity(1,1) constraint PK_Ref_CategoryChangeLookup PRIMARY KEY NONCLUSTERED,

CategoryCode varchar(20),
ItemCode varchar(20),

FromValue decimal(16,6),
ToValue decimal(16,6),

DimensionCode varchar(20),
DimensionKey int, --optional

Notes varchar(200)
) on [PRIMARY]


insert into ref.categorychangelookup(categorycode,itemcode,fromvalue,tovalue,dimensioncode, dimensionkey, notes)
values('BPChange','Systolic',-999999, -50, 'Unknown',-1,'The change is too large to be valid'),
	('BPChange','Systolic',-50, -5, 'Better',0,'Reduced BP is better'),
	('BPChange','Systolic',-5, 2, 'Same',1,'The change is small enough to be margin of error'),
	('BPChange','Systolic',2, 50, 'Worse',2,'Increase BP is worse'),
	('BPChange','Systolic',50, 999999, 'Unknown',-1,'The change is too large to be valid'),
	('BPChange','Diastolic',-999999, -25, 'Unknown',-1,'The change is to large to be valid'),
	('BPChange','Diastolic',-25, -3, 'Better',-1,'Reduced BP is better'),
	('BPChange','Diastolic',-3, 2, 'Same',-1,'The change is small enough to be margin of error'),
	('BPChange','Diastolic',2, 25, 'Worse',-1,'Increase BP is worse'),
	('BPChange','Diastolic',25, 999999, 'Unknown',-1,'The change is to large to be valid')


select * from ref.categorychangelookup