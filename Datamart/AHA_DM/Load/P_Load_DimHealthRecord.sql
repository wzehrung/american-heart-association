use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_HealthRecord' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_HealthRecord'
	drop procedure dbo.Load_HealthRecord
end
else
	print 'New Procedure created - Load_HealthRecord'
GO

Create procedure dbo.Load_HealthRecord
@MasterControlID int

as
/*
exec dbo.Load_HealthRecord -10
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load HealthRecord'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

--logging here - log extract row count
set @TotalRowCount = isnull((select count(z.[Extract_HealthRecordExtendedKey]) from [H360].[Extract_HealthRecordExtended] z WHERE FOUNDMATCHFLAG = 0),0)

--truncate table [BP].[DimHealthRecord]
insert into [BP].[DimHealthRecord](UserHealthRecordID, HRCreatedDate, HRUpdatedDate, HRLastActivityDate, 
	CurrentCampaignID, IsMigrated, PolkaID, BloodType, 
	CountryRegion, ZipCode, Gender, Ethnicity, MaritalStatus, YearOfBirth, 
	CurrentCampaign_SFKey, Geography_SFKey, Gender_SFKey, Ethnicity_SFKey, 
	ExtractControlID, LEID)
select
	UserHealthRecordID, HRCreatedDate, HRUpdatedDate, HRLastActivityDate, 
	CurrentCampaignID, IsMigrated, PolkaID, BloodType, 
	CountryRegion, ZipCode, Gender, Ethnicity, MaritalStatus, YearOfBirth, 
	--
	CurrentCampaign_SFKey = isnull((select top 1 campaignkey from [dbo].[DimCampaign] l where e.currentcampaignid = l.campaignid order by updateddate desc),-1), 
	Geography_SFKey = isnull((select top 1 geographykey from dbo.dimgeography l 
							where e.countryregion = replace(l.country,'USA','United States') and e.zipcode = l.zipcode order by populateddatetime desc),-1), 
	Gender_SFKey = isnull((select top 1 genderkey from dbo.dimgender l where e.gender = l.gendercode),-1), 
	Ethnicity_SFKey = isnull((select top 1 ethnicitykey from dbo.dimethnicity l 
							where isnull(nullif(nullif(e.Ethnicity,''),'Select:'),'Not Reported') = l.ethnicitydescription order by populateddatetime desc),-1),
	--
	ExtractControlID, ExtractControlID
from [H360].[Extract_HealthRecordExtended] e 
where FoundMatchFlag = 0


set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into dimension')

set @totalrowcount = 0
set @st2 = getdate()

update d
set
	d.PolkaID = case when isnull(d.PolkaID,'') <>  isnull(e.PolkaID,'') then e.PolkaID else d.PolkaID end,
	d.BloodType	= case when isnull(d.BloodType,'') <>  isnull(e.BloodType,'') then e.BloodType else d.BloodType end,
	d.CountryRegion	= case when isnull(d.CountryRegion,'') <>  isnull(e.CountryRegion,'') then e.CountryRegion else d.CountryRegion end,
	d.ZipCode = case when isnull(d.ZipCode,'') <>  isnull(e.ZipCode,'') then e.ZipCode else d.ZipCode end,
	d.Gender = case when isnull(d.Gender,'') <>  isnull(e.Gender,'') then e.Gender else d.Gender end,
	d.Ethnicity = case when isnull(d.Ethnicity,'') <>  isnull(e.Ethnicity,'') then e.Ethnicity else d.Ethnicity end,
	d.MaritalStatus	= case when isnull(d.MaritalStatus,'') <>  isnull(e.MaritalStatus,'') then e.MaritalStatus else d.MaritalStatus end,
	d.YearOfBIrth = case when isnull(d.YearOfBIrth,-1) <>  isnull(e.YearOfBIrth,-1) then e.YearOfBIrth else d.YearOfBIrth end,
	d.IsMigrated = case when isnull(d.IsMigrated,-1) <>  isnull(e.IsMigrated,-1) then e.IsMigrated else d.IsMigrated end,
	d.CurrentCampaignID	= case when isnull(d.CurrentCampaignID,0) <>  isnull(e.CurrentCampaignID,0) then isnull(e.CurrentCampaignID,0) else isnull(d.CurrentCampaignID,0) end,
	d.HRCreatedDate	= case when isnull(d.HRCreatedDate,'1900-01-01') <>  isnull(e.HRCreatedDate,'1900-01-01') then e.HRCreatedDate else d.HRCreatedDate end,
	d.HRUpdatedDate	= case when isnull(d.HRUpdatedDate,'9999-12-31') <>  isnull(e.HRUpdatedDate,'9999-12-31') then e.HRUpdatedDate else d.HRUpdatedDate end,
	d.HRLastActivityDate = case when isnull(d.HRLastActivityDate,'9999-12-31') <>  isnull(e.HRLastActivityDate,'9999-12-31') then e.HRLastActivityDate else d.HRLastActivityDate end,
	--
	CurrentCampaign_SFKey = isnull((select top 1 campaignkey from [dbo].[DimCampaign] l where isnull(e.currentcampaignid,0) = l.campaignid order by updateddate desc),-1), 
	Geography_SFKey = isnull((select top 1 geographykey from dbo.dimgeography l 
							where e.countryregion = replace(l.country,'USA','United States') and e.zipcode = l.zipcode order by populateddatetime desc),-1), 
	Gender_SFKey = isnull((select top 1 genderkey from dbo.dimgender l where e.gender = l.gendercode),-1), 
	Ethnicity_SFKey = isnull((select top 1 ethnicitykey from dbo.dimethnicity l 
							where isnull(nullif(nullif(e.Ethnicity,''),'Select:'),'Not Reported') = l.ethnicitycode order by populateddatetime desc),-1),
	UpdatedDate = getdate(),
	LEID = @MasterControlID
from [H360].Extract_HealthRecordExtended e
	inner join BP.DimHealthRecord d on e.foundmatchflag = d.HealthRecordKey
where isnull(e.FoundMatchFlag,0) <> 0 and e.datachangeflag <> 0


set @TotalRowCount = isnull((select count(z.[Extract_HealthRecordExtendedKey]) from H360.[Extract_HealthRecordExtended] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag <> 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@st2, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'UpdatedRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Updated in dimension')

set @TotalRowCount = isnull((select count(z.[Extract_HealthRecordExtendedKey]) from H360.[Extract_HealthRecordExtended] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag = 0),0)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ExistingRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Existing Rows Ignored in dimension')

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

--unknown dimension key statistic
set @TotalRowCount = isnull((select count(*) from bp.DimHealthRecord z 
	WHERE ([ExtractControlID] = @mastercontrolid OR [LEID] = @mastercontrolid) and 
			(currentcampaign_sfkey = -1 OR geography_sfkey = -1 OR gender_sfkey = -1 OR ethnicity_SFKey = -1) ),0)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'DimMapStats', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '/RowCount><Message>Rows with unknown dimension key</Message>', 'Statistics')


insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessCompleted', '<RowCount>0</RowCount><Message>Success</Message>', 'Dimension Load Completed')


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end