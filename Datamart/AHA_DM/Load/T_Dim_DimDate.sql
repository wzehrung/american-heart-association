/*
Date dimension
*/

if exists (select * from sysobjects where name = 'DimDate' and xtype = 'U')
	begin
		print 'Table Dimension Date exists - dropped to recreate - needs repopulation'
		drop table dbo.dimDate
	end
else
	print 'Table DimDate did not exist - creating new table - needs to be populated'
go

create table dbo.DimDate(
--
	[DateKey] [int] not null constraint PK_dbo_DimDate primary key nonclustered,
	[DateValue] [date] NULL,
--
	[YearValue] [int] NULL,
	[QuarterValue] [int] NULL,
	[MonthValue] [int] NULL,
	[DayOfMonthValue] [int] NULL,
	[DayOfWeekValue] [int] NULL,
	[WeekOFYearValue] [int] NULL,
	[DayOfYearValue] [int] NULL,
--
	[YearQuarterKey] [int] NULL,
	[YearMonthKey] [int] NULL,
	[YearWeekKey] [int] NULL,
--
	[FullDate] [varchar](50) NULL,
	[FullDateDOW] [nvarchar](50) NULL,
	[QuarterDescription] [nvarchar](50) NULL,
	[MonthDescription] [nvarchar](50) NULL,
	[DayOfWeekDescription] [nvarchar](50) NULL,
	[YearQuarterDescription] [nvarchar](80) NULL,
	[QuarterYearDescription] [nvarchar](80) NULL,
	[YearMonthDescription] [nvarchar](80) NULL,
	[MonthYearDescription] [nvarchar](80) NULL,
	[YearWeekDescription] [nvarchar](80) NULL,
	[WeekYearDescription] [nvarchar](80) NULL,
	[YearDOYDescription] [nvarchar](80) NULL,
	[DOYYearDescription] [nvarchar](80) NULL
) on [PRIMARY]

