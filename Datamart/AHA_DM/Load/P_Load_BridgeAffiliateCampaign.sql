use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_BridgeAffiliateCampaign' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_BridgeAffiliateCampaign'
	drop procedure dbo.Load_BridgeAffiliateCampaign
end
else
	print 'New Procedure created - Load_BridgeAffiliateCampaign'
GO

Create procedure dbo.Load_BridgeAffiliateCampaign
@MasterControlID int

as
/*
exec dbo.Load_BridgeAffiliateCampaign -6
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load AffiliateCampaign'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


insert into [BP].[Bridge_AffiliateCampaign] ( AffiliateID, CampaignID, AffiliateKey, CampaignKey, PopulatedDateTime, ControlID)
select AffiliateID, CampaignID, AffiliateMatchKey, CampaignMatchKey, [ExtractPopulatedDateTime], ExtractControlID
from [H360].[Extract_AffiliateCampaign] e 
where [AffiliateCampaignMatch] = -99


--logging here - log extract row count
set @TotalRowCount = isnull((select count(*) from [H360].[Extract_AffiliateCampaign] z WHERE z.[AffiliateCampaignMatch] = -99),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into bridge')


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end