use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_Small_Dimensions' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_Small_Dimensions'
	drop procedure BP.Load_Small_Dimensions
end
else
	print 'New Procedure created - Load_Small_Dimensions'
GO


Create Procedure BP.Load_Small_Dimensions
@MasterControlID int = -1

as

begin

/*
Trivial Dimensions
--gender (male, female)
--retention (retained,not retained)
--validuser (yes,no)
--bpcategory (high,pre,normal)
--improvementcategroy (better,same,worse)
*/
truncate table ref.bloodpressurerange

insert into ref.bloodpressurerange(SystolicFrom, SystolicTo, DiastolicFrom, DiastolicTo, BPCategoryName, BPCategoryKey, Notes)
values(-999,0,-999,0, 'Unknown', -1, 'Unknown or values dont map correctly'),
		(0,120,0,80,'Normal',0,'In the normal range (including low BP) - Both figures below max'),
		(120,140,80,90,'PreHyperTensive',1,'Either figure over minimum but not over maximum'),
		(140,999,90,999,'High',2,'Either figure is above the minimum level')

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MasterControlID, 2, getdate(), 0.001, 'RowCount', '<RowCount>4</RowCount><Message>Success</Message>', 'Repopulate Blood Pressure Reference Table')


truncate table dbo.DimGender

insert into dbo.DimGender(genderkey,gendercode, genderdescription)
values(-1,'Unknown','Unknown'),(0,'NA','NA'),(1,'Male','Male'),(2,'Female','Female')

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MasterControlID, 2, getdate(), 0.001, 'RowCount', '<RowCount>4</RowCount><Message>Success</Message>', 'Repopulate Gender dimension')



truncate table bp.DimRetention

insert into bp.DimRetention(RetentionKey,RetentionCode, RetentionDescription)
values(-1,'Unknown','Unknown'),(0,'Not Retained','Does not meet rules for retention'),(1,'Retained','Meets rules for retention')

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MasterControlID, 2, getdate(), 0.001, 'RowCount', '<RowCount>3</RowCount><Message>Success</Message>', 'Repopulate Retention dimension')


truncate table bp.DimValidUser

insert into bp.DimValidUser(ValidUserKey,ValidUserCode, ValidUserDescription)
values(-1,'Unknown','Unknown'),(0,'No','Does not meet valid user requirment'),(1,'Yes','Meets requirement for valid user')

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MasterControlID, 2, getdate(), 0.001, 'RowCount', '<RowCount>3</RowCount><Message>Success</Message>', 'Repopulate ValidUser dimension')


truncate table bp.DimBPCategory

insert into bp.DimBPCategory(BPCategoryKey,BPCategoryCode, BPCategoryDescription)
values(-1,'Unknown','Unknown'),(0,'Normal','In normal range <120/80'),(1,'PreHyperTensive','Between 120-140 or 80-90'),(2,'High','Over 140/90')

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MasterControlID, 2, getdate(), 0.001, 'RowCount', '<RowCount>4</RowCount><Message>Success</Message>', 'Repopulate Blood Pressure Category dimension')


truncate table bp.DimImprovementCategory

insert into bp.DimImprovementCategory(ImprovementCategoryKey,ImprovementCategoryCode, ImprovementCategoryDescription)
values(-1,'Unknown','Unknown'),(0,'Better','Measured value has improved'),(1,'Same','Measured value is same or similar'),(2,'Worse','Measured value has deteriorated')

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MasterControlID, 2, getdate(), 0.001, 'RowCount', '<RowCount>4</RowCount><Message>Success</Message>', 'Repopulate Blood Pressure Improvement dimension')

end