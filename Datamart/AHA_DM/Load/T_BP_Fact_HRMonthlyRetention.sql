use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Fact_HRMonthlyRetention' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Fact_HRMonthlyRetention'
	drop table BP.Fact_HRMonthlyRetention
end
else
	print 'New Table created - Fact_HRMonthlyRetention'
GO



create table BP.Fact_HRMonthlyRetention(
Fact_HRMonthlyRetentionKey bigint identity(1,1) 
	constraint PK_BP_Fact_HRMonthlyRetention PRIMARY KEY NONCLUSTERED,
HealthRecordKey int, 
UserHealthRecordID int, --bk just incase rekeying is required
--descriptor dimensions -these are derived off the primary fact so doesnt need the bk
ReportingMonthKey int,

--all dimensions if null then -1 (unknown)

--measures
FirstReadingInPeriod date,
LatestReadingInPeriod date,

Month1Count int,
Month2Count int,
Month3Count int,
Month4Count int,


--this is the "within the 4 month" perspective
RetainedUserFlag int,
 
--= case when count(*) > 7 then 1 else 0 end,  ---rule from first part of if statement on retention calculation sheet column B
validuserflag int,
--= case when count(*) = 1 then 0		when datediff(d,min(BPMeasuredDate),max(BPMeasuredDate)) > 7 then 1		else 0		end

--control
PopulationDateTime datetime
	constraint DF_BP_Fact_HRMonthlyRetention_PopulationDateTime default(getdate()),

ExtractControlID int


) on [PRIMARY]
Go

create index IDX_BP_Fact_HRMonthlyRetention_Population_Perf001
ON BP.Fact_HRMonthlyRetention(ReportingMonthKey,HealthRecordKey) include(RetainedUserFlag, validuserflag)
Go

