use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_DimAffiliate' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_DimAffiliate'
	drop procedure dbo.Load_DimAffiliate
end
else
	print 'New Procedure created - Load_DimAffiliate'
GO

Create procedure dbo.Load_DimAffiliate
@MasterControlID int

as
/*
exec dbo.Load_DimAffiliate -9
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load Affiliate'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


--scd type 1 == current values plus historic items left in table.
--truncate table DIM.AHAGeography

insert into [dbo].[DimAffiliate] ( AffiliateCode, AffiliateName, AffiliateAbbreviation, ExtractControlID)
select cast(e.AffiliateID as varchar(10)) ,e.[AffiliateName] ,e.[AffiliateAbbr] ,e.[ExtractControlID]
from [H360].[Extract_Affiliate] e
where e.foundmatchflag = 0


--logging here - log extract row count
set @TotalRowCount = isnull((select count(z.[Extract_AffiliateKey]) from H360.Extract_Affiliate z WHERE z.FOUNDMATCHFLAG = 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into dimension')

set @totalrowcount = 0
set @st2 = getdate()

update d
set d.[AffiliateName] = case when isnull(d.[AffiliateName] ,'') <> isnull(e.[AffiliateName] ,'') then isnull(e.[AffiliateName],'') else d.[AffiliateName] end, 
	d.[AffiliateAbbreviation] = case when isnull(d.[AffiliateAbbreviation] ,'') <> isnull(e.[AffiliateAbbr] ,'') then isnull(e.[AffiliateAbbr],'') else d.[AffiliateAbbreviation] end,  
	populateddatetime = getdate(), 
	extractcontrolid = @mastercontrolID
from 
	dbo.DimAffiliate d
	inner join [H360].[Extract_Affiliate] e on e.foundmatchflag = d.AffiliateKey
where isnull(e.FoundMatchFlag,0) <> 0 and e.datachangeflag <> 0

set @TotalRowCount = isnull((select count(z.[Extract_AffiliateKey]) from H360.[Extract_Affiliate] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag <> 0),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@st2, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'UpdatedRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Updated in dimension')

set @TotalRowCount = isnull((select count(z.[Extract_AffiliateKey]) from H360.[Extract_Affiliate] z WHERE isnull(z.FoundMatchFlag,0) <> 0 and z.datachangeflag = 0),0)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ExistingRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Existing Rows Ignored in dimension')

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessCompleted', '<RowCount>0</RowCount><Message>Success</Message>', 'Dimension Load Completed')


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end
--select * from dimaffiliate