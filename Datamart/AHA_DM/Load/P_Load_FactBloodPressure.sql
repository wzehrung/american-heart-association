use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Load_FactBloodPressure' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Load_FactBloodPressure'
	drop procedure BP.Load_FactBloodPressure
end
else
	print 'New Procedure created - Load_FactBloodPressure'
GO

Create procedure BP.Load_FactBloodPressure
@MasterControlID int

as
/*
exec BP.Load_FactBloodPressure -201
*/

begin

declare @startTime datetime = getdate(), @st2 datetime, @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Load Fact BloodPressure'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.[Extract_HealthRecordExtendedKey]) from [H360].[Extract_HealthRecordExtended] z WHERE FOUNDMATCHFLAG = 0),0)
--truncate table [BP].[FactBloodPressure] 

insert into [BP].[FactBloodPressure] ( BloodPressureID, UserHealthRecordID, HealthRecordKey, AffiliateKey, CampaignKey, 
	GeographyKey, EthnicityKey, GenderKey, BPMeasuredDate, RowCreatedDate, RowUpdatedDate, 
	BPMeasuredDateKey,[BPReportingMonthKey], 
	Systolic, Diastolic, BPCategoryKey, Pulse, 
	BPCommentKey, PopulatedDateTime, ExtractControlID )
SELECT [BloodPressureID]      ,[UserHealthRecordID]      ,[HealthRecordKey]      ,[AffiliateKey]      ,[CampaignKey]
      ,[GeographyKey]      ,[EthnicityKey]      ,[GenderKey]      ,[BPMeasuredDate]      ,[RowCreatedDate]      ,[RowUpdatedDate]
      ,[BPMeasuredDateKey]      ,[BPReportingMonthKey]
      ,[Systolic]      ,[Diastolic]      ,[BPCategoryKey]      ,[Pulse]
      ,[BPCommentKey]  ,[PopulatedDateTime]  ,[ExtractControlID]
FROM [HealthDataMart].[H360].[Staging_BloodPressure]
where ProcessingFlag = 0

--are we allowed to update this fact after it has been commited?
--what can change - the fact is BP values on a date for a person, everything else is inferred or lookup, pulse and comment are secondary information
--yes it can change - corrections etc

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'NewRows', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'New Rows Inserted into dimension')


end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end