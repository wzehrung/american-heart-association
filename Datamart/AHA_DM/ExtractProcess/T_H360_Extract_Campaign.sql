use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_Campaign' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Extract_Campaign'
	drop table H360.Extract_Campaign
end
else
	print 'New Table created - Extract_Campaign'
GO

create table H360.Extract_Campaign
(
	--surrogate key
	Extract_CampaignKey int identity(-1,1)
		constraint PK_H360_Extract_Campaign Primary Key NONCLUSTERED, 
	--main extract table field list
	CampaignID int not null, --bk
	CampaignCode varchar(40) null, 
	FromDate date,
	ToDate date,
	CampaignTitle varchar(200), --bus altkey
	--control columns 
	ExtractPopulatedDateTime datetime
		constraint DF_H360_Extract_Campaign_ExtractDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	FoundMatchFlag int
		constraint DF_H360_Extract_Campaign_MatchFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	DataChangeFlag int
		constraint DF_H360_Extract_Campaign_DataChanged default(0)
		--only valid if matchflag = 1
)

--index - create index on main business key; include extract control - if scd2 include from and to date in main index
create index IDX_H360_Extract_Campaign_MatchPerf001
ON H360.Extract_Campaign(CampaignID, CampaignTitle) INCLUDE(ExtractControlID) --scd1

--check business logic of dimension for NA and Unknown otherwise Unknown = -1, NA = 0

insert into H360.Extract_Campaign( CampaignID, CampaignCode, FromDate, ToDate, CampaignTitle ,ExtractControlID) 
-- -1 Extract Control ID is the default initial insert of unknown and test data; 0 = initial population data; > 1 live populations
values(-1,'Unknown','1900-01-01','9999-12-31','Unknown', -1),(0,'NA','1900-01-01','9999-12-31','NA', -1)
go