use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_HealthRecordExtendedInfo' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Extract_HealthRecordExtendedInfo'
	drop procedure H360.Extract_HealthRecordExtendedInfo
end
else
	print 'New Procedure created - Extract_HealthRecordExtendedInfo'
GO

Create procedure H360.Extract_HealthRecordExtendedInfo
@MasterControlID int

as
/*
exec H360.Extract_HealthRecordExtendedInfo -9 --  -1 is control for admin preload dimensions; 0 is admin update of preload. 1 is first run
*/

begin

declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Extract HealthRecord'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

truncate table h360.Extract_HealthRecordExtended

insert into H360.Extract_HealthRecordExtended( UserHealthRecordID, ExtendedProfileID, CurrentCampaignID, Ethnicity, ZipCode ,ExtractControlID) 
-- -1 Extract Control ID is the default initial insert of unknown and test data; 0 = initial population data; > 1 live populations
values (-1,-1,-1,'Unknown','Unknown', -1)

insert into h360.Extract_HealthRecordExtended(UserHealthRecordID, HRCreatedDate, HRUpdatedDate, HRLastActivityDate, CurrentCampaignID, IsMigrated, PolkaID, ExtendedProfileID, BloodType, CountryRegion, ZipCode, Gender, Ethnicity, MaritalStatus, YearOfBirth, ExtractControlID)
select 
	H.UserHealthRecordID, H.CreatedDate, H.UpdatedDate, H.LastActivityDate, h.CampaignID, h.IsMigrated, h.PolkaID, 
	e.ExtendedProfileID, e.BloodType, e.[Country/Region], e.ZipCode, e.Gender, e.Ethnicity, e.MaritalStatus, e.YearOfBirth, @MasterControlID
from [Heart360].[dbo].[HealthRecord] h
	left join [Heart360].[dbo].[ExtendedProfile] e on h.[UserHealthRecordID] = e.[UserHealthRecordID]


set @TotalRowCount = isnull((select count(z.Extract_HealthRecordExtendedKey) -1 from H360.Extract_HealthRecordExtended z),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Extracted from source')

end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end



end
