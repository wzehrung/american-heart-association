use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_AffiliateCampaignInfo' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Extract_AffiliateCampaignInfo'
	drop procedure H360.Extract_AffiliateCampaignInfo
end
else
	print 'New Procedure created - Extract_AffiliateCampaignInfo'
GO

Create procedure H360.Extract_AffiliateCampaignInfo
@MasterControlID int

as
/*
exec H360.Extract_AffiliateCampaignInfo -6 --  -1 is control for admin preload dimensions; 0 is admin update of preload. 1 is first run
*/

begin

declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Extract AffiliateCampaign'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


truncate table h360.Extract_AffiliateCampaign

insert into h360.extract_affiliatecampaign(AffiliateID, CampaignID, ExtractControlID)
values ( -1,-1, @mastercontrolid),( -1, 0, @mastercontrolid),( 0,-1, @mastercontrolid),( 0,0, @mastercontrolid)
--Unknown, Unknown; Unknown, NA (No campaign); NA (No Affiliate), Unknown; NA (No Affiliate), NA (No Campaign);

insert into h360.extract_affiliatecampaign(AffiliateID, CampaignID, ExtractControlID)
select
	c.affiliateid, c.campaignid, @mastercontrolid
from [Heart360].[dbo].[AffiliatesCampaigns] c 

--logging here - log extract row count
set @TotalRowCount = isnull((select count(*) from H360.extract_affiliatecampaign z),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Extracted from source')
/*
set @startTime = getdate()

update e
set e.AffiliateCampaignMatch = isnull(b.Bridge_AffiliateCampaignKey, -99)
from h360.extract_affiliatecampaign e
	left join [BP].[Bridge_AffiliateCampaign] b on e.affiliateID = b.affiliateid and e.campaignId = b.campaignid

set @TotalRowCount = isnull((select count(*) from H360.extract_affiliatecampaign z where AffiliateCampaignMatch <> -99),0)

set @endtime = getdate()

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'Matching', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Rows Matched to bridge table</Message>', 'Extract compared to Bridge')
*/
end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end