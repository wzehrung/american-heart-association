use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_BloodPressure' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Extract_BloodPressure'
	drop table H360.Extract_BloodPressure
end
else
	print 'New Table created - Extract_BloodPressure'
GO

create table H360.Extract_BloodPressure
(
	--surrogate key
	Extract_BloodPressureKey int identity(-1,1)
		constraint PK_H360_Extract_BloodPressure Primary Key NONCLUSTERED, 
	--main extract table field list
	--BP
	BloodPressureID int not null, --bk
	UserHealthRecordID int not null, --rif
	BPMeasuredDate datetime,
	RowCreatedDate datetime,
	RowUpdatedDate datetime,
	Systolic int,
	Diastolic int,
	Pulse int,
	Comments varchar(max), --exclude the lorm ipsum text on reading 367182 during staging

	--control columns
	PopulatedDateTime datetime
		constraint DF_H360_Extract_BloodPressure_ExtractDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	FoundMatchFlag int
		constraint DF_H360_Extract_BloodPressure_MatchFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	DataChangeFlag int
		constraint DF_H360_Extract_BloodPressure_DataChanged default(0)
		--only valid if matchflag = 1

)
--select len(comments), * from [dbo].[BloodPressure] order by len(comments) desc --6831 max length