use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_CampaignInfo' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Extract_CampaignInfo'
	drop procedure H360.Extract_CampaignInfo
end
else
	print 'New Procedure created - Extract_CampaignInfo'
GO

Create procedure H360.Extract_CampaignInfo
@MasterControlID int

as
/*
exec H360.Extract_CampaignInfo -6 --  -1 is control for admin preload dimensions; 0 is admin update of preload. 1 is first run
*/

begin

declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Extract Campaign'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


truncate table h360.Extract_Campaign

insert into h360.extract_campaign(CampaignID, CampaignCode, FromDate, ToDate, CampaignTitle, ExtractControlID)
select
c.campaignid, c.campaigncode, c.startdate, c.enddate, d.title, @mastercontrolid
from [Heart360].[dbo].[Campaign] c 
	left join [Heart360].dbo.campaigndetails d on c.campaignID = d.campaignid

--logging here - log extract row count
set @TotalRowCount = isnull((select count(z.Extract_CampaignKey) from H360.Extract_Campaign z),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Extracted from source')

end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end

end