use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_AffiliateCampaign' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Extract_AffiliateCampaign'
	drop table H360.Extract_AffiliateCampaign
end
else
	print 'New Table created - Extract_AffiliateCampaign'
GO

create table H360.Extract_AffiliateCampaign
(
	--surrogate key
	Extract_AffiliateCampaignKey int identity(-1,1)
		constraint PK_H360_Extract_AffiliateCampaign Primary Key NONCLUSTERED, 
	--main extract table field list
	AffiliateID int not null, 
	CampaignID int not null, --bk
	--control columns 
	ExtractPopulatedDateTime datetime
		constraint DF_H360_Extract_AC_ExtractDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	AffiliateMatchKey int
		constraint DF_H360_Extract_AC_Affiliate_MatchFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	CampaignMatchKey int
		constraint DF_H360_Extract_AC_Campaign_MatchFlag default(0),
	AffiliateCampaignMatch int
		constraint DF_H360_Extract_AC_AffiliateCampaign_Match default(0)
		--only valid if matchflag = 1
)


