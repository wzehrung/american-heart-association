USE [HealthDataMart]
GO

DROP TABLE H360.Extract_ZipRegionMapping
GO

CREATE TABLE H360.Extract_ZipRegionMapping(
ExtractRowID int identity(1,1),
	[ZIP_Code] [varchar](50) NULL,
	[ZIP_Name] [varchar](200) NULL,
	[State] [varchar](100) NULL,
	[Affiliate] [varchar](200) NULL,
	[TotalPopulation] [float] NULL,
	[CBSA_Code] [varchar](50) NULL,
	[CBSA_Name] [varchar](200) NULL,
	[CountyFIPS] varchar(50) NULL,
	[CountyName] [varchar](200) NULL,
--control columns
	ExtractPopulatedDateTime datetime
		constraint DF_H360_Extract_ZipRegionMapping_ExtractDT default(getdate()),
	ExtractControlID int,-- master wrapper controlID
	FoundMatchFlag int
		constraint DF_H360_Extract_ZipRegionMapping_MatchFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	DataChangeFlag int
		constraint DF_H360_Extract_ZipRegionMapping_DataChanged default(0)
		--only valid if matchflag = 1
) ON [PRIMARY]

GO


create index IDX_H360_Extract_ZipRegionMapping_MatchPerf01
on H360.Extract_ZipRegionMapping(zip_code, zip_name, state)


create index IDX_H360_Extract_ZipRegionMapping_MatchPerf02
on H360.Extract_ZipRegionMapping(zip_code, affiliate, countyname) 