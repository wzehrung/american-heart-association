/*
SIMPLE SCD1 Dimension

copy.
search and replace H360 with schema actual name
search and replace Extract_Ethnicity with dimension actual name
save

add field list to d

change the Null/unknown/NA insert row to suit columns and business rules
save

execute
*/
use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_Ethnicity' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Extract_Ethnicity'
	drop table H360.Extract_Ethnicity
end
else
	print 'New Table created - Extract_Ethnicity'
GO

create table H360.Extract_Ethnicity
(
	--surrogate key
	Extract_EthnicityKey int identity(-1,1)
		constraint PK_H360_Extract_Ethnicity Primary Key NONCLUSTERED, 
	--main extract table field list
	EthnicityCode varchar(20),
	EthnicityDescription varchar(80),
	LookupListRowID int,
	--control columns
	PopulatedDateTime datetime
		constraint DF_H360_Extract_Ethnicity_ExtractDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	FoundMatchFlag int
		constraint DF_H360_Extract_Ethnicity_MatchFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	DataChangeFlag int
		constraint DF_H360_Extract_Ethnicity_DataChanged default(0)
		--only valid if matchflag = 1
	
)

--index - create index on main business key; include extract control - if scd2 include from and to date in main index
create index IDX_H360_Extract_Ethnicity_MatchPerf001
ON H360.Extract_Ethnicity(EthnicityCode) INCLUDE(ExtractControlID) --scd1

--check business logic of dimension for NA and Unknown otherwise Unknown = -1, NA = 0

insert into H360.Extract_Ethnicity( EthnicityCode, EthnicityDescription, LookupListRowID ,ExtractControlID) 
-- -1 Extract Control ID is the default initial insert of unknown and test data; 0 = initial population data; > 1 live populations
values('Unknown','Unknown' ,-1,-1),('Not Reported','Not Reported',0, -1)
go



/*

*/