/*
SIMPLE SCD1 Dimension

copy.
search and replace H360 with schema actual name
search and replace Extract_Affiliate with dimension actual name
save

add field list to d

change the Null/unknown/NA insert row to suit columns and business rules
save

execute
*/
use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_Affiliate' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - Extract_Affiliate'
	drop table H360.Extract_Affiliate
end
else
	print 'New Table created - Extract_Affiliate'
GO

create table H360.Extract_Affiliate
(
	--surrogate key
	Extract_AffiliateKey int identity(-1,1)
		constraint PK_H360_Extract_Affiliate Primary Key NONCLUSTERED, 
	--main extract table field list
	AffiliateID int,
	AffiliateName varchar(50),
	AffiliateAbbr varchar(10),

	--control columns
	PopulatedDateTime datetime
		constraint DF_H360_Extract_Affiliate_ExtractDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	FoundMatchFlag int
		constraint DF_H360_Extract_Affiliate_MatchFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	DataChangeFlag int
		constraint DF_H360_Extract_Affiliate_DataChanged default(0)
		--only valid if matchflag = 1
)

--index - create index on main business key; include extract control - if scd2 include from and to date in main index
create index IDX_H360_Extract_Affiliate_MatchPerf01
ON H360.Extract_Affiliate(AffiliateID, AffiliateName) INCLUDE(ExtractControlID) --scd1

--check business logic of dimension for NA and Unknown otherwise Unknown = -1, NA = 0

insert into H360.Extract_Affiliate( AffiliateID,AffiliateName, AffiliateAbbr,ExtractControlID) 
-- -1 Extract Control ID is the default initial insert of unknown and test data; 0 = initial population data; > 1 live populations
values (-1,'Unknown','Unk', -1),(0,'NA','NA', -1)
go



/*

*/