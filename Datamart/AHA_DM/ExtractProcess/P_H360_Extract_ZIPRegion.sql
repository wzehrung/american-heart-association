use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Extract_ZIPRegion' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Extract_ZIPRegion'
	drop procedure H360.Extract_ZIPRegion
end
else
	print 'New Procedure created - Extract_ZIPRegion'
GO

Create procedure H360.Extract_ZIPRegion
@MasterControlID int

as
/*
exec H360.Extract_ZIPRegion -9 --  -1 is control for admin preload dimensions; 0 is admin update of preload. 1 is first run
*/

begin

declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Extract Geography'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int


truncate table H360.Extract_ZipRegionMapping

insert into H360.Extract_ZipRegionMapping(
--source data	
	[ZIP_Code],	[ZIP_Name],	[State],[Affiliate], [TotalPopulation], [CBSA_Code], [CBSA_Name], [CountyFIPS], [CountyName],
--control columns
	ExtractControlID
	)
select 
	[ZIP_Code],	[ZIP_Name],	[State],	[Affiliate],	[TotalPopulation],	[CBSA_Code],	[CBSA_Name],	[CountyFIPS],	[CountyName],
--control columns
	@MasterControlID
from [Heart360].[dbo].[ZipRegionMapping]

--incremental not possible as it does not have an updated/modified datetime in source.
set @TotalRowCount = isnull((select count(z.ExtractRowID) from H360.Extract_ZipRegionMapping z),0)

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount><Message>Success</Message>', 'Rows Extracted from source')

end
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + '(' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end


end