use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Param_Affiliate' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Param_Affiliate'
	drop procedure Rpt.Param_Affiliate
end
else
	print 'New Procedure created - Param_Affiliate'
GO

create procedure Rpt.Param_Affiliate
@IncludeUnknown int = 3


as
/*
purpose - to allow selection for reports of the affiliate key - including unknown or not assigned.


expected return would be a string built by ssrs for a multiple list or integer for a single selection item
e.g. "-1,0,1,2" or 2
for multi list use the list to table converter table function.
------------------------------
exec Rpt.Param_Affiliate 3

*/
begin


SELECT 
AffiliateKey, AffiliateName	
FROM [dbo].[DimAffiliate]
where (@IncludeUnknown&1 = 1 and AffiliateKey = -1)
	OR (@IncludeUnknown&2 = 2 and AffiliateKey = 0) 
	OR AffiliateKey > 0 --always return the actual affiliates
order by 2

end