use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'ReadingsSparkline' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - ReadingsSparkline'
	drop procedure Rpt.ReadingsSparkline
end
else
	print 'New Procedure created - ReadingsSparkline'
GO

create procedure Rpt.ReadingsSparkline
@ToYMKey int,
@History int = 12
--can also do this as a return list from a multibox

as
/*
Rpt.ReadingsSparkline 201303,12
Rpt.ReadingsSparkline 201512,12
*/
begin

declare @FromYMKey int, @ToDate date = null

set @ToDate = (select max(datevalue) from dimdate where yearmonthkey = @ToYMKey)
set @FromYMKey = (select top 1 yearmonthkey from dimdate where datevalue = dateadd(month, (-1 * @history), @Todate))

select
d.yearmonthkey, isnull(count(f.BloodPressureID),0) as NrReadings
from dbo.dimdate d 
	left outer join bp.factbloodpressure f on --f.bpmeasureddatekey = d.datekey 
		f.bpreportingmonthkey = d.yearmonthkey and d.DayOfMonthValue = 1
where d.yearmonthkey between @FromYMKey and @ToYMKey
group by d.yearmonthkey
order by d.yearmonthkey

end
/*

select
d.yearmonthkey, count(*) as NrReadings
from bp.factbloodpressure f
	inner join dbo.dimdate d on f.bpmeasureddatekey = d.datekey
where d.yearmonthkey between @FromYMKey and @ToYMKey
group by d.yearmonthkey

end

select
d.yearmonthkey, isnull(count(f.BloodPressureID),0) as NrReadings
from dbo.dimdate d 
	left outer join bp.factbloodpressure f on --f.bpmeasureddatekey = d.datekey 
		f.bpreportingmonthkey = d.yearmonthkey and d.DayOfMonthValue = 1
where d.yearmonthkey between @FromYMKey and @ToYMKey
group by d.yearmonthkey

*/