drop procedure rpt.HS_AllParticipants
go

create procedure rpt.HS_AllParticipants
@FromDate date,
@ToDate date,
@AffiliateCode varchar(500) = '*',
@CampaignCode varchar(5000) = '*'

as
/*
rpt.HS_AllParticipants '2014-07-01','2014-11-30' --1624,2971,2324,6919 vC --vB 1624,2971,2324,6919

rpt.HS_AllParticipants '2014-07-01','2014-11-30', '-1,0,1,2,3,4,5,6,7'
*/
begin

--declare 	@FromDate date = '2010-01-01',@ToDate date = '2014-01-01',	@AffiliateCode varchar(50) = '*',	@CampaignCode varchar(50) = '*'


select
	NormalBP = sum(case when bpcategorykey = 0 then nrpatients else 0 end), 
	PreHypertensive = sum(case when bpcategorykey = 1 then nrpatients else 0 end),
	HighBP = sum(case when bpcategorykey = 2 then nrpatients else 0 end),
	Total = sum(nrpatients)
from 
	(
	select
		BPCategoryKey, count(healthrecordkey) as NrPatients

	from (
		select f.healthrecordkey, f.bpmeasureddate as FirstReadingDate, f.bpcategorykey
		from (
			select p1.healthrecordkey, p1.bpmeasureddate, p1.BPCategoryKey,
				dense_rank () over (
					partition by p1.healthrecordkey
					order by p1.bpmeasureddate, p1.bloodpressurekey
				) as FirstPressureRank 
			from [BP].[FactBloodPressure] p1 with(nolock)
			where 	
				p1.bpmeasureddate between @FromDate and @ToDate
				and p1.bpcategorykey >= 0 --not invalid
				and (@Affiliatecode = '*' OR isnull(p1.affiliatekey,-1) in (select parameter from fnMultiValuedParameterAsTable(@AffiliateCode) ) )--affiliate code
				and (@Campaigncode = '*' OR isnull(p1.CampaignKey,-1) in (select parameter from fnMultiValuedParameterAsTable(@CampaignCode) ) )--campaign code
			) f --effectively distinct list on healthkey record for first record by date and id (covers multiple submissions on a date)
		WHERE  f.FirstPressureRank = 1

		) b
	
	group by BPCategoryKey 
	) c


end

/*

When I run this on QA for our test range (July-October 2014) I get:
�	Normal: 1638 (+2)  [1636] ((+1
�	Pre-Hyper: 2307 (-1) [2308] ((-2
�	High: 1423 (+2) [1421] ((+1
�	Total: 5366 (+1) [5365] ((=

rpt.HS_AllParticipants '2014-07-01','2014-11-30', '-1,0,1,2,3,4,5,6,7','*'  
--'2014-07-01','2014-11-30' --1637,2306,1422,5365
--'2014-07-01','2014-10-31' --1433,1980,1191,4604


*/

/* cut 2 from Andrew.
	(
	select
		BPCategoryKey, count(healthrecordkey) as NrPatients
	
	from (
		select a.healthrecordkey, a.FirstReading, f.bpcategorykey
		from (
			select f.healthrecordkey, min(f.bpmeasureddate) as FirstReading
			from [BP].[FactBloodPressure] f with(nolock)
			where 
				f.bpmeasureddate between @FromDate and @ToDate and 
				f.bpcategorykey >= 0 --not invalid
			group by f.healthrecordkey
		) a
		inner join (
			select p1.healthrecordkey, p1.bpmeasureddate, p1.BPCategoryKey,
				dense_rank () over (
					partition by p1.healthrecordkey, p1.bpmeasureddate 
					order by p1.bpmeasureddate, p1.bloodpressurekey
				) as FirstPressureRank 
			from [BP].[FactBloodPressure] p1 with(nolock)
		) f ON 
			a.healthrecordkey = f.healthrecordkey and 
			a.firstreading = f.bpmeasureddate and 
			f.FirstPressureRank = 1
	) b
	
	group by BPCategoryKey
	) c


=======================
original code

select
	NormalBP = sum(case when bpcategorykey = 0 then nrpatients else 0 end), 
	PreHypertensive = sum(case when bpcategorykey = 1 then nrpatients else 0 end),
	HighBP = sum(case when bpcategorykey = 2 then nrpatients else 0 end),
	Total = sum(nrpatients)
from
(
select
	c.BPCategoryKey, c.BPCategoryCode, count(healthrecordkey) as NrPatients
from
(--declare @FromDate date = '2012-03-01', @ToDate date = '2013-03-01'
select healthrecordkey, max([BPCategoryKey]) as UserBPCategory-- count(distinct [HealthRecordKey] ) as CountRow
from [BP].[FactBloodPressure] f 
where f.bpmeasureddate between @FromDate and @ToDate
		and f.bpcategorykey >= 0 --not invalid
group by healthrecordkey) a 
inner join [BP].[DimBPCategory] c on a.UserBPCategory = c.BPCategoryKey
group by c.BPCategoryKey, c.BPCategoryCode
) b
*/