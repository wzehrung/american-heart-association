use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'UsersSparkline' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - UsersSparkline'
	drop procedure Rpt.UsersSparkline
end
else
	print 'New Procedure created - UsersSparkline'
GO

create procedure Rpt.UsersSparkline
@ToYMKey int,
@History int = 12
--can also do this as a return list from a multibox

as
/*
Rpt.UsersSparkline 201308,12

Rpt.UsersSparkline 201512,12

Rpt.UsersSparkline 201308,12
*/
begin

declare @FromYMKey int, @ToDate date = null

set @ToDate = (select max(datevalue) from dimdate where yearmonthkey = @ToYMKey)
set @FromYMKey = (select top 1 yearmonthkey from dimdate where datevalue = dateadd(month, (-1 * @history), @Todate))


select
d.yearmonthkey, isnull(count(distinct f.healthrecordkey),0) as NrUsers
from dbo.dimdate d 
	left outer join bp.factbloodpressure f on f.bpmeasureddatekey = d.datekey --f.bpreportingmonthkey = d.yearmonthkey and d.dayofmonthvalue = 1
where d.yearmonthkey between @FromYMKey and @ToYMKey
group by d.yearmonthkey
order by d.yearmonthkey

end
/*

select
d.yearmonthkey, isnull(count(distinct f.healthrecordkey),0) as NrUsers
from dbo.dimdate d 
	left outer join bp.factbloodpressure f on f.bpmeasureddatekey = d.datekey --f.bmreportingmonthkey = d.yearmonthkey 
where d.yearmonthkey between @FromYMKey and @ToYMKey
group by d.yearmonthkey


select
d.yearmonthkey, count(distinct healthrecordkey) as NrUsers
from bp.factbloodpressure f
	inner join dbo.dimdate d on f.bpmeasureddatekey = d.datekey
where d.yearmonthkey between @FromYMKey and @ToYMKey
group by d.yearmonthkey

*/