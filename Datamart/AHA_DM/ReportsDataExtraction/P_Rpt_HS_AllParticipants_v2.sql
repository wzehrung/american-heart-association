use [HealthDataMart]
go

drop procedure rpt.HS_AllParticipants
go

create procedure rpt.HS_AllParticipants
@FromDate date,
@ToDate date,
@AffiliateCode varchar(50) = '*',
@CampaignCode varchar(50) = '*'

as
/*
rpt.HS_AllParticipants '2012-03-01','2013-03-01'
*/
begin

select
	NormalBP = sum(case when bpcategorykey = 0 then nrpatients else 0 end), 
	PreHypertensive = sum(case when bpcategorykey = 1 then nrpatients else 0 end),
	HighBP = sum(case when bpcategorykey = 2 then nrpatients else 0 end),
	Total = sum(nrpatients)
from (
	select
		BPCategoryKey, count(healthrecordkey) as NrPatients
	from (
		select a.healthrecordkey, a.FirstReading, f.bpcategorykey
		from (
			select f.healthrecordkey, min(f.bpmeasureddate) as FirstReading
			from [BP].[FactBloodPressure] f with(nolock)
			where 
				f.bpmeasureddate between @FromDate and @ToDate and 
				f.bpcategorykey >= 0 --not invalid
			group by f.healthrecordkey
		) a
		inner join (
			select p1.healthrecordkey, p1.bpmeasureddate, p1.BPCategoryKey,
				dense_rank () over (
					partition by p1.healthrecordkey, p1.bpmeasureddate 
					order by p1.bpmeasureddate, p1.bloodpressurekey
				) as FirstPressureRank 
			from [BP].[FactBloodPressure] p1 with(nolock)
		) f ON 
			a.healthrecordkey = f.healthrecordkey and 
			a.firstreading = f.bpmeasureddate and 
			f.FirstPressureRank = 1
	) b
	group by BPCategoryKey
) c

/* original code
select
	NormalBP = sum(case when bpcategorykey = 0 then nrpatients else 0 end), 
	PreHypertensive = sum(case when bpcategorykey = 1 then nrpatients else 0 end),
	HighBP = sum(case when bpcategorykey = 2 then nrpatients else 0 end),
	Total = sum(nrpatients)
from
(
select
	c.BPCategoryKey, c.BPCategoryCode, count(healthrecordkey) as NrPatients
from
(--declare @FromDate date = '2012-03-01', @ToDate date = '2013-03-01'
select healthrecordkey, max([BPCategoryKey]) as UserBPCategory-- count(distinct [HealthRecordKey] ) as CountRow
from [BP].[FactBloodPressure] f 
where f.bpmeasureddate between @FromDate and @ToDate
		and f.bpcategorykey >= 0 --not invalid
group by healthrecordkey) a 
inner join [BP].[DimBPCategory] c on a.UserBPCategory = c.BPCategoryKey
group by c.BPCategoryKey, c.BPCategoryCode
) b
*/
end