/*
rpt.HS_AllParticipants '2012-03-01','2013-03-01'
*/

declare 
	@FromDate date = '2010-01-01',
	@ToDate date = '2014-01-01',
	@AffiliateCode varchar(50) = '*',
	@CampaignCode varchar(50) = '*'


select
	--InvalidBP = sum(case when bpcategorykey < 0 then nrpatients else 0 end), 
	NormalBP = sum(case when bpcategorykey = 0 then nrpatients else 0 end), 
	PreHypertensive = sum(case when bpcategorykey = 1 then nrpatients else 0 end),
	HighBP = sum(case when bpcategorykey = 2 then nrpatients else 0 end),
	Total = sum(nrpatients)
from 
	(
	select
		BPCategoryKey, count(healthrecordkey) as NrPatients

	from (
		select f.healthrecordkey, f.bpmeasureddate as FirstReadingDate, f.bpcategorykey
		from (
			select p1.healthrecordkey, p1.bpmeasureddate, p1.BPCategoryKey,
				dense_rank () over (
					partition by p1.healthrecordkey
					order by p1.bpmeasureddate, p1.bloodpressurekey
				) as FirstPressureRank 
			from [BP].[FactBloodPressure] p1 with(nolock)
			where 	
				p1.bpmeasureddate between @FromDate and @ToDate
				and p1.bpcategorykey >= 0 --not invalid
			) f --effectively distinct list on healthkey record for first record by date and id (covers multiple submissions on a date)
		WHERE  f.FirstPressureRank = 1

		) b
	
	group by BPCategoryKey 
	) c


/*

1624	2971	2324	6919


*/

/*v2 with group and ...*/
	select
		bpreportingmonthkey, BPCategoryKey, count(healthrecordkey) as NrPatients

	from (
		select f.healthrecordkey, f.bpmeasureddate as FirstReadingDate, f.bpcategorykey, f.BPReportingMonthKey
		from (
			select 
				p1.healthrecordkey, p1.userhealthrecordid, p1.bpmeasureddate, p1.BPCategoryKey, 
				p1.bpreportingmonthkey, p1.systolic, p1.diastolic, p1.[RowCreatedDate],
				dense_rank () over (
					partition by p1.healthrecordkey
					order by p1.bpmeasureddate, p1.bloodpressurekey
				) as FirstPressureRank 
			from [BP].[FactBloodPressure] p1 with(nolock)
			where 	
				p1.bpmeasureddate between '2014-07-01' and '2014-11-30'
				and p1.bpcategorykey >= 0 --not invalid
				and p1.userhealthrecordid = 83937
				--affiliate code
				--campaign code
			) f --effectively distinct list on healthkey record for first record by date and id (covers multiple submissions on a date)
		WHERE  f.FirstPressureRank = 1

		) b
	
	group by bpreportingmonthkey, BPCategoryKey with cube
	order by isnull(BPReportingMonthKey,999999), isnull(BPCategoryKey,99)



	select * from [BP].[FactBloodPressure] where userhealthrecordid = 83951 and BPreportingmonthKey = 201407

select * from [Heart360].[dbo].[BloodPressure] where UserHealthRecordID = 83951 and DateMeasured between '2014-07-01' and '2014-07-31 23:59:59'
--[BloodPressureID] in (579407,579408)