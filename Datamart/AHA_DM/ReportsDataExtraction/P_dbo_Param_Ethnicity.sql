use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Param_Ethnicity' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Param_Ethnicity'
	drop procedure Rpt.Param_Ethnicity
end
else
	print 'New Procedure created - Param_Ethnicity'
GO

create procedure Rpt.Param_Ethnicity
@IncludeUnknown int = 1


as
/*
purpose - to allow selection for reports of the ethnicity key.

this process also allows supression of the unknown and not reported value in the master list.

expected return would be a string built by ssrs for a multiple list or integer for a single selection item
e.g. "-1,0,1,2" or 2
for multi list use the list to table converter table function.
------------------------------
exec Rpt.Param_Ethnicity 1
exec Rpt.Param_Ethnicity 0

*/
begin


SELECT 
	[EthnicityKey]
      
    ,[EthnicityDescription]
      
FROM [HealthDataMart].[dbo].[DimEthnicity]
where @IncludeUnknown = 1 OR (@IncludeUnknown = 0 and ethnicitykey > 0)
order by EthnicityDescription

end