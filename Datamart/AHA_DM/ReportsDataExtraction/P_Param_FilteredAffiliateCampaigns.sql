use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Param_FilteredAffiliateCampaigns' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Param_FilteredAffiliateCampaigns'
	drop procedure Rpt.Param_FilteredAffiliateCampaigns
end
else
	print 'New Procedure created - Param_FilteredAffiliateCampaigns'
GO

create procedure Rpt.Param_FilteredAffiliateCampaigns
@AffiliateList varchar(500) = '*',
@IncludeUnknown int = 3
-- in this case the include unknown applies to the campaign keys

as
/*

	exec Rpt.Param_FilteredAffiliateCampaigns '-1,0,1,2,3,4,5,6,7',3

*/

begin

	SELECT 
		distinct c.campaignkey, c.campaigntitle
		--b.*, c.*
	FROM dbo.DimCampaign c
	where
		((@IncludeUnknown&1 = 1 OR @IncludeUnknown&2 = 2 ) 
		and (c.CampaignKey in (-1,0)
		 		OR c.CampaignKey not in (select campaignkey from BP.Bridge_AffiliateCampaign)) ) --get the unknowns or unassociated
		--now get the associated
		OR 
		c.campaignkey in (select b.campaignkey from BP.Bridge_AffiliateCampaign b
								where (@AffiliateList = '*' OR
										b.affiliateKey in (select Parameter from dbo.fnMultiValuedParameterAsTable(@AffiliateList))
								) )

		--always return the actual affiliates
	order by c.campaigntitle

end