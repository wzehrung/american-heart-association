drop procedure Rpt.Monthly_UserFirstReadingSparkline
go

create procedure Rpt.Monthly_UserFirstReadingSparkline
@ToDate int,-- date,
@history int = 12

as
/*
Rpt.Monthly_UserFirstReadingSparkline 201302, 36
*/
begin

declare @FromYMKey int, @ToYMKey int = null, @EndDate date

set @ToYMKey = @ToDate 
set @EndDate = (select max(datevalue) from dimdate where yearmonthkey = @ToDate)
set @FromYMKey = (select top 1 yearmonthkey from dimdate where datevalue = dateadd(month, (-1 * @history), @EndDate))


select
d.yearmonthkey, isnull(count(distinct f.healthrecordkey),0) as NrUsers
from dbo.dimdate d 
	left outer join bp.staging_HRTotalHistory f on f.firstreadingymkey = d.yearmonthkey 
where d.yearmonthkey between @FromYMKey and @ToYMKey and d.dayofmonthvalue = 1
group by d.yearmonthkey
order by d.yearmonthkey

end