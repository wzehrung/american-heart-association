use [HealthDataMart]
go

drop procedure rpt.HS_ValidParticipants
go

create procedure rpt.HS_ValidParticipants
@FromDate Date,
@ToDate Date,
@AffiliateCode varchar(50) = '*',
@CampaignCode varchar(50) = '*'

as
/*
exec rpt.HS_AllParticipants 201302, 11

exec rpt.HS_ValidParticipants '2014-07-01','2014-11-30','*','*'


*/
begin

;with bpcoredata as (
select a.* , --f.*, l.*
	f.systolic as FirstSystolic, l.systolic as LatestSystolic, systolicImprovement = f.systolic - l.systolic, SysImpPct = cast(cast((f.systolic - l.systolic) as decimal(16,6))/cast(f.systolic as decimal(16,6)) as decimal(16,6)),
	f.diastolic as FirstDiastolic, l.diastolic as LatestDiastolic, diastolicImprovement = f.diastolic - l.diastolic, DiaImpPct = cast((f.diastolic - l.diastolic)/cast(f.diastolic as decimal(16,6)) as decimal(16,6)),
	f.bpcategorykey, --l.bpcategorykey, 
	case when l.bpcategorykey in (0,1) and f.bpcategorykey = 2 then 1 else 0 end as ControlledImprovement 
from
	( -- group with valid data
		--declare @Fromdate date = '2012-03-01', @Todate date = '2013-03-01', @AffiliateCode varchar(20) = '*', @CampaignCode varchar(20) = '*'
		select
			healthrecordkey, 
			count(*) as NrReadings,
			min(bpmeasureddate) as FirstReading,
			max(bpmeasureddate) as LatestReading		 
		from [BP].[FactBloodPressure] f with(nolock)
		where bpmeasureddate between @FromDate and @todate
			and (@affiliatecode = '*' OR isnull(f.affiliatekey,-1) in (select parameter from dbo.fnmultivaluedparameterastable(@Affiliatecode) ))--like replace(@Affiliatecode,'*','%') )
			and (@campaigncode = '*' OR isnull(f.campaignkey,-1) in (select parameter from dbo.fnmultivaluedparameterastable(@campaigncode) ) ) --like replace(@CampaignCode,'*','%') )
		group by healthrecordkey
		having count(*) > 1 and datediff(d,min(bpmeasureddate),max(bpmeasureddate)) > 7
	) a
	inner join (select p1.healthrecordkey, --first reading data
					p1.bpmeasureddate,
					p1.systolic, p1.diastolic, p1.BPCategoryKey,
					dense_rank () over(partition by p1.healthrecordkey, p1.bpmeasureddate order by p1.bpmeasureddate, p1.bloodpressurekey) as FirstPressureRank 
			from [BP].[FactBloodPressure] p1 with(nolock)
	) f ON a.healthrecordkey = f.healthrecordkey and a.firstreading = f.bpmeasureddate and f.FirstPressureRank = 1
	inner join (select p2.healthrecordkey, --latest reading data 
					p2.bpmeasureddate,
					p2.systolic, p2.diastolic, p2.BPCategoryKey,
					dense_rank () over(partition by p2.healthrecordkey, p2.bpmeasureddate order by p2.bpmeasureddate, p2.bloodpressurekey desc) as LatestPressureRank 
			from [BP].[FactBloodPressure] p2 with(nolock)
	) l ON a.healthrecordkey = L.healthrecordkey and a.LatestReading = l.bpmeasureddate and l.LatestPressureRank = 1

--order by a.firstreading, a.healthrecordkey
)
select 
	count(distinct HealthRecordKey) as ValidUserCount,
	sum(case when systolicimprovement >0 then 1 else 0 end) as ImprovedSystolic,
	SysImprovmentAmountPct = avg(case when systolicimprovement >0 then SysImpPct else null end), 
	sum(case when diastolicimprovement > 0 then 1 else 0 end) as ImprovedDiastolic, 
	DiaImprovementAmountPct = avg(case when diastolicimprovement > 0 then DiaImpPct else null end),

	sum(case when systolicimprovement >0 OR diastolicimprovement > 0 then 1 else 0 end) as TotalImprovedUserCount, 
	--
	sum(case when bpcategorykey = 2 and (systolicimprovement >0 OR diastolicimprovement >0) then 1 else 0 end) as HighPressureCount,
	avg(cast(case when bpcategorykey = 2 and systolicimprovement >0 then systolicImprovement else null end as decimal(16,6))) as HighSystolicImprovemntAmt, 
	avg(case when bpcategorykey = 2 and systolicimprovement >0 then SysImpPct else null end) as HighSystolicImprovementPct,
	avg(cast(case when bpcategorykey = 2 and diastolicimprovement >0 then diastolicimprovement else null end as decimal(16,6))) as HighDiastolicImprovementAmt, 
	avg(case when bpcategorykey = 2 and diastolicimprovement >0 then DiaImpPct else null end) as HighDIastolicImprovementPct,
	--
	sum(case when bpcategorykey = 1 and (systolicimprovement >0 OR diastolicimprovement >0) then 1 else 0 end) as PreHTSystolicCount,
	avg(cast(case when bpcategorykey = 1 and systolicimprovement >0 then systolicImprovement else null end as decimal(16,6))) as PreHTSystolicImprovemntAmt, 
	avg(case when bpcategorykey = 1 and systolicimprovement >0 then SysImpPct else null end) as PreHTSystolicImprovementPct,
	avg(cast(case when bpcategorykey = 1 and diastolicimprovement >0 then diastolicimprovement else null end as decimal(16,6))) as PreHTDiastolicImprovementAmt, 
	avg(case when bpcategorykey = 1 and diastolicimprovement >0 then DiaImpPct else null end) as PreHTDIastolicImprovementPct,
	--
	sum(case when bpcategorykey = 0 and (systolicimprovement >0 OR diastolicimprovement >0) then 1 else 0 end) as NormalSystolicCount,
	avg(cast(case when bpcategorykey = 0 and systolicimprovement >0 then systolicImprovement else null end as decimal(16,6))) as NormalSystolicImprovemntAmt, 
	avg(case when bpcategorykey = 0 and systolicimprovement >0 then SysImpPct else null end) as NormalSystolicImprovementPct,
	avg(cast(case when bpcategorykey = 0 and diastolicimprovement >0 then diastolicimprovement else null end as decimal(16,6))) as NormalDiastolicImprovementAmt, 
	avg(case when bpcategorykey = 0 and diastolicimprovement >0 then DiaImpPct else null end) as NormalDIastolicImprovementPct,

	ImprovedFromUncontrolled = sum(ControlledImprovement)
from bpcoredata b
--order by b.firstreading, b.healthrecordkey






end

--select * from bp.factbloodpressure where healthrecordkey = 13482 and bpmeasureddate = '2012-03-01 06:30:00.000' --'2013-01-24 07:00:00.000' --