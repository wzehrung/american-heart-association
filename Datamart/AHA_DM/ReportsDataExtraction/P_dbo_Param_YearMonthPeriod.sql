use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Param_YearMonthPeriod' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Param_YearMonthPeriod'
	drop procedure Rpt.Param_YearMonthPeriod
end
else
	print 'New Procedure created - Param_YearMonthPeriod'
GO

create procedure Rpt.Param_YearMonthPeriod
@ToDate date = null,
@History int = 12

as
/*

the purpose of this procedure is to provide reports with a year and month selector for a range of dates from a historic point to a specified date
date handling would be on the report data procedure using the key provided.
A setting of zero gives only the month selected in Todate
	all others give the number of months + current so 12 + 1, 
	to get only 12 months use a param of 11 (11+1 = 12)
==========================
example usage
--------------------------
exec Rpt.param_yearmonthperiod '2015-10-03',0
exec Rpt.param_yearmonthperiod '2015-11-22',8
exec Rpt.param_yearmonthperiod '2015-12-07',11

*/
begin

select yearmonthkey, yearmonthdescription
from dimdate
where datevalue between dateadd(month, -1 * isnull(@history,12), isnull(@Todate, getdate())) and isnull(@Todate, getdate())
group by yearmonthkey, yearmonthdescription
order by yearmonthkey

end