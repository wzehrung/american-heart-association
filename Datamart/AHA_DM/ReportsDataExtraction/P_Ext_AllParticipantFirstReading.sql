drop procedure rpt.Ext_AllParticipants
go

create procedure rpt.Ext_AllParticipants
@FromDate date,
@ToDate date,
@AffiliateCode varchar(50) = '*',
@CampaignCode varchar(50) = '*',
@CategoryFilter int = -999

/*

This procedure is to extract the valid first reading records for a H360 participant
=====================
exec rpt.Ext_AllParticipants '2014-07-01','2014-08-31 23:59'
*/

as

begin

select f.healthrecordkey, f.UserHealthRecordID, f.bpmeasureddate as FirstReadingDate, f.bpcategorykey, f.systolic, f.diastolic,
case when bpcategorykey = 0 then 'Normal Reading'
	when BPCategoryKey = 1 and systolic < 120 then 'Diastolic 80 or over'
	when BPCategoryKey = 1 and diastolic < 80 then 'Systolic 120 or over'
	when BPCategoryKey = 1 then 'Both over PreHyp threshold'
	when BPCategoryKey = 2 and systolic < 140 then 'Diastolic 90 or over'
	when BPCategoryKey = 2 and diastolic < 90 then 'Systolic 140 or over'
	when BPCategoryKey = 2 then 'Both over High BP threshold'
	else 'Not sure how we got here'
	end AS RuleAppliedDesc
from (
			select p1.healthrecordkey, p1.userhealthrecordid, p1.bpmeasureddate, p1.BPCategoryKey, p1.systolic, p1.diastolic,
				dense_rank () over (
					partition by p1.healthrecordkey
					order by p1.bpmeasureddate, p1.bloodpressurekey
				) as FirstPressureRank 
			from [BP].[FactBloodPressure] p1 with(nolock)
			where 	
				p1.bpmeasureddate between @FromDate and @ToDate
				and p1.bpcategorykey >= 0 --not invalid
			) f --effectively distinct list on healthkey record for first record by date and id (covers multiple submissions on a date)
	

WHERE  f.FirstPressureRank = 1 and (@categoryfilter = -999 or bpcategorykey = @CategoryFilter)
--alternate code for sql purists  bpcategorykey = isnull(nullif(@CategoryFilter,-999),bpcategoryKey)
	--put in additional filters here for affiliate and campaign
order by UserHealthRecordID

end