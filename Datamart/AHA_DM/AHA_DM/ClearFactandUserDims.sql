use [HealthDataMart]
GO


--dimensions
truncate table [BP].[DimBPReadingComments]
truncate table [BP].[DimHealthRecord]
truncate table [dbo].[DimAffiliate]
truncate table [dbo].[DimCampaign]
truncate table [dbo].[DimGeography]


--Facts
truncate table [BP].[Bridge_AffiliateCampaign]
truncate table [BP].[Fact_HRMonthlyRetention]
truncate table [BP].[Fact_HRTotalHistory]
truncate table [BP].[FactBloodPressure]




/* SPECIFIC FOR AFFILIATE AND CAMPAIGN */
--contributing bridges and dimensions
truncate table [BP].[Bridge_AffiliateCampaign]
truncate table [dbo].[DimAffiliate]
truncate table [dbo].[DimCampaign]

exec H360.Extract_AffiliateInfo -899
exec H360.Extract_CampaignInfo -899
exec H360.Transform_DimAffiliate -899
exec H360.Transform_Campaign -899
exec dbo.Load_DimAffiliate -899
exec dbo.Load_DimCampaign -899

exec h360.Extract_HealthRecordExtendedInfo -899
exec h360.Transform_HealthRecord -899
exec dbo.Load_HealthRecord -899


exec H360.Extract_AffiliateCampaignInfo -899
exec H360.Transform_AffiliateCampaign -899
exec dbo.Load_BridgeAffiliateCampaign -899

--health record key needs to be reset for current campaign
update bp.dimhealthrecord set currentcampaignid = 0 where currentcampaignID is null

update h
set  [CurrentCampaign_SFKey] = isnull( c.[CampaignKey], -1)
from bp.dimhealthrecord h
	left outer join  [dbo].[DimCampaign] c on h.currentcampaignid = c.campaignid

--the fact is adversley affected and doesnt have the business keys for affiliate or campaign


truncate table bp.factbloodpressure

exec H360.Extract_BloodPressureInfo -899
exec H360.Transform_BloodPressureInfo -899
exec BP.Load_FactBloodPressure -899


