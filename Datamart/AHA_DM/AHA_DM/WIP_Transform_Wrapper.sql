use [HealthDataMart]
go

if exists(select * from sysobjects where name = 'Transform_<DimFact>' and xtype = 'P')
begin
	print 'Procedure exists - dropping for recreate - Transform_<DimFact>'
	drop procedure H360.Transform_<DimFact>
end
else
	print 'New Procedure created - Transform_<DimFact>'
GO
--still using extract centric name as this is a match and change flag update process
--target is an scd1 --business key is country/zipcode - no country --> USA

create procedure H360.Transform_<DimFact>
@MasterControlID int

as
/*

exec H360.Transform_<DimFact> -2

*/

begin


declare @startTime datetime = getdate(), @EndTime datetime, @ElapsedSeconds decimal(16,6), @ProcessID int, @ProcessName varchar(50) = 'Transform <DimFact>'

--is the ETL process enabled in the master control
set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName and isEnabled = 1),-1)
print @ProcessID

if @ProcessID <> -1
begin
/*
This is an scd1 target dimension
*/
declare  @RowCountUpdated int, @TotalRowCount int

set @TotalRowCount = isnull((select count(z.Extract_<DimFact>Key) from H360.extract_zipregionmapping z),0)

--update not found + table aliases : e = extract, d = dimension
update e
set e.foundmatchflag = isnull((select d.<dimfact>key from dbo.<DimFact> d where g.<businesskey> = e.<businesskey> ),0) 
from [H360].[Extract_ZipRegionMapping] e


--update data differences
update e
set e.datachangeflag = 
	case
		when isnull(d.<datafield>,'') <>  isnull(e.<datafield>,'') then 1
		else 0 
	end
from [H360].[Extract_<dimfact>] e
	inner join dbo.<dimfact> g on e.foundmatchflag = d.<dimfact>key
where isnull(e.FoundMatchFlag,0) <> 0

set @endtime = getdate()
set @ElapsedSeconds = datediff(s,@starttime, @endtime)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'RowCount', '<RowCount>' + cast(@TotalRowCount as varchar(10)) + '</RowCount>', 'All rows Found and Match Flags Updated')
--output(@ThisProcessID = processRunLogID) 

end --end of enabled process control
else
begin

set @ProcessID = isnull((select ProcessDefinitionID from ETLC.ProcessDefinition where ProcessType in ('Any','SP') and ProcessName = @ProcessName),-99)
declare @message varchar(50)
set @message = case when @ProcessID = -99 then 'Process name not found' else 'Process Disabled' end + ' (' + @ProcessName + ')'

set @endtime = getdate()
set @ElapsedSeconds = cast(datediff(ms,@starttime, @endtime)/1000.000000 as decimal(16,6))

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@MastercontrolID, @ProcessID, @starttime, @ElapsedSeconds, 'ProcessBypassed', '<RowCount>0</RowCount><Message>' + @Message + '</Message>', 'This process was bypassed')

end
end

