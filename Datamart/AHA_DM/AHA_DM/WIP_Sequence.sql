Sequence of work.

Control tables
ExtractControl - this is a run date and time with an sequential identifier

EntityLoad - this is for incremental control - has the last successful run
ETLProcess - this has a list of the processes and what their current state is (active/disabled), what level of processing/debug history to log.
 
ProcessingHistory - this has a history of the ETL Runs at package level
DebugHistory - this has detailed item history and log of sub processes.

Clean data (DQS may be a better alternative for some) 
	columns: process, source entity, from value (lookup), to value(lookup match) - it is limited to one field at a time - exact values - no ranges


Try to identify in this order
	Reference Tables first.
	Then SCD type 1
	Then SCD type 2
	Then the facts. (bridges,snapshot,normal facts, timescross join facts)

Check facts for embedded dimension elements.
Check facts for qualitiative measures (descriptives rather than numerics)
	Treat embedded dimensions / qualitative measures as scd1 (no valid date range)







Reference tables.
Create Table for Source In Ref schema, Dimension in appropriate schema (DM or shared Dim for conformed)

If direct data from a spreadsheet - run a one time import.
If a small reference table in a spreadsheet, document or IP (in someones head) - code an insert statement
If it is a multipurpose excel/doc table - ie 1 source table becomes multiple dimensions - treat it as Ref to extract and then multiple scd1 dimensions after for load


SCD1 dimensions.
Create extract table - to be copy of source unless transfer will overwhelm system.
Check source to see if incremental load can occur.
If it is a multipurpose sql table - ie 1 source table becomes multiple dimensions - treat it as multiple scd1 dimensions after extract


Add control fields
	xyzRowID - arbitrary import row id counter
	extract date time
	extract control id
	match flag
	change flag (depends on business rule for each dimension)

Create extract raw data process
	if incremental then extract from last incremental success date
	Best Practice - do this in SSIS so that connections can be managed dynamically.

Create Clean/Transform process
	may be as simple as lookup and see if there is a match or if data has changed
	May be as complicated as do a data clean on entity data matching a value and replaced by a lookup then match and change

create target dimension
	load data - insert new, update changes
	
log to stats in control tables


SCD2 dimensions
Similar to Scd1 except that scd2 will have the following additional controls

 scd2_isCurrent - shows at a glance the current value of a business key dimension.
 from date (initial row may be 1900-01-01
 to date - as the names express this is the range of time that the dimension value is valid from and to 
	current row may be 9999-12-31
Normal practice is that no update of data (that is a different level of scd complexity)


Facts
Bridges - normal many to many break rules - but keep as small as possible (key1, key2, date, executionrun, businesskey1, businesskey2 - pk on key1&2)
Snapshot facts - keep to specific reporting requirements

Normalise down to star, keep all business keys for dimensions in play if possible so that the fact can be repopulated with surrogate keys.

try to not have scd2 type facts or updateable facts.
	

