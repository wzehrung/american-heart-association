use TestHealthDataMart
Go

create schema H360 authorization dbo
go

create schema BP authorization dbo
go

create schema Ref authorization dbo
go


create schema ETLC authorization dbo
go

create schema Rpt authorization dbo
go


/*

=========================================================================================================================
schema expected usage.
-----------------------
--control and log
ETLC - ETL Control objects (logs, control flags, processes, entities)

--source
H360 - anything sourced, extracted, staged or transformed from the h360 database system
Ref - reference data not held in a system but from individual knowledge or personal spreadsheets lookups etc

--target
BP - the target data mart for blood pressure fact information

dbo - conformed or cross datamart dimensions (Date, Time offset etc)
dbo - conformed or multi datamart facts

--delivery
Rpt - direct database reporting (rather than from a cube etc)
BI - for direct access columnstore index tables (due to compatibility issues and non update rules etc in 2008r2, 2012)
=========================================================================================================================

*/

--print @@Version
