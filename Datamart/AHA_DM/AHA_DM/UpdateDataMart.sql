
/*
========================================
PROCESSSING ORDER
========================================
*/
USE [HealthDataMart]
GO
/*
Initialise the data mart
*/
--ETLC.Initialise_ETLCLogs -1
--ETLC.Initialise_DimensionAndBridges -1
--ETLC.Truncate_FactsAndBridges -1
--ETLC.SyncFactDimKeyMapping -1



/*
equivalent of the daily job 
*/
DECLARE @MasterControlID int = -999 -- testrun

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(null, 1, getdate(), null, 'Job Run', '<Message>BP DataMart Processing</Message>', 'In Progress')

set @mastercontrolID = isnull((select max(processRunLogID) from ETLC.ProcessRunLog where processid = 1 and rootprocessexecutionid is null),-999)

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@mastercontrolID, 1, getdate(), null, 'Process Progress', '<Message>Starting Dimension Processing</Message>', '')


/*
========================================
DIMENSIONS FIRST
========================================
ALL extracts first
Then match and changes
Then Load
----------------------------------------
*/


exec BP.Load_Small_Dimensions @MasterControlID
--date - load once at table creation time
--gender (male, female)
--retention (retained,not retained)
--validuser (yes,no)
--bpcategory (high,pre,normal)
--BPReferenceCategory (gives value range for bp category - this is the one that calcuates the inflowing data)
--improvementcategroy (better,same,worse)


--Extract 1nnn
exec h360.Extract_AffiliateInfo @MasterControlID --logs 
exec h360.Extract_CampaignInfo @MasterControlID --logs
exec h360.Extract_EthnicityLookup @MasterControlID --logs
exec h360.Extract_ZipRegion @MasterControlID --logs  --geography
exec h360.Extract_HealthRecordExtendedInfo @MasterControlID --logs


--Transform 2nnn
exec h360.Transform_DimAffiliate @MasterControlID --
exec h360.Transform_Campaign @MasterControlID --
exec H360.Transform_Ethnicity @MasterControlID --
exec h360.Transform_HealthRecord @MasterControlID --logs --have to do this one first so that can include inferred dimensions
exec H360.Transform_HR_InferredGeog @MasterControlID
exec H360.Transform_ZipRegion @MasterControlID --logs


--Load 3nnn
exec dbo.Load_DimAffiliate @MasterControlID --logs
exec dbo.Load_DimCampaign @MasterControlID --logs
exec dbo.Load_DimGeography @MasterControlID --logs
exec dbo.Load_DimEthnicity @MasterControlID --
exec dbo.Load_HealthRecord @MasterControlID --



insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@mastercontrolID, 1, getdate(), null, 'Process Progress', '<Message>Completed Dimension Processing</Message>', '')


/*
========================================
DIMENSION BRIDGES NEXT
========================================
Extract
Outer Matches
Inner matches
Transforms
Load
----------------------------------------
*/

--Extract_Bridge_AffiliateCampaigns

/*
========================================
FACTS NEXT
========================================
----------------------------------------
*/
insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@mastercontrolID, 1, getdate(), null, 'Process Progress', '<Message>Starting Fact Processing</Message>', 'Includes Fact derived dimensions')



exec H360.Extract_BloodPressureInfo @MasterControlID --logs


exec H360.Transform_BPCommentInfo @MasterControlID
exec dbo.Load_DimBPReadingCommentInfo @MasterControlID

exec H360.Transform_BloodPressureInfo @MasterControlID
exec bp.Load_FactBloodPressure @MasterControlID


insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@mastercontrolID, 1, getdate(), null, 'Process Progress', '<Message>Completed Fact Processing</Message>', '')

/*
========================================
SNAPSHOT and TEMPORAL Fact summaries and FACT BRIDGES
========================================
----------------------------------------
*/
--individual by month snapshot - temporal summary
insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@mastercontrolID, 1, getdate(), null, 'Process Progress', '<Message>Starting Aggregation/Precalc Snapshot Fact/Dim</Message>', '')


exec bp.transform_hrtotalhistory @MastercontrolID
exec BP.Load_FactUserTotalHistory @MastercontrolID

exec bp.Transform_HRMonthlyRetention @MasterControlID
exec bP.Load_FactUserMonthlyRetention @MastercontrolID

/*
Other potential datamart subject areas
*/
--BloodPressureGoalInfo --extension of BP
--cholesterol info --new mart
--Glucose info --new mart
--excercise --?
--weight --?



/*
STATISTICS from this run

*/

insert into ETLC.ProcessRunLog(RootProcessExecutionID, ProcessID, ProcessStarted, ExecutionTime, KPIType, KPIMetric, Notes)
values(@mastercontrolID, 1, getdate(), null, 'Process Progress', '<Message>Aggregate Processing Completed</Message>', 'All BP Processing Completed')


update ETLC.ProcessRunLog
set Notes = 'Complete', ExecutionTime = cast( datediff(ms,ProcessStarted, getdate() )/1000.000000 as decimal(16,6))
where RootProcessExecutionID = @MasterControlID and processrunlogid = @mastercontrolid


select d.processname, r.*, d.* 
from etlc.processrunlog r left join etlc.processdefinition d on r.processid = d.[ProcessDefinitionID]
where rootprocessexecutionid = 48
order by rootprocessexecutionid, processstarted

--truncate table etlc.processrunlog

--update etlc.processrunlog  set [RootProcessExecutionID] = 48 where processStarted > '2016-01-20 13:30:00.000'