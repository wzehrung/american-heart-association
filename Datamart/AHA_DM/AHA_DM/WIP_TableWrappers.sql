/*
SIMPLE SCD1 Dimension

copy.
search and replace <sch> with schema actual name
search and replace <dimension> with dimension actual name
save

add field list to d

change the Null/unknown/NA insert row to suit columns and business rules
save

execute
*/
use [HealthDataMart]
go

if exists(select * from sysobjects where name = '<dimension>' and xtype = 'u')
begin
	print 'Table exists - dropping for recreate - <dimension>'
	drop table <Sch>.<dimension>
end
else
	print 'New Table created - <dimension>'
GO

create table <sch>.<dimension>
(
	--surrogate key
	<Dimension>Key int identity(-1,1)
		constraint PK_<sch>_<dimension> Primary Key NONCLUSTERED, 
	--main extract table field list



	--control columns
	PopulatedDateTime datetime
		constraint DF_<sch>_<dimension>_ExtractDT default(getdate()),
	ExtractControlID int, -- master wrapper controlID
	--SSCD2_isCurrentFlag int NULL, ValidFrom Datetime, ValidTo Datetime   --data :  1, '1900-01-01', '9999-12-31'
	--extract control
	FoundMatchFlag int
		constraint DF_<sch>_<dimension>_MatchFlag default(0),
		--set the match to zero if not found - otherwise surrogate key in dimension
	DataChangeFlag int
		constraint DF_<sch>_<dimension>_DataChanged default(0)
		--only valid if matchflag = 1
	
	ValidFromDate date,
	ValidToDate date,
	SCD2RowIsCurrent int
)

--index - create index on main business key; include extract control - if scd2 include from and to date in main index
create index IDX_<sch>_<dimension>_[indexname]
ON <sch>.<dimension>([Business key]) INCLUDE(ExtractControlID) --scd1

create index IDX_<sch>_<dimension>_[indexname]
ON <sch>.<dimension>(ValidFromDate, ValidToDate, [Business key]) INCLUDE(ExtractControlID) --scd2

create index IDX_<sch>_<dimension>_[indexname]
ON <sch>.<dimension>(SCD2RowIsCurrent desc, ValidFromDate, ValidToDate, [Business key]) INCLUDE(ExtractControlID)--scd2 current row

--check business logic of dimension for NA and Unknown otherwise Unknown = -1, NA = 0

insert into <sch>.<dimension>( [field list] ,ExtractControlID) 
-- -1 Extract Control ID is the default initial insert of unknown and test data; 0 = initial population data; > 1 live populations
values('NA', -1),('Unknown', -1)
go


/*

*/