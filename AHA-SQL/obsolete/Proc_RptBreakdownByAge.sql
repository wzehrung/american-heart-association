﻿

-- =============================================
-- Author:		Scott Shipp
-- Create date: January 30, 2009
-- Description:	This is a snapshot of the number
-- of users per age group.
-- 
-- =============================================
CREATE PROCEDURE [dbo].[Proc_RptBreakdownByAge]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select '20 and below' as "Category", COUNT(*) as "Total" from ExtendedProfile WHERE DateOfBirth > '01-16-1987' and UserHealthRecordID in (Select UserHealthRecordID from AHAUserHealthRecord) UNION
	select '20-40', COUNT(*) from ExtendedProfile WHERE (DateOfBirth BETWEEN '01-16-1967' AND '01-16-1987') and UserHealthRecordID in (Select UserHealthRecordID from AHAUserHealthRecord) UNION
	select '40-60', COUNT(*) from ExtendedProfile WHERE (DateOfBirth BETWEEN '01-16-1947' AND '01-16-1967') and UserHealthRecordID in (Select UserHealthRecordID from AHAUserHealthRecord) UNION
	select '60 and above', COUNT(*) from ExtendedProfile where DateOfBirth < '01-16-1947' and UserHealthRecordID in (Select UserHealthRecordID from AHAUserHealthRecord)	

END


