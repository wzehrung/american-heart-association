﻿
		CREATE Procedure [dbo].[Proc_DataMigration]
		As
		Begin

		Begin Try
		Begin Transaction


				select * into  DeprecatedProfile 
				from OPENROWSET(
								'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.Profile
								) 

				select * into DeprecatedZips 
				from OPENROWSET(
								'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.Zips
								) 

				select * into DeprecatedWeight 
				from OPENROWSET(
								'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.Weight
								) 

				select * into DeprecatedPhysicalActivity 
				from OPENROWSET(
								'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.PhysicalActivity
								) 


				select * into DeprecatedIncident 
				from OPENROWSET(
								'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.Incident
								) 

				select * into DeprecatedExercise 
				from OPENROWSET(
								'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.Exercise 
								) 

				select * into DeprecatedBloodPressure 
				from OPENROWSET(
								'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.BloodPressure
								) 




		Declare @HealthRecordProfileMapping table
		(
		 GUID UniqueIdentifier
		,UserHealthRecordID int
		,ProfileID int
		)

				Insert HealthRecord 
				( 
				 PersonalItemGUID
				,CreatedDate
				,UpdatedDate
				,IsMigrated
				)

				select	 GUID
						,LastUpdated
						,LastUpdated 
						,1

				from	OPENROWSET(
									'MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
									MSHealth.dbo.Profile
									) AS A





		insert @HealthRecordProfileMapping

		select B.GUID,A.UserHealthRecordID,B.ProfileID

			 from HealthRecord A         
			 INNER JOIN
			 OPENROWSET('MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
						 MSHealth.dbo.Profile
						) AS  B 
			 on A.PersonalItemGUID=B.GUID 


				Insert ExtendedProfile 
				(
				 UserHealthRecordID
				,ZipCode
				,[Country/Region]
				,DateOfBirth
				,Gender
				,Ethnicity
				,FamilyHistoryHD
				,FamilyHistoryStroke
				,HighBPDiet
				,HighBPMedication
				,KidneyDisease
				,DiabetesDiagnosis
				,HeartDiagnosis	
				,Stroke	
				,PhysicalActivityLevel
				,CreatedDate
				,UpdatedDate
				)


				select 

				 A.UserHealthRecordID 
				,B.Zip					
				,B.Country				
				,B.DateOfBirth			
				,B.Gender					
				,B.Ethnicity				
				,B.FamilyHistoryHD		
				,B.FamilyHistoryStroke	
				,B.HBPDiet				
				,B.HBPMedication			
				,B.KidneyDiagnosis		
				,B.DiabetesDiagnosis		
				,B.HeartDiagnosis				
				,B.Stroke					
				,B.PhysicalActivity	
				,B.LastUpdated
				,B.LastUpdated	

				from  HealthRecord A          
					 INNER JOIN
					 OPENROWSET('MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.Profile
								) AS  B 
				 on A.PersonalItemGUID=B.GUID 




		Insert  BloodPressure    
		(
		 UserHealthRecordID
		,HVItemGuid
		,DateMeasured
		,Diastolic
		,Systolic
		,Comments
		,CreatedDate
		,UpdatedDate
		)

		select 

		 B.UserHealthRecordID 
		,A.GUID
		,A.BPDate
		,A.Diastolic
		,A.Systolic
		,A.Notes
		,A.LastUpdated
		,A.LastUpdated

		from  OPENROWSET('MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
						MSHealth.dbo.BloodPressure
						) AS A   
						INNER JOIN 
				@HealthRecordProfileMapping B

		on A.ProfileID = B.ProfileID


				Insert Weight     
				(
				 UserHealthRecordID
				,HVItemGuid
				,DateMeasured
				,Weight
				,Comments
				,CreatedDate
				,UpdatedDate
				)

				Select 
						
				 B.UserHealthRecordID 
				,A.GUID
				,A.WeightDate
				,A.Weight
				,A.Notes
				,A.LastUpdated
				,A.LastUpdated


				from OPENROWSET('MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
								MSHealth.dbo.Weight
								) AS A  
				INNER JOIN @HealthRecordProfileMapping B
				on A.ProfileID = B.ProfileID



		Insert Exercise    
		(
		 UserHealthRecordID
		,HVItemGuid
		,DateofSession
		,ExerciseType
		,Duration
		,Comments
		,CreatedDate
		,UpdatedDate
		)

		select 

		 B.UserHealthRecordID 
		,A.GUID
		,A.ExerciseDate
		,A.ExerciseType
		,A.Duration
		,A.Notes
		,A.LastUpdated
		,A.LastUpdated

		from OPENROWSET('MSDASQL','DRIVER={SQL Server};SERVER=TXDA1SBEPW6;UID=sa;PWD=learnandlive',
						MSHealth.dbo.Exercise
						) AS A   -- Source
		INNER JOIN @HealthRecordProfileMapping B
		on A.ProfileID = B.ProfileID


		COMMIT
		End Try

		Begin Catch

		  If @@TRANCOUNT > 0
			ROLLBACK
			Declare @ErrMsg nvarchar(4000), @ErrSeverity int
			Select  @ErrMsg = ERROR_MESSAGE(),
				  @ErrSeverity = ERROR_SEVERITY()
			Raiserror(@ErrMsg, @ErrSeverity, 1)

		 End Catch
		End




		