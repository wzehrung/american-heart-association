﻿



CREATE VIEW [dbo].[v_Reporting_YTP_SWA]
AS

SELECT [AffiliateID]
      ,[AffiliateName]
      ,[AffiliateAbbr]
      ,[CampaignID]
      ,[Title]
      ,[URL]
	  ,zrm.ZipCode
      ,[EndDate]
  
  
  FROM [dbo].[v_AffiliatesCampaigns] ac
  INNER JOIN [v_ZipRegionMapping] zrm
  ON ac.AffiliateName = zrm.Affiliate

  WHERE CampaignID = 122
  AND AffiliateName = 'SouthWest'



