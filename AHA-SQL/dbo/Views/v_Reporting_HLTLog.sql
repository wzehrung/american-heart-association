﻿


CREATE VIEW [dbo].[v_Reporting_HLTLog]

AS

SELECT 
		PatientID
      ,hlt.ZipCode AS 'hlt-ZipCode'
	  ,ep.ZipCode AS 'ep-ZipCode'
	  ,zrm.City
	  ,zrm.State
	  ,zrm.Affiliate
	  ,ep.PhoneContact
	  ,ep.PhoneMobile
      ,cd.Title AS 'Campaign'
      ,dbo.ProperCase([FirstName]) AS FirstName
      ,dbo.ProperCase([LastName]) AS LastName
      ,hlt.EmailAddress
      ,hlt.[Error]
      ,hlt.[YearOfBirth] AS 'hlt-YearOfBirth'
	  ,ep.YearOfBirth AS 'ep-YearOfBirth'
	  ,hr.CampaignID
  FROM [dbo].[HLTLog] hlt

  INNER JOIN HealthRecord hr
  ON hr.UserHealthRecordID = PatientID

  LEFT OUTER JOIN ExtendedProfile ep
  ON ep.UserHealthRecordID = PatientID
	
	LEFT OUTER JOIN CampaignDetails cd
	ON hr.CampaignID = cd.CampaignID

	LEFT OUTER JOIN v_ZipRegionMapping zrm
	ON zrm.ZipCode = 
		CASE WHEN hlt.ZipCode <> ''
			THEN
				hlt.ZipCode
			ELSE
				ep.ZipCode
		END

