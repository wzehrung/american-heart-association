﻿




CREATE VIEW [dbo].[v_Get_Reminders_Categories]

AS

SELECT
	ReminderCategoryID,
	CAST(ReminderCategoryID AS varchar) + CAST(GroupID AS varchar) AS ReminderCategoryIDGroupID,
	dbo.ProperCase(Name) AS ReminderCategoryName
	
FROM [3CiPatientReminderCategory]

WHERE ReminderCategoryID <> 7



