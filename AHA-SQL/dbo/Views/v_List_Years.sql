﻿
CREATE VIEW v_List_Years

AS

with yearlist as 
(
    select 2012 as year
    union all
    select yl.year + 1 as year
    from yearlist yl
    where yl.year + 1 <= YEAR(GetDate())
)

select year from yearlist 