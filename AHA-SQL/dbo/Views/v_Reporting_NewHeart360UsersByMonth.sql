﻿



CREATE VIEW [dbo].[v_Reporting_NewHeart360UsersByMonth]

AS

SELECT 
	au.UserID,
	au.SelfHealthRecordID,
	au.CreatedDate,
	YEAR(au.CreatedDate) AS Year, 
    MONTH(au.CreatedDate) AS Month,
	DAY(au.CreatedDate) AS Day,
	LEFT(DATENAME(month, au.CreatedDate), 3) AS 'MonthName',
	CONCAT(LEFT(DATENAME(month, au.CreatedDate), 3), ', ', RIGHT(YEAR(au.CreatedDate),2)) AS 'MonthYear',
	CASE au.AllowAHAEmail
		WHEN 1 THEN 'Yes'
		WHEN 0 THEN 'No'
	END AS AllowAHAEmail,
	au.DefaultLanguageID
FROM AHAUser au



