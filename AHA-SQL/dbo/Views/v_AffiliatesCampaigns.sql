﻿




CREATE VIEW [dbo].[v_AffiliatesCampaigns] --WITH SCHEMABINDING
AS
SELECT
    ac.AffiliateID,
    a.AffiliateName,
	a.AffiliateAbbr,
    cd.CampaignID,
    cd.Title
	,(SELECT COUNT(*) FROM HealthRecord WHERE CampaignID = cd.CampaignID) AS Participants

FROM dbo.AffiliatesCampaigns AS ac 

    INNER JOIN dbo.Affiliates AS a 
	   ON ac.AffiliateID = a.AffiliateID 
    INNER JOIN dbo.CampaignDetails AS cd 
	   ON ac.CampaignID = cd.CampaignID




