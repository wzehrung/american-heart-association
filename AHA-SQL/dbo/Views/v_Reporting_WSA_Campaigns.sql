﻿



CREATE VIEW [dbo].[v_Reporting_WSA_Campaigns]
AS
SELECT        c.CampaignID, cd.Title, c.URL, c.StartDate, c.EndDate
FROM            dbo.Campaign AS c INNER JOIN
                         dbo.CampaignDetails AS cd ON c.CampaignID = cd.CampaignID
WHERE
(
	c.CampaignID IN 
		(
			SELECT CampaignID FROM Campaign
			WHERE 
			URL IN
				(
'AKA-AGO',
'AlcanzaTuMetaOC',
'AltaMed',
'AV',
'BayArea',
'BeYourBest',
'caesars',
'CCCLA',
'CCC-SB',
'CCCWSA',
'centromaravilla',
'CET',
'Clinica',
'coachellasc',
'CoachellaValley',
'ELASC',
'Faith-IE',
'Faith-LA',
'FarmerJohn',
'genhealth',
'GetToGoalAAGreaterBayArea',
'GetToGoal-Seattle',
'Hawaii',
'HCHC',
'HEQSDIE',
'HolyCross',
'houseofhealth',
'idepsca',
'indiosc',
'JobTrain',
'LAOC',
'LatinoHealthAccess',
'LBC',
'LosAngeles',
'missionfoods',
'MonumentImpact',
'Nevada',
'NLBWA',
'Ohlone',
'PepsicoLasVegas',
'QMC',
'Sacramento',
'Salinas',
'SanFranciscoAPI',
'SanJose',
'SESD',
'SiliconValley',
'SYH',
'UltraPro',
'Vivebien',
'WashingtonState'


				)
		)
)



