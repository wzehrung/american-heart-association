﻿



CREATE VIEW [dbo].[v_Reporting_SWA_Campaigns]
AS
SELECT        c.CampaignID, cd.Title, c.URL, c.StartDate, c.EndDate
FROM            dbo.Campaign AS c INNER JOIN
                         dbo.CampaignDetails AS cd ON c.CampaignID = cd.CampaignID
WHERE
(
	c.CampaignID IN 
		(
			SELECT CampaignID FROM Campaign
			WHERE 
			Url IN
				(
'aaqol',
'ABC',
'agape',
'Arkansas',
'attsa',
'Austin',
'Austin-can',
'BexarCounty',
'bodyfitness',
'bps',
'bsoaka',
'c-a-tribes',
'cbhc',
'celanese',
'centura',
'centurylink',
'CHW',
'Cigna-Denver',
'communitycare1',
'communitycare2',
'communitycare3',
'communitycare4',
'COZ',
'CREA',
'CrowleyISD',
'dallasvogelalcove',
'DART',
'DARTCROF',
'DARTEastDallas',
'DARTNRV',
'DARTNW',
'DARTNWROF',
'DARTPioneer',
'DARTPolice',
'DARTSOC',
'DCP',
'Denver',
'DevonEnergyOKC',
'DFW',
'DrWright',
'DSSWsa',
'DSTA',
'FridayFirm',
'FWISD',
'GetToGoalArkansas',
'GetToGoalAustin',
'GetToGoalColorado',
'GetToGoalDallas-FtWorth',
'GetToGoalHouston',
'GetToGoal-OKCNativeAmerican',
'GetToGoalOklahomaCity',
'GetToGoalSanAntonio',
'GetToGoalTulsa',
'gpisd',
'heb12',
'heb16',
'heb2',
'heb30',
'heb5',
'Heifer',
'Heldenfels',
'Houston',
'KleinISD-Annex',
'KleinISD-Benfer',
'KleinISD-Benignus',
'KleinISD-Bernshausen',
'KleinISD-Blackshear',
'KleinISD-Brill',
'KleinISD-Central',
'KleinISD-Doerre',
'KleinISD-Ehrhardt',
'KleinISD-Eiland',
'KleinISD-EppsIsland',
'KleinISD-Frank',
'KleinISD-GraceEngland',
'KleinISD-GreenwoodForest',
'KleinISD-Hassler',
'KleinISD-Haude',
'KleinISD-Hildebrandt',
'KleinISD-Kaiser',
'KleinISD-KIC',
'KleinISD-Kleb',
'KleinISD-KleinCollins',
'KleinISD-KleinForest',
'KleinISD-KleinHigh',
'KleinISD-KleinIntermediate',
'KleinISD-KleinOak',
'KleinISD-Klenk',
'KleinISD-Kohrville',
'KleinISD-Krahn',
'KleinISD-Kreinhop',
'KleinISD-Krimmel',
'KleinISD-Kuehnle',
'KleinISD-Lemm',
'KleinISD-Maintenance',
'KleinISD-McDougle',
'KleinISD-Metzler',
'KleinISD-Mittelstadt',
'KleinISD-Mueller',
'KleinISD-MultiPurpose',
'KleinISD-Nitsch',
'KleinISD-Northampton',
'KleinISD-Police',
'KleinISD-Roth',
'KleinISD-Schindewolf',
'KleinISD-Schultz',
'KleinISD-Strack',
'KleinISD-StudentsServices',
'KleinISD-Theiss',
'KleinISD-TransportationN',
'KleinISD-TransportationS',
'KleinISD-Ulrich',
'KleinISD-Vistas',
'KleinISD-Wunderlich',
'KleinISD-Zwink',
'LGBS',
'littleton',
'madrinas',
'MaranathaSA',
'METRO',
'NativeAmerican',
'NETexas',
'newmexico',
'nmhpdp',
'NWA',
'nylemaxwell',
'OKC',
'pottershouse',
'Pueblo',
'pwc',
'qep',
'rackspace',
'SABHS',
'SaintMarkLR',
'SanAntonio',
'SAOC',
'SAomnihotels',
'SAselect',
'SAVestidoRojo',
'seton',
'SouthTexas',
'TexasLegion',
'THD',
'theluke',
'TheT',
'THR',
'Tulsa',
'unitedway-austin',
'vestidorojookc',
'WalgreensTulsa',
'XTO'
				)
		)
)


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 6
               Left = 248
               Bottom = 136
               Right = 477
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'v_Reporting_SWA_Campaigns';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'v_Reporting_SWA_Campaigns';

