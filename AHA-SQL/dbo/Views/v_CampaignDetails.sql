﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date: 2.11.2014
-- Description:	Pull relevant data into a view for use by stored procedures and/or reports
-- =============================================

CREATE VIEW [dbo].[v_CampaignDetails]

AS

SELECT
	c.CampaignID,
	c.CampaignCode,
	cd.Title AS CampaignTitle,
	c.URL AS CampaignURL,
	cd.Description AS CampaignDescription,
	cd.PromotionalTagLine,
	ab.BusinessID,
	ab.Name AS BusinessName,
	ac.AffiliateID,
	ac.AffiliateName,
	CONVERT(varchar, c.StartDate, 101) AS StartDate,
	CONVERT(varchar, c.EndDate, 101) AS EndDate,
	CONVERT(varchar, c.CreatedDate, 101) AS CreatedDate,
	CONVERT(varchar, c.UpdatedDate, 101) AS UpdatedDate,
	CONVERT(int, cd.IsPublished) AS IsPublished,
	cd.ResourceTitle,
	cd.ResourceURL,
	cd.PartnerLogo,
	cd.PartnerLogoContentType,
	cd.PartnerLogoVersion,
	cd.PartnerImage,
	cd.PartnerImageContentType,
	cd.PartnerImageVersion
	,(SELECT COUNT(*) FROM HealthRecord WHERE CampaignID = cd.CampaignID) AS Participants
	,cd.LanguageID

FROM Campaign c

	INNER JOIN CampaignDetails cd
		ON c.CampaignID = cd.CampaignID

	LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

	LEFT JOIN v_AffiliatesCampaigns ac
		ON c.CampaignID = ac.CampaignID


