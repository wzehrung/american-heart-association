﻿

CREATE VIEW [dbo].[v_ZipRegionMapping]
AS

SELECT
	CASE
		WHEN LEN(ZIP_Code) < 5
		THEN '0' + [ZIP_Code]
		ELSE ZIP_Code
	END ZipCode,
	Zip_Name AS City,
	State,
	Affiliate,
	CountyName AS County

FROM ZipRegionMapping