﻿
CREATE VIEW [dbo].[v_Reporting_ExtendedProfile]

AS

SELECT 
	hr.UserHealthRecordID,
	hr.CreatedDate,
	YEAR(hr.CreatedDate) AS Year, 
    MONTH(hr.CreatedDate) AS Month,
	DAY(hr.CreatedDate) AS Day,
	LEFT(DATENAME(month, hr.CreatedDate), 3) AS 'MonthName',
	CONCAT(LEFT(DATENAME(month, hr.CreatedDate), 3), ', ', RIGHT(YEAR(hr.CreatedDate),2)) AS 'MonthYear',
	ep.Gender,
	ep.Ethnicity,
	ep.YearOfBirth,
	YEAR(GETDATE()) - ep.YearOfBirth AS EstimatedAge
FROM HealthRecord hr

INNER JOIN ExtendedProfile ep
	ON ep.UserHealthRecordID = hr.UserHealthRecordID


