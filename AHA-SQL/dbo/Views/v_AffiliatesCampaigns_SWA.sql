﻿

CREATE VIEW [dbo].[v_AffiliatesCampaigns_SWA]
AS
SELECT     ac.AffiliateID, a.AffiliateName, cd.CampaignID, cd.Title, c.URL, c.EndDate
FROM         dbo.AffiliatesCampaigns AS ac 
	INNER JOIN dbo.Affiliates AS a 
		ON ac.AffiliateID = a.AffiliateID 
	INNER JOIN dbo.CampaignDetails AS cd 
		ON ac.CampaignID = cd.CampaignID
	INNER JOIN Campaign c
		ON ac.CampaignID = c.CampaignID

  WHERE ac.AffiliateID = 6
  
  AND URL NOT IN
  (
  'Colorado',
'COZ', 
'DARTCROF-WSA', 
'DARTMaintenance', 
'DARTNWROF-WSA', 
'Ehrhardt',
'HEQTest', 
'KleinISD',
'MaranthaSA', 
'RWJFW',
'SAomnihotels',
'vestidorojookc',
'SAselect'
)
AND URL NOT LIKE '%test%'

