﻿CREATE VIEW v_AllProviders

AS

SELECT
	ProviderID
	,UserName
	,Salutation
	,dbo.ProperCase(FirstName) AS FirstName
	,dbo.ProperCase(LastName) AS LastName
	,UserTypeID
	,ut.Display
	,Email
	,dbo.ProperCase(PracticeName) AS PracticeName
	,PracticeEmail
	,PracticeAddress
	,
	CASE 
	WHEN LEN(LTRIM(RTRIM(PracticePhone)))='10' THEN '('+SUBSTRING(PracticePhone,1,3)+')'+' '+SUBSTRING(PracticePhone,4,3)+'-'+SUBSTRING(PracticePhone,7,4) 
	WHEN LEN(LTRIM(RTRIM(PracticePhone)))='' THEN ' ' END 
	AS PracticePhone

	,
	CASE 
	WHEN LEN(LTRIM(RTRIM(PracticeFax)))='10' THEN '('+SUBSTRING(PracticeFax,1,3)+')'+' '+SUBSTRING(PracticeFax,4,3)+'-'+SUBSTRING(PracticeFax,7,4) 
	WHEN LEN(LTRIM(RTRIM(PracticeFax)))='' THEN ' ' END 
	AS PracticeFax

	,dbo.ProperCase(Speciality) AS Specialty
	,CONVERT(nvarchar,DateOfRegistration) AS RegistrationDate
	,ProfileMessage
	,PracticeURL
	,dbo.ProperCase(City) AS City
	,dbo.ProperCase(li.ListItemName) AS [State]
	,Zip AS ZipCode
	,ProviderCode
	,p.CampaignID
	,c.URL AS CampaignURL

FROM Provider p
 
	INNER JOIN ListItems li ON p.StateID = li.ListItemID
	INNER JOIN Campaign c ON c.CampaignID = p.CampaignID
	INNER JOIN UserType ut ON ut.ID = p.UserTypeID

WHERE li.LanguageID = 1