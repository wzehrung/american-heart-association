﻿


CREATE VIEW [dbo].[v_Reporting_SSRS_BloodPressure]
AS

SELECT 

	ROW_NUMBER() OVER(PARTITION BY [UserHealthRecordID] ORDER BY [UserHealthRecordID]) AS ReadingNumber,
	[UserHealthRecordID],
	[JoinedDate],
	[Systolic],
	[Diastolic],
	[Gender],
	[Ethnicity],
	[Year],
	[Month],
	[Day],
	--FORMAT([DateMeasured], 'yyyyMMdd') AS [DateMeasuredComposite],
	[DateMeasured],
	[CampaignID],
	[Campaign],
	[AffiliateID],
	[AffiliateName],
	[ZipCode],
	[City],
	[State],
	--'' AS ReadingID
	CONCAT(ROW_NUMBER() OVER(PARTITION BY [UserHealthRecordID] ORDER BY [UserHealthRecordID]),[UserHealthRecordID]) AS ReadingID
	,[YearOfBirth]
	,YEAR(GETDATE()) - YearOfBirth AS EstimatedAge
	,CreatedDate AS ReadingCreatedDate
	--INTO #bloodpressure
FROM [dbo].[v_Reporting_BloodPressure]

--WHERE 
--	DateMeasured BETWEEN '2015-02-01' AND '2015-10-03'
--	AND 
--	(
--		CampaignID 	IN
--		(
--			115
--			--SELECT CampaignID FROM v_Reporting_CCC_YearEnd_Campaigns
--		)
--	)
	--AND (CreatedDate < '2015-07-16')

--ORDER BY UserHealthRecordID, DateMeasured

--SELECT 
--* FROM #bloodpressure
----GROUP BY UserHealthRecordID
----HAVING COUNT(*) > 1
--ORDER BY UserHealthRecordID, DateMeasured

