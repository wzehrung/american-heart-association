﻿

CREATE VIEW [dbo].[v_Reporting_IVR_Reminders]

AS

SELECT
	pr.ReminderID,
	pr.PatientID,
	RIGHT(pr.PhoneNumber, 10) AS PhoneNumber,
	pr.Time,
	pr.Day,
	pr.TimeZone,
	pr.Enabled,
	pr.CreatedDateTime,
	pr.UpdatedDateTime,
	prl.Responded,
	prl.RetryAttempts,
	prl.FinalAction,
	prl.ActionDate,
	prl.ActionSource,
	prl.CreatedDateTime AS 'LogDate'


FROM IVRPatientReminder pr

LEFT JOIN IVRPatientReminderLog prl
	ON pr.PatientID = prl.PatientID
