﻿

CREATE VIEW [dbo].[v_Reporting_SMS_Reminders_Log]

AS

SELECT
	pr.ReminderID,
	pr.PatientID,
	RIGHT(pr.MobileNumber, 10) AS MobileNumber,
	prl.Message,
	prl.MessageDate,
	
	CASE prl.Direction
		WHEN 0 THEN 'Outgoing'
		WHEN 1 THEN 'Incoming'
	END AS Direction

FROM [3CiPatientReminder] pr

LEFT JOIN [3CiPatientReminderLog] prl
	ON pr.PatientID = prl.PatientID
