﻿
CREATE VIEW v_Reporting_ProviderVolunteerEmailAddresses

AS

SELECT
	dbo.ProperCase(p.FirstName) AS FirstName,
	dbo.ProperCase(p.LastName) AS LastName,
	p.Email,
	p.PracticeName,
	p.DateOfRegistration,
	cd.Title,
	p.Zip
FROM Provider p
	INNER JOIN CampaignDetails cd
		ON cd.CampaignID = p.CampaignID