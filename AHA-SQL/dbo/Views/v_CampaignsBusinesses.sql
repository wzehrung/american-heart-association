﻿
CREATE VIEW [dbo].[v_CampaignsBusinesses] WITH SCHEMABINDING

AS

SELECT
	c.CampaignID
	,c.URL
	,cd.Title
	,cd.PromotionalTagline
	,cd.IsPublished
	,cd.ResourceTitle
	,cd.ResourceURL
	,c.StartDate
	,c.EndDate
	,c.CreatedDate
	,c.UpdatedDate
	,c.BusinessID
	,ab.Name AS BusinessName
	,ab.Comments


FROM dbo.Campaign c

INNER JOIN dbo.CampaignDetails cd 
	ON c.CampaignID = cd.CampaignID

LEFT JOIN dbo.AdminBusiness ab
	ON c.BusinessID = ab.BusinessID


