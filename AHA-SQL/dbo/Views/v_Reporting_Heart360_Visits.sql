﻿
CREATE VIEW v_Reporting_Heart360_Visits

AS

SELECT
	AHAUserID,
	LoggedInDate,
	YEAR(LoggedInDate) AS [Year], 
    MONTH(LoggedInDate) AS [Month],
	DAY(LoggedInDate) AS [Day],
	CONCAT(LEFT(DATENAME(month, LoggedInDate), 3), ', ', RIGHT(YEAR(LoggedInDate),2)) AS 'MonthYear'


FROM AHAUserLoginDetails