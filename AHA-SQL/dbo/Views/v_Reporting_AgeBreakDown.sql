﻿
CREATE VIEW [dbo].[v_Reporting_AgeBreakDown]

AS

SELECT 
	ep.UserHealthRecordID,
	ep.CreatedDate,
	ep.MonthYear,
	Year,
	Month,
	Day,
	ep.YearOfBirth,
	EstimatedAge,
	CASE
		WHEN EstimatedAge < 25
		THEN 'Under 25'
		WHEN EstimatedAge BETWEEN 24 and 35
		THEN '25-34'
		WHEN EstimatedAge BETWEEN 34 and 45
		THEN '35-44'
		WHEN EstimatedAge BETWEEN 44 and 55
		THEN '45-54'
		WHEN EstimatedAge BETWEEN 54 and 65
		THEN '55-64'
		WHEN EstimatedAge BETWEEN 64 and 75
		THEN '65-74'
		WHEN EstimatedAge > 74
		THEN '75 or older'
		ELSE 'Not Reported'
	END AS 'Range'

FROM v_Reporting_ExtendedProfile ep

