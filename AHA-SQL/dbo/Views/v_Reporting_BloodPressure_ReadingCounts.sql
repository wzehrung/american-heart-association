﻿


CREATE VIEW [dbo].[v_Reporting_BloodPressure_ReadingCounts]

AS

SELECT DISTINCT
	UserHealthRecordID,
	COUNT(UserHealthRecordID) OVER (PARTITION BY UserHealthRecordID) AS ReadingCount

FROM BloodPressure


