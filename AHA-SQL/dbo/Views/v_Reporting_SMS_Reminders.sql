﻿

CREATE VIEW [dbo].[v_Reporting_SMS_Reminders]

AS

SELECT
	pr.ReminderID,
	pr.PatientID,
	RIGHT(pr.MobileNumber, 10) AS MobileNumber,
	pr.TimeZone,
	pr.Terms,
	pr.PinValidated,
	pr.CreatedDateTime,
	pr.UpdatedDateTime,
	prs.Status,
	prc.Name AS 'Category',
	prs.Message,
	prs.Sunday,
	prs.Monday,
	prs.Tuesday,
	prs.Wednesday,
	prs.Thursday,
	prs.Friday,
	prs.Saturday,
	prs.CreatedDateTime AS 'ScheduleCreatedDate',
	prs.UpdatedDateTime AS 'ScheduleUpdatedDate'


FROM [3CiPatientReminder] pr

LEFT JOIN [3CiPatientReminderSchedule] prs
	ON pr.ReminderID = prs.ReminderID

LEFT JOIN [3CiPatientReminderCategory] prc
	ON prs.ReminderCategoryID = prc.ReminderCategoryID
