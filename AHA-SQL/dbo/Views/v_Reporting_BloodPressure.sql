﻿

CREATE VIEW [dbo].[v_Reporting_BloodPressure]
AS
SELECT     TOP (100) PERCENT 
	hr.UserHealthRecordID,
	hr.CreatedDate AS JoinedDate,
	bp.Systolic,
	bp.Diastolic,
	YEAR(bp.DateMeasured) AS Year, 
    MONTH(bp.DateMeasured) AS Month,
	DAY(bp.DateMeasured) AS DAY,
	bp.DateMeasured,
	hr.CampaignID,
	cd.Title AS Campaign,
	ac.AffiliateID,
	af.AffiliateName, 
	zrm.City,
	zrm.State,
	ep.ZipCode,
	ep.Gender, 

	CASE
		WHEN ep.Ethnicity IS NULL
			OR ep.Ethnicity = ''
			OR ep.Ethnicity = 'Select:'
		THEN 'Not Reported'
		
		WHEN ep.Ethnicity = '1'
		THEN 'American Indian or Alaska Native'
		
		WHEN ep.Ethnicity = '2'
		THEN 'Asian'
		
		WHEN ep.Ethnicity = '3'
		THEN 'Black or African American'
		
		WHEN ep.Ethnicity = '4'
		THEN 'Hispanic or Latino'
		
		WHEN ep.Ethnicity = '5'
		THEN 'Native Hawaiian or Other Pacific Islander'		

		WHEN ep.Ethnicity = '6'
		THEN 'White'	

		WHEN ep.Ethnicity = '7'
		THEN 'Mixed Race'	
		
		WHEN ep.Ethnicity = '8'
		THEN 'Other Race'			
		
		ELSE ep.Ethnicity
	END Ethnicity,
	ep.YearOfBirth, bp.CreatedDate


FROM
	dbo.HealthRecord AS hr 
	INNER JOIN dbo.BloodPressure AS bp 
		ON hr.UserHealthRecordID = bp.UserHealthRecordID 
	LEFT OUTER JOIN dbo.CampaignDetails AS cd 
		ON cd.CampaignID = hr.CampaignID 
	LEFT OUTER JOIN	dbo.AffiliatesCampaigns AS ac 
		ON ac.CampaignID = hr.CampaignID 
	LEFT OUTER JOIN dbo.Affiliates AS af 
		ON af.AffiliateID = ac.AffiliateID
	LEFT OUTER JOIN ExtendedProfile ep
		ON ep.UserHealthRecordID = hr.UserHealthRecordID
	LEFT OUTER JOIN v_ZipRegionMapping zrm 
		ON zrm.ZipCode = ep.ZipCode

--WHERE     (hr.CampaignID IS NOT NULL)
ORDER BY hr.UserHealthRecordID


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4[60] 2) )"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 3
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "hr"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "bp"
            Begin Extent = 
               Top = 6
               Left = 281
               Bottom = 114
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "cd"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ac"
            Begin Extent = 
               Top = 114
               Left = 285
               Bottom = 207
               Right = 436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "af"
            Begin Extent = 
               Top = 210
               Left = 285
               Bottom = 303
               Right = 436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1935
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWi', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'v_Reporting_BloodPressure';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'dths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'v_Reporting_BloodPressure';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'v_Reporting_BloodPressure';

