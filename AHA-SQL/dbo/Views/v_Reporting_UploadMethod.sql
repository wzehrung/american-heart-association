﻿
CREATE VIEW v_Reporting_UploadMethod

AS

SELECT 
	UserHealthRecordID, 
	CreatedDate,
	CONCAT(LEFT(DATENAME(month, CreatedDate), 3), ', ', RIGHT(YEAR(CreatedDate),2)) AS 'MonthYear',
	CASE 
		WHEN ReadingSource IS NULL THEN 'Other' 
		WHEN ReadingSource Like 'Aetna%' THEN 'mobile'
		WHEN ReadingSource Like 'And%' THEN 'Home'
		WHEN ReadingSource Like 'Omron%' THEN 'Home'
		WHEN ReadingSource Like 'homedco%' THEN 'Home'
		WHEN ReadingSource Like 'Polka%' THEN 'Mobile'
		WHEN ReadingSource Like 'homedics%' THEN 'Home'
		WHEN ReadingSource Like 'microlife%' THEN 'Home'
		WHEN ReadingSource Like 'Doctor%Office%' THEN 'Doctor Office'
		WHEN ReadingSource Like 'mobile%' THEN 'mobile'
		WHEN ReadingSource Like 'Pharmacy%' THEN 'Pharmacy'
		WHEN ReadingSource Like 'Home%' THEN 'Home'
		WHEN ReadingSource Like 'Hospital%' THEN 'Hospital'
		WHEN ReadingSource Like 'Health Fair%' THEN 'Health Fair'
		WHEN ReadingSource Like 'IVR%' THEN 'IVR'
		WHEN ReadingSource Like 'Device%' THEN 'Device'
		WHEN ReadingSource Like 'YMCA%' THEN 'YMCA'
	ELSE 'Other' END ReadingSource

FROM BloodPressure