﻿


CREATE proc [dbo].[proc_UnPublishCampaign]
(
 @CampaignID int
,@LanguageID int
)
as
begin
	set nocount on

IF @LanguageID IS NULL
update CampaignDetails set IsPublished=0 where CampaignID=@CampaignID
	
	
ELSE
Update CampaignDetails set isPublished=0 where CampaignID=@CampaignID and LanguageID=@LanguageID
	
end 