﻿
--[Proc_GetPatientsNotLoggedInForLastNdays] 20
CREATE PROC Proc_GetPatientsNotLoggedInForLastNdays
(
 @NumberofDays int
,@CampaignID  int =null
)
As

BEGIN

	;WITH LoggedInDetails AS
		(
			SELECT 
				U.UserID, 
				CASE 
					WHEN LD.LoggedInDate IS NULL THEN U.CreatedDate
					ELSE LD.LoggedInDate
				END LastLoggedDate,
				Rank() 
					OVER
					(
						PARTITION BY U.UserID 
						ORDER BY 
							CASE 
								WHEN LD.LoggedInDate IS NULL THEN U.CreatedDate
					ELSE LD.LoggedInDate
				END DESC) Rank
			FROM AHAUser U left join AHAUserLoginDetails LD ON U.UserID=LD.AHAUserID
			WHERE AllowAHAEmail=1
		)
	SELECT 
		U.*,Lan.LanguageLocale 
	FROM 
		AHAUser U INNER JOIN LoggedInDetails LD ON U.UserID = LD.UserID
		inner join Languages Lan on U.DefaultLanguageID = Lan.LanguageID
		inner join AHAUserHealthRecord AHR on U.UserID=AHR.UserID
		inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID 

	WHERE 
		Rank=1
		AND	CONVERT(DATETIME,CONVERT(VARCHAR,LastLoggedDate,103),103)=DATEADD(DAY,-1*@NumberofDays,CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103))
		and (H.CampaignID=@CampaignID or @CampaignID is null)

END
