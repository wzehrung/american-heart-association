﻿CREATE PROC [dbo].[Proc_Report_GetPartnersForCampaigns] (@CampaignIDs NVARCHAR(500))
as BEGIN

	DECLARE @SQLQuery AS NVARCHAR(500)

	SET @SQLQuery = 'SELECT DISTINCT ProviderID, PracticeName FROM provider WHERE campaignid  
	IN(' + @CampaignIDs + ') ORDER BY PracticeName'
	EXECUTE sp_executesql @SQLQuery

END
