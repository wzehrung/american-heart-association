﻿


CREATE PROC [dbo].[Proc_UpdateResource]
(
 @ResourceID int
,@ContentTitle NVARCHAR(80)
,@ContentTitleUrl nvarchar(255)
,@ContentText nvarchar(MAX)
,@ExpirationDate datetime
,@UpdatedByUserID int
,@LanguageID int
,@providerID int = 0
)
As
Begin

set nocount on
if(@ProviderID IS NULL)
BEGIN
 SET @ProviderID=0
END
if(@ProviderID = NULL)
BEGIN
 SET @ProviderID=0
END
if (@ProviderID = 0)
begin

Update Resources 
SET
ContentTitle = @ContentTitle
,ContentTitleUrl = @ContentTitleUrl
,ContentText = @ContentText
,ExpirationDate =@ExpirationDate
,UpdatedByUserID =@UpdatedByUserID
WHERE ResourceID=@ResourceID and LanguageID=@LanguageID and ProviderID is null

end
else
begin
Update Resources 
SET
ContentTitle = @ContentTitle
,ContentTitleUrl = @ContentTitleUrl
,ContentText = @ContentText
,ExpirationDate =@ExpirationDate
,UpdatedByUserID =@UpdatedByUserID
,ProviderID = @ProviderID
WHERE ResourceID=@ResourceID and LanguageID=@LanguageID and ProviderID = @ProviderID
end
End




