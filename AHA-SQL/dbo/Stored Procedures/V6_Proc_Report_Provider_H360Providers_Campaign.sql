﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 6.18.2013
-- Description:	Refactor of Proc_Report_Provider_H360Providers_Campaign for Campaign/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Provider_H360Providers_Campaign] 
(  
 @StartDate Datetime,  
 @EndDate Datetime,
 @CampaignID int = null,	-- null specifies all campaigns
 @Markets NVARCHAR(MAX),	-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
 @UserTypeID int = null
) 
AS
BEGIN
	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
		from ZipRegionMapping 
		where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	--
	-- Create yet another temp table containing all the providers who are in the given campaign(s) and markets
	--
	-- drop table #RptProviders
	SELECT P.ProviderID, P.DateOfRegistration, P.DefaultLanguageID INTO #RptProviders
		FROM Provider P
		WHERE ((@CampaignId is null and P.CampaignID is not null) or (@CampaignId is not null and @CampaignId = P.CampaignID)) AND
			  (@ZipCodeCount = 0 or P.Zip IN (SELECT Zip_Code FROM #RptMarketZipCodes)) AND
			   P.UserTypeID = case  when @UserTypeID IS NULL then P.UserTypeID else @UserTypeID end
 
	--
	-- Now generate the reports
	--
	
	;with RptTypes As
	(
		select 1 RptID,'New' As RptType
		union all
		select 2 RptID,'Total at end of month' As RptType
		union all
		select 3 RptID,'Total Active this month' As RptType
		union all
		select 4 RptID,'Total Spanish at end of month' As RptType
	),
	RptTypesWithDates As
	(
		select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,Result As
	(
		select RptID, R.Date1, count(distinct P.ProviderID)  ProviderCount, R.RptType
			from RptTypesWithDates R 
			Left join ProviderLoginDetails A on A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2
			left join #RptProviders P on P.ProviderID=A.ProviderID
			where  R.RptID=3 
			GROUP BY R.Date1, R.RptType, RptID

		union all

		select RptID, R.Date1,count(distinct P.ProviderID)  ProviderCount, R.RptType
			from RptTypesWithDates R 
			Left join #RptProviders P on P.DateOfRegistration < R.Date2 
			where  R.RptID=2
			GROUP BY R.Date1,R.RptType,RptID

		union all

		select RptID, R.Date1, count(distinct P.ProviderID)  ProviderCount, R.RptType
			from RptTypesWithDates R 
			Left join #RptProviders P on P.DateOfRegistration >= R.Date1 
				and  P.DateOfRegistration < R.Date2  
			where  R.RptID=1
			GROUP BY R.Date1,R.RptType,RptID

		union all

		select RptID, R.Date1,count(distinct P.ProviderID)  ProviderCount, R.RptType
			from RptTypesWithDates R 
			Left join #RptProviders P on P.DateOfRegistration < R.Date2 and P.DefaultLanguageID=2
			where  R.RptID=4
			GROUP BY R.Date1,R.RptType,RptID

	)


--,ToBePivoted As
--(
	Select date1,RptType,ProviderCount,RptID,
			'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
			into #ToBePivoted
			from Result
			order by date1
--)

	select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1

	DECLARE @MaxCount INTEGER
	DECLARE @Count INTEGER
	DECLARE @Txt VARCHAR(MAX)
	SET @Count = 1
	SET @Txt = '['
	SET @MaxCount = (SELECT MAX(RowID) FROM #c)
	WHILE @Count<=@MaxCount
		BEGIN
		IF @Txt!=''
			SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
		ELSE
			SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
		SET @Count=@Count+1
		END
	--SELECT @Txt AS Txt

	SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

	SET @Cols = @Txt
	/*
	SELECT  @cols = STUFF(( SELECT DISTINCT TOP 100 PERCENT
									'],[' + t.ColumnIndex
							FROM #ToBePivoted AS t
							ORDER BY '],[' + t.ColumnIndex
							FOR XML PATH('')
						  ), 1, 2, '') + ']'

	*/
	SET @query = N'SELECT RptType, '+ @cols +' FROM
	(SELECT RptType, ColumnINdex, ProviderCount,RptID FROM #ToBePivoted ) A 
	PIVOT (SUM(ProviderCount) FOR ColumnIndex IN ( '+ @cols +' )) B  ;'

	EXECUTE(@query)
	
END

