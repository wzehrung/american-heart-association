﻿



CREATE PROCEDURE [dbo].[Proc_FetchContentinformationForSpecificAbbreviation]
(
@Abbreviation nvarchar(3)
,@LanguageID int
)
AS
Begin

/* Author			:Shipin Anand
   Created Date	    :2008-08-18  
   Description      :Randomly selecting Content Information for Specific Abbreviation
					
*/

set nocount on


		select top 5 
					 A.DidYouKnowLinkUrl
					,A.Title
					,A.DidYouKnowText
					,A.DidYouKnowLinkTitle

					from  

						  
						dbo.[Content] A inner join
						dbo.ContentRuleMapping C on
						A.ContentID=C.ContentID  and A.LanguageID=C.LanguageID
						inner join
						dbo.[ContentRules] B on
						B.ContentRulesID=C.ContentRulesID and  B.LanguageID=C.LanguageID
						and B.Abbreviation   = @Abbreviation 
                       where A.LanguageID=@LanguageID
		--where isnull(A.ExpirationDate,'9999.12.31')>=getdate()						
						order by newID()

End





