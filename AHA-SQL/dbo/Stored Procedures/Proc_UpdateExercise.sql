﻿

CREATE procedure [dbo].[Proc_UpdateExercise]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@DateofSession Datetime
,@ExerciseType nvarchar(50)  
,@Intensity nvarchar(50) 
,@Duration float 
,@Comments nvarchar(max)
,@NumberOfSteps int
,@ReadingSource nvarchar(100)
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :updating Exercise table ..
*/


Update dbo.Exercise

          SET

		   HVVersionStamp		=  @HVVersionStamp
		  ,DateofSession		=  @DateofSession 
		  ,ExerciseType			=  @ExerciseType 
		  ,Intensity			=  @Intensity 
		  ,Duration				=  @Duration 
		  ,Comments				=  @Comments 
		  ,UpdatedDate			=  GetDate()
		  ,NumberOfSteps        =  @NumberOfSteps
		  ,ReadingSource		=  @ReadingSource
          Where HVItemGuid=@HVItemGuid 

End


