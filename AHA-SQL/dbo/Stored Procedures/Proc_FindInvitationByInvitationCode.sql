﻿

CREATE PROCEDURE [dbo].[Proc_FindInvitationByInvitationCode]
(
 @InvitationCode nchar(6)
)
As
Begin
set nocount on

	select *
	 from  PatientProviderInvitation where InvitationCode=@InvitationCode
	
End
