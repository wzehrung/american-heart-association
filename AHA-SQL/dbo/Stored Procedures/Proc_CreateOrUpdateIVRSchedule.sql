﻿CREATE PROCedure [dbo].[Proc_CreateOrUpdateIVRSchedule]

(
	@PatientID Int
	,@PIN varchar(4)
	,@Time nvarchar(35)
	,@Day nvarchar(9)
	,@TimeZone nvarchar(25)
	,@Enabled bit
	,@PhoneNumber nvarchar(15)
)

AS

BEGIN

    SET @PhoneNumber = replace(@PhoneNumber, '(', '')
    SET @PhoneNumber = replace(@PhoneNumber, ')', '')
    SET @PhoneNumber = replace(@PhoneNumber, '-', '')

	IF EXISTS (SELECT * FROM IVRPatientReminder WHERE PatientID=@PatientID)  
	
		UPDATE IVRPatientReminder 
			
		SET [Enabled] = @Enabled, PIN = @PIN, [Time] = @Time, [Day] = @Day, TimeZone = @TimeZone, PhoneNumber = @PhoneNumber
		
		WHERE PatientID = @PatientID

	ELSE

	BEGIN
	
		INSERT IVRPatientReminder
			(PatientID, PIN, [Time], [Day], TimeZone, [Enabled], CreatedDateTime, UpdatedDateTime, PhoneNumber)
			SELECT @PatientID, @PIN,@Time,@Day,@TimeZone,@Enabled, GetDate(), GetDate(), @PhoneNumber

	END

END
