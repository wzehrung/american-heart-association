﻿
create Procedure Proc_CreateOrChangeExerciseGoal
(
 @PersonalItemGUID uniqueidentifier
,@HVItemGuid uniqueidentifier 
,@HVVersionStamp uniqueidentifier 
,@Intensity nvarchar(50)
,@Duration float
)

AS
Begin

/* 

   Author               :Shipin Anand
   Created Date         :2010-09-06

   Description			:modification/insertion (ExerciseGoal table )

						Rule : if exists same Itemguid,HealthGuid and
						VersionGUID then Update the table
						else  Insert a new entry

*/

set nocount on

Begin Try
Begin Transaction

                         

if exists    (
              select ExerciseGoalId from dbo.ExerciseGoal where
              HVItemGuid=@HVItemGuid 
              )

Exec [Proc_UpdateExerciseGoal]		     @HVItemGuid  
										,@HVVersionStamp  
										,@Intensity 
										,@Duration 
										

else

Begin

Declare @UserHealthRecordID int
select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
                             where PersonalItemGUID=@PersonalItemGUID
                                 



Exec Proc_InsertExerciseGoal		     @UserHealthRecordID
										,@HVItemGuid  
										,@HVVersionStamp 
										,@Intensity 
										,@Duration 
										

End

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End
