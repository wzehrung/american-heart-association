﻿

CREATE PROCEDURE [dbo].[Proc_Get_NewAnnouncementsForPatient]
(
@PatientID int
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @LoginDate datetime

SET @LoginDate = (
SELECT LoggedInDate
FROM
(
SELECT 
	[LoginDetailsID]
    ,[AHAUserID]
    ,LoggedInDate
	,ROW_NUMBER() OVER(ORDER BY LoggedInDate DESC) AS [rownum]

FROM [dbo].[AHAUserLoginDetails]

	WHERE AHAUserID IN (SELECT UserID FROM AHAUserHealthRecord WHERE UserHealthRecordID = @PatientID)
) T

WHERE rownum = (2)
)

SELECT DISTINCT
	pc.PatientContentID
	,pc.ProviderID
	,ISNULL(p.Salutation, '') + ' ' + dbo.ProperCase(p.FirstName) + ' ' + dbo.ProperCase(p.LastName) AS ProviderName
	,Convert(varchar, ISNULL(pc.UpdatedDate, GETDATE()), 101) AS UpdatedDate
	,Convert(varchar, ISNULL(pc.CreatedDate, GETDATE()), 101) AS CreatedDate
	,pgm.PatientID

FROM PatientContent pc

	INNER JOIN Provider p
		ON pc.ProviderID = p.ProviderID

    INNER JOIN PatientGroupDetails pgd
        ON p.ProviderID = pgd.ProviderID
                  
    INNER JOIN PatientGroupMapping pgm 
        ON pgd.GroupID=PGM.GroupID AND pgm.PatientID=@PatientID
	
WHERE pc.ProviderID IS NOT NULL
	AND pc.UpdatedDate > @LoginDate

END
