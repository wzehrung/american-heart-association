﻿
CREATE Procedure Proc_CreateOrChangeWizardDetails
(
 @PersonalItemGUID uniqueidentifier 
,@WizardStepIndex int
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (BloodPressure table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

set nocount on


Begin Try
Begin Transaction

Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID


if exists(
		  select UserHealthRecordID from dbo.ExtendedProfile where 
		  UserHealthRecordID=@UserHealthRecordID 
		  )

update dbo.ExtendedProfile set WizardStepIndex = @WizardStepIndex where 
UserHealthRecordID=@UserHealthRecordID 


else 
begin
insert into dbo.ExtendedProfile(UserHealthRecordID,WizardStepIndex) values (@UserHealthRecordID,@WizardStepIndex)
end	


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End
