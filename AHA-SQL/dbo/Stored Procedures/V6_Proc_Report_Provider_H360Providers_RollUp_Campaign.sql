﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 2.12.2014
-- Description:	Extension of Proc_Report_Provider_H360Providers
--              to handle new requirements from AHA
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Provider_H360Providers_RollUp_Campaign] 
(  
 @StartDate Datetime,  
 @EndDate Datetime,
 @CampaignID int = null,	-- null specifies all campaigns
 @Markets NVARCHAR(MAX),	-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
 @UserTypeID int = null	
)
AS
BEGIN
	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
		from ZipRegionMapping 
		where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	--
	-- Create yet another temp table containing all the providers who are in the given campaign(s) and markets
	--
	-- drop table #RptProviders
	SELECT P.ProviderID, P.DateOfRegistration, P.DefaultLanguageID, 
	       Case when P.Speciality = 'Cardiologist' then 'Cardiologists'
	            when P.Speciality = 'AHA Volunteer' then 'Volunteers'
	            when P.Speciality is not null AND P.Speciality != 'Cardiologist' AND P.Speciality != 'AHA Volunteer' then 'Other HCPs'
	            when P.Speciality is null then 'Unknown'
	        End ProvSpecialty
	    INTO #RptProviders
		FROM Provider P
		WHERE
		  (@CampaignId IS NULL OR @CampaignId = P.CampaignID)
		  AND
		  (@ZipCodeCount = 0 OR P.Zip IN (SELECT Zip_Code FROM #RptMarketZipCodes))
		  AND
		  (P.UserTypeID = case  when @UserTypeID IS NULL then P.UserTypeID else @UserTypeID end)
		  -- Only get those in a campaign
		  AND
		  (P.CampaignID IS NOT NULL)
		  
	--
	-- Now generate the reports
	--
    
    ;with RptTypes As
	(
		select 1 RptID,'New' As RptType, 'All' as RptSpecialty
		union all
		select 2 RptID,'New' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 3 RptID,'New' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 4 RptID,'New' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 5 RptID,'New' As RptType, 'Unknown' as RptSpecialty
		union all
		select 6 RptID,'Total at end of month' As RptType, 'All' as RptSpecialty
		union all
		select 7 RptID,'Total at end of month' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 8 RptID,'Total at end of month' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 9 RptID,'Total at end of month' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 10 RptID,'Total at end of month' As RptType, 'Unknown' as RptSpecialty
		union all
		select 11 RptID,'Total Active this month' As RptType, 'All' as RptSpecialty
		union all
		select 12 RptID,'Total Active this month' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 13 RptID,'Total Active this month' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 14 RptID,'Total Active this month' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 15 RptID,'Total Active this month' As RptType, 'Unknown' as RptSpecialty
		union all
		select 16 RptID,'Total Spanish at end of month' As RptType, 'All' as RptSpecialty
		union all
		select 17 RptID,'Total Spanish at end of month' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 18 RptID,'Total Spanish at end of month' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 19 RptID,'Total Spanish at end of month' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 20 RptID,'Total Spanish at end of month' As RptType, 'Unknown' as RptSpecialty
	),
	RptTypesWithDates As
	(
		select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,Result As
	(
		--
		-- Reports 11-15 (Total Active this month)
		--
		select RptID, R.Date1,count(distinct H.ProviderID)  ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join ProviderLoginDetails A on A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2
			left join #RptProviders H on H.ProviderID = A.ProviderID  
			where  R.RptID=11
			GROUP BY R.Date1,R.RptType,RptID

		union all
		
		select RptID, R.Date1,count(distinct H.ProviderID)  ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join ProviderLoginDetails A on A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2
			left join #RptProviders H on H.ProviderID = A.ProviderID  
			where  R.RptID > 11 AND R.RptID < 16 AND R.RptSpecialty = H.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty

		union all

		--
		-- Reports 6-10 (Total at end of month)
		--
		select RptID, R.Date1, count(distinct A.ProviderID) ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 
			where  R.RptID=6
			GROUP BY R.Date1,R.RptType,RptID

		union all

		select RptID, R.Date1, count(distinct A.ProviderID) ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 
			where  R.RptID > 6 AND R.RptID < 11 AND R.RptSpecialty = A.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty
			
		union all

		--
		-- Report 1-5 (New)
		--
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration >= R.Date1 and  A.DateOfRegistration < R.Date2
			where  R.RptID=1
			GROUP BY R.Date1,R.RptType,RptID

		union all
		
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration >= R.Date1 and  A.DateOfRegistration < R.Date2
			where  R.RptID > 1 AND R.RptID < 6 AND R.RptSpecialty = A.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty

		union all

		--
		-- Reports 16-20 (Total Spanish at end of month)
		--
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 and A.DefaultLanguageID=2
			where R.RptID=16
			GROUP BY R.Date1,R.RptType,RptID
			
		union all
		
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 and A.DefaultLanguageID=2
			where R.RptID > 16 AND R.RptID < 21 AND R.RptSpecialty = A.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty

	)


	--Select date1, RptType, ProviderCount, RptID, RptSpecialty,
	--	   'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
	--	into #ToBePivoted
	--	from Result
	--	order by date1
	
	Select R.date1, R.RptType, S.ProviderCount, R.RptID, R.RptSpecialty,
		   'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
		into #ToBePivoted
		from RptTypesWithDates R
			-- left join will force the creation of RptType
		left join Result S ON S.date1 = R.date1 AND S.RptType=R.RptType and S.RptSpecialty=R.RptSpecialty
		order by date1


	select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1


	DECLARE @MaxCount INTEGER
	DECLARE @Count INTEGER
	DECLARE @Txt VARCHAR(MAX)
	SET @Count = 1
	SET @Txt = '['
	SET @MaxCount = (SELECT MAX(RowID) FROM #c)
	WHILE @Count<=@MaxCount
		BEGIN
		IF @Txt!=''
			SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
		ELSE
			SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
		SET @Count=@Count+1
		END
	--SELECT @Txt AS Txt

	SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

	SET @Cols = @Txt
	/*
	SELECT  @cols = STUFF(( SELECT DISTINCT TOP 100 PERCENT
									'],[' + t.ColumnIndex
							FROM #ToBePivoted AS t
							ORDER BY '],[' + t.ColumnIndex
							FOR XML PATH('')
						  ), 1, 2, '') + ']'

	*/
	
	--SET @query = N'SELECT RptType, RptSpecialty, '+ @cols +' FROM
	--(SELECT RptType, RptSpecialty, ColumnINdex, ProviderCount,RptID FROM #ToBePivoted ) A 
	--PIVOT (SUM(ProviderCount) FOR ColumnIndex IN ( '+ @cols +' )) B  ;'
	
	-- PJB: Need to order by RptID because one of the RptType names has a symbol
	SET @query = N'with Pivoted as ( SELECT RptID, RptType, RptSpecialty, '+ @cols +' FROM
	(SELECT RptType, RptSpecialty, ColumnINdex, ProviderCount,RptID FROM #ToBePivoted ) A 
	PIVOT (SUM(ProviderCount) FOR ColumnIndex IN ( '+ @cols +' )) B  ) SELECT RptType, RptSpecialty, ' + @cols + ' FROM Pivoted Order By RptID'

	EXECUTE(@query)
	
	-- for debug
	-- SELECT * from #ToBePivoted order by Speciality, RptType
END
