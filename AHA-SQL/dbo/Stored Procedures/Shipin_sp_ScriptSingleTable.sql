﻿
--Shipin_sp_ScriptSingleTable 'Tradespacecommunities'

Create PROCedure [dbo].[Shipin_sp_ScriptSingleTable] 
(@tableName varchar(100)) as  
 
begin

	DECLARE ProcessTableColumns CURSOR FAST_FORWARD FOR  
	 
			SELECT column_name,data_type FROM information_schema.columns WHERE table_name = @tableName  

		    OPEN ProcessTableColumns  
					DECLARE @string nvarchar(max)   
					DECLARE @stringData nvarchar(max)   
					DECLARE @dataType nvarchar(1000) 
					DECLARE @colName nvarchar(1000)  
			    

			SET @string='INSERT INTO '+@tableName+'('  
			SET @stringData=''  

			FETCH NEXT FROM ProcessTableColumns INTO @colName,@dataType  
			  
					IF @@fetch_status<>0  
					begin  
						 close ProcessTableColumns  
						 deallocate ProcessTableColumns  
						 return  
					END  
			  
			WHILE @@FETCH_STATUS=0  
			BEGIN  

						IF @dataType in ('varchar','char','nchar','nvarchar','text','ntext')  
						BEGIN  
							 SET @stringData=@stringData+''''+'''+isnull('''''+'''''+'+
							 'replace(cast('+@colName+' as nvarchar(max))'+','+''''''''''+','+''''''''''''''+')'+
							'+'''''+''''',''NULL'')+'',''+'  
					
					
						END  

						ELSE   
						IF @dataType in ('datetime' ,'smalldatetime')
						BEGIN  
							SET @stringData=@stringData+'''convert(datetime,'+'''+isnull('''''+
							'''''+convert(varchar(200),'+@colName+',121)+'''''+''''',''NULL'')+'',121),''+'  
						END  
						ELSE   

						IF @dataType='image'   
					
						BEGIN

							SET @stringData=@stringData+''''+'''+isnull(cast(convert(varbinary(max),'+@colName+') 
							as nvarchar(max)),''NULL'')+'',''+'  
						END  
						ELSE   
						BEGIN  
							SET @stringData=@stringData+''''+'''+isnull('''''+
							'''''+convert(nvarchar(max),'+@colName+')+'''''+''''',''NULL'')+'',''+'  
						END  
					  
			SET @string=@string+@colName+','  
			FETCH NEXT FROM ProcessTableColumns INTO @colName,@dataType  
			END  



	
CLOSE ProcessTableColumns  
DEALLOCATE ProcessTableColumns  

	DECLARE @ssql nvarchar(MAX)  

		set @ssql=''



		if OBJECTPROPERTY(OBJECT_ID(@tableName),'TableHasIdentity')=1
		begin

			
			SET @ssql ='SELECT ' + '''SET IDENTITY_INSERT '+ ''+@tableName +''
						+'  ON  ''' + '   UNION ALL   '
		end
		 
				SET @ssql =@ssql+'SELECT '''+substring(@string,0,len(@string)) + ') VALUES(''+ ' + 
				substring(@stringData,0,len(@stringData)-2)+'''+'')'' FROM '+@tableName  

		if OBJECTPROPERTY(OBJECT_ID(@tableName),'TableHasIdentity')=1
		begin
			SET @ssql =@ssql + '   UNION ALL   '+ 'SELECT ' + '''SET IDENTITY_INSERT '+ ''+@tableName +''
						+'  OFF  ''' 
		end

   
		EXEC sp_executesql @ssql 

--print @ssql

end
 

