﻿


CREATE PROCEDURE [dbo].[Proc_FindPendingInvitationByInvitationCode]
(
  @InvitationCode nchar(6)
)
As
Begin
set nocount on


	select PPI.*
	 from  PatientProviderInvitation PPI left join PatientProviderInvitationDetails PPID
    on PPI.InvitationID = PPID.InvitationID where PPI.InvitationCode=@InvitationCode 
	AND status=0
	
End


