﻿

CREATE PROC [dbo].[proc_UpdateHealthRecord_LastActivityDate_PrimaryKey]
(
@UserHealthRecordID int
)
As
Begin

set nocount on


	Update dbo.HealthRecord set LastActivityDate=getdate()
	where UserHealthRecordID=@UserHealthRecordID

end


