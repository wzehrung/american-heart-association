﻿
CREATE Procedure [dbo].[Proc_DeleteAHAUserLoginDetails]
(
 @UserID int
)

As 

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-10  
   Description      :Delete AHAUserLoginDetails Record .. 
*/



delete from  dbo.AHAUserLoginDetails Where AHAUserID= @UserID 
End



