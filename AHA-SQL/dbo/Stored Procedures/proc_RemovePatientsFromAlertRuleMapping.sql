﻿
--proc_RemovePatientsFromAlertRuleMapping
CREATE proc [dbo].[proc_RemovePatientsFromAlertRuleMapping]
(
 @AlertRuleID int
,@PatientID nvarchar(max)
)

As

Begin

SET nocount on

SET @PatientID=@PatientID+','

;WITH PatientIDs(PatientID, Child)
	AS
	(
	SELECT
	SUBSTRING(@PatientID,1, CHARINDEX(',', @PatientID)-1) PatientID,
	SUBSTRING(@PatientID,CHARINDEX(',', @PatientID)+1, len(@PatientID)) Child
	UNION ALL
	SELECT
	SUBSTRING(Child,1, CHARINDEX(',', Child)-1) PatientID,
	SUBSTRING(Child,CHARINDEX(',', Child)+1, len(Child)) Child
	FROM PatientIDs
	WHERE
	CHARINDEX(',', Child) > 0
	)

Delete A from  AlertMapping A inner join PatientIDs B on
A.Patientid=B.PatientID and A.AlertRuleID=@AlertRuleID

End

