﻿
CREATE Procedure [dbo].[Proc_DeletePatientSMSReminderScheduleDay]
(
 @ReminderScheduleID Int
 ,@Day varchar(10)
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on
DECLARE @SQL VARCHAR(150)

if (@Day = 'Monday')
	BEGIN
		UPDATE [3CiPatientReminderSchedule] SET Monday = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID
	END	
if (@Day = 'Tuesday')
	BEGIN		
		UPDATE [3CiPatientReminderSchedule] SET Tuesday = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID
	END
if (@Day = 'Wednesday')
	BEGIN	
		UPDATE [3CiPatientReminderSchedule] SET Wednesday = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID
	END
if (@Day = 'Thursday')
	BEGIN			
		UPDATE [3CiPatientReminderSchedule] SET Thursday = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID
	END
if (@Day = 'Friday')			
	BEGIN
		UPDATE [3CiPatientReminderSchedule] SET Friday = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID
	END
if (@Day = 'Saturday')
	BEGIN			
		UPDATE [3CiPatientReminderSchedule] SET Saturday = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID
	END
if (@Day = 'Sunday')
	BEGIN	
		UPDATE [3CiPatientReminderSchedule] SET Sunday = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID
	END		

		--set status flag to zero if all days have been deleted		
		UPDATE [3CiPatientReminderSchedule] SET Status = 0 
		WHERE ReminderScheduleID = @ReminderScheduleID AND Monday = 0 AND Tuesday = 0 AND Wednesday = 0 AND Thursday = 0 
		AND Friday = 0 AND Saturday = 0 AND Sunday = 0

--SET @SQL = 'UPDATE [3CiPatientReminderSchedule] SET ' + @Day + ' = 0 '
--SET @SQL = @SQL + 'WHERE AND a.ReminderScheduleID = ' + CAST(@ReminderScheduleID AS varchar)

End
