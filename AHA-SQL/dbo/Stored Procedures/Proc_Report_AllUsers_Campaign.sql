﻿CREATE PROC [dbo].[Proc_Report_AllUsers_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@PartnerID int = null
)
As

Begin
DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

set nocount on

select * into #h from	
(select UserID,AHR.UserHealthRecordID,PGD.ProviderID from  AHAUserHealthRecord AHR
				inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID
				
				AND CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
				
				left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
				left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
) a

	;with RptTypes As
	(
	select 1 RptID,'1 Visit (All)' As RptType
	union all
	select 2 RptID,'2-4 Visits (All)' As RptType
	union all
	select 3 RptID,'5-10 Visits (All)' As RptType
	union all
	select 4 RptID,'>10 Visits (All)' As RptType
	union all
	select 5 RptID,'1 Visit (New)' As RptType
	union all
	select 6 RptID,'2-4 Visits (New)' As RptType
	union all
	select 7 RptID,'5-10 Visits (New)' As RptType
	union all
	select 8 RptID,'>10 Visits (New)' As RptType
	),
	RptTypesWithDates As
	(
	select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,EligibleLogins As --All
	(
	Select R.Date1,AHAUserID,Count(A.UserID) as Visits from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
	AHAUserLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2 
	inner join 
	(
	SELECT DISTINCT AHR.UserID FROM AHAUserHealthRecord AHR 
				inner join #h H on H.UserHealthRecordID=AHR.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)

--	INNER join #h H on H.UserHealthRecordID=AHR.UserHealthRecordID 
--	left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
--	left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID 
--	where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
--	AND isnull(PGD.ProviderID,-1) = isnull(isnull(@PartnerID, PGD.ProviderID),-1)
	) A ON A.UserID=ALD.AHAUserID  group by R.Date1,AHAUserID
	)
	,EligibleUsers As --new 
	(
	Select distinct A.UserID,R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
		inner join AHAUser A 
		on A.CreatedDate >=R.Date1 and A.CreatedDate <R.Date2 
		
		inner join #h H on H.UserID=A.UserID

										
	where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
	)
	,Visits As
	(
	select      E1.date1, 0 as AHAUserID, Case when Visits =1 then '1 Visit (All)' when Visits between 2 and 4 then '2-4 Visits (All)'
				when Visits between 5 and 10 then '5-10 Visits (All)'
				when Visits >10 then '>10 Visits (All)' End RptType from EligibleLogins E1
	union ALL
	Select R.Date1,ALD.AHAUserID
		,Case when count(AHAUserID) =1 then '1 Visit (New)' when count(AHAUserID) between 2 and 4 then '2-4 Visits (New)'
			  when count(AHAUserID) between 5 and 10 then '5-10 Visits (New)'
			  when count(AHAUserID) >10 then '>10 Visits (New)' End RptType 
			  
	   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
				AHAUserLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2  inner join  
				EligibleUsers E1 on E1.UserID=ALD.AHAUserID and E1.date1=R.date1
	group by R.Date1,AHAUserID
	)
	 
--,ToBePivoted As
--(
	Select RptID,R.date1,R.RptType,Count(V.RptType) UserCount 
	,'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
	,cast(dense_Rank() over (order by R.date1) as int) IntIndex
	into #ToBePivoted
	from RptTypesWithDates R left join Visits V
	on V.Date1=R.Date1 and V.RptType=R.RptType  group by R.date1,R.RptType,RptID
--)

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

--select * from #ToBePivoted
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'


SET @query = N'SELECT RptType, '+ @cols +' FROM
(SELECT RptType, ColumnINdex, UserCount,rptID FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;'

EXECUTE(@query)

	
--select 
--
-- RptType
--,[Column # 1] as [Column # 1] 
--,[Column # 2] as [Column # 2]
--,[Column # 3] as [Column # 3]
--,[Column # 4] as [Column # 4] 
--,[Column # 5] as [Column # 5]
--,[Column # 6] as [Column # 6]
--,[Column # 7] as [Column # 7]
--,[Column # 8] as [Column # 8] 
--,[Column # 9] as [Column # 9]
--,[Column # 10] as [Column # 10]
--,[Column # 11] as [Column # 11]
--,[Column # 12] as [Column # 12]
--
--
--from
--(
--select	RptType,ColumnIndex,UserCount,RptID
--		from ToBePivoted
--) A
--PIVOT
--(
--SUM (UserCount)
--FOR ColumnIndex IN
--( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
-- , [Column # 10],[Column # 11],[Column # 12] )
--) B
--order by RptID

End

/************************************************************************************************************************/

/****** Object:  StoredProcedure [dbo].[PROC_Report_AllUsersPercentage]    Script Date: 10/29/2012 20:31:31 ******/
SET ANSI_NULLS ON
