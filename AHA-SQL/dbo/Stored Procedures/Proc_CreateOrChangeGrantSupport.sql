﻿CREATE PROC [dbo].[Proc_CreateOrChangeGrantSupport]
(
 @Title nvarchar(128)
,@Description nvarchar(max)
,@Logo varbinary(max)
,@IsForProvider bit
,@ContentType nvarchar(10)
,@UpdatedByUserID INT
,@LanguageID int
,@GrantSupportID INT output
)

As

Begin
set nocount on

if exists (select * from GrantSupport where GrantSupportID=@GrantSupportID and LanguageID=@LanguageID)

exec Proc_UpdateGrantSupport

 @GrantSupportID 
,@Title 
,@Description 
,@Logo
,@IsForProvider 
,@ContentType 
,@UpdatedByUserID 
,@LanguageID 

else



exec Proc_CreateGrantSupport

 @Title 
,@Description 
,@Logo 
,@IsForProvider 
,@ContentType 
,@UpdatedByUserID 
,@LanguageID 
,@GrantSupportID  output




End


