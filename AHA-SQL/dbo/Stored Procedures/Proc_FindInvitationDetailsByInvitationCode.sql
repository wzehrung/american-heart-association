﻿
CREATE PROCEDURE [dbo].[Proc_FindInvitationDetailsByInvitationCode]
(
 @InvitationCode nchar(6)
)
As
Begin
set nocount on

dECLARE @InvitationID int
set @InvitationID =(select InvitationID from  PatientProviderInvitation where InvitationCode=@InvitationCode)

	select *
	 from  PatientProviderInvitationDetails where InvitationID=@InvitationID
	
End
