﻿
-- Proc_GetProviderTipsByProviderTipsID 24,1

CREATE PROC [dbo].[Proc_GetProviderTipsByProviderTipsID]
(
@ProviderTipsid int
,@LanguageID INT
)

As

Begin
set nocount on

Declare @ProviderTipsTypeids nvarchar(max)

select @ProviderTipsTypeids = COALESCE(@ProviderTipsTypeids + ',', '')+
cast (ProviderTipsTypeID   as Varchar(20))
from ProviderTipsMapping 
where  ProviderTipsid=@ProviderTipsid  and LanguageID=@LanguageID

select *,@ProviderTipsTypeids ProviderTipsTypeIDs from ProviderTips

Where ProviderTipsid=@ProviderTipsid AND LanguageID=@LanguageID

End
