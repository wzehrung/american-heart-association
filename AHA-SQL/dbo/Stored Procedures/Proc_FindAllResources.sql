﻿CREATE PROC [dbo].[Proc_FindAllResources]
(
@IsProvider bit,
@LanguageID int,
@ProviderID int = 0
)
As

Begin 
set nocount on

if (@ProviderID = 0) 
begin
    select * from Resources WHERE LanguageID=@LanguageID and IsProvider=@IsProvider and ProviderID is null ORDER BY SortOrder asc
end
else
begin
   select * from Resources WHERE LanguageID=@LanguageID and IsProvider=@IsProvider and ProviderID=@ProviderID ORDER BY SortOrder asc
end
END
