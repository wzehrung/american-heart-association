﻿


CREATE proc [dbo].[proc_PublishCampaign]
(
   @CampaignID int
  ,@LanguageID int
  ,@IsPublished bit OutPut
)
as
begin
	
	set @IsPublished=0
	
	if exists (select * from CampaignDetails where CampaignID=@CampaignID and LanguageID=@LanguageID)
	begin
	
	set @IsPublished=1
	Update CampaignDetails set isPublished=1 where CampaignID=@CampaignID and LanguageID=@LanguageID
	
	end
	else if (select count(*) from Languages)=(select count(*) from CampaignDetails where CampaignID=@CampaignID ) and @LanguageID is null
	begin
	set @IsPublished=1
    update CampaignDetails set IsPublished=1 where CampaignID=@CampaignID
    end
    
end 


