﻿
CREATE PROCEDURE [dbo].[Proc_CreateOrUpdate_MyProfile]
(
 @UserHealthRecordID int
,@LastSynchDate Datetime = NULL
,@Country  nvarchar(50)= NULL
,@Gender nvarchar(10) = NULL
,@Ethnicity nvarchar(50) = NULL
,@YearOfBirth int
,@ZipCode nvarchar(50) = NULL
,@PhoneContactCountryCodeID int
,@PhoneContact nvarchar (20)
,@PhoneMobileCountryCodeID int
,@PhoneMobile nvarchar (20) 
)

AS

BEGIN

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

IF EXISTS(
			  SELECT ExtendedProfileID FROM dbo.ExtendedProfile WHERE 
			  UserHealthRecordID=@UserHealthRecordID
		  )

UPDATE ExtendedProfile
	SET
		LastSynchDate				= @LastSynchDate,
		[Country/Region]			= @Country,
		Gender						= @Gender, 
		Ethnicity					= @Ethnicity,
		YearOfBirth					= @YearOfBirth,
		ZipCode						= @ZipCode,
		PhoneContactCountryCodeID	= @PhoneContactCountryCodeID,
		PhoneContact				= @PhoneContact,
		PhoneMobileCountryCodeID	= @PhoneMobileCountryCodeID,
		PhoneMobile					= @PhoneMobile

	WHERE UserHealthRecordID = @UserHealthRecordID										

ELSE 

INSERT ExtendedProfile(
		LastSynchDate,
		[Country/Region],
		Gender,
		Ethnicity, 
		YearOfBirth,
		ZipCode,
		PhoneContactCountryCodeID,
		PhoneContact,
		PhoneMobileCountryCodeID,
		PhoneMobile
)	 
SELECT
		@LastSynchDate,
		@Country,  
		@Gender,
		@Ethnicity ,
		@YearOfBirth,
		@ZipCode,
		@PhoneContactCountryCodeID,
		@PhoneContact,
		@PhoneMobileCountryCodeID,
		@PhoneMobile

DECLARE @PersonalItemGUID uniqueidentifier

SELECT PersonalItemGUID=@PersonalItemGUID
FROM dbo.HealthRecord
WHERE @UserHealthRecordID = UserHealthRecordID 
					 


EXEC proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID

COMMIT
END TRY

BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK	
			DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
			SELECT  @ErrMsg = ERROR_MESSAGE(),
					@ErrSeverity = ERROR_SEVERITY()
		RAISERROR(@ErrMsg, @ErrSeverity, 1)

	END CATCH

END
