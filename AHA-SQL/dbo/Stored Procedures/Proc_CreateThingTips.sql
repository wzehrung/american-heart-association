﻿
CREATE PROC [dbo].[Proc_CreateThingTips]  
(  
 @ThingTipsData nvarchar(max)  
,@ThingTipsTypeIds nvarchar(max)  
,@UpdatedByUserID INT  
,@LanguageID int 
,@ThingTipsID INT OUTPUT
)  
  
As  
  
Begin  
set nocount on  
  
IF @ThingTipsID   IS NULL OR @ThingTipsID=0
set @ThingTipsID=(select max(ThingTipsId)+1 from ThingTips )

insert ThingTips (ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,LanguageID)  
  
select @ThingTipsID,@ThingTipsData,@UpdatedByUserID,@UpdatedByUserID,@LanguageID  
  
--Declare @ThingTipsID int  
  
--set @ThingTipsID = Scope_Identity()  
  
set @ThingTipsTypeIds=@ThingTipsTypeIds+','  
  
  ;WITH Strings(Segments, Processing)  
  AS  
  (  
  SELECT  
     SUBSTRING(@ThingTipsTypeIds,1, CHARINDEX(',', @ThingTipsTypeIds)-1) Segments  
    ,SUBSTRING(@ThingTipsTypeIds,CHARINDEX(',', @ThingTipsTypeIds)+1, len(@ThingTipsTypeIds)) Processing  
  
    UNION ALL  
  
    SELECT  
     SUBSTRING(Processing,1, CHARINDEX(',', Processing)-1) Segments  
    ,SUBSTRING(Processing,CHARINDEX(',', Processing)+1, len(Processing)) Processing  
  
    FROM Strings  
  
    WHERE  
    CHARINDEX(',', Processing) > 0  
  )  

--select @ThingTipsID,Segments,@CreatedByUserID,@UpdatedByUserID,@LanguageID from Strings    
  
Insert ThingTipsMapping (ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,LanguageID)  
  
  
select @ThingTipsID,Segments,@UpdatedByUserID,@UpdatedByUserID,@LanguageID from Strings  
  
  
End  
  
  
  

