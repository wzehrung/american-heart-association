﻿CREATE PROC [dbo].[Proc_CreateOrChangePatientContent]
(
 
 @ZoneTitle nvarchar(128)
,@ContentTitle nvarchar(128)
,@ContentTitleUrl nvarchar(1128)
,@ContentText nvarchar(MAX)
,@UpdatedByUserID INT
,@LanguageID int
,@PatientContentID int 
,@ProviderID int=0
)
As
Begin

set nocount on


if exists (select * from PatientContent WHERE PatientContentID=@PatientContentID and LanguageID=@LanguageID)

exec 

Proc_UpdatePatientContent

 @PatientContentID 
,@ZoneTitle 
,@ContentTitle 
,@ContentTitleUrl 
,@ContentText 
,@UpdatedByUserID 
,@LanguageID 
,@ProviderID

else

begin

EXEC Proc_CreatePatientContent

 @ZoneTitle
,@ContentTitle 
,@ContentTitleUrl 
,@ContentText 
,@UpdatedByUserID 
,@LanguageID 
,@PatientContentID  
,@ProviderID

end

End
