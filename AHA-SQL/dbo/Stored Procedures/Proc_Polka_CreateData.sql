﻿
CREATE PROC [dbo].[Proc_Polka_CreateData]
(
 @PersonalItemGUID Uniqueidentifier
,@xmlPolkaData XML
)

As

Begin


Begin Try
Begin Transaction

	declare @UserHealthRecordID int

	set @UserHealthRecordID =(select UserHealthRecordID  from HealthRecord where PersonalItemGUID=@PersonalItemGUID)

	insert PolkaData

	(
	UserHealthRecordID,PolkaDataUniqueID,ItemType,PolkaData,DateCreated,IsUploadedToHV
	)


	--WeightLogItem

	select @UserHealthRecordID,C.D.value('@WTRecordID[1]','int'),'WeightLogItem',C.D.query('.'),getdate(),0

	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('WeightLogItem') C(D)

	union all

	--PhysicalActivityLogItem

	select @UserHealthRecordID,C.D.value('@PARecordID[1]','int'),'PhysicalActivityLogItem',C.D.query('.'),getdate(),0

	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('PhysicalActivityLogItem') C(D)

	union all

	--BloodGlucoseLogItem
	select @UserHealthRecordID,C.D.value('@BGRecordID[1]','int'),'BloodGlucoseLogItem',C.D.query('.'),getdate(),0

	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('BloodGlucoseLogItem') C(D)

	union all

	--BloodPressureLogItem
	select @UserHealthRecordID,C.D.value('@BPRecordID[1]','int'),'BloodPressureLogItem',C.D.query('.'),getdate(),0

	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('BloodPressureLogItem') C(D)

	union all
	--CholesterolLogItem
	select @UserHealthRecordID,C.D.value('@CHRecordID[1]','int'),'CholesterolLogItem',C.D.query('.'),getdate(),0

	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('CholesterolLogItem') C(D)

	union all
	--MedicationLogItem
	select @UserHealthRecordID,C.D.value('@MDRecordID[1]','int'),'MedicationLogItem',C.D.query('.'),getdate(),0

	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('MedicationLogItem') C(D)


COMMIT
End Try

Begin Catch

 If @@TRANCOUNT > 0
	ROLLBACK

	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
  
      
End


