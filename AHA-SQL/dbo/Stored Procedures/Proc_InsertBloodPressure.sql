﻿

CREATE Procedure [dbo].[Proc_InsertBloodPressure]
(
 @UserHealthRecordID int  
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Systolic int  
,@Diastolic int 
,@Pulse int 
,@ReadingSource nvarchar(100)
,@Comments nvarchar(max) 
,@DateMeasured datetime  
,@IrregularHeartbeat bit
)
AS

Begin


--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a record to BloodPressure
*/

Insert dbo.BloodPressure 
					(
						UserHealthRecordID
					   ,HVItemGuid
					   ,HVVersionStamp
					   ,Systolic   
					   ,Diastolic  
					   ,Pulse 
					   ,ReadingSource 
					   ,Comments 
					   ,DateMeasured   
					   ,IrregularHeartbeat                     
                     )
Select                  @UserHealthRecordID
                       ,@HVItemGuid
                       ,@HVVersionStamp
                       ,@Systolic   
					   ,@Diastolic  
					   ,@Pulse  
					   ,@ReadingSource
					   ,@Comments 
					   ,@DateMeasured      
					   ,@IrregularHeartbeat                  
End








