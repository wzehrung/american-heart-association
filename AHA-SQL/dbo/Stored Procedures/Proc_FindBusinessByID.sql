﻿
CREATE PROC [dbo].[Proc_FindBusinessByID]  
(  
	@BusinessID int
)  
  
AS  
  
BEGIN  
SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION
  
SELECT BusinessID, Name AS BusinessName, Comments FROM AdminBusiness
WHERE BusinessID = @BusinessID

COMMIT
END TRY

BEGIN CATCH

  IF @@TRANCOUNT > 0
	ROLLBACK

	
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT  @ErrMsg = ERROR_MESSAGE(),
			@ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

 END CATCH 
  
END  
  



