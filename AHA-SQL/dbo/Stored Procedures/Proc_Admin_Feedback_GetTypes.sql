﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.20.2014
-- Description:	Get feedback types used in the feedback form on the patient portal
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_GetTypes]
(
    @Enabled bit
)

AS

BEGIN
SET NOCOUNT ON

IF @Enabled = 0
    BEGIN
	   SELECT
		  FeedbackTypeID,
		  FeedbackTypeName,
		  [Enabled]

	   FROM FeedbackTypes
    END

ELSE
    BEGIN
	   SELECT
		  FeedbackTypeID,
		  FeedbackTypeName,
		  [Enabled]

	   FROM FeedbackTypes
	   WHERE [Enabled] = 1
    END
END