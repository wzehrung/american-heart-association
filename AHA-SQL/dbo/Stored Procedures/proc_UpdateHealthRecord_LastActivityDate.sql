﻿
CREATE PROC [dbo].[proc_UpdateHealthRecord_LastActivityDate]
(
@PersonalItemGUID uniqueidentifier
)
As
Begin

set nocount on

Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID

	Update dbo.HealthRecord set LastActivityDate=getdate()
	where UserHealthRecordID=@UserHealthRecordID

end


