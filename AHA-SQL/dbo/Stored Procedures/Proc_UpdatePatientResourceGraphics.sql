﻿  
  
CREATE PROC [dbo].[Proc_UpdatePatientResourceGraphics]  
(  
 @PatientResourceGraphicsID int  
,@Graphics varbinary(max)  
,@UpdatedByUserID INT 
,@LanguageID INT 
,@ImageURL nvarchar(255) 
)  
As  
  
Begin  
  
set nocount on  
  
if (@PatientResourceGraphicsID =0)
begin

set @PatientResourceGraphicsID=(select  ISNULL(MAX(PatientResourceGraphicsID), 0) +1 from PatientResourceGraphics )

INSERT INTO [dbo].[PatientResourceGraphics]
           ([PatientResourceGraphicsID]
           ,[Graphics]
           ,[Version]
           ,[CreatedByUserID]
           ,[UpdatedByUserID]
           ,[LanguageID]
           ,[ImageURL])
     VALUES
           (@PatientResourceGraphicsID
           ,@Graphics
           ,0
           ,@UpdatedByUserID
           ,@UpdatedByUserID
           ,@LanguageID
		   ,@ImageURL)
end

else
begin
update PatientResourceGraphics set Graphics=@Graphics,Version=Version+1,UpdatedByUserID=@UpdatedByUserID,ImageURL=@ImageURL  
  where PatientResourceGraphicsID=@PatientResourceGraphicsID and LanguageID=@LanguageID
end 
  
End  
  
