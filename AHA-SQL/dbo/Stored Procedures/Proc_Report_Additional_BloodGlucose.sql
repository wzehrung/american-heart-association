﻿--Proc_Report_Additional_BloodGlucose '2010.002.01'
CREATE PROC [dbo].[Proc_Report_Additional_BloodGlucose]
(
 @StartDate datetime
,@EndDate datetime
,@ProviderID int
,@campaignID int
)

AS

Begin

   

declare @startUserHealthrecordIDs table
	(
	UserHealthrecordID int
	primary key (UserHealthrecordID)
	)

		if @ProviderID is not null

		insert @startUserHealthrecordIDs
		select  BG.UserHealthrecordID from 

		   PatientGroupDetails PGD 
			inner join 
			PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID  and PGD.ProviderID =@providerID
			inner join HealthRecord HR on HR.UserHealthRecordID=PGM.PatientID and (HR.CampaignID=@campaignID or @campaignID is null)
			inner join BloodGlucose  BG  on BG.UserHealthrecordID=HR.UserHealthRecordID
			where DateMeasured >=@StartDate and DateMeasured<dateadd(month,1,@StartDate) and    readingtype='fasting' 
			group by BG.UserHealthrecordID
		else
			insert @startUserHealthrecordIDs
			select  BG.UserHealthrecordID from BloodGlucose  BG
			inner join HealthRecord HR on HR.UserHealthRecordID=BG.UserHealthRecordID and (HR.CampaignID=@campaignID or @campaignID is null)

			where DateMeasured >=@StartDate 
			and DateMeasured<dateadd(month,1,@StartDate) and    readingtype='fasting' 
			group by BG.UserHealthrecordID




	;with BloodGlucoseIds As
	(
	 select max(BloodGlucoseId) BloodGlucoseId,Date1  from BloodGlucose BG inner join 
	 @startUserHealthrecordIDs shi on shi.UserHealthrecordID=BG.UserHealthrecordID
	 inner join  dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) D on
	 BG.DateMeasured>=D.Date1 and  BG.DateMeasured<D.Date2 	  where  readingtype='fasting' 
	 group by BG.UserHealthrecordID,D.Date1
    )

	select 

	UDR.Date1 
	,sum(Case when BloodGlucoseValue < 100  then 1 else 0 end) Healthy
	,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when BloodGlucoseValue < 100 then 1 else 0 end)*1.0/(select count(*) from @startUserHealthrecordIDs) end *100 HealthyPerc

	,sum(Case when BloodGlucoseValue BETWEEN 100 AND 125 then 1 else 0 end) Acceptable

	,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when BloodGlucoseValue BETWEEN 100 AND 125 then 1 else 0 end)*1.0/(select count(*) from @startUserHealthrecordIDs) end *100 AcceptablePerc

	,sum(Case when BloodGlucoseValue >=126  then 1 else 0 end ) AtRisk
	      
	,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when BloodGlucoseValue >=126 then 1 else 0 end) *1.0 /(select count(*) from @startUserHealthrecordIDs) end	*100 AtRiskPerc

	,(select count(*) from @startUserHealthrecordIDs)-
	(
	 sum(Case when BloodGlucoseValue < 100  then 1 else 0 end)
	+sum(Case when BloodGlucoseValue BETWEEN 100 AND 125 then 1 else 0 end)
	+sum(Case when BloodGlucoseValue >=126  then 1 else 0 end )
	)
	inactiveUsers
	
	,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else
	(
	(select count(*) from @startUserHealthrecordIDs)-
	(
	 sum(Case when BloodGlucoseValue < 100  then 1 else 0 end)
	+sum(Case when BloodGlucoseValue BETWEEN 100 AND 125 then 1 else 0 end)
	+sum(Case when BloodGlucoseValue >=126  then 1 else 0 end )
	)
	)		
	*1.0 
	/(select count(*) from @startUserHealthrecordIDs) end	*100 inactiveUsersPerc
	
	from dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) UDR left join 
				 
	 (select Date1,BloodGlucoseValue from BloodGlucose BG   inner join BloodGlucoseIds BGID on BG.BloodGlucoseId=BGID.BloodGlucoseId)
	 
	 BG on BG.Date1=UDR.Date1 
	 
				 
	group by 	UDR.Date1 		
	
	
End
