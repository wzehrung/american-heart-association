﻿CREATE PROC [dbo].[Proc_Report_Provider_AllProviderPercentage_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@UserTypeID int = null
,@PartnerID int = NULL
)
As

Begin
set nocount on


DECLARE @cols NVARCHAR(2000)
DECLARE @ColsConv NVARCHAR(2000) -- NEW
DECLARE @query NVARCHAR(4000)

	;with RptTypes As
	(
	select 1 RptID,'1 Visit' As RptType
	union all
	select 2 RptID,'2-4 Visits' As RptType
	union all
	select 3 RptID,'5-10 Visits' As RptType
	union all
	select 4 RptID,'>10 Visits' As RptType
	),
	RptTypesWithDates As
	(
	select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,EligibleLogins As
	(
	Select R.Date1,ald.ProviderID,Count(ald.ProviderID) as Visits from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
	ProviderLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2 
	inner join Provider H on H.ProviderID=ALD.ProviderID  
		and (H.UserTypeID=@UserTypeID or @UserTypeID IS NULL)
		and (H.ProviderID=@PartnerID or @PartnerID is null)
    where CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
	group by R.Date1,ALD.ProviderID
	)

	,Visits As
	(
	select      ProviderID,E1.date1,Case when Visits =1 then '1 Visit' when Visits between 2 and 4 then '2-4 Visits'
				when Visits between 5 and 10 then '5-10 Visits'
				when Visits >10 then '>10 Visits' End RptType from EligibleLogins E1
	)
	,VisitsFinal As
	(
	select  R.RptID,R.date1,R.RptType,Count(V.RptType) UserCount,1 AS ID  from RptTypesWithDates R left join Visits V
	on V.Date1=R.Date1 and V.RptType=R.RptType   
	group by R.date1,R.RptType, R.RptID
	)
	,TotalVisits As
	(
	select SUM(Usercount)  TotalUserCount,Date1  from
	VisitsFinal group by date1
	)

	Select RptID,N.date1,N.RptType,
	isnull(case when TotalUserCount=0 then 0 else cast(userCount as real)*100/cast (TotalUserCount as real) end,0) as Perc
	,'Column # ' + cast(dense_Rank() over (order by N.date1) as varchar(10)) ColumnIndex
	,cast(dense_Rank() over (order by N.Date1) as int) IntIndex -- NEW
    into #ToBePivoted
	from VisitsFinal N inner join TotalVisits T
	on N.date1=T.date1 

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'



SET @query = N'SELECT RptType, '+ @colsconv +' FROM
(SELECT RptType, ColumnIndex, round(Perc,2) AS Perc, RptID FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B;'

EXECUTE(@query)
		
end

--SET @query = N'SELECT RptType, '+ @cols +' FROM
--(SELECT RptType, ColumnINdex, round(Perc,2) as perc,RptID FROM #ToBePivoted ) A
--PIVOT (SUM(perc) FOR ColumnIndex IN ( '+ @cols +' )) B;'

--SELECT DISTINCT ColumnIndex INTO #ColumnsIndex FROM #ToBePivoted

--select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1

--DECLARE @MaxCount INTEGER
--DECLARE @Count INTEGER
--DECLARE @Txt VARCHAR(MAX)
--SET @Count = 1
--SET @Txt = '['
--SET @MaxCount = (SELECT MAX(RowID) FROM #c)
--WHILE @Count<=@MaxCount
--    BEGIN
--    IF @Txt!=''
--        SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
--    ELSE
--        SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
--    SET @Count=@Count+1
--    END
--SELECT @Txt AS Txt

--SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

--SET @Cols = @Txt
/*
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t

                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
*/
--SET @query = N'SELECT RptType, '+ @cols +' FROM
--(SELECT RptType, ColumnINdex, round(Perc,2) as perc,RptID FROM #ToBePivoted ) A
--PIVOT (SUM(perc) FOR ColumnIndex IN ( '+ @cols +' )) B;'
 
--execute(@query)

--End
