﻿-- Proc_Report_Tracking '2011.01.01','2011.02.28',null

CREATE Procedure [dbo].[Proc_Report_Tracking_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@PartnerID int = null
)

As

Begin
DECLARE @cols NVARCHAR(4000)
DECLARE @ColsConv NVARCHAR(4000)
DECLARE @query NVARCHAR(4000)
		
		select * into #h from	
(select UserID,AHR.UserHealthRecordID,PGD.ProviderID from  AHAUserHealthRecord AHR
				inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID
				
				AND CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
				
				left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
				left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
) a
	

		;With Tracker As
		(
			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Weight' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Weight W 
			on W.CreatedDate<D.Date2 
			
			left join #h H on W.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Blood Pressure' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodPressure BP 
			on BP.CreatedDate<D.Date2 
			left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Physical Activity' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Exercise E
			on E.CreatedDate<D.Date2
			
			left join #h H on E.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
			GROUP BY Date1


			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Medication' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Medication M
			on M.CreatedDate<D.Date2 
			left join #h H on M.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
				
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Cholesterol' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Cholesterol C 
			on C.CreatedDate<D.Date2 
			left join #h  H on C.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Blood Glucose' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodGlucose BG 
			on BG.CreatedDate<D.Date2 
			left join #h H on BG.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1
		)
		
		
		
		,
		UserCountP As

		(
		select D.Date1,H.UserHealthRecordID 
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Weight W 
			on W.CreatedDate<D.Date2 
			
			left join #h H on W.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
			

			union 

			select D.Date1,H.UserHealthRecordID 
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodPressure BP 
			on BP.CreatedDate<D.Date2 
			left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
		

			union 

			select D.Date1,H.UserHealthRecordID 
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Exercise E
			on E.CreatedDate<D.Date2
			
			left join #h H on E.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
	


			union 

			select D.Date1,H.UserHealthRecordID
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Medication M
			on M.CreatedDate<D.Date2 
			left join #h H on M.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
				
		

			union 

			select D.Date1,H.UserHealthRecordID
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Cholesterol C 
			on C.CreatedDate<D.Date2 
			left join #h H on C.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
		

			union 

			select D.Date1,H.UserHealthRecordID
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodGlucose BG 
			on BG.CreatedDate<D.Date2 
			left join #h H on BG.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			
		
		
		
		)
		,
		UserCount As

		(
		select Date1,count(distinct UserHealthRecordID) UserCount  from UserCountP group by Date1
		)


		Select T.Date1,Tracker,case when UserCount=0 then 0 else cast(TrackerCount as real)*100/cast (UserCount as real) end as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Tracker T inner join UserCount U on T.Date1=U.Date1


SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'


-- (size of @query was maxing out)
--SET @query = 
EXECUTE (N'SELECT Tracker, '+ @colsconv +' FROM
(SELECT Tracker, ColumnINdex, Perc FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B;')

--print '@colsconv ' + cast(len(@colsconv) as varchar(20))
--print '@cols ' + cast(len(@cols) as varchar(20))
--print '@query ' + cast(len(@query) as varchar(20))
--print @query
--EXECUTE(@query)
		
end
