﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.11.2014
-- Description:	Used by the admin portal to update a campaign to accept the @AffiliatedID variable.
--				Derived from original SP: Proc_UpdateCampaign.
--				Added _Admin to better group sp's.
-- =============================================

CREATE PROC [dbo].[Proc_Admin_UpdateCampaign]
(
    @CampaignID int,
    @Title nvarchar(1024),
    @Description nvarchar(max),
    @StartDate Datetime,
    @EndDate Datetime,
    @LanguageID int,
    @PromotionalTagLine nvarchar(MAX),
    @PartnerLogo varbinary(MAX),
    @PartnerLogoContentType nvarchar(10),
    @PartnerImage varbinary(MAX),
    @PartnerImageContentType nvarchar(10),
    @strRscTitle nvarchar(255),
    @strRscURL nvarchar(255),
    @BusinessID int,
    @AffiliateID nvarchar(32),
    @strImages nvarchar(32)
)

AS

BEGIN
SET NOCOUNT ON

UPDATE Campaign 
	SET
     StartDate		= @StartDate,
	EndDate		= @EndDate,
	UpdatedDate	= getdate(),
	BusinessID	= @BusinessID

WHERE CampaignID = @CampaignID 

IF EXISTS (SELECT * FROM CampaignDetails WHERE CampaignID = @CampaignID AND LanguageID = @LanguageID)  

UPDATE CampaignDetails 

SET
    [Title]				  = @Title,
    [Description]			  = @Description,
    [PromotionalTagLine]		  = @PromotionalTagLine,
    [PartnerLogo]			  = @PartnerLogo,
    [PartnerLogoContentType]	  = @PartnerLogoContentType,
    [PartnerImage]			  = @PartnerImage,
    [PartnerImageContentType]	  = @PartnerImageContentType,
    PartnerLogoVersion		  = case when @PartnerLogo is not null and ISNULL([PartnerLogo],CAST('1' AS VARBINARY))<>@PartnerLogo then  newid() else case when @PartnerLogo is not null then PartnerLogoVersion end end,
    PartnerImageVersion		  = case when @PartnerImage is not null and ISNULL([PartnerImage],CAST('1' AS VARBINARY))<>@PartnerImage then  newid() else case when @PartnerImage is not null then PartnerImageVersion end end,
    ResourceTitle			  = @strRscTitle,
    ResourceURL			  = @strRscURL,
    CampaignImages			  = @strImages

WHERE CampaignID = @CampaignID AND LanguageID = @LanguageID


IF @AffiliateID IS NOT NULL
BEGIN
    -- "clean the slate"
    DELETE FROM AffiliatesCampaigns WHERE CampaignID = @CampaignID
    -- Now, add the affiliates
    EXEC Proc_Admin_AssignAffiliateCampaign @AffiliateID, @CampaignID
END
ELSE
    -- This occurs when @AffiliateID = NULL (as in, when no affiliate is selected
    DELETE FROM AffiliatesCampaigns WHERE CampaignID = @CampaignID

END
