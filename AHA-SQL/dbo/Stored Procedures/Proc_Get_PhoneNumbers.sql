﻿

CREATE Procedure [dbo].[Proc_Get_PhoneNumbers]
(
 @UserHealthRecordID int  
)
AS

BEGIN

SELECT
	ep.UserHealthRecordID AS PatientID
	,ep.PhoneContactCountryCodeID
	,ep.PhoneContact
	,ep.PhoneMobileCountryCodeID
	,ep.PhoneMobile
	,pr.MobileNumber AS PhoneSMS
	,pr.TimeZone
	,pr.PinValidated
	,pr.Terms

FROM ExtendedProfile ep

	LEFT JOIN [3CiPatientReminder] pr
		ON ep.UserHealthRecordID = pr.PatientID

WHERE
	UserHealthRecordID = @UserHealthRecordID

END
