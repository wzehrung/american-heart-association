﻿


CREATE Procedure [dbo].[Proc_DropDiet]
(
@HVItemGuid uniqueidentifier  
)
AS
Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-21  
   Description      :Delete record of Diet by ItemGUid and HealthrecordID
*/

set nocount on

Begin Try
Begin Transaction


Delete from dbo.Diet  Where  HVItemGuid=@HVItemGuid 

Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.Diet
							 Where HVItemGuid=@HVItemGuid 

exec proc_UpdateHealthRecord_LastActivityDate_PrimaryKey @UserHealthRecordID

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End








