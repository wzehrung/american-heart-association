﻿
-- =============================================
-- Authors:		Phil Brock/Wade Zehrung
-- Create date:	2.17.2014
-- Updated date:	2.24.2014
-- Description:	Refactor of V6_Proc_Report_Provider_NewProviderVisits to add sub-items of
--				Cardiologists, Other HCPs, Volunteers and Unknown
--				Update: Removed campaign, market and usertype criteria
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Provider_NewProviderVisits_RollUp] 
(  
    @StartDate Datetime,  
    @EndDate Datetime
) 
AS
BEGIN
	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--
	-- Create yet another temp table containing all the providers who are in the given campaign(s) and markets
	--
	-- drop table #RptProviders
	SELECT P.ProviderID, P.DateOfRegistration, P.DefaultLanguageID, 
	       Case when P.Speciality = 'Cardiologist' then 'Cardiologists'
	            when P.Speciality = 'AHA Volunteer' then 'Volunteers'
	            when P.Speciality is not null AND P.Speciality != 'Cardiologist' AND P.Speciality != 'AHA Volunteer' then 'Other HCPs'
	            when P.Speciality is null then 'Unknown'
	        End ProvSpecialty
	    INTO #RptProviders
		FROM Provider P

	--
	-- Now generate the report
	--
	
    ;with RptTypes As
	(
		select 1 RptID,'1 Visit' As RptType, 'All' as RptSpecialty
		union all
		select 2 RptID,'1 Visit' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 3 RptID,'1 Visit' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 4 RptID,'1 Visit' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 5 RptID,'1 Visit' As RptType, 'Unknown' as RptSpecialty
		union all
		select 6 RptID,'2-4 Visits' As RptType, 'All' as RptSpecialty
		union all
		select 7 RptID,'2-4 Visits' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 8 RptID,'2-4 Visits' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 9 RptID,'2-4 Visits' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 10 RptID,'2-4 Visits' As RptType, 'Unknown' as RptSpecialty
		union all
		select 11 RptID,'5-10 Visits' As RptType, 'All' as RptSpecialty
		union all
		select 12 RptID,'5-10 Visits' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 13 RptID,'5-10 Visits' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 14 RptID,'5-10 Visits' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 15 RptID,'5-10 Visits' As RptType, 'Unknown' as RptSpecialty
		union all
		select 16 RptID,'>10 Visits' As RptType, 'All' as RptSpecialty
		union all
		select 17 RptID,'>10 Visits' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 18 RptID,'>10 Visits' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 19 RptID,'>10 Visits' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 20 RptID,'>10 Visits' As RptType, 'Unknown' as RptSpecialty
	),
	RptTypesWithDates As
	(
		select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,EligibleUsers As
	(
		Select ProviderID, R.Date1 from 
			DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner join  #RptProviders A on A.DateOfRegistration >=R.Date1 and A.DateOfRegistration <R.Date2 
	)
	,EligibleLogins As
	(
		Select R.Date1, ALD.ProviderID, Count(ALD.ProviderID) as Visits, H.ProvSpecialty
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner join ProviderLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2 
			inner join #RptProviders H on H.ProviderID=ALD.ProviderID  
			group by R.Date1,ALD.ProviderID, H.ProvSpecialty
	)
	,NewVisits As
	(
			-- get summaries by Visit
		select E1.date1, 
			   Case when Visits =1 then '1 Visit' 
					when Visits between 2 and 4 then '2-4 Visits'
					when Visits between 5 and 10 then '5-10 Visits'
					when Visits >10 then '>10 Visits' 
				End RptType,
				'All' as ProvSpecialty
			from EligibleUsers E1 
			inner join EligibleLogins E2 on E1.ProviderID = E2.ProviderID and E1.Date1 = E2.date1
			
		union all
			 -- get summaries by Visit and RptSpecialty
		select E1.date1, 
			   Case when Visits =1 then '1 Visit' 
					when Visits between 2 and 4 then '2-4 Visits'
					when Visits between 5 and 10 then '5-10 Visits'
					when Visits >10 then '>10 Visits' 
				End RptType,
				E2.ProvSpecialty
			from EligibleUsers E1 
			inner join EligibleLogins E2 on E1.ProviderID = E2.ProviderID and E1.Date1 = E2.date1
		
	)

	
	Select RptID, R.date1, R.RptType, R.RptSpecialty, Count(V.RptType) UserCount,
			'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
		into #ToBePivoted
		from RptTypesWithDates R
			-- left join will force the creation of RptType
		left join NewVisits V on V.Date1=R.Date1 and V.RptType=R.RptType and V.ProvSpecialty=R.RptSpecialty
		group by R.date1,R.RptType,RptID, R.RptSpecialty
		-- Order by RptID

	SELECT DISTINCT ColumnIndex INTO #ColumnsIndex FROM #ToBePivoted
 
	SELECT distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1

	DECLARE @MaxCount INTEGER
	DECLARE @Count INTEGER
	DECLARE @Txt VARCHAR(MAX)
	SET @Count = 1
	SET @Txt = '['
	SET @MaxCount = (SELECT MAX(RowID) FROM #c)
	WHILE @Count<=@MaxCount
		BEGIN
		IF @Txt!=''
			SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
		ELSE
			SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
		SET @Count=@Count+1
		END

	SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

	SET @Cols = @Txt

	-- PJB: Need to order by RptID because one of the RptType names has a symbol
	SET @query = N'with Pivoted as ( SELECT RptID, RptType, RptSpecialty, '+ @cols +' FROM
	(SELECT RptType, RptSpecialty, ColumnINdex, UserCount,RptID FROM #ToBePivoted ) A
	PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B ) SELECT RptType, RptSpecialty, ' + @cols + ' FROM Pivoted Order By RptID '
	 
	EXECUTE(@query) 
	
END
