﻿

--Proc_FetchContentinformation  'D,H,W'

CREATE Procedure [dbo].[Proc_FetchContentinformation]
(
 @AbbreviationString nvarchar(20)
,@LanguageID int
)

As

Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-30  
   Description      :Randomly selecting Content Information for each abbreviation
					
*/

set nocount on

Declare @Random table
(
 Url nvarchar(1024)
,Title  nvarchar(200)
,DidYouKnowText nvarchar(max)
,DidYouKnowLink nvarchar(max)
,abbreviation varchar(2)
)



set @AbbreviationString=@AbbreviationString+','

		;WITH Strings(Segments, Processing)
		AS
		(
		SELECT
				 SUBSTRING(@AbbreviationString,1, CHARINDEX(',', @AbbreviationString)-1) Segments
				,SUBSTRING(@AbbreviationString,CHARINDEX(',', @AbbreviationString)+1, len(@AbbreviationString)) Processing

				UNION ALL

				SELECT
				 SUBSTRING(Processing,1, CHARINDEX(',', Processing)-1) Segments
				,SUBSTRING(Processing,CHARINDEX(',', Processing)+1, len(Processing)) Processing

				FROM Strings

				WHERE
				CHARINDEX(',', Processing) > 0
		)

	   ,RandomSelection as
		(
		select		 
					Row_Number() over(partition by B.Abbreviation order by newid()) as Row
					,B.Abbreviation
					,A.DidYouKnowLinkUrl
					,A.Title
					,A.DidYouKnowText
					,A.DidYouKnowLinkTitle
										from  
										dbo.[Content] A inner join
										dbo.ContentRuleMapping C on
										A.ContentID=C.ContentID  and A.LanguageID=C.LanguageID
										inner join
										dbo.[ContentRules] B on
										 B.ContentRulesID=C.ContentRulesID and  B.LanguageID=C.LanguageID
                                         where A.LanguageID=@LanguageID  
									--where isnull(A.ExpirationDate,'9999.12.31')>=getdate()
					
		)

		,RandomSelectionAvoidDuplicate as
		(
		select	     A.DidYouKnowLinkUrl
					,A.Title
					,A.DidYouKnowText
					,A.DidYouKnowLinkTitle
					,max(A.Abbreviation) Abbreviation 
						from RandomSelection A 
						where A.Row <= 2 
					group by   

					A.DidYouKnowLinkUrl
					,A.Title
					,A.DidYouKnowText
					,A.DidYouKnowLinkTitle
	    )

		select  top 6 
					 A.DidYouKnowLinkUrl
					,A.Title
					,A.DidYouKnowText
					,A.DidYouKnowLinkTitle

		 from  RandomSelectionAvoidDuplicate A left join Strings B
				on A.Abbreviation=B.Segments
				order by case when ltrim(rtrim(@AbbreviationString)) = ',' 
				then cast(newid() as varchar(50))
				else B.Segments end desc


End




