﻿  
  
CREATE PROC [dbo].[Proc_GetPatientResourceGraphics] 
(
 @LanguageID int
) 
  
As  
  
Begin  
  
set nocount on  
  
select * from PatientResourceGraphics where LanguageID=@LanguageID
  
  
End