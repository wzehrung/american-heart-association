﻿

CREATE Procedure [dbo].[Proc_ModifyAHAUser_IsHealthHistoryCompleted]  
(  
 @PersonGUID uniqueidentifier
,@IsHealthHistoryCompleted bit
)
AS  
  
Begin  

/* Author			:Shipin Anand  
   Created Date     :2008-07-04    
   Description      :Updating IsHealthHistoryCompleted Column of AHAUser for a PersonGUID  

   Note : Please dont try to update a indexed foreign key 
*/  

set nocount on  

Update dbo.AHAUser  
                SET IsHealthHistoryCompleted =@IsHealthHistoryCompleted
				where PersonGUID=@PersonGUID 
End



