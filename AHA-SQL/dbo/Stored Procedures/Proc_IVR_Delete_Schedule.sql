﻿CREATE PROCEDURE [dbo].[Proc_IVR_Delete_Schedule]

(
	@PatientID Int
)

AS

BEGIN

	IF EXISTS (SELECT * FROM IVRPatientReminder WHERE PatientID=@PatientID)  
	
		DELETE IVRPatientReminder 
				
		WHERE PatientID = @PatientID

END