﻿



CREATE Procedure [dbo].[Proc_CreateOrChangeBloodPressureGoal]
(
 @PersonalItemGUID uniqueidentifier
,@HVItemGuid uniqueidentifier 
,@HVVersionStamp uniqueidentifier 
,@TargetSystolic int 
,@TargetDiastolic int 
)

AS
Begin

/* 

   Author               :Shipin Anand
   Created Date         :2008-07-21 

   Description			:modification/insertion (BloodPressureGoal table )

						Rule : if exists same Itemguid,HealthGuid and
						VersionGUID then Update the table
						else  Insert a new entry

*/

set nocount on

Begin Try
Begin Transaction

                         

if exists    (
              select BloodPressureGoalID from dbo.BloodPressureGoal where
              HVItemGuid=@HVItemGuid 
              )

Exec Proc_UpdateBloodPressureGoal		 @HVItemGuid  
										,@HVVersionStamp  
										,@TargetSystolic 
										,@TargetDiastolic 
										

else

Begin

Declare @UserHealthRecordID int
select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
                             where PersonalItemGUID=@PersonalItemGUID
                                 



Exec Proc_InsertBloodPressureGoal		 @UserHealthRecordID
										,@HVItemGuid  
										,@HVVersionStamp 
										,@TargetSystolic 
										,@TargetDiastolic 
										

End

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End






