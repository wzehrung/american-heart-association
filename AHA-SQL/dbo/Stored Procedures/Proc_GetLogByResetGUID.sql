﻿
CREATE PROCEDURE [dbo].[Proc_GetLogByResetGUID]
(
 @ResetGUID uniqueidentifier
)
As
Begin
set nocount on

	select * from ResetPasswordLog WHERE ResetGUID=@ResetGUID
	
End
