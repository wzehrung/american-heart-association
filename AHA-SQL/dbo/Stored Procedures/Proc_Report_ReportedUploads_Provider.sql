﻿-- Proc_Report_Tracking '2011.01.01','2011.02.28',null

CREATE Procedure [dbo].[Proc_Report_ReportedUploads_Provider]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
,@UserTypeID int = null
,@PartnerID int = null
)

As

Begin
DECLARE @cols NVARCHAR(2000)
DECLARE @ColsConv NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)
DECLARE @I INT
		select * into #h from	
(select UserID,AHR.UserHealthRecordID,PGD.ProviderID from  AHAUserHealthRecord AHR
				inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID  and  isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
				left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
				left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
				inner join Provider p on p.providerid = pgd.providerid and (p.UserTypeID = @UserTypeID OR p.UserTypeID IS NULL)

) a

	
;With Tracker As
		(

			Select TrackerCount, Date1, 'Weight' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(W.UserHealthRecordID) as tracker,
			R.Date1
			--, CampaignID, PGD.ProviderID
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Weight W 
				on W.CreatedDate >=R.Date1 and W.CreatedDate <R.Date2 
				left join #h H on H.UserHealthRecordID=W.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, W.USerHealthRecordID
			HAVING COUNT(W.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			LEFT JOIN weight  W
			on W.createddate >= R.Date1 and W.createddate < R.Date2
				left join #h H on H.UserHealthRecordID=W.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			having count(W.userhealthRecordid) =0 or count(W.Userhealthrecordid) =1
			) A

			union all
			
			Select TrackerCount, Date1, 'Blood Pressure' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(BP.UserHealthRecordID) as tracker,
			R.Date1
			--, CampaignID, PGD.ProviderID
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join BloodPressure BP 
				on BP.CreatedDate >=R.Date1 and BP.CreatedDate <R.Date2 
				left join #h H on H.UserHealthRecordID=BP.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, BP.USerHealthRecordID
			HAVING COUNT(BP.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			LEFT JOIN BloodPressure BP 
			on BP.createddate >= R.Date1 and BP.createddate < R.Date2
				left join #h H on H.UserHealthRecordID=BP.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			having count(BP.userhealthRecordid) =0 or count(BP.Userhealthrecordid) =1
			) A

			union all

			Select TrackerCount, Date1, 'Physical Activity' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(E.UserHealthRecordID) as tracker,
			R.Date1
			--, CampaignID, PGD.ProviderID
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Exercise E 
				on E.CreatedDate >=R.Date1 and E.CreatedDate <R.Date2 
				left join #h H on H.UserHealthRecordID=E.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, E.USerHealthRecordID
			HAVING COUNT(E.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			LEFT JOIN Exercise E 
			on E.createddate >= R.Date1 and E.createddate < R.Date2
				left join #h H on H.UserHealthRecordID=E.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			having count(E.userhealthRecordid) =0 or count(E.Userhealthrecordid) =1
			) A


			union all

			Select TrackerCount, Date1, 'Medication' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(M.UserHealthRecordID) as tracker,
			R.Date1
			--, CampaignID, PGD.ProviderID
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Medication M 
				on M.CreatedDate >=R.Date1 and M.CreatedDate <R.Date2 
				left join #h H on H.UserHealthRecordID=M.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, M.USerHealthRecordID
			HAVING COUNT(M.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			LEFT JOIN Medication M 
			on M.createddate >= R.Date1 and M.createddate < R.Date2
				left join #h H on H.UserHealthRecordID=M.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			having count(M.userhealthRecordid) =0 or count(M.Userhealthrecordid) =1
			) A

			union all

			Select TrackerCount, Date1, 'Cholesterol' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(C.UserHealthRecordID) as tracker,
			R.Date1
			--, CampaignID, PGD.ProviderID
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Cholesterol C 
				on C.CreatedDate >=R.Date1 and C.CreatedDate <R.Date2 
				left join #h H on H.UserHealthRecordID=C.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, C.USerHealthRecordID
			HAVING COUNT(C.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			LEFT JOIN Cholesterol C 
			on C.createddate >= R.Date1 and C.createddate < R.Date2
				left join #h H on H.UserHealthRecordID=C.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			having count(C.userhealthRecordid) =0 or count(C.Userhealthrecordid) =1
			) A

			union all

			Select TrackerCount, Date1, 'Blood Glucose' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(BG.UserHealthRecordID) as tracker,
			R.Date1
			--, CampaignID, PGD.ProviderID
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join BloodGlucose BG 
				on BG.CreatedDate >=R.Date1 and BG.CreatedDate <R.Date2 
				left join #h H on H.UserHealthRecordID=BG.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, BG.USerHealthRecordID
			HAVING COUNT(BG.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			LEFT JOIN BloodGlucose BG 
			on BG.createddate >= R.Date1 and BG.createddate < R.Date2
				left join #h H on H.UserHealthRecordID=BG.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			having count(BG.userhealthRecordid) =0 or count(BG.Userhealthrecordid) =1
			) A
		)
		

--		
--		,ToBePivoted As
--		(
select T.Date1,Tracker, TrackerCount as UserCount
		--,case when UserCount=0 then 0 else cast(TrackerCount as real)*100/cast (UserCount as real) end as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Tracker T 
		--inner join UserCount U on T.Date1=U.Date1
		--)

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

		--select * from #ToBePivoted

--SELECT  @colsConv = STUFF(( SELECT DISTINCT TOP 100 PERCENT
--                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
--                        FROM #ToBePivoted AS t
--                        ORDER BY '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
--                        FOR XML PATH('')
--                      ), 1, 2, '') + ']'

SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'


		select @i = count(*) from #h --where providerid = @PartnerID
        if(@i = 0)
        begin
         select 'NONE' as Tracker, '0.00%' as [Column # 1], '0.00%' as [Column # 2] where 1=1
--print @Cols
--SET @Query = N'Select ''NONE''as Tracker, '+@Cols + ' WHERE 1=1'
--EXECUTE(@Query)
        end
        else
        begin
SET @query = N'SELECT Tracker, '+ @cols +' FROM
(SELECT Tracker, ColumnINdex, UserCount FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'

EXECUTE(@query)
		
--		,ToBePivoted As
--		(
--		Select T.Date1,Tracker,case when UserCount=0 then 0 else cast(TrackerCount as real)*100/cast (UserCount as real) end as Perc
--			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
--		From Tracker T inner join UserCount U on T.Date1=U.Date1
--		)
--
--
--		select 
--
--		 Tracker
--		,cast(cast([Column # 1] as numeric(15,2)) as varchar(20))+'%'  [Column # 1]
--		,cast(cast([Column # 2] as numeric(15,2)) as varchar(20))+'%' as [Column # 2]
--		,cast(cast([Column # 3] as numeric(15,2)) as varchar(20))+'%' as [Column # 3]
--		,cast(cast([Column # 4] as numeric(15,2)) as varchar(20))+'%' as [Column # 4] 
--		,cast(cast([Column # 5] as numeric(15,2)) as varchar(20))+'%' as [Column # 5]
--		,cast(cast([Column # 6] as numeric(15,2)) as varchar(20))+'%' as [Column # 6]
--		,cast(cast([Column # 7] as numeric(15,2)) as varchar(20))+'%' as [Column # 7]
--		,cast(cast([Column # 8] as numeric(15,2)) as varchar(20))+'%' as [Column # 8] 
--		,cast(cast([Column # 9] as numeric(15,2)) as varchar(20))+'%' as [Column # 9]
--		,cast(cast([Column # 10] as numeric(15,2)) as varchar(20))+'%' as [Column # 10]
--		,cast(cast([Column # 11] as numeric(15,2)) as varchar(20))+'%' as [Column # 11]
--		,cast(cast(ISNULL([Column # 12], 0) as numeric(15,2)) as varchar(20))+'%' as [Column # 12]
--
--
--
--		from
--		(
--		select	Tracker,ColumnIndex,Perc
--				from ToBePivoted
--		) A
--		PIVOT
--		(
--		SUM (Perc)
--		FOR ColumnIndex IN
--		( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
--		 , [Column # 10],[Column # 11],isnull([Column # 12], 0) AS [Column # 12] )
--		) B
  end
End
/************************************************************************************************************************/
