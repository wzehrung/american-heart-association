﻿






CREATE proc [dbo].[Proc_UpdateAlertRule]
(
 @PatientID INT
,@GroupID INT
,@AlertData XML
,@Type NVARCHAR(128)
,@SendMessageToPatient bit
,@MessageToPatient nvarchar(max)
,@AlertRuleID int 
)

AS

Begin
set nocount on

Begin Try
Begin Transaction


declare @ProviderID int

select @ProviderID=ProviderID from AlertRule where AlertRuleID=@AlertRuleID


 SET @GroupID=	(SELECT	case when @GroupID = 0 
						then (select GroupID from PatientGroupDetails where ProviderID=@ProviderID and isDefault=1)
							else @GroupID end)


Update AlertRule 

set 

 Type					= @Type
,AlertData				= @AlertData
,SendMessageToPatient	= @SendMessageToPatient
,GroupID				= @GroupID
,MessageToPatient		= @MessageToPatient
where  AlertRuleID		= @AlertRuleID


delete from AlertMapping where AlertRuleID=@AlertRuleID

if @GroupID is not null

insert Alertmapping(AlertRuleID,PatientID,GroupID)
		SELECT @AlertRuleID,PatientID,GroupID FROM PatientGroupMapping
		 where GroupID= @GroupID

else

insert Alertmapping(AlertRuleID,PatientID,GroupID)
	select @AlertRuleID,@PatientID,@GroupID


--UPDATE AlertMapping SET GroupID = @GroupID where  AlertRuleID=@AlertRuleID


--declare @ProviderID int
--
--select @ProviderID=ProviderID from AlertRule where AlertRuleID=@AlertRuleID
--
--	IF  EXISTS (select * from Alert where AlertRuleID=@AlertRuleID)
--	begin
--
--		update AlertRule set isActive=0 where AlertRuleID=@AlertRuleID
--
--		exec Proc_CreateAlertRule  @ProviderID 
--		,@PatientID 
--		,@GroupID 
--		,@Type 
--		,@AlertData 
--		,@SendMessageToPatient
--		,@AlertRuleID  output
--
--	End
--
--	else
--	begin
--		delete from AlertMapping where AlertRuleID=@AlertRuleID
--		delete from Alert where AlertRuleID=@AlertRuleID
--		delete from AlertRule where AlertRuleID=@AlertRuleID
--		
--		exec Proc_CreateAlertRule  @ProviderID 
--		,@PatientID 
--		,@GroupID 
--		,@Type 
--		,@AlertData 
--		,@SendMessageToPatient
--		,@AlertRuleID  output
--
--    End

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch	

End





