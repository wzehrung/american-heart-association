﻿CREATE PROC [dbo].[PROC_Report_AllProviderVisits]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
)
As

Begin
set nocount on

	;with RptTypes As
	(
	select 1 RptID,'1 Visit' As RptType
	union all
	select 2 RptID,'2-4 Visits' As RptType
	union all
	select 3 RptID,'5-10 Visits' As RptType
	union all
	select 4 RptID,'>10 Visits' As RptType
	),
	RptTypesWithDates As
	(
	select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,EligibleLogins As
	(
	Select R.Date1,ald.ProviderID,Count(ald.ProviderID) as Visits from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
	ProviderLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2 
	inner join Provider H on H.ProviderID=ALD.ProviderID  where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
	group by R.Date1,ald.ProviderID
	)

	,Visits As
	(
	select      E1.date1,Case when Visits =1 then '1 Visit' when Visits between 2 and 4 then '2-4 Visits'
				when Visits between 5 and 10 then '5-10 Visits'
				when Visits >10 then '>10 Visits' End RptType from EligibleLogins E1
	)
	 
,ToBePivoted As
(
	Select RptID,R.date1,R.RptType,Count(V.RptType) UserCount 
	,'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
	from RptTypesWithDates R left join Visits V
	on V.Date1=R.Date1 and V.RptType=R.RptType  group by R.date1,R.RptType,RptID
)
	
select 

 RptType
,[Column # 1] as [Column # 1] 
,[Column # 2] as [Column # 2]
,[Column # 3] as [Column # 3]
,[Column # 4] as [Column # 4] 
,[Column # 5] as [Column # 5]
,[Column # 6] as [Column # 6]
,[Column # 7] as [Column # 7]
,[Column # 8] as [Column # 8] 
,[Column # 9] as [Column # 9]
,[Column # 10] as [Column # 10]
,[Column # 11] as [Column # 11]
,[Column # 12] as [Column # 12]


from
(
select	RptType,ColumnIndex,UserCount,RptID
		from ToBePivoted
) A
PIVOT
(
SUM (UserCount)
FOR ColumnIndex IN
( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
 , [Column # 10],[Column # 11],[Column # 12] )
) B

ORDER BY RptID

End



