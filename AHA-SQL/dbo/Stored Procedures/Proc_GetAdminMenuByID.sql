﻿CREATE PROC [dbo].[Proc_GetAdminMenuByID]
(
 @AdminUserID int
)

As

Begin
set nocount on
declare @RoleID int

SELECT @RoleID = roleid FROM AdminUser WHERE AdminUserID = @AdminUserID

IF (@RoleID = 1)
BEGIN
SELECT  
	am.MenuID,
	am.Name,
	am.Description,
	am.CategoryID,
	ar.enabled, 
	am.SortID

FROM AdminMenu am, AdminRolesMenu ar, AdminUser au  
 
WHERE  au.AdminUserID = @AdminUserID  
   and  ar.roleid = au.roleid
   and  ar.menuid = am.menuid

UNION
 SELECT 
  MenuID, Name, Description, 3, 1, SortID
 FROM AdminMenu where Name = 'Manage User Roles'
 ORDER BY SortID
END

ELSE

BEGIN
SELECT  
	am.MenuID,
	am.Name,
	am.Description,
	am.CategoryID,
	ar.enabled, 
	am.SortID

FROM AdminMenu am, AdminRolesMenu ar, AdminUser au  
 
WHERE  au.AdminUserID = @AdminUserID  
   and  ar.roleid = au.roleid
   and  ar.menuid = am.menuid

UNION

 SELECT 
  MenuID, Name, Description, 3, 0, SortID
 FROM AdminMenu where Name = 'Manage User Roles'
ORDER BY SortID
END
End

