﻿

CREATE PROCEDURE [dbo].[Proc_Insert_HbA1c]
(
 @UserHealthRecordID int  
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@HbA1cValue float  
,@DateMeasured datetime 
,@Notes nvarchar(MAX) 
,@ReadingSource nvarchar(100) 
)
AS

BEGIN

INSERT dbo.BloodGlucose_HbA1c
					(
                       UserHealthRecordID
                      ,HVItemGuid
                      ,HVVersionStamp
                      ,HbA1cValue
                      ,DateMeasured
					  ,CreatedDate
					  ,UpdatedDate
                      ,Notes  
					  ,ReadingSource                    
                    )
SELECT                 @UserHealthRecordID
                      ,@HVItemGuid
                      ,@HVVersionStamp
                      ,@HbA1cValue
                      ,@DateMeasured
					  ,GetDate()
					  ,GetDate()
                      ,@Notes
					  ,@ReadingSource
                     
END
