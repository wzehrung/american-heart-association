﻿CREATE Procedure [dbo].[Proc_UpdateBloodPressureGoal]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@TargetSystolic int 
,@TargetDiastolic int 
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand  
   Created Date     :2008-07-21    
   Description      :Updating records of BloodPressureGoal

   Note : Please dont try to update a indexed foreign key 
*/  

Update dbo.BloodPressureGoal

				   SET 
				   HVVersionStamp	= @HVVersionStamp
				  ,TargetSystolic	= @TargetSystolic 
				  ,TargetDiastolic	= @TargetDiastolic 
                  ,UpdatedDate		= GetDate()

                    
		Where HVItemGuid=@HVItemGuid 
					
End


	



