﻿CREATE PROC [dbo].[Proc_GetAdminReportByID]
(
 @AdminUserID int
)

As

Begin
set nocount on

SELECT  c.*,b.enabled FROM AdminUser a, AdminRolesReport b, AdminReport c
 where  a.AdminUserID = @AdminUserID 
   and  a.roleid = b.roleid
   and  b.Reportid = c.Reportid

End
