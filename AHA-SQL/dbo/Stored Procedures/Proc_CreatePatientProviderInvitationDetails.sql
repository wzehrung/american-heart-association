﻿


CREATE PROCEDURE [dbo].[Proc_CreatePatientProviderInvitationDetails]
(
 @InvitationID int
,@FirstName nvarchar(50)
,@LastName nvarchar(50)
,@Email nvarchar(50)
,@Status tinyint
,@InvitationDetailsID int output
)
As
Begin
set nocount on

insert PatientProviderInvitationDetails(InvitationID,FirstName,LastName,Email,Status)
select @InvitationID,@FirstName,@LastName,@Email,@Status

SET @InvitationDetailsID=SCOPE_IDENTITY()
	
End
