﻿


--[Proc_getAllCampaigns] 1
CREATE PROC [dbo].[Proc_getAllCampaigns]
(
@LanguageID int
)
As
Begin

with camps as
(
select CampaignID,Title,row_number() over(partition by campaignid order by languageID) row  
from CampaignDetails where title is not null
)

	SELECT C. *,case when Title is null then (select title from camps cs where cs.CampaignID=C.CampaignID and row=1)  
	else Title end As Title , C.BusinessID, ab.Name AS BusinessName
	from campaign C left join CampaignDetails CD on C.CampaignID=CD.CampaignID
	and LanguageID=@LanguageID	

		LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

	where EndDate >= dateadd(year,-2,getdate()) and StartDate<=getdate() 
	order by Title

End	


