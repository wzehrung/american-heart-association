﻿

CREATE PROC [dbo].[Proc_FindAllExpiredCampaign]

(
@LanguageID int
)
As


Begin
set nocount on


select C.*,CD.LanguageID,CD.Description,CD.Title,CD.PromotionalTagLine,CD.PartnerLogoVersion,CD.PartnerImageVersion ,CD.IsPublished  
 , C.BusinessID, ab.Name AS BusinessName

from Campaign C left join CampaignDetails CD on C.CampaignID=CD.CampaignID and LanguageID=@LanguageID

	LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

where Convert(Datetime,Convert(Varchar,getdate(),103),103) not between StartDate and  EndDate 
order by startdate
End



