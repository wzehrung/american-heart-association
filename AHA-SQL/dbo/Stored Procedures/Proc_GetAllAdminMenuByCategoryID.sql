﻿CREATE PROC [dbo].[Proc_GetAllAdminMenuByCategoryID]

(
@CategoryID int
)
As

Begin
set nocount on

SELECT  a.*, '1' as Enabled FROM AdminMenu a 
WHERE CategoryID = @CategoryID

End
