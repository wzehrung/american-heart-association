﻿--Proc_Report_PatientsPerProvider  '2010.02.01','2010.08.01',null

CREATE Procedure [dbo].[Proc_Report_Provider_PatientsPerProvider_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@UserTypeID int = null
,@PartnerID int = NULL
)

As

Begin



DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

		;With RptTypes As
		(
		select 'Number Of Participants' as RptType,1 rptID
		UNION ALL
		select 'Number of Providers and/or Volunteers' as RptType,2 rptID
		--UNION ALL
		--select 'Average Number Of Patients Per Provider' as RptType,3 rptID
		)
		,RptTypesWithDates As
		(
		select D.Date1,R.RptType,rptID,D.date2   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D cross join RptTypes R
		)
		,PatientGroupMapping_LogCTE As
		(
		select max(PatientGroupMapping_LogID) PatientGroupMapping_LogID 
		from PatientGroupMapping_Log PGL  
					inner join HealthRecord H  on H.UserHealthRecordID=PGL.PatientID 
					AND CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
					group by PatientID,GroupID
		)	
				
		,Patients As
		(
		 select rptID,RptType,D.Date1,COUNT(DISTINCT PatientID) UserCount from RptTypesWithDates D 
																
		left join  (
					select Modifieddate,PatientID from PatientGroupMapping_Log PGL  
					inner join PatientGroupMapping_LogCTE B on PGL.PatientGroupMapping_LogID=B.PatientGroupMapping_LogID and PGL.Action=0
					)H  on --H.Modifieddate>=D.Date1  and 
					H.Modifieddate<D.Date2 					
					
		 where  rptID=1 
		 group by rptID,RptType,D.Date1
		)
		,Providers As
		( 
		 select rptID,RptType,D.Date1,COUNT(DISTINCT ProviderID) UserCount from RptTypesWithDates D left join 
		 (
		 select ProviderID,PGL.GroupID,PGL.Modifieddate,PGL.Action from 
		 PatientGroupMapping_LogCTE PGLC inner join PatientGroupMapping_Log PGL  
		 on PGL.PatientGroupMapping_LogID=PGLC.PatientGroupMapping_LogID and PGL.Action=0
		 inner join PatientGroupDetails PGD on PGD.GroupID=PGL.GroupID
		 inner join HealthRecord H  on H.UserHealthRecordID=PGL.PatientID
		 where CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
		 --CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
		 )PGL
		   on --PGL.Modifieddate>=D.Date1  and 
		   PGL.Modifieddate<D.Date2 	
		   where rptID=2
		 
		 group by rptID,RptType,D.Date1
		 
		)
	
	  ,Result As
		(
		select * from Patients
		union all
		select * from Providers
		union all
		
		select 3,'Average',A.date1
		,cast(cast(A.UserCount as numeric(10,2))/case when B.UserCount=0 then 1 else B.UserCount end as numeric(10,2))
		from Patients A inner join Providers B on A.date1=B.date1 --where A.rptid=1
		)


/*
		,ToBePivoted As
		(
		Select rptID,T.Date1,RptType,UserCount
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
		From Result T 
		)

		select 

		 RptType
		,case when rptid in (1,2) then substring(cast([Column # 1] as varchar(10)),0,charindex('.',cast([Column # 1] as varchar(10)))) 
		 else cast([Column # 1] as varchar(10)) end as [Column # 1] 
		,case when rptid in (1,2) then substring(cast([Column # 2] as varchar(10)),0,charindex('.',cast([Column # 2] as varchar(10)))) 
		 else cast([Column # 2] as varchar(10)) end as [Column # 2] 
		,case when rptid in (1,2) then substring(cast([Column # 3] as varchar(10)),0,charindex('.',cast([Column # 3] as varchar(10)))) 
		 else cast([Column # 3] as varchar(10)) end as [Column # 3] 
		,case when rptid in (1,2) then substring(cast([Column # 4] as varchar(10)),0,charindex('.',cast([Column # 4] as varchar(10)))) 
		 else cast([Column # 4] as varchar(10)) end as [Column # 4] 
		,case when rptid in (1,2) then substring(cast([Column # 5] as varchar(10)),0,charindex('.',cast([Column # 5] as varchar(10)))) 
		 else cast([Column # 5] as varchar(10)) end as [Column # 5] 
		,case when rptid in (1,2) then substring(cast([Column # 6] as varchar(10)),0,charindex('.',cast([Column # 6] as varchar(10)))) 
		 else cast([Column # 6] as varchar(10)) end as [Column # 6] 
		,case when rptid in (1,2) then substring(cast([Column # 7] as varchar(10)),0,charindex('.',cast([Column # 7] as varchar(10)))) 
		 else cast([Column # 7] as varchar(10)) end as [Column # 7] 
		,case when rptid in (1,2) then substring(cast([Column # 8] as varchar(10)),0,charindex('.',cast([Column # 8] as varchar(10)))) 
		 else cast([Column # 8] as varchar(10)) end as [Column # 8] 
		,case when rptid in (1,2) then substring(cast([Column # 9] as varchar(10)),0,charindex('.',cast([Column # 9] as varchar(10)))) 
		 else cast([Column # 9] as varchar(10)) end as [Column # 9] 
		,case when rptid in (1,2) then substring(cast([Column # 10] as varchar(10)),0,charindex('.',cast([Column # 10] as varchar(10)))) 
		 else cast([Column # 10] as varchar(10)) end as [Column # 10] 
		,case when rptid in (1,2) then substring(cast([Column # 11] as varchar(10)),0,charindex('.',cast([Column # 11] as varchar(10)))) 
		 else cast([Column # 11] as varchar(10)) end as [Column # 11] 
		,case when rptid in (1,2) then substring(cast([Column # 12] as varchar(10)),0,charindex('.',cast([Column # 12] as varchar(10)))) 
		 else cast([Column # 12] as varchar(10)) end as [Column # 12] 


      
		from
		(
		select	RptType,ColumnIndex,UserCount,rptID
				from ToBePivoted
		) A
		PIVOT
		(
		SUM (UserCount)
		FOR ColumnIndex IN
		( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
		 , [Column # 10],[Column # 11],[Column # 12] )
		) B
		ORDER BY rptID
			


*/

		Select rptID,T.Date1,RptType,UserCount
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
		into #ToBePivoted
		From Result T 
SELECT DISTINCT ColumnIndex INTO #ColumnsIndex FROM #ToBePivoted
 
--select substring(cast([UserCount] as varchar(10)),0,charindex('.',cast([UserCount] as varchar(10))))
--from #ToBePivoted

select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1

DECLARE @MaxCount INTEGER
DECLARE @Count INTEGER
DECLARE @Txt VARCHAR(MAX)
SET @Count = 1
SET @Txt = '['
SET @MaxCount = (SELECT MAX(RowID) FROM #c)
WHILE @Count<=@MaxCount
    BEGIN
    IF @Txt!=''
        SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
    ELSE
        SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
    SET @Count=@Count+1
    END
--SELECT @Txt AS Txt

SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

SET @Cols = @Txt
/*
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
--                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
 
*/
SET @query = N'SELECT RptType, '+ @cols +' FROM
(SELECT RptType, ColumnINdex, UserCount,RptID FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'
	

execute(@query)	


End
