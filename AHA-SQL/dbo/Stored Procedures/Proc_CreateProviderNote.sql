﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 06.02.2014
-- Description:	Adds a row into the ProviderNote table
-- =============================================
CREATE PROCEDURE [dbo].[Proc_CreateProviderNote]
(
@ProviderID int,
@PatientID int,
@CreationDate DateTime,
@Title nvarchar(128),
@Note nvarchar(max),
@ProviderNoteID int Output
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Begin Try
	Begin Transaction

	INSERT ProviderNote(ProviderID, PatientID, CreationDate, Title, Note)
		SELECT @ProviderID, @PatientID, @CreationDate,@Title,@Note

	SET @ProviderNoteID = SCOPE_IDENTITY()

	COMMIT
	End Try

	Begin Catch

	  If @@TRANCOUNT > 0
		ROLLBACK

	
		Declare @ErrMsg nvarchar(4000), @ErrSeverity int
		Select  @ErrMsg = ERROR_MESSAGE(),
			  @ErrSeverity = ERROR_SEVERITY()
		Raiserror(@ErrMsg, @ErrSeverity, 1)

	 End Catch
END

