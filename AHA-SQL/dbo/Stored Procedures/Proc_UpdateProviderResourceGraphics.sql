﻿

CREATE PROC [dbo].[Proc_UpdateProviderResourceGraphics]
(
 @ProviderResourceGraphicsID int
,@Graphics varbinary(max)
,@UpdatedByUserID INT
,@LanguageID int
,@ImageURL varchar(255)

)
As

Begin

set nocount on

if (@ProviderResourceGraphicsID =0)
begin

set @ProviderResourceGraphicsID=(select  ISNULL(MAX(ProviderResourceGraphicsID), 0) +1 from ProviderResourceGraphics )

INSERT INTO [dbo].[ProviderResourceGraphics]
           ([ProviderResourceGraphicsID]
           ,[Graphics]
           ,[Version]
           ,[CreatedByUserID]
           ,[UpdatedByUserID]
           ,[LanguageID]
		   ,[ImageURL])
     VALUES
           (@ProviderResourceGraphicsID
           ,@Graphics
           ,0
           ,@UpdatedByUserID
           ,@UpdatedByUserID
           ,@LanguageID
		   ,@ImageURL)
end

else
begin
update ProviderResourceGraphics set Graphics=@Graphics,Version=Version+1,UpdatedByUserID=@UpdatedByUserID, ImageURL=@ImageURL 

		where ProviderResourceGraphicsID=@ProviderResourceGraphicsID and LanguageID=@LanguageID
end

End


