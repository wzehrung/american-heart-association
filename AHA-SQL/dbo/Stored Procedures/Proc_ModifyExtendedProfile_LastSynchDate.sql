﻿


CREATE Procedure [dbo].[Proc_ModifyExtendedProfile_LastSynchDate]  
(  
 @PersonalItemGUID uniqueidentifier 
,@LastSynchDate Datetime
)
AS  
  
Begin  

/* Author			:Shipin Anand  
   Created Date     :2008-07-04    
   Description      :Updating LastSynchDate Column of ExtendedProfile for a UserHealthRecordGUID  

   Note : Please dont try to update a indexed foreign key 
*/  

set nocount on  

Begin Try
Begin Transaction

Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID	

Update dbo.ExtendedProfile
  
                SET  LastSynchDate = @LastSynchDate
				where UserHealthRecordID=@UserHealthRecordID 

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End





