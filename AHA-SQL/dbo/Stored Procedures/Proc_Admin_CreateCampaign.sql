﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.10.2014
-- Description:	Used by the admin portal to create a campaign to accept
--				the @AffiliatedID variable.
--				Derived from original SP: Proc_CreateCampaign.
--				Added _Admin to better group sp's.
-- =============================================

CREATE PROC [dbo].[Proc_Admin_CreateCampaign]
(
    @Title nvarchar(1024),
    @Description nvarchar(max), 
    @CampaignCode nvarchar(64),
    @Url nvarchar(2048), 
    @StartDate Datetime,  
    @EndDate Datetime,  
    @LanguageID int, 
    @CampaignID int,
    @PromotionalTagLine nvarchar(MAX),
    @PartnerLogo varbinary(MAX),
    @PartnerLogoContentType nvarchar(10),
    @PartnerImage varbinary(MAX),
    @PartnerImageContentType nvarchar(10),
    @strRscTitle nvarchar(255),
    @strRscURL nvarchar(255),
    @BusinessID int,
    @AffiliateID nvarchar(32),
    @strImages nvarchar(32)
)

AS

BEGIN
SET NOCOUNT ON
	
INSERT Campaign
(
    CampaignCode,
    Url,
    StartDate,
    EndDate,
    BusinessID
) 

SELECT
    @CampaignCode,
    @Url,
    @StartDate,
    @EndDate,
    @BusinessID

SET @CampaignID = SCOPE_IDENTITY()

INSERT CampaignDetails
(
    CampaignID,
    LanguageID,
    Description,
    Title,
    PromotionalTagLine,
    PartnerLogo,
    PartnerLogoContentType,
    PartnerImage,
    PartnerImageContentType,
    PartnerLogoVersion,
    PartnerImageVersion,
    ResourceTitle,
    ResourceURL,
    CampaignImages
)

SELECT
    @CampaignID,
    @LanguageID,
    @Description,
    @Title,
    @PromotionalTagLine,
    @PartnerLogo,
    @PartnerLogoContentType,
    @PartnerImage,
    @PartnerImageContentType,
    CASE 
	   WHEN @PartnerLogo IS NOT NULL 
	   THEN NEWID()
    END,
    CASE
	   WHEN @PartnerImage IS NOT NULL
	   THEN NEWID()
    END,
    @strRscTitle,
    @strRscURL,
    @strImages
--from languages B

IF @AffiliateID IS NOT NULL

    EXEC Proc_Admin_AssignAffiliateCampaign @AffiliateID, @CampaignID

END
