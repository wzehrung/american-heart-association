﻿

CREATE Procedure [dbo].[Proc_InsertWeightGoal]
(
 @UserHealthRecordID int 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@StartWeight float  
,@StartDate datetime 
,@TargetWeight float  
,@TargetDate datetime   
)
AS

Begin

--This Sp should not be called from within application/product  
  
/* Author			:Shipin Anand  
   Created Date     :2008-07-21    
   Description      :Insert Record to WeightGoal
*/  

Insert dbo.WeightGoal (
                       UserHealthRecordID  
					  ,HVItemGuid   
					  ,HVVersionStamp   
					  ,StartWeight   
					  ,StartDate  
					  ,TargetWeight   
					  ,TargetDate                
                    )
Select                 @UserHealthRecordID  
					  ,@HVItemGuid   
					  ,@HVVersionStamp   
					  ,@StartWeight   
					  ,@StartDate  
					  ,@TargetWeight   
					  ,@TargetDate    
End


