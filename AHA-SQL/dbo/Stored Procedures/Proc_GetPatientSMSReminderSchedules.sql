﻿
CREATE Procedure [dbo].[Proc_GetPatientSMSReminderSchedules]
(
 @ReminderID Int
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on


SELECT 	
ReminderScheduleID
,a.ReminderID AS ReminderID
,Message
,Monday
,Tuesday
,Wednesday
,Thursday
,Friday
,Saturday
,Sunday
,Time
,DisplayTime
,a.ReminderCategoryID AS ReminderCategoryID
,b.Name AS ReminderCategory
,c.TimeZone AS TimeZone
,b.GroupID as GroupID
FROM [3CiPatientReminderSchedule] a, [3CiPatientReminderCategory] b, [3CiPatientReminder] c
WHERE 
a.ReminderCategoryID = b.ReminderCategoryID
AND a.ReminderID = c.ReminderID
AND a.ReminderID = @ReminderID 
AND a.Status = 1


End


/**************************************************************************************************************************/


/****** Object:  StoredProcedure [dbo].[Proc_GetPatientSelectedReminderCategories]    Script Date: 09/26/2012 15:30:34 ******/
SET ANSI_NULLS ON
