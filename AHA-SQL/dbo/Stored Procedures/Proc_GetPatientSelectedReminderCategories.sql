﻿
CREATE Procedure [dbo].[Proc_GetPatientSelectedReminderCategories]
@ReminderID Int

AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-09-26 
   Description      :Returns all categories patient has setup reminders for
*/

select DISTINCT 
b.ReminderCategoryID as ReminderCategoryID
, b.Name AS ReminderCategory
, b.TriggerID as ReminderTriggerID
, b.GroupID as ReminderGroupID
from [3CiPatientReminderSchedule]  a, [3CiPatientReminderCategory] b
where a.ReminderCategoryID = b.ReminderCategoryID
AND a.ReminderID = @ReminderID
AND a.Status = 1


End



/****** Object:  StoredProcedure [dbo].[Proc_DeletePatientSMSReminderScheduleDay]    Script Date: 09/28/2012 07:59:04 ******/
SET ANSI_NULLS ON
