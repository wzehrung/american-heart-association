﻿CREATE PROC [dbo].[Proc_DeleteResource]
(
 @ResourceID int,
 @LanguageID int
)

As

Begin 
set nocount on

delete from Resources where ResourceID=@ResourceID and LanguageID=@LanguageID

END

