﻿CREATE PROC [dbo].[Proc_CreateActivityLog]
(
 @ActivityID int
,@ProviderID int
,@PatientID int
)
As
Begin
set nocount on

Insert  ActivityLog (ActivityID,ProviderID,PatientID)
 select @ActivityID ,@ProviderID ,@PatientID 

End

