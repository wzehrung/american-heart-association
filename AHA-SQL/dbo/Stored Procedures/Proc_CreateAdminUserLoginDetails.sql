﻿CREATE PROC [dbo].[Proc_CreateAdminUserLoginDetails]
(
 @AdminUserID int
,@AdminUserLoginDetailsID int output
)

As

Begin
set nocount on

Insert AdminUserLoginDetails (AdminUserID)
select @AdminUserID

set @AdminUserLoginDetailsID=scope_identity()

End
