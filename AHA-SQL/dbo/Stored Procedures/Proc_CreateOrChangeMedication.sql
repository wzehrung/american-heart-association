﻿



CREATE Procedure [dbo].[Proc_CreateOrChangeMedication]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier 
,@HVVersionStamp uniqueidentifier  
,@MedicationName nvarchar (255) 
,@MedicationType nvarchar (255) 
,@Dosage nvarchar(50)  
,@Strength nvarchar(50)  
,@Frequency nvarchar (50) 
,@StartDate Datetime
,@DateDiscontinued Datetime
,@Notes nvarchar(max)
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (Medication table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

set nocount on

Begin Try
Begin Transaction

if exists(
		  select MedicationID from dbo.Medication where 
		  HVItemGuid=@HVItemGuid 	
		  )

Exec Proc_UpdateMedication	 @HVItemGuid  
							,@HVVersionStamp   
							,@MedicationName 
							,@MedicationType 
							,@Dosage   
							,@Strength
							,@Frequency 
							,@StartDate 
							,@DateDiscontinued 
							,@Notes 
												

else 

Begin


Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							

Exec Proc_InsertMedication   @UserHealthRecordID 
							,@HVItemGuid  
							,@HVVersionStamp   
							,@MedicationName 
							,@MedicationType 
							,@Dosage   
							,@Strength
							,@Frequency  
							,@StartDate
							,@DateDiscontinued 
							,@Notes 
End

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

												
End










