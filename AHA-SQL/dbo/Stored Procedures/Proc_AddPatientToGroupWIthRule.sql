﻿



CREATE PROCEDURE [dbo].[Proc_AddPatientToGroupWIthRule]
(
 @GroupID int
,@PatientID nvarchar(max)
,@ProviderID int
)
As
Begin
set nocount on


Begin Try
Begin Transaction

declare @SplitOn char
set @SplitOn=','
set @PatientID=@PatientID+','

if @PatientID is null
begin
	delete from  PatientGroupMapping  where GroupID=@GroupID
	delete from alert where  alertruleid in
	(SELECT alertruleid from alertmapping where GroupID=@GroupID ) 
	delete from alertmapping   where GroupID=@GroupID
	
end
else

begin


if @GroupID is null
begin
select @GroupID=GroupID from PatientGroupDetails 
where ProviderID=@ProviderID and isDefault=1
end



	Declare @PatientIDs table
	(
	 PatientID int
	,GroupID int
	)
/*
	;WITH PatientIDs(PatientID, Child)
	AS
	(
	SELECT
	SUBSTRING(@PatientID,1, CHARINDEX(',', @PatientID)-1) PatientID,
	SUBSTRING(@PatientID,CHARINDEX(',', @PatientID)+1, len(@PatientID)) Child
	UNION ALL
	SELECT
	SUBSTRING(Child,1, CHARINDEX(',', Child)-1) PatientID,
	SUBSTRING(Child,CHARINDEX(',', Child)+1, len(Child)) Child
	FROM PatientIDs
	WHERE
	CHARINDEX(',', Child) > 0
	)

	insert @PatientIDs select PatientID,@GroupID from PatientIDs
*/

 While (Charindex(@SplitOn,@PatientID)>0)
	Begin
		insert @PatientIDs 
		Select 
			 ltrim(rtrim(Substring(@PatientID,1,Charindex(@SplitOn,@PatientID)-1))),@GroupID 
			 where ltrim(rtrim(Substring(@PatientID,1,Charindex(@SplitOn,@PatientID)-1)))<>0
		Set @PatientID = Substring(@PatientID,Charindex(@SplitOn,@PatientID)+1,len(@PatientID))
		
	End

	Declare @PatientGroupMappingPrimer  TABLE
	(
	 ExistingPatientID int
	,InputPatientID int
    )

	insert @PatientGroupMappingPrimer(ExistingPatientID,InputPatientID)
	Select A.PatientID As ExistingPatientID,B.PatientID As InputPatientID
	FROM @PatientIDs B full join (select * from PatientGroupMapping where GroupID=@GroupID)A  
	on A.PatientID=B.PatientID
	
	
	-- Delete

	delete from  PatientGroupMapping where PatientID in
		(select ExistingPatientID from @PatientGroupMappingPrimer where InputPatientID is null)
	and GroupID=@GroupID
	
	
	-- Insert

	insert PatientGroupMapping(GroupID,PatientID)
	select @GroupID,InputPatientID from @PatientGroupMappingPrimer where ExistingPatientID is null
	

Declare @AlertMappingPrimer TABLE
(
 ExistingPatientID int
,InputPatientID int
,alertRuleID int
)

;With AlertMappingPrimer As
(



select distinct A.PatientID,A.alertRuleID,A.groupID,0 isNew from 
AlertRule C 
inner join PatientGroupDetails P on C.ProviderID=P.ProviderID
and C.ProviderID=@ProviderID 
inner join  alertmapping A on A.AlertRuleID=C.AlertRuleID  
and A.GroupID=P.GroupID and P.GroupID=@GroupID
)

insert @AlertMappingPrimer(ExistingPatientID,InputPatientID,alertRuleID)

	select  A.PatientID,B.PatientID,A.alertRuleID from 
	AlertMappingPrimer A full join @PatientIDs   B on A.PatientID=B.PatientID
	where AlertRuleID is not null


--Insert 

insert alertmapping(AlertRuleID,PatientID,groupid)

select AlertRuleID,InputPatientID,@GroupID from
(select InputPatientID from @AlertMappingPrimer where ExistingPatientID is null) A
cross join (select AlertRuleID from @AlertMappingPrimer where InputPatientID is null) B


insert alertmapping(PatientID,groupid,AlertRuleID)

select PatientID,@GroupID GroupID,A.AlertRuleID from @PatientIDs
cross join  
(select A.AlertRuleID from alertrule A where ProviderID=@ProviderID AND GroupID=@GroupID
and not exists (select * from AlertMapping where A.AlertRuleID=AlertRuleID)) A


--Delete 

delete from alertmapping where PatientID in 
(select ExistingPatientID from @AlertMappingPrimer  where InputPatientID is null)
and GroupID=@GroupID



delete from alert where  PatientID in
	(select ExistingPatientID from @AlertMappingPrimer  where InputPatientID is null)



--
--delete from alert where  alertruleid in
--	(select alertRuleID from @AlertMappingPrimer  where InputPatientID is null) 

end


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch



End