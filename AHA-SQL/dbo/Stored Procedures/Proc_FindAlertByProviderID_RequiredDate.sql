﻿









CREATE proc [dbo].[Proc_FindAlertByProviderID_RequiredDate]
(
 @ProviderID INT 
,@RequiredDate dATEtIME
)

AS

Begin
set nocount on

select DISTINCT C.AlertID, C.PatientID, IsDismissed ,cast(AlertXml as nvarchar(max)) AlertXml
,cast(AlertRuleXml as nvarchar(max)) AlertData, CreatedDate, [type],Notes 
,ItemEffectiveDate 
from PatientGroupDetails A
inner join PatientGroupMapping B
on A.GroupID=B.GroupID and A.ProviderID=@ProviderID
INNER JOIN Alert C On B.PatientID=C.PatientID AND convert(datetime,convert(varchar,ItemEffectiveDate,103),103)=@RequiredDate
inner join AlertRule D on C.AlertRuleId=D.AlertRuleID-- AND ISACTIVE=1
and D.ProviderID = A.ProviderID
order by ItemEffectiveDate desc


End









