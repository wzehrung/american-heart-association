﻿CREATE PROC [dbo].[Proc_FindAllAnnouncements]
(
@LanguageID int,
@ProviderID int = 0
)
As

Begin 
set nocount on

if (@ProviderID = 0) 
begin
    select *, '' as ProviderName from PatientContent WHERE LanguageID=@LanguageID and ProviderID is null 
end
else
begin
    select pc.*, p.Salutation + ' ' + p.FirstName + ' ' + p.LastName AS ProviderName
    from PatientContent pc 
		INNER JOIN Provider p
			ON pc.ProviderID = p.ProviderID 
    where pc.providerid = @ProviderID
end
END
