﻿CREATE PROC [dbo].[Proc_DeleteAnnoucement]
(
 @AnnouncementID int,
 @LanguageID int
)

As

Begin 
set nocount on

delete from PatientContent where PatientContentID=@AnnouncementID and LanguageID=@LanguageID

END
