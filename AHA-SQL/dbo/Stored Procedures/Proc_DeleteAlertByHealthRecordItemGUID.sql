﻿

create proc [dbo].[Proc_DeleteAlertByHealthRecordItemGUID]
(
 @HealthRecordItemGUID uniqueidentifier 
)

AS

Begin
set nocount on

delete from Alert where HealthRecordItemGUID=@HealthRecordItemGUID
End

