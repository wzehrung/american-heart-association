﻿--[Proc_Report_Additional_BMI] '2010.08.01'
CREATE PROC [dbo].[Proc_Report_Additional_BMI]
(
 @StartDate datetime
,@EndDate datetime
,@ProviderID int
,@campaignID int
)

AS

Begin

	
declare @startUserHealthrecordIDs table
(
UserHealthrecordID int
primary key (UserHealthrecordID)
)





		if @ProviderID is not null

		insert @startUserHealthrecordIDs
		select  distinct H.UserHealthrecordID from 
		PatientGroupDetails PGD 
		inner join PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID  and PGD.ProviderID =@providerID
		inner join HealthRecord HR on HR.UserHealthRecordID=PGM.PatientID and (HR.CampaignID=@campaignID or @campaignID is null)
		inner join Height H     on H.UserHealthrecordID=HR.UserHealthRecordID
		where H.DateMeasured >=@StartDate and H.DateMeasured<dateadd(month,1,@StartDate)
		and exists(select * from Weight W where DateMeasured >=@StartDate and DateMeasured<dateadd(month,1,@StartDate) and H.UserHealthrecordID=W.UserHealthrecordID )

		else
		insert @startUserHealthrecordIDs
		select distinct H.UserHealthrecordID from Height H 
		inner join HealthRecord HR on HR.UserHealthRecordID=H.UserHealthRecordID and (HR.CampaignID=@campaignID or @campaignID is null)
		where H.DateMeasured >=@StartDate
		and H.DateMeasured<dateadd(month,1,@StartDate)
		and exists(select * from Weight W where DateMeasured >=@StartDate
		and DateMeasured<dateadd(month,1,@StartDate) and H.UserHealthrecordID=W.UserHealthrecordID )


	 
;with HeightIDs As
(
 select max(HeightID) HeightID, H.UserHealthrecordID,D.Date1  from Height H
 inner join @startUserHealthrecordIDs shi on shi.UserHealthrecordID=H.UserHealthrecordID
 inner join  dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) D on
 H.DateMeasured>=D.Date1 and  H.DateMeasured<D.Date2 
 group by H.UserHealthrecordID,D.Date1
 
 ),
 WeightIDs As
 (
 select max(WeightID) WeightID,W.UserHealthrecordID,D.Date1  from Weight  W
 inner join @startUserHealthrecordIDs shi on shi.UserHealthrecordID=W.UserHealthrecordID
 inner join  dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) D on
 W.DateMeasured>=D.Date1 and  W.DateMeasured<D.Date2 
 group by W.UserHealthrecordID,D.Date1
 )


	select 

	UDR.Date1  
	,sum(Case when((Weight*1.0/2.2) ) / ((Height) * (Height))<25 then 1 else 0 end) Healthy
	,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when((Weight*1.0/2.2) ) / ((Height) * (Height))<25 then 1 else 0 end)*1.0/count(*) end *100 HealthyPerc

	,sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) between 25 and 29.9  then 1 else 0 end) Acceptable

	,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) between 25 and 29.9  then 1 else 0 end)*1.0/count(*) end *100 AcceptablePerc

	,sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) >=30  then 1 else 0 end) AtRisk
	      
	,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) >=30  then 1 else 0 end) *1.0 /count(*) end	*100 AtRiskPerc

	
 ,(select count(*) from @startUserHealthrecordIDs)-
	(
	 sum(Case when((Weight*1.0/2.2) ) / ((Height) * (Height))<25 then 1 else 0 end)
	+sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) between 25 and 29.9  then 1 else 0 end)
	+sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) >=30  then 1 else 0 end)
	)
	inactiveUsers

 ,
  case when(select count(*) from @startUserHealthrecordIDs) =0 then 0 else
	(
	(select count(*) from @startUserHealthrecordIDs)-
	(
	 sum(Case when((Weight*1.0/2.2) ) / ((Height) * (Height))<25 then 1 else 0 end)
	+sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) between 25 and 29.9  then 1 else 0 end)
	+sum(Case when ((Weight*1.0/2.2) ) / ((Height) * (Height)) >=30  then 1 else 0 end)
	))*1.0 
	/(select count(*) from @startUserHealthrecordIDs) end	*100 inactiveUsersPerc
	

	from dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) UDR left join 
				 
				 (
				   select H.Date1,Weight ,Height  from Height BP  inner join HeightIDs H on BP.HeightID=H.HeightID
                  inner join WeightIDs W on W.UserHealthRecordID=H.UserHealthRecordID and H.Date1=W.Date1
                  inner join Weight WO on WO.WeightID=W.WeightID  
				 )
				 
				 C on C.Date1=UDR.Date1  
				 
				 
	group by 	UDR.Date1 		

End
