﻿





CREATE PROC [dbo].[PROC_MarkMessageAsReadOrUnRead]
(
 @MessageID int
,@UserType nvarchar(50) 
,@UserID int 
,@isRead bit 
)
As
Begin
set nocount on

declare @UserFolderMappingIDCurrent int

IF @UserType='Patient'
BEGIN

 select   @UserFolderMappingIDCurrent=UFM.UserFolderMappingID

	from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.PatientID=@UserID 
	inner join Messagedetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
	and MD.MessageID=@MessageID and MD.PatientID=UFM.PatientID

 Update MessageDetails set isRead=@isRead where MessageID=@MessageID and PatientID = @UserID and 
 UserFolderMappingID = @UserFolderMappingIDCurrent

END

ELSE IF @UserType='Provider'
BEGIN
select   @UserFolderMappingIDCurrent=UFM.UserFolderMappingID

	from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.ProviderID=@UserID 
	inner join Messagedetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
	and MD.MessageID=@MessageID and MD.ProviderID=UFM.ProviderID

Update MessageDetails set isRead=@isRead where MessageID=@MessageID and ProviderID = @UserID
and UserFolderMappingID = @UserFolderMappingIDCurrent

END




End



