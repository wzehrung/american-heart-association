﻿CREATE Procedure [dbo].[Proc_Report_Gender_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@PartnerID int = null
)

As

Begin
DECLARE @cols NVARCHAR(2000)
DECLARE @colsConv NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

		;With GenderBase As
		(
			select D.Date1, count(distinct AH.UserHealthRecordID) GenderCount,
			CASE WHEN E.Gender IS NULL OR E.Gender='Unknown' THEN 'Unreported' ELSE Gender END Gender
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D inner join    
			AHAUserHealthRecord AH on AH.CreatedDate<D.Date2 
			left join  ExtendedProfile E ON 
			E.UserHealthRecordID=AH.UserHealthRecordID   
			left join HealthRecord H on H.UserHealthRecordID=AH.UserHealthRecordID  
			left join PatientGroupMapping PGM on PGM.PatientID = AH.UserHealthRecordID
			left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID 
			
			where CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
			
			AND isnull(PGD.ProviderID,-1) = isnull(isnull(@PartnerID, PGD.ProviderID),-1)
			GROUP BY Date1,CASE WHEN Gender IS NULL OR Gender='Unknown' THEN 'Unreported' ELSE Gender END
		)
		,GenderTypes As
		(
		select 'Male' as GenderType,1 rptID
		union all
		select 'Female' as GenderType ,2 rptID
		union all
		select 'Unreported' as GenderType,3 rptID
		)
		,GenderDates As
		(
		select rptID,D.Date1,G.GenderType   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D cross join GenderTypes G
		)
		,Gender As
		(
		 select rptID,D.Date1,GB.GenderCount,D.GenderType Gender  from GenderDates D left join GenderBase GB on 
		 D.Date1=GB.date1 and D.GenderType=GB.Gender
		)

		,UserCount As
		(
		select Date1,sum(GenderCount) UserCount from Gender group by Date1

		)

		Select rptID,T.Date1,gender,ISNULL(case when UserCount=0 then 0 else cast(GenderCount as real)*100/cast (UserCount as real) end,0) as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Gender T inner join UserCount U on T.Date1=U.Date1

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'


SET @query = N'SELECT Gender, '+ @colsConv +' FROM
(SELECT Gender, ColumnINdex, Perc,rptID FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;'

EXECUTE(@query)   

End

/************************************************************************************************************************/
