﻿



CREATE Procedure [dbo].[Proc_CreateOrChangeExercise]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@DateofSession Datetime 
,@ExerciseType nvarchar(50) 
,@Intensity nvarchar(50)  
,@Duration float  
,@Comments nvarchar(max)
,@NumberOfSteps int
,@ReadingSource  nvarchar(100)
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (Weight table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

set nocount on
Begin Try
Begin Transaction

if exists(
		  select ExerciseID from dbo.Exercise where 
		  HVItemGuid=@HVItemGuid 
		  )

Exec Proc_UpdateExercise	 @HVItemGuid 
							,@HVVersionStamp 
							,@DateofSession 
							,@ExerciseType 
							,@Intensity 
							,@Duration 
							,@Comments
							,@NumberOfSteps
							,@ReadingSource
						

else 

begin

Declare @UserHealthRecordID int
select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							 

Exec Proc_InsertExercise     @UserHealthRecordID
							,@HVItemGuid 
							,@HVVersionStamp 
							,@DateofSession 
							,@ExerciseType 
							,@Intensity 
							,@Duration  
							,@Comments
							,@NumberOfSteps
							,@ReadingSource
end

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End

