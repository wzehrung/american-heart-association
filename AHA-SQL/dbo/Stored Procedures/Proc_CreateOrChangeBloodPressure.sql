﻿




CREATE Procedure [dbo].[Proc_CreateOrChangeBloodPressure]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Systolic int  
,@Diastolic int  
,@Pulse int 
,@ReadingSource nvarchar(100)
,@Comments nvarchar(max) 
,@DateMeasured datetime  
,@IrregularHeartbeat bit

)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (BloodPressure table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

set nocount on


Begin Try
Begin Transaction


if exists(
		  select BloodPressureID from dbo.BloodPressure where 
		  HVItemGuid=@HVItemGuid 
		  )

Exec Proc_UpdateBloodPressure	  @HVItemGuid 
								 ,@HVVersionStamp 
								 ,@Systolic 
								 ,@Diastolic 
								 ,@Pulse 
								 ,@ReadingSource
								 ,@Comments 
								 ,@DateMeasured 
								 ,@IrregularHeartbeat


else 
begin

Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							



Exec Proc_InsertBloodPressure	  @UserHealthRecordID 
								 ,@HVItemGuid 
								 ,@HVVersionStamp 
								 ,@Systolic 
								 ,@Diastolic 
								 ,@Pulse 
								 ,@ReadingSource
								 ,@Comments 
								 ,@DateMeasured 
								 ,@IrregularHeartbeat
end	


exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End










