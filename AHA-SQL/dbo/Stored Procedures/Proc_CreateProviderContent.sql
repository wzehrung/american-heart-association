﻿

CREATE PROC [dbo].[Proc_CreateProviderContent]
(
 @ZoneTitle nvarchar(50)
,@ContentTitle nvarchar(80)
,@ContentTitleUrl nvarchar(255)
,@ContentText nvarchar(MAX)
,@CreatedByUserID int
,@LanguageID int
,@ContentID int
)

As

Begin

IF @ContentID IS NULL OR @ContentID=0
set @ContentID=(select max(ContentID)+1 from ProviderContent )

insert ProviderContent
(
 ContentID
,ZoneTitle
,ContentTitle
,ContentTitleUrl
,ContentText
,CreatedByUserID
,UpdatedByUserID
,LanguageID
)

SELECT

 @ContentID
,@ZoneTitle 
,@ContentTitle 
,@ContentTitleUrl 
,@ContentText 
,@CreatedByUserID 
,@CreatedByUserID 
,@LanguageID 

End

