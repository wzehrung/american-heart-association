﻿create procedure [dbo].[Proc_UpdateLanguage_AdminUser]
(
	@LanguageID int,
	@AdminUserID int
)
as
begin
	update AdminUser set DefaultLanguageID = @LanguageID where AdminUserID = @AdminUserID
end 