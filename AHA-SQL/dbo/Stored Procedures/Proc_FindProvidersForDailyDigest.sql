﻿


CREATE PROC  [dbo].[Proc_FindProvidersForDailyDigest]
As

Begin

select P.*,Lan.LanguageLocale from Provider P
inner join Languages Lan on P.DefaultLanguageID = Lan.LanguageID
where P.IncludeAlertNotificationInDailySummary=1 or P.IncludeMessageNotificationInDailySummary = 1

End

