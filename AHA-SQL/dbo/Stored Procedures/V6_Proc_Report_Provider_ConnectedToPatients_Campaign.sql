﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 6.18.2013
-- Description:	Refactor of Proc_Report_Provider_ConnectedToPatients_Campaign for Campaign/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Provider_ConnectedToPatients_Campaign] 
(  
 @StartDate Datetime,  
 @EndDate Datetime,
 @CampaignID int = null,	-- null specifies all campaigns
 @Markets NVARCHAR(MAX),	-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
 @UserTypeID int = null		-- Note: the pre-refactored version (v5) did not use this in logic, so we don't
)
AS
BEGIN
	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
		from ZipRegionMapping 
		where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	--
	-- Create yet another temp table containing all the providers who are in the given campaign(s) and markets
	--
	-- drop table #RptPatients
	SELECT PGL.PatientGroupMapping_LogID, PGL.PatientID, PGL.GroupID INTO #RptPatients
		FROM PatientGroupMapping_Log PGL
		inner join HealthRecord H  on H.UserHealthRecordID = PGL.PatientID
		left join ExtendedProfile E ON E.UserHealthRecordID = H.UserHealthRecordID  
		WHERE ((@CampaignID is null AND H.CampaignID is not null) OR (@CampaignID is not null AND @CampaignID = H.CampaignID)) AND
			  (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
 
	-- NOTE: NOT SURE HOW TO INTEGRATE USER TYPE!!!!! ----------------------------------------
	--  P.UserTypeID = case  when @UserTypeID IS NULL then P.UserTypeID else @UserTypeID end
	
	--
	-- Now generate the report
	--
	
	
    ;with RptTypes As
	(
		select 1 RptID,'1-10 Participants' As RptType
		union all
		select 2 RptID,'11-25 Participants' As RptType
		union all
		select 3 RptID,'26-75 Participants' As RptType
		union all
		select 4 RptID,'75-100 Participants' As RptType
		union all
		select 5 RptID,'>100 Participants' As RptType
	),
	RptTypesWithDates As
	(
		select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	),
	EligiblePatients As
	(
		select A.* 
			from PatientGroupMapping_Log A 
			inner join 
			(
				select max(PatientGroupMapping_LogID) PatientGroupMapping_LogID  
					from #RptPatients
					group by [PatientID],GroupID 
				
			) B on A.PatientGroupMapping_LogID=B.PatientGroupMapping_LogID and [Action]=0
	),
	EligibleProviders As
	(
		Select R.Date1, ProviderID, Count(distinct PatientID) as Patients 
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R 
			inner join EligiblePatients PML on PML.ModifiedDate < R.Date2 -- and PML.ModifiedDate >= R.Date1
			inner join PatientGroupDetails PGD on PGD.groupid=PML.groupid 
			group by R.Date1,ProviderID
	),
	Patients As
	(
		select Date1, ProviderID, Case when Patients between 1 and 10 then '1-10 participants' 
									   when Patients between 11 and 25 then '11-25 participants'
									   when Patients between 26 and 75 then '26-75 participants' 
									   when Patients between 75 and 100 then '75-100 participants'
									   when Patients >100 then '100+ participants' 
								  End RptType 
			from EligibleProviders 
	)



	Select RptID, R.date1, R.RptType, Count(distinct V.ProviderID) UserCount,
		   'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
		into #ToBePivoted
		from RptTypesWithDates R 
		left join Patients V on V.Date1=R.Date1 and V.RptType=R.RptType  
		group by R.date1,R.RptType,RptID

	SELECT DISTINCT ColumnIndex INTO #ColumnsIndex FROM #ToBePivoted
 
	select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1


	DECLARE @MaxCount INTEGER
	DECLARE @Count INTEGER
	DECLARE @Txt VARCHAR(MAX)
	SET @Count = 1
	SET @Txt = '['
	SET @MaxCount = (SELECT MAX(RowID) FROM #c)
	WHILE @Count<=@MaxCount
		BEGIN
		IF @Txt!=''
			SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
		ELSE
			SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
		SET @Count=@Count+1
		END
	--SELECT @Txt AS Txt

	SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

	SET @Cols = @Txt

	SET @query = N'SELECT RptType, '+ @cols +' FROM
	(SELECT RptType, ColumnINdex, UserCount,RptID FROM #ToBePivoted ) A
	PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'
	 
	EXECUTE(@query) 

END
