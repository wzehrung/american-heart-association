﻿CREATE PROC [dbo].[Proc_UpdatePatientContent]
(
 @PatientContentID int
,@ZoneTitle nvarchar(128)
,@ContentTitle nvarchar(128)
,@ContentTitleUrl nvarchar(1128)
,@ContentText nvarchar(MAX)
,@UpdatedByUserID INT
,@LanguageID int
,@ProviderID int = 0
)

As

Begin
set nocount on

if(@ProviderID=0)
begin
Update PatientContent set 
						 ZoneTitle = @ZoneTitle
						,ContentTitle = @ContentTitle
						,ContentTitleUrl = @ContentTitleUrl
						,ContentText = @ContentText
						,UpdatedByUserID=@UpdatedByUserID
						,UpdatedDate = GETDATE()

where  PatientContentID = @PatientContentID and LanguageID=@LanguageID and ProviderID is null
end
else
begin

Update PatientContent set 
						 ZoneTitle = @ZoneTitle
						,ContentTitle = @ContentTitle
						,ContentTitleUrl = @ContentTitleUrl
						,ContentText = @ContentText
						,UpdatedByUserID=@UpdatedByUserID
						,UpdatedDate = GETDATE()

where  PatientContentID = @PatientContentID and LanguageID=@LanguageID and ProviderID = @ProviderID

end
End

