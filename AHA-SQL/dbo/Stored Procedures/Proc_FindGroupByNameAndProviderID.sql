﻿


CREATE PROCEDURE [dbo].[Proc_FindGroupByNameAndProviderID]
(
@ProviderID int,
@GroupName nvarchar(128)
)
As
Begin
set nocount on

declare @TotalAlerts int

select @TotalAlerts=count(distinct AlertID) from alertRule AR inner join
AlertMapping AM on AR.AlertRuleID=AM.AlertRuleID and ProviderID=@ProviderID 
inner join PatientGroupDetails P
on AM.GroupID=P.GroupID and P.Name=@GroupName
inner join alert A on AR.AlertRuleID=A.AlertRuleID AND A.isDismissed=0


	select *,(SELECT COUNT(*) FROM PatientGroupMapping WHERE GroupID in
	(SELECT GroupID FROM PatientGroupDetails WHERE ProviderID=@ProviderID and Name=@GroupName)) totalmembers 
	,@TotalAlerts as TotalAlerts

  from

	 PatientGroupDetails WHERE ProviderID=@ProviderID and Name=@GroupName
							
	
End
