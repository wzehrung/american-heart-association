﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.10.2014
-- Description:	Used by the admin portal to create or update a campaign to accept
--				the @AffiliatedID variable.
--				Derived from original SP: Proc_CreateOrChangeCampaign.
--				Added _Admin to better group sp's.
-- =============================================

CREATE PROC [dbo].[Proc_Admin_CreateOrUpdateCampaign]  
(  
    @Title nvarchar(1024),
    @Description nvarchar(max), 
    @CampaignCode nvarchar(64),
    @Url nvarchar(2048), 
    @StartDate Datetime,  
    @EndDate Datetime,  
    @LanguageID int, 
    @CampaignID int,
    @PromotionalTagLine nvarchar(MAX),
    @PartnerLogo varbinary(MAX),
    @PartnerLogoContentType nvarchar(10),
    @PartnerImage varbinary(MAX),
    @PartnerImageContentType nvarchar(10),
    @strRscTitle nvarchar(255),
    @strRscURL nvarchar(255),
    @BusinessID int,
    @AffiliateID nvarchar(32)
)  
  
AS  
  
BEGIN  
SET NOCOUNT ON  

BEGIN TRY
BEGIN TRANSACTION
  
IF EXISTS (SELECT * FROM Campaign WHERE CampaignID=@CampaignID )  
  
EXEC Proc_Admin_UpdateCampaign  
    @Title, 
    @Description, 
    @StartDate, 
    @EndDate,
    @LanguageID,
    @CampaignID,
    @PromotionalTagLine,
    @PartnerLogo,
    @PartnerLogoContentType,
    @PartnerImage,
    @PartnerImageContentType,
    @strRscTitle,
    @strRscURL, 
    @BusinessID,
    @AffiliateID
ELSE 
  
EXEC Proc_Admin_CreateCampaign
    @Title, 
    @Description, 
    @CampaignCode,
    @Url,
    @StartDate, 
    @EndDate,
    @LanguageID,
    @CampaignID,
    @PromotionalTagLine,
    @PartnerLogo,
    @PartnerLogoContentType,
    @PartnerImage,
    @PartnerImageContentType,
    @strRscTitle,
    @strRscURL, 
    @BusinessID,
    @AffiliateID
 
COMMIT
END TRY

BEGIN CATCH

  If @@TRANCOUNT > 0
	ROLLBACK
	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch 
  
End