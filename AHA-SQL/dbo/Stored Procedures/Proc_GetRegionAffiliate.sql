﻿

CREATE Procedure [dbo].[Proc_GetRegionAffiliate]

AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2014-10-14  
   Description      :Returns unique region affiliate
*/

select distinct 0 as ListItemID, Affiliate as ListItemName, affiliate as ListItemCode FROM ZipRegionMapping
ORDER BY affiliate



End
