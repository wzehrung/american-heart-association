﻿

CREATE proc [dbo].[Proc_FindAlertByHealthRecordItemGUIDAndAlertRuleID]
(
 @HealthRecordItemGUID UNIQUEIDENTIFIER
, @AlertRuleID int
)

AS

Begin
set nocount on

select DISTINCT C.AlertID, C.PatientID, IsDismissed
,CAST(AlertXml AS NVARCHAR(MAX)) AlertXml, CAST(AlertRuleXml AS  NVARCHAR(MAX)) AlertData, CreatedDate,D.type,Notes ,ItemEffectiveDate 
FROM
Alert C inner join AlertRule D on C.AlertRuleId=D.AlertRuleID AND C.HealthRecordItemGUID=@HealthRecordItemGUID and C.AlertRuleID = @AlertRuleID
order by ItemEffectiveDate desc



End


