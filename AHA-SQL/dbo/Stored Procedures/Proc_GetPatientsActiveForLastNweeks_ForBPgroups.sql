﻿
--[Proc_GetPatientsNotSignedUpInForLastNweeks_ForBPgroups] 3
create PROC Proc_GetPatientsActiveForLastNweeks_ForBPgroups
(
 @Numberofweeks int
,@CampaignID  int =null
)
As

BEGIN

;with GruoupDatais as
(
SELECT 
H.UserHealthRecordID


,Case when avg(Systolic) < 140 and avg(Diastolic) <90 then 1 else 0 end as group1
,Case when ((avg(Systolic) <140) and (avg(Diastolic) between 90 and 99)) or ((avg(Systolic) between 140 and 159) and (avg(Diastolic)< 99))  then 1 else 0 end as group2

--,Case when (avg(Systolic) between 120 and 139) or (avg(Diastolic) between 80 and 89) then 1 else 0 end as group2
--,Case when (avg(Systolic) between 120 and 139) or (avg(Diastolic) between 80 and 89) 
--then 0 else Case when (avg(Systolic)>139) or (avg(Diastolic) > 89) then 1 else 0 end end as group3

,Case when ((avg(Systolic) between 140 and 159) and (avg(Diastolic) >100)) or
  ((avg(Systolic) >160) and (avg(Diastolic)<99)) or
 ((avg(Systolic) >160) and (avg(Diastolic)>100)) or
((avg(Systolic) <140) and (avg(Diastolic)>100)) then 1 else 0 end as group3
 
FROM 

HealthRecord H inner join BloodPressure BP on BP.UserHealthRecordID=H.UserHealthRecordID 

WHERE 

CONVERT(DATETIME,CONVERT(VARCHAR,H.CreatedDate,103),103)=DATEADD(WEEK,-1*@Numberofweeks,CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103))
and (H.CampaignID=@CampaignID or @CampaignID is null) 
group by  H.UserHealthRecordID 
)		

select U.*,Lan.LanguageLocale,G.Group1,G.Group2,G.Group3 from 
AHAUser U  
inner join Languages Lan on U.DefaultLanguageID = Lan.LanguageID and AllowAHAEmail=1
inner join AHAUserHealthRecord AHR on U.UserID=AHR.UserID
inner join GruoupDatais G on G.UserHealthRecordID=AHR.UserHealthRecordID


END
