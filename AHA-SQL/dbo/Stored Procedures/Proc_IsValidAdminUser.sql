﻿CREATE PROC [dbo].[Proc_IsValidAdminUser]
(
 @UserName nvarchar(128)
,@Password nvarchar(50)
,@IsValidAdminUser BIT output
)

As

Begin
set nocount on

if exists (SELECT  * FROM AdminUser where  UserName = @UserName and Password=@Password and IsActive = 1)
set @IsValidAdminUser=1
else
set @IsValidAdminUser=0


End

