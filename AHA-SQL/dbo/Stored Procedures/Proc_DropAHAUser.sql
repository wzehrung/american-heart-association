﻿
CREATE Procedure [dbo].[Proc_DropAHAUser]
(
@PersonGUID uniqueidentifier
)
AS
Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Deleting AHAUser Record in 3 steps
					 step1:Delete record from AHAuser
					 step2:Delete record from HealthRecord --this will clear all health related tables
					 step3:Delete record from AHAUserHealthRecord (user-healthrecord mapping)
					 step4:Delete AHAUserLoginDetails
*/


set nocount on

Declare @UserID int 
Declare @SelfHealthRecordID int 

Begin Try
Begin Transaction


select  @UserID= UserID 
	   ,@SelfHealthRecordID=SelfPersonalItemID

from dbo.AHAUser Where PersonGUID= @PersonGUID 
--step4:
Exec Proc_DeleteAHAUserLoginDetails @UserID
--step3:
exec Proc_DeleteAHAUserHealthRecord  @UserID,null
--step1:
exec Proc_DeleteAHAUser @PersonGUID
--step2:
exec Proc_DeleteHealthRecord  @SelfHealthRecordID,1

COMMIT

End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
End


