﻿

CREATE PROCEDURE [dbo].[Proc_FindPatientProviderInvitationDetails]
(
  @InvitationDetailsID int
)
As
Begin

set nocount on
select * from PatientProviderInvitationDetails
   where InvitationDetailsID=@InvitationDetailsID

End