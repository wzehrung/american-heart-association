﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 06.03.2014
-- Description:	Find a ProviderNote by its ID
-- =============================================
CREATE PROCEDURE [dbo].[Proc_FindProviderNoteByID] 
(
	@ProviderNoteID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ProviderNoteID, ProviderID, PatientID, CreationDate, Title, Note
		FROM ProviderNote
		WHERE ProviderNoteID = @ProviderNoteID
END

