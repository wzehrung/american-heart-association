﻿--Proc_GetThingTipsByThingTipsID 1
CREATE PROC [dbo].[Proc_GetThingTipsByThingTipsID]
(
@ThingTipsid int
,@LanguageID int
)

As

Begin
set nocount on

Declare @ThingTipsTypeids nvarchar(max)

select @ThingTipsTypeids = COALESCE(@ThingTipsTypeids + ',', '')+
cast (ThingTipsTypeID   as Varchar(20))
from ThingTipsMapping 
where  ThingTipsid=@ThingTipsid  and LanguageID=@LanguageID

select *,@ThingTipsTypeids ThingTipsTypeIDs from ThingTips

Where ThingTipsid=@ThingTipsid and LanguageID=@LanguageID

End
