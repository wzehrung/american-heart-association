﻿


-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.20.2014
-- Description:	Get users admins and above to assign as feedback recipients
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_GetRecipients]

AS

BEGIN
SET NOCOUNT ON

SELECT

    Email

FROM AdminUser

WHERE FeedbackRecipient = 1

END
