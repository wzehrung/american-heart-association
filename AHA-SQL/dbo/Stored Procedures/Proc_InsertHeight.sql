﻿
CREATE Procedure [dbo].[Proc_InsertHeight]
(
 @UserHealthRecordID int
,@HVItemGuid uniqueidentifier
,@HVVersionStamp uniqueidentifier
,@Height float 
,@Comments nvarchar (max) 
,@DateMeasured Datetime
)
AS

Begin


--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a record to Height table..
*/

Insert dbo.Height 
			 (
                 UserHealthRecordID 
				,HVItemGuid 
				,HVVersionStamp 
				,Height  
				,Comments 
				,DateMeasured 
              )
Select           @UserHealthRecordID 
				,@HVItemGuid 
				,@HVVersionStamp 
				,@Height  
				,@Comments 
				,@DateMeasured 

End


