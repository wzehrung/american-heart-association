﻿
Create Procedure [dbo].[Proc_DropDeprecatedTables]
As

Begin

	Drop table  DeprecatedIncident
	Drop table  DeprecatedExercise
	Drop table  DeprecatedBloodPressure
	Drop table  DeprecatedProfile
	Drop table  DeprecatedZips
	Drop table  DeprecatedWeight
	Drop table  DeprecatedPhysicalActivity

End