﻿




CREATE proc [dbo].[Proc_FindAlertRuleByGroupID]
(
@GroupID int 
)

AS

Begin
set nocount on

select distinct D.AlertRuleID,D.ProviderID,Type,Cast(AlertData as varchar(max)) AlertData,IsActive,SendMessageToPatient
--,case when C.GroupID is not null then null else C.PatientID end PatientID
,null  PatientID
,A.GroupID
,A.Name   GroupName
,'G' As AlertRuleSubscriberType
,MessageToPatient

from PatientGroupDetails A
--inner join alertmapping C on A.GroupID=C.GroupID AND A.GroupID=@GroupID
inner join AlertRule D on A.GroupID=D.GroupID AND ISACTIVE=1 and A.GroupID=@GroupID

End





