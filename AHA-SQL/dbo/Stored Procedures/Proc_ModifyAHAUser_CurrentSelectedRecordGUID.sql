﻿

CREATE Procedure [dbo].[Proc_ModifyAHAUser_CurrentSelectedRecordGUID]  
(  
 @PersonGUID uniqueidentifier
,@CurrentSelectedRecordGUID uniqueidentifier

)
AS  
  
Begin  

/* Author			:Shipin Anand  
   Created Date     :2008-07-31    
   Description      :Updating CurrentSelectedRecordGUID Column of AHAUser for a PersonGUID  

   Note : Please dont try to update a indexed foreign key 
*/  

set nocount on  

Update dbo.AHAUser  
                SET  CurrentSelectedRecordGUID = @CurrentSelectedRecordGUID
					 ,UpdatedDate			   = getDate()
				where PersonGUID=@PersonGUID 
End
 

