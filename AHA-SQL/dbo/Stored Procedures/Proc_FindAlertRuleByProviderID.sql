﻿


--[Proc_FindAlertRuleByProviderID] 4

CREATE proc [dbo].[Proc_FindAlertRuleByProviderID]
(
@ProviderID int 
)

AS

Begin
set nocount on

select  distinct 

 D.AlertRuleID
,D.ProviderID
,Type,Cast(AlertData as varchar(max)) AlertData
,IsActive
,SendMessageToPatient
,case when C.GroupID is not null then null else C.PatientID end PatientID
,D.GroupID  GroupID
--,case when C.GroupID is null and D.GroupID is null  then null else A.Name end  GroupName
,A.Name  GroupName
,Case when D.GroupID is null then 'P' when D.GroupID is not null and isDefault=1 then 'A' 
 when  D.GroupID is not null and isDefault=0 then  'G' end As AlertRuleSubscriberType
--,case when C.GroupID = A.GroupID and isDefault=1 then 'A' 
--      when C.GroupID is null and D.GroupID is null then 'P' 
--		WHEN D.GroupID is NOT null And isDefault=0 THEN 'G' End As AlertRuleSubscriberType

,MessageToPatient

from	AlertRule D  
left join PatientGroupDetails A on A.GroupID=D.GroupID
and A.ProviderID=D.ProviderID 
left join alertmapping C on C.AlertRuleID=D.AlertRuleID AND ISACTIVE=1
and isnull(C.GroupID,0)=case when C.GroupID is null then 0 else A.GroupID end

where  D.ProviderID=@ProviderID

End








