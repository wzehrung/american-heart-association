﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 6.17.2013
-- Description:	Refactor of Proc_Report_ReportedUploads_Campaign to handle Campaign/Markets
-- =============================================

CREATE PROCEDURE [dbo].[V6_Proc_Report_ReportedUploads_Campaign_WZ]
(
@StartDate DATETIME,
@EndDate DATETIME,
@CampaignID INT = NULL,

-- null specifies all campaigns

@Markets NVARCHAR (MAX) 

-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)

) 
AS

BEGIN

    /* For testing */

    --DROP TABLE #RptMarketZipCodes
    --DROP TABLE #h
    --DROP TABLE #ToBePivoted
    --DROP TABLE #ColumnsIndex
    --DECLARE @StartDate Datetime  
    --DECLARE @EndDate Datetime
    --DECLARE @CampaignID int
    --DECLARE @Markets NVARCHAR(MAX)
    --SET @StartDate='Jan  1 2012 12:00:00:000AM'
    --SET @EndDate='Jan  1 2013 12:00:00:000AM'
    --SET @CampaignID=76

    DECLARE @cols NVARCHAR (MAX) 

    DECLARE @colsConv NVARCHAR (MAX) 

    DECLARE @I INT

    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.

    SET NOCOUNT ON;

    -- Create a temp table to contain all the zip codes that relate to given Markets
    -- Also keep a counter of how many zip codes are in the temp table
    --
    -- drop table #RptMarketZipCodes

    DECLARE @ZipCodeCount INT

    SELECT
           Zip_Code INTO
                         #RptMarketZipCodes
      FROM ZipRegionMapping
      WHERE CBSA_Code IN (
                SELECT
                       VALUE
                  FROM dbo.udf_ListToTableString (@Markets, '|')) 

    -- this line below was where I expected markets to be the CBSA_Name
    -- the +4 after charindex to add ', XX' where = 2-letter state abbr.
    --where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))

    SELECT
           @ZipCodeCount = COUNT (*) 
      FROM #RptMarketZipCodes

    --
    -- Create a temp table of all users who are in campaign/markets logic
    --

    SELECT * INTO #h
    FROM 
	   (
		  SELECT
			 UserID,
			 AHR.UserHealthRecordID
		  FROM
			 AHAUserHealthRecord AHR
				INNER JOIN HealthRecord H
				    ON H.UserHealthRecordID = AHR.UserHealthRecordID
				LEFT JOIN ExtendedProfile E
				    ON E.UserHealthRecordID = AHR.UserHealthRecordID

/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
just get those is specified) */

		  WHERE
			 (
				@CampaignId IS NULL
				AND H.CampaignID IS NOT NULL
				OR @CampaignId IS NOT NULL
				AND @CampaignId = H.CampaignID
			 ) 
				AND (
					   @ZipCodeCount = 0
					   OR E.ZipCode
					   IN  (
							 SELECT
							 Zip_Code
							 FROM #RptMarketZipCodes
						  )
				    )
	   ) a

    --
    -- generate the report
    --
    -- ******************
    -- PJB: NOTE THAT IN THE QUERIES BELOW, THERE IS NOT SYMMETRY BETWEEN THE VARIOUS TRACKERS.
    --      I LEFT THE COMMENTED LINES AS IS.
    -- ******************

    ;

    WITH Tracker
        AS 
		  (
			 SELECT
				TrackerCount,
				Date1,
				'Weight' Tracker
			 FROM 
				(
				    SELECT
						 COUNT (*) AS TrackerCount,
						 Date1
				    FROM (
							 SELECT
								COUNT (W.UserHealthRecordID) AS tracker,
								R.Date1
							 FROM
								DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
								    INNER JOIN Weight W
									   ON W.DateMeasured >= R.Date1
										  AND W.DateMeasured < R.Date2
								    INNER JOIN #h H
									   ON H.UserHealthRecordID = W.UserHealthRecordID
							 GROUP BY
								Date1,
								W.UserHealthRecordID
							 HAVING COUNT (W.UserHealthRecordID) > 1
						  ) A
				    GROUP BY
					   Date1

				    UNION

				    SELECT
						 0 AS TrackerCount,
						 R.Date1
				    FROM
					   DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
						  LEFT JOIN  weight W
							 ON W.DateMeasured >= R.Date1
					   AND W.DateMeasured < R.Date2
						  LEFT JOIN #h H
							 ON H.UserHealthRecordID = W.UserHealthRecordID
				    GROUP BY
					   R.Date1

               --having count(W.userhealthRecordid) =0 or count(W.Userhealthrecordid) =1

               ) A
        UNION ALL
        SELECT
               TrackerCount,
               Date1,
               'Blood Pressure' Tracker
          FROM (
                    SELECT
                           COUNT (*) AS TrackerCount,
                           Date1
                      FROM (
                                SELECT
                                       COUNT (BP.UserHealthRecordID) AS tracker,
                                       R.Date1
                                  FROM
                                       DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
                                           INNER JOIN BloodPressure BP
                                               ON
                                        BP.DateMeasured >= R.Date1
                                    AND BP.DateMeasured < R.Date2
                                           INNER JOIN #h H
                                               ON H.UserHealthRecordID = BP.UserHealthRecordID
                                  GROUP BY
                                           Date1,
                                           BP.UserHealthRecordID
                                  HAVING COUNT (BP.UserHealthRecordID) > 1) A
                      GROUP BY
                               Date1
                    UNION
                    SELECT
                           0 AS TrackerCount,
                           R.Date1
                      FROM
                           DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
                               LEFT JOIN BloodPressure BP
                                   ON
                            BP.DateMeasured >= R.Date1
                        AND BP.DateMeasured < R.Date2

                      LEFT JOIN  #h H on H.UserHealthRecordID=BP.UserHealthRecordID											

                      GROUP BY
                               R.Date1

               --having count(BP.userhealthRecordid) =0 or count(BP.Userhealthrecordid) =1

               )A
        UNION ALL
        SELECT
               TrackerCount,
               Date1,
               'Physical Activity' Tracker
          FROM (
                    SELECT
                           COUNT (*) AS TrackerCount,
                           Date1
                      FROM (
                                SELECT
                                       COUNT (E.UserHealthRecordID) AS tracker,
                                       R.Date1
                                  FROM
                                       DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
                                           INNER JOIN Exercise E
                                               ON
                                        E.DateofSession >= R.Date1
                                    AND E.DateofSession < R.Date2
                                           INNER JOIN #h H
                                               ON H.UserHealthRecordID = E.UserHealthRecordID
                                  GROUP BY
                                           Date1,
                                           E.USerHealthRecordID
                                  HAVING COUNT (E.UserHealthRecordID) > 1) A
                      GROUP BY
                               Date1
                    UNION
                    SELECT
                           0 AS TrackerCount,
                           R.Date1
                      FROM
                           DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
                               LEFT JOIN Exercise E
                                   ON
                            E.DateofSession >= R.Date1
                        AND E.DateofSession < R.Date2

                      LEFT JOIN #h H on H.UserHealthRecordID=E.UserHealthRecordID											

                      GROUP BY
                               R.Date1

               --having count(E.userhealthRecordid) =0 or count(E.Userhealthrecordid) =1

               )A
        UNION ALL
        SELECT
               TrackerCount,
               Date1,
               'Medication' Tracker
          FROM (
                    SELECT
                           COUNT (*) AS TrackerCount,
                           Date1
                      FROM (
                                SELECT
                                       COUNT (M.UserHealthRecordID) AS tracker,
                                       R.Date1
                                  FROM
                                       DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
                                           INNER JOIN Medication M
                                               ON
                                        M.CreatedDate >= R.Date1
                                    AND M.CreatedDate < R.Date2
                                           INNER JOIN #h H
                                               ON H.UserHealthRecordID = M.UserHealthRecordID
                                  GROUP BY
                                           Date1,
                                           M.USerHealthRecordID
                                  HAVING COUNT (M.UserHealthRecordID) > 1) A
                      GROUP BY
                               Date1
                    UNION
			 SELECT
				0 AS TrackerCount,
				R.Date1
			 FROM
				DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
				    LEFT JOIN Medication M
					   ON M.createddate >= R.Date1
						  AND M.createddate < R.Date2

					   LEFT JOIN #h H
						  ON H.UserHealthRecordID = M.UserHealthRecordID

			 GROUP BY
			 R.Date1

               --having count(M.userhealthRecordid) =0 or count(M.Userhealthrecordid) =1

               )A
        UNION ALL
        SELECT
               TrackerCount,
               Date1,
               'Cholesterol' Tracker
          FROM (
                    SELECT
                           COUNT (*) AS TrackerCount,
                           Date1
                      FROM (
                                SELECT
                                       COUNT (C.UserHealthRecordID) AS tracker,
                                       R.Date1
                                  FROM
                                       DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
                                           INNER JOIN Cholesterol C
                                               ON
                                        C.DateMeasured >= R.Date1
                                    AND C.DateMeasured < R.Date2
                                           INNER JOIN #h H
                                               ON H.UserHealthRecordID = C.UserHealthRecordID
                                  GROUP BY
                                           Date1,
                                           C.USerHealthRecordID
                                  HAVING COUNT (C.UserHealthRecordID) > 1) A
                      GROUP BY
                               Date1
                    UNION
                    SELECT
                           0 AS TrackerCount,
                           R.Date1
                      FROM
                           DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
                               LEFT JOIN Cholesterol C
                                   ON
                            C.DateMeasured >= R.Date1
                        AND C.DateMeasured < R.Date2

					   LEFT JOIN #h H
						  ON H.UserHealthRecordID = C.UserHealthRecordID

                      GROUP BY
                               R.Date1

               --having count(C.userhealthRecordid) =0 or count(C.Userhealthrecordid) =1

               )A

        UNION ALL

        SELECT
		  TrackerCount,
		  Date1,
		  'Blood Glucose' Tracker
	   FROM 
		  (
			 SELECT
				COUNT (*) AS TrackerCount,
				Date1
			 FROM 
				(
				    SELECT
					   COUNT (BG.UserHealthRecordID) AS tracker,
					   R.Date1
				    FROM
					   DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
						  INNER JOIN BloodGlucose BG
							 ON BG.DateMeasured >= R.Date1
								AND BG.DateMeasured < R.Date2
						  INNER JOIN #h H
							 ON H.UserHealthRecordID = BG.UserHealthRecordID
				    GROUP BY
					   Date1,
					   BG.USerHealthRecordID
				    HAVING
					   COUNT (BG.UserHealthRecordID) > 1
				) A
			 GROUP BY
				Date1
				    
			 UNION

				SELECT
				    0 AS TrackerCount,
				    R.Date1
				FROM
				    DBO.udf_GetDatesWithRange (@StartDate, @EndDate) R
					   LEFT JOIN BloodGlucose BG
						  ON BG.DateMeasured >= R.Date1
							 AND BG.DateMeasured < R.Date2
					   LEFT JOIN #h H
						  ON H.UserHealthRecordID = BG.UserHealthRecordID
				GROUP BY
				    R.Date1
				--HAVING
				--    COUNT (BG.userhealthRecordid) = 0
				--    OR COUNT (BG.Userhealthrecordid) = 1
			 ) A
		  )

        SELECT
		  T.Date1,
		  Tracker,
		  TrackerCount AS UserCount,
		  'Column # ' + CAST (DENSE_RANK () OVER (ORDER BY T.Date1) AS VARCHAR (10)) ColumnIndex,
		  CAST (DENSE_RANK () OVER (ORDER BY T.Date1) AS INT) IntIndex
        INTO
             #ToBePivoted
	   FROM Tracker T

    SELECT DISTINCT
        ColumnIndex,
        IntIndex
    INTO
	   #ColumnsIndex
    FROM 
	   #ToBePivoted

    SELECT
           @cols = STUFF ((
                SELECT TOP 100 PERCENT
                       '],['
                     + t.ColumnIndex
                  FROM #ColumnsIndex AS t
                  ORDER BY
                           t.IntIndex
                  FOR
                  XML PATH ('') 
                          ), 1, 2, '') 
                 + ']'

    --SELECT @i = count(*) from #h
    --   IF(@i = 0)
    --   BEGIN
    --	SELECT 'NONE' as Tracker, '0.00%' as [Column # 1], '0.00%' as [Column # 2] where 1=1
    --   END
    --   ELSE

    BEGIN
        EXECUTE (N'SELECT Tracker, '+ @cols +' FROM
		(SELECT Tracker, ColumnINdex, UserCount FROM #ToBePivoted ) A
		PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;') 
    END

--SELECT * FROM #ToBePivoted

END