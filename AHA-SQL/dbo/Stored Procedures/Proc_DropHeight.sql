﻿



CREATE Procedure [dbo].[Proc_DropHeight]
(
 @HVItemGuid uniqueidentifier  = NULL
)
AS
Begin
/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Deleet a record from Height table by Healthrecord and itemGUID
*/
set nocount on

Begin Try
Begin Transaction


Delete from  dbo.Height   
			       Where  HVItemGuid=@HVItemGuid 


Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.Height   
							 Where HVItemGuid=@HVItemGuid 

exec proc_UpdateHealthRecord_LastActivityDate_PrimaryKey @UserHealthRecordID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End






