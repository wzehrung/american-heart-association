﻿CREATE Procedure [dbo].[Proc_InsertExtendedProfile]
(
 @UserHealthRecordID int 
,@FamilyHistoryHD int
,@FamilyHistoryStroke int
,@HighBPDiet int 
,@HighBPMedication int 
,@KidneyDisease int 
,@DiabetesDiagnosis int 
,@HeartDiagnosis int 
,@Stroke int 
,@PhysicalActivityLevel nvarchar(100) 
,@BloodType nvarchar(100) 
,@MedicationAllergy nvarchar(max)
,@TravelTimeToWorkInMinutes float
,@MeansOfTransportation nvarchar(255)
,@AttitudesTowardsHealthcare nvarchar(100)
,@DoYouSmoke bit
,@HaveYouEverTriedToQuitSmoking  bit
,@LastSynchDate Datetime
,@Country  nvarchar(50)= NULL
,@Gender nvarchar(10) = NULL
,@Ethnicity nvarchar(50) = NULL
,@MaritalStatus nvarchar(50) = NULL
,@YearOfBirth INT
,@ZipCode nvarchar(50)
)

AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a new entry to ExtendedProfile Table
*/

set nocount on

if(@ZipCode = '0')
begin
  SET @ZipCode = null
end 
insert dbo.ExtendedProfile
 
						(
						 UserHealthRecordID
						,FamilyHistoryHD
						,FamilyHistoryStroke
						,HighBPDiet
						,HighBPMedication
						,KidneyDisease
						,DiabetesDiagnosis
						,HeartDiagnosis
						,Stroke
						,PhysicalActivityLevel
						,BloodType
						,MedicationAllergy
						,TravelTimeToWorkInMinutes
						,MeansOfTransportation
						,AttitudesTowardsHealthcare
						,DoYouSmoke
						,HaveYouEverTriedToQuitSmoking
						,LastSynchDate
						,[Country/Region]
						,Gender 
						,Ethnicity 
						,MaritalStatus
						,YearOfBirth 
						,ZipCode
						)

select

						 @UserHealthRecordID
						,@FamilyHistoryHD
						,@FamilyHistoryStroke
						,@HighBPDiet
						,@HighBPMedication
						,@KidneyDisease
						,@DiabetesDiagnosis
						,@HeartDiagnosis
						,@Stroke
						,@PhysicalActivityLevel
						,@BloodType
						,@MedicationAllergy
						,@TravelTimeToWorkInMinutes
						,@MeansOfTransportation
						,@AttitudesTowardsHealthcare
						,@DoYouSmoke
						,@HaveYouEverTriedToQuitSmoking
						,@LastSynchDate
						,@Country  
						,@Gender 
						,@Ethnicity 
						,@MaritalStatus
						,@YearOfBirth
						,@ZipCode

End
