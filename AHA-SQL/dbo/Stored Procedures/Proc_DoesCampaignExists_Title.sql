﻿
CREATE PROC [dbo].[Proc_DoesCampaignExists_Title]
(
 @Title nvarchar(1024)
,@LanguageID int
,@isValid bit output
)

As


Begin
set nocount on

if exists (select * from Campaign  C inner join CampaignDetails CD on C.CampaignID=CD.CampaignID 
 where Title=@Title and LanguageID=@LanguageID)
set @isValid=1
else
set @isValid=0
End