﻿CREATE proc [dbo].[Proc_GetNewAlertCountForProvider]
(
 @ProviderID INT 
,@Count int output
)

AS

Begin
set nocount on

select @Count=COUNT(distinct C.AlertID) 

from PatientGroupDetails A
inner join PatientGroupMapping B
on A.GroupID=B.GroupID and A.ProviderID=@ProviderID
inner join AlertRule D on A.ProviderID=D.ProviderID
INNER JOIN Alert C On B.PatientID=C.PatientID and isDismissed=0
and C.AlertRuleID=D.AlertRuleiD

End
