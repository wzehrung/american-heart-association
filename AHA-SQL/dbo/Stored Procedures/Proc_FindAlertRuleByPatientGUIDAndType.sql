﻿


CREATE proc [dbo].[Proc_FindAlertRuleByPatientGUIDAndType]
(
 @UserHealthRecordGUID uniqueidentifier 
,@Type nvarchar(128)=null
)

AS

Begin
set nocount on

select  

distinct

D.AlertRuleID,D.ProviderID,Type,Cast(AlertData as varchar(max)) AlertData,IsActive,SendMessageToPatient
,C.GroupID
,case when C.GroupID is null then null else A.Name end  GroupName
,case when C.GroupID =A.GroupID and A.isDefault=1 then 'A' 
when C.GroupID is null then 'P' else 'G' End As AlertRuleSubscriberType
,case when C.GroupID is not null then null else C.PatientID end PatientID
,MessageToPatient


from 

	healthrecord h inner join PatientGroupMapping B
	on h.userhealthrecordid=b.PatientID and h.OfflineHealthRecordGUID=@UserHealthRecordGUID
	inner join  PatientGroupDetails A
	on A.GroupID=B.GroupID 
	inner join alertmapping C on B.PatientID=C.PatientID
	inner join AlertRule D on C.AlertRuleID=D.AlertRuleID
	WHERE D.Type=isnull(@Type,D.Type) AND ISACTIVE=1   
	and isnull(C.GroupID,0)=case when C.GroupID is null then 0 else A.GroupID end


End


