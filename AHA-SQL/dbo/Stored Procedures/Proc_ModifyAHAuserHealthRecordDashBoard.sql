﻿

CREATE PROC [dbo].[Proc_ModifyAHAuserHealthRecordDashBoard]
(
 @PersonGUID uniqueidentifier
,@HealthRecordGUID uniqueidentifier
,@DashboardSortOrder xml
)

As

Begin
declare @UserID int,@HealthRecordID int

set @UserID = (select UserID from AHAuser where PersonGUID=@PersonGUID)

set @HealthRecordID = (select UserHealthRecordID from HealthRecord where UserHealthRecordGUID=@HealthRecordGUID)

UPDATE AHAuserHealthRecord SET DashboardSortOrder=@DashboardSortOrder WHERE UserID=@UserID  AND UserHealthRecordID=@HealthRecordID

End
