﻿

CREATE PROCEDURE [dbo].[Proc_CreateResetPasswordLog]
(
 @ResetGUID uniqueidentifier
,@ProviderID int
,@PasswordResetID int output
)
As
Begin

set nocount on

insert ResetPasswordLog(ResetGUID,ProviderID,CreatedDate)
select @ResetGUID,@ProviderID,getdate()
set @PasswordResetID=scope_identity()

End


