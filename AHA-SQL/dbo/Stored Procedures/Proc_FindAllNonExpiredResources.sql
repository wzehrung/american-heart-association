﻿
CREATE PROC [dbo].[Proc_FindAllNonExpiredResources]
(
@IsProvider bit,
@LanguageID int
)
As

Begin 
set nocount on

select * from Resources WHERE LanguageID=@LanguageID and IsProvider=@IsProvider and isnull(ExpirationDate,'9999.12.31')>=getdate() ORDER BY SortOrder asc

END