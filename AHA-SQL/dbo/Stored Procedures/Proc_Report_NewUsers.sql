﻿CREATE PROC [dbo].[Proc_Report_NewUsers]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
)
As

BEGIN
set nocount on
;with RptTypes As
(
select 1 RptID,'1 Visit' As RptType
union all
select 2 RptID,'2-4 Visits' As RptType
union all
select 3 RptID,'5-10 Visits' As RptType
union all
select 4 RptID,'>10 Visits' As RptType
),
RptTypesWithDates As
(
select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,EligibleUsers As
(
Select distinct A.UserID,R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
									inner join AHAUser A 
									on A.CreatedDate >=R.Date1 and A.CreatedDate <R.Date2 
									
									left join AHAUserHealthRecord AHR on AHR.UserID=A.UserID
									left join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID  
									
where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
)
,NewVisits As
(
Select R.Date1,ALD.AHAUserID
		,Case when count(AHAUserID) =1 then '1 Visit' when count(AHAUserID) between 2 and 4 then '2-4 Visits'
			  when count(AHAUserID) between 5 and 10 then '5-10 Visits'
			  when count(AHAUserID) >10 then '>10 Visits' End RptType 
			  
	   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
				AHAUserLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2  inner join  
				EligibleUsers E1 on E1.UserID=ALD.AHAUserID and E1.date1=R.date1
group by R.Date1,AHAUserID
)

,ToBePivoted As
(
Select RptID,R.date1,R.RptType,Count(V.RptType) UserCount 
,'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
from RptTypesWithDates R left join NewVisits V
on V.Date1=R.Date1 and V.RptType=R.RptType  group by R.date1,R.RptType,RptID
)
		
select 

 RptType
,[Column # 1] as [Column # 1] 
,[Column # 2] as [Column # 2]
,[Column # 3] as [Column # 3]
,[Column # 4] as [Column # 4] 
,[Column # 5] as [Column # 5]
,[Column # 6] as [Column # 6]
,[Column # 7] as [Column # 7]
,[Column # 8] as [Column # 8] 
,[Column # 9] as [Column # 9]
,[Column # 10] as [Column # 10]
,[Column # 11] as [Column # 11]
,[Column # 12] as [Column # 12]


from
(
select	RptType,ColumnIndex,UserCount,RptID
		from ToBePivoted
) A
PIVOT
(
SUM (UserCount)
FOR ColumnIndex IN
( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
 , [Column # 10],[Column # 11],[Column # 12] )
) B

ORDER BY RptID

END
