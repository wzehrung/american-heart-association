﻿



CREATE proc [dbo].[Proc_DismissAlertsOlderThanNWeeks]


AS

Begin
set nocount on


UPDATE A SET IsDismissed=1  from  Provider P inner Join AlertRule AR
on P.ProviderID = AR.ProviderID
inner join Alert A on AR.AlertRuleID=A.AlertRuleID
WHERE convert(datetime,convert(varchar,CreatedDate,103),103)<= 
convert(datetime,convert(varchar, dateadd(week,-1*DismissAlertAfterWeeks,getdate()),103),103)
  

End



