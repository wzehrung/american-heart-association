﻿
CREATE Procedure [dbo].[Proc_UpdateCholesterol]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Totalcholesterol int
,@LDL int  
,@HDL int 
,@Triglyceride int 
,@TestLocation nVarchar(200) 
,@DateMeasured datetime 
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :updating Cholesterol table ..
*/


Update dbo.Cholesterol

                  SET

				   HVVersionStamp	= @HVVersionStamp
				  ,Totalcholesterol	= @Totalcholesterol  	
				  ,LDL				= @LDL 
				  ,HDL				= @HDL 
				  ,Triglyceride		= @Triglyceride
				  ,TestLocation		= @TestLocation 
				  ,DateMeasured		= @DateMeasured 
                  ,UpdatedDate		= GetDate()
                  
                  Where HVItemGuid=@HVItemGuid 
End










