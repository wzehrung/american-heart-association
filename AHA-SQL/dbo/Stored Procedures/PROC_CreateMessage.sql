﻿



CREATE PROC [dbo].[PROC_CreateMessage]
(
 @FromUserID int
,@ToUserID nvarchar(4000)
,@MessageType char(1)
,@IsSentByProvider bit
,@GroupID int
,@MessageID int output
)

As
Begin

SET NOCOUNT ON

Begin Try
Begin Transaction

		--DECLARE @MessageID int
		Declare @SentItemFolderID int



		if @IsSentByProvider=0

		begin



		set @SentItemFolderID=
		(
		select UFM.UserFolderMappingID from
		Folder F  
		inner join UserFolderMapping UFM
		on F.FolderID=UFM.FolderID and UFM.PatientID=@FromUserID
		and FolderType='S'
		)


		SET @ToUserID=@ToUserID+','



		INSERT [Message] 
						(		
						MessageType
					   ,IsSentByProvider
					   ,GroupID
						)

		SELECT		
					 @MessageType 
					,@IsSentByProvider
					,@GroupID


		SET @MessageID = SCOPE_IDENTITY()


		;WITH ToUserIDS(ToUserID, Child)
				AS
				(
				SELECT
				SUBSTRING(@ToUserID,1, CHARINDEX(',', @ToUserID)-1) ToUserID,
				SUBSTRING(@ToUserID,CHARINDEX(',', @ToUserID)+1, len(@ToUserID)) Child
				UNION ALL
				SELECT
				SUBSTRING(Child,1, CHARINDEX(',', Child)-1) ToUserID,
				SUBSTRING(Child,CHARINDEX(',', Child)+1, len(Child)) Child
				FROM ToUserIDS
				WHERE
				CHARINDEX(',', Child) > 0
				)

				INSERT [MessageDetails] 
				(
				 MessageID
				,PatientID 
				,ProviderID
				,UserFolderMappingID
				,isSentByProvider
				)
				SELECT 
				@MessageID
			   ,@FromUserID
			   ,TUI.ToUserID 
			   ,UFM.UserFolderMappingID 
			   ,@isSentByProvider

				FROM ToUserIDS TUI
				inner join UserFolderMapping UFM
				on UFM.ProviderID=TUI.ToUserID inner join
				Folder F  
				on F.FolderID=UFM.FolderID and FolderType='I'

				Union all

				SELECT 
				@MessageID
			   ,@FromUserID
			   ,ToUserID
			   ,@SentItemFolderID
			   ,1
			
				FROM ToUserIDS
				OPTION(MAXRECURSION 500) --allow for more than 100 participants
			 
		End




		else if @IsSentByProvider=1

		begin




		set @SentItemFolderID=
		(
		select UFM.UserFolderMappingID from
		Folder F  
		inner join UserFolderMapping UFM
		on F.FolderID=UFM.FolderID and UFM.ProviderID=@FromUserID
		and FolderType='S'
		)


		SET @ToUserID=@ToUserID+','



		INSERT [Message] 
						(		
						MessageType
						,IsSentByProvider
						,GroupID
						)

		SELECT		
					 @MessageType 
					,@IsSentByProvider
					,@GroupID


		SET @MessageID = SCOPE_IDENTITY()


		;WITH ToUserIDS(ToUserID, Child)
				AS
				(
				SELECT
				SUBSTRING(@ToUserID,1, CHARINDEX(',', @ToUserID)-1) ToUserID,
				SUBSTRING(@ToUserID,CHARINDEX(',', @ToUserID)+1, len(@ToUserID)) Child
				UNION ALL
				SELECT
				SUBSTRING(Child,1, CHARINDEX(',', Child)-1) ToUserID,
				SUBSTRING(Child,CHARINDEX(',', Child)+1, len(Child)) Child
				FROM ToUserIDS
				WHERE
				CHARINDEX(',', Child) > 0
				)

				INSERT [MessageDetails] 
				(
				 MessageID
				,ProviderID
				,PatientID 		
				,UserFolderMappingID
				,isSentByProvider
				)
				SELECT 
				@MessageID
			   ,@FromUserID
			   ,TUI.ToUserID 
			   ,UFM.UserFolderMappingID 
			   ,@isSentByProvider

				FROM ToUserIDS TUI
				inner join UserFolderMapping UFM
				on UFM.PatientID=TUI.ToUserID inner join
				Folder F  
				on F.FolderID=UFM.FolderID and FolderType='I'
				Union all
				

				SELECT 
				@MessageID
			   ,@FromUserID	   
			   ,ToUserID	   
			   ,@SentItemFolderID
			   ,0
			
				FROM ToUserIDS 
				OPTION(MAXRECURSION 500) --allow for more than 100 participants
			 
		End



COMMIT
End Try

Begin Catch

 If @@TRANCOUNT > 0
	ROLLBACK

	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch




END













