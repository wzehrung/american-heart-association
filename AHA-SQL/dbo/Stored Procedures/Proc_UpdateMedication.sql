﻿
CREATE Procedure [dbo].[Proc_UpdateMedication]
( 
 @HVItemGuid uniqueidentifier 
,@HVVersionStamp uniqueidentifier  
,@MedicationName nvarchar (255) 
,@MedicationType nvarchar (255) 
,@Dosage nvarchar (50)   
,@Strength nvarchar (50)   
,@Frequency nvarchar (50) 
,@StartDate Datetime
,@DateDiscontinued Datetime
,@Notes nvarchar(max)

)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Updating Medication table..
*/

Update dbo.Medication

          SET 
				 HVVersionStamp		= @HVVersionStamp
				,MedicationName		= @MedicationName
				,MedicationType		= @MedicationType
				,Dosage				= @Dosage
				,Strength			= @Strength
				,Frequency			= @Frequency
				,StartDate          = @StartDate
				,DateDiscontinued	= @DateDiscontinued
				,Notes				= @Notes
				,UpdatedDate		= GetDate()

          Where
				HVItemGuid=@HVItemGuid 
End







