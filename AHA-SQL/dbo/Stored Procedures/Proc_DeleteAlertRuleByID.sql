﻿
CREATE proc [dbo].[Proc_DeleteAlertRuleByID] 
(
@AlertRuleID int 
)

AS

Begin
set nocount on

Begin Try
Begin Transaction

--IF EXISTS (select * from Alert where AlertRuleID=@AlertRuleID)
--	begin
--		update AlertRule set isActive=0 where AlertRuleID=@AlertRuleID
--	End
--
--	else
--
--	begin
		delete from AlertMapping where AlertRuleID=@AlertRuleID
		delete from Alert where AlertRuleID=@AlertRuleID
		delete from AlertRule where AlertRuleID=@AlertRuleID
--    End


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End




