﻿


-- =============================================
-- Author:		Scott Shipp
-- Create date: January 30, 2009
-- Description:	This SP runs all reports.
-- Reports start with Proc_Rpt...
-- =============================================
CREATE PROCEDURE [dbo].[Proc_RptRunAllReports]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    Exec Proc_RptBreakdownByAge
    Exec Proc_RptBreakdownByGender
    Exec Proc_RptUsersTrackingBloodGlucose
    Exec Proc_RptUsersTrackingBloodPressure
	Exec Proc_RptUsersTrackingCholesterol
    Exec Proc_RptUsersTrackingMedication
	Exec Proc_RptUsersTrackingPhysicalActivity	
    Exec Proc_RptUsersTrackingWeight
	Exec Proc_RptVisitsPerUser
	
END



