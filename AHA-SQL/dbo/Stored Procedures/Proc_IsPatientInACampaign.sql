﻿


CREATE Procedure [dbo].[Proc_IsPatientInACampaign]
(
 @PatientID Int
)
AS

Begin

set nocount on

SELECT
	uh.UserHealthRecordID AS PatientID
	,hr.CampaignID

	FROM
		AHAUser u

	INNER JOIN
		AHAUserHealthRecord uh
	
	ON
		u.UserID = uh.UserID

	INNER JOIN HealthRecord hr
	
	ON
		uh.UserHealthRecordID = hr.UserHealthRecordID

	WHERE hr.CampaignID IS NOT NULL AND uh.UserHealthRecordID = @PatientID 
					
End


