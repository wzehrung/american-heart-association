﻿CREATE PROC [dbo].[Proc_GetAdminUserByUserName]
(
	@UserName NVARCHAR(128)
)

AS

BEGIN
SET NOCOUNT ON

SELECT a.*,l.LanguageLocale, ar.Name AS RoleName, ab.BusinessID, ab.Name AS BusinessName FROM AdminUser a
	INNER JOIN Languages l 
		ON a.DefaultLanguageID = l.LanguageID

	INNER JOIN AdminRoles ar
		ON a.RoleID = ar.RoleID

	LEFT JOIN AdminBusiness ab
		ON a.BusinessID = ab.BusinessID
	
WHERE a.UserName = @UserName AND a.isActive=1

END

