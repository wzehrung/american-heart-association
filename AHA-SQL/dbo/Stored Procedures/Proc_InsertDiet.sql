﻿
CREATE procedure [dbo].[Proc_InsertDiet]
(
 @UserHealthRecordID int 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier
,@CalorieAmount int 
,@Meal nvarchar(50) 
,@FoodItems nvarchar(max) 
,@DateOfEntry datetime 
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Inserting record in to Diet table ..
*/

Insert dbo.Diet	  (                     
					 UserHealthRecordID
					,HVItemGuid
					,HVVersionStamp
					,CalorieAmount
					,Meal
					,FoodItems
					,DateOfEntry				            
                  )
Select               @UserHealthRecordID
					,@HVItemGuid
					,@HVVersionStamp
					,@CalorieAmount
					,@Meal
					,@FoodItems
					,@DateOfEntry	
End

