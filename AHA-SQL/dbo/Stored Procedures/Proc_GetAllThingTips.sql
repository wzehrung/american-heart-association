﻿
--[Proc_GetAllThingTips] 1


CREATE PROC [dbo].[Proc_GetAllThingTips]
(
@LanguageID int
)
As

Begin
set nocount on


;WITH Duplicate (RowNumber, ThingTipsID, ThingTipsTypeID, ThingTipsTypeIDs) AS
(
  SELECT 1, C.ThingTipsID, cast ((B.ThingTipsTypeID) as varchar(20)), CAST((B.ThingTipsType) AS VARCHAR(8000)) 
  FROM ThingTipsMapping C
  INNER JOIN ThingTipsType B ON B.ThingTipsTypeID=C.ThingTipsTypeID and B.LanguageID=C.LanguageID and B.LanguageID=@LanguageID and C.LanguageID=@LanguageID
 -- INNER JOIN ThingTips E ON E.ThingTipsID=C.ThingTipsID and E.LanguageID=@LanguageID
  GROUP BY C.ThingTipsID,B.ThingTipsTypeID,ThingTipsType having B.ThingTipsTypeID=min(C.ThingTipsTypeID)

  UNION ALL

  SELECT CT.RowNumber + 1, C.ThingTipsID, cast (B.ThingTipsTypeID as varchar(20)) , CT.ThingTipsTypeIDs + ', ' + cast (B.ThingTipsType as varchar(20))
  FROM ThingTipsMapping C 
  INNER JOIN ThingTipsType B ON B.ThingTipsTypeID=C.ThingTipsTypeID and B.LanguageID=C.LanguageID and B.LanguageID=@LanguageID and C.LanguageID=@LanguageID
  --INNER JOIN ThingTips E ON E.ThingTipsID=C.ThingTipsID and E.LanguageID=@LanguageID
  INNER JOIN Duplicate CT ON CT.ThingTipsID = C.ThingTipsID 
  WHERE cast (C.ThingTipsTypeID as varchar(20)) > CT.ThingTipsTypeID
)


Select A.*,B.ThingTipsTypeIDs from

ThingTips A left join 
(
SELECT A.ThingTipsID, ThingTipsTypeIDs
from Duplicate A 
inner JOIN (SELECT ThingTipsID, MAX(RowNumber) 
AS MaxRow FROM Duplicate GROUP BY ThingTipsID) R
ON A.RowNumber = R.MaxRow AND A.ThingTipsID = R.ThingTipsID
) B

on A.ThingTipsID=B.ThingTipsID where  A.LanguageID=@LanguageID


End






