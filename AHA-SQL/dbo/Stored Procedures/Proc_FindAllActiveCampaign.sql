﻿

CREATE PROC [dbo].[Proc_FindAllActiveCampaign] 
(
	@LanguageID int,
	@Affiliate nvarchar(32),
	@CampaignURL nvarchar(32)
)
AS

--DECLARE	
--	@LanguageID int,
--	@Affiliate nvarchar(32),
--	@CampaignURL nvarchar(32)

--SET @LanguageID = 1
--SET @Affiliate = 'All'
--SET @CampaignURL = ''

BEGIN
SET NOCOUNT ON

IF @Affiliate = 'All' BEGIN SET @Affiliate = NULL END
IF @CampaignURL = NULL BEGIN SET @CampaignURL = '' END

SELECT 
	cd.CampaignID,
	cd.CampaignCode,
	cd.CampaignURL AS 'URL',
	cd.BusinessID, 
	cd.BusinessName, 
	cd.AffiliateID, 
	cd.AffiliateName,
	CONVERT(varchar, cd.StartDate, 101) AS StartDate,
	CONVERT(varchar, cd.EndDate, 101) AS EndDate,
	CONVERT(varchar, cd.CreatedDate, 101) AS CreatedDate,
	CONVERT(varchar, cd.UpdatedDate, 101) AS UpdatedDate,
	cd.LanguageID,
	cd.CampaignDescription AS 'Description',
	cd.CampaignTitle AS 'Title',
	cd.PromotionalTagLine,
	cd.PartnerLogoVersion,
	cd.PartnerImageVersion,
	cd.IsPublished,
	cd.Participants
	--case 
	--	when @LangCount=(select count(*) from CampaignDetails cda where cda.CampaignID=cd.CampaignID and isPublished=0)
	--	then 1  
	--	else 0 
	--end PublishToAll,

	--case 
	--	when @LangCount=(select count(*) from CampaignDetails cda where cda.CampaignID=cd.CampaignID and isPublished=1)
	--	then 1  
	--	else 0 
	--end NotPublishToAll

FROM v_CampaignDetails cd

	--LEFT JOIN CampaignDetails cd
	--	ON c.CampaignID=cd.CampaignID 
	--		AND LanguageID=@LanguageID

	--LEFT JOIN AdminBusiness ab
	--	ON c.BusinessID = ab.BusinessID

	--LEFT JOIN v_CampaignDetails ac
	--	ON c.CampaignID = ac.CampaignID
WHERE 
	(CONVERT(Datetime,CONVERT(Varchar,GETDATE(),103),103) BETWEEN StartDate AND EndDate)
	AND (
			(cd.AffiliateName = @Affiliate)
		OR	(@Affiliate IS NULL)		
	)
	AND ((cd.CampaignURL LIKE @CampaignURL + '%'))

--ORDER BY StartDate

END

