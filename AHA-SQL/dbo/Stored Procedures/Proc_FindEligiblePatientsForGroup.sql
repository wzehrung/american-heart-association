﻿--Proc_FindEligiblePatientsForGroup 19

CREATE PROCEDURE [dbo].[Proc_FindEligiblePatientsForGroup]
(
 @GroupID int
)

As

begin
set nocount on

declare @ProviderID int


SELECT @ProviderID=ProviderID from PatientGroupDetails where GroupID=@GroupID
;With Patient As
(
select PatientID from PatientGroupMapping where GroupID=@GroupID
)
,Patients As
(
	SELECT  
	distinct
	PGM.PatientID
	,case when P.PatientID is null  then 0 else 1 end As TobeAvoided

	from PatientGroupDetails  PGD 
	inner join
	PatientGroupMapping PGM 
	on PGD.GroupID=PGM.GroupID and  ProviderID=@ProviderID 
	left join Patient P on PGM.PatientID=P.PatientID
	
)


select H.* from HealthRecord H inner join Patients P
on  H.UserHealthRecordID =P.PatientId where P.TobeAvoided = 0

End
