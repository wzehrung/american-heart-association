﻿--Proc_Report_HyperTension_Bloodpressure @gender=null,@StartDate='2010.07.01',@EndDate='2010.08.17',@takeActiveUsers=1

CREATE PROC [dbo].[Proc_Report_HyperTension_Bloodpressure]
(
 @gender NVARCHAR(10)=NULL
,@campaignID int=null
,@providerID int=null
,@takeActiveUsers bit=0
,@StartDate Datetime
,@EndDate Datetime=null
)

As

Begin

if @EndDate is null
set @takeActiveUsers=0

Declare @HealthRecordIds table 
(
 UserHealthRecordID int
,Gender nvarchar(10)
primary key (UserHealthRecordID)
)

if @takeActiveUsers=1
begin

	if @providerID is  null

	insert @HealthRecordIds
	select distinct HR.UserHealthRecordID,Gender from HealthRecord HR inner join ExtendedProfile EP on HR.UserHealthRecordID=EP. UserHealthRecordID
	and (Gender=@gender  or @gender is null) and (HR.CampaignID=@campaignID or @campaignID is null) 
	inner join BloodPressure BP on  BP.UserHealthRecordID= HR.UserHealthRecordID where BP.DateMeasured<=@startDate+1 
	and (gender is not null and  gender <>'unknown')

	else

	insert @HealthRecordIds
	
	select distinct HR.UserHealthRecordID,Gender from PatientGroupDetails PGD 
	inner join 
	PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID  and PGD.ProviderID =@providerID
	--inner join AHAUserhealthRecord AHR on AHR.UserID=PGM.PatientID
	inner join HealthRecord HR on HR.UserHealthRecordID=PGM.PatientID and (HR.CampaignID=@campaignID or @campaignID is null)
	inner join ExtendedProfile EP on HR.UserHealthRecordID=EP. UserHealthRecordID and (Gender=@gender  or @gender is null)
	and (gender is not null and  gender <>'unknown')
	inner join BloodPressure BP on  BP.UserHealthRecordID= HR.UserHealthRecordID where BP.DateMeasured<=@startDate+1 


end

else

begin

	if @providerID is  null

	insert @HealthRecordIds
	select HR.UserHealthRecordID,Gender from HealthRecord HR inner join ExtendedProfile EP on HR.UserHealthRecordID=EP. UserHealthRecordID
	and (Gender=@gender  or @gender is null) and (HR.CampaignID=@campaignID or @campaignID is null) where (gender is not null and  gender <>'unknown')

	else

	insert @HealthRecordIds
	select distinct HR.UserHealthRecordID,Gender from PatientGroupDetails PGD 
	inner join 
	PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID  and PGD.ProviderID =@providerID
	--inner join AHAUserhealthRecord AHR on AHR.UserID=PGM.PatientID
	inner join HealthRecord HR on HR.UserHealthRecordID=PGM.PatientID and (HR.CampaignID=@campaignID or @campaignID is null)
	inner join ExtendedProfile EP on HR.UserHealthRecordID=EP. UserHealthRecordID and (Gender=@gender  or @gender is null)
	and (gender is not null and  gender <>'unknown')

end


Declare @StartData table
(
 Gender nvarchar(10)
,Healthy int
,HealthyPerc numeric(3,2)
,Acceptable int
,AcceptablePerc numeric(3,2)
,AtRisk int
,AtRiskPerc numeric(3,2)

)



Declare @EndData table
(
 Gender nvarchar(10)
,Healthy int
,HealthyPerc numeric(3,2)
,Acceptable int
,AcceptablePerc numeric(3,2)
,AtRisk int
,AtRiskPerc numeric(3,2)

)

;with BloodPressureIDs As
(
 select max(BloodPressureID) BloodPressureID  from BloodPressure where (DateMeasured<=@startDate+1)
 group by UserHealthrecordID
 )



insert @StartData

select 
	
	 Gender
	,sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end) Healthy
	
	,case when count(*) =0 then 0 else sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end)*1.0/count(*) end 
	
	,sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) then 1 else 0 end) Acceptable
	
	,case when count(*) =0 then 0 else sum(Case when (Systolic between 120 and 139) or 
		(Diastolic between 80 and 89) then 1 else 0 end)*1.0/count(*) end 
	
	,sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) 
	      then 0 else Case when (Systolic>139) or (Diastolic > 89) then 1 else 0 end end) AtRisk
	      
   ,case when count(*) =0 then 0 else sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) 
	      then 0 else Case when (Systolic>139) or (Diastolic > 89) then 1 else 0 end end)*1.0 /count(*) end	 


	      
from BloodPressure BP inner join BloodPressureIDs B on BP.BloodPressureID=B.BloodPressureID
 inner join 
@HealthRecordIds H on BP.UserHealthRecordID= H.UserHealthRecordID 
group by Gender



;with gender as
(
select 'Male' gender,0 Healthy,0 HealthyPerc,0 Acceptable,0 AcceptablePerc,0 AtRisk,0 AtRiskPerc
union all
select 'Female',0 Healthy,0 HealthyPerc,0 Acceptable,0 AcceptablePerc,0 AtRisk,0 AtRiskPerc
)
insert @StartData select * from gender A where not exists(select * from @StartData B where A.gender=B.Gender )
and ( A.gender=@gender or @gender is null)

select Gender,Healthy,HealthyPerc*100 HealthyPerc ,Acceptable,AcceptablePerc*100 AcceptablePerc,AtRisk,AtRiskPerc*100 AtRiskPerc

from @StartData


;with BloodPressureIDs As
(
 select max(BloodPressureID) BloodPressureID  from BloodPressure where (DateMeasured<=@EndDate+1 )
 group by UserHealthrecordID
 )


insert @EndData

select 
	
	 Gender
	,sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end) Healthy
	
	,case when count(*) =0 then 0 else sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end)*1.0/count(*) end 
	
	,sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) then 1 else 0 end) Acceptable
	
	,case when count(*) =0 then 0 else sum(Case when (Systolic between 120 and 139) or 
		(Diastolic between 80 and 89) then 1 else 0 end)*1.0/count(*) end 
	
	,sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) 
	      then 0 else Case when (Systolic>139) or (Diastolic > 89) then 1 else 0 end end) AtRisk
	      
   ,case when count(*) =0 then 0 else sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) 
	      then 0 else Case when (Systolic>139) or (Diastolic > 89) then 1 else 0 end end)*1.0 /count(*) end	 


	      
from BloodPressure BP inner join BloodPressureIDs B on BP.BloodPressureID=B.BloodPressureID
inner join @HealthRecordIds H on BP.UserHealthRecordID= H.UserHealthRecordID 

group by Gender


;with gender as
(
select 'Male' gender,0 Healthy,0 HealthyPerc,0 Acceptable,0 AcceptablePerc,0 AtRisk,0 AtRiskPerc
union all
select 'Female',0 Healthy,0 HealthyPerc,0 Acceptable,0 AcceptablePerc,0 AtRisk,0 AtRiskPerc
)
insert @EndData select * from gender A where not exists(select * from @EndData B where A.gender=B.Gender )
and ( A.gender=@gender or @gender is null)

select Gender,Healthy,HealthyPerc*100 HealthyPerc ,Acceptable,AcceptablePerc*100 AcceptablePerc,AtRisk,AtRiskPerc*100 AtRiskPerc

from @EndData


select 

 A.gender 
,A.Healthy-B.Healthy Healthy
,(A.HealthyPerc-B.HealthyPerc)*100 HealthyPerc

,A.Acceptable-B.Acceptable Acceptable
,(A.AcceptablePerc-B.AcceptablePerc)*100 AcceptablePerc

,A.AtRisk-B.AtRisk AtRisk
,(A.AtRiskPerc-B.AtRiskPerc)*100 AtRiskPerc

from @EndData A INNER join @StartData B on A.Gender=B.Gender


End
