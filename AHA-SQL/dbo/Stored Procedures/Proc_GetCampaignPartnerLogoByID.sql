﻿
CREATE PROC [dbo].[Proc_GetCampaignPartnerLogoByID]
(
 @CampaignID int
,@LanguageID int
)

As


Begin
set nocount on

select  CD.PartnerLogo,CD.PartnerLogoContentType,CD.PartnerLogoVersion 
,case when Convert(Datetime,Convert(Varchar,getdate(),103),103) between StartDate and  EndDate  and isPublished=1 
then 1 else 0 end As isActive

from Campaign C inner join CampaignDetails CD on C.CampaignID=CD.CampaignID AND C.CampaignID=@CampaignID
where LanguageID=@LanguageID


End



