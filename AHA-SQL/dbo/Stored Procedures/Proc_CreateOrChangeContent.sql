﻿CREATE PROC [dbo].[Proc_CreateOrChangeContent]
(
 @Title nvarchar(200)
,@DidYouKnowLinkUrl nvarchar(1024)
,@DidYouKnowLinkTitle nvarchar(max)
,@DidYouKnowText nvarchar(max)
,@ContentRuleIDs nvarchar(max)
,@ExpirationDate datetime
,@UpdatedByUserID int
,@LanguageID int
,@ContentID int output
)

As

Begin
set nocount on

Begin Try
Begin Transaction

if exists (select * from Content where ContentID=@ContentID and LanguageID=@LanguageID)

exec Proc_ChangeContent

 @Title
,@DidYouKnowLinkUrl 
,@DidYouKnowLinkTitle 
,@DidYouKnowText 
,@ContentID 
,@ContentRuleIDs
,@ExpirationDate   
,@UpdatedByUserID  
,@LanguageID 

else

exec Proc_CreateContent

 @Title 
,@DidYouKnowLinkUrl 
,@DidYouKnowLinkTitle 
,@DidYouKnowText 
,@ContentRuleIDs 
,@ExpirationDate 
,@UpdatedByUserID 
,@LanguageID 
,@ContentID output




COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End










