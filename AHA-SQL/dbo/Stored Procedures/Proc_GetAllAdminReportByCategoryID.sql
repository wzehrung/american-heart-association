﻿CREATE PROC [dbo].[Proc_GetAllAdminReportByCategoryID]
(
@CategoryID int
)
As

Begin
set nocount on

SELECT  a.*, '1' as Enabled FROM AdminReport a 
WHERE CategoryID = @CategoryID

End
