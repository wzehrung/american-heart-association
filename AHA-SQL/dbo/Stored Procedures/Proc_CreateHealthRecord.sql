﻿
CREATE Procedure Proc_CreateHealthRecord
(
 @UserHealthRecordGUID uniqueidentifier 
,@PersonalItemGUID uniqueidentifier
,@UserGUID uniqueidentifier  = NULL
,@HealthRecordID int OutPut
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      : Create HealthRecord in 2 steps
					 Case 1 : PersonalItemGUID should not have existed in database
					 Step :1 Populate HealthRecord table
					 Step :2 Populate AHAUserHealthRecord table

					Case 2 : PersonalItemGUID already exists in database
					 Step :1 Update HealthRecord table
					 Step :2 Populate AHAUserHealthRecord table
*/

set nocount on

Begin Try
Begin Transaction

--************************* Case 1  *************************
if not exists 
(
select PersonalItemGUID from dbo.HealthRecord 
										  where  PersonalItemGUID=@PersonalItemGUID
) 

Begin
	IF NOT  EXISTS (SELECT * FROM dbo.HealthRecord  WHERE UserHealthRecordGUID=@UserHealthRecordGUID)
	BEGIN
		Declare @UserHealthRecordID int

		Exec Proc_InsertHealthRecord 
									 
									  
									 @UserHealthRecordGUID
									,@PersonalItemGUID
									,@UserHealthRecordID  OutPut

		set @HealthRecordID=@UserHealthRecordID
			
		Exec Proc_InsertAHAUserHealthRecord @UserGUID,@UserHealthRecordID 
	END
	ELSE
	begin
	DECLARE @PersonalItemGUIDold uniqueidentifier
	set @PersonalItemGUIDold=(SELECT PersonalItemGUID FROM dbo.HealthRecord  WHERE UserHealthRecordGUID=@UserHealthRecordGUID)
	Exec Proc_ChangeHealthRecord @UserHealthRecordGUID,@PersonalItemGUID,@PersonalItemGUIDold
    end
End

Else
begin

--************************* Case 2  *************************
Exec Proc_ChangeHealthRecord @UserHealthRecordGUID,@PersonalItemGUID,@PersonalItemGUID

--************************* End of Case 1  *************************
end


COMMIT

End Try

Begin Catch

	If @@TRANCOUNT > 0
	ROLLBACK

	set @HealthRecordID=NULL
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

End Catch

End
