﻿--Proc_Report_Additional_Cholesterol '2010.06.01'
CREATE PROC [dbo].[Proc_Report_Additional_Cholesterol]
(
 @StartDate datetime
,@EndDate datetime
,@ProviderID int
,@campaignID int
)

AS

Begin

declare @startUserHealthrecordIDs table
(
UserHealthrecordID int
primary key (UserHealthrecordID)
)


		if @ProviderID is not null

		insert @startUserHealthrecordIDs
		select  C.UserHealthrecordID from 
		PatientGroupDetails PGD 
		inner join PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID  and PGD.ProviderID =@providerID
		inner join HealthRecord HR on HR.UserHealthRecordID=PGM.PatientID and (HR.CampaignID=@campaignID or @campaignID is null)
		inner join Cholesterol  C  on HR.UserHealthrecordID=C.UserHealthRecordID
		where DateMeasured >=@StartDate and DateMeasured<dateadd(month,1,@StartDate) group by C.UserHealthrecordID

		else

		insert @startUserHealthrecordIDs
		select  C.UserHealthrecordID from Cholesterol C 
		inner join HealthRecord HR on HR.UserHealthRecordID=C.UserHealthRecordID and (HR.CampaignID=@campaignID or @campaignID is null)
		where DateMeasured >=@StartDate and DateMeasured<dateadd(month,1,@StartDate)
		group by C.UserHealthrecordID



;with CholesterolIDs As
(
select max(C.CholesterolID) CholesterolID,D.Date1  from Cholesterol C inner join 
@startUserHealthrecordIDs shi on shi.UserHealthrecordID=C.UserHealthrecordID
inner join  dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) D on
C.DateMeasured>=D.Date1 and  C.DateMeasured<D.Date2 
group by C.UserHealthrecordID,D.Date1
)

select 

UDR.Date1 
,sum(Case when TotalCholesterol<200  then 1 else 0 end) Healthy
,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when TotalCholesterol<200 then 1 else 0 end)*1.0
/(select count(*) from @startUserHealthrecordIDs)  end *100 HealthyPerc

,sum(Case when TotalCholesterol between 200 and 239 then 1 else 0 end) Acceptable

,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when TotalCholesterol between 200 and 239 then 1 else 0 end)*1.0
/(select count(*) from @startUserHealthrecordIDs) end *100 AcceptablePerc

,sum(Case when TotalCholesterol >=240  then 1 else 0 end ) AtRisk

,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(case when TotalCholesterol >=240  then 1 else 0 end) *1.0 
/(select count(*) from @startUserHealthrecordIDs) end	*100 AtRiskPerc

,

(select count(*) from @startUserHealthrecordIDs)-
(
sum(Case when TotalCholesterol<200  then 1 else 0 end)
+sum(Case when TotalCholesterol between 200 and 239 then 1 else 0 end)
+sum(Case when TotalCholesterol >=240  then 1 else 0 end)
)
inactiveUsers

,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else
(
(select count(*) from @startUserHealthrecordIDs)-
(
sum(Case when TotalCholesterol<200  then 1 else 0 end)
+sum(Case when TotalCholesterol between 200 and 239 then 1 else 0 end)
+sum(Case when TotalCholesterol >=240  then 1 else 0 end)
) 
)		
*1.0 
/(select count(*) from @startUserHealthrecordIDs) end	*100 inactiveUsersPerc


from dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) UDR left join 

(
select Date1,TotalCholesterol from Cholesterol CD   inner join CholesterolIDs CID on CD.CholesterolID=CID.CholesterolID
)

C on C.Date1=UDR.Date1 


group by 	UDR.Date1 		
	
End
