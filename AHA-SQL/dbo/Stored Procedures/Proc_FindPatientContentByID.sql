﻿CREATE PROC [dbo].[Proc_FindPatientContentByID]
(
 @LanguageID int
,@PatientContentID int = 0 
)

As

Begin

	select * from PatientContent
	where  PatientContentID = @PatientContentID and LanguageID=@LanguageID 

End
