﻿

CREATE PROCEDURE [dbo].[Proc_Get_AnnouncementsForPatient]
(
--@IsProvider bit,
@LanguageID int,
@PatientID int
)
As
Begin
set nocount on

SELECT DISTINCT
	pc.PatientContentID
	--,pc.ZoneTitle
	,pc.ContentTitle AS AnnouncementTitle
	,pc.ContentText AS AnnouncementText
	--,pl.ContentText AS ContentTextPL
	--,pc.CreatedByUserID
	--,pc.UpdatedByUserID
	,pc.ProviderID
	,ISNULL(p.Salutation, '') + ' ' + dbo.ProperCase(p.FirstName) + ' ' + dbo.ProperCase(p.LastName) AS ProviderName
	,Convert(varchar, ISNULL(pc.UpdatedDate, GETDATE()), 101)AS UpdatedDate
	,Convert(varchar, ISNULL(pc.CreatedDate, GETDATE()), 101)AS CreatedDate
	,pc.LanguageID
	,pgm.PatientID

FROM PatientContent pc

	INNER JOIN Provider p
		ON pc.ProviderID = p.ProviderID

    INNER JOIN PatientGroupDetails pgd
        ON p.ProviderID = pgd.ProviderID
                  
    INNER JOIN PatientGroupMapping pgm 
        ON pgd.GroupID=PGM.GroupID AND pgm.PatientID=@PatientID

WHERE pc.ProviderID IS NOT NULL
	AND pc.LanguageID = @LanguageID

END
