﻿



CREATE PROC [dbo].[Proc_ChangeContent]
(
 @Title nvarchar(200)
,@DidYouKnowLinkUrl nvarchar(1024)
,@DidYouKnowLinkTitle nvarchar(max)
,@DidYouKnowText nvarchar(max)
,@ContentID int
,@ContentRuleIDs nvarchar(max)
,@ExpirationDate datetime  
,@UpdatedByUserID int 
,@LanguageID int
)

As

Begin
set nocount on

Begin Try
Begin Transaction

UpDate Content 
set 

	 Title = @Title
	,DidYouKnowLinkUrl =@DidYouKnowLinkUrl
	,DidYouKnowLinkTitle = @DidYouKnowLinkTitle
	,DidYouKnowText = @DidYouKnowText
	,ExpirationDate =@ExpirationDate  
    ,UpdatedByUserID = @UpdatedByUserID  

where ContentID=@ContentID and LanguageID=@LanguageID


Delete from ContentRuleMapping where ContentID=@ContentID and LanguageID=@LanguageID


set @ContentRuleIDs=@ContentRuleIDs+','

		;WITH Strings(Segments, Processing)
		AS
		(
		SELECT
				 SUBSTRING(@ContentRuleIDs,1, CHARINDEX(',', @ContentRuleIDs)-1) Segments
				,SUBSTRING(@ContentRuleIDs,CHARINDEX(',', @ContentRuleIDs)+1, len(@ContentRuleIDs)) Processing

				UNION ALL

				SELECT
				 SUBSTRING(Processing,1, CHARINDEX(',', Processing)-1) Segments
				,SUBSTRING(Processing,CHARINDEX(',', Processing)+1, len(Processing)) Processing

				FROM Strings

				WHERE
				CHARINDEX(',', Processing) > 0
		)


Insert ContentRuleMapping (ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,LanguageID)

select Segments,@ContentID,(select CreatedByUserID from content where ContentID=@ContentID and LanguageID=@LanguageID),@UpdatedByUserID,@LanguageID from Strings



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End




