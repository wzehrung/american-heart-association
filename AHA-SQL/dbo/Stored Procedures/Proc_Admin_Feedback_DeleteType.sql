﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.17.2014
-- Description:	Delete feedback types used in the feedback form on the patient portal
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_DeleteType]
(
    @FeedbackTypeID int
)

AS

BEGIN
SET NOCOUNT ON

DELETE FROM FeedbackTypes
WHERE FeedbackTypeID = @FeedbackTypeID

END