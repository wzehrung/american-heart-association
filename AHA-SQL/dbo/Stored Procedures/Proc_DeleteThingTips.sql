﻿





CREATE PROC [dbo].[Proc_DeleteThingTips]
(
@ThingTipsid int
,@LanguageID int
)

As

Begin
set nocount on

Begin Try
Begin Transaction


Delete from ThingTipsMapping where ThingTipsid=@ThingTipsid and LanguageID=@LanguageID


Delete from ThingTips Where ThingTipsid=@ThingTipsid and LanguageID=@LanguageID

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End




