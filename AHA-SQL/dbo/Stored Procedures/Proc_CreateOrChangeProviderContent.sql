﻿
CREATE PROC [dbo].[Proc_CreateOrChangeProviderContent]
(
 @ZoneTitle nvarchar(50)
,@ContentTitle nvarchar(80)
,@ContentTitleUrl nvarchar(255)
,@ContentText nvarchar(MAX)
,@CreatedByUserID int
,@LanguageID int
,@ContentID INT
)

As

Begin


if exists (select * from ProviderContent where ZoneTitle=@ZoneTitle and LanguageID=@LanguageID)
exec Proc_ChangeProviderContent @ZoneTitle ,@ContentTitle ,@ContentTitleUrl ,@ContentText ,@CreatedByUserID ,@LanguageID 
else
exec Proc_CreateProviderContent @ZoneTitle ,@ContentTitle ,@ContentTitleUrl ,@ContentText ,@CreatedByUserID ,@LanguageID,@ContentID

End

