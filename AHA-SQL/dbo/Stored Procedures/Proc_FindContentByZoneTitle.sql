﻿

CREATE PROC [dbo].[Proc_FindContentByZoneTitle]
(
@ZoneTitle nvarchar(50)
,@LanguageID int
)
As

Begin 
set nocount on

select * from ProviderContent where ZoneTitle=@ZoneTitle and LanguageID=@LanguageID

END


