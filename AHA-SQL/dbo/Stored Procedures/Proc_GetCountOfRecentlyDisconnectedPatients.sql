﻿
CREATE proc [dbo].[Proc_GetCountOfRecentlyDisconnectedPatients]
(
 @ProviderID int
,@NoOfDays int
,@count int output
)
As
Begin

  SELECT @count=count(Distinct DP.PatientID)
  FROM PatientGroupMapping PGM 
  INNER JOIN DisconnectedPatients DP on DP.PatientID = PGM.PatientID
  INNER JOIN PatientGroupDetails PGD ON   PGD.GroupID=PGM.GroupID and PGD.ProviderID=@ProviderID
  and isDefault=1  and DP.date between dateadd(day,-1*@NoOfDays,getdate()) and getdate()

End