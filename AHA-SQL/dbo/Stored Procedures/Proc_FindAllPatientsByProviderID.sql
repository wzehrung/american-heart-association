﻿CREATE PROCEDURE [dbo].[Proc_FindAllPatientsByProviderID]
(
@ProviderID int
)
As
Begin
set nocount on


	
;WITH Duplicate (RowNumber,PatientID,GroupID,GroupIDs) AS
(
  SELECT 1
		,PGM.PatientID
        ,cast (min(PGD.GroupID) as varchar(20))
		,CAST(min(PGD.GroupID) AS VARCHAR(8000))

  FROM PatientGroupMapping PGM 
  INNER JOIN PatientGroupDetails PGD ON   PGD.GroupID=PGM.GroupID and PGD.ProviderID=@ProviderID
  GROUP BY PatientID 

  UNION ALL

  SELECT CT.RowNumber + 1
		, PGM.PatientID
		, cast(PGD.GroupID as varchar(20))  
		,CT.GroupIDs+',' +cast(PGD.GroupID as varchar(20))

  FROM PatientGroupMapping PGM 
  INNER JOIN PatientGroupDetails PGD ON   PGD.GroupID=PGM.GroupID and PGD.ProviderID=@ProviderID
  INNER JOIN Duplicate CT ON CT.PatientID = PGM.PatientID 
  WHERE PGM.GroupID > CAST(CT.GroupID AS INT)
),

Results As
(
SELECT A.PatientID, GroupIDs
from Duplicate A 
inner JOIN (SELECT PatientID, MAX(RowNumber) 
AS MaxRow FROM Duplicate GROUP BY PatientID) R
ON A.RowNumber = R.MaxRow AND A.PatientID = R.PatientID
)

select P.*, isnull(TotalAlerts,0) as TotalAlerts,R.GroupIDs
,PGM.Date ProviderConnectionDate

from Results R inner join
HealthRecord P on P.UserHealthRecordID=R.PatientID
inner join PatientGroupMapping  PGM ON PGM.PatientID=P.UserHealthRecordID
inner join PatientGroupDetails PGD ON PGD.GroupID=PGM.GroupID  and PGD.isdefault=1  and PGD.ProviderID=@ProviderID
LEFT JOIN 
(

select A.PatientID,count(distinct AlertID) TotalAlerts from alertRule AR inner join
alert A on AR.AlertRuleID=A.AlertRuleID AND A.isDismissed=0 and ProviderID=@ProviderID

group by A.PatientID
) A

on A.PatientID=R.PatientID

	
End







