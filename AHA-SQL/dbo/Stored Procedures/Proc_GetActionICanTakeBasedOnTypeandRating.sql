﻿--[Proc_GetActionICanTakeBasedOnTypeandRating] 3,'Medication',1

CREATE PROC [dbo].[Proc_GetActionICanTakeBasedOnTypeandRating]
(
 @Rating int
,@Type nvarchar(254)
,@LanguageID int
)
As

Begin

if @Type='Medication' 
set @Rating=null

select MIT.* from  MLCType  MT 
inner join  MLCActionICanTake MIT 
		 on (MT.MLCTypeID=MIT.MLCTypeID) 
		and (MT.LanguageID=MIT.LanguageID)
		and MT.LanguageID=@LanguageID 
		and MT.MLCTypeName=@Type
		and (MIT.Rating=@Rating or (MIT.Rating is null and @rating is null))
		and MIT.LanguageID =@LanguageID

End

