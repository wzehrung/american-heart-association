﻿
CREATE PROCEDURE [dbo].[Proc_FindEligibleGroupsForPatient]
(
 @PatientID int
,@ProviderID int
)

As

begin
set nocount on

SELECT * from PatientGroupDetails WHERE groupID NOT IN
(
select  GroupID from PatientGroupMapping where PatientID = @PatientID
) 

AND GroupID IN (select GroupID from PatientGroupDetails where ProviderID=@ProviderID)

AND ISDEFAULT=0

End
