﻿
CREATE PROC [dbo].[Proc_GetResourcesExpiringOn]
(
 @ExpirationDate DATETIME
)
As

Begin
set nocount on

select * from  Resources where Convert(datetime,Convert(varchar,ExpirationDate,103),103)=@ExpirationDate

End