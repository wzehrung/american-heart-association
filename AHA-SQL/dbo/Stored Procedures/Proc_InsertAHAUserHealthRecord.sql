﻿
CREATE Procedure [dbo].[Proc_InsertAHAUserHealthRecord]
(
  @UserGUID uniqueidentifier  
 ,@UserHealthRecordID int 
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a record to AHAUserHealthRecord table
*/

Declare @UserID int

set @UserID =(select UserID from dbo.AHAuser where PersonGUID=@UserGUID)


if not exists (select * from dbo.AHAUserHealthRecord 
			   where UserID=@UserID and UserHealthRecordID=	@UserHealthRecordID)

Insert  dbo.AHAUserHealthRecord 
						   (
							UserID
						   ,UserHealthRecordID
							)
Select 
						   @UserID
						  ,@UserHealthRecordID
End






