﻿CREATE PROCEDURE [dbo].[Proc_CreateProvider]
(
 @UserName nvarchar(255)
,@Email nvarchar(255)
,@Password nvarchar(255)
,@FirstName nvarchar(50)
,@LastName nvarchar(50)
,@Salutation nvarchar(50)
,@PracticeName nvarchar(50)
,@PracticeEmail nvarchar(50)
,@PracticeAddress nvarchar(50)
,@PracticePhone nvarchar(50)
,@PracticeFax nvarchar(50)
,@Speciality nvarchar(50)
,@PharmacyPartner nvarchar(50)
,@ProfileMessage nvarchar(max)
,@PracticeUrl nvarchar(255)
,@City nvarchar(50)
,@StateID int
,@Zip nvarchar(50)
--,@MobileForNotification nvarchar(50)
--,@EmailForNotification nvarchar(50)
,@AllowPatientsToSendMessages bit
,@DismissAlertAfterWeeks int
,@IncludeAlertNotificationInDailySummary bit
,@IncludeMessageNotificationInDailySummary bit
,@NotifyNewAlertImmediately bit
,@NotifyNewMessageImmediately bit
,@EmailForDailySummary nvarchar(255)
,@EmailForIndividualAlert nvarchar(255)
,@EmailForIndividualMessage nvarchar(255)
,@AllowAHAEmail bit
,@LanguageID int
,@UserTypeID int
,@ProviderID int OutPut

)
As
Begin

set nocount on


Begin Try
Begin Transaction

exec Proc_InsertProvider
 @UserName,@Email,@Password,@FirstName,@LastName,@Salutation
,@PracticeName,@PracticeEmail,@PracticeAddress
,@PracticePhone,@PracticeFax,@Speciality,@PharmacyPartner
,@ProfileMessage ,@PracticeUrl ,@City,@StateID ,@Zip
,@AllowPatientsToSendMessages
,@DismissAlertAfterWeeks,@IncludeAlertNotificationInDailySummary, @IncludeMessageNotificationInDailySummary
,@NotifyNewAlertImmediately, @NotifyNewMessageImmediately,@EmailForDailySummary
,@EmailForIndividualAlert ,@EmailForIndividualMessage,@AllowAHAEmail,@LanguageID,@UserTypeID
,@ProviderID  OutPut



insert PatientGroupDetails(Name,ProviderID,isDefault)
select 'DefaultGroupfor - '+@FirstName,@ProviderID,1

 insert UserFolderMapping(ProviderID,FolderID)
 select @ProviderID,FolderID from Folder

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

	
End

/*************************************************************************************************************************/


/****** Object:  StoredProcedure [dbo].[Proc_InsertProvider]    Script Date: 10/12/2012 00:12:05 ******/
SET ANSI_NULLS ON
