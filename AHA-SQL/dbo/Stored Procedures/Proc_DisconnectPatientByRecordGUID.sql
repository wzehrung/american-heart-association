﻿
create PROCEDURE Proc_DisconnectPatientByRecordGUID
(
 @ProviderID int
,@UserHealthRecordGUID uniqueidentifier
)
As
Begin
set nocount on

declare @PatientId int
select @PatientId = UserHealthRecordID from healthrecord where UserHealthRecordGUID = @UserHealthRecordGUID

if(@PatientId is null)
	return
	
Begin Try
Begin Transaction



	Delete PGM from

	PatientGroupDetails PGD inner join 
	PatientGroupMapping PGM  on PGD.GroupID=PGM.GroupID and PGD.ProviderID=@ProviderID
								 and PGM.PatientID=@PatientId


delete A from AlertMapping A inner join AlertRule B
on A.AlertRuleID=B.AlertRuleID and B.ProviderID=@ProviderID
and A.PatientID=@PatientId

delete A from Alert A inner join AlertRule B
on A.AlertRuleID=B.AlertRuleID and B.ProviderID=@ProviderID
and A.PatientID=@PatientId


delete A from AlertRule A where ProviderID=@ProviderID and
not exists(select * from alertMapping where AlertRuleID=A.AlertRuleID)
and not exists(select * from Alert where AlertRuleID=A.AlertRuleID)

insert DisconnectedPatients(ProviderID,PatientID) select @ProviderID,@PatientID




	
Delete from MessageDetails where UserFolderMappingID in 
(select UserFolderMappingID from UserFolderMapping where ProviderID=@ProviderID)

delete M  from [Message] M inner join
(
select MessageID from MessageDetails where UserFolderMappingID in 
(select UserFolderMappingID from UserFolderMapping where ProviderID=@ProviderID)
) A on A.MessageID=M.MessageID where not exists

(select * from MessageDetails MD where MD.MessageID=M.MessageID)




	
Delete  from MessageDetails where UserFolderMappingID in 
(select UserFolderMappingID from UserFolderMapping where PatientId=@PatientId)

delete M from [Message] M inner join
(
select MessageID from MessageDetails where UserFolderMappingID in 
(select UserFolderMappingID from UserFolderMapping where PatientId=@PatientId)
) A on A.MessageID=M.MessageID where not exists

(select * from MessageDetails MD where MD.MessageID=M.MessageID)





COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

	
End
