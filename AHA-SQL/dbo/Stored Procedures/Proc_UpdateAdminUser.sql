﻿CREATE PROC [dbo].[Proc_UpdateAdminUser]
(
	 @AdminUserID int
	,@Name nvarchar(128)
	,@Email nvarchar(128)
	,@RoleID int
	,@BusinessID int
)

AS

BEGIN
SET NOCOUNT ON

UPDATE AdminUser set 
	Name		= @Name,
	Email		= @Email,
	RoleID		= @RoleID,
	BusinessID	= @BusinessID

WHERE  AdminUserID = @AdminUserID

END

