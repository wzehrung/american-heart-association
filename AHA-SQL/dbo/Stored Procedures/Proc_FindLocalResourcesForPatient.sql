﻿CREATE PROCEDURE [dbo].[Proc_FindLocalResourcesForPatient]
(
@IsProvider bit,
@LanguageID int,
@PatientID int
)
As
Begin
set nocount on

 select * from Resources WHERE LanguageID=@LanguageID --and IsProvider=@IsProvider 
   and ProviderID in 
   (
     select distinct ProviderID 
	 from PatientGroupDetails  PGD 
	 inner join
	 PatientGroupMapping PGM 
	 on PGD.GroupID=PGM.GroupID and  PGM.PatientID=@PatientID 
	)
  ORDER BY SortOrder asc
End
