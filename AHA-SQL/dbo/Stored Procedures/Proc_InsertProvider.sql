﻿CREATE PROCEDURE [dbo].[Proc_InsertProvider]
(
 @UserName nvarchar(255)
,@Email nvarchar(255),@Password nvarchar(255)
,@FirstName nvarchar(50)
,@LastName nvarchar(50)
,@Salutation nvarchar(50)
,@PracticeName nvarchar(50)
,@PracticeEmail nvarchar(50)
,@PracticeAddress nvarchar(50)
,@PracticePhone nvarchar(50)
,@PracticeFax nvarchar(50)
,@Speciality nvarchar(50)
,@PharmacyPartner nvarchar(50)
,@ProfileMessage nvarchar(max)
,@PracticeUrl nvarchar(255)
,@City nvarchar(50)
,@StateID int
,@Zip nvarchar(50)
--,@MobileForNotification nvarchar(50)
--,@EmailForNotification nvarchar(50)
,@AllowPatientsToSendMessages bit
,@DismissAlertAfterWeeks int
,@IncludeAlertNotificationInDailySummary bit
,@IncludeMessageNotificationInDailySummary bit
,@NotifyNewAlertImmediately bit
,@NotifyNewMessageImmediately bit
,@EmailForDailySummary nvarchar(255)
,@EmailForIndividualAlert nvarchar(255)
,@EmailForIndividualMessage nvarchar(255)
,@AllowAHAEmail bit
,@LanguageID int
,@UserTypeID int
,@ProviderID int OutPut
)
As
Begin

set nocount on

declare @ProviderCode nvarchar(6)
exec GenerateProviderCode @ProviderCode output


INSERT provider 
(
 UserName,Email,Password,FirstName,LastName,Salutation
 ,PracticeName,PracticeEmail,PracticeAddress
 ,PracticePhone,PracticeFax,Speciality,PharmacyPartner
 ,DateOfRegistration ,ProfileMessage ,PracticeUrl 
 ,City ,StateID ,Zip,AllowPatientsToSendMessages
,DismissAlertAfterWeeks,IncludeAlertNotificationInDailySummary
, IncludeMessageNotificationInDailySummary, NotifyNewAlertImmediately, NotifyNewMessageImmediately
,EmailForDailySummary
,EmailForIndividualAlert ,EmailForIndividualMessage,ProviderCode,AllowAHAEmail,DefaultLanguageID,UserTypeID
)

SELECT

@UserName,@Email,@Password,@FirstName,@LastName,@Salutation
,@PracticeName,@PracticeEmail,@PracticeAddress
,@PracticePhone,@PracticeFax,@Speciality,@PharmacyPartner
,getdate() ,@ProfileMessage ,@PracticeUrl,@City 
,@StateID ,@Zip,@AllowPatientsToSendMessages
,@DismissAlertAfterWeeks,@IncludeAlertNotificationInDailySummary, @IncludeMessageNotificationInDailySummary
, @NotifyNewAlertImmediately, @NotifyNewMessageImmediately,@EmailForDailySummary
,@EmailForIndividualAlert ,@EmailForIndividualMessage,@ProviderCode,@AllowAHAEmail,@LanguageID,@UserTypeID

SET @ProviderID=SCOPE_IDENTITY()
	
End


/****** Object:  Table [dbo].[AffiliateRegion]    Script Date: 10/14/2012 16:47:26 ******/
SET ANSI_NULLS ON
