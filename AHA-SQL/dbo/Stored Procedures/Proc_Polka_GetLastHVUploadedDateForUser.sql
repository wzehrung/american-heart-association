﻿
CREATE PROC [dbo].[Proc_Polka_GetLastHVUploadedDateForUser]
(
 @PersonalItemGUID Uniqueidentifier
)
As


Begin 


	declare @UserHealthRecordID int

	set @UserHealthRecordID =(select UserHealthRecordID  from HealthRecord where PersonalItemGUID=@PersonalItemGUID)


select max(DateCreated) LastHVUploadedDate from PolkaData where UserHealthRecordID=@UserHealthRecordID 



End