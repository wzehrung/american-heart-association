﻿

create  PROCEDURE [dbo].[Proc_Report_BloodPressureData_WZ]
(
	@StartDate datetime,
	@EndDate datetime 
)
AS
BEGIN
SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

SELECT
	[UserHealthRecordID],
	[JoinedDate],
	[Systolic],
	[Diastolic],
	[Gender],
	[Ethnicity],
	[Year],
	[Month],
	[Day],
	[DateMeasured]
	[CampaignID],
	[Campaign],
	[AffiliateID],
	[AffiliateName],
	[ZipCode]

FROM [dbo].[v_Reporting_BloodPressure]

WHERE DateMeasured BETWEEN @StartDate AND @EndDate AND (CampaignID <> '122')

	AND CampaignID IN
	(
		SELECT CampaignID FROM v_Reporting_CCC_Campaigns
		WHERE EndDate > @StartDate
	)

COMMIT
END TRY

BEGIN CATCH

  If @@TRANCOUNT > 0
	ROLLBACK
	
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

 END CATCH
	
END

