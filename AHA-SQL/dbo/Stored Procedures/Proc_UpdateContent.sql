﻿

CREATE PROC [dbo].[Proc_UpdateContent]
(
 @ZoneTitle nvarchar(50)
,@ContentTitle nvarchar(80)
,@ContentTitleUrl nvarchar(255)
,@ContentText nvarchar(MAX)
,@UpdatedByUserID int
)

As

Begin

Update ProviderContent 

set 

 ContentTitle = @ContentTitle
,ContentTitleUrl = @ContentTitleUrl
,ContentText = @ContentText
,UpdatedByUserID=@UpdatedByUserID

where ZoneTitle=@ZoneTitle

End