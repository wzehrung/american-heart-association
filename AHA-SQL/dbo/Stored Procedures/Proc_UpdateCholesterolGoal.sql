﻿

CREATE Procedure [dbo].[Proc_UpdateCholesterolGoal]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Totalcholesterol int
,@LDL int
,@HDL int 
,@Triglyceride int
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand  
   Created Date     :2008-08-12    
   Description      :Updating records of CholesterolGoal

   Note : Please dont try to update a indexed foreign key 
*/  

Update dbo.CholesterolGoal

				   SET 
				   HVVersionStamp	 = @HVVersionStamp
				  ,Totalcholesterol  = @Totalcholesterol
				  ,LDL				 = @LDL
				  ,HDL				 = @HDL 
				  ,Triglyceride		 = @Triglyceride 
                  ,UpdatedDate		 =  GetDate()

                    
		Where HVItemGuid=@HVItemGuid 
					
End


	





