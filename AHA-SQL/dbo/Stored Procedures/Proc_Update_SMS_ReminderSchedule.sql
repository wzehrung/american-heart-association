﻿
CREATE Procedure [dbo].[Proc_Update_SMS_ReminderSchedule]
(
	@ReminderScheduleID Int
	,@ReminderID Int
	,@Message varchar(160)
	,@Monday bit
	,@Tuesday bit
	,@Wednesday bit
	,@Thursday bit
	,@Friday bit
	,@Saturday bit
	,@Sunday bit
	,@Time varchar(5)
	,@DisplayTime varchar (10)
	,@ReminderCategoryID Int
)

AS

BEGIN

SET NOCOUNT ON

UPDATE [3CiPatientReminderSchedule] 

SET
	ReminderID			= @ReminderID
	,Message			= @Message	
	,Monday				= @Monday
	,Tuesday			= @Tuesday
	,Wednesday			= @Wednesday
	,Thursday			= @Thursday
	,Friday				= @Friday	
	,Saturday			= @Saturday
	,Sunday				= @Sunday
	,Time				= @Time
	,DisplayTime		= @DisplayTime
	,ReminderCategoryID	= @ReminderCategoryID
	,CreatedDateTime	= getdate()
	,UpdatedDateTime	= getdate()

WHERE
	ReminderScheduleID	= @ReminderScheduleID

End