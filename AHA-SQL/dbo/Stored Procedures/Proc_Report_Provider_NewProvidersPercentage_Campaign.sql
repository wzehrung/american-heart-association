﻿CREATE PROC [dbo].[Proc_Report_Provider_NewProvidersPercentage_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@UserTypeID int = null
,@PaRTNERid INT = NULL
)
As

BEGIN
set nocount on


DECLARE @cols NVARCHAR(4000)
DECLARE @query NVARCHAR(4000)
DECLARE @ColsConv NVARCHAR(4000) -- NEW

;with RptTypes As
(
select 1 RptID,'1 Visit' As RptType
union all
select 2 RptID,'2-4 Visits' As RptType
union all
select 3 RptID,'5-10 Visits' As RptType
union all
select 4 RptID,'>10 Visits' As RptType
),
RptTypesWithDates As
(
select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,EligibleUsers As
(

Select ProviderID,R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R inner join Provider A 
on A.DateOfRegistration >=R.Date1 and A.DateOfRegistration <R.Date2 
    and (a.UserTypeID=@UserTypeID or @UserTypeID IS NULL)
	and (a.ProviderID=@PartnerID or @PartnerID IS NULL)
where CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))

)
,EligibleLogins As
(
Select R.Date1,ALD.ProviderID,Count(ALD.ProviderID) as Visits from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
ProviderLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2 
inner join Provider H on H.ProviderID=ALD.ProviderID  
	and (h.UserTypeID=@UserTypeID or @UserTypeID IS NULL)
	and (h.ProviderID=@PartnerID or @PartnerID is null)
where CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
group by R.Date1,ALD.ProviderID
)
,NewVisits As
(
select     E1.date1,Case when Visits =1 then '1 Visit' when Visits between 2 and 4 then '2-4 Visits'
			when Visits between 5 and 10 then '5-10 Visits'
			when Visits >10 then '>10 Visits' End RptType 
			from EligibleUsers E1 inner join EligibleLogins E2
			on E1.ProviderID=E2.ProviderID and E1.Date1=E2.date1 
)

,
NewVistsFinal As
(
Select RptID,R.date1,R.RptType,Count(V.RptType) userCount,1 AS ID 
from RptTypesWithDates R left join NewVisits V
on V.Date1=R.Date1 and V.RptType=R.RptType  
group by R.date1,R.RptType,RptID
)
,TotalNewVisits As
(
select SUM(Usercount)  TotalUserCount,Date1  from
NewVistsFinal group by Date1
)

Select RptID,N.date1,N.RptType,isnull(case when TotalUserCount=0 then 0 else cast(userCount as real)*100/cast (TotalUserCount as real) end,0) as Perc
,'Column # ' + cast(dense_Rank() over (order by N.date1) as varchar(10)) ColumnIndex
	,cast(dense_Rank() over (order by N.Date1) as int) IntIndex -- NEW
into #ToBePivoted
from NewVistsFinal N inner join TotalNewVisits T
on N.Date1=T.Date1 

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'



SET @query = N'SELECT RptType, '+ @colsconv +' FROM
(SELECT RptType, ColumnIndex, round(Perc,2) AS Perc, RptID FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B;'

EXECUTE(@query)
		
END
