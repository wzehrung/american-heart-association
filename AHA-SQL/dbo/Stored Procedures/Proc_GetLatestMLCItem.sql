﻿
CREATE Procedure Proc_GetLatestMLCItem
(
 @PersonalItemGUID uniqueidentifier 
)

AS

Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a new entry to ExtendedProfile Table
*/

set nocount on


Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							 
select top 1 * from dbo.MyLifeCheck where 
		  UserHealthRecordID=@UserHealthRecordID order by UpdatedDate desc

End
