﻿
--[Proc_GetPatientsNotSignedUpInForLastNMonths] 1
create PROC Proc_GetPatientsSignedUpInForLastNMonths
(
 @NumberofMonths int
,@CampaignID  int =null
)
As

BEGIN

	
	SELECT 
		U.*,Lan.LanguageLocale 
	FROM 
		AHAUser U 
		inner join Languages Lan on U.DefaultLanguageID = Lan.LanguageID
		inner join AHAUserHealthRecord AHR on U.UserID=AHR.UserID
		inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID 

	WHERE 

		CONVERT(DATETIME,CONVERT(VARCHAR,H.CreatedDate,103),103)=DATEADD(MONTH,-1*@NumberofMonths,CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103))
		and AllowAHAEmail=1
		and (H.CampaignID=@CampaignID or @CampaignID is null)

END
