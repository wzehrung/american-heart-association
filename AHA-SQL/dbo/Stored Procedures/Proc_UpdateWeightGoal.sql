﻿
CREATE Procedure [dbo].[Proc_UpdateWeightGoal]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@StartWeight float  
,@StartDate datetime 
,@TargetWeight float  
,@TargetDate datetime  
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand  
   Created Date     :2008-07-21    
   Description      :Updating records of WeightGoal

   Note : Please dont try to update a indexed foreign key 
*/  

Update dbo.WeightGoal

                    SET 

					 HVVersionStamp		= @HVVersionStamp
                    ,StartWeight		= @StartWeight 
					,StartDate			= @StartDate 
					,TargetWeight		= @TargetWeight 
					,TargetDate			= @TargetDate 
					,UpdatedDate		= GetDate()
                    
					Where HVItemGuid=@HVItemGuid 
					
End


	

