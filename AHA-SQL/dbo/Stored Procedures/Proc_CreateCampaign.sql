﻿CREATE PROC [dbo].[Proc_CreateCampaign]
	(
		 @Title nvarchar(1024)
		,@Description nvarchar(max)
		,@CampaignCode nvarchar(64)
		,@Url nvarchar(2048)
		,@StartDate Datetime
		,@EndDate Datetime
		,@LanguageID int
		,@CampaignID int 
		,@PromotionalTagLine nvarchar(max)
		,@PartnerLogo varbinary(MAX)
		,@PartnerLogoContentType nvarchar(10)
		,@PartnerImage varbinary(MAX)
		,@PartnerImageContentType nvarchar(10)
		,@strRscTitle nvarchar(255)
		,@strRscURL nvarchar(255)
		,@BusinessID int
	)

AS

BEGIN
SET NOCOUNT ON
	
INSERT Campaign(CampaignCode,Url,StartDate,EndDate,BusinessID) 
SELECT @CampaignCode,@Url,@StartDate,@EndDate, @BusinessID

SET @CampaignID = SCOPE_IDENTITY()

insert CampaignDetails
(
CampaignID,LanguageID,Description,Title,PromotionalTagLine,PartnerLogo,PartnerLogoContentType,PartnerImage
,PartnerImageContentType,PartnerLogoVersion,PartnerImageVersion,ResourceTitle,ResourceURL
)
select

@CampaignID,@LanguageID,@Description,@Title,@PromotionalTagLine,@PartnerLogo,@PartnerLogoContentType,@PartnerImage
,@PartnerImageContentType
,case when @PartnerLogo is not null then  newid() end
, case  when  @PartnerImage is not null then newid() end
,@strRscTitle
,@strRscURL
--from languages B

END

