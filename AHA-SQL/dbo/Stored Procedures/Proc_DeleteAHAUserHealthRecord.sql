﻿
CREATE Procedure [dbo].[Proc_DeleteAHAUserHealthRecord]
( 
 @UserID int =null
,@UserHealthRecordID int =null
)
AS
Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Delete AHAUserHealthRecord Record in two ways
					 1: Delete by AhauserID
					 2: Delete by HealthRecordID
*/


if @UserID is null and @UserHealthRecordID is not null
begin
Delete from  dbo.AHAUserHealthRecord
		      Where UserHealthRecordID= @UserHealthRecordID 

end

else if @UserID is not null and @UserHealthRecordID is  null

Begin
Delete from  dbo.AHAUserHealthRecord Where UserID= @UserID 

End

End



