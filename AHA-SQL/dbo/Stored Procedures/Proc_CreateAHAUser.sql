﻿

CREATE Procedure [dbo].[Proc_CreateAHAUser]
(
 @PersonGUID uniqueidentifier 
,@SelfUserHealthRecordGUID	uniqueidentifier = NULL
,@SelfPersonalItemGUID	uniqueidentifier = NULL
,@LanguageID int
,@UserID int OutPut
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04 
 
*/

set nocount on

Declare @AHAUserID int
Declare @SelfHealthRecordID int
Declare @hasSelf bit
Declare @UserHealthRecordID int


if @SelfPersonalItemGUID is null and @SelfUserHealthRecordGUID is null
set @hasSelf=0
else if  @SelfPersonalItemGUID is not null and @SelfUserHealthRecordGUID is not null
set @hasSelf=1


Begin Try
Begin Transaction

-- *************  Case # 1    *************************
select @AHAUserID=UserID from dbo.AHAUser where PersonGUID=@PersonGUID 
if @hasSelf=0
begin

if @AHAUserID is  null
Exec Proc_InsertAHAUser      @PersonGUID
							,null   
							,null 
							,@LanguageID
							,@AHAUserID Output

--Call CreateHealthRecord with in Application 

end
-- ************* End of Case # 1    *************************



-- *************  Case # 2    *************************

else if @hasSelf=1

begin
	
	select   	@SelfHealthRecordID = UserHealthRecordID
				from dbo.HealthRecord where PersonalItemGUID=@SelfPersonalItemGUID 

	
	if @SelfHealthRecordID is not null
	Exec Proc_ChangeHealthRecord @SelfUserHealthRecordGUID,@SelfPersonalItemGUID,@SelfPersonalItemGUID

	Else if @SelfHealthRecordID is null
	begin
	Exec Proc_InsertHealthRecord		 
																 
										 @UserHealthRecordGUID = @SelfUserHealthRecordGUID
										,@PersonalItemGUID = @SelfPersonalItemGUID
										,@UserHealthRecordID = @UserHealthRecordID  OutPut
	set @SelfHealthRecordID=@UserHealthRecordID
    End


	if @AHAUserID is null
	
	Exec Proc_InsertAHAUser  @PersonGUID
							,@SelfHealthRecordID
							,@SelfHealthRecordID
							,@LanguageID
							,@AHAUserID Output

	else if @AHAUserID is not null

	begin
	Exec Proc_UpdateSelfRecordOfAHAuser  @AHAUserID,@SelfHealthRecordID 
	end

	Exec Proc_InsertAHAUserHealthRecord  @PersonGUID
										,@SelfHealthRecordID 


end
-- ************* End of Case # 2    *************************


-- *************  Case # 3    *************************

ELSE IF @hasSelf IS NULL
Raiserror('Invalid HealthRecord', 16, 1)

-- ************* End of Case # 3    *************************

set @UserID = @AHAUserID

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	set @UserID=NULL
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End



