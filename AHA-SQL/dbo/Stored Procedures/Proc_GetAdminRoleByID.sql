﻿CREATE PROC [dbo].[Proc_GetAdminRoleByID]
(
 @AdminUserID int
)

As

Begin
set nocount on

SELECT  a.RoleID, b.Name, b.Description FROM AdminUser a, AdminRoles b
 where  AdminUserID = @AdminUserID 
   and  a.roleid = b.roleid

End
