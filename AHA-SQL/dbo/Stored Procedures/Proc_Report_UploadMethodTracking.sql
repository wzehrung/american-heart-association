﻿
CREATE Procedure [dbo].[Proc_Report_UploadMethodTracking]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
,@PartnerID int = null
)

As

Begin

DECLARE @cols NVARCHAR(MAX)
DECLARE @colsConv NVARCHAR(MAX)
DECLARE @i int

		select * into #h from	
(select UserID,AHR.UserHealthRecordID,PGD.ProviderID from  AHAUserHealthRecord AHR
				inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID AND CampaignID IS NOT NULL
				left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
				left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
) a
		
		select @i = count(*) from #h
        if(@i = 0)
        begin
         select 'NONE' as ReadingSource, '0.00%' as [Column # 1], '0.00%' as [Column # 2] where 1=2
        end
        else
        begin

		;With Tracker As
		(
			select D.Date1, COUNT(DIstinct H.UserHealthRecordID) TrackerCount,'Blood Pressure' Tracker, 
			CASE 
				WHEN ReadingSource IS NULL THEN 'Other' 
				WHEN ReadingSource Like 'Aetna%' THEN 'mobile'
				WHEN ReadingSource Like 'And%' THEN 'Home'
				WHEN ReadingSource Like 'Omron%' THEN 'Home'
				WHEN ReadingSource Like 'homedco%' THEN 'Home'
				WHEN ReadingSource Like 'Polka%' THEN 'Mobile'
				WHEN ReadingSource Like 'homedics%' THEN 'Home'
				WHEN ReadingSource Like 'microlife%' THEN 'Home'
				WHEN ReadingSource Like 'Doctor%Office%' THEN 'Doctor Office'
				WHEN ReadingSource Like 'mobile%' THEN 'mobile'
				WHEN ReadingSource Like 'Pharmacy%' THEN 'Pharmacy'
				WHEN ReadingSource Like 'Home%' THEN 'Home'
				WHEN ReadingSource Like 'Hospital%' THEN 'Hospital'
				WHEN ReadingSource Like 'Health Fair%' THEN 'Health Fair'
				WHEN ReadingSource Like 'IVR%' THEN 'IVR'
				WHEN ReadingSource Like 'Device%' THEN 'Device'
				WHEN ReadingSource Like 'YMCA%' THEN 'YMCA'
				else 'Other' end ReadingSource
			
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodPressure BP 
			on BP.CreatedDate<D.Date2 
			left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			WHERE H.UserHealthRecordID is not null
			GROUP BY Date1,CASE
				WHEN ReadingSource IS NULL THEN 'Other' 
				WHEN ReadingSource Like 'Aetna%' THEN 'mobile'
				WHEN ReadingSource Like 'And%' THEN 'Home'
				WHEN ReadingSource Like 'Omron%' THEN 'Home'
				WHEN ReadingSource Like 'homedco%' THEN 'Home'
				WHEN ReadingSource Like 'Polka%' THEN 'Mobile'
				WHEN ReadingSource Like 'homedics%' THEN 'Home'
				WHEN ReadingSource Like 'microlife%' THEN 'Home'
				WHEN ReadingSource Like 'Doctor%Office%' THEN 'Doctor Office'
				WHEN ReadingSource Like 'mobile%' THEN 'mobile'
				WHEN ReadingSource Like 'Pharmacy%' THEN 'Pharmacy'
				WHEN ReadingSource Like 'Home%' THEN 'Home'
				WHEN ReadingSource Like 'Hospital%' THEN 'Hospital'
				WHEN ReadingSource Like 'Health Fair%' THEN 'Health Fair'
				WHEN ReadingSource Like 'IVR%' THEN 'IVR'
				WHEN ReadingSource Like 'Device%' THEN 'Device'
				WHEN ReadingSource Like 'YMCA%' THEN 'YMCA'
				else 'Other' end 
		)
		
		,
		UserCountP As

		(
			select D.Date1,H.UserHealthRecordID 
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodPressure BP 
			on BP.CreatedDate<D.Date2 
			left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID  AND isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
		)
		,
		UserCount As

		(
		select Date1,count(distinct UserHealthRecordID) UserCount  from UserCountP group by Date1
		)

		Select T.Date1,Tracker, ReadingSource,case when UserCount=0 then 0 else cast(TrackerCount as real)*100/cast (UserCount as real) end as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Tracker T inner join UserCount U on T.Date1=U.Date1

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

EXECUTE(N'SELECT ReadingSource, '+ @colsConv +' FROM
(SELECT ReadingSource, Tracker, ColumnINdex, Perc FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B;')
	
   end
   
END

/************************************************************************************************************************/
