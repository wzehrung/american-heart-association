﻿
--[Proc_FetchListItems] 'AI',2

CREATE Procedure Proc_FetchListItems
(
 @ListCode nvarchar(2),
 @LanguageID int
)
AS

Begin


/* Auther			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :
*/
set nocount on


IF @ListCode='AI' OR @ListCode='AP'

BEGIN
DECLARE @ListItemID int

set @ListItemID=
(
select ListItemID From dbo.ListItems LI
Inner Join dbo.Lists L
On L.ListID=LI.ListID and L.ListCode=@ListCode and LI.LanguageID = 1 and LI.ListItemName='Other'
)

select  ListItemName,ListItemCode,ListItemID from
(
select ListItemName,ListItemCode,ListItemID,rank() over(order by ListItemName) rank
From dbo.ListItems LI
Inner Join dbo.Lists L
On L.ListID=LI.ListID and L.ListCode=@ListCode and LI.LanguageID = @LanguageID
where ListItemID!=@ListItemID

union all


select ListItemName,ListItemCode,ListItemID,50000 rank
From dbo.ListItems LI
Inner Join dbo.Lists L
On L.ListID=LI.ListID and L.ListCode=@ListCode and LI.LanguageID = @LanguageID
where ListItemID=@ListItemID
) A
order by rank


END

ELSE

select ListItemName,ListItemCode,ListItemID From dbo.ListItems LI
Inner Join dbo.Lists L
On L.ListID=LI.ListID and L.ListCode=@ListCode and LI.LanguageID = @LanguageID
Order By case when L.ListID=3 OR L.ListID=12  then null 
when (L.ListID=29 or  L.ListID=28)  then null else ListItemName end

End
