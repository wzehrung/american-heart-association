﻿
CREATE PROCEDURE [dbo].[Proc_IsLogExistsAndActive]
( 
  @ResetGUID UNIQUEIDENTIFIER
 ,@IsLogExistsAndActive bit OUTPUT
)
As
Begin

set nocount on

set @IsLogExistsAndActive=0

if exists (select * from ResetPasswordLog where  ResetGUID=@ResetGUID)
set @IsLogExistsAndActive=1
	
End
