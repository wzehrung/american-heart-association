﻿CREATE PROC [dbo].[PROC_GetNumberOfUnreadMessages]
(
 @UserID int
,@UserType NVARCHAR(10)
,@FolderType CHAR
)

As

Begin
SET NOCOUNT ON


IF @UserType='Patient'

select Count(distinct MD.MessageID) 
NumberOfMessages from
Folder F  
inner join UserFolderMapping UFM
on F.FolderID=UFM.FolderID and UFM.PatientID=@UserID and F.FolderType=@FolderType
inner join MessageDetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
AND MD.isRead = 0

else IF @UserType='Provider'

select Count(distinct MD.MessageID) 
NumberOfMessages from
Folder F  
inner join UserFolderMapping UFM
on F.FolderID=UFM.FolderID and UFM.ProviderID=@UserID and F.FolderType=@FolderType
inner join MessageDetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
AND MD.isRead = 0

End


