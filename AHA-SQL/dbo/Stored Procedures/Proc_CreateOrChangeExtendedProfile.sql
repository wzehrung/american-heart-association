﻿CREATE Procedure [dbo].[Proc_CreateOrChangeExtendedProfile]
(
 @PersonalItemGUID uniqueidentifier 
,@FamilyHistoryHD int = NULL
,@FamilyHistoryStroke int = NULL
,@HighBPDiet int  = NULL
,@HighBPMedication int  = NULL 
,@KidneyDisease int  = NULL
,@DiabetesDiagnosis int  = NULL
,@HeartDiagnosis int  = NULL
,@Stroke int  = NULL
,@PhysicalActivityLevel nvarchar(100)  = NULL
,@BloodType nvarchar(100)  = NULL
,@MedicationAllergy nvarchar(max) = NULL
,@TravelTimeToWorkInMinutes float = NULL
,@MeansOfTransportation nvarchar(255) = NULL
,@AttitudesTowardsHealthcare nvarchar(100) = NULL
,@DoYouSmoke bit  = NULL
,@HaveYouEverTriedToQuitSmoking  bit = NULL
,@LastSynchDate Datetime = NULL
,@Country  nvarchar(50)= NULL
,@Gender nvarchar(10) = NULL
,@Ethnicity nvarchar(50) = NULL
,@MaritalStatus nvarchar(50) = NULL
,@YearOfBirth INT
,@PhoneNumber nvarchar(50) = NULL
,@ZipCode nvarchar(50) = NULL
)

AS

Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a new entry to ExtendedProfile Table
*/

set nocount on

Begin Try
Begin Transaction


Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
					 
if exists(
		  select ExtendedProfileID from dbo.ExtendedProfile where 
		  UserHealthRecordID=@UserHealthRecordID
		  )

Exec Proc_UpdateExtendedProfile		 @UserHealthRecordID
									,@FamilyHistoryHD 
									,@FamilyHistoryStroke 
									,@HighBPDiet  
									,@HighBPMedication  
									,@KidneyDisease  
									,@DiabetesDiagnosis  
									,@HeartDiagnosis  
									,@Stroke  
									,@PhysicalActivityLevel 
									,@BloodType 
									,@MedicationAllergy 
									,@TravelTimeToWorkInMinutes 
									,@MeansOfTransportation 
									,@AttitudesTowardsHealthcare 
									,@DoYouSmoke 
									,@HaveYouEverTriedToQuitSmoking 
									,@LastSynchDate 
									,@Country  
									,@Gender 
									,@Ethnicity 
									,@MaritalStatus
									,@YearOfBirth
									,@ZipCode
												

else 


Exec Proc_InsertExtendedProfile		 @UserHealthRecordID
									,@FamilyHistoryHD 
									,@FamilyHistoryStroke 
									,@HighBPDiet  
									,@HighBPMedication  
									,@KidneyDisease  
									,@DiabetesDiagnosis  
									,@HeartDiagnosis  
									,@Stroke  
									,@PhysicalActivityLevel 
									,@BloodType 
									,@MedicationAllergy 
									,@TravelTimeToWorkInMinutes 
									,@MeansOfTransportation 
									,@AttitudesTowardsHealthcare 
									,@DoYouSmoke 
									,@HaveYouEverTriedToQuitSmoking  
									,@LastSynchDate
									,@Country  
									,@Gender 
									,@Ethnicity 
									,@MaritalStatus
									,@YearOfBirth
									,@ZipCode

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID

--IVR Phone Number
if (@phonenumber != '0') 
begin
    SET @PhoneNumber = replace(@PhoneNumber, '(', '')
    SET @PhoneNumber = replace(@PhoneNumber, ')', '')
    SET @PhoneNumber = replace(@PhoneNumber, '-', '')
    IF (len(@PhoneNumber)=10)
    begin
     SET @PhoneNumber = '1' + @PhoneNumber
    end
 update IVRPatientReminder set PhoneNumber= @phonenumber where PatientID=@UserHealthRecordID --and @phonenumber is not null and @phonenumber != null
end

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End


