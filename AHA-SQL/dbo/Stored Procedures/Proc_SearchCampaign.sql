﻿CREATE PROC [dbo].[Proc_SearchCampaign]
(
 @Title nvarchar(1024)
,@StartDate Datetime
,@EndDate Datetime
,@LanguageID int
)
As


Begin
set nocount on


select *,Description,Title , C.BusinessID, ab.Name AS BusinessName 
from Campaign C inner join CampaignDetails CD on C.CampaignID=CD.CampaignID 

		LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

where Title=isnull(@Title,Title)
							 and StartDate>=isnull(@StartDate,StartDate)
							 and EndDate <=isnull(@EndDate,EndDate) and LanguageID=@LanguageID

End





