﻿

CREATE  PROC [dbo].[Proc_MarkEmailListSchedulerDownloaded]
(
 @SchedulerID int 
,@IsDownloaded bit
)

AS

Begin

UPDATE EmailListScheduler

SET IsDownloaded=@IsDownloaded WHERE SchedulerID=@SchedulerID


End



