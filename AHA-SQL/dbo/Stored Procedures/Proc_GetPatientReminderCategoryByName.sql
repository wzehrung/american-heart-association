﻿
CREATE Procedure [dbo].[Proc_GetPatientReminderCategoryByName]
(
@Name varchar(25)
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/

SELECT ReminderCategoryID 
FROM [3CiPatientReminderCategory]
WHERE Name = RTrim(@Name)


End
