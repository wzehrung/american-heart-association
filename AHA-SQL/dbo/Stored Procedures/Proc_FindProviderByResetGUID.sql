﻿
CREATE PROCEDURE [dbo].[Proc_FindProviderByResetGUID]
( 
  @ResetGUID UNIQUEIDENTIFIER
)
As
Begin

set nocount on

select * from ResetPasswordLog A 
inner join Provider B on A.ProviderID=B.ProviderID
and A.ResetGUID=@ResetGUID

End


