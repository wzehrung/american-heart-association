﻿
CREATE proc [dbo].[Proc_DismissAllPatientAlerts]
(
 @ProviderID INT
,@PatientHealthRecordGUID uniqueidentifier
)

AS

Begin
set nocount on

UPDATE A SET IsDismissed=1  from  Provider P inner Join AlertRule AR
on P.ProviderID = AR.ProviderID AND P.ProviderID=@ProviderID
inner join AlertMapping AM on AR.AlertRuleID=AM.AlertRuleID 
inner join HealthRecord H on H.UserHealthRecordID=AM.PatientID and H.UserHealthRecordGUID=@PatientHealthRecordGUID

inner join Alert A on AR.AlertRuleID=A.AlertRuleID
  

End

