﻿
CREATE PROCEDURE [dbo].[Proc_UpdatePatientNameInvitation]
		(
		 @InvitationID int
		,@FirstName nvarchar(50)
		,@LastName nvarchar(50)
		)
		As
		Begin
		set nocount on

			update PatientProviderInvitationDetails set FirstName=@FirstName
			,LastName=@LastName
			where InvitationID=@InvitationID
			
		End

