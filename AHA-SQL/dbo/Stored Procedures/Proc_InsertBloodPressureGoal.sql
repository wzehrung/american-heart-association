﻿CREATE Procedure [dbo].[Proc_InsertBloodPressureGoal]
(
 @UserHealthRecordID int 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@TargetSystolic int 
,@TargetDiastolic int  
)
AS

Begin

--This Sp should not be called from within application/product  
  
/* Author			:Shipin Anand  
   Created Date     :2008-07-21    
   Description      :Insert Record to BloodPressureGoal
*/  

Insert dbo.BloodPressureGoal 
					(
                       UserHealthRecordID  
					  ,HVItemGuid   
					  ,HVVersionStamp   
					  ,TargetSystolic  
					  ,TargetDiastolic                
                    )
Select                 @UserHealthRecordID  
					  ,@HVItemGuid   
					  ,@HVVersionStamp   
					  ,@TargetSystolic  
					  ,@TargetDiastolic  
End




