﻿

CREATE Procedure  [dbo].[Proc_ClearDB]

As

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-08-18  
   Description      :For Arun 
*/

set nocount on


Begin Try
Begin Transaction


Delete from  dbo.BloodGlucose		

Delete from dbo.BloodPressure		

Delete from dbo.BloodPressureGoal		

Delete from dbo.Cholesterol			
	    
Delete from dbo.FamilyHistory				

Delete from dbo.Height							

Delete from dbo.Medication						
		
Delete from dbo.Weight					

Delete from dbo.Diet					

Delete from dbo.Exercise			

Delete from dbo.ExtendedProfile			
				
Delete from dbo.WeightGoal				

Delete from dbo.CholesterolGoal			

Delete from dbo.AHAUserHealthRecord 
	
Delete from AHAUserLoginDetails

Delete from AHAUser  

Delete from dbo.HealthRecord 	

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK


	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
 
End


