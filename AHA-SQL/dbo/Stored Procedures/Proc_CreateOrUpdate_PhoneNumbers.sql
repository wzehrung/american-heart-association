﻿


CREATE Procedure [dbo].[Proc_CreateOrUpdate_PhoneNumbers]
(
 @UserHealthRecordID int  
,@PhoneContactCountryCodeID int
,@PhoneContact nvarchar (20)
,@PhoneMobileCountryCodeID int
,@PhoneMobile nvarchar (20) 
)
AS

BEGIN

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

IF EXISTS
	(
		SELECT ExtendedProfileID FROM dbo.ExtendedProfile WHERE 
		UserHealthRecordID=@UserHealthRecordID
	)

UPDATE ExtendedProfile
	SET 
		PhoneContactCountryCodeID	= @PhoneContactCountryCodeID
		,PhoneContact				= @PhoneContact
		,PhoneMobileCountryCodeID	= @PhoneMobileCountryCodeID
		,PhoneMobile				= @PhoneMobile
		,UpdatedDate				= CURRENT_TIMESTAMP
	WHERE
		UserHealthRecordID = @UserHealthRecordID

ELSE 

INSERT ExtendedProfile
	(
		UserHealthRecordID,
		PhoneContactCountryCodeID,
		PhoneContact,
		PhoneMobileCountryCodeID,
		PhoneMobile,
		UpdatedDate
	)	 
SELECT
		@UserHealthRecordID,
		@PhoneContactCountryCodeID,
		@PhoneContact,
		@PhoneMobileCountryCodeID,
		@PhoneMobile,
		CURRENT_TIMESTAMP

DECLARE @PersonalItemGUID uniqueidentifier

SELECT PersonalItemGUID=@PersonalItemGUID
FROM dbo.HealthRecord
WHERE @UserHealthRecordID = UserHealthRecordID 

COMMIT
END TRY

BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK	
			DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
			SELECT  @ErrMsg = ERROR_MESSAGE(),
					@ErrSeverity = ERROR_SEVERITY()
		RAISERROR(@ErrMsg, @ErrSeverity, 1)

	END CATCH

END
