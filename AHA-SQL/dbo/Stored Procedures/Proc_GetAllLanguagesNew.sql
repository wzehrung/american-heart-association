﻿CREATE procedure [dbo].[Proc_GetAllLanguagesNew]
(@SelectedLanguageID int)
as 
begin
	select L.LanguageID,LT.LanguageTitle as LanguageName ,L.LanguageLocale,L.IsDefault
 from Languages L INNER JOIN LanguageTitle LT on L.LanguageID=LT.ToLanguageID and LT.FromLanguageID=@SelectedLanguageID
end
