﻿  
-- [Proc_FindAllProviderTipsTypes]  
  
CREATE proc [dbo].[Proc_FindAllProviderTipsTypes]  
(
@LanguageID int
)
  
AS  
  
Begin  
set nocount on  
  
select * from ProviderTipsType where LanguageID=  @LanguageID
  
  
End  
  
  