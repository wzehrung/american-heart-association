﻿
CREATE Procedure [dbo].[Proc_CreateAHAUserLoginDetails]
(
  @PersonGUID UniqueIdentifier
 ,@LoginDetailsID int output
)

As

Begin 

/* Author           :Shipin Anand  
   Created Date     :2008-07-10    
   Description      :Creating record for  AHAUserLoginDetails
*/ 

set nocount on

	IF  Exists (Select UserID from dbo.AHAUser where PersonGUID=@PersonGUID )
	begin
	Insert dbo.AHAUserLoginDetails(AHAUserID)
	Select UserID from dbo.AHAUser where PersonGUID=@PersonGUID 
	set @LoginDetailsID=SCOPE_IDENTITY()
	end
	else 
	set @LoginDetailsID=null

End



