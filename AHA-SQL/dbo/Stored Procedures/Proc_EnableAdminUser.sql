﻿


CREATE PROC [dbo].[Proc_EnableAdminUser]
(
 @AdminUserID int
)

As

Begin
set nocount on

Update AdminUser set 
						 IsActive = 1
					
where  AdminUserID = @AdminUserID

End
