﻿



CREATE proc [dbo].[proc_attachcampaigns_1] as
begin

declare @patientid int
declare @campaignid int
declare @providerid int

set @patientid = 0
set @campaignid = 0
set @providerid = 0

select * into healthrecord_03272013_A from healthrecord

create table attachcampaigns_3 (patientid int, campaignid int)

declare f cursor for
select providerid, campaignid from provider
where campaignid is not null

open f

fetch f into @providerid, @campaignid

while @@fetch_status = 0
begin

 insert into attachcampaigns_2 exec dbo.Proc_FindAllPatientsByProviderID_ForUpdatingCampaigns @providerid

 fetch f into @providerid, @campaignid

end

close f
deallocate f

 update healthrecord set campaignid = a.campaignid from attachcampaigns_2 a where healthrecord.userhealthrecordid = a.patientid


end




