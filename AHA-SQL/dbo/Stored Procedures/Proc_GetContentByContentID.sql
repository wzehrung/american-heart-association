﻿
--[Proc_GetContentByContentID] 1
CREATE PROC [dbo].[Proc_GetContentByContentID]
(
@ContentID int
,@LanguageID int
)

As

Begin
set nocount on

Declare @ContentRulesIDs nvarchar(max)

select @ContentRulesIDs = COALESCE(@ContentRulesIDs + ',', '')+
cast (ContentRulesID   as Varchar(20))
from ContentRuleMapping 
where  ContentID=@ContentID and LanguageID=@LanguageID

select *,@ContentRulesIDs ContentRulesIDs from Content

Where ContentID=@ContentID and LanguageID=@LanguageID

End



