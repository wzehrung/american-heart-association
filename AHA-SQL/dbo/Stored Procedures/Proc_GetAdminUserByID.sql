﻿CREATE PROC [dbo].[Proc_GetAdminUserByID]
(
	@AdminUserID int
)

AS

BEGIN
SET NOCOUNT ON

SELECT  au.*, ar.Name AS RoleName, ab.BusinessID, ab.Name AS BusinessName FROM AdminUser au

INNER JOIN AdminRoles ar
	ON au.RoleID = ar.RoleID

LEFT OUTER JOIN AdminBusiness ab
	ON au.BusinessID = ab.BusinessID

WHERE  AdminUserID = @AdminUserID 

END
