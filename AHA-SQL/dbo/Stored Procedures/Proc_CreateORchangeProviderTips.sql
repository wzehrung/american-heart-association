﻿CREATE PROC [dbo].[Proc_CreateORchangeProviderTips]  
(  
 @ProviderTipsData nvarchar(max)  
,@ProviderTipsTypeIds nvarchar(max)  
,@ProviderTipsID int output 
,@CreatedByUserID INT  
,@UpdatedByUserID INT  
,@LanguageID int  
)  
  
As  
  
Begin  
set nocount on  
  
if exists(select * from ProviderTips where ProviderTipsID=@ProviderTipsID and LanguageID=@LanguageID)  
EXEC Proc_ChangeProviderTips @ProviderTipsData ,@ProviderTipsTypeIds,@ProviderTipsID ,@UpdatedByUserID ,@LanguageID   
  
ELSE   
  
EXEC Proc_CreateProviderTips @ProviderTipsData ,@ProviderTipsTypeIds ,@ProviderTipsID output ,@CreatedByUserID ,@LanguageID  
  
End  
  