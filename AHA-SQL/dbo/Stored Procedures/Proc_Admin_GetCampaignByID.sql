﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.13.2014
-- Description:	Find campaign based on given campaign ID
-- =============================================

CREATE PROC [dbo].[Proc_Admin_GetCampaignByID]
(
    @CampaignID int,
    @LanguageID int
)

AS

--DECLARE
--    @CampaignID int,
--    @LanguageID int
--SET @CampaignID = 116
--SET @LanguageID = 1

BEGIN
SET NOCOUNT ON

SELECT
    c.CampaignID,
    c.Url,
    c.StartDate,
    c.EndDate,
    c.CreatedDate,
    c.UpdatedDate,
    c.BusinessID,
    cd.LanguageID,
    cd.Description,
    cd.Title,
    cd.PromotionalTagLine,
    cd.PartnerLogoVersion,
    cd.PartnerImageVersion,
    cd.IsPublished,
    CASE 
	   WHEN Convert(Datetime,Convert(Varchar,getdate(),103),103) BETWEEN StartDate AND EndDate AND isPublished=1 
	   THEN 1 
    ELSE 0 
    END AS isActive,
    cd.ResourceTitle,
    cd.ResourceURL,
    ab.Name AS BusinessName,
    cd.CampaignImages

FROM Campaign c

    LEFT JOIN CampaignDetails cd 
	   ON c.CampaignID = cd.CampaignID 
		  AND LanguageID = @LanguageID 
				
    LEFT JOIN AdminBusiness ab
	   ON c.BusinessID = ab.BusinessID


WHERE c.CampaignID = @CampaignID

END