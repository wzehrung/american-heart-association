﻿
CREATE Procedure [dbo].[Proc_InsertPatientSMSReminder]
(
 @PatientID Int
,@MobileNumber varchar(12)
,@PIN varchar(4)
,@TimeZone nvarchar(35)
,@Terms bit
,@PinValidated bit
,@ReminderID int OutPut
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on


Insert dbo.[3CiPatientReminder] 
					(
					 PatientID
					,MobileNumber					
					,PIN
					,TimeZone
					,Terms
					,PinValidated
					,CreatedDateTime
					,UpdatedDateTime
					)
Select              
					 @PatientID
					,@MobileNumber
					,@PIN
					,@TimeZone
					,@Terms
					,@PinValidated
					,getdate()
					,getdate()

SET @ReminderID= SCOPE_IDENTITY()


End
