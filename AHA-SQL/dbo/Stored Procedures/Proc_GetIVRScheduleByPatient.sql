﻿

CREATE Procedure [dbo].[Proc_GetIVRScheduleByPatient]

(
	@PatientID Int
)

AS

BEGIN

set nocount on

SELECT
	[ReminderID]
	,[PatientID]
	,[PhoneNumber]
	,[PIN]
	,[Time]
	,[Day]
	,[TimeZone]
	,[Enabled]
	,[CreatedDateTime]
	,[UpdatedDateTime]

FROM IVRPatientReminder

WHERE PatientID = @PatientID

END
