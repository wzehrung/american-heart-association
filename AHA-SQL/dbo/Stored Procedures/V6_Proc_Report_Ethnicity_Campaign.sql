﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 6.17.2013
-- Description:	Refactor of Proc_Report_Ethnicity_Campaign for Campaign/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Ethnicity_Campaign] 
(
	@StartDate Datetime,  
	@EndDate Datetime,
	@CampaignID int = null,	-- null specifies all campaigns
	@Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
	@CampaignIDs nvarchar(max)
)
AS
BEGIN

	DECLARE @cols NVARCHAR(MAX)
	DECLARE @colsConv NVARCHAR(MAX)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

        -- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
		from ZipRegionMapping 
		where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 
    
    
 	--
	-- Create a temp table of all users who are in campaign/markets logic
	--
	select * into #h from	
		(select UserID, AHR.UserHealthRecordID, D.Date1, E.Ethnicity
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D
			inner join AHAUserHealthRecord AHR ON AHR.CreatedDate < D.Date2
			inner join HealthRecord H on H.UserHealthRecordID = AHR.UserHealthRecordID
			left join  ExtendedProfile E ON E.UserHealthRecordID = AHR.UserHealthRecordID

			/* OLD WAY
			/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
					just get those is specified */
			WHERE ((@CampaignId IS NULL AND H.CampaignID IS NOT NULL) OR (@CampaignId IS NOT NULL AND @CampaignId = H.CampaignID))
				AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
				*/
			WHERE
			@CampaignIDs IS NULL AND H.CampaignID IS NOT NULL
			OR (H.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AHR.UserID IS NOT NULL)
		) a
	
	--
	-- generate the report
	--
	
	;WITH EthnicityBase AS
	(
		SELECT  H.Date1,
				COUNT(DISTINCT H.UserHealthRecordID) EthnicityCount,
				CASE
					WHEN H.Ethnicity IS NULL
						OR H.Ethnicity = ''
						OR H.Ethnicity = '[not specified]'
						OR H.Ethnicity = 'Unspecified'
						OR H.Ethnicity = 'No Selection'
						OR H.Ethnicity = 'None'
					THEN 'Other Race'
					
					WHEN H.Ethnicity = 'Caucasion'
						OR H.Ethnicity = 'Caucasian'
					THEN 'White'
					
					WHEN isnumeric(H.Ethnicity) = 1
					THEN 'Other race'
					
					ELSE H.Ethnicity
				END Ethnicity
			FROM #h H
			
			GROUP BY
				Date1,
				CASE
					WHEN H.Ethnicity IS NULL
						OR H.Ethnicity = ''
						OR H.Ethnicity = '[not specified]'
						OR H.Ethnicity = 'Unspecified'
						OR H.Ethnicity = 'No Selection'
						OR H.Ethnicity = 'None'
					THEN 'Other Race'
					
					WHEN H.Ethnicity = 'Caucasion'
						OR H.Ethnicity = 'Caucasian'
					THEN 'White'
					
					WHEN isnumeric(H.Ethnicity) = 1
					THEN 'Other race'
					
					ELSE H.Ethnicity
				END
	)
	,EthnicityTypes As
	(
		select 'American Indian or Alaska Native' as EthnicityType,1 rptID
		union all
		select 'Asian' as EthnicityType ,2 rptID
		union all
		select 'Black or African American' as EthnicityType,3 rptID
		union all
		select 'Hispanic or Latino' as EthnicityType ,4 rptID
		union all
		select 'Native Hawaiian or Other Pacific Islander' as EthnicityType ,5 rptID
		union all
		select 'White' as EthnicityType,6 rptID
		union all
		select 'Mixed Race' as EthnicityType,7 rptID
		union all
		select 'Other Race' as EthnicityType,8 rptID
	)
	,EthnicityDates As
	(
		select rptID,D.Date1,E.EthnicityType
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D
			CROSS JOIN EthnicityTypes E
	)
	,Ethnicity As
	(
		select rptID,D.Date1,EB.EthnicityCount,D.EthnicityType Ethnicity
			from EthnicityDates D
			left join EthnicityBase EB on D.Date1=EB.date1 and D.EthnicityType=EB.Ethnicity
	)

	,UserCount As
	(
		select Date1,sum(EthnicityCount) UserCount from Ethnicity group by Date1
	)

	Select rptID,T.Date1,Ethnicity,ISNULL(case when UserCount=0 then 0 else cast(EthnicityCount as real)*100/cast (UserCount as real) end,0) as Perc
		   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
			,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
	into #ToBePivoted
	From Ethnicity T inner join UserCount U on T.Date1=U.Date1
	

	SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


	SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
							'],[' + t.ColumnIndex
					FROM #ColumnsIndex AS t
					ORDER BY t.IntIndex
					FOR XML PATH('')
				  ), 1, 2, '') + ']'

	SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
							'],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
					FROM #ColumnsIndex AS t
					ORDER BY t.IntIndex
					FOR XML PATH('')
				  ), 1, 2, '') + ']'

	EXECUTE(N'SELECT Ethnicity, '+ @colsConv +' FROM
	(SELECT Ethnicity, ColumnINdex, Perc,rptID FROM #ToBePivoted ) A
	PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;')   
	
	
END

