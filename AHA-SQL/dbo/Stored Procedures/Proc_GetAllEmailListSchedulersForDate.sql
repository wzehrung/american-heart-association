﻿
--Proc_GetAllEmailListSchedulersForDate '2011-01-13 00:00:00.000',2
CREATE  PROC [dbo].[Proc_GetAllEmailListSchedulersForDate]
(
 @ScheduledDate datetime,
 @LanguageID int,
 @ProviderOnly bit
)

AS
Begin

if(@ScheduledDate is null)

	select EL.*
	,ISNULL(C.Title,(SELECT top 1 Title from CampaignDetails where CampaignID=EL.CampaignID and title is not null order by LanguageID)) Title 

	from EmailListScheduler EL
	left join CampaignDetails C
	on EL.CampaignID = C.CampaignID AND  C.LanguageID = @LanguageID
	where EL.IsProviderList = 1 OR @ProviderOnly = 0 
	order by EL.ScheduledDate desc
else 
	select EL.*
	,ISNULL(C.Title,(SELECT top 1 Title from CampaignDetails where CampaignID=EL.CampaignID and title is not null order by LanguageID)) Title 
	from EmailListScheduler EL
	left join CampaignDetails C
	on EL.CampaignID = C.CampaignID and C.LanguageID = @LanguageID
	where 
	convert(datetime,Convert(varchar,EL.ScheduledDate,103),103)=convert(datetime,Convert(varchar,@ScheduledDate,103),103)

	order by EL.ScheduledDate desc

End
