﻿
CREATE Procedure [dbo].[Proc_InsertCholesterolGoal]
(
 @UserHealthRecordID int 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Totalcholesterol int
,@LDL int
,@HDL int 
,@Triglyceride int
)
AS

Begin

--This Sp should not be called from within application/product  
  
/* Author			:Shipin Anand  
   Created Date     :2008-07-21    
   Description      :Insert Record to CholesterolGoal
*/  

Insert dbo.CholesterolGoal 
					(
                       UserHealthRecordID  
					  ,HVItemGuid   
					  ,HVVersionStamp   
					  ,Totalcholesterol 
					  ,LDL 
					  ,HDL    
					  ,Triglyceride            
                    )
Select                 @UserHealthRecordID  
					  ,@HVItemGuid   
					  ,@HVVersionStamp   
					  ,@Totalcholesterol 
					  ,@LDL 
					  ,@HDL 
					  ,@Triglyceride
End






