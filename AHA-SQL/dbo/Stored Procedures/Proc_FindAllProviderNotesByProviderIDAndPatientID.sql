﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 06.02.2014
-- Description:	Find all ProviderNotes given a provider and a patient
-- =============================================
CREATE PROCEDURE [dbo].[Proc_FindAllProviderNotesByProviderIDAndPatientID]
(
@ProviderID int,
@PatientID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ProviderNoteID, ProviderID, PatientID, CreationDate, Title, Note
		FROM ProviderNote
		WHERE ProviderID = @ProviderID and PatientID = @PatientID and IsDeleted = 0
END

