﻿


--PROC_Report_newUsersPercentage '2009.11.01','2010.01.01'
--PROC_Report_newUsers '2009.11.01','2010.01.01'



CREATE PROC [dbo].[PROC_Report_newProvidersPercentage]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
)
As

BEGIN
set nocount on
;with RptTypes As
(
select 1 RptID,'1 Visit' As RptType
union all
select 2 RptID,'2-4 Visits' As RptType
union all
select 3 RptID,'5-10 Visits' As RptType
union all
select 4 RptID,'>10 Visits' As RptType
),
RptTypesWithDates As
(
select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,EligibleUsers As
(

Select ProviderID,R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R inner join Provider A 
on A.DateOfRegistration >=R.Date1 and A.DateOfRegistration <R.Date2 where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
)
,EligibleLogins As
(
Select R.Date1,ALD.ProviderID,Count(ALD.ProviderID) as Visits from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
ProviderLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2 
inner join Provider H on H.ProviderID=ALD.ProviderID  where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
group by R.Date1,ALD.ProviderID
)
,NewVisits As
(
select     E1.date1,Case when Visits =1 then '1 Visit' when Visits between 2 and 4 then '2-4 Visits'
			when Visits between 5 and 10 then '5-10 Visits'
			when Visits >10 then '>10 Visits' End RptType 
			from EligibleUsers E1 inner join EligibleLogins E2
			on E1.ProviderID=E2.ProviderID and E1.Date1=E2.date1 
)

,
NewVistsFinal As
(
Select RptID,R.date1,R.RptType,Count(V.RptType) userCount,1 AS ID 
from RptTypesWithDates R left join NewVisits V
on V.Date1=R.Date1 and V.RptType=R.RptType  
group by R.date1,R.RptType,RptID
)
,TotalNewVisits As
(
select SUM(Usercount)  TotalUserCount,Date1  from
NewVistsFinal group by Date1
)
,ToBePivoted As
(

Select RptID,N.date1,N.RptType,isnull(case when TotalUserCount=0 then 0 else cast(userCount as real)*100/cast (TotalUserCount as real) end,0) as Perc
,'Column # ' + cast(dense_Rank() over (order by N.date1) as varchar(10)) ColumnIndex
from NewVistsFinal N inner join TotalNewVisits T
on N.Date1=T.Date1 


)

		
select 

 RptType
,cast(cast([Column # 1] as numeric(15,2)) as varchar(20))+'%'  [Column # 1]
,cast(cast([Column # 2] as numeric(15,2)) as varchar(20))+'%' as [Column # 2]
,cast(cast([Column # 3] as numeric(15,2)) as varchar(20))+'%' as [Column # 3]
,cast(cast([Column # 4] as numeric(15,2)) as varchar(20))+'%' as [Column # 4] 
,cast(cast([Column # 5] as numeric(15,2)) as varchar(20))+'%' as [Column # 5]
,cast(cast([Column # 6] as numeric(15,2)) as varchar(20))+'%' as [Column # 6]
,cast(cast([Column # 7] as numeric(15,2)) as varchar(20))+'%' as [Column # 7]
,cast(cast([Column # 8] as numeric(15,2)) as varchar(20))+'%' as [Column # 8] 
,cast(cast([Column # 9] as numeric(15,2)) as varchar(20))+'%' as [Column # 9]
,cast(cast([Column # 10] as numeric(15,2)) as varchar(20))+'%' as [Column # 10]
,cast(cast([Column # 11] as numeric(15,2)) as varchar(20))+'%' as [Column # 11]
,cast(cast([Column # 12] as numeric(15,2)) as varchar(20))+'%' as [Column # 12]



from
(
select	RptID,RptType,ColumnIndex,Perc
		from ToBePivoted
) A
PIVOT
(
SUM (Perc)
FOR ColumnIndex IN
( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
 , [Column # 10],[Column # 11],[Column # 12] )
) B
order by RptID

END






