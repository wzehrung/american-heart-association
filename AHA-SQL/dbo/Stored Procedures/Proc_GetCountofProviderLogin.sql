﻿

CREATE Proc [dbo].[Proc_GetCountofProviderLogin]
(
@ProviderID int,
@Count int output
)
As
Begin
set nocount on
select @Count = count(*) from  ProviderLoginDetails where ProviderID= @ProviderID
End

