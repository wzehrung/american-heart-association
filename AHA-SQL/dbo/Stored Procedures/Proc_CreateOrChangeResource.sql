﻿CREATE PROC [dbo].[Proc_CreateOrChangeResource]  
(  
 @ContentTitle NVARCHAR(80)  
,@ContentTitleUrl nvarchar(255)  
,@ContentText nvarchar(MAX)  
,@IsProvider bit  
,@ExpirationDate datetime  
,@UpdatedByUserID int  
,@LanguageID int  
,@ResourceID INT  output 
,@providerID int = 0
)  
As  
Begin  
  
set nocount on  
  
  
  
if exists (select * from Resources WHERE ResourceID=@ResourceID and LanguageID=@LanguageID)  
  
exec   
  
Proc_UpdateResource  
  
 @ResourceID   
,@ContentTitle   
,@ContentTitleUrl   
,@ContentText   
,@ExpirationDate   
,@UpdatedByUserID   
,@LanguageID   
,@ProviderID 
  
else  
  
begin  
  
exec  
  
Proc_CreateResource  
  
 @ContentTitle   
,@ContentTitleUrl  
,@ContentText   
,@IsProvider   
,@ExpirationDate   
,@UpdatedByUserID   
,@LanguageID  
  
,@ResourceID output  
,@ProviderID 
  
end  
  
End
