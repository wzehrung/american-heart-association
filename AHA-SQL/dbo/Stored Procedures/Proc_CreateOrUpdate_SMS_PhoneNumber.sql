﻿
CREATE Procedure [dbo].[Proc_CreateOrUpdate_SMS_PhoneNumber]
(
	@UserHealthRecordID int,
	@PhoneMobileCountryCodeID varchar(1),
	@PhoneMobile varchar(12),
	@Pin varchar(4),
	@TimeZone nvarchar(35),
	@Delete nvarchar(5)
)
AS

BEGIN

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

IF @Delete = N'true'

	BEGIN
		DELETE FROM [3CiPatientReminder] WHERE PatientID = @UserHealthRecordID

		DELETE FROM [3CiPatientReminderSchedule] WHERE ReminderID = (SELECT ReminderID FROM [3CiPatientReminder] WHERE PatientID = @UserHealthRecordID)

		UPDATE ExtendedProfile
			SET 
				PhoneMobileCountryCodeID = '',
				PhoneMobile = ''
			WHERE
				UserHealthRecordID = @UserHealthRecordID
	END

ELSE

	BEGIN
	IF EXISTS
		(
			SELECT UserHealthRecordID FROM ExtendedProfile WHERE 
			UserHealthRecordID = @UserHealthRecordID
		)

		UPDATE ExtendedProfile
			SET 
				PhoneMobileCountryCodeID = @PhoneMobileCountryCodeID,
				PhoneMobile = @PhoneMobile
			WHERE
				UserHealthRecordID = @UserHealthRecordID

	ELSE

		INSERT ExtendedProfile
			(
				PhoneMobileCountryCodeID,
				PhoneMobile
			)
		VALUES
			(
				@PhoneMobileCountryCodeID,
				@PhoneMobile
			)

	IF EXISTS
		(
			SELECT PatientID FROM [3CiPatientReminder] WHERE 
			PatientID=@UserHealthRecordID
		)

		UPDATE [3CiPatientReminder]
			SET 
				MobileNumber	= @PhoneMobileCountryCodeID + @PhoneMobile,
				PIN				= @Pin,
				TimeZone		= @TimeZone,
				UpdatedDateTime	= CURRENT_TIMESTAMP
			WHERE
				PatientID = @UserHealthRecordID

	ELSE 

	INSERT [3CiPatientReminder]
		(
			PatientID,
			MobileNumber,
			Pin,
			TimeZone,
			CreatedDateTime
		)	 
	SELECT
			@UserHealthRecordID,
			@PhoneMobileCountryCodeID + @PhoneMobile,
			@Pin,
			@TimeZone,
			CURRENT_TIMESTAMP
	END

COMMIT
END TRY

BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK	
			DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
			SELECT  @ErrMsg = ERROR_MESSAGE(),
					@ErrSeverity = ERROR_SEVERITY()
		RAISERROR(@ErrMsg, @ErrSeverity, 1)

	END CATCH

END
