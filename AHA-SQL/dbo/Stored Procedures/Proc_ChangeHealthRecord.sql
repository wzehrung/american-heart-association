﻿



CREATE Procedure [dbo].[Proc_ChangeHealthRecord]
(
 @UserHealthRecordGUID uniqueidentifier
,@PersonalItemGUIDnew uniqueidentifier  
,@PersonalItemGUIDold uniqueidentifier
)
AS

Begin
/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Updating records of HealthRecord for a @PersonalItemGUID
*/

set nocount on

Update dbo.HealthRecord
                    SET  UserHealthRecordGUID	= @UserHealthRecordGUID
						,PersonalItemGUID		= @PersonalItemGUIDnew 
						,UpdatedDate			= GetDate()   
                 
                    Where PersonalItemGUID= @PersonalItemGUIDold 
End











