﻿CREATE PROC [dbo].[Proc_UpdateCampaign]
	(
		 @Title nvarchar(1024)  
		,@Description nvarchar(max)
		,@StartDate Datetime
		,@EndDate Datetime
		,@CampaignID int 
		,@LanguageID int
		,@PromotionalTagLine nvarchar(MAX)
		,@PartnerLogo varbinary(MAX)
		,@PartnerLogoContentType nvarchar(10)
		,@PartnerImage varbinary(MAX)
		,@PartnerImageContentType nvarchar(10)
		,@strRscTitle nvarchar(255)
		,@strRscURL nvarchar(255)
		,@BusinessID int
	)

AS

BEGIN
SET NOCOUNT ON

UPDATE Campaign 
	SET
     StartDate		= @StartDate
	,EndDate		= @EndDate
	,UpdatedDate	= getdate()
	,BusinessID		= @BusinessID

WHERE CampaignID = @CampaignID 

IF EXISTS (SELECT * FROM CampaignDetails WHERE CampaignID = @CampaignID AND LanguageID = @LanguageID)  

UPDATE CampaignDetails 

SET
   [Title]									= @Title  
  ,[Description]							= @Description
  ,[PromotionalTagLine]						= @PromotionalTagLine
  ,[PartnerLogo]							= @PartnerLogo
  ,[PartnerLogoContentType]					= @PartnerLogoContentType
  ,[PartnerImage]							= @PartnerImage
  ,[PartnerImageContentType]				= @PartnerImageContentType
  ,PartnerLogoVersion						= case when @PartnerLogo is not null and ISNULL([PartnerLogo],CAST('1' AS VARBINARY))<>@PartnerLogo then  newid() else case when @PartnerLogo is not null then PartnerLogoVersion end end
  ,PartnerImageVersion						= case when @PartnerImage is not null and ISNULL([PartnerImage],CAST('1' AS VARBINARY))<>@PartnerImage then  newid() else case when @PartnerImage is not null then PartnerImageVersion end end
  ,ResourceTitle							= @strRscTitle
  ,ResourceURL								= @strRscURL

WHERE CampaignID = @CampaignID AND LanguageID = @LanguageID

ELSE

INSERT INTO CampaignDetails
	(
		[CampaignID]
		,[Title]
		,[Description]
		,[PromotionalTagLine]
		,[PartnerLogo]
		,[PartnerLogoContentType]
		,[PartnerImage]
		,[PartnerImageContentType]
		,[PartnerLogoVersion]
		,[PartnerImageVersion]
		,[LanguageID]
		,[ResourceTitle]
		,[ResourceURL]
	)

SELECT @CampaignID
      , @Title
      , @Description
      , @PromotionalTagLine
      ,@PartnerLogo
      ,@PartnerLogoContentType
      ,@PartnerImage
      ,@PartnerImageContentType
     ,case when @PartnerLogo is not null then  newid() end
	 ,case when  @PartnerImage is not null then newid() end
      ,@LanguageID
	  ,@strRscTitle
	  ,@strRscURL
END

