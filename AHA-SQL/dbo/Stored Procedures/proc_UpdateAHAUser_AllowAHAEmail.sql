﻿


CREATE PROC [dbo].[proc_UpdateAHAUser_AllowAHAEmail]
(
 @PersonGUID uniqueidentifier
,@AllowAHAEmail bit
)
As
Begin

set nocount on


	Update dbo.AHAUser set AllowAHAEmail=@AllowAHAEmail,AllowAHAEmailUpdatedDate=getdate()
	where PersonGUID=@PersonGUID

end

