﻿
CREATE Proc [dbo].[Proc_UpdateSelfRecordOfAHAuser]
(
 @UserID int
,@UserHealthRecordID int
)

AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :updating SelfRecordID of AHAuser
*/



Update dbo.AHAUser set   SelfHealthRecordID=@UserHealthRecordID
						,SelfPersonalItemID=@UserHealthRecordID
						,UpdatedDate=getDate()

where UserID=@UserID 

End

