﻿

--select * from provider

--[Proc_SearchProvider] '',null,113,1

CREATE proc [dbo].[Proc_SearchProvider]
( 
 @FirstName nvarchar(250)
,@LastName nvarchar(250)
,@PracticeName nvarchar(250)
,@PracticePhone nvarchar(250)
,@City nvarchar(50)
,@StateID int
,@LanguageID int
,@ZipCode nvarchar(50) = null  -- optional parameter added on 8.26.2013
,@PracticeEmail nvarchar(50) = null -- optional parameter added on 8.26.2013
)

AS

Begin
set nocount on

select p.*,li.listitemcode StateName from provider p 
inner join listitems li
on li.listitemid = p.stateid
where li.LanguageID = @LanguageID and 
(p.FirstName like '%' + @FirstName + '%'
or p.LastName like '%' + @LastName + '%'
or p.PracticeName like '%' + @PracticeName + '%'
or p.PracticePhone like '%' + @PracticePhone + '%'
or p.City like '%' + @City + '%'
or p.StateID = @StateID
or (@ZipCode is not null and p.Zip like '%' + @ZipCode + '%')
or (@PracticeEmail is not null and p.PracticeEmail like '%' + @PracticeEmail + '%'))
End









