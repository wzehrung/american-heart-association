﻿CREATE procedure [dbo].[Proc_FindEmailSchedulerByID]
(@SchedulerID int)
as
begin

select EL.*,C.Title Title from EmailListScheduler EL
left join CampaignDetails C
on EL.CampaignID = C.CampaignID
where EL.SchedulerID = @SchedulerID
end