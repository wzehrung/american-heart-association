﻿


CREATE Procedure [dbo].[Proc_CreateOrChangeDiet]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier 
,@CalorieAmount int
,@Meal nvarchar(50) 
,@FoodItems nvarchar(max) 
,@DateOfEntry datetime  
)

AS

Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (Diet table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

set nocount on

Begin Try
Begin Transaction

if exists(select DietID from dbo.Diet where HVItemGuid=@HVItemGuid )

Exec Proc_UpdateDiet	     @HVItemGuid
							,@HVVersionStamp
							,@CalorieAmount
							,@Meal
							,@FoodItems
							,@DateOfEntry
									
												

else 
begin
		
Declare @UserHealthRecordID int
select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							where PersonalItemGUID=@PersonalItemGUID
							


Exec Proc_InsertDiet				 @UserHealthRecordID
									,@HVItemGuid
									,@HVVersionStamp
									,@CalorieAmount
									,@Meal
									,@FoodItems
									,@DateOfEntry
												
end

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End






