﻿
CREATE PROCEDURE [dbo].[Proc_FindProviderByEmail]
(
@Email nvarchar(255)
)
As
Begin
set nocount on

IF (SELECT TOP 1 CampaignID FROM Provider WHERE Email = @Email) IS NOT NULL

	select p.*,l.LanguageLocale, c.URL AS CampaignURL from Provider p
	inner join Languages l on p.DefaultLanguageID = l.LanguageID
	INNER JOIN Campaign c ON c.CampaignID = p.CampaignID
	where email=@email

ELSE
	
	SELECT p.*,l.LanguageLocale, '' AS CampaignURL FROM Provider p
		INNER JOIN Languages l ON p.DefaultLanguageID = l.LanguageID

	WHERE email=@email

END
