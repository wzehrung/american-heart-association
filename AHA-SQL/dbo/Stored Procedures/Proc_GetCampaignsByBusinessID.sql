﻿


CREATE PROC [dbo].[Proc_GetCampaignsByBusinessID]
(
	@BusinessID int
)

AS

BEGIN

WITH camps AS
(
	SELECT CampaignID,Title,row_number() OVER(PARTITION BY campaignid ORDER BY CampaignID) ROW  
	FROM CampaignDetails 
	WHERE title IS NOT NULL
)

	SELECT C. *,case when Title is null then (select title from camps cs where cs.CampaignID=C.CampaignID and row=1)  
	else Title end As Title , C.BusinessID, ab.Name AS BusinessName
	from campaign C left join CampaignDetails CD on C.CampaignID=CD.CampaignID
	--and LanguageID=@LanguageID	

		LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

	where EndDate >= dateadd(year,-2,getdate()) and StartDate<=getdate()
		AND c.BusinessID = @BusinessID

	order by Title

END


