﻿

CREATE PROC  [dbo].[Proc_MarkExistingInvitationsAsBlocked]
(
 @PatientID int
,@ProviderId int
,@isSentByProvider bit=1
)
As
begin

set nocount on 

if @isSentByProvider=1

	update B  set Status=3
	FROM PatientProviderInvitation A
	inner join
	PatientProviderInvitationDetails B
	on A.InvitationID=B.InvitationID and SentProviderId=@ProviderId and AcceptedPatientID=@PatientID
	and Status not in (3,4)
else if @isSentByProvider=0

	update B  set Status=3
	FROM PatientProviderInvitation A
	inner join
	PatientProviderInvitationDetails B
	on A.InvitationID=B.InvitationID and SentPatientID=@PatientID and AcceptedProviderID=@ProviderId
	and Status not in (3,4)

else
	begin

	update B  set Status=3
		FROM PatientProviderInvitation A
		inner join
		PatientProviderInvitationDetails B
		on A.InvitationID=B.InvitationID and SentProviderId=@ProviderId and AcceptedPatientID=@PatientID

	update B  set Status=3
	FROM PatientProviderInvitation A
	inner join
	PatientProviderInvitationDetails B
	on A.InvitationID=B.InvitationID and SentPatientID=@PatientID and AcceptedProviderID=@ProviderId
	and Status not in (3,4)

	end


End



