﻿
CREATE PROCEDURE [dbo].[Proc_SetHasProvidedOfflineAccess]
(
 @PatientID int
)
As
Begin

Update HEALTHRECORD set HasProvidedOfflineAccess=1 
,UpdatedDate=GETDATE()
where UserHealthRecordID=@PatientID

End

