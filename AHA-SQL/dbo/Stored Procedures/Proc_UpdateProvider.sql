﻿
CREATE PROCEDURE [dbo].[Proc_UpdateProvider]
(
 @Email nvarchar(255)
--,@Password nvarchar(255)
,@FirstName nvarchar(50)
,@LastName nvarchar(50)
,@Salutation nvarchar(50)
,@PracticeName nvarchar(50)
,@PracticeEmail nvarchar(50)
,@PracticeAddress nvarchar(50)
,@PracticePhone nvarchar(50)
,@PracticeFax nvarchar(50)
,@Speciality nvarchar(50)
,@PharmacyPartner nvarchar(50)
,@ProfileMessage nvarchar(max)
,@PracticeUrl nvarchar(255)
,@City nvarchar(50)
,@StateID int
,@Zip nvarchar(50)
--,@MobileForNotification nvarchar(50)
--,@EmailForNotification nvarchar(50)
,@AllowPatientsToSendMessages bit
,@DismissAlertAfterWeeks INT
,@IncludeAlertNotificationInDailySummary bit
,@IncludeMessageNotificationInDailySummary bit
,@NotifyNewAlertImmediately bit
,@NotifyNewMessageImmediately bit
,@EmailForDailySummary nvarchar(255)
,@EmailForIndividualAlert nvarchar(255)
,@EmailForIndividualMessage nvarchar(255)
,@AllowAHAEmail bit
,@ProviderID int 

)
As
Begin

set nocount on

Update provider 

set  Email=@Email
	--,Password =@Password
	,FirstName =@FirstName
	,LastName =@LastName
	,Salutation =@Salutation
	,PracticeName =@PracticeName
	,PracticeEmail =@PracticeEmail
	,PracticeAddress =@PracticeAddress
	,PracticePhone= @PracticePhone
	,PracticeFax= @PracticeFax
	,Speciality =@Speciality
	,PharmacyPartner=@PharmacyPartner
	,ProfileMessage=@ProfileMessage 
	,PracticeUrl=@PracticeUrl
	,City = @City
	,StateID = @StateID
	,Zip = @Zip
	--,@MobileForNotification nvarchar(50)
	--,EmailForNotification = @EmailForNotification 
	,AllowPatientsToSendMessages = @AllowPatientsToSendMessages 
	,DismissAlertAfterWeeks = @DismissAlertAfterWeeks 
	,IncludeAlertNotificationInDailySummary = @IncludeAlertNotificationInDailySummary
	,IncludeMessageNotificationInDailySummary =@IncludeMessageNotificationInDailySummary
	,NotifyNewAlertImmediately = @NotifyNewAlertImmediately
	,NotifyNewMessageImmediately = @NotifyNewMessageImmediately 
	,EmailForDailySummary = @EmailForDailySummary
	,EmailForIndividualAlert= @EmailForIndividualAlert
	,EmailForIndividualMessage =@EmailForIndividualMessage
	,AllowAHAEmail = @AllowAHAEmail


where ProviderID = @ProviderID  
	
End





