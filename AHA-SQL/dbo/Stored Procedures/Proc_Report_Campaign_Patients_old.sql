﻿
--[PROC_Report_Campaign_Patients] '2009.06.01','2010.05.05'

CREATE PROC [dbo].[Proc_Report_Campaign_Patients_old]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null

)
As

BEGIN
set nocount on
;with RptTypes As
(
select C.CampaignID,Title,CampaignCode,StartDate,EndDate from Campaign C inner join CampaignDetails CD on C.CampaignID=CD.CampaignID
and LanguageID=1
),

RptTypesWithDates As
(
select *,Case when (StartDate>=date1 and  StartDate < Date2) or (EndDate >=date1 and EndDate< Date2) then 1 else 0 end isEligible
				from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,CampaignCount As
(
select R.CampaignID,count(distinct A.UserID) TotalCount
from RptTypesWithDates R Left join AHAUser A on A.CreatedDate < R.Date2 
left join AHAUserHealthRecord AHR on AHR.UserID=A.UserID
left join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID  and H.CampaignID=R.CampaignID
where isnull(H.CampaignID,1)=isnull(isnull(@CampaignID,H.CampaignID),1)
GROUP BY R.CampaignID

)
,Result As
(
select Date1,Title,case when isEligible=1 then ISNULL(TotalCount,0) else 0 End As UserCount,ISeLIGIBLE
from RptTypesWithDates R left join CampaignCount C on R.CampaignID=C.CampaignID
)

--/*
,ToBePivoted As
(
Select date1,Title, UserCount
,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
from Result
)
		
select 

 Title
,[Column # 1] as [Column # 1] 
,[Column # 2] as [Column # 2]
,[Column # 3] as [Column # 3]
,[Column # 4] as [Column # 4] 
,[Column # 5] as [Column # 5]
,[Column # 6] as [Column # 6]
,[Column # 7] as [Column # 7]
,[Column # 8] as [Column # 8] 
,[Column # 9] as [Column # 9]
,[Column # 10] as [Column # 10]
,[Column # 11] as [Column # 11]
,[Column # 12] as [Column # 12]


from
(
select	Title,ColumnIndex,UserCount
		from ToBePivoted
) A
PIVOT
(
SUM (UserCount)
FOR ColumnIndex IN
( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
 , [Column # 10],[Column # 11],[Column # 12] )
) B

ORDER BY Title
--*/

END

