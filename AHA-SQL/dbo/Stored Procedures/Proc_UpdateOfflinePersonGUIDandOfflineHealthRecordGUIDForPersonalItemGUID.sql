﻿

CREATE PROC [dbo].[Proc_UpdateOfflinePersonGUIDandOfflineHealthRecordGUIDForPersonalItemGUID]
(
 @PersonalItemGUID uniqueidentifier
,@HealthRecordGUID uniqueidentifier
,@OfflinePersonGUID uniqueidentifier
)
As

Begin
set nocount on

Update HealthRecord set OfflinePersonGUID=@OfflinePersonGUID,OfflineHealthRecordGUID= @HealthRecordGUID
						
where @PersonalItemGUID=PersonalItemGUID


End
