﻿
CREATE PROC [dbo].[Proc_Report_GetAllCampaignsForAffiliates] (@Affiliate NVARCHAR(500))
AS

BEGIN

--DECLARE @Affiliate NVARCHAR(500)
DECLARE @SQLQuery AS NVARCHAR(500)

--SET @Affiliate = '''Western States'''

IF @Affiliate IS NOT NULL
BEGIN

	SELECT CampaignID, Title FROM Heart360..v_AffiliatesCampaigns WHERE AffiliateName = @Affiliate ORDER BY Title

END

ELSE
BEGIN

	SET @SQLQuery = 'SELECT CampaignID, Title FROM Heart360..CampaignDetails ORDER BY Title'	

END

EXECUTE sp_executesql @SQLQuery

END