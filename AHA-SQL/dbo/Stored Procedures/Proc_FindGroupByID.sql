﻿
CREATE PROCEDURE [dbo].[Proc_FindGroupByID]
(
@GroupID int
)
As
Begin
set nocount on

declare @TotalAlerts int

select @TotalAlerts=count(distinct AlertID) from alertRule AR inner join
AlertMapping AM on AR.AlertRuleID=AM.AlertRuleID
AND  AM.GroupID=@GroupID
inner join alert A on AR.AlertRuleID=A.AlertRuleID AND A.isDismissed=0


	select *,(SELECT Count(*) from PatientGroupMapping where GroupID=@GroupID)
	As TotalMembers ,@TotalAlerts as TotalAlerts from PatientGroupDetails where GroupID=@GroupID
	
End

