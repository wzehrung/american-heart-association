﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROC [dbo].[Proc_Report_GetAllMarketsForAffiliate] (@AffiliateName NVARCHAR(500))
AS

BEGIN

DECLARE @SQLQuery AS NVARCHAR(500)

IF @AffiliateName IS NOT NULL
BEGIN

	SET @SQLQuery = 'SELECT DISTINCT CBSA_Code as MarketId, SUBSTRING( CBSA_Name, 1, CHARINDEX( '','', CBSA_Name) + 3) as MarketName from Heart360..ZipRegionMapping WHERE Affiliate = ''' + @AffiliateName + ''' AND CBSA_Code IS NOT NULL ORDER BY MarketId'

END

ELSE
BEGIN

	SET @SQLQuery = 'SELECT DISTINCT CBSA_Code as MarketId, SUBSTRING( CBSA_Name, 1, CHARINDEX( '','', CBSA_Name) + 3) as MarketName from Heart360..ZipRegionMapping WHERE CBSA_Code IS NOT NULL ORDER BY MarketId'	

END

EXECUTE sp_executesql @SQLQuery

END

