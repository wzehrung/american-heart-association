﻿

CREATE PROC [dbo].[Proc_CreateContent]
(
 @Title nvarchar(200)
,@DidYouKnowLinkUrl nvarchar(1024)
,@DidYouKnowLinkTitle nvarchar(max)
,@DidYouKnowText nvarchar(max)
,@ContentRuleIDs nvarchar(max)
,@ExpirationDate datetime
,@CreatedByUserID int
,@LanguageID int
,@ContentID int output
)

As

Begin
set nocount on

Begin Try
Begin Transaction


	IF @ContentID IS NULL OR @ContentID=0
	set @ContentID = (select max(ContentID)+1 from Content )

	insert Content (ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate ,CreatedByUserID ,UpdatedByUserID,LanguageID )

	select @ContentID,@Title,@DidYouKnowLinkUrl,@DidYouKnowLinkTitle,@DidYouKnowText,@ExpirationDate ,@CreatedByUserID ,@CreatedByUserID,@LanguageID



	set @ContentRuleIDs=@ContentRuleIDs+','

			;WITH Strings(Segments, Processing)
			AS
			(
			SELECT
					 SUBSTRING(@ContentRuleIDs,1, CHARINDEX(',', @ContentRuleIDs)-1) Segments
					,SUBSTRING(@ContentRuleIDs,CHARINDEX(',', @ContentRuleIDs)+1, len(@ContentRuleIDs)) Processing

					UNION ALL

					SELECT
					 SUBSTRING(Processing,1, CHARINDEX(',', Processing)-1) Segments
					,SUBSTRING(Processing,CHARINDEX(',', Processing)+1, len(Processing)) Processing

					FROM Strings

					WHERE
					CHARINDEX(',', Processing) > 0
			)


	Insert ContentRuleMapping (ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,LanguageID)

	select Segments,@ContentID,@CreatedByUserID,@CreatedByUserID,@LanguageID from Strings

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End

