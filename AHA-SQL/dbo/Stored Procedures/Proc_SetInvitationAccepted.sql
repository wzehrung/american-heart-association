﻿

CREATE PROCEDURE [dbo].[Proc_SetInvitationAccepted]
(
 @InvitationCode nvarchar(6)
)
As
Begin

declare @InvitationID int

set @InvitationID=(select InvitationID  from PatientProviderInvitation where InvitationCode=@InvitationCode)

Update PatientProviderInvitationDetails set DateAccepted=getdate() where InvitationID=@InvitationID

End


