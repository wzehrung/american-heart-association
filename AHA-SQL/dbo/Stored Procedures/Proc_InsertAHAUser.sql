﻿CREATE Procedure [dbo].[Proc_InsertAHAUser]
(
 @PersonGUID uniqueidentifier  
,@SelfHealthRecordID int 
,@SelfPersonalItemID int
,@LanguageID int
,@UserID int OutPut
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :This SP Insert a Record on AHAUser Table With required Fields
*/


	Insert dbo.AHAUser 
				  (					 
					 PersonGUID
					,SelfHealthRecordID
					,SelfPersonalItemID
					,DefaultLanguageID
				   )
	Select			 @PersonGUID   
					,@SelfHealthRecordID 
					,@SelfPersonalItemID 
					,@LanguageID
					
	SET @UserID=SCOPE_IDENTITY()

End



