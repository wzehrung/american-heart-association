﻿
CREATE PROC [dbo].[Proc_Polka_MarkPolkaDataAsUploadedToHV]
(
 @PersonalItemGUID Uniqueidentifier
,@xmlPolkaData xml
)
As


Begin 


Begin Try
Begin Transaction

	declare @UserHealthRecordID int

	set @UserHealthRecordID =(select UserHealthRecordID  from HealthRecord where PersonalItemGUID=@PersonalItemGUID)


	--WeightLogItem


	Update P set P.IsUploadedToHV=1 from PolkaData P inner  join
	(
	select  C.D.value('@WTRecordID[1]','int') UniqueRecordID
	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('WeightLogItem') C(D)
	) X  on

	X.UniqueRecordID=P.PolkaDataUniqueID and  P.UserHealthRecordID=@UserHealthRecordID 
	and ItemType='WeightLogItem'


	--PhysicalActivityLogItem

	Update P set P.IsUploadedToHV=1 from PolkaData P inner  join
	(
	select C.D.value('@PARecordID[1]','int') UniqueRecordID
	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('PhysicalActivityLogItem') C(D)
	) X  on

	X.UniqueRecordID=P.PolkaDataUniqueID and  P.UserHealthRecordID=@UserHealthRecordID  
	and ItemType='PhysicalActivityLogItem'


	--BloodGlucoseLogItem
	Update P set P.IsUploadedToHV=1 from PolkaData P inner  join
	(
	select  C.D.value('@BGRecordID[1]','int') UniqueRecordID
	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('BloodGlucoseLogItem') C(D)
	) X  on

	X.UniqueRecordID=P.PolkaDataUniqueID and  P.UserHealthRecordID=@UserHealthRecordID  
	and ItemType='BloodGlucoseLogItem'

	--BloodPressureLogItem

	Update P set P.IsUploadedToHV=1 from PolkaData P inner  join
	(
	select C.D.value('@BPRecordID[1]','int') UniqueRecordID
	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('BloodPressureLogItem') C(D)
	) X  on

	X.UniqueRecordID=P.PolkaDataUniqueID and  P.UserHealthRecordID=@UserHealthRecordID  
	and ItemType='BloodPressureLogItem'

	--CholesterolLogItem

	Update P set P.IsUploadedToHV=1 from PolkaData P inner  join
	(
	select C.D.value('@CHRecordID[1]','int') UniqueRecordID
	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('CholesterolLogItem') C(D)
	) X  on

	X.UniqueRecordID=P.PolkaDataUniqueID and  P.UserHealthRecordID=@UserHealthRecordID  
	and ItemType='CholesterolLogItem'


	--MedicationLogItem

	Update P set P.IsUploadedToHV=1 from PolkaData P inner  join
	(
	select C.D.value('@MDRecordID[1]','int') UniqueRecordID
	from @xmlPolkaData.nodes('/PolkaData') A(B) cross apply A.B.nodes('MedicationLogItem') C(D)
	) X  on
	X.UniqueRecordID=P.PolkaDataUniqueID and  P.UserHealthRecordID=@UserHealthRecordID  
	and ItemType='MedicationLogItem'


COMMIT
End Try

Begin Catch

 If @@TRANCOUNT > 0
	ROLLBACK

	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End



