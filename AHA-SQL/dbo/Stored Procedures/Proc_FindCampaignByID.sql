﻿
CREATE PROC [dbo].[Proc_FindCampaignByID]
	(
		 @CampaignID int
		,@LanguageID int
	)

AS

BEGIN
SET NOCOUNT ON

select C.*,CD.LanguageID,CD.Description,CD.Title,CD.PromotionalTagLine,CD.PartnerLogoVersion,CD.PartnerImageVersion,
CONVERT(int, cd.IsPublished) AS IsPublished, 
case when Convert(Datetime,Convert(Varchar,getdate(),103),103) between StartDate and  EndDate  and isPublished=1 
then 1 else 0 end As isActive, cd.ResourceTitle, cd.ResourceURL, C.BusinessID, ab.Name AS BusinessName, ac.AffiliateID, ac.AffiliateName

--,(SELECT COUNT(*) FROM HealthRecord WHERE CampaignID = @CampaignID) AS Participants

FROM Campaign C 
	LEFT JOIN CampaignDetails CD 
		ON C.CampaignID = CD.CampaignID 
			AND LanguageID = @LanguageID 
				
	LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

	LEFT JOIN v_AffiliatesCampaigns ac
		ON c.CampaignID = ac.CampaignID

WHERE C.CampaignID = @CampaignID

END
