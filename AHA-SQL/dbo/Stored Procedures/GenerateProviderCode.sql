﻿

CREATE PROC [dbo].[GenerateProviderCode]
(
@alpha_numeric NVARCHAR(6) OUTPUT
)
As

BEGIN  
  

SET @alpha_numeric=''

SELECT @alpha_numeric=@alpha_numeric+CHAR(n) FROM

(
    SELECT TOP 6 number AS n FROM master..spt_values

    WHERE TYPE='p' and (number between 48 and 57 or number between 65 and 90)

    ORDER BY NEWID() 

) AS t


                   
IF EXISTS (SELECT * FROM Provider WHERE 
ProviderCode=@alpha_numeric)
exec GenerateProviderCode @alpha_numeric output
 
     
END

