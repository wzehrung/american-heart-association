﻿
CREATE Procedure [dbo].[Proc_InsertPatientPINValidationLog]
(
 @ReminderID Int
,@PIN varchar(4)
,@TimeZone nvarchar(25)
,@Valid bit
)
AS

Begin

--This Sp should not be called from within application/product

/* Author	    :Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on


Insert [3CiPINValidationLog] 
					(
					 ReminderID					
					,PIN
					,Valid
					,CreatedDateTime
					)
Select              
					 @ReminderID
					,@PIN
					,@Valid
					,getdate()

End
