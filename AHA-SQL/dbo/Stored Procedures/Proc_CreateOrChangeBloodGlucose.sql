﻿



CREATE Procedure [dbo].[Proc_CreateOrChangeBloodGlucose]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@BloodGlucoseValue float
,@ActionITook  nvarchar(100)
,@ReadingType  nvarchar(100)
,@DateMeasured datetime
,@Notes nvarchar(MAX) 
,@ReadingSource nvarchar(100)
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (BloodGlucose table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/
set nocount on

Begin Try
Begin Transaction						

if exists(
		  select BloodGlucoseID from dbo.BloodGlucose where 
		  HVItemGuid=@HVItemGuid 	
		  )

Exec Proc_UpdateBloodGlucose	 @HVItemGuid 
								,@HVVersionStamp 
								,@BloodGlucoseValue 
								,@ActionITook
								,@ReadingType
								,@DateMeasured 
								,@Notes
								,@ReadingSource
								
												

else 
Begin

Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
					     
	  

Exec Proc_InsertBloodGlucose     @UserHealthRecordID 
								,@HVItemGuid 
								,@HVVersionStamp 
								,@BloodGlucoseValue 
								,@ActionITook
								,@ReadingType
								,@DateMeasured 
								,@Notes
								,@ReadingSource					
								
End
	
exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

																							
End












