﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 06.03.2014
-- Description:	Update values of an existing ProviderNote
-- =============================================
CREATE PROCEDURE [dbo].[Proc_UpdateProviderNote]
(
	@ProviderNoteID int,
	@Title  nvarchar(128),
	@Note   nvarchar(max)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   UPDATE ProviderNote SET Title=@Title, Note=@Note WHERE ProviderNoteID=@ProviderNoteID
END

