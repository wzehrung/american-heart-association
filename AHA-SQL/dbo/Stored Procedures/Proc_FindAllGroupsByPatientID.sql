﻿


CREATE PROC [dbo].[Proc_FindAllGroupsByPatientID] 
(
 @PatientID int
,@ProviderID int
)

As

Begin

select P1.* from PatientGroupDetails P1
inner join PatientGroupMapping P2 on P1.GroupID=P2.GroupID
and P2.PatientID=@PatientID and P1.ProviderID=@ProviderID


End

