﻿
CREATE  PROC [dbo].[Proc_CreateEmailListScheduler]
(
 @CampaignID int 
,@ScheduledDate datetime
,@IsProviderList bit=0
,@IsDownloaded bit =0
,@SchedulerID int output
)

AS

Begin

insert EmailListScheduler
(
CampaignID,ScheduledDate,IsProviderList,IsDownloaded,CreatedDate,UpdatedDate
)
select @CampaignID,@ScheduledDate,@IsProviderList,@IsDownloaded,getdate(),getdate()

set @SchedulerID=scope_identity()


End



