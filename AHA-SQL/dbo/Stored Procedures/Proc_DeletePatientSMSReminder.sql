﻿
CREATE Procedure [dbo].[Proc_DeletePatientSMSReminder]
(
 @PatientID Int
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-09-14  
   Description      :Deleting Patient SMS reminder
*/
set nocount on
DECLARE @ReminderID int

SELECT 	@ReminderID = ReminderID
FROM [3CiPatientReminder]
WHERE PatientID = @PatientID 

DELETE [3CiPatientReminderSchedule] WHERE ReminderID = @ReminderID

DELETE [3CiPatientReminder] WHERE PatientID = @PatientID 
					


End


/**************************************************************************/


/****** Object:  StoredProcedure [dbo].[Proc_GetPatientSMSReminderSchedules]    Script Date: 08/21/2012 17:35:51 ******/
SET ANSI_NULLS ON
