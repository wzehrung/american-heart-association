﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 05.24.2013
-- Description:	Needed by Admin Tool
-- =============================================
CREATE PROCEDURE [dbo].[Proc_UpdateUserCampaign]
(
	@UserID int,
	@CampaignID int
)
AS
BEGIN

	UPDATE dbo.Provider
	   SET CampaignID = @CampaignID
		WHERE ProviderID = @UserID
       
END

