﻿
--PROC_Report_H360Providers '2009.02.01','2010.02.01',18

CREATE PROC PROC_Report_H360Providers
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
)
As

BEGIN
set nocount on
;with RptTypes As
(
select 1 RptID,'Heart360 New Providers' As RptType
union all
select 2 RptID,'Heart360 Total Providers at end of month' As RptType
union all
select 3 RptID,'Heart360 Total Active Providers this month' As RptType
union all
select 4 RptID,'Heart360 Total Spanish Providers at end of month' As RptType

),
RptTypesWithDates As
(
select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,Result As
(
select RptID,R.Date1,count(distinct H.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join ProviderLoginDetails A on A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2
left join Provider H on H.ProviderID=A.ProviderID  and isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
where  R.RptID=3 
GROUP BY R.Date1,R.RptType,RptID

union all

select RptID,R.Date1,count(distinct A.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join Provider A on A.DateOfRegistration < R.Date2 
 and isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1) 
 where  R.RptID=2
 GROUP BY R.Date1,R.RptType,RptID

union all

select RptID,R.Date1,count(distinct A.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join Provider A on A.DateOfRegistration >= R.Date1 
and  A.DateOfRegistration < R.Date2  and isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1) 
where  R.RptID=1
GROUP BY R.Date1,R.RptType,RptID

union all

select RptID,R.Date1,count(distinct A.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join Provider A on A.DateOfRegistration < R.Date2 and A.DefaultLanguageID=2
 and isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1) 
 where  R.RptID=4
 GROUP BY R.Date1,R.RptType,RptID

)


,ToBePivoted As
(
Select date1,RptType,ProviderCount,RptID 
,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
from Result
)
		
select 

 RptType
,[Column # 1] as [Column # 1] 
,[Column # 2] as [Column # 2]
,[Column # 3] as [Column # 3]
,[Column # 4] as [Column # 4] 
,[Column # 5] as [Column # 5]
,[Column # 6] as [Column # 6]
,[Column # 7] as [Column # 7]
,[Column # 8] as [Column # 8] 
,[Column # 9] as [Column # 9]
,[Column # 10] as [Column # 10]
,[Column # 11] as [Column # 11]
,[Column # 12] as [Column # 12]


from
(
select	RptType,ColumnIndex,ProviderCount,RptID
		from ToBePivoted
) A
PIVOT
(
SUM (ProviderCount)
FOR ColumnIndex IN
( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
 , [Column # 10],[Column # 11],[Column # 12] )
) B

order by RptID


END
