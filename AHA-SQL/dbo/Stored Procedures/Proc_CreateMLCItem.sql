﻿
create Procedure Proc_CreateMLCItem
(
 @PersonalItemGUID uniqueidentifier 
,@HealthScore float
,@ActivityRating int 
,@BloodSugarRating int 
,@BPRating int 
,@CholRating int 
,@WeightRating int 
,@DietRating int 
,@SmokingRating int 
,@SystolicBP int
,@DiastolicBP int
,@TotalCholesterol int
,@BloodGlucose float
,@BMI float
)

AS

Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a new entry to ExtendedProfile Table
*/

set nocount on

Begin Try
Begin Transaction


Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							 

Exec Proc_InsertMLCItem		 @UserHealthRecordID
									,@HealthScore,@ActivityRating
						,@BloodSugarRating
						,@BPRating
						,@CholRating
						,@WeightRating
						,@DietRating
						,@SmokingRating
						,@SystolicBP
						,@DiastolicBP
						,@TotalCholesterol
						,@BloodGlucose
						,@BMI

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End
