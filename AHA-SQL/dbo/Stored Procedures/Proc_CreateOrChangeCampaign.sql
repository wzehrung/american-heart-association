﻿
CREATE PROC [dbo].[Proc_CreateOrChangeCampaign]  
(  
 @Title nvarchar(1024)  
,@Description nvarchar(max)  
,@CampaignCode nvarchar(64)  
,@Url nvarchar(2048)  
,@StartDate Datetime  
,@EndDate Datetime  
,@LanguageID int  
,@CampaignID int 
,@PromotionalTagLine nvarchar(MAX)
,@PartnerLogo varbinary(MAX)
,@PartnerLogoContentType nvarchar(10)
,@PartnerImage varbinary(MAX)
,@PartnerImageContentType nvarchar(10)
,@strRscTitle nvarchar(255)
,@strRscURL nvarchar(255)
,@BusinessID int
)  
  
As  
  
Begin  
set nocount on  



Begin Try
Begin Transaction

  
if exists (select * from Campaign where CampaignID=@CampaignID )  
  
exec Proc_UpdateCampaign  
 @Title 
,@Description   
,@StartDate   
,@EndDate   
,@CampaignID   
,@LanguageID   
,@PromotionalTagLine
,@PartnerLogo
,@PartnerLogoContentType
,@PartnerImage 
,@PartnerImageContentType
,@strRscTitle
,@strRscURL
,@BusinessID
else  
  
  
  
exec Proc_CreateCampaign  
  
 @Title   
,@Description   
,@CampaignCode   
,@Url   
,@StartDate   
,@EndDate   
,@LanguageID   
,@CampaignID
,@PromotionalTagLine
,@PartnerLogo
,@PartnerLogoContentType
,@PartnerImage   
,@PartnerImageContentType  
,@strRscTitle
,@strRscURL  
,@BusinessID
 
COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch 
  
End  
  


