﻿

CREATE Procedure [dbo].[Proc_UpdateBloodPressure]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Systolic int  
,@Diastolic int 
,@Pulse int 
,@ReadingSource nvarchar(100)
,@Comments nvarchar(max) 
,@DateMeasured datetime  
,@IrregularHeartbeat bit
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :updating BloodPressure table..
*/


Update dbo.BloodPressure

                    SET 
					 HVVersionStamp		= @HVVersionStamp
					,Systolic			= @Systolic 
					,Diastolic			= @Diastolic 
					,Pulse				= @Pulse 
					,ReadingSource		= @ReadingSource 
					,Comments			= @Comments 
					,DateMeasured		= @DateMeasured 
					,UpdatedDate		= getDate()  
					,IrregularHeartbeat = @IrregularHeartbeat

					Where HVItemGuid=@HVItemGuid 
End





