﻿-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.06.2014
-- Description:	Used by the admin portal to assign affiliates to a campaign
-- =============================================
CREATE PROCEDURE [dbo].[Proc_Admin_AssignAffiliateCampaign]
    (
	   @AffiliateID nvarchar(32),
	   @CampaignID int
    )

AS

-- DEBUGGING:
--DROP TABLE #AssignAffiliate
--DECLARE @AffiliateID nvarchar(32),
--	   @CampaignID int

--SET @AffiliateID=N'1|3|6|'
--SET @CampaignID = 999

BEGIN

;WITH AffiliateCampaign AS
(
    SELECT
	   a.AffiliateID,
	   @CampaignID AS CampaignID --c.CampaignID

    FROM Affiliates a 
	   --CROSS JOIN Campaign c 
    WHERE AffiliateID IN (SELECT VALUE FROM dbo.udf_ListToTableString(@AffiliateID,'|'))
	   --AND c.CampaignID = @CampaignID
)

SELECT * INTO #AssignAffiliate FROM AffiliateCampaign

DECLARE cr CURSOR FORWARD_ONLY FAST_FORWARD READ_ONLY FOR SELECT AffiliateID, CampaignID FROM #AssignAffiliate

DECLARE
    @aid int,
    @cid int

OPEN cr

    FETCH NEXT FROM cr INTO @aid, @cid
    WHILE @@FETCH_STATUS = 0

    BEGIN
	   INSERT INTO dbo.AffiliatesCampaigns
	   SELECT @aid, @cid
	   FETCH NEXT FROM cr INTO @aid, @cid
    END

CLOSE cr

DEALLOCATE cr

-- DEBUGGING:
--SELECT * FROM AffiliateCampaign
--SELECT * FROM #AssignAffiliate

END