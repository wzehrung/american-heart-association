﻿CREATE PROC [dbo].[Proc_UpdateAlertNotes]
(
 @AlertID int
,@Notes  nvarchar(max)
)

AS
Begin
set nocount on
Update alert set Notes=@Notes where AlertID=@AlertID

End