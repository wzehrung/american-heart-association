﻿

CREATE PROC [dbo].[Proc_CreateRxTerms]
(
 @RxTermList xml
)
As

Begin


SET NOCOUNT ON

Begin Try
Begin Transaction

	TRUNCATE TABLE RxTerm

	insert RxTerm (RxTerm,IsActive)

	select RxTerms.value('.','nvarchar(4000)'),1

			from @RxTermList.nodes('/RxTermList/string') A(RxTerms)



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End



