﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 6/13/2013
-- Description:	Refactor of Proc_Report_Gender_Campaign for Campaigns/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Gender_Campaign] 
(
	@StartDate Datetime,  
	@EndDate Datetime,
	@CampaignID int = null,	-- null specifies all campaigns
	@Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
	@CampaignIDs nvarchar(max)
)
AS
BEGIN

    DECLARE @cols NVARCHAR(MAX)
	DECLARE @colsConv NVARCHAR(MAX)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
	from ZipRegionMapping 
	where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	--
	-- generate the report
	--
	;With GenderBase As
	(
		SELECT D.Date1, count(distinct AH.UserHealthRecordID) GenderCount,
			   CASE WHEN E.Gender IS NULL OR E.Gender='Unknown' THEN 'Unreported' ELSE Gender END Gender
			FROM DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D 
			inner join AHAUserHealthRecord AH on AH.CreatedDate<D.Date2 
			left join  ExtendedProfile E ON E.UserHealthRecordID=AH.UserHealthRecordID   
			left join HealthRecord H on H.UserHealthRecordID=AH.UserHealthRecordID

			/* OLD WAY
			/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
					just get those is specified */
			WHERE ((@CampaignId IS NULL AND H.CampaignID IS NOT NULL) OR (@CampaignId IS NOT NULL AND @CampaignId = H.CampaignID))
				AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
				*/

			--  NEW WAY:
			--  WHZ: 7/22/2015
			WHERE
			@CampaignIDs IS NULL AND H.CampaignID IS NOT NULL
			OR (H.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AH.UserID IS NOT NULL)

			GROUP BY Date1,CASE WHEN Gender IS NULL OR Gender='Unknown' THEN 'Unreported' ELSE Gender END
	)
	,GenderTypes As
	(
		select 'Male' as GenderType,1 rptID
		union all
		select 'Female' as GenderType ,2 rptID
		union all
		select 'Unreported' as GenderType,3 rptID
	)
	,GenderDates As
	(
		select rptID,D.Date1,G.GenderType 
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D 
			cross join GenderTypes G
	)
	,Gender As
	(
		select rptID,D.Date1,GB.GenderCount,D.GenderType Gender 
			from GenderDates D 
			left join GenderBase GB on D.Date1=GB.date1 and D.GenderType=GB.Gender
	)

	,UserCount As
	(
		select Date1,sum(GenderCount) UserCount 
			from Gender 
			group by Date1

	)

	Select rptID, T.Date1, gender,
	       ISNULL(case when UserCount=0 then 0 else cast(GenderCount as real)*100/cast (UserCount as real) end,0) as Perc,
		   'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex,
		   cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Gender T 
		inner join UserCount U on T.Date1=U.Date1
		

	SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


	SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
									'],[' + t.ColumnIndex
							FROM #ColumnsIndex AS t
							ORDER BY t.IntIndex
							FOR XML PATH('')
						  ), 1, 2, '') + ']'

	SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
									'],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
							FROM #ColumnsIndex AS t
							ORDER BY t.IntIndex
							FOR XML PATH('')
						  ), 1, 2, '') + ']'


	EXECUTE(N'SELECT Gender, '+ @colsConv +' FROM
	(SELECT Gender, ColumnINdex, Perc,rptID FROM #ToBePivoted ) A
	PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;')
END

