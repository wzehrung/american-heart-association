﻿




CREATE proc [dbo].[Proc_CreateAlert]
( 
 @AlertRuleID INT
,@PatientID INT
,@HealthRecordItemGUID UNIQUEIDENTIFIER
,@AlertXml xml
,@ItemEffectiveDate Datetime
,@AlertID INT output
)

AS

Begin
set nocount on

Begin Try
Begin Transaction

if not exists (select * from Alert where HealthRecordItemGUID=@HealthRecordItemGUID and AlertRuleID=@AlertRuleID)
begin

Declare @AlertRuleXml xml

set  @AlertRuleXml =(select AlertData from AlertRule where AlertRuleID=@AlertRuleID)

insert Alert(AlertRuleID,PatientID,HealthRecordItemGUID,AlertXml,ItemEffectiveDate,AlertRuleXml)
select @AlertRuleID,@PatientID,@HealthRecordItemGUID,@AlertXml,@ItemEffectiveDate,@AlertRuleXml

set @AlertID=scope_identity()

end
else 

--Update Alert set 
--
-- UpdatedDate = getdate()
--,ItemEffectiveDate=@ItemEffectiveDate
--,AlertXml	 = @AlertXml where HealthRecordItemGUID=@HealthRecordItemGUID

set @AlertID = (select AlertID from Alert where HealthRecordItemGUID=@HealthRecordItemGUID and AlertRuleID=@AlertRuleID)


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End








