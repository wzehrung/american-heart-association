﻿
CREATE PROCEDURE [dbo].[Proc_FindProviderByCode]
(
@Code nvarchar(50)
)
As
Begin
set nocount on
select p.*,l.LanguageLocale from Provider p
inner join Languages l on p.DefaultLanguageID = l.LanguageID
where p.ProviderCode = @Code
	
End




