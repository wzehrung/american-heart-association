﻿
CREATE Procedure [dbo].[Proc_UpdateBloodGlucose]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@BloodGlucoseValue float 
,@ActionITook  nvarchar(100)
,@ReadingType  nvarchar(100)
,@DateMeasured datetime 
,@Notes nvarchar(MAX) 
,@ReadingSource nvarchar(100)
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand  
   Created Date     :2008-07-04    
   Description      :Updating records of BloodGlucose

   Note : Please dont try to update a indexed foreign key 
*/  

Update dbo.BloodGlucose

                    SET 

					 HVVersionStamp		= @HVVersionStamp
                    ,BloodGlucoseValue	= @BloodGlucoseValue 
					,DateMeasured		= @DateMeasured 
					,ActionITook		= @ActionITook
					,ReadingType		= @ReadingType
                    ,UpdatedDate		= GetDate()
					,ReadingSource		= @ReadingSource
                    ,Notes = @Notes
					Where HVItemGuid=@HVItemGuid 
					
End


	







