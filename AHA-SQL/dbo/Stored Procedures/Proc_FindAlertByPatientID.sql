﻿






CREATE proc [dbo].[Proc_FindAlertByPatientID]
(
 @PatientID INT 
,@ProviderID int
)

AS

Begin
set nocount on

select DISTINCT C.AlertID, C.PatientID, IsDismissed
,CAST(AlertXml AS NVARCHAR(MAX)) AlertXml, CAST(AlertRuleXml AS  NVARCHAR(MAX)) AlertData, CreatedDate,D.type ,Notes 
,ItemEffectiveDate 
FROM
Alert C inner join AlertRule D on C.AlertRuleId=D.AlertRuleID AND C.PatientID=@PatientID
and D.ProviderID=@ProviderID-- and D.isActive=1

order by IsDismissed asc,ItemEffectiveDate desc,CreatedDate desc
End








