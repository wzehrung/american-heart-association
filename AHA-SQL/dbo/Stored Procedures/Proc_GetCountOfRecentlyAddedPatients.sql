﻿
CREATE proc [dbo].[Proc_GetCountOfRecentlyAddedPatients]
(
 @ProviderID int
,@NoOfDays int
,@count int output
)
As
Begin

  SELECT @count=count(Distinct PatientID)
  FROM PatientGroupMapping PGM 
  INNER JOIN PatientGroupDetails PGD ON   PGD.GroupID=PGM.GroupID and PGD.ProviderID=@ProviderID
  and isDefault=1  and date between dateadd(day,-1*@NoOfDays,getdate()) and getdate()

End