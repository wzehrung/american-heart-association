﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.10.2014
-- Description:	Used by the admin portal to get the affiliates assigned to a campaign
-- =============================================

CREATE PROC [dbo].[Proc_Admin_GetAffiliatesByCampaignID]
(
    @CampaignID int
)

AS

BEGIN
SET NOCOUNT ON
	
    SELECT
	   AffiliateID,
	   CampaignID

    FROM AffiliatesCampaigns

    WHERE CampaignID = @CampaignID

END
