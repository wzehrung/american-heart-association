﻿

Create Proc [dbo].[Proc_IsValidPatientForProvider]
(
 @ProviderID int
,@PatientID int
,@IsValid bit output
)
As
Begin

set @IsValid=0

if exists
	(
	select * from PatientGroupDetails A inner join
	PatientGroupMapping B on A.GroupID=B.GroupID and A.ProviderID=@ProviderID
	and B.PatientID=@PatientID
	)
set @IsValid=1


End