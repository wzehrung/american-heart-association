﻿  
--[Proc_DeleteProviderTips]  
CREATE PROC [dbo].[Proc_DeleteProviderTips]  
(  
@ProviderTipsid int
,@LanguageID int  
)  
  
As  
  
Begin  
set nocount on  
  
Begin Try  
Begin Transaction  
  
  
Delete from ProviderTipsMapping where ProviderTipsid=@ProviderTipsid and LanguageID=@LanguageID 
  
Delete from ProviderTips Where ProviderTipsid=@ProviderTipsid  and LanguageID=@LanguageID
  
COMMIT  
End Try  
  
Begin Catch  
  
  If @@TRANCOUNT > 0  
 ROLLBACK  
  
   
 Declare @ErrMsg nvarchar(4000), @ErrSeverity int  
 Select  @ErrMsg = ERROR_MESSAGE(),  
    @ErrSeverity = ERROR_SEVERITY()  
 Raiserror(@ErrMsg, @ErrSeverity, 1)  
  
 End Catch  
  
End  
  
  
  
  