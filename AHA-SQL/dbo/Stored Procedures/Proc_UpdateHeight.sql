﻿

CREATE Procedure [dbo].[Proc_UpdateHeight]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier
,@Height float  
,@Comments nvarchar (max) 
,@DateMeasured datetime 
)
AS

Begin


--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :updating Height table..
*/


Update dbo.Height

              SET 
				 HVVersionStamp		=  @HVVersionStamp
				,Height				=  @Height 
				,Comments			=  @Comments 
				,DateMeasured		=  @DateMeasured 
				,UpdatedDate		=  getDate()  
              
              Where HVItemGuid=@HVItemGuid 
End





