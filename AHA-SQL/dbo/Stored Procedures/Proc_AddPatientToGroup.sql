﻿CREATE PROCEDURE [dbo].[Proc_AddPatientToGroup]
(
 @GroupID nvarchar(max)
,@PatientID nvarchar(max)
,@ProviderID int
,@isSinglePatient bit
)
As
Begin
set nocount on

declare @SplitOn char
set @SplitOn=','

DECLARE @TempGroupID nvarchar(10)

set @TempGroupID=@GroupID

set @PatientID=@PatientID+','

set @GroupID=@GroupID+','


Begin Try
Begin Transaction

if @GroupID is null
begin
select @GroupID=GroupID from PatientGroupDetails 
where ProviderID=@ProviderID and isDefault=1
end

if @isSinglePatient = 0

begin

	SET @GroupID=REPLACE(@GroupID,',','') 
     

	Declare @PatientIDs table
	(
	 PatientID int
	,GroupID int
	)



	/*

	;WITH PatientIDs(PatientID, Child)
	AS
	(
	SELECT
	SUBSTRING(@PatientID,1, CHARINDEX(',', @PatientID)-1) PatientID,
	SUBSTRING(@PatientID,CHARINDEX(',', @PatientID)+1, len(@PatientID)) Child
	UNION ALL
	SELECT
	SUBSTRING(Child,1, CHARINDEX(',', Child)-1) PatientID,
	SUBSTRING(Child,CHARINDEX(',', Child)+1, len(Child)) Child
	FROM PatientIDs
	WHERE
	CHARINDEX(',', Child) > 0
	)
	*/

 While (Charindex(@SplitOn,@PatientID)>0)
	Begin
		insert @PatientIDs 
		Select 
			 ltrim(rtrim(Substring(@PatientID,1,Charindex(@SplitOn,@PatientID)-1))),@GroupID 
			 where ltrim(rtrim(Substring(@PatientID,1,Charindex(@SplitOn,@PatientID)-1)))<>0
		Set @PatientID = Substring(@PatientID,Charindex(@SplitOn,@PatientID)+1,len(@PatientID))
		
	End

	

	;with ExistingPatients As
	(
	select distinct PatientID from PatientGroupMapping where GroupID=@GroupID
	)
	

	insert PatientGroupMapping(GroupID,PatientID)
		select @GroupID,A.PatientID from @PatientIDs A left join  ExistingPatients B 
				on A.PatientID=B.PatientID where B.PatientID is null


	Declare @Exists table
	(
	PatientID int
	)
	insert @Exists
	select distinct PatientID from alertmapping where GroupID=@GroupID

	insert alertmapping(AlertRuleID,PatientID,groupid)
	Select distinct AlertRuleID,PatientID,@GroupID from
	(
	Select A.PatientID from @PatientIDs A left join @Exists B on A.PatientID=B.PatientID
	where B.PatientID is null
	) A cross join

	(select AlertRuleID from AlertRule where GroupID=@GroupID) B


End
else  if @isSinglePatient = 1 AND @TempGroupID is not null
begin

SET @PatientID=REPLACE(@PatientID,',','') 

	--Declare @GroupIDs table
	--(
	--GroupID int
	--)
	--
	--;WITH GroupIDs(GroupID, Child)
	--		AS
	--		(
	--		SELECT
	--		SUBSTRING(@GroupID,1, CHARINDEX(',', @GroupID)-1) GroupID,
	--		SUBSTRING(@GroupID,CHARINDEX(',', @GroupID)+1, len(@GroupID)) Child
	--		UNION ALL
	--		SELECT
	--		SUBSTRING(Child,1, CHARINDEX(',', Child)-1) GroupID,
	--		SUBSTRING(Child,CHARINDEX(',', Child)+1, len(Child)) Child
	--		FROM GroupIDs
	--		WHERE
	--		CHARINDEX(',', Child) > 0
	--		)
	--insert @GroupIDs select GroupID from GroupIDs
	--
	--insert PatientGroupMapping(PatientID,GroupID)
	--	select @PatientID,GroupID from @GroupIDs
	--
	--insert alertmapping(AlertRuleID,PatientID,groupid)
	--select distinct AlertRuleID,@PatientID,A.groupid from alertmapping A inner join
	--@GroupIDs B on A.groupid=B.groupid
	--
	--
	--
	--
	--if not exists
	--	(
	--	select A.alertRuleID from 
	--	AlertRule C 
	--	inner join PatientGroupDetails P on C.ProviderID=P.ProviderID
	--	and C.ProviderID=@ProviderID and P.isDefault=1
	--	inner join  alertmapping A on A.AlertRuleID=C.AlertRuleID  
	--	and A.GroupID=P.GroupID and A.PatientID=@PatientID 
	--	)
	--
	--	insert alertmapping(AlertRuleID,PatientID,groupid)
	--
	----  select distinct  A.AlertRuleID,@PatientID,A.groupid from alertmapping A 
	----	inner join  AlertRule B on A.AlertRuleID=B.AlertRuleID and A.GroupID=0
	----	and B.ProviderID=@ProviderID
	--
	--	select distinct A.alertRuleID,@PatientID,A.groupid from 
	--	AlertRule C 
	--	inner join PatientGroupDetails P on C.ProviderID=P.ProviderID
	--	and C.ProviderID=@ProviderID and P.isDefault=1
	--	inner join  alertmapping A on A.AlertRuleID=C.AlertRuleID  
	--	and A.GroupID=P.GroupID 
	



End
else if @isSinglePatient = 1 AND @TempGroupID is  null
begin

SET @PatientID=REPLACE(@PatientID,',','') 
insert PatientGroupMapping(PatientID,GroupID)
	select @PatientID,@GroupID 

insert alertmapping(AlertRuleID,PatientID,groupid)

 select AlertRuleID,@PatientID,@GroupID from AlertRule where GroupID=@GroupID

--select distinct A.alertRuleID,@PatientID,A.groupid from 
--	AlertRule C 
--	inner join PatientGroupDetails P on C.ProviderID=P.ProviderID
--	and C.ProviderID=@ProviderID and P.isDefault=1
--	inner join  alertmapping A on A.AlertRuleID=C.AlertRuleID  
--	and A.GroupID=P.GroupID 


End



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End


