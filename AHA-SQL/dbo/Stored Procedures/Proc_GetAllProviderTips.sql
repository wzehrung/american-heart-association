﻿

-- [Proc_GetAllProviderTips]

CREATE PROC [dbo].[Proc_GetAllProviderTips]

(
@LanguageID int
)
As

Begin
set nocount on


;WITH Duplicate (RowNumber, ProviderTipsID, ProviderTipsTypeID, ProviderTipsTypeIDs) AS
(
  SELECT 1, C.ProviderTipsID, cast ((B.ProviderTipsTypeID) as varchar(20)), CAST((B.ProviderTipsType) AS VARCHAR(8000)) 
  FROM ProviderTipsMapping C
  INNER JOIN ProviderTipsType B ON B.ProviderTipsTypeID=C.ProviderTipsTypeID and B.LanguageID=C.LanguageID and B.LanguageID=@LanguageID and C.LanguageID=@LanguageID
  GROUP BY ProviderTipsID,B.ProviderTipsTypeID,ProviderTipsType having B.ProviderTipsTypeID=min(C.ProviderTipsTypeID)

  UNION ALL

  SELECT CT.RowNumber + 1, C.ProviderTipsID, cast (B.ProviderTipsTypeID as varchar(20)) , CT.ProviderTipsTypeIDs + ', ' + cast (B.ProviderTipsType as varchar(20))
  FROM ProviderTipsMapping C 
  INNER JOIN ProviderTipsType B ON B.ProviderTipsTypeID=C.ProviderTipsTypeID and B.LanguageID=C.LanguageID and B.LanguageID=@LanguageID and C.LanguageID=@LanguageID
  INNER JOIN Duplicate CT ON CT.ProviderTipsID = C.ProviderTipsID 
  WHERE cast (C.ProviderTipsTypeID as varchar(20)) > CT.ProviderTipsTypeID
)

Select A.*,B.ProviderTipsTypeIDs from

ProviderTips A left join 
(
SELECT A.ProviderTipsID, ProviderTipsTypeIDs
from Duplicate A 
inner JOIN (SELECT ProviderTipsID, MAX(RowNumber) 
AS MaxRow FROM Duplicate GROUP BY ProviderTipsID) R
ON A.RowNumber = R.MaxRow AND A.ProviderTipsID = R.ProviderTipsID
) B

on A.ProviderTipsID=B.ProviderTipsID where  A.LanguageID=@LanguageID


End



