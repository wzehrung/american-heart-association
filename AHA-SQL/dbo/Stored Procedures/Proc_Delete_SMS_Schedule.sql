﻿

CREATE Procedure [dbo].[Proc_Delete_SMS_Schedule]
(
	@ReminderScheduleID Int
)
AS

BEGIN

SET NOCOUNT ON

DELETE [3CiPatientReminderSchedule] WHERE ReminderScheduleID = @ReminderScheduleID

END
