﻿

CREATE PROCEDURE [dbo].[Proc_Update_HbA1c]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@HbA1cValue float 
,@DateMeasured datetime 
,@Notes nvarchar(MAX) 
,@ReadingSource nvarchar(100)
)
AS

BEGIN

UPDATE dbo.BloodGlucose_HbA1c

    SET 

	HVVersionStamp		= @HVVersionStamp
    ,HbA1cValue 		= @HbA1cValue  
	,DateMeasured		= @DateMeasured 
    ,UpdatedDate		= GetDate()
	,Notes				= @Notes
	,ReadingSource		= @ReadingSource

	Where HVItemGuid=@HVItemGuid 
					
END
