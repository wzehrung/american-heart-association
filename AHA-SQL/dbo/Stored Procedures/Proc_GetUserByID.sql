﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 05.24.2013
-- Description:	Needed for Admin Tool
-- =============================================
CREATE PROCEDURE [dbo].[Proc_GetUserByID]
	@UserID int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- simple way to handle two cases where the volunteer
	-- has a campaign or doesn't have a campaign
    SELECT 
		p.ProviderID as UserID,
		p.LastName,
		p.FirstName,
		p.Email,
		p.CampaignID,
		cd.Title as CampaignName
		FROM Provider p
		INNER JOIN CampaignDetails cd
		ON p.CampaignID = cd.CampaignID
		WHERE p.ProviderID = @UserID AND cd.LanguageID = 1
	UNION	
	SELECT 
		pp.ProviderID as UserID,
		pp.LastName,
		pp.FirstName,
		pp.Email,
		NULL as CampaignID,
		NULL as CampaignName
		FROM Provider pp WHERE pp.ProviderID = @UserID AND pp.CampaignID is null
END

