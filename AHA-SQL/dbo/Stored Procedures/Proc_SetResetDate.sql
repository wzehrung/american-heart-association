﻿

CREATE PROCEDURE [dbo].[Proc_SetResetDate]
( 
  @ResetGUID UNIQUEIDENTIFIER
)
As
Begin

set nocount on

Update ResetPasswordLog set ResetedDate=getdate() where ResetGUID=@ResetGUID

End



