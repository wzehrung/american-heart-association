﻿

CREATE PROCEDURE [dbo].[Proc_FindPatientByHealthRecordGUID]
(
 @UserHealthRecordGUID uniqueidentifier
)
As
Begin
set nocount on

declare @TotalAlerts int

select @TotalAlerts=count(distinct AlertID) from alertRule AR inner join
AlertMapping AM on AR.AlertRuleID=AM.AlertRuleID  
inner join alert A on AR.AlertRuleID=A.AlertRuleID AND A.isDismissed=0
inner join HealthRecord H on H.UserHealthRecordID=A.PatientID and UserHealthRecordGUID=@UserHealthRecordGUID


	select *, @TotalAlerts as TotalAlerts from  HealthRecord 
					where UserHealthRecordGUID=@UserHealthRecordGUID
	
End

