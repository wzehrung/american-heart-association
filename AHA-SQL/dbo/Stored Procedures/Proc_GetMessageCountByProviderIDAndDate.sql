﻿CREATE PROC [dbo].[Proc_GetMessageCountByProviderIDAndDate]
(
 @ProviderID int
,@RequiredDate Datetime
,@Count int output
)

As

Begin
SET NOCOUNT ON

select @Count=Count(distinct MD.MessageID)  from
Folder F  
inner join UserFolderMapping UFM
on F.FolderID=UFM.FolderID and UFM.ProviderID=@ProviderID and F.FolderType='I'
inner join MessageDetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
inner join message m  on MD.messageid=m.messageid 
and convert(datetime,convert(varchar,m.sentdate,103),103)=convert(datetime,convert(varchar,@RequiredDate,103),103)

End
