﻿

-- =============================================
-- Author:		Phil Brock
-- Create date: 6.17.2013
-- Description:	Refactor of Proc_Report_AllUsers_Campaign to handle Campaign/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_AllUsers_Campaign] 
(
	@StartDate Datetime,  
	@EndDate Datetime,
	@CampaignID int = null,	-- null specifies all campaigns
	@Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
	@CampaignIDs nvarchar(max)
)
AS
BEGIN

	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
	from ZipRegionMapping 
	where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	--
	-- generate the report
	--
	
	select * into #h from	
		(select UserID, AHR.UserHealthRecordID
			from  AHAUserHealthRecord AHR
			inner join HealthRecord H on H.UserHealthRecordID = AHR.UserHealthRecordID
			left join  ExtendedProfile E ON E.UserHealthRecordID = AHR.UserHealthRecordID
			/* OLD WAY
			/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
					just get those is specified */
			WHERE ((@CampaignId IS NULL AND H.CampaignID IS NOT NULL) OR (@CampaignId IS NOT NULL AND @CampaignId = H.CampaignID))
				AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
				*/
			--  NEW WAY:
			--  WHZ: 7/22/2015
			WHERE
			@CampaignIDs IS NULL AND H.CampaignID IS NOT NULL
			OR (H.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AHR.UserID IS NOT NULL)
		) a

	;with RptTypes As
	(
		select 1 RptID,'1 Visit (All)' As RptType
		union all
		select 2 RptID,'2-4 Visits (All)' As RptType
		union all
		select 3 RptID,'5-10 Visits (All)' As RptType
		union all
		select 4 RptID,'>10 Visits (All)' As RptType
		union all
		select 5 RptID,'1 Visit (New)' As RptType
		union all
		select 6 RptID,'2-4 Visits (New)' As RptType
		union all
		select 7 RptID,'5-10 Visits (New)' As RptType
		union all
		select 8 RptID,'>10 Visits (New)' As RptType
	),
	RptTypesWithDates As
	(
		select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,EligibleLogins As --All
	(
		Select R.Date1, ALD.AHAUserID, Count(H.UserID) as Visits 
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R 
			inner join AHAUserLoginDetails ALD on (ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2) 
			inner join #h H ON H.UserID = ALD.AHAUserID
			group by R.Date1,ALD.AHAUserID
	)
	,EligibleUsers As --new 
	(
		Select distinct A.UserID,R.Date1 
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner join AHAUser A on A.CreatedDate >=R.Date1 and A.CreatedDate <R.Date2 
			inner join #h H on H.UserID=A.UserID
	)
	,Visits As
	(
		select E1.date1, 0 as AHAUserID, 
		       Case when Visits =1 then '1 Visit (All)' 
		            when Visits between 2 and 4 then '2-4 Visits (All)'
				    when Visits between 5 and 10 then '5-10 Visits (All)'
				    when Visits >10 then '>10 Visits (All)' 
				End RptType 
			from EligibleLogins E1
		union ALL
		Select R.Date1, ALD.AHAUserID,
			   Case when count(AHAUserID) =1 then '1 Visit (New)' 
			        when count(AHAUserID) between 2 and 4 then '2-4 Visits (New)'
					when count(AHAUserID) between 5 and 10 then '5-10 Visits (New)'
					when count(AHAUserID) >10 then '>10 Visits (New)' 
				End RptType 
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R 
			inner join AHAUserLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2  
			inner join EligibleUsers E1 on E1.UserID=ALD.AHAUserID and E1.date1=R.date1
		group by R.Date1,AHAUserID
	)
	 
--,ToBePivoted As
--(
	Select RptID,R.date1,R.RptType,Count(V.RptType) UserCount 
	,'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
	,cast(dense_Rank() over (order by R.date1) as int) IntIndex
	into #ToBePivoted
	from RptTypesWithDates R left join Visits V
	on V.Date1=R.Date1 and V.RptType=R.RptType  group by R.date1,R.RptType,RptID
--)

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

--select * from #ToBePivoted
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'


SET @query = N'SELECT RptType, '+ @cols +' FROM
(SELECT RptType, ColumnINdex, UserCount,rptID FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;'

EXECUTE(@query)
	
	
	
	
END



