﻿

-- =============================================
-- Author:		Scott Shipp
-- Create date: January 30, 2009
-- Description:	This is a snapshot of the number
-- of users tracking medication.
-- 
-- =============================================
CREATE PROCEDURE [dbo].[Proc_RptUsersTrackingMedication]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select cast ( (select cast(COUNT(distinct UserHealthRecordID) as real) from Medication) / (select cast(count(UserID) as real) from AHAUserHealthRecord) * 100 as numeric(10,2)) as PercentTrackingMedication


END


