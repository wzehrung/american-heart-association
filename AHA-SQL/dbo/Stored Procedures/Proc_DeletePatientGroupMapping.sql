﻿
CREATE PROCEDURE [dbo].[Proc_DeletePatientGroupMapping]
(
 @GroupID int
)
As
Begin
set nocount on

	delete from PatientGroupMapping where GroupID=@GroupID
	
End

