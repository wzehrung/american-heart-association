﻿

CREATE Procedure [dbo].[Proc_InsertMedication]
(
 @UserHealthRecordID int  
,@HVItemGuid uniqueidentifier 
,@HVVersionStamp uniqueidentifier  
,@MedicationName nvarchar (255) 
,@MedicationType nvarchar (255) 
,@Dosage nvarchar (50)   
,@Strength nvarchar(50)  
,@Frequency nvarchar (50) 
,@StartDate Datetime
,@DateDiscontinued Datetime
,@Notes nvarchar(max)
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a record to Medication table..
*/


Insert dbo.Medication 
				(
                     UserHealthRecordID   
					,HVItemGuid  
					,HVVersionStamp   
					,MedicationName 
					,MedicationType 
					,Dosage
					,Strength   
					,Frequency 
					,StartDate
					,DateDiscontinued 
					,Notes                
                  )
Select               @UserHealthRecordID   
					,@HVItemGuid  
					,@HVVersionStamp   
					,@MedicationName 
					,@MedicationType 
					,@Dosage  
					,@Strength 
					,@Frequency 
					,@StartDate
					,@DateDiscontinued 
					,@Notes  
End







