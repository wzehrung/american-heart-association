﻿

CREATE PROC [dbo].[PROC_MarkMessageAsFlagedOrUnflaged]
(
 @MessageID int
,@UserType nvarchar(50) 
,@UserID int 
,@isFlaged bit 
)
As
Begin
set nocount on

declare @UserFolderMappingIDCurrent int

IF @UserType='Patient'
BEGIN
select   @UserFolderMappingIDCurrent=UFM.UserFolderMappingID

	from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.PatientID=@UserID 
	inner join Messagedetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
	and MD.MessageID=@MessageID and MD.PatientID=UFM.PatientID


 Update MessageDetails set isFlaged=@isFlaged where MessageID=@MessageID and PatientID = @UserID and 
 UserFolderMappingID = @UserFolderMappingIDCurrent

END

ELSE IF @UserType='Provider'
begin
select   @UserFolderMappingIDCurrent=UFM.UserFolderMappingID

	from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.ProviderID=@UserID 
	inner join Messagedetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
	and MD.MessageID=@MessageID and MD.ProviderID=UFM.ProviderID

Update MessageDetails set isFlaged=@isFlaged where MessageID=@MessageID and ProviderID = @UserID
and UserFolderMappingID = @UserFolderMappingIDCurrent

end



End

