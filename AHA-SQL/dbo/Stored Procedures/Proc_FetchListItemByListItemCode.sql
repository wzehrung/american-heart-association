﻿CREATE Procedure [dbo].[Proc_FetchListItemByListItemCode]
(
 @ListItemCode nvarchar(20),
 @ListCode nvarchar(2),
 @LanguageID int
)
AS

Begin


/* Auther			:Shipin Anand
   Created Date	    :2008-09-16  
   Description      :
*/
set nocount on

	--select ListItemName From dbo.ListItems  WHERE 
	--						  ListItemCode = @ListItemCode
	select ListItemName From dbo.ListItems LI
					Inner Join dbo.Lists L
		   On L.ListID=LI.ListID and L.ListCode=@ListCode and LI.LanguageID = @LanguageID and LI.ListItemCode = @ListItemCode

End