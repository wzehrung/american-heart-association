﻿

CREATE PROC [dbo].[Proc_FetchDashboardSortOrderForAHAuserHealthRecord]
(
 @PersonGUID uniqueidentifier
,@HealthRecordGUID uniqueidentifier
)

As

Begin
declare @UserID int,@HealthRecordID int

set @UserID = (select UserID from AHAuser where PersonGUID=@PersonGUID)

set @HealthRecordID = (select UserHealthRecordID from HealthRecord where UserHealthRecordGUID=@HealthRecordGUID)

select DashboardSortOrder from AHAuserHealthRecord  WHERE UserID=@UserID  AND UserHealthRecordID=@HealthRecordID

End

