﻿CREATE Procedure [dbo].[Proc_ModifyAHAUser_NewTermsAndConditions] 
( 
 @PersonGUID uniqueidentifier
,@NewTermsAndConditions bit
)
AS 
   
Begin 
/* Author           :Shipin Anand 
   Created Date     :2008-07-31   
   Updated Date     :2016-05-26 - Added simultaneous setting of datetime
   Description      :Updating NewTermsAndConditions of AHAUser for a PersonGUID 
   Note : Please dont try to update a indexed foreign key
*/ 
set nocount on 
Update dbo.AHAUser 
                SET
                     NewTermsAndConditions  = @NewTermsAndConditions,
                     TermsAcceptanceDate = GETDATE()
                    where PersonGUID=@PersonGUID
End

/* ---------- */

