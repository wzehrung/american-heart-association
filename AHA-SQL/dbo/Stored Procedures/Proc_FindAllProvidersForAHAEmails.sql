﻿CREATE PROC [dbo].[Proc_FindAllProvidersForAHAEmails]
(
@CampaignID int = null
)
As


Begin
set nocount on

select * from Provider where AllowAHAEmail=1 AND (CampaignID=@CampaignID or @CampaignID is null)

End

