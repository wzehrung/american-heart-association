﻿--select * from PatientGroupDetails

-- Proc_FindPatientsByGroupID 8

CREATE PROCEDURE [dbo].[Proc_FindPatientsByGroupID]
(
@GroupID int
)
As
Begin
set nocount on

select P.*,PGM.Date, ISNULL(TotalAlerts,0) as TotalAlerts ,
(
select Pmap.Date from  PatientGroupMapping  Pmap 
inner join PatientGroupDetails Pgroup ON Pmap.GroupID=Pgroup.GroupID  where  P.UserHealthRecordID=Pmap.PatientID
and Pgroup.isdefault=1  and Pgroup.ProviderID=PGD.ProviderID
) As ProviderConnectionDate

from

	PatientGroupDetails PGD inner join 
	PatientGroupMapping PGM  on PGD.GroupID=PGM.GroupID and PGD.GroupID=@GroupID
							inner join
	HealthRecord P on P.UserHealthRecordID=PGM.PatientID
	
	LEFT JOIN 
	(	
	select A.PatientID,count(distinct AlertID) TotalAlerts from alertRule AR inner join
	AlertMapping AM on AR.AlertRuleID=AM.AlertRuleID 
	AND AM.GroupID=@GroupID
	inner join alert A on AR.AlertRuleID=A.AlertRuleID AND A.isDismissed=0
	GROUP BY A.PatientID
	) A ON  A.PatientID=P.UserHealthRecordID
	
End

