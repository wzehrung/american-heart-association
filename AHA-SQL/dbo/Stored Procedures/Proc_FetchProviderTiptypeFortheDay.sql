﻿
CREATE Proc [dbo].[Proc_FetchProviderTiptypeFortheDay]  
(  
 @ProviderTipsType nvarchar(100)
,@LanguageID int  
)  
  
As  
  
Begin  
  
  
set nocount on  
  
  
  
select TOP 1 TT.ProviderTipsData from   
  
     dbo.ProviderTipsType TTT   
     inner join ProviderTipsMapping  TTM on  
     TTM.ProviderTipsTypeId=TTT.ProviderTipsTypeId   and TTT.LanguageID=TTM.LanguageID and TTT.LanguageID=@LanguageID  and TTM.LanguageID=@LanguageID 
     inner join   
     dbo.ProviderTips TT on  
     TT.ProviderTipsID=TTM.ProviderTipsId   and TT.LanguageID=TTM.LanguageID 
     and TTT.ProviderTipsType=@ProviderTipsType and TT.LanguageID=@LanguageID    AND TTM.LanguageID=@LanguageID 
     Order by RAND((1000 * TT.ProviderTipsId) * DATEPART(second, GETDATE()))  
End  