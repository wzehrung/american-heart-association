﻿
CREATE PROCEDURE [dbo].[Proc_CreateOrUpdate_HealthHistory]
(
 @UserHealthRecordID int
,@FamilyHistoryHD int = NULL
,@FamilyHistoryStroke int = NULL
,@HighBPDiet int  = NULL
,@HighBPMedication int  = NULL 
,@KidneyDisease int  = NULL
,@DiabetesDiagnosis int  = NULL
,@HeartDiagnosis int  = NULL
,@Stroke int  = NULL
,@DoYouSmoke bit  = NULL
,@HaveYouEverTriedToQuitSmoking  bit = NULL
,@BloodType nvarchar(100)  = NULL
,@MedicationAllergy nvarchar(max) = NULL
,@LastSynchDate Datetime = NULL
)

AS

BEGIN

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION
				 
IF EXISTS(
			  SELECT ExtendedProfileID FROM dbo.ExtendedProfile WHERE 
			  UserHealthRecordID=@UserHealthRecordID
		  )

UPDATE ExtendedProfile

	SET	 
	FamilyHistoryHD					= @FamilyHistoryHD,
	FamilyHistoryStroke				= @FamilyHistoryStroke,
	HighBPDiet						= @HighBPDiet,
	HighBPMedication				= @HighBPMedication,
	KidneyDisease					= @KidneyDisease,
	DiabetesDiagnosis				= @DiabetesDiagnosis,
	HeartDiagnosis					= @HeartDiagnosis,
	Stroke							= @Stroke,
	BloodType						= @BloodType,
	MedicationAllergy				= @MedicationAllergy,
	DoYouSmoke						= @DoYouSmoke,
	HaveYouEverTriedToQuitSmoking	= @HaveYouEverTriedToQuitSmoking,
	LastSynchDate					= isnull(@LastSynchDate,LastSynchDate)

	WHERE UserHealthRecordID = @UserHealthRecordID													

ELSE 

INSERT ExtendedProfile		 
		(
		UserHealthRecordID
		,FamilyHistoryHD
		,FamilyHistoryStroke
		,HighBPDiet
		,HighBPMedication
		,KidneyDisease
		,DiabetesDiagnosis
		,HeartDiagnosis
		,Stroke
		,BloodType
		,MedicationAllergy
		,DoYouSmoke
		,HaveYouEverTriedToQuitSmoking
		,LastSynchDate
		)

SELECT
		@UserHealthRecordID
		,@FamilyHistoryHD
		,@FamilyHistoryStroke
		,@HighBPDiet
		,@HighBPMedication
		,@KidneyDisease
		,@DiabetesDiagnosis
		,@HeartDiagnosis
		,@Stroke
		,@BloodType
		,@MedicationAllergy
		,@DoYouSmoke
		,@HaveYouEverTriedToQuitSmoking
		,@LastSynchDate

DECLARE @PersonalItemGUID uniqueidentifier

SELECT @UserHealthRecordID = UserHealthRecordID
FROM dbo.HealthRecord
WHERE PersonalItemGUID=@PersonalItemGUID

EXEC proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID

COMMIT
END TRY

BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK	
			DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
			SELECT  @ErrMsg = ERROR_MESSAGE(),
					@ErrSeverity = ERROR_SEVERITY()
		RAISERROR(@ErrMsg, @ErrSeverity, 1)

	END CATCH

END
