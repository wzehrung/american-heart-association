﻿
create Procedure Proc_InsertExerciseGoal
(
 @UserHealthRecordID int 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Intensity nvarchar(50)
,@Duration float
)
AS

Begin

--This Sp should not be called from within application/product  
  
/* Author			:Shipin Anand  
   Created Date     :2010-09-06    
   Description      :Insert Record to ExerciseGoal
*/  

Insert dbo.ExerciseGoal 
					(
                       UserHealthRecordID  
					  ,HVItemGuid   
					  ,HVVersionStamp   
					  ,Intensity 
					  ,Duration             
                    )
Select                 @UserHealthRecordID  
					  ,@HVItemGuid   
					  ,@HVVersionStamp   
					  ,@Intensity
					  ,@Duration 
End
