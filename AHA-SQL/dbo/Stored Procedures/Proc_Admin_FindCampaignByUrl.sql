﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.17.2014
-- Description:	Used by the landing page to get campaign info based on /campaignURL
--				Derived from original SP: FindCampaignByUrl.
-- =============================================

CREATE PROC [dbo].[Proc_Admin_FindCampaignByUrl]
(
    @Url nvarchar(2048),
    @LanguageID int
)

AS

BEGIN
SET NOCOUNT ON

SELECT
    c.CampaignID,
    c.Url,
    c.StartDate,
    c.EndDate,
    c.CreatedDate,
    c.UpdatedDate,
    c.BusinessID,
    cd.LanguageID,
    cd.Description,
    cd.Title,
    cd.PromotionalTagLine,
    cd.PartnerLogoVersion,
    cd.PartnerLogoContentType,
    cd.PartnerImageVersion,
    cd.PartnerImageContentType,
    cd.IsPublished,
    CASE 
	   WHEN Convert(Datetime,Convert(Varchar,getdate(),103),103) BETWEEN StartDate AND EndDate AND isPublished=1 
	   THEN 1 
    ELSE 0 
    END AS isActive,
    cd.ResourceTitle,
    cd.ResourceURL,
    ab.Name AS BusinessName,
    cd.CampaignImages

FROM Campaign c

    LEFT JOIN CampaignDetails cd 
	   ON c.CampaignID = cd.CampaignID 
		  AND LanguageID = @LanguageID
				
    LEFT JOIN AdminBusiness ab
	   ON c.BusinessID = ab.BusinessID


WHERE c.Url = @Url

END

