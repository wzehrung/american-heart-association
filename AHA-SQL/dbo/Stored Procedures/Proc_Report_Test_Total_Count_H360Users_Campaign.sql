﻿
CREATE PROC [dbo].[Proc_Report_Test_Total_Count_H360Users_Campaign] 
(  
 @StartDate Datetime  
,@EndDate Datetime  
,@CampaignID NVARCHAR(MAX)
,@PartnerID int = null 
)  
As  
  
BEGIN  
DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

set nocount on  
--drop table #RptTypes
create table #RptTypes (RptID int, RptType varchar(255))
insert into #RptTypes values (1, 'Heart360 New Users')
insert into #RptTypes values (2, 'Heart360 Total Users at end of month')
insert into #RptTypes values (3, 'Heart360 Total Active Users this month')
insert into #RptTypes values (4, 'Heart360 users opted to provide their email address')
insert into #RptTypes values (5, 'Heart360 Total Spanish Users at end of month')
 
--drop table #RptTypesWithDates
SELECT * INTO #RptTypesWithDates from  #RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate) 
 
--drop table #result


SELECT RptID,R.Date1,COUNT(DISTINCT AB.UserID) UserCount,R.RptType INTO #result 
FROM #RptTypesWithDates R LEFT JOIN AHAUserLoginDetails A ON A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2

	LEFT JOIN
	(  
		SELECT UserID, AHR.UserHealthRecordID, PGD.ProviderID
		FROM AHAUserHealthRecord AHR  
		INNER JOIN HealthRecord H
			ON H.UserHealthRecordID=AHR.UserHealthRecordID
			AND CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
		LEFT JOIN PatientGroupMapping PGM
			ON PGM.PatientID = AHR.UserHealthRecordID
		LEFT JOIN PatientGroupDetails PGD
			ON PGD.GroupID = PGM.GroupID
	) AB 
	ON A.AHAUserID=AB.UserID  AND (AB.ProviderID=@PartnerID or @PartnerID is null)
  
WHERE R.RptID=3   
  
GROUP BY R.Date1,R.RptType,RptID 
 
UNION ALL

select RptID,R.Date1,count(distinct AB.UserID)  UserCount,R.RptType  
from #RptTypesWithDates R Left join AHAUserHealthRecord A on A.CreatedDate < R.Date2 
  
	LEFT JOIN
	(  
		SELECT UserID, AHR.UserHealthRecordID, PGD.ProviderID
		FROM AHAUserHealthRecord AHR  
		INNER JOIN HealthRecord H
			ON H.UserHealthRecordID=AHR.UserHealthRecordID
			AND CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
		LEFT JOIN PatientGroupMapping PGM
			ON PGM.PatientID = AHR.UserHealthRecordID
		LEFT JOIN PatientGroupDetails PGD
			ON PGD.GroupID = PGM.GroupID
	) AB 
	ON A.UserID=AB.UserID  AND (AB.ProviderID=@PartnerID or @PartnerID is null)
 
WHERE R.RptID=2 
GROUP BY R.Date1,R.RptType,RptID 

select date1,RptType,UserCount,RptID   
,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex  
,cast(dense_Rank() over (order by Date1) as int) IntIndex
into #ToBePivoted
from #Result  

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted
 
 
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
 
 
SET @query = N'SELECT RptType, '+ @cols +' FROM
(SELECT RptType, ColumnINdex, UserCount,RptID FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'
 
EXECUTE(@query) 

END
