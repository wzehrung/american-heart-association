﻿

CREATE PROC [dbo].[Proc_Report_GetAllAffiliatesForCampaign] (@CampaignId INT)
AS

BEGIN

DECLARE @SQLQuery AS NVARCHAR(500)

IF @CampaignId IS NOT NULL
BEGIN

	SET @SQLQuery = 'SELECT AffiliateID, AffiliateName FROM Heart360..v_AffiliatesCampaigns WHERE CampaignID = ' + CAST(@CampaignId as VARCHAR(50)) + ' ORDER BY AffiliateName'

END

ELSE
BEGIN

	SET @SQLQuery = 'SELECT AffiliateID, AffiliateName FROM Heart360..Affiliates ORDER BY AffiliateName'	

END

EXECUTE sp_executesql @SQLQuery

END

