﻿
-- [Proc_ChangeProviderTips]


CREATE PROC [dbo].[Proc_ChangeProviderTips]
(
 @ProviderTipsData nvarchar(max)
,@ProviderTipsTypeIds nvarchar(max)
,@ProviderTipsID int
,@UpdatedByUserID INT
,@LanguageID int
)

As

Begin
set nocount on


Begin Try
Begin Transaction

UpDate ProviderTips 
set 

	 ProviderTipsData = @ProviderTipsData,UpdatedByUserID=@UpdatedByUserID
	
where ProviderTipsID=@ProviderTipsID AND LanguageID = @LanguageID 


Delete from ProviderTipsMapping where ProviderTipsID=@ProviderTipsID AND LanguageID = @LanguageID 




set @ProviderTipsTypeIds=@ProviderTipsTypeIds+','

		;WITH Strings(Segments, Processing)
		AS
		(
		SELECT
				 SUBSTRING(@ProviderTipsTypeIds,1, CHARINDEX(',', @ProviderTipsTypeIds)-1) Segments
				,SUBSTRING(@ProviderTipsTypeIds,CHARINDEX(',', @ProviderTipsTypeIds)+1, len(@ProviderTipsTypeIds)) Processing

				UNION ALL

				SELECT
				 SUBSTRING(Processing,1, CHARINDEX(',', Processing)-1) Segments
				,SUBSTRING(Processing,CHARINDEX(',', Processing)+1, len(Processing)) Processing

				FROM Strings

				WHERE
				CHARINDEX(',', Processing) > 0
		)



Insert ProviderTipsMapping (ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,LanguageID)

select @ProviderTipsID,Segments,(SELECT CreatedByUserID FROM ProviderTips 
where ProviderTipsID=@ProviderTipsID AND LanguageID=@LanguageID),@UpdatedByUserID,@LanguageID from Strings



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End

