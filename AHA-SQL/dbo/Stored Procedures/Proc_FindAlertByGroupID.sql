﻿






CREATE proc [dbo].[Proc_FindAlertByGroupID]
(
 @GroupID INT 
)

AS

Begin
set nocount on

select DISTINCT C.AlertID, C.PatientID, IsDismissed,CAST(AlertXml AS NVARCHAR(MAX)) AlertXml,ItemEffectiveDate 
,CAST(AlertRuleXml AS NVARCHAR(MAX)) AlertData, CreatedDate,D.Type,Notes 
FROM
PatientGroupMapping B
INNER JOIN Alert C On B.PatientID=C.PatientID AND B.GroupID=@GroupID
inner join AlertRule D on C.AlertRuleId=D.AlertRuleID AND d.ISACTIVE=1

order by ItemEffectiveDate desc

End







