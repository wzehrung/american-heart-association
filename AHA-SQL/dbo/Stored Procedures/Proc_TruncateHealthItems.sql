﻿
CREATE Procedure  [dbo].[Proc_TruncateHealthItems]
(
@HealthRecordID int
)
As

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Deleting all health item tables
*/

set nocount on


Delete from dbo.BloodGlucose	    Where UserHealthRecordID= @HealthRecordID

Delete from dbo.BloodPressure	    Where UserHealthRecordID= @HealthRecordID
		
Delete from dbo.BloodPressureGoal	Where UserHealthRecordID= @HealthRecordID	

Delete from dbo.Cholesterol			Where UserHealthRecordID= @HealthRecordID	    
	    		
Delete from dbo.FamilyHistory		Where UserHealthRecordID= @HealthRecordID		

Delete from dbo.Height				Where UserHealthRecordID= @HealthRecordID			

Delete from dbo.Medication			Where UserHealthRecordID= @HealthRecordID			
		
Delete from dbo.Weight				Where UserHealthRecordID= @HealthRecordID	

Delete from dbo.Diet				Where UserHealthRecordID= @HealthRecordID	

Delete from dbo.Exercise			Where UserHealthRecordID= @HealthRecordID

Delete from dbo.ExtendedProfile		Where UserHealthRecordID= @HealthRecordID	

Delete from dbo.WeightGoal			Where UserHealthRecordID= @HealthRecordID	

Delete from dbo.CholesterolGoal		Where UserHealthRecordID= @HealthRecordID	
	

Delete from dbo.Alert Where PatientID= @HealthRecordID	

Delete from dbo.UserFolderMapping Where PatientID= @HealthRecordID	

Delete from dbo.MessageDetails Where PatientID= @HealthRecordID	

Delete from dbo.PatientGroupMapping Where PatientID= @HealthRecordID	


;WITH invids As
(
select InvitationID from dbo.PatientProviderInvitation Where SentPatientID= @HealthRecordID
)

delete from PatientProviderInvitationDetails where InvitationID in (select InvitationID from invids )

Delete A from dbo.PatientProviderInvitationDetails A 
inner join dbo.PatientProviderInvitation B on A.InvitationID=B.InvitationID
and A.AcceptedPatientID = @HealthRecordID	

Delete from dbo.PatientProviderInvitation Where SentPatientID= @HealthRecordID	

delete from alertMapping where PatientID  = @HealthRecordID	


End


