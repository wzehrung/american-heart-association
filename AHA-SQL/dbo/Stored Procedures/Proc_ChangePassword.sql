﻿
CREATE PROC [dbo].[Proc_ChangePassword]
(
 @AdminUserID int
,@Password nvarchar(50)
)

As

Begin
set nocount on

Update AdminUser set 
						 Password = @Password
					
where  AdminUserID = @AdminUserID

End

