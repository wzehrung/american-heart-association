﻿

CREATE PROC [dbo].[Proc_ModifyAHAuserHealthRecordTrackerDetails]
(
 @PersonGUID uniqueidentifier
,@HealthRecordGUID uniqueidentifier
,@TrackerDetails xml
)

As

Begin
declare @UserID int,@HealthRecordID int

set @UserID = (select UserID from AHAuser where PersonGUID=@PersonGUID)

set @HealthRecordID = (select UserHealthRecordID from HealthRecord where UserHealthRecordGUID=@HealthRecordGUID)

UPDATE AHAuserHealthRecord SET TrackerDetails=@TrackerDetails WHERE UserID=@UserID  AND UserHealthRecordID=@HealthRecordID

End
