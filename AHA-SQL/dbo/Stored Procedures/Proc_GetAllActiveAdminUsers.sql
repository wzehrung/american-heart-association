﻿--[Proc_GetAllActiveAdminUsers]
CREATE PROC [dbo].[Proc_GetAllActiveAdminUsers]

AS

BEGIN
SET NOCOUNT ON

SELECT  A.*,Lan.LanguageLocale
FROM AdminUser A

	INNER JOIN Languages Lan 
		ON A.DefaultLanguageID = Lan.LanguageID

WHERE A.IsActive = 1

END

