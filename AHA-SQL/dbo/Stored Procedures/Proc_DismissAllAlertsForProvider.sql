﻿
Create proc [dbo].[Proc_DismissAllAlertsForProvider]
(
@ProviderID INT
)

AS

Begin
set nocount on

UPDATE A SET IsDismissed=1  from  Provider P inner Join AlertRule AR
on P.ProviderID = AR.ProviderID AND P.ProviderID=@ProviderID
inner join Alert A on AR.AlertRuleID=A.AlertRuleID
  

End
