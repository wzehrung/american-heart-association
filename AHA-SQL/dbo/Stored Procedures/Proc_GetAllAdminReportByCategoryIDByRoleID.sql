﻿CREATE PROC [dbo].[Proc_GetAllAdminReportByCategoryIDByRoleID]

(
 @CategoryID int
,@RoleID int
)
As

Begin
set nocount on

SELECT  a.*, [Enabled] FROM AdminReport a, AdminRolesReport b
WHERE CategoryID = @CategoryID
and b.RoleID = @RoleID
and a.ReportID = b.ReportID
End
