﻿

CREATE PROC [dbo].[Proc_GetNewMessageCountForProvider]
(
 @ProviderID int
,@Count int output

)

As

Begin
SET NOCOUNT ON

select @Count=Count(distinct MD.MessageID)  from
Folder F  
inner join UserFolderMapping UFM
on F.FolderID=UFM.FolderID and UFM.ProviderID=@ProviderID and F.FolderType='I'
inner join MessageDetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
AND MD.isRead = 0

End

