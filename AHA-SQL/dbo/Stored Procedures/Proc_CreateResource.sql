﻿CREATE PROC [dbo].[Proc_CreateResource]
(
 @ContentTitle NVARCHAR(80)
,@ContentTitleUrl nvarchar(255)
,@ContentText nvarchar(MAX)
,@IsProvider bit
,@ExpirationDate datetime
,@UpdatedByUserID int
,@LanguageID int
,@ResourceID INT OUTPUT
,@providerID int = 0
)
As
Begin

set nocount on

Declare @SortOrder int


set @SortOrder=(select max(SortOrder)+1 from Resources )

IF @ResourceID IS NULL OR @ResourceID=0
set @ResourceID=(select max(ResourceID)+1 from Resources )

if @SortOrder is null
set @SortOrder = 1


if (@ProviderID = 0)
begin

Insert Resources (ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate ,CreatedByUserID ,UpdatedByUserID,LanguageID )
select @ResourceID,@ContentTitle,@ContentTitleUrl,@ContentText,@SortOrder,@IsProvider,@ExpirationDate ,@UpdatedByUserID ,@UpdatedByUserID,@LanguageID

end
else
begin

Insert Resources (ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate ,CreatedByUserID ,UpdatedByUserID,LanguageID, ProviderID )
select @ResourceID,@ContentTitle,@ContentTitleUrl,@ContentText,@SortOrder,@IsProvider,@ExpirationDate ,@UpdatedByUserID ,@UpdatedByUserID,@LanguageID,@ProviderID

end
End
