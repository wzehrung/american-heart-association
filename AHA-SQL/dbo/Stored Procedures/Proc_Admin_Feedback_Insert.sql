﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.18.2014
-- Description:	Feedback from the users are entered here
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_Insert]
(
    @FeedbackText nvarchar(1024),
    @FeedbackTypeID int
)

AS

BEGIN
SET NOCOUNT ON

INSERT INTO Feedback (FeedbackText, FeedbackTypeID, FeedbackDate)
VALUES(@FeedbackText, @FeedbackTypeID, GETDATE())

END