﻿
--[Proc_GetProvidersNotLoggedInForLastNdays] 30

CREATE PROC Proc_GetProvidersNotLoggedInForLastNdays
(
 @NumberofDays int
,@CampaignID int=NULL
)
As

Begin


;With Latest As
	(
	SELECT	P.ProviderID
			,case when PL.ProviderID is null then DateOfRegistration else LoggedInDate end  LoggedInDate
			,rank() over (partition by P.ProviderID order by case when PL.ProviderID is null 
			then DateOfRegistration else LoggedInDate end  desc) rank 

	FROM Provider P left join ProviderLoginDetails PL on P.ProviderID=PL.ProviderID 
	)

select P.*,Lan.LanguageLocale  from Provider P inner join Latest L on P.ProviderID=L.ProviderID 
inner join Languages Lan on P.DefaultLanguageID = Lan.LanguageID
where Rank=1 and 
Convert(datetime,Convert(varchar,LoggedInDate,103),103)=dateadd(day,-1*@NumberofDays,Convert(datetime,Convert(varchar,getdate(),103),103))
and (CampaignID=@CampaignID or @CampaignID is null)

End
