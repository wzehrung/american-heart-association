﻿--PROC_Report_H360Providers '2009.02.01','2010.02.01',18

CREATE PROC [dbo].[Proc_Report_Provider_H360Providers_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@UserTypeID int = null
,@PartnerID int = null
)
As

BEGIN
DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

set nocount on


;with RptTypes As
(
select 1 RptID,'New' As RptType
union all
select 2 RptID,'Total at end of month' As RptType
union all
select 3 RptID,'Total Active this month' As RptType
union all
select 4 RptID,'Total Spanish at end of month' As RptType

),
RptTypesWithDates As
(
select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,Result As
(
select RptID,R.Date1,count(distinct H.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join ProviderLoginDetails A on A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2
left join Provider H on H.ProviderID=A.ProviderID  and CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
	and (h.UserTypeID=@UserTypeID or @UserTypeID IS NULL)
	and (h.ProviderID=@PartnerID or @PartnerID is null)
where  R.RptID=3 

GROUP BY R.Date1,R.RptType,RptID

union all

select RptID,R.Date1,count(distinct A.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join Provider A on A.DateOfRegistration < R.Date2 
 and CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
 and (A.UserTypeID=@UserTypeID or @UserTypeID IS NULL)
 and (A.ProviderID=@PartnerID or @PartnerID is null)
where  R.RptID=2
 GROUP BY R.Date1,R.RptType,RptID

union all

select RptID,R.Date1,count(distinct A.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join Provider A on A.DateOfRegistration >= R.Date1 
and  A.DateOfRegistration < R.Date2  and CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
 and (A.UserTypeID=@UserTypeID or @UserTypeID IS NULL)
 and (A.ProviderID=@PartnerID or @PartnerID is null)
where  R.RptID=1
GROUP BY R.Date1,R.RptType,RptID

union all

select RptID,R.Date1,count(distinct A.ProviderID)  ProviderCount,R.RptType
from RptTypesWithDates R Left join Provider A on A.DateOfRegistration < R.Date2 and A.DefaultLanguageID=2
 and CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
 and (A.UserTypeID=@UserTypeID or @UserTypeID IS NULL)
 and (A.ProviderID=@PartnerID or @PartnerID is null)
 where  R.RptID=4
 GROUP BY R.Date1,R.RptType,RptID

)


--,ToBePivoted As
--(
Select date1,RptType,ProviderCount,RptID 
,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
into #ToBePivoted
from Result
order by date1
--)

select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1

DECLARE @MaxCount INTEGER
DECLARE @Count INTEGER
DECLARE @Txt VARCHAR(MAX)
SET @Count = 1
SET @Txt = '['
SET @MaxCount = (SELECT MAX(RowID) FROM #c)
WHILE @Count<=@MaxCount
    BEGIN
    IF @Txt!=''
        SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
    ELSE
        SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
    SET @Count=@Count+1
    END
--SELECT @Txt AS Txt

SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

SET @Cols = @Txt
/*
SELECT  @cols = STUFF(( SELECT DISTINCT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ToBePivoted AS t
                        ORDER BY '],[' + t.ColumnIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

*/
SET @query = N'SELECT RptType, '+ @cols +' FROM
(SELECT RptType, ColumnINdex, ProviderCount,RptID FROM #ToBePivoted ) A 
PIVOT (SUM(ProviderCount) FOR ColumnIndex IN ( '+ @cols +' )) B  ;'

EXECUTE(@query)
		
--select 
--
-- RptType
--,[Column # 1] as [Column # 1] 
--,[Column # 2] as [Column # 2]
--,[Column # 3] as [Column # 3]
--,[Column # 4] as [Column # 4] 
--,[Column # 5] as [Column # 5]
--,[Column # 6] as [Column # 6]
--,[Column # 7] as [Column # 7]
--,[Column # 8] as [Column # 8] 
--,[Column # 9] as [Column # 9]
--,[Column # 10] as [Column # 10]
--,[Column # 11] as [Column # 11]
--,[Column # 12] as [Column # 12]
--
--
--from
--(
--select	RptType,ColumnIndex,ProviderCount,RptID
--		from ToBePivoted
--) A
--PIVOT
--(
--SUM (ProviderCount)
--FOR ColumnIndex IN
--( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
-- , [Column # 10],[Column # 11],[Column # 12] )
--) B
--
--order by RptID


END
