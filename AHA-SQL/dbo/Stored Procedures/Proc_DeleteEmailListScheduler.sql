﻿
create PROC Proc_DeleteEmailListScheduler
(
 @SchedulerID int
)
As

Begin
set nocount on

DELETE FROM EmailListScheduler WHERE SchedulerID=@SchedulerID

End
