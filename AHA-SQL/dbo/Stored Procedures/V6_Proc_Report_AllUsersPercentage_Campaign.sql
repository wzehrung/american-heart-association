﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 6.17.2013
-- Description:	Refactor of Proc_Report_AllUsersPercentage_Campaign for Campaigns/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_AllUsersPercentage_Campaign] 
(
	@StartDate Datetime,  
	@EndDate Datetime,
	@CampaignID int = null,	-- null specifies all campaigns
	@Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
	@CampaignIDs nvarchar(max)
)
AS
BEGIN

	DECLARE @cols NVARCHAR(MAX)
	DECLARE @colsConv NVARCHAR(MAX)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
		from ZipRegionMapping 
		where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	
	--
	-- Create a temp table of all users who are in campaign/markets logic
	--
	select * into #h from	
		(select UserID, AHR.UserHealthRecordID
			from  AHAUserHealthRecord AHR
			inner join HealthRecord H on H.UserHealthRecordID = AHR.UserHealthRecordID
			left join  ExtendedProfile E ON E.UserHealthRecordID = AHR.UserHealthRecordID
			/* OLD WAY
			/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
					just get those is specified */
			WHERE ((@CampaignId IS NULL AND H.CampaignID IS NOT NULL) OR (@CampaignId IS NOT NULL AND @CampaignId = H.CampaignID))
				AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
				*/
			--  NEW WAY:
			--  WHZ: 7/22/2015
			WHERE
			@CampaignIDs IS NULL AND H.CampaignID IS NOT NULL
			OR (H.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AHR.UserID IS NOT NULL)
		) a
	
	--
	-- generate the report
	--
	
	;with RptTypes As
	(
		select 1 RptID,'1 Visit (All)' As RptType
		union all
		select 2 RptID,'2-4 Visits (All)' As RptType
		union all
		select 3 RptID,'5-10 Visits (All)' As RptType
		union all
		select 4 RptID,'>10 Visits (All)' As RptType
	),
	NewRptTypes As
	(
		select 5 RptID,'1 Visit (New)' As RptType
		union all
		select 6 RptID,'2-4 Visits (New)' As RptType
		union all
		select 7 RptID,'5-10 Visits (New)' As RptType
		union all
		select 8 RptID,'>10 Visits (New)' As RptType
	)
	,RptTypesWithDates As
	(
		select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	),
	NewRptTypesWithDates As
	(
		select * from  NewRptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,EligibleLogins As
	(
		-- NOTE: this is the same as V6_Proc_Report_AllUsers_Campaign		
		Select R.Date1, ALD.AHAUserID, Count(H.UserID) as Visits 
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R 
			inner join AHAUserLoginDetails ALD on (ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2) 
			inner join #h H ON H.UserID = ALD.AHAUserID
			group by R.Date1,ALD.AHAUserID
		
	)
	,EligibleUsers As
	(	
		-- NOTE: this is the same as V6_Proc_Report_AllUsers_Campaign
		Select distinct A.UserID,R.Date1 
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner join AHAUser A on A.CreatedDate >=R.Date1 and A.CreatedDate <R.Date2 
			inner join #h H on H.UserID=A.UserID
	)

	,Visits As
	(
		-- NOTE: this is **NOT** the same as V6_Proc_Report_AllUsers_Campaign
		select E1.date1,AHAUserID,
		       Case when Visits =1 then '1 Visit (All)' 
					when Visits between 2 and 4 then '2-4 Visits (All)'
					when Visits between 5 and 10 then '5-10 Visits (All)'
					when Visits >10 then '>10 Visits (All)' 
				End RptType 
			from EligibleLogins E1
	)
	,NewVisits As
	(
		Select R.Date1, ALD.AHAUserID,
			   Case when count(AHAUserID) =1 then '1 Visit (New)' 
					when count(AHAUserID) between 2 and 4 then '2-4 Visits (New)'
					when count(AHAUserID) between 5 and 10 then '5-10 Visits (New)'
					when count(AHAUserID) >10 then '>10 Visits (New)' 
			   End RptType 
		   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R 
		   inner join AHAUserLoginDetails ALD on (ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2) 
		   inner join EligibleUsers E1 on (E1.UserID=ALD.AHAUserID and E1.date1=R.date1)
			group by R.Date1,AHAUserID
	)
	,VisitsFinal As
	(
		select R.RptID, R.date1, R.RptType, Count(V.RptType) UserCount, 1 AS ID
			from RptTypesWithDates R 
			left join Visits V on (V.Date1=R.Date1 and V.RptType=R.RptType)
			group by R.date1,R.RptType, R.RptID
	)
	,NewVistsFinal As
	(
		Select RptID,R.date1,R.RptType,Count(V.RptType) userCount,1 AS ID 
			from NewRptTypesWithDates R 
			left join NewVisits V on (V.Date1=R.Date1 and V.RptType=R.RptType)
			group by R.date1,R.RptType,RptID
	)
	,TotalVisits As
	(
		select SUM(Usercount) TotalUserCount, Date1  
			from VisitsFinal 
			group by date1
	)
	,TotalNewVisits As
	(
		select SUM(Usercount) TotalUserCount, Date1
			from NewVistsFinal 
			group by Date1
	)

	SELECT RptID, date1, RptType, Perc,
	       'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex,
			cast(dense_Rank() over (order by date1) as int) IntIndex
		INTO #ToBePivoted
		FROM (
			Select RptID, N.date1, N.RptType, 
			       isnull(case when TotalUserCount=0 then 0 else cast(userCount as real)*100/cast (TotalUserCount as real) end,0) as Perc
				from VisitsFinal N 
				inner join TotalVisits T on N.date1=T.date1 
			UNION ALL
			Select RptID, N.date1, N.RptType, 
				   isnull(case when TotalUserCount=0 then 0 else cast(userCount as real)*100/cast (TotalUserCount as real) end,0) as Perc
				from NewVistsFinal N 
				inner join TotalNewVisits T on N.Date1=T.Date1 
		) A

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
		
SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

EXECUTE(N'SELECT RptType, '+ @colsConv +' FROM
(SELECT RptType, ColumnINdex, Perc, rptid FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;')
	
END


