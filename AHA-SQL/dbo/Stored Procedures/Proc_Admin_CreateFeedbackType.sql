﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.17.2014
-- Description:	Used by the admin portal to create a feedback type
-- =============================================

CREATE PROC [dbo].[Proc_Admin_CreateFeedbackType]
(
    @FeedbackTypeName nvarchar(1024)
)

AS

BEGIN
SET NOCOUNT ON
	
INSERT FeedbackTypes
(
	FeedbackTypeName
) 

SELECT
	@FeedbackTypeName

END

