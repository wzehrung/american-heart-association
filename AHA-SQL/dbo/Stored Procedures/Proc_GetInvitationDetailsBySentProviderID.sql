﻿
CREATE PROCEDURE [dbo].[Proc_GetInvitationDetailsBySentProviderID]
(
  @SentProviderID int
)
As
Begin
set nocount on


	 select PPID.*
	from  PatientProviderInvitation PPI inner join PatientProviderInvitationDetails PPID
    on PPI.InvitationID = PPID.InvitationID and PPI.SentProviderID=@SentProviderID 
	AND PPID.DateAccepted IS NULL and PPID.Status != 5
	ORDER BY DateInserted DESC

	
End

