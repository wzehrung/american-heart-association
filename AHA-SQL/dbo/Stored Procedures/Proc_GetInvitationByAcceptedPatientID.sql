﻿CREATE PROCEDURE [dbo].[Proc_GetInvitationByAcceptedPatientID]
(
  @AcceptedPatientID int
 ,@Status tinyint
)
As
Begin
set nocount on
	if(@status = 1)
	select PPI.*
	 from  PatientProviderInvitation PPI inner join PatientProviderInvitationDetails PPID
    on PPI.InvitationID = PPID.InvitationID and PPID.AcceptedPatientID=@AcceptedPatientID 
	and DateAccepted is null and (status =1 or status = 5)
	ORDER BY DateSent DESC
	else

	select PPI.*
	 from  PatientProviderInvitation PPI inner join PatientProviderInvitationDetails PPID
    on PPI.InvitationID = PPID.InvitationID and PPID.AcceptedPatientID=@AcceptedPatientID 
	and DateAccepted is null and status =@status
	ORDER BY DateSent DESC
End

