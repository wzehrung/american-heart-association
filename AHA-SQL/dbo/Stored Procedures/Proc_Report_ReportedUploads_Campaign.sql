﻿CREATE Procedure [dbo].[Proc_Report_ReportedUploads_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@PartnerID int = null
)

As

Begin
DECLARE @cols NVARCHAR(2000)
DECLARE @ColsConv NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)
DECLARE @I INT
		select * into #h from	
(select UserID,AHR.UserHealthRecordID,PGD.ProviderID from  AHAUserHealthRecord AHR
				inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID
				
				AND CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
				
				left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
				left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
) a

	
;With Tracker As
		(

			Select TrackerCount, Date1, 'Weight' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(W.UserHealthRecordID) as tracker,
			R.Date1
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Weight W 
				on W.CreatedDate >=R.Date1 and W.CreatedDate <R.Date2 
				inner join #h H on H.UserHealthRecordID=W.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, W.USerHealthRecordID
			HAVING COUNT(W.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner JOIN weight  W
			on W.createddate >= R.Date1 and W.createddate < R.Date2
				left join #h H on H.UserHealthRecordID=W.UserHealthRecordID											
			--where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			--having count(W.userhealthRecordid) =0 or count(W.Userhealthrecordid) =1
			) A

			union all
			
			Select TrackerCount, Date1, 'Blood Pressure' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(BP.UserHealthRecordID) as tracker,
			R.Date1
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join BloodPressure BP 
				on BP.CreatedDate >=R.Date1 and BP.CreatedDate <R.Date2 
				inner join #h H on H.UserHealthRecordID=BP.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, BP.USerHealthRecordID
			HAVING COUNT(BP.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner JOIN BloodPressure BP 
			on BP.createddate >= R.Date1 and BP.createddate < R.Date2
			--	inner join #h H on H.UserHealthRecordID=BP.UserHealthRecordID											
			--where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			--having count(BP.userhealthRecordid) =0 or count(BP.Userhealthrecordid) =1
			) A

			union all

			Select TrackerCount, Date1, 'Physical Activity' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(E.UserHealthRecordID) as tracker,
			R.Date1
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Exercise E 
				on E.CreatedDate >=R.Date1 and E.CreatedDate <R.Date2 
				inner join #h H on H.UserHealthRecordID=E.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, E.USerHealthRecordID
			HAVING COUNT(E.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner JOIN Exercise E 
			on E.createddate >= R.Date1 and E.createddate < R.Date2
			--	inner join #h H on H.UserHealthRecordID=E.UserHealthRecordID											
			--where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			--having count(E.userhealthRecordid) =0 or count(E.Userhealthrecordid) =1
			) A


			union all

			Select TrackerCount, Date1, 'Medication' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(M.UserHealthRecordID) as tracker,
			R.Date1
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Medication M 
				on M.CreatedDate >=R.Date1 and M.CreatedDate <R.Date2 
				inner join #h H on H.UserHealthRecordID=M.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, M.USerHealthRecordID
			HAVING COUNT(M.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner JOIN Medication M 
			on M.createddate >= R.Date1 and M.createddate < R.Date2
			--	inner join #h H on H.UserHealthRecordID=M.UserHealthRecordID											
			--where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			--having count(M.userhealthRecordid) =0 or count(M.Userhealthrecordid) =1
			) A

			union all

			Select TrackerCount, Date1, 'Cholesterol' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(C.UserHealthRecordID) as tracker,
			R.Date1
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join Cholesterol C 
				on C.CreatedDate >=R.Date1 and C.CreatedDate <R.Date2 
				inner join #h H on H.UserHealthRecordID=C.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, C.USerHealthRecordID
			HAVING COUNT(C.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner JOIN Cholesterol C 
			on C.createddate >= R.Date1 and C.createddate < R.Date2
			--	inner join #h H on H.UserHealthRecordID=C.UserHealthRecordID											
			--where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			--having count(C.userhealthRecordid) =0 or count(C.Userhealthrecordid) =1
			) A

			union all

			Select TrackerCount, Date1, 'Blood Glucose' Tracker FROM (
			select COunt(*) as TrackerCount, Date1 FROM (
			Select COUNT(BG.UserHealthRecordID) as tracker,
			R.Date1
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
				inner join BloodGlucose BG 
				on BG.CreatedDate >=R.Date1 and BG.CreatedDate <R.Date2 
				inner join #h H on H.UserHealthRecordID=BG.UserHealthRecordID											
			where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY Date1, BG.USerHealthRecordID
			HAVING COUNT(BG.UserHealthRecordID) > 1
			) A GROUP BY Date1

			union

			select 0 as TrackerCount, R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
			inner JOIN BloodGlucose BG 
			on BG.createddate >= R.Date1 and BG.createddate < R.Date2
			--	inner join #h H on H.UserHealthRecordID=BG.UserHealthRecordID											
			--where isnull(H.ProviderID,-1) = isnull(isnull(@PartnerID, H.ProviderID),-1)
			GROUP BY R.Date1
			--having count(BG.userhealthRecordid) =0 or count(BG.Userhealthrecordid) =1
			) A

		)

select T.Date1,Tracker, TrackerCount as UserCount
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Tracker T 

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted



--select * from #ToBePivoted
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

/*
declare @cnt int
declare @j int
declare @cols1 nvarchar(2000)

SET @j = 1
SET @cols = ''
SET @cols1 = ''

select @cnt =count(*) from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
while (@j <= @cnt)
begin
  SET @cols = @cols + 'ISNULL([Column # ' + cast(@j as varchar) + '],''0'') as [Column # ' + cast(@j as varchar) + '],'
  SET @cols1 = @cols1 + '[Column # ' + cast(@j as varchar) + '],'
  SET @j = @j+1
end
SET @cols = left(@cols, len(@cols)-1)
SET @cols1 = left(@cols1, len(@cols1)-1)
*/
		select @i = count(*) from #h
        if(@i = 0)
        begin
         select 'NONE' as Tracker, '0.00%' as [Column # 1], '0.00%' as [Column # 2] where 1=1

        end
        else
        begin
SET @query = N'SELECT Tracker, '+ @cols +' FROM
(SELECT Tracker, ColumnINdex, UserCount FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'

--print @query
EXECUTE(@query)

  end

End
