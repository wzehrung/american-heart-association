﻿

CREATE PROCEDURE [dbo].[Proc_SetTOUAcceptedByPatient]
(
 @PatientID int
)
As
Begin

Update HEALTHRECORD set HasAgreedProviderTOU=1 
,UpdatedDate=GETDATE()
where UserHealthRecordID=@PatientID

End

