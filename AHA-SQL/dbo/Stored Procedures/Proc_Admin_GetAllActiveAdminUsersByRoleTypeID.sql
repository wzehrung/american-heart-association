﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03/05/2014
-- Description:	Get admin users by pipe delimited list of role type IDs
-- =============================================

CREATE PROC [dbo].[Proc_Admin_GetAllActiveAdminUsersByRoleTypeID]
(
    @RoleTypeIDs NVARCHAR(MAX)
)

AS

BEGIN
SET NOCOUNT ON

SELECT  au.*, ar.Name AS 'RoleName', ab.BusinessID, ab.Name AS 'BusinessName', Lan.LanguageLocale
FROM AdminUser au

	INNER JOIN Languages Lan 
		ON au.DefaultLanguageID = Lan.LanguageID

	INNER JOIN AdminRoles ar
		ON au.RoleID = ar.RoleID

	LEFT JOIN AdminBusiness ab
		ON au.BusinessID = ab.BusinessID

WHERE au.IsActive = 1
    AND au.RoleID IN (SELECT VALUE FROM dbo.udf_ListToTableString(@RoleTypeIDs,'|'))

END
