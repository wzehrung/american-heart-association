﻿
CREATE PROC [dbo].[Proc_Report_ProviderUsers]
As

Begin

Declare @ProviderTotals Table
(
 Id int
,Total int
,Date Datetime
)
insert @ProviderTotals
select ID,Count(ProviderID) ProviderCount,Date
from  DBO.udf_GetDates ('2009.07.01','2009.9.01') D left join  Provider P 
on P.DateOfRegistration<D.Date GROUP BY ID,Date


;With LoginsRpt As
(
select
D.Date
,P.ProviderID
,Count(P.ProviderID) ProviderCount

from ProviderLoginDetails P
inner join @ProviderTotals D --DBO.udf_GetDates ('2009.07.01','2009.9.01') D 
on P.LoggedInDate>=dateadd(month,-1,D.Date)and P.LoggedInDate< D.Date 
group by P.ProviderID,D.Date

),

RptTypes As
(
select 1 RptID,'LogIns 0' As RptType
union all
select 2 RptID,'LogIns 1-10' As RptType
union all
select 3 RptID,'LogIns 11-20' As RptType
union all
select 4 RptID,'LogIns > 20' As RptType
union all
select 5 RptID,'TotalUsers' As RptType
union all
select 6 RptID,'NewUsers' As RptType
union all
select 7 RptID,'LogInsTotal' As RptType

),
RptTypesWithDates As
(
select * from  RptTypes cross join @ProviderTotals --DBO.udf_GetDates ('2009.07.01','2009.9.01')
)
,
FinalRpt As
(

select
	  Date
	 ,case when ProviderCount between 1 and 10 then 2
	  when ProviderCount between 11 and 20 then 3
	  when ProviderCount > 20 then 4 end As RptID
	 ,Count(ProviderID) As ProviderCount

from  LoginsRpt

GROUP BY 
	  Date
	 ,case when ProviderCount between 1 and 10 then 2
	  when ProviderCount between 11 and 20 then 3
	  when ProviderCount > 20 then 4 end

),
FinalRpt_With_AllTypes As
(

select D.Date,7 RptID ,Count(Distinct ProviderID) ProviderCount,0 ProviderPerc 
from  DBO.udf_GetDates ('2009.07.01','2009.9.01') D left join  ProviderLoginDetails P 
on P.LoggedInDate>=dateadd(month,-1,D.Date)and P.LoggedInDate< D.Date 
GROUP BY D.Date
union all

select D.Date,5 RptID ,Count(ProviderID) ProviderCount,0 ProviderPerc 
from  DBO.udf_GetDates ('2009.07.01','2009.9.01') D left join  Provider P 
on P.DateOfRegistration<D.Date GROUP BY D.Date

union all

select D.Date,6 RptID,Count(ProviderID) ProviderCount,0 ProviderPerc 
from  DBO.udf_GetDates ('2009.07.01','2009.9.01') D left join  Provider P 
on P.DateOfRegistration>=dateadd(month,-1,D.Date)and P.DateOfRegistration< D.Date 
GROUP BY D.Date

union all
select *,0 ProviderPerc  from FinalRpt
union all

Select A.Date,A.RptID,Total-ProviderCount  ProviderCount ,ProviderPerc
from
(
select Date,1 RptID,Sum(ProviderCount) ProviderCount,0 ProviderPerc  from FinalRpt  Group By Date
) A
inner join @ProviderTotals P on A.date=P.Date
)


,ToBePivoted As
(
Select R.Date,RptType,case when F1.ProviderCount is null and R.RptID=1 then Total 
							when F1.ProviderCount is null then 0 else ProviderCount end ProviderCount  
,'Column # ' + cast(dense_Rank() over (order by R.Date) as varchar(10)) ColumnIndex

,((case when F1.ProviderCount is null and R.RptID=1 then Total 
							when F1.ProviderCount is null then 0 else ProviderCount end )/total)*100 
  ProviderPerc


from  RptTypesWithDates R
left join FinalRpt_With_AllTypes  F1 on R.RptID=F1.RptID and R.Date=F1.Date
)


--select 
--
-- RptType
--,[Column # 1] as [Column # 1] 
--,([Column # 1]/21)*100 as [Column # 1 Perc] 
--,[Column # 2] as [Column # 2]
--,([Column # 2]/21)*100 as [Column # 2 Perc] 
--,[Column # 3] as [Column # 3]
--,([Column # 3]/21)*100 as [Column # 3 Perc] 
--,([Column # 1]+[Column # 2]+[Column # 3])/3 As [Average for Period]
--
--from
--(
--select	RptType,ColumnIndex,ProviderCount
--		from ToBePivoted
--) A
--PIVOT
--(
--SUM (ProviderCount)
--FOR ColumnIndex IN
--( [Column # 1] , [Column # 2],[Column # 3])
--) B


select * from ToBePivoted

End