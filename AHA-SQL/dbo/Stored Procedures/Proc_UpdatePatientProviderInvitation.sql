﻿

CREATE PROCEDURE [dbo].[Proc_UpdatePatientProviderInvitation]
(
 @InvitationCode nvarchar(6) 
,@UserHealthRecordID int
,@ProviderID int
)
As
Begin

UPDATE	PatientProviderInvitation 

SET		
		 SentPatientID = @UserHealthRecordID
		,SentProviderID = @ProviderID
		

WHERE	InvitationCode=@InvitationCode

End
