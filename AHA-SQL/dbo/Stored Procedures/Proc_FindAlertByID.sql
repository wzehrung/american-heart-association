﻿





CREATE proc [dbo].[Proc_FindAlertByID]
(
 @AlertID INT 
)

AS

Begin
set nocount on

select AlertID, PatientID, IsDismissed,AlertXml, AlertRuleXml AlertData, CreatedDate,B.Type,Notes ,ItemEffectiveDate 
from  Alert  A inner join AlertRule B on A.AlertRuleId=B.AlertRuleID
and AlertID=@AlertID--  AND b.IsActive=1
order by ItemEffectiveDate desc


End




