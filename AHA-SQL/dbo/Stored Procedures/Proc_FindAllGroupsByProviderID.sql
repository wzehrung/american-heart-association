﻿


CREATE PROCEDURE [dbo].[Proc_FindAllGroupsByProviderID]
(
@ProviderID int
)
As
Begin
set nocount on

	select P.*, isnull(TotalAlerts,0) as TotalAlerts

	from
	(
		SELECT 
		B.GroupID
		,Name
		,ProviderID
		,DateCreated
		,isDefault
		,Count(A.PatientID) totalmembers
		,Description
		,cast (GroupRuleXml as nvarchar(max)) GroupRuleXml
		,RemoveOption
		,IsSystemGenerated
		FROM PatientGroupDetails B 
		LEFT JOIN PatientGroupMapping A ON A.GroupID=B.GroupID
		WHERE  ProviderID=@ProviderID 
		group by B.GroupID,Name,ProviderID,DateCreated,isDefault,Description,cast (GroupRuleXml as nvarchar(max)),RemoveOption,IsSystemGenerated

	) P

LEFT JOIN

	(
		select am.GroupID
		,count(distinct AlertID)  TotalAlerts
		from alertRule AR inner join
		AlertMapping AM on AR.AlertRuleID=AM.AlertRuleID and ProviderID=@ProviderID 
		inner join PatientGroupDetails P
		on AM.GroupID=P.GroupID 
		inner join alert A on AR.AlertRuleID=A.AlertRuleID AND A.isDismissed=0
		group By am.GroupID
	) A



ON A.GroupID=P.GroupID
WHERE ProviderID=@ProviderID							
	
End



