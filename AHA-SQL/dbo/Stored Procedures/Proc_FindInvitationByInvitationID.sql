﻿
CREATE PROCEDURE [dbo].[Proc_FindInvitationByInvitationID]
(
 @InvitationID int
)
As
Begin
set nocount on

	select *
	 from  PatientProviderInvitation where InvitationID=@InvitationID
	
End