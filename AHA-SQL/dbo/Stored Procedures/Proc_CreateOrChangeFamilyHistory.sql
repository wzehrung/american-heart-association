﻿



CREATE Procedure [dbo].[Proc_CreateOrChangeFamilyHistory]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@FamilyMember nvarchar (255) 
,@Disease nvarchar (255) 
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (FamilyHistory table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

set nocount on

Begin Try
Begin Transaction

if exists(
		  select FamilyHistoryID from dbo.FamilyHistory where 
		  HVItemGuid=@HVItemGuid 	
		  )

Exec Proc_UpdateFamilyHistory	  @HVItemGuid 
								 ,@HVVersionStamp 
								 ,@FamilyMember 
								 ,@Disease 
								 
													

else 
begin

Declare @UserHealthRecordID int
select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							 	

Exec Proc_InsertFamilyHistory     @UserHealthRecordID 
								 ,@HVItemGuid 
								 ,@HVVersionStamp 
								 ,@FamilyMember 
								 ,@Disease 
								
End	

exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
									
End







