﻿

--select * from Resources_Log

CREATE PROC [dbo].[Proc_FindAuditDetails]
(
 @ID int
,@Type nvarchar(128)
,@LanguageID int
)

As

Begin
set nocount on

		if @Type='ProviderResources'
		begin

		select ContentTitle [Title],ModifiedDate AuditDate,B.[Name] from Resources_Log  A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where ResourceID=@ID and [Action]= 0 and isProvider=1 and LanguageID=@LanguageID

		union all
		select ContentTitle [Title],ModifiedDate AuditDate,[Name] from Resources_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where ResourceID=@ID and [Action]= 1 and RowType='NewRow' and isProvider=1 and LanguageID=@LanguageID
		order by AuditDate desc

		end
		if @Type='PatientResources'
		begin

		select ContentTitle [Title],ModifiedDate AuditDate, [Name] from Resources_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where ResourceID=@ID and [Action]= 0 and isProvider=0 and LanguageID=@LanguageID
		union all
		select ContentTitle [Title],ModifiedDate AuditDate, [Name] from Resources_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where ResourceID=@ID and [Action]= 1 and RowType='NewRow' and isProvider=0 and LanguageID=@LanguageID
		order by AuditDate desc

		end

		if @Type= 'ProviderContent'
		begin

		select ZoneTitle [Title],ModifiedDate AuditDate, [Name] from ProviderContent_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where ContentID=@ID and [Action]= 0 and LanguageID=@LanguageID
		union all
		select ZoneTitle [Title],ModifiedDate AuditDate, [Name] from ProviderContent_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID 
		where ContentID=@ID and [Action]= 1 and RowType='NewRow' and LanguageID=@LanguageID
		order by AuditDate desc

		end

		if @Type=  'PatientContent'        
		begin

		select ContentTitle [Title],ModifiedDate AuditDate, [Name] from PatientContent_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where PatientContentID=@ID and [Action]= 0 and LanguageID=@LanguageID
		union all
		select ContentTitle [Title],ModifiedDate AuditDate, [Name] from PatientContent_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where PatientContentID=@ID and [Action]= 1 and RowType='NewRow' and LanguageID=@LanguageID
		order by AuditDate desc

		end

		if @Type=  'ProviderGrantSupport'     
		begin

		select Title [Title],ModifiedDate  AuditDate, [Name]from GrantSupport_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where GrantSupportID=@ID and [Action]= 0 and IsForProvider=1 and LanguageID=@LanguageID
		union all
		select Title [Title],ModifiedDate AuditDate, [Name]  from GrantSupport_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID
		where GrantSupportID=@ID and [Action]= 1 and RowType='NewRow' and IsForProvider=1 and LanguageID=@LanguageID
		order by AuditDate desc

		end


		if @Type=   'PatientGrantSupport'  
		begin

		select Title [Title],ModifiedDate AuditDate, [Name] from GrantSupport_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where GrantSupportID=@ID and [Action]= 0 and IsForProvider=0 and LanguageID=@LanguageID
		union all
		select Title [Title],ModifiedDate AuditDate, [Name] from GrantSupport_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where GrantSupportID=@ID and [Action]= 1 and RowType='NewRow' and IsForProvider=0 and LanguageID=@LanguageID
		order by AuditDate desc

		end



		if @Type=  'ProviderResourceGraphics'
		begin

		select Title [Title],ModifiedDate AuditDate, [Name] from ProviderResourceGraphics_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where ProviderResourceGraphicsID=@ID and [Action]= 0 and LanguageID=@LanguageID
		union all
		select Title [Title],ModifiedDate AuditDate, [Name] from ProviderResourceGraphics_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID 
		where ProviderResourceGraphicsID=@ID and [Action]= 1 and RowType='NewRow' and LanguageID=@LanguageID
		order by AuditDate desc

		end



		if @Type=  'PatientResourceGraphics'
		begin

		select Title [Title],ModifiedDate AuditDate, [Name] from PatientResourceGraphics_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where PatientResourceGraphicsID=@ID and [Action]= 0 and LanguageID=@LanguageID
		union all
		select Title [Title],ModifiedDate AuditDate, [Name] from PatientResourceGraphics_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where PatientResourceGraphicsID=@ID and [Action]= 1 and RowType='NewRow' and LanguageID=@LanguageID
		order by AuditDate desc

		end


		if @Type=  'ProviderTips'
		begin

		
		select ProviderTipsData [Title],ModifiedDate AuditDate, [Name] from ProviderTips_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where ProviderTipsID=@ID and [Action]= 0 and LanguageID=@LanguageID
		union 
		select ProviderTipsData [Title],ModifiedDate AuditDate, [Name] from ProviderTips_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID  where ProviderTipsID=@ID and [Action]= 1 and RowType='NewRow' and LanguageID=@LanguageID
		union
		select ProviderTipsData [Title],ModifiedDate AuditDate, [Name] from ProviderTipsMapping_Log A 
						inner join ProviderTips B 
		on A.ProviderTipsID=B.ProviderTipsID and [Action]= 0 AND B.ProviderTipsID=@ID   and A.LanguageID=B.LanguageID
		and  A.LanguageID=@LanguageID and  B.LanguageID=@LanguageID
		INNER JOIN AdminUser C
		on A.UpdatedByUserID=C.AdminUserID 
		union 
		select ProviderTipsData [Title],ModifiedDate AuditDate, [Name] from ProviderTipsMapping_Log A 
						inner join ProviderTips B 
		on A.ProviderTipsID=B.ProviderTipsID and [Action]= 1 and RowType='NewRow' AND B.ProviderTipsID=@ID 
		and A.LanguageID=B.LanguageID and  A.LanguageID=@LanguageID and  B.LanguageID=@LanguageID
		INNER JOIN AdminUser C
		on A.UpdatedByUserID=C.AdminUserID  
		order by AuditDate desc

		end


		if @Type=  'ThingTips'
		begin

		
		select ThingTipsData [Title],ModifiedDate AuditDate, [Name] from ThingTips_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID  where  ThingTipsID=@ID and [Action]= 0 and LanguageID=@LanguageID
		union 
		select ThingTipsData [Title],ModifiedDate AuditDate, [Name] from ThingTips_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where  ThingTipsID=@ID and [Action]= 1 and RowType='NewRow' and LanguageID=@LanguageID
		union
		select ThingTipsData [Title],ModifiedDate AuditDate, [Name] from ThingTipsMapping_Log A 
						inner join ThingTips B 
		on A.ThingTipsID=B.ThingTipsID and [Action]= 0 AND B.ThingTipsID=@ID and A.LanguageID=B.LanguageID
		and  A.LanguageID=@LanguageID and  B.LanguageID=@LanguageID

		INNER JOIN AdminUser C
		on A.UpdatedByUserID=C.AdminUserID 

		union 
		select ThingTipsData [Title],ModifiedDate AuditDate, [Name] from ThingTipsMapping_Log A 
						inner join ThingTips B 
		on A.ThingTipsID=B.ThingTipsID and [Action]= 1 and RowType='NewRow' AND B.ThingTipsID=@ID
		and A.LanguageID=B.LanguageID and  A.LanguageID=@LanguageID and  B.LanguageID=@LanguageID
		
		INNER JOIN AdminUser C
		on A.UpdatedByUserID=C.AdminUserID

		order by AuditDate desc
		end


		if @Type= 'Content'
		begin

		
		select Title [Title],ModifiedDate AuditDate, [Name] from Content_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where  ContentID=@ID and  [Action]= 0 and LanguageID=@LanguageID
		union 
		select Title [Title],ModifiedDate AuditDate, [Name] from Content_Log A INNER JOIN AdminUser B
		on A.UpdatedByUserID=B.AdminUserID where  ContentID=@ID and [Action]= 1 and RowType='NewRow' and LanguageID=@LanguageID
		union
		select Title [Title],ModifiedDate AuditDate, [Name] from ContentRuleMapping_Log A 
						inner join Content B 
		on A.ContentID=B.ContentID and [Action]= 0 AND B.ContentID=@ID
		and A.LanguageID=B.LanguageID and  A.LanguageID=@LanguageID and  B.LanguageID=@LanguageID
		INNER JOIN AdminUser C
		on A.UpdatedByUserID=C.AdminUserID

		union 
		select Title [Title],ModifiedDate AuditDate, [Name] from ContentRuleMapping_Log A 
						inner join Content B 
		on A.ContentID=B.ContentID and [Action]= 1 and RowType='NewRow' AND B.ContentID=@ID
		and A.LanguageID=B.LanguageID and  A.LanguageID=@LanguageID and  B.LanguageID=@LanguageID
		INNER JOIN AdminUser C
		on A.UpdatedByUserID=C.AdminUserID

		order by AuditDate desc

		end



End

