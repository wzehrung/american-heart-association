﻿CREATE PROCEDURE [dbo].[Proc_GetPatientsWithProviderGroupRules]

As
Begin
set nocount on

;WITH Providers As
(
select distinct ProviderID from PatientGroupDetails WHERE GroupRuleXml IS NOT NULL
)

SELECT DISTINCT P.*

  FROM  PatientGroupDetails PGD  
  inner join Providers C on PGD.ProviderID=C.ProviderID AND ISDEFAULT=1
  INNER JOIN PatientGroupMapping PGM ON  PGD.GroupID=PGM.GroupID 
  INNER join HealthRecord P on P.UserHealthRecordID=PGM.PatientID
 
  

	
End

