﻿
CREATE PROC [dbo].[Proc_FindCampaignByUrl]
(
	 @Url nvarchar(2048)
	,@LanguageID int
)

As


Begin
set nocount on

select C.*,CD.LanguageID,CD.Description,CD.Title,CD.PromotionalTagLine,CD.PartnerLogoVersion,CD.PartnerLogoContentType,CD.PartnerImageVersion,CD.PartnerImageContentType,CD.IsPublished  
,case when Convert(Datetime,Convert(Varchar,getdate(),103),103) between StartDate and  EndDate  and isPublished=1 
then 1 else 0 end As isActive, C.BusinessID, ab.Name AS BusinessName

from Campaign C inner join CampaignDetails CD on C.CampaignID=CD.CampaignID 
AND C.Url=@Url

	LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

where LanguageID=@LanguageID 

End


