﻿

--[Proc_FindAlertRuleByID] 127


CREATE proc [dbo].[Proc_FindAlertRuleByID]
(
@AlertRuleID int 
)

AS

Begin
set nocount on

select distinct A.AlertRuleID,A.ProviderID,Type,Cast(AlertData as varchar(max)) AlertData
,IsActive,SendMessageToPatient
,case when C.GroupID is not null then null else C.PatientID end PatientID

,A.GroupID
--,D.Name  As GroupName
,(SELECT Name FROM PatientGroupDetails B WHERE B.GROUPID=A.GroupID)  GroupName
,case when A.GroupID =D.GroupID and D.isDefault=1 then 'A' 
      when C.GroupID is null and A.GroupID is null then 'P' else 'G' End As AlertRuleSubscriberType

,MessageToPatient


from (select * from AlertRule where AlertRuleID=@AlertRuleID) A 
left join PatientGroupDetails D on A.GroupID=D.GroupID
left join alertmapping C on A.AlertRuleID=C.AlertRuleID AND ISACTIVE=1
--AND C.GroupID=D.GroupID
--and A.AlertRuleID=@AlertRuleID


End


