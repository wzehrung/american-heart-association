﻿



CREATE Procedure [dbo].[Proc_DropWeight]
(
@HVItemGuid uniqueidentifier  = NULL
)
AS
Begin

/* Auth0r			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Delete a record from Weight table
*/

set nocount on

Begin Try
Begin Transaction

Delete from  dbo.Weight	  
					   Where  HVItemGuid=@HVItemGuid 
Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.Weight	  
							 Where HVItemGuid=@HVItemGuid 

exec proc_UpdateHealthRecord_LastActivityDate_PrimaryKey @UserHealthRecordID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End





