﻿


CREATE proc [dbo].[proc_UpdateGroup]
(
 @GroupID int
,@Description nvarchar(MAX)
,@GroupRuleXml xml
,@GroupName nvarchar(255)
,@RemoveOption char(1)
)
as
begin
	set nocount on

	update PatientGroupDetails set Name=@GroupName, Description = @Description, GroupRuleXml = @GroupRuleXml
								,RemoveOption = @RemoveOption 

	where GroupID=@GroupID
end 

