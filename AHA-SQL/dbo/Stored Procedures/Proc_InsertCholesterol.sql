﻿
CREATE Procedure [dbo].[Proc_InsertCholesterol]
(
 @UserHealthRecordID int 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier
,@Totalcholesterol int  
,@LDL int  
,@HDL int  
,@Triglyceride int
,@TestLocation nVarchar(200)
,@DateMeasured datetime 
)
AS

Begin


--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :insert record to Cholesterol table ..
*/


Insert dbo.Cholesterol 
				 (
                     UserHealthRecordID
                    ,HVItemGuid
                    ,HVVersionStamp
					,Totalcholesterol
                    ,LDL
                    ,HDL
					,Triglyceride
                    ,TestLocation
                    ,DateMeasured                    
                  )
Select               @UserHealthRecordID
                    ,@HVItemGuid
                    ,@HVVersionStamp
					,@Totalcholesterol
                    ,@LDL
                    ,@HDL
					,@Triglyceride
                    ,@TestLocation
                    ,@DateMeasured
                   
End






