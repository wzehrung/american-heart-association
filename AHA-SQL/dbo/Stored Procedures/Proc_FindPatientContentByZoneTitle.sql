﻿
CREATE PROC [dbo].[Proc_FindPatientContentByZoneTitle]
(
@ZoneTitle nvarchar(128)
,@LanguageID int
,@ProviderID int = 0 
)

As

Begin
set nocount on

if (@ProviderID = 0) 
begin
	select * from PatientContent
	where  ZoneTitle = @ZoneTitle and LanguageID=@LanguageID and ProviderID is null
end

else
begin
	select * from PatientContent
	where  ZoneTitle = @ZoneTitle and LanguageID=@LanguageID and ProviderID = @ProviderID
end
End
