﻿CREATE PROC [dbo].[Proc_UpdateGrantSupport]
(
 @GrantSupportID INT
,@Title nvarchar(128)
,@Description nvarchar(max)
,@Logo varbinary(max)
,@IsForProvider bit
,@ContentType nvarchar(10)
,@UpdatedByUserID INT
,@LanguageID int

)

As

Begin
set nocount on

Update GrantSupport set 
						 Title = @Title
						,Description = @Description
						,Logo = isnull(@Logo,Logo)
						,IsForProvider = @IsForProvider
						,ContentType = @ContentType 
						,Version = Version+1
						,UpdatedByUserID=@UpdatedByUserID
where  GrantSupportID = @GrantSupportID and LanguageID=@LanguageID

End


