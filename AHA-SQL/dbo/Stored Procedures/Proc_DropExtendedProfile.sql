﻿




CREATE Procedure [dbo].[Proc_DropExtendedProfile]
(
 @PersonalItemGUID uniqueidentifier 
)
AS
Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-21  
   Description      :Delete record of ExtendedProfile by ItemGUid and HealthrecordID
*/

set nocount on

Begin Try
Begin Transaction


Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							 

Delete from  dbo.ExtendedProfile Where UserHealthRecordID = @UserHealthRecordID

exec proc_UpdateHealthRecord_LastActivityDate_PrimaryKey @UserHealthRecordID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
								
End








