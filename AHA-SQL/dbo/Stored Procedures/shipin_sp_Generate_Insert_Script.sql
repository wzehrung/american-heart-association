﻿
Create Procedure [dbo].[shipin_sp_Generate_Insert_Script]

As

  Begin

 Declare  GenerateScript_cursor CURSOR FAST_FORWARD FOR 
 select TableName from MasterLookUpTables

    OPEN GenerateScript_cursor
	declare @tableList varchar(500)

		declare @looktable table
		(
			LookUpID  int identity (1,1),
			script nvarchar(max)
		)
	   
           FETCH NEXT FROM GenerateScript_cursor INTO @tableList
			  
			IF @@fetch_status<>0  
			begin  
                 close GenerateScript_cursor  
				 deallocate GenerateScript_cursor  
				 return  
			END  

			WHILE @@FETCH_STATUS=0  
			begin  
      			
				
			insert @looktable 
			exec Shipin_sp_ScriptSingleTable @tableList
			



			FETCH NEXT FROM GenerateScript_cursor INTO @tableList
			end  
	
	CLOSE GenerateScript_cursor  
	DEALLOCATE GenerateScript_cursor  
	  
	select * from @looktable  order by LookUpID

			/* 
			**** Please Do Not Delete This ******
				
				Truncate Table MasterLookUpTables
				go
				insert into masterlookuptables 
				select 'HearAbout'
				union all
				select 'CountryMapping'
				union all
				select 'BusinessSize'
				union all
				select 'BusinessSubCategory'
				union all
				select 'Concern'
				union all
				select 'ReservedSiteURL'
				union all
				select 'IsoCountryCode'
				union all
				select 'TradespaceUrlMappings'
				union all
				select 'RegionLKup'
				union all
				select 'channelcategory'
				union all
				select 'channel'
				union all
				select 'businesstype'
				union all
				select 'tradespacecommunitycategories'
				union all
				select 'tradespacecommunitysubcategories'
				union all
				select 'Tradespacecommunities'
				union all
				select 'TipofDay'
				union all
				select 'Whatsnew' */


  End
