﻿
CREATE Procedure [dbo].[Proc_UpdatePatientSMSReminder]
(
 @PatientID Int
,@MobileNumber varchar(11)
,@PIN varchar(4)
,@TimeZone nvarchar(25)
,@Terms bit
,@PinValidated bit
,@ReminderID int
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on


UPDATE dbo.[3CiPatientReminder] SET
		 MobileNumber = @MobileNumber					
		,PIN = @PIN
		,TimeZone = @TimeZone
		,Terms = @Terms
		,PinValidated = @PinValidated
		,UpdatedDateTime = getdate()
WHERE ReminderID = @ReminderID AND PatientID = @PatientID			



End
