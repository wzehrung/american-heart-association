﻿
CREATE PROCEDURE [dbo].[Proc_FindProviderByUsername]
(
 @UserName nvarchar(255)
)
AS
BEGIN

SET NOCOUNT ON

IF (SELECT CampaignID FROM Provider WHERE UserName = @UserName) IS NOT NULL 

SELECT p.*,l.LanguageLocale, c.URL AS CampaignURL FROM Provider p
	INNER JOIN Languages l ON p.DefaultLanguageID = l.LanguageID
	INNER JOIN Campaign c ON c.CampaignID = p.CampaignID

WHERE p.UserName = @UserName

ELSE

SELECT p.*,l.LanguageLocale, '' AS CampaignURL FROM Provider p
	INNER JOIN Languages l ON p.DefaultLanguageID = l.LanguageID

WHERE p.UserName = @UserName

END
