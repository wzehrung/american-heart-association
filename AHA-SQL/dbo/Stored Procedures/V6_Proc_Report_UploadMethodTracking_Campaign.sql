﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 6.17.2013
-- Description:	Refactor of Proc_Report_UploadMethodTracking_Campaign for Campaigns/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_UploadMethodTracking_Campaign] 
(
	@StartDate Datetime,  
	@EndDate Datetime,
	@CampaignID int = null,	-- null specifies all campaigns
	@Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
	@CampaignIDs nvarchar(max)
)
AS

BEGIN

/* For testing */
--DROP TABLE #RptMarketZipCodes
--DROP TABLE #h
--DROP TABLE #ToBePivoted
--DROP TABLE #ColumnsIndex

--DECLARE @StartDate Datetime  
--DECLARE @EndDate Datetime
--DECLARE @CampaignID int
--DECLARE @Markets NVARCHAR(MAX)

/* For testing */
--SET @StartDate='Jan  1 2012 12:00:00:000AM'
--SET @EndDate='Jan  1 2013 12:00:00:000AM'
--SET @CampaignID=76

	DECLARE @cols NVARCHAR(MAX)
	DECLARE @colsConv NVARCHAR(MAX)
	DECLARE @i int 

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
		from ZipRegionMapping 
		where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 
    
    
 	--
	-- Create a temp table of all users who are in campaign/markets logic
	--
	select * into #h from	
		(select UserID, AHR.UserHealthRecordID
			from  AHAUserHealthRecord AHR
			inner join HealthRecord H on H.UserHealthRecordID = AHR.UserHealthRecordID
			left join  ExtendedProfile E ON E.UserHealthRecordID = AHR.UserHealthRecordID

			/* OLD WAY
			--AND (@CampaignID is null OR @CampaignID = H.CampaignID) AND
			--	  (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))

			/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
					just get those is specified */
			WHERE ((@CampaignId IS NULL AND H.CampaignID IS NOT NULL) OR (@CampaignId IS NOT NULL AND @CampaignId = H.CampaignID))
				AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))		) a
			*/

			--  NEW WAY:
			--  WHZ: 7/22/2015
			WHERE
			@CampaignIDs IS NULL AND H.CampaignID IS NOT NULL
			OR (H.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AHR.UserID IS NOT NULL)
				) a

	
	--
	-- generate the report
	--
	
	SELECT @i = count(*) FROM #h
    IF(@i = 0)
    BEGIN
         SELECT 'NONE' as ReadingSource, '0.00%' as [Column # 1], '0.00%' as [Column # 2] where 1=2
    END
    ELSE
    BEGIN

		;With Tracker As
		(
			select D.Date1, COUNT(DIstinct H.UserHealthRecordID) TrackerCount,'Blood Pressure' Tracker, 
					CASE 
						WHEN ReadingSource IS NULL THEN 'Other' 
						WHEN ReadingSource Like 'Aetna%' THEN 'mobile'
						WHEN ReadingSource Like 'And%' THEN 'Home'
						WHEN ReadingSource Like 'Omron%' THEN 'Home'
						WHEN ReadingSource Like 'homedco%' THEN 'Home'
						WHEN ReadingSource Like 'Polka%' THEN 'Mobile'
						WHEN ReadingSource Like 'homedics%' THEN 'Home'
						WHEN ReadingSource Like 'microlife%' THEN 'Home'
						WHEN ReadingSource Like 'Doctor%Office%' THEN 'Doctor Office'
						WHEN ReadingSource Like 'mobile%' THEN 'mobile'
						WHEN ReadingSource Like 'Pharmacy%' THEN 'Pharmacy'
						WHEN ReadingSource Like 'Home%' THEN 'Home'
						WHEN ReadingSource Like 'Hospital%' THEN 'Hospital'
						WHEN ReadingSource Like 'Health Fair%' THEN 'Health Fair'
						WHEN ReadingSource Like 'IVR%' THEN 'IVR'
						WHEN ReadingSource Like 'Device%' THEN 'Device'
						WHEN ReadingSource Like 'YMCA%' THEN 'YMCA'
						ELSE 'Other' 
					END ReadingSource
			
				from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D 
				left join BloodPressure BP on BP.CreatedDate < D.Date2 
				left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID
			--WHERE H.UserHealthRecordID is not null
				GROUP BY Date1,CASE
					WHEN ReadingSource IS NULL THEN 'Other' 
					WHEN ReadingSource Like 'Aetna%' THEN 'mobile'
					WHEN ReadingSource Like 'And%' THEN 'Home'
					WHEN ReadingSource Like 'Omron%' THEN 'Home'
					WHEN ReadingSource Like 'homedco%' THEN 'Home'
					WHEN ReadingSource Like 'Polka%' THEN 'Mobile'
					WHEN ReadingSource Like 'homedics%' THEN 'Home'
					WHEN ReadingSource Like 'microlife%' THEN 'Home'
					WHEN ReadingSource Like 'Doctor%Office%' THEN 'Doctor Office'
					WHEN ReadingSource Like 'mobile%' THEN 'mobile'
					WHEN ReadingSource Like 'Pharmacy%' THEN 'Pharmacy'
					WHEN ReadingSource Like 'Home%' THEN 'Home'
					WHEN ReadingSource Like 'Hospital%' THEN 'Hospital'
					WHEN ReadingSource Like 'Health Fair%' THEN 'Health Fair'
					WHEN ReadingSource Like 'IVR%' THEN 'IVR'
					WHEN ReadingSource Like 'Device%' THEN 'Device'
					WHEN ReadingSource Like 'YMCA%' THEN 'YMCA'
					else 'Other' end 
		)
		
		,
		UserCountP As

		(
			select D.Date1,H.UserHealthRecordID 
				from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D 
				left join BloodPressure BP on BP.CreatedDate<D.Date2 
				left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID
		)
		,
		UserCount As
		(
			select Date1,count(distinct UserHealthRecordID) UserCount  from UserCountP group by Date1
		)

		Select T.Date1,Tracker, ReadingSource,
		       case when UserCount=0 then 0 else cast(TrackerCount as real)*100/cast (UserCount as real) end as Perc,
			   'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex,
				cast(dense_Rank() over (order by T.Date1) as int) IntIndex
			into #ToBePivoted
			From Tracker T 
			inner join UserCount U on T.Date1=U.Date1

		SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


		SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

		SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

		EXECUTE(N'SELECT ReadingSource, '+ @colsConv +' FROM
				(SELECT ReadingSource, Tracker, ColumnINdex, Perc FROM #ToBePivoted ) A
				PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B;')
	
   END   
    
    
    
END


