﻿-- =============================================
-- Author:		Scott Shipp
-- Create date: January 30, 2009
-- Description:	This is a snapshot of the number
-- of visits per user and their join date at
-- the time the query is run.
-- =============================================
CREATE PROCEDURE Proc_RptVisitsPerUser 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select AHAUserID as "User ID", count(AHAUserID) as Visits, AHAUser.CreatedDate as 'Joined On' from AHAUserLoginDetails inner join AHAUser on AHAUserID=UserID group by AHAUserID, CreatedDate order by Visits desc
END
