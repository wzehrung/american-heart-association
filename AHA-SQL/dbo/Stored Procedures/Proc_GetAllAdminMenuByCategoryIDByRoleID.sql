﻿CREATE PROC [dbo].[Proc_GetAllAdminMenuByCategoryIDByRoleID]

(
@CategoryID int
,@RoleID int
)
As

Begin
set nocount on

IF (@RoleID = 1 and @CategoryID =3)
BEGIN

SELECT
	a.MenuID
	,a.Name-- + ' ' + CONVERT(nvarchar,a.MenuID) AS Name
	,a.Description
	,a.CategoryID
	,b.Enabled
	,a.SortID
FROM AdminMenu a, AdminRolesMenu b
WHERE CategoryID = @CategoryID
and b.RoleID = @RoleID
and a.MenuID = b.MenuID
UNION
SELECT 
  MenuID, Name, Description, 3, 1, SortID
FROM AdminMenu where Name = 'Manage User Roles'
ORDER BY a.SortID

END

ELSE

BEGIN

SELECT
	a.MenuID
	,a.Name-- + ' ' + CONVERT(nvarchar,a.MenuID) AS Name
	,a.Description
	,a.CategoryID
	,b.Enabled
	,a.SortID
FROM AdminMenu a, AdminRolesMenu b
WHERE CategoryID = @CategoryID
and b.RoleID = @RoleID
and a.MenuID = b.MenuID
ORDER BY a.SortID

END


End

