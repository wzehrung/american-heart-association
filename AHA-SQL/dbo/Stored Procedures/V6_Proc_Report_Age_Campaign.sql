﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 6/13/2013
-- Description:	Refactor of Proc_Report_Age_Campaign to handle Campaign/Market
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Age_Campaign] 
(
	@StartDate Datetime,  
	@EndDate Datetime,
	@CampaignID int = null,	-- null specifies all campaigns
	@Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
	@CampaignIDs nvarchar(max)
)
AS
BEGIN
	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
	from ZipRegionMapping 
	where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	--
	-- generate the report
	--

    ;With AgeBase As
		(
			select D.Date1,count(distinct AH.UserHealthRecordID)  AgeCount
			,CASE  WHEN (year(getdate())-YearOfBirth ) IS NULL  THEN 'Unreported'
			WHEN (year(getdate())-YearOfBirth ) <25  THEN 'Under 25'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 25 AND 34 THEN '25-34'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 35 AND 44 THEN '35-44'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 45 AND 54 THEN '45-54'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 55 AND 64 THEN '55-64'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 65 AND 74 THEN '65-74'
			WHEN (year(getdate())-YearOfBirth ) >=75  THEN '75 or older' END AGE

			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D 
			inner join AHAUserHealthRecord AH on AH.CreatedDate<D.Date2 
			left join  ExtendedProfile E ON E.UserHealthRecordID=AH.UserHealthRecordID  
			inner join HealthRecord H on H.UserHealthRecordID=AH.UserHealthRecordID

			/* OLD WAY
			/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
					just get those is specified */
			WHERE ((@CampaignId IS NULL AND H.CampaignID IS NOT NULL) OR (@CampaignId IS NOT NULL AND @CampaignId = H.CampaignID))
				AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
				*/
			WHERE
			@CampaignIDs IS NULL AND H.CampaignID IS NOT NULL
			OR (H.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AH.UserID IS NOT NULL)

			GROUP BY Date1,CASE  WHEN (year(getdate())-YearOfBirth ) IS NULL  THEN 'Unreported'
			WHEN (year(getdate())-YearOfBirth ) <25  THEN 'Under 25'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 25 AND 34 THEN '25-34'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 35 AND 44 THEN '35-44'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 45 AND 54 THEN '45-54'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 55 AND 64 THEN '55-64'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 65 AND 74 THEN '65-74'
			WHEN (year(getdate())-YearOfBirth ) >=75  THEN '75 or older' END
		)
		,AgeTypes As
		(
			select 'Under 25' as AgeType,1 rptID
			union all
			select '25-34' as AgeType,2 rptID
			union all
			select '35-44' as AgeType,3 rptID
			union all
			select '45-54' as AgeType,4 rptID
			union all
			select '55-64' as AgeType,5 rptID
			union all
			select '65-74' as AgeType,6 rptID
			union all
			select '75 or older' as AgeType,7 rptID
			union all
			select 'Unreported' as AgeType,8 rptID
		)
		,AgeDates As
		(
			select D.Date1,G.AgeType,rptID   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D cross join AgeTypes G
		)
		,Age As
		(
			 select rptID,D.Date1,GB.AgeCount,D.AgeType Age 
				from AgeDates D 
				left join AgeBase GB on D.Date1=GB.date1 and D.AgeType=GB.Age
		)

--		,UserCount As
--		(
--		select D.Date,COUNT(UserHealthRecordID) UserCount
--		from  DBO.udf_GetDates (@StartDate,@EndDate) D left join   AHAUserHealthRecord AH 
--		on AH.CreatedDate<D.Date GROUP BY Date
--		)
----		,ToBePivoted As
----		(
		Select rptID,T.Date1,Age,isnull(CASE WHEN T.Date1 >=@StartDate and T.Date1 <= @EndDate and AgeCount is null then 0 else AgeCount end,0) AgeCount
			 --UserCount--case when UserCount=0 then 0 else cast(GenderCount as real)*100/cast (UserCount as real) end as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Age T --inner join UserCount U on T.Date=U.Date
----		)

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'


SET @query = N'SELECT Age, '+ @cols +' FROM
(SELECT Age, ColumnINdex, AgeCounT,rptID FROM #ToBePivoted ) A
PIVOT (SUM(AgeCount) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;'

EXECUTE(@query)
END

