﻿
CREATE PROC [dbo].[Proc_Polka_GetPolkaDataForUploadForPatient]
( 
@PersonalItemGUID Uniqueidentifier
)

As


Begin


declare @UserHealthRecordID int

set @UserHealthRecordID =(select UserHealthRecordID  from HealthRecord where PersonalItemGUID=@PersonalItemGUID)

declare @xmlPolkaData nvarchar(max)
set @xmlPolkaData=''

select @xmlPolkaData=@xmlPolkaData+cast(PolkaData as nvarchar(max)) from PolkaData where UserHealthRecordID=@UserHealthRecordID
and IsUploadedToHV<>1

set @xmlPolkaData='<PolkaData>'+@xmlPolkaData+'</PolkaData>'

select cast(@xmlPolkaData as xml) xmlPolkaData

End

