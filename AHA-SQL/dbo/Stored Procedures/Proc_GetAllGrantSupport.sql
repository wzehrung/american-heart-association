﻿  
CREATE PROC [dbo].[Proc_GetAllGrantSupport]  
  (
@LanguageID int
)
As  
  
Begin  
set nocount on  
  
SELECT  * FROM GrantSupport where LanguageID=@LanguageID
  
End