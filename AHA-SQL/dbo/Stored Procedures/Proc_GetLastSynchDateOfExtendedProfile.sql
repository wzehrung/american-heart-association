﻿
CREATE PROCEDURE [dbo].[Proc_GetLastSynchDateOfExtendedProfile] 
(
  @PersonalItemGUID uniqueidentifier 
)

As

Begin 

/* Author           :Shipin Anand  
   Created Date     :2008-08-07    
   Description      :Getting LastSynchDate for the given UserHealthRecordGUID
					 
*/ 

set nocount on


Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							 	

select	LastSynchDate from 
							dbo.ExtendedProfile where
							UserHealthRecordID=@UserHealthRecordID 	

End

