﻿
CREATE Procedure Proc_InsertMLCItem
(
 @UserHealthRecordID int 
,@HealthScore float
,@ActivityRating int 
,@BloodSugarRating int 
,@BPRating int 
,@CholRating int 
,@WeightRating int 
,@DietRating int 
,@SmokingRating int 
,@SystolicBP int
,@DiastolicBP int
,@TotalCholesterol int
,@BloodGlucose float
,@BMI float
)

AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a new entry to ExtendedProfile Table
*/

set nocount on


insert dbo.MyLifeCheck 
						(
						 UserHealthRecordID
						,HealthScore
						,ActivityRating
						,BloodSugarRating
						,BPRating
						,CholRating
						,WeightRating
						,DietRating
						,SmokingRating 
						,SystolicBP
						,DiastolicBP
						,TotalCholesterol
						,BloodGlucose
						,BMI
						)

select

						 @UserHealthRecordID
						,@HealthScore
						,@ActivityRating
						,@BloodSugarRating
						,@BPRating
						,@CholRating
						,@WeightRating
						,@DietRating
						,@SmokingRating
						,@SystolicBP
						,@DiastolicBP
						,@TotalCholesterol
						,@BloodGlucose
						,@BMI
End
