﻿
CREATE PROC [dbo].[Proc_Report_GetBusinessAdminCampaigns]
	(
		@BusinessID int
	)

AS

BEGIN

--DECLARE @BusinessID int
DECLARE @SQLQuery AS NVARCHAR(500)

--SET @BusinessID = 2

IF @BusinessID IS NOT NULL
BEGIN

	SET @SQLQuery = 'SELECT CampaignID, Title FROM Heart360..v_CampaignsBusinesses WHERE BusinessID = ' + CONVERT(nvarchar,@BusinessID) + ' ORDER BY Title'

END

ELSE
BEGIN

	SET @SQLQuery = 'SELECT CampaignID, Title FROM Heart360..v_CampaignsBusinesses ORDER BY Title'	

END

EXECUTE sp_executesql @SQLQuery

END
