﻿CREATE PROC [dbo].[Proc_GetALLPatientGrantSupport]
(
@LanguageID int
)
as
begin
set nocount on

select * from GrantSupport where IsForProvider=0 and LanguageID=@LanguageID

end
