﻿CREATE PROC [dbo].[Proc_UpdateAdminReport]
(
 @RoleID int
,@ReportID int
,@Enabled BIT
)

As

Begin
set nocount on

if exists(select 1 from adminrolesReport where roleid = @roleid and Reportid = @Reportid)
begin
  Update AdminRolesReport set 
						 Enabled = @Enabled
  where  roleid = @roleid and Reportid = @Reportid
end
else
begin
    insert into adminrolesReport (RoleID, ReportID, Enabled) VALUES (@Roleid, @ReportID, @Enabled)
end
End
