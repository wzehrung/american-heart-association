﻿

--Proc_Report_ProviderDemoGraphics  '2009.07.01','2009.9.01'

CREATE PROC [dbo].[Proc_Report_ProviderDemoGraphics]
(
 @StartDate datetime
,@enddate datetime
)
As

Begin


	declare @TotalProviderCount int


	select	@TotalProviderCount=
			COUNT(ProviderID) 

	from Provider where  DateOfRegistration between  @StartDate and @enddate 


	select  LI.ListItemName,COUNT(ProviderID) ProviderCount
			,CAST(CAST (COUNT(ProviderID) AS NUMERIC(4,2))/CAST (@TotalProviderCount AS NUMERIC(4,2))*100 AS NUMERIC(4,2))
			ProviderPerc from 

				Lists L 
				inner join 
				ListItems LI

	on L.ListID=LI.ListID
	and ListName='States'

	lEFT JOIN 

	Provider P  on P.StateID=LI.ListITemID AND  DateOfRegistration between  @StartDate and @enddate 

	Group by LI.ListItemName



End


