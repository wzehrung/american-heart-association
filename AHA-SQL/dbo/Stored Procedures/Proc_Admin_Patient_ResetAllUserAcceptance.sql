﻿CREATE proc [dbo].Proc_Admin_Patient_ResetAllUserAcceptance
AS
Begin
set nocount on
update dbo.AHAUser
set NewTermsAndConditions = 0, TermsAcceptanceDate = null, UpdatedDate = GETDATE()
End

/* ---------- */

