﻿


CREATE Procedure [dbo].[Proc_UpdateWeight]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Weight float  
,@ReadingSource nvarchar(100)
,@Comments nvarchar(max) 
,@DateMeasured Datetime 
)
AS

Begin


--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :updating weight table..
*/

Update dbo.Weight
              SET 
				 HVVersionStamp		= @HVVersionStamp
				,Weight				= @Weight 
				,Comments			= @Comments 
				,ReadingSource		= @ReadingSource
				,DateMeasured		= @DateMeasured 
				,UpdatedDate		= getDate()  

              Where 
			  HVItemGuid=@HVItemGuid 
End







