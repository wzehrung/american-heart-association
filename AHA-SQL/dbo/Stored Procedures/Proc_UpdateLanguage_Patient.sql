﻿create procedure [dbo].[Proc_UpdateLanguage_Patient]
(
	@LanguageID int,
	@PersonGUID uniqueidentifier
)
as
begin
	update AHAUser set DefaultLanguageID = @LanguageID where PersonGUID = @PersonGUID
end 