﻿
CREATE PROCEDURE [dbo].[proc_GetSiteErrorLog]
    @PageIndex INT, 
    @PageSize INT 
AS

BEGIN 

WITH LogEntries AS ( 
SELECT ROW_NUMBER() OVER (ORDER BY DateInserted DESC)
AS Row, * 
FROM SiteErrorLog)

SELECT ErrorID,ErrorText,ErrorMessage,IsErrorFixed,DateInserted,DateUpdated
FROM LogEntries 
WHERE Row between 

(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize


END 
