﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.20.2014
-- Description:	Get users admins and above to assign as feedback recipients
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_GetAdminUsers]

AS

BEGIN
SET NOCOUNT ON

SELECT
    AdminUserID,
    Name,
    Email,
    FeedbackRecipient

FROM AdminUser

WHERE RoleID = '1' OR RoleID = '2'

END