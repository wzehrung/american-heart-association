﻿CREATE Procedure [dbo].[Proc_UpdateExtendedProfile]
(
 @UserHealthRecordID int
,@FamilyHistoryHD int
,@FamilyHistoryStroke int
,@HighBPDiet int 
,@HighBPMedication int 
,@KidneyDisease int 
,@DiabetesDiagnosis int 
,@HeartDiagnosis int 
,@Stroke int 
,@PhysicalActivityLevel nvarchar(100) 
,@BloodType nvarchar(100) 
,@MedicationAllergy nvarchar(max)
,@TravelTimeToWorkInMinutes float
,@MeansOfTransportation nvarchar(255)
,@AttitudesTowardsHealthcare nvarchar(100)
,@DoYouSmoke bit
,@HaveYouEverTriedToQuitSmoking  bit
,@LastSynchDate Datetime
,@Country  nvarchar(50) 
,@Gender nvarchar(10)  
,@Ethnicity nvarchar(50)  
,@MaritalStatus nvarchar(50)  
,@YearOfBirth INT
,@ZipCode nvarchar(50)

)

AS

Begin


--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Update ExtendedProfile Table
*/

set nocount on

if(@ZipCode = '0')
begin
  SET @ZipcOde = null
end

Update  dbo.ExtendedProfile 

						SET	
						 
						 FamilyHistoryHD				=@FamilyHistoryHD
						,FamilyHistoryStroke			=@FamilyHistoryStroke
						,HighBPDiet						=@HighBPDiet
						,HighBPMedication				=@HighBPMedication
						,KidneyDisease					=@KidneyDisease
						,DiabetesDiagnosis				=@DiabetesDiagnosis
						,HeartDiagnosis					=@HeartDiagnosis
						,Stroke							=@Stroke
						,PhysicalActivityLevel			=@PhysicalActivityLevel
						,BloodType						=@BloodType
						,MedicationAllergy				=@MedicationAllergy
						,TravelTimeToWorkInMinutes		=@TravelTimeToWorkInMinutes
						,MeansOfTransportation			=@MeansOfTransportation
						,AttitudesTowardsHealthcare		=@AttitudesTowardsHealthcare
						,DoYouSmoke						=@DoYouSmoke
						,HaveYouEverTriedToQuitSmoking	=@HaveYouEverTriedToQuitSmoking
						,LastSynchDate					=isnull(@LastSynchDate,LastSynchDate)
						,[Country/Region]				= @Country 
						,Gender							= @Gender 
						,Ethnicity						= @Ethnicity 
						,MaritalStatus					= @MaritalStatus
						,UpdatedDate					= GetDate()
						,YearOfBirth					= @YearOfBirth 
						,ZipCode						= @ZipCode


					Where UserHealthRecordID=@UserHealthRecordID 

End



