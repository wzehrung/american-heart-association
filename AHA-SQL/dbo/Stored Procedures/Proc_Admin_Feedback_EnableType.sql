﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.20.2014
-- Description:	Enable/disable feedback types used in the feedback form on the patient portal
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_EnableType]
(
    @Enabled bit,
    @FeedbackTypeID int
)

AS

BEGIN
SET NOCOUNT ON

UPDATE FeedbackTypes
SET
    [Enabled] = @Enabled
WHERE FeedbackTypeID = @FeedbackTypeID

END