﻿
CREATE Procedure [dbo].[Proc_UpdatePatientSMSReminderPINValidated]
(
 @PatientID Int
,@PINValidated bit
,@ReminderID int
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on


UPDATE dbo.[3CiPatientReminder] SET					
		 PinValidated = @PINValidated
		,UpdatedDateTime = getdate()
WHERE ReminderID = @ReminderID AND PatientID = @PatientID


End
