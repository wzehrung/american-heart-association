﻿
create Procedure Proc_UpdateExerciseGoal
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Intensity nvarchar(50)
,@Duration float
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand  
   Created Date     :2010-09-06    
   Description      :Updating records of ExerciseGoal

   Note : Please dont try to update a indexed foreign key 
*/  

Update dbo.ExerciseGoal

				   SET 
				   HVVersionStamp = @HVVersionStamp
				  ,Intensity = @Intensity 
				  ,Duration	= @Duration 
                  ,UpdatedDate	= GetDate()

                    
		Where HVItemGuid=@HVItemGuid 
					
End
