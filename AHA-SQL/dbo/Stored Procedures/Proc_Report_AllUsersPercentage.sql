﻿CREATE PROC [dbo].[Proc_Report_AllUsersPercentage]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
,@PartnerID int = null
)
As

Begin
DECLARE @cols NVARCHAR(2000)
DECLARE @colsConv NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

set nocount on

		;with RptTypes As
	(
	select 1 RptID,'1 Visit (All)' As RptType
	union all
	select 2 RptID,'2-4 Visits (All)' As RptType
	union all
	select 3 RptID,'5-10 Visits (All)' As RptType
	union all
	select 4 RptID,'>10 Visits (All)' As RptType
	),
	NewRptTypes As
	(
	select 5 RptID,'1 Visit (New)' As RptType
	union all
	select 6 RptID,'2-4 Visits (New)' As RptType
	union all
	select 7 RptID,'5-10 Visits (New)' As RptType
	union all
	select 8 RptID,'>10 Visits (New)' As RptType
	)
	,RptTypesWithDates As
	(
	select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	),
	NewRptTypesWithDates As
	(
	select * from  NewRptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,EligibleLogins As
	(
	Select R.Date1,AHAUserID,Count(A.UserID) as Visits from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
	AHAUserLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2 
	LEFT join 
	(
	SELECT DISTINCT AHR.UserID, PGD.ProviderID  FROM AHAUserHealthRecord AHR 
	INNER join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID 
	left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
	left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
	 where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
	) A ON A.UserID=ALD.AHAUserID AND isnull(A.ProviderID,-1) = isnull(isnull(@PartnerID, A.ProviderID),-1)
	group by R.Date1,AHAUserID
	)
	,EligibleUsers As
	(
	Select distinct A.UserID,R.Date1 from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R 
		inner join AHAUser A 
		on A.CreatedDate >=R.Date1 and A.CreatedDate <R.Date2 
		
		left join AHAUserHealthRecord AHR on AHR.UserID=A.UserID
		left join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID  
		left join PatientGroupMapping PGM on PGM.PatientID = AHR.UserHealthRecordID
		left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
										
	where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
	AND isnull(PGD.ProviderID,-1) = isnull(isnull(@PartnerID, PGD.ProviderID),-1)
	)

	,Visits As
	(
	select      E1.date1,AHAUserID,Case when Visits =1 then '1 Visit (All)' when Visits between 2 and 4 then '2-4 Visits (All)'
				when Visits between 5 and 10 then '5-10 Visits (All)'
				when Visits >10 then '>10 Visits (All)' End RptType from EligibleLogins E1
	)
	,NewVisits As
	(
	Select R.Date1,ALD.AHAUserID
			,Case when count(AHAUserID) =1 then '1 Visit (New)' when count(AHAUserID) between 2 and 4 then '2-4 Visits (New)'
				  when count(AHAUserID) between 5 and 10 then '5-10 Visits (New)'
				  when count(AHAUserID) >10 then '>10 Visits (New)' End RptType 
				  
		   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
					AHAUserLoginDetails ALD on ALD.LoggedInDate >= R.Date1 and ALD.LoggedInDate < R.Date2  inner join  
					EligibleUsers E1 on E1.UserID=ALD.AHAUserID and E1.date1=R.date1
	group by R.Date1,AHAUserID
	)
	,VisitsFinal As
	(
	select  R.RptID,R.date1,R.RptType,Count(V.RptType) UserCount,1 AS ID  from RptTypesWithDates R left join Visits V
	on V.Date1=R.Date1 and V.RptType=R.RptType   
	group by R.date1,R.RptType, R.RptID
	)
	,NewVistsFinal As
	(
	Select RptID,R.date1,R.RptType,Count(V.RptType) userCount,1 AS ID 
	from NewRptTypesWithDates R left join NewVisits V
	on V.Date1=R.Date1 and V.RptType=R.RptType  
	group by R.date1,R.RptType,RptID
	)
	,TotalVisits As
	(
	select SUM(Usercount)  TotalUserCount,Date1  from
	VisitsFinal group by date1
	)
	,TotalNewVisits As
	(
	select SUM(Usercount)  TotalUserCount,Date1  from
	NewVistsFinal group by Date1
	)

	SELECT RptID, date1, RptType, Perc,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
	,cast(dense_Rank() over (order by date1) as int) IntIndex
	INTO #ToBePivoted
	FROM (
	Select RptID,N.date1,N.RptType,isnull(case when TotalUserCount=0 then 0 else cast(userCount as real)*100/cast (TotalUserCount as real) end,0) as Perc

	from VisitsFinal N inner join TotalVisits T
	on N.date1=T.date1 
	UNION ALL
	Select RptID,N.date1,N.RptType,isnull(case when TotalUserCount=0 then 0 else cast(userCount as real)*100/cast (TotalUserCount as real) end,0) as Perc

	from NewVistsFinal N inner join TotalNewVisits T
	on N.Date1=T.Date1 
	) A

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
		
SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

SET @query = N'SELECT RptType, '+ @colsConv +' FROM
(SELECT RptType, ColumnINdex, Perc, rptid FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;'

EXECUTE(@query)

End
