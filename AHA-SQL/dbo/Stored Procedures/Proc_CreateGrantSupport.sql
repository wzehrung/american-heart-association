﻿
CREATE PROC [dbo].[Proc_CreateGrantSupport]
(
 @Title nvarchar(128)
,@Description nvarchar(max)
,@Logo varbinary(max)
,@IsForProvider bit
,@ContentType nvarchar(10)
,@CreatedByUserID INT
,@LanguageID int
,@GrantSupportID INT OUTPUT
)

As

Begin
set nocount on

IF @GrantSupportID IS NULL OR @GrantSupportID=0
set @GrantSupportID=(select max(GrantSupportID)+1 from GrantSupport )


Insert GrantSupport (GrantSupportID,Title,Description,Logo,IsForProvider,ContentType,Version,CreatedByUserID,UpdatedByUserID,LanguageID)
select @GrantSupportID,@Title,@Description,@Logo,@IsForProvider,@ContentType,0,@CreatedByUserID,@CreatedByUserID,@LanguageID



End





