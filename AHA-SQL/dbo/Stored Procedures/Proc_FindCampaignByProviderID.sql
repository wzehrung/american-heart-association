﻿

CREATE PROC [dbo].[Proc_FindCampaignByProviderID]
(
	 @ProviderID int
	,@LanguageID int
)

As


Begin
set nocount on

declare @CampaignID int

select @CampaignID = CampaignID from Provider where ProviderID = @ProviderID

select C.*,CD.LanguageID,CD.Description,CD.Title,CD.PromotionalTagLine,CD.PartnerLogoVersion,CD.PartnerImageVersion,CD.IsPublished  
,case when Convert(Datetime,Convert(Varchar,getdate(),103),103) between StartDate and  EndDate  and isPublished=1 
then 1 else 0 end As isActive, cd.ResourceTitle, cd.ResourceURL, C.BusinessID, ab.Name AS BusinessName

from Campaign C left join CampaignDetails CD on C.CampaignID=CD.CampaignID 
and LanguageID=@LanguageID  

	LEFT JOIN AdminBusiness ab
		ON c.BusinessID = ab.BusinessID

where C.CampaignID=@CampaignID

End



