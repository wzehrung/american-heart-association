﻿
CREATE  Procedure [dbo].[Proc_UpdateFamilyHistory]
(
 @HVItemGuid uniqueidentifier 
,@HVVersionStamp uniqueidentifier 
,@FamilyMember nvarchar (255)
,@Disease nvarchar (255) 
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Updating Weight table..
*/


Update dbo.FamilyHistory

                    SET 
					 HVVersionStamp =  @HVVersionStamp
				    ,FamilyMember	=  @FamilyMember 
				    ,Disease		=  @Disease 
                    ,UpdatedDate	=  GetDate()
                
                    Where HVItemGuid = @HVItemGuid
End



