﻿CREATE PROC [dbo].[Proc_GetAllAdminMenuByRoleID]

(
@RoleID int
)
As

Begin
set nocount on

SELECT  a.*, [Enabled] FROM AdminMenu a, AdminRolesMenu b
WHERE  b.RoleID = @RoleID
AND a.MenuID = b.MenuID

End
