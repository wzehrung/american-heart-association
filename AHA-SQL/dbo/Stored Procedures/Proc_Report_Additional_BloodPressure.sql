﻿--Proc_Report_Additional_BloodPressure '2009.09.01'
CREATE PROC [dbo].[Proc_Report_Additional_BloodPressure]
(
 @StartDate datetime
,@EndDate datetime
,@ProviderID int
,@campaignID int
)

AS

Begin

declare @startUserHealthrecordIDs table
(
UserHealthrecordID int
primary key (UserHealthrecordID)
)


		if @ProviderID is not null

		insert @startUserHealthrecordIDs
		select  BP.UserHealthrecordID from 
		PatientGroupDetails PGD 
		inner join PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID  and PGD.ProviderID =@providerID
		inner join HealthRecord HR on HR.UserHealthRecordID=PGM.PatientID and (HR.CampaignID=@campaignID or @campaignID is null)
		inner join BloodPressure  BP  on BP.UserHealthrecordID=HR.UserHealthRecordID
		where DateMeasured >=@StartDate and DateMeasured<dateadd(month,1,@StartDate) group by BP.UserHealthrecordID

		else

		insert @startUserHealthrecordIDs
		select  BP.UserHealthrecordID from BloodPressure  BP 
		inner join HealthRecord HR on HR.UserHealthRecordID=BP.UserHealthRecordID and (HR.CampaignID=@campaignID or @campaignID is null)
		where DateMeasured >=@StartDate
		and DateMeasured<dateadd(month,1,@StartDate)
		group by BP.UserHealthrecordID
			

;with BloodPressureIDs As
(
select max(BloodPressureID) BloodPressureID,D.Date1  from BloodPressure BP inner join 
@startUserHealthrecordIDs shi on shi.UserHealthrecordID=BP.UserHealthrecordID
inner join  dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) D on
BP.DateMeasured>=D.Date1 and  BP.DateMeasured<D.Date2 
group by BP.UserHealthrecordID,D.Date1
)

select 

UDR.Date1 

,sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end) Healthy

,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end)*1.0/count(*) end *100 HealthyPerc

,sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) then 1 else 0 end) Acceptable

,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when (Systolic between 120 and 139) or 
(Diastolic between 80 and 89) then 1 else 0 end)*1.0/count(*) end *100 AcceptablePerc

,sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89)
then 0 else Case when (Systolic >= 140 ) or (Diastolic >=90) then 1 else 0 end end) AtRisk


,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89)
then 0 else Case when (Systolic >= 140 ) or (Diastolic >=90) then 1 else 0 end end)
*1.0/count(*) end *100 AtRiskPerc

--,sum(Case when (Systolic between 140 and 159) or (Diastolic between 90 and 99) 
--      then 0 else Case when (Systolic>159) or (Diastolic > 99) then 1 else 0 end end) VeryHighRisk

--  ,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else sum(Case when (Systolic between 140 and 159) or (Diastolic between 90 and 99) 
--      then 0 else Case when (Systolic>159) or (Diastolic > 99) then 1 else 0 end end)*1.0 /count(*) end	* 100 VeryHighRiskPerc 	


,

(select count(*) from @startUserHealthrecordIDs)-
(
sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end)
+sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) then 1 else 0 end)
+sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89)
then 0 else Case when (Systolic >= 140 ) or (Diastolic >=90) then 1 else 0 end end)

)
inactiveUsers


,case when (select count(*) from @startUserHealthrecordIDs) =0 then 0 else
(
(select count(*) from @startUserHealthrecordIDs)-
(
sum(Case when Systolic < 120 and Diastolic <80 then 1 else 0 end)
+sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89) then 1 else 0 end)
+sum(Case when (Systolic between 120 and 139) or (Diastolic between 80 and 89)
then 0 else Case when (Systolic >= 140 ) or (Diastolic >=90) then 1 else 0 end end)
)
)		
*1.0 
/(select count(*) from @startUserHealthrecordIDs) end	*100 inactiveUsersPerc



from dbo.udf_GetDatesWithRange(@StartDate ,@EndDate) UDR left join 

(
select Date1,Systolic,Diastolic from BloodPressure BP   
inner join BloodPressureIDs BID on BP.BloodPressureID=BID.BloodPressureID
)

BP on BP.Date1=UDR.Date1 


group by 	UDR.Date1 


				 
	
End
