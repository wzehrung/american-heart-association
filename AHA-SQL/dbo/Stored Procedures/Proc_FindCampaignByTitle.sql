﻿

CREATE PROC [dbo].[Proc_FindCampaignByTitle]
(
 @Title nvarchar(1024)
,@LanguageID int
)

As


Begin
set nocount on

select c.*,Description from Campaign c inner join CampaignDetails CD on C.CampaignID=CD.CampaignID

where Title=@Title and LanguageID=@LanguageID


End



