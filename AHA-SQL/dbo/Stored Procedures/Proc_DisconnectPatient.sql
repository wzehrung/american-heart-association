﻿

CREATE PROCEDURE [dbo].[Proc_DisconnectPatient]
(
 @ProviderID int
,@PatientId int
)
As
Begin
set nocount on

Begin Try
Begin Transaction

	-- Remove the patient from any of the Provider's groups
	-- Delete any rows in PatientGroupMapping 
	Delete PGM from PatientGroupDetails PGD inner join 
	                PatientGroupMapping PGM  on 
	                PGD.GroupID=PGM.GroupID and PGD.ProviderID=@ProviderID and PGM.PatientID=@PatientId

	-- Remove any alerts the Provider created for the Patient
	Delete A from AlertMapping A inner join 
				  AlertRule B on 
				  A.AlertRuleID=B.AlertRuleID and B.ProviderID=@ProviderID and A.PatientID=@PatientId

	Delete A from Alert A inner join 
	              AlertRule B on 
	              A.AlertRuleID=B.AlertRuleID and B.ProviderID=@ProviderID and A.PatientID=@PatientId


	Delete A from AlertRule A 
	         where ProviderID=@ProviderID and
                   not exists(select * from alertMapping where AlertRuleID=A.AlertRuleID) and
                   not exists(select * from Alert where AlertRuleID=A.AlertRuleID)

	-- Add the Provider/Patient duo to disconnected patients
	Insert DisconnectedPatients(ProviderID,PatientID) select @ProviderID,@PatientID


	-- Remove all messages between the Patient and Provider
	Delete from MessageDetails where UserFolderMappingID in 
		(select UserFolderMappingID from UserFolderMapping where ProviderID=@ProviderID and PatientID=@PatientID)

	delete M  from [Message] M inner join
	(
		select MessageID from MessageDetails where UserFolderMappingID in 
			(select UserFolderMappingID from UserFolderMapping where ProviderID=@ProviderID and PatientID=@PatientID)
	) A on 
	A.MessageID=M.MessageID where not exists (select * from MessageDetails MD where MD.MessageID=M.MessageID)


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK
	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
	
End
