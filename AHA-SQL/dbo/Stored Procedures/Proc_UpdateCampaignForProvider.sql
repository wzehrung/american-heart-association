﻿


CREATE PROC [dbo].[Proc_UpdateCampaignForProvider]
(
 @ProviderID int
,@CampaignID int
)

As

Begin
set nocount on

if (select CampaignID from Provider where ProviderID=@ProviderID) is null
Update Provider set CampaignID=@CampaignID where ProviderID=@ProviderID

End

