﻿
CREATE procedure [dbo].[Proc_InsertExercise]
(
 @UserHealthRecordID int
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@DateofSession Datetime
,@ExerciseType nvarchar(50)  
,@Intensity nvarchar(50) 
,@Duration float
,@Comments nvarchar(max) 
,@NumberOfSteps int
,@ReadingSource nvarchar(100)
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Inserting record in to Exercise table ..
*/

Insert dbo.Exercise	  

				 (
                     UserHealthRecordID
					,HVItemGuid
					,HVVersionStamp
					,DateofSession
					,ExerciseType
					,Intensity
					,Duration
					,Comments
					,NumberOfSteps	
					,ReadingSource				            
                  )
Select               @UserHealthRecordID
					,@HVItemGuid
					,@HVVersionStamp
					,@DateofSession
					,@ExerciseType
					,@Intensity
					,@Duration	
					,@Comments
					,@NumberOfSteps	
					,@ReadingSource
End


