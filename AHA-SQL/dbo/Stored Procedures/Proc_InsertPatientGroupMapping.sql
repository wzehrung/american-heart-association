﻿
CREATE PROCEDURE [dbo].[Proc_InsertPatientGroupMapping]
(
 @GroupID int
,@PatientID int
)
As
Begin
set nocount on

	insert PatientGroupMapping(GroupID,PatientID)
	select @GroupID,@PatientID
	
End


