﻿
CREATE PROC [dbo].[Proc_Polka_GetPatientsForPolkaDataProcessing]

As

Begin


select H.* from HealthRecord H inner join 

(
select distinct UserHealthRecordID from PolkaData  where IsUploadedToHV<>1
) P

on H.UserHealthRecordID=P.UserHealthRecordID 

End
