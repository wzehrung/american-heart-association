﻿

CREATE Procedure [dbo].[Proc_Report_Ethnicity]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
,@PartnerID int = null
)

AS

BEGIN

DECLARE @cols NVARCHAR(MAX)
DECLARE @colsConv NVARCHAR(MAX)

		;WITH EthnicityBase AS
		(
			SELECT
				D.Date1,
				COUNT(DISTINCT E.UserHealthRecordID) EthnicityCount,
				CASE
					WHEN E.Ethnicity IS NULL
						OR E.Ethnicity = ''
					THEN 'Other Race'
					
					ELSE E.Ethnicity
				END Ethnicity
			
			FROM DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D
			
			INNER JOIN ExtendedProfile E
				ON e.CreatedDate BETWEEN D.Date1 AND D.Date2 
			INNER JOIN HealthRecord H
				ON H.UserHealthRecordID=E.UserHealthRecordID

			WHERE isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1)
				--AND isnull(PGD.ProviderID,-1) = isnull(isnull(@PartnerID, PGD.ProviderID),-1)
				
			GROUP BY
				Date1,
				CASE
					WHEN E.Ethnicity IS NULL
						OR E.Ethnicity = ''
					THEN 'Other Race'
					
					ELSE E.Ethnicity
				END
		)
		,EthnicityTypes As
		(
			select 'American Indian or Alaska Native' as EthnicityType,1 rptID
			union all
			select 'Asian' as EthnicityType ,2 rptID
			union all
			select 'Black or African American' as EthnicityType,3 rptID
			union all
			select 'Hispanic or Latino' as EthnicityType ,4 rptID
			union all
			select 'Native Hawaiian or Other Pacific Islander' as EthnicityType ,5 rptID
			union all
			select 'White' as EthnicityType,6 rptID
			union all
			select 'Mixed Race' as EthnicityType,7 rptID
			union all
			select 'Other Race' as EthnicityType,8 rptID
		)
		,EthnicityDates As
		(
			select rptID,D.Date1,E.EthnicityType
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D
				CROSS JOIN EthnicityTypes E
		)
		,Ethnicity As
		(
			select rptID,D.Date1,EB.EthnicityCount,D.EthnicityType Ethnicity
			from EthnicityDates D
				left join EthnicityBase EB
					on D.Date1=EB.date1
					and D.EthnicityType=EB.Ethnicity
		)

		,UserCount As
		(
		select Date1,sum(EthnicityCount) UserCount from Ethnicity group by Date1
		)

		Select rptID,T.Date1,Ethnicity,ISNULL(case when UserCount=0 then 0 else cast(EthnicityCount as real)*100/cast (UserCount as real) end,0) as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Ethnicity T inner join UserCount U on T.Date1=U.Date1

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
								'],[' + t.ColumnIndex
						FROM #ColumnsIndex AS t
						ORDER BY t.IntIndex
						FOR XML PATH('')
					  ), 1, 2, '') + ']'

SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
								'],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
						FROM #ColumnsIndex AS t
						ORDER BY t.IntIndex
						FOR XML PATH('')
					  ), 1, 2, '') + ']'

EXECUTE(N'SELECT Ethnicity, '+ @colsConv +' FROM
(SELECT Ethnicity, ColumnINdex, Perc,rptID FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;')   

END

/************************************************************************************************************************/
