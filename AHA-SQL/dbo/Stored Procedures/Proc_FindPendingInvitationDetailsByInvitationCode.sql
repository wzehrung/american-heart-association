﻿



CREATE PROCEDURE [dbo].[Proc_FindPendingInvitationDetailsByInvitationCode]
(
  @InvitationCode nchar(6)
)
As
Begin
set nocount on


	select PPID.*
	 from  PatientProviderInvitation PPI inner join PatientProviderInvitationDetails PPID
    on PPI.InvitationID = PPID.InvitationID and PPI.InvitationCode=@InvitationCode where status=0

	
End


