﻿






CREATE proc [dbo].[Proc_FindAlertByPatientHealthRecordGUID]
(
 @PatientHealthRecordGUID UNIQUEIDENTIFIER 
,@ProviderID int
)

AS

Begin
set nocount on

select DISTINCT C.AlertID, C.PatientID, IsDismissed
,CAST(AlertXml AS NVARCHAR(MAX)) AlertXml, CAST(AlertRuleXml AS  NVARCHAR(MAX)) AlertData, C.CreatedDate,D.type,Notes  
,ItemEffectiveDate 
FROM
Alert C inner join AlertRule D on C.AlertRuleId=D.AlertRuleID 
inner join Healthrecord H on H.UserHealthRecordID= C.PatientID
AND H.UserHealthRecordGUID=@PatientHealthRecordGUID
and D.ProviderID=@ProviderID-- and D.isActive=1

order by ItemEffectiveDate desc


End






