﻿
CREATE Procedure [dbo].[Proc_InsertFamilyHistory]
(
 @UserHealthRecordID int 
,@HVItemGuid uniqueidentifier 
,@HVVersionStamp uniqueidentifier 
,@FamilyMember nvarchar (255) 
,@Disease nvarchar (255)
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a record to FamilyHistory table..
*/


Insert dbo.FamilyHistory 

					(
                        UserHealthRecordID
                       ,HVItemGuid
                       ,HVVersionStamp
                       ,FamilyMember
                       ,Disease                       
                     )
Select                  @UserHealthRecordID
                       ,@HVItemGuid
                       ,@HVVersionStamp
                       ,@FamilyMember
                       ,@Disease
                       
End





