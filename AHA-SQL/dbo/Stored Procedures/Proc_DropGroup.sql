﻿CREATE PROCEDURE [dbo].[Proc_DropGroup]
(
 @GroupID int
)
As
Begin
set nocount on

Begin Try
Begin Transaction

	

	delete A from Alert A inner join AlertMapping B on A.AlertRuleID=B.AlertRuleID
	AND A.PatientID=B.PatientID
	and B.GroupID=@GroupID 

	declare @ids table
	(
	AlertRuleID int
	)
	insert @ids select distinct AlertRuleID from AlertMapping  where GroupID=@GroupID 

	delete from AlertMapping  where GroupID=@GroupID 

	Delete FROM AlertRule where AlertRuleID in (select AlertRuleID from @ids)
	
	Delete FROM AlertRule where GroupID=@GroupID

	exec Proc_DeletePatientGroupMapping @GroupID
	exec Proc_DeletePatientGroupDetails @GroupID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
	
End
