﻿CREATE Procedure [dbo].[Proc_Report_Age_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@PartnerID int=null
)

As

Begin
DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

		;With AgeBase As
		(
			select D.Date1,count(distinct AH.UserHealthRecordID)  AgeCount
			,CASE  WHEN (year(getdate())-YearOfBirth ) IS NULL  THEN 'Unreported'
			WHEN (year(getdate())-YearOfBirth ) <25  THEN 'Under 25'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 25 AND 34 THEN '25-34'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 35 AND 44 THEN '35-44'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 45 AND 54 THEN '45-54'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 55 AND 64 THEN '55-64'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 65 AND 74 THEN '65-74'
			WHEN (year(getdate())-YearOfBirth ) >=75  THEN '75 or older' END AGE

			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D 
			inner join    
			AHAUserHealthRecord AH on AH.CreatedDate<D.Date2 
			left join  ExtendedProfile E ON E.UserHealthRecordID=AH.UserHealthRecordID  
			inner join HealthRecord H on H.UserHealthRecordID=AH.UserHealthRecordID  
			AND CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))

			left join PatientGroupMapping PGM on PGM.PatientID = AH.UserHealthRecordID
			left join PatientGroupDetails PGD on PGD.GroupID = PGM.GroupID
			
			
			AND isnull(PGD.ProviderID,-1) = isnull(isnull(@PartnerID, PGD.ProviderID),-1)
			GROUP BY Date1,CASE  WHEN (year(getdate())-YearOfBirth ) IS NULL  THEN 'Unreported'
			WHEN (year(getdate())-YearOfBirth ) <25  THEN 'Under 25'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 25 AND 34 THEN '25-34'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 35 AND 44 THEN '35-44'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 45 AND 54 THEN '45-54'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 55 AND 64 THEN '55-64'
			WHEN (year(getdate())-YearOfBirth ) BETWEEN 65 AND 74 THEN '65-74'
			WHEN (year(getdate())-YearOfBirth ) >=75  THEN '75 or older' END
		)
		,AgeTypes As
		(


		select 'Under 25' as AgeType,1 rptID
		union all
		select '25-34' as AgeType,2 rptID
		union all
		select '35-44' as AgeType,3 rptID
		union all
		select '45-54' as AgeType,4 rptID
		union all
		select '55-64' as AgeType,5 rptID
		union all
		select '65-74' as AgeType,6 rptID
		union all
		select '75 or older' as AgeType,7 rptID
		union all
		select 'Unreported' as AgeType,8 rptID
		)
		,AgeDates As
		(
		select D.Date1,G.AgeType,rptID   from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D cross join AgeTypes G
		)
		,Age As
		(
		 select rptID,D.Date1,GB.AgeCount,D.AgeType Age  from AgeDates D left join AgeBase GB on 
		 D.Date1=GB.date1 and D.AgeType=GB.Age
		)

--		,UserCount As
--		(
--		select D.Date,COUNT(UserHealthRecordID) UserCount
--		from  DBO.udf_GetDates (@StartDate,@EndDate) D left join   AHAUserHealthRecord AH 
--		on AH.CreatedDate<D.Date GROUP BY Date
--		)
----		,ToBePivoted As
----		(
		Select rptID,T.Date1,Age,isnull(CASE WHEN T.Date1 >=@StartDate and T.Date1 <= @EndDate and AgeCount is null then 0 else AgeCount end,0) AgeCount
			 --UserCount--case when UserCount=0 then 0 else cast(GenderCount as real)*100/cast (UserCount as real) end as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Age T --inner join UserCount U on T.Date=U.Date
----		)

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted


SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'


SET @query = N'SELECT Age, '+ @cols +' FROM
(SELECT Age, ColumnINdex, AgeCounT,rptID FROM #ToBePivoted ) A
PIVOT (SUM(AgeCount) FOR ColumnIndex IN ( '+ @cols +' )) B ORDER BY rptID;'

EXECUTE(@query)

End

/************************************************************************************************************************/
