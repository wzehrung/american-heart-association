﻿
CREATE PROCEDURE [dbo].[Proc_FindProviderByUserNameandPassword]
(
 @UserName nvarchar(255)
,@Password nvarchar(255)
)
As
Begin

set nocount on

IF (SELECT CampaignID FROM Provider WHERE UserName = @UserName) IS NOT NULL 

SELECT p.*,l.LanguageLocale, c.URL AS CampaignURL FROM Provider p
	INNER JOIN Languages l ON p.DefaultLanguageID = l.LanguageID
	INNER JOIN Campaign c ON c.CampaignID = p.CampaignID
WHERE UserName=@UserName AND Password=@Password --COLLATE SQL_Latin1_General_CP1_CS_AS

ELSE

SELECT p.*,l.LanguageLocale, '' AS CampaignURL FROM Provider p
	INNER JOIN Languages l ON p.DefaultLanguageID = l.LanguageID
WHERE UserName=@UserName AND Password=@Password --COLLATE SQL_Latin1_General_CP1_CS_AS

END

/* ########################## */
