﻿



CREATE PROCEDURE [dbo].[Proc_RemovePatientFromGroup]
(
 @GroupID int
,@PatientID INT
)
As
Begin

Begin Try
Begin Transaction

	delete from PatientGroupMapping where GroupID=@GroupID AND PatientID=@PatientID

DECLARE @AlertRuleids table
(
AlertRuleID int
)
insert @AlertRuleids
select distinct AlertRuleID from AlertMapping   where GroupID=@GroupID AND PatientID=@PatientID


	
	delete from AlertMapping   where GroupID=@GroupID AND PatientID=@PatientID 

	delete A from Alert A inner join @AlertRuleids B on A.AlertRuleID=B.AlertRuleID	AND PatientID=@PatientID

	--delete A from AlertRule A inner join @AlertRuleids B on A.AlertRuleID=B.AlertRuleID


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End



