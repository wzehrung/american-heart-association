﻿
CREATE Procedure Proc_GetWizardDetails
(
 @PersonalItemGUID uniqueidentifier 
)
AS

Begin

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (BloodPressure table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

Declare @UserHealthRecordID int

select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID

select WizardStepIndex  from dbo.ExtendedProfile
							 where UserHealthRecordID=@UserHealthRecordID



End
