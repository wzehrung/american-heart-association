﻿CREATE PROC [dbo].[Proc_UpdateAdminMenu]
(
 @RoleID int
,@MenuID int
,@Enabled BIT
)

As

Begin
set nocount on

if exists(select 1 from adminrolesmenu where roleid = @roleid and menuid = @menuid)
begin
  Update AdminRolesMenu set 
						 Enabled = @Enabled
  where  roleid = @roleid and menuid = @menuid
end
else
begin
    insert into adminrolesmenu (RoleID, MenuID, Enabled) VALUES (@Roleid, @MenuID, @Enabled)
end
End
