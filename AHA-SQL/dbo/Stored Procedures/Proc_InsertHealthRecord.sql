﻿
CREATE Procedure Proc_InsertHealthRecord
(
 @UserHealthRecordGUID uniqueidentifier  = NULL
,@PersonalItemGUID uniqueidentifier  
,@UserHealthRecordID int OutPut
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :inserting record to Health Record table
*/
set nocount on

declare @PolkaID NVARCHAR(6) 
EXEC GeneratePolkaID @PolkaID OUTPUT

Insert dbo.HealthRecord 
					(
					 UserHealthRecordGUID
					,PersonalItemGUID
					,PolkaID
					)
Select              
					 @UserHealthRecordGUID
					,@PersonalItemGUID
					,@PolkaID

SET @UserHealthRecordID= SCOPE_IDENTITY()

insert UserFolderMapping(PatientID,FolderID)
select @UserHealthRecordID,FolderID from Folder


End
