﻿
CREATE PROC [dbo].[Proc_UpdateCampaignForPatient]
(
 @PersonalItemGUID uniqueidentifier
,@CampaignID int
)

As

Begin
set nocount on

if (select CampaignID from HealthRecord where PersonalItemGUID=@PersonalItemGUID) is null
Update HealthRecord set CampaignID=@CampaignID where PersonalItemGUID=@PersonalItemGUID

End


