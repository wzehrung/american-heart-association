﻿


-- =============================================
-- Author:		Scott Shipp
-- Create date: January 30, 2009
-- Description:	This is a snapshot of the number
-- of users tracking exercise.
-- 
-- =============================================
CREATE PROCEDURE [dbo].[Proc_RptUsersTrackingCholesterol]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select cast ( (select cast(COUNT(distinct UserHealthRecordID) as real) from Cholesterol) / (select cast(count(UserID) as real) from AHAUserHealthRecord) * 100 as numeric(10,2)) as PercentTrackingCholesterol


END



