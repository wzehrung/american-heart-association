﻿

CREATE Procedure [dbo].[Proc_ModifyAHAUser_IsPersonalInfoCompleted]  
(  
 @PersonGUID uniqueidentifier
,@IsPersonalInfoCompleted bit
)
AS  
  
Begin  

/* Author			:Shipin Anand  
   Created Date     :2008-07-04    
   Description      :Updating IsPersonalInfoCompleted Column of AHAUser for a PersonGUID  

   Note : Please dont try to update a indexed foreign key 
*/  

set nocount on  

Update dbo.AHAUser  
                SET IsPersonalInfoCompleted =@IsPersonalInfoCompleted
		where PersonGUID=@PersonGUID 
End



