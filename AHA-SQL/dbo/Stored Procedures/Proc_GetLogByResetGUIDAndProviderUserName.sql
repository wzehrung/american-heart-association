﻿

CREATE PROCEDURE [dbo].[Proc_GetLogByResetGUIDAndProviderUserName]
(
 @ResetGUID uniqueidentifier
,@UserName nvarchar(255)
)
As
Begin
set nocount on

	select R.* from ResetPasswordLog R inner join Provider P
    on  R.ProviderID=P.ProviderID and P.UserName=@UserName
    and R.ResetGUID=@ResetGUID
	
End




