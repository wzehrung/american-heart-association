﻿


-- =============================================
-- Author:		Scott Shipp
-- Create date: January 30, 2009
-- Description:	This is a snapshot of the number
-- of users per gender.
-- 
-- =============================================
CREATE PROCEDURE [dbo].[Proc_RptBreakdownByGender]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select 'Female' as Gender, cast( (select cast(count(gender) as real) from ExtendedProfile where gender='Female' and UserHealthRecordID in (Select UserHealthRecordID from AHAUserHealthRecord)) / (select cast(count(UserHealthRecordID) as real) from AHAUserHealthRecord) *100 as numeric(10,2)) as Percentage Union

	select 'Male' as Gender, cast( (select cast(count(gender) as real) from ExtendedProfile where gender='Male' and UserHealthRecordID in (Select UserHealthRecordID from AHAUserHealthRecord)) / (select cast(count(UserHealthRecordID) as real) from AHAUserHealthRecord) *100 as numeric(10,2)) as Percentage


END



