﻿

CREATE Procedure [dbo].[Proc_InsertWeight]
(
 @UserHealthRecordID int = NULL
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Weight float 
,@ReadingSource nvarchar(100)
,@Comments nvarchar(max) 
,@DateMeasured Datetime 
)
AS



Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Insert a record to Weight table..
*/

Insert dbo.Weight (
					 UserHealthRecordID 
					,HVItemGuid  
					,HVVersionStamp 
					,Weight
					,ReadingSource
					,Comments 
					,DateMeasured 
				   )
Select           
					 @UserHealthRecordID 
					,@HVItemGuid  
					,@HVVersionStamp 
					,@Weight 
					,@ReadingSource
					,@Comments 
					,@DateMeasured 
End





