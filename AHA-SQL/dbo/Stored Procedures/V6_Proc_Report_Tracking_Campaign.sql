﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 6/13/2013
-- Description:	Refactoring of Proc_Report_Tracking_Campaign to handle Campaign/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Tracking_Campaign] 
(
	@StartDate Datetime,  
	@EndDate Datetime,
	@CampaignID int = null,	-- null specifies all campaigns
	@Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
	@CampaignIDs nvarchar(max)
)
AS
BEGIN

	DECLARE @cols NVARCHAR(MAX)
	DECLARE @colsConv NVARCHAR(MAX)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int
	
	SELECT Zip_Code INTO #RptMarketZipCodes 
	from ZipRegionMapping 
	where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 
 
	--
	-- Now generate the report
	--
			select * into #h from	
				(select UserID,AHR.UserHealthRecordID
					from  AHAUserHealthRecord AHR
					inner join HealthRecord H on H.UserHealthRecordID=AHR.UserHealthRecordID
					left JOIN ExtendedProfile EP ON EP.UserHealthRecordID = H.UserHealthRecordID
					/* WZ - Modified 2.3.2014 to correct campaign logic (if a null campaign ID is passed get everyone, otherwise
			just get those is specified */
					--where ((@CampaignId IS NULL AND H.CampaignID IS NOT NULL) OR (@CampaignId IS NOT NULL AND @CampaignId = H.CampaignID)) AND
					--	  (@ZipCodeCount = 0 or EP.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))

			--  NEW WAY:
			WHERE
			@CampaignIDs IS NULL AND H.CampaignID IS NOT NULL
			OR (H.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or EP.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AHR.UserID IS NOT NULL)
				) a

		;With Tracker As
		(
			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Weight' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Weight W 
			on W.CreatedDate<D.Date2 
			
			left join #h H on W.UserHealthRecordID=H.UserHealthRecordID
			
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Blood Pressure' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodPressure BP 
			on BP.CreatedDate<D.Date2 
			left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Physical Activity' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Exercise E
			on E.CreatedDate<D.Date2
			
			left join #h H on E.UserHealthRecordID=H.UserHealthRecordID
			
			GROUP BY Date1


			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Medication' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Medication M
			on M.CreatedDate<D.Date2 
			left join #h H on M.UserHealthRecordID=H.UserHealthRecordID
				
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Cholesterol' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Cholesterol C 
			on C.CreatedDate<D.Date2 
			left join #h  H on C.UserHealthRecordID=H.UserHealthRecordID
			
			GROUP BY Date1

			union all

			select D.Date1,COUNT(distinct H.UserHealthRecordID) TrackerCount,'Blood Glucose' Tracker
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodGlucose BG 
			on BG.CreatedDate<D.Date2 
			left join #h H on BG.UserHealthRecordID=H.UserHealthRecordID
			GROUP BY Date1 
		)
		,

		UserCountP As

		(
		select D.Date1,H.UserHealthRecordID 
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Weight W 
			on W.CreatedDate<D.Date2 
			
			left join #h H on W.UserHealthRecordID=H.UserHealthRecordID

			union 

			select D.Date1,H.UserHealthRecordID 
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodPressure BP 
			on BP.CreatedDate<D.Date2 
			left join #h H on BP.UserHealthRecordID=H.UserHealthRecordID

			union 

			select D.Date1,H.UserHealthRecordID 
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Exercise E
			on E.CreatedDate<D.Date2
			
			left join #h H on E.UserHealthRecordID=H.UserHealthRecordID

			union 

			select D.Date1,H.UserHealthRecordID
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D 
			left join   Medication M on M.CreatedDate<D.Date2 
			left join #h H on M.UserHealthRecordID=H.UserHealthRecordID

			union 

			select D.Date1,H.UserHealthRecordID
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   Cholesterol C 
			on C.CreatedDate<D.Date2 
			left join #h H on C.UserHealthRecordID=H.UserHealthRecordID
			
			union 

			select D.Date1,H.UserHealthRecordID
			from  DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D left join   BloodGlucose BG 
			on BG.CreatedDate<D.Date2 
			left join #h H on BG.UserHealthRecordID=H.UserHealthRecordID 		
		)
		,
		UserCount As

		(
		select Date1,count(distinct UserHealthRecordID) UserCount  from UserCountP group by Date1
		)

		Select T.Date1,Tracker,case when UserCount=0 then 0 else cast(TrackerCount as real)*100/cast (UserCount as real) end as Perc
			   ,'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
				,cast(dense_Rank() over (order by T.Date1) as int) IntIndex
		into #ToBePivoted
		From Tracker T inner join UserCount U on T.Date1=U.Date1

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted

SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

SELECT  @colsConv = STUFF(( SELECT TOP 100 PERCENT
                                '],cast(cast(ISNULL([' + t.ColumnIndex + '],0) as numeric(15,2)) as varchar(20)) + ''%''' + ' as  [' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'

EXECUTE (N'SELECT Tracker, '+ @colsconv +' FROM
(SELECT Tracker, ColumnINdex, Perc FROM #ToBePivoted ) A
PIVOT (SUM(Perc) FOR ColumnIndex IN ( '+ @cols +' )) B;')
	
END

