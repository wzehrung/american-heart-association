﻿
CREATE PROC [dbo].[Proc_DoesCampaignExists_Url]  
(  
 @Url nvarchar(2048) 
,@isValid bit output  
)  
  
As  
  
  
Begin  
set nocount on  
  
--if exists (select * from Campaign  C inner join CampaignDetails CD on C.CampaignID=CD.CampaignID 
	--		where Url=@Url and LanguageID=@LanguageID) 
	if exists (select * from Campaign  where Url=@Url)  
			
set @isValid=1  
else  
set @isValid=0  
End 

