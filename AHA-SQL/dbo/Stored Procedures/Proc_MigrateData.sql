﻿

--Proc_MigrateData 'Dbserv','LinkedServerUser','123456','AHAOriginal'

CREATE procedure [dbo].[Proc_MigrateData]
(
 @ServerName nvarchar(500)
,@UserName nvarchar(500)
,@PassWord nvarchar(500)
,@SourceDataBaseName nvarchar(500)
)

AS

Begin

Begin Try
Begin Transaction

Declare @statement nvarchar(Max)

		SET @statement = N'
		ALTER Procedure [dbo].[Proc_DataMigration]
		As
		Begin

		Begin Try
		Begin Transaction


				select * into  DeprecatedProfile 
				from OPENROWSET(
								''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.Profile
								) 

				select * into DeprecatedZips 
				from OPENROWSET(
								''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.Zips
								) 

				select * into DeprecatedWeight 
				from OPENROWSET(
								''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.Weight
								) 

				select * into DeprecatedPhysicalActivity 
				from OPENROWSET(
								''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.PhysicalActivity
								) 


				select * into DeprecatedIncident 
				from OPENROWSET(
								''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.Incident
								) 

				select * into DeprecatedExercise 
				from OPENROWSET(
								''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.Exercise 
								) 

				select * into DeprecatedBloodPressure 
				from OPENROWSET(
								''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.BloodPressure
								) 




		Declare @HealthRecordProfileMapping table
		(
		 GUID UniqueIdentifier
		,UserHealthRecordID int
		,ProfileID int
		)

				Insert HealthRecord 
				( 
				 PersonalItemGUID
				,CreatedDate
				,UpdatedDate
				,IsMigrated
				)

				select	 GUID
						,LastUpdated
						,LastUpdated 
						,1

				from	OPENROWSET(
									''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
									DataBaseName.dbo.Profile
									) AS A





		insert @HealthRecordProfileMapping

		select B.GUID,A.UserHealthRecordID,B.ProfileID

			 from HealthRecord A         
			 INNER JOIN
			 OPENROWSET(''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
						 DataBaseName.dbo.Profile
						) AS  B 
			 on A.PersonalItemGUID=B.GUID 


				Insert ExtendedProfile 
				(
				 UserHealthRecordID
				,ZipCode
				,[Country/Region]
				,DateOfBirth
				,Gender
				,Ethnicity
				,FamilyHistoryHD
				,FamilyHistoryStroke
				,HighBPDiet
				,HighBPMedication
				,KidneyDisease
				,DiabetesDiagnosis
				,HeartDiagnosis	
				,Stroke	
				,PhysicalActivityLevel
				,CreatedDate
				,UpdatedDate
				)


				select 

				 A.UserHealthRecordID 
				,B.Zip					
				,B.Country				
				,B.DateOfBirth			
				,B.Gender					
				,B.Ethnicity				
				,B.FamilyHistoryHD		
				,B.FamilyHistoryStroke	
				,B.HBPDiet				
				,B.HBPMedication			
				,B.KidneyDiagnosis		
				,B.DiabetesDiagnosis		
				,B.HeartDiagnosis				
				,B.Stroke					
				,B.PhysicalActivity	
				,B.LastUpdated
				,B.LastUpdated	

				from  HealthRecord A          
					 INNER JOIN
					 OPENROWSET(''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.Profile
								) AS  B 
				 on A.PersonalItemGUID=B.GUID 




		Insert  BloodPressure    
		(
		 UserHealthRecordID
		,HVItemGuid
		,DateMeasured
		,Diastolic
		,Systolic
		,Comments
		,CreatedDate
		,UpdatedDate
		)

		select 

		 B.UserHealthRecordID 
		,A.GUID
		,A.BPDate
		,A.Diastolic
		,A.Systolic
		,A.Notes
		,A.LastUpdated
		,A.LastUpdated

		from  OPENROWSET(''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
						DataBaseName.dbo.BloodPressure
						) AS A   
						INNER JOIN 
				@HealthRecordProfileMapping B

		on A.ProfileID = B.ProfileID


				Insert Weight     
				(
				 UserHealthRecordID
				,HVItemGuid
				,DateMeasured
				,Weight
				,Comments
				,CreatedDate
				,UpdatedDate
				)

				Select 
						
				 B.UserHealthRecordID 
				,A.GUID
				,A.WeightDate
				,A.Weight
				,A.Notes
				,A.LastUpdated
				,A.LastUpdated


				from OPENROWSET(''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
								DataBaseName.dbo.Weight
								) AS A  
				INNER JOIN @HealthRecordProfileMapping B
				on A.ProfileID = B.ProfileID



		Insert Exercise    
		(
		 UserHealthRecordID
		,HVItemGuid
		,DateofSession
		,ExerciseType
		,Duration
		,Comments
		,CreatedDate
		,UpdatedDate
		)

		select 

		 B.UserHealthRecordID 
		,A.GUID
		,A.ExerciseDate
		,A.ExerciseType
		,A.Duration
		,A.Notes
		,A.LastUpdated
		,A.LastUpdated

		from OPENROWSET(''MSDASQL'',''DRIVER={SQL Server};SERVER=ServerName;UID=UserName;PWD=PassWord'',
						DataBaseName.dbo.Exercise
						) AS A   -- Source
		INNER JOIN @HealthRecordProfileMapping B
		on A.ProfileID = B.ProfileID


		COMMIT
		End Try

		Begin Catch

		  If @@TRANCOUNT > 0
			ROLLBACK
			Declare @ErrMsg nvarchar(4000), @ErrSeverity int
			Select  @ErrMsg = ERROR_MESSAGE(),
				  @ErrSeverity = ERROR_SEVERITY()
			Raiserror(@ErrMsg, @ErrSeverity, 1)

		 End Catch
		End




		' 


			IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Proc_DataMigration]') AND type in (N'P', N'PC'))
			BEGIN
			SET @statement =
					replace
							(
							 replace
									(
									replace (

											 replace (
														 replace(@statement,'UserName',@UserName)
														,'PassWord',@PassWord
													   )
											,'ServerName',@ServerName
											)
									,'DataBaseName',@SourceDataBaseName
									)		
							,'ALTER','CREATE'
							)
					EXEC dbo.sp_executesql @statement
			END
			ELSE
			BEGIN
			SET @statement =
					 replace
							(
							replace (

									 replace (
												 replace(@statement,'UserName',@UserName)
												,'PassWord',@PassWord
											   )
									,'ServerName',@ServerName
									)
							,'DataBaseName',@SourceDataBaseName
							)	

					EXEC dbo.sp_executesql @statement	
			END


Exec Proc_DataMigration

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End



