﻿
CREATE PROC [dbo].[Proc_DisableAdminUser]
(
 @AdminUserID int
)

As

Begin
set nocount on

Update AdminUser set 
						 IsActive = 0
					
where  AdminUserID = @AdminUserID

End