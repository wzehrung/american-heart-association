﻿CREATE PROCEDURE [dbo].[Proc_GetInvitationByAcceptedProviderID]
(
  @AcceptedProviderID int
 ,@Status tinyint
)
As
Begin
set nocount on


	select PPI.*
	 from  PatientProviderInvitation PPI inner join PatientProviderInvitationDetails PPID
    on PPI.InvitationID = PPID.InvitationID and PPID.AcceptedProviderID=@AcceptedProviderID 
	and DateAccepted is null and status =@status
	ORDER BY DateSent DESC
End
