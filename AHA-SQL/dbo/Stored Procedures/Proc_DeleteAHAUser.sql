﻿


CREATE Procedure [dbo].[Proc_DeleteAHAUser]
(
 @PersonGUID uniqueidentifier
)
AS
Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :Delete AHAUser Record .. returns selfrecordid and userid
*/


Delete from dbo.AHAUser Where PersonGUID= @PersonGUID 	

End

