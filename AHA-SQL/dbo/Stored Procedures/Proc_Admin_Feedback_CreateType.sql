﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.17.2014
-- Description:	Create feedback types used in the feedback form on the patient portal
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_CreateType]
(
    @FeedbackTypeName nvarchar(100),
    @Enabled bit
)

AS

BEGIN
SET NOCOUNT ON

INSERT INTO FeedbackTypes (FeedbackTypeName, [Enabled])
VALUES(@FeedbackTypeName, @Enabled)

END