﻿CREATE proc [dbo].Proc_Admin_Patient_FindByLogin
(
@login nvarchar(50)
)
AS
Begin
set nocount on
select a.CreatedDate, a.UpdatedDate, a.NewTermsAndConditions as TermsAccepted, a.TermsAcceptanceDate,
b.PatientID, b.FirstName, b.LastName, b.EmailAddress from dbo.AHAUser a, dbo.HVData b
where a.SelfHealthRecordID = b.PatientID
and b.EmailAddress like @login
order by b.LastName, b.FirstName
End