﻿CREATE PROC [dbo].[Proc_GetContentsExpiringOn]
(
 @ExpirationDate DATETIME
)
As

Begin
set nocount on

select * from  [CONTENT] where Convert(datetime,Convert(varchar,ExpirationDate,103),103)=@ExpirationDate 

End