﻿
CREATE PROCEDURE [dbo].[Proc_FindProviderByID]
(
@ProviderID INT
)
AS
BEGIN
SET NOCOUNT ON

IF (SELECT CampaignID FROM Provider WHERE ProviderID = @ProviderID) IS NOT NULL

SELECT p.*,l.LanguageLocale, c.URL AS CampaignURL FROM Provider p
	INNER JOIN Languages l ON p.DefaultLanguageID = l.LanguageID
	INNER JOIN Campaign c ON c.CampaignID = p.CampaignID
WHERE p.ProviderID = @ProviderID

	ELSE
	
SELECT p.*,l.LanguageLocale, '' AS CampaignURL FROM Provider p
	INNER JOIN Languages l ON p.DefaultLanguageID = l.LanguageID

WHERE p.ProviderID = @ProviderID

END
