﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.17.2014
-- Description:	Update feedback types used in the feedback form on the patient portal
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_UpdateType]
(
    @FeedbackTypeName nvarchar(100),
    @FeedbackTypeID int
)

AS

BEGIN
SET NOCOUNT ON

UPDATE FeedbackTypes
SET
    FeedbackTypeName = @FeedbackTypeName
WHERE FeedbackTypeID = @FeedbackTypeID

END