﻿CREATE PROCEDURE [dbo].[Proc_IsMessagingEnabledForPatient]
(
 @PatientID int
,@Result bit output
)
As
Begin
set nocount on

	select distinct PR.* from Provider PR inner join
	PatientGroupDetails PGD on PR.ProviderID=PGD.ProviderID
	inner join
	PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID and PGM.PatientID=@PatientID

	where AllowPatientsToSendMessages=1

	IF(@@ROWCOUNT>0)
		SET @Result = 1
	ELSE
		SET @Result = 0
	
	
End
