﻿
CREATE Procedure [dbo].[Proc_InsertPatientSMSReminderSchedule]
(
 @ReminderID Int
,@Message varchar(160)
,@Monday bit
,@Tuesday bit
,@Wednesday bit
,@Thursday bit
,@Friday bit
,@Saturday bit
,@Sunday bit
,@Time varchar(5)
,@DisplayTime varchar (10)
,@ReminderCategoryID Int
,@ReminderScheduleID Int OUTPUT
)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on


Insert [3CiPatientReminderSchedule] 
					(
					 ReminderID
					,Message					
					,Monday
					,Tuesday
					,Wednesday
					,Thursday
					,Friday
					,Saturday
					,Sunday
					,Time
					,DisplayTime
					,ReminderCategoryID
					,[Status]
					,CreatedDateTime
					,UpdatedDateTime
					)
Select              
					 @ReminderID
					,@Message
					,@Monday
					,@Tuesday
					,@Wednesday
					,@Thursday
					,@Friday
					,@Saturday
					,@Sunday
					,@Time
					,@DisplayTime
					,@ReminderCategoryID
					,1
					,getdate()
					,getdate()

SET @ReminderScheduleID = SCOPE_IDENTITY()


End
