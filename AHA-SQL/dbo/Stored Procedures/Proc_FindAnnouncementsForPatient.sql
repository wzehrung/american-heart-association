﻿

CREATE PROCEDURE [dbo].[Proc_FindAnnouncementsForPatient]
(
--@IsProvider bit,
@LanguageID int,
@PatientID int
)
As
Begin
set nocount on

SELECT DISTINCT
      pc.ContentTitle AS AnnouncementTitle
      ,pc.ContentText AS AnnouncementText
      ,p.Salutation + ' ' + p.FirstName + ' ' + p.LastName AS ProviderName
      ,pc.LanguageID
      --,IsProvider = NULL
      ,pgm.PatientID
      

      FROM PatientContent pc
      
            INNER JOIN Provider p
                  ON pc.ProviderID = p.ProviderID
            
            INNER JOIN PatientGroupDetails pgd
                  ON p.ProviderID = pgd.ProviderID
                  
            INNER JOIN PatientGroupMapping pgm 
                  ON pgd.GroupID=PGM.GroupID AND pgm.PatientID=@PatientID
                                          
      WHERE LanguageID=@LanguageID 
      
         AND pc.ProviderID IN 
         (
            SELECT DISTINCT pgd.ProviderID 
                  FROM PatientGroupDetails pgd 

            )

END



