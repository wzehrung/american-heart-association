﻿

-- =============================================
-- Author:		Wade Zehrung
-- Create date:	03.20.2014
-- Description:	Set FeedbackRecipient bit in AdminUser table
-- =============================================

CREATE PROC [dbo].[Proc_Admin_Feedback_AddRecipients]
(
    @AdminUserID int,
    @Recipient bit
)

AS

BEGIN
SET NOCOUNT ON

UPDATE AdminUser
SET FeedbackRecipient = @Recipient

WHERE AdminUserID = @AdminUserID

END