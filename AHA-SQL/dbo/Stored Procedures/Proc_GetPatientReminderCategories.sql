﻿
CREATE Procedure [dbo].[Proc_GetPatientReminderCategories]

AS

Begin

--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/

SELECT ReminderCategoryID, Name, TriggerID FROM [3CiPatientReminderCategory]


End
