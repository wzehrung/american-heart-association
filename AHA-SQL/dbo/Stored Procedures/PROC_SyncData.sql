﻿

CREATE PROC [dbo].[PROC_SyncData]
(
 @SyncDataXml xml
 ,@UpdateLastSyncDate bit
,@isSynched bit output
,@phonenumber varchar(50)
,@zipCode varchar(50)
)
AS

BEGIN

set nocount on

Begin Transaction
Begin Try


Declare @UserHealthRecordID int

select @UserHealthRecordID =UserHealthRecordID from HealthRecord
where PersonalItemGUID=

(
select
PersonalItemGUID.value('.','UniqueIdentifier') 
from @SyncDataXml.nodes('/AHASyncData/PersonalItemGUID') A(PersonalItemGUID)
)


--Extentded Profile 

		Declare @ExtendedProfile table
		(
			 UserHealthRecordID int
			,FamilyHistoryHD bit
			,FamilyHistoryStroke bit
			,HighBPDiet bit
			,HighBPMedication bit
			,KidneyDisease bit
			,DiabetesDiagnosis bit
			,HeartDiagnosis bit
			,Stroke bit
			,PhysicalActivityLevel  nvarchar(100)
			,BloodType nvarchar(100)
			,MedicationAllergy nvarchar(max)
			,TravelTimeToWorkInMinutes float
			,MeansOfTransportation  nvarchar(255)
			,AttitudesTowardsHealthcare nvarchar(100)
			,DoYouSmoke bit
			,HaveYouEverTriedToQuitSmoking bit
			,[Country/Region]  nvarchar(50)
			,Gender nvarchar(10)
			,Ethnicity  nvarchar(50)
			,MaritalStatus nvarchar(50)
			,YearOfBirth INT
			,isUpdate bit
		)



		;With ExtendedProfileT as
		(
			select 

			@UserHealthRecordID UserHealthRecordID
			,CASE WHEN ExtendedProfile.value('(HasFamilyHeartDiseaseHistory)[1]','nvarchar(MAX)')='' THEN NULL ELSE ExtendedProfile.value('(HasFamilyHeartDiseaseHistory)[1]','nvarchar(MAX)') END  HasFamilyHeartDiseaseHistory
			,CASE WHEN ExtendedProfile.value('(HasFamilyStrokeHistory)[1]','nvarchar(MAX)') ='' THEN NULL ELSE  ExtendedProfile.value('(HasFamilyStrokeHistory)[1]','nvarchar(MAX)') END HasFamilyStrokeHistory
			,CASE WHEN ExtendedProfile.value('(IsOnHypertensionDiet)[1]','nvarchar(MAX)') ='' THEN NULL ELSE ExtendedProfile.value('(IsOnHypertensionDiet)[1]','nvarchar(MAX)') END IsOnHypertensionDiet
			,CASE WHEN ExtendedProfile.value('(IsOnHypertensionMedication)[1]','nvarchar(MAX)') ='' THEN NULL ELSE ExtendedProfile.value('(IsOnHypertensionMedication)[1]','nvarchar(MAX)')END IsOnHypertensionMedication
			,CASE WHEN ExtendedProfile.value('(HasRenalFailureBeenDiagnosed)[1]','nvarchar(MAX)') ='' THEN NULL ELSE ExtendedProfile.value('(HasRenalFailureBeenDiagnosed)[1]','nvarchar(MAX)') END HasRenalFailureBeenDiagnosed
			,CASE WHEN ExtendedProfile.value('(HasDiabetesBeenDiagnosed)[1]','nvarchar(MAX)')='' THEN NULL ELSE  ExtendedProfile.value('(HasDiabetesBeenDiagnosed)[1]','nvarchar(MAX)') END HasDiabetesBeenDiagnosed
			,CASE WHEN ExtendedProfile.value('(HasPersonalHeartDiseaseHistory)[1]','nvarchar(MAX)')='' THEN NULL ELSE ExtendedProfile.value('(HasPersonalHeartDiseaseHistory)[1]','nvarchar(MAX)') END HasPersonalHeartDiseaseHistory
			,CASE WHEN ExtendedProfile.value('(HasPersonalStrokeHistory)[1]','nvarchar(MAX)') ='' THEN NULL ELSE ExtendedProfile.value('(HasPersonalStrokeHistory)[1]','nvarchar(MAX)') END HasPersonalStrokeHistory
			,CASE WHEN ExtendedProfile.value('(ActivityLevel)[1]','nvarchar(100)') ='' THEN NULL ELSE ExtendedProfile.value('(ActivityLevel)[1]','nvarchar(100)') END ActivityLevel
			,CASE WHEN ExtendedProfile.value('(BloodType)[1]','nvarchar(100)') ='' THEN NULL ELSE ExtendedProfile.value('(BloodType)[1]','nvarchar(100)') END BloodType
			,CASE WHEN ExtendedProfile.value('(Allergies)[1]','nvarchar(MAX)')='' THEN NULL ELSE ExtendedProfile.value('(Allergies)[1]','nvarchar(MAX)') END Allergies
			,CASE WHEN ExtendedProfile.value('(TravelTime)[1]','nvarchar(MAX)')='' THEN NULL ELSE ExtendedProfile.value('(TravelTime)[1]','nvarchar(MAX)') END TravelTime
			,CASE WHEN ExtendedProfile.value('(MeansOfTransportation)[1]','nvarchar(MAX)') ='' THEN NULL ELSE ExtendedProfile.value('(MeansOfTransportation)[1]','nvarchar(MAX)') END MeansOfTransportation
			,CASE WHEN ExtendedProfile.value('(AttitudeTowardsHealthCare)[1]','nvarchar(100)') ='' THEN NULL ELSE ExtendedProfile.value('(AttitudeTowardsHealthCare)[1]','nvarchar(100)') END AttitudeTowardsHealthCare
			,CASE WHEN ExtendedProfile.value('(IsSmokeCigarette)[1]','nvarchar(MAX)')='' THEN NULL ELSE  ExtendedProfile.value('(IsSmokeCigarette)[1]','nvarchar(MAX)') END IsSmokeCigarette
			,CASE WHEN ExtendedProfile.value('(HasTriedToQuitSmoking)[1]','nvarchar(MAX)')='' THEN NULL ELSE ExtendedProfile.value('(HasTriedToQuitSmoking)[1]','nvarchar(MAX)') END HasTriedToQuitSmoking
			,CASE WHEN ExtendedProfile.value('(Country)[1]','nvarchar(50)')='' THEN NULL ELSE ExtendedProfile.value('(Country)[1]','nvarchar(50)')END  Country
			,CASE WHEN ExtendedProfile.value('(GenderOfPerson)[1]','nvarchar(10)') ='' THEN NULL ELSE ExtendedProfile.value('(GenderOfPerson)[1]','nvarchar(10)') END GenderOfPerson
			,CASE WHEN ExtendedProfile.value('(Ethnicity)[1]','nvarchar(50)')='' THEN NULL ELSE ExtendedProfile.value('(Ethnicity)[1]','nvarchar(50)') END Ethnicity
			,CASE WHEN ExtendedProfile.value('(MaritalStatus)[1]','nvarchar(50)') ='' THEN NULL ELSE ExtendedProfile.value('(MaritalStatus)[1]','nvarchar(50)') END MaritalStatus
			,CASE WHEN ExtendedProfile.value('(YearOfBirth)[1]','nvarchar(50)') ='' THEN NULL ELSE ExtendedProfile.value('(YearOfBirth)[1]','nvarchar(50)') END YearOfBirth
			from @SyncDataXml.nodes('/AHASyncData/ExtendedProfile') A(ExtendedProfile)

		)


		insert @ExtendedProfile
		Select EP1.*, case when EP2.ExtendedProfileID is null then 0 else 1 end
		 from ExtendedProfileT EP1 left join ExtendedProfile EP2 on EP1.UserHealthRecordID=EP2.UserHealthRecordID

		Update E1 

		set  E1.FamilyHistoryHD			=E2.FamilyHistoryHD
			,E1.FamilyHistoryStroke		=E2.FamilyHistoryStroke
			,E1.HighBPDiet				=E2.HighBPDiet
			,E1.HighBPMedication		=E2.HighBPMedication
			,E1.KidneyDisease			=E2.KidneyDisease
			,E1.DiabetesDiagnosis		=E2.DiabetesDiagnosis
			,E1.HeartDiagnosis			=E2.HeartDiagnosis
			,E1.Stroke					=E2.Stroke
			,E1.PhysicalActivityLevel	=E2.PhysicalActivityLevel
			,E1.BloodType				=E2.BloodType
			,E1.MedicationAllergy		=E2.MedicationAllergy
			,E1.TravelTimeToWorkInMinutes		=E2.TravelTimeToWorkInMinutes
			,E1.MeansOfTransportation			=E2.MeansOfTransportation
			,E1.AttitudesTowardsHealthcare		=E2.AttitudesTowardsHealthcare
			,E1.DoYouSmoke						=E2.DoYouSmoke
			,E1.HaveYouEverTriedToQuitSmoking	=E2.HaveYouEverTriedToQuitSmoking
			,E1.[Country/Region]				=E2.[Country/Region]
			,E1.Gender							=E2.Gender
			,E1.Ethnicity						=E2.Ethnicity
			,E1.MaritalStatus					=E2.MaritalStatus
			,E1.UpdatedDate						= getdate()
			,E1.LastSynchDate					= getdate()
			,E1.YearOfBirth						= E2.YearOfBirth
		From ExtendedProfile E1 inner join @ExtendedProfile E2 on E1.UserHealthRecordID=E2.UserHealthRecordID
		and E2.isUpdate=1
		insert ExtendedProfile
		(
		 UserHealthRecordID,FamilyHistoryHD,FamilyHistoryStroke,HighBPDiet,HighBPMedication,KidneyDisease
		,DiabetesDiagnosis,HeartDiagnosis,Stroke,PhysicalActivityLevel,BloodType,MedicationAllergy
		,TravelTimeToWorkInMinutes,MeansOfTransportation,AttitudesTowardsHealthcare,DoYouSmoke,HaveYouEverTriedToQuitSmoking
		,[Country/Region],Gender,Ethnicity,MaritalStatus,YearOfBirth
		)

		select 
		 UserHealthRecordID,FamilyHistoryHD,FamilyHistoryStroke,HighBPDiet,HighBPMedication,KidneyDisease
		,DiabetesDiagnosis,HeartDiagnosis,Stroke,PhysicalActivityLevel,BloodType,MedicationAllergy
		,TravelTimeToWorkInMinutes,MeansOfTransportation,AttitudesTowardsHealthcare,DoYouSmoke,HaveYouEverTriedToQuitSmoking
		,[Country/Region],Gender,Ethnicity,MaritalStatus,YearOfBirth from @ExtendedProfile where isUpdate=0


		--select * from @ExtendedProfile

		--Weight


		Declare  @Weight table
		(
		 UserHealthRecordID	int
		,HVItemGuid			uniqueidentifier
		,HVVersionStamp		uniqueidentifier
		,Weight				float
		,ReadingSource		nvarchar(100)
		,Comments			nvarchar(max)
		,DateMeasured		datetime
		,isUpdate			bit
		)


		;with WeightT as
		(
		select 

		 @UserHealthRecordID UserHealthRecordID
		,Weight.value('(ThingID)[1]','uniqueidentifier') HVItemGuid
		,Weight.value('(VersionStamp)[1]','uniqueidentifier') VersionStamp
		,CASE WHEN Weight.value('(WeightValue)[1]','nvarchar(MAX)')='' THEN NULL ELSE Weight.value('(WeightValue)[1]','nvarchar(MAX)') END WeightValue
		,CASE WHEN Weight.value('(Source)[1]','nvarchar(100)') ='' THEN NULL ELSE Weight.value('(Source)[1]','nvarchar(100)') END  Source
		,CASE WHEN Weight.value('(Note)[1]','nvarchar(MAX)') ='' THEN NULL ELSE Weight.value('(Note)[1]','nvarchar(MAX)')END  Note
		,CASE WHEN Weight.value('(When)[1]','nvarchar(MAX)') ='' THEN NULL ELSE Weight.value('(When)[1]','nvarchar(MAX)') END [When]

		from @SyncDataXml.nodes('/AHASyncData/Weight') A(Weight) 
		)

		insert @Weight

				Select W1.*, case when W2.weightid  is null then 0 else 1 end from WeightT W1 left join Weight W2 on W1.HVItemGuid=W2.HVItemGuid


Update W1  set

 W1.HVVersionStamp	= W2.HVVersionStamp
,W1.Weight			= W2.Weight
,W1.ReadingSource	= W2.ReadingSource
,W1.Comments		= W2.Comments
,W1.DateMeasured	= W2.DateMeasured
,W1.UpdatedDate		= getdate()
 
 From	Weight W1 inner join @Weight W2 on W1.HVItemGuid=W2.HVItemGuid
		and W2.isUpdate=1


Insert Weight
(
 UserHealthRecordID
,HVItemGuid
,HVVersionStamp
,Weight
,ReadingSource
,Comments
,DateMeasured
)
select  

UserHealthRecordID
,HVItemGuid
,HVVersionStamp
,Weight
,ReadingSource
,Comments
,DateMeasured

from @Weight where isUpdate=0

--select * from @Weight



	--Height


		Declare  @Height table
		(
		 UserHealthRecordID	int
		,HVItemGuid			uniqueidentifier
		,HVVersionStamp		uniqueidentifier
		,Height				float
		,DateMeasured		datetime
		,isUpdate			bit
		)


		;with HeightT as
		(
		select 

		 @UserHealthRecordID UserHealthRecordID
		,Height.value('(ThingID)[1]','uniqueidentifier') HVItemGuid
		,Height.value('(VersionStamp)[1]','uniqueidentifier') VersionStamp
		,CASE WHEN Height.value('(HeightValue)[1]','nvarchar(MAX)')='' THEN NULL ELSE Height.value('(HeightValue)[1]','nvarchar(MAX)') END HeightValue
		,CASE WHEN Height.value('(When)[1]','nvarchar(MAX)') ='' THEN NULL ELSE Height.value('(When)[1]','nvarchar(MAX)') END [When]

		from @SyncDataXml.nodes('/AHASyncData/Height') A(Height) 
		)

		insert @Height

				Select H1.*, case when H2.HeightID  is null then 0 else 1 end from HeightT H1 left join Height H2 on H1.HVItemGuid=H2.HVItemGuid


Update H1  set

 H1.HVVersionStamp	= H2.HVVersionStamp
,H1.Height			= H2.Height
,H1.DateMeasured	= H2.DateMeasured
,H1.UpdatedDate		= getdate()
 
 From	Height H1 inner join @Height H2 on H1.HVItemGuid=H2.HVItemGuid
		and H2.isUpdate=1


Insert Height
(
 UserHealthRecordID
,HVItemGuid
,HVVersionStamp
,Height
,DateMeasured
)
select  

UserHealthRecordID
,HVItemGuid
,HVVersionStamp
,Height
,DateMeasured

from @Height where isUpdate=0

--select * from @Height


--BloodPressure

		Declare  @BloodPressure table
		(
		 UserHealthRecordID		int
		,HVItemGuid				uniqueidentifier
		,HVVersionStamp			uniqueidentifier
		,Systolic				int
		,Diastolic				int
		,Pulse					int
		,ReadingSource			nvarchar(100)
		,Comments				nvarchar(max)
		,DateMeasured			datetime
		,isUpdate				bit
		)

		;with BloodPressureT as
		(
		select 
		 @UserHealthRecordID UserHealthRecordID
		,BloodPressure.value('(ThingID)[1]','uniqueidentifier') HVItemGuid
		,BloodPressure.value('(VersionStamp)[1]','uniqueidentifier') HVVersionStamp
		,CASE WHEN  BloodPressure.value('(Systolic)[1]','nvarchar(MAX)')='' THEN NULL ELSE BloodPressure.value('(Systolic)[1]','nvarchar(MAX)') END Systolic
		,CASE WHEN  BloodPressure.value('(Diastolic)[1]','nvarchar(MAX)') ='' THEN NULL ELSE   BloodPressure.value('(Diastolic)[1]','nvarchar(MAX)') END Diastolic
		,CASE WHEN  BloodPressure.value('(Pulse)[1]','nvarchar(MAX)')=''  THEN NULL ELSE BloodPressure.value('(Pulse)[1]','nvarchar(MAX)') END Pulse
		,CASE WHEN  BloodPressure.value('(Source)[1]','nvarchar(100)')='' THEN NULL ELSE  BloodPressure.value('(Source)[1]','nvarchar(100)') END Source
		,CASE WHEN  BloodPressure.value('(Note)[1]','nvarchar(max)')='' THEN NULL ELSE BloodPressure.value('(Note)[1]','nvarchar(max)') END Note
		,CASE WHEN  BloodPressure.value('(When)[1]','nvarchar(MAX)')=''  THEN NULL ELSE  BloodPressure.value('(When)[1]','nvarchar(MAX)') END [When]


		from @SyncDataXml.nodes('/AHASyncData/BloodPressure') A(BloodPressure)
		)

		insert @BloodPressure

				Select B1.*, case when B2.BloodPressureID  is null then 0 else 1 end from BloodPressureT B1 left join 
														BloodPressure B2 on B1.HVItemGuid=B2.HVItemGuid

Update B1  set

 B1.HVVersionStamp			= B2.HVVersionStamp
,B1.Systolic	 			= B2.Systolic
,B1.Diastolic				= B2.Diastolic
,B1.Pulse					= B2.Pulse
,B1.ReadingSource			= B2.ReadingSource
,B1.Comments				= B2.Comments
,B1.DateMeasured			= B2.DateMeasured
,B1.UpdatedDate				= getdate()
 

 From	BloodPressure B1 inner join @BloodPressure B2 on B1.HVItemGuid=B2.HVItemGuid
		and B2.isUpdate=1


Insert BloodPressure
(
 UserHealthRecordID
,HVItemGuid
,HVVersionStamp
,Systolic
,Diastolic
,Pulse
,ReadingSource
,Comments
,DateMeasured
)
select  
 UserHealthRecordID
,HVItemGuid
,HVVersionStamp
,Systolic
,Diastolic
,Pulse
,ReadingSource
,Comments
,DateMeasured

from @BloodPressure where isUpdate=0

--select * from @BloodPressure

		--Cholesterol 


		Declare  @Cholesterol table
		(
		 UserHealthRecordID			int
		,HVItemGuid					uniqueidentifier
		,HVVersionStamp				uniqueidentifier
		,Totalcholesterol			int
		,LDL						int
		,HDL						int
		,Triglyceride				int
		,TestLocation				nvarchar(max)
		,DateMeasured				datetime
		,isUpdate					bit
		)

		;with CholesterolT as
		(

		select 
		@UserHealthRecordID UserHealthRecordID
		,Cholesterol.value('(ThingID)[1]','uniqueidentifier')  HVItemGuid
		,Cholesterol.value('(VersionStamp)[1]','uniqueidentifier') HVVersionStamp
		,CASE WHEN Cholesterol.value('(TotalCholesterol)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Cholesterol.value('(TotalCholesterol)[1]','nvarchar(MAX)')END TotalCholesterol
		,CASE WHEN Cholesterol.value('(LDL)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Cholesterol.value('(LDL)[1]','nvarchar(MAX)') END LDL
		,CASE WHEN Cholesterol.value('(HDL)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Cholesterol.value('(HDL)[1]','nvarchar(MAX)') END HDL
		,CASE WHEN Cholesterol.value('(Triglyceride)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Cholesterol.value('(Triglyceride)[1]','nvarchar(MAX)')END Triglyceride
		,CASE WHEN Cholesterol.value('(Note)[1]','nvarchar(max)')=''  THEN NULL ELSE Cholesterol.value('(Note)[1]','nvarchar(max)') END Note
		,CASE WHEN Cholesterol.value('(When)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Cholesterol.value('(When)[1]','nvarchar(MAX)') END [When]

		from @SyncDataXml.nodes('/AHASyncData/Cholesterol') A(Cholesterol)



		)

		insert @Cholesterol

				Select C1.*, case when C2.CholesterolID  is null then 0 else 1 end from CholesterolT C1 left join 
														Cholesterol C2 on C1.HVItemGuid=C2.HVItemGuid

		Update C1  set

		 C1.HVVersionStamp			= C2.HVVersionStamp
		,C1.Totalcholesterol		= C2.Totalcholesterol
--		,C1.ReadingSource			= C2.ReadingSource
		,C1.LDL						= C2.LDL
		,C1.HDL						= C2.HDL
		,C1.Triglyceride			= C2.Triglyceride
		,C1.TestLocation			= C2.TestLocation
		,C1.DateMeasured			= C2.DateMeasured
		,C1.UpdatedDate				= getdate()
		 
		 From	Cholesterol C1 inner join @Cholesterol C2 on C1.HVItemGuid=C2.HVItemGuid
				and C2.isUpdate=1


		Insert Cholesterol
		(
		 UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,Totalcholesterol
		,LDL
		,HDL
		,Triglyceride
		,TestLocation
		,DateMeasured

		)
		select  

		 UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,Totalcholesterol
		,LDL
		,HDL
		,Triglyceride
		,TestLocation
		,DateMeasured


		from @Cholesterol where isUpdate=0


--select * from @Cholesterol
 



		--BloodGlucose 


		
Declare  @BloodGlucose table
		(
		 UserHealthRecordID	int
		,HVItemGuid				uniqueidentifier
		,HVVersionStamp			uniqueidentifier
		,BloodGlucoseValue		float
		,ActionITook			nvarchar(100)
		,ReadingType			nvarchar(100)
		,DateMeasured			datetime
		,isUpdate			bit
		)


		;with BloodGlucoseT as
		(

		select 

		 @UserHealthRecordID UserHealthRecordID
		,BloodGlucose.value('(ThingID)[1]','uniqueidentifier')  HVItemGuid
		,BloodGlucose.value('(VersionStamp)[1]','uniqueidentifier') HVVersionStamp
		,CASE WHEN BloodGlucose.value('(Value)[1]','nvarchar(MAX)')=''  THEN NULL ELSE BloodGlucose.value('(Value)[1]','nvarchar(MAX)') END [Value]
		,CASE WHEN BloodGlucose.value('(ActionTaken)[1]','nvarchar(100)')=''  THEN NULL ELSE BloodGlucose.value('(ActionTaken)[1]','nvarchar(100)') END ActionTaken
		,CASE WHEN BloodGlucose.value('(ReadingType)[1]','nvarchar(100)')=''  THEN NULL ELSE BloodGlucose.value('(ReadingType)[1]','nvarchar(100)') END ReadingType
		,CASE WHEN BloodGlucose.value('(When)[1]','nvarchar(MAX)')=''  THEN NULL ELSE BloodGlucose.value('(When)[1]','nvarchar(MAX)')END  [When]

		from @SyncDataXml.nodes('/AHASyncData/BloodGlucose') A(BloodGlucose)



		)

		insert @BloodGlucose

				Select B1.*, case when B2.BloodGlucoseId  is null then 0 else 1 end from BloodGlucoseT B1 left join BloodGlucose B2 
																						on B1.HVItemGuid=B2.HVItemGuid


		Update B1  set

		 B1.HVVersionStamp			= B2.HVVersionStamp
		,B1.BloodGlucoseValue		= B2.BloodGlucoseValue
		,B1.ActionITook				= B2.ActionITook
		,B1.ReadingType				= B2.ReadingType
		,B1.DateMeasured			= B2.DateMeasured
		,B1.UpdatedDate				= getdate()
		 
		 From	BloodGlucose B1 inner join @BloodGlucose B2 on B1.HVItemGuid=B2.HVItemGuid
				and B2.isUpdate=1


		Insert BloodGlucose
		(
		 UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,BloodGlucoseValue
		,ActionITook
		,ReadingType
		,DateMeasured
		)
		select  

		UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,BloodGlucoseValue
		,ActionITook
		,ReadingType
		,DateMeasured

		from @BloodGlucose where isUpdate=0

		--select * from @BloodGlucose




--Medication


	
	Declare  @Medication table
		(
		 UserHealthRecordID	int
		,HVItemGuid				uniqueidentifier
		,HVVersionStamp			uniqueidentifier
		,MedicationName			nvarchar(255)
		,MedicationType			nvarchar(255)
		,Dosage					nvarchar(50)
		,Strength				nvarchar(50)
		,Frequency				nvarchar(50)
		,DateDiscontinued		datetime
		,Notes					nvarchar(max)
		,isUpdate				bit
		)


		;with MedicationT as
		(
		select 

		 @UserHealthRecordID UserHealthRecordID
		,Medication.value('(ThingID)[1]','uniqueidentifier')    HVItemGuid
		,Medication.value('(VersionStamp)[1]','uniqueidentifier') HVVersionStamp
		,CASE WHEN Medication.value('(MedicationName)[1]','nvarchar(255)')=''  THEN NULL ELSE Medication.value('(MedicationName)[1]','nvarchar(255)') END MedicationName
		,CASE WHEN Medication.value('(MedicationType)[1]','nvarchar(255)')=''  THEN NULL ELSE Medication.value('(MedicationType)[1]','nvarchar(255)') END MedicationType
		,CASE WHEN Medication.value('(Dosage)[1]','nvarchar(50)')=''  THEN NULL ELSE Medication.value('(Dosage)[1]','nvarchar(50)') END Dosage
		,CASE WHEN Medication.value('(Strength)[1]','nvarchar(50)')=''  THEN NULL ELSE Medication.value('(Strength)[1]','nvarchar(50)') END Strength
		,CASE WHEN Medication.value('(Frequency)[1]','nvarchar(50)')=''  THEN NULL ELSE Medication.value('(Frequency)[1]','nvarchar(50)') END Frequency
		,CASE WHEN Medication.value('(DateDiscontinued)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Medication.value('(DateDiscontinued)[1]','nvarchar(MAX)') END DateDiscontinued
		,CASE WHEN Medication.value('(Note)[1]','nvarchar(max)')=''  THEN NULL ELSE Medication.value('(Note)[1]','nvarchar(max)') END Note



		from @SyncDataXml.nodes('/AHASyncData/Medication') A(Medication)

		)

		insert @Medication

				Select M1.*, case when M2.MedicationId  is null then 0 else 1 end from MedicationT M1 left join Medication M2 on M1.HVItemGuid=M2.HVItemGuid



		Update M1  set

		 M1.HVVersionStamp				= M2.HVVersionStamp
		,M1.MedicationName				= M2.MedicationName
		,M1.MedicationType				= M2.MedicationType
		,M1.Dosage						= M2.Dosage
		,M1.Strength					= M2.Strength
		,M1.Frequency					= M2.Frequency
		,M1.DateDiscontinued			= M2.DateDiscontinued
		,M1.UpdatedDate					= getdate()
		 
		 From	Medication M1 inner join @Medication M2 on M1.HVItemGuid=M2.HVItemGuid
				and M2.isUpdate=1


		Insert Medication
		(
		UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,MedicationName
		,MedicationType
		,Dosage
		,Strength
		,Frequency
		,DateDiscontinued
		,Notes


		)
		select  

		 UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,MedicationName
		,MedicationType
		,Dosage
		,Strength
		,Frequency
		,DateDiscontinued
		,Notes


		from @Medication where isUpdate=0


--select * from @Medication
 




		--Exercise


		Declare  @Exercise table
		(
		 UserHealthRecordID	int
		,HVItemGuid				uniqueidentifier
		,HVVersionStamp			uniqueidentifier
		,DateofSession			datetime
		,ExerciseType			nvarchar(50)
		,Intensity				nvarchar(50)
		,Duration				float
		,Comments				nvarchar(max)
		,NumberOfSteps			int
		,isUpdate				bit
		)


		;with ExerciseT as
		(

		select 
		 @UserHealthRecordID UserHealthRecordID
		,Exercise.value('(ThingID)[1]','uniqueidentifier') HVItemGuid
		,Exercise.value('(VersionStamp)[1]','uniqueidentifier') HVVersionStamp
		,CASE WHEN Exercise.value('(When)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Exercise.value('(When)[1]','nvarchar(MAX)') END DateofSession
		,CASE WHEN Exercise.value('(Activity)[1]','nvarchar(50)')=''  THEN NULL ELSE Exercise.value('(Activity)[1]','nvarchar(50)') END Activity
		,CASE WHEN Exercise.value('(Intensity)[1]','nvarchar(50)')=''  THEN NULL ELSE Exercise.value('(Intensity)[1]','nvarchar(50)') END Intensity
		,CASE WHEN Exercise.value('(DurationInMinutes)[1]','nvarchar(MAX)')=''  THEN NULL ELSE  Exercise.value('(DurationInMinutes)[1]','nvarchar(MAX)') END DurationInMinutes
		,CASE WHEN Exercise.value('(Note)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Exercise.value('(Note)[1]','nvarchar(MAX)') END Note
		,CASE WHEN Exercise.value('(NumberOfSteps)[1]','nvarchar(MAX)')=''  THEN NULL ELSE Exercise.value('(NumberOfSteps)[1]','nvarchar(MAX)') END NumberOfSteps

		from @SyncDataXml.nodes('/AHASyncData/Exercise') A(Exercise)





		)

		insert @Exercise

				Select E1.*, case when E2.ExerciseID  is null then 0 else 1 end from ExerciseT E1 left join Exercise E2 on E1.HVItemGuid=E2.HVItemGuid



		Update E1  set
		 E1.HVVersionStamp				= E2.HVVersionStamp
		,E1.DateofSession				= E2.DateofSession
		,E1.ExerciseType				= E2.ExerciseType
		,E1.Intensity					= E2.Intensity
		,E1.Duration					= E2.Duration
		,E1.Comments					= E2.Comments
		,E1.NumberOfSteps				= E2.NumberOfSteps
		,E1.UpdatedDate					= getdate()
		 
		 From	Exercise E1 inner join @Exercise E2 on E1.HVItemGuid=E2.HVItemGuid
				and E2.isUpdate=1


		Insert Exercise
		(
		UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,DateofSession
		,ExerciseType
		,Intensity
		,Duration
		,Comments
		,NumberOfSteps


		)
		select  

		 UserHealthRecordID
		,HVItemGuid
		,HVVersionStamp
		,DateofSession
		,ExerciseType
		,Intensity
		,Duration
		,Comments
		,NumberOfSteps


		from @Exercise where isUpdate=0


--		select * from @Exercise

-- IVR Reminder
if (@phonenumber != '0') 
begin
    SET @PhoneNumber = replace(@PhoneNumber, '(', '')
    SET @PhoneNumber = replace(@PhoneNumber, ')', '')
    SET @PhoneNumber = replace(@PhoneNumber, '-', '')
    SET @PhoneNumber = replace(@PhoneNumber, '.', '')
    SET @PhoneNumber = replace(@PhoneNumber, ' ', '')
    IF (len(@PhoneNumber)=10)
    begin
     SET @PhoneNumber = '1' + @PhoneNumber
    end
 update IVRPatientReminder 
	set PhoneNumber = @phonenumber 
	,UpdatedDateTime = GetDate()
 where PatientID=@UserHealthRecordID --and @phonenumber is not null and @phonenumber != null
 
end
-- ZipCode
if (@zipCode != '0')
begin
   update ExtendedProfile set ZipCode= @zipCode where UserHealthRecordID=@UserHealthRecordID 
end
--lastactivity
if @UpdateLastSyncDate=1
	Update HealthRecord set LastActivityDate=getdate() where UserHealthRecordID=@UserHealthRecordID

set @isSynched=1

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

SET @isSynched=0
	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch



END



