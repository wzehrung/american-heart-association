﻿







CREATE proc [dbo].[Proc_FindAlertByProviderID]
(
@ProviderID INT 
)

AS

Begin
set nocount on

select distinct C.AlertID, C.PatientID, IsDismissed,cast(AlertXml as nvarchar(max)) AlertXml
,cast(AlertRuleXml as nvarchar(max)) AlertData , CreatedDate,Type ,Notes,ItemEffectiveDate 

from PatientGroupDetails A
inner join PatientGroupMapping B
on A.GroupID=B.GroupID and A.ProviderID=@ProviderID
INNER JOIN Alert C On B.PatientID=C.PatientID
inner join AlertRule D on C.AlertRuleId=D.AlertRuleID--and D.isActive=1
and A.ProviderID=D.ProviderID
order by IsDismissed asc,ItemEffectiveDate desc, CreatedDate desc
End







