﻿

CREATE Procedure [dbo].[Proc_CreateOrChangeHbA1c]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@HbA1cValue float
,@DateMeasured datetime
,@Notes nvarchar(MAX) 
,@ReadingSource nvarchar(100)
)
AS

BEGIN

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION						

IF EXISTS(
		  SELECT HbA1cID FROM dbo.BloodGlucose_HbA1c WHERE 
		  HVItemGuid=@HVItemGuid 	
		  )

EXEC Proc_Update_HbA1c	 
	@HVItemGuid 
	,@HVVersionStamp 
	,@HbA1cValue 
	,@DateMeasured 
	,@Notes
	,@ReadingSource

ELSE 
BEGIN

DECLARE @UserHealthRecordID int

SELECT @UserHealthRecordID = UserHealthRecordID  FROM dbo.HealthRecord
							 WHERE PersonalItemGUID=@PersonalItemGUID

EXEC Proc_Insert_HbA1c   
	@UserHealthRecordID 
	,@HVItemGuid 
	,@HVVersionStamp 
	,@HbA1cValue 
	,@DateMeasured 
	,@Notes
	,@ReadingSource					
								
END
	
EXEC proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

																							
End
