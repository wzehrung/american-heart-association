﻿  
CREATE  PROC [dbo].[Proc_DoesEmailSchedulerExistForCampaignIDAndDate]  
(  
 @CampaignID int  
,@ScheduledDate datetime  
,@IsProviderList bit  
)  
  
AS  
  
Begin  
if(@CampaignID is null)
select * from EmailListScheduler where   
convert(datetime,Convert(varchar,ScheduledDate,103),103)=convert(datetime,Convert(varchar,@ScheduledDate,103),103)  
and CampaignID is null   
and IsProviderList = @IsProviderList  
else 
select * from EmailListScheduler where   
convert(datetime,Convert(varchar,ScheduledDate,103),103)=convert(datetime,Convert(varchar,@ScheduledDate,103),103)  
and CampaignID=@CampaignID   
and IsProviderList = @IsProviderList  
End  