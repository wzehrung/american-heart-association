﻿

--select * from AlertRule

CREATE proc [dbo].[Proc_CreateAlertRule]
(
 @ProviderID INT
,@PatientID INT
,@GroupID INT
,@Type NVARCHAR(128)
,@AlertData XML
,@SendMessageToPatient bit
,@MessageToPatient nvarchar(max)
,@AlertRuleID int output
)

AS

Begin
set nocount on


Begin Try
Begin Transaction

insert AlertRule(ProviderID,Type,AlertData,SendMessageToPatient,GroupID,MessageToPatient)
select @ProviderID,@Type,@AlertData,@SendMessageToPatient
,Case when @GroupID=0 then 
(select GroupID from PatientGroupDetails where ProviderID=@ProviderID and isDefault=1)
 else @GroupID End,@MessageToPatient


set @AlertRuleID=scope_identity()

if @GroupID=0

	insert Alertmapping(AlertRuleID,PatientID,GroupID)
	select Distinct @AlertRuleID,B.PatientID,A.GroupID
	from PatientGroupDetails A
	inner join PatientGroupMapping B
	on A.GroupID=B.GroupID and A.ProviderID=@ProviderID and isDefault=1

else if @GroupID is null

	insert Alertmapping(AlertRuleID,PatientID,GroupID)
	select @AlertRuleID,@PatientID,@GroupID

else 

	insert Alertmapping(AlertRuleID,PatientID,GroupID)
		SELECT @AlertRuleID,PatientID,GroupID FROM PatientGroupMapping
		 where GroupID= @GroupID
	


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End




