﻿

CREATE PROCEDURE [dbo].[Proc_CreatePatientProviderInvitation]
(
 @InvitationCode nvarchar(6) output
,@UserHealthRecordID int
,@ProviderID int
,@sentConfirmation bit
,@InvitationID int output
)
As
Begin
set nocount on

Begin Try
Begin Transaction

--declare @InvitationCode NVARCHAR(6)

exec GenerateInvitationCode @InvitationCode output

--SET @GeneratedCode = @InvitationCode


	insert PatientProviderInvitation(InvitationCode,SentPatientID,SentProviderID,sentConfirmation)
	select @InvitationCode,@UserHealthRecordID,@ProviderID,@sentConfirmation

SET @InvitationID=SCOPE_IDENTITY()


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
	
End

