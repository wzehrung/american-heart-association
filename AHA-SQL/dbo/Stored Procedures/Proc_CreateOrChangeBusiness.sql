﻿
CREATE PROC [dbo].[Proc_CreateOrChangeBusiness]  
(  
	@BusinessID int,
	@BusinessName varchar(64),  
	@Comments nvarchar(1024)
)  
  
AS  
  
BEGIN  
SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION
  
IF EXISTS (SELECT BusinessID FROM AdminBusiness WHERE BusinessID = @BusinessID )  
  
UPDATE AdminBusiness
	SET 
		Name		= @BusinessName,
		Comments	= @Comments
	WHERE
		BusinessID	= @BusinessID
ELSE  
  
INSERT AdminBusiness
	(
		Name,
		Comments
	)	
SELECT
		@BusinessName,
		@Comments

COMMIT
END TRY

BEGIN CATCH

  IF @@TRANCOUNT > 0
	ROLLBACK

	
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
	SELECT  @ErrMsg = ERROR_MESSAGE(),
			@ErrSeverity = ERROR_SEVERITY()
	RAISERROR(@ErrMsg, @ErrSeverity, 1)

 END CATCH 
  
END  
  



