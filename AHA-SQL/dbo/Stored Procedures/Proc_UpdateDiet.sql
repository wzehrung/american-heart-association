﻿
CREATE Procedure [dbo].[Proc_UpdateDiet]
(
 @HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier
,@CalorieAmount int 
,@Meal nvarchar(50) 
,@FoodItems nvarchar(max) 
,@DateOfEntry datetime 

)
AS

Begin

--This Sp should not be called from within application/product

/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :updating Diet table..
*/


Update dbo.Diet

              SET 
					 HVVersionStamp	=  @HVVersionStamp
					,CalorieAmount	=  @CalorieAmount 
					,Meal			=  @Meal 
					,FoodItems		=  @FoodItems 
					,DateOfEntry	=  @DateOfEntry 
					,UpdatedDate	= getDate()  
              

				  Where HVItemGuid=@HVItemGuid 
End







