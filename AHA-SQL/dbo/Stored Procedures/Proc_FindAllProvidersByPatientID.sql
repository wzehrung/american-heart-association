﻿	CREATE PROCEDURE [dbo].[Proc_FindAllProvidersByPatientID]
		(
		@PatientID int
		)
		As
		Begin
		set nocount on

			select distinct PR.* from Provider PR inner join
			PatientGroupDetails PGD on PR.ProviderID=PGD.ProviderID
			inner join
			PatientGroupMapping PGM on PGD.GroupID=PGM.GroupID and PGM.PatientID=@PatientID

			
		End