﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 06.03.2014
-- Description:	Deletes a Provider Note
-- =============================================
CREATE PROCEDURE [dbo].[Proc_DeleteProviderNote] 
(
	@ProviderNoteID int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE ProviderNote SET IsDeleted = 1 WHERE ProviderNoteID=@ProviderNoteID
END

