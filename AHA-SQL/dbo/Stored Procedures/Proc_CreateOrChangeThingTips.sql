﻿CREATE PROC [dbo].[Proc_CreateOrChangeThingTips]  
(    
 @ThingTipsData nvarchar(max)    
,@ThingTipsTypeIds nvarchar(max)    
,@UpdatedByUserID INT    
,@LanguageID int   
,@ThingTipsID INT output  
)    
As  
Begin  
set nocount on  
  
Begin Try  
Begin Transaction  
  
  
if exists (select * from ThingTips where ThingTipsID=@ThingTipsID and LanguageID=@LanguageID)  
  
exec Proc_ChangeThingTips  
  
 @ThingTipsData   
,@ThingTipsTypeIds   
,@ThingTipsID   
,@UpdatedByUserID   
,@LanguageID   
  
else  
  
exec Proc_CreateThingTips  
  
 @ThingTipsData   
,@ThingTipsTypeIds   
,@UpdatedByUserID     
,@LanguageID    
,@ThingTipsID output 
  
  
  
COMMIT  
End Try  
  
Begin Catch  
  
  If @@TRANCOUNT > 0  
 ROLLBACK  
  
   
 Declare @ErrMsg nvarchar(4000), @ErrSeverity int  
 Select  @ErrMsg = ERROR_MESSAGE(),  
    @ErrSeverity = ERROR_SEVERITY()  
 Raiserror(@ErrMsg, @ErrSeverity, 1)  
  
 End Catch  
End  