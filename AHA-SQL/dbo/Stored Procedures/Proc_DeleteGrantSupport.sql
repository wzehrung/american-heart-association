﻿
CREATE PROC [dbo].[Proc_DeleteGrantSupport]
(
 @GrantSupportID int
 ,@LanguageID int
)
As

Begin
set nocount on

DELETE   FROM GrantSupport WHERE GrantSupportID=@GrantSupportID and LanguageID=@LanguageID

End