﻿
CREATE proc [dbo].[Proc_DismissAlert]
(
 @AlertID INT 
,@notes nvarchar(max)
)

AS

Begin
set nocount on

UPDATE Alert SET IsDismissed=1,Notes=@notes WHERE AlertID=@AlertID


End




