﻿




CREATE PROC [dbo].[Proc_DeleteContent]
(
 @ContentID int
,@LanguageID int
)

As

Begin
set nocount on

Begin Try
Begin Transaction


Delete from ContentRuleMapping where ContentID=@ContentID and LanguageID=@LanguageID

Delete from Content Where ContentID=@ContentID and LanguageID=@LanguageID

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End


