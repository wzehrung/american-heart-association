﻿-- =============================================
-- Author:		Phil Brock
-- Create date: 6.19.2013
-- Description:	Refactor of Proc_Report_Provider_PatientsPerProvider_Campaign
--              to handle Campaign/Markets
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Provider_PatientsPerProvider_Campaign] 
(  
 @StartDate Datetime,  
 @EndDate Datetime,
 @CampaignID int = null,	-- null specifies all campaigns
 @Markets NVARCHAR(MAX),	-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
 @UserTypeID int = null		-- Note: the pre-refactored version (v5) did not use this in logic, so we don't
)
AS
BEGIN
	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
		-- Create a temp table to contain all the zip codes that relate to given Markets
	-- Also keep a counter of how many zip codes are in the temp table
	--
	-- drop table #RptMarketZipCodes
	DECLARE @ZipCodeCount int

	
	SELECT Zip_Code INTO #RptMarketZipCodes 
		from ZipRegionMapping 
		where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

	SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

	--
	-- Create yet another temp table containing all the providers who are in the given campaign(s) and markets
	--
	-- drop table #RptPatients
	SELECT PGL.PatientGroupMapping_LogID, PGL.PatientID, PGL.GroupID, PGL.ModifiedDate, PGL.Action INTO #RptPatients
		FROM PatientGroupMapping_Log PGL
		inner join HealthRecord H  on H.UserHealthRecordID = PGL.PatientID
		left join ExtendedProfile E ON E.UserHealthRecordID = H.UserHealthRecordID  
		WHERE ((@CampaignID is null AND H.CampaignID is not null) OR (@CampaignID is not null AND @CampaignID = H.CampaignID)) AND
			  (@ZipCodeCount = 0 or E.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
 
	-- NOTE: NOT SURE HOW TO INTEGRATE USER TYPE!!!!! ----------------------------------------
	--  P.UserTypeID = case  when @UserTypeID IS NULL then P.UserTypeID else @UserTypeID end
	
	--
	-- Now generate the report
	--
	

    ;With RptTypes As
	(
		select 'Number Of Participants' as RptType,1 rptID
		UNION ALL
		select 'Number of Providers and/or Volunteers' as RptType,2 rptID
		--UNION ALL
		--select 'Average Number Of Patients Per Provider' as RptType,3 rptID
	)
	,RptTypesWithDates As
	(
		select D.Date1,R.RptType,rptID,D.date2
			from DBO.udf_GetDatesWithRange (@StartDate,@EndDate) D cross join RptTypes R
	)
	,PatientGroupMapping_LogCTE As
	(
		select max(PatientGroupMapping_LogID) PatientGroupMapping_LogID 
			from #RptPatients  
			group by PatientID,GroupID
	)		
	,Patients As
	(
		 select rptID, RptType, D.Date1,COUNT(DISTINCT PatientID) UserCount 
			from RptTypesWithDates D 													
			left join
			(
				select ModifiedDate,PatientID 
					from #RptPatients P  
					inner join PatientGroupMapping_LogCTE B on P.PatientGroupMapping_LogID=B.PatientGroupMapping_LogID and P.Action=0
			) H  on H.Modifieddate<D.Date2 -- H.Modifieddate>=D.Date1 and H.Modifieddate<D.Date2	
			where  rptID=1 
			group by rptID,RptType,D.Date1
	)
	,Providers As
	( 
		 select rptID, RptType, D.Date1, COUNT(DISTINCT ProviderID) UserCount 
			from RptTypesWithDates D 
			left join 
			(
				select PGD.ProviderID, PGL.GroupID, PGL.ModifiedDate, PGL.Action 
					from PatientGroupMapping_LogCTE PGLC 
					inner join PatientGroupMapping_Log PGL on PGL.PatientGroupMapping_LogID=PGLC.PatientGroupMapping_LogID and PGL.Action=0
					inner join PatientGroupDetails PGD on PGD.GroupID=PGL.GroupID
					inner join #RptPatients P on P.PatientID = PGL.PatientID
			) PGL on PGL.Modifieddate < D.Date2 --PGL.Modifieddate>=D.Date1  and PGL.Modifieddate<D.Date2
			where rptID=2
			group by rptID, RptType, D.Date1
		 
	)
	,Result As
	(
		select * from Patients
		union all
		select * from Providers
		union all
		
		select 3,'Average', A.date1,
			   cast(cast(A.UserCount as numeric(10,2))/case when B.UserCount=0 then 1 else B.UserCount end as numeric(10,2))
			from Patients A 
			inner join Providers B on A.date1 = B.date1 --where A.rptid=1
	)


	Select rptID, T.Date1, RptType, UserCount,
		   'Column # ' + cast(dense_Rank() over (order by T.Date1) as varchar(10)) ColumnIndex
		into #ToBePivoted
		From Result T 
		
	SELECT DISTINCT ColumnIndex INTO #ColumnsIndex FROM #ToBePivoted
 
	--select substring(cast([UserCount] as varchar(10)),0,charindex('.',cast([UserCount] as varchar(10))))
	--from #ToBePivoted

	select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1


	DECLARE @MaxCount INTEGER
	DECLARE @Count INTEGER
	DECLARE @Txt VARCHAR(MAX)
	SET @Count = 1
	SET @Txt = '['
	SET @MaxCount = (SELECT MAX(RowID) FROM #c)
	WHILE @Count<=@MaxCount
		BEGIN
		IF @Txt!=''
			SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
		ELSE
			SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
		SET @Count=@Count+1
		END
	--SELECT @Txt AS Txt

	SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

	SET @Cols = @Txt

	SET @query = N'SELECT RptType, '+ @cols +' FROM
	(SELECT RptType, ColumnINdex, UserCount,RptID FROM #ToBePivoted ) A
	PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'
	
	execute(@query)	
END
