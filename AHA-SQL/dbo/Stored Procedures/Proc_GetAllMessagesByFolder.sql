﻿

CREATE PROC [dbo].[Proc_GetAllMessagesByFolder]
(
 @UserID int
,@UserType NVARCHAR(10)
,@FolderType char(1)
)
As

Begin
SET NOCOUNT ON


	IF @UserType='Patient'
	BEGIN

	;With Concatinate As
	(
		select 			 

				 MD.MessageID	
				,MD.isRead
				,MD.ProviderID
				,IsFlaged
				
					
		from

		Folder F  
		inner join UserFolderMapping UFM
		on F.FolderID=UFM.FolderID and UFM.PatientID=@UserID
		and F.FolderType=@FolderType
		inner join MessageDetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID	
		and MD.IsSentByProvider=1
		
													
	),
	Results As
	(
	select 
		MessageID 
		,IsFlaged
		,@UserID  PatientID
		,stuff( ( select ','+ Cast(ProviderID as varchar(10))
		from Concatinate c1
		where c2.MessageID = c1.MessageID
		for xml path('') 
		),1,1,'') ProviderID
		,isRead


	from Concatinate c2
	group by MessageID,isRead,IsFlaged
	)

	select 

	M.MessageID
	,PatientID
	,ProviderID 	
	,isRead
	,IsFlaged
	,SentDate
	,MessageType 
	,M.GroupID
	from [Message] M inner join Results R on M.MessageID=R.MessageID

	END



	ELSE IF @UserType='Provider'
	BEGIN


		;With Concatinate As
		(
			select 			 

					 MD.MessageID	
					,MD.isRead
					,MD.PatientID
					,IsFlaged
						
			from

			Folder F  
			inner join UserFolderMapping UFM
			on F.FolderID=UFM.FolderID and UFM.ProviderID=@UserID
			and F.FolderType=@FolderType
			inner join MessageDetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID	
			and MD.IsSentByProvider=0
			
														
		),
		Results As
		(
		select 
		MessageID 
		,IsFlaged
		,@UserID  ProviderID
		,stuff( ( select ','+ Cast(PatientID as varchar(10))
		from Concatinate c1
		where c2.MessageID = c1.MessageID
		for xml path('') 
		),1,1,'') PatientID
		,isRead


		from Concatinate c2
		group by MessageID,isRead,IsFlaged
		)

		select 

		M.MessageID
		,PatientID
		,ProviderID 	
		,isRead
		,IsFlaged
		,SentDate
		,MessageType 
		,M.GroupID
		from [Message] M inner join Results R on M.MessageID=R.MessageID


	END

End




