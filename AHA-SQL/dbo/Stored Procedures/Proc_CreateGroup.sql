﻿



CREATE PROCEDURE [dbo].[Proc_CreateGroup]
(
 @Name nvarchar(50)
,@Description nvarchar(MAX)
,@GroupRuleXml xml
,@ProviderID int
,@RemoveOption char(1)
,@IsSystemGenerated bit
,@GroupID int output

)
As
Begin
set nocount on

Begin Try
Begin Transaction

	Exec Proc_InsertPatientGroup @Name, @Description,@GroupRuleXml ,@ProviderID,@RemoveOption,@IsSystemGenerated,@GroupID output

COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
	
End
