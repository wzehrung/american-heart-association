﻿CREATE PROCEDURE [dbo].[Proc_InsertPatientGroup]
(
 @Name nvarchar(50)
,@Description nvarchar(MAX)
,@GroupRuleXml xml
,@ProviderID int
,@RemoveOption char(1)
,@IsSystemGenerated bit
,@GroupID int output

)
As
Begin
set nocount on

insert PatientGroupDetails(Name,Description,GroupRuleXml,ProviderID,RemoveOption,IsSystemGenerated)
select @Name,@Description,@GroupRuleXml,@ProviderID,@RemoveOption,@IsSystemGenerated
set @GroupID=scope_identity()

End

