﻿--[PROC_Report_ProviderConnectedToPatients] '2009.03.01','2010.01.02'
--PROC_Report_ProviderConnectedToPatients   '2010.02.01','2010.08.01',null


CREATE PROC [dbo].[Proc_Report_Provider_ConnectedToPatients]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID int = null
)
As

BEGIN
set nocount on

DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)


;with RptTypes As
(
select 1 RptID,'1-10 participants' As RptType
union all
select 2 RptID,'11-25 participants' As RptType
union all
select 3 RptID,'26-75 participants' As RptType
union all
select 4 RptID,'75-100 participants' As RptType
union all
select 5 RptID,'>100 participants' As RptType
),
RptTypesWithDates As
(
select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,EligiblePatients As
(
 select A.* from PatientGroupMapping_Log A inner join 
(
	select max(PatientGroupMapping_LogID) PatientGroupMapping_LogID  from PatientGroupMapping_Log  PGL
	--inner join DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R on PGL.ModifiedDate >= R.Date1 and PGL.ModifiedDate < R.Date2 
	inner join HealthRecord H  on H.UserHealthRecordID=PGL.PatientID where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1) 
	 group by [PatientID],groupid 
	
	--union all
	
	--select max(PatientGroupMapping_LogID) PatientGroupMapping_LogID,Date1  from PatientGroupMapping_Log  PGL
	--inner join DBO.udf_GetDatesWithRange (@StartDate,@EndDate) R on PGL.ModifiedDate >= R.Date1 and PGL.ModifiedDate < R.Date2  
	--inner join HealthRecord H  on H.UserHealthRecordID=PGL.PatientID where isnull(CampaignID,-1)=isnull(isnull(@CampaignID,CampaignID),-1) 
	--and [Action]=1 and RowType='NewRow' group by [PatientID],Date1,groupid
	
) B on A.PatientGroupMapping_LogID=B.PatientGroupMapping_LogID and [Action]=0
)

,EligibleProviders As
(
Select R.Date1,ProviderID,Count(distinct PatientID) as Patients from DBO.udf_GetDatesWithRange (@StartDate,@EndDate)  R inner join 
EligiblePatients PML on -- PML.ModifiedDate >= R.Date1 and
 PML.ModifiedDate < R.Date2 
inner join patientgroupdetails PGD on PGD.groupid=PML.groupid group by R.Date1,ProviderID
)

--select * from EligiblePatients


,Patients As
(
select      date1,ProviderID,Case when Patients between 1 and 10 then '1-10 patients' when Patients between 11 and 25 then '11-25 patients'
			when Patients between 26 and 75 then '26-75 patients' when Patients between 75 and 100 then '75-100 patients'
			when Patients >100 then '100+ patients' End RptType from EligibleProviders 
)

/*
,ToBePivoted As
(
Select RptID,R.date1,R.RptType,Count(distinct V.ProviderID) UserCount 
,'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
from RptTypesWithDates R left join Patients V
on V.Date1=R.Date1 and V.RptType=R.RptType  group by R.date1,R.RptType,RptID
)
		
select 

 RptType
,[Column # 1] as [Column # 1] 
,[Column # 2] as [Column # 2]
,[Column # 3] as [Column # 3]
,[Column # 4] as [Column # 4] 
,[Column # 5] as [Column # 5]
,[Column # 6] as [Column # 6]
,[Column # 7] as [Column # 7]
,[Column # 8] as [Column # 8] 
,[Column # 9] as [Column # 9]
,[Column # 10] as [Column # 10]
,[Column # 11] as [Column # 11]
,[Column # 12] as [Column # 12]


from
(
select	RptType,ColumnIndex,UserCount,RptID
		from ToBePivoted
) A
PIVOT
(
SUM (UserCount)
FOR ColumnIndex IN
( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
 , [Column # 10],[Column # 11],[Column # 12] )
) B

ORDER BY RptID

*/

Select RptID,R.date1,R.RptType,Count(distinct V.ProviderID) UserCount 
,'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
into #ToBePivoted
from RptTypesWithDates R left join Patients V
on V.Date1=R.Date1 and V.RptType=R.RptType  group by R.date1,R.RptType,RptID

SELECT DISTINCT ColumnIndex INTO #ColumnsIndex FROM #ToBePivoted
 
select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1

DECLARE @MaxCount INTEGER
DECLARE @Count INTEGER
DECLARE @Txt VARCHAR(MAX)
SET @Count = 1
SET @Txt = '['
SET @MaxCount = (SELECT MAX(RowID) FROM #c)
WHILE @Count<=@MaxCount
    BEGIN
    IF @Txt!=''
        SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
    ELSE
        SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
    SET @Count=@Count+1
    END
--SELECT @Txt AS Txt

SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

SET @Cols = @Txt
/*
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
--                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
 
*/ 
SET @query = N'SELECT RptType, '+ @cols +' FROM
(SELECT RptType, ColumnINdex, UserCount,RptID FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'
 
EXECUTE(@query) 
END
