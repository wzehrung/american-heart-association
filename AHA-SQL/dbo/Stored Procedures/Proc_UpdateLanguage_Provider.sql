﻿create procedure [dbo].[Proc_UpdateLanguage_Provider]
(
	@LanguageID int,
	@ProviderID int
)
as
begin
	update Provider set DefaultLanguageID = @LanguageID where ProviderID = @ProviderID
end 