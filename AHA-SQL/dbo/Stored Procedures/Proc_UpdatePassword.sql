﻿

CREATE PROCEDURE [dbo].[Proc_UpdatePassword]
( 
 @Password nvarchar(255)
,@ProviderID int 

)
As
Begin

set nocount on

Update provider 
set Password =@Password where ProviderID = @ProviderID  
	
End

