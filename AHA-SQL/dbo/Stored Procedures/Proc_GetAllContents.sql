﻿--[Proc_GetAllContents] 2

CREATE PROC [dbo].[Proc_GetAllContents]
(
@LanguageID int
)
As

Begin
set nocount on


;WITH Duplicate (RowNumber, ContentID, ContentRulesID, ContentRulesIDs) AS
(
  SELECT 1, C.ContentID, cast ((B.ContentRulesID) as varchar(20)), CAST(B.RuleName AS VARCHAR(8000)) 
  FROM ContentRuleMapping C
  INNER JOIN ContentRules B ON B.ContentRulesID=C.ContentRulesID and C.LanguageID=B.LanguageID and B.LanguageID=@LanguageID and C.LanguageID=@LanguageID
  GROUP BY ContentID,RuleName,B.ContentRulesID having B.ContentRulesID=min(C.ContentRulesID)

  UNION ALL

  SELECT CT.RowNumber + 1, C.ContentID, cast (B.ContentRulesID as varchar(20)) , CT.ContentRulesIDs + ', ' + cast (B.RuleName as varchar(20))
  FROM ContentRuleMapping C 
  INNER JOIN [ContentRules] B ON B.ContentRulesID=C.ContentRulesID and C.LanguageID=B.LanguageID and B.LanguageID=@LanguageID and C.LanguageID=@LanguageID
  INNER JOIN Duplicate CT ON CT.ContentID = C.ContentID 
  WHERE cast (C.ContentRulesID as varchar(20)) > CT.ContentRulesID
)

Select A.*,B.ContentRulesIDs from

Content A left join 
(
SELECT A.ContentID, ContentRulesIDs
from Duplicate A 
inner JOIN (SELECT ContentID, MAX(RowNumber) 
AS MaxRow FROM Duplicate GROUP BY ContentID) R
ON A.RowNumber = R.MaxRow AND A.ContentID = R.ContentID
) B

on A.ContentID=B.ContentID where A.LanguageID=@LanguageID 


End
