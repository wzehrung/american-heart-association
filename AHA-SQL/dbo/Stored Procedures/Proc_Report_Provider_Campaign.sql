﻿--select * from Provider

--[PROC_Report_Campaign_Patients] '2009.06.01','2010.05.05'

CREATE PROC [dbo].[Proc_Report_Provider_Campaign]
(
 @StartDate Datetime
,@EndDate Datetime
,@CampaignID NVARCHAR(MAX)
,@UserTypeID int = null
,@PartnerID int = null
)
As

BEGIN
set nocount on


DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

;with RptTypes As
(
select C.CampaignID,Title,CampaignCode,StartDate,EndDate from Campaign C inner join CampaignDetails CD on C.CampaignID=CD.CampaignID
and LanguageID=1
),

RptTypesWithDates As
(
select *,Case when (StartDate>=date1 and  StartDate < Date2) or (EndDate >=date1 and EndDate< Date2) then 1 else 0 end isEligible
				from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
)
,CampaignCount As
(
select CampaignID,Count(CampaignID) As TotalCount from Provider 
where CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignID,','))
  and (UserTypeID=@UserTypeID or @UserTypeID IS NULL)
  and (ProviderID=@PartnerID or @PartnerID IS NULL)
group by CampaignID
)
,Result As
(
select Date1,Title,case when isEligible=1 then ISNULL(TotalCount,0) else 0 End As UserCount,ISeLIGIBLE
from RptTypesWithDates R left join CampaignCount C on R.CampaignID=C.CampaignID
)

/*
,ToBePivoted As
(
Select date1,Title, UserCount
,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
from Result
)
		
select 

 Title
,[Column # 1] as [Column # 1] 
,[Column # 2] as [Column # 2]
,[Column # 3] as [Column # 3]
,[Column # 4] as [Column # 4] 
,[Column # 5] as [Column # 5]
,[Column # 6] as [Column # 6]
,[Column # 7] as [Column # 7]
,[Column # 8] as [Column # 8] 
,[Column # 9] as [Column # 9]
,[Column # 10] as [Column # 10]
,[Column # 11] as [Column # 11]
,[Column # 12] as [Column # 12]


from
(
select	Title,ColumnIndex,UserCount
		from ToBePivoted
) A
PIVOT
(
SUM (UserCount)
FOR ColumnIndex IN
( [Column # 1] , [Column # 2],[Column # 3],[Column # 4] , [Column # 5],[Column # 6],[Column # 7] , [Column # 8],[Column # 9]
 , [Column # 10],[Column # 11],[Column # 12] )
) B

ORDER BY Title
*/

Select date1,Title, UserCount
,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex
into #ToBePivoted
from Result


SELECT DISTINCT ColumnIndex INTO #ColumnsIndex FROM #ToBePivoted
 
 
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
--                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
 
 
SET @query = N'SELECT Title, '+ @cols +' FROM
(SELECT Title, ColumnINdex, UserCount FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'
 
EXECUTE(@query) 
END
