﻿

-- =============================================
-- Author:		Phil Brock
-- Create date: 06.11.2013
-- Description:	refactoring of Proc_Report_H360Users_Campaign for V6
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_H360Users_Campaign] 
(  
 @StartDate Datetime,  
 @EndDate Datetime,
 @CampaignID int = null,	-- null specifies all campaigns
 @Markets NVARCHAR(MAX),		-- PSV (P=Pipe) list of MarketId (CBSA_Code from ZipRegionMapping table)
 @CampaignIDs nvarchar(max)
)  
As  
  
BEGIN  
DECLARE @cols NVARCHAR(2000)
DECLARE @query NVARCHAR(4000)

set nocount on
--
-- Create a temp table containing the different report types we need to generate
--
--drop table #RptTypes
create table #RptTypes (RptID int, RptType varchar(255))
insert into #RptTypes values (1, 'Heart360 New Users')
insert into #RptTypes values (2, 'Heart360 Total Users at end of month')
insert into #RptTypes values (3, 'Heart360 Total Active Users this month')
insert into #RptTypes values (4, 'Heart360 users opted to provide their email address')
insert into #RptTypes values (5, 'Heart360 Total Spanish Users at end of month')

--
-- Create a temp table containing a column for report type and every month within the date range
--
--drop table #RptTypesWithDates
SELECT * INTO #RptTypesWithDates from  #RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate) 

--
-- Create a temp table to contain all the zip codes that relate to given Markets
-- Also keep a counter of how many zip codes are in the temp table
--
-- drop table #RptMarketZipCodes
DECLARE @ZipCodeCount int

	
SELECT Zip_Code INTO #RptMarketZipCodes 
	from ZipRegionMapping 
	where CBSA_Code IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	-- this line below was where I expected markets to be the CBSA_Name
	-- the +4 after charindex to add ', XX' where = 2-letter state abbr.
	--where SUBSTRING(CBSA_Name,0,CHARINDEX(',',CBSA_Name)+4) IN (SELECT VALUE FROM dbo.udf_ListToTableString(@Markets,'|'))
	

SELECT @ZipCodeCount = COUNT(*) FROM #RptMarketZipCodes 

--
-- Create yet another temp table containing all the users who are in the given campaign(s) and markets
--
-- drop table #RptUsers

--IF @CampaignIDs IS NOT NULL
--	BEGIN
		
		SELECT HR.UserHealthRecordID, AU.AHAUserID, HR.CreatedDate, AU.LoggedInDate, HR.CampaignID INTO #RptUsers 
			FROM HealthRecord HR
			LEFT JOIN ExtendedProfile EP ON EP.UserHealthRecordID = HR.UserHealthRecordID
			LEFT JOIN AHAUser AR ON AR.SelfHealthRecordId = HR.UserHealthRecordID
			LEFT JOIN AHAUserLoginDetails AU ON AR.UserId = AU.AHAUserID	
		WHERE 

			@CampaignIDs IS NULL AND HR.CampaignID IS NOT NULL
			--OR (@CampaignIDs IS NOT NULL AND @CampaignIDs = HR.CampaignID)
			OR (HR.CampaignID IN (SELECT VALUE FROM dbo.udf_ListToTableInt(@CampaignIDs,',')))
			AND (@ZipCodeCount = 0 or EP.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
			AND (AR.UserID IS NOT NULL)

--	END
--ELSE
--	BEGIN

--		SELECT HR.UserHealthRecordID, AU.AHAUserID, HR.CreatedDate, AU.LoggedInDate, HR.CampaignID INTO #RptUsers2 
--		FROM HealthRecord HR
--			LEFT JOIN ExtendedProfile EP ON EP.UserHealthRecordID = HR.UserHealthRecordID
--			LEFT JOIN AHAUser AR ON AR.SelfHealthRecordId = HR.UserHealthRecordID
--			LEFT JOIN AHAUserLoginDetails AU ON AR.UserId = AU.AHAUserID
--		WHERE
--			((@CampaignID IS NULL AND HR.CampaignID IS NOT NULL) OR (@CampaignID IS NOT NULL AND @CampaignId = HR.CampaignID) )
--			AND (@ZipCodeCount = 0 or EP.ZipCode IN (SELECT Zip_Code FROM #RptMarketZipCodes))
--			AND (AR.UserID IS NOT NULL)
--	END

--
-- Now generate the reports
--
--drop table #result

-- Report type 3: active users
SELECT RptID,R.Date1,COUNT(DISTINCT RU.AHAUserID) UserCount,R.RptType INTO #result 
	FROM #RptTypesWithDates R 
	LEFT JOIN #RptUsers RU ON RU.LoggedInDate >= R.Date1 and RU.LoggedInDate < R.Date2
	WHERE R.RptID=3   
	GROUP BY R.Date1,R.RptType,RptID 
 
UNION ALL

-- Report type 2: total users at end of month
SELECT RptID,R.Date1,count(distinct RU.AHAUserID)  UserCount, R.RptType  
	FROM #RptTypesWithDates R 
	LEFT JOIN #RptUsers RU ON RU.CreatedDate < R.Date2 
	WHERE R.RptID=2   
	GROUP BY R.Date1,R.RptType,RptID  
  
UNION ALL  

-- Report type 1: new users
SELECT RptID,R.Date1,count(distinct RU.AHAUserID)  UserCount,R.RptType  
	FROM #RptTypesWithDates R 
	LEFT JOIN #RptUsers RU ON RU.CreatedDate >= R.Date1 and  RU.CreatedDate < R.Date2   
	WHERE  R.RptID=1   
	GROUP BY R.Date1,R.RptType,RptID  
  
UNION ALL  

-- Report type 4: user opted to provide email address
SELECT RptID,R.Date1,count(distinct RU.AHAUserID)  UserCount,R.RptType  
	FROM #RptTypesWithDates R 
	LEFT JOIN AHAUser A ON (A.AllowAHAEmailUpdatedDate < R.Date2 and A.AllowAHAEmail=1)
	LEFT JOIN #RptUsers RU ON RU.UserHealthRecordID = A.SelfHealthRecordId
	where R.RptID=4   
	GROUP BY R.Date1,R.RptType,RptID  
 
UNION ALL  

-- Report type 5: total spanish users at end of month
SELECT RptID,R.Date1,count(distinct RU.AHAUserID)  UserCount,R.RptType
	FROM #RptTypesWithDates R
	LEFT JOIN AHAUser A ON A.DefaultLanguageID = 2
	LEFT JOIN #RptUsers RU ON (RU.CreatedDate < R.Date2 and A.SelfHealthRecordID = RU.UserHealthRecordID)
	WHERE R.RptID=5   
	GROUP BY R.Date1,R.RptType,RptID  
 
Select date1,RptType,UserCount,RptID   
,'Column # ' + cast(dense_Rank() over (order by date1) as varchar(10)) ColumnIndex  
,cast(dense_Rank() over (order by Date1) as int) IntIndex
into #ToBePivoted
from #Result  

SELECT DISTINCT ColumnIndex, IntIndex INTO #ColumnsIndex FROM #ToBePivoted
 
 
SELECT  @cols = STUFF(( SELECT TOP 100 PERCENT
                                '],[' + t.ColumnIndex
                        FROM #ColumnsIndex AS t
                        ORDER BY t.IntIndex
                        FOR XML PATH('')
                      ), 1, 2, '') + ']'
 
 
SET @query = N'SELECT RptType, '+ @cols +' FROM
(SELECT RptType, ColumnINdex, UserCount,RptID FROM #ToBePivoted ) A
PIVOT (SUM(UserCount) FOR ColumnIndex IN ( '+ @cols +' )) B;'
 
EXECUTE(@query) 
  
END



