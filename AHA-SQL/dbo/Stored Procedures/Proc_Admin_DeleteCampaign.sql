﻿
-- =============================================
-- Author:		Wade Zehrung
-- Create date: 2015-10-13
-- Description:	Add ability to delete a campaign
-- =============================================

CREATE PROCEDURE [dbo].[Proc_Admin_DeleteCampaign]
(
    @CampaignID int
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM CampaignDetails WHERE CampaignID = @CampaignID

	DELETE FROM Campaign WHERE CampaignID = @CampaignID

	-- If a user was assigned to specified campaign, set thier campign to NULL

	UPDATE HealthRecord SET CampaignID = NULL WHERE CampaignID = @CampaignID

END

