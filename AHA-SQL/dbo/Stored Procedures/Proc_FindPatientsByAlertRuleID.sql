﻿
create proc [dbo].[Proc_FindPatientsByAlertRuleID]
(
@AlertRuleID int
)

As

Begin

;WITH Patients as
(
	select distinct C.PatientID,CASE WHEN d.PatientId is null then 0 else 1 end HasAlertRule
	 from AlertRule A inner join PatientGroupDetails B
	on A.ProviderID = B.ProviderID and A.AlertRuleID=@AlertRuleID
	inner join PatientGroupMapping C on B.GroupID=C.GroupID
	left join AlertMapping D on A.AlertRuleID=D.AlertRuleID 
	and C.PatientID = D.PatientID
)

select H.* ,B.HasAlertRule from HealthRecord H inner join Patients B on H.UserHealthRecordID=B.PatientID


End
