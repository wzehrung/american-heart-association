﻿


CREATE Procedure [dbo].[Proc_DeleteHealthRecord]  
(  
 @UserHealthRecordID int
,@isFromAHAUser bit
)  
AS  
Begin  
  
--This Sp should not be called from within application/product  
  
/* Author   :Shipin Anand  
   Created Date     :2008-07-04    
   Description      :Delete HealthRecord Record in two ways  
  
      1: Delete by HealthRecordID(Deleting from Ahauser Table)  
      ----------------------------  
       Step 1: Delete HealthRecord by HealthRecordID  
       Step 2: Delet all HealthRecord related items  
  
      2: Delete by HealthRecordGUID(Deleting from HealthRecord table directly)  
       ----------------------------  
     Step 1: Delete HealthRecord by HealthRecordGUID  
     Step 2:  set selfHealthRecordid as null in AHAuser table 
     Step 3: Delet all HealthRecord related items  
  
*/  
  

if @isFromAHAUser=1   --1  
begin  
--Step 2:  
 Exec Proc_TruncateHealthItems @UserHealthRecordID  
--Step 1:  
Delete from dbo.HealthRecord  
       Where UserHealthRecordID= @UserHealthRecordID 

  
end  
  
else if @isFromAHAUser=0  --2  
Begin  

--Step 3:  
 Exec Proc_TruncateHealthItems @UserHealthRecordID 

--Step 2:  
 Update dbo.AHaUser set 
	   SelfHealthRecordID = NULL
	  ,SelfPersonalItemID = NULL
      ,UpdatedDate=getDate()  
      Where SelfPersonalItemID= @UserHealthRecordID 
 
 
--Step 1:  
 Delete from  dbo.HealthRecord  
       Where UserHealthRecordID= @UserHealthRecordID

  
End  
  
End  
  
  



