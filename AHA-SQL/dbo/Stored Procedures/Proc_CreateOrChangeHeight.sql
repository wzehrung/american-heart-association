﻿



CREATE Procedure [dbo].[Proc_CreateOrChangeHeight]
(
 @PersonalItemGUID uniqueidentifier 
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@Height float  
,@Comments nvarchar (max) 
,@DateMeasured datetime  
)
AS

Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :modification/insertion (Height table )
					 Rule : if exists same Itemguid,HealthGuid and
					 VersionGUID then Update the table
					 else  Insert a new entry
*/

set nocount on

Begin Try
Begin Transaction

if exists(
		  select HeightID from dbo.Height where 
		  HVItemGuid=@HVItemGuid 
		  )

Exec Proc_UpdateHeight	     @HVItemGuid 
							,@HVVersionStamp 
							,@Height 
							,@Comments 
							,@DateMeasured 
												

else 
begin

Declare @UserHealthRecordID int
select @UserHealthRecordID = UserHealthRecordID  from dbo.HealthRecord
							 where PersonalItemGUID=@PersonalItemGUID
							 	


Exec Proc_InsertHeight	     @UserHealthRecordID 
							,@HVItemGuid 
							,@HVVersionStamp 
							,@Height 
							,@Comments 
							,@DateMeasured 
end


exec proc_UpdateHealthRecord_LastActivityDate @PersonalItemGUID



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
												
End








