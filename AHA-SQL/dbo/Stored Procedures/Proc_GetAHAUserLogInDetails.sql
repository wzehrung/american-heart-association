﻿




CREATE Procedure [dbo].[Proc_GetAHAUserLogInDetails]
(
  @PersonGUID UniqueIdentifier 
 ,@LastVistedDate Datetime Output
 ,@CurrentLoginTime Datetime Output
 ,@NoOfLogIns int Output
)

As

Begin 

/* Author           :Shipin Anand  
   Created Date     :2008-07-10    
   Description      :Getting LogInDetails Date for the given personID
					 Step 1: Fetch LoginDetails for that PersonID and sort
                             by logindate
							 (Details are stored in AHAUserLoginDetails table
							   for each and every login)
					 step 2:Fetch total np.of visits,Current Login time and
							Last Login Time
*/ 

set nocount on

Declare @UserID int
select @UserID= UserID from dbo.AHAUser where PersonGUID=@PersonGUID 

Declare  @AHAUserLogInCached table
(
 Rank int
,LoggedInDate Datetime
)
 
insert @AHAUserLogInCached
select  Row_Number() Over (Order by LoggedInDate desc) As  Rank
       ,LoggedInDate from dbo.AHAUserLoginDetails 
		where AHAUserID=@UserID 


select @NoOfLogIns=max(rank) from @AHAUserLogInCached 
select @CurrentLoginTime=LoggedInDate from @AHAUserLogInCached where rank=1
select @LastVistedDate=LoggedInDate from @AHAUserLogInCached where rank=2


End




