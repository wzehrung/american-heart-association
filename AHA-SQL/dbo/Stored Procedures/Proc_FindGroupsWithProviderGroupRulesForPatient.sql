﻿
--[Proc_FindGroupsWithProviderGroupRulesForPatient] 4

CREATE PROCEDURE Proc_FindGroupsWithProviderGroupRulesForPatient
(
@PatientID int
)
As
Begin
set nocount on

DECLARE @Providers TABLE
(
ProviderID int
,Date datetime
)

insert @Providers

select distinct ProviderID,date from PatientGroupDetails PGD inner join
PatientGroupMapping  PGM ON  PGD.GroupID=PGM.GroupID and  PGM.PatientID=@PatientID and isDefault=1


SELECT
 
DISTINCT 

PGD.GroupID
,Name,Description
,pgd.ProviderID
,DateCreated
,isDefault,Cast(GroupRuleXml as nvarchar(max)) GroupRuleXml
,RemoveOption
,Date ProviderConnectionDate
  FROM  PatientGroupDetails PGD  
  inner join @Providers C on PGD.ProviderID=C.ProviderID 
 WHERE GroupRuleXml IS NOT NULL 

End
