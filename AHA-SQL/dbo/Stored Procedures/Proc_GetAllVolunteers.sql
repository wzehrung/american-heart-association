﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: 05.23.2013
-- Description:	Get all volunteers for Admin Tool
-- =============================================
CREATE PROCEDURE [dbo].[Proc_GetAllVolunteers] 
(
 @OnlyNullCampaigns bit
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @OnlyNullCampaigns = 1
	BEGIN
		SELECT 
			pp.ProviderID as UserID,
			pp.LastName,
			pp.FirstName,
			pp.Email,
			NULL as CampaignID,
			NULL as CampaignName
			FROM Provider pp WHERE pp.CampaignID is null AND pp.UserTypeID = 2 
		ORDER BY LastName, FirstName, Email
	END
	ELSE
	BEGIN
		SELECT 
			p.ProviderID as UserID,
			p.LastName,
			p.FirstName,
			p.Email,
			p.CampaignID,
			cd.Title as CampaignName
			FROM Provider p
			INNER JOIN CampaignDetails cd
			ON p.CampaignID = cd.CampaignID
			WHERE cd.LanguageID = 1 AND p.UserTypeID = 2
		UNION	
		SELECT 
			pp.ProviderID as UserID,
			pp.LastName,
			pp.FirstName,
			pp.Email,
			NULL as CampaignID,
			NULL as CampaignName
			FROM Provider pp WHERE pp.CampaignID is null AND pp.UserTypeID = 2
		ORDER BY LastName, FirstName, Email
	END
END

