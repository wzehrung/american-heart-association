﻿




Create PROCEDURE [dbo].[Proc_GetNumberOfProvidersForPatient]
(
@PatientID int
)
As
Begin
set nocount on

    select Count(distinct ProviderID) As NoOfProviders
	from PatientGroupDetails  PGD 
	inner join
	PatientGroupMapping PGM 
	on PGD.GroupID=PGM.GroupID and  PGM.PatientID=@PatientID 
	
	
End





