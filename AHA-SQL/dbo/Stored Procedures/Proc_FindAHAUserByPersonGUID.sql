﻿
CREATE proc [dbo].[Proc_FindAHAUserByPersonGUID]
(
@PersonGUID uniqueidentifier 
)

AS

Begin
set nocount on

select a.*,l.LanguageLocale from AHAUser a
inner join Languages l on a.DefaultLanguageID = l.LanguageID
where a.PersonGUID = @PersonGUID


End

