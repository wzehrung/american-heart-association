﻿

CREATE PROC [dbo].[PROC_DeleteMessage]
(
  @MessageID int
 ,@UserID INT
 ,@UserType NVARCHAR(50)
 ,@deletefromHV bit output
)
As
Begin
set nocount on


Begin Try
Begin Transaction


declare @UserFolderMappingID int
declare @FolderType char
declare @UserFolderMappingIDCurrent int

set @deletefromHV=0

IF @UserType='Patient'
begin

	select  @FolderType = FolderType, @UserFolderMappingIDCurrent=UFM.UserFolderMappingID

	from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.PatientID=@UserID 
	inner join Messagedetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
	and MD.MessageID=@MessageID and MD.PatientID=UFM.PatientID
	

	if @FolderType <>'T'
	begin

	select @UserFolderMappingID=UserFolderMappingID from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.PatientID=@UserID and FolderType='T'

	Update MessageDetails set UserFolderMappingID=@UserFolderMappingID where MessageID=@MessageID
	AND PatientID=@UserID and UserFolderMappingID=@UserFolderMappingIDCurrent

	end
	else

	Begin
	Delete from MessageDetails where MessageID=@MessageID and PatientID=@UserID  and UserFolderMappingID=@UserFolderMappingIDCurrent
	if not exists (select * from MessageDetails where MessageID=@MessageID)
	begin
	delete from [Message] where MessageID=@MessageID
	set @deletefromHV=1
	end

    end

End



else IF @UserType='Provider'
begin

	select  @FolderType = FolderType, @UserFolderMappingIDCurrent=UFM.UserFolderMappingID

	from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.ProviderID=@UserID 
	inner join Messagedetails MD on UFM.UserFolderMappingID=MD.UserFolderMappingID
	and MD.MessageID=@MessageID and MD.ProviderID=UFM.ProviderID


	if @FolderType <>'T'
	begin

	select @UserFolderMappingID=UserFolderMappingID from UserFolderMapping UFM  Inner join Folder F
	on UFM.FolderID=F.FolderID and UFM.ProviderID=@UserID and FolderType='T'

	Update MessageDetails set UserFolderMappingID=@UserFolderMappingID where MessageID=@MessageID
	AND ProviderID=@UserID and UserFolderMappingID=@UserFolderMappingIDCurrent

	end
	else

	Begin
	Delete from MessageDetails where MessageID=@MessageID and ProviderID=@UserID  and UserFolderMappingID=@UserFolderMappingIDCurrent
	if not exists (select * from MessageDetails where MessageID=@MessageID)
	begin
	delete from [Message] where MessageID=@MessageID
	set @deletefromHV=1
	end
    end

End


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End


