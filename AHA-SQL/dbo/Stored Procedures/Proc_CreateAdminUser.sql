﻿CREATE PROC [dbo].[Proc_CreateAdminUser]
(
	 @Name nvarchar(128)
	,@UserName nvarchar(128)
	,@Password nvarchar(50)
	,@Email nvarchar(128)
	,@RoleID int
	,@BusinessID int
	,@LanguageID int
	,@AdminUserID INT OUTPUT
)

AS

BEGIN
SET NOCOUNT ON

INSERT AdminUser
	(
		Name,
		UserName,
		Password,
		Email,
		RoleID,
		BusinessID,
		DefaultLanguageID
	)

SELECT 
	@Name,
	@UserName,
	@Password,
	@Email,
	@RoleID,
	@BusinessID,
	@LanguageID

SET @AdminUserID=scope_identity()

END

