﻿
-- =============================================
-- Authors:		Phil Brock/Wade Zehrung
-- Create date:	2.12.2014
-- Updated date:	2.24.2014
-- Description:	Refactor of V6_Proc_Report_Provider_H360Providers to handle
--				new requirements from AHA dealing with Provider Specialty sub-items
--				Needed to create a rollup for the non-campaign report
-- =============================================
CREATE PROCEDURE [dbo].[V6_Proc_Report_Provider_H360Providers_RollUp] 
(  
    @StartDate Datetime,  
    @EndDate Datetime
)
AS
BEGIN
	DECLARE @cols NVARCHAR(2000)
	DECLARE @query NVARCHAR(4000)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--
	-- Create yet another temp table containing all the providers
	--
	-- drop table #RptProviders
	SELECT P.ProviderID, P.DateOfRegistration, P.DefaultLanguageID, 
	   Case 
		  when P.Speciality = 'Cardiologist' then 'Cardiologists'
		  when P.Speciality = 'AHA Volunteer' then 'Volunteers'
		  when P.Speciality is not null AND P.Speciality != 'Cardiologist' AND P.Speciality != 'AHA Volunteer' then 'Other HCPs'
		  when P.Speciality is null then 'Unknown'
	   End ProvSpecialty
    INTO #RptProviders
    FROM Provider P
 
	--
	-- Now generate the reports
	--
    
    ;with RptTypes As
	(
		select 1 RptID,'New' As RptType, 'All' as RptSpecialty
		union all
		select 2 RptID,'New' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 3 RptID,'New' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 4 RptID,'New' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 5 RptID,'New' As RptType, 'Unknown' as RptSpecialty
		union all
		select 6 RptID,'Total at end of month' As RptType, 'All' as RptSpecialty
		union all
		select 7 RptID,'Total at end of month' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 8 RptID,'Total at end of month' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 9 RptID,'Total at end of month' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 10 RptID,'Total at end of month' As RptType, 'Unknown' as RptSpecialty
		union all
		select 11 RptID,'Total Active this month' As RptType, 'All' as RptSpecialty
		union all
		select 12 RptID,'Total Active this month' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 13 RptID,'Total Active this month' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 14 RptID,'Total Active this month' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 15 RptID,'Total Active this month' As RptType, 'Unknown' as RptSpecialty
		union all
		select 16 RptID,'Total Spanish at end of month' As RptType, 'All' as RptSpecialty
		union all
		select 17 RptID,'Total Spanish at end of month' As RptType, 'Cardiologists' as RptSpecialty
		union all
		select 18 RptID,'Total Spanish at end of month' As RptType, 'Other HCPs' as RptSpecialty
		union all
		select 19 RptID,'Total Spanish at end of month' As RptType, 'Volunteers' as RptSpecialty
		union all
		select 20 RptID,'Total Spanish at end of month' As RptType, 'Unknown' as RptSpecialty
	),
	RptTypesWithDates As
	(
		select * from  RptTypes cross join DBO.udf_GetDatesWithRange (@StartDate,@EndDate)
	)
	,Result As
	(
		--
		-- Reports 11-15 (Total Active this month)
		--
		select RptID, R.Date1,count(distinct H.ProviderID)  ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join ProviderLoginDetails A on A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2
			left join #RptProviders H on H.ProviderID = A.ProviderID  
			where  R.RptID=11
			GROUP BY R.Date1,R.RptType,RptID

		union all
		
		select RptID, R.Date1,count(distinct H.ProviderID)  ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join ProviderLoginDetails A on A.LoggedInDate >= R.Date1 and A.LoggedInDate < R.Date2
			left join #RptProviders H on H.ProviderID = A.ProviderID  
			where  R.RptID > 11 AND R.RptID < 16 AND R.RptSpecialty = H.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty

		union all

		--
		-- Reports 6-10 (Total at end of month)
		--
		select RptID, R.Date1, count(distinct A.ProviderID) ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 
			where  R.RptID=6
			GROUP BY R.Date1,R.RptType,RptID

		union all

		select RptID, R.Date1, count(distinct A.ProviderID) ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 
			where  R.RptID > 6 AND R.RptID < 11 AND R.RptSpecialty = A.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty
			
		union all

		--
		-- Report 1-5 (New)
		--
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration >= R.Date1 and  A.DateOfRegistration < R.Date2
			where  R.RptID=1
			GROUP BY R.Date1,R.RptType,RptID

		union all
		
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration >= R.Date1 and  A.DateOfRegistration < R.Date2
			where  R.RptID > 1 AND R.RptID < 6 AND R.RptSpecialty = A.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty

		union all

		--
		-- Reports 16-20 (Total Spanish at end of month)
		--
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, 'All' as RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 and A.DefaultLanguageID=2
			where R.RptID=16
			GROUP BY R.Date1,R.RptType,RptID
			
		union all
		
		select RptID, R.Date1, count(distinct A.ProviderID)  ProviderCount, R.RptType, R.RptSpecialty
			from RptTypesWithDates R 
			Left join #RptProviders A on A.DateOfRegistration < R.Date2 and A.DefaultLanguageID=2
			where R.RptID > 16 AND R.RptID < 21 AND R.RptSpecialty = A.ProvSpecialty
			GROUP BY R.Date1,R.RptType,RptID,R.RptSpecialty

	)

	
	Select R.date1, R.RptType, S.ProviderCount, R.RptID, R.RptSpecialty,
		   'Column # ' + cast(dense_Rank() over (order by R.date1) as varchar(10)) ColumnIndex
		into #ToBePivoted
		from RptTypesWithDates R
			-- left join will force the creation of RptType
		left join Result S ON S.date1 = R.date1 AND S.RptType=R.RptType and S.RptSpecialty=R.RptSpecialty
		order by date1


	select distinct columnindex, RowID = IDENTITY(INT,1,1), date1 into #c from #ToBePivoted order by date1


	DECLARE @MaxCount INTEGER
	DECLARE @Count INTEGER
	DECLARE @Txt VARCHAR(MAX)
	SET @Count = 1
	SET @Txt = '['
	SET @MaxCount = (SELECT MAX(RowID) FROM #c)
	WHILE @Count<=@MaxCount
		BEGIN
		IF @Txt!=''
			SET @Txt=@Txt+ (SELECT columnindex FROM #c WHERE RowID=@Count) + '],['
		ELSE
			SET @Txt=(SELECT columnindex FROM #c WHERE RowID=@Count)
		SET @Count=@Count+1
		END

	SELECT  @Txt = LEFT(@Txt,LEN(@Txt)-2)

	SET @Cols = @Txt
	
	-- PJB: Need to order by RptID because one of the RptType names has a symbol
	SET @query = N'with Pivoted as ( SELECT RptID, RptType, RptSpecialty, '+ @cols +' FROM
	(SELECT RptType, RptSpecialty, ColumnINdex, ProviderCount,RptID FROM #ToBePivoted ) A 
	PIVOT (SUM(ProviderCount) FOR ColumnIndex IN ( '+ @cols +' )) B  ) SELECT RptType, RptSpecialty, ' + @cols + ' FROM Pivoted Order By RptID'

	EXECUTE(@query)

END
