﻿
CREATE PROC [dbo].[Proc_GetAllAdminUsers]

AS

BEGIN
SET NOCOUNT ON

SELECT 
	au.AdminUserID
    ,au.Name
    ,au.UserName
    ,au.Password
    ,au.Email
    ,au.IsActive
    --,au.IsSuperAdmin
    ,au.DefaultLanguageID
    ,au.RoleID
    ,ar.Name AS RoleName
	,ab.BusinessID
	,ab.Name AS BusinessName

FROM AdminUser au

	INNER JOIN AdminRoles ar
		ON au.RoleID = ar.RoleID

	LEFT JOIN AdminBusiness ab
		ON au.BusinessID = ab.BusinessID

END
