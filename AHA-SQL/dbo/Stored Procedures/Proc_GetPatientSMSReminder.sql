﻿
CREATE Procedure [dbo].[Proc_GetPatientSMSReminder]
(
 @PatientID Int
)
AS

Begin
--This Sp should not be called from within application/product

/* Author			:Anees Shaik
   Created Date	    :2012-08-18  
   Description      :inserting record to Health Record table
*/
set nocount on


SELECT 	ReminderID
		,PatientID
		,MobileNumber					
		,PIN
		,TimeZone
		,Terms
		,PinValidated
		,CreatedDateTime
		,UpdatedDateTime
FROM dbo.[3CiPatientReminder]
WHERE PatientID = @PatientID 
					


End
