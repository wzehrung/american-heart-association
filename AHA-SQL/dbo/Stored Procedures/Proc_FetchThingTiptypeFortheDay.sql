﻿--[Proc_FetchThingTiptypeFortheDay] 'Blood Glucose'

CREATE Proc [dbo].[Proc_FetchThingTiptypeFortheDay]
(
 @ThingTipsType nvarchar(100)
,@LanguageID int
)

As

Begin


set nocount on

/* Author			:Shipin Anand
   Created Date	    :2008-07-21  
   Description      :Randomly selecting Thingtipsdata for a day for a give Type
					
*/

select TOP 1 TT.ThingTipsData from 

					dbo.ThingTipsType TTT 
					inner join ThingTipsMapping  TTM on
					TTM.ThingTipsTypeId=TTT.ThingTipsTypeId   and TTT.LanguageID=TTM.LanguageID and TTT.LanguageID=@LanguageID  and TTM.LanguageID=@LanguageID 
					inner join 
					dbo.ThingTips TT on
					TT.ThingTipsID=TTM.ThingTipsId and TT.LanguageID=TTM.LanguageID 
					and TTT.ThingTipsType=@ThingTipsType
                    and TT.LanguageID=@LanguageID    AND TTM.LanguageID=@LanguageID 
					Order by RAND((1000 * TT.ThingTipsId) * DATEPART(Day, GETDATE()))
End



