﻿

CREATE PROC [dbo].[PROC_MoveMessageToFolder]
(
 @MessageID int
,@UserType nvarchar(50) 
,@UserID int 
,@FolderType char
)
As
Begin
set nocount on

Begin Try
Begin Transaction


declare @UserFolderMappingID int

IF @UserType='Patient'
BEGIN

select @UserFolderMappingID=UserFolderMappingID from UserFolderMapping UFM  Inner join Folder F
on UFM.FolderID=F.FolderID and UFM.PatientiD=@UserID and FolderType=@FolderType

Update MessageDetails set UserFolderMappingID=@UserFolderMappingID where MessageID=@MessageID
AND PatientiD=@UserID 

End

else IF @UserType='Provider'
BEGIN

select @UserFolderMappingID=UserFolderMappingID from UserFolderMapping UFM  Inner join Folder F
on UFM.FolderID=F.FolderID and UFM.ProviderID=@UserID and FolderType=@FolderType

Update MessageDetails set UserFolderMappingID=@UserFolderMappingID where MessageID=@MessageID
AND ProviderID=@UserID 

End


COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch

End


