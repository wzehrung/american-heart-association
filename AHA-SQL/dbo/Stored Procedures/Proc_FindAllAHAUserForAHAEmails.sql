﻿CREATE PROCEDURE [dbo].[Proc_FindAllAHAUserForAHAEmails]
(
@CampaignID int = null
)

As
Begin
set nocount on

	select distinct A.* from AHAUser  A inner join AHAUserHealthRecord AHR on A.UserID=AHR.UserID and A.AllowAHAEmail=1 
	inner join HealthRecord HR on HR.UserHealthRecordID=AHR.UserHealthRecordID
	where (CampaignID=@CampaignID or @CampaignID is null)
	
End
