﻿

CREATE Procedure [dbo].[Proc_DropHealthRecord]
(
@PersonalItemGUID uniqueidentifier
)
AS
Begin


/* Author			:Shipin Anand
   Created Date	    :2008-07-04  
   Description      :delete record from  HealthRecord table..
					 2 steps : find comments in inner stored procs
*/

set nocount on

Declare @HealthRecordID int

Begin Try
Begin Transaction

select @HealthRecordID = UserHealthRecordID from dbo.HealthRecord
						 where PersonalItemGUID=@PersonalItemGUID

exec Proc_DeleteAHAUserHealthRecord  null,@HealthRecordID
exec Proc_DeleteHealthRecord  @HealthRecordID,0

COMMIT

End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch
End


