﻿
CREATE Procedure [dbo].[Proc_InsertBloodGlucose]
(
 @UserHealthRecordID int  
,@HVItemGuid uniqueidentifier  
,@HVVersionStamp uniqueidentifier  
,@BloodGlucoseValue float  
,@ActionITook  nvarchar(100)
,@ReadingType  nvarchar(100)
,@DateMeasured datetime 
,@Notes nvarchar(MAX) 
,@ReadingSource nvarchar(100) 
)
AS

Begin

--This Sp should not be called from within application/product  
  
/* Author			:Shipin Anand  
   Created Date     :2008-07-04    
   Description      :Insert Record to BloodGlucose
*/  

Insert dbo.BloodGlucose 
					(
                       UserHealthRecordID
                      ,HVItemGuid
                      ,HVVersionStamp
                      ,BloodGlucoseValue
					  ,ActionITook
					  ,ReadingType
                      ,DateMeasured
                      ,Notes  
					  ,ReadingSource                    
                    )
Select                 @UserHealthRecordID
                      ,@HVItemGuid
                      ,@HVVersionStamp
                      ,@BloodGlucoseValue
					  ,@ActionITook
					  ,@ReadingType
                      ,@DateMeasured
                      ,@Notes
					  ,@ReadingSource
                     
End







