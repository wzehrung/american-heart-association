﻿

CREATE PROCEDURE [dbo].[Proc_IsUserExists]
( 
  @Email nvarchar(255)
 ,@IsUserExists bit OUTPUT
)
As
Begin

set nocount on

set @IsUserExists=0

if exists (select * from provider where  Email=@Email)
set @IsUserExists=1
	
End


