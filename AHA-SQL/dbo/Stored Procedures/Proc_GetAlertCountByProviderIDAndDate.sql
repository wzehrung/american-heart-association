﻿
CREATE  proc Proc_GetAlertCountByProviderIDAndDate
(
 @ProviderID int
,@RequiredDate Datetime
,@Count int output
)

AS

Begin
set nocount on

select @Count = COUNT (DISTINCT C.AlertID)
from PatientGroupDetails A
inner join PatientGroupMapping B
on A.GroupID=B.GroupID and A.ProviderID=@ProviderID
INNER JOIN Alert C On B.PatientID=C.PatientID AND convert(datetime,convert(varchar,ItemEffectiveDate,103),103)=@RequiredDate
inner join AlertRule D on C.AlertRuleId=D.AlertRuleID-- AND ISACTIVE=1
and D.ProviderID = A.ProviderID
End
