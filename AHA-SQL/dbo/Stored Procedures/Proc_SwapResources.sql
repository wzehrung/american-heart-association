﻿
CREATE PROC [dbo].[Proc_SwapResources]
(
 @FirstResourceID int
,@SecondResourceID int
,@UpdatedByUserID INT
,@LanguageID int
)

As


Begin 
set nocount on


Begin Try
Begin Transaction	


Declare @SO1 int ,@SO2 int


select @SO1=SortOrder from Resources where ResourceID=@SecondResourceID and LanguageID=@LanguageID 
select @SO2=SortOrder from Resources where ResourceID=@FirstResourceID  and LanguageID=@LanguageID 

update Resources  set SortOrder=@SO1 ,UpdatedByUserID=@UpdatedByUserID where ResourceID=@FirstResourceID and LanguageID=@LanguageID
update Resources  set SortOrder=@SO2 ,UpdatedByUserID=@UpdatedByUserID where ResourceID=@SecondResourceID and LanguageID=@LanguageID



COMMIT
End Try

Begin Catch

 If @@TRANCOUNT > 0
	ROLLBACK

	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End


