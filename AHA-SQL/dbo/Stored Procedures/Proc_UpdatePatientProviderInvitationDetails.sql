﻿CREATE PROCEDURE [dbo].[Proc_UpdatePatientProviderInvitationDetails]
(
 @InvitationDetailsID int
,@DateAccepted datetime
,@Status int
,@AcceptedPatientID int
,@AcceptedProviderID int
)
As
Begin

UPDATE	patientproviderinvitationdetails 

SET		
		 DateAccepted = @DateAccepted
		,Status = @Status
		,AcceptedProviderID=@AcceptedProviderID
		,AcceptedPatientID=@AcceptedPatientID

WHERE	InvitationDetailsID=@InvitationDetailsID

End