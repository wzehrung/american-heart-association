﻿
create PROC GeneratePolkaID
(
@alpha_numeric NVARCHAR(6) OUTPUT
)
As

BEGIN  
  

SET @alpha_numeric=''

SELECT @alpha_numeric=@alpha_numeric+CHAR(n) FROM

(
    SELECT TOP 6 number AS n FROM master..spt_values

    WHERE TYPE='p' and (number between 48 and 57 or number between 65 and 90)

    ORDER BY NEWID() 

) AS t


                   
IF EXISTS (SELECT * FROM Healthrecord WHERE 
PolkaID=@alpha_numeric)
exec GeneratePolkaID @alpha_numeric output
 
     
END
