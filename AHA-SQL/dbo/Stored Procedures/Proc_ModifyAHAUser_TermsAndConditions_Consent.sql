﻿


CREATE Procedure [dbo].[Proc_ModifyAHAUser_TermsAndConditions_Consent]  
(  
 @PersonGUID uniqueidentifier
,@TermsAndConditions bit
,@Consent bit
)
AS  
  
Begin  

/* Author			:Shipin Anand  
   Created Date     :2008-07-31    
   Description      :Updating TermsAndConditions,Consent Columns of AHAUser for a PersonGUID  

   Note : Please dont try to update a indexed foreign key 
*/  

set nocount on  

Update dbo.AHAUser  
                SET 
					 TermsAndConditions	= @TermsAndConditions 
					,Consent			= @Consent

				where PersonGUID=@PersonGUID 
End


 


