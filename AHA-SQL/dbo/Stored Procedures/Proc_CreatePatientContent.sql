﻿
CREATE PROC [dbo].[Proc_CreatePatientContent]
(
 
 @ZoneTitle nvarchar(128)
,@ContentTitle nvarchar(128)
,@ContentTitleUrl nvarchar(1128)
,@ContentText nvarchar(MAX)
,@CreatedByUserID INT
,@LanguageID int
,@PatientContentID int 
,@ProviderID int=0
)
As
Begin

set nocount on

IF @PatientContentID IS NULL OR @PatientContentID =0
set @PatientContentID=(select ISNULL(MAX(PatientContentID), 0) + 1 from PatientContent )

IF(@ProviderID = 0)
begin
Insert PatientContent (PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,LanguageID,CreatedDate,UpdatedDate )
select @PatientContentID,@ZoneTitle,@ContentTitle,@ContentTitleUrl,@ContentText,@CreatedByUserID,@CreatedByUserID,@LanguageID,GETDATE(),GETDATE()
end
else
begin
Insert PatientContent (PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,LanguageID,ProviderID,CreatedDate,UpdatedDate )
select @PatientContentID,@ZoneTitle,@ContentTitle,@ContentTitleUrl,@ContentText,@CreatedByUserID,@CreatedByUserID,@LanguageID,@ProviderID,GETDATE(),GETDATE()
end
End


