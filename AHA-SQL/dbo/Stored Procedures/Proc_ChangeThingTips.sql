﻿
CREATE PROC [dbo].[Proc_ChangeThingTips]
(
 @ThingTipsData nvarchar(max)
,@ThingTipsTypeIds nvarchar(max)
,@ThingTipsID int
,@UpdatedByUserID INT
,@LanguageID int
)

As

Begin
set nocount on


Begin Try
Begin Transaction

UpDate ThingTips 
set 

	 ThingTipsData		= @ThingTipsData
	,UpdatedByUserID	= @UpdatedByUserID
	
where ThingTipsID=@ThingTipsID and LanguageID=@LanguageID


Delete from ThingTipsMapping where ThingTipsID=@ThingTipsID and  LanguageID=@LanguageID


set @ThingTipsTypeIds=@ThingTipsTypeIds+','

		;WITH Strings(Segments, Processing)
		AS
		(
		SELECT
				 SUBSTRING(@ThingTipsTypeIds,1, CHARINDEX(',', @ThingTipsTypeIds)-1) Segments
				,SUBSTRING(@ThingTipsTypeIds,CHARINDEX(',', @ThingTipsTypeIds)+1, len(@ThingTipsTypeIds)) Processing

				UNION ALL

				SELECT
				 SUBSTRING(Processing,1, CHARINDEX(',', Processing)-1) Segments
				,SUBSTRING(Processing,CHARINDEX(',', Processing)+1, len(Processing)) Processing

				FROM Strings

				WHERE
				CHARINDEX(',', Processing) > 0
		)



Insert ThingTipsMapping (ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,LanguageID)

select @ThingTipsID,Segments,(SELECT CreatedByUserID FROM ThingTips where ThingTipsID=@ThingTipsID and  LanguageID=@LanguageID )
	  ,@UpdatedByUserID,@LanguageID from Strings



COMMIT
End Try

Begin Catch

  If @@TRANCOUNT > 0
	ROLLBACK

	
	Declare @ErrMsg nvarchar(4000), @ErrSeverity int
	Select  @ErrMsg = ERROR_MESSAGE(),
		  @ErrSeverity = ERROR_SEVERITY()
	Raiserror(@ErrMsg, @ErrSeverity, 1)

 End Catch


End






