﻿CREATE TABLE [dbo].[Feedback] (
    [FeedbackID]     INT             IDENTITY (1, 1) NOT NULL,
    [FeedbackText]   NVARCHAR (1024) NULL,
    [FeedBackTypeID] INT             NOT NULL,
    [FeedbackDate]   DATETIME        NULL,
    CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED ([FeedbackID] ASC)
);

