﻿CREATE TABLE [dbo].[PatientProviderInvitation] (
    [InvitationID]     INT       IDENTITY (1, 1) NOT NULL,
    [InvitationCode]   NCHAR (6) NOT NULL,
    [SentPatientID]    INT       NULL,
    [SentProviderID]   INT       NULL,
    [DateSent]         DATETIME  CONSTRAINT [DF_PatientProviderInvitation_DateSent] DEFAULT (getdate()) NULL,
    [SentConfirmation] BIT       CONSTRAINT [DF_PatientProviderInvitation_sentConfirmation] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_PatientProviderInvitation] PRIMARY KEY CLUSTERED ([InvitationID] ASC),
    CONSTRAINT [FK_PatientProviderInvitation_HealthRecord] FOREIGN KEY ([SentPatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_PatientProviderInvitation_Provider] FOREIGN KEY ([SentProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

