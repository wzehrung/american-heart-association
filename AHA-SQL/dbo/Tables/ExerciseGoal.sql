﻿CREATE TABLE [dbo].[ExerciseGoal] (
    [ExerciseGoalId]     INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [Intensity]          NVARCHAR (50)    NOT NULL,
    [Duration]           FLOAT (53)       NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DF_ExerciseGoal_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DF_ExerciseGoal_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ExerciseGoal] PRIMARY KEY CLUSTERED ([ExerciseGoalId] ASC),
    CONSTRAINT [FK_ExerciseGoal_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID])
);

