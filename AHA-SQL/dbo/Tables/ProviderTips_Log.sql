﻿CREATE TABLE [dbo].[ProviderTips_Log] (
    [ProviderTips_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [ProviderTipsID]     INT              NOT NULL,
    [ProviderTipsData]   NVARCHAR (MAX)   NOT NULL,
    [CreatedByUserID]    INT              NULL,
    [UpdatedByUserID]    INT              NULL,
    [ModifiedDate]       DATETIME         CONSTRAINT [DF_ProviderTips_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]             TINYINT          NOT NULL,
    [TransactionID]      UNIQUEIDENTIFIER NOT NULL,
    [RowType]            NVARCHAR (50)    NULL,
    [User]               NVARCHAR (50)    NOT NULL,
    [LanguageID]         INT              NOT NULL,
    CONSTRAINT [PK_ProviderTips_Log] PRIMARY KEY CLUSTERED ([ProviderTips_LogID] ASC)
);

