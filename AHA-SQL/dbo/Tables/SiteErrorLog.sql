﻿CREATE TABLE [dbo].[SiteErrorLog] (
    [ErrorID]      INT            IDENTITY (1, 1) NOT NULL,
    [ErrorText]    NVARCHAR (MAX) NULL,
    [ErrorMessage] NVARCHAR (MAX) NULL,
    [IsErrorFixed] BIT            CONSTRAINT [DFC_SiteErrorLog_IsErrorFixed] DEFAULT ((0)) NULL,
    [DateInserted] DATETIME       CONSTRAINT [DFC_SiteErrorLog_DateInserted] DEFAULT (getdate()) NULL,
    [DateUpdated]  DATETIME       NULL,
    CONSTRAINT [PK_SiteErrorLog] PRIMARY KEY CLUSTERED ([ErrorID] ASC)
);

