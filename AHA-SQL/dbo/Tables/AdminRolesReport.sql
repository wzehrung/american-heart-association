﻿CREATE TABLE [dbo].[AdminRolesReport] (
    [RoleID]   INT NOT NULL,
    [ReportID] INT NOT NULL,
    [Enabled]  BIT NOT NULL,
    CONSTRAINT [PK_AdminRolesReport] PRIMARY KEY CLUSTERED ([RoleID] ASC, [ReportID] ASC)
);

