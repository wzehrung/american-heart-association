﻿CREATE TABLE [dbo].[timezone_offsets] (
    [time_zone_id]   VARCHAR (50) NOT NULL,
    [start_time_gmt] DATETIME     NOT NULL,
    [end_time_gmt]   DATETIME     NOT NULL,
    [offset]         NUMERIC (18) NOT NULL
);


GO
CREATE CLUSTERED INDEX [tz1]
    ON [dbo].[timezone_offsets]([time_zone_id] ASC, [start_time_gmt] ASC, [end_time_gmt] ASC);

