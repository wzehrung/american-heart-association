﻿CREATE TABLE [dbo].[Cholesterol] (
    [CholesterolID]      INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [Totalcholesterol]   INT              NULL,
    [LDL]                INT              NULL,
    [HDL]                INT              NULL,
    [Triglyceride]       INT              NULL,
    [TestLocation]       NVARCHAR (200)   NULL,
    [DateMeasured]       DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_Cholesterol_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_Cholesterol_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Cholesterol] PRIMARY KEY CLUSTERED ([CholesterolID] ASC),
    CONSTRAINT [FK_Cholesterol_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_Cholesterol_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

