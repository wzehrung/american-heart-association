﻿CREATE TABLE [dbo].[HVDataError] (
    [PatientID]      INT            NOT NULL,
    [ErrorText]      NVARCHAR (500) NULL,
    [ErrorTimestamp] DATETIME       NULL
);

