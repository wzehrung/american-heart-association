﻿CREATE TABLE [dbo].[MyLifeCheck] (
    [MLCID]              INT        IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT        NOT NULL,
    [HealthScore]        FLOAT (53) NOT NULL,
    [ActivityRating]     INT        NULL,
    [BloodSugarRating]   INT        NULL,
    [BPRating]           INT        NULL,
    [CholRating]         INT        NULL,
    [WeightRating]       INT        NULL,
    [DietRating]         INT        NULL,
    [SmokingRating]      INT        NULL,
    [UpdatedDate]        DATETIME   CONSTRAINT [DF_MyLifeCheck_UpdatedDate] DEFAULT (getdate()) NULL,
    [BMI]                FLOAT (53) NULL,
    [BloodGlucose]       FLOAT (53) NULL,
    [TotalCholesterol]   INT        NULL,
    [SystolicBP]         INT        NULL,
    [DiastolicBP]        INT        NULL,
    CONSTRAINT [PK_MyLifeCheck] PRIMARY KEY CLUSTERED ([MLCID] ASC),
    CONSTRAINT [FK_MyLifeCheck_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID])
);

