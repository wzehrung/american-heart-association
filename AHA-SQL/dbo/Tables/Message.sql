﻿CREATE TABLE [dbo].[Message] (
    [MessageID]        INT      IDENTITY (1, 1) NOT NULL,
    [SentDate]         DATETIME CONSTRAINT [DF_MessageDetails_SentDate] DEFAULT (getdate()) NOT NULL,
    [MessageType]      CHAR (1) NULL,
    [IsSentByProvider] BIT      CONSTRAINT [DF_Message_IsSentByProvider] DEFAULT ((0)) NULL,
    [GroupID]          INT      NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([MessageID] ASC),
    CONSTRAINT [FK_Message_PatientGroupDetails] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[PatientGroupDetails] ([GroupID])
);

