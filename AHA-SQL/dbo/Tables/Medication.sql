﻿CREATE TABLE [dbo].[Medication] (
    [MedicationId]       INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [MedicationName]     NVARCHAR (255)   NULL,
    [MedicationType]     NVARCHAR (255)   NULL,
    [Dosage]             NVARCHAR (50)    NULL,
    [Frequency]          NVARCHAR (50)    NULL,
    [DateDiscontinued]   DATETIME         NULL,
    [Notes]              NVARCHAR (MAX)   NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_Medication_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_Medication_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [Strength]           NVARCHAR (50)    NULL,
    [StartDate]          DATETIME         NULL,
    CONSTRAINT [PK_Medication] PRIMARY KEY CLUSTERED ([MedicationId] ASC),
    CONSTRAINT [FK_Medication_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_Medication_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

