﻿CREATE TABLE [dbo].[ProviderResourceGraphics] (
    [ProviderResourceGraphicsID] INT             NOT NULL,
    [Title]                      NVARCHAR (1024) NULL,
    [Graphics]                   VARBINARY (MAX) NULL,
    [Version]                    INT             CONSTRAINT [DF_ProviderResourceGraphics_Version] DEFAULT ((0)) NOT NULL,
    [CreatedByUserID]            INT             NULL,
    [UpdatedByUserID]            INT             NULL,
    [LanguageID]                 INT             NOT NULL,
    [ImageURL]                   NVARCHAR (255)  NULL,
    CONSTRAINT [PK_ProviderResourceGraphics] PRIMARY KEY CLUSTERED ([ProviderResourceGraphicsID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ProviderResourceGraphics_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO

CREATE TRIGGER [dbo].[Trig_ProviderResourceGraphics_Log]
ON [dbo].[ProviderResourceGraphics]
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into ProviderResourceGraphics_Log 
			(
			ProviderResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID,ImageURL
			)
			select	
			ProviderResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID,ImageURL
			 from deleted

			union all

			select			
			ProviderResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID,ImageURL
			 from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into ProviderResourceGraphics_Log 
			(
			ProviderResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID,ImageURL
			)
			select			
			ProviderResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID,ImageURL
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into ProviderResourceGraphics_Log 
			(
			ProviderResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID,ImageURL
			)
			select			
			ProviderResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID,ImageURL
			from inserted

		end

END
