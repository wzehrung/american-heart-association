﻿CREATE TABLE [dbo].[ProviderTipsMapping_Log] (
    [ProviderTipsMapping_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [ProviderTipsID]            INT              NOT NULL,
    [ProviderTipsTypeID]        INT              NOT NULL,
    [CreatedByUserID]           INT              NULL,
    [UpdatedByUserID]           INT              NULL,
    [ModifiedDate]              DATETIME         CONSTRAINT [DF_ProviderTipsMapping_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]                    TINYINT          NOT NULL,
    [TransactionID]             UNIQUEIDENTIFIER NOT NULL,
    [RowType]                   NVARCHAR (50)    NULL,
    [User]                      NVARCHAR (50)    NOT NULL,
    [LanguageID]                INT              CONSTRAINT [DF_ProviderTipsMapping_Log_LanguageID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ProviderTipsMapping_Log] PRIMARY KEY CLUSTERED ([ProviderTipsMapping_LogID] ASC)
);

