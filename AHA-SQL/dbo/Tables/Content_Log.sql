﻿CREATE TABLE [dbo].[Content_Log] (
    [Content_LogID]       INT              IDENTITY (1, 1) NOT NULL,
    [ContentID]           INT              NOT NULL,
    [Title]               NVARCHAR (250)   NOT NULL,
    [DidYouKnowLinkUrl]   NVARCHAR (1024)  NOT NULL,
    [DidYouKnowLinkTitle] NVARCHAR (MAX)   NOT NULL,
    [DidYouKnowText]      NVARCHAR (MAX)   NOT NULL,
    [ExpirationDate]      DATETIME         NULL,
    [CreatedByUserID]     INT              NULL,
    [UpdatedByUserID]     INT              NULL,
    [ModifiedDate]        DATETIME         CONSTRAINT [DF_Content_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]              TINYINT          NOT NULL,
    [TransactionID]       UNIQUEIDENTIFIER NOT NULL,
    [RowType]             NVARCHAR (50)    NULL,
    [User]                NVARCHAR (50)    NOT NULL,
    [LanguageID]          INT              NOT NULL,
    CONSTRAINT [PK_Content_Log] PRIMARY KEY CLUSTERED ([Content_LogID] ASC)
);

