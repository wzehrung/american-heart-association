﻿CREATE TABLE [dbo].[Languages] (
    [LanguageID]     INT            IDENTITY (1, 1) NOT NULL,
    [LanguageName]   NVARCHAR (250) NOT NULL,
    [LanguageLocale] NVARCHAR (50)  NULL,
    [IsDefault]      BIT            NULL,
    CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED ([LanguageID] ASC)
);

