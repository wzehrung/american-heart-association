﻿CREATE TABLE [dbo].[AffiliatesCampaigns] (
    [ID]          INT IDENTITY (1, 1) NOT NULL,
    [AffiliateID] INT NOT NULL,
    [CampaignID]  INT NOT NULL,
    CONSTRAINT [PK_AffiliatesCampaigns] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [AffiliatesCampaigns_idx2]
    ON [dbo].[AffiliatesCampaigns]([AffiliateID] ASC);


GO
CREATE NONCLUSTERED INDEX [AffiliatesCampaigns_idx1]
    ON [dbo].[AffiliatesCampaigns]([CampaignID] ASC);

