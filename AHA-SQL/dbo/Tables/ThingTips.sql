﻿CREATE TABLE [dbo].[ThingTips] (
    [ThingTipsId]     INT            NOT NULL,
    [ThingTipsData]   NVARCHAR (MAX) NOT NULL,
    [CreatedByUserID] INT            NULL,
    [UpdatedByUserID] INT            NULL,
    [LanguageID]      INT            NOT NULL,
    CONSTRAINT [PK_ThingTips] PRIMARY KEY CLUSTERED ([ThingTipsId] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ThingTips_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO

create TRIGGER Trig_ThingTips_Log
ON ThingTips
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into ThingTips_Log 
			(
			ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select	
			ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID
			 from deleted

			union all

			select			
			ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID
			 from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into ThingTips_Log 
			(
			ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into ThingTips_Log 
			(
			ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ThingTipsId,ThingTipsData,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID
			from inserted

		end

END


