﻿CREATE TABLE [dbo].[Diet] (
    [DietID]             INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [CalorieAmount]      INT              NULL,
    [Meal]               NVARCHAR (50)    NULL,
    [FoodItems]          NVARCHAR (MAX)   NULL,
    [DateOfEntry]        DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_Diet_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_Diet_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Diet] PRIMARY KEY CLUSTERED ([DietID] ASC),
    CONSTRAINT [FK_Diet_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_Diet_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

