﻿CREATE TABLE [dbo].[AffiliateRegion] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [ZipCode]    VARCHAR (10)  NOT NULL,
    [ZipName]    VARCHAR (50)  NOT NULL,
    [State]      VARCHAR (50)  NOT NULL,
    [Affiliate]  VARCHAR (50)  NOT NULL,
    [HBPMarket]  BIT           NOT NULL,
    [CBSAName]   VARCHAR (250) NULL,
    [CountyFIPS] VARCHAR (10)  NULL,
    [CountyName] VARCHAR (50)  NULL
);

