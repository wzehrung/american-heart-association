﻿CREATE TABLE [dbo].[AdminReport] (
    [ReportID]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (200) NULL,
    [CategoryID]  INT            NOT NULL,
    CONSTRAINT [PK_AdminReport] PRIMARY KEY CLUSTERED ([ReportID] ASC)
);

