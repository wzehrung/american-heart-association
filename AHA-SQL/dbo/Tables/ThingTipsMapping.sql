﻿CREATE TABLE [dbo].[ThingTipsMapping] (
    [ThingTipsID]     INT NOT NULL,
    [ThingTipsTypeID] INT NOT NULL,
    [CreatedByUserID] INT NULL,
    [UpdatedByUserID] INT NULL,
    [LanguageID]      INT CONSTRAINT [DF_ThingTipsMapping_LanguageID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ThingTipsMapping] PRIMARY KEY CLUSTERED ([ThingTipsID] ASC, [ThingTipsTypeID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ThingTipsMapping_ThingTips] FOREIGN KEY ([ThingTipsID], [LanguageID]) REFERENCES [dbo].[ThingTips] ([ThingTipsId], [LanguageID]),
    CONSTRAINT [FK_ThingTipsMapping_ThingTipsType] FOREIGN KEY ([ThingTipsTypeID], [LanguageID]) REFERENCES [dbo].[ThingTipsType] ([ThingTipsTypeId], [LanguageID])
);


GO

CREATE TRIGGER Trig_ThingTipsMapping_Log
ON ThingTipsMapping
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into ThingTipsMapping_Log 
			(
			ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select	
			ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID
			 from deleted

			union all

			select			
			ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID
			 from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into ThingTipsMapping_Log 
			(
			ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into ThingTipsMapping_Log 
			(
			ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ThingTipsID,ThingTipsTypeID,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID
			from inserted

		end

END
