﻿CREATE TABLE [dbo].[HLTLogDetail] (
    [patientid]     INT             NOT NULL,
    [zipcode]       NCHAR (10)      NULL,
    [url]           NVARCHAR (2048) NULL,
    [readingsource] NVARCHAR (100)  NULL,
    [datemeasured]  DATETIME        NOT NULL,
    [monthyear]     NVARCHAR (50)   NULL
);

