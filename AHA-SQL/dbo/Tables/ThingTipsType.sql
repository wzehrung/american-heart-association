﻿CREATE TABLE [dbo].[ThingTipsType] (
    [ThingTipsTypeId] INT            NOT NULL,
    [ThingTipsType]   NVARCHAR (100) NOT NULL,
    [DisplayName]     NVARCHAR (100) NULL,
    [LanguageID]      INT            NOT NULL,
    CONSTRAINT [PK_ThingTipsType] PRIMARY KEY CLUSTERED ([ThingTipsTypeId] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ThingTipsType_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);

