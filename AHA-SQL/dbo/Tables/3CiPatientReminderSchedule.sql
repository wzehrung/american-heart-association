﻿CREATE TABLE [dbo].[3CiPatientReminderSchedule] (
    [ReminderScheduleID] INT           IDENTITY (1, 1) NOT NULL,
    [ReminderID]         INT           NOT NULL,
    [Message]            VARCHAR (160) NULL,
    [Monday]             BIT           NULL,
    [Tuesday]            BIT           NULL,
    [Wednesday]          BIT           NULL,
    [Thursday]           BIT           NULL,
    [Friday]             BIT           NULL,
    [Saturday]           BIT           NULL,
    [Sunday]             BIT           NULL,
    [Time]               VARCHAR (5)   NULL,
    [DisplayTime]        VARCHAR (10)  NULL,
    [ReminderCategoryID] INT           NULL,
    [Status]             BIT           CONSTRAINT [DF_3CiPatientReminderSchedule_Status] DEFAULT ((1)) NOT NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    CONSTRAINT [PK_3CiPatientReminderSchedule] PRIMARY KEY CLUSTERED ([ReminderScheduleID] ASC)
);

