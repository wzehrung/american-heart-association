﻿CREATE TABLE [dbo].[Affiliates] (
    [AffiliateID]   INT           IDENTITY (1, 1) NOT NULL,
    [AffiliateName] NVARCHAR (50) NOT NULL,
    [AffiliateAbbr] NVARCHAR (10) NULL,
    CONSTRAINT [PK_Affiliates] PRIMARY KEY CLUSTERED ([AffiliateID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_affiliate_name]
    ON [dbo].[Affiliates]([AffiliateName] ASC);

