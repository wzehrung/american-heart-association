﻿CREATE TABLE [dbo].[MLCType] (
    [MLCTypeID]   INT           NOT NULL,
    [LanguageID]  INT           NOT NULL,
    [MLCTypeName] NVARCHAR (50) NOT NULL,
    [DisplayName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_MLCType_1] PRIMARY KEY CLUSTERED ([MLCTypeID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_MLCType_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);

