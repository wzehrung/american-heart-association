﻿CREATE TABLE [dbo].[ProviderContent] (
    [ContentID]       INT            NOT NULL,
    [ZoneTitle]       NVARCHAR (50)  NOT NULL,
    [ContentTitle]    NVARCHAR (80)  NULL,
    [ContentTitleUrl] NVARCHAR (255) NULL,
    [ContentText]     NVARCHAR (MAX) NULL,
    [CreatedByUserID] INT            NULL,
    [UpdatedByUserID] INT            NULL,
    [LanguageID]      INT            NOT NULL,
    [ProviderID]      INT            NULL,
    CONSTRAINT [PK_ProviderContent] PRIMARY KEY CLUSTERED ([ContentID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ProviderContent_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO

create TRIGGER Trig_ProviderContent_Log
ON ProviderContent
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into ProviderContent_Log 
			(
			ContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID
			 from deleted

			union all

			select			
			ContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID
			  from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into ProviderContent_Log 
			(
			ContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into ProviderContent_Log 
			(
			ContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID
			from inserted

		end

END
