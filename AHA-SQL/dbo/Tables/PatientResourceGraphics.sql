﻿CREATE TABLE [dbo].[PatientResourceGraphics] (
    [PatientResourceGraphicsID] INT             NOT NULL,
    [Title]                     NVARCHAR (1024) NULL,
    [Graphics]                  VARBINARY (MAX) NULL,
    [Version]                   INT             CONSTRAINT [DF_PatientResourceGraphics_Version] DEFAULT ((0)) NOT NULL,
    [CreatedByUserID]           INT             NULL,
    [UpdatedByUserID]           INT             NULL,
    [LanguageID]                INT             NOT NULL,
    [ImageURL]                  NVARCHAR (255)  NULL,
    CONSTRAINT [PK_PatientResourceGraphics] PRIMARY KEY CLUSTERED ([PatientResourceGraphicsID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_PatientResourceGraphics_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO
CREATE TRIGGER [dbo].[Trig_PatientResourceGraphics_Log]
ON [dbo].[PatientResourceGraphics]
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into PatientResourceGraphics_Log 
			(
			PatientResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID,ImageURL
			)
			select	
			PatientResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID,ImageURL
			 from deleted

			union all

			select			
			PatientResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID,ImageURL
			 from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into PatientResourceGraphics_Log 
			(
			PatientResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID,ImageURL
			)
			select			
			PatientResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID,ImageURL
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into PatientResourceGraphics_Log 
			(
			PatientResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID,ImageURL
			)
			select			
			PatientResourceGraphicsID,Title,Graphics,Version,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID,ImageURL
			from inserted

		end

END
