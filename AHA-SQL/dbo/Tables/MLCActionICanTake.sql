﻿CREATE TABLE [dbo].[MLCActionICanTake] (
    [MLCActionID]       INT            NOT NULL,
    [LanguageID]        INT            NOT NULL,
    [MLCTypeID]         INT            NOT NULL,
    [Rating]            INT            NULL,
    [RatingDisplayText] NVARCHAR (50)  NULL,
    [ActionText]        NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_MLCActionICanTake] PRIMARY KEY CLUSTERED ([MLCActionID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_MLCActionICanTake_MLCType] FOREIGN KEY ([MLCTypeID], [LanguageID]) REFERENCES [dbo].[MLCType] ([MLCTypeID], [LanguageID])
);

