﻿CREATE TABLE [dbo].[BloodPressure] (
    [BloodPressureID]    INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [Systolic]           INT              NOT NULL,
    [Diastolic]          INT              NOT NULL,
    [Pulse]              INT              NULL,
    [ReadingSource]      NVARCHAR (100)   NULL,
    [Comments]           NVARCHAR (MAX)   NULL,
    [DateMeasured]       DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_BloodPressure_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_BloodPressure_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [IrregularHeartbeat] BIT              NULL,
    CONSTRAINT [PK_BloodPressure] PRIMARY KEY CLUSTERED ([BloodPressureID] ASC),
    CONSTRAINT [FK_BloodPressure_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_BloodPressure_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [BloodPressure_idx1]
    ON [dbo].[BloodPressure]([UserHealthRecordID] ASC);

