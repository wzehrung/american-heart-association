﻿CREATE TABLE [dbo].[EmailListScheduler] (
    [SchedulerID]    INT      IDENTITY (1, 1) NOT NULL,
    [CampaignID]     INT      NULL,
    [ScheduledDate]  DATETIME NOT NULL,
    [IsProviderList] BIT      NOT NULL,
    [IsDownloaded]   BIT      NULL,
    [CreatedDate]    DATETIME CONSTRAINT [DF_EmailListScheduler_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]    DATETIME CONSTRAINT [DF_EmailListScheduler_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EmailListScheduler] PRIMARY KEY CLUSTERED ([SchedulerID] ASC),
    CONSTRAINT [FK_EmailListScheduler_Campaign] FOREIGN KEY ([CampaignID]) REFERENCES [dbo].[Campaign] ([CampaignID])
);

