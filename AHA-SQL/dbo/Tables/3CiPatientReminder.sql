﻿CREATE TABLE [dbo].[3CiPatientReminder] (
    [ReminderID]      INT           IDENTITY (1, 1) NOT NULL,
    [PatientID]       INT           NOT NULL,
    [MobileNumber]    VARCHAR (12)  NOT NULL,
    [PIN]             VARCHAR (4)   NOT NULL,
    [TimeZone]        NVARCHAR (35) NOT NULL,
    [Terms]           BIT           NULL,
    [PinValidated]    BIT           NULL,
    [CreatedDateTime] DATETIME      NULL,
    [UpdatedDateTime] DATETIME      NULL,
    CONSTRAINT [PK_3CiPatientReminder] PRIMARY KEY CLUSTERED ([ReminderID] ASC)
);

