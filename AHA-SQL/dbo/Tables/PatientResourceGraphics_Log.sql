﻿CREATE TABLE [dbo].[PatientResourceGraphics_Log] (
    [PatientResourceGraphics_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [PatientResourceGraphicsID]     INT              NOT NULL,
    [Title]                         NVARCHAR (1024)  NULL,
    [Graphics]                      VARBINARY (MAX)  NULL,
    [Version]                       INT              NOT NULL,
    [CreatedByUserID]               INT              NULL,
    [UpdatedByUserID]               INT              NULL,
    [ModifiedDate]                  DATETIME         CONSTRAINT [DF_PatientResourceGraphics_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]                        TINYINT          NOT NULL,
    [TransactionID]                 UNIQUEIDENTIFIER NOT NULL,
    [RowType]                       NVARCHAR (50)    NULL,
    [User]                          NVARCHAR (50)    NOT NULL,
    [LanguageID]                    INT              NOT NULL,
    [ImageURL]                      NVARCHAR (255)   NULL,
    CONSTRAINT [PK_PatientResourceGraphics_Log] PRIMARY KEY CLUSTERED ([PatientResourceGraphics_LogID] ASC)
);

