﻿CREATE TABLE [dbo].[ThingTipsMapping_Log] (
    [ThingTipsMapping_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [ThingTipsID]            INT              NOT NULL,
    [ThingTipsTypeID]        INT              NOT NULL,
    [CreatedByUserID]        INT              NULL,
    [UpdatedByUserID]        INT              NULL,
    [ModifiedDate]           DATETIME         CONSTRAINT [DF_ThingTipsMapping_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]                 TINYINT          NOT NULL,
    [TransactionID]          UNIQUEIDENTIFIER NOT NULL,
    [RowType]                NVARCHAR (50)    NULL,
    [User]                   NVARCHAR (50)    NOT NULL,
    [LanguageID]             INT              NOT NULL,
    CONSTRAINT [PK_ThingTipsMapping_Log] PRIMARY KEY CLUSTERED ([ThingTipsMapping_LogID] ASC)
);

