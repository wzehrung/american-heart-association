﻿CREATE TABLE [dbo].[AdminRolesMenu] (
    [RoleID]  INT NOT NULL,
    [MenuID]  INT NOT NULL,
    [Enabled] BIT NOT NULL,
    CONSTRAINT [PK_AdminRolesMenu] PRIMARY KEY CLUSTERED ([RoleID] ASC, [MenuID] ASC)
);

