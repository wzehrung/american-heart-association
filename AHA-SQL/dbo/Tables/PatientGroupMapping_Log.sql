﻿CREATE TABLE [dbo].[PatientGroupMapping_Log] (
    [PatientGroupMapping_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [GroupID]                   INT              NOT NULL,
    [PatientID]                 INT              NOT NULL,
    [ModifiedDate]              DATETIME         CONSTRAINT [DF_PatientGroupMapping_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]                    TINYINT          NOT NULL,
    [TransactionID]             UNIQUEIDENTIFIER NOT NULL,
    [RowType]                   NVARCHAR (50)    NULL,
    [User]                      NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_PatientGroupMapping_Log] PRIMARY KEY CLUSTERED ([PatientGroupMapping_LogID] ASC)
);

