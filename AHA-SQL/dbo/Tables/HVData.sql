﻿CREATE TABLE [dbo].[HVData] (
    [PatientID]     INT            NOT NULL,
    [FirstName]     NVARCHAR (50)  NULL,
    [LastName]      NVARCHAR (50)  NULL,
    [EmailAddress]  NVARCHAR (200) NULL,
    [ZipCode]       NCHAR (10)     NULL,
    [DataTimestamp] DATETIME       NULL
);

