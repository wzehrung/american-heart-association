﻿CREATE TABLE [dbo].[GrantSupport_Log] (
    [GrantSupport_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [GrantSupportID]     INT              NOT NULL,
    [Title]              NVARCHAR (128)   NOT NULL,
    [Description]        NVARCHAR (MAX)   NULL,
    [Logo]               VARBINARY (MAX)  NULL,
    [ContentType]        NVARCHAR (10)    NULL,
    [IsForProvider]      BIT              NOT NULL,
    [Version]            NVARCHAR (50)    NULL,
    [CreatedByUserID]    INT              NULL,
    [UpdatedByUserID]    INT              NULL,
    [ModifiedDate]       DATETIME         CONSTRAINT [DF_GrantSupport_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]             TINYINT          NOT NULL,
    [TransactionID]      UNIQUEIDENTIFIER NOT NULL,
    [RowType]            NVARCHAR (50)    NULL,
    [User]               NVARCHAR (50)    NOT NULL,
    [LanguageID]         INT              NOT NULL,
    CONSTRAINT [PK_GrantSupport_Log] PRIMARY KEY CLUSTERED ([GrantSupport_LogID] ASC)
);

