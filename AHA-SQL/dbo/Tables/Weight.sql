﻿CREATE TABLE [dbo].[Weight] (
    [WeightID]           INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [Weight]             FLOAT (53)       NOT NULL,
    [ReadingSource]      NVARCHAR (100)   NULL,
    [Comments]           NVARCHAR (MAX)   NULL,
    [DateMeasured]       DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_Weight_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_Weight_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Weight] PRIMARY KEY CLUSTERED ([WeightID] ASC),
    CONSTRAINT [FK_Weight_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_Weight_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

