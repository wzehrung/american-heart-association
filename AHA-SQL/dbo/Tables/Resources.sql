﻿CREATE TABLE [dbo].[Resources] (
    [ResourceID]      INT            NOT NULL,
    [ContentTitle]    NVARCHAR (80)  NOT NULL,
    [ContentTitleUrl] NVARCHAR (255) NOT NULL,
    [ContentText]     NVARCHAR (MAX) NULL,
    [SortOrder]       INT            NOT NULL,
    [IsProvider]      BIT            NULL,
    [ExpirationDate]  DATETIME       NULL,
    [CreatedByUserID] INT            NULL,
    [UpdatedByUserID] INT            NULL,
    [LanguageID]      INT            NOT NULL,
    [ProviderID]      INT            NULL,
    CONSTRAINT [PK_Resources] PRIMARY KEY CLUSTERED ([ResourceID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_Resources_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO

CREATE TRIGGER [dbo].[Trig_Resources_Log]
ON [dbo].[Resources]
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into Resources_Log 
			(
			ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID, ProviderID
			)
			select			
			ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID, ProviderID
			 from deleted

			union all

			select			
			ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID, ProviderID
			  from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into Resources_Log 
			(
			ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID, ProviderID
			)
			select			
			ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID, ProviderID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into Resources_Log 
			(
			ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID, ProviderID
			)
			select			
			ResourceID,ContentTitle,ContentTitleUrl,ContentText,SortOrder,IsProvider,ExpirationDate,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID, ProviderID
			from inserted

		end

END
