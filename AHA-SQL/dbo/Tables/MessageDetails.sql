﻿CREATE TABLE [dbo].[MessageDetails] (
    [MessageDetailsID]    INT IDENTITY (1, 1) NOT NULL,
    [MessageID]           INT NOT NULL,
    [ProviderID]          INT NOT NULL,
    [PatientID]           INT NULL,
    [isRead]              BIT CONSTRAINT [DF_MessageDetails_isRead] DEFAULT ((0)) NOT NULL,
    [IsFlaged]            BIT NULL,
    [UserFolderMappingID] INT NOT NULL,
    [IsSentByProvider]    BIT CONSTRAINT [DF_MessageDetails_IsSentByProvider] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MessageDetails] PRIMARY KEY CLUSTERED ([MessageDetailsID] ASC),
    CONSTRAINT [FK_MessageDetails_HealthRecord] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_MessageDetails_Message] FOREIGN KEY ([MessageID]) REFERENCES [dbo].[Message] ([MessageID]),
    CONSTRAINT [FK_MessageDetails_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID]),
    CONSTRAINT [FK_MessageDetails_UserFolderMapping] FOREIGN KEY ([UserFolderMappingID]) REFERENCES [dbo].[UserFolderMapping] ([UserFolderMappingID])
);

