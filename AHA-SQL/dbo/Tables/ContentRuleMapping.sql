﻿CREATE TABLE [dbo].[ContentRuleMapping] (
    [ContentRulesID]  INT NOT NULL,
    [ContentID]       INT NOT NULL,
    [CreatedByUserID] INT NULL,
    [UpdatedByUserID] INT NULL,
    [LanguageID]      INT CONSTRAINT [DF_ContentRuleMapping_LanguageID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_SuggestedContentRule_1] PRIMARY KEY CLUSTERED ([ContentRulesID] ASC, [ContentID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ContentRuleMapping_Content] FOREIGN KEY ([ContentID], [LanguageID]) REFERENCES [dbo].[Content] ([ContentID], [LanguageID]),
    CONSTRAINT [FK_ContentRuleMapping_ContentRules] FOREIGN KEY ([ContentRulesID], [LanguageID]) REFERENCES [dbo].[ContentRules] ([ContentRulesID], [LanguageID])
);


GO

CREATE TRIGGER Trig_ContentRuleMapping_Log
ON ContentRuleMapping
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into ContentRuleMapping_Log 
			(
			ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID
			 from deleted

			union all

			select			
			ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID
			  from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into ContentRuleMapping_Log 
			(
			ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into ContentRuleMapping_Log 
			(
			ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentRulesID,ContentID,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID
			from inserted

		end

END
