﻿CREATE TABLE [dbo].[3CiPatientReminderCategory] (
    [ReminderCategoryID] INT          IDENTITY (1, 1) NOT NULL,
    [Name]               VARCHAR (25) NOT NULL,
    [TriggerID]          VARCHAR (50) NOT NULL,
    [GroupID]            VARCHAR (50) NULL,
    CONSTRAINT [PK_3CiPatientReminderCategory] PRIMARY KEY CLUSTERED ([ReminderCategoryID] ASC)
);

