﻿CREATE TABLE [dbo].[PatientGroupMapping] (
    [GroupID]   INT      NOT NULL,
    [PatientID] INT      NOT NULL,
    [Date]      DATETIME CONSTRAINT [DF_PatientGroupMapping_Date] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_PatientGroup_1] PRIMARY KEY CLUSTERED ([GroupID] ASC, [PatientID] ASC),
    CONSTRAINT [FK_PatientGroup_PatientGroupDetails] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[PatientGroupDetails] ([GroupID]),
    CONSTRAINT [FK_PatientGroupMapping_HealthRecord] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID])
);


GO


CREATE TRIGGER [dbo].[Trig_PatientGroupMapping_Log]
ON [dbo].[PatientGroupMapping]
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into PatientGroupMapping_Log 
			(
			[GroupID],[PatientID],[Action],[TransactionID],[RowType],[User]
			)
			select			
			[GroupID],[PatientID],1,@RowID,'oldRow',SYSTEM_USER
			 from deleted

			union all

			select			
			[GroupID],[PatientID],1,@RowID,'NewRow',SYSTEM_USER
			  from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into PatientGroupMapping_Log 
			(
			[GroupID],[PatientID],[Action],[TransactionID],[RowType],[User]
			)
			select			
			[GroupID],[PatientID],2,@RowID,null,SYSTEM_USER
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into PatientGroupMapping_Log 
			(
			[GroupID],[PatientID],[Action],[TransactionID],[RowType],[User]
			)
			select			
			[GroupID],[PatientID],0,@RowID,null,SYSTEM_USER
			from inserted

		end

END