﻿CREATE TABLE [dbo].[AdminCategory] (
    [MenuCategoryID]   INT           IDENTITY (1, 1) NOT NULL,
    [MenuCategoryName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_AdminMenuCategory] PRIMARY KEY CLUSTERED ([MenuCategoryID] ASC)
);

