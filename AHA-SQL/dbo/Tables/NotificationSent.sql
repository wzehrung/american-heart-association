﻿CREATE TABLE [dbo].[NotificationSent] (
    [NotificationID] INT            NOT NULL,
    [ProviderID]     INT            NOT NULL,
    [Type]           NVARCHAR (50)  NOT NULL,
    [Body]           NVARCHAR (MAX) NOT NULL,
    [SentDate]       DATETIME       NOT NULL,
    CONSTRAINT [PK_NotificationSent] PRIMARY KEY CLUSTERED ([NotificationID] ASC)
);

