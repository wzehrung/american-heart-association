﻿CREATE TABLE [dbo].[Height] (
    [HeightID]           INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [Height]             FLOAT (53)       NOT NULL,
    [Comments]           NVARCHAR (MAX)   NULL,
    [DateMeasured]       DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_Height_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_Height_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Height] PRIMARY KEY CLUSTERED ([HeightID] ASC),
    CONSTRAINT [FK_Height_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_Height_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

