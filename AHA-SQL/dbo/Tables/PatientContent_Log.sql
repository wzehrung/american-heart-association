﻿CREATE TABLE [dbo].[PatientContent_Log] (
    [PatientContent_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [PatientContentID]     INT              NOT NULL,
    [ZoneTitle]            NVARCHAR (128)   NULL,
    [ContentTitle]         NVARCHAR (128)   NULL,
    [ContentTitleUrl]      NVARCHAR (1128)  NULL,
    [ContentText]          NVARCHAR (MAX)   NULL,
    [CreatedByUserID]      INT              NULL,
    [UpdatedByUserID]      INT              NULL,
    [ModifiedDate]         DATETIME         CONSTRAINT [DF_PatientContent_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]               TINYINT          NOT NULL,
    [TransactionID]        UNIQUEIDENTIFIER NOT NULL,
    [RowType]              NVARCHAR (50)    NULL,
    [User]                 NVARCHAR (50)    NOT NULL,
    [LanguageID]           INT              NOT NULL,
    [ProviderID]           INT              NULL,
    CONSTRAINT [PK_PatientContent_Log] PRIMARY KEY CLUSTERED ([PatientContent_LogID] ASC)
);

