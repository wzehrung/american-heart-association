﻿CREATE TABLE [dbo].[AdminBusiness] (
    [BusinessID] INT             IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (64)    NULL,
    [Comments]   NVARCHAR (1024) NULL,
    CONSTRAINT [PK_AdminBusiness] PRIMARY KEY CLUSTERED ([BusinessID] ASC)
);

