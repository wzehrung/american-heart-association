﻿CREATE TABLE [dbo].[DisconnectedPatients] (
    [LogID]      INT      IDENTITY (1, 1) NOT NULL,
    [ProviderID] INT      NOT NULL,
    [PatientID]  INT      NOT NULL,
    [Date]       DATETIME CONSTRAINT [DF_DisconnectedPatients_Date] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DisconnectedPatients] PRIMARY KEY CLUSTERED ([LogID] ASC)
);

