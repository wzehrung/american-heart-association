﻿CREATE TABLE [dbo].[ListItems] (
    [ListItemID]   INT            NOT NULL,
    [ListID]       INT            NOT NULL,
    [ListItemName] NVARCHAR (100) NOT NULL,
    [ListItemCode] NVARCHAR (15)  NULL,
    [LanguageID]   INT            NOT NULL,
    CONSTRAINT [PK_ListItems_1] PRIMARY KEY CLUSTERED ([ListItemID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ListItems_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID]),
    CONSTRAINT [FK_ListItems_Lists] FOREIGN KEY ([ListID]) REFERENCES [dbo].[Lists] ([ListID])
);

