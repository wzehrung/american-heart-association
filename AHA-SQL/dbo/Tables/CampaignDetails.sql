﻿CREATE TABLE [dbo].[CampaignDetails] (
    [CampaignID]              INT              NOT NULL,
    [LanguageID]              INT              NOT NULL,
    [Description]             NVARCHAR (MAX)   NULL,
    [Title]                   NVARCHAR (1024)  NULL,
    [PromotionalTagLine]      NVARCHAR (MAX)   NULL,
    [PartnerLogo]             VARBINARY (MAX)  NULL,
    [PartnerLogoContentType]  NVARCHAR (10)    NULL,
    [PartnerLogoVersion]      UNIQUEIDENTIFIER NULL,
    [PartnerImage]            VARBINARY (MAX)  NULL,
    [PartnerImageContentType] NVARCHAR (10)    NULL,
    [PartnerImageVersion]     UNIQUEIDENTIFIER NULL,
    [IsPublished]             BIT              CONSTRAINT [DF_CampaignDetails_IsPublished] DEFAULT ((0)) NOT NULL,
    [ResourceTitle]           NVARCHAR (1024)  NULL,
    [ResourceURL]             NVARCHAR (255)   NULL,
    [CampaignImages]          NVARCHAR (50)    NULL,
    CONSTRAINT [PK_CampaignDetails] PRIMARY KEY CLUSTERED ([CampaignID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_CampaignDetails_Campaign] FOREIGN KEY ([CampaignID]) REFERENCES [dbo].[Campaign] ([CampaignID]),
    CONSTRAINT [FK_CampaignDetails_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO
CREATE NONCLUSTERED INDEX [CampaignDetails_idx1]
    ON [dbo].[CampaignDetails]([CampaignID] ASC);

