﻿CREATE TABLE [dbo].[ListItems_A] (
    [ItemID]      INT           IDENTITY (1000, 1) NOT NULL,
    [ItemName]    NVARCHAR (50) NOT NULL,
    [ItemAbbr]    NVARCHAR (50) NULL,
    [ItemValue]   NVARCHAR (50) NOT NULL,
    [ItemType]    NVARCHAR (50) NULL,
    [Description] NVARCHAR (50) NULL,
    CONSTRAINT [PK_ListItems_A] PRIMARY KEY CLUSTERED ([ItemID] ASC)
);

