﻿CREATE TABLE [dbo].[RxTerm] (
    [RxTermID] INT            IDENTITY (1, 1) NOT NULL,
    [RxTerm]   NVARCHAR (MAX) NOT NULL,
    [IsActive] BIT            CONSTRAINT [DF_RxTerm_IsNew] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_RxTerm] PRIMARY KEY CLUSTERED ([RxTermID] ASC)
);

