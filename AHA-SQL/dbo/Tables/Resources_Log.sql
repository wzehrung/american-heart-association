﻿CREATE TABLE [dbo].[Resources_Log] (
    [Resources_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [ResourceID]      INT              NOT NULL,
    [ContentTitle]    NVARCHAR (80)    NOT NULL,
    [ContentTitleUrl] NVARCHAR (255)   NOT NULL,
    [ContentText]     NVARCHAR (MAX)   NULL,
    [SortOrder]       INT              NOT NULL,
    [IsProvider]      BIT              NULL,
    [ExpirationDate]  DATETIME         NULL,
    [CreatedByUserID] INT              NULL,
    [UpdatedByUserID] INT              NULL,
    [ModifiedDate]    DATETIME         CONSTRAINT [DF_Resources_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]          TINYINT          NOT NULL,
    [TransactionID]   UNIQUEIDENTIFIER NOT NULL,
    [RowType]         NVARCHAR (50)    NULL,
    [User]            NVARCHAR (50)    NOT NULL,
    [LanguageID]      INT              NOT NULL,
    [ProviderID]      INT              NULL,
    CONSTRAINT [PK_Resources_Log] PRIMARY KEY CLUSTERED ([Resources_LogID] ASC)
);

