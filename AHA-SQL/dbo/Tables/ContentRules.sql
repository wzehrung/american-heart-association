﻿CREATE TABLE [dbo].[ContentRules] (
    [ContentRulesID] INT            NOT NULL,
    [LanguageID]     INT            NOT NULL,
    [RuleName]       NVARCHAR (50)  NOT NULL,
    [ClassName]      NVARCHAR (100) NOT NULL,
    [Abbreviation]   NVARCHAR (3)   NOT NULL,
    [DisplayName]    NVARCHAR (100) NULL,
    CONSTRAINT [PK_ContentRules] PRIMARY KEY CLUSTERED ([ContentRulesID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ContentRules_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);

