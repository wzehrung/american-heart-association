﻿CREATE TABLE [dbo].[Groups] (
    [Name]              NVARCHAR (50)  NOT NULL,
    [Description]       NVARCHAR (MAX) NULL,
    [isDefault]         BIT            NOT NULL,
    [GroupRuleXml]      XML            NULL,
    [IsSystemGenerated] BIT            NOT NULL,
    [RemoveOption]      CHAR (1)       NULL
);

