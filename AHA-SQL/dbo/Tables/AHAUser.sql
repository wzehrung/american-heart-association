﻿CREATE TABLE [dbo].[AHAUser] (
    [UserID]                    INT              IDENTITY (1, 1) NOT NULL,
    [PersonGUID]                UNIQUEIDENTIFIER NOT NULL,
    [SelfHealthRecordID]        INT              NULL,
    [SelfPersonalItemID]        INT              NULL,
    [IsPersonalInfoCompleted]   BIT              CONSTRAINT [DFC_AHAUser_IsPersonalInfoCompleted] DEFAULT ((0)) NOT NULL,
    [IsHealthHistoryCompleted]  BIT              CONSTRAINT [DFC_AHAUser_IsHealthHistoryCompleted] DEFAULT ((0)) NOT NULL,
    [TermsAndConditions]        BIT              NULL,
    [Consent]                   BIT              NULL,
    [CreatedDate]               DATETIME         CONSTRAINT [DFC_AHAUser_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]               DATETIME         CONSTRAINT [DFC_AHAUser_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [CurrentSelectedRecordGUID] UNIQUEIDENTIFIER NULL,
    [NewTermsAndConditions]     BIT              NULL,
    [AllowAHAEmail]             BIT              DEFAULT ((0)) NOT NULL,
    [AllowAHAEmailUpdatedDate]  DATETIME         NULL,
    [DefaultLanguageID]         INT              NULL,
    [TermsAcceptanceDate]       DATETIME         NULL,
    CONSTRAINT [PK_AHAUser] PRIMARY KEY CLUSTERED ([UserID] ASC),
    CONSTRAINT [FK_AHAUser_HealthRecord] FOREIGN KEY ([SelfHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_AHAUser_HealthRecord_SelfPersonnelItemID] FOREIGN KEY ([SelfPersonalItemID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_AHAUser_Languages] FOREIGN KEY ([DefaultLanguageID]) REFERENCES [dbo].[Languages] ([LanguageID]),
    CONSTRAINT [UQC_AHAUser_PersonGUID] UNIQUE NONCLUSTERED ([PersonGUID] ASC)
);

