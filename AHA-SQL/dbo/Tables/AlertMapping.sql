﻿CREATE TABLE [dbo].[AlertMapping] (
    [AlertRuleID] INT NOT NULL,
    [PatientID]   INT NOT NULL,
    [GroupID]     INT NULL,
    CONSTRAINT [PK_AlertMapping] PRIMARY KEY CLUSTERED ([AlertRuleID] ASC, [PatientID] ASC),
    CONSTRAINT [FK_AlertMapping_AlertRule] FOREIGN KEY ([AlertRuleID]) REFERENCES [dbo].[AlertRule] ([AlertRuleID]),
    CONSTRAINT [FK_AlertMapping_HealthRecord] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_AlertMapping_PatientGroupDetails] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[PatientGroupDetails] ([GroupID])
);

