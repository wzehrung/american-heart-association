﻿CREATE TABLE [dbo].[BloodGlucose_HbA1c] (
    [HbA1cID]            INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NOT NULL,
    [HbA1cValue]         FLOAT (53)       NOT NULL,
    [DateMeasured]       DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         NOT NULL,
    [UpdatedDate]        DATETIME         NOT NULL,
    [Notes]              NVARCHAR (MAX)   NULL,
    [ReadingSource]      NVARCHAR (100)   NULL,
    CONSTRAINT [PK_BloodGlucose_HbA1c] PRIMARY KEY CLUSTERED ([HbA1cID] ASC),
    CONSTRAINT [FK_BloodGlucose_A1c_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID])
);

