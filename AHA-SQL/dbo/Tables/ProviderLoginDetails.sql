﻿CREATE TABLE [dbo].[ProviderLoginDetails] (
    [LoginDetailsID] INT      IDENTITY (1, 1) NOT NULL,
    [ProviderID]     INT      NOT NULL,
    [LoggedInDate]   DATETIME CONSTRAINT [DFC_ProviderLoginDetails_LoggedInDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ProviderLoginDetails] PRIMARY KEY CLUSTERED ([LoginDetailsID] ASC),
    CONSTRAINT [FK_ProviderLoginDetails_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

