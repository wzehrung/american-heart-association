﻿CREATE TABLE [dbo].[ResetPasswordLog] (
    [PasswordResetID] INT              IDENTITY (1, 1) NOT NULL,
    [ResetGUID]       UNIQUEIDENTIFIER NOT NULL,
    [ProviderID]      INT              NOT NULL,
    [CreatedDate]     DATETIME         NULL,
    [ResetedDate]     DATETIME         NULL,
    CONSTRAINT [PK_ResetPasswordLog] PRIMARY KEY CLUSTERED ([PasswordResetID] ASC),
    CONSTRAINT [FK_ResetPasswordLog_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

