﻿CREATE TABLE [dbo].[EmailDespatchDetails] (
    [EmailDespatchDetailsID] INT             IDENTITY (1, 1) NOT NULL,
    [UserID]                 INT             NULL,
    [ProviderID]             INT             NULL,
    [CampaignID]             INT             NULL,
    [EmailType]              NVARCHAR (1024) NOT NULL,
    [CreateDate]             DATETIME        CONSTRAINT [DF_EmailDespatchDetails_CreateDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EmailDespatchDetails] PRIMARY KEY CLUSTERED ([EmailDespatchDetailsID] ASC),
    CONSTRAINT [FK_EmailDespatchDetails_AHAUser] FOREIGN KEY ([UserID]) REFERENCES [dbo].[AHAUser] ([UserID]),
    CONSTRAINT [FK_EmailDespatchDetails_Campaign] FOREIGN KEY ([CampaignID]) REFERENCES [dbo].[Campaign] ([CampaignID]),
    CONSTRAINT [FK_EmailDespatchDetails_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

