﻿CREATE TABLE [dbo].[3CiPINValidationLog] (
    [ID]              INT         IDENTITY (1, 1) NOT NULL,
    [ReminderID]      INT         NOT NULL,
    [PIN]             VARCHAR (4) NOT NULL,
    [Valid]           BIT         NOT NULL,
    [CreatedDateTime] DATETIME    NOT NULL,
    CONSTRAINT [PK_3CiPINValidationLog] PRIMARY KEY CLUSTERED ([ID] ASC)
);

