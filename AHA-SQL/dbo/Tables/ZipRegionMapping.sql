﻿CREATE TABLE [dbo].[ZipRegionMapping] (
    [ZIP_Code]        NVARCHAR (50)  NULL,
    [ZIP_Name]        NVARCHAR (255) NULL,
    [State]           NVARCHAR (255) NULL,
    [Affiliate]       NVARCHAR (255) NULL,
    [TotalPopulation] FLOAT (53)     NULL,
    [CBSA_Code]       NVARCHAR (50)  NULL,
    [CBSA_Name]       NVARCHAR (255) NULL,
    [CountyFIPS]      NVARCHAR (50)  NULL,
    [CountyName]      NVARCHAR (255) NULL
);

