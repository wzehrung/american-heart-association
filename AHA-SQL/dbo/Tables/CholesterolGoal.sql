﻿CREATE TABLE [dbo].[CholesterolGoal] (
    [CholesterolGoalID]  INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [Totalcholesterol]   INT              NOT NULL,
    [LDL]                INT              NOT NULL,
    [HDL]                INT              NOT NULL,
    [Triglyceride]       INT              NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_CholesterolGoal_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_CholesterolGoal_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CholesterolGoal] PRIMARY KEY CLUSTERED ([CholesterolGoalID] ASC),
    CONSTRAINT [FK_CholesterolGoal_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_CholesterolGoal_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

