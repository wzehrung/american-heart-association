﻿CREATE TABLE [dbo].[PatientContent] (
    [PatientContentID] INT             NOT NULL,
    [ZoneTitle]        NVARCHAR (128)  NULL,
    [ContentTitle]     NVARCHAR (128)  NULL,
    [ContentTitleUrl]  NVARCHAR (1128) NULL,
    [ContentText]      NVARCHAR (MAX)  NULL,
    [CreatedByUserID]  INT             NULL,
    [UpdatedByUserID]  INT             NULL,
    [LanguageID]       INT             NOT NULL,
    [ProviderID]       INT             NULL,
    [CreatedDate]      DATETIME        NULL,
    [UpdatedDate]      DATETIME        NULL,
    CONSTRAINT [PK_PatientContent] PRIMARY KEY CLUSTERED ([PatientContentID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_PatientContent_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO

CREATE TRIGGER [dbo].[Trig_PatientContent_Log]
ON [dbo].[PatientContent]
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into PatientContent_Log 
			(
			PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID, ProviderID
			)
			select			
			PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID, ProviderID
			 from deleted

			union all

			select			
			PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID, ProviderID
			  from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into PatientContent_Log 
			(
			PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID, ProviderID
			)
			select			
			PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID, ProviderID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into PatientContent_Log 
			(
			PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID, ProviderID
			)
			select			
			PatientContentID,ZoneTitle,ContentTitle,ContentTitleUrl,ContentText,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID, ProviderID
			from inserted

		end

END
