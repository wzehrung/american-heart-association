﻿CREATE TABLE [dbo].[IVRPatientReminder] (
    [ReminderID]      INT           IDENTITY (1, 1) NOT NULL,
    [PatientID]       INT           NOT NULL,
    [PhoneNumber]     VARCHAR (12)  NOT NULL,
    [PIN]             VARCHAR (4)   NOT NULL,
    [Time]            NVARCHAR (35) NOT NULL,
    [Day]             NVARCHAR (9)  NULL,
    [TimeZone]        NVARCHAR (35) NOT NULL,
    [Enabled]         BIT           NOT NULL,
    [CreatedDateTime] DATETIME      NULL,
    [UpdatedDateTime] DATETIME      NULL,
    CONSTRAINT [PK_IVRPatientReminder] PRIMARY KEY CLUSTERED ([ReminderID] ASC)
);

