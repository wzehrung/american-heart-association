﻿CREATE TABLE [dbo].[Folder] (
    [FolderID]   INT            IDENTITY (1, 1) NOT NULL,
    [FolderName] NVARCHAR (100) NOT NULL,
    [FolderType] CHAR (1)       NOT NULL,
    CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED ([FolderID] ASC)
);

