﻿CREATE TABLE [dbo].[ContentRuleMapping_Log] (
    [ContentRuleMapping_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [ContentRulesID]           INT              NOT NULL,
    [ContentID]                INT              NOT NULL,
    [CreatedByUserID]          INT              NULL,
    [UpdatedByUserID]          INT              NULL,
    [ModifiedDate]             DATETIME         CONSTRAINT [DF_ContentRuleMapping_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]                   TINYINT          NOT NULL,
    [TransactionID]            UNIQUEIDENTIFIER NOT NULL,
    [RowType]                  NVARCHAR (50)    NULL,
    [User]                     NVARCHAR (50)    NOT NULL,
    [LanguageID]               INT              NOT NULL,
    CONSTRAINT [PK_ContentRuleMapping_Log] PRIMARY KEY CLUSTERED ([ContentRuleMapping_LogID] ASC)
);

