﻿CREATE TABLE [dbo].[FamilyHistory] (
    [FamilyHistoryId]    INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [FamilyMember]       NVARCHAR (255)   NULL,
    [Disease]            NVARCHAR (255)   NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_FamilyHistory_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_FamilyHistory_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FamilyHistory] PRIMARY KEY CLUSTERED ([FamilyHistoryId] ASC),
    CONSTRAINT [FK_FamilyHistory_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_FamilyHistory_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

