﻿CREATE TABLE [dbo].[PolkaData] (
    [ItemID]             INT           IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT           NOT NULL,
    [PolkaDataUniqueID]  INT           NOT NULL,
    [ItemType]           NVARCHAR (50) NOT NULL,
    [PolkaData]          XML           NOT NULL,
    [DateCreated]        DATETIME      NOT NULL,
    [IsUploadedToHV]     TINYINT       CONSTRAINT [DF_PolkaData_IsUploadedToHV] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PolkaData] PRIMARY KEY CLUSTERED ([ItemID] ASC),
    CONSTRAINT [FK_PolkaData_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID])
);

