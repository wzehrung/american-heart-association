﻿CREATE TABLE [dbo].[Lists] (
    [ListID]   INT            IDENTITY (1, 1) NOT NULL,
    [ListCode] NVARCHAR (2)   NOT NULL,
    [ListName] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Lists] PRIMARY KEY CLUSTERED ([ListID] ASC),
    CONSTRAINT [UQK_ListCode] UNIQUE NONCLUSTERED ([ListCode] ASC)
);

