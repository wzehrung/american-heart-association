﻿CREATE TABLE [dbo].[3CiPatientReminderLog] (
    [ReminderScheduleLogID] INT           IDENTITY (1, 1) NOT NULL,
    [ReminderScheduleID]    INT           NULL,
    [PatientID]             INT           NOT NULL,
    [Message]               VARCHAR (MAX) NOT NULL,
    [MessageDate]           DATETIME      NOT NULL,
    [Direction]             INT           NOT NULL,
    [CreatedDateTime]       DATETIME      CONSTRAINT [DF_3CiPatientReminderLog_CreatedDateTime] DEFAULT (getdate()) NOT NULL,
    [UpdatedDateTime]       NCHAR (10)    NULL,
    CONSTRAINT [PK_3CiPatientReminderLog] PRIMARY KEY CLUSTERED ([ReminderScheduleLogID] ASC)
);

