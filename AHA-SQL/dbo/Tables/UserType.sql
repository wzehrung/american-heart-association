﻿CREATE TABLE [dbo].[UserType] (
    [ID]      INT          IDENTITY (1, 1) NOT NULL,
    [Type]    VARCHAR (50) NOT NULL,
    [Display] VARCHAR (50) NOT NULL
);

