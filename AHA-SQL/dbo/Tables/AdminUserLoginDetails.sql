﻿CREATE TABLE [dbo].[AdminUserLoginDetails] (
    [AdminUserLoginDetailsID] INT      IDENTITY (1, 1) NOT NULL,
    [AdminUserID]             INT      NOT NULL,
    [LoggedInDate]            DATETIME CONSTRAINT [DF_AdminUserLoginDetails_LoggedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AdminUserLoginDetails] PRIMARY KEY CLUSTERED ([AdminUserLoginDetailsID] ASC),
    CONSTRAINT [FK_AdminUserLoginDetails_AdminUser] FOREIGN KEY ([AdminUserID]) REFERENCES [dbo].[AdminUser] ([AdminUserID])
);

