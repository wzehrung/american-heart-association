﻿CREATE TABLE [dbo].[ServicesErrorLog] (
    [ErrorID]      INT            IDENTITY (1, 1) NOT NULL,
    [ServiceName]  NVARCHAR (50)  NOT NULL,
    [ErrorDate]    DATETIME       CONSTRAINT [DF_ServicesErrorLog_ErrorDate] DEFAULT (getdate()) NOT NULL,
    [ErrorContent] NVARCHAR (MAX) NOT NULL
);

