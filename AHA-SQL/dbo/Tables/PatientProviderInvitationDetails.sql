﻿CREATE TABLE [dbo].[PatientProviderInvitationDetails] (
    [InvitationDetailsID] INT           IDENTITY (1, 1) NOT NULL,
    [InvitationID]        INT           NOT NULL,
    [FirstName]           NVARCHAR (50) NULL,
    [LastName]            NVARCHAR (50) NULL,
    [Email]               NVARCHAR (50) NULL,
    [DateInserted]        DATETIME      CONSTRAINT [DF_PatientProviderInvitationDetails_DateInserted] DEFAULT (getdate()) NOT NULL,
    [DateAccepted]        DATETIME      NULL,
    [Status]              TINYINT       NULL,
    [AcceptedPatientID]   INT           NULL,
    [AcceptedProviderID]  INT           NULL,
    CONSTRAINT [PK_PatientProviderInvitationDetails] PRIMARY KEY CLUSTERED ([InvitationDetailsID] ASC),
    CONSTRAINT [FK_PatientProviderInvitationDetails_HealthRecord] FOREIGN KEY ([AcceptedPatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_PatientProviderInvitationDetails_PatientProviderInvitation] FOREIGN KEY ([InvitationID]) REFERENCES [dbo].[PatientProviderInvitation] ([InvitationID]),
    CONSTRAINT [FK_PatientProviderInvitationDetails_Provider] FOREIGN KEY ([AcceptedProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

