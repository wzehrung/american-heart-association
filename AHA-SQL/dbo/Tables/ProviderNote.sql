﻿CREATE TABLE [dbo].[ProviderNote] (
    [ProviderNoteID] INT            IDENTITY (1, 1) NOT NULL,
    [ProviderID]     INT            NOT NULL,
    [PatientID]      INT            NOT NULL,
    [CreationDate]   DATETIME       NOT NULL,
    [Title]          NVARCHAR (128) NOT NULL,
    [Note]           NVARCHAR (MAX) NULL,
    [IsDeleted]      BIT            CONSTRAINT [DF_ProviderNote_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ProviderNote] PRIMARY KEY CLUSTERED ([ProviderNoteID] ASC),
    CONSTRAINT [FK_ProviderNote_HealthRecord] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_ProviderNote_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

