﻿CREATE TABLE [dbo].[Exercise] (
    [ExerciseID]         INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [DateofSession]      DATETIME         NULL,
    [ExerciseType]       NVARCHAR (50)    NULL,
    [Intensity]          NVARCHAR (50)    NULL,
    [Duration]           FLOAT (53)       NULL,
    [Comments]           NVARCHAR (MAX)   NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_Exercise_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_Exercise_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [NumberOfSteps]      INT              NULL,
    [ReadingSource]      NVARCHAR (100)   NULL,
    CONSTRAINT [PK_Exercise] PRIMARY KEY CLUSTERED ([ExerciseID] ASC),
    CONSTRAINT [FK_Exercise_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_Exercise_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

