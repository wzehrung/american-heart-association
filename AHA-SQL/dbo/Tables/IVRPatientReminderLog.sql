﻿CREATE TABLE [dbo].[IVRPatientReminderLog] (
    [ReminderScheduleLogID] INT            IDENTITY (1, 1) NOT NULL,
    [PatientID]             INT            NOT NULL,
    [Responded]             CHAR (1)       NULL,
    [RetryAttempts]         INT            NULL,
    [FinalAction]           NVARCHAR (255) NULL,
    [ActionDate]            DATETIME       NOT NULL,
    [ActionSource]          NVARCHAR (255) NOT NULL,
    [CreatedDateTime]       DATETIME       CONSTRAINT [DF_IVRPatientReminderLog_CreatedDateTime] DEFAULT (getdate()) NOT NULL,
    [UpdatedDateTime]       NCHAR (10)     NULL,
    CONSTRAINT [PK_IVRPatientReminderLog] PRIMARY KEY CLUSTERED ([ReminderScheduleLogID] ASC)
);

