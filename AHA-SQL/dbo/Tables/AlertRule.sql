﻿CREATE TABLE [dbo].[AlertRule] (
    [AlertRuleID]          INT            IDENTITY (1, 1) NOT NULL,
    [ProviderID]           INT            NOT NULL,
    [Type]                 NVARCHAR (50)  NULL,
    [AlertData]            XML            NULL,
    [IsActive]             BIT            CONSTRAINT [DF_AlertRule_IsActive] DEFAULT ((1)) NULL,
    [SendMessageToPatient] BIT            NULL,
    [GroupID]              INT            NULL,
    [MessageToPatient]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Alert] PRIMARY KEY CLUSTERED ([AlertRuleID] ASC),
    CONSTRAINT [FK_AlertRule_PatientGroupDetails] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[PatientGroupDetails] ([GroupID]),
    CONSTRAINT [FK_AlertRule_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

