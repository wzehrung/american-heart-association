﻿CREATE TABLE [dbo].[ResentInvitation] (
    [ResentInvitationID]  INT      IDENTITY (1, 1) NOT NULL,
    [InvitationDetailsID] INT      NULL,
    [DateResent]          DATETIME CONSTRAINT [DF_ResentInvitation_DateResent] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ResentInvitation] PRIMARY KEY CLUSTERED ([ResentInvitationID] ASC),
    CONSTRAINT [FK_ResentInvitation_PatientProviderInvitationDetails] FOREIGN KEY ([InvitationDetailsID]) REFERENCES [dbo].[PatientProviderInvitationDetails] ([InvitationDetailsID])
);

