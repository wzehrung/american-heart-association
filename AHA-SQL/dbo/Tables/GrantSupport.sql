﻿CREATE TABLE [dbo].[GrantSupport] (
    [GrantSupportID]  INT             NOT NULL,
    [Title]           NVARCHAR (128)  NOT NULL,
    [Description]     NVARCHAR (MAX)  NULL,
    [Logo]            VARBINARY (MAX) NULL,
    [ContentType]     NVARCHAR (10)   NULL,
    [IsForProvider]   BIT             CONSTRAINT [DF_GrantSupport_IsForProvider] DEFAULT ((0)) NOT NULL,
    [Version]         NVARCHAR (50)   NULL,
    [CreatedByUserID] INT             NULL,
    [UpdatedByUserID] INT             NULL,
    [LanguageID]      INT             NOT NULL,
    CONSTRAINT [PK_GrantSupport] PRIMARY KEY CLUSTERED ([GrantSupportID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_GrantSupport_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO

create TRIGGER Trig_GrantSupport_Log
ON GrantSupport
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into GrantSupport_Log 
			(
			GrantSupportID,Title,Description,Logo,ContentType,IsForProvider,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select	
			GrantSupportID,Title,Description,Logo,ContentType,IsForProvider,Version,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID
			 from deleted

			union all

			select			
			GrantSupportID,Title,Description,Logo,ContentType,IsForProvider,Version,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID
			 from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into GrantSupport_Log 
			(
			GrantSupportID,Title,Description,Logo,ContentType,IsForProvider,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			GrantSupportID,Title,Description,Logo,ContentType,IsForProvider,Version,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into GrantSupport_Log 
			(
			GrantSupportID,Title,Description,Logo,ContentType,IsForProvider,Version,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			GrantSupportID,Title,Description,Logo,ContentType,IsForProvider,Version,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID
			from inserted

		end

END
