﻿CREATE TABLE [dbo].[ProviderContent_Log] (
    [ProviderContent_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [ContentID]             INT              NOT NULL,
    [ZoneTitle]             NVARCHAR (50)    NOT NULL,
    [ContentTitle]          NVARCHAR (80)    NULL,
    [ContentTitleUrl]       NVARCHAR (255)   NULL,
    [ContentText]           NVARCHAR (MAX)   NULL,
    [CreatedByUserID]       INT              NULL,
    [UpdatedByUserID]       INT              NULL,
    [ModifiedDate]          DATETIME         CONSTRAINT [DF_ProviderContent_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]                TINYINT          NOT NULL,
    [TransactionID]         UNIQUEIDENTIFIER NOT NULL,
    [RowType]               NVARCHAR (50)    NULL,
    [User]                  NVARCHAR (50)    NOT NULL,
    [LanguageID]            INT              NOT NULL,
    [ProviderID]            INT              NULL,
    CONSTRAINT [PK_ProviderContent_Log] PRIMARY KEY CLUSTERED ([ProviderContent_LogID] ASC)
);

