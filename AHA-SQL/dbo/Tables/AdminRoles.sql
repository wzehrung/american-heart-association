﻿CREATE TABLE [dbo].[AdminRoles] (
    [RoleID]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (200) NULL,
    CONSTRAINT [PK_AdminRoles] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);

