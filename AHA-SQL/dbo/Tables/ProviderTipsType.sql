﻿CREATE TABLE [dbo].[ProviderTipsType] (
    [ProviderTipsTypeID] INT            NOT NULL,
    [ProviderTipsType]   NVARCHAR (100) NOT NULL,
    [DisplayName]        NVARCHAR (100) NULL,
    [LanguageID]         INT            NOT NULL,
    CONSTRAINT [PK_ProviderTipsType] PRIMARY KEY CLUSTERED ([ProviderTipsTypeID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ProviderTipsType_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);

