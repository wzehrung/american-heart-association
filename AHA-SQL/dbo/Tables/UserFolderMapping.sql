﻿CREATE TABLE [dbo].[UserFolderMapping] (
    [UserFolderMappingID] INT IDENTITY (1, 1) NOT NULL,
    [PatientID]           INT NULL,
    [ProviderID]          INT NULL,
    [FolderID]            INT NOT NULL,
    CONSTRAINT [PK_UserFolderMapping] PRIMARY KEY CLUSTERED ([UserFolderMappingID] ASC),
    CONSTRAINT [FK_UserFolderMapping_Folder] FOREIGN KEY ([FolderID]) REFERENCES [dbo].[Folder] ([FolderID]),
    CONSTRAINT [FK_UserFolderMapping_HealthRecord] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [FK_UserFolderMapping_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

