﻿CREATE TABLE [dbo].[Alert] (
    [AlertID]              INT              IDENTITY (1, 1) NOT NULL,
    [AlertRuleID]          INT              NOT NULL,
    [HealthRecordItemGUID] UNIQUEIDENTIFIER NOT NULL,
    [PatientID]            INT              NOT NULL,
    [IsDismissed]          BIT              CONSTRAINT [DF_Alert_IsDismissed] DEFAULT ((0)) NOT NULL,
    [CreatedDate]          DATETIME         CONSTRAINT [DF_Alert_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]          DATETIME         CONSTRAINT [DF_Alert_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [AlertXml]             XML              NULL,
    [AlertRuleXml]         XML              NULL,
    [Notes]                NVARCHAR (MAX)   NULL,
    [ItemEffectiveDate]    DATETIME         NULL,
    CONSTRAINT [PK_Alert1] PRIMARY KEY CLUSTERED ([AlertID] ASC),
    CONSTRAINT [FK_Alert_HealthRecord] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID])
);

