﻿CREATE TABLE [dbo].[Content] (
    [ContentID]           INT             NOT NULL,
    [Title]               NVARCHAR (250)  NOT NULL,
    [DidYouKnowLinkUrl]   NVARCHAR (1024) NOT NULL,
    [DidYouKnowLinkTitle] NVARCHAR (MAX)  NOT NULL,
    [DidYouKnowText]      NVARCHAR (MAX)  NOT NULL,
    [ExpirationDate]      DATETIME        NULL,
    [CreatedByUserID]     INT             NULL,
    [UpdatedByUserID]     INT             NULL,
    [LanguageID]          INT             NOT NULL,
    CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED ([ContentID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_Content_Languages] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);


GO

create TRIGGER Trig_Content_Log
ON Content
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into Content_Log 
			(
			ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID
			 from deleted

			union all

			select			
			ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID
			  from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into Content_Log 
			(
			ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into Content_Log 
			(
			ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ContentID,Title,DidYouKnowLinkUrl,DidYouKnowLinkTitle,DidYouKnowText,ExpirationDate,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID
			from inserted

		end

END
