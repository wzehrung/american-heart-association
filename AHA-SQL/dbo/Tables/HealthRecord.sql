﻿CREATE TABLE [dbo].[HealthRecord] (
    [UserHealthRecordID]       INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordGUID]     UNIQUEIDENTIFIER NULL,
    [PersonalItemGUID]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]              DATETIME         CONSTRAINT [DFC_HealthRecord_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]              DATETIME         CONSTRAINT [DFC_HealthRecord_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [IsMigrated]               BIT              CONSTRAINT [DFC_HealthRecord_IsMigrated] DEFAULT ((0)) NOT NULL,
    [HasAgreedProviderTOU]     BIT              NULL,
    [HasProvidedOfflineAccess] BIT              NULL,
    [OfflinePersonGUID]        UNIQUEIDENTIFIER NULL,
    [LastActivityDate]         DATETIME         NULL,
    [CampaignID]               INT              NULL,
    [OfflineHealthRecordGUID]  UNIQUEIDENTIFIER NULL,
    [PolkaID]                  NCHAR (6)        NULL,
    CONSTRAINT [PK_UserRecords] PRIMARY KEY CLUSTERED ([UserHealthRecordID] ASC),
    CONSTRAINT [FK_HealthRecord_Campaign] FOREIGN KEY ([CampaignID]) REFERENCES [dbo].[Campaign] ([CampaignID]),
    CONSTRAINT [UQC_HealthRecord_PersonalItemGUID] UNIQUE NONCLUSTERED ([PersonalItemGUID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [AK_UserRecords]
    ON [dbo].[HealthRecord]([UserHealthRecordGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [HealthRecord_idx1]
    ON [dbo].[HealthRecord]([CampaignID] ASC);

