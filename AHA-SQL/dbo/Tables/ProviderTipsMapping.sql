﻿CREATE TABLE [dbo].[ProviderTipsMapping] (
    [ProviderTipsID]     INT NOT NULL,
    [ProviderTipsTypeID] INT NOT NULL,
    [CreatedByUserID]    INT NULL,
    [UpdatedByUserID]    INT NULL,
    [LanguageID]         INT CONSTRAINT [DF_ProviderTipsMapping_LanguageID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ProviderTipMapping] PRIMARY KEY CLUSTERED ([ProviderTipsID] ASC, [ProviderTipsTypeID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ProviderTipsMapping_ProviderTips] FOREIGN KEY ([ProviderTipsID], [LanguageID]) REFERENCES [dbo].[ProviderTips] ([ProviderTipsID], [LanguageID]),
    CONSTRAINT [FK_ProviderTipsMapping_ProviderTipsType] FOREIGN KEY ([ProviderTipsTypeID], [LanguageID]) REFERENCES [dbo].[ProviderTipsType] ([ProviderTipsTypeID], [LanguageID])
);


GO

CREATE TRIGGER Trig_ProviderTipsMapping_Log
ON ProviderTipsMapping
FOR INSERT,UPDATE,DELETE

AS
BEGIN


declare @RowID uniqueidentifier
-- Updations

		if( exists ( select * from inserted )and exists (select * from deleted))
		begin
			
			set @RowID=newID()
			insert into ProviderTipsMapping_Log 
			(
			ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select	
			ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,1,@RowID,'oldRow',SYSTEM_USER,LanguageID
			 from deleted

			union all

			select			
			ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,1,@RowID,'NewRow',SYSTEM_USER,LanguageID
			 from inserted

		end

--Deletions

		else if exists (select * from deleted) and not exists(select * from inserted)
		begin


			
			set @RowID=newID()

			insert into ProviderTipsMapping_Log 
			(
			ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,2,@RowID,null,SYSTEM_USER,LanguageID
			 from deleted


		end

--insertions
		else if exists (select * from inserted) and not exists (select * from deleted)
		begin
				
			set @RowID=newID()
			insert into ProviderTipsMapping_Log 
			(
			ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,[Action],TransactionID,RowType,[User],LanguageID
			)
			select			
			ProviderTipsID,ProviderTipsTypeID,CreatedByUserID,UpdatedByUserID,0,@RowID,null,SYSTEM_USER,LanguageID
			from inserted

		end

END
