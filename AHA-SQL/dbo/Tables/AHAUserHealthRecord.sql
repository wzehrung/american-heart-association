﻿CREATE TABLE [dbo].[AHAUserHealthRecord] (
    [UserID]             INT      NOT NULL,
    [UserHealthRecordID] INT      NOT NULL,
    [CreatedDate]        DATETIME CONSTRAINT [DFC_AHAUserHealthRecord_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME CONSTRAINT [DFC_AHAUserHealthRecord_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [DashboardSortOrder] XML      NULL,
    [TrackerDetails]     XML      NULL,
    CONSTRAINT [PK_AHAUserUserRecord] PRIMARY KEY CLUSTERED ([UserID] ASC, [UserHealthRecordID] ASC),
    CONSTRAINT [FK_AHAUserHealthRecord_AHAUser] FOREIGN KEY ([UserID]) REFERENCES [dbo].[AHAUser] ([UserID]),
    CONSTRAINT [FK_AHAUserHealthRecord_UserRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID])
);

