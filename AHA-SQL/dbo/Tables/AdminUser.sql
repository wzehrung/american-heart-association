﻿CREATE TABLE [dbo].[AdminUser] (
    [AdminUserID]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (128) NOT NULL,
    [UserName]          NVARCHAR (128) NOT NULL,
    [Password]          NVARCHAR (50)  NOT NULL,
    [Email]             NVARCHAR (128) NOT NULL,
    [IsActive]          BIT            CONSTRAINT [DF_AdminUser_IsActive] DEFAULT ((1)) NOT NULL,
    [IsSuperAdmin]      BIT            CONSTRAINT [DF_AdminUser_IsSuperAdmin] DEFAULT ((0)) NOT NULL,
    [DefaultLanguageID] INT            NOT NULL,
    [RoleID]            INT            NULL,
    [BusinessID]        INT            NULL,
    [FeedbackRecipient] BIT            CONSTRAINT [DF_AdminUser_FeedbackRecipient] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AdminUser] PRIMARY KEY CLUSTERED ([AdminUserID] ASC),
    CONSTRAINT [FK_AdminUser_Languages] FOREIGN KEY ([DefaultLanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);

