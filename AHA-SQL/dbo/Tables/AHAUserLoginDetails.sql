﻿CREATE TABLE [dbo].[AHAUserLoginDetails] (
    [LoginDetailsID] INT      IDENTITY (1, 1) NOT NULL,
    [AHAUserID]      INT      NOT NULL,
    [LoggedInDate]   DATETIME CONSTRAINT [DFC_AHAUserLoginDetails_LoggedInDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AHAUserLoginDetails] PRIMARY KEY CLUSTERED ([LoginDetailsID] ASC),
    CONSTRAINT [FK_AHAUserLoginDetails_AHAUser] FOREIGN KEY ([AHAUserID]) REFERENCES [dbo].[AHAUser] ([UserID])
);

