﻿CREATE TABLE [dbo].[Campaign] (
    [CampaignID]   INT             IDENTITY (1, 1) NOT NULL,
    [CampaignCode] NVARCHAR (64)   NULL,
    [Url]          NVARCHAR (2048) NULL,
    [StartDate]    DATETIME        NOT NULL,
    [EndDate]      DATETIME        NOT NULL,
    [CreatedDate]  DATETIME        CONSTRAINT [DF_Campaign_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]  DATETIME        CONSTRAINT [DF_Campaign_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [BusinessID]   INT             NULL,
    CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED ([CampaignID] ASC),
    CONSTRAINT [FK_Campaign_AdminBusiness] FOREIGN KEY ([BusinessID]) REFERENCES [dbo].[AdminBusiness] ([BusinessID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_url]
    ON [dbo].[Campaign]([Url] ASC);

