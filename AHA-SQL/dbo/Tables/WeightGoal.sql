﻿CREATE TABLE [dbo].[WeightGoal] (
    [WeightGoalId]       INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [StartWeight]        FLOAT (53)       NOT NULL,
    [StartDate]          DATETIME         NOT NULL,
    [TargetWeight]       FLOAT (53)       NOT NULL,
    [TargetDate]         DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_WeightGoal_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_WeightGoal_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_WeightGoal] PRIMARY KEY CLUSTERED ([WeightGoalId] ASC),
    CONSTRAINT [FK_WeightGoal_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_WeightGoal_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

