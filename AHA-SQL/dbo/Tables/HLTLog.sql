﻿CREATE TABLE [dbo].[HLTLog] (
    [PatientID]    INT            NOT NULL,
    [ZipCode]      NCHAR (10)     NULL,
    [Campaign]     NVARCHAR (100) NULL,
    [BPSource]     NVARCHAR (100) NULL,
    [BPDate]       DATETIME       NULL,
    [FirstName]    NVARCHAR (50)  NULL,
    [LastName]     NVARCHAR (50)  NULL,
    [EmailAddress] NVARCHAR (200) NULL,
    [Error]        NVARCHAR (500) NULL,
    [DOB]          NVARCHAR (25)  NULL,
    [YearOfBirth]  NVARCHAR (50)  NULL
);

