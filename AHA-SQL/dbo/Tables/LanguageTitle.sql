﻿CREATE TABLE [dbo].[LanguageTitle] (
    [FromLanguageID] INT            NOT NULL,
    [ToLanguageID]   INT            NULL,
    [LanguageTitle]  NVARCHAR (250) NOT NULL,
    CONSTRAINT [FK_LanguageTitle_Languages] FOREIGN KEY ([FromLanguageID]) REFERENCES [dbo].[Languages] ([LanguageID]),
    CONSTRAINT [FK_LanguageTitle_Languages1] FOREIGN KEY ([ToLanguageID]) REFERENCES [dbo].[Languages] ([LanguageID])
);

