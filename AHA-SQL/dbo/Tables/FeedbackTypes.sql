﻿CREATE TABLE [dbo].[FeedbackTypes] (
    [FeedbackTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [FeedbackTypeName] NVARCHAR (128) NOT NULL,
    [Enabled]          BIT            CONSTRAINT [DF_FeedbackTypes_Enabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_FeedbackTypes] PRIMARY KEY CLUSTERED ([FeedbackTypeID] ASC)
);

