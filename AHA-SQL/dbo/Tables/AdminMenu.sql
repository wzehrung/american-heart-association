﻿CREATE TABLE [dbo].[AdminMenu] (
    [MenuID]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (200) NULL,
    [CategoryID]  INT            NOT NULL,
    [SortID]      INT            NULL,
    CONSTRAINT [PK_AdminMenu] PRIMARY KEY CLUSTERED ([MenuID] ASC)
);

