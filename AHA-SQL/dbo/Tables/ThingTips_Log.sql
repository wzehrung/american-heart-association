﻿CREATE TABLE [dbo].[ThingTips_Log] (
    [ThingTips_LogID] INT              IDENTITY (1, 1) NOT NULL,
    [ThingTipsId]     INT              NOT NULL,
    [ThingTipsData]   NVARCHAR (MAX)   NOT NULL,
    [CreatedByUserID] INT              NULL,
    [UpdatedByUserID] INT              NULL,
    [ModifiedDate]    DATETIME         CONSTRAINT [DF_ThingTips_Log_Date] DEFAULT (getdate()) NOT NULL,
    [Action]          TINYINT          NOT NULL,
    [TransactionID]   UNIQUEIDENTIFIER NOT NULL,
    [RowType]         NVARCHAR (50)    NULL,
    [User]            NVARCHAR (50)    NOT NULL,
    [LanguageID]      INT              NOT NULL,
    CONSTRAINT [PK_ThingTips_Log] PRIMARY KEY CLUSTERED ([ThingTips_LogID] ASC)
);

