﻿CREATE TABLE [dbo].[BloodGlucose] (
    [BloodGlucoseId]     INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID] INT              NOT NULL,
    [HVItemGuid]         UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]     UNIQUEIDENTIFIER NULL,
    [BloodGlucoseValue]  FLOAT (53)       NOT NULL,
    [ActionITook]        NVARCHAR (100)   NULL,
    [ReadingType]        NVARCHAR (100)   NULL,
    [DateMeasured]       DATETIME         NOT NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DFC_BloodGlucose_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]        DATETIME         CONSTRAINT [DFC_BloodGlucose_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    [Notes]              NVARCHAR (MAX)   NULL,
    [ReadingSource]      NVARCHAR (100)   NULL,
    CONSTRAINT [PK_BloodGlucose] PRIMARY KEY CLUSTERED ([BloodGlucoseId] ASC),
    CONSTRAINT [FK_BloodGlucose_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_BloodGlucose_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

