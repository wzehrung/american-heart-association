﻿CREATE TABLE [dbo].[PatientGroupDetails] (
    [GroupID]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (50)  NOT NULL,
    [Description]       NVARCHAR (MAX) NULL,
    [ProviderID]        INT            NOT NULL,
    [DateCreated]       DATETIME       CONSTRAINT [DF_PatientGroupDetails_DateCreated] DEFAULT (getdate()) NOT NULL,
    [isDefault]         BIT            CONSTRAINT [DF_PatientGroupDetails_isDefault] DEFAULT ((0)) NOT NULL,
    [GroupRuleXml]      XML            NULL,
    [IsSystemGenerated] BIT            CONSTRAINT [DF_PatientGroupDetails_IsSystemGenerated] DEFAULT ((0)) NOT NULL,
    [RemoveOption]      CHAR (1)       NULL,
    CONSTRAINT [PK_PatientGroupDetails] PRIMARY KEY CLUSTERED ([GroupID] ASC),
    CONSTRAINT [FK_PatientGroupDetails_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[Provider] ([ProviderID])
);

