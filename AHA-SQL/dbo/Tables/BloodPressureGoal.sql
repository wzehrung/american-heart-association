﻿CREATE TABLE [dbo].[BloodPressureGoal] (
    [BloodPressureGoalId] INT              IDENTITY (1, 1) NOT NULL,
    [UserHealthRecordID]  INT              NOT NULL,
    [HVItemGuid]          UNIQUEIDENTIFIER NOT NULL,
    [HVVersionStamp]      UNIQUEIDENTIFIER NULL,
    [TargetSystolic]      INT              NOT NULL,
    [TargetDiastolic]     INT              NOT NULL,
    [CreatedDate]         DATETIME         CONSTRAINT [DFC_BloodPressureGoal_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedDate]         DATETIME         CONSTRAINT [DFC_BloodPressureGoal_UpdatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_BloodPressureGoal] PRIMARY KEY CLUSTERED ([BloodPressureGoalId] ASC),
    CONSTRAINT [FK_BloodPressureGoal_HealthRecord] FOREIGN KEY ([UserHealthRecordID]) REFERENCES [dbo].[HealthRecord] ([UserHealthRecordID]),
    CONSTRAINT [UQC_BloodPressureGoal_HVItemGUID] UNIQUE NONCLUSTERED ([HVItemGuid] ASC)
);

