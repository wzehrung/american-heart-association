﻿
-- =============================================
-- Author:		Phil Brock
-- Create date: June 12, 2013
-- Description:	Need a function to split CSV string values into a table
-- =============================================
CREATE FUNCTION [dbo].[udf_ListToTableString] 
(
	@list as varchar(8000), 
	@delim as varchar(10)
)
RETURNS 
@listTable TABLE 
(
	Value varchar(200)
)
AS
BEGIN
	--Declare helper to identify the position of the delim
    DECLARE @DelimPosition INT
    
    --Prime the loop, with an initial check for the delim
    SET @DelimPosition = CHARINDEX(@delim, @list)

    --Loop through, until we no longer find the delimiter
    WHILE @DelimPosition > 0
    BEGIN
        --Add the item to the table
        INSERT INTO @listTable(Value)
            VALUES(RTRIM(LEFT(@list, @DelimPosition - 1)))
    
        --Remove the entry from the List
        SET @list = right(@list, len(@list) - @DelimPosition)

        --Perform position comparison
        SET @DelimPosition = CHARINDEX(@delim, @list)
    END

    --If we still have an entry, add it to the list
    IF len(@list) > 0
        insert into @listTable(Value)
        values(RTRIM(@list))
	
	RETURN 
END

