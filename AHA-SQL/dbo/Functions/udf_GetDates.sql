﻿
CREATE Function [dbo].[udf_GetDates]
(
 @StartDate datetime
,@enddate datetime
)
RETURNS TABLE

AS

RETURN

with Dates (Id,Date)As
(
select 1,Date = @StartDate 
union all 
select Id=Id+1,Date=dateadd(month,1,Date)  from  Dates where Date < dateadd(month,-1,@enddate)
)

SELECT * FROM DATES


