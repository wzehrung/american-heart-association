﻿
CREATE FUNCTION dbo.fnMultiValuedParameterAsTable (@MultiValuedParameter nvarchar(max)) 
RETURNS @Values TABLE (Parameter nvarchar(4000))AS 
BEGIN

	DECLARE @chrind INT 
	DECLARE @Piece nvarchar(100)

	SELECT @chrind = 1 
	
	WHILE @chrind > 0
	BEGIN
		SELECT @chrind = CHARINDEX(',',@MultiValuedParameter)
		
		IF @chrind > 0
		BEGIN
			SELECT @Piece = LEFT(@MultiValuedParameter,@chrind - 1)
		END
		ELSE
		BEGIN
			SELECT @Piece = @MultiValuedParameter
		END
		
		INSERT @Values(Parameter) VALUES(CAST(@Piece AS VARCHAR))
		SELECT @MultiValuedParameter = RIGHT(@MultiValuedParameter,LEN(@MultiValuedParameter) - @chrind)
	
		IF LEN(@MultiValuedParameter) = 0 BREAK
	END 
	   
	RETURN 
END

