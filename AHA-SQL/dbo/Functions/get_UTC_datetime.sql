﻿
-- This function returns the datetime in UTC timezone, if invalid time zone id is passed to the function it returns

CREATE function [dbo].[get_UTC_datetime]
	(@date datetime, @time_zone varchar(25))
returns datetime as

BEGIN
	declare @local_time datetime
	declare @offset_time int
	select @offset_time = offset from timezone_offsets where @date between start_time_gmt and end_time_gmt and time_zone_id = @time_zone
	set @local_time = dateadd(ms, isnull(@offset_time,0), @date)
	return @local_time
END