﻿

CREATE Function [dbo].[udf_GetDatesWithRange]
(
 @StartDate datetime
,@enddate datetime
)
RETURNS TABLE

AS

RETURN

with Dates (Id,Date)As
(
select 1,Date = @StartDate 
union all 
select Id=Id+1,Date=dateadd(month,1,Date)  from  Dates where Date<=dateadd(month,-1,@enddate)
)

SELECT Id,Date as Date1 ,dateadd(month,1,Date) as Date2  FROM DATES

